﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ReportExecution2005;
using ReportService2010;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

namespace Reporting
{
    public class ReportManager
    {
        public static CatalogItem[] GetReportsList(string argPath)
        {
            ReportingService2010 rs = new ReportingService2010();
            rs.Url = ConfigurationManager.AppSettings["REPORTSERVER_NAME"];
            rs.CookieContainer = new CookieContainer();
            rs.LogonUser(ConfigurationManager.AppSettings["REPORTUSERNAME"],
                ConfigurationManager.AppSettings["REPORTPASSWORD"],
                ConfigurationManager.AppSettings["REPORTSERVER_NAME"]);
            CatalogItem[] items = rs.ListChildren(String.Format("/{0}", argPath), true).Where(item => item.TypeName == "Report").ToArray();
            return items;
        }

        public static byte[] GetReport(string argReportPath, string argName, ReportExecution2005.ParameterValue[] argParameters, string argFormat)
        {
            ReportExecutionService rsExec = new ReportExecutionService();
            rsExec.Url = ConfigurationManager.AppSettings["REPORTSERVER_NAME"];
            rsExec.CookieContainer = new CookieContainer();

            //Use this syntax for Azure Report Service
            //rsExec.LogonUser(ConfigurationManager.AppSettings["REPORTUSERNAME"],
            //    ConfigurationManager.AppSettings["REPORTPASSWORD"],
            //    ConfigurationManager.AppSettings["REPORTDOMAIN"]);

            //Use this syntax for Standart Report Service
            NetworkCredential creds = new NetworkCredential(ConfigurationManager.AppSettings["REPORTUSERNAME"], ConfigurationManager.AppSettings["REPORTPASSWORD"],
                                                            ConfigurationManager.AppSettings["REPORTDOMAIN"]);

            rsExec.Credentials = creds;
            byte[] tmpResult = null;
            string tmpFullPath = String.Format("/{0}/{1}", argReportPath, argName);
            string tmpHistoryID = null;
            // Setting the deviceinfo parameters
            string deviceInfo = @"<DeviceInfo><FieldDelimiter>;</FieldDelimiter></DeviceInfo>"; ;
            string extension;
            string encoding;
            string mimeType;
            ReportExecution2005.Warning[] warnings = null;
            string[] streamIDs = null;

            ExecutionInfo execInfo = new ExecutionInfo();
            ExecutionHeader execHeader = new ExecutionHeader();
            rsExec.ExecutionHeaderValue = execHeader;

            execInfo = rsExec.LoadReport(tmpFullPath, tmpHistoryID);
            String SessionId = rsExec.ExecutionHeaderValue.ExecutionID;

            // code snippet to set the DataSource credentials.
            if (execInfo.CredentialsRequired)
            {
                List<ReportExecution2005.DataSourceCredentials> credentials = new List<ReportExecution2005.DataSourceCredentials>();
                foreach (ReportExecution2005.DataSourcePrompt dsp in execInfo.DataSourcePrompts)
                {
                    ReportExecution2005.DataSourceCredentials cred = new ReportExecution2005.DataSourceCredentials();
                    cred.DataSourceName = dsp.Name;
                    cred.UserName = ConfigurationManager.AppSettings["REPORTUSERNAME"];
                    cred.Password = ConfigurationManager.AppSettings["REPORTPASSWORD"];
                    credentials.Add(cred);
                }

                Console.WriteLine("Setting data source credentials...");
                execInfo = rsExec.SetExecutionCredentials(credentials.ToArray());
            }

            rsExec.SetExecutionParameters(argParameters, "en-us");

            // Code snippet to render the file to a specified format.
            try
            {
                tmpResult = rsExec.Render(argFormat, deviceInfo, out extension, out mimeType, out encoding, out warnings, out streamIDs);
                execInfo = rsExec.GetExecutionInfo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tmpResult;
        }

        public static void SaveReport(string argReportPath, string argName, ReportExecution2005.ParameterValue[] argParameters, string argFormat, string argSavedFileFullPath, string argSavedFileName)
        {
            Byte[] tmpReportData = ReportManager.GetReport(argReportPath, argName, argParameters, argFormat);

            string tmpNewFileName = argSavedFileName;

            string tmpFullPath = Path.Combine(argSavedFileFullPath, tmpNewFileName);

            if (!File.Exists(tmpFullPath))
            {
                try
                {
                    FileStream stream = File.Create(tmpFullPath, tmpReportData.Length);
                    stream.Write(tmpReportData, 0, tmpReportData.Length);
                    stream.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static byte[][] RenderReportPages(string argReportPath, string argName, ReportExecution2005.ParameterValue[] argParameters)
        {
            ReportExecutionService rsExec = new ReportExecutionService();
            rsExec.Url = ConfigurationManager.AppSettings["REPORTSERVER_NAME"];
            rsExec.CookieContainer = new CookieContainer();
            NetworkCredential creds = new NetworkCredential(ConfigurationManager.AppSettings["REPORTUSERNAME"], ConfigurationManager.AppSettings["REPORTPASSWORD"],
                                                        ConfigurationManager.AppSettings["REPORTDOMAIN"]);

            rsExec.Credentials = creds;

            string deviceInfo = String.Format(@"<DeviceInfo><OutputFormat>{0}</OutputFormat></DeviceInfo>", "emf");
            string format = "IMAGE";
            Byte[] firstPage = null;
            string encoding;
            string mimeType;
            ReportExecution2005.Warning[] warnings = null;
            string[] streamIDs = null;
            byte[][] tmpResult = null;

            string tmpFullPath = String.Format("/{0}/{1}", argReportPath, argName);
            string tmpHistoryID = null;
            string extension;

            ExecutionInfo execInfo = new ExecutionInfo();
            ExecutionHeader execHeader = new ExecutionHeader();
            rsExec.ExecutionHeaderValue = execHeader;

            execInfo = rsExec.LoadReport(tmpFullPath, tmpHistoryID);
            String SessionId = rsExec.ExecutionHeaderValue.ExecutionID;

            // code snippet to set the DataSource credentials.
            if (execInfo.CredentialsRequired)
            {
                List<ReportExecution2005.DataSourceCredentials> credentials = new List<ReportExecution2005.DataSourceCredentials>();
                foreach (ReportExecution2005.DataSourcePrompt dsp in execInfo.DataSourcePrompts)
                {
                    ReportExecution2005.DataSourceCredentials cred = new ReportExecution2005.DataSourceCredentials();
                    cred.DataSourceName = dsp.Name;
                    cred.UserName = ConfigurationManager.AppSettings["REPORTUSERNAME"];
                    cred.Password = ConfigurationManager.AppSettings["REPORTPASSWORD"];
                    credentials.Add(cred);
                }

                Console.WriteLine("Setting data source credentials...");
                execInfo = rsExec.SetExecutionCredentials(credentials.ToArray());
            }

            rsExec.SetExecutionParameters(argParameters, "en-us");

            try
            {
                firstPage = rsExec.Render(format, deviceInfo, out extension, out mimeType, out encoding, out warnings, out streamIDs);
                execInfo = rsExec.GetExecutionInfo();

                int tmpNumberOfPages = streamIDs.Length + 1;
                tmpResult = new byte[tmpNumberOfPages][];

                tmpResult[0] = firstPage;

                for (int pageIndex = 1; pageIndex < tmpNumberOfPages; pageIndex++)
                {
                    deviceInfo = String.Format(@"<DeviceInfo><OutputFormat>{0}</OutputFormat><StartPage>{1}</StartPage></DeviceInfo>", "emf", pageIndex + 1);
                    tmpResult[pageIndex] = rsExec.Render(format, deviceInfo, out extension, out mimeType, out encoding, out warnings, out streamIDs);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tmpResult;
        }

        /// <summary>
        /// RenderReportPages. Overload for HostPlus.Service task.
        /// </summary>
        /// <param name="argReportPath"></param>
        /// <param name="argName"></param>
        /// <param name="argParameters"></param>
        /// <param name="argReportServer"></param>
        /// <param name="argReportUser"></param>
        /// <param name="argReportPassword"></param>
        /// <returns></returns>
        public static byte[][] RenderReportPages(string argReportPath, string argName, ReportExecution2005.ParameterValue[] argParameters, string argReportServer, string argReportUser, string argReportPassword)
        {
            ReportExecutionService rsExec = new ReportExecutionService();
            rsExec.Url = ConfigurationManager.AppSettings["REPORTSERVER_NAME"];
            rsExec.CookieContainer = new CookieContainer();
            NetworkCredential creds = new NetworkCredential(ConfigurationManager.AppSettings["REPORTUSERNAME"], ConfigurationManager.AppSettings["REPORTPASSWORD"],
                                                       ConfigurationManager.AppSettings["REPORTDOMAIN"]);

            rsExec.Credentials = creds;

            string deviceInfo = String.Format(@"<DeviceInfo><OutputFormat>{0}</OutputFormat></DeviceInfo>", "emf");
            string format = "IMAGE";
            Byte[] firstPage = null;
            string encoding;
            string mimeType;
            ReportExecution2005.Warning[] warnings = null;
            string[] streamIDs = null;
            byte[][] tmpResult = null;

            string tmpFullPath = String.Format("/{0}/{1}", argReportPath, argName);
            string tmpHistoryID = null;
            string extension;

            ExecutionInfo execInfo = new ExecutionInfo();
            ExecutionHeader execHeader = new ExecutionHeader();
            rsExec.ExecutionHeaderValue = execHeader;

            execInfo = rsExec.LoadReport(tmpFullPath, tmpHistoryID);
            String SessionId = rsExec.ExecutionHeaderValue.ExecutionID;

            // code snippet to set the DataSource credentials.
            if (execInfo.CredentialsRequired)
            {
                List<ReportExecution2005.DataSourceCredentials> credentials = new List<ReportExecution2005.DataSourceCredentials>();
                foreach (ReportExecution2005.DataSourcePrompt dsp in execInfo.DataSourcePrompts)
                {
                    ReportExecution2005.DataSourceCredentials cred = new ReportExecution2005.DataSourceCredentials();
                    cred.DataSourceName = dsp.Name;
                    cred.UserName = argReportUser;
                    cred.Password = argReportPassword;
                    credentials.Add(cred);
                }

                Console.WriteLine("Setting data source credentials...");
                execInfo = rsExec.SetExecutionCredentials(credentials.ToArray());
            }

            rsExec.SetExecutionParameters(argParameters, "en-us");

            try
            {
                firstPage = rsExec.Render(format, deviceInfo, out extension, out mimeType, out encoding, out warnings, out streamIDs);
                execInfo = rsExec.GetExecutionInfo();

                int tmpNumberOfPages = streamIDs.Length + 1;
                tmpResult = new byte[tmpNumberOfPages][];

                tmpResult[0] = firstPage;

                for (int pageIndex = 1; pageIndex < tmpNumberOfPages; pageIndex++)
                {
                    deviceInfo = String.Format(@"<DeviceInfo><OutputFormat>{0}</OutputFormat><StartPage>{1}</StartPage></DeviceInfo>", "emf", pageIndex + 1);
                    tmpResult[pageIndex] = rsExec.Render(format, deviceInfo, out extension, out mimeType, out encoding, out warnings, out streamIDs);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tmpResult;
        }

        public static void PrintReport(string argReportPath, string argName, ReportExecution2005.ParameterValue[] argParameters, string argPrinterName, int argNoOfCopies)
        {
            Byte[][] tmpReport = ReportManager.RenderReportPages(argReportPath, argName, argParameters);
            ReportPrintingManager tmpPrintingManager = new ReportPrintingManager(tmpReport, argPrinterName, argNoOfCopies);
            tmpPrintingManager.PrintReport();
        }

        /// <summary>
        /// Print report. Overload for HostPlus.Service task.
        /// </summary>
        /// <param name="argReportPath"></param>
        /// <param name="argName"></param>
        /// <param name="argParameters"></param>
        /// <param name="argPrinterName"></param>
        /// <param name="argReportServer"></param>
        /// <param name="argReportUser"></param>
        /// <param name="argReportPassword"></param>
        public static void PrintReport(string argReportPath, string argName, ReportExecution2005.ParameterValue[] argParameters, string argPrinterName, int argNoOfCopies, string argReportServer, string argReportUser, string argReportPassword)
        {
            Byte[][] tmpReport = ReportManager.RenderReportPages(argReportPath, argName, argParameters, argReportServer, argReportUser, argReportPassword);
            ReportPrintingManager tmpPrintingManager = new ReportPrintingManager(tmpReport, argPrinterName, argNoOfCopies);
            tmpPrintingManager.PrintReport();
        }

        public static void PrintImage(byte[] image, string argPrinterName, int argNoOfCopies)
        {
            MemoryStream ms = new MemoryStream(image, 0, image.Length);

            ms.Write(image, 0, image.Length);
            Image img = Image.FromStream(ms, true);
            ReportPrintingManager tmpPrintingManager = new ReportPrintingManager(argPrinterName, argNoOfCopies);
            tmpPrintingManager.PrintImage(img);
        }
    }
}
