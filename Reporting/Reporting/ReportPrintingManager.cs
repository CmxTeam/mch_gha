﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Reporting
{
    internal class ReportPrintingManager
    {
        private byte[][] renderedReport;
        private Graphics.EnumerateMetafileProc m_delegate = null;
        private MemoryStream m_currentPageStream;
        private Metafile m_metafile = null;
        int m_numberOfPages;
        private int m_currentPrintingPage;
        private int m_lastPrintingPage;
        private string printerName;
        private int noOfCopies;
        private Image m_img;

        internal ReportPrintingManager(byte[][] argReport, string argPrinterName, int argNoOfCopies)
        {
            this.renderedReport = argReport;
            this.printerName = argPrinterName;
            this.noOfCopies = argNoOfCopies;
            this.m_numberOfPages = argReport.Length;
        }

        internal ReportPrintingManager(string argPrinterName, int argNoOfCopies)
        {
            this.printerName = argPrinterName;
            this.noOfCopies = argNoOfCopies;
            this.m_numberOfPages = 1;
        }

        // Method to draw the current emf memory stream 
        private void ReportDrawPage(Graphics g)
        {
            if (null == m_currentPageStream || 0 == m_currentPageStream.Length || null == m_metafile)
                return;
            lock (this)
            {
                // Set the metafile delegate.
                m_delegate = new Graphics.EnumerateMetafileProc(MetafileCallback);
                // Draw in the rectangle
                Point[] points = new Point[3];

                Point destPoint = new Point(0, 0);
                Point destPoint1 = new Point(859, 0);
                Point destPoint2 = new Point(0, 1118);

                points[0] = destPoint;
                points[1] = destPoint1;
                points[2] = destPoint2;
                g.EnumerateMetafile(m_metafile, points, m_delegate);
                // Clean up
                m_delegate = null;
            }
        }

        private bool MetafileCallback(EmfPlusRecordType recordType, int flags, int dataSize, IntPtr data, PlayRecordCallback callbackData)
        {
            byte[] dataArray = null;
            // Dance around unmanaged code.
            if (data != IntPtr.Zero)
            {
                // Copy the unmanaged record to a managed byte buffer 
                // that can be used by PlayRecord.
                dataArray = new byte[dataSize];
                Marshal.Copy(data, dataArray, 0, dataSize);
            }
            // play the record.      
            this.m_metafile.PlayRecord(recordType, flags, dataSize, dataArray);

            return true;
        }

        private bool MoveToPage(Int32 page)
        {
            // Check to make sure that the current page exists in
            // the array list
            if (null == this.renderedReport[m_currentPrintingPage - 1])
                return false;
            // Set current page stream equal to the rendered page
            this.m_currentPageStream = new MemoryStream(this.renderedReport[m_currentPrintingPage - 1]);
            // Set its postion to start.
            this.m_currentPageStream.Position = 0;
            // Initialize the metafile
            if (null != m_metafile)
            {
                this.m_metafile.Dispose();
                this.m_metafile = null;
            }
            // Load the metafile image for this page
            this.m_metafile = new Metafile((Stream)m_currentPageStream);
            return true;
        }

        public bool PrintReport()
        {
            for (int i = 0; i < this.noOfCopies; i++)
            {
                try
                {
                    // Wait for the report to completely render.
                    if (m_numberOfPages < 1)
                        return false;
                    PrinterSettings printerSettings = new PrinterSettings();
                    printerSettings.MaximumPage = m_numberOfPages;
                    printerSettings.MinimumPage = 1;
                    printerSettings.PrintRange = PrintRange.SomePages;
                    printerSettings.FromPage = 1;
                    printerSettings.ToPage = m_numberOfPages;
                    printerSettings.PrinterName = this.printerName;
                    PrintDocument pd = new PrintDocument();
                    this.m_currentPrintingPage = 1;
                    this.m_lastPrintingPage = m_numberOfPages;
                    pd.PrinterSettings = printerSettings;
                    // Print report
                    Console.WriteLine("Printing report...");
                    pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
                    pd.Print();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    // Clean up goes here.
                }
            }
            return true;
        }

        public bool PrintImage(Image img)
        {
            m_img = img;
            PrinterSettings printerSettings = new PrinterSettings();
            printerSettings.PrinterName = this.printerName;
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += PrintImagePage;
            pd.Print();
            return true;
        }

        private void PrintImagePage(object o, PrintPageEventArgs e)
        {
            Rectangle m = e.MarginBounds;

            if ((double)m_img.Width / (double)m_img.Height > (double)m.Width / (double)m.Height) // image is wider
            {
                m.Height = (int)((double)m_img.Height / (double)m_img.Width * (double)m.Width);
            }
            else
            {
                m.Width = (int)((double)m_img.Width / (double)m_img.Height * (double)m.Height);
            }

            e.Graphics.DrawImage(m_img, m);
            m_img.Dispose();
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            ev.HasMorePages = false;
            if (m_currentPrintingPage <= m_lastPrintingPage && MoveToPage(m_currentPrintingPage))
            {
                // Draw the page
                ReportDrawPage(ev.Graphics);
                // If the next page is less than or equal to the last page, 
                // print another page.
                if (++m_currentPrintingPage <= m_lastPrintingPage)
                    ev.HasMorePages = true;
            }
        }
    }
}
