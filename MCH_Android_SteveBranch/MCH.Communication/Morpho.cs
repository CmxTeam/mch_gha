﻿ 
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class Morpho
    {
        private static Morpho instance;
        private Morpho() {}

        public static Morpho Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new Morpho();
                }
                return instance;
            }
        }
 
//          SFO.C.01.28.2009.241
//          LAX.C.01.08.2009.568
//          LAX.C.11.01.2011.744
//          ATL.C.04.09.2009.151
//          IAH.C.06.09.2010.580
//          LAX.C.12.14.2009.970
//          JFK.C.10.22.2009.773

        
        public ScreeningInspectionRemarks GetScreeningInspectionRemarks(long warehouseId)
        {
 


            ScreeningInspectionRemarks result = new ScreeningInspectionRemarks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHScreening/GetScreeningInspectionRemarks");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            result = WebService.Instance.JsonGetMethod<ScreeningInspectionRemarks>(url,parameters);
            return result;

        }

        public CCSF ValidateCertificationNumber(string certificationNumber)
        {
            CCSF result = new CCSF();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHScreening/ValidateCertificationNumber");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("certificationNumber", certificationNumber);
            result = WebService.Instance.JsonGetMethod<CCSF>(url,parameters);
            return result;
        }
 
        public MorphoDevices GetScreeningDevices(long warehouseId, ScreeningDeviceTypes deviceType)
        {

            MorphoDevices result = new MorphoDevices();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHScreening/GetScreeningDevices");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            //parameters.Add("deviceType",((int)deviceType).ToString());
            //As per Aram
            parameters.Add("deviceType","1");
            parameters.Add("warehouseId", warehouseId.ToString());
            result = WebService.Instance.JsonGetMethod<MorphoDevices>(url,parameters);
            return result;
 
        }

        public MorphoDevice GetDeviceInfo(long deviceId)
        {
            MorphoDevice result = new MorphoDevice();

             
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHScreening/GetDeviceInfo");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("deviceId",deviceId.ToString());
            result = WebService.Instance.JsonGetMethod<MorphoDevice>(url,parameters);
            return result;
 
        }

        public CommunicationTransaction LinkDeviceToUser(long deviceId, long userId)
        {

            CommunicationTransaction result = new CommunicationTransaction();
   
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHScreening/LinkDeviceToUser");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("deviceId",deviceId.ToString());
            parameters.Add("userId",userId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
 
        }

        public ScreeningTask GetScreeningTask(long warehouseId, long taskId, long userId, string barcode)
        {
            ScreeningTask result  = new ScreeningTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHScreening/GetScreeningTask");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("barcode", barcode);
            parameters.Add("warehouseid", warehouseId.ToString());

            result = WebService.Instance.JsonGetMethod<ScreeningTask>(url,parameters);
 
            
            return result;
        }

        public MorphoScreening GetMorphoScreeningInfo (long deviceId, long userId)
        {
  
            MorphoScreening  result  = new MorphoScreening();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHScreening/GetMorphoScreeningInfo");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("deviceId",deviceId.ToString());
            parameters.Add("userId",userId.ToString());
            result = WebService.Instance.JsonGetMethod<MorphoScreening>(url,parameters);
            return result;
 
        }


        public CommunicationTransaction  SaveScreeningTransaction(ScreeningTransactionModel screeningTransactionItem)
        {
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHScreening/SaveScreeningTransaction");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,screeningTransactionItem);
            return result;
        }


    }


    public class ScreeningInspectionRemarkItem
    {
        public int Id  { get; set; }
        public string Remark{ get; set; }
    }

    public class ScreeningInspectionRemarks
    {
        public List<ScreeningInspectionRemarkItem> Data {get;set;}
        public CommunicationTransaction Transaction{ get; set; }
    }

public class ScreeningTransactionModel
    {
        public long UserId { get; set; }
        public long TaskId { get; set; }
        public int Pieces { get; set; }
        public ScreeningDeviceItem Data { get; set; }
        public ScreeningResult Result { get; set; }
        public string SubstancesFound { get; set; }
        public string SampleNumber { get; set; } 
        public long? PrescreenedCCSFId { get; set; }
        public string Comment { get; set; }
   }


    public class ScreeningDeviceItem
    {
       public ScreeningDeviceTypes device {get;set;}
        public long DeviceId {get;set;}
        public string SerialNumber {get;set;}
    }

 
    public class MorphoDeviceItem
    {
        public ScreeningDeviceTypes DeviceType  { get; set;}
        public bool IsActive { get; set;}
        public long ? UserId { get; set;}
        public long DeviceId {get;set;}
        public string SerialNumber {get;set;}

        public MorphoDeviceItem(long deviceId, string serialNumber,bool isActive, long ? userId)
        {
            this.DeviceId = deviceId;   
            this.SerialNumber = serialNumber;
            this.IsActive = isActive;
            this.UserId = userId;
        }
    }

    public enum ScreeningResult
    {
       PASS=0,
       ALARM = 1,
       SUSPECT = 3,
    }

    public class ScreeningTask
    {
        public ScreeningTaskItem Data {get;set;}
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class ScreeningTaskItem
    {
        public long TaskId { get; set;}
        public long ? Awb { get; set;}
        public long ? Hwb { get; set;}
        public string ShipmentReference { get; set;}
        public int TotalPieces { get; set;}
        public int ScreenedPieces { get; set;}
        public bool HasAlarm { get; set;}
        public string Origin { get; set;}
        public string Destination { get; set;}
    }

    public class MorphoScreening
    {
        public List<MorphoScreeningItem> Data {get;set;}
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class MorphoScreeningItem
    {
        public MorphoDeviceItem DeviceInfo {get;set;}
        public string SubstancesFound {get;set;}
        public string FileName {get;set;}
        public string SystemMessage {get;set;}
        public string ScreeningResults {get;set;}
    }

    public class MorphoDevices
    {
        public List<MorphoDeviceItem> Data {get;set;}
        public CommunicationTransaction Transaction{ get; set; }
    }
 
    public class MorphoDevice
    {
        public MorphoDeviceItem Data {get;set;}
        public CommunicationTransaction Transaction{ get; set; }
    }
 
    public enum ScreeningDeviceTypes
    {
               ETD=1,
               XRAY = 2,
               CANINE = 3,
               PHYSICAL = 4,
               CUSTOMER = 5,
               CO2=6,
               EDS=7,
    }

    public class CCSF
        {
            public CCSFItem Data {get;set;}
            public CommunicationTransaction Transaction{ get; set; }
        }

        public class CCSFItem
        {
            public long CCSFId {get;set;}
            public string CertificationNumber {get;set;}
        }

}  

 