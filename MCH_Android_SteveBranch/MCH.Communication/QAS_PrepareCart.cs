﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class QAS_PrepareCart
    {
        private static QAS_PrepareCart instance;
        private QAS_PrepareCart() {}

        public static QAS_PrepareCart Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new QAS_PrepareCart();
                }
                return instance;
            }
        }



        public Airports GetDestinations()
        {
            Airports result = new Airports();

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("onlyActive", true.ToString());
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/common/GetDestinations");
        
            result = WebService.Instance.JsonGetMethod<Airports>(url,parameters);
            return result;  
        }


        public QASPrepareCartTasks GetActiveCarts(GetCartsParam model)
        {
            QASPrepareCartTasks result = new QASPrepareCartTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/cart/GetActiveCarts");
            result = WebService.Instance.JsonPostMethod<QASPrepareCartTasks>(url,model);
            return result;  
        }

 
        public CommunicationTransaction UpdateCartStatus(long cartId,enmCartStatus statusId, long userId)
        {

            CartStatusUpdateParam model = new CartStatusUpdateParam();
            model.CartId = cartId;
            model.ClientDateTime = DateTime.Now;
            model.StatusId = statusId;
            model.UserId = userId;


            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/cart/UpdateCartStatus");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;
        }


      
        public QASPrepareCartTask PrepareNewCart(PrepareCartParam model)
        {
            QASPrepareCartTask result = new QASPrepareCartTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/ScannerOutbound/PrepareNewCart");
            result = WebService.Instance.JsonPostMethod<QASPrepareCartTask>(url,model);
            return result;
        }

        public CommunicationTransaction UpdateCartDestination(UpdateCartParams model)
        {
            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath,  "/ScannerOutbound/UpdateCartDestination");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;    

        }


        public CommunicationTransaction UpdateCartWeight(UpdateCartParams model)
        {
            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath,  "/scanneroutbound/UpdateCartWeight");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;   
        }

        public CommunicationTransaction DropCartAtGate(long cartId,decimal latitude, decimal longitude ,long userId)
        {

            DropCartParams model = new DropCartParams();
            model.ActionId = null;
            model.CartId = cartId;
            model.ClientDateTime = DateTime.Now;
            model.Latitude = latitude;
            model.Longitude = longitude;
            model.UserId = userId;


            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/ScannerOutbound/DropCartAtGate");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;

        }


        public CommunicationTransaction DeactivateCart(CartStatusUpdateParam model)
        {
            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath,  "/cart/DeactivateCart");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;

        }

      
        public UOMs GetUOMs(enmUOMTypes typeId)
        {
            UOMs result = new UOMs();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath,  "/common/getuoms");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("typeId", ((int)typeId).ToString());
            result = WebService.Instance.JsonGetMethod<UOMs>(url,parameters);
            return result;
        }

      
    }

    public class QASPrepareCartTask
    {
        public CartListItem Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASPrepareCartTasks
    {
        public List<CartListItem> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class GetCartsParam
    {
        public enmCartDirections ? CartDirection { get; set; } // instead of bool ? IsInbound
        public long ? UserId { get; set; }
        public long[] StatusIds { get; set; }
        public int ? WarehouseLocationId { get; set; }
    }

    public class CartStatusUpdateParam
    {
        public long CartId{get;set;}
        public long UserId{get;set;}
        public DateTimeOffset ClientDateTime { get; set; }
        public enmCartStatus ? StatusId { get; set; }
    }

    public class PrepareCartParam
    {
        public string Barcode{get;set;}
        public long UserId {get;set;}
        public DateTimeOffset ClientDateTime { get; set; }
    }

    public class DropCartParams{
        public long CartId { get; set; }
        public long UserId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public long ? ActionId { get; set; }
        public DateTimeOffset ClientDateTime { get; set; }
    }

    public class UpdateCartParams
    {
        public long CartId{get;set;}
        public long ? DestinationId { get; set; }
        public double ? Weight{get;set;}
//        public int ? UomId { get; set; } // id of the unit of measurement , you can get the list of UOMs refer to the GetUOMs method down below.
    }

    public class CartFlightDetails
    {
        public long Id { get; set; }
        public string FlightFSId { get; set; }
        public string FlightNumber { get; set; }
        public string FlightOrigin { get; set; }
        public string FlightDestination { get; set; }
        public string ArrivalGate { get; set; }
        public string DepartureGate { get; set; }
        public string CarrierCode { get; set; }

        public DateTimeOffset? STA { get; set; }
        public DateTimeOffset? STD { get; set; }
    }

    public class  CartListItem
    {
        public long CartId { get; set; }
        public string CartNumber { get; set; }
        public string Cartbarcode { get; set; }
        public string RouteOrigin { get; set; }
        public string RouteDestination { get; set; }
        public string CartDestination {get; set; }
        public double ? GrossWeight {get;set;}
        public double ? TareWeight {get;set;}
        public enmCartStatus StatusId { get; set; }
        public CartFlightDetails InboundFlight { get; set; }
        public CartFlightDetails OutboundFlight { get; set; }

        public string Reference { 
            get
            {
                if (string.IsNullOrEmpty(CartNumber))
                {
                    return Cartbarcode;
                }
                else
                {
                    return CartNumber;
                }
            }
        }


        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}",CartNumber,RouteOrigin, RouteDestination,Cartbarcode);
        }
    }

    public enum enmCartDirections
    {
        OUTBOUND =1,
        INBOUND = 2,
        TRANSFER = 3,
    }

    public enum enmCartStatus
    {
        ALL = -1,
        PENDING = 100, 
        READY_TO_FILL = 101,
        CLOSED = 102,
        WEIGHED = 103,
        STAGED_NO_GATE = 104, 
        STAGED_NO_FLIGHT = 105, 
        READY_FOR_PICKUP = 106,
        ENROUTE_TO_GATE = 107,
        DROPPED_AT_GATE = 108,
        CONFIRMED_LOADED = 109, 
        DEPARTED = 110, 
        NOT_LOADED_AT_GATE = 111,
        NOT_LOADED_RETRIVED_BY_RUNNER = 112, 
        RETRIEVED_BY_RUNNER = 113, 
        DROPPED_AT_INBOUND_STAGE = 114, 
        EMPTIED = 115, 
        DEACTIVATED = 116, 
        INBOUND_INFLIGHT = 118,
        DROPPED_AT_XFER_GATE = 119,
        InboundNewCart = 120,
    }


    public enum enmUOMTypes
    {
        WEIGHT = 1,
        VOLUME = 2,
        DIMENSION = 3,
        PRODUCT = 4
    }

    public class UOMs
    {        
        public List<UomDto> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }


    public class UomDto
    {
        public int Id { get; set; }
        public string TreeCharCode { get; set; }
        public string TwoCharCode { get; set; } // show on the UI
        public string Name { get; set; }
        public int ? TypeId { get; set; }
        public bool ? IsDefault { get; set; }
    }

    public class Airports
    {
        public List<AirportDto> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class AirportDto
    {
        public long AirportId { get; set; }
        public string IATACode { get; set; }
        public string Name { get; set; }
    }

}



