﻿ 

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class RecoverFlight
    {
        private static RecoverFlight instance;
        private RecoverFlight() {}

        public static RecoverFlight Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new RecoverFlight();
                }
                return instance;
            }
        }



        public int GetHawbId(string barcode)
        {
 
            CommunicationTransaction transaction = new CommunicationTransaction();

            string url = "scannermch/GetHawbId";


            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("barcode", barcode);
 

            string json = JsonConvert.SerializeObject(parameters).ToString();
            transaction = WebService.Instance.PostMethod(url,json);

            if (transaction.Status)
            {
                try
                {

                    return int.Parse(transaction.Content);

                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
     


        }

        private class ShipmentDetailJson
        {
            public int hawbId  { get; set; }
            public int userId  { get; set; }
            public int flightId  { get; set; }
            public ShipmentDetail detail  { get; set; }
        }

        public CommunicationTransaction UpdateShipmentDetail(int hawbId,int userId,int flightId, ShipmentDetail detail)
        {
 

            ShipmentDetailJson j = new ShipmentDetailJson();
            j.detail = detail;
            j.hawbId = hawbId;
            j.flightId = flightId;
            j.userId = userId;

            CommunicationTransaction result = new CommunicationTransaction();

            string url = "scannermch/UpdateShipmentDetail";
 

            string json = JsonConvert.SerializeObject(j).ToString();
            result = WebService.Instance.PostMethod(url,json);

            return result;


        }

        public PackageTypes GetPackageTypes(int hawbId)
        {
            PackageTypes result = new PackageTypes();
             
            string url = "common/getpackagetypes";

            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hawbId", hawbId.ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<PackageTypes> (transaction.Content);
                    result.Transaction = transaction;

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;
        }

        public ServiceTypes GetServiceTypes(int hawbId)
        {
            ServiceTypes result = new ServiceTypes();
             
            string url = "common/getservicetypes";

            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hawbId", hawbId.ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceTypes> (transaction.Content);
                    result.Transaction = transaction;

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;
        }

        public ShipmentDetail GetShipmentDetail(int hawbId)
        {
   
            //hawbId  = 10016; 
            ShipmentDetail result = new ShipmentDetail();

            string url = "scannermch/GetShipmentDetail";

            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hawbId", hawbId.ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<ShipmentDetail> (transaction.Content);
                    result.Transaction = transaction;

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;



        }


        public PrintStatus GenerateShipment(int hawbId, int userId,int flightId)
        {
            PrintStatus result = new PrintStatus();

            string url = "scannermch/GenerateShipment";

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hawbId", hawbId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightId", flightId.ToString());

            CommunicationTransaction transaction;

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<PrintStatus> (transaction.Content);
                    result.Transaction.Content  = transaction.Content;
                    result.Transaction.Url = transaction.Url;

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;
 
        }
   
        public PackageStatus ScanPackage(int packageId,int userId,int flightId)
        {

            PackageStatus result = new PackageStatus();

            string url = "scannermch/ScanPackage";


            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("packageId", packageId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightId", flightId.ToString());

            CommunicationTransaction transaction;
 
            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<PackageStatus> (transaction.Content);
                    result.Transaction = transaction;

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;
        }


        public CommunicationTransaction RecoverUld(int flightId, int uldId,int userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();

            string url = "scannermch/RecoverUld";


            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("userId", userId.ToString());

            string json = JsonConvert.SerializeObject(parameters).ToString();
            result = WebService.Instance.PostMethod(url,json);

            return result;

        }

        public CommunicationTransaction Recover(int flightId, string location,int userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();

            string url = "scannermch/Recover";


            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            parameters.Add("location", location);
            parameters.Add("userId", userId.ToString());

            string json = JsonConvert.SerializeObject(parameters).ToString();
            result = WebService.Instance.PostMethod(url,json);

            return result;
        }

        public CommunicationTransaction Stage(int flightId, string location,int userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();

            string url = "scannermch/Stage";


            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            parameters.Add("location", location);
            parameters.Add("userId", userId.ToString());

            string json = JsonConvert.SerializeObject(parameters).ToString();
            result = WebService.Instance.PostMethod(url,json);

            return result;
        }

        public CommunicationTransaction SaveReleaseInfo(int flightId, string notes, RecoverFlightReleaseTypes releaseType,int userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = "scannermch/SaveReleaseInfo";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            parameters.Add("notes", notes);
            parameters.Add("releaseType", ((int)releaseType).ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

 
        public RecoverFlightReleaseInfo GetRecoverFlightReleaseInfo(int flightId)
        {

            RecoverFlightReleaseInfo result  = new RecoverFlightReleaseInfo();

            string url = "scannermch/GetRecoverFlightReleaseInfo";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<RecoverFlightReleaseInfo> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;

        }
 
        public RecoverFlightTasks GetRecoverFlightTask( int flightId)
        {
           
            RecoverFlightTasks result  = new RecoverFlightTasks();

            string url = "scannermch/GetRecoverFlightTask";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<RecoverFlightTasks> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;         

        }

        public RecoverFlightTasks GetRecoverFlightTasks(int userId, TaskStatusTypes status)
        {
            RecoverFlightTasks result  = new RecoverFlightTasks();

            string url = "scannermch/GetRecoverFlightTasks";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("status", ((int)status).ToString());
             
            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<RecoverFlightTasks> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;
        }

 
 
        public CheckInUlds GetCheckInUld(int uldId)
        {

            CheckInUlds result  = new CheckInUlds();

            string url = "scannermch/GetCheckInUld";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uldId", uldId.ToString());
 

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckInUlds> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;

        }

        public CheckInUlds GetCheckInUlds(int flightId, UldCheckInStatusTypes status)
        {


            CheckInUlds result  = new CheckInUlds();

            string url = "scannermch/GetCheckInUlds";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            parameters.Add("status", ((int)status).ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckInUlds> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;


        }


        public RecoverFlightUlds GetRecoverUlds(int flightId, RecoverStatusTypes status)
        {


            RecoverFlightUlds result  = new RecoverFlightUlds();

            string url = "scannermch/GetRecoverUlds";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            parameters.Add("status", ((int)status).ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<RecoverFlightUlds> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;


        }

 
        public CommunicationTransaction PrintLabel(long packageId, long userId)
        {
            CommunicationTransaction transaction = new CommunicationTransaction();

            string url = "scannermch/PrintLabel";


            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("packageId", packageId.ToString());
            parameters.Add("userId", userId.ToString());

 
            transaction = WebService.Instance.GetMethod(url,parameters);

            return transaction;

        }

        public Packages GetPackagesByHawbId(int hawbId)
        {
 
            Packages result  = new Packages();

            string url = "scannermch/GetPackagesByHawbId";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hawbId", hawbId.ToString());
 

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<Packages> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;
        }

         
        private class UpdatePackageDimsJson
        {
            public int PackageId  { get; set; }
            public int UserId  { get; set; }
            public int PackageTypeId  { get; set; }
            public PackageDims dims  { get; set; }
        }


        public CommunicationTransaction UpdatePackageDims(int packageId,int userId, PackageDims dims, int packageTypeId)
        {
            UpdatePackageDimsJson j = new UpdatePackageDimsJson();
            j.PackageId = packageId;
            j.UserId = userId;
            j.PackageTypeId = packageTypeId;
            j.dims = dims;

            CommunicationTransaction result = new CommunicationTransaction();

            string url = "scannermch/UpdatePackageDims";


            string json = JsonConvert.SerializeObject(j).ToString();
            result = WebService.Instance.PostMethod(url,json);

            return result;

        }


        public CommunicationTransaction FinalizeCheckIn(int flightId ,int userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();

            string url = "scannermch/FinalizeCheckIn";


            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            parameters.Add("userId", userId.ToString());

            string json = JsonConvert.SerializeObject(parameters).ToString();
            result = WebService.Instance.PostMethod(url,json);

            return result;

        }


       
        public Packages GetPackages(int uldId, CheckInStatusTypes status, int flightId)
        {


            Packages result  = new Packages();

            string url = "scannermch/GetPackages";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("status", ((int)status).ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<Packages> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;

             
        }

        public ShipmentStatus GetShipmentStatus(int hawbId)
        {
            ShipmentStatus result  = new ShipmentStatus();

            string url = "scannermch/GetShipmentStatus";
            CommunicationTransaction transaction;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hawbId", hawbId.ToString());

            transaction = WebService.Instance.GetMethod(url,parameters);

            if (transaction.Status)
            {
                try
                {

                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<ShipmentStatus> (transaction.Content);
                    if(result.Transaction.Status)
                    {
                        result.Transaction = transaction;
                    }
                    else
                    {
                        result.Transaction.Content = transaction.Content;
                        result.Transaction.Url = transaction.Url;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Status = false;
                    transaction.Error = ex.Message;
                    result.Transaction = transaction;
                }
            }
            else
            {
                result.Transaction = transaction;
            }
            return result;
        }


    }
 
    public class ShipmentStatusData
    {
        public bool IsHawbReady { get; set; }
        public bool ShipmentCreated { get; set; }
    }
    public class ShipmentStatus
    {
        public ShipmentStatusData Data{ get; set; }
        public CommunicationTransaction Transaction;
    }


        
    public class PrintStatusData
    {
        public bool IsReady { get; set; }
        public string Hawb { get; set; }
        public List<string> ShipmentNumbers { get; set; }
        public int Pcs { get; set; } 
    }

    public class PrintStatus
    {
        public PrintStatusData Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class PackageStatusData
    {
        public bool IsCheckInSuccess { get; set; }
        public bool IsAlreadyCheckedIn { get; set; }
        public bool IsHawbReady { get; set; }
    }

    public class PackageStatus
    {
        public PackageStatusData Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class RecoverFlightReleaseInfo
    {
        public RecoverFlightReleaseData Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }


    public class RecoverFlightReleaseData
    {
        public string Notes { get; set; }
        public RecoverFlightReleaseTypes ReleaseType { get; set; }
    }

    public enum RecoverFlightReleaseTypes
    {
        NA = 0,
        Clear = 1,
        On_Hold = 2,
    }

    public class RecoverFlightTasks
    {
        public List<RecoverFlightItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class RecoverFlightItem
    {
        public long TaskId { get; set; }
        public string Flight { get; set; }
        public int FlightId { get; set; }
        public DateTime ETA { get; set; }
        public string Reference { get; set; }
        public int Ulds { get; set; }
        public int Pcs { get; set; }
        public TaskStatusTypes Status { get; set; }
        public double Progress { get; set; }
        public int RecoveredUlds { get; set; }
        public int CheckedInPcs { get; set; }
        public RecoverFlightReleaseTypes ReleaseType  { get; set; }
        public string RecoverLocation { get; set; }
        public string StageLocation { get; set; }
        public string StatusDescription { get; set; }

        public override string ToString()
        {
            return Flight + Reference;
        }
    }

    public enum TaskStatusTypes
    {
        Open=0,
        Pending = 1,
        In_Progress = 2,
        Completed = 3,
        Customs = 4,
    }

    public enum RecoverStatusTypes
    {
        Not_Recovered= 0,
        Recovered = 1,
    }

    public enum CheckInStatusTypes
    {
        Not_CheckedIn = 0,
        CheckedIn= 1,
        CheckedIn_Errors =2,
    }

    public enum UldCheckInStatusTypes
    {
        Not_CheckedIn = 0,
        CheckedIn= 1,
    }

    public class RecoverFlightUlds
    {
        public List<RecoverFlightUldItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class RecoverFlightUldItem
    {
        public RecoverStatusTypes Status   { get; set; }
        public int Id { get; set; }
        public string UldType { get; set; }
        public string UldNumber { get; set; }
        public override string ToString()
        {
            return UldNumber;
        }
    }

    public class CheckInUlds
    {
        public List<CheckInUldItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class CheckInUldItem
    {
        public UldCheckInStatusTypes Status   { get; set; }
        public int Id { get; set; }
        public string UldType { get; set; }
        public string UldNumber { get; set; }
        public int Pcs { get; set; }
        public int CheckedInPcs { get; set; }
        public override string ToString()
        {
            return UldNumber;
        }
    }


    public class Packages
    {
        public List<PackageItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }


 
    public class ServiceTypes
    {
        public List<ServiceTypeItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }


    public class ServiceTypeItem
    {
        public int ServiceTypeId { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceType { get; set; }
    }



    public class PackageTypes
    {
        public List<PackageTypeItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class PackageTypeItem
    {
        public int PackageTypeId { get; set; }
        public string PackageType { get; set; }
    }

    public class ShipmentDetail
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public int ServiceTypeId { get; set; }
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class PackageItem
    {
        public int Id { get; set; }
        public string PackageNumber { get; set; }
        public string PackageType { get; set; }
        public int PackageTypeId { get; set; }
        public string ServiceType { get; set; }
        public string Hawb { get; set; }
        public int HawbId { get; set; }
        public string ShipmentNumber { get; set; }
        public int Pcs { get; set; }
        public int CheckedInPcs { get; set; }
        public CheckInStatusTypes Status  { get; set; }

        public override string ToString()
        {
            return PackageNumber + Hawb + ShipmentNumber;
        }

        public PackageDims Dims { get; set; }

    }

    public class PackageDims
    {
        public double Weight { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Uom { get; set; }
        public string DimUom { get; set; }
    }
 


}  

    