﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    //methords
    public class QAS_Admin
    {
        private static QAS_Admin instance;
        private QAS_Admin() {}

        public static QAS_Admin Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new QAS_Admin();
                }
                return instance;
            }
        }




        public QASCartTypeTasks GetCartType()
        {
            
            QASCartTypeTasks result = new QASCartTypeTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/cart/gettypes");           
            result = WebService.Instance.JsonGetMethod<QASCartTypeTasks>(url);
            return result;   

        }

        public QASAddCartTask AddNewCart (string barcode , string cartnumber, int? carTypeId, double? taleweight)
        {
            AddCartParam model = new AddCartParam();
            model.CartBarcode = barcode;
            model.CartNumber = cartnumber;
            if (carTypeId != 0)
            {
                model.CartTypeId = carTypeId;
            }
            else
            {
                model.CartTypeId = null;
            }

            if (taleweight != -1)
            {
                model.TareWeight = taleweight;
            }
            else
            {
                model.TareWeight = null;
            }


            QASAddCartTask result = new QASAddCartTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/cart/AddNewCart");
            result = WebService.Instance.JsonPostMethod<QASAddCartTask>(url,model);
            return result;
        }
      
    }


    public class QASCartTypeTasks
    {
        public List<CartTypeListItem> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASAddCartTask
    {
        public CartDto Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }


    public class CartTypeListItem
    {
        public int Id { get; set; }
        public string Name { get; set; } 
    }

    public class AddCartParam
    {
        public string CartBarcode { get; set; }
        public string CartNumber { get; set; }
        public double? TareWeight { get; set; }
        public int? CartTypeId { get; set; }

    }

    public class CartDto
    {
        public string Barcode { get; set; }
        public long Id { get; set; }
        public string Number { get; set; }
        public int? CartTypeId { get; set; }
        public double? TareWeight { get; set; }
    }


}


