﻿ 

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class RecoverFreight
    {
        private static RecoverFreight instance;
        private RecoverFreight() {}

        public static RecoverFreight Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new RecoverFreight();
             }
             return instance;
          }
        }
  


        public ForkLiftView GetForkliftView(long taskId, long userId)
        {
            ForkLiftView result = new ForkLiftView();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetForkliftView");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<ForkLiftView>(url,parameters);
            return result; 
        }

        public LocationItemCount  GetAwbPiecesCountInLocation(long awbId, long locationId)
        {
            LocationItemCount result = new LocationItemCount();
            string url = WebService.Instance.BuildUrl(WebService.Instance.AppPath, "/MCHCargoReceiver/GetAwbPiecesCountInLocation");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("awbId", awbId.ToString());
            parameters.Add("locationId", locationId.ToString());
            result = WebService.Instance.JsonGetMethod<LocationItemCount>(url,parameters);
            return result;
        }

 
        public CommunicationTransaction RemoveItemsFromForklift(long forkliftDetailsId, int count)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/RemoveItemsFromForklift");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("forkliftDetailsId", forkliftDetailsId.ToString());
            parameters.Add("count", count.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

//        public CommunicationTransaction RelocateAwbPieces(long awbId, long oldLocationId, long newLocationId, int pcs, long taskId, long userId)
//        {
//            CommunicationTransaction result = new CommunicationTransaction();
//            string url = "MCHCargoReceiver/RelocateAwbPieces";
//            Dictionary<string, string> parameters = new Dictionary<string, string>();
//            parameters.Add("awbId", awbId.ToString());
//            parameters.Add("oldLocationId", oldLocationId.ToString());
//            parameters.Add("newLocationId", newLocationId.ToString());
//            parameters.Add("pcs", pcs.ToString());
//            parameters.Add("taskId", taskId.ToString());
//            parameters.Add("userId", userId.ToString());
// 
//            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
//            return result;
//        }

        public ShipmentUldView GetUldView(long taskId, long userId, long uldid, RecoverFreightStatusTypes status )
        {
            ShipmentUldView result = new ShipmentUldView();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetUldView");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("uldid", uldid.ToString());
            parameters.Add("status", ((int)status).ToString());
            result = WebService.Instance.JsonGetMethod<ShipmentUldView>(url,parameters);
            return result;
        }

        public ValidateScanData ValidateShipment(long userId, long taskId, long uldId, string shipment)
        {
            ValidateScanData result = new ValidateScanData();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/ValidateShipment");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("shipment", shipment.ToString());
            result = WebService.Instance.JsonGetMethod<ValidateScanData>(url,parameters);
            return result;
        }

        public ScanData ScanShipment(long userId, long taskId, string shipmentNumber)
        {
            ScanData result = new ScanData();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/ScanShipment");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("shipmentNumber", shipmentNumber.ToString());
            result = WebService.Instance.JsonGetMethod<ScanData>(url,parameters);
            return result;
        }

        public ForkLiftCount GetForkliftCount(long userId, long taskId)
        {
            ForkLiftCount result = new ForkLiftCount();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetForkliftCount");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
  
            result = WebService.Instance.JsonGetMethod<ForkLiftCount>(url,parameters);
            return result;
        }
 
        public CommunicationTransaction AddForkliftPieces(long detailsId, long taskId, int quantity, long userid)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/AddForkliftPieces");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("detailsId", detailsId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("quantity", quantity.ToString());
            parameters.Add("userid", userid.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }


        public CommunicationTransaction RecoverPiecesToLocation(long taskId, long userId, long locationId, long awbId, int pieces)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/RecoverPiecesToLocation");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("awbId", awbId.ToString());
            parameters.Add("pieces", pieces.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;  
        }


        public CommunicationTransaction DropPiecesToLocation(long taskId, long userId, long locationId, long forkliftDetailsId, int pieces)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/DropPiecesToLocation");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("forkliftDetailsId", forkliftDetailsId.ToString());
            parameters.Add("pieces", pieces.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;  
        }

        public CommunicationTransaction DropForkliftPieces(long taskId, long userId, long locationId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/DropForkliftPieces");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("locationId", locationId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction RecoverUld(long uldId, long userId, long flightManifestId, long locationId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/RecoverUld");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("locationId", locationId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public FlightUldView GetFlightULDs(long flightManifestId, UldStatusTypes status)
        {
            FlightUldView result = new FlightUldView();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetFlightULDs");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("status", ((int)status).ToString());
            parameters.Add("filter",string.Empty);

          

            result = WebService.Instance.JsonGetMethod<FlightUldView>(url,parameters);
            return result;
        }

        public FlightView GetFlightView(long taskId, long manifestId)
        {
            FlightView result = new FlightView();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetFlightView");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("manifestId", manifestId.ToString());
            result = WebService.Instance.JsonGetMethod<FlightView>(url,parameters);
            return result;
        }

   
        public CommunicationTransaction FinalizeReceiver(long taskId, long manifestId, long userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/FinalizeReceiver");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("manifestId", manifestId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction SwitchBUPMode(string station, long uldId, long userId, long manifestId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/SwitchBUPMode");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("station", station);
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("manifestId", manifestId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }


        public CommunicationTransaction UpdateFlightETA(string station, long flightManifestId, long taskId, long userId, DateTime eta)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/UpdateFlightETA");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("station", station);
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("eta", string.Format("{0:yyyy-MM-dd}T{0:HH:mm:ss}",eta));
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction RecoverFlight(string station, long taskId, long userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/RecoverFlight");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("station", station);
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction LinkTaskToUserId(long taskId, long userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/LinkTaskToUserId");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public FlightTasks GetAvailableFlightsByEta(long carrierId, DateTime eta, string station, long userId)
        {
            FlightTasks result = new FlightTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetAvailableFlightsByEta");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("carrierId", carrierId.ToString());
            parameters.Add("eta", string.Format("{0:yyyy-MM-dd}T{0:HH:mm:ss}",eta));
            parameters.Add("station", station);
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<FlightTasks>(url,parameters);
            return result;
        }


        public FlightTasks GetAvailableFlightsByFlightNo(long carrierId, string flightNo, string station, long userId)
        {
            FlightTasks result = new FlightTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetAvailableFlightsByFlightNo");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("carrierId", carrierId.ToString());
            parameters.Add("flightNo", flightNo.ToString());
            parameters.Add("station", station);
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<FlightTasks>(url,parameters);
            return result;
        }


        public FlightTasks GetAvailableFlightsByCarrier(long carrierId,string station, long userId)
        {
            FlightTasks result = new FlightTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetAvailableFlightsByCarrier");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("carrierId", carrierId.ToString());
            parameters.Add("station", station);
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<FlightTasks>(url,parameters);
            return result;
        }

        public Carriers GetAvailableCarriers(string station, int userID)
        {
            Carriers result = new Carriers();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetAvailableCarriers");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("station", station);
            parameters.Add("userID", userID.ToString());
            result = WebService.Instance.JsonGetMethod<Carriers>(url,parameters);
            return result;
        }

        public Etas GetAvailableEtas(long carrierId, string station, long userId)
        {
            Etas result = new Etas();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetAvailableEtas");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("station", station);
            parameters.Add("userId", userId.ToString());
            parameters.Add("carrierId", carrierId.ToString());
            result = WebService.Instance.JsonGetMethod<Etas>(url,parameters);
            return result;
        }

 
         

        public FlightUldView GetCargoReceiverUlds(long userId, long warehouseId, UldStatusTypes status)
        {
            FlightUldView result = new FlightUldView();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetCargoReceiverUlds");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("status", ((int)status).ToString());
            parameters.Add("userId",userId.ToString());
            result = WebService.Instance.JsonGetMethod<FlightUldView>(url,parameters);
            return result;
        }

 


        public RecoverTaskList GetCargoReceiverFlights(string station, int userID,RecoverFreightStatusTypes status)
        {
            RecoverTaskList result = new RecoverTaskList();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetCargoReceiverFlights");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("station", station);
            parameters.Add("userID", userID.ToString());
            parameters.Add("status", ((int)status).ToString());
            parameters.Add("carrierNo", string.Empty);
            parameters.Add("origin", string.Empty);
            parameters.Add("flightNo", string.Empty);
            parameters.Add("sortBy", "0");
           
            result = WebService.Instance.JsonGetMethod<RecoverTaskList>(url,parameters);
            return result;
        }

        public RecoverTask GetFlightManifestById(long manifestId, long taskId, long userId)
        {
            RecoverTask result = new RecoverTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetFlightManifestById");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("manifestId", manifestId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());

            result = WebService.Instance.JsonGetMethod<RecoverTask>(url,parameters);
            return result;
        }

 
        public RecoverTaskList GetFlightManifestByShipment(long warehouseId, string shipmentRef, long userId)
        {
            RecoverTaskList result = new RecoverTaskList();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetFlightManifestByShipment");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("shipmentRef", shipmentRef.ToString());
            parameters.Add("userid", userId.ToString());

            result = WebService.Instance.JsonGetMethod<RecoverTaskList>(url,parameters);
            return result;
        }


        public FlightView GetShipmentUldView(long taskId, long manifestId, string shipmentRef)
        {
            FlightView result = new FlightView();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetShipmentUldView");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("manifestId", manifestId.ToString());
            parameters.Add("shipmentRef", shipmentRef.ToString());

            result = WebService.Instance.JsonGetMethod<FlightView>(url,parameters);
            return result;
        }

 
        public FlightUldView GetFlightULD(long uldId, long flightManifestId)
        {
            FlightUldView result = new FlightUldView();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCargoReceiver/GetFlightULD");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
 

            result = WebService.Instance.JsonGetMethod<FlightUldView>(url,parameters);
            return result;
        }
 
  }
 
    public enum RecoverFreightStatusTypes
    {

        All = 1,
        Completed = 2,
        In_Progress = 3, 
        Open = 4, 
        Pending = 5,

    }

    public enum UldStatusTypes
    {

        All = 0,
        Pending = 1,
        In_Progress = 2,
        Completed = 3,
        Open = 4,
 
    }
 
//    public class LocationItemId
//    {
//        public long Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }

    public class LocationItemCount
    {
        public int Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class ForkLiftCount
    {
        public int Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class FlightTasks
    {
        public List<FlightTaskItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class FlightTaskItem
    {
        public long    TaskId {get;set;}
        public string    Station {get;set;}
        public long    Id {get;set;}
        public string    Name {get;set;}
    }

    public class Etas
    {
        public List<DateTime> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class Carriers
    {
        public List<CarrierItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }



    public class CarrierItem
    {
        public string Station{get;set;}
        public long Id {get;set;}
        public string Name {get;set;}
    }

    public class RecoverTaskList
    {
        public List<RecoverTaskItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }


    public class RecoverTask
    {
        public RecoverTaskItem Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }


 


    public class RecoverTaskItem
 {
        public string CarrierLogo{get;set;}
        public string CarrierCode{get;set;}
        public string CarrierName{get;set;}
        public string FlightNumber{get;set;}
        public DateTime ? ETA{get;set;}
        public string Origin{get;set;}
        public int ULDCount{get;set;}
        public int AWBCount{get;set;}
        public int TotalPieces{get;set;}
        public long TaskId{get;set;}
        public long FlightManifestId{get;set;}
        public string RecoverLocation{get;set;}
        public string RecoveredBy{get;set;}
        public DateTime ? RecoveredDate{get;set;}
        public int RecoveredULDs{get;set;}
        public int ReceivedPieces{get;set;}
        public long Flag{get;set;}
        public RecoverFreightStatusTypes Status{get;set;}  
        public List<string> References {get;set;}

 


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

             
            if (References != null)
            {
                foreach (string s in References)
                {
                    sb.Append(s + " ");
                }
            }

            //return sb.ToString();

            return string.Format("{0} {1} {2} {3} {4}", CarrierCode, CarrierName, FlightNumber,  Origin,  sb.ToString());
        }


        public DateTime GetEta()
        {
            DateTime eta = DateTime.Now;
            if(ETA != null)
            {
                eta = DateTime.Parse(ETA.ToString());
            }
            return eta;
        }

 }

    public class FlightView 
    {
        public List<FlightViewItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }


    public class FlightViewItem
    {

        public List<string>References{get;set;}
        public long Flag {get;set;}
        public long AwbId {get;set;}
        public long UldId {get;set;}
        public string Uld {get;set;}
        public string AWB {get;set;}
        public int TotalPieces {get;set;}
        public int ForkliftPieces{get;set;}
        public int ReceivedPieces{get;set;}
        public int AvailablePieces {get;set;}
        public string Locations {get;set;}
        public long DetailId {get;set;}
        public int Counter{get;set;}
        public string Origin {get;set;}
        public string Destination {get;set;}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(AWB + " ");

            if (References != null)
            {
                foreach (string s in References)
            {
                sb.Append(s + " ");
            }
            }

            return sb.ToString();
        }
      

    }



//    public class ForkLiftView 
//    {
//        public List<ForkLiftViewItem> Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }
//
//    public class ForkLiftViewItem
//    {
//        public long AwbId   {get;set;}
//        public long UldId   {get;set;}
//        public string Uld   {get;set;}
//        public string AWB   {get;set;}
//        public int TotalPieces   {get;set;}
//        public int ForkliftPieces   {get;set;}
//        public int ReceivedPieces   {get;set;}
//        public int AvailablePieces   {get;set;}
//        public string Locations   {get;set;}                 
//        public long DetailId   {get;set;}
//        //public int Counter   {get;set;}
//    }

    public class ShipmentUldView
    {
        public List<ShipmentUldViewItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class ShipmentUldViewItem
    {
        public List<string> References  {get;set;}
        public long Flag  {get;set;}
        public long  AwbId  {get;set;}
        public long UldId  {get;set;}
        public string Uld  {get;set;}
        public string AWB  {get;set;}
        public int TotalPieces {get;set;}
        public int ForkliftPieces  {get;set;}
        public int ReceivedPieces {get;set;}
        public int AvailablePieces {get;set;}
        public string Locations {get;set;}
        public long DetailId {get;set;}
        public int Counter {get;set;}
        public string Origin {get;set;}
        public string Destination {get;set;}

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            string shipment = AWB.Replace("-", "");

            sb.Append(AWB + " " + shipment + " ");
            if (References != null)
            {
                foreach (string s in References)
            {
                sb.Append(s + " ");
            }
            }

            return sb.ToString();
        }

    }

    public class FlightUldView
    {
        public FlightUldViewItem Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class FlightUldViewItem
    {
        public int FlightManifestId{get;set;}
        public int TotalUlds{get;set;}
        public int RecoveredUlds{get;set;}
        public double PercentRecovered{get;set;}
        public List<UldViewItem> Ulds{get;set;}
        public int TotalPieces{get;set;}
        public int PutAwayPieces{get;set;}
        public double PercentPutAway{get;set;}

    }

    public class ScanData
    {
        public List<ScanItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class ValidateScanData
    {
        public ShipmentScanItem Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class ScanItem
    {
        public UldViewItem Uld{get;set;}
        public ShipmentScanItem Shipment{get;set;}
    }

    public class ShipmentScanItem
    {
        //public string UldPrefix{get;set;}

        public long DetailId{get;set;}
        public long AwbId{get;set;}
        public string Awb{get;set;}
        public int AvailablePieces{get;set;}
        public string Origin{get;set;}
        public string Destination{get;set;}
    }

 

    public class UldViewItem
    {
        public string UldPrefix{get;set;}
        public string ULD {get;set;}
        public string CarrierCode {get;set;}
        public int TotalUnitCount{get;set;}
        public double PercentRecovered{get;set;}
        public int RecoveredCount {get;set;}
        public bool IsBUP {get;set;}
        public string Location {get;set;}
        public UldStatusTypes Status{get;set;}  
        public int TotalPieces {get;set;}
        public int ReceivedPieces {get;set;}
        public double PercentReceived {get;set;}
        public List<string> References {get;set;}
        public long Flag {get;set;}
        public long UldId {get;set;}
        public string  UldType {get;set;}
        public string UldSerialNo {get;set;}

        public bool IsLoose
        {
            get
            {
                if (UldPrefix.ToLower() == "all")
                {
                    return true;
                }
                else
                {
                    return (UldPrefix.ToLower() == "loose") ? true : false;
                }

            
            }
        
        }

        public bool IsRecovered
        {
            get
            {
                if (UldPrefix.ToLower() == "all")
                {
                    return true;
                }
                else
                {
                    if (Location.ToLower() == "n/a" || string.IsNullOrEmpty(Location))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }


            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(UldPrefix + UldSerialNo + " ");
            if (References != null)
            {
                foreach (string s in References)
                {
                    sb.Append(s + " ");
                }
            }

            return sb.ToString();
        }

    }
}


 