﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class Relocate
    {
        private static Relocate instance;
        private Relocate() {}

        public static Relocate Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new Relocate();
                }
                return instance;
            }
        }

        public RelocateTask GetRelocateTask(long warehouseId, long userId, string barcode)
        {
            RelocateTask result  = new RelocateTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/mchrelocate/GetRelocateTask");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("barcode", barcode.ToString());
            result = WebService.Instance.JsonGetMethod<RelocateTask>(url,parameters);
            return result;


//            if (barcode == "1")
//            {
//                RelocateTask result  = new RelocateTask();
//                result.Transaction = new CommunicationTransaction();
//                result.Transaction.Status = true;
//                result.Data = new RelocateTaskItem();
//
//                result.Data.Reference = "12345678";
//                result.Data.ReferenceId = 101;
//                result.Data.ReferenceType = ReferenceTypes.HWB;
//
//                result.Data.Destination = "BOS";
//                result.Data.Origin = "HKG";
//
//                result.Data.Skids = 2;
//                result.Data.TotalPieces = 11;
//                result.Data.ScannedPieces = 9;
//                result.Data.Locations = "B-1 (3), B-2 (7)"; 
//                return result;
//            }
//            else
//            {
//                RelocateTask result  = new RelocateTask();
//                result.Transaction = new CommunicationTransaction();
//                result.Transaction.Status = true;
//                result.Data = new RelocateTaskItem();
//
//                result.Data.Reference = "800-12345678";
//                result.Data.ReferenceId = 101;
//
//                result.Data.ReferenceType = ReferenceTypes.AWB;
//
//                result.Data.Destination = "BOS";
//                result.Data.Origin = "HKG";
//
//                result.Data.Skids = 2;
//                result.Data.TotalPieces = 11;
//                result.Data.ScannedPieces = 9;
//
//                result.Data.Locations = "A-1 (3), A-2 (7), A-3 (7)"; 
//                return result;
//            }
//
//
//            return null;


        }




        public CommunicationTransaction RelocatePieces(long referenceId, ReferenceTypes referenceType , long oldLocationId, long newLocationId, int pcs, long taskId, long userId)
        {
 

            CommunicationTransaction result  = new CommunicationTransaction();
                    string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/mchrelocate/RelocatePieces");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("referenceId", referenceId.ToString());
            parameters.Add("referenceType", ((int)referenceType).ToString());
            parameters.Add("oldLocationId", oldLocationId.ToString());
            parameters.Add("newLocationId", newLocationId.ToString());
            parameters.Add("pcs", pcs.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;


        }


    }


    public class RelocateTask
    {
        public RelocateTaskItem Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }


    public class LocationListItem
    {
        public string Location { get; set; }
        public int Pieces { get; set; }
    }

//    public enum ReferenceTypes
//    {
//        AWB = 2, 
//        HWB = 3, 
//        ULD = 4
//    }

    public class RelocateTaskItem
    {
        public long RelocateTaskId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Reference { get; set; }
        public long ReferenceId { get; set; }
        public ReferenceTypes ReferenceType { get; set; }
        public int Skids { get; set; }
        public int TotalPieces { get; set; }
        public int ScannedPieces { get; set; }
        public string Locations  { get; set; }
        public double Weight {get;set;}
        public string WeightUOM {get;set;}

        public List<LocationListItem> LocationList
        {
            get
            {



                List<LocationListItem> list = new List<LocationListItem>();
                if (!string.IsNullOrEmpty(Locations))
                {
                    Locations = Locations.Trim().Replace(")", "");

                    string[] locationsarray = Locations.Split(',');
                    if (locationsarray.Length > 0)
                    {
                        foreach (string l in locationsarray)
                        {
                            string[] locationarray = l.Split('(');
                            if (locationarray.Length == 2)
                            {
                                LocationListItem i = new LocationListItem();
                                i.Location = locationarray[0].Trim();
                                //i.Pieces = int.Parse(locationarray[1]);
                                int pcs;
                                int.TryParse(locationarray[1].Trim(),out pcs);
                                i.Pieces = pcs;
                                if (pcs > 0)
                                {
                                    list.Add(i);
                                }

                            }
                        }




                    }

                }





                return list;
            }
        }



    }


}



