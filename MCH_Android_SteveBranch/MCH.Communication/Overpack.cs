﻿ 
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class Overpack
    {
        private static Overpack instance;
        private Overpack() {}

        public static Overpack Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new Overpack();
                }
                return instance;
            }
        }

        public OverPackTask GetOverPackTask(long warehouseId, long taskId, long userId, string barcode)
        {
            OverPackTask result  = new OverPackTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/overpack/GetOverpackTask");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("barcode", barcode.ToString());
            result = WebService.Instance.JsonGetMethod<OverPackTask>(url,parameters);
            return result;
        }

        public OverPackItems GetOverPackItems(long taskId)
        {
            OverPackItems result  = new OverPackItems();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/overpack/GetOverPackItems");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<OverPackItems>(url,parameters);
            return result;
        }

        public OverpackTypes GetOverpackTypes(long warehouseId)
        {
            OverpackTypes result  = new OverpackTypes();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/overpack/GetOverpackTypes");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            result = WebService.Instance.JsonGetMethod<OverpackTypes>(url,parameters);
            return result;
        }

        public CommunicationTransaction StageOverpack(long taskId,long overpackId,long userId,long locationId)
        {
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/overpack/StageOverpack");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("overpackId", overpackId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction AddOverpack(long taskId,long userId,long OverpackTypeId,SkidOwnerTypes skidOwnerType, int pieces)
        {
      
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/overpack/AddOverpack");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("OverpackTypeId", OverpackTypeId.ToString());
            parameters.Add("skidOwnerType", ((int)skidOwnerType).ToString());
            parameters.Add("pieces", pieces.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction EditOverpack(long taskId,long overpackId,long userId,long OverpackTypeId,SkidOwnerTypes skidOwnerType, int pieces)
        {
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/overpack/EditOverpack");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("overpackId", overpackId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("OverpackTypeId", OverpackTypeId.ToString());
            parameters.Add("skidOwnerType", ((int)skidOwnerType).ToString());
            parameters.Add("pieces", pieces.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction DeleteOverpack(long taskId,long overpackId,long userId)
        {
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/overpack/DeleteOverpack");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("overpackId", overpackId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }
    }

 
    public class OverPackTask
    {
        public OverPackTaskItem Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class OverPackTaskItem
    {
        public long OverPackTaskId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Awb { get; set; }
        public long AwbId { get; set; }
        public string Hwb { get; set; }
        public long HwbId { get; set; }
        public int Skids { get; set; }
        public int TotalPieces { get; set; }
        public int TotalSkidPieces { get; set; }
        //public int TotalScannedPieces { get; set; }
        public string Reference 
        {
            get
            {
                if (AwbId != 0)
                {
                    return Awb;
                }
                if (HwbId != 0)
                {
                    return Hwb;
                }
                return "";
            }
        }

        public long ReferenceId 
        {
            get
            {
                if (AwbId != 0)
                {
                    return AwbId;
                }
                if (HwbId != 0)
                {
                    return HwbId;
                }
                return 0;
            }
        }

    }

    public class OverPackItems
    {
        public List<OverPackItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }
    
    public class OverPackItem
    {

//        public long OverPackTaskId { get; set; }
//        public long AwbId { get; set; }


        public long OverPackItemId { get; set; }
        public int OverPackItemNumber { get; set; }
        public int Pieces { get; set; }
        public SkidOwnerTypes SkidOwnerType { get; set; }
        public string OverpackType { get; set; }
        public long OverpackTypeId { get; set; }
        public long LocationId { get; set; }
        public string Location { get; set; }
    }

    public enum SkidOwnerTypes
    {
        QAS= 0,
        CUSTOMER = 1,
    }


    public class OverpackTypes
    {
        public List<OverpackTypeItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class OverpackTypeItem
    {
        public string OverpackType { get; set; }
        public long OverpackTypeId { get; set; }
        public bool IsDefault { get; set; }
    }

}



