﻿

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;
using System.Linq;
namespace MCH.Communication
{
    public class Miscellaneous
    {
        private static Miscellaneous instance;
        private Miscellaneous() {}

        public static Miscellaneous Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new Miscellaneous();
             }
             return instance;
          }
        }
  
     
        public CarrierList GetCarrierList()
        {
            CarrierList result = new CarrierList();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCommon/GetCarrierList");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("motId", "1"); //AIR
            result = WebService.Instance.JsonGetMethod<CarrierList>(url,parameters);
            return result;
        }


        public UldUnitTypes GetShipmentUnitTypes()
        {

            UldUnitTypes result = new UldUnitTypes();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCommon/GetShipmentUnitTypes");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            result = WebService.Instance.JsonGetMethod<UldUnitTypes>(url,parameters);
            return result;
        }


        public UldTypes GetUldTypes(long warehouseId)
        {
            UldTypes result = new UldTypes();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCommon/GetUldTypes");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            result = WebService.Instance.JsonGetMethod<UldTypes>(url,parameters);
            return result;
        }

        public Printers GetPrinters (long userId,PrinterTypes printerType)
        {
            //long ? userId, PrinterType ? printerType

            Printers result = new Printers();

            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/printer/GetPrinters");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("printerType",  ((int)printerType).ToString());
            result = WebService.Instance.JsonGetMethod<Printers>(url,parameters);
 
            return result;




//            GetPrintersFilter filter = new GetPrintersFilter();
//            filter.UserId =userId;
//            filter.PrinterType =printerType;
// 
//            
//            Printers result  = new Printers();
//
//            result.Transaction = new CommunicationTransaction();
//            result.Transaction.Status = true;
//            result.Data = new List<PrinterItem>();
//
//
//            PrinterItem a = new PrinterItem();
//            a.IsDefault = true;
//            a.PrinterId = 1;
//            a.PrinterName = "LP1";
//            result.Data.Add(a);
//
//
//            PrinterItem b = new PrinterItem();
//            b.IsDefault = false;
//            b.PrinterId = 2;
//            b.PrinterName = "LP2";
//            result.Data.Add(b);
//            
//            //string url = "scannermch/GetPrinters";
//            //result = WebService.Instance.JsonPostMethod<Printers>(url,filter);
//            return result;
        }

        public CommunicationTransaction SetUserDefault (long userId,int printerId)
        {
            CommunicationTransaction result  = new CommunicationTransaction();

            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/printer/SetUserDefault");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("printerId", printerId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
 
        
        }

        public CommunicationTransaction PrintLabel(long hwbId,long awbId, long userId,long overPackItemId, int count)
        {

//            CommunicationTransaction t = new CommunicationTransaction();
//            t.Status = true;
//            return t;

            if (hwbId == 0)
            {
                return PrintAWBLabel(awbId, userId,overPackItemId, count);
            }
            else
            {
                return PrintHWBLabel(hwbId, userId,overPackItemId, count);
            }

        }

        private CommunicationTransaction PrintHWBLabel(long hwbId, long userId,long overPackItemId, int count)
        {
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/printer/PrintHWBLabel");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("hwbId", hwbId.ToString());
            parameters.Add("count", count.ToString());
            parameters.Add("overPackItemId", overPackItemId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        private CommunicationTransaction PrintAWBLabel(long awbId, long userId,long overPackItemId, int count)
        {
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/printer/PrintAWBLabel");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("awbId", awbId.ToString());
            parameters.Add("count", count.ToString());
            parameters.Add("overPackItemId", overPackItemId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public Location GetLocationIdByLocationBarcode(string station, string barcode)
        {
            Location result = new Location();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/scannermch/GetLocationIdByLocationBarcode");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("station", station);
            parameters.Add("barcode", barcode);
            result = WebService.Instance.JsonGetMethod<Location>(url,parameters);
            return result;
        }

        public Locations GetLocations(params LocationTypes[] locationTypes)
        {
            Locations result  = new Locations();


            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHCommon/GetLocations");



            LocationTypeList locationTypeList = new LocationTypeList();
            locationTypeList.LocationTypes = new List<long>();


            if (locationTypes == null || locationTypes.Length == 0)
            {
                locationTypeList.LocationTypes.Add(1);
                locationTypeList.LocationTypes.Add(2);
                locationTypeList.LocationTypes.Add(3);
                locationTypeList.LocationTypes.Add(4);
                locationTypeList.LocationTypes.Add(5);
                locationTypeList.LocationTypes.Add(6);

            }
            else
            {
                foreach (LocationTypes i in locationTypes)
                {
                    switch (i)
                    {
                        case LocationTypes.Door:
                            locationTypeList.LocationTypes.Add(1);
                            break;
                        case LocationTypes.Area:
                            locationTypeList.LocationTypes.Add(2);
                            locationTypeList.LocationTypes.Add(4);
                            locationTypeList.LocationTypes.Add(5);
                            break;
                        case LocationTypes.ScreeningArea:
                            locationTypeList.LocationTypes.Add(6);
                            break;
                        case LocationTypes.Truck:
                            locationTypeList.LocationTypes.Add(3);
                            break;
                        default:
                            locationTypeList.LocationTypes.Add(1);
                            locationTypeList.LocationTypes.Add(2);
                            locationTypeList.LocationTypes.Add(3);
                            locationTypeList.LocationTypes.Add(4);
                            locationTypeList.LocationTypes.Add(5);
                            locationTypeList.LocationTypes.Add(6);
                            break;
                    }


                }
            }

            result = WebService.Instance.JsonPostMethod<Locations>(url,locationTypeList);
 
       
//            if (result.Transaction.Status && result.Data != null)
//            {
//
//                var querys =   result.Data.Select(item => item).OrderByDescending(item => item.Location);
//
//
//                var query = 
//                    from l in result.Data
//                    orderby l.Location descending
//                    select l; 
//
//
//          
//
//               
//            }


            return result;
        }
 


        public Locations GetWarehouseLocations(long warehouseId,params LocationTypes[] locationTypes)
        {
            WarehouseLocations result  = new WarehouseLocations();
 
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/common/GetWarehouseLocations");


            List<enmWarehouseLocationTypes> list = new List<enmWarehouseLocationTypes>();


            if (locationTypes == null || locationTypes.Length == 0)
            {
                list.Add(enmWarehouseLocationTypes.Door);
            }
            else
            {
                foreach (LocationTypes i in locationTypes)
                {
                    switch (i)
                    {
                        case LocationTypes.Door:
                            list.Add(enmWarehouseLocationTypes.Door);
                            break;
                        default:
                            list.Add(enmWarehouseLocationTypes.Door);
                            break;
                    }


                }
            }

            WarehouseLocationParams parameters = new WarehouseLocationParams();
            parameters.WarehouseId = warehouseId;
            parameters.TypeIds = list.ToArray();

            result = WebService.Instance.JsonPostMethod<WarehouseLocations>(url,parameters);
 

            Locations locations = new Locations();
            locations.Transaction = result.Transaction;
            locations.Data = new List<LocationItem>();
 
            if (result.Transaction.Status)
            {
                if (result.Data != null)
                {
                    foreach (var i in result.Data)
                    {
                        locations.Data.Add( new LocationItem(i.Id ,i.Location,i.Barcode,"B"));
                    }
                }
            }

            return locations;
        }



        public List<SelectionItem> GetConditions()
        {

            List<SelectionItem> l = new List<SelectionItem>();
            l.Add(new SelectionItem("Damage",0));
            l.Add(new SelectionItem("Tore",1));
            l.Add(new SelectionItem("Wet",2));
            l.Add(new SelectionItem("Retaped",3));
            l.Add(new SelectionItem("Opened",4));
            l.Add(new SelectionItem("Minor Damage",5));
            l.Add(new SelectionItem("Minor Tore",6));
            l.Add(new SelectionItem("Minor Wet",7));
            l.Add(new SelectionItem("Minor Retaped",8));
            l.Add(new SelectionItem("Minor Opened",9));
            l.Add(new SelectionItem("Major Damage",10));
            l.Add(new SelectionItem("Major Tore",11));
            l.Add(new SelectionItem("Major Wet",12));
            l.Add(new SelectionItem("Major Retaped",13));
            l.Add(new SelectionItem("Major Opened",14));
            return l;


 
        }

 
        public List<SelectionItem> GetIndicators()
        {
            List<SelectionItem> l = new List<SelectionItem>();
            l.Add(new SelectionItem("Expedite",1,@"Indicators/Indicator_1"));
            l.Add(new SelectionItem("Physically Inspected",1024,@"Indicators/Indicator_1024"));
            l.Add(new SelectionItem("Dry Ice",1048576,@"Indicators/Indicator_1048576"));
            l.Add(new SelectionItem("Screening Completed",128,@"Indicators/Indicator_128"));
            l.Add(new SelectionItem("Domestic",131072,@"Indicators/Indicator_131072"));
            l.Add(new SelectionItem("Oversize Shipment",16,@"Indicators/Indicator_16"));
            l.Add(new SelectionItem("Staged",16384,@"Indicators/Indicator_16384"));
            l.Add(new SelectionItem("Transfer",16777216,@"Indicators/Indicator_16777216"));
            l.Add(new SelectionItem("Perishable Shipment",2,@"Indicators/Indicator_2"));
            l.Add(new SelectionItem("Damage",2048,@"Indicators/Indicator_2048"));
            l.Add(new SelectionItem("High Value",2097152,@"Indicators/Indicator_2097152"));
            l.Add(new SelectionItem("Screening Not Completed",256,@"Indicators/Indicator_256"));
            l.Add(new SelectionItem("Pending Photo Capture",262144,@"Indicators/Indicator_262144"));
            l.Add(new SelectionItem("Alert",32,@"Indicators/Indicator_32"));
            l.Add(new SelectionItem("Shared Task",32768,@"Indicators/Indicator_32768"));
            l.Add(new SelectionItem("Unknown Shipper",4,@"Indicators/Indicator_4"));
            l.Add(new SelectionItem("Piece Mode",4096,@"Indicators/Indicator_4096"));
            l.Add(new SelectionItem("Live Animal",4194304,@"Indicators/Indicator_4194304"));
            l.Add(new SelectionItem("Screening Pending",512,@"Indicators/Indicator_512"));
            l.Add(new SelectionItem("Ocean Transporatation",524288,@"Indicators/Indicator_524288"));
            l.Add(new SelectionItem("Split Shipment",64,@"Indicators/Indicator_64"));
            l.Add(new SelectionItem("Import",65536,@"Indicators/Indicator_65536"));
            l.Add(new SelectionItem("Hazard",8 ,@"Indicators/Indicator_8"));
            l.Add(new SelectionItem("Count Mode",8192,@"Indicators/Indicator_8192"));
            l.Add(new SelectionItem("Human Remains",8388608,@"Indicators/Indicator_8388608"));
 

            return l;
        }


        public CommunicationTransaction SetGPSLocation(string deviceId, long userId, double  latitude, double longtitude, double  altitude, double  acurracy, double  bearing, double speed, string provider)
        {
            CoordinatesDto co = new CoordinatesDto();
            co.Altitude = altitude;
            co.Latitude = latitude;
            co.Longitude = longtitude;

            GpsDataDto to = new GpsDataDto();
            to.Accuracy = acurracy;
            to.Bearing = bearing;
            to.Coordinates = co;
            to.Provider = provider;
            to.Speed = speed;

            SetGpsDataParam model = new SetGpsDataParam();
            model.ClientDateTime = DateTime.Now;
            model.DeviceId = deviceId;
            model.UserId = userId;
            model.DpsData = to;
            model.ActionId = null;

            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/gpsdata/setgpslocation");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;

        }

    }
       
    public class SelectionItem
    {

        
        public bool Selected { get; set; }
        public string Name { get; set; }
        public long Id { get; set; }
        public string IconName { get; set; }
        public int IconId { get; set; }

        public SelectionItem(string name ,long id)
        {
            this.IconId = 0;
            this.Name = name;
            this.Id = id;
            this.IconName = string.Empty;
            this.Selected  =false;  
        }
     
        public SelectionItem(string name ,long id, bool selected)
        {
            this.IconId = 0;
            this.Name = name;
            this.Id = id;
            this.IconName = string.Empty;
            this.Selected  =selected;  
        }


        public SelectionItem(string name ,long id, string iconName)
        {
            this.IconId = 0;
            this.Name = name;
            this.Id = id;
            this.IconName = iconName;  
            this.Selected  =false;  
        }

        public SelectionItem(string name ,long id, string iconName, bool selected)
        {
            this.IconId = 0;
            this.Name = name;
            this.Id = id;
            this.IconName = iconName;  
            this.Selected  =selected;  
        }
            
        public SelectionItem(string name ,long id, int iconId)
        {
            this.IconId = iconId;
            this.Name = name;
            this.Id = id;
            this.IconName = string.Empty;
            this.Selected  =false;  
        }

        public SelectionItem(string name ,long id, int iconId, bool selected)
        {
            this.IconId = iconId;
            this.Name = name;
            this.Id = id;
            this.IconName =string.Empty;
            this.Selected  =selected;  
        }

        public override string ToString()
        {
            return this.Name ?? string.Empty ;
        }

    }

    public class UserPrinter 
    {
        public long UserId { get; set; }
        public int PrinterId { get; set; }
    }

    public class GetPrintersFilter
    {
        public long UserId { get; set; }
        public PrinterTypes PrinterType { get; set; }
    }
 
    public class Printers
    {
        public List<PrinterItem> Data{ get; set; } 
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class PrinterItem
    {
        public string PrinterName { get; set;}
        public int PrinterId { get; set;}
        public bool IsDefault { get; set;}
        public PrinterTypes PrinterType { get; set;}
    }

    public enum PrinterTypes
    {
        Document=1,
        Label=2,
    }

    public enum LocationTypes
    {
        Area=0,
        ScreeningArea=1,
        Truck=2,
        Door=3,
    }

    public class Locations
    {
        public List<LocationItem> Data{ get; set; } 
        public CommunicationTransaction Transaction{ get; set; } 
    }


    public class Location
    {
        public LocationItem Data{ get; set; } 
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class LocationId
    {
        public long Id {get;set;}
    }

    public class LocationTypeList
    {
        public List<long> LocationTypes { get; set;}
    }

    public class LocationItem
    {
 
        public string Location { get; set;}
        public string LocationBarcode { get; set;}
        public long LocationId { get; set;}
        public string LocationPrefix { get; set;}
        public string Zone { get; set; }

        public LocationItem(long locationId, string location ,string locationBarcode, string locationPrefix )
        {
            this.Location = location;
            this.LocationBarcode = locationBarcode;
            this.LocationId = locationId; 
            this.LocationPrefix = locationPrefix;  

        }

        public override string ToString()
        {
            return Location + " " + LocationBarcode + " " + LocationPrefix + "-" +  LocationBarcode + " " + LocationPrefix + "*" +  LocationBarcode;
        }

 

    }

    public class ConditionTypes
    {
        public List<ConditionTypeItem> Data{ get; set; } 
        public CommunicationTransaction Transaction{ get; set; }
    }
    public class ConditionTypeItem
    {
        public int ConditionId { get; set; }
        public string Condition { get; set; }
        public string IconName { get; set; }
        public bool IsDamage { get; set;}
        public bool IsDefault { get; set;}

        public ConditionTypeItem(string condition,int conditionId,bool isDamage)
        {
            this.Condition = condition;
            this.ConditionId = conditionId;
            this.IsDamage = isDamage;
        }
          

    }

    public class UldTypes
    {
        public List<UldTypeItem> Data{ get; set;}
        public CommunicationTransaction Transaction{ get; set;}
    }

    public class UldTypeItem
    {
        public long UldTypeId{get;set;}
        public string UldType{get;set;}

    }

    public class UldUnitTypes
    {
        public List<IdCodeItem> Data{ get; set;}
        public CommunicationTransaction Transaction{ get; set;}
    }
 
    public class IdCodeItem
    {
        public long Id { get; set; }


        private string code;
        public string Code
        {
            get{ return code ?? string.Empty; }
            set{ code = value; }
        }

        public override string ToString()
        {
            return  Code ?? string.Empty;
        }
    }

    public class CarrierList
    {
        public List<CarrierListItem> Data{ get; set;}
        public CommunicationTransaction Transaction{ get; set;}
    }

    public class CarrierListItem
    {
        public long Id{get;set;}
        //public string CarrierCode{get;set;}
        public string CarrierName{get;set;}
        public string Carrier3Code{get;set;}

        public override string ToString()
        {
            return  CarrierCode + " " + CarrierName + " " + Carrier3Code ;
        }


        private string carrierCode;
        public string CarrierCode
        {
            get{ return carrierCode ?? string.Empty; }
            set{ carrierCode = value; }
        }

    }

    //GPS location
    public class SetGpsDataParam
    {   public int ? ActionId { get; set; }
        public long UserId { get; set; }
        public string DeviceId { get; set; }  //mac number
        public DateTimeOffset ClientDateTime { get; set; }
        public GpsDataDto DpsData { get; set; }
    }

    public class GpsDataDto
    {
        public double Accuracy { get; set; }
        public CoordinatesDto Coordinates { get; set; }
        public double Bearing { get; set; }
        public double Speed { get; set; }
        public string Provider { get; set; }
    }

    public class CoordinatesDto
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double? Altitude { get; set; }
    }
 


    public enum enmWarehouseLocationTypes:long
    {
        Door = 1
    }

    public class WarehouseLocationParams
    {
        public long ? WarehouseId { get; set; }
        public enmWarehouseLocationTypes [] TypeIds {get;set;}
    }
  
    public class WarehouseLocations
    {
        public List<WarehouseLocationDto> Data{ get; set; } 
        public CommunicationTransaction Transaction{ get; set; } 
    }

    public class WarehouseLocationDto
    {

        public long Id { get; set; }

        public string Location { get; set; }

        public long? WarehouseId { get; set; }

        public string LocationType { get; set; }

        public string Zone { get; set; }

        public string Barcode { get; set; }

 
    }

}