﻿ 
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class CargoSnapShot
    {
        private static CargoSnapShot instance;
        private CargoSnapShot() {}

        public static CargoSnapShot Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new CargoSnapShot();
             }
             return instance;
          }
        }
   
        public ConditionTypes GetConditionTypes(int companyId)
        {

         


            ConditionTypes result  = new ConditionTypes();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHSnapShot/GetConditionTypes");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("companyId", companyId.ToString());
            result = WebService.Instance.JsonGetMethod<ConditionTypes>(url,parameters);

            //this 
//            if (result.Data == null)
//            {
//                result.Data = new List<ConditionTypeItem>();
//                result.Transaction = new CommunicationTransaction();
//                result.Transaction.Status = true;
// 
//                result.Data.Add(new ConditionTypeItem("Damage",1,true));
//                result.Data.Add(new ConditionTypeItem("Wet",2,true));
//                result.Data.Add(new ConditionTypeItem("Tore",3,true));
//            }


            return result;

        }

        public SnapShotTask GetSnapShotTask(long warehouseId, long taskId, long userId, string barcode)
        {
            //WHY!!!!!!
            taskId = 0;
            SnapShotTask result  = new SnapShotTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHSnapshot/GetSnapShotTask");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("barcode", barcode);
            parameters.Add("warehouseid", warehouseId.ToString());

            result = WebService.Instance.JsonGetMethod<SnapShotTask>(url,parameters);

            //test
//            result.Data = new SnapShotTaskItem();
//            result.Data.SnapShotTaskId = 0;
//            result.Data.SnapShotReference = barcode;
//            result.Transaction.Status = true;

            return result;
        }
            
        public CommunicationTransaction UploadSnapShotImage(SnapShotImageItem snapShotItem)
        {
            
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHSnapshot/UploadSnapshotImage");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url, snapShotItem);


//            try
//            {
//                if (!result.Status)
//            {
//                string json = JsonConvert.SerializeObject(snapShotItem);
//                WebService.Instance.WriteLogFile(url, json,DateTime.Now,result.Error);
//            }
//            }
//            catch
//            {
//            }


            return result;
        }
 
    }

 
 
    public class SnapShotTask
    {
        public SnapShotTaskItem Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class SnapShotTaskItem
    {
        public long SnapShotTaskId { get; set; }
        public string SnapShotReference { get; set; }
    }


    public class SnapShotImageItem
    {
        
        public long UserId { get; set; }
        public long SnapshotTaskId { get; set; }
        public int PieceCount { get; set; }
        public string SnapshotImage { get; set; }
        public double  Latitude { get; set; }
        public double Longitude { get; set; }

        public List<SnapShotImageConditionItem> SnapShotImageConditionItems  { get; set; }
    }
 
    public class SnapShotImageConditionItem
    {
        public long ConditionId { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
    }

}