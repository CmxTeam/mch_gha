﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;

namespace MCH.Communication
{
    public class Menu
    {
        private static Menu instance;
        private Menu() {}

        public static Menu Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new Menu();
             }
             return instance;
          }
        }
  

        public UserTasksCounter GetUserTaskCounts(int userId, List<int> taskTypes, long warehouseId)
        {

            UserTasksCounter result  = new UserTasksCounter();



            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath, "scannermch/GetUserTaskCounts");

            UserTaskCounterParamerter parameters = new UserTaskCounterParamerter();
            parameters.UserId =userId;
            parameters.TaskTypeIds = taskTypes;
            parameters.WarehouseId= warehouseId;
             
            result = WebService.Instance.JsonPostMethod<UserTasksCounter>(url,parameters);
            return result;
        }

   

        public UserWarehouses GetUserWarehouses (int userId, string warehouseIds)
		{
 
			
			UserWarehouses result = new UserWarehouses ();

            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "scannermch/GetUserWarehouses");
			CommunicationTransaction transaction = new CommunicationTransaction();

			UserWarehousesParameter parameters = new UserWarehousesParameter ();
			parameters.UserId = userId;


			if (warehouseIds.Trim () != string.Empty) {
				string[] ws = warehouseIds.Split (',');
				parameters.WarehouseShellIds = new List<int> ();
				foreach (string w in ws) {
					parameters.WarehouseShellIds.Add (int.Parse(w));
				}
			} else {
				transaction.Status = false;
				transaction.Error = "Missing Warehouse Ids.";
				result.Transaction = transaction;
				return result;
			}
                
            result = WebService.Instance.JsonPostMethod<UserWarehouses>(url,parameters);
            return result;
 
        }
 
        public class UserTaskCounter
        {
            public int TaskTypeId { get; set; }
            public int OpenAssignedTaskCount { get; set; }
        }
        public class UserTasksCounter
        {
            public List<UserTaskCounter> Data=new List<Menu.UserTaskCounter>();
            public CommunicationTransaction Transaction { get; set;}
        }

        public class UserTaskCounterParamerter
        {
            public long WarehouseId;
            public int UserId;
            public List<int> TaskTypeIds;
        }


        public Categories GetMenuCategories(string appName, int roleId,int userId, long warehouseId)
        {

            Categories result  = new Categories();
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath, "Menu/GetMenuCategories");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("appName", appName);
            parameters.Add("roleId", roleId.ToString());
            parameters.Add("ApplicationTypeId", ((int)ApplicationTypes.Android).ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("warehouseId", warehouseId.ToString());
            
            result = WebService.Instance.JsonPostMethod<Categories>(url,parameters);
            return result;


//            Categories result  = new Categories();
//            result.Transaction = new CommunicationTransaction();
//            result.Transaction.Status = true;
//            result.Data = new List<Category>();
//
//
//            Category b = new Category();
//            b.CategoryId = 1;
//            b.CategoryName = "Outbound";
//            b.IsDefault = true;
//            result.Data.Add(b);
//
//            Category c = new Category();
//            c.CategoryId = 2;
//            c.CategoryName = "Inbound";
//            c.IsDefault = false;
//            result.Data.Add(c);
//
//            return result;
        }

        public MenuList GetAppRoleMenus(string appName, int roleId,int userId, long warehouseId)
        {

 
            MenuList result  = new MenuList();
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath, "/Menu/GetAppRoleMenus");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("appName", appName);
            parameters.Add("roleId", roleId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("ApplicationTypeId", ((int)ApplicationTypes.Android).ToString());
            result = WebService.Instance.JsonPostMethod<MenuList>(url,parameters);


            if (result.Transaction.Status)
            {
                List<int> l = GetTaskTypeList(result.Data);
                if (l.Count > 0)
                {
                    UserTasksCounter counters = GetUserTaskCounts(userId, l, warehouseId);
                    if (counters.Transaction.Status)
                    {
                        foreach (UserTaskCounter c in counters.Data)
                        {
                            foreach (MenuItem m in result.Data)
                            {
                                if (m.MenuParam == c.TaskTypeId.ToString())
                                {
                                    m.OpenAssignedTaskCount = c.OpenAssignedTaskCount;
                                }
                            }
                        }

                    }
                }





            }

            //test
            if (!result.Transaction.Status)
            {
                result.Transaction = new CommunicationTransaction();
                result.Transaction.Status = true;
                result.Data = new List<MenuItem>();

            }

//            // test inbound
//            MenuItem m3 =  new MenuItem();
//            m3.CategoryId = 77;
//            m3.NavigationPath = "qas_tug_checkout";
//            m3.Name = "Check Out Tug";
//            m3.IconKey = "TugCheckOut";
//            result.Data.Add(m3);
//
//            MenuItem m4 =  new MenuItem();
//            m4.CategoryId = 77;
//            m4.NavigationPath = "qas_tug_checkin";
//            m4.Name = "Check In Tug";
//            m4.IconKey = "Tug";
//            result.Data.Add(m4);

 
            return result;
        }


		private List<int> GetTaskTypeList (List<MenuItem> menus)
		{
			List<int> l = new List<int> ();
			foreach (MenuItem m in menus) {
				try 
				{
					l.Add (int.Parse (m.MenuParam));
				}
				catch {}
			}
			return l;
		}

    }



    public class MenuList
    {
        public List<MenuItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class MenuItem
    {
        public int Id{ get; set; }
        public string Name{ get; set; }
        public string Text{ get; set; }
        //public string ChildItems{ get; set; }
        public int Order{ get; set; }
        public string IconKey{ get; set; }
        public string NavigationPath{ get; set; }
        public string IconBase64{ get; set; }
        public bool IsDefault{ get; set; }
        public string MenuParam{ get; set; }
        public int OpenAssignedTaskCount{ get; set; }
        public List<StaticRestrictions> Restrictions{ get; set; }
        public long CategoryId{ get; set; }
 
    }

        public enum StaticRestrictions : int
        {
            NoAccess = 3,
            NoUpdate = 1,
            NoDelete = 2,
        }

	    public class UserWarehousesParameter
		{
            public List<int> WarehouseShellIds{ get; set; }
            public int UserId { get; set; }
		}

		public class UserWarehouses
		{
            public List<UserWarehouse> Data { get; set; }
            public CommunicationTransaction Transaction { get; set; }
		}

		public class UserWarehouse
		{
            public string Code { get; set; }
            public int ExternalId { get; set; }
            public int Id { get; set; }
		}


    public class Categories
    {
        public List<Category> Data { get; set; }
        public CommunicationTransaction Transaction { get; set; }
    }

    public class Category
    {
        public string CategoryName { get; set; }
        public long CategoryId { get; set; }
        public bool IsDefault { get; set; }
    }
}

