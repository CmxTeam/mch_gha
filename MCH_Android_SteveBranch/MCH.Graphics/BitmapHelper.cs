﻿ 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;

namespace MCH.Graphics
{
    using System.IO;

    using Android.Graphics;

    public static class BitmapHelper
    {



        public static byte[] BitmapToArray(Bitmap bitmap)
        {
            
            byte[] byteData;
            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                byteData = stream.ToArray();
            }

            bitmap.Recycle();
            bitmap = null;

            return byteData;

        }

        private static Bitmap bitmap;
        public static byte[] RotateImage(byte[] imageData, int rotation)
        {




            if (bitmap != null)
            {
                bitmap.Recycle();
                bitmap = null;
            }

            if (rotation == 0)
            {
                return imageData;
            }

            //BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            bitmap = BitmapFactory.DecodeByteArray(imageData,0,imageData.Length);
            Matrix matrix = new Matrix();
            matrix.PostRotate(rotation);
            bitmap = Bitmap.CreateBitmap(bitmap , 0, 0, bitmap.Width, bitmap.Height,matrix,true);
            byte[] byteData;
            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                byteData = stream.ToArray();
            }

            bitmap.Recycle();
            bitmap = null;

            
            return byteData;
        }



//        public static byte[] RotateImage(byte[] imageData, int rotation)
//        {
//            //BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
//            Bitmap bitmapToDisplay = BitmapFactory.DecodeByteArray(imageData,0,imageData.Length);
//            Matrix matrix = new Matrix();
//            matrix.PostRotate(rotation);
//            Bitmap bitmapRotated = Bitmap.CreateBitmap(bitmapToDisplay , 0, 0, bitmapToDisplay.Width, bitmapToDisplay.Height,matrix,true);
//            byte[] byteData;
//            using (var stream = new MemoryStream())
//            {
//                bitmapRotated.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
//                byteData = stream.ToArray();
//            }
//
//            bitmapRotated.Recycle();
//            bitmapRotated = null;
//            bitmapToDisplay.Recycle();
//            bitmapToDisplay = null;
//
//            GC.Collect();
//            return byteData;
//        }

//        public static byte[] RotateImage(byte[] imageData, int rotation)
//        {
//            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
//            Bitmap bitmapToDisplay = BitmapFactory.DecodeByteArray(imageData,0,imageData.Length);
//            Matrix matrix = new Matrix();
//            matrix.PostRotate(rotation);
//            bitmapToDisplay = Bitmap.CreateBitmap(bitmapToDisplay , 0, 0, bitmapToDisplay.Width, bitmapToDisplay.Height,matrix,true);
//            byte[] byteData;
//            using (var stream = new MemoryStream())
//            {
//                bitmapToDisplay.Compress(Bitmap.CompressFormat.Jpeg, 0, stream);
//                byteData = stream.ToArray();
//            }
//            bitmapToDisplay.Recycle();
//            bitmapToDisplay = null;
//
//            GC.Collect();
//            return byteData;
//        }

//        public static Bitmap ByteToBitmap(byte[] imageData)
//        {
//            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
//            Bitmap bitmapToDisplay = BitmapFactory.DecodeByteArray(imageData,0,imageData.Length);
//            return bitmapToDisplay;
//        }

        public static Bitmap LoadAndResizeBitmap(this string fileName, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                    ? outHeight / height
                    : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

            return resizedBitmap;
        }

        public static async void LoadImage(string filePath, ImageView pic, int width, int height, int rotation)
        {
            BitmapFactory.Options options = await GetBitmapOptionsOfImageAsync(filePath);
          
            Bitmap bitmapToDisplay = await LoadScaledDownBitmapForDisplayAsync(filePath, options, width, height);
          

            if (rotation > 0)
            {
                Matrix matrix = new Matrix();
                matrix.PostRotate(rotation);
                Bitmap rotatedBitmap = Bitmap.CreateBitmap(bitmapToDisplay , 0, 0, bitmapToDisplay.Width, bitmapToDisplay.Height,matrix,true);
                pic.SetImageBitmap (rotatedBitmap);
            }
            else
            {
                pic.SetImageBitmap (bitmapToDisplay);
            }
 

        }




//        public static async void LoadImage(string filePath,ImageView pic,int width, int height)
//        {
//            BitmapFactory.Options options = await GetBitmapOptionsOfImageAsync(filePath);
//            Bitmap bitmapToDisplay = await LoadScaledDownBitmapForDisplayAsync (filePath, options, width, height);
//            pic.SetImageBitmap (bitmapToDisplay);
//
//
//                        Matrix matrix = new Matrix();
//                        matrix.PostRotate(90);
//                        Bitmap rotatedBitmap = Bitmap.CreateBitmap(bitmapToDisplay , 0, 0, bitmapToDisplay.Width, bitmapToDisplay.Height,matrix,true);
//                        pic.SetImageBitmap (rotatedBitmap);
//
//        }


//        public static async void LoadImageFromBitmap(Bitmap bitmapToDisplay,ImageView pic,int width, int height)
//        {
//            //BitmapFactory.Options options = await GetBitmapOptionsOfImageAsync(filePath);
//            //Bitmap bitmapToDisplay = await LoadScaledDownBitmapForDisplayAsync (filePath, options, width, height);
//            pic.SetImageBitmap (bitmapToDisplay);
//
//
//                        Matrix matrix = new Matrix();
//                        //matrix.PostRotate(90);
//                        Bitmap rotatedBitmap = Bitmap.CreateBitmap(bitmapToDisplay , 0, 0, bitmapToDisplay.Width, bitmapToDisplay.Height,matrix,true);
//                        pic.SetImageBitmap (rotatedBitmap);
//
//        }

        public static async Task<bool> LoadImageAsync(string filePath,ImageView pic,int width, int height,int rotation)
        {
            BitmapFactory.Options options = await GetBitmapOptionsOfImageAsync(filePath);



            Bitmap bitmapToDisplay = await LoadScaledDownBitmapForDisplayAsync (filePath, options, width, height);
 
           

//            Matrix matrix = new Matrix();
//            matrix.PostRotate(90);
//            Bitmap rotatedBitmap = Bitmap.CreateBitmap(bitmapToDisplay , 0, 0, bitmapToDisplay.Width, bitmapToDisplay.Height,matrix,true);
//
//            pic.SetImageBitmap (rotatedBitmap);
//
//            return true;




            if (rotation > 0)
            {
                Matrix matrix = new Matrix();
                matrix.PostRotate(rotation);
                Bitmap rotatedBitmap = Bitmap.CreateBitmap(bitmapToDisplay , 0, 0, bitmapToDisplay.Width, bitmapToDisplay.Height,matrix,true);
                pic.SetImageBitmap (rotatedBitmap);

                 
            }
            else
            {
                pic.SetImageBitmap (bitmapToDisplay);
            }

            return true;

        }

 

        public static async  Task<BitmapFactory.Options> GetBitmapOptionsOfImageAsync(string filePath)
        {
            BitmapFactory.Options options = new BitmapFactory.Options
                {
                    InJustDecodeBounds = true
                };

   

            Bitmap result = await  BitmapFactory.DecodeFileAsync(filePath,options);

            //int imageHeight = options.OutHeight;
            //int imageWidth = options.OutWidth;
   

            return options;
        }

        public static async Task<Bitmap> LoadScaledDownBitmapForDisplayAsync(string filePath, BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Calculate inSampleSize
            options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.InJustDecodeBounds = false;
 


            return await BitmapFactory.DecodeFileAsync(filePath,options);
        }


        public static byte[] GetBytesFromImage(string filePath)
        {
           
            byte[] byteArray = System.IO.File.ReadAllBytes(filePath);
            return byteArray;

        }

        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            float height = options.OutHeight;
            float width = options.OutWidth;
            double inSampleSize = 1D;

            if (height > reqHeight || width > reqWidth)
            {
                int halfHeight = (int)(height / 2);
                int halfWidth = (int)(width / 2);

                // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
                while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
                {
                    inSampleSize *= 2;
                }
            }

            return (int)inSampleSize;
        }


    }

}

