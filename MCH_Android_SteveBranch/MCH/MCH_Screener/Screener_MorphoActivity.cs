﻿ 
using System.Collections;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
using System.ComponentModel;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Screener_MorphoActivity : BaseActivity
    {
        //public static ScreenerMenuItem.ScreenerMenuType Mode;
        public static long CurrentDeviceId = 0;
        bool IsNew = false;
        MorphoDevice device;
        long ReferenceId = 0;
        string Reference = string.Empty;
        public static string LastReference = string.Empty;
        public static int LastReferencePieces = 0;
        int ReferencePieces = 0;
        string SerialNumber = string.Empty;
        string barcode  = string.Empty;
        ScreeningTask currentScreeningTask;

        BackgroundWorker bgWorker;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Screener_Morpho);
            Initialize();
 

            this.bgWorker = new BackgroundWorker();
            this.bgWorker.WorkerSupportsCancellation = true;
            this.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
           

            ClearResults();
            CheckConnection();
 
        }

        private void CheckConnection()
        {

            if (Screener_MorphoActivity.CurrentDeviceId > 0)
            {
                device = Morpho.Instance.GetDeviceInfo(Screener_MorphoActivity.CurrentDeviceId);
                if (!device.Transaction.Status)
                {
                    Screener_MorphoActivity.CurrentDeviceId = 0;
                }
                else
                {
                    if (device.Data == null)
                    {
                        Screener_MorphoActivity.CurrentDeviceId = 0;
                    }
                    else
                    {
                        if (device.Data.UserId != null)
                        {
                            if (device.Data.UserId != ApplicationSessionState.User.Data.UserId)
                            {
                                Screener_MorphoActivity.CurrentDeviceId = 0;
                            } 
                        }

                        if (!device.Data.IsActive)
                        {
                            Screener_MorphoActivity.CurrentDeviceId = 0;
                        }
                        else
                        {
                            this.SerialNumber = device.Data.SerialNumber;
                        }

                    }
                }
            }



            if(Screener_MorphoActivity.CurrentDeviceId==0)
            {
                StopWorker();
                Device.Text = "DEVICE: NOT CONNECTED";
                ClearResults();
                ShowLastScanned();
                ShowEtdDevices();
            }
            else
            {
                Device.Text = "DEVICE: " + this.SerialNumber;
                ShowLastScanned();
            }
        }

        private void ClearResults()
        {
                btnStop.Visibility = ViewStates.Gone;
                SystemMessage.Text = string.Empty;
                ScreeningResults.Text = string.Empty;
                //SubstancesFound.Text = string.Empty;
                //FileName.Text= string.Empty;
                ReferenceNumber.Text= string.Empty; 
        }

        private void OnbtnStop_Click(object sender, EventArgs e)
        {
            ClearResults();
            CheckConnection();
            StopWorker();
        }

 

        private void StartWorker()
        {
            if (this.bgWorker != null && !this.bgWorker.IsBusy)
            {
                //btnStop.Visibility = ViewStates.Gone;
                this.bgWorker.RunWorkerAsync();
            }
        }

        private void StopWorker()
        {
            btnStop.Visibility = ViewStates.Gone;
            if (this.bgWorker != null && this.bgWorker.IsBusy)
            {
                this.bgWorker.CancelAsync(); 
            } 
        }

            
        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            IsNew = false;


            if (!IsDeviceReady())
            {
                RunOnUiThread(delegate
                        {
                        Screener_MorphoActivity.CurrentDeviceId = 0;
                   
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                                {
                                    CheckConnection();
                                };
                            m.ShowAlert("Device is not ready.",MessageBox.AlertType.Error);
                        });
                return;
            }


            while (true)
            {
                if (this.bgWorker.CancellationPending) {
                    RunOnUiThread(delegate
                        {
                            btnStop.Visibility = ViewStates.Gone;
                        });

                    break;
                }
                else
                {
                    RunOnUiThread (() => btnStop.Visibility = ViewStates.Visible);





                    DoScreening();

                }

                Thread.Sleep (1000);

            }
        }

        bool IsDeviceReady()
        {
            try
            {
                MorphoScreening morphoScreening = Morpho.Instance.GetMorphoScreeningInfo(Screener_MorphoActivity.CurrentDeviceId, ApplicationSessionState.User.Data.UserId);
                if (morphoScreening.Transaction.Status && morphoScreening.Data != null)
                {
                    if(morphoScreening.Data.Count == 0 || Screener_MorphoActivity.CurrentDeviceId != morphoScreening.Data[0].DeviceInfo.DeviceId)
                    {
                        return false;
                    }
                    else
                    {
                        if(morphoScreening.Data[0].SystemMessage.ToLower() == "no alarm - ready")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }

                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }



        void DoScreening()
        {


          

            MorphoScreening morphoScreening = Morpho.Instance.GetMorphoScreeningInfo(Screener_MorphoActivity.CurrentDeviceId, ApplicationSessionState.User.Data.UserId);
            if (morphoScreening.Transaction.Status && morphoScreening.Data != null)
            {

                RunOnUiThread(delegate
                    {
                        if(morphoScreening.Data.Count == 0 || Screener_MorphoActivity.CurrentDeviceId != morphoScreening.Data[0].DeviceInfo.DeviceId)
                        {
                            Screener_MorphoActivity.CurrentDeviceId = 0;
                            StopWorker();
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                                {
                                    CheckConnection();
                                };
                            m.ShowAlert("Device was disconnected.",MessageBox.AlertType.Error);
                            return;
                        }
                        else if(morphoScreening.Data[0].SystemMessage.ToLower().Contains("not ready"))
                        {
                            Screener_MorphoActivity.CurrentDeviceId = 0;
                            StopWorker();
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                                {
                                    CheckConnection();
                                };
                            m.ShowAlert("Device is not ready.",MessageBox.AlertType.Error);
                            return;
                        }
                        else
                        {
                            SystemMessage.Text = morphoScreening.Data[0].SystemMessage; 
                            ScreeningResults.Text =   morphoScreening.Data[0].ScreeningResults; 
                        }

                    });

                if(morphoScreening.Data[0].SystemMessage.ToLower() == "sampling...")
                {

                    IsNew =true;
                }

                //No Alarm - Wait


                //if (IsNew && morphoScreening.Data[0].FileName != string.Empty)
                if (IsNew && (morphoScreening.Data[0].SystemMessage.ToLower() == "no alarm - ready" || 
                    (morphoScreening.Data[0].SystemMessage.ToLower() == "explosives detected" && morphoScreening.Data[0].ScreeningResults.ToLower().Contains("alarm"))
                ) )
                {

        

                    Screener_MorphoActivity.LastReferencePieces = ReferencePieces;
                    Screener_MorphoActivity.LastReference = Reference;
                     
                    ShowLastScanned();

                    RunOnUiThread(delegate
                        {
                            ScreeningResults.Text =   morphoScreening.Data[0].ScreeningResults; 
                            if( morphoScreening.Data[0].SubstancesFound == string.Empty)
                            {
                                //SubstancesFound.Text = "None"; 
                            }
                            else
                            {
                                //SubstancesFound.Text =   morphoScreening.Data[0].SubstancesFound; 
                            }

                            //FileName.Text =   morphoScreening.Data[0].FileName; 
                        });

 

                    if (morphoScreening.Data[0].ScreeningResults.ToLower() == "alarm")
                    {
                    
                        Action<string,bool,string,string> ClickAction = OnCommentsAction;
                        var transaction = this.FragmentManager.BeginTransaction();
                        var dialogFragment = new Screener_ResultDialogActivity(this, ClickAction,"Screening Result",Reference,ReferencePieces,true, morphoScreening.Data[0].SubstancesFound,morphoScreening.Data[0].FileName);
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                    }
                    else
                    {   
                        Action<string,bool,string,string> ClickAction = OnCommentsAction;
                        var transaction = this.FragmentManager.BeginTransaction();
                        var dialogFragment = new Screener_ResultDialogActivity(this, ClickAction,"Screening Result",Reference,ReferencePieces,false, string.Empty,morphoScreening.Data[0].FileName);
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                    }


                    ShowLastScanned();

                    StopWorker();
                }
                else
                {
                    RunOnUiThread(delegate
                        {
                            
                            blinker.Start();
   
                            lastScanLayout.Visibility = ViewStates.Gone;
                             
                            txtStatus.Visibility = ViewStates.Visible;
                            if(IsNew)
                            {
                                txtStatus.Text = "IN PROGRESS...";
                            }
                            else
                            {
                                txtStatus.Text = "PLEASE INSERT SAMPLE...";
                            }

                        });
                }

               
            }
            else
            {
                Screener_MorphoActivity.CurrentDeviceId = 0;
                RunOnUiThread(delegate
                    {
                        StopWorker();
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) => 
                            {
                                CheckConnection();
                            };
                        m.ShowAlert("Device was disconnected",MessageBox.AlertType.Alert);
                    });
            }





        }
 

        void DoScanDevice(Barcode barcode)
        {
 
            if (txtBarcode.Text == string.Empty)
            {
                return;
            }

            if (btnStop.Visibility == ViewStates.Visible)
            {
                return;
            }


            MorphoDevices devices = Morpho.Instance.GetScreeningDevices(ApplicationSessionState.SelectedWarehouseId, ScreeningDeviceTypes.ETD);
            if (devices.Transaction.Status)
            {
                foreach (var d in devices.Data)
                {
                    if (barcode.ITDX == d.SerialNumber)
                    {
                        SelectDeviceId(d.DeviceId);
                        return;
                    }
                }
            }

          
            MessageBox m = new MessageBox(this);
            m.ShowAlert("Device not available.",MessageBox.AlertType.Information);

        }

    
        void OnEnterEvent(object sender, EditTextEventArgs e)
        {




            if (txtBarcode.Text == string.Empty)
            {
                return;
            }




            if (e.IsBarcode)
            {
                if (e.BarcodeData.BarcodeType == BarcodeTypes.ITDX)
                {
                    DoScanDevice(e.BarcodeData);
                }
                else
                {

                    if (Screener_MorphoActivity.CurrentDeviceId == 0)
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowAlert("Please select a device.", MessageBox.AlertType.Information);
                        return;
                    }
                    DoScan(e.BarcodeData);
                }  

            }
            else
            {



                if (Screener_MorphoActivity.CurrentDeviceId == 0)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert("Please select a device.", MessageBox.AlertType.Information);
                    return;
                }

                Barcode b = new Barcode();
                b.BarcodeType = BarcodeTypes.NA;
                b.BarcodeText = txtBarcode.Text;
                DoScan(b);
            }
        }

        private void DoScan(Barcode barcode)
        {
            
            IsNew = false;
            ReferenceId = 0;
            Reference = string.Empty;
            StopWorker();

//            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Please_Wait), true);
//            new Thread(new ThreadStart(delegate
//                {

            if (barcode.BarcodeText != string.Empty)
            {


                if (!IsDeviceReady())
                {
                    ClearResults();
                    CheckConnection();
                    StopWorker();
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert("Device is not ready.",MessageBox.AlertType.Error);
                    return;
                }



                currentScreeningTask = Morpho.Instance.GetScreeningTask(ApplicationSessionState.SelectedWarehouseId,0, ApplicationSessionState.User.Data.UserId, barcode.BarcodeText);

                if (currentScreeningTask.Transaction.Status)
                {

                    this.barcode = barcode.BarcodeText;
                    ReferenceId = currentScreeningTask.Data.Hwb ?? (currentScreeningTask.Data.Awb ?? 0);
                    Reference  = currentScreeningTask.Data.ShipmentReference ?? string.Empty; 
                    EditTextListener.Text = string.Empty;

                    if (currentScreeningTask.Data.HasAlarm)
                    {
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) => 
                        {
                                if(result)
                                {

                                    Action<int> PiecesClickAction = OnPiecesClickAction;
                                    var transaction = FragmentManager.BeginTransaction();
                                    var dialogFragment = new PiecesDialog(this,currentScreeningTask.Data.ShipmentReference, PiecesClickAction,currentScreeningTask.Data.TotalPieces, currentScreeningTask.Data.TotalPieces - currentScreeningTask.Data.ScreenedPieces , false );
                                    //dialogFragment.SetStyle(DialogFragmentStyle.Normal,Resource.Style.Theme_NoTitle_Basic);
                                    dialogFragment.Cancelable = false;
                                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
                                     
 
                                }
                                else
                                {
                                    return;
                                }
                        };
                        m.ShowConfirmationMessage("Shipment Number " + currentScreeningTask.Data.ShipmentReference + " has an 'Alarm'. Do you want to clear the alarm?" ,"Yes","No");
                    }
                    else
                    {
                        
                                    Action<int> PiecesClickAction = OnPiecesClickAction;
                                    var transaction = FragmentManager.BeginTransaction();
                        var dialogFragment = new PiecesDialog(this,currentScreeningTask.Data.ShipmentReference, PiecesClickAction,currentScreeningTask.Data.TotalPieces, currentScreeningTask.Data.TotalPieces - currentScreeningTask.Data.ScreenedPieces, false );
                                    //dialogFragment.SetStyle(DialogFragmentStyle.Normal,Resource.Style.Theme_NoTitle_Basic);
                                    dialogFragment.Cancelable = false;
                                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
                            
 
                    }        

                }
                else
                {
                    ClearResults();
                    CheckConnection();
                    StopWorker();
                    MessageBox m = new MessageBox(this);
                    //m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
                    m.ShowAlert("Unable to validate shipment.", MessageBox.AlertType.Information);
                }


            }       
            else
            {
                ClearResults();
                CheckConnection();
                StopWorker();
                MessageBox m = new MessageBox(this);
                //m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
                m.ShowAlert("Unable to validate shipment.", MessageBox.AlertType.Information);
            }

//                    RunOnUiThread(() => progressDialog.Hide());
//                })).Start();

        }

        public void ShowEtdDevices()
        {

            MorphoDevices devices = Morpho.Instance.GetScreeningDevices(ApplicationSessionState.SelectedWarehouseId,ScreeningDeviceTypes.ETD);
            if (!devices.Transaction.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(devices.Transaction.Error, MessageBox.AlertType.Error);
                return;
            }
            else
            {
                if (devices.Data == null || devices.Data.Count == 0)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(Resource.String.No_ETD_Devices, MessageBox.AlertType.Alert);
                    return;
                }
            }



            List< MCH.Communication.SelectionItem> deviceList = new List< MCH.Communication.SelectionItem>();
            foreach (var d in devices.Data)
            {

                bool isDefault = false;
                if (ApplicationSessionState.GetInt(this, "DefaultETDDeviceId") == d.DeviceId)
                {
                    isDefault = true;
                }

                deviceList.Add(new SelectionItem(d.SerialNumber,d.DeviceId, "Icons/" + ScreenerMenuItem.ScreenerMenuType.ETD_Screening.ToString(),isDefault));
            }


            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnDeviceClickSelectionAction;    
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.ETD_Devices), this, deviceList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection,false);
            dialogFragment.Cancelable = false;
            dialogFragment.AllowNoSelection = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void LinkDeviceToUser(long deviceId, string serialNumber)
        {
            CommunicationTransaction t = Morpho.Instance.LinkDeviceToUser(deviceId, ApplicationSessionState.User.Data.UserId);
            if (t.Status)
            {
                Screener_MorphoActivity.CurrentDeviceId = deviceId;
                this.SerialNumber = serialNumber;
                ClearResults();
                ShowLastScanned();
                ApplicationSessionState.SaveString(this,"DefaultETDDeviceId",deviceId.ToString());
            }
            else
            {
                Screener_MorphoActivity.CurrentDeviceId = 0;
                this.SerialNumber = string.Empty;
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick  += delegate
                    {
                        CheckConnection();
                        return; 
                    };
                m.ShowAlert(t.Error,MessageBox.AlertType.Alert);
                return;
            }

            CheckConnection();
        }

        void SelectDeviceId(long deviceId)
        {
            Screener_MorphoActivity.CurrentDeviceId = 0;
                    this.SerialNumber = string.Empty;
                    IsNew = false;
            
                    StopWorker();

                    MorphoDevice device = Morpho.Instance.GetDeviceInfo(deviceId);
                    if (!device.Transaction.Status)
                    {
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick  += delegate
                        {
                                CheckConnection();
                                return; 
                        };
                        m.ShowAlert(device.Transaction.Error,MessageBox.AlertType.Alert);
                        return;
                    }
 

            if (device.Data.UserId == null  || device.Data.UserId == 0 || device.Data.UserId == ApplicationSessionState.User.Data.UserId)
                    {
                        LinkDeviceToUser(device.Data.DeviceId, device.Data.SerialNumber);
                        return;
                    }
                    else
                    {
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool answer) => 
                            {
                                if(answer)
                                {
                                    LinkDeviceToUser(device.Data.DeviceId, device.Data.SerialNumber);
                                    return;   
                                }
                                else
                                {
                                    CheckConnection();
                                    return;
                                }
                            };
                        m.ShowConfirmationMessage("This device is being used by another user. Do you still want to connect to it?", "Yes", "No");
                        return;
                    }
        }

        private void OnDeviceClickSelectionAction(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            if (result != null)
            {
                foreach (var r in result)
                {

                    SelectDeviceId(r.Id);
                    


 
                }
            }
 
//            if (Screener_MorphoActivity.CurrentDeviceId == 0)
//            {
//                StartActivity (typeof(Screener_MainMenuActivity));
//                this.Finish(); 
//            }
 
        }

 

        private void OnPiecesClickAction(int pieces)
        {
   
           
        
            if (pieces > 0)
            {
  
                ReferencePieces = pieces;
                RunOnUiThread(() => ReferenceNumber.Text=  Reference);
                        RunOnUiThread(() => txtBarcode.Text = string.Empty);
                        StartWorker();
            }

        }



        private void OnCommentsAction(string commentsdata, bool alarm, string substance, string file)
        {

            ScreeningTransactionModel screeningTransactionItem = new ScreeningTransactionModel();
            screeningTransactionItem.Comment = commentsdata;
            screeningTransactionItem.Pieces = ReferencePieces;
            screeningTransactionItem.Result = alarm ? ScreeningResult.ALARM : ScreeningResult.PASS; 
            screeningTransactionItem.PrescreenedCCSFId = 0;
            screeningTransactionItem.SampleNumber = file;
            screeningTransactionItem.SubstancesFound = substance;
            screeningTransactionItem.TaskId = currentScreeningTask.Data.TaskId;
            screeningTransactionItem.UserId = ApplicationSessionState.User.Data.UserId;
            screeningTransactionItem.Data = new ScreeningDeviceItem();
            screeningTransactionItem.Data.device = ScreeningDeviceTypes.ETD;
            screeningTransactionItem.Data.DeviceId = device.Data.DeviceId;
            screeningTransactionItem.Data.SerialNumber = device.Data.SerialNumber;



            CommunicationTransaction t = Morpho.Instance.SaveScreeningTransaction(screeningTransactionItem);
            if (!t.Status)
            {
                Screener_MorphoActivity.CurrentDeviceId = 0;
                this.SerialNumber = string.Empty;
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += delegate
                {
                    CheckConnection();
                    return; 
                };
                m.ShowAlert(t.Error, MessageBox.AlertType.Alert);
                return;
            }
            else
            {
                CheckConnection();
                if (alarm)
                {
                    GoToCamera(typeof(Screener_MorphoActivity), typeof(Screener_MorphoActivity), currentScreeningTask.Data.TaskId, this.barcode,false);
                }
                else
                {
                    StartActivity(typeof(Screener_MorphoActivity));
                    this.Finish();
                    //ClearResults();
                }

            }
            //ShowLastScanned();
        }

 
        void ShowLastScanned()
        {
            RunOnUiThread(delegate
                        {



                    blinker.Stop();
                    if(string.IsNullOrEmpty(Screener_MorphoActivity.LastReference))
                    {
                        txtStatus.Text = string.Empty;
                        txtStatus.Visibility = ViewStates.Gone;
                        lastScanLayout.Visibility = ViewStates.Gone;
                        //lblShipment.Text = "LAST SCAN:";
                        //txtShipment.Text = "N/A";
                        //lblPieces.Text = "PCS:";
                        //txtPieces.Text = "0";

                    }
                    else
                    {
                        txtStatus.Visibility = ViewStates.Gone;
                        lastScanLayout.Visibility = ViewStates.Visible;
                        lblShipment.Text = "LAST SCAN:";
                        txtShipment.Text = Screener_MorphoActivity.LastReference;
                        lblPieces.Text = "PCS:";
                        txtPieces.Text = LastReferencePieces.ToString();
                        lastScanLayout.Visibility = ViewStates.Gone;
                        lastScanLayout.Visibility = ViewStates.Visible;


                    }
                            
                        });
        }

    }
}





