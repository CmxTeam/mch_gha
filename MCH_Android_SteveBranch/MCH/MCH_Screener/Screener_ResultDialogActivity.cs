﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class Screener_ResultDialogActivity: DialogFragment
    {
        
        Button btnClose;
        Button btnOk;
        TextView txtComments;
        TextView txtMessage;
        TextView txtFile;
        TextView txtSubstance;
        TextView txtReference;
        TextView txtPieces;



        Action<string,bool,string,string> okClickAction;
        Activity context;
      
        
        string fileName;
        bool alarm;
        string substance;
        string title;
        string reference;
        int pieces;

        public Screener_ResultDialogActivity (Activity context, Action<string,bool,string,string> okClickAction,string title,string reference, int pieces, bool alarm, string substance, string file)
        {
 
            this.context = context;
            this.okClickAction = okClickAction;
            this.fileName=file;
            this.alarm=alarm;
            this.substance=substance;
            this.title = title;
            this.pieces = pieces;
            this.reference = reference;
        }
 

        //ADD this for error when rotating
        public Screener_ResultDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.Screener_ResultsDialog, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 

            txtComments = DialogInstance.FindViewById<TextView>(Resource.Id.txtComments);
            txtMessage = DialogInstance.FindViewById<TextView>(Resource.Id.txtMessage);
            txtFile = DialogInstance.FindViewById<TextView>(Resource.Id.txtFile);
            txtSubstance = DialogInstance.FindViewById<TextView>(Resource.Id.txtSubstance);
            txtReference = DialogInstance.FindViewById<TextView>(Resource.Id.txtReference);
            txtPieces = DialogInstance.FindViewById<TextView>(Resource.Id.txtPieces);
 
            btnClose = DialogInstance.FindViewById<Button>(Resource.Id.btnClose);
            btnClose.Click += OnCancelClick;

 

            btnOk.Click += OnOk_Click;

           
            if (alarm)
            {
                txtReference.Text = reference;
                txtPieces.Text = pieces.ToString();
                txtMessage.Text = "* * * A L A R M * * *";
                txtSubstance.Text =  substance;
                txtFile.Text = fileName;
            }
            else
            {
                txtReference.Text = reference;
                txtPieces.Text = pieces.ToString();
                txtMessage.Text = "Result: No Alarm";
                txtSubstance.Text = "None";
                txtFile.Text = fileName;
            }


            return DialogInstance;
        }


        private void OnOk_Click(object sender, EventArgs e)
        {

            if (alarm)
            {
                if (txtComments.Text != string.Empty)
                {
                    okClickAction.Invoke (txtComments.Text,this.alarm,this.substance,this.fileName);
                    Dismiss();
                }
                else
                {
                    MessageBox m = new MessageBox(context);
                    m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Comments) + ".");
                }
            }
            else
            {
                okClickAction.Invoke (txtComments.Text,this.alarm,this.substance,this.fileName);
                Dismiss();
            }


        }

  
        void OnCancelClick(object sender, EventArgs e)
        {

 

            okClickAction.Invoke (string.Empty,this.alarm,this.substance,this.fileName);
            Dismiss();
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle(this.title);
 
         
            base.OnActivityCreated(savedInstanceState);

            if (this.alarm)
            {
                Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.AlertIcon);
                MediaSounds.Instance.Alert(this.context);
            }
            else
            {
                Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.InfoIcon);
                MediaSounds.Instance.Beep(this.context);
            }
           

            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }

//
//        private void ShowAlertMessage(string message)
//        {
//            context.RunOnUiThread (delegate {
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                AlertDialog dialog = builder.Create();
//                dialog.SetIcon (Resource.Drawable.Icon);
//                dialog.SetTitle(Application.Context.GetText(Resource.String.app_name));
//                dialog.SetMessage(message);
//                dialog.SetCancelable(false);
//                dialog.SetButton (Application.Context.GetText(Resource.String.Ok),delegate{
//         
//                    //txtComments.RequestFocus ();
//                });
//                dialog.Show();
//            });
//        }
//
//        private void ShowAlertMessage(int message)
//        {
//            ShowAlertMessage(Application.Context.GetText(message));
//        }

   

    }
}




