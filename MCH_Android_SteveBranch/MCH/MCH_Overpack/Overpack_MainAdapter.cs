﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class Overpack_MainAdapter : BaseAdapter<OverPackItem> {

        List<OverPackItem> items;
        Activity context;
        Action<int> OnDeleteClickAction;
        Action<int> OnPrintClickAction;

        public Overpack_MainAdapter(Activity context, List<OverPackItem> items,Action<int> onDeleteClickAction,Action<int> onPrintClickAction): base()
        {
            this.OnDeleteClickAction = onDeleteClickAction;
            this.OnPrintClickAction = onPrintClickAction;
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override OverPackItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        { 

            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new

            if (item.OverpackType.ToString().ToUpper() == "LOOSE")
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.Overpack_LooseLayout, null);
            }
            else
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.Overpack_MainRow, null); 
            }
           

            ImageView btnPrint= view.FindViewById<ImageView>(Resource.Id.btnPrint);
            ImageView btnDelete= view.FindViewById<ImageView>(Resource.Id.btnDelete);
            TextView txtCount = view.FindViewById<TextView>(Resource.Id.txtCount);
            TextView txtType = view.FindViewById<TextView>(Resource.Id.txtType);
            TextView txtPieces = view.FindViewById<TextView>(Resource.Id.txtPieces);
 
            if (item.OverpackType.ToString().ToUpper() == "LOOSE")
            {
                txtCount.Text =  "LOOSE";
                txtPieces.Text = string.Format("PCS: {0}", item.Pieces); 
            }
            else
            {
                txtCount.Text = string.Format("OVERPACK {0}", item.OverPackItemNumber);
                txtType.Text = string.Format("**BUILT ON {0} {1}**", item.SkidOwnerType.ToString().ToUpper(), item.OverpackType.ToString().ToUpper());
                txtPieces.Text = string.Format("PCS: {0}", item.Pieces);
                btnDelete.Tag = position.ToString();
                btnDelete.Click += btnDelete_Click;

            }

            btnPrint.Tag = position.ToString();
            btnPrint.Click += btnPrint_Click;

            return view;
        }

        public void btnPrint_Click (object sender, EventArgs e)
        {
            
            ImageView btn = (ImageView)sender;




            MessageBox m = new MessageBox(this.context);
            m.OnConfirmationClick+= (bool result) => 
                {
                    if(result)
                    {
                        OnPrintClickAction.Invoke(int.Parse(btn.Tag.ToString()));
                    }
                };
            m.ShowConfirmationMessage("Are you sure you want to print this item?");

        }

        public void btnDelete_Click (object sender, EventArgs e)
        {
            ImageView btn = (ImageView)sender;
      
            MessageBox m = new MessageBox(this.context);
            m.OnConfirmationClick+= (bool result) => 
                {
                    if(result)
                    {
                        OnDeleteClickAction.Invoke(int.Parse(btn.Tag.ToString()));
                    }
                };
            m.ShowConfirmationMessage("Are you sure you want to delete this item?");
        }



    }
}




