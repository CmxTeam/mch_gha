﻿  
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Overpack_MainActivity : BaseActivity
    {
        OverPackItems list;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Overpack_MainLayout);
            Initialize();
            RefreshAll();


        }

//        public void PrintLabel()
//        {
//
//            var transaction = this.FragmentManager.BeginTransaction();
//
//            
//            var dialogFragment = new CopiesDialog(this, Overpack_SessionState.CurrentOverpackTask.Reference , GetDefautPrinter()  ,100);
//            dialogFragment.OkClicked += (int pieces) => 
//                {
//
//                    CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Overpack_SessionState.CurrentOverpackTask.HwbId,Overpack_SessionState.CurrentOverpackTask.AwbId,ApplicationSessionState.User.Data.AppUserId,0,pieces);
//                    if (!t.Status)
//                    {
//                        MessageBox mb = new MessageBox(this);
//                        mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
//                    }
//                    else
//                    {
//                        MessageBox mb = new MessageBox(this);
//                        mb.ShowAlert("Label was successfully printed.", MessageBox.AlertType.Information);
//                    }
//                };
//            dialogFragment.Cancelable = false;
//            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
//
//        }

        private void AddSkid()
        {
            var transaction = this.FragmentManager.BeginTransaction();

            var dialogFragment = new Overpack_AddDialog(this,Overpack_SessionState.CurrentOverpackTask.Reference ,Overpack_SessionState.CurrentOverpackTask.TotalPieces - Overpack_SessionState.CurrentOverpackTask.TotalSkidPieces,1,1);
            dialogFragment.OkClicked += () => 
                {
                    RefreshAll();   

                };
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            AddSkid();

 

        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            PrintItems();
        }
        private void btnDone_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void RefreshAll()
        {
            LoadData();
            LoadGrid();
        }

        private void LoadData()
        {

            OverPackTask task = MCH.Communication.Overpack.Instance.GetOverPackTask(ApplicationSessionState.SelectedWarehouseId,Overpack_SessionState.CurrentTaskId,  ApplicationSessionState.User.Data.UserId,Overpack_SessionState.CurrentOverpackTask.Reference);
            if (task.Transaction.Status == true && task.Data != null)
            {
                Overpack_SessionState.CurrentOverpackTask = task.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(task.Transaction.Error, MessageBox.AlertType.Error);
            }
 
            LoadDetail();
        }
 
        private void LoadDetail()
        {

            if (Overpack_SessionState.CurrentOverpackTask.HwbId == 0)
            {
                txtReference.Text = string.Format("AWB: {0}-{1}-{2}", Overpack_SessionState.CurrentOverpackTask.Origin, Overpack_SessionState.CurrentOverpackTask.Reference, Overpack_SessionState.CurrentOverpackTask.Destination);
            }
            else
            {
                txtReference.Text = string.Format("HWB: {0}-{1}-{2}", Overpack_SessionState.CurrentOverpackTask.Origin, Overpack_SessionState.CurrentOverpackTask.Reference, Overpack_SessionState.CurrentOverpackTask.Destination);
            }

            txtCounts.Text = string.Format("PCS: {0}     SKIDS: {1}",  Overpack_SessionState.CurrentOverpackTask.TotalPieces, Overpack_SessionState.CurrentOverpackTask.Skids);

           // txtCounts.Text = string.Format("PCS: {0} of {1}     SKIDS: {2}", Overpack_SessionState.CurrentOverpackTask.TotalSkidPieces, Overpack_SessionState.CurrentOverpackTask.TotalPieces, Overpack_SessionState.CurrentOverpackTask.Skids);
 
        }




        private void DoBack()
        {
            GoToScreen(Overpack_SessionState.BackScreen);
        }
 
  

        private void LoadGrid()
        {
          
            list = MCH.Communication.Overpack.Instance.GetOverPackItems(Overpack_SessionState.CurrentOverpackTask.OverPackTaskId);




            if (list.Transaction.Status == true && list.Data != null)
            {
                titleLabel.Text = GetText(Resource.String.Overpack).ToUpper() + string.Format(" - ({0})", list.Data.Count);


                int loose = Overpack_SessionState.CurrentOverpackTask.TotalPieces - Overpack_SessionState.CurrentOverpackTask.TotalSkidPieces;

                List<OverPackItem> items = new List<OverPackItem>();


                if (loose > 0)
                {
                    OverPackItem l = new OverPackItem();
                    l.OverPackItemId = 0;
                    l.OverPackItemNumber = 0;
                    l.OverpackType = "LOOSE";
                    l.Pieces = loose;
                    l.SkidOwnerType = SkidOwnerTypes.QAS;
                    items.Add(l);
                    foreach (OverPackItem o in list.Data)
                    {
                        items.Add(o);
                    }
                    list.Data = items;
                }
                else
                {
                    items = list.Data;
                }



                listView.Adapter = new Overpack_MainAdapter(this, items, DeleteItem,PrintItem);
                listView.ItemClick -= OnListItemClick;
                listView.ItemClick += OnListItemClick; 
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(list.Transaction.Error, MessageBox.AlertType.Error);
            }

        }


        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            OverPackItem item = list.Data[e.Position];
            if (item.OverPackItemId == 0)
            {
                return;
            }


            var transaction = this.FragmentManager.BeginTransaction();

             
            int maxValue =  Overpack_SessionState.CurrentOverpackTask.TotalPieces - Overpack_SessionState.CurrentOverpackTask.TotalSkidPieces+item.Pieces;
            var dialogFragment = new Overpack_AddDialog(this,Overpack_SessionState.CurrentOverpackTask.Reference ,maxValue,1, item.Pieces ,item);
            dialogFragment.OkClicked += () => 
                {
                    RefreshAll();   

                };
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }


//        private void PrintItem(int position)
//        {
//            OverPackItem item = list.Data[position];
//
//            var transaction = this.FragmentManager.BeginTransaction();
//
//
//            var dialogFragment = new CopiesDialog(this, Overpack_SessionState.CurrentOverpackTask.Reference , GetDefautPrinter()  ,100);
//            dialogFragment.OkClicked += (int pieces) => 
//                {
//
//                    CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Overpack_SessionState.CurrentOverpackTask.HwbId,Overpack_SessionState.CurrentOverpackTask.AwbId,ApplicationSessionState.User.Data.AppUserId,item.OverPackItemId ,pieces);
//                    if (!t.Status)
//                    {
//                        MessageBox mb = new MessageBox(this);
//                        mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
//                    }
//                    else
//                    {
//                        MessageBox mb = new MessageBox(this);
//                        mb.ShowAlert("Label was successfully printed.", MessageBox.AlertType.Information);
//                    }
//                };
//            dialogFragment.Cancelable = false;
//            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
//
//        }
        private void DeleteItem(int position)
        {
            OverPackItem item = list.Data[position];
            CommunicationTransaction t = MCH.Communication.Overpack.Instance.DeleteOverpack(Overpack_SessionState.CurrentOverpackTask.OverPackTaskId,item.OverPackItemId,ApplicationSessionState.User.Data.UserId);
            if (t.Status)
            {
                RefreshAll();   
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
        }
 

        private void PrintItem(int position)
        {
            OverPackItem item = list.Data[position];
            if (item.OverPackItemId == 0)
            {
                var transaction = this.FragmentManager.BeginTransaction();
                var dialogFragment = new CopiesDialog(this, Overpack_SessionState.CurrentOverpackTask.Reference , GetDefautPrinter()  ,item.Pieces,"ENTER NUMBER OF LOOSE COPIES:");
                dialogFragment.OkClicked += (int pieces) => 
                    {

                        CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Overpack_SessionState.CurrentOverpackTask.HwbId,Overpack_SessionState.CurrentOverpackTask.AwbId,ApplicationSessionState.User.Data.UserId,0 ,pieces);
                        if (!t.Status)
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                        else
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowAlert("Labels printed successfully.", MessageBox.AlertType.Information);
                        }
                    };
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
            }
            else
            {
                CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Overpack_SessionState.CurrentOverpackTask.HwbId,Overpack_SessionState.CurrentOverpackTask.AwbId,ApplicationSessionState.User.Data.UserId,item.OverPackItemId ,1);
                if (!t.Status)
                {
                    MessageBox mb = new MessageBox(this);
                    mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }
                else
                {
                    MessageBox mb = new MessageBox(this);
                    mb.ShowAlert("Labels printed successfully.", MessageBox.AlertType.Information);
                }
            }



        }




        public void PrintItems()
        {


            int loose = Overpack_SessionState.CurrentOverpackTask.TotalPieces - Overpack_SessionState.CurrentOverpackTask.TotalSkidPieces;
            if (loose > 0)
            {

                string str = string.Empty;
                foreach (var s in list.Data)
                {
                    if (str == string.Empty)
                    {
                        str = s.OverpackType.ToString() + s.OverPackItemNumber; 
                    }
                    else
                    {
                        str = ", " + s.OverpackType.ToString() + s.OverPackItemNumber; 
                    }
                }

                string r = string.Empty;
                if (str == string.Empty)
                {
                    r = Overpack_SessionState.CurrentOverpackTask.Reference; 
                }
                else
                {
                    r = Overpack_SessionState.CurrentOverpackTask.Reference + "\n" + str; 
                }

                var transaction = this.FragmentManager.BeginTransaction();
                var dialogFragment = new CopiesDialog(this, r , GetDefautPrinter()  ,loose, "ENTER NUMBER OF LOOSE COPIES:");
                dialogFragment.OkClicked += (int pieces) => 
                    {
 

                        foreach (OverPackItem o in list.Data)
                        {
                            int copies = 0;
                            if(o.OverpackType.ToUpper()=="LOOSE")
                            {
                                copies = pieces;
                            }
                            else
                            {
                                copies = 1;
                            }

                            CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Overpack_SessionState.CurrentOverpackTask.HwbId,Overpack_SessionState.CurrentOverpackTask.AwbId,ApplicationSessionState.User.Data.UserId,o.OverPackItemId ,copies);
                            if (!t.Status)
                            {
                                MessageBox mb = new MessageBox(this);
                                mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                return;
                            }


                        }
                        MessageBox m = new MessageBox(this);
                        m.ShowAlert("Labels printed successfully.", MessageBox.AlertType.Information);




                    };
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 



            }
            else
            {


                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick += (bool result) => 
                    {
                        if(result)
                        {
                            foreach (OverPackItem o in list.Data)
                            {
                                CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Overpack_SessionState.CurrentOverpackTask.HwbId,Overpack_SessionState.CurrentOverpackTask.AwbId,ApplicationSessionState.User.Data.UserId,o.OverPackItemId ,1);
                                if (!t.Status)
                                {
                                    MessageBox mb = new MessageBox(this);
                                    mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                    return;
                                }
                            }
                            MessageBox m = new MessageBox(this);
                            m.ShowAlert("Labels printed successfully.", MessageBox.AlertType.Information);
                        }

                    };
                msg.ShowConfirmationMessage("Are you sure you want to print all labels", "YES", "NO");



            }
 
        }



    }
}




