﻿
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Lookup_MainActivity : BaseActivity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.LookupMainLayout);
            Initialize();
            LoadDetail();
            LoadGrid();
        }

        private void RefreshAll()
        {
            LoadData();
            LoadGrid();
        }


        private void LoadGrid()
        {
           
            listView.Adapter = new Lookup_MainAdapter(this,Lookup_SessionState.CurrentRelocateTask.LocationList);
            listView.ItemClick -= OnListItemClick;
            listView.ItemClick += OnListItemClick; 
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            LocationListItem item =Lookup_SessionState.CurrentRelocateTask.LocationList[e.Position];
             
            Location l = Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, item.Location);
            if (l.Transaction.Status)
            {
                if (l.Data != null)
                {
                    var transaction = this.FragmentManager.BeginTransaction();
                    var dialogFragment = new PiecesDialog(this,string.Format("RELOCATE {0} (PCS: {1})", item.Location,item.Pieces),null,0 ,item.Pieces );
                    dialogFragment.OkClicked += (int pieces) => 
                        {

                            RelocatePieces(l.Data.LocationId,pieces);


                        };
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert("Unable to find location.", MessageBox.AlertType.Error);    
                }
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(l.Transaction.Error, MessageBox.AlertType.Error);               
            }
        }

        private void RelocatePieces(long oldLocationId, int pcs)
        {

            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new LocationDialogActivity(this,null, "Location","Select Location",true, null);
            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked += (Barcode barcode) => 
                {
                    CommunicationTransaction t = MCH.Communication.Relocate.Instance.RelocatePieces(Lookup_SessionState.CurrentRelocateTask.ReferenceId,Lookup_SessionState.CurrentRelocateTask.ReferenceType,oldLocationId,barcode.Id ,pcs,Lookup_SessionState.CurrentRelocateTask.RelocateTaskId, ApplicationSessionState.User.Data.UserId);
                    if(t.Status)
                    {
                        RefreshAll();
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }
                }; 
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void LoadDetail()
        {
            txtReference.Text =string.Format("{0}: {1} {2} {3}",Lookup_SessionState.CurrentRelocateTask.ReferenceType.ToString() ,Lookup_SessionState.CurrentRelocateTask.Origin,Lookup_SessionState.CurrentRelocateTask.Reference, Lookup_SessionState.CurrentRelocateTask.Destination);

            if (Lookup_SessionState.CurrentRelocateTask.Skids > 0)
            {
                txtPieces.Text = string.Format("PCS: {0} of {1}     SKIDS: {2}     WGT: {3:0.0}{4}", Lookup_SessionState.CurrentRelocateTask.ScannedPieces, Lookup_SessionState.CurrentRelocateTask.TotalPieces,Lookup_SessionState.CurrentRelocateTask.Skids,Lookup_SessionState.CurrentRelocateTask.Weight,Lookup_SessionState.CurrentRelocateTask.WeightUOM).ToUpper();

            }
            else
            {
                txtPieces.Text = string.Format("PCS: {0} of {1}     WGT: {2:0.0}{3}", Lookup_SessionState.CurrentRelocateTask.ScannedPieces, Lookup_SessionState.CurrentRelocateTask.TotalPieces,Lookup_SessionState.CurrentRelocateTask.Weight,Lookup_SessionState.CurrentRelocateTask.WeightUOM).ToUpper();
            }

        }


  

        private void LoadData()
        {

            RelocateTask data=  MCH.Communication.Relocate.Instance.GetRelocateTask(ApplicationSessionState.SelectedWarehouseId,  ApplicationSessionState.User.Data.UserId,Lookup_SessionState.CurrentRelocateTask.Reference);
            if (data.Transaction.Status)
            {
                Lookup_SessionState.CurrentRelocateTask = data.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(data.Transaction.Error, MessageBox.AlertType.Error);
            }
            LoadDetail();
        }



        private void DoBack()
        {
            GotoMainMenu();
        }
 
  

    }
}

