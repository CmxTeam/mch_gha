﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class Lookup_MainAdapter : BaseAdapter<LocationListItem> {

        List<LocationListItem> items ;
        Activity context;


        public Lookup_MainAdapter(Activity context, List<LocationListItem> items)
            : base()
        {
            this.context = context;



            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override LocationListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {


            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new



            view = context.LayoutInflater.Inflate(Resource.Layout.LookupMainRow, null);
     
            TextView txtPieces= view.FindViewById<TextView>(Resource.Id.txtPieces);
            TextView txtLocation= view.FindViewById<TextView>(Resource.Id.txtLocation);
            txtPieces.Text =string.Format("PCS: {0}",item.Pieces);
           


            txtLocation.Text =string.Format("LOC: {0}",item.Location);
            return view;
        }






    }
}





 

