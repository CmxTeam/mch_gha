﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using Android.Views.InputMethods;
using MCH.Communication;
namespace MCH
{



    public class Inventory_DiscrepancyDialog: DialogFragment
    {
        ListView listView;
       
        List<DiscrepancyItem> list;

        Activity context;


     
        Button btnCancel;
        Button btnOk;

        public delegate void OkClickActionEventHandler();
        public event OkClickActionEventHandler OkClicked;

        public Inventory_DiscrepancyDialog (Activity context, List<DiscrepancyItem> list )
        {
            this.context = context;
            this.list =  list;

        }

        //ADD this for error when rotating
        public Inventory_DiscrepancyDialog()
        {
            Dismiss();
        }



        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.Inventory_DiscrepancyDialog, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            listView = DialogInstance.FindViewById<ListView>(Resource.Id.SelectionListView); 

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk);
            btnOk.Click += OnOkClick;
            btnCancel = DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;

 
            LoadData();
 
            return DialogInstance;
        }


        void LoadData()
        {
       
  
            var adapter  = new Inventory_DiscrepancyAdapter(this.context, list);
            listView.Adapter = adapter;
            listView.RequestFocus();
            listView.ItemClick -= OnListItemClick;
            listView.ItemClick += OnListItemClick; 

        }


        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            //Toast.MakeText (this.context, e.Position.ToString(), ToastLength.Long).Show ();
 
         
        }
        
        void OnCancelClick(object sender, EventArgs e)
        {
 
            Dismiss();
        }




        void OnOkClick(object sender, EventArgs e)
        {
            if (OkClicked != null)
                OkClicked();

            Dismiss();
        }
 

 

        public override void OnActivityCreated( Bundle savedInstanceState)
        {

//            if (ApplicationSessionState.IsTest(this.context))
//            {
//
//                Dialog.Window.SetTitle ("DISCREPANCY" + " (" + GetString(Resource.String.Connection_Mode_Test) + ")");
//
//            }
//            else
//            {
//
               Dialog.Window.SetTitle ("DISCREPANCY");
//
//            }


            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }





    }
}


