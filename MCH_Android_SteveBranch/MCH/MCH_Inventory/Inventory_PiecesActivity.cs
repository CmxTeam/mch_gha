
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    //[Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle.Basic", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Inventory_PiecesActivity : BaseActivity
    {


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Inventory_PiecesLayout);
            Initialize();

            LoadDetail();



            if (Inventory_SessionState.CurrentShipment.Data.ScannedPieces >= Inventory_SessionState.CurrentShipment.Data.TotalPieces )
            {
                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick += (bool result) => 
                    {
                        if(result)
                        {
                            Reset(); 
                        }
                        else
                        {
                            //DoBack();
                        }
                    };
      
//                msg.OnNeutalClick += delegate()
//                {
//                        Relocate();
//                };


                msg.OnNeutalClick += () => Relocate();
 
                
                msg.ShowConfirmationMessage("All the pieces for this shipment were inventoried. Select one of the options.", "RESET", "CANCEL", "RELOCATE");
            }

        }

        private void LoadDetail()
        {

            if (Inventory_SessionState.CurrentShipment.Data.AvailablePieces > 0)
            {
                np.MaxValue = Inventory_SessionState.CurrentShipment.Data.AvailablePieces;
                np.MinValue = 1;
                np.Value = Inventory_SessionState.CurrentShipment.Data.AvailablePieces;
            }
            else
            {
                np.MaxValue = 0;
                np.MinValue = 0;
                np.Value = 0;
            }



            if (Inventory_SessionState.CurrentShipment.Data.ScannedPieces > 0 && Inventory_SessionState.CurrentShipment.Data.TotalPieces > Inventory_SessionState.CurrentShipment.Data.ScannedPieces)
            {
                txtPieces.SetTextColor(Color.Red);
            }
            else
            {
                txtPieces.SetTextColor(Color.Black);
            }

            txtPieces.Text = string.Format("{0} of {1}",Inventory_SessionState.CurrentShipment.Data.ScannedPieces, Inventory_SessionState.CurrentShipment.Data.TotalPieces);
            txtLocation.Text = "CURRENT LOCATION: " + Inventory_SessionState.CurrentLocation;


     
                txtAwb.Text = string.Format("{0}-{1}-{2}", Inventory_SessionState.CurrentShipment.Data.Origin , Inventory_SessionState.CurrentShipment.Data.Reference,Inventory_SessionState.CurrentShipment.Data.Destination  );
 
                
  

            txtWeight.Text = string.Format("{0:0.0} {1}", Inventory_SessionState.CurrentShipment.Data.Weight, Inventory_SessionState.CurrentShipment.Data.WeightUOM); 
            txtLocations.Text =  Inventory_SessionState.CurrentShipment.Data.Locations;
            //EditTextListener.Text = Inventory_SessionState.CurrentShipment.Data.AvailablePieces.ToString();
            txtTotalPieces.Text = " OF " + Inventory_SessionState.CurrentShipment.Data.TotalPieces ;


            //ShowSelectedPcs(np.Value);

            //txtPieces.Visibility == ViewStates.Gone;
            np.RequestFocus();
        }
        private void DoBack()
        {
            this.GoToScreen(typeof(Inventory_MainActivity));
        }


        private void ScanShipment(bool gotoSnap)
        {
            int enteredpieces = 0;

            if (int.TryParse(np.Value.ToString(), out enteredpieces))
            {
                if(enteredpieces==0)
                {
                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            LoadDetail();
                        };
                    m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);
                    return;
                }

                if(enteredpieces > Inventory_SessionState.CurrentShipment.Data.AvailablePieces)
                {
                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            LoadDetail();
                        };
                    m.ShowAlert(Resource.String.More_Pieces , MessageBox.AlertType.Information);
                    return;     
                }



                CommunicationTransaction t =  Inventory.Instance.ScanShipment(ApplicationSessionState.User.Data.UserId,Inventory_SessionState.CurrentShipment.Data.AwbId,Inventory_SessionState.CurrentShipment.Data.HwbId,Inventory_SessionState.CurrentLocationId,enteredpieces,Inventory_SessionState.CurrentTaskId );
                if (t.Status)
                {
                    Inventory_SessionState.LastScanShipment = Inventory_SessionState.CurrentShipment.Data.Origin +"-"+ Inventory_SessionState.CurrentShipment.Data.Reference +"-"+ Inventory_SessionState.CurrentShipment.Data.Destination;
                    Inventory_SessionState.LastScanPieces = enteredpieces;


                    MediaSounds.Instance.Beep(this);
                    if (gotoSnap)
                    {
                        GoToCamera(typeof( Inventory_MainActivity),typeof( Inventory_MainActivity ),Inventory_SessionState.CurrentTaskId,Inventory_SessionState.CurrentShipment.Data.Reference);
                    }
                    else
                    {
                        DoBack();
                    }


                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }


            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {
                        LoadDetail();
                    };
                m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);

            }




        }

 

        void Reset()
        {

            MessageBox ms = new MessageBox(this);
            ms.OnConfirmationClick += (bool result) => 
                {

                    if(result)
                    {
                        CommunicationTransaction t = MCH.Communication.Inventory.Instance.ResetInventory(Inventory_SessionState.CurrentShipment.Data.AwbId,Inventory_SessionState.CurrentShipment.Data.HwbId ,Inventory_SessionState.CurrentTaskId, ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            ReloadData();
                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }
                };
            ms.ShowConfirmationMessage("Are you sure you want to reset the inventory for this shipment?");
 
        }

        void btnOkSnap_Click(object sender, EventArgs e)
        {
            txtHidden.RequestFocus();
            ScanShipment(true);
        }

        void btnOk_Click(object sender, EventArgs e)
        {
            txtHidden.RequestFocus();
            ScanShipment(false);
        }




        private void RelocateAwbPieces(long oldLocationId, int pcs)
        {
            if(pcs==0)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {
                        LoadDetail();
                    };
                m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);
                return;
            }

            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new LocationDialogActivity(this,null, "Location","Select Location",true, LocationTypes.Area);
            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked += (Barcode barcode) => 
                {

                    CommunicationTransaction t = MCH.Communication.Inventory.Instance.RelocatePieces(Inventory_SessionState.CurrentShipment.Data.AwbId,Inventory_SessionState.CurrentShipment.Data.HwbId ,oldLocationId,barcode.Id ,pcs,Inventory_SessionState.CurrentTaskId, ApplicationSessionState.User.Data.UserId);
                    if(t.Status)
                    {
 
                        ReloadData();
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }
                }; 
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }


        void ReloadData()
        {
            Inventory_SessionState.CurrentShipment = MCH.Communication.Inventory.Instance.ValidateShipment(Inventory_SessionState.CurrentShipment.Data.Reference, ApplicationSessionState.SelectedWarehouseId, Inventory_SessionState.CurrentTaskId, ApplicationSessionState.User.Data.UserId);

            if (Inventory_SessionState.CurrentShipment.Transaction.Status)
            {
                LoadDetail();
            }

        }


        private void OnLocationEditPcsCountSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {

            foreach (var r in result)
            {

                string location;
                string[] locarray = r.Name.Split('(');
                if (locarray.Length > 1)
                {
                    location = locarray[0].Trim();
                }
                else
                {
                    location = r.Name;
                }


                Location l = Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, location);
                if (l.Transaction.Status && l.Data != null)
                {
                    InventoryItemCount count = MCH.Communication.Inventory.Instance.GetPiecesCountInLocation(Inventory_SessionState.CurrentShipment.Data.AwbId,Inventory_SessionState.CurrentShipment.Data.HwbId, l.Data.LocationId,Inventory_SessionState.CurrentTaskId);
                    if (count.Transaction.Status)
                    {
                        var transaction = this.FragmentManager.BeginTransaction();
                        var dialogFragment = new PiecesDialog(this,string.Format("{0} ({1})",Inventory_SessionState.CurrentShipment.Data.Reference,l.Data.Location),0 ,count.Data );
                        dialogFragment.SetMinToZero();
                        dialogFragment.OkClicked += (int pieces) => 
                            {

                                CommunicationTransaction t = MCH.Communication.Inventory.Instance.EditPiecesCount(Inventory_SessionState.CurrentShipment.Data.AwbId,Inventory_SessionState.CurrentShipment.Data.HwbId , l.Data.LocationId ,pieces,Inventory_SessionState.CurrentTaskId, ApplicationSessionState.User.Data.UserId);
                                if(t.Status)
                                {

                                    ReloadData();
                                }
                                else
                                {
                                    MessageBox msg = new MessageBox(this);
                                    msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                }


                            };
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
                    }  
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(count.Transaction.Error, MessageBox.AlertType.Error);
                    }
                }




                return;
            }

        }



        private void OnLocationSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {

            foreach (var r in result)
            {

                string location;
                string[] locarray = r.Name.Split('(');
                if (locarray.Length > 1)
                {
                    location = locarray[0].Trim();
                }
                else
                {
                    location = r.Name;
                }


                Location l = Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, location);
                if (l.Transaction.Status && l.Data != null)
                {
                    InventoryItemCount count = MCH.Communication.Inventory.Instance.GetPiecesCountInLocation(Inventory_SessionState.CurrentShipment.Data.AwbId,Inventory_SessionState.CurrentShipment.Data.HwbId, l.Data.LocationId,Inventory_SessionState.CurrentTaskId);
                    if (count.Transaction.Status)
                    {
                        var transaction = this.FragmentManager.BeginTransaction();
                        var dialogFragment = new PiecesDialog(this,string.Format("{0} ({1})",Inventory_SessionState.CurrentShipment.Data.Reference,l.Data.Location),null,0 ,count.Data );
                        dialogFragment.OkClicked += (int pieces) => 
                            {

                                RelocateAwbPieces(l.Data.LocationId,pieces);


                            };
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
                    }  
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(count.Transaction.Error, MessageBox.AlertType.Error);
                    }
                }
 



                return;
            }

        }


        public void Edit()
        {
            string locationList = Inventory_SessionState.CurrentShipment.Data.Locations;
            if (locationList.ToLower() == "n/a")
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("This shipment does not have any locations.", MessageBox.AlertType.Information);
                return;
            }


            string[] locations = locationList.Split(',');

            List< MCH.Communication.SelectionItem> locationlist = new List< MCH.Communication.SelectionItem>();

            for (int i = 0; i < locations.Length; i++)
            {
                if (Inventory_SessionState.CurrentShipment.Data.AwbId == 0)
                {
                    locationlist.Add(new SelectionItem(locations[i].Trim() ,Inventory_SessionState.CurrentShipment.Data.HwbId,Resource.Drawable.Map));
                }
                else
                {
                    locationlist.Add(new SelectionItem(locations[i].Trim() ,Inventory_SessionState.CurrentShipment.Data.AwbId,Resource.Drawable.Map)); 
                }


            }


            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Edit Piece Count", this, locationlist,OnLocationEditPcsCountSelection , SelectionListAdapter.SelectionMode.SingleSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            return;
        }

        public void PrintLabel()
        {

            var transaction = this.FragmentManager.BeginTransaction();

            var dialogFragment = new CopiesDialog(this, Inventory_SessionState.CurrentShipment.Data.Reference,GetDefautPrinter(),100);
            dialogFragment.OkClicked += (int pieces) => 
                {

                    CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Inventory_SessionState.CurrentShipment.Data.HwbId,Inventory_SessionState.CurrentShipment.Data.AwbId,ApplicationSessionState.User.Data.UserId,0,pieces);
                    if (!t.Status)
                    {
                        MessageBox mb = new MessageBox(this);
                        mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }
                    else
                    {
                        MessageBox mb = new MessageBox(this);
                        mb.ShowAlert("Label was successfully printed.", MessageBox.AlertType.Information);
                    }
                };
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 

        }

        public void Relocate() 
        {
      
            string locationList = Inventory_SessionState.CurrentShipment.Data.Locations;
            if (locationList.ToLower() == "n/a")
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("This shipment does not have any locations.", MessageBox.AlertType.Information);
                return;
            }


            string[] locations = locationList.Split(',');

            List< MCH.Communication.SelectionItem> locationlist = new List< MCH.Communication.SelectionItem>();

            for (int i = 0; i < locations.Length; i++)
            {
                if (Inventory_SessionState.CurrentShipment.Data.AwbId == 0)
                {
                    locationlist.Add(new SelectionItem(locations[i].Trim() ,Inventory_SessionState.CurrentShipment.Data.HwbId,Resource.Drawable.Map));
                }
                else
                {
                    locationlist.Add(new SelectionItem(locations[i].Trim() ,Inventory_SessionState.CurrentShipment.Data.AwbId,Resource.Drawable.Map)); 
                }


            }


            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Relocate", this, locationlist,OnLocationSelection , SelectionListAdapter.SelectionMode.SingleSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            return;

        }


        public void History()
        {
            InventoryShipments shipments = Inventory.Instance.GetLocationsHistory(ApplicationSessionState.SelectedWarehouseId,Inventory_SessionState.CurrentShipment.Data.Reference,  Inventory_SessionState.CurrentTaskId, ApplicationSessionState.User.Data.UserId);
            if (shipments.Transaction.Status )
            {
                if (shipments.Data != null)
                {
                    
                }
                else
                {
                    MessageBox msg = new MessageBox(this);
                    msg.ShowAlert("There is no history available for this shipment.", MessageBox.AlertType.Information);
                }
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(shipments.Transaction.Error, MessageBox.AlertType.Error);
            }
        }

    }
}


