﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class Inventory_DiscrepancyAdapter : BaseAdapter<DiscrepancyItem> {

        List<DiscrepancyItem> items;
        Activity context;
   

        public Inventory_DiscrepancyAdapter(Activity context,  List<DiscrepancyItem> items): base()
        {
           
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override DiscrepancyItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {




            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new
            view = context.LayoutInflater.Inflate(Resource.Layout.Inventory_DiscrepancyRow, null);

            TextView txtShipment= view.FindViewById<TextView>(Resource.Id.txtShipment);
            TextView txtCount= view.FindViewById<TextView>(Resource.Id.txtCount);
     
            if (item.AwbId == 0)
            {
                txtShipment.Text =string.Format("HWB: {0} {1} {2}", item.Origin ,item.Reference,item.Destination  );
            }
            else
            {
                txtShipment.Text =string.Format("AWB: {0} {1} {2}", item.Origin ,item.Reference,item.Destination  );
            }

            txtCount.Text =string.Format("PCS: {0} of {1}     {2} ({3})",item.ScannedPieces, item.TotalPieces,item.DiscrepancyType,item.DiscrepancyCount);
            

          


            return view;
        }



    }
}






