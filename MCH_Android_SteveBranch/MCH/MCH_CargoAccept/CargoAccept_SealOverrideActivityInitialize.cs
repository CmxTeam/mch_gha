﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCH
{
     		
    public partial class CargoAccept_SealOverrideActivity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;

        TextView FullName;
        TextView Company;
        TextView Counts;
        TextView Locations;
        TextView Date;
        TextView ProgressText;
        ProgressBar bar;
        ImageView imageIcon;
        ImageView AirlineImage;
        Button validateseal;
        RadioButton ReScreenOption;
        RadioButton NoScreenOption;
        EditText sealcomments;

        private void Initialize()
        {
            string title = Intent.GetStringExtra ("Title") ?? ""; 
            Initialize(title);
        }

        private void Initialize(int title)
        {
            Initialize(Application.Context.GetText(title));
        }

        private void Initialize(string tilte)
        {

            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;

            FullName = FindViewById<TextView>(Resource.Id.FullName);
            Company = FindViewById<TextView>(Resource.Id.Company);
            Counts = FindViewById<TextView>(Resource.Id.Counts);
            Locations = FindViewById<TextView>(Resource.Id.Locations);
            Date = FindViewById<TextView>(Resource.Id.Date);
            ProgressText = FindViewById<TextView>(Resource.Id.Progress);
            bar = FindViewById<ProgressBar>(Resource.Id.TaskProgressBar);
            imageIcon = FindViewById<ImageView>(Resource.Id.taskIcon);
            AirlineImage= FindViewById<ImageView>(Resource.Id.AirlineImage);
            ReScreenOption= FindViewById<RadioButton>(Resource.Id.ReScreenOption);
            NoScreenOption= FindViewById<RadioButton>(Resource.Id.NoScreenOption);
            sealcomments= FindViewById<EditText>(Resource.Id.sealcomments);
            validateseal= FindViewById<Button>(Resource.Id.validateseal);

            validateseal.Click += OnValidateSealClick;

            headerText.Text = tilte;

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                headerImage.SetImageResource (Resource.Drawable.Clock);
            }

        }


        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();
 
            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();
 
            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}

