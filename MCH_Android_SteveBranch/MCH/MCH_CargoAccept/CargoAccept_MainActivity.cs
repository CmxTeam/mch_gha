﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.Res;
using MCH.Communication;
using Android.InputMethodServices;


namespace MCH
{
    [Activity (ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]  		
    public class CargoAccept_MainActivity : BaseActivity
    {

        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;
        EditText search;
        TextView titleLabel; 
        ImageView optionButton;
        ImageView backButton;
        CargoAcceptTasks cargoAcceptTasks;
        ImageView imageHeader;
        ImageButton CheckStatusButton;
        ListView listView;
        ImageButton LocationsOptionButton;
        protected override void OnCreate(Bundle bundle)
        {
            
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.CargoAccept_MainLayout);
   
            InitControls();

            RefreshData(string.Empty,false);

        }

        protected void InitControls()
        {
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            search = FindViewById<EditText>(Resource.Id.searchtextbox);
            EditTextListener = new EditTextEventListener(search);
            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
            {
                    string data = e.Data;
                    if (e.IsBarcode)
                    {
                        //Parse Barcode Data
                    }
                    RefreshData(data,e.IsBarcode);
            };

            ClearSearch = FindViewById<ImageButton>(Resource.Id.ClearSearch);
            SearchButton = FindViewById<ImageButton>(Resource.Id.SearchButton);
            titleLabel = FindViewById<TextView>(Resource.Id.txtHeader);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            imageHeader = FindViewById<ImageView>(Resource.Id.imageHeader);
            CheckStatusButton = FindViewById<ImageButton>(Resource.Id.CheckStatusButton);
            LocationsOptionButton= FindViewById<ImageButton>(Resource.Id.LocationsOptionButton);
            listView = FindViewById<ListView>(Resource.Id.grid);

            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name;
            
            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            ClearSearch.Click += OnClearSearch_Click;
            CheckStatusButton.Click += OnCheckStatusButton_Click;
            LocationsOptionButton.Click += LocationsOptionButton_Click;
            SearchButton.Click += OnSearchButton_Click;

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                imageHeader.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }

        }

        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            RefreshData(string.Empty,false);
            search.RequestFocus ();
        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {
            RefreshData(search.Text,false);
        }

        private void GoToTask(CargoAcceptTaskItem task)
        {
            CargoAccept_SessionState.CurrentCargoAcceptTask = task;

            ImageGallery.Instance.Clear();
            ImageGallery.Instance.ImageItems.Clear();
            ImageGallery.CurrentTaskId = task.TaskId;
            ImageGallery.BackScreen = typeof(CargoAccept_MainActivity);
            ImageGallery.NextScreen = typeof(CargoAccept_ValidateSealActivity);
            string taskTitle =string.Format("{0}\n{1}", CargoAccept_SessionState.CurrentCargoAcceptTask.Name,CargoAccept_SessionState.CurrentCargoAcceptTask.Company);
            var nextScreen = new Intent(this, typeof(Camera_GalleryActivity));
            nextScreen.PutExtra("Title", taskTitle);
            nextScreen.PutExtra("Icon", ApplicationSessionState.SelectedMenuItem.IconKey);
            StartActivity(nextScreen);
            this.Finish();


        }

        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
 
                Action<int> ImageClickAction = OnImageClickAction;
            
                    cargoAcceptTasks = MCH.Communication.CargoAccept.Instance.GetCargoAcceptTasks(ApplicationSessionState.User.Data.UserId);

                RunOnUiThread (delegate {
                    if(cargoAcceptTasks.transaction.Status)
                    {
                        if(cargoAcceptTasks.CargoAcceptTaskItems!=null)
                        {

                                if(searchData!=string.Empty)
                                {
                                    List<CargoAcceptTaskItem> taskList = new List<CargoAcceptTaskItem>();
                                    foreach (var task in cargoAcceptTasks.CargoAcceptTaskItems)
                                    {
                                        if (task.ToString().ToLower().Contains(searchData.ToLower()))
                                        {
                                            taskList.Add(task);   
                                        }
                                    }
                                    cargoAcceptTasks.CargoAcceptTaskItems =taskList;
                                }

                                if( cargoAcceptTasks.CargoAcceptTaskItems.Count == 1 && isBarcode)
                                {
                                    GoToTask( cargoAcceptTasks.CargoAcceptTaskItems[0]);
                                }

                            listView.Adapter = new CargoAccept_MainListAdapter(this, cargoAcceptTasks.CargoAcceptTaskItems,ImageClickAction);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;  
                        
                            }
                    }

                        search.Text = string.Empty;
                        search.RequestFocus ();

                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();



        }

        private void OnImageClickAction(int position)
        {

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new ImageDialog(this,cargoAcceptTasks.CargoAcceptTaskItems[position].TaskImage );
                dialogFragment.Cancelable = true;
                dialogFragment.Show(transaction, "MCH");

            search.RequestFocus ();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            GoToTask(cargoAcceptTasks.CargoAcceptTaskItems[e.Position]);  
        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            GotoMainMenu();
        }

        private void OnCheckStatusButton_Click(object sender, EventArgs e)
        {
             
        }

        private void LocationsOptionButton_Click(object sender, EventArgs e)
        {
            PopupMenu menu = new PopupMenu(this, LocationsOptionButton);
                menu.Menu.Add(0, 0, 1, "Not Started");
                menu.Menu.Add(0, 0, 2, "In Progress");
                menu.Menu.Add(0, 0, 3, "Completed");
                menu.MenuInflater.Inflate(Resource.Menu.PopupMenu, menu.Menu);
                menu.MenuItemClick += (s1, arg1) =>
                {
                    Toast.MakeText(this, arg1.Item.ToString(), ToastLength.Long).Show();
                };
                menu.Show(); 
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
                    options.Add(new OptionItem(GetText(Resource.String.Refresh),OptionActions.Refresh ,Resource.Drawable.Refresh));
                    options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
                    options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
                    options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

                   Action<OptionItem> OptionClickAction = OnOptionClickAction;
                var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new OptionDialog(this, options, OptionClickAction);
                dialogFragment.Cancelable = true;
                dialogFragment.Show(transaction, "MCH");
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
            case OptionActions.Refresh:
                    RefreshData(string.Empty,false);
                    break;
            case  OptionActions.Logout:
                ConfirmLogOut();
                break;
            case  OptionActions.Exit:
                ConfirmExit();
                break;
            case  OptionActions.MainMenu:
                    GotoMainMenu();
                break;
            }
    
            search.RequestFocus ();

        }

        public override void OnBackPressed ()
        {
            GotoMainMenu();
        }

        protected override void OnRestart()
        {
            RefreshData(string.Empty,false);
            search.RequestFocus ();
            base.OnRestart ();
        }

    }
}

