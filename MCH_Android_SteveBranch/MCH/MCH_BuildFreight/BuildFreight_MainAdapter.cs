﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class BuildFreight_MainAdapter : BaseAdapter<BuildFreightTaskItem> {

        List<BuildFreightTaskItem> items;
        Activity context;
       


        public BuildFreight_MainAdapter(Activity context, List<BuildFreightTaskItem> items): base()
        {

            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override BuildFreightTaskItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        { 

            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.BuildFreight_MainRow, null);

  
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);             LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);
            TextView txtFlight = view.FindViewById<TextView>(Resource.Id.txtFlight);
            TextView txtETA = view.FindViewById<TextView>(Resource.Id.txtETA);
            TextView txtCounts= view.FindViewById<TextView>(Resource.Id.txtCounts);
            TextView txtUlds= view.FindViewById<TextView>(Resource.Id.txtUlds);

            //TextView txtUlds = view.FindViewById<TextView>(Resource.Id.txtUlds);
            //TextView txtPcs = view.FindViewById<TextView>(Resource.Id.txtPcs);
            ImageView airlineImage= view.FindViewById<ImageView>(Resource.Id.airlineImage);

            TextView txtLocations = view.FindViewById<TextView>(Resource.Id.txtLocations);
            TextView txtProgress = view.FindViewById<TextView>(Resource.Id.txtProgress);
            ProgressBar progressBar = view.FindViewById<ProgressBar>(Resource.Id.progressBar);

            txtFlight.Text =string.Format("{0} {1} {2}",item.Destination,item.CarrierCode, item.FlightNumber);
            txtETA.Text = string.Format("ETD: {0:dd-MMM-yy HH:mm}",item.ETD).ToUpper();

  
            txtCounts.Text = string.Format("AWBS: {0} of {1} - PCS: {2} of {3}",  item.ScannedAwbs, item.TotalAwbs ,item.ScannedPieces, item.TotalPieces);
            txtUlds.Text = string.Format("ULDS: {0} of {1}",  item.ScannedUlds, item.TotalUlds);

 

            progressBar.Progress = (int)item.PercentProgress;
            txtLocations.Text = "LOC: " + item.Locations;
            txtProgress.Text = string.Format("{0:0}%", item.PercentProgress);

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);

            try 
            {
                System.IO.Stream ims = context.Assets.Open(string.Format(@"Airlines/{0}.png",item.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            switch (item.Status)             {                 case BuildFreightStatusTypes.Pending:                     RowIcon.SetImageResource (Resource.Drawable.Pending);                     break;                 case BuildFreightStatusTypes.Completed:                     RowIcon.SetImageResource (Resource.Drawable.Completed);                     break;                 case BuildFreightStatusTypes.In_Progress:                     RowIcon.SetImageResource (Resource.Drawable.InProgress );                     break;                 default:                     RowIcon.SetImageResource (Resource.Drawable.Pending);                     break;             }
 

            return view;
        }

 




    }
}




