﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{


    public class BuildFreight_WeightDialog: DialogFragment
    {

        Button btnCancel;
        Button btnOk;
        Activity context;
        string title;
 
 
        TextView txtGrossWeight;
        TextView txtNetWeight;
        TextView txtTareWeight;
        TextView txtULD;
        Switch chkUOM;
 

        double grossWeight;
        double tareWeight;
        //double netWeight;
        string uld;
        string WeightUOM;

        public delegate void OkClickEventHandler(double grossWeight, string WeightUOM);
        public event OkClickEventHandler OkClicked;

        public BuildFreight_WeightDialog(Activity context, string title,string uld, double grossWeight, double tareWeight, string WeightUOM)
        {
 
            this.title = title;
            this.context = context;
            this.grossWeight = grossWeight;
            this.tareWeight = tareWeight;
            this.uld = uld;
            this.WeightUOM = WeightUOM;

        }
 

        //ADD this for error when rotating
        public BuildFreight_WeightDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.BuildFreight_WeightDialog, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 
            btnOk.Click += OnOk_Click;
 


            btnCancel = DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;


  
            txtGrossWeight = DialogInstance.FindViewById<TextView>(Resource.Id.txtGrossWeight);




            txtNetWeight = DialogInstance.FindViewById<TextView>(Resource.Id.txtNetWeight);
            txtTareWeight = DialogInstance.FindViewById<TextView>(Resource.Id.txtTareWeight);
            txtULD = DialogInstance.FindViewById<TextView>(Resource.Id.txtULD);
   
            txtULD.Text = this.uld;    

            txtTareWeight.Text = string.Format("{0:0.0}", this.tareWeight);
            txtGrossWeight.Text = string.Format("{0:0.0}", this.grossWeight);


            CalculateNetWeight();



            chkUOM = DialogInstance.FindViewById<Switch>(Resource.Id.chkUOM);
            if (WeightUOM.ToLower() == "lb")
            {
                chkUOM.Checked = false;
            }
            else
            {
                chkUOM.Checked = true;
            }
 

            txtGrossWeight.AfterTextChanged  += (object sender, Android.Text.AfterTextChangedEventArgs e) => 
            {
                    CalculateNetWeight();
            };


            chkUOM.CheckedChange += (object sender, CompoundButton.CheckedChangeEventArgs e) => 
            {
                    if(this.WeightUOM.ToLower() =="kg")
                    {
                        this.WeightUOM = "LB";
                        grossWeight = WeightHelper.KgToLb (grossWeight);
                        tareWeight = WeightHelper.KgToLb (tareWeight);
                    }
                    else
                    {
                        this.WeightUOM = "KG";
                        grossWeight = WeightHelper.LbToKg (grossWeight);
                        tareWeight = WeightHelper.LbToKg (tareWeight);
                    }
                    txtTareWeight.Text = string.Format("{0:0.0}", this.tareWeight);
                    txtGrossWeight.Text = string.Format("{0:0.0}", this.grossWeight);
            };


            return DialogInstance;
        }


        private void CalculateNetWeight()
        {


            if (!double.TryParse(txtGrossWeight.Text, out this.grossWeight))
            {
                this.grossWeight = 0;
            }
 
//            if (!double.TryParse(txtTareWeight.Text, out this.tareWeight))
//            {
//                this.tareWeight = 0;
//            }

            double netWeight = this.grossWeight - this.tareWeight;
            if (netWeight < 0)
            {
                netWeight = 0;
            }
            txtNetWeight.Text = string.Format("{0:0.0}",netWeight);
        }
 


        private void OnOk_Click(object sender, EventArgs e)
        {
            if (chkUOM.Checked)
            {
                this.WeightUOM = "KG";
            }
            else
            {
                this.WeightUOM = "LB";
            }


            if (!double.TryParse(txtGrossWeight.Text, out this.grossWeight))
            {
                this.grossWeight = 0;
            }

            if (this.grossWeight > 0)
            {
                OkClicked.Invoke(this.grossWeight,this.WeightUOM);
                Dismiss();
            }
            else
            {
                MessageBox m = new MessageBox(this.context);
                m.ShowAlert("Invalid entry.",MessageBox.AlertType.Alert);
            }


        }

 
 
        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);

            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }
 



    }
}


 