﻿//BuildFreight_UldDialog

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{


    public class BuildFreight_UldDialog: DialogFragment
    {

        Button btnCancel;
        Button btnOk;
        Activity context;
        string title;

 
        string uldPrefix;
        string serialNumber;
        string carrierCode;
        List<IdCodeItem> uldUnitTypes;
        List<CarrierListItem> carrierList;

        LinearLayout pnlSerialNumber;
        TextView txtUldType;
        TextView txtSerialNumber;
        TextView txtCarrierCode;
        Button btnCarriers;
        Button btnOptions;

        public delegate void OkClickEventHandler(long uldUnitTypeId,string uldPrefix, string serialNumber, string carrierCode);
        public event OkClickEventHandler OkClicked;

        public BuildFreight_UldDialog(Activity context, string title, string uldPrefix, string serialNumber, string carrierCode)
        {
 
            this.title = title;
            this.context = context;
            this.uldPrefix = uldPrefix;
            this.serialNumber = serialNumber;
            //this.uldUnitTypes = uldUnitTypes;
            this.carrierCode = carrierCode;

            UldUnitTypes uldunittypes = Miscellaneous.Instance.GetShipmentUnitTypes();
            if (uldunittypes.Transaction.Status && uldunittypes.Data != null)
            {
                uldUnitTypes = uldunittypes.Data;
            }
            else
            {
                uldUnitTypes = new List<IdCodeItem>();
            }

            CarrierList carriers = Miscellaneous.Instance.GetCarrierList();
            if (carriers.Transaction.Status && carriers.Data != null)
            {
                carrierList = carriers.Data;
            }
            else
            {
                carrierList = new List<CarrierListItem>();
            }
 
        }
 

        //ADD this for error when rotating
        public BuildFreight_UldDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.BuildFreight_UldDialog, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 
            btnOk.Click += OnOk_Click;
 


            btnCancel = DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;


            btnOptions = DialogInstance.FindViewById<Button>(Resource.Id.btnOptions);
            btnOptions.Click += OnOptionsClick;
  
            pnlSerialNumber = DialogInstance.FindViewById<LinearLayout>(Resource.Id.pnlSerialNumber);
            txtSerialNumber = DialogInstance.FindViewById<TextView>(Resource.Id.txtSerialNumber);
            txtUldType = DialogInstance.FindViewById<TextView>(Resource.Id.txtUldType);


            txtCarrierCode= DialogInstance.FindViewById<TextView>(Resource.Id.txtCarrierCode);
            btnCarriers= DialogInstance.FindViewById<Button>(Resource.Id.btnCarriers);
            btnCarriers.Click += OnCarriersClick;

            DisplayUld(uldPrefix, serialNumber, carrierCode);
   
            return DialogInstance;
        }


        void OnCarriersClick(object sender, EventArgs e)
        {
            List<SelectionItem> list  = new List<SelectionItem>();
 
            foreach (var n in carrierList)
            {
                list.Add(new SelectionItem(n.CarrierCode,n.Id,Resource.Drawable.Plane));
            }

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Carriers", this.context, list, SelectionListAdapter.SelectionMode.SingleSelection, true);
            dialogFragment.Cancelable = false;
            dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
            {
                    foreach (var r in selection)
                    {
                        DisplayUld(txtUldType.Text,txtSerialNumber.Text,r.Name);
                    }

            };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void DisplayUld(string uldPrefix, string serialNumber,string carrierCode)
        {
            txtUldType.Text = uldPrefix;
            if (uldPrefix.ToLower() == "loose")
            {
                pnlSerialNumber.Visibility = ViewStates.Gone;
 
            }
            else
            {
                pnlSerialNumber.Visibility = ViewStates.Visible;
                txtCarrierCode.Text = carrierCode;
                txtSerialNumber.Text = serialNumber;
            }
        }


        private void OnOk_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(txtUldType.Text))
            {
                List<IdCodeItem> uldqueryresult = LinqHelper.Query<IdCodeItem>(uldUnitTypes, txtUldType.Text);
                if (uldqueryresult.Count > 0)
                {
                    if (uldqueryresult[0].Code.ToLower() == "loose")
                    {
                        OkClicked.Invoke(uldqueryresult[0].Id, uldqueryresult[0].Code, string.Empty, string.Empty);
                        Dismiss();
                        return;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txtSerialNumber.Text) && txtSerialNumber.Text.Length == 5)
                        {

                          

                            if (!string.IsNullOrEmpty(txtCarrierCode.Text) && txtCarrierCode.Text.Length == 2)
                            {
                                var carrierqueryresult = 
                                from l in carrierList
                                                        where l.CarrierCode.ToLower().Contains(txtCarrierCode.Text.Trim().ToLower()) &&
                                                            !string.IsNullOrEmpty(l.CarrierCode)
                                                        select  l.CarrierCode;  
                            
                                foreach (var carrier in carrierqueryresult)
                                {
                                    OkClicked.Invoke(uldqueryresult[0].Id,uldqueryresult[0].Code,txtSerialNumber.Text,carrier);
                                    Dismiss();
                                    return;
                                }
                            }



 

                        }

                    
                    }

                } 
            }

            MessageBox m = new MessageBox(this.context);
            m.ShowAlert("Invalid entry.",MessageBox.AlertType.Alert);

        }

 
        void OnOptionsClick(object sender, EventArgs e)
        {
 
            List<SelectionItem> list = new List<SelectionItem>();
 
            foreach (var n in uldUnitTypes)
            {
                if (n.Code.ToLower() == "loose")
                {
                    list.Add(new SelectionItem(n.Code,n.Id,Resource.Drawable.shipment));
                }
                else
                {
                    list.Add(new SelectionItem(n.Code,n.Id,Resource.Drawable.ULD));
                }
         
            }

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Uld Unit Types", this.context, list, SelectionListAdapter.SelectionMode.SingleSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
            {
                    foreach (var r in selection)
                    {
                        DisplayUld(r.Name,txtSerialNumber.Text,txtCarrierCode.Text);
                    }

            };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }
 
        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);

            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }
 



    }
}


 