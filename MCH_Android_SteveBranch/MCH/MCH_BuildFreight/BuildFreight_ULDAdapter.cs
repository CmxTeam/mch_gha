﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class BuildFreight_ULDAdapter : BaseAdapter<BuildFreightUldListItem> {

        List<BuildFreightUldListItem> items ;
        Activity context;
        Action<int> OnDeleteClickAction;

        public BuildFreight_ULDAdapter(Activity context, List<BuildFreightUldListItem> items, Action<int> onDeleteClickAction)
            : base()
        {
            this.context = context;

            this.OnDeleteClickAction = onDeleteClickAction;

            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override BuildFreightUldListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {


            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new


            if (item.UldPrefix.ToLower() == "all")
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.BuildFreight_UldsLayout, null);
                return view;
            }

            view = context.LayoutInflater.Inflate(Resource.Layout.BuildFreight_UldsRow, null);

     
            TextView txtUld= view.FindViewById<TextView>(Resource.Id.txtUld);
 
            TextView txtLocation= view.FindViewById<TextView>(Resource.Id.txtLocation);
 

            ImageView btnEdit= view.FindViewById<ImageView>(Resource.Id.btnEdit);
            LinearLayout btnBup= view.FindViewById<LinearLayout>(Resource.Id.btnBup);
            ImageView imgBup= view.FindViewById<ImageView>(Resource.Id.imgBup);
            TextView txtBup= view.FindViewById<TextView>(Resource.Id.txtBup);
            TextView txtCounts = view.FindViewById<TextView>(Resource.Id.txtCounts);
            TextView txtWeight = view.FindViewById<TextView>(Resource.Id.txtWeight);
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            ImageView btnDelete = view.FindViewById<ImageView>(Resource.Id.btnDelete);
            LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);


            if (item.IsLoose)
            {
                txtUld.Text =string.Format("{0}",item.UldPrefix);
            }
            else
            {
                txtUld.Text =string.Format("{0}{1}",item.UldPrefix,item.UldSerialNo);
            }

            //txtWeight.Text =  string.Format("Weight: {0:0} {1}",item.Weight, item.WeightUOM);

            txtCounts.Text =string.Format("PCS: {0} AWBS: {1} WGT: {2:0} {3}",item.TotalPieces, item.TotalAwbs,item.Weight, item.WeightUOM);
            txtLocation.Text =string.Format("LOC: {0}",item.Locations);

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);

            if (item.IsBUP)
            {
                txtBup.Text = "BUP";
                imgBup.SetImageResource (Resource.Drawable.ValidateWhite);
            }
            else
            {
                txtBup.Text = "BUP";
                imgBup.SetImageResource (Resource.Drawable.NotValidatedWhite);
            }


            switch (item.Status)
            {
                case BuildFreightStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case BuildFreightStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case BuildFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress);
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            if (item.IsLoose)
            {
                btnBup.Visibility = ViewStates.Gone;  
            }
            else
            {
                btnBup.Visibility = ViewStates.Visible;  
                btnBup.Tag = position.ToString();
                btnBup.Click -= btnBup_Click;
                btnBup.Click += btnBup_Click;  
            }

            btnEdit.Tag = position.ToString();
            btnEdit.Click += btnEdit_Click;


            btnDelete.Tag = position.ToString();
            btnDelete.Click -= btnDelete_Click;
            btnDelete.Click += btnDelete_Click;
 
            return view;
        }

        public void btnDelete_Click (object sender, EventArgs e)
        {
            ImageView btn = (ImageView)sender;
            //BuildFreightUldListItem item = items[int.Parse(btn.Tag.ToString())];

            MessageBox m = new MessageBox(this.context);
                m.OnConfirmationClick+= (bool result) => 
                    {
                        if(result)
                        {
                            OnDeleteClickAction.Invoke(int.Parse(btn.Tag.ToString()));
                        }
                    };
                m.ShowConfirmationMessage("Are you sure you want to delete this ULD?");
        }

        public void btnEdit_Click(object sender, EventArgs e)
        {
            ImageView btn = (ImageView)sender;
            BuildFreightUldListItem item = items[int.Parse(btn.Tag.ToString())];

            var transaction = this.context.FragmentManager.BeginTransaction();
            var dialogFragment = new BuildFreight_UldDialog(this.context,"Edit Uld",item.UldPrefix,item.UldSerialNo, item.CarrierCode);
            dialogFragment.OkClicked += (long uldUnitTypeId, string uldPrefix, string serialNumber, string carrierCode) =>  
            {

                    CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.EditUld(item.UldId,ApplicationSessionState.User.Data.UserId, BuildFreight_SessionState.CurrentBuildTask.FlightManifestId,uldUnitTypeId,serialNumber,carrierCode);
                    if(t.Status)
                    {
                        item.UldPrefix = uldPrefix;
                        item.UldSerialNo = serialNumber;
                        this.NotifyDataSetChanged();
                    }
                    else
                    {
                        MessageBox m = new MessageBox(this.context);
                        m.ShowAlert(t.Error,MessageBox.AlertType.Error);
                    }
            };
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }

        public void btnBup_Click(object sender, EventArgs e)
        {
            LinearLayout btn = (LinearLayout)sender;
 
            BuildFreightUldListItem item = items[int.Parse(btn.Tag.ToString())];
 

            if (item.Status != BuildFreightStatusTypes.Pending)
            {
                MessageBox m = new MessageBox(this.context);
                m.ShowAlert("This ULD is In progress or completed.",MessageBox.AlertType.Alert);
                return;
            }
            else
            {
                string msg;
                if (item.IsBUP)
                {
                    msg = "Are you sure you want to change from BUP to Not BUP?";
                }
                else
                {
                    msg = "Are you sure you want to change from Not BUP to BUP?";
                }
                    MessageBox m = new MessageBox(this.context);
                    m.OnConfirmationClick+= (bool result) => 
                        {
                            if(result)
                            {

                            CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.SwitchBUPMode(item.UldId ,ApplicationSessionState.User.Data.UserId, BuildFreight_SessionState.CurrentBuildTask.FlightManifestId,BuildFreight_SessionState.CurrentBuildTask.TaskId   );
                                if (!t.Status)
                                {
                                    MessageBox m1 = new MessageBox(this.context);
                                    m1.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                }
                                else
                                {
                                    item.IsBUP =  !item.IsBUP;
                                    this.NotifyDataSetChanged();
                               
                                }

                                

                            }
                        };
                    m.ShowConfirmationMessage(msg);
            }


       


        }



    }
}





 

