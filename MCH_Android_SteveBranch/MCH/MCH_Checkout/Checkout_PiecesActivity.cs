
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    //[Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle.Basic", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Checkout_PiecesActivity : BaseActivity
    {


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Inventory_PiecesLayout);
            Initialize();

            LoadDetail();
 

        }

        private void LoadDetail()
        {

            if (Inventory_SessionState.CurrentShipment.Data.AvailablePieces > 0)
            {
                np.MaxValue = Inventory_SessionState.CurrentShipment.Data.AvailablePieces;
                np.MinValue = 1;
                np.Value = Inventory_SessionState.CurrentShipment.Data.AvailablePieces;
            }
            else
            {
                np.MaxValue = 0;
                np.MinValue = 0;
                np.Value = 0;
            }



            if (Inventory_SessionState.CurrentShipment.Data.ScannedPieces > 0 && Inventory_SessionState.CurrentShipment.Data.TotalPieces > Inventory_SessionState.CurrentShipment.Data.ScannedPieces)
            {
                txtPieces.SetTextColor(Color.Red);
            }
            else
            {
                txtPieces.SetTextColor(Color.Black);
            }

            txtPieces.Text = string.Format("{0} of {1}",Inventory_SessionState.CurrentShipment.Data.ScannedPieces, Inventory_SessionState.CurrentShipment.Data.TotalPieces);
            txtLocation.Text = "CURRENT LOCATION: " + Inventory_SessionState.CurrentLocation;


     
                txtAwb.Text = string.Format("{0}-{1}-{2}", Inventory_SessionState.CurrentShipment.Data.Origin , Inventory_SessionState.CurrentShipment.Data.Reference,Inventory_SessionState.CurrentShipment.Data.Destination  );
 
                
  

            txtWeight.Text = string.Format("{0:0.0} {1}", Inventory_SessionState.CurrentShipment.Data.Weight, Inventory_SessionState.CurrentShipment.Data.WeightUOM); 
            txtLocations.Text =  Inventory_SessionState.CurrentShipment.Data.Locations;
            //EditTextListener.Text = Inventory_SessionState.CurrentShipment.Data.AvailablePieces.ToString();
            txtTotalPieces.Text = " OF " + Inventory_SessionState.CurrentShipment.Data.TotalPieces ;


            //ShowSelectedPcs(np.Value);

            //txtPieces.Visibility == ViewStates.Gone;
            np.RequestFocus();
        }
        private void DoBack()
        {
            this.GoToScreen(typeof(Checkout_MainActivity));
        }


        private void ScanShipment(bool gotoSnap,string truckNumber)
        {
            int enteredpieces = 0;

            if (int.TryParse(np.Value.ToString(), out enteredpieces))
            {
                if(enteredpieces==0)
                {
                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            LoadDetail();
                        };
                    m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);
                    return;
                }

                if(enteredpieces > Inventory_SessionState.CurrentShipment.Data.AvailablePieces)
                {
                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            LoadDetail();
                        };
                    m.ShowAlert(Resource.String.More_Pieces , MessageBox.AlertType.Information);
                    return;     
                }



                CommunicationTransaction t =  Inventory.Instance.DischargePieces(Inventory_SessionState.CurrentShipment.Data.AwbId,Inventory_SessionState.CurrentShipment.Data.HwbId,Inventory_SessionState.CurrentLocationId,enteredpieces,Inventory_SessionState.CurrentTaskId,ApplicationSessionState.User.Data.UserId,truckNumber );
                if (t.Status)
                {
                    Inventory_SessionState.LastScanShipment = Inventory_SessionState.CurrentShipment.Data.Origin +"-"+ Inventory_SessionState.CurrentShipment.Data.Reference +"-"+ Inventory_SessionState.CurrentShipment.Data.Destination;
                    Inventory_SessionState.LastScanPieces = enteredpieces;


                    MediaSounds.Instance.Beep(this);
                    if (gotoSnap)
                    {
                        GoToCamera(typeof( Checkout_MainActivity),typeof( Checkout_MainActivity ),Inventory_SessionState.CurrentTaskId,Inventory_SessionState.CurrentShipment.Data.Reference);
                    }
                    else
                    {
                        DoBack();
                    }


                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }


            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {
                        LoadDetail();
                    };
                m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);

            }




        }

 

 

        void btnOkSnap_Click(object sender, EventArgs e)
        {
            txtHidden.RequestFocus();
            GetReference(true);
        }

        void btnOk_Click(object sender, EventArgs e)
        {
            txtHidden.RequestFocus();

            GetReference(false);




          
        }



        void GetReference(bool gotoSnap)
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new ReferenceDialog(this,ApplicationSessionState.SelectedMenuItem.Name.ToUpper(),"Enter truck number.");
            dialogFragment.OkClicked+= (Barcode barcode) =>  
                {
                    ScanShipment(gotoSnap,barcode.BarcodeText);
                };

            //OR
            //dialogFragment.OkClicked+= (Barcode barcode) =>  ScanShipment(gotoSnap,barcode.BarcodeText);
               

            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }
 

        void ReloadData()
        {
            Inventory_SessionState.CurrentShipment = MCH.Communication.Inventory.Instance.ValidateShipment(Inventory_SessionState.CurrentShipment.Data.Reference, ApplicationSessionState.SelectedWarehouseId, Inventory_SessionState.CurrentTaskId, ApplicationSessionState.User.Data.UserId);

            if (Inventory_SessionState.CurrentShipment.Transaction.Status)
            {
                LoadDetail();
            }

        }


  

    }
}


