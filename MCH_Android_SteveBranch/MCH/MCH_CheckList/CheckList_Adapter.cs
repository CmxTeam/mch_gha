﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.Res;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
using Android.Views.InputMethods;
namespace MCH
{

    public class CheckListItem
    {
        public  CheckListItem(long id, string number,string parentNumber,string question, string buttons,string answer,string comments)
        {
            this.Id = id;
            this.Number = number;
            this.Question = question;
            this.Buttons = buttons;
            this.Answer = answer;
            this.IsIncomplete = false;
            this.Summary = null;
            this.ParentNumber = parentNumber;
            this.Comments = comments;
        }

        public  CheckListItem(CheckListSummary summary)
        {
            this.Id = 0;
            this.ParentNumber = string.Empty;
            this.Number = string.Empty;
            this.Question = string.Empty;
            this.Buttons = string.Empty;
            this.Answer = string.Empty;
            this.IsIncomplete = false;
            this.Comments = string.Empty;
            this.Summary = summary;
            this.Title = null;
        }

        public  CheckListItem(CheckListTitle title)
        {
            this.Id = 0;
            this.ParentNumber = string.Empty;
            this.Number = string.Empty;
            this.Question = string.Empty;
            this.Buttons = string.Empty;
            this.Answer = string.Empty;
            this.IsIncomplete = false;
            this.Comments = string.Empty;
            this.Summary = null;
            this.Title = title;
        }

        public string ParentNumber { get; set;}
        public string Number { get; set;}
        public string Comments { get; set;}
        public string Question { get; set;}
        public string Buttons { get; set;}
        public long Id { get; set;}
        public string Answer { get; set;}
        public bool IsIncomplete { get; set;}
        public CheckListSummary Summary { get; set;}
        public CheckListTitle Title  { get; set;}
    }

    public class CheckListTitle
    {
        public string Title { get; set;}
        public string Year { get; set;}
        public string Note { get; set;}
    }

    public class CheckListSummary
    {
        public string UserName { get; set;}
        public string Place { get; set;}
        public string Comments { get; set;}
        public long EntityId { get; set;}
        public long ? TaskId { get; set;}
    }

    public class CheckList_Adapter : BaseAdapter<CheckListItem> {

        List<CheckListItem> items;
        Activity context;

        TextView question;
        Button yes;
        Button no;
        Button na;
        LinearLayout buttons;
        LinearLayout comments;
        Button save;
        Button finalize;
        EditText comment;
        TextView commentline;
        TextView username;
        TextView place;

        Action<int> OnChange;
        Action<bool> OnSave;

        static int currentPosition = 0;

        public CheckList_Adapter(Activity context, List<CheckListItem> items,Action<int> onChange,Action<bool> onSave): base()
        {
            this.OnSave = onSave;
            this.OnChange = onChange;
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CheckListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

           var item = items[position];
           View view = convertView;

            //if (view == null) // no view to re-use, create new
            //{
            if (item.Summary != null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.CheckList_SummaryRow, null);

                save= view.FindViewById<Button>(Resource.Id.btnSave);
                finalize= view.FindViewById<Button>(Resource.Id.btnFinalize);
                comment= view.FindViewById<EditText>(Resource.Id.txtComments);
                username= view.FindViewById<TextView>(Resource.Id.txtUserName);
                place= view.FindViewById<TextView>(Resource.Id.txtPlace);

                comment.Text = item.Summary.Comments;
                place.Text = item.Summary.Place;
                username.Text = item.Summary.UserName;

                comment.Tag = position.ToString();
                place.Tag = position.ToString();
                username.Tag = position.ToString();

                //comment.TextChanged += OnComment_TextChanged;
                place.TextChanged += OnPlace_TextChanged;
                username.TextChanged += OnUserName_TextChanged;



          
                comment.FocusChange += OnCommentEntered_TextChanged;
 

                finalize.Click -= finalize_click;
                save.Click -= save_click;

                finalize.Click += finalize_click;
                save.Click += save_click;




                return view;
            }
            if (item.Title  != null)
            {
 
                view = context.LayoutInflater.Inflate(Resource.Layout.CheckList_TitleRow, null);

                TextView note= view.FindViewById<TextView>(Resource.Id.txtNote);
                TextView year= view.FindViewById<TextView>(Resource.Id.txtYear);
                TextView title= view.FindViewById<TextView>(Resource.Id.txtTitle);

                title.SetTextSize (global::Android.Util.ComplexUnitType.Sp, 25f);
                title.SetTypeface (Typeface.Default, TypefaceStyle.Bold);

                year.SetTextSize (global::Android.Util.ComplexUnitType.Sp, 25f);
                year.SetTypeface (Typeface.Default, TypefaceStyle.Bold);

                note.SetTextSize (global::Android.Util.ComplexUnitType.Sp, 20f);
                note.SetTypeface (Typeface.Default, TypefaceStyle.Italic);
     
                
                year.Text = item.Title.Year;
                title.Text = item.Title.Title;
                note.Text = item.Title.Note;
 
                return view;
            }
            else
            {
                if (item.ParentNumber==string.Empty)
                {
                    view = context.LayoutInflater.Inflate(Resource.Layout.CheckList_Row, null);

                }
                else
                {
                    view = context.LayoutInflater.Inflate(Resource.Layout.CheckList_SubRow, null);
                }    
            }


            //}

            question = view.FindViewById<TextView>(Resource.Id.txtQuestion);
            yes = view.FindViewById<Button>(Resource.Id.btnYes);
            no = view.FindViewById<Button>(Resource.Id.btnNo);
            na = view.FindViewById<Button>(Resource.Id.btnNA);
            buttons = view.FindViewById<LinearLayout>(Resource.Id.panelButtons);
            comments = view.FindViewById<LinearLayout>(Resource.Id.panelComments);
            commentline= view.FindViewById<TextView>(Resource.Id.txtComments);


            yes.Tag = position.ToString();
            no.Tag = position.ToString();
            na.Tag = position.ToString();

            yes.Click -= yes_click;
            no.Click -= no_click;
            na.Click -= na_click;

            yes.Click += yes_click;
            no.Click += no_click;
            na.Click += na_click;


         
 
            if (item.Number != string.Empty)
            {
                question.SetTextSize (global::Android.Util.ComplexUnitType.Sp,20f);
                if (item.ParentNumber==string.Empty)
                {
                    question.SetTypeface (Typeface.Default, TypefaceStyle.Normal);
                    question.Text = string.Format("{0}. {1}", item.Number, item.Question.Trim()); 
                }
                else
                {
                    question.SetTypeface (Typeface.Default, TypefaceStyle.Normal);
                    question.Text = string.Format("{0} {1}",item.Number, item.Question.Trim());

                }

                if (string.IsNullOrEmpty(item.Comments))
                {
                    comments.Visibility = ViewStates.Gone ;
                }
                else
                {
                    commentline.Text =  Application.Context.GetText(Resource.String.Comments) + ": " + item.Comments;
                    commentline.SetTextSize (global::Android.Util.ComplexUnitType.Sp,20f);
                    commentline.SetTypeface (Typeface.Default, TypefaceStyle.BoldItalic);
                    comments.Visibility  = ViewStates.Visible;
                }

            }
            else
            {
                comments.Visibility = ViewStates.Gone ;
                question.SetTextSize (global::Android.Util.ComplexUnitType.Sp, 25f);
                question.SetTypeface (Typeface.Default, TypefaceStyle.Bold);
                question.Text = item.Question;
            }
 
            if (item.IsIncomplete)
            {
                buttons.SetBackgroundColor(Color.Red);
            }
            else
            {
                buttons.SetBackgroundColor(Color.White);
            }

            RefreshButtons(position);
   
          view.Tag = item.Id.ToString();
          return view;
        }

        void  OnCommentEntered_TextChanged(object sender, View.FocusChangeEventArgs e)
        {
            EditText txt  = (EditText)sender;
            txt.FocusChange -= OnCommentEntered_TextChanged;
            int position = int.Parse(txt.Tag.ToString());
            currentPosition = position;
            Action<string> ClickAction = OnCommentsAction;
            var transaction = context.FragmentManager.BeginTransaction();
            var dialogFragment = new CommentsDialogActivity(context, ClickAction, Application.Context.GetText(Resource.String.Comments), Application.Context.GetText(Resource.String.Comments),txt.Text );
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            return;  
        }

        private void OnCommentsAction(string commentsdata)
        {
            int position = currentPosition;
            var item = items[position];
            item.Summary.Comments = commentsdata; 
            OnChange.Invoke (position);
        }

//        void OnComment_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
//        {
//            EditText txt  = (EditText)sender;
//            int position = int.Parse(txt.Tag.ToString());
//            var item = items[position];
//            item.Summary.Comments = txt.Text; 
//            OnChange.Invoke (position);
//        }

        void OnPlace_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            TextView txt  = (TextView)sender;
            int position = int.Parse(txt.Tag.ToString());
            var item = items[position];
            item.Summary.Place = txt.Text; 
            OnChange.Invoke (position);
        }

        void OnUserName_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            TextView txt  = (TextView)sender;
            int position = int.Parse(txt.Tag.ToString());
            var item = items[position];
            item.Summary.UserName = txt.Text; 
            OnChange.Invoke (position);
        }

        void save_click(object sender, EventArgs e)
        {
            OnSave.Invoke(false);
        }

        void finalize_click(object sender, EventArgs e)
        {
            OnSave.Invoke(true);
        }

 

        void yes_click(object sender, EventArgs e)
        {
            //buttons.SetBackgroundColor(Color.White);
            Button btn  = (Button)sender;
            int position = int.Parse(btn.Tag.ToString());

            UpdateAnswer(position,"YES");
            //OnChange.Invoke (position,"YES");
        }

        void no_click(object sender, EventArgs e)
        {
            
            //buttons.SetBackgroundColor(Color.White);
            Button btn  = (Button)sender;
            int position = int.Parse(btn.Tag.ToString());
            UpdateAnswer(position,"NO");
            //OnChange.Invoke (position,"NO");
        }

        void na_click(object sender, EventArgs e)
        {
            
            Button btn  = (Button)sender;
            int position = int.Parse(btn.Tag.ToString());
            UpdateAnswer(position,"NA");
            //OnChange.Invoke (position,"NA");
        }

        void UpdateAnswer(int position, string answer)
        {
            var item = items[position];

           
             

            if (!string.IsNullOrEmpty(answer) && answer.ToUpper() == "NO")
            {
                currentPosition = position;
                Action<string> ClickAction = OnClickAction;
                var transaction = context.FragmentManager.BeginTransaction();

                string label = string.Empty;
                if (items[position].ParentNumber == string.Empty)
                {
                    label = items[position].Number;
                }
                else
                {
                    label = items[position].ParentNumber + "-" +items[position].Number;
                }

                var dialogFragment = new CommentsDialogActivity(context, ClickAction, Application.Context.GetText(Resource.String.Comments),  Application.Context.GetText(Resource.String.Comments_For_Question_Number) + " " + label);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;
            }
            else
            {
                items[position].Comments = string.Empty;
            }
 
            items[position].IsIncomplete = false;
            item.Answer = answer.ToUpper();
            this.NotifyDataSetChanged();
            OnChange.Invoke (position);
        }

        void RefreshButtons(int position)
        {



            var item = items[position];

//            if (item.Buttons == null)
//            {
//                buttons.Visibility = ViewStates.Gone;
//                return;
//            }
//            else
//            {
            if (string.IsNullOrEmpty(item.Buttons))
                {
                    buttons.Visibility = ViewStates.Gone;
                    return;
                }
                else
                {
                    buttons.Visibility = ViewStates.Visible;
                }
//            }


            if (item.Buttons.ToLower().Contains("yes"))
            {
                yes.Visibility = ViewStates.Visible; 


                if (item.Answer.ToLower().Contains("yes") )
                {
                    yes.SetBackgroundResource(Resource.Drawable.SquareButton);
                    yes.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ValidateWhite, 0, 0, 0);
                }
                else
                {
                    yes.SetBackgroundResource(Resource.Drawable.SquareGreyButton);
                    yes.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.NotValidatedWhite, 0, 0, 0);
                }

            }
            else
            {
                yes.Visibility = ViewStates.Gone;
            }

            if (item.Buttons.ToLower().Contains("no"))
            {
                no.Visibility = ViewStates.Visible; 

                if (item.Answer.ToLower().Contains("no") )
                {
                    no.SetBackgroundResource(Resource.Drawable.SquareButton);
                    no.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ValidateWhite, 0, 0, 0);
                }
                else
                {
                    no.SetBackgroundResource(Resource.Drawable.SquareGreyButton);
                    no.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.NotValidatedWhite, 0, 0, 0);
                }

            }
            else
            {
                no.Visibility = ViewStates.Gone;
            }

            if (item.Buttons.ToLower().Contains("na"))
            {
                na.Visibility = ViewStates.Visible; 

                if (item.Answer.ToLower().Contains("na") )
                {
                    na.SetBackgroundResource(Resource.Drawable.SquareButton);
                    na.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ValidateWhite, 0, 0, 0);
                }
                else
                {
                    na.SetBackgroundResource(Resource.Drawable.SquareGreyButton);
                    na.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.NotValidatedWhite, 0, 0, 0);
                }

            }
            else
            {
                na.Visibility = ViewStates.Gone;
            }
        }


        private void OnClickAction(string commentsdata)
        {
            
            items[currentPosition].Comments =  commentsdata;
       

            items[currentPosition].IsIncomplete = false;
            items[currentPosition].Answer = "NO";
            this.NotifyDataSetChanged();
            OnChange.Invoke (currentPosition);
        }



    }
}




