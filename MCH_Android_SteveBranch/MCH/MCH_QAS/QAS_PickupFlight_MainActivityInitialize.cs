﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MCH.Communication;

namespace MCH
{

    public partial class QAS_PickupFlight_MainActivity : BaseActivity
    {

        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;
        EditText search;
        TextView titleLabel; 
        ImageView optionButton;
        ImageView backButton;
        ImageView imageHeader;
 
        ListView listView;
        ImageButton DropDownButton;
        TextView DropDownText;
        LinearLayout DropDownBox;
 
        private void Initialize()
        {

            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            search = FindViewById<EditText>(Resource.Id.SearchText);
            EditTextListener = new EditTextEventListener(search);
            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
                {
                    string data = e.Data;
                    if (e.IsBarcode)
                    {
                        //Parse Barcode Data
                    }
                    RefreshData(data,e.IsBarcode);
                };

            DropDownBox= FindViewById<LinearLayout>(Resource.Id.DropDownBox);
            DropDownText = FindViewById<TextView>(Resource.Id.DropDownText);
            //Initialize status
            DropDownText.Text = FlightStatusTypes.Inbound_Pending.ToString().Replace("_"," ");

            ClearSearch = FindViewById<ImageButton>(Resource.Id.ClearSearchButton);
            SearchButton = FindViewById<ImageButton>(Resource.Id.SearchButton);
            titleLabel = FindViewById<TextView>(Resource.Id.HeaderText);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            imageHeader = FindViewById<ImageView>(Resource.Id.HeaderImage);
            DropDownButton= FindViewById<ImageButton>(Resource.Id.DropDownButton);
            listView = FindViewById<ListView>(Resource.Id.GridControl);

            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name;

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            ClearSearch.Click += OnClearSearch_Click;
            DropDownButton.Click += OnDropDownButton_Click;
            DropDownBox.Click += OnDropDownButton_Click;
            DropDownText.Click += OnDropDownButton_Click;
            SearchButton.Click += OnSearchButton_Click;

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                imageHeader.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }

            //Find here new controls

        }


        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            RefreshData(string.Empty,false);
            EditTextListener.Text = string.Empty;
            search.RequestFocus ();
        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {
            RefreshData(search.Text,false);
        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }


        private void OnDropDownButton_Click(object sender, EventArgs e)
        {

            PopupMenu menu = new PopupMenu(this, DropDownText);

            Dictionary<int, string> result = EnumHelper.GetEnumToList<FlightStatusTypes>();
            foreach (KeyValuePair<int, string> kvp in result)
            {
                if(kvp.Key == 500) // Not all status shows on the dropdown
                {
                    menu.Menu.Add(0,kvp.Key,kvp.Key,kvp.Value);
                }
            }

            menu.MenuInflater.Inflate(Resource.Menu.PopupMenu, menu.Menu);
            menu.MenuItemClick += (s1, arg1) =>
                {
                    DropDownText.Text = arg1.Item.ToString();
                    search.Text = string.Empty;
                    RefreshData(string.Empty,false);            
                };
            menu.Show(); 

        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();

            options.Add(new OptionItem(GetText(Resource.String.Refresh),OptionActions.Refresh, Resource.Drawable.Refresh));
//            options.Add(new OptionItem(GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Refresh:
                    search.Text = string.Empty;
                    RefreshData(string.Empty,false);
                    break;
                case  OptionActions.Camera:
//                    GoToCamera(typeof(QAS_PickupCart_MainActivity),typeof( QAS_PickupCart_MainActivity ),QAS_PickupCart_SessionState.CurrentCartTask.TaskId);
                    break;

            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}

