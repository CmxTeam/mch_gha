﻿using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class QAS_PickupCart_MainActivity : BaseActivity
    {


       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.QAS_PickupCart_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();

        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
//            listView.ItemClick -= OnListItemClick;
            QAS_PickupCart_SessionState.QASPickupCartTask = QAS_PickupCart_SessionState.QASPickupCartTasks.Data[e.Position];
            TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PickupCart_SessionState.QASPickupCartTask.StatusId.ToString()));
        }


        private void TaskSwitcher(enmCartStatus status)
        {

            switch (status)
            {
               
                case enmCartStatus.RETRIEVED_BY_RUNNER:
                    GoToTask_DropCart();
                break;

                case enmCartStatus.DROPPED_AT_INBOUND_STAGE:
                    GoToTask_UnloadCart();
                break;

            }


        } 
        private void GoToTask_CartDetails()
        {

            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new QAS_CartDetailDialog(this,"Cart Details",enmCartDirections.INBOUND);

            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);


        }
        private void GoToTask_DropCart()
        {
            List<OptionItem> taskOptions = new List<OptionItem>();
            taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_Cart_at_Gate),OptionActions.DropAtGate, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_OutboundStage),OptionActions.DropAtOutboundStage, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_InboundStage),OptionActions.DropAtInboundStage, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Cancel),OptionActions.Exit, Resource.Drawable.Menu));

            Action<OptionItem> OptionClickAction = TaskOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PickupCart_SessionState.QASPickupCartTask.Reference.ToString());
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }


        private void GoToTask_UnloadCart() 
        {
            string cartNo = string.Empty;
            if (QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber != null)
            {
                cartNo = "Cart# "+ QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber;
            }
            else if(QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode != null)
            {
                cartNo = "Barcode# "+ QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode;
            }


            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {                       
                        CommunicationTransaction t = MCH.Communication.QAS_PickupCart.Instance.UpdateCartStatus(QAS_PickupCart_SessionState.QASPickupCartTask.CartId,enmCartStatus.EMPTIED, ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {
                                    RefreshData(search.Text, false);
                                };
                            msg.ShowAlert(String.Format("{0} has been emptied.", cartNo),MessageBox.AlertType.Information);
                                
                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };


            m.ShowConfirmationMessage(cartNo+" - "+QAS_PickupCart_SessionState.QASPickupCartTask.CartDestination +  "\n\n"+ GetText(Resource.String.Confirm_Unload_Cart),GetText(Resource.String.Yes), GetText(Resource.String.No));

        }

        private void LoadtitleLabel(enmCartStatus status)
        {
            switch (status)
            {
                case enmCartStatus.RETRIEVED_BY_RUNNER:
                    //titleLabel.Text =GetText(Resource.String.DROP_INBOUND_CART).ToUpper() + string.Format(" - ({0})", QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count);
                    titleLabel.Text =GetText(Resource.String.DROP_INBOUND_CART).ToUpper();
                    break;
                case enmCartStatus.DROPPED_AT_INBOUND_STAGE:
                    //titleLabel.Text = GetText(Resource.String.Unload_Cart_at_Stage).ToUpper() + string.Format(" - ({0})", QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count);
                    titleLabel.Text = GetText(Resource.String.Unload_Cart_at_Stage).ToUpper();
                    break;
                default:
                    //titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper() + string.Format(" - ({0})",QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count);
                    titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper();
                    break;
            }

        }
       
        private void TaskOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.DropAtGate:
                    DropAtGate();                  
                    break;
                case  OptionActions.DropAtOutboundStage:
                    DropAtOutboundStage();
                    break;
                case  OptionActions.DropAtInboundStage:
                    DropAtInboundStage();
                    break;
                case OptionActions.Info:
                    GoToTask_CartDetails();
                    break;
                case  OptionActions.Exit:
                  
                    break;
            }

        }



        private void DropAtOutboundStage()
        {
            string cartNo = string.Empty;
            if (QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber != null)
            {
                cartNo = "Cart# "+ QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber;
            }
            else if(QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode != null)
            {
                cartNo = "Barcode# "+ QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode;
            }
            CommunicationTransaction t = MCH.Communication.QAS_PickupCart.Instance.UpdateCartStatus(QAS_PickupCart_SessionState.QASPickupCartTask.CartId,enmCartStatus.READY_FOR_PICKUP,ApplicationSessionState.User.Data.UserId);
            if (t.Status)
            {
                //                            Toast.MakeText (this, QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber.ToString() + " has been dropped at stage.", ToastLength.Long).Show ();
                //                            RefreshData("", false);

                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(String.Format( "{0} has been dropped at outbound stage.", cartNo),MessageBox.AlertType.Information);
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }

        private void DropAtInboundStage()
        {
            string cartNo = string.Empty;
            if (QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber != null)
            {
                cartNo = "Cart# "+ QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber;
            }
            else if(QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode != null)
            {
                cartNo = "Barcode# "+ QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode;
            }
            CommunicationTransaction t = MCH.Communication.QAS_PickupCart.Instance.UpdateCartStatus(QAS_PickupCart_SessionState.QASPickupCartTask.CartId,enmCartStatus.DROPPED_AT_INBOUND_STAGE,ApplicationSessionState.User.Data.UserId);
            if (t.Status)
            {
                //                            Toast.MakeText (this, QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber.ToString() + " has been dropped at stage.", ToastLength.Long).Show ();
                //                            RefreshData("", false);

                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(String.Format( "{0} has been dropped at inbound stage.", cartNo),MessageBox.AlertType.Information);
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }

        private void DropAtGate()
        {
            string cartNo = string.Empty;
            if (QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber != null)
            {
                cartNo = "Cart# "+ QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber;
            }
            else if(QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode != null)
            {
                cartNo = "Barcode# "+ QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode;
            }
            CommunicationTransaction t = MCH.Communication.QAS_PickupCart.Instance.UpdateCartStatus(QAS_PickupCart_SessionState.QASPickupCartTask.CartId,enmCartStatus.DROPPED_AT_GATE,ApplicationSessionState.User.Data.UserId);
            if (t.Status)
            {
                //                            Toast.MakeText (this, QAS_PickupCart_SessionState.QASPickupCartTask.CartNumber.ToString() + " has been dropped at stage.", ToastLength.Long).Show ();
                //                            RefreshData("", false);

                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(String.Format( "{0} has been dropped at gate.", cartNo),MessageBox.AlertType.Information);
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }



        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
                    long[] s = new long[1];
                    enmCartStatus StatusId =    EnumHelper.GetEnumItem<enmCartStatus>(DropDownText.Text);
                    if(!isBarcode)
                    {
                        s[0] = (long)StatusId;
                    }
                    else
                    {
                        s[0] = (long)enmCartStatus.ALL;
                    }

                    QAS_PickupCart_SessionState.QASPickupCartTasks = MCH.Communication.QAS_PickupCart.Instance.GetActiveCarts(enmCartDirections.INBOUND,s);

                    RunOnUiThread (delegate {

                    if(QAS_PickupCart_SessionState.QASPickupCartTasks.Transaction.Status)
                    {
                            if(QAS_PickupCart_SessionState.QASPickupCartTasks.Data!=null)
                        {
                                QAS_PickupCart_SessionState.QASPickupCartTasks.Data =  LinqHelper.Query<CartListItem>( QAS_PickupCart_SessionState.QASPickupCartTasks.Data,searchData);
//                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",QAS_Pickup_DropCart_SessionState.QASPickupCartTasks.Data.Count);
                                LoadtitleLabel(StatusId);
                                listView.Adapter = new QAS_PickupCart_Adapter(this, QAS_PickupCart_SessionState.QASPickupCartTasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;                          
                        }
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(QAS_PickupCart_SessionState.QASPickupCartTasks.Transaction.Error, MessageBox.AlertType.Error);
                    }
          
                     if(isBarcode)
                        {    

                            QAS_PickupCart_SessionState.QASPickupCartTasks.Data =  LinqHelper.Query<CartListItem>(QAS_PickupCart_SessionState.QASPickupCartTasks.Data,searchData);
                            if(QAS_PickupCart_SessionState.QASPickupCartTasks.Data!=null && QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count==1)
                            {
                                QAS_PickupCart_SessionState.QASPickupCartTask = QAS_PickupCart_SessionState.QASPickupCartTasks.Data[0];
                                TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PickupCart_SessionState.QASPickupCartTask.StatusId.ToString()));   
                            }
                            else
                            {
                                try
                                {
                                   
                                    QASPickupCartTask task=MCH.Communication.QAS_PickupCart.Instance.GetScanCart(searchData);  
                                    if(task.Transaction.Status)
                                    {
                                        if(task.Data!=null)
                                        {
                                            QAS_PickupCart_SessionState.QASPickupCartTask =  task.Data;
                                            EditTextListener.Text = string.Empty;
                                            search.RequestFocus ();

                                            TaskSwitcher(task.Data.StatusId);
                                           
                                            RefreshData(string.Empty,false);
                                        }
                                        else
                                        {
                                            //do nothign
                                        }
                                    }
                                    else
                                    {
                                        RunOnUiThread(() => progressDialog.Hide());
                                        //show msg ...... task.Transaction.Error

                                        MessageBox m = new MessageBox(this);
                                        m.ShowAlert(task.Transaction.Error,MessageBox.AlertType.Error);
                                        return;
                                    }
                                    return;
                                }
                                catch(Exception ex)
                                {
                                    MessageBox m = new MessageBox(this);
                                    m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
                                }
                            }



                        }
                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

       

    }
}

