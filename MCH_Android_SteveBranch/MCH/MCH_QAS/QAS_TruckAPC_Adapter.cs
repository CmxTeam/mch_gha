﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class QAS_TruckAPC_Adapter : BaseAdapter<ApcItem> {

        List<ApcItem> items;
        Activity context;
        


        public QAS_TruckAPC_Adapter(Activity context, List<ApcItem> items  ): base()
        {
 
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override ApcItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.QAS_TruckAPC_MainRow, parent, false);

            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            TextView txtLabel = view.FindViewById<TextView>(Resource.Id.txtLabel);
            TextView txtDate = view.FindViewById<TextView>(Resource.Id.txtDate);

            txtLabel.Text = item.Barcode;
            txtDate.Text = string.Format("{0:MMM-dd-yy}", item.StaleDate) ;
            RowIcon.SetImageResource (Resource.Drawable.Pending);
           
           
            return view;
        }


    }
}


