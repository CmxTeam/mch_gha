﻿using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class QAS_PickupFlight_MainActivity : BaseActivity
    {


       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.QAS_PickupFlight_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();

        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
//            listView.ItemClick -= OnListItemClick;
           
            QAS_PickupFlight_SessionState.QASPickupFlightTask  = QAS_PickupFlight_SessionState.QASPickupFlightTasks.Data[e.Position];
            StartActivity(typeof(QAS_PickupFlightCart_MainActivity));
//            Toast.MakeText (this, QAS_PickupFlight_SessionState.QASPickupFlightTask.FlightNumber.ToString() + " has been picked ", ToastLength.Long).Show ();

        }

    



        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
                    FlightStatusTypes status =    EnumHelper.GetEnumItem<FlightStatusTypes>(DropDownText.Text);
                    QAS_PickupFlight_SessionState.QASPickupFlightTasks = MCH.Communication.QAS_PickupCart.Instance.GetFlights();

                    RunOnUiThread (delegate {
                    
                    if(QAS_PickupFlight_SessionState.QASPickupFlightTasks.Transaction.Status)
                    {
                        if(QAS_PickupFlight_SessionState.QASPickupFlightTasks.Data!=null)
                        {
                           QAS_PickupFlight_SessionState.QASPickupFlightTasks.Data =  LinqHelper.Query<PickupFlightListItem>( QAS_PickupFlight_SessionState.QASPickupFlightTasks.Data,searchData);
                           //titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",QAS_PickupFlight_SessionState.QASPickupFlightTasks.Data.Count);
                           titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name;
//                            LoadtitleLabel(status);
                            listView.Adapter = new QAS_PickupFlight_MainMenuAdapter(this, QAS_PickupFlight_SessionState.QASPickupFlightTasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;                          
                        }
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(QAS_PickupFlight_SessionState.QASPickupFlightTasks.Transaction.Error, MessageBox.AlertType.Error);
                    }
          
                    if(isBarcode)
                    {                  
//                        QASPickupFlightTask task=MCH.Communication.QAS_PickupFlight.Instance.GetQASPickupFlightTask(ApplicationSessionState.SelectedWarehouseId, ApplicationSessionState.User.Data.AppUserId,searchData);  
//                        QAS_PickupFlight_SessionState.QASPickupFlightTask =  task.Data;
                        
                   
                                EditTextListener.Text = string.Empty;
                                search.RequestFocus ();

                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert("Please select a flight", MessageBox.AlertType.Error);
                            RefreshData("",false);
                           //RunOnUiThread(() => progressDialog.Hide());
                        return;

                    }
                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

       

    }
}

