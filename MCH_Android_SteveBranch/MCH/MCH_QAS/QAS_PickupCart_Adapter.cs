﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class QAS_PickupCart_Adapter : BaseAdapter<CartListItem> {

        List<CartListItem> items;
        Activity context;
        


        public QAS_PickupCart_Adapter(Activity context, List<CartListItem> items  ): base()
        {
 
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CartListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.QAS_PickupCart_MainRow, parent, false);

            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            TextView txtCart = view.FindViewById<TextView>(Resource.Id.txtCart);
            TextView txtFlightInfo = view.FindViewById<TextView>(Resource.Id.txtFlightInfo);

            if (item.CartNumber != null)
            {
                txtCart.Text = item.CartNumber;
            }
            else
            {
                txtCart.Text = item.Cartbarcode;
            }

            if (item.OutboundFlight != null)
            {
                string gate = string.Empty;
                if (item.OutboundFlight.ArrivalGate != null && item.OutboundFlight.ArrivalGate != string.Empty)
                {
                    gate = "GATE:" + item.OutboundFlight.ArrivalGate;
                }

                txtFlightInfo.Text = string.Format("{0} {1} {2:MMM-dd-yy HH:mm} {3}", item.RouteDestination, item.OutboundFlight.FlightNumber, item.OutboundFlight.STD,gate);

            }

           
            //todo  need to change to correct status images
            switch (item.StatusId)
            {
                
                case enmCartStatus.RETRIEVED_BY_RUNNER:
                    RowIcon.SetImageResource (Resource.Drawable.Unload);
                    break;
                case enmCartStatus.DROPPED_AT_INBOUND_STAGE:
                    RowIcon.SetImageResource (Resource.Drawable.Remove);
                    break;
               
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            return view;
        }


    }
}


