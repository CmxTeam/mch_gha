﻿using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class QAS_TruckAPC_MainActivity : BaseActivity
    {

        private const int ETA_TIME = 1;
        private const int ETA_DATE = 2;
       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.QAS_TruckAPC_MainLayout);
            Initialize();             
            search.RequestFocus ();
            if (ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_unload" || ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_load")
            {
                QAS_TruckAPC_SessionState.QASTruckTask = null;
                GetReference(false);
            }
          
            RefreshData(string.Empty,false);

        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                QAS_TruckAPC_SessionState.QASTruckAPCTask = QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data[e.Position];
                TaskSwitcher((APCStatusTypes)QAS_TruckAPC_SessionState.QASTruckAPCTask.Status.Id);
            }
            catch
            {
            }
        }



        void GetReference(bool gotoSnap)
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new ReferenceDialog(this,ApplicationSessionState.SelectedMenuItem.Name.ToUpper(),"Enter truck number.");
            dialogFragment.OkClicked+= (Barcode barcode) =>  
                {
                    QAS_TruckAPC_SessionState.trucknumber = barcode.BarcodeText;
                    QAS_TruckAPC_SessionState.QASTruckTask = MCH.Communication.QAS_TruckAPC.Instance.GetTruck(barcode.BarcodeText);
                    if(QAS_TruckAPC_SessionState.QASTruckTask.Transaction.Status)
                    {
                        if(QAS_TruckAPC_SessionState.QASTruckTask.Data!=null)
                        {
                            btnDone.Visibility = ViewStates.Visible;
                            RefreshData(string.Empty,false);
//                            Toast.MakeText (this,QAS_TruckAPC_SessionState.QASTruckLocationTask.Data.TruckNumber, ToastLength.Long).Show ();
                        }
                        else
                        {                           
                            SelectLocation();
//                            Toast.MakeText (this,"No match for truck number. Need to create instance of truck and location of door.", ToastLength.Long).Show ();
                        }
                    }
                    else
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowAlert(QAS_TruckAPC_SessionState.QASTruckAPCTasks.Transaction.Error,MessageBox.AlertType.Error);
                        return;
                    }

                };

            //OR
            //dialogFragment.OkClicked+= (Barcode barcode) =>  ScanShipment(gotoSnap,barcode.BarcodeText);


            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void SelectLocation()
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new QAS_LocationDialogActivity(this, "Location","Select Location",true, LocationTypes.Door);
            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked+= (Barcode barcode) => 
                {
                    CreateTruck(barcode.Id);
                };
            dialogFragment.CancelClicked+= () => 
                {
                    DoBack();
//                    if (QAS_TruckAPC_SessionState.QASTruckLocationTask.Data.LocationId == 0)
//                    {
//                        DoBack();
//                    }
                };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void CreateTruck(long LocationId)
        {
            QAS_TruckAPC_SessionState.QASTruckTask = MCH.Communication.QAS_TruckAPC.Instance.CreateTruck(QAS_TruckAPC_SessionState.trucknumber,LocationId,ApplicationSessionState.User.Data.UserId, DateTime.Now);
  
            if (QAS_TruckAPC_SessionState.QASTruckTask.Transaction.Status)
            {

                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        btnDone.Visibility = ViewStates.Visible;
                        RefreshData(search.Text, false);

                    };
                msg.ShowAlert(GetText(Resource.String.Start_ScanAPC),MessageBox.AlertType.Information);

            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(QAS_TruckAPC_SessionState.QASTruckTask.Transaction.Error, MessageBox.AlertType.Error);
            }       
          
           
        }
        private void TaskSwitcher(APCStatusTypes status)
        {


            switch (status)
            {
//                case APCStatusTypes.APC_PENDING: 
//                    GoToTask_AddAPC();
//                    break;

                case APCStatusTypes.RECEIVED:                   
                    if (ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_unload")
                    {
                        GoToTask_ShowOptions();
                    }
                    else if (ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_process")
                    {
                        GoToTask_MarkProcessed();
                    }
                break;

                case APCStatusTypes.PROCESSED:
                    GoToTask_Emptied();
                    break;

                case APCStatusTypes.READY_TO_RETURN:
                    GoToTask_Loaded();
                    break;
            }
        }

//        private void GoToTask_AddAPC()
//        {
//            QAS_TruckAPC_SessionState.QASTruckAPCTask = null;
//
//            var transaction = this.FragmentManager.BeginTransaction();
//            var dialogFragment = new DateDialog(this,"Date","Enter Stale Date.",DateTime.Now);
//            dialogFragment.OkClicked += (DateTime date) => 
//                {
//                    updateAPCDate(date);
//                };
//            dialogFragment.Cancelable = false;
//            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//
//        }

        private void GoToTask_UpdateAPCDate()
        {

            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new DateDialog(this,"Date","Enter Stale Date.",QAS_TruckAPC_SessionState.QASTruckAPCTask.StaleDate);
            dialogFragment.OkClicked += (DateTime date) => 
                {
                    updateAPCDate(date);
                };
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);


        }

       
        private void updateAPCDate(DateTime eta)
        {
            CommunicationTransaction t = MCH.Communication.QAS_TruckAPC.Instance.UpdateApcStaleDate(QAS_TruckAPC_SessionState.QASTruckAPCTask.Id,eta,ApplicationSessionState.User.Data.UserId);
            if (t.Status)
            {
                
                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {

                        RefreshData(search.Text, false);

                    };
                msg.ShowAlert(string.Format("APC# {0} has been updated.", QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode),MessageBox.AlertType.Information);

            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }       
        }



        private void GoToTask_ShowOptions()
        {
            List<OptionItem> taskOptions = new List<OptionItem>();
            taskOptions.Add(new OptionItem(GetText(Resource.String.Remove),OptionActions.Remove, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Change_Date),OptionActions.Date, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Mark_Processed),OptionActions.Processed, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Go_Back),OptionActions.Exit, Resource.Drawable.Menu));


            Action<OptionItem> OptionClickAction = TaskOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"APC# "+ QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//            Toast.MakeText (this,"RECEIVED, location:" +QAS_TruckAPC_SessionState.QASTruckLocationTask.Data.Location  , ToastLength.Long).Show ();
        }

        private void TaskOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Remove:
                    GoToTask_RemoveAPC();                   
                    break;

                case  OptionActions.Date:
                    GoToTask_UpdateAPCDate();                
                    break;
                case  OptionActions.Processed:
                    GoToTask_MarkProcessed();                
                    break;
                case  OptionActions.Exit:
                                   
                    break;

            }

        }

        private void GoToTask_RemoveAPC()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                        CommunicationTransaction t = MCH.Communication.QAS_TruckAPC.Instance.UpdateAPCStatus(QAS_TruckAPC_SessionState.QASTruckAPCTask.Id,APCStatusTypes.REMOVE,false,ApplicationSessionState.User.Data.UserId);
                    if (t.Status)
                    {

                        MessageBox msg = new MessageBox(this);
                        msg.OnConfirmationClick += (bool r) =>
                        {                                   
                            RefreshData(search.Text, false);                                    
                        };
                        msg.ShowAlert(string.Format("APC# {0} has been removed.", QAS_TruckAPC_SessionState.QASTruckAPCTask.Id), MessageBox.AlertType.Information);


                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }
                    }

                };


            m.ShowConfirmationMessage("APC# "+QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode+ "\n\n"+ GetText(Resource.String.Confirm_RemoveAPC),GetText(Resource.String.Yes), GetText(Resource.String.No));

        }

        private void GoToTask_MarkProcessed()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                        
                        CommunicationTransaction t = MCH.Communication.QAS_TruckAPC.Instance.UpdateAPCStatus(QAS_TruckAPC_SessionState.QASTruckAPCTask.Id,APCStatusTypes.PROCESSED,false,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {                                   
                                    RefreshData(search.Text, false);                                    
                                };
                            msg.ShowAlert(string.Format("APC# {0} has been processed." , QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode)  ,MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };


            m.ShowConfirmationMessage("APC# "+QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode+ "\n\n"+ GetText(Resource.String.Confirm_APC_Process),GetText(Resource.String.Yes), GetText(Resource.String.No));


        }

        private void GoToTask_Emptied()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                        
                        CommunicationTransaction t = MCH.Communication.QAS_TruckAPC.Instance.UpdateAPCStatus(QAS_TruckAPC_SessionState.QASTruckAPCTask.Id,APCStatusTypes.READY_TO_RETURN,false,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {                                   
                                    RefreshData(search.Text, false);                                    
                                };
                            msg.ShowAlert(string.Format("APC# {0} has been built." , QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode)  ,MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };
            m.OnNeutalClick += () =>
                {
                   
                    CommunicationTransaction t = MCH.Communication.QAS_TruckAPC.Instance.UpdateAPCStatus(QAS_TruckAPC_SessionState.QASTruckAPCTask.Id,APCStatusTypes.READY_TO_RETURN,true,ApplicationSessionState.User.Data.UserId);
                    if (t.Status)
                    {

                        MessageBox msg = new MessageBox(this);
                        msg.OnConfirmationClick+= (bool r) => 
                            {                                   
                                RefreshData(search.Text, false);                                    
                            };
                        msg.ShowAlert(string.Format("APC# {0} has been emptied." , QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode)  ,MessageBox.AlertType.Information);
                       
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }

                };

            m.ShowConfirmationMessage("APC# "+QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode+ "\n\n"+ GetText(Resource.String.Confirm_APC_Empty),GetText(Resource.String.Build), GetText(Resource.String.No),GetText(Resource.String.Return_Empty));


        }

        private void GoToTask_Loaded()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                        CommunicationTransaction t = MCH.Communication.QAS_TruckAPC.Instance.LoadApcTotruck(QAS_TruckAPC_SessionState.QASTruckAPCTask.Id,DateTime.Now,ApplicationSessionState.User.Data.UserId,QAS_TruckAPC_SessionState.QASTruckAPCTask.StaleDate,QAS_TruckAPC_SessionState.QASTruckTask.Data.TruckID);
                        if (t.Status)
                        {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {                                   
                                    RefreshData(search.Text, false);                                    
                                };
                            msg.ShowAlert(string.Format("APC# {0} has been loaded." , QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode)  ,MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };
           

            m.ShowConfirmationMessage("APC# "+QAS_TruckAPC_SessionState.QASTruckAPCTask.Barcode+ "\n\n"+ GetText(Resource.String.Confirm_APC_Load),GetText(Resource.String.Yes), GetText(Resource.String.No));


        }

        private void btnDone_Click(object sender, EventArgs e)
        {
//            if (QAS_TruckAPC_SessionState.QASTruckTask.Data.WarehouseLocationId < 1)
//            {
//                MessageBox msg = new MessageBox(this);
//                msg.ShowAlert("This truck did not assign warehouse location.", MessageBox.AlertType.Error);
//                return;
//            }

            TruckStatuses completeStatus = new TruckStatuses();
            completeStatus = TruckStatuses.CLOSED;  
            if (ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_unload")
            {
                
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (!result)
                    {
                                         
                         CommunicationTransaction t = MCH.Communication.QAS_TruckAPC.Instance.UpdateUSPSTruckStatus(completeStatus, QAS_TruckAPC_SessionState.QASTruckTask.Data.TruckID, ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick += (bool r) =>
                            {
                                GotoMainMenu();
                            };
                            msg.ShowAlert("Truck status updated.", MessageBox.AlertType.Information);

                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                            return;
                        }

                    }


                };

                m.ShowConfirmationMessage(GetText(Resource.String.Confirm_TruckStatus), GetText(Resource.String.Yes), GetText(Resource.String.No));

            }
            else
            {                               
                CommunicationTransaction t = MCH.Communication.QAS_TruckAPC.Instance.UpdateUSPSTruckStatus(completeStatus, QAS_TruckAPC_SessionState.QASTruckTask.Data.TruckID, ApplicationSessionState.User.Data.UserId);
                if (t.Status)
                {

                    MessageBox msg = new MessageBox(this);
                    msg.OnConfirmationClick += (bool r) =>
                        {
                            GotoMainMenu();
                        };
                    msg.ShowAlert("Truck status updated.", MessageBox.AlertType.Information);

                }
                else
                {
                    MessageBox msg = new MessageBox(this);
                    msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    return;
                }
            }

        }

        private void findAPC()
        {
               
            try
            {

                QASTruckAPCTask task= MCH.Communication.QAS_TruckAPC.Instance.GetApcByCode(QAS_TruckAPC_SessionState.barcode);  
                if(task.Transaction.Status)
                {
                    if(task.Data!=null)
                    {
                        QAS_TruckAPC_SessionState.QASTruckAPCTask =  task.Data;
                        EditTextListener.Text = string.Empty;
                        search.RequestFocus ();
                        TaskSwitcher((APCStatusTypes)task.Data.Status.Id);

                    }
                    else
                    {
                        GoToTask_CreateAPC();
                    }
                }
                else
                {                   
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(task.Transaction.Error,MessageBox.AlertType.Error);
                    return;
                }
                return;
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
            }

        }

        private void GoToTask_CreateAPC()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        QAS_TruckAPC_SessionState.QASTruckAPCTask = null;

                        var transaction = this.FragmentManager.BeginTransaction();
                        var dialogFragment = new DateDialog(this,"Date","Enter Stale Date.",DateTime.Now);
                        dialogFragment.OkClicked += (DateTime date) => 
                            {
                                QASTruckAPCTask t = MCH.Communication.QAS_TruckAPC.Instance.CreateNewApc(QAS_TruckAPC_SessionState.barcode,date,ApplicationSessionState.User.Data.UserId,QAS_TruckAPC_SessionState.QASTruckTask.Data.TruckID);
                                if (t.Transaction.Status)
                                {

                                    MessageBox msg = new MessageBox(this);
                                    msg.OnConfirmationClick+= (bool r) => 
                                        {                                   
                                            RefreshData(search.Text, false);                                    
                                        };
                                    msg.ShowAlert(string.Format("APC# {0} has been received." , QAS_TruckAPC_SessionState.barcode)  ,MessageBox.AlertType.Information);


                                }
                                else
                                {
                                    MessageBox msg = new MessageBox(this);
                                    msg.ShowAlert(t.Transaction.Error, MessageBox.AlertType.Error);
                                }
                            };
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

                    }

                };


            m.ShowConfirmationMessage("APC# "+QAS_TruckAPC_SessionState.barcode+ "\n\n"+ GetText(Resource.String.Confirm_NewAPC),GetText(Resource.String.Yes), GetText(Resource.String.No));


        }

        private void RefreshData(string searchData,bool isBarcode)
        {
            if (ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_unload" && QAS_TruckAPC_SessionState.QASTruckTask == null)
            {
                return;
            }
            else if (ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_load" && QAS_TruckAPC_SessionState.QASTruckTask == null)
            {
                return;
            }
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
                    APCStatusTypes StatusId =    EnumHelper.GetEnumItem<APCStatusTypes>(DropDownText.Text);

                    if(ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_unload")
                    {
                        //unload truck pass status 300
                        QAS_TruckAPC_SessionState.QASTruckAPCTasks = MCH.Communication.QAS_TruckAPC.Instance.GetApcByTruck(QAS_TruckAPC_SessionState.QASTruckTask.Data.TruckID,APCStatusTypes.RECEIVED);
                    }
                    else if(ApplicationSessionState.SelectedMenuItem.NavigationPath == "qas_apc_load")
                    {
                        //load truck pass status 303
                        QAS_TruckAPC_SessionState.QASTruckAPCTasks = MCH.Communication.QAS_TruckAPC.Instance.GetApcByTruck(QAS_TruckAPC_SessionState.QASTruckTask.Data.TruckID,APCStatusTypes.READY_TO_RETURN);
                    }
                    else
                    {
                        QAS_TruckAPC_SessionState.QASTruckAPCTasks = MCH.Communication.QAS_TruckAPC.Instance.GetApcByStatus(StatusId);
                    }
                   

                    RunOnUiThread (delegate {

                   if(QAS_TruckAPC_SessionState.QASTruckAPCTasks.Transaction.Status)
                    {
                            if(QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data!=null)
                        {
                                QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data =  LinqHelper.Query<ApcItem>( QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data,searchData);
                                //titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data.Count);
                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name;

                            listView.Adapter = new QAS_TruckAPC_Adapter(this, QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;                          
                        }
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(QAS_TruckAPC_SessionState.QASTruckAPCTasks.Transaction.Error, MessageBox.AlertType.Error);
                    }

                        if(isBarcode)
                        {    

                            QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data =  LinqHelper.Query<ApcItem>(QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data,searchData);
                            if(QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data!=null && QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data.Count==1)
                            {
                                QAS_TruckAPC_SessionState.QASTruckAPCTask = QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data[0];
//                                Toast.MakeText (this, QAS_TruckAPC_SessionState.QASTruckAPCTasks.Data[0].Barcode, ToastLength.Long).Show ();
                                TaskSwitcher(EnumHelper.GetEnumItem<APCStatusTypes>(QAS_TruckAPC_SessionState.QASTruckAPCTask.Status.ToString()));   
                            }
                            else
                            {

                                QAS_TruckAPC_SessionState.barcode = searchData;                              
                                if(ApplicationSessionState.SelectedMenuItem.NavigationPath != "qas_apc_load")
                                {
                                  findAPC();
                                }
                                else
                                {

                                    MessageBox msg = new MessageBox(this);
                                    msg.ShowAlert("Invalid barcode", MessageBox.AlertType.Error);
                                    RefreshData("",false);
                                }
//                                try
//                                {
//
//                                    QASTruckAPCTask task= MCH.Communication.QAS_TruckAPC.Instance.GetAPC(searchData);  
//                                    if(task.Transaction.Status)
//                                    {
//                                        if(task.Data!=null)
//                                        {
//                                            QAS_TruckAPC_SessionState.QASTruckAPCTask =  task.Data;
//                                            EditTextListener.Text = string.Empty;
//                                            search.RequestFocus ();
////                                            Toast.MakeText (this, task.Data.Barcode, ToastLength.Long).Show ();
//
//                                            TaskSwitcher(task.Data.StatusId);
//
//                                        }
//                                        else
//                                        {
//                                            GoToTask_AddAPC();
//                                        }
//                                    }
//                                    else
//                                    {
//                                        RunOnUiThread(() => progressDialog.Hide());
//                                        //show msg ...... task.Transaction.Error
//
//                                        MessageBox m = new MessageBox(this);
//                                        m.ShowAlert(task.Transaction.Error,MessageBox.AlertType.Error);
//                                        return;
//                                    }
//                                    return;
//                                }
//                                catch(Exception ex)
//                                {
//                                    MessageBox m = new MessageBox(this);
//                                    m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
//                                }

                                RunOnUiThread(() => progressDialog.Hide());
                            }



                        }
          

                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

       

    }
}

