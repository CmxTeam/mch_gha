﻿using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class QAS_PickupFlightCart_MainActivity : BaseActivity
    {


       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.QAS_PickupFlightCart_MainLayout);
            Initialize();
            LoadDetail();
            RefreshData(string.Empty,false);
            search.RequestFocus ();

        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
//            listView.ItemClick -= OnListItemClick;
            QAS_PickupCart_SessionState.QASPickupCartTask = QAS_PickupCart_SessionState.QASPickupCartTasks.Data[e.Position];
            GoToTask_RetrieveCart(QAS_PickupCart_SessionState.QASPickupCartTask.Cartbarcode);
//            TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PickupCart_SessionState.QASPickupCartTask.StatusId.ToString()));
        }



        private void GoToTask_InboundNewCart(string barcode)
        {
            
            MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) =>
                            {
                                if (result)
                                {                       
                                   GoToTask_RetrieveCart(barcode);
                                }
            
                            };
            
            
            m.ShowConfirmationMessage("Barcode# " + barcode+ "\n\n"+ GetText(Resource.String.Confirm_Pickup_FromFlight),GetText(Resource.String.Yes), GetText(Resource.String.No));
            
        }

        private void GoToTask_RetrieveCart(string barcode)
        {
            
            List<string> list = new List<string>();
            list.Add(barcode);

            CommunicationTransaction t = MCH.Communication.QAS_PickupCart.Instance.RetrieveCartsFromFlight(ApplicationSessionState.User.Data.UserId,list,QAS_PickupFlight_SessionState.QASPickupFlightTask.FlightId,(decimal)ApplicationSessionState.Latitude,(decimal)ApplicationSessionState.Longitude);  
                if (t.Status)
                {

                    MessageBox msg = new MessageBox(this);
                    msg.OnConfirmationClick+= (bool r) => 
                        {
                            RefreshData(search.Text, false);
                        };
                msg.ShowAlert(String.Format("{0} has been retrieved.", barcode),MessageBox.AlertType.Information);
                }
                else
                {
                    MessageBox msg = new MessageBox(this);
                    msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }
        }



        private void btnDone_Click(object sender, EventArgs e)
        {
            
                FlightStatusTypes completeStatus = new FlightStatusTypes();
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                    {
                        if (result)
                        {
                            completeStatus = FlightStatusTypes.Inbound_PickupPartial;                   
                        }
                        else
                        {
                            completeStatus = FlightStatusTypes.Inbound_PickupComplete;   
                        }

                         CommunicationTransaction t = MCH.Communication.QAS_PickupCart.Instance.UpdateFlightCartStatus(completeStatus,QAS_PickupFlight_SessionState.QASPickupFlightTask.FlightId,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {
                                    GotoMainMenu();
                                };
                        msg.ShowAlert(GetText(Resource.String.Confirm_FlightStatus_Updated),MessageBox.AlertType.Information);

                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                            return;
                        }
                    };

                m.ShowConfirmationMessage(GetText(Resource.String.Confirm_MoreCart), GetText(Resource.String.Yes), GetText(Resource.String.No));


        }



        private void btnCancel_Click(object sender, EventArgs e)
        {           

            StartActivity (typeof(QAS_PickupFlight_MainActivity));

        }

       
       

        private void RefreshData(string searchData,bool isBarcode)
        {
            if(searchData.Length >100)
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(GetText(Resource.String.Enter_Exceed_Limit), MessageBox.AlertType.Information);
                return;
            }
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {


                    QAS_PickupCart_SessionState.QASPickupCartTasks = MCH.Communication.QAS_PickupCart.Instance.GetFlightCarts(QAS_PickupFlight_SessionState.QASPickupFlightTask.FlightId);


                    RunOnUiThread (delegate {

                    if(QAS_PickupCart_SessionState.QASPickupCartTasks.Transaction.Status)
                    {
                            if(QAS_PickupCart_SessionState.QASPickupCartTasks.Data!=null)
                        {
                                QAS_PickupCart_SessionState.QASPickupCartTasks.Data =  LinqHelper.Query<CartListItem>( QAS_PickupCart_SessionState.QASPickupCartTasks.Data,searchData);
                                //titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count);
                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name;
//                                LoadtitleLabel(StatusId);
                                listView.Adapter = new QAS_PickupCart_Adapter(this, QAS_PickupCart_SessionState.QASPickupCartTasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;                          
                        }
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(QAS_PickupCart_SessionState.QASPickupCartTasks.Transaction.Error, MessageBox.AlertType.Error);
                    }
          
                     if(isBarcode)
                        {    

                            QAS_PickupCart_SessionState.QASPickupCartTasks.Data =  LinqHelper.Query<CartListItem>(QAS_PickupCart_SessionState.QASPickupCartTasks.Data,searchData );
                            if(QAS_PickupCart_SessionState.QASPickupCartTasks.Data!=null && QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count==1)
                            {
                                QAS_PickupCart_SessionState.QASPickupCartTask = QAS_PickupCart_SessionState.QASPickupCartTasks.Data[0];
                                GoToTask_RetrieveCart(searchData);
//                                TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PickupCart_SessionState.QASPickupCartTask.StatusId.ToString()));   
                            }
                            else
                            {

                                GoToTask_InboundNewCart(searchData);                               

                            }



                        }
                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

       

    }
}

