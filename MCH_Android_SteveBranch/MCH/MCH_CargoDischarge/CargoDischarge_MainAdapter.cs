﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class CargoDischarge_MainAdapter : BaseAdapter<CargoDischargeTaskItem> {

        List<CargoDischargeTaskItem> items;
        Activity context;
        Action<int> OnImageClickAction;
 
        public CargoDischarge_MainAdapter(Activity context,Action<int> onImageClickAction, List<CargoDischargeTaskItem> items): base()
        {
            this.OnImageClickAction = onImageClickAction;
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CargoDischargeTaskItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        { 

            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new
            view = context.LayoutInflater.Inflate(Resource.Layout.CargoDischarge_MainRow, null);

            ImageView personImage= view.FindViewById<ImageView>(Resource.Id.personImage);
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);
            TextView txtCompany = view.FindViewById<TextView>(Resource.Id.txtCompany);
            TextView txtDriver = view.FindViewById<TextView>(Resource.Id.txtDriver);
            TextView txtCounts= view.FindViewById<TextView>(Resource.Id.txtCounts);

 
            TextView txtLocations = view.FindViewById<TextView>(Resource.Id.txtLocations);
            TextView txtProgress = view.FindViewById<TextView>(Resource.Id.txtProgress);
            ProgressBar progressBar = view.FindViewById<ProgressBar>(Resource.Id.progressBar);
 
            txtCounts.Text = string.Format("SHIP: {0} of {1} - PCS: {2} of {3}",  item.ScannedShipments, item.TotalShipments ,item.ScannedPieces, item.TotalPieces);

            progressBar.Progress = (int)item.PercentProgress;
           txtLocations.Text = "LOC: " + item.Locations;
           txtProgress.Text = string.Format("{0:0}%", item.PercentProgress);
            txtDriver.Text = item.Driver.ToUpper();
            txtCompany.Text = item.Company.ToUpper();

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);


            try
            {
                if (!string.IsNullOrEmpty(item.DriverPhoto)) {

                    Byte[] image  = System.Convert.FromBase64String(item.DriverPhoto);
                    personImage.SetImageBitmap (BitmapFactory.DecodeByteArray (image, 0, image.Length));
                }
                else
                {
                    personImage.SetImageResource (Resource.Drawable.person);

                }
            }
            catch
            {
                personImage.SetImageResource (Resource.Drawable.person);
            }

 
            switch (item.Status)
            {
                case CargoDischargeStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case CargoDischargeStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case CargoDischargeStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            personImage.Tag = position.ToString();
            personImage.Click -= OnImageClick;
            personImage.Click += OnImageClick;

            return view;
        }

        public void OnImageClick(object sender, EventArgs e) 
        {
            ImageView img = (ImageView)sender;
            OnImageClickAction.Invoke (int.Parse(img.Tag.ToString()));
        }




    }
}




