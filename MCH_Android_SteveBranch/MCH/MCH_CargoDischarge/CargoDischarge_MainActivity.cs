﻿
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class CargoDischarge_MainActivity : BaseActivity
    {

        CargoDischargeTasks Tasks;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.CargoDischarge_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();
            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;
        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                GoToTask(Tasks.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(CargoDischargeTaskItem task)
        {
            CargoDischarge_SessionState.CurrentDischargeTask = task;
            //Toast.MakeText (this, task.Company, ToastLength.Long).Show ();
            StartActivity(typeof(CargoDischarge_ShipmentActivity));
        }

  



        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {


                    CargoDischargeStatusTypes status =    EnumHelper.GetEnumItem<CargoDischargeStatusTypes>(DropDownText.Text);
 
                    if(isBarcode)
                    {
                        status = CargoDischargeStatusTypes.All;
                    }


                    Tasks = MCH.Communication.CargoDischarge.Instance.GetCargoDischargeTasks(ApplicationSessionState.SelectedWarehouseId,ApplicationSessionState.User.Data.UserId, status);
                    RunOnUiThread (delegate {

                        if(Tasks.Transaction.Status & Tasks.Data!=null)
                        {
                            List<CargoDischargeTaskItem> list =  LinqHelper.Query<CargoDischargeTaskItem>(Tasks.Data,searchData);

                            if(isBarcode)
                            {
                                if(list.Count == 1)
                                {
                                    GoToTask( list[0]);
                                }
                                else if(list.Count == 0)
                                {
                                    LoadGrid(Tasks.Data);
                                }
                                else
                                {
                                    LoadGrid(list);
                                }
                            }
                            else
                            {
                                LoadGrid(list);
                            }

                        }
                        else
                        {
                            LoadGrid(null);
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }




        private void OnImageClickAction(int position)
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new ImageDialog(this,Tasks.Data[position].DriverPhoto );
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            search.RequestFocus ();
        }

        private void LoadGrid(List<CargoDischargeTaskItem> list)
        {
            if (list == null)
            {
                list = new List<CargoDischargeTaskItem>();
            }
            Action<int> ImageClickAction = OnImageClickAction;

            Tasks.Data  = list;
            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper() + string.Format(" - ({0})",list.Count);
            listView.Adapter = new CargoDischarge_MainAdapter(this,ImageClickAction, list);
           
            listView.ItemClick -= OnListItemClick;
            listView.ItemClick += OnListItemClick; 
        }


    }
}

