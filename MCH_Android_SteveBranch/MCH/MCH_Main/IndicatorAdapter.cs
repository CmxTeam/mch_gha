﻿using System;
using System.Collections.Generic;
using Android.Widget;

 
 
using Android.App;
//using Android.Content;
//using Android.OS;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Android.Graphics;
using Android.Graphics.Drawables;
//using Android.Content.Res;
 
 


namespace MCH
{
    public class IndicatorAdapter
    {
        Activity context;
        LinearLayout container;
     
        List<MCH.Communication.SelectionItem> selectedIndicatorList;
        List<MCH.Communication.SelectionItem> indicatorList;
        public IndicatorAdapter(Activity context,LinearLayout container)
        {
            
            this.context = context;
            this.container = container;

            this.indicatorList =  MCH.Communication.Miscellaneous.Instance.GetIndicators();
 
        }


        public void Load(long flags)
        {

            if (flags > 0)
            {
                container.Visibility = Android.Views.ViewStates.Visible; 
                container.Click -= OnClick;    
                container.Click += OnClick;
            }
            else
            {
                container.Visibility = Android.Views.ViewStates.Gone; 
            }

            List<long> indicators = GetIndicators(flags);
 
             
            if (container.ChildCount > 0)
            {
                container.RemoveViews(0, container.ChildCount );
            }

            selectedIndicatorList = new List<MCH.Communication.SelectionItem>();
            foreach (var indicator in indicatorList)
            {

                if (indicators.Contains(indicator.Id))
                {
                    selectedIndicatorList.Add(indicator);
                }
                 
            }

            foreach (var indicator in selectedIndicatorList)
            {
                ImageView img = new ImageView(context);

                try 
                {
                        System.IO.Stream ims = context.Assets.Open(string.Format(@"{0}.png",indicator.IconName));
                        Drawable d = Drawable.CreateFromStream(ims, null);
                        img.SetImageDrawable(d);

                   
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(40,40);
                    container.AddView(img,layoutParams);

                } 
                catch 
                {
                    img.SetImageResource (Resource.Drawable.Icon);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(40,40);
                    container.AddView(img,layoutParams);
                }
            }


        }

        private void OnClick(object sender, EventArgs e)
        {
       
            if (selectedIndicatorList.Count == 0)
            {
                return;
            }

            var transaction = context.FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(Application.Context.GetText(Resource.String.Indicators),context, selectedIndicatorList, null,SelectionListAdapter.SelectionMode.Row,false);
            dialogFragment.Cancelable = false;
            dialogFragment.CancelButtonText = Application.Context.GetText(Resource.String.Close);
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            return;

        }

        private List<long> GetIndicators(long flags)
        {
            List<long> retObj = new List<long>();
          
            int power = 0;
            int testflag = 1;
            while (testflag <= flags)
            {
                if ((flags & testflag) == testflag)
                {
                        retObj.Add(testflag);
                }
                power++;
                testflag = (int)Math.Pow(2, power);
            }
            return retObj;
        }

    }
}


