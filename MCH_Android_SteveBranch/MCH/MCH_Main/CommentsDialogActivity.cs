﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class CommentsDialogActivity: DialogFragment
    {
        TextView textViewMessage;
        Button btnCancel;
        Button btnOk;
        TextView txtComments;
         
        Action<string> okClickAction;
        Activity context;
        string title;
        string message;
        string comments;
        public CommentsDialogActivity (Activity context, Action<string> okClickAction, string title, string message)
        {

            this.title = title;
            this.message = message;
            this.context = context;
            this.okClickAction = okClickAction;
            this.comments = string.Empty;

        }


        public CommentsDialogActivity (Activity context, Action<string> okClickAction, int title, int message)
        {

            this.title =context.GetText(title);
            this.message = context.GetText(message);
            this.context = context;
            this.okClickAction = okClickAction;
            this.comments = string.Empty;
        }


        public CommentsDialogActivity (Activity context, Action<string> okClickAction, string title, string message,string comments)
        {

            this.title = title;
            this.message = message;
            this.context = context;
            this.okClickAction = okClickAction;
            this.comments = comments;

        }


        public CommentsDialogActivity (Activity context, Action<string> okClickAction, int title, int message,string comments)
        {

            this.title =context.GetText(title);
            this.message = context.GetText(message);
            this.context = context;
            this.okClickAction = okClickAction;
            this.comments = comments;
        }


        //ADD this for error when rotating
        public CommentsDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView (inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.CommentsDialog, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 

            txtComments = DialogInstance.FindViewById<TextView>(Resource.Id.txtComments);
            if (comments != string.Empty)
            {
                txtComments.Text = comments;
            }

            textViewMessage = DialogInstance.FindViewById<TextView>(Resource.Id.txtMessage);
            textViewMessage.Text = this.message;

            btnCancel =DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;

 

            btnOk.Click += OnOk_Click;

           

            return DialogInstance;
        }


        private void OnOk_Click(object sender, EventArgs e)
        {

            if (txtComments.Text != string.Empty)
            {
                okClickAction.Invoke (txtComments.Text);
                Dismiss();
            }
            else
            {
                MessageBox m = new MessageBox(context);
                m.ShowMessage(GetText(Resource.String.Invalid) + " " + title + ".");
            }


        }

  
        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title);
 
         
            base.OnActivityCreated (savedInstanceState);


            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);

            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }


        private void ShowAlertMessage(string message)
        {
            context.RunOnUiThread (delegate {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                AlertDialog dialog = builder.Create();
                dialog.SetIcon (Resource.Drawable.Icon);
                dialog.SetTitle(Application.Context.GetText(Resource.String.app_name));
                dialog.SetMessage(message);
                dialog.SetCancelable(false);
                dialog.SetButton (Application.Context.GetText(Resource.String.Ok),delegate{
         
                    //txtComments.RequestFocus ();
                });
                dialog.Show();
            });
        }

        private void ShowAlertMessage(int message)
        {
            ShowAlertMessage(Application.Context.GetText(message));
        }

   

    }
}




