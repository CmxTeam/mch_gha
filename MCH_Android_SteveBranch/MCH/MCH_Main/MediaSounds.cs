﻿using System;

using Android.App;
//using Android.Content;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Android.OS;
using Android.Media;

namespace MCH
{
    public class MediaSounds
    {
 
        MediaPlayer _player;

        private static MediaSounds instance;
        private MediaSounds() {}

        public static MediaSounds Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new MediaSounds();
 
             }
             return instance;
          }
        }

        public void Beep(Activity context)
        {
            //if (_player == null)
           // {
                _player = MediaPlayer.Create(context, Resource.Raw.beep);
           // }
            _player.Start();
        }

        public void Alert(Activity context)
        {
            //if (_player == null)
            // {
            _player = MediaPlayer.Create(context, Resource.Raw.Alert);
            // }
            _player.Start();
        }

        public void Confirm(Activity context)
        {
            //if (_player == null)
            // {
            _player = MediaPlayer.Create(context, Resource.Raw.Confirm);
            // }
            _player.Start();
        }

        public void Error(Activity context)
        {
            //if (_player == null)
            // {
            _player = MediaPlayer.Create(context, Resource.Raw.Error);
            // }
            _player.Start();
        }

    }
}

