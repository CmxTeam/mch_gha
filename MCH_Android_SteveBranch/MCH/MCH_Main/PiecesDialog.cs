﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class PiecesDialog: DialogFragment
    {
        TextView txtHidden;
        Button btnCancel;
        Button btnOk;
        TextView txtPieces;
        TextView txtLabelPcs;
        Action<int> okClickAction;
        Activity context;
        string reference;
        TextView  txtTotalPieces;
        TextView txtOfTotalPieces;
        bool haslimited;
        int remainingPieces;
        int totalPieces;
        EditTextEventListener EditTextListener;
        NumberPicker np;
        LinearLayout NumberPicker;
        LinearLayout NoNumberPicker;
        CheckBox AllowOverage;
        public delegate void OkClickActionEventHandler(int pieces);
        public event OkClickActionEventHandler OkClicked;

        private int minValue=1;
        public void SetMinToZero()
        {
            minValue = 0;
        }


        public PiecesDialog(Activity context,string reference, Action<int> okClickAction, int totalPieces, int remainingPieces)
        {
            this.reference = reference;
            this.context = context;
            this.okClickAction = okClickAction;

            this.totalPieces = totalPieces;


            if (remainingPieces < 0)
            {
                this.remainingPieces = 0;
            }
            else
            {
                this.remainingPieces = remainingPieces;
            }

            this.haslimited = true;

          

        }

        public PiecesDialog (Activity context,string reference, Action<int> okClickAction, int totalPieces, int remainingPieces, bool haslimited)
        {

            
            this.reference = reference;
            this.context = context;
            this.okClickAction = okClickAction;

            this.totalPieces = totalPieces;
            if (remainingPieces < 0)
            {
                this.remainingPieces = 0;
            }
            else
            {
                this.remainingPieces = remainingPieces;
            }
            this.haslimited = haslimited;

        }


        public PiecesDialog (Activity context, string reference,  int totalPieces, int remainingPieces)
        {
            this.reference = reference;
            this.context = context;
            this.okClickAction = null;

            this.totalPieces = totalPieces;
            if (remainingPieces < 0)
            {
                this.remainingPieces = 0;
            }
            else
            {
                this.remainingPieces = remainingPieces;
            }
            this.haslimited = true;

        }

        public PiecesDialog (Activity context,string reference,   int totalPieces, int remainingPieces, bool haslimited)
        {

            this.reference = reference;
            this.context = context;
            this.okClickAction = null;

            this.totalPieces = totalPieces;
            if (remainingPieces < 0)
            {
                this.remainingPieces = 0;
            }
            else
            {
                this.remainingPieces = remainingPieces;
            }
            this.haslimited = haslimited;

        }

        //ADD this for error when rotating
        public PiecesDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.PiecesDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);




            AllowOverage = DialogInstance.FindViewById<CheckBox>(Resource.Id.AllowOverage);
            AllowOverage.CheckedChange += (object sender, CompoundButton.CheckedChangeEventArgs e) =>
            {
                if (NoNumberPicker.Visibility == ViewStates.Visible)
                {
                    NumberPicker.Visibility = ViewStates.Visible;
                    NoNumberPicker.Visibility = ViewStates.Gone;

                }
                else
                {
                    NumberPicker.Visibility = ViewStates.Gone;
                    NoNumberPicker.Visibility = ViewStates.Visible;
                       
                        txtPieces.SetSelectAllOnFocus(true);
                        txtPieces.RequestFocus();
                }
            };

            if (haslimited)
            {
                AllowOverage.Visibility = ViewStates.Gone;
            }
            else
            {
                AllowOverage.Visibility = ViewStates.Visible;
            }


            txtPieces = DialogInstance.FindViewById<TextView>(Resource.Id.txtPieces);
            txtPieces.Text = remainingPieces.ToString();


            NumberPicker = DialogInstance.FindViewById<LinearLayout>(Resource.Id.NumberPicker);
            NoNumberPicker = DialogInstance.FindViewById<LinearLayout>(Resource.Id.NoNumberPicker);

            NumberPicker.Visibility =  ViewStates.Visible;
            NoNumberPicker.Visibility =  ViewStates.Gone;


            NumberPicker.KeyPress += OnKeyPress;

            txtHidden = DialogInstance.FindViewById<TextView>(Resource.Id.txtHidden);
            txtLabelPcs= DialogInstance.FindViewById<TextView>(Resource.Id.txtLabelPcs);
            txtTotalPieces= DialogInstance.FindViewById<TextView>(Resource.Id.txtTotalPieces);
            txtOfTotalPieces= DialogInstance.FindViewById<TextView>(Resource.Id.txtOfTotalPieces);
            if (totalPieces == 0)
            {
       
                txtTotalPieces.Visibility = ViewStates.Gone; 
                txtOfTotalPieces.Visibility = ViewStates.Gone; 
                txtLabelPcs.Visibility = ViewStates.Gone; 

            }
            else
            {   
                txtLabelPcs.Visibility = ViewStates.Visible; 
                txtTotalPieces.Visibility = ViewStates.Visible; 
                txtOfTotalPieces.Visibility = ViewStates.Visible; 
                txtTotalPieces.Text = GetText(Resource.String.Pieces_of).ToUpper() + " " +  totalPieces.ToString();
                txtOfTotalPieces.Text = GetText(Resource.String.Pieces_of).ToUpper() + " " +  totalPieces.ToString();
            }
            
 
            EditTextListener = new EditTextEventListener(txtPieces);
            EditTextListener.OnEnterEvent += OnEnterEvent;

      

            btnCancel =DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 
            btnOk.Click += OnOk_Click;
 




            np = (NumberPicker) DialogInstance.FindViewById(Resource.Id.npId);             np.ValueChanged += (object sender, NumberPicker.ValueChangeEventArgs e) =>              {                     //ShowSelectedPcs(e.NewVal);             };                 if (remainingPieces > 0)             {                 np.MaxValue = remainingPieces;                 np.MinValue = minValue;                 np.Value = remainingPieces;
                //ShowSelectedPcs(remainingPieces);             }             else             {                 np.MaxValue = 0;                 np.MinValue = 0;                 np.Value = 0;
                //ShowSelectedPcs(0);             } 




            return DialogInstance;
        }


//        void ShowSelectedPcs(int pcs)
//        {
//            btnOk.Text = string.Format("APPLY ({0})", pcs);
//        }

        private void OnOk_Click(object sender, EventArgs e)
        {
            txtHidden.RequestFocus();
            Validate();
        }
        private void Validate()
        {

            try
            {

                int enteredpieces = 0;
                if(NoNumberPicker.Visibility ==  ViewStates.Visible)
                {
                    enteredpieces = int.Parse(txtPieces.Text);
                }
                else
                {
                    enteredpieces =  np.Value;
                }

                if (minValue > 0)
                {
                    if(enteredpieces==0)
                    {
                        MessageBox m = new MessageBox(context);
                        m.OnConfirmationClick += (bool result) => 
                            {
                                ResetPieces();
                            };
                        m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);
                        return;
                    }
                }


                if(haslimited && enteredpieces > remainingPieces)
                {
                    MessageBox m = new MessageBox(context);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            ResetPieces();
                        };
                    m.ShowAlert(Resource.String.More_Pieces , MessageBox.AlertType.Information);
                    return;     
                }
                else if(!haslimited && enteredpieces > remainingPieces)
                {
                    MessageBox m = new MessageBox(context);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            if(result)
                            {

                                //new 
                                if (OkClicked != null)
                                    OkClicked(enteredpieces);
                                if (okClickAction != null)
                                okClickAction.Invoke (enteredpieces);
                                
                                Dismiss();   
                            }
                            else
                            {
                                ResetPieces();
                            }
                        };
                    m.ShowConfirmationMessage(Resource.String.More_Pieces_Question,Resource.String.Yes,Resource.String.No);
                    return;
                }

             
                //new 
                if (OkClicked != null)
                    OkClicked(enteredpieces);
                if (okClickAction != null)
                    okClickAction.Invoke (enteredpieces);
                Dismiss();
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(context);
                m.OnConfirmationClick += (bool result) => 
                    {
                        ResetPieces();
                    };
                m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);
            }

  
               
        }
 
        void ResetPieces()
        {
            context.RunOnUiThread(delegate
                {
                    EditTextListener.Text = remainingPieces.ToString();
                    //txtPieces.SetSelectAllOnFocus(true);
                    //txtPieces.SetTextIsSelectable(true);
                    txtPieces.RequestFocus();
                });
        }

        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            Validate();
        }

        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            if (string.IsNullOrEmpty(this.reference))
            {
                Dialog.Window.SetTitle( GetText( Resource.String.Pieces));    
            }
            else
            {
                Dialog.Window.SetTitle(this.reference);
            }

            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }



        private void OnKeyPress(object sender, View.KeyEventArgs e) 
        {
            
            if ( e.KeyCode == Keycode.Enter)
            {
                if (e.Event.Action== KeyEventActions.Down)
                {    
                    Validate();
                }

                e.Handled = true;
                return;
            }
            else
            {
                e.Handled = false;
            }

        }


    }
}



