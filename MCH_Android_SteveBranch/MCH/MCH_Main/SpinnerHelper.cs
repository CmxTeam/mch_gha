﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
namespace MCH
{
    public class SpinnerHelper
    {
        ArrayAdapter adapter;
        Spinner control;
        Activity context;
        public SpinnerHelper(Activity context,Spinner control, List<string> data)
        {
            this.context = context;
            adapter = new ArrayAdapter(context,Android.Resource.Layout.SimpleSpinnerItem);
            foreach (string d in data)
            {
                adapter.Add(d);
            }
            this.control = control;
            this.control.Adapter = adapter;
 
        }

        public SpinnerHelper(Activity context,Spinner control, params string[] data)
        {
            this.context = context;
            adapter = new ArrayAdapter(context,Android.Resource.Layout.SimpleSpinnerItem);

            foreach (string s in data)
            {
                adapter.Add(s);
            }

            this.control = control;
            this.control.Adapter = adapter;
        }
 
        public SpinnerHelper(Activity context,Spinner control, int resourceArray)
        {
            this.context = context;
            adapter = ArrayAdapter.CreateFromResource (context, resourceArray, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
            this.control = control;
            this.control.Adapter = adapter;
        }

  


        public string GetSelectedItem()
        {
            return control.SelectedItem.ToString();  
        }

        public void SelectItem(string value)
        {
            control.SetSelection(GetPosition(value));
        }

        private int GetPosition(string value)
        {
            if (value != null)
            {
                for (int i = 0; i < adapter.Count; i++)
                {
                    var obj = adapter.GetItem(i);
                    if (obj.ToString().ToUpper() == value.ToUpper())
                    {
                        return i;
                    }
                }
            }
            return 0;
        }


    }
}

