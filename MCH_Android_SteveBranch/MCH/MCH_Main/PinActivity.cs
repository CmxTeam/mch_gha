﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

namespace MCH
{
    [Activity (ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]  
    public class PinActivity : Activity
    {
        EditTextEventListener EditTextListener;
        TextView pinNumber;
        TextView version;
		TextView rights;
        Button buttonLogin;
        Button buttonManual;
        TextView title;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
        
            SetContentView(Resource.Layout.Pin);
         

            buttonLogin = FindViewById<Button>(Resource.Id.login);
            buttonManual = FindViewById<Button>(Resource.Id.manual);
            pinNumber = FindViewById<TextView>(Resource.Id.pin);
            version = FindViewById<TextView>(Resource.Id.vesion);
			rights = FindViewById<TextView>(Resource.Id.rights);


            title = FindViewById<TextView> (Resource.Id.title);
            if (ApplicationSessionState.IsTest(this))
            {
                title.Text = GetText(Resource.String.ApplicationName) + " (" + ApplicationSessionState.Mode(this).ToString() + ")";
            }
            else
            {
                title.Text = GetText(Resource.String.ApplicationName);
            }

			//pin.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;

            EditTextListener = new EditTextEventListener(pinNumber);
 
            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
            {
                    DoLogin(e.Data);
            };


            buttonLogin.Click += (object sender, EventArgs e) =>
            {
                    DoLogin(pinNumber.Text);
            };


            if (ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.Pin)
            {
                buttonManual.Visibility = ViewStates.Gone;
            }
            else
            {
                buttonManual.Click+= (object sender, EventArgs e) => 
                {
                    StartActivity(typeof(LoginActivity));
                    this.Finish();
                };

         
            }
           
       
			rights.Text  =string.Format(GetText(Resource.String.All_Rights_Reserved),DateTime.Now.Year);
            version.Text = string.Format(GetText(Resource.String.Version) , ApplicationSessionState.GetVersion(this));



            ImageView  logo = FindViewById<ImageView>(Resource.Id.logo);
            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Companies/{0}.png",ApplicationSessionState.DeviceInfo.Data.CompanyName.ToUpper().Replace(" ","_")));
                Drawable d = Drawable.CreateFromStream(ims, null);
                logo.SetImageDrawable(d);
            } 
            catch 
            {
                //logo.SetImageResource (Resource.Drawable.Cargomatrix);
            }


            if(ApplicationSessionState.IsTest(this))
            {
                EditTextListener.Text =GetText(Resource.String.DefaultPin); 

            }



        }
    

        public void RestartApplication()
        {
            var intent = new Intent (this, typeof (MainActivity));
            intent.SetFlags (ActivityFlags.ClearTop);
            StartActivity(intent);
            this.Finish();
        }

        private void ResetDevice()
        {
            // MCH.Communication.DeviceInfo device 
            ApplicationSessionState.DeviceInfo.Data.MAC = "00:00:00:00:00:00";
            ApplicationSessionState.DeviceInfo = MCH.Communication.Membership.Instance.SaveDevice(ApplicationSessionState.DeviceInfo);
            if (!ApplicationSessionState.DeviceInfo.Transaction.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ApplicationSessionState.DeviceInfo.Transaction.Error);
                return;
            }
            else
            {
                //                        var intent = new Intent (this, typeof (SplashActivity));
                //                        intent.SetFlags (ActivityFlags.ClearTop);
                //                        StartActivity(intent);
                //                        this.Finish();
                RestartApplication();
                return;
            }
        }

        private void DoLogin(string pin)
        {
 
                var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Checking_Account_Info), true);
                new Thread(new ThreadStart(delegate
                    {
 
                    ApplicationSessionState.ApplicationList= MCH.Communication.Membership.Instance.GetUserAppsByPin(pin);
 
                    if(ApplicationSessionState.ApplicationList.Transaction.Status)
                        {

                        if(ApplicationSessionState.ApplicationList.Data == null || ApplicationSessionState.ApplicationList.Data.Count == 0)
                            {
                                MessageBox m = new MessageBox(this);
                                m.OnConfirmationClick += (bool result) => 
                                    {
                                        if(!result)
                                        {
                                            MessageBox m1 = new MessageBox(this);
                                            m1.OnConfirmationClick += (bool r) => 
                                                {
                                                    if(r)
                                                    {
                                                        ResetDevice();
                                                    }
                                                };
                                            m1.ShowConfirmationMessage(Resource.String.Reset_Device_Confirmation);
                                        }
                                        else
                                        {
                                            pinNumber.Text =string.Empty;
                                            pinNumber.RequestFocus (); 
                                        }

                                    };

                                m.ShowConfirmationMessage(Application.Context.GetText( Resource.String.Login_Failed),"CLOSE","RESET DEVICE",MessageBox.AlertType.Error);
                                RunOnUiThread(() => progressDialog.Hide());
                                return;
                            }
                        else if(ApplicationSessionState.ApplicationList.Data.Count == 1)
                            {
                            ApplicationSessionState.SelectedApplication = ApplicationSessionState.ApplicationList.Data[0];
                            RunOnUiThread (delegate 
                                {
                                    DoLoginAuthentication(pin);
                                });
                            }
                            else
                            {
                            ApplicationSessionState.SelectedApplication = ApplicationSessionState.ApplicationList.Data[1];
                              
                                    RunOnUiThread (delegate 
                                        {

                                    SelectApplication( pin);
                    


                                        });

                            RunOnUiThread(() => progressDialog.Hide());
                            return;
                          
                            }


                        }
                        else
                        {
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                                {
                                    pinNumber.Text =string.Empty;
                                    pinNumber.RequestFocus ();
                                };
                            m.ShowAlert(Resource.String.Login_Failed,MessageBox.AlertType.Error);


                            RunOnUiThread(() => progressDialog.Hide());
                            return;

                        }


                         
                     



                        RunOnUiThread(() => progressDialog.Hide());
                    })).Start();

            }


        private void DoLoginAuthentication(string pin)
        {
 

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Checking_Account_Info), true);
            new Thread(new ThreadStart(delegate
            {

                   
                    MCH.Communication.PinParameters parameters = new MCH.Communication.PinParameters();
                    parameters.Location = new MCH.Communication.GeoLocation();
                    parameters.Location.Longitude = ApplicationSessionState.Longitude;
                    parameters.Location.Latitude = ApplicationSessionState.Latitude;
 
                    parameters.Pin  = pin;
                    parameters.DeviceId = ApplicationSessionState.GetMac(this);
                    parameters.AppName = ApplicationSessionState.SelectedApplication.Name; 
 
                    ApplicationSessionState.User =  MCH.Communication.Membership.Instance.AuthenticateUserPin(parameters);
                    if(ApplicationSessionState.User.Transaction.Status)
                {
                        if(ApplicationSessionState.User.Data.IsAthenticated)
                        {

					 
//                            ApplicationSessionState.UserWarehouses =	MCH.Communication.Menu.Instance.GetUserWarehouses(ApplicationSessionState.User.Data.UserId,ApplicationSessionState.User.Data.ShellSetting.WarehouseIds);
//                            if (ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId != null)
//                    {
//                                ApplicationSessionState.SelectedWarehouseId=long.Parse(ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId.ToString());
//                    }
//                    else
//                    {
//                                ApplicationSessionState.SelectedWarehouseId =0;
//                    }


                            if (ApplicationSessionState.DeviceInfo.Data.Id == 0)
                            {
                                ApplicationSessionState.DeviceInfo = MCH.Communication.Membership.Instance.SaveDevice(ApplicationSessionState.DeviceInfo);
                                if (!ApplicationSessionState.DeviceInfo.Transaction.Status)
                                {
                                    MessageBox m = new MessageBox(this);
                                    m.OnConfirmationClick += (bool result) => 
                                        {
                                            this.Finish();
                                        };
                                    m.ShowMessage(ApplicationSessionState.DeviceInfo.Transaction.Error);
                                    
                                    return;
                                }
                            }

                            if(ApplicationSessionState.SelectedApplication.IsSecureApi)
                            {
                                MCH.Communication.TokenInfo tokenInfo = MCH.Communication.Membership.Instance.GetToken(ApplicationSessionState.SelectedApplication.ApiRoot,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.User.Data.SessionId);
                                if(tokenInfo.Transaction.Status && !string.IsNullOrEmpty(tokenInfo.Data))
                                {
                                    string[] arr = tokenInfo.Data.Split('"');
                                    MCH.Communication.WebService.Instance.Token = arr[3];
                                }
                                else
                                {
                                    MessageBox m = new MessageBox(this);
                                    m.OnConfirmationClick += (bool result) => 
                                        {
                                            this.Finish();
                                        };
                                    m.ShowAlert("Missing token.",MessageBox.AlertType.Error);
                                    return;
                                }
                
                                MCH.Communication.WebService.Instance.AppPath = ApplicationSessionState.SelectedApplication.ApiRoot;
                     
                            }
                            else
                            {
                                MCH.Communication.WebService.Instance.AppPath = ApplicationSessionState.SelectedApplication.ApiRoot;
                                MCH.Communication.WebService.Instance.Token = string.Empty;
                            }


                            ApplicationSessionState.UserWarehouses =    MCH.Communication.Menu.Instance.GetUserWarehouses(ApplicationSessionState.User.Data.UserId,ApplicationSessionState.User.Data.ShellSetting.WarehouseIds);
                            if (ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId != null)
                            {
                                ApplicationSessionState.SelectedWarehouseId=long.Parse(ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId.ToString());
                            }
                            else
                            {
                                ApplicationSessionState.SelectedWarehouseId =0;
                            }

                            ApplicationSessionState.LastUseTime = DateTime.UtcNow;
                            ApplicationSessionState.CurrentCategoryId = null;
                            StartActivity(typeof(MainMenuActivity));
                            this.Finish();
                        }
                        else
                        {

                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                                {
                                    if(!result)
                                    {
                                        MessageBox m1 = new MessageBox(this);
                                        m1.OnConfirmationClick += (bool r) => 
                                            {
                                                if(r)
                                                {
                                                    ResetDevice();
                                                }
                                            };
                                        m1.ShowConfirmationMessage(Resource.String.Reset_Device_Confirmation);
                                    }
                                    else
                                    {
                                        pinNumber.Text =string.Empty;
                                        pinNumber.RequestFocus (); 
                                    }

                                };
                           
                            m.ShowConfirmationMessage(Application.Context.GetText( Resource.String.Login_Failed),"CLOSE","RESET DEVICE",MessageBox.AlertType.Error);
                           
                            //here

                        }

                }
                else
                {
 
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) => 
                            {
                                if(!result)
                                {
                                    MessageBox m1 = new MessageBox(this);
                                    m1.OnConfirmationClick += (bool r) => 
                                        {
                                            if(r)
                                            {
                                                ResetDevice();
                                            }
                                        };
                                    m1.ShowConfirmationMessage(Resource.String.Reset_Device_Confirmation);
                                }
                                else
                                {
                                    pinNumber.Text =string.Empty;
                                    pinNumber.RequestFocus (); 
                                }

                            };

                        m.ShowConfirmationMessage(Application.Context.GetText( Resource.String.Login_Failed),"CLOSE","RESET DEVICE",MessageBox.AlertType.Error);


                }



                    RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

        private string tempPin = string.Empty;
         private void SelectApplication( string pin)
        {
            tempPin = pin;
            List<MCH.Communication.SelectionItem> appList = new List<MCH.Communication.SelectionItem>();
     

            foreach (var apps in  ApplicationSessionState.ApplicationList.Data)
                {
                appList.Add(new MCH.Communication.SelectionItem(apps.AppDisplayName, apps.AppId, Resource.Drawable.Device));
                }

            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnApplicationSelectionAction;
                var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Applications", this, appList, SelectionAction, SelectionListAdapter.SelectionMode.SingleSelection, false);
                dialogFragment.CancelButtonText = GetText(Resource.String.Cancel);
                dialogFragment.Cancelable = false;
                dialogFragment.AllowNoSelection = true;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

    

        }

        private void OnApplicationSelectionAction(List<MCH.Communication.SelectionItem>  result, bool isExtra)
        {
          

            if (result == null || result.Count == 0)
            {
                //this.Finish();
                pinNumber.Text =string.Empty;
                pinNumber.RequestFocus (); 
                return;              
            }
            else
            {
                foreach (var app in ApplicationSessionState.ApplicationList.Data)
                {
                    if (app.AppId == result[0].Id)
                    {
                        ApplicationSessionState.SelectedApplication = app;
                        DoLoginAuthentication(tempPin);
                        return;
                    }
                }

            }

            pinNumber.Text =string.Empty;
            pinNumber.RequestFocus (); 
            return;  

        }

        public override void OnBackPressed ()
        {
            System.Environment.Exit(0);

        }

    }
}

