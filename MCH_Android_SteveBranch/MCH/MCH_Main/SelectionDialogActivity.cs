﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using Android.Views.InputMethods;

namespace MCH
{



    public class SelectionDialogActivity: DialogFragment
    {
        ListView listView;
        List<MCH.Communication.SelectionItem> originalSelectionList;
        List<MCH.Communication.SelectionItem> choiseList;
        Action<List<MCH.Communication.SelectionItem>,bool> selectionClickAction;
        Activity context;
        string title;
        //bool allowMultiSelection;
        SelectionListAdapter.SelectionMode selectionMode;
        bool allowSeach;

        EditText SearchText;
        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;


        public string OkButtonText { get; set; }
        public string CancelButtonText { get; set; }
        public string ExtraButtonText { get; set; }

        Button cancel;
        Button ok;
        Button extra;

        public bool AllowNoSelection { get; set;}

        public delegate void SelectionEventHandler(List<MCH.Communication.SelectionItem> selection,bool isExtra);
        public event SelectionEventHandler OnSelectionClick;

 

        public SelectionDialogActivity (string title, Activity context, List<MCH.Communication.SelectionItem> selectionList,  SelectionListAdapter.SelectionMode selectionMode, bool allowSeach)
        {
            this.context = context;
            this.selectionClickAction = null;
            this.originalSelectionList =  selectionList;
 
            this.title = title;
            this.selectionMode = selectionMode;
            this.allowSeach = allowSeach;
        }





        public SelectionDialogActivity (string title, Activity context, List<MCH.Communication.SelectionItem> selectionList, Action<List<MCH.Communication.SelectionItem>,bool> selectionClickAction,   SelectionListAdapter.SelectionMode selectionMode, bool allowSeach)
        {
            this.context = context;
            this.selectionClickAction = selectionClickAction;
            this.originalSelectionList =  selectionList;
 
            this.title = title;
            this.selectionMode = selectionMode;
            this.allowSeach = allowSeach;
        }

        //ADD this for error when rotating
        public SelectionDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.SelectionLayout, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            listView = DialogInstance.FindViewById<ListView>(Resource.Id.SelectionListView); 

            ok = DialogInstance.FindViewById<Button>(Resource.Id.ok);
            if(!string.IsNullOrEmpty(OkButtonText))
            {
                    ok.Text = OkButtonText;
            }

            extra = DialogInstance.FindViewById<Button>(Resource.Id.extra);
            if(!string.IsNullOrEmpty(ExtraButtonText))
            {
                extra.Visibility = ViewStates.Visible;  
                extra.Text = ExtraButtonText;
            }
            else
            {
                extra.Visibility = ViewStates.Gone;  
            }

        
            cancel = DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            if(!string.IsNullOrEmpty(CancelButtonText))
            {
                cancel.Text = CancelButtonText;
            }
            cancel.Click += OnCancelClick;
     

            if (selectionMode == SelectionListAdapter.SelectionMode.CheckBoxSeletion)
            {
                ok.Visibility = ViewStates.Visible;
                ok.Click += OnOkClick;

                if (ExtraButtonText != string.Empty)
                {
                    extra.Click += OnExtraClick;
                }

//                if (!string.IsNullOrEmpty(CancelButtonText))
//                {
//                    cancel.Text = CancelButtonText;
//                }
//                else
//                {
//                    cancel.Text = GetText(Resource.String.Cancel);
//                }

            }
            else if (selectionMode == SelectionListAdapter.SelectionMode.RadioButtonSelection)
            {
                ok.Visibility = ViewStates.Visible;
                ok.Click += OnOkClick;


            }
            else if (selectionMode == SelectionListAdapter.SelectionMode.SingleSelection)
            {

                if (selectionClickAction != null || OnSelectionClick != null)
                {
                    listView.ItemClick += OnListItemClick;
                }
 

                ok.Visibility = ViewStates.Gone;
                extra.Visibility = ViewStates.Gone;  


            }
            else
            {
                ok.Visibility = ViewStates.Gone;
                extra.Visibility = ViewStates.Gone; 
            }


            if (!allowSeach)
            {
                LinearLayout SearchBar = DialogInstance.FindViewById<LinearLayout>(Resource.Id.SearchBar);
                SearchBar.Visibility = ViewStates.Gone;
            }
            else
            {

                SearchText = DialogInstance.FindViewById<EditText>(Resource.Id.SeachText);
                ClearSearch = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearSearch);
                SearchButton = DialogInstance.FindViewById<ImageButton>(Resource.Id.SearchButton);
                EditTextListener = new EditTextEventListener(SearchText);
                EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
                {
                        string data = e.Data;
                        if (!e.IsBarcode)
                        {
                            UpdateOriginalList();
                            RefreshData(data);
                        }

                };
                ClearSearch.Click += OnClearSearch_Click;
                SearchButton.Click += OnSearchButton_Click;
                SearchText.RequestFocus ();
            }
            RefreshData(string.Empty);
            return DialogInstance;
        }


        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            UpdateOriginalList();
            SearchText.Text = string.Empty;
            RefreshData(string.Empty);
            SearchText.RequestFocus ();
        }


        private void UpdateOriginalList()
        {
            if (choiseList != null)
            {

                foreach (var originallist in  this.originalSelectionList)
                {
                    foreach (var seachlist in  this.choiseList)
                    {

                        if (seachlist.Id == originallist.Id)
                        {
                            originallist.Selected = seachlist.Selected;
                        }

                    }
                }
            }

        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {

            UpdateOriginalList();


            RefreshData(SearchText.Text);
        }

        void RefreshData(string search)
        {
            this.choiseList = new List<MCH.Communication.SelectionItem>();
            foreach (var choise in originalSelectionList)
            {   



                if (search == string.Empty)
                {
                    if (choise.IconId > 0)
                    {
                        this.choiseList.Add(new MCH.Communication.SelectionItem(choise.Name, choise.Id,choise.IconId,choise.Selected));
                    }
                    else
                    {
                        this.choiseList.Add(new MCH.Communication.SelectionItem(choise.Name, choise.Id,choise.IconName,choise.Selected));
                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(choise.Name) && choise.Name.ToLower().Contains(search.ToLower()))
                    {
                        if (choise.IconId > 0)
                        {
                            this.choiseList.Add(new MCH.Communication.SelectionItem(choise.Name, choise.Id,choise.IconId,choise.Selected));
                        }
                        else
                        {
                            this.choiseList.Add(new MCH.Communication.SelectionItem(choise.Name, choise.Id,choise.IconName,choise.Selected));
                        }

                    }
                }

            }
            listView.Adapter = new SelectionListAdapter(this.context, choiseList, originalSelectionList, selectionMode);
            //SearchText.RequestFocus ();


        
            listView.RequestFocus();
            InputMethodManager manager = (InputMethodManager)context.GetSystemService(Context.InputMethodService);

            if (allowSeach)
            {
                manager.HideSoftInputFromWindow(SearchText.WindowToken, 0);
                SearchText.RequestFocus ();
            }
         

 


        }

        void OnCancelClick(object sender, EventArgs e)
        {
            if (AllowNoSelection)
            {
                if (selectionClickAction != null)
                {
                    selectionClickAction.Invoke(null, false);
                }

                if (OnSelectionClick != null)
                {
                    OnSelectionClick.Invoke(null,false);
                }

            }

 

            Dismiss();
        }


        void OnOkClick(object sender, EventArgs e)
        {
            OnOk(false);
        }

        void OnExtraClick(object sender, EventArgs e)
        {
            OnOk(true);
        }

        void OnOk(bool isExtra)
        {
            UpdateOriginalList();

            List<MCH.Communication.SelectionItem> result = new List<MCH.Communication.SelectionItem>();
            foreach (var selection in originalSelectionList)
            {
                if (selection.Selected)
                {
                    result.Add(selection);
                    selection.Selected = false;
                }
            }

            if (result.Count == 0)
            {
                MessageBox m = new MessageBox(context);
                m.ShowMessage(GetText(Resource.String.Selection_Dialog_No_Items_Selected_Error), this.title);
            }
            else
            {
                if (selectionClickAction != null)
                {
                    selectionClickAction.Invoke (result,isExtra);
                }

                if (OnSelectionClick != null)
                {
                    OnSelectionClick.Invoke(result,isExtra);
                }

                Dismiss();
            }

        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
       
                List<MCH.Communication.SelectionItem> result = new List<MCH.Communication.SelectionItem>();
                result.Add(choiseList[e.Position]);
                if (selectionClickAction != null)
                {
                selectionClickAction.Invoke (result,false);
                }

                if (OnSelectionClick != null)
                {
                    OnSelectionClick.Invoke(result,false);
                }

                Dismiss();

        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
    

//            if (ApplicationSessionState.IsTest(this.context))
//            {
//                if(string.IsNullOrEmpty(title))
//                {
//                    Dialog.Window.SetTitle (GetString(Resource.String.Options) + " (" + GetString(Resource.String.Connection_Mode_Test) + ")");
//                }
//                else
//                {
//                    Dialog.Window.SetTitle (title + " (" + GetString(Resource.String.Connection_Mode_Test) + ")");
//                }
//            }
//            else
//            {
                if(string.IsNullOrEmpty(title))
                {
                Dialog.Window.SetTitle (GetString(Resource.String.Options).ToUpper());
                }
                else
                {
                Dialog.Window.SetTitle (title.ToUpper());
                }
            //}  


            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }
     
 
     


    }
}


