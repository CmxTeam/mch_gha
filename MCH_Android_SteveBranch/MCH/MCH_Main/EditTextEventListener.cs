﻿ 
using System;
 
using Android.Widget;
using Android.Views;
using Android.App;
namespace MCH
{

    public class EditTextEventArgs : EventArgs
    {


        private string data;

        public string Data
        {
            get { return data; }
            set { data = value; }
        }

        private bool isBarcode = false;
        public bool IsBarcode
        {
            get { return isBarcode; }
            set { isBarcode = value; }
        }

        private Barcode barcodeData;
        public Barcode BarcodeData
        {
            get { return barcodeData; }
            set { barcodeData = value; }
        }
 

        public EditTextEventArgs (string data,bool isBarcode, Barcode barcodeData) : base ()
        {
            this.isBarcode = isBarcode;
            this.data = data;
            this.barcodeData = barcodeData;
        }

    }



    public class EditTextEventListener
    {
        
        private bool WasKeyedIn = false;
        string lasttext = "";
        TextView editText;
        int lastposition;    
        bool keepScannedDataInTextVew = false;
        string ScannerPrefix = string.Empty;

        public event EventHandler<EditTextEventArgs> OnEnterEvent;

        public EditTextEventListener(TextView editText,bool keepScannedDataInTextVew) : this(editText)
        {
            this.keepScannedDataInTextVew = keepScannedDataInTextVew;
            try
            {
                this.ScannerPrefix = Application.Context.GetText(Resource.String.ScannerPrefix);
            }
            catch
            {
                ScannerPrefix = string.Empty;
            }

        }

        public EditTextEventListener(TextView editText)
        {
            this.editText = editText;

            this.editText.KeyPress+= OnKeyPress;
            this.editText.TextChanged += OnTextChanged;
            this.editText.BeforeTextChanged +=OnBeforeTextChanged;
            this.editText.FocusChange += OnFocusChange;
            this.editText.Click += (object sender, EventArgs e) => 
                {
                    WasKeyedIn = true;
                };
            try
            {
                this.ScannerPrefix = Application.Context.GetText(Resource.String.ScannerPrefix);
            }
            catch
            {
                ScannerPrefix = string.Empty;
            }

        }

        public void ClearText()
        {
            this.editText.TextChanged -= OnTextChanged;
            editText.Text = string.Empty;
            this.editText.TextChanged += OnTextChanged;
        }

        public bool EnableEnter
        {
            set
            {
                if (value)
                {
                    this.editText.TextChanged -= OnTextChanged;
                    this.editText.TextChanged += OnTextChanged;
                }
                else
                {
                    this.editText.TextChanged -= OnTextChanged;
                }
            }
        }

        private bool enableBarcode=true;
        public bool EnableBarcode
        {
            set
            {
                enableBarcode = value;

                if (value)
                {
                    ClearText();
                }


            
            }
            get
            {
                return enableBarcode;
            }
        }

        public string Text
        {
            set
            {
                this.editText.TextChanged -= OnTextChanged;
                this.editText.Text = value;
                this.editText.TextChanged += OnTextChanged;
                this.editText.RequestFocus();
            }
            get
            { 
                return this.editText.Text;
            }
        }


        private void OnFocusChange(object sender, View.FocusChangeEventArgs e)
        {
            WasKeyedIn = false;
        }

        private void OnBeforeTextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            lastposition = editText.SelectionStart;
            lasttext = editText.Text;
        }

        private void OnTextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {

            string newtext = editText.Text;

            int diff = newtext.Length - lasttext.Length;


            if (lasttext.Length == 0 && newtext.Length == 1)
            {
                WasKeyedIn = true;
            }
            else if(diff > 2)
            {
                int end =  diff;
                string data = editText.Text.Substring(lastposition, end);

                WasKeyedIn = false;
                OnEnter(data,WasKeyedIn);
                return;
            }

            if (newtext == string.Empty) {
                WasKeyedIn = false;
                return;
            }

            if (WasKeyedIn == false) {
                OnEnter(editText.Text,WasKeyedIn);
            }

        }

        private void OnEnter(string data, bool wasKeyedIn)
        {
            bool isBarcode = false;
            if (!wasKeyedIn)
            {
                if (!enableBarcode)
                {
                    return;
                }
                Barcode barcode = BarcodeParser.Parse(data);
                isBarcode = true;
                OnEnterEvent.Invoke(this, new EditTextEventArgs(data.Trim(), isBarcode, barcode));
            }
            else if (data.Trim()!=string.Empty && this.ScannerPrefix !=string.Empty && data.Substring(0, 1).ToUpper().Trim() ==  this.ScannerPrefix.ToUpper())
            {
                if (!enableBarcode)
                {
                    return;
                }
                wasKeyedIn = false;
                data = data.Substring(1, data.Length - 1);
                Barcode barcode = BarcodeParser.Parse(data);
                isBarcode = true;
                OnEnterEvent.Invoke(this, new EditTextEventArgs(data.Trim(), isBarcode, barcode));
            }
            else
            {
                OnEnterEvent.Invoke(this, new EditTextEventArgs(data.Trim(), isBarcode,null));
            }



            if (!wasKeyedIn)
            {
                if (this.keepScannedDataInTextVew)
                {
                    //this.editText.TextChanged -= OnTextChanged;
                    this.EnableEnter = false;
                    editText.Text = data;
                    this.EnableEnter = true;
                    //this.editText.TextChanged += OnTextChanged;
                }
                else
                {
                    editText.Text ="";
                }
                  
               
            }
            editText.RequestFocus();
        }



        private void OnKeyPress(object sender, View.KeyEventArgs e) 
        {
            WasKeyedIn = true;
            if ( e.KeyCode == Keycode.Enter)
            {
                if (e.Event.Action== KeyEventActions.Down)
                {    
                    OnEnter(editText.Text, WasKeyedIn);
                }

                e.Handled = true;
                return;
            }
            else
            {
                e.Handled = false;
            }

        }

    }




 



}














 