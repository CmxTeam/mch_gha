﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;

 

namespace MCH
{
 
    public class PinDialogActivity: DialogFragment
    {
        TextView textViewMessage;
        Button cancel;
        Button login;
        TextView pinNumber;
        EditTextEventListener EditTextListener;
        Action<MCH.Communication.User> loginClickAction;
        Activity context;
        string title;
        string message;
        string negativeButtonText;
        string positiveButtonText;
        bool authenticateAdminOnly;

        public PinDialogActivity (Activity context,bool authenticateAdminOnly, Action<MCH.Communication.User> loginClickAction, string title, string message,string positiveButtonText,string negativeButtonText)
        {

            this.authenticateAdminOnly = authenticateAdminOnly;

            this.negativeButtonText = negativeButtonText; 
            this.positiveButtonText = positiveButtonText;

            this.title = title;
            this.message = message;
            this.context = context;
            this.loginClickAction = loginClickAction;
        }

        public PinDialogActivity (Activity context,bool authenticateAdminOnly, Action<MCH.Communication.User> loginClickAction, string title, string message)
        {
  
            this.authenticateAdminOnly = authenticateAdminOnly;

            this.negativeButtonText = context.GetText(Resource.String.Cancel); 
            this.positiveButtonText = context.GetText(Resource.String.Login);

            this.title = title;
            this.message = message;
            this.context = context;
            this.loginClickAction = loginClickAction;

        }
 
        public PinDialogActivity (Activity context,bool authenticateAdminOnly, Action<MCH.Communication.User> loginClickAction, int title, int message,int positiveButtonText,int negativeButtonText)
        {
            this.authenticateAdminOnly = authenticateAdminOnly;

            this.negativeButtonText = context.GetText(negativeButtonText); 
            this.positiveButtonText =context.GetText(positiveButtonText);
            this.title =context.GetText(title);
            this.message = context.GetText(message);
            this.context = context;
            this.loginClickAction = loginClickAction;
        }

        public PinDialogActivity (Activity context,bool authenticateAdminOnly, Action<MCH.Communication.User> loginClickAction, int title, int message)
        {
            this.authenticateAdminOnly = authenticateAdminOnly;

            this.negativeButtonText = context.GetText(Resource.String.Cancel); 
            this.positiveButtonText =context.GetText(Resource.String.Login);
            this.title =context.GetText(title);
            this.message = context.GetText(message);
            this.context = context;
            this.loginClickAction = loginClickAction;
        }

        //ADD this for error when rotating
        public PinDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView (inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.PinDialog, container, false);
 

            login = DialogInstance.FindViewById<Button>(Resource.Id.login); 
            login.Text = this.positiveButtonText;

            pinNumber = DialogInstance.FindViewById<TextView>(Resource.Id.pin);

            textViewMessage = DialogInstance.FindViewById<TextView>(Resource.Id.message);
            textViewMessage.Text = this.message;

            cancel =DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            cancel.Text = negativeButtonText;

            cancel.Click += OnCancelClick;

            TextView userLabel = DialogInstance.FindViewById<TextView>(Resource.Id.CurrentUser); 
            userLabel.Text = string.Format("{0}: {1} {2}", GetText(Resource.String.Current_User), ApplicationSessionState.User.Data.Firstname, ApplicationSessionState.User.Data.Lastname);


            EditTextListener = new EditTextEventListener(pinNumber);

            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
                {
                    DoPinLogin(e.Data);
                };


            login.Click += (object sender, EventArgs e) =>
                {
                    DoPinLogin(pinNumber.Text);
                };


            return DialogInstance;
        }

 
        void OnCancelClick(object sender, EventArgs e)
        {
            loginClickAction.Invoke (null);
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }


        private void ShowAlertMessage(string text)
        {
            MessageBox m = new MessageBox(context);
            m.ShowAlert(text,MessageBox.AlertType.Error);

        }
                                        
        private void ShowAlertMessage(int text)
        {
            ShowAlertMessage(Application.Context.GetText(text));
        }

        private void DoPinLogin(string pin)
        {


            var progressDialog = ProgressDialog.Show(context, GetText(Resource.String.Please_Wait), GetText(Resource.String.Checking_Account_Info), true);
            new Thread(new ThreadStart(delegate
                {



                    MCH.Communication.PinParameters parameters = new MCH.Communication.PinParameters();
                    //param.Location = new MCH.Communication.GeoLocation();
                    //param.Location.Longitude = ApplicationSessionState.Longitude;
                    //param.Location.Latitude = ApplicationSessionState.Latitude;
                    parameters.Pin  = pin;
                    parameters.DeviceId = ApplicationSessionState.GetMac(context);
                    parameters.AppName = ApplicationSessionState.SelectedApplication.Name; 

                    MCH.Communication.User user =  MCH.Communication.Membership.Instance.GetUserAuthDataByPin(parameters);


                   

                    if(user.Transaction.Status)
                    {
                        if(user.Data.IsAthenticated)
                        {


                            if(authenticateAdminOnly)
                            {
                                MCH.Communication.MenuList  menulist = MCH.Communication.Menu.Instance.GetAppRoleMenus(ApplicationSessionState.SelectedApplication.Name,user.Data.RoleId,user.Data.UserId,ApplicationSessionState.SelectedWarehouseId);
                                if(menulist.Data.Count > 0)
                                {
                                    bool isAdmin = false;
 

                                    //Administrator
                                    if(user.Data.IsAdmin)
                                    {
                                        isAdmin = true;
                                    }
                                    else
                                    {
                                        //check here if user is admin for this menu
                                        foreach(MCH.Communication.MenuItem m in menulist.Data)
                                        {
                                            if(m.Id == ApplicationSessionState.SelectedMenuItem.Id)
                                            {
                                                if(m.Restrictions.Count == 0)
                                                {
                                                    isAdmin = true;
                                                    break;
                                                }
                                            }
                                        }

                                    }


                                    if(isAdmin)
                                    {
                                        loginClickAction.Invoke (user);
                                        Dismiss();
                                    }
                                    else
                                    {
                                        ShowAlertMessage(Resource.String.Invalid_Pin);

                                    }


                                }
                                else
                                {
                                    ShowAlertMessage(Resource.String.Invalid_Pin);

                                }
                            }
                            else
                            {
                                loginClickAction.Invoke (user);
                                Dismiss();
                            }



                        }
                        else
                        {
                            if(authenticateAdminOnly)
                            {

                                ShowAlertMessage(Resource.String.Invalid_Pin);

                            }
                            else
                            {
                                ShowAlertMessage(Resource.String.Login_Failed);

                            }

                        }

                    }
                    else
                    {
                        ShowAlertMessage(Resource.String.Login_Failed);
                        //ShowAlertMessage(user.Transaction.Error);

                    }



                    context.RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }

    }
}

