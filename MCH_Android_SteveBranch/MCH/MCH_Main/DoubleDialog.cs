﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class DoubleDialog: DialogFragment
    {
        TextView textViewMessage;
        Button cancel;
        Button ok;
        TextView txtWeight;
        EditTextEventListener EditTextListener;
        //        //Action<SnapShotTaskItem> okClickAction;
        Activity context;
        string title;
        string message;
        ImageButton ClearSearch;


        public delegate void OkClickActionEventHandler(Double  value);
        public event OkClickActionEventHandler OkClicked;

        public DoubleDialog (Activity context,string title ,string message )
        {

            this.title = title.ToUpper();
            this.message = message.ToUpper();
            this.context = context;

        }

 

        //ADD this for error when rotating
        public DoubleDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.DoubleDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            ok = DialogInstance.FindViewById<Button>(Resource.Id.ok); 

            txtWeight = DialogInstance.FindViewById<TextView>(Resource.Id.txtWeight);

            textViewMessage = DialogInstance.FindViewById<TextView>(Resource.Id.message);
            textViewMessage.Text = this.message;

            cancel =DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            cancel.Click += OnCancelClick;

            EditTextListener = new EditTextEventListener(txtWeight);
            EditTextListener.OnEnterEvent += OnEnterEvent;


            ok.Click += OnOk_Click;


            ClearSearch = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearSearchButton);
            ClearSearch.Click += OnClearSearch_Click;
            //
            //            //txtBarcode.Text = "bp008708702il";

            return DialogInstance;
        }


        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            EditTextListener.Text = string.Empty;
            txtWeight.RequestFocus ();
        }

        private void OnOk_Click(object sender, EventArgs e)
        {
            
            if (txtWeight.Text != string.Empty)
            {
                Double result = Double.Parse(txtWeight.Text);
                if(OkClicked!=null)
                {
                    OkClicked.Invoke(result);
                }

                Dismiss();
            }



        }

        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
           
                Barcode b = new Barcode();
                b.BarcodeType = BarcodeTypes.NA;
            b.BarcodeText = txtWeight.Text;
               
        }


        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {

            Dialog.Window.SetTitle( this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;

        }

 



    }
}



