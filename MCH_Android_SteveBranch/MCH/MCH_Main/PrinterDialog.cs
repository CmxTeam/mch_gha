﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;


namespace MCH
{
 
    public class PrinterDialog: DialogFragment
    {
        TextView lblReference;
        TextView lblPrinter;
        TextView txtPieces;

        Button cancel;
        Button options;
        Button ok;
      
       
        Action<long,long,int> okClickAction;
        Activity context;
        string title;
        string reference;
        long referenceId;
        int pieces;
        long printerId;
        string printerName;

        MCH.Communication.Printers printers;
        List< MCH.Communication.SelectionItem> printersList = new List< MCH.Communication.SelectionItem>();

        public PrinterDialog (Activity context, Action<long,long,int> okClickAction, string title, string reference, long referenceId, int pieces)
        {

            this.title = title;
            this.reference = reference;
            this.context = context;
            this.okClickAction = okClickAction;
            this.referenceId=referenceId;
            this.pieces =pieces;
            this.printerId=0;   
            this.printerName = "N/A";
            printers = MCH.Communication.Miscellaneous.Instance.GetPrinters(ApplicationSessionState.User.Data.UserId, MCH.Communication.PrinterTypes.Label);


            if (printers.Transaction.Status)
            {


                if (printers.Data != null)
                {

                    foreach (var printer in printers.Data)
                    {
                        printersList.Add(new MCH.Communication.SelectionItem(printer.PrinterName, printer.PrinterId, @"Icons/Printer", printer.IsDefault));
                        if (printer.IsDefault)
                        {
                            this.printerId= printer.PrinterId;   
                            this.printerName = printer.PrinterName;
                             
                        }
                       
                    }

 
                }

            }

        }

        //ADD this for error when rotating
        public PrinterDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView (inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.PrinterDialog, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);
 
            ok = DialogInstance.FindViewById<Button>(Resource.Id.ok); 
            ok.Click += OnOk_Click;

            cancel =DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            cancel.Click += OnCancelClick;


            options =DialogInstance.FindViewById<Button>(Resource.Id.options);
            options.Click += OnOptionsClick;
   

            lblReference=DialogInstance.FindViewById<TextView>(Resource.Id.lblReference);
            lblPrinter=DialogInstance.FindViewById<TextView>(Resource.Id.lblPrinter);
            txtPieces=DialogInstance.FindViewById<TextView>(Resource.Id.txtPieces);

            lblReference.Text = "Reference: " + this.reference;
            txtPieces.Text = this.pieces.ToString();
            lblPrinter.Text = "Printer: " + printerName;
            txtPieces.RequestFocus();
             






            return DialogInstance;
        }


        private void OnOk_Click(object sender, EventArgs e)
        {

            DoPieces();

        }

        void DoPieces()
        {
            int pcs;
            if (int.TryParse(txtPieces.Text,out pcs))
            {
                if (pcs > 0 && pcs <= this.pieces)
                {
                    okClickAction.Invoke(this.referenceId, this.printerId,pcs);
                    Dismiss();    
                }
                else
                {
                    MessageBox m = new MessageBox(context);
                    m.ShowMessage(GetText(Resource.String.Invalid_number_of_pieces) );
                } 
            }
            else
            {
                MessageBox m = new MessageBox(context);
                m.ShowMessage(GetText(Resource.String.Invalid_number_of_pieces) );
            }   
        }
 
        void OnOptionsClick(object sender, EventArgs e)
        {
            ShowPrinters();
        }

        public void ShowPrinters()
        {
 
            printers = MCH.Communication.Miscellaneous.Instance.GetPrinters(ApplicationSessionState.User.Data.UserId, MCH.Communication.PrinterTypes.Label);


            if (printers.Transaction.Status)
            {


                if (printers.Data != null)
                {
                    printersList.Clear();
                    foreach (var printer in printers.Data)
                    {
                        printersList.Add(new MCH.Communication.SelectionItem(printer.PrinterName, printer.PrinterId, @"Icons/Printer", printer.IsDefault));
 

                    }

                    Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnPrinterClickSelectionAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Printers), this.context, printersList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection, false);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                }

            }


    

        }


        private void OnPrinterClickSelectionAction(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {

            foreach (var r in result)
            {
                MCH.Communication.Miscellaneous.Instance.SetUserDefault(ApplicationSessionState.User.Data.UserId,(int)r.Id);
                this.printerId= r.Id;   
                this.printerName = r.Name;
                lblPrinter.Text = "Printer: " + printerName;
            }

        }


        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }



    }
}



