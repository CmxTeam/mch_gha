﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCH
{
    [Activity(Label = "WeightHelper")]			
    public class WeightHelper : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
        }

        public static double KgToLb(double weight)
        {
            return weight / 2.20462;
        }

        public static double LbToKg(double weight)
        {
            return weight * 2.20462;    
        }

    }
}

