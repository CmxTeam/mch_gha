﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class ReferenceDialog: DialogFragment
    {
        TextView textViewMessage;
        Button cancel;
        Button ok;
        TextView txtBarcode;
        EditTextEventListener EditTextListener;
        //        //Action<SnapShotTaskItem> okClickAction;
        Activity context;
        string title;
        string message;
        ImageButton ClearSearch;


        public delegate void OkClickActionEventHandler(Barcode  barcode);
        public event OkClickActionEventHandler OkClicked;

        public ReferenceDialog (Activity context,string title ,string message )
        {

            this.title = title.ToUpper();
            this.message = message.ToUpper();
            this.context = context;

        }

 

        //ADD this for error when rotating
        public ReferenceDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.BarcodeDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            ok = DialogInstance.FindViewById<Button>(Resource.Id.ok); 

            txtBarcode = DialogInstance.FindViewById<TextView>(Resource.Id.barcode);

            textViewMessage = DialogInstance.FindViewById<TextView>(Resource.Id.message);
            textViewMessage.Text = this.message;

            cancel =DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            cancel.Click += OnCancelClick;

            EditTextListener = new EditTextEventListener(txtBarcode);
            EditTextListener.OnEnterEvent += OnEnterEvent;


            ok.Click += OnOk_Click;


            ClearSearch = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearSearchButton);
            ClearSearch.Click += OnClearSearch_Click;
            //
            //            //txtBarcode.Text = "bp008708702il";

            return DialogInstance;
        }


        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            EditTextListener.Text = string.Empty;
            txtBarcode.RequestFocus ();
        }

        private void OnOk_Click(object sender, EventArgs e)
        {

            if (txtBarcode.Text != string.Empty)
            {
                Barcode b = new Barcode();
                b.BarcodeType = BarcodeTypes.NA;
                b.BarcodeText = txtBarcode.Text;
                DoScan(b);
            }
            else
            {
                EditTextListener.EnableBarcode = false;
                MessageBox m = new MessageBox(context);
                m.OnConfirmationClick+= (bool result) => 
                    {
                        EditTextListener.EnableBarcode = true;
                    };
                m.ShowMessage(GetText(Resource.String.Invalid) + " " + context.GetText(Resource.String.Reference_Number) + ".");
            }


        }

        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            if(e.IsBarcode)
            {
                DoScan(e.BarcodeData);
            }
            else
            {
                Barcode b = new Barcode();
                b.BarcodeType = BarcodeTypes.NA;
                b.BarcodeText = txtBarcode.Text;
                DoScan(b);
            }
        }


        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {

            Dialog.Window.SetTitle( this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;

        }

 

        private void DoScan(Barcode barcode)
        {


            if (barcode.BarcodeText != string.Empty)
            {
 
                    if(OkClicked!=null)
                    {
                        OkClicked.Invoke(barcode);
                    }
 
                    Dismiss();
         

 
            }       
            else
            {
                EditTextListener.EnableBarcode = false;
                MessageBox m = new MessageBox(context);
                m.OnConfirmationClick+= (bool result) => 
                    {
                        EditTextListener.EnableBarcode = true;
                    };
                m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
            }

        }

    }
}



