﻿using System;

namespace MCH
{
    public class DataTypeHelper
    {
        public DataTypeHelper()
        {
        }

        public static bool  IsDate(string value)
        {
            try
            {
                DateTime v = DateTime.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsInt(string value)
        {
            try
            {
                int v = int.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static bool IsDouble(string value)
        {
            try
            {
                double v = double.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsNumeric(string value)
        {
            return IsDouble(value);
        }
    }
}

