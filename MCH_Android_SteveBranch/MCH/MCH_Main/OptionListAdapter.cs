﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.Res;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;

namespace MCH
{

    public enum OptionActions
    {
        Refresh,
        Logout,
        MainMenu,
        Exit,
        Gallery,
        DeleteAll,
        Finalize,
        Printers,
        Camera,
        Settings,
        Device,
        Connection,
        Log,
        Info,
        Search,
        Update,
        Date,
        Time,
        Add,
        Delete,
        Location,
        Edit,
        Print,
        View,
        OverPack,
        History,
        Remove,
        Reopen,
        Weight,
        Move,
        DropAtGate,
        DropAtStage,
        DropAtInboundStage,
        DropAtOutboundStage,
        Processed,
    }

    public class OptionItem
    {
        public OptionItem(string optionName ,OptionActions optionAction,int optionImage)
        {
            this.OptionName = optionName;
            this.OptionImage = optionImage;
            this.OptionAction = optionAction;
            this.OptionImageName = string.Empty;
        }

        public OptionItem(string optionName ,OptionActions optionAction,string optionImageName)
        {
            this.OptionName = optionName;
            this.OptionImage = 0;
            this.OptionAction = optionAction;
            this.OptionImageName = optionImageName;
        }

        public string OptionName;
        public OptionActions OptionAction;
        public int OptionImage;
        public string OptionImageName;
    }

    public class OptionListAdapter : BaseAdapter<OptionItem> {
        
        List<OptionItem> items;
        Activity context;

        public OptionListAdapter(Activity context, List<OptionItem> items): base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override OptionItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }

       
        public  override View GetView (int position, View convertView, ViewGroup parent)
        {

            var item = items [position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate (Resource.Layout.OptionRow, null);

 
            view.FindViewById<TextView> (Resource.Id.OptionName).Text = item.OptionName.ToUpper();
            ImageView pic = view.FindViewById<ImageView> (Resource.Id.OptionIcon);

 

            if (item.OptionImage > 0)
            {   
                try
                {
                    pic.SetImageResource(item.OptionImage);
                }
                catch
                {
                    pic.SetImageResource (Resource.Drawable.Menu);
                }
            }
            else
            {
                if(!String.IsNullOrEmpty(item.OptionImageName))
                    {
                        try
                        {
                        System.IO.Stream ims = context.Assets.Open(string.Format(@"{0}.png", item.OptionImageName));
                            Drawable d = Drawable.CreateFromStream(ims, null);
                            pic.SetImageDrawable(d);
                        }
                        catch
                        {
                            pic.SetImageResource(Resource.Drawable.Menu);
                        }
                    }
                    else
                    {
                        pic.SetImageResource(Resource.Drawable.Menu); 
                    }
 
            }




            pic.Tag = position.ToString();

            return view;
        }

 

 
 

    }
}

 