﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class Camera_ScanDialogActivity: DialogFragment
    {
        TextView textViewMessage;
        Button cancel;
        Button ok;
        TextView txtBarcode;
        EditTextEventListener EditTextListener;
//        //Action<SnapShotTaskItem> okClickAction;
        Activity context;
        string title;
        string message;
        ImageButton ClearSearch;
       

        public delegate void OkClickActionEventHandler(SnapShotTaskItem task);
        public event OkClickActionEventHandler OkClicked;

        public Camera_ScanDialogActivity (Activity context  )
        {

            this.title =context.GetText(Resource.String.CargoSnapShot).ToUpper();
            this.message = context.GetText(Resource.String.Shipment_Reference_Number).ToUpper();
            this.context = context;
            
        }

//        public Camera_ScanDialogActivity (Activity context, Action<SnapShotTaskItem> okClickAction ,bool confirmSkip)
//        {
//
//            this.title =context.GetText(Resource.String.CargoSnapShot);
//            this.message = context.GetText(Resource.String.Reference_Number);
//            this.context = context;
//            //this.okClickAction = okClickAction;
//            this.confirmSkip = confirmSkip;
//        }

        //ADD this for error when rotating
        public Camera_ScanDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
 
            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.BarcodeDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            ok = DialogInstance.FindViewById<Button>(Resource.Id.ok); 

            txtBarcode = DialogInstance.FindViewById<TextView>(Resource.Id.barcode);

            textViewMessage = DialogInstance.FindViewById<TextView>(Resource.Id.message);
            textViewMessage.Text = this.message;

            cancel =DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            cancel.Click += OnCancelClick;

            EditTextListener = new EditTextEventListener(txtBarcode);
            EditTextListener.OnEnterEvent += OnEnterEvent;


            ok.Click += OnOk_Click;


            ClearSearch = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearSearchButton);
            ClearSearch.Click += OnClearSearch_Click;
//
//            //txtBarcode.Text = "bp008708702il";

            return DialogInstance;
        }


        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            EditTextListener.Text = string.Empty;
            txtBarcode.RequestFocus ();
        }

        private void OnOk_Click(object sender, EventArgs e)
        {

            if (txtBarcode.Text != string.Empty)
            {
                Barcode b = new Barcode();
                b.BarcodeType = BarcodeTypes.NA;
                b.BarcodeText = txtBarcode.Text;
                DoScan(b);
            }
            else
            {
                EditTextListener.EnableBarcode = false;
                MessageBox m = new MessageBox(context);
                m.OnConfirmationClick+= (bool result) => 
                    {
                        EditTextListener.EnableBarcode = true;
                    };
                m.ShowMessage(GetText(Resource.String.Invalid) + " " + context.GetText(Resource.String.Reference_Number) + ".");
            }


        }

        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            if(e.IsBarcode)
            {
                DoScan(e.BarcodeData);
            }
            else
            {
                Barcode b = new Barcode();
                b.BarcodeType = BarcodeTypes.NA;
                b.BarcodeText = txtBarcode.Text;
                DoScan(b);
            }
        }


        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
 
            Dialog.Window.SetTitle( this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;

        }


        private void ShowAlertMessage(string message)
        {
            context.RunOnUiThread (delegate {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                AlertDialog dialog = builder.Create();
                dialog.SetIcon (Resource.Drawable.Icon);
                dialog.SetTitle(Application.Context.GetText(Resource.String.app_name));
                dialog.SetMessage(message);
                dialog.SetCancelable(false);
                dialog.SetButton (Application.Context.GetText(Resource.String.Ok),delegate{
                    txtBarcode.Text =string.Empty;
                    txtBarcode.RequestFocus ();
                });
                dialog.Show();
            });
        }

        private void ShowAlertMessage(int message)
        {
            ShowAlertMessage(Application.Context.GetText(message));
        }

        private void DoScan(Barcode barcode)
        {


            var progressDialog = ProgressDialog.Show(context, GetText(Resource.String.Please_Wait), GetText(Resource.String.Please_Wait), true);
            new Thread(new ThreadStart(delegate
                {

                    if (barcode.BarcodeText != string.Empty)
                    {

                        SnapShotTask task =  MCH.Communication.CargoSnapShot.Instance.GetSnapShotTask(ApplicationSessionState.SelectedWarehouseId,ImageGallery.CurrentTaskId,ApplicationSessionState.User.Data.UserId, barcode.BarcodeText);


                        if(task.Transaction.Status)
                        {
                            if(OkClicked!=null)
                            {
                                OkClicked.Invoke(task.Data);
                            }

                            //okClickAction.Invoke (task.Data);
                            Dismiss();
                        }
                        else
                        {
                            EditTextListener.EnableBarcode = false;
                            MessageBox m = new MessageBox(context);
                            m.OnConfirmationClick+= (bool result) => 
                                {
                                    EditTextListener.EnableBarcode = true;
                                };
                            m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
                        }





                    }       
                    else
                    {
                        EditTextListener.EnableBarcode = false;
                        MessageBox m = new MessageBox(context);
                        m.OnConfirmationClick+= (bool result) => 
                            {
                                EditTextListener.EnableBarcode = true;
                            };
                        m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
                    }

                    context.RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }

    }
}



