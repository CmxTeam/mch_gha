﻿using System;
using System.Collections.Generic;

namespace MCH
{
    public class EnumHelper
    {
        public EnumHelper()
        {
        }


        public static Dictionary<int, string> GetEnumToList<T>()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();   
            T[] values = (T[])Enum.GetValues(typeof(T));
            foreach (T n in values)
            {
                T en = (T)Enum.Parse(typeof(T), n.ToString());
                result.Add((int)Enum.Parse(typeof(T), n.ToString()),en.ToString().Replace("_"," "));
            }
            return result;
        }

        public static T GetEnumItem<T>(string value)
        {
            T en = (T)Enum.Parse(typeof(T), value.Replace(" ","_"));
            return en;
        }


    }
}

