﻿using System;
using System.Linq;
using System.Collections.Generic;
using MCH.Communication;
namespace MCH
{
    public class LinqHelper
    {
        public LinqHelper()
        {
        }

        public static List<T> Query<T>(List<T> data, string search)
        {
            List<T> result = new List<T>();
            if (string.IsNullOrEmpty(search))
            {
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        result.Add(item);
                    }  
                }
            }
            else
            {
                if (data != null)
                {

                    var query = 
                    from l in data  
                        where l.ToString().ToLower().Contains(search.Trim().ToLower())
                    select l;  
      
                    foreach (var item in query)
                    {
                        result.Add(item);
                    }

                }
            }

 
            return result;
        }

    }
}

