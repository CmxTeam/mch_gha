﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Camera = Android.Hardware.Camera;

namespace MCH
{
            
    public partial class Camera_Activity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;
 
        Camera camera;
        TextureView textureView;
        Button snap;         ImageView LastImage;
        Button NextButton;
        LinearLayout LastImageBorder;
        private void Initialize()
        {
            Initialize(string.Empty,string.Empty);
        }

        private void Initialize(string title, int icon)
        {
            Initialize(title, string.Empty);

            if (title != string.Empty)
            {
                if (icon == 0)
            {
                headerImage.Visibility = ViewStates.Gone;
            }
            else
            {
                try 
                {
                    headerImage.SetImageResource (icon);
                } 
                catch 
                {
                    headerImage.SetImageResource (Resource.Drawable.Clock);
                }
                headerImage.Visibility = ViewStates.Visible;
            }
            }


        }

        private void Initialize(string title, string icon)
        {
            headerImage = FindViewById<ImageView>(Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;
 
 
            headerText.Text = string.Empty;
            header.Visibility = ViewStates.Gone;

            

            if (icon == string.Empty)
            {
                headerImage.Visibility = ViewStates.Gone;
            }
            else
            {
                try 
                {
                    System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",icon));
                    Drawable d = Drawable.CreateFromStream(ims, null);
                    headerImage.SetImageDrawable(d);
                } 
                catch 
                {
                    headerImage.SetImageResource (Resource.Drawable.Clock);
                }
                headerImage.Visibility = ViewStates.Visible;
            }

            LastImageBorder = FindViewById<LinearLayout> (Resource.Id.LastImageBorder);
            LastImage = FindViewById<ImageView> (Resource.Id.LastImage);
            LastImage.Click += OnLastImageClick;
            textureView = FindViewById<TextureView> (Resource.Id.CameraView);             textureView.SurfaceTextureAvailable += EnableCamera;

            snap = FindViewById<Button> (Resource.Id.SnapButton);
            //snap.Click += TakaPicture;
            snap.Visibility =  ViewStates.Invisible;

            NextButton = FindViewById<Button> (Resource.Id.NextButton);
            NextButton.Click += OnNextButtonClick;
        }

 

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }
        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            LoadOptions();
        }

 

        private void LoadOptions()
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.Save_And_Upload),OptionActions.Finalize, Resource.Drawable.Validate));
            options.Add(new OptionItem(GetText(Resource.String.DeleteAll_Option),OptionActions.DeleteAll, Resource.Drawable.Delete));
            //options.Add(new OptionItem(GetText(Resource.String.Camera_Resolution),OptionActions.Camera ,Resource.Drawable.Camera));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.DeleteAll:
                    AskForDeleteAll();
                    break;
                case  OptionActions.Finalize:
                    DoNext();
                    break;
//                case  OptionActions.Camera:
//                    ShowCameraResolutions();
//                    break;
            }

        }

        public void AskForDeleteAll()
        {
            MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DeleteAllFiles();
                        RefreshData();
                    }
                };
            m.ShowConfirmationMessage(Resource.String.DeleteAll_Question);
        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            try
                {
                camera = Camera.Open ();
                camera.SetDisplayOrientation(90);
                camera.SetPreviewTexture (textureView.SurfaceTexture);
                camera.StartPreview ();
                }
                catch(Exception ex)
                {
                    base.ShowAlertMessage(ex.Message);
                }

            //textureView.SurfaceTextureAvailable -= EnableCamera;
            //textureView.SurfaceTextureAvailable += EnableCamera;
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();
 
            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();
 
            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {

            try
            {
                camera.StopPreview ();
                camera.Release ();
            }
            catch
            {
                //do nothing here and fix by checking camera status
            }
               
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}

