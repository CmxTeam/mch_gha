﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{


    public class LocationDialogActivity: DialogFragment
    {
        TextView textViewMessage;
        Button cancel;
        Button options;
        Button ok;
        TextView txtBarcode;
        EditTextEventListener EditTextListener;
        Action<Barcode> okClickAction;
        Activity context;
        string title;
        string message;
        bool showOptions;
        List<LocationTypes> locationTypes;
        Locations locations;


        public delegate void OkClickActionEventHandler(Barcode barcode);
        public event OkClickActionEventHandler OkClicked;

        public delegate void CancelClickActionEventHandler();
        public event CancelClickActionEventHandler CancelClicked;

        public LocationDialogActivity (Activity context, string title, string message, bool showOptions,params LocationTypes[] locationTypes)
        {
 
            this.locationTypes = new List<LocationTypes>();
            if (locationTypes != null && locationTypes.Length > 0)
            {
                foreach (LocationTypes i in locationTypes)
                {
                    this.locationTypes.Add(i);
                }
            }

            locations = Miscellaneous.Instance.GetLocations(this.locationTypes.ToArray());
            if (!locations.Transaction.Status)
            {
                EditTextListener.EnableBarcode = false;
               MessageBox m = new MessageBox(this.context);
               m.OnConfirmationClick += (bool result) => 
               {
                        EditTextListener.EnableBarcode = true;
                Dismiss(); 
               };
               m.ShowAlert(locations.Transaction.Error,MessageBox.AlertType.Error);

            }
 
            this.title = title;
            this.message = message;
            this.context = context;
            this.okClickAction = null;
            this.showOptions = showOptions;

        }
 
        //change this one for overload
        public LocationDialogActivity (Activity context,  int title, int message, bool showOptions,params LocationTypes[] locationTypes)
        {
 

            this.locationTypes = new List<LocationTypes>();
            if (locationTypes != null && locationTypes.Length > 0)
            {
                foreach (LocationTypes i in locationTypes)
                {
                    this.locationTypes.Add(i);
                }
            }

            locations = Miscellaneous.Instance.GetLocations(this.locationTypes.ToArray());
            if (!locations.Transaction.Status)
            {
                EditTextListener.EnableBarcode = false;
                MessageBox m = new MessageBox(this.context);
               m.OnConfirmationClick += (bool result) => 
               {
                        EditTextListener.EnableBarcode = true;
                Dismiss(); 
               };
               m.ShowAlert(locations.Transaction.Error,MessageBox.AlertType.Error);
            }
 
            this.title =context.GetText(title);
            this.message = context.GetText(message);
            this.context = context;
            this.okClickAction = null;
            this.showOptions = showOptions;
        }



        public LocationDialogActivity (Activity context, Action<Barcode> okClickAction, string title, string message, bool showOptions,params LocationTypes[] locationTypes)
        {
 
            this.locationTypes = new List<LocationTypes>();
            if (locationTypes != null && locationTypes.Length > 0)
            {
                foreach (LocationTypes i in locationTypes)
                {
                    this.locationTypes.Add(i);
                }
            }

            locations = Miscellaneous.Instance.GetLocations(this.locationTypes.ToArray());
            if (!locations.Transaction.Status)
            {
                EditTextListener.EnableBarcode = false;
               MessageBox m = new MessageBox(this.context);
               m.OnConfirmationClick += (bool result) => 
               {
                        EditTextListener.EnableBarcode = true;
                Dismiss(); 
               };
               m.ShowAlert(locations.Transaction.Error,MessageBox.AlertType.Error);

            }
 
            this.title = title;
            this.message = message;
            this.context = context;
            this.okClickAction = okClickAction;
            this.showOptions = showOptions;

        }
 
        //change this one for overload
        public LocationDialogActivity (Activity context, Action<Barcode> okClickAction, int title, int message, bool showOptions,params LocationTypes[] locationTypes)
        {
 

            this.locationTypes = new List<LocationTypes>();
            if (locationTypes != null && locationTypes.Length > 0)
            {
                foreach (LocationTypes i in locationTypes)
                {
                    this.locationTypes.Add(i);
                }
            }

            locations = Miscellaneous.Instance.GetLocations(this.locationTypes.ToArray());
            if (!locations.Transaction.Status)
            {
                EditTextListener.EnableBarcode = false;
                MessageBox m = new MessageBox(this.context);
               m.OnConfirmationClick += (bool result) => 
               {
                        EditTextListener.EnableBarcode = true;
                Dismiss(); 
               };
               m.ShowAlert(locations.Transaction.Error,MessageBox.AlertType.Error);
            }
 
            this.title =context.GetText(title).ToUpper();
            this.message = context.GetText(message);
            this.context = context;
            this.okClickAction = okClickAction;
            this.showOptions = showOptions;
        }

        //ADD this for error when rotating
        public LocationDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView (inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.LocationDialog, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            ok = DialogInstance.FindViewById<Button>(Resource.Id.ok); 

            txtBarcode = DialogInstance.FindViewById<TextView>(Resource.Id.barcode);

            textViewMessage = DialogInstance.FindViewById<TextView>(Resource.Id.message);
            textViewMessage.Text = this.message.ToUpper();

            cancel =DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            cancel.Click += OnCancelClick;


            options = DialogInstance.FindViewById<Button>(Resource.Id.options);
            if (this.showOptions)
            {
                if (locations.Data.Count == 0)
                {
                    options.Visibility = ViewStates.Gone;
                }
                else
                {
                    options.Click += OnOptionsClick;
                    options.Visibility = ViewStates.Visible;
                }
            }
            else
            {
                options.Visibility = ViewStates.Gone;
            }
           
 

            EditTextListener = new EditTextEventListener(txtBarcode);
            EditTextListener.OnEnterEvent += OnEnterEvent;


            ok.Click += OnOk_Click;


            return DialogInstance;
        }


        private void OnOk_Click(object sender, EventArgs e)
        {
            DoSearch(txtBarcode.Text);  
        }


        private void DoSearch(string value)
        {
 


            var query = 
                    from l in locations.Data  
                    where l.Location.ToLower() == value.ToLower() || l.LocationBarcode.ToLower() == value.ToLower()
                select l;  

                foreach (var item in query)
                {
                    Barcode barcode = BarcodeParser.Parse(item.LocationPrefix + "-" + item.LocationBarcode); 
                    barcode.Id = item.LocationId;
                    barcode.Location = item.Location;

                //new
                if (OkClicked != null)
                    OkClicked(barcode);
                if (okClickAction != null)
                    okClickAction.Invoke (barcode);
                
                    Dismiss();
                    return;
                }


            EditTextListener.EnableBarcode = false;
            MessageBox m = new MessageBox(context);
            m.OnConfirmationClick+= (bool result) => 
                {
                    EditTextListener.EnableBarcode = true;
                };
            m.ShowMessage(GetText(Resource.String.Invalid) + " " + title + ".");
        }



        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            if(e.IsBarcode)
            {
                DoScan(e.BarcodeData);
            }
            else
            {
                DoSearch(txtBarcode.Text);  
            }
        }

        void OnOptionsClick(object sender, EventArgs e)
        {

           

            if (locations.Data.Count > 0)
            {
                //no need to create and action just pass the method as delegate
                //Action<List<MCH.Communication.LocationItem>> LocationClickAction = OnLocationClickAction;
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new LocationListDialogActivity(GetText(Resource.String.Locations), context,locations.Data, OnLocationClickAction, (locations.Data.Count > 10) ? true : false);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }
            
             
        }


       private void OnLocationClickAction(List<LocationItem>  result)
        {
            foreach (var r in result)
            {
                EditTextListener.EnableEnter = false;

        
                txtBarcode.Text = r.Location;

                EditTextListener.EnableEnter = true;
           


                //New //new
                DoSearch(r.Location); 



            }
        }



        void OnCancelClick(object sender, EventArgs e)
        {

            if (CancelClicked != null)
                CancelClicked();

            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title.ToUpper());
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);

            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }
 

        private void DoScan(Barcode barcode)
        {
            
            if (barcode.BarcodeType != BarcodeTypes.NA)
            {

                var query = 
                    from l in locations.Data  
                        where (l.Location.ToLower() == barcode.Location.ToLower() || l.LocationBarcode.ToLower() == barcode.Location.ToLower()) && l.LocationPrefix.ToLower()  == barcode.BarcodePrefix.ToLower()
                select l;  

                foreach (var item in query)
                {
                        barcode.Id = item.LocationId;
                        barcode.Location = item.Location;
 

                    //new
                    if (OkClicked != null)
                        OkClicked(barcode);
                    if (okClickAction != null)
                        okClickAction.Invoke (barcode);
                    
                        Dismiss();
                        return;    
                }

 
            }
 
            EditTextListener.EnableBarcode = false;
            MessageBox m = new MessageBox(context);
            m.OnConfirmationClick+= (bool result) => 
                {
                    EditTextListener.EnableBarcode = true;
                };
                
            m.ShowMessage(GetText(Resource.String.Invalid) + " " + title + ".");
 

        }

    }
}

