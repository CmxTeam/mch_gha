﻿
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Graphics;

namespace MCH
{
    //Add This to activate OnConfigurationChanged event
    //ConfigurationChanges=Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize
    //ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
    [Activity ( Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]     
        
    public partial class Camera_Activity : BaseActivity , Android.Hardware.Camera.IPictureCallback ,Android.Hardware.Camera.IPreviewCallback, Android.Hardware.Camera.IShutterCallback
    {

        private bool confirmSkip = false;

        public override bool OnCreateOptionsMenu (IMenu menu)
    {
            
            LoadOptions();
            return true;
    }

        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);
            SetContentView (Resource.Layout.Camera_Layout_Full);

            string extra = Intent.GetStringExtra("ConfirmSkip");
            if (!string.IsNullOrEmpty(extra))
            {
                confirmSkip = bool.Parse(extra);
            }
     
            Initialize();

            CreateDirectoryForPictures();

            RefreshData();


            ThreadPool.QueueUserWorkItem (o => EnableCameraButton ());


            Toast.MakeText (this, "#" + ImageGallery.ImageGalleryReference, ToastLength.Long).Show ();




        }
 
        private void EnableCameraButton()
        {
           

            Thread.Sleep (2000);
 
                    RunOnUiThread (delegate 
                    {
                        snap.Click += TakaPicture;
                        snap.Visibility =  ViewStates.Visible;
                    });
 



        }

        private int GetDisplayOrientation()
        {
           
            int result = 0;
            var surfaceOrientation = WindowManager.DefaultDisplay.Rotation;

            if (ApplicationSessionState.GetDeviceName().ToLower().Contains("samsung") && ApplicationSessionState.GetDeviceName().ToLower().Contains("ek-"))
            {
                if (surfaceOrientation == SurfaceOrientation.Rotation0)
                {
                    result  = 0;
                }
                else if (surfaceOrientation == SurfaceOrientation.Rotation90)
                {
                    result = 270;
                }
                else if (surfaceOrientation == SurfaceOrientation.Rotation270)
                {
                    result = 90;
                }
                else if (surfaceOrientation == SurfaceOrientation.Rotation180)
                {
                    result = 180;
                }  
            }
            else
            {
           
            if (surfaceOrientation == SurfaceOrientation.Rotation0)
            {
                result  = 90;
            }
            else if (surfaceOrientation == SurfaceOrientation.Rotation90)
            {
                result = 0;
            }
            else if (surfaceOrientation == SurfaceOrientation.Rotation270)
            {
                result = 180;
            }
            else if (surfaceOrientation == SurfaceOrientation.Rotation180)
            {
                result = 270;
            }

            }
            return result;

        }

        private bool IsLandscape()
        {
            var surfaceOrientation = WindowManager.DefaultDisplay.Rotation;
            if (surfaceOrientation == SurfaceOrientation.Rotation0)
            {
                return false;
            }

            if (surfaceOrientation == SurfaceOrientation.Rotation180)
            {
                return false;
            }

            if (surfaceOrientation == SurfaceOrientation.Rotation90)
            {
                return true;
            }
            if (surfaceOrientation == SurfaceOrientation.Rotation270)
            {
                return true;
            }
   

            return false;

        }


        private void DoBack()
        {
           
       

            if (ImageGallery.Instance.Count > 0)
            {

                var nextScreen = new Intent(this, typeof(Camera_GalleryActivity));
                nextScreen.PutExtra("Title", "Camera");
                StartActivity(nextScreen);
                this.Finish();
 
            }
            else
            {
 
                StartActivity(ImageGallery.BackScreen);
                this.Finish();
 

            }
 

        }
 
//        public override void OnConfigurationChanged (Android.Content.Res.Configuration newConfig)
//        {
//            base.OnConfigurationChanged (newConfig);
//
//            if (newConfig.Orientation == Android.Content.Res.Orientation.Portrait) {
//              //Change to Portrait
//            } else if (newConfig.Orientation == Android.Content.Res.Orientation.Landscape) {
//              //Changed to landscape
//            }
//  
//        }

        void EnableCamera(object sender, TextureView.SurfaceTextureAvailableEventArgs e)
        {


            try
            {
                if(camera!=null)
                {
                    camera.StopPreview ();
                    camera.Release ();
                }

            }
            catch(Exception ex)
            {
                base.ShowAlertMessage(ex.Message);
            }

            try
            {


                 
                camera = Camera.Open ();
                camera.SetDisplayOrientation(GetDisplayOrientation());
                camera.SetPreviewTexture (textureView.SurfaceTexture);
                camera.StartPreview ();
                snap.Enabled =true;
 
            }
            catch(Exception ex)
            {
                base.ShowAlertMessage(ex.Message);
            }
        }

        void OnNextButtonClick(object sender, EventArgs e)
        {
            DoNext();
        }
        void DoNext()
        {

            if (ImageGallery.Instance.Count > 0)
            {
                DoUpload();
            }
            else
            {

                if (confirmSkip)
                {
                                    MessageBox m = new MessageBox(this);
                                    m.OnConfirmationClick+= (bool result) => 
                                    {
                                        if(result)
                                        {
                                            StartActivity(ImageGallery.NextScreen);
                                            this.Finish();
                                        }
                                    };
                                    m.ShowConfirmationMessage(Resource.String.Confirm_Skip_CargoSnapShot);
                }
                else
                {
                    StartActivity(ImageGallery.NextScreen);
                    this.Finish(); 
                }



            }

        }



        void TakaPicture(object sender, EventArgs e)
        {

            int max = ApplicationSessionState.MaxImageCount(this);
            //int.TryParse(GetText(Resource.String.MaxImageCount),out max);


            if (ImageGallery.Instance.Count >=   max &&  max >0)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.Image_Limit_Message);
                return;
            }


                try
                {
                snap.Enabled =false;
                Flash(true);
               
                camera.TakePicture(this,this,this);
                }
                catch(Exception ex)
                {
                    base.ShowAlertMessage(ex.Message);
                }
        }

        void Flash(bool on)
        {
            bool isAuto = false;
            try
            {
                 
               
                var p = camera.GetParameters();
                var supportedFlashModes = p.SupportedFlashModes;

                if (supportedFlashModes == null)
                    supportedFlashModes = new List<string>();

                var flashMode = string.Empty;

                if (on)
                {
                    

                    if (supportedFlashModes.Contains(Android.Hardware.Camera.Parameters.FlashModeAuto))
                    {
                        flashMode = Android.Hardware.Camera.Parameters.FlashModeAuto;
                        isAuto = true;
                    }
                    else if (supportedFlashModes.Contains(Android.Hardware.Camera.Parameters.FlashModeTorch))
                        flashMode = Android.Hardware.Camera.Parameters.FlashModeTorch;
                    else if (supportedFlashModes.Contains(Android.Hardware.Camera.Parameters.FlashModeOn))
                        flashMode = Android.Hardware.Camera.Parameters.FlashModeOn;


                }
                else
                {
                    if (supportedFlashModes.Contains(Android.Hardware.Camera.Parameters.FlashModeOff))
                        flashMode = Android.Hardware.Camera.Parameters.FlashModeOff;
                }




                try
                {
                    IList<Camera.Size> sizes = p.SupportedPictureSizes;


                    int resolution = 0;
                    if (ApplicationSessionState.GetString(this, "Resolution") == string.Empty)
                    {
                        resolution = (((sizes.Count - (sizes.Count % 2)) / 2) + (sizes.Count % 2)) - 1;
                        ApplicationSessionState.SaveString(this,"Resolution",resolution.ToString());
                    }
                    else
                    {
                        resolution = ApplicationSessionState.GetInt(this, "Resolution");
                    }



                    p.SetPictureSize(sizes[resolution].Width, sizes[resolution].Height);
                    p.JpegQuality = 100;
                    camera.SetParameters(p);
                }
                catch (Exception ex1)
                {
                    throw new Exception(ex1.Message);
                }


                if (!string.IsNullOrEmpty(flashMode))
                {
                    p.FlashMode = flashMode;
                    camera.SetParameters(p);
                }



            }
            catch (Exception ex)
            {
                base.ShowAlertMessage(ex.Message);
            }






            if (on)
            {
                if (!isAuto)
                {
                    Thread.Sleep(1000);
                }
 
            }

        }


        void Camera.IPictureCallback.OnPictureTaken(byte[] data, Android.Hardware.Camera camera)
        {
            FileOutputStream outStream = null;
 
            File dataDir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);


            if (data != null) {
                try{


                  
                    string file =  Guid.NewGuid().ToString() ;
                    outStream = new FileOutputStream(dataDir + "/" + file  + ".jpg");
                    outStream.Write(data);
                    outStream.Close();
                     
                    ImageGallery.Instance.Stack(file,GetDisplayOrientation(),IsLandscape());
 
                    RefreshData();


                }catch(FileNotFoundException e){
                    base.ShowAlertMessage(e.Message);
                }catch(IOException ie){
                    base.ShowAlertMessage(ie.Message);
                }






                camera.SetDisplayOrientation(GetDisplayOrientation());


                camera.SetPreviewTexture (textureView.SurfaceTexture);
                camera.StartPreview ();

            
                snap.Enabled =true;

                Flash(false);



            }

  
        }


        private void RefreshData()
        {

            //EnableCameraButton(true);
            snap.Text = ImageGallery.Instance.Count.ToString();
            if (ImageGallery.Instance.Count > 0)
            {
                NextButton.Text = GetText(Resource.String.Next);
                LastImage.Visibility = ViewStates.Visible;
                LastImageBorder.Visibility = ViewStates.Visible;
                File dataDir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);
                MCH.Graphics.BitmapHelper.LoadImage(dataDir + "/" + ImageGallery.Instance.ImageItems[0].FileName + ".jpg", LastImage, 58, 58, ImageGallery.Instance.ImageItems[0].Rotation);
                //LastImage.SetScaleType (ImageView.ScaleType.FitXy);
            }
            else
            {
                NextButton.Text = GetText(Resource.String.Skip);
                LastImage.Visibility =  ViewStates.Invisible;
                LastImageBorder.Visibility = ViewStates.Invisible;
            }
        
            

        }

        private void CreateDirectoryForPictures()
        {
            File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures),ApplicationSessionState.ImageFolder);
            if (!dir.Exists())
            {
                dir.Mkdirs();
            }

        }

        void Camera.IPreviewCallback.OnPreviewFrame(byte[] b, Android.Hardware.Camera c)
        {

        }

        void Camera.IShutterCallback.OnShutter()
        {

        }


 
        private void OnLastImageClick(object sender, EventArgs e)
        {
 
            if (ImageGallery.Instance.Count > 0)
            {
                var nextScreen = new Intent (this, typeof(Camera_FragmentActivity));
                nextScreen.PutExtra ("ImageNumber", "0");
                nextScreen.PutExtra("FromCamera", "true");
                StartActivity (nextScreen);
                this.Finish();
 

            }

        }
 

 


    }
}


 