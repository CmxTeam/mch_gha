﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using Android.Graphics;

using SignaturePad;

namespace MCH
{
 
    public class SignatureDialogActivity: DialogFragment
    {
        
        Button cancel;
        Button ok;
        SignaturePadView signature;
        System.Drawing.PointF [] points;

        static Activity context;
        static Action<Bitmap> okClickAction;

        public SignatureDialogActivity (Activity context, Action<Bitmap> okClickAction)
        {
 
            SignatureDialogActivity.context = context;
            SignatureDialogActivity.okClickAction = okClickAction;
        }

  
        //ADD this for error when rotating
        public SignatureDialogActivity()
        {
           // Dismiss();
        }


  

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView (inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.SignatureDialog, container, false);
 

            ok = DialogInstance.FindViewById<Button>(Resource.Id.OkButton); 
 
 

            cancel =DialogInstance.FindViewById<Button>(Resource.Id.CancelButton);
 

            cancel.Click += OnCancelClick;



            ok.Click += OnOkClick;
 


            


            signature = DialogInstance.FindViewById<SignaturePadView> (Resource.Id.SignatureView);                         signature.Caption.Text = Application.Context.GetText(Resource.String.Signature);             signature.Caption.SetTypeface (Typeface.Serif, TypefaceStyle.BoldItalic);             signature.Caption.SetTextSize (global::Android.Util.ComplexUnitType.Sp, 16f);             signature.SignaturePrompt.Text = Application.Context.GetText(Resource.String.SignaturePrompt);             signature.SignaturePrompt.TextAlignment = TextAlignment.Center;             signature.SignaturePrompt.SetTypeface (Typeface.SansSerif, TypefaceStyle.Normal);             signature.SignaturePrompt.SetTextSize (global::Android.Util.ComplexUnitType.Sp, 20f);             //signature.BackgroundColor = Color.Rgb (255, 255, 200); // a light yellow.
            signature.BackgroundColor = Color.Rgb (234, 234, 234); // a light gray.             signature.StrokeColor = Color.Black;              signature.BackgroundImageView.SetImageResource (Resource.Drawable.Icon);             signature.BackgroundImageView.SetAlpha (16);             signature.BackgroundImageView.SetAdjustViewBounds (true);             var layout = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.FillParent, RelativeLayout.LayoutParams.FillParent);             layout.AddRule (LayoutRules.CenterInParent);             layout.SetMargins (20, 20, 20, 20);             signature.BackgroundImageView.LayoutParameters = layout;              // You can change paddings for positioning...             var caption = signature.Caption;             caption.SetPadding (caption.PaddingLeft, 1, caption.PaddingRight, 25);         
            return DialogInstance;
        }

 
        void OnCancelClick(object sender, EventArgs e)
        {
            SignatureDialogActivity.okClickAction.Invoke(null);
            ClearObjects();
            Dismiss();
        }

        void OnOkClick(object sender, EventArgs e)
        {
            if (signature.IsBlank)
            { 
                ShowAlertMessage(Resource.String.Invalid_Signature);
                points = signature.Points;
            }
            else
            {
                points = signature.Points;
                SignatureDialogActivity.okClickAction.Invoke(signature.GetImage());
                ClearObjects();
                Dismiss();
            }
  
        }

        private void ClearObjects()
        {
            SignatureDialogActivity.okClickAction = null;
            SignatureDialogActivity.context = null;

        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (string.Empty);
            Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }

        private void ShowAlertMessage(string message)
        {
            SignatureDialogActivity.context.RunOnUiThread (delegate {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        AlertDialog dialog = builder.Create();
                                        dialog.SetIcon (Resource.Drawable.Icon);
                                        dialog.SetTitle(Application.Context.GetText(Resource.String.app_name));
                                        dialog.SetMessage(message);
                                        dialog.SetCancelable(false);
                                        dialog.SetButton (Application.Context.GetText(Resource.String.Ok),delegate{
                                 
                                        });
                                        dialog.Show();
                                        });
        }
                                        
        private void ShowAlertMessage(int message)
        {
            ShowAlertMessage(Application.Context.GetText(message));
        }

 

    }
}

