﻿using System;
using System.Text.RegularExpressions;
using System.Text;
using MCH.Communication;
namespace MCH
{
    public class BarcodeParser
    {
        private BarcodeParser()
        {

        }

        public static Barcode Parse(LocationItem location)
        {
            Barcode scan = Parse(location.LocationPrefix + "-" + location.LocationBarcode);
            scan.Id = location.LocationId;
            return  scan;
        }

        public static long GetLocationId(string location)
        {
            try
            {
                MCH.Communication.Location l = MCH.Communication.Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, location);
                if (l.Transaction.Status && l.Data != null)
                {
                    return l.Data.LocationId;
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }

        }

        public static Barcode Parse(string barcodeText)
        {
            Barcode scan = new Barcode();
            scan.BarcodeText = barcodeText;
            scan.BarcodeType = BarcodeTypes.NA;
            try
            {

                Match MatchObj;

                MatchObj = Regex.Match(barcodeText, "^P[-|*](.?)*", RegexOptions.IgnoreCase);
                if (MatchObj.Success == true)
                {
                    scan.BarcodePrefix = MatchObj.Value.Substring(0, 1);
                    scan.BarcodeSuffix = MatchObj.Value.Substring(2, MatchObj.Value.Trim().Length - 2);
                    scan.BarcodeType = BarcodeTypes.Printer;
                    scan.PrinterName = scan.BarcodeSuffix;
                    return scan;
                }


                MatchObj = Regex.Match(barcodeText, "^[A|B|C|R][-|*](.?)*", RegexOptions.IgnoreCase);
                if (MatchObj.Success == true)
                {
                    scan.BarcodePrefix = MatchObj.Value.Substring(0, 1);
                    scan.BarcodeSuffix = MatchObj.Value.Substring(2, MatchObj.Value.Trim().Length - 2);
                    scan.BarcodeType = BarcodeTypes.Area;
                    scan.Location = scan.BarcodeSuffix;
                    scan.Id = GetLocationId(scan.BarcodeSuffix);
                    return scan;
                }


                MatchObj = Regex.Match(barcodeText, "^D[-|*](.?)*", RegexOptions.IgnoreCase);
                if (MatchObj.Success == true)
                {
                    scan.BarcodePrefix = MatchObj.Value.Substring(0, 1);
                    scan.BarcodeSuffix = MatchObj.Value.Substring(2, MatchObj.Value.Trim().Length - 2);
                    scan.BarcodeType = BarcodeTypes.Door;
                    scan.Location = scan.BarcodeSuffix;
                    scan.Id = GetLocationId(scan.BarcodeSuffix);
                    return scan;
                }

                MatchObj = Regex.Match(barcodeText, "^S[-|*](.?)*", RegexOptions.IgnoreCase);
                if (MatchObj.Success == true)
                {
                    scan.BarcodePrefix = MatchObj.Value.Substring(0, 1);
                    scan.BarcodeSuffix = MatchObj.Value.Substring(2, MatchObj.Value.Trim().Length - 2);
                    scan.BarcodeType = BarcodeTypes.ScreeningArea;
                    scan.Location = scan.BarcodeSuffix;
                    scan.Id = GetLocationId(scan.BarcodeSuffix);
                    return scan;
                }

                MatchObj = Regex.Match(barcodeText, "^T[-|*](.?)*", RegexOptions.IgnoreCase);
                if (MatchObj.Success == true)
                {
                    scan.BarcodePrefix = MatchObj.Value.Substring(0, 1);
                    scan.BarcodeSuffix = MatchObj.Value.Substring(2, MatchObj.Value.Trim().Length - 2);
                    scan.BarcodeType = BarcodeTypes.Truck;
                    scan.Location = scan.BarcodeSuffix;
                    scan.Id = GetLocationId(scan.BarcodeSuffix);
                    return scan;
                }

                MatchObj = Regex.Match(barcodeText, @"^PIN[-|*]\d+", RegexOptions.IgnoreCase);
                if (MatchObj.Success == true)
                {
                    int suffixLen = 3;
                    scan.BarcodePrefix = MatchObj.Value.Substring(0, suffixLen);
                    scan.BarcodeSuffix = MatchObj.Value.Substring(suffixLen + 1, MatchObj.Value.Trim().Length - (suffixLen + 1));
                    scan.BarcodeType = BarcodeTypes.UserPin;
                    scan.UserPin = int.Parse(scan.BarcodeSuffix);
                    return scan;
                }

                MatchObj = Regex.Match(barcodeText, @"^ITDX[-|*]\d+", RegexOptions.IgnoreCase);
                if (MatchObj.Success == true)
                {
                    int suffixLen = 4;
                    scan.BarcodePrefix = MatchObj.Value.Substring(0, suffixLen);
                    scan.BarcodeSuffix = MatchObj.Value.Substring(suffixLen + 1, MatchObj.Value.Trim().Length - (suffixLen+1));
                    scan.BarcodeType = BarcodeTypes.ITDX;
                    scan.ITDX = scan.BarcodeSuffix;
                    return scan;
                }

 
 


                return scan;
            }
            catch
            {
                return scan;
            }
 
        }
    }

    public class Barcode
    {
        public string BarcodeText { get; set; }
        public string BarcodePrefix { get; set; }
        public string BarcodeSuffix { get; set; }
        public BarcodeTypes BarcodeType{ get; set; }
        public string Location { get; set; }
        public int UserPin { get; set; }
        public int UserId { get; set; }
        public int UserPassword { get; set; }
        public string PrinterName { get; set; }
        public string ITDX { get; set; }
        public long Id { get; set; }
    }

    public enum BarcodeTypes
    {
        NA,
        Door,
        Area,
        ScreeningArea,
        Truck,
        UserPin,
        Printer,
        ITDX,
    }

}

