﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class RecoverFreight_PiecesDialog: DialogFragment
    {
        TextView txtHidden;
        Button btnCancel;
        Button btnOP;
        Button btnPA;
 
        TextView txtLabelPcs;

        Activity context;
        string reference;
        
        TextView txtOfTotalPieces;
 
        int remainingPieces;
        int totalPieces;
         
        NumberPicker np;
        LinearLayout NumberPicker;
   
         
        public delegate void OkClickActionEventHandler(int pieces);
        public event OkClickActionEventHandler OkClicked;

        public delegate void OverpackActionEventHandler(int pieces);
        public event OverpackActionEventHandler OverpackClicked;

 

        private int minValue=1;
        public void SetMinToZero()
        {
            minValue = 0;
        }
 
        public RecoverFreight_PiecesDialog(Activity context,string reference,int totalPieces, int remainingPieces)
        {
            this.reference = reference;
            this.context = context;


            this.totalPieces = totalPieces;


            if (remainingPieces < 0)
            {
                this.remainingPieces = 0;
            }
            else
            {
                this.remainingPieces = remainingPieces;
            }

             


        }


        //ADD this for error when rotating
        public RecoverFreight_PiecesDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.RecoverFreight_PiecesDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);
 

          


            NumberPicker = DialogInstance.FindViewById<LinearLayout>(Resource.Id.NumberPicker);
             

            NumberPicker.Visibility =  ViewStates.Visible;
         


            NumberPicker.KeyPress += OnKeyPress;

            txtHidden = DialogInstance.FindViewById<TextView>(Resource.Id.txtHidden);
            txtLabelPcs= DialogInstance.FindViewById<TextView>(Resource.Id.txtLabelPcs);
           
            txtOfTotalPieces= DialogInstance.FindViewById<TextView>(Resource.Id.txtOfTotalPieces);
            if (totalPieces == 0)
            {
 
                txtOfTotalPieces.Visibility = ViewStates.Gone; 
                txtLabelPcs.Visibility = ViewStates.Gone; 

            }
            else
            {   
                txtLabelPcs.Visibility = ViewStates.Visible; 
                 
                txtOfTotalPieces.Visibility = ViewStates.Visible; 
                 
                txtOfTotalPieces.Text = GetText(Resource.String.Pieces_of).ToUpper() + " " +  totalPieces.ToString();
            }
 



            btnCancel =DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;

            btnOP = DialogInstance.FindViewById<Button>(Resource.Id.btnOP); 
            btnOP.Click += OnOP_Click;

            btnPA = DialogInstance.FindViewById<Button>(Resource.Id.btnPA); 
            btnPA.Click += OnPA_Click;




            np = (NumberPicker) DialogInstance.FindViewById(Resource.Id.npId);
            np.ValueChanged += (object sender, NumberPicker.ValueChangeEventArgs e) => 
                {
                    //ShowSelectedPcs(e.NewVal);
                };  

            if (remainingPieces > 0)
            {
                np.MaxValue = remainingPieces;
                np.MinValue = minValue;
                np.Value = remainingPieces;
                //ShowSelectedPcs(remainingPieces);
            }
            else
            {
                np.MaxValue = 0;
                np.MinValue = 0;
                np.Value = 0;
                //ShowSelectedPcs(0);
            }





            return DialogInstance;
        }


        //        void ShowSelectedPcs(int pcs)
        //        {
        //            btnOk.Text = string.Format("APPLY ({0})", pcs);
        //        }

        private void OnOP_Click(object sender, EventArgs e)
        {
            txtHidden.RequestFocus();
            ValidateOverpack();
        }
        private void OnPA_Click(object sender, EventArgs e)
        {
            txtHidden.RequestFocus();
            Validate();
        }


        private void ValidateOverpack()
        {

            try
            {

                int enteredpieces = 0;

                enteredpieces =  np.Value;


                if (OverpackClicked != null)
                    OverpackClicked(enteredpieces);

                Dismiss();
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(context);
                m.OnConfirmationClick += (bool result) => 
                    {

                    };
                m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);
            }



        }

        private void Validate()
        {

            try
            {

                int enteredpieces = 0;
      
                    enteredpieces =  np.Value;
           
 
                if (OkClicked != null)
                    OkClicked(enteredpieces);

                Dismiss();
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(context);
                m.OnConfirmationClick += (bool result) => 
                    {
                        
                    };
                m.ShowAlert(Resource.String.Invalid_number_of_pieces, MessageBox.AlertType.Information);
            }



        }

 

        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            if (string.IsNullOrEmpty(this.reference))
            {
                Dialog.Window.SetTitle( GetText( Resource.String.Pieces));    
            }
            else
            {
                Dialog.Window.SetTitle(this.reference);
            }

            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }



        private void OnKeyPress(object sender, View.KeyEventArgs e) 
        {

            if ( e.KeyCode == Keycode.Enter)
            {
                if (e.Event.Action== KeyEventActions.Down)
                {    
                    Validate();
                }

                e.Handled = true;
                return;
            }
            else
            {
                e.Handled = false;
            }

        }


    }
}




