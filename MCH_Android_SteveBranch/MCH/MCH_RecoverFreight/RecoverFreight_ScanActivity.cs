﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFreight_ScanActivity : BaseActivity
    {
        ValidateScanData scandata;
        //ScanData lastscan;
        public static  string LastScanText;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFreight_ScanLayout);
            Initialize();
          
            RefreshAll();


            if(!string.IsNullOrEmpty(Intent.GetStringExtra("Barcode")))
            {
                ValidateShipmentScan(Intent.GetStringExtra("Barcode"));
                }
            

        }
        private void RefreshAll()
        {
            LoadData();
            LoadDetail(); 
        }
        private void LoadDetail()
        {
            txtFlight.Text =string.Format("{0} {1} {2}",RecoverFreight_SessionState.CurrentRecoverTask.Origin,RecoverFreight_SessionState.CurrentRecoverTask.CarrierCode, RecoverFreight_SessionState.CurrentRecoverTask.FlightNumber);
            txtETA.Text = string.Format("ETA: {0:dd-MMM-yy HH:mm}",RecoverFreight_SessionState.CurrentRecoverTask.ETA).ToUpper();
            txtAwbs.Text = string.Format("AWBS: {0}", RecoverFreight_SessionState.CurrentRecoverTask.AWBCount);
            txtUlds.Text= string.Format("ULDS: {0} of {1}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredULDs,RecoverFreight_SessionState.CurrentRecoverTask.ULDCount);
            txtPcs.Text= string.Format("PCS: {0} of {1}", RecoverFreight_SessionState.CurrentRecoverTask.ReceivedPieces ,RecoverFreight_SessionState.CurrentRecoverTask.TotalPieces );
            txtRecover.Text= string.Format("RECOVERED: {0:dd-MMM-yy HH:mm}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredDate).ToUpper();
            txtRecoverBy.Text= string.Format("BY: {0}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredBy).ToUpper();





            IndicatorAdapter indicatorObj = new IndicatorAdapter(this, Indicator);
            indicatorObj.Load(RecoverFreight_SessionState.CurrentRecoverTask.Flag);

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",RecoverFreight_SessionState.CurrentRecoverTask.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }


            switch (RecoverFreight_SessionState.CurrentRecoverTask.Status)
            {
                case RecoverFreightStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case RecoverFreightStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case RecoverFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            btnFork.Text = GetForkLiftCount().ToString();

            if (!string.IsNullOrEmpty(LastScanText))
            {
                txtLastScan.Text = "Last Scan: " + LastScanText;
            }
            else
            {
                txtLastScan.Text = string.Empty;
            }

            if (!RecoverFreight_SessionState.CurrentUld.IsLoose)
            {
                txtULDNumber.Text =  RecoverFreight_SessionState.CurrentUld.UldPrefix + RecoverFreight_SessionState.CurrentUld.UldSerialNo; 
            }
            else
            {
                txtULDNumber.Text =  RecoverFreight_SessionState.CurrentUld.UldPrefix ; 
            }

           

        }

        private int GetForkLiftCount()
        {

         


            try
            {
                ForkLiftCount fl = MCH.Communication.RecoverFreight.Instance.GetForkliftCount(ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId);
                btnFork.Text = fl.Data.ToString();
                return fl.Data;
            }
            catch
            {
                btnFork.Text = "0";
                return 0;
            }
           
        }

        private void LoadData()
        {
            RecoverTask task=  MCH.Communication.RecoverFreight.Instance.GetFlightManifestById(RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId);
            if (task.Transaction.Status)
            {
                RecoverFreight_SessionState.CurrentRecoverTask = task.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(task.Transaction.Error, MessageBox.AlertType.Error);
            }
        }

        private void DoBack()
        {
            StartActivity (typeof(RecoverFreight_ULDActivity));
            this.Finish();
        }

        private void btnDrop_Click(object sender, EventArgs e)
        {
            if (GetForkLiftCount() == 0)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);
                return;
            }
           
            MessageBox mb = new MessageBox(this);
            mb.OnConfirmationClick += (bool result) => 
                {
                    if(result)
                    {
 
                            var transaction = FragmentManager.BeginTransaction();
                            var dialogFragment = new LocationDialogActivity(this, ShowBarcodeId, "Location","Select Location",true, LocationTypes.Area);
                            dialogFragment.Cancelable = false;
                            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
 
                    }
                };
            mb.ShowConfirmationMessage("Are you sure you want to drop all items from your forklift?");


        }
        private void ShowBarcodeId(Barcode  barcode)
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.DropForkliftPieces(RecoverFreight_SessionState.CurrentRecoverTask.TaskId ,ApplicationSessionState.User.Data.UserId  ,barcode.Id );
            if (t.Status)
            {
                RefreshAll();  
            }
            else
            {
                MessageBox mb = new MessageBox(this);
                mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }
   

        private void btnClear_Click(object sender, EventArgs e)
        {
            EditTextListener.Text = string.Empty;
        }
        private void DropSelectedPieces(List<ForkLiftSelectionItem> selection)
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new LocationDialogActivity(this, null, "Location","Select Location",true, LocationTypes.Area);
            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked += (Barcode barcode) => 
                {
                    foreach(var s in selection)
                    {
                        CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.DropPiecesToLocation(RecoverFreight_SessionState.CurrentRecoverTask.TaskId ,ApplicationSessionState.User.Data.UserId ,barcode.Id,s.DetailId,s.Pieces);
                        if (!t.Status)
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }
                    RefreshAll(); 
                };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

          
        }

        private void btnFork_Click(object sender, EventArgs e)
        {
            if (GetForkLiftCount() == 0)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);
                return;
            }

            List<ForkLiftSelectionItem > fl = new List< ForkLiftSelectionItem>();
        
            ForkLiftView fv =  MCH.Communication.RecoverFreight.Instance.GetForkliftView( RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId );
            if (fv.Transaction.Status && fv.Data!=null)
            {

                foreach (var i in fv.Data)
                {
                    fl.Add(new ForkLiftSelectionItem( i.Reference,i.DetailId ,i.ForkliftPieces, MCH.Communication.ReferenceTypes.AWB));
                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new RecoverFreight_ForkLiftDialog(  this, fl);
                dialogFragment.Cancelable = false;
                dialogFragment.OkClicked += (List<ForkLiftSelectionItem> selection,long locationId) => 
                    {

                        if(selection==null)
                        {
                            RefreshAll();
                        }
                        else
                        {
                            if(locationId==0)
                            {

                                DropSelectedPieces(selection);
                            }
                            else
                            {

                                foreach(var s in selection)
                                {
                                    CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.DropPiecesToLocation(RecoverFreight_SessionState.CurrentRecoverTask.TaskId ,ApplicationSessionState.User.Data.UserId ,locationId,s.DetailId,s.Pieces);
                                    if (!t.Status)
                                    {
                                        MessageBox mb = new MessageBox(this);
                                        mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                    }
                                }
                                RefreshAll();

                            }

                        }
                                    
                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;

            }

            

     

        }



        private void btnUlds_Click(object sender, EventArgs e)
        {
            StartActivity (typeof(RecoverFreight_ULDViewActivity));
            this.Finish();

        }

        private void UpdateEta(DateTime eta)
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.UpdateFlightETA(ApplicationSessionState.SelectedWarehouse,RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId ,RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId, eta);
            if (!t.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
            else
            {
                RecoverFreight_SessionState.CurrentRecoverTask.ETA = eta;
                txtETA.Text = string.Format("ETA: {0:dd-MMM-yy HH:mm}",RecoverFreight_SessionState.CurrentRecoverTask.ETA);
            }
 
        }


        private void DoScan(object sender, EditTextEventArgs e)
        {
            string data = e.Data;
            if (e.IsBarcode)
            {
                if (e.BarcodeData.BarcodeType == BarcodeTypes.Area ||
                    e.BarcodeData.BarcodeType == BarcodeTypes.Door ||
                    e.BarcodeData.BarcodeType == BarcodeTypes.ScreeningArea)
                {
                  
                    Location l = Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, e.BarcodeData.BarcodeSuffix);
                    if (l.Transaction.Status && l.Data != null)
                    {


                        if (GetForkLiftCount() > 0)
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.OnConfirmationClick += (bool result) => 
                                {
                                    if(result)
                                    {
                                        CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.DropForkliftPieces(RecoverFreight_SessionState.CurrentRecoverTask.TaskId ,ApplicationSessionState.User.Data.UserId  ,l.Data.LocationId );
                                        if (t.Status)
                                        {
                                            RefreshAll();  
                                        }
                                        else
                                        {
                                            MessageBox mbs = new MessageBox(this);
                                            mbs.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                        }
                                    }
                                };
                            mb.ShowConfirmationMessage("Are you sure you want to drop all items from your forklift into this location?");

                        }



                    }
                }
                else
                {
                    ValidateShipmentScan(e.Data);
                }

            }
            else
            {
                ValidateShipmentScan(e.Data);
            }
        }



        private void OnPiecesClickAction(int pieces)
        {
            if (pieces > 0)
            {
    
                CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.AddForkliftPieces(scandata.Data.DetailId , RecoverFreight_SessionState.CurrentRecoverTask.TaskId, pieces, ApplicationSessionState.User.Data.UserId);

                if (!t.Status)
                {
                    MessageBox mb = new MessageBox(this);
                    mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }
                else
                {
                    MediaSounds.Instance.Beep(this);
                }
                LastScanText = scandata.Data.Awb;
                RefreshAll();  
            }

        }

        private void ValidateShipmentScan(string reference)
        {

            scandata =  MCH.Communication.RecoverFreight.Instance.ValidateShipment(ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, RecoverFreight_SessionState.CurrentUld.UldId, reference);
 
            if (scandata.Transaction.Status)
            {

                EditTextListener.Text = string.Empty;

                if (scandata.Data.DetailId > 0)
                {
                    if (scandata.Data.AvailablePieces > 0)
                    {

                        var transaction = FragmentManager.BeginTransaction();
                        var dialogFragment = new PiecesDialog(this,scandata.Data.Awb, OnPiecesClickAction,0 ,scandata.Data.AvailablePieces );
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 

                    }
                    else
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowAlert("There are no pieces available.", MessageBox.AlertType.Information);
                    }
                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert("Unable to validate shipment.", MessageBox.AlertType.Information);
                }



            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(scandata.Transaction.Error, MessageBox.AlertType.Error);
            }

        }

        //            ScanData lastscan = MCH.Communication.RecoverFreight.Instance.ScanShipment(ApplicationSessionState.User.Data.AppUserId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, reference);
//            if (lastscan.Transaction.Status)
//            {
//                
//                EditTextListener.Text = string.Empty;
//
//                if (lastscan.Data.Count > 0)
//                {
//                    if (lastscan.Data[0].Shipment.AvailablePieces > 0)
//                    {
//
//                        var transaction = FragmentManager.BeginTransaction();
//                        var dialogFragment = new PiecesDialog(this, OnPiecesClickAction,0 ,lastscan.Data[0].Shipment.AvailablePieces , false );
//                        dialogFragment.Cancelable = false;
//                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
//
//                    }
//                    else
//                    {
//                        MessageBox m = new MessageBox(this);
//                        m.ShowAlert("There are no pieces available.", MessageBox.AlertType.Information);
//                    }
//                }
//                else
//                {
//                    MessageBox m = new MessageBox(this);
//                    m.ShowAlert("Unable to validate shipment.", MessageBox.AlertType.Information);
//                }
// 
// 
//              
//            }
//            else
//            {
//                MessageBox m = new MessageBox(this);
//                m.ShowAlert(lastscan.Transaction.Error, MessageBox.AlertType.Error);
//            }
//
//        }


    }
}


