﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class RecoverFreight_MainAdapter : BaseAdapter<RecoverTaskItem> {

        List<RecoverTaskItem> items;
        Activity context;
       


        public RecoverFreight_MainAdapter(Activity context, List<RecoverTaskItem> items): base()
        {

            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override RecoverTaskItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

         
 

            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFreight_MainRow, null);

  
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);             LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);
            TextView txtFlight = view.FindViewById<TextView>(Resource.Id.txtFlight);
            TextView txtETA = view.FindViewById<TextView>(Resource.Id.txtETA);
            TextView txtCounts = view.FindViewById<TextView>(Resource.Id.txtCounts);
            ImageView airlineImage= view.FindViewById<ImageView>(Resource.Id.airlineImage);

            txtFlight.Text =string.Format("{0} {1} {2}",item.Origin,item.CarrierCode, item.FlightNumber);
            txtETA.Text = string.Format("ETA: {0:dd-MMM-yy HH:mm}",item.ETA).ToUpper();
            txtCounts.Text = string.Format("ULDS: {0}   AWBS: {1}     PCS: {2}", item.ULDCount, item.AWBCount, item.TotalPieces);


            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);
            if (item.Flag == 0)
            {
                Indicator.Visibility = ViewStates.Gone;
            }
            else
            {
                Indicator.Visibility = ViewStates.Visible;
            }

            try 
            {
                System.IO.Stream ims = context.Assets.Open(string.Format(@"Airlines/{0}.png",item.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            switch (item.Status)             {                 case RecoverFreightStatusTypes.Pending:                     RowIcon.SetImageResource (Resource.Drawable.Pending);                     break;                 case RecoverFreightStatusTypes.Completed:                     RowIcon.SetImageResource (Resource.Drawable.Completed);                     break;                 case RecoverFreightStatusTypes.In_Progress:                     RowIcon.SetImageResource (Resource.Drawable.InProgress );                     break;                 default:                     RowIcon.SetImageResource (Resource.Drawable.Pending);                     break;             }
 

            return view;
        }

 




    }
}




