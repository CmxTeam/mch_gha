﻿ 

using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class AcceptFreight_BuldUldShipmentsActivity : BaseActivity
    {

        Awbs awbs;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.AcceptFreight_BuldUldShipments);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();
        }


        private void DoBack()
        {
            StartActivity (typeof(AcceptFreight_SealActivity));
            this.Finish();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                GoToTask(awbs.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(AwbItem task)
        {
                               var transaction = FragmentManager.BeginTransaction();                     var dialogFragment = new LocationDialogActivity(this, ShowBarcodeId, "Location","Select Location",true, LocationTypes.Area);                     dialogFragment.Cancelable = false;                     dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void ShowBarcodeId(Barcode  barcode)
        {
            if (barcode.BarcodeType != BarcodeTypes.NA)
            {
                

                Location l = Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, barcode.BarcodeSuffix);
                if (l.Transaction.Status && l.Data != null)
                {
                    Barcode b = BarcodeParser.Parse(l.Data); 
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(b.Id.ToString());
                }



            }
 
        }

        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {

                    AcceptFreightStatusTypes status = EnumHelper.GetEnumItem<AcceptFreightStatusTypes>(DropDownText.Text);

                    awbs = MCH.Communication.AcceptFreight.Instance.GetBulkUldAwbs(AcceptFreight_SessionState.CurrentAcceptFreightTask.TaskId ,status);

                    RunOnUiThread (delegate {

                        if(awbs.Transaction.Status)
                        {
                            if(awbs.Data!=null)
                            {
                                
                                awbs.Data =  LinqHelper.Query<AwbItem>(awbs.Data,searchData);
                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",awbs.Data.Count);
                                listView.Adapter = new AcceptFreight_BuldUldShipmentsAdapter(this, awbs.Data);
                                listView.ItemClick -= OnListItemClick;
                                listView.ItemClick += OnListItemClick;                          
                            }
                        }

                        if(isBarcode)
                        {
                            EditTextListener.Text = string.Empty;
                            search.RequestFocus ();
                            if( awbs.Data.Count == 1)
                            {
                                GoToTask( awbs.Data[0]);
                                return;
                            }
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }

    }
}

