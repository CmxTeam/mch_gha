﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Graphics;

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class AcceptFreight_SealActivity : BaseActivity
    {


       

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView (Resource.Layout.AcceptFreight_SealLayout);
            Initialize(ApplicationSessionState.SelectedMenuItem.Name);

            RefreshData();

        }
 
        private void OnValidateSealClick(object sender, EventArgs e)
        {
            StartActivity (typeof(AcceptFreight_ShipmentsActivity));
            this.Finish();
        }



        private void RefreshData()
        {


        }

 
        private void DoBack()
        {
            StartActivity (typeof(AcceptFreight_ActionActivity));
            this.Finish();
        }
 


    }
}


 