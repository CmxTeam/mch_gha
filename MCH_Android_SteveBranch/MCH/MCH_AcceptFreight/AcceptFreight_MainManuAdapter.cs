﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class AcceptFreight_MainManuAdapter : BaseAdapter<AcceptFreightTaskItem> {

        List<AcceptFreightTaskItem> items;
        Activity context;
        Action<int> OnImageClickAction;


        public AcceptFreight_MainManuAdapter(Activity context, List<AcceptFreightTaskItem> items, Action<int> onImageClickAction ): base()
        {

            this.OnImageClickAction = onImageClickAction;
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override AcceptFreightTaskItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.AcceptFreight_MainRow, null);


            TextView txtName = view.FindViewById<TextView>(Resource.Id.txtName);
            TextView txtCompany = view.FindViewById<TextView>(Resource.Id.txtCompany);
            TextView txtCounts = view.FindViewById<TextView>(Resource.Id.txtCounts);
            TextView txtLocations = view.FindViewById<TextView>(Resource.Id.txtLocations);
            TextView txtDate = view.FindViewById<TextView>(Resource.Id.txtDate);
            TextView txtProgress = view.FindViewById<TextView>(Resource.Id.txtProgress);
            TextView txtShipper = view.FindViewById<TextView>(Resource.Id.txtShipper);
            ProgressBar progressBar = view.FindViewById<ProgressBar>(Resource.Id.progressBar);
            ImageView personImage = view.FindViewById<ImageView>(Resource.Id.personImage);
            ImageView airlineImage= view.FindViewById<ImageView>(Resource.Id.airlineImage);

            progressBar.Progress = (int)item.Progress;
            txtName.Text = item.DriverName;
            txtCompany.Text = item.DriverCompany;
            txtCounts.Text = string.Format("AWBs: {0}, Pcs: {1}", item.Awbs, item.Pcs);
            txtLocations.Text = "Location: " + item.Locations;
            txtDate.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", item.Date);
            txtProgress.Text = string.Format("{0:0}%", item.Progress);
            txtShipper.Text = "Shipper: " + item.Shipper;

            try 
            {
                System.IO.Stream ims = context.Assets.Open(string.Format(@"Airlines/{0}.png",item.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            try
            {
                if (!string.IsNullOrEmpty(item.DriverImageThumbnail)) {

                    Byte[] image  = System.Convert.FromBase64String(item.DriverImageThumbnail);
                    personImage.SetImageBitmap (BitmapFactory.DecodeByteArray (image, 0, image.Length));
                }
                else
                {
                    personImage.SetImageResource (Resource.Drawable.person);

                }
            }
            catch
            {
                personImage.SetImageResource (Resource.Drawable.person);
            }


            personImage.Tag = position.ToString();
            personImage.Click -= OnImageClick;
            personImage.Click += OnImageClick;

            return view;
        }

        public void OnImageClick(object sender, EventArgs e) 
        {
            ImageView img = (ImageView)sender;
            OnImageClickAction.Invoke (int.Parse(img.Tag.ToString()));
        }




    }
}


