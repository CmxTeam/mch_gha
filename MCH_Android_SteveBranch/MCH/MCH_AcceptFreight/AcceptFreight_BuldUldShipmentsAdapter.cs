﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class AcceptFreight_BuldUldShipmentsAdapter : BaseAdapter<AwbItem> {

        List<AwbItem> items;
        Activity context;

        public AcceptFreight_BuldUldShipmentsAdapter(Activity context, List<AwbItem> items): base()
        {

            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override AwbItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.AcceptFreight_BuldUldShipmentRow, null);

            TextView txtUldType = view.FindViewById<TextView>(Resource.Id.txtUldType);
            TextView txtLocations = view.FindViewById<TextView>(Resource.Id.txtLocations);
            TextView txtShipmentNumber = view.FindViewById<TextView>(Resource.Id.txtShipmentNumber);
            TextView txtPieces = view.FindViewById<TextView>(Resource.Id.txtPieces);
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Indicator);

            txtShipmentNumber.Text = string.Format("{0}", item.Awb);
            txtPieces.Text = string.Format("Pcs: {0}", item.TotalPcs);
            if (item.UldType.ToLower() == "loose")
            {
                txtUldType.Text = string.Format("{0}", item.UldType);
            }
            else
            {
                txtUldType.Text = string.Format("{0}-{1}", item.UldType, item.UldNumber);
            }

            txtLocations.Text = string.Format("Locations: {0}", item.Locations);

            switch (item.Status)
            {
                case AcceptFreightStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case AcceptFreightStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case AcceptFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }


            return view;
        }


    }
}



