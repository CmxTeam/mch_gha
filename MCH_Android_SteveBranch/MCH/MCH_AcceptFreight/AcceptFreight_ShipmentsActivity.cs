﻿ 
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class AcceptFreight_ShipmentsActivity : BaseActivity
    {

        Shipments AcceptFreightShipments;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.AcceptFreight_ShipmentsLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();
        }


        private void DoBack()
        {
            StartActivity (typeof(AcceptFreight_SealActivity));
            this.Finish();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                GoToTask(AcceptFreightShipments.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(ShipmentItem task)
        {
            Action<long,long,int> PrintAction = Print;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new PrinterDialog(this,PrintAction, "Print Label", "123-123123",111,20);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        void Print(long reference, long printer, int pieces)
        {
            Toast.MakeText (this, pieces.ToString(), ToastLength.Long).Show ();
        }


        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {

                    AcceptFreightStatusTypes status = EnumHelper.GetEnumItem<AcceptFreightStatusTypes>(DropDownText.Text);
    
                    AcceptFreightShipments = MCH.Communication.AcceptFreight.Instance.GetShipments(AcceptFreight_SessionState.CurrentAcceptFreightTask.TaskId ,status);

                    RunOnUiThread (delegate {

                        if(AcceptFreightShipments.Transaction.Status)
                        {
                            if(AcceptFreightShipments.Data!=null)
                            {
                                AcceptFreightShipments.Data =  LinqHelper.Query<ShipmentItem>(AcceptFreightShipments.Data,searchData);
                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",AcceptFreightShipments.Data.Count);
                                listView.Adapter = new AcceptFreight_ShipmentsAdapter(this, AcceptFreightShipments.Data);
                                listView.ItemClick -= OnListItemClick;
                                listView.ItemClick += OnListItemClick;                          
                            }
                        }

                        if(isBarcode)
                        {
                            EditTextListener.Text = string.Empty;
                            search.RequestFocus ();
                            if( AcceptFreightShipments.Data.Count == 1)
                            {
                                GoToTask( AcceptFreightShipments.Data[0]);
                                return;
                            }
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }

    }
}

