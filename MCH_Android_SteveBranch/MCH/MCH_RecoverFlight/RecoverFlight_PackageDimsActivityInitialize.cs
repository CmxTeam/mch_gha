﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCH
{

    public partial class RecoverFlight_PackageDimsActivity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;

        //Add here new controls
 
        ArrayAdapter dimUomAdapter;
        ArrayAdapter uomAdapter;
        ArrayAdapter packageAdapter;

        TextView txtPackageNumber;
        Spinner txtPackageType;
        TextView txtWeight;
        Spinner txtUOM;
        TextView txtLength;
        TextView txtWidth;
        TextView txtHeight;
        Spinner txtDimUOM;
        Button btnSave;



        private void Initialize()
        {
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            headerText.Text = "Edit Package Dims";
            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }


            //Find here new controls

            txtPackageNumber= FindViewById<TextView> (Resource.Id.txtPackageNumber);
            txtPackageType= FindViewById<Spinner> (Resource.Id.txtPackageType);
            txtWeight= FindViewById<TextView> (Resource.Id.txtWeight);
            txtUOM= FindViewById<Spinner> (Resource.Id.txtUOM);
            txtLength= FindViewById<TextView> (Resource.Id.txtLength);
            txtWidth= FindViewById<TextView> (Resource.Id.txtWidth);
            txtHeight= FindViewById<TextView> (Resource.Id.txtHeight);
            txtDimUOM= FindViewById<Spinner> (Resource.Id.txtDimUOM);
            btnSave= FindViewById<Button> (Resource.Id.btnSave);
            btnSave.Click += OnbtnSave_Click;

     

//            packageAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.PackageType_Array, Android.Resource.Layout.SimpleSpinnerItem);
//            packageAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
//            txtPackageType.Adapter = packageAdapter;

            uomAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.UOM_Array, Android.Resource.Layout.SimpleSpinnerItem);
            uomAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
            txtUOM.Adapter = uomAdapter;
 
            dimUomAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.DimUOM_Array, Android.Resource.Layout.SimpleSpinnerItem);
            dimUomAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
            txtDimUOM.Adapter = dimUomAdapter;


        }




        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}


