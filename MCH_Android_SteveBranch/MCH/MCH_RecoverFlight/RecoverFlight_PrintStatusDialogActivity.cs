﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{
 
    public class RecoverFlight_PrintStatusDialogActivity: DialogFragment
    {
 
        Activity context;
        string title;
        string message;
        string hawb;
        int pcs;
        string result;
        bool showEdit;

        TextView txtShipmentNumber;
        TextView txtMessage;
        TextView txtPieces;
        TextView txtResult;
        Button btnEdit;
        Button btnOk;

        Action<bool> okEditAction;

        public RecoverFlight_PrintStatusDialogActivity (Activity context, Action<bool> okEditAction, string title, string message, string hawb, int pcs , string result, bool showEdit)
        {
 
            this.title = title;
            this.message = message;
            this.context = context;
            this.hawb=hawb;
            this.pcs=pcs;
            this.result=result;
            this.showEdit = showEdit;
            this.okEditAction = okEditAction;
        }
 
 
        //ADD this for error when rotating
        public RecoverFlight_PrintStatusDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.RecoverFlight_PrintStatusDialog, container, false);

            btnEdit = DialogInstance.FindViewById<Button>(Resource.Id.btnEdit);
            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk);
            btnOk.Click += OnOk_Click;

            txtShipmentNumber = DialogInstance.FindViewById<TextView>(Resource.Id.txtShipmentNumber);
            txtMessage = DialogInstance.FindViewById<TextView>(Resource.Id.txtMessage);
            txtPieces = DialogInstance.FindViewById<TextView>(Resource.Id.txtPieces);
            txtResult = DialogInstance.FindViewById<TextView>(Resource.Id.txtResult);

   
            txtMessage.Text = message;
            txtShipmentNumber.Text = "Hawb#: " + this.hawb;
            txtPieces.Text = "Pcs: " + this.pcs.ToString();
            txtResult.Text = this.result;


            if (showEdit)
            {
                btnEdit.Click += OnEdit_Click;
                btnEdit.Visibility = ViewStates.Visible;
            }
            else
            {
                btnEdit.Visibility = ViewStates.Gone;
            }

            return DialogInstance;
        }

        private void OnEdit_Click(object sender, EventArgs e)
        {
            okEditAction.Invoke (true);
            Dismiss();
        }

        private void OnOk_Click(object sender, EventArgs e)
        {
            Dismiss();
        }
 
        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }

 

    }
}


 
