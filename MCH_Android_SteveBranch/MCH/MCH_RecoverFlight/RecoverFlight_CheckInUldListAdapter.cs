﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{


    public class RecoverFlight_CheckInUldListAdapter : BaseAdapter<CheckInUldItem> {

        List<CheckInUldItem> items;
        Activity context;


        public RecoverFlight_CheckInUldListAdapter(Activity context, List<CheckInUldItem> items  ): base()
        {


            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CheckInUldItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {
 
            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFlight_CheckInUldRow, null);


            TextView RowText = view.FindViewById<TextView>(Resource.Id.RowText);
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);

            if (item.Id == 0)
            {
                RowText.Text = item.UldNumber;
                RowIcon.SetImageResource (Resource.Drawable.Menu);
            }
            else
            {
                RowText.Text = string.Format("ULD: {0} - {1}", item.UldType, item.UldNumber);
                switch (item.Status)
                {
                    case UldCheckInStatusTypes.Not_CheckedIn:
                        RowIcon.SetImageResource (Resource.Drawable.Pending);
                        break;
                    case UldCheckInStatusTypes.CheckedIn:
                        RowIcon.SetImageResource (Resource.Drawable.Completed);
                        break;
                }
            }


            return view;
        }






    }
}


