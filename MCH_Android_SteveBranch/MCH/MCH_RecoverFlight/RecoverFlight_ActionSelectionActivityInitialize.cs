﻿ 
        using System;
        using System.Collections.Generic;
        using System.Linq;
        using System.Text;
        using Android.Graphics.Drawables;
        using Android.App;
        using Android.Content;
        using Android.OS;
        using Android.Runtime;
        using Android.Views;
        using Android.Widget;
using  MCH.Communication;
        namespace MCH
        {

        public partial class RecoverFlight_ActionSelectionActivity : BaseActivity
            {
                TextView headerText; 
                ImageView optionButton;
                ImageView backButton;
                ImageView headerImage;
                LinearLayout header;

        Button RecoverButton;
        Button CustomsButton;
        Button CheckInButton;

                private void Initialize()
                {
                    headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
                    backButton = FindViewById<ImageView>(Resource.Id.BackButton);
                    optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
                    headerText = FindViewById<TextView>(Resource.Id.HeaderText);
                    header = FindViewById<LinearLayout>(Resource.Id.Header);

                    backButton.Click += OnBackButton_Click;
                    optionButton.Click += OnOptionButton_Click;

           
                    headerText.Text = ApplicationSessionState.SelectedMenuItem.Name;
                    try 
                    {
                        System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                        Drawable d = Drawable.CreateFromStream(ims, null);
                        headerImage.SetImageDrawable(d);
                    } 
                    catch 
                    {
                        //imageHeader.SetImageResource (Resource.Drawable.Icon);
                    }

    
                    //Find here new controls

            TextView Flight  = FindViewById<TextView>(Resource.Id.Flight);
            Flight.Text = GetText(Resource.String.RecoverFlight_FlightTitle) + " " + RecoverFlight_SessionState.CurrentFlightTask.Flight;

            CustomsButton  = FindViewById<Button>(Resource.Id.CustomsButton);
            CustomsButton.Click += OnCustomsButton_Click;
            RecoverButton  = FindViewById<Button>(Resource.Id.RecoverButton); 
            RecoverButton.Click += OnRecoverButton_Click;
            CheckInButton  = FindViewById<Button>(Resource.Id.CheckInButton); 
            CheckInButton.Click += OnCheckInButton_Click;
         
      
                }


        private void OnCustomsButton_Click(object sender, EventArgs e)
        {
            StartActivity (typeof(RecoverFlight_ReleaseActivity));
            this.Finish();
        }
        private void OnRecoverButton_Click(object sender, EventArgs e)
        {
//            if (RecoverFlight_SessionState.CurrentFlightTask.Status == MCH.Communication.TaskStatusTypes.Pending)
//            {
//                Action<BarcodeData> BarcodeClickAction = OnTruckLocationClickAction;
//                var transaction = FragmentManager.BeginTransaction();
//                var dialogFragment = new BarcodeDialogActivity(this, BarcodeClickAction, Resource.String.RecoverFlight_Truck_Location, Resource.String.RecoverFlight_Truck_Location_Message);
//                dialogFragment.Cancelable = false;
//                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//            }
//            else if (RecoverFlight_SessionState.CurrentFlightTask.Status == MCH.Communication.TaskStatusTypes.In_Progress)
//            {
//                StartActivity (typeof(RecoverFlight_RecoverUldActivity));
//                this.Finish();
//            }
//            else
//            {
//                MessageBox m = new MessageBox(this);
//                m.ShowAlertMessage(Resource.String.RecoverFlight_Already_Recovered_Message);
//            }


            if (RecoverFlight_SessionState.CurrentFlightTask.Status == MCH.Communication.TaskStatusTypes.Completed)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.RecoverFlight_Already_Recovered_Message);
            }
            else
            {
                if (RecoverFlight_SessionState.CurrentFlightTask.RecoverLocation == string.Empty)
                {
                    Action<Barcode> BarcodeClickAction = OnTruckLocationClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new LocationDialogActivity(this, BarcodeClickAction, Resource.String.RecoverFlight_Truck_Location, Resource.String.RecoverFlight_Truck_Location_Message,true, LocationTypes.Truck);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                }
                else
                {
                    StartActivity (typeof(RecoverFlight_RecoverUldActivity));
                    this.Finish();
                }
            }



        }
        private void OnCheckInButton_Click(object sender, EventArgs e)
        {
//            if (RecoverFlight_SessionState.CurrentFlightTask.Status == MCH.Communication.TaskStatusTypes.Pending )
//            {
//            Action<BarcodeData> BarcodeClickAction = OnStageLocationClickAction;
//            var transaction = FragmentManager.BeginTransaction();
//            var dialogFragment = new BarcodeDialogActivity(this,BarcodeClickAction, Resource.String.RecoverFlight_Stage_Location, Resource.String.RecoverFlight_Stage_Location_Message);
//            dialogFragment.Cancelable = false;
//            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//            }
//            else if(RecoverFlight_SessionState.CurrentFlightTask.Status == MCH.Communication.TaskStatusTypes.In_Progress)
//            {
//                StartActivity (typeof(RecoverFlight_CheckInUldActivity));
//                this.Finish();
//            }
//            else
//            {
//                MessageBox m = new MessageBox(this);
//                m.ShowAlertMessage(Resource.String.RecoverFlight_Already_Recovered_Message);
//            }


            if (RecoverFlight_SessionState.CurrentFlightTask.Status == MCH.Communication.TaskStatusTypes.Completed)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.RecoverFlight_Already_Recovered_Message);
            }
            else
            {
                if (RecoverFlight_SessionState.CurrentFlightTask.StageLocation == string.Empty)
                {
                    Action<Barcode> BarcodeClickAction = OnStageLocationClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new LocationDialogActivity(this,BarcodeClickAction, Resource.String.RecoverFlight_Stage_Location, Resource.String.RecoverFlight_Stage_Location_Message,true,LocationTypes.Area,LocationTypes.ScreeningArea);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                }
                else
                {
                    StartActivity (typeof(RecoverFlight_CheckInUldActivity));
                    this.Finish();
                }
            }


        }
 
        private void OnStageLocationClickAction(Barcode  barcode)
        {
            //CommunicationTransaction t =  MCH.Communication.RecoverFlight.Instance.Stage(RecoverFlight_SessionState.CurrentFlightTask.FlightId, barcode.Location,ApplicationSessionState.User.Data.UserId);
 
            CommunicationTransaction t;
            if (barcode.BarcodeType == BarcodeTypes.Area)
            {
                t = MCH.Communication.RecoverFlight.Instance.Stage(RecoverFlight_SessionState.CurrentFlightTask.FlightId, barcode.Location, ApplicationSessionState.User.Data.UserId);

            }
            else
            {
                t = MCH.Communication.RecoverFlight.Instance.Stage(RecoverFlight_SessionState.CurrentFlightTask.FlightId, barcode.BarcodeText, ApplicationSessionState.User.Data.UserId);

            }



            if (t.Status)
            {
                StartActivity (typeof(RecoverFlight_CheckInUldActivity));
                this.Finish();
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(t.Error);
            }

        }

        private void OnTruckLocationClickAction(Barcode  barcode)
        {
            CommunicationTransaction t;
            if (barcode.BarcodeType == BarcodeTypes.Truck)
            {
                t = MCH.Communication.RecoverFlight.Instance.Recover(RecoverFlight_SessionState.CurrentFlightTask.FlightId, barcode.Location, ApplicationSessionState.User.Data.UserId);

            }
            else
            {
                t = MCH.Communication.RecoverFlight.Instance.Recover(RecoverFlight_SessionState.CurrentFlightTask.FlightId, barcode.BarcodeText, ApplicationSessionState.User.Data.UserId);

            }

 
            if (t.Status)
            {
                StartActivity (typeof(RecoverFlight_RecoverUldActivity));
                this.Finish();
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(t.Error);
            }


        

        }

                private void OnBackButton_Click(object sender, EventArgs e)
                {
                    DoBack();
                }

                private void OnOptionButton_Click(object sender, EventArgs e)
                {
                    List<OptionItem> options = new List<OptionItem>();
                    options.Add(new OptionItem(GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
                    options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
                    options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
                    options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

                    Action<OptionItem> OptionClickAction = OnOptionClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new OptionDialog(this, options, OptionClickAction);
                    dialogFragment.Cancelable = true;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                }

                private void OnOptionClickAction(OptionItem  option)
                {
                    switch (option.OptionAction)
                    {
                        case  OptionActions.Logout:
                            ConfirmLogOut();
                            break;
                        case  OptionActions.Exit:
                            ConfirmExit();
                            break;
                        case  OptionActions.MainMenu:
                            GotoMainMenu();
                            break;
                        case  OptionActions.Camera:
                    GoToCamera(typeof( RecoverFlight_ActionSelectionActivity),typeof( RecoverFlight_ActionSelectionActivity ),RecoverFlight_SessionState.CurrentFlightTask.TaskId);
                            break;
                            }

                }


                public override void OnBackPressed ()
                {
                    DoBack();
                }

                protected override void OnRestart()
                {
                    //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
                    // Refresh Data Here
                    base.OnRestart ();
                }

                protected override void OnStart()
                {
                    //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

                    base.OnStart();
                }
                protected override void OnResume()
                {

                    //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

                    base.OnResume();
                }
                protected override void OnPause()
                {
                    //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
                    base.OnPause();
                }
                protected override void OnStop()
                {
                    //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
                    base.OnStop();
                }
                protected override void OnDestroy ()
                {
                    base.OnDestroy ();
                    //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
                }
            }
        }


