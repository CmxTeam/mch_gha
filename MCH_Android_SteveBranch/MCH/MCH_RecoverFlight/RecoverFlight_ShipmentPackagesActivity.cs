﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_ShipmentPackagesActivity : BaseActivity
    {

        Packages packages;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_ShipmentPackagesLayout);
            Initialize();

            RefreshData();
        }


        private void DoBack()
        {
            StartActivity (typeof(RecoverFlight_ShipmentDetailActivity));
            this.Finish();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
           
            try
            {
                GoToTask(packages.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(PackageItem task)
        {
 
            RecoverFlight_SessionState.CurrentPackageTask = task;
            RecoverFlight_SessionState.CurrentPackageDim =  RecoverFlight_SessionState.CurrentPackageTask;
            StartActivity (typeof(RecoverFlight_PackageDimsActivity));
            this.Finish(); 


        }

 

        private void RefreshData()
        {
        
            txtShipmentNumber.Text = "Hawb#: " + RecoverFlight_SessionState.CurrentPackageTask.Hawb;

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {

                     
                    packages =    MCH.Communication.RecoverFlight.Instance.GetPackagesByHawbId(RecoverFlight_SessionState.CurrentPackageTask.HawbId);


           
                    RunOnUiThread (delegate {

                        if(packages.Transaction.Status && packages.Data!=null)
                        {

                             
                            titleLabel.Text = string.Format("Edit Packages ({0})",packages.Data.Count);
                            listView.Adapter = new RecoverFlight_PackageListAdapter(this, packages.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;  

                        }

 

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }

    }
}





 