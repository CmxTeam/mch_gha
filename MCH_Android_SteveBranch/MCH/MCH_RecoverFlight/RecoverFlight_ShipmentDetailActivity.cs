﻿ 

using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_ShipmentDetailActivity : BaseActivity
    {
        ServiceTypes serviceTypes;
        Packages pks;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_ShipmentDetailLayout);
            Initialize();


            LoadData();

           
        }

        private int GetServiceTypeId(string serviceType)
        {
            foreach (var s in serviceTypes.Data)
            {
                if (s.ServiceType.ToLower() == serviceType.ToLower())
                {
                    return s.ServiceTypeId;
                }
            }
            return 0;
        }

        private string GetServiceType(int serviceTypeId)
        {
            foreach (var s in serviceTypes.Data)
            {
                if (s.ServiceTypeId == serviceTypeId)
                {
                    return s.ServiceType;
                }
            }
            return string.Empty;
        }


        private void LoadData()
        {

            serviceTypes = MCH.Communication.RecoverFlight.Instance.GetServiceTypes(RecoverFlight_SessionState.CurrentPackageTask.HawbId);
            if (!serviceTypes.Transaction.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(serviceTypes.Transaction.Error);
            }
            else
            {

                serviceAdapter = new ArrayAdapter(this,Android.Resource.Layout.SimpleSpinnerItem);
                foreach (var s in serviceTypes.Data)
                {
                    serviceAdapter.Add(s.ServiceType);
                }
 
                txtService.Adapter = serviceAdapter;
            }



            ShipmentDetail detail =   MCH.Communication.RecoverFlight.Instance.GetShipmentDetail(RecoverFlight_SessionState.CurrentPackageTask.HawbId);
            if (detail.Transaction.Status)
            {
                txtShipmentNumber.Text = "Hawb#: " + RecoverFlight_SessionState.CurrentPackageTask.Hawb;
                txtName.Text = detail.Name;
                txtCity.Text = detail.City;
                txtPostalCode.Text = detail.PostalCode;
                txtAddress1.Text = detail.Address1;
                txtAddress2.Text = detail.Address2;
                txtContactName.Text = detail.ContactName;
                txtContactPhone.Text = detail.ContactPhone;


                txtCountry.SetSelection(GetPosition(countryAdapter, detail.Country));
                txtState.SetSelection(GetPosition(stateAdapter, detail.State));

                string serviceType =GetServiceType(detail.ServiceTypeId);
                txtService.SetSelection(GetPosition(serviceAdapter, serviceType));
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(detail.Transaction.Error);
            }


             
 
        }


        private int GetPosition(ArrayAdapter adapter, string value)
        {
            if (value != null)
            {
                for(int i=0 ; i<adapter.Count ; i++){
                    var obj = adapter.GetItem(i);
                    if (obj.ToString().ToUpper() == value.ToUpper())
                    {
                        return i;
                    }
                }
            }


            return 0;
        }

        private void DoBack()
        {
            StartActivity (typeof(RecoverFlight_PackageActivity));
            this.Finish();
        }


        private void OnbtnPrint_Click(object sender, EventArgs e)
        {


 
//                    var transaction = FragmentManager.BeginTransaction();
//                    var dialogFragment = new RecoverFlight_PrintStatusDialogActivity(this, GetText(Resource.String.Print_Status), GetText(Resource.String.Print_Successful),"1111111",2,"Tracking Numbers:\nAAAAA\nBBBBBB\nCCCCC");
//                    dialogFragment.Cancelable = false;
//                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//                    return;




          ShipmentStatus status = MCH.Communication.RecoverFlight.Instance.GetShipmentStatus(RecoverFlight_SessionState.CurrentPackageTask.HawbId);
            if (status.Transaction.Status)
            {

                if(status.Data.IsHawbReady == true && status.Data.ShipmentCreated  == false)
                {
                    Generate(RecoverFlight_SessionState.CurrentPackageTask.HawbId);
                }
                else if(status.Data.IsHawbReady == false && status.Data.ShipmentCreated  == true)
                {
                    Print(RecoverFlight_SessionState.CurrentPackageTask.HawbId);
                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(Resource.String.RecoverFlight_Housebill_Not_Print_Ready);
                }

            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(status.Transaction.Error);
            }
          
        }

 
        private void Print(int hawbId)
        {

            List<MCH.Communication.SelectionItem> packages = new List<MCH.Communication.SelectionItem>();
            pks =    MCH.Communication.RecoverFlight.Instance.GetPackagesByHawbId(RecoverFlight_SessionState.CurrentPackageTask.HawbId);

            if (pks.Transaction.Status)
            {
               
                int total = pks.Data.Count;
                int counter = 0;

                foreach (var pk in pks.Data)
                {
  
                    CommunicationTransaction t = MCH.Communication.RecoverFlight.Instance.PrintLabel(pk.Id, ApplicationSessionState.User.Data.UserId);
                    if (!t.Status)
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowMessage(pks.Transaction.Error);
                    }
                    else
                    {
                        counter++;
                    }

                }

                if (counter == total)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(GetText(Resource.String.RecoverFlight_Printed) + ". Total Packages: " + counter.ToString());
                }
 
 
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(pks.Transaction.Error);
            }
        }

        private void Generate(int hawbId)
        {



            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.RecoverFlight_Creating_Shipment), true);
            new Thread(new ThreadStart(delegate
                {

                    Thread.Sleep(1000);


                    PrintStatus result = MCH.Communication.RecoverFlight.Instance.GenerateShipment(hawbId,ApplicationSessionState.User.Data.UserId,RecoverFlight_SessionState.CurrentFlightTask.FlightId);
//                    if(!result.Transaction.Status)
//                    {
//                        RunOnUiThread (delegate {
//                            OnPrintingError(result.Transaction.Error);
//                        });
//                    }
                    RunOnUiThread (delegate {
                        OnPrinted(result);
                     });



                    RunOnUiThread (delegate {
                        LoadData();
                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }


        private void OnPrinted(PrintStatus result)
        {
            if (!result.Transaction.Status)
            {
//                        RunOnUiThread (delegate {
//                            OnPrintingError(result.Transaction.Error);
//                        });
                        
                Action<bool> EditClickAction = OnEditClickAction;
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new RecoverFlight_PrintStatusDialogActivity(this, EditClickAction, GetText(Resource.String.RecoverFlight_Print_Status), GetText(Resource.String.RecoverFlight_Shipment_Printing_Failed), RecoverFlight_SessionState.CurrentPackageTask.Hawb, RecoverFlight_SessionState.CurrentPackageTask.Pcs, "Error:\n" + result.Transaction.Error, true);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;

            }
            else
            {

                string resulttext = "Tracking Numbers:";
                foreach (string n in result.Data.ShipmentNumbers)
                {
                    resulttext +=  "\n" + n;
                }
                        
                        Action<bool> EditClickAction = OnEditClickAction;
                        var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new RecoverFlight_PrintStatusDialogActivity(this,EditClickAction, GetText(Resource.String.RecoverFlight_Print_Status), GetText(Resource.String.RecoverFlight_Print_Successful),RecoverFlight_SessionState.CurrentPackageTask .Hawb,RecoverFlight_SessionState.CurrentPackageTask.Pcs,resulttext,false);
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                        return;

                    }
        }


        private void OnEditClickAction(bool  action)
        {
            if (action)
            {
                StartActivity (typeof(RecoverFlight_ShipmentDetailActivity));
            this.Finish();
            }


}

//        private void OnPrintingError(string error)
//        {
//            MessageBox m = new MessageBox(this);
//            m.ShowAlertMessage(GetText(Resource.String.RecoverFlight_Shipment_Printing_Failed) + ". " + error);
// 
//        }


        private void EditShipment()
        {
            StartActivity (typeof(RecoverFlight_ShipmentDetailActivity));
            this.Finish();
        }

        private void OnbtnPackages_Click(object sender, EventArgs e)
        {

            StartActivity (typeof(RecoverFlight_ShipmentPackagesActivity));
            this.Finish();
            return;


//            List<MCH.Communication.SelectionItem> packages = new List<MCH.Communication.SelectionItem>();
//            pks =    MCH.Communication.RecoverFlight.Instance.GetPackagesByHawbId(RecoverFlight_SessionState.CurrentPackageTask.HawbId);
//
//            if (pks.Transaction.Status)
//            {
//                int counter = 0;
//                foreach (var pk in pks.Data)
//                {
//
//                    packages.Add(new SelectionItem(pk.PackageNumber, counter, Resource.Drawable.Menu));
//                    counter++;
//                }
//
//
//
//                Action<List<MCH.Communication.SelectionItem>> SelectionAction = OnSelectionAction;
//                var transaction = FragmentManager.BeginTransaction();
//                var dialogFragment = new SelectionDialogActivity("Packages", this, packages, SelectionAction, false, false);
//                dialogFragment.Cancelable = false;
//                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//                return;
//            }
//            else
//            {
//                MessageBox m = new MessageBox(this);
//                m.ShowAlertMessage(pks.Transaction.Error);
//            }


        }


        private void OnSelectionAction(List<MCH.Communication.SelectionItem>  result)
        {
            foreach (var r in result)
            {
  
                RecoverFlight_SessionState.CurrentPackageDim = pks.Data[(int)r.Id];
                StartActivity (typeof(RecoverFlight_PackageDimsActivity));
                this.Finish(); 
            }
             
        }

        private void OnbtnSave_Click(object sender, EventArgs e)
        {

            if (RecoverFlight_SessionState.CurrentPackageTask.ShipmentNumber != string.Empty)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.RecoverFlight_Shipment_Has_Been_Generated);
                return;
            }

            try
            {

                ShipmentDetail detail = new ShipmentDetail();
                detail.Name = txtName.Text;
                detail.Country = txtCountry.SelectedItem.ToString(); 
                detail.State = txtState.SelectedItem.ToString();  
                detail.City  =  txtCity.Text ;
                detail.PostalCode = txtPostalCode.Text ;
                detail.Address1  = txtAddress1.Text;
                detail.Address2  = txtAddress2.Text ;
                detail.ContactName  = txtContactName.Text;
                detail.ContactPhone  = txtContactPhone.Text;
                detail.ServiceTypeId = GetServiceTypeId(txtService.SelectedItem.ToString()); 
   
                CommunicationTransaction transaction =   MCH.Communication.RecoverFlight.Instance.UpdateShipmentDetail(RecoverFlight_SessionState.CurrentPackageTask.HawbId,ApplicationSessionState.User.Data.UserId,RecoverFlight_SessionState.CurrentFlightTask.FlightId,detail); 
                if(transaction.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(Resource.String.Data_Successfully_Saved);

                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(GetText(Resource.String.Error_Saving_Data).ToString() + ". " + transaction.Error.ToString());
                }


   



            }
            catch
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.Error_Saving_Data);
            }
           
        }

        public void ShowPrinters()
        {
            List< MCH.Communication.SelectionItem> printersList = new List< MCH.Communication.SelectionItem>();
            MCH.Communication.Printers printers = MCH.Communication.Miscellaneous.Instance.GetPrinters(ApplicationSessionState.User.Data.UserId, MCH.Communication.PrinterTypes.Label);
            if (printers.Transaction.Status)
            {
 
     
            if (printers.Data != null)
            {

                foreach (var printer in printers.Data)
                {
                    printersList.Add(new MCH.Communication.SelectionItem(printer.PrinterName, printer.PrinterId, @"Icons/Printer", printer.IsDefault));
                }

                bool allowSearch = (printersList.Count > 10) ? true : false;
                Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnPrinterClickSelectionAction;
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Printers),this,printersList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection,allowSearch);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }

            }


        }

        private void OnPrinterClickSelectionAction(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            
            foreach (var r in result)
            {
                MCH.Communication.Miscellaneous.Instance.SetUserDefault(ApplicationSessionState.User.Data.UserId,(int)r.Id);
            }
    
        }

    }
}


 