﻿ 


using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_ActionSelectionActivity : BaseActivity
    {
 
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_ActionSelectionLayout);
            Initialize();
            LoadData();
 
        }

        private void LoadData()
        {
 
            RecoverFlightTasks t= MCH.Communication.RecoverFlight.Instance.GetRecoverFlightTask( RecoverFlight_SessionState.CurrentFlightTask.FlightId);

 
            if (t.Transaction.Status == false)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(t.Transaction.Error);
                return;
            }
            else
            {
               
 
                Drawable ValidateWhite = Application.Context.Resources.GetDrawable(Resource.Drawable.ValidateWhite);  
                Drawable NotValidatedWhite = Application.Context.Resources.GetDrawable(Resource.Drawable.NotValidatedWhite);  

                if (t.Data[0].ReleaseType == RecoverFlightReleaseTypes.Clear)
                {
                    RecoverButton.Visibility = ViewStates.Visible;
                    CustomsButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ValidateWhite, 0, 0, 0);
                }
                else
                {
                    RecoverButton.Visibility = ViewStates.Gone;
                    CustomsButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.NotValidatedWhite, 0, 0, 0);
                }

                if (t.Data[0].RecoverLocation != string.Empty)
                {
                    RecoverButton.SetCompoundDrawablesWithIntrinsicBounds(ValidateWhite, null, null, null);
                    CheckInButton.Visibility = ViewStates.Visible;
                }
                else
                {
                    RecoverButton.SetCompoundDrawablesWithIntrinsicBounds(NotValidatedWhite, null, null, null);
                    CheckInButton.Visibility = ViewStates.Gone;
                }

                if (t.Data[0].Status == TaskStatusTypes.Completed)
                {
                    CheckInButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ValidateWhite, 0, 0, 0);
                }
                else
                {
                    CheckInButton.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.NotValidatedWhite, 0, 0, 0);
                }

            }

  
        }


        private void DoBack()
        {
            StartActivity (typeof(RecoverFlight_MainManuActivity));
            this.Finish();
        }
 
        

    }
}

