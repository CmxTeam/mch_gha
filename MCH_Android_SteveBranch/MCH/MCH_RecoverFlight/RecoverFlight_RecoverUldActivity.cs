﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_RecoverUldActivity : BaseActivity
    {
 
        RecoverFlightUlds recoverFlightUlds;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_RecoverUldLayout);
            Initialize();
           
            RefreshData(string.Empty,false);
        }


        private void DoBack()
        {
            StartActivity (typeof(RecoverFlight_ActionSelectionActivity));
            this.Finish();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
           
            try
            {
                GoToTask(recoverFlightUlds.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(RecoverFlightUldItem task)
        {

            if (task.Status == RecoverStatusTypes.Not_Recovered)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                            MCH.Communication.RecoverFlight.Instance.RecoverUld(RecoverFlight_SessionState.CurrentFlightTask.FlightId,task.Id,ApplicationSessionState.User.Data.UserId);
                            search.Text = string.Empty;
                            RefreshData(string.Empty,false);
                    }
                    else
                    {
                        search.Text = string.Empty;
                        RefreshData(string.Empty,false);
                    }
                };
                m.ShowConfirmationMessage(Resource.String.RecoverFlight_ULD_Recover_Confirmation, Resource.String.Yes, Resource.String.No);
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.RecoverFlight_ULD_Already_Recovered_Message);
                search.Text = string.Empty;
                RefreshData(string.Empty,false);
            }

 

        }

        private void LoadData()
        {

            RecoverFlightTasks t= MCH.Communication.RecoverFlight.Instance.GetRecoverFlightTask( RecoverFlight_SessionState.CurrentFlightTask.FlightId);


       
            if (t.Transaction.Status == false)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(t.Transaction.Error);
                return;
            }
            else
            {

                try
                {
                    RecoverFlight_SessionState.CurrentFlightTask = t.Data[0];
                }
                catch
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(t.Transaction.Error);
                    return;
                }
     
            }


            Flight.Text = string.Format("Flight: {0}", RecoverFlight_SessionState.CurrentFlightTask.Flight);
            Eta.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", RecoverFlight_SessionState.CurrentFlightTask.ETA);
            Reference.Text = string.Format("Reference: {0}", RecoverFlight_SessionState.CurrentFlightTask.Reference);
            Counters.Text = string.Format("Ulds: {0} of {1} Pcs: {2} of {3}", RecoverFlight_SessionState.CurrentFlightTask.RecoveredUlds,RecoverFlight_SessionState.CurrentFlightTask.Ulds,RecoverFlight_SessionState.CurrentFlightTask.CheckedInPcs,RecoverFlight_SessionState.CurrentFlightTask.Pcs);
           

           // Counters.Text = string.Format("Ulds: {0} Pcs: {1}", RecoverFlight_SessionState.CurrentFlightTask,RecoverFlight_SessionState.CurrentFlightTask.Ulds,  RecoverFlight_SessionState.CurrentFlightTask.RecoveredUlds,RecoverFlight_SessionState.CurrentFlightTask.Pcs);
            StatusDescription.Text =  string.Format("Status: {0}", RecoverFlight_SessionState.CurrentFlightTask.StatusDescription);

            Progress.Text = string.Format("{0:0}%", RecoverFlight_SessionState.CurrentFlightTask.Progress);
            ProgressBar.Progress = (int)RecoverFlight_SessionState.CurrentFlightTask.Progress;

            switch (RecoverFlight_SessionState.CurrentFlightTask.Status)
            {
                case TaskStatusTypes.Pending:
                    Status.SetImageResource (Resource.Drawable.Pending);
                    break;
                case TaskStatusTypes.In_Progress:
                    Status.SetImageResource (Resource.Drawable.InProgress);
                    break;
                case TaskStatusTypes.Completed:
                    Status.SetImageResource (Resource.Drawable.Completed);
                    break;
            }
        }

        private void RefreshData(string searchData,bool isBarcode)
        {
            LoadData();
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {


                    RecoverStatusTypes en = (RecoverStatusTypes)Enum.Parse(typeof(RecoverStatusTypes), DropDownText.Text.Replace(" ","_"));

                    recoverFlightUlds = MCH.Communication.RecoverFlight.Instance.GetRecoverUlds(RecoverFlight_SessionState.CurrentFlightTask.FlightId,en);

                    RunOnUiThread (delegate {


                       


                        if(recoverFlightUlds.Transaction.Status && recoverFlightUlds.Data!=null)
                        {

                            if(searchData!=string.Empty)
                            {
                                List<RecoverFlightUldItem> taskList = new List<RecoverFlightUldItem>();
                                foreach (var task in recoverFlightUlds.Data)
                                {
                                    if (task.ToString().ToLower().Contains(searchData.ToLower()))
                                    {
                                        taskList.Add(task);   
                                    }
                                }
                                recoverFlightUlds.Data =taskList;
                            }

                            if( recoverFlightUlds.Data.Count == 1 && isBarcode)
                            {
                                GoToTask( recoverFlightUlds.Data[0]);
                            }

                            //titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - Recover ({0})",recoverFlightUlds.Data.Count);
                            titleLabel.Text = string.Format("Recover ({0})",recoverFlightUlds.Data.Count);
                            listView.Adapter = new RecoverFlight_RecoverUldListAdapter(this, recoverFlightUlds.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;  

                        }

                        if(isBarcode)
                        {
                            search.Text = string.Empty;
                        }
                        search.RequestFocus ();

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }

    }
}


 