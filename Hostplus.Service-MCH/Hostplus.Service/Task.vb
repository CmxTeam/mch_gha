Imports ParameterObj
Public Enum Statuses
    Pending
    InProgress
    Completed
    Failed
    Cancelled
End Enum

Public Class Task

    Private _Id As Integer
    Private _SessionName As String
    Private _TaskName As String
    Private _TaskId As Integer
    Private _Parameter As Parameters
    Private _Trace As String
    Private _Progress As String
    Private _Timer As Double
    Private _TimerType As String
    Private _TimerWeekDays As String
    Private _NextDate As Date
    Private _LastDate As Date
    Private _Status As Statuses
    Private _ErrorMessage As String
    Private _SessionId As Integer
    Private _IsTraceable As Boolean
    Private _Connection As String

    Private _GlobalConnection As String
    Private _Email As String
    Private _ErrorStackTrace As String

    Private _LocationId As Integer
    Private _Location As String
    Private _TaskType As String

    Private _maxRetries As New Integer?
    Private _retries As New Integer?

    Public Property Location() As String
        Get
            Return _Location
        End Get
        Set(ByVal value As String)
            _Location = value
        End Set
    End Property

    Public Property TimerType() As String
        Get
            Return _TimerType
        End Get
        Set(ByVal value As String)
            _TimerType = value
        End Set
    End Property
    Public Property TimerWeekDays() As String
        Get
            Return _TimerWeekDays
        End Get
        Set(ByVal value As String)
            _TimerWeekDays = value
        End Set
    End Property

    Public Property TaskType() As String
        Get
            Return _TaskType
        End Get
        Set(ByVal value As String)
            _TaskType = value
        End Set
    End Property
    Public Property LocationId() As Integer
        Get
            Return _LocationId
        End Get
        Set(ByVal value As Integer)
            _LocationId = value
        End Set
    End Property

    Public Property Connection() As String
        Get
            Return _Connection
        End Get
        Set(ByVal value As String)
            _Connection = value
        End Set
    End Property

    Public Property GlobalConnection() As String
        Get
            Return _GlobalConnection
        End Get
        Set(ByVal value As String)
            _GlobalConnection = value
        End Set
    End Property

    Public Property IsTraceable() As Boolean
        Get
            Return _IsTraceable
        End Get
        Set(ByVal value As Boolean)
            _IsTraceable = value
        End Set
    End Property

    Public Property Progress() As String
        Get
            Return _Progress
        End Get
        Set(ByVal value As String)
            _Progress = value
        End Set
    End Property

    Public Property SessionId() As Integer
        Get
            Return _SessionId
        End Get
        Set(ByVal value As Integer)
            _SessionId = value
        End Set
    End Property
    Public Property ErrorMessage() As String
        Get
            Return _ErrorMessage
        End Get
        Set(ByVal value As String)
            _ErrorMessage = value
        End Set
    End Property

    Public Property SessionName() As String
        Get
            Return _SessionName
        End Get
        Set(ByVal value As String)
            _SessionName = value
        End Set
    End Property

    Public Property Status() As Statuses
        Get
            Return _Status
        End Get
        Set(ByVal value As Statuses)
            _Status = value
        End Set
    End Property

    Public Property Timer() As Double
        Get
            Return _Timer
        End Get
        Set(ByVal value As Double)
            _Timer = value
        End Set
    End Property
    Public Property LastDate() As Date
        Get
            Return _LastDate
        End Get
        Set(ByVal value As Date)
            _LastDate = value
        End Set
    End Property

    Public Property NextDate() As Date
        Get
            Return _NextDate
        End Get
        Set(ByVal value As Date)
            _NextDate = value
        End Set
    End Property

    Public Property Trace() As String
        Get
            If _IsTraceable = True Then
                Return _Trace
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            _Trace = value
        End Set
    End Property

    Public Property Parameter() As Parameters
        Get
            Return _Parameter
        End Get
        Set(ByVal value As Parameters)
            _Parameter = value
        End Set
    End Property

    Public Property TaskId() As Integer
        Get
            Return _TaskId
        End Get
        Set(ByVal value As Integer)
            _TaskId = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Public Property TaskName() As String
        Get
            Return _TaskName
        End Get
        Set(ByVal value As String)
            _TaskName = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _Email
        End Get
        Set(value As String)
            _Email = value
        End Set
    End Property

    Public Property ErrorStackTrace As String
        Get
            Return _ErrorStackTrace
        End Get
        Set(value As String)
            _ErrorStackTrace = value
        End Set
    End Property

    Public Property Retries() As Integer?
        Get
            Return _retries
        End Get
        Set(ByVal value As Integer?)
            _retries = value
        End Set
    End Property

    Public Property MaxRetries() As Integer?
        Get
            Return _maxRetries
        End Get
        Set(ByVal value As Integer?)
            _maxRetries = value
        End Set
    End Property

    Public Sub New(ByVal IdVal As Integer)
        _Id = IdVal
        Try
            Dim dt As DataTable
            Using com As New SqlClient.SqlCommand("SELECT   HostPlus_SessionTasks.LocationId,  HostPlus_SessionTasks.IsTraceable, HostPlus_SessionTasks.Timer, HostPlus_SessionTasks.TimerWeekDays, HostPlus_SessionTasks.TimerType, HostPlus_SessionTasks.NextDate, Hostplus_SessionTasks.Retries, HostPlus_Tasks.RetriesOnFail as MaxRetries, HostPlus_Tasks.Name AS TaskName,  HostPlus_Sessions.Name AS SessionName, HostPlus_SessionTasks.RecID AS TaskId, HostPlus_Sessions.RecId AS SessionId, HostPlus_SessionTypes.Name AS TaskType, HostPlus_Locations.Location, HostPlus_Locations.Connection FROM  HostPlus_SessionTasks INNER JOIN HostPlus_Tasks ON HostPlus_SessionTasks.TaskId = HostPlus_Tasks.RecId INNER JOIN HostPlus_Sessions ON HostPlus_SessionTasks.SessionId = HostPlus_Sessions.RecId INNER JOIN  HostPlus_SessionTypes ON HostPlus_Sessions.SessionTypeId = HostPlus_SessionTypes.RecId INNER JOIN HostPlus_Locations ON HostPlus_SessionTasks.LocationId = HostPlus_Locations.RecId WHERE     (HostPlus_SessionTasks.RecID = @RecID)", New SqlClient.SqlConnection(Settings.Connection))
                com.Parameters.Add("@RecID", SqlDbType.Int).Value = _Id
                Using da As New SqlClient.SqlDataAdapter(com)
                    dt = New DataTable : da.Fill(dt)
                End Using
            End Using
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    _Timer = dr.Item("Timer")
                    _TaskName = dr.Item("TaskName").ToString
                    _SessionName = dr.Item("SessionName").ToString
                    _SessionId = dr.Item("SessionId")
                    _TaskId = dr.Item("TaskId")
                    _Progress = ""
                    _Trace = ""
                    _ErrorMessage = ""
                    _NextDate = If(dr.Item("NextDate"), DateTime.Now)
                    _LastDate = Now
                    _Status = Statuses.Pending
                    _IsTraceable = dr.Item("IsTraceable")
                    _Connection = dr.Item("Connection").ToString

                    _GlobalConnection = Settings.Connection

                    _LocationId = dr.Item("LocationId").ToString
                    _Location = dr.Item("Location").ToString
                    _TaskType = dr.Item("TaskType")
                    _TimerWeekDays = dr.Item("TimerWeekDays").ToString
                    _TimerType = dr.Item("TimerType").ToString
                    _Parameter = New Parameters()

                    Dim dtParams As DataTable
                    Using com As New SqlClient.SqlCommand("SELECT     Name, Value FROM         HostPlus_SessionTaskParameters WHERE     (SessionTaskId = @TaskId)", New SqlClient.SqlConnection(Settings.Connection))
                        com.Parameters.Add("@TaskId", SqlDbType.Int).Value = _TaskId
                        Using da As New SqlClient.SqlDataAdapter(com)
                            dtParams = New DataTable : da.Fill(dtParams)
                        End Using
                    End Using
                    For Each drParams As DataRow In dtParams.Rows
                        _Parameter.Add(drParams.Item("Name").ToString, drParams.Item("Value").ToString)
                        If (drParams.Item("Name").ToString = "EMAIL") Then
                            _Email = drParams.Item("Value").ToString
                        End If
                    Next
                    Using com As New SqlClient.SqlCommand("SELECT     Name, Value FROM         HostPlus_SessionParameters  WHERE     (SessionId = @SessionId)", New SqlClient.SqlConnection(Settings.Connection))
                        com.Parameters.Add("@SessionId", SqlDbType.Int).Value = _SessionId
                        Using da As New SqlClient.SqlDataAdapter(com)
                            dtParams = New DataTable : da.Fill(dtParams)
                        End Using
                    End Using
                    For Each drParams As DataRow In dtParams.Rows
                        _Parameter.Add(drParams.Item("Name").ToString, drParams.Item("Value").ToString)
                        If (drParams.Item("Name").ToString = "EMAIL") Then
                            _Email = drParams.Item("Value").ToString
                        End If
                    Next
                Next
            Else
                _Parameter = New Parameters()
                _Timer = 1
                _TaskName = "UNKNOWN #" & _Id
                _SessionName = "UNKNOWN"
                _SessionId = 0
                _TaskId = 0
                _Progress = ""
                _Trace = ""
                _NextDate = Now
                _LastDate = Now
                _Status = Statuses.Pending
                _IsTraceable = False
                _Connection = ""
                _Location = ""
                _LocationId = 0
                _TaskType = ""
                _TimerWeekDays = ""
                _TimerType = ""
                _ErrorMessage = ""
                Settings.Log("Task #" & _Id & " not found! (" & Settings.Server & ")")
            End If
        Catch ex As Exception
            Throw New ApplicationException(Me.GetType.ToString & ". " & Reflection.MethodInfo.GetCurrentMethod.Name & ". Error creating task. (" & _Id & ")" & ex.Message)
        End Try

    End Sub

End Class
