Imports System.Linq

Public Class Service

    Private WithEvents _worker As System.ComponentModel.BackgroundWorker
    Private _Sessions As New List(Of Session)

    Private WithEvents currentDomain As AppDomain = AppDomain.CurrentDomain
    Private Sub CurrentDomain_UnhandledException(ByVal sender As Object, ByVal e As System.UnhandledExceptionEventArgs)
        Diagnostics.EventLog.WriteEntry("HostPlus", "Service Failed. Error: " & e.ExceptionObject.Message, EventLogEntryType.Error)
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            AddHandler currentDomain.UnhandledException, AddressOf CurrentDomain_UnhandledException
            Diagnostics.EventLog.WriteEntry("HostPlus", "Service Starting...", EventLogEntryType.Information)
            Settings.Log("Service Starting... (" & Settings.Server & ")")
            _worker = New System.ComponentModel.BackgroundWorker
            _worker.WorkerSupportsCancellation = True
            _worker.RunWorkerAsync()
            Settings.Log("Service Started. (" & Settings.Server & ")")
            Diagnostics.EventLog.WriteEntry("HostPlus", "Service Started.", EventLogEntryType.Information)
        Catch ex As Exception
            Diagnostics.EventLog.WriteEntry("HostPlus", "Service Failed. Error: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub


    Protected Overrides Sub OnStop()
        Diagnostics.EventLog.WriteEntry("HostPlus", "Service Stopping...", EventLogEntryType.Information)
        Settings.Log("Service Stopping... (" & Settings.Server & ")")
        With _worker
            If .IsBusy Then
                Settings.Log("Worker Busy, Requesting Cancellation... (" & Settings.Server & ")")
                .CancelAsync()
            End If
            Do While .IsBusy
                Settings.Log("Worker Busy, Requesting more time... (" & Settings.Server & ")")
                Me.RequestAdditionalTime(1000)
                Threading.Thread.Sleep(1000)
            Loop
        End With
        Settings.Log("Service Stopped. (" & Settings.Server & ")")
        Diagnostics.EventLog.WriteEntry("HostPlus", "Service Stopped.", EventLogEntryType.Information)
    End Sub

    Private Sub mWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles _worker.DoWork
        Try
            Do
                If _worker.CancellationPending Then
                    Exit Do
                End If

                Try
                    Process()
                Catch ex As Exception
                    Settings.Log("Process Fatal Error: " & ex.Message & " (" & Settings.Server & ")")
                End Try

                Threading.Thread.Sleep(1000)
            Loop
        Catch ex As Exception
            Settings.Log("Worker Fatal Error: " & ex.Message & " (" & Settings.Server & ")")
        End Try
    End Sub

    Private Sub Process()
        Dim processId As Integer = 0
        ClearNoBusy()
        Try

            Dim dt As DataTable
            Using com As New SqlClient.SqlCommand("SELECT     HostPlus_SessionTasks.RecID, HostPlus_SessionTasks.SessionId, HostPlus_Sessions.Name, HostPlus_SessionTasks.TaskId, HostPlus_SessionTasks.TimerWeekDays FROM HostPlus_Sessions INNER JOIN  HostPlus_SessionTasks ON HostPlus_Sessions.RecId = HostPlus_SessionTasks.SessionId WHERE     (HostPlus_Sessions.IsActive = 1) AND (HostPlus_SessionTasks.IsActive = 1) AND (HostPlus_SessionTasks.NextDate < @CurrentDate) AND (HostPlus_SessionTasks.TimerWeekDays LIKE N'%" & Format(Now, "ddd").ToString.ToUpper & "%') AND (HostPlus_Sessions.Server = @Server) ORDER BY HostPlus_SessionTasks.NextDate", New SqlClient.SqlConnection(Settings.Connection))
                com.Parameters.Add("@Server", SqlDbType.NVarChar).Value = Settings.Server
                com.Parameters.Add("@CurrentDate", SqlDbType.DateTime).Value = Now
                Using da As New SqlClient.SqlDataAdapter(com)
                    dt = New DataTable : da.Fill(dt)
                End Using
            End Using

            For Each r As DataRow In dt.Rows
                If Not IsSession(r.Item("Name").ToString) Then
                    processId = r.Item("RecID")
                    AddSession(r.Item("RecID"))
                End If
            Next

        Catch ex As Exception
            Throw New Exception("Error Stating Process (" & processId & "). " & ex.Message)
        End Try
    End Sub
    Private Sub ClearNoBusy()
        For Each s As Session In _Sessions
            If s.IsBusy = False Then
                _Sessions.Remove(s)
                ClearNoBusy()
                Return
            End If
        Next
    End Sub

    Private Function IsSession(ByVal sessionName As String) As Boolean
        Return _Sessions.Any(Function(s) s.SessionName = sessionName)
    End Function

    Private Sub AddSession(ByVal idVal As Integer)
        Dim t As New Task(idVal)
        _Sessions.Add(New Session(t, True))
    End Sub
End Class
