Imports System.Linq
Imports MailClient
Imports PreAlertProcessing
Imports QVProcessing
Imports Automail

Public Class Session

    Private WithEvents _Thread As System.ComponentModel.BackgroundWorker
    Private _SessionName As String
    Private _SessionId As Integer
    Private _Task As Task

    Public ReadOnly Property SessionId() As Integer
        Get
            Return _SessionId
        End Get
    End Property

    Public ReadOnly Property Task() As Task
        Get
            Return _Task
        End Get
    End Property
    Public ReadOnly Property SessionName() As String
        Get
            Return _SessionName
        End Get
    End Property

    Public Sub New(ByVal TaskVal As Task, Optional ByVal Start As Boolean = False)
        Try
            _SessionName = TaskVal.SessionName
            _SessionId = TaskVal.SessionId
            _Task = TaskVal
            _Thread = New System.ComponentModel.BackgroundWorker
            _Thread.WorkerSupportsCancellation = True
            _Thread.WorkerReportsProgress = True
            If Start = True Then
                StartProcess()
            End If
        Catch ex As Exception
            Throw New ApplicationException(Me.GetType.ToString & ". " & Reflection.MethodInfo.GetCurrentMethod.Name & ". Creating Session Failed!. " & ex.Message)
        End Try
    End Sub

    Public Sub StartProcess()
        If Not _Thread.IsBusy Then
            _Thread.RunWorkerAsync(_Task)
        End If
    End Sub

    Public Sub StopProcess()
        If _Thread.IsBusy Then
            _Thread.CancelAsync()
        End If
    End Sub

    Public ReadOnly Property IsBusy() As Boolean
        Get
            Return _Thread.IsBusy
        End Get
    End Property

    Private Sub _Thread_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles _Thread.DoWork
        Dim t As Task = CType(e.Argument, Task)
        Try

            t.Trace = ""
            t.Status = Statuses.InProgress
            t.Progress = "Executing..."
            t.ErrorMessage = ""
            _Thread.ReportProgress(0, t)


            If _Thread.CancellationPending Then
                t.Trace = ""
                t.Status = Statuses.Cancelled
                t.Progress = "Cancelled."
                t.ErrorMessage = ""
                _Thread.ReportProgress(0, t)
                Exit Sub
            End If


            Select Case t.TaskType
                Case "SERVICE"

                    Select Case t.TaskName
                        Case "TEST"
                            Diagnostics.EventLog.WriteEntry("HostPlus", "Testing.... Date: " & Now & " Message: " & t.Parameter.Value("MESSAGE"), EventLogEntryType.SuccessAudit)

                        Case "GET FILES FTP"
                            Using ftp As New FTPModule.FtpModule(t.Parameter.Value("FTPHOST"), t.Parameter.Value("FTPPORT"), t.Parameter.Value("USERID"), t.Parameter.Value("PASSWORD"))
                                ftp.DownloadFolder(t.Parameter.Value("REMOTEFOLDER"), t.Parameter.Value("REMOTEBACKUPFOLDER"), t.Parameter.Value("LOCALFOLDER"), CBool(t.Parameter.Value("DELETEFILE")), t.Parameter.Value("EXTENSION"))
                            End Using

                        Case "PUT FILES FTP"
                            Using ftp As New FTPModule.FtpModule(t.Parameter.Value("FTPHOST"), t.Parameter.Value("FTPPORT"), t.Parameter.Value("USERID"), t.Parameter.Value("PASSWORD"))
                                ftp.UploadFolder(t.Parameter.Value("REMOTEFOLDER"), t.Parameter.Value("LOCALFOLDER"), t.Parameter.Value("ARCHIVEFOLDER"), CBool(t.Parameter.Value("DELETEFILE")))
                            End Using
                        Case "PROCESS PREALERT FILES"
                            Using parser As New PreAlertParser(t.Connection, t.GlobalConnection, t.Parameter.Value("FOLDERPATH"), t.Parameter.Value("ARCHIVEFOLDERPATH"), t.Parameter.Value("ACCOUNTCODDE"))
                                parser.ParseFolder()
                            End Using

                        Case "PROCESS EVENTS"
                            Using eventProc As New EventService.EventsProcessing(t.Connection, t.Parameter.Value("TEVELSERVICEURL"), t.Parameter.Value("USER"), t.Parameter.Value("SYSTEMCODE"), t.Parameter.Value("PASSWORD"))
                                eventProc.SendEvents()
                            End Using

                        Case "SEND EMAILS"
                            Using mailSender As New MailSender(t.Parameter.Value("SMTP"), t.Parameter.Value("SMTPACCOUNT"), t.Parameter.Value("SMTPPASSWORD"), t.Parameter.Value("CONNECTION"))
                                mailSender.SendEmails()
                            End Using

                        Case "AUTOEMAIL"
                            Using autoEmailProcess As New Automail.Automail(t.Parameter.Value("CONNECTION"), t.Parameter.Value("ATTACHMENTFOLDER"), t.Parameter.Value("REPORTSERVERFOLDER"))
                                autoEmailProcess.ProcessAutomail()
                            End Using

                        Case "GET TRACKING"
                            Using qv As New QVProcess(t.Parameter.Value("CONNECTION"), t.Parameter.Value("QVURL"), t.Parameter.Value("ACCOUNTCARRIERID"))
                                qv.ProcessQVEvents()
                            End Using

                        Case "DOWNLOAD EMAILS"
                            Using emailClient As New MailDownloader(t.Parameter.Value("EMAILHOSTNAME"), t.Parameter.Value("EMAILUSERNAME"), t.Parameter.Value("EMAILPASSWORD"), t.Parameter.Value("STARTFROMDATE"), t.Parameter.Value("DELETEFROMSERVER"))
                                Dim stringSeparators() As String = {";"}
                                emailClient.From = t.Parameter.Value("FROMLIST").Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries).ToList
                                Dim lastDate As DateTime = emailClient.DownloadIntoDatabase(t.Parameter.Value("EMAILCONNECTION"))
                                SaveLastDate(lastDate)
                            End Using

                        Case "PROCESS INCOMING CIMP"
                            Dim cmp As New Cimp.FileProcessor(t.Parameter.Value("LOCALCONNECTIONSTRING"), t.Parameter.Value("MAILBOXNAME"), t.Parameter.Value("GLOBALCONNECTIONSTRING"))
                            cmp.ProcessDatabaseMessages()

                        Case "PROCESS OUTGOING ARINC"
                            Dim cop As New Cimp.OutgoingMessageProcessor(t.Parameter.Value("CONNECTION"), t.Parameter.Value("FOLDER"))
                            cop.ProcessMessages(t.Parameter.Value("EXTENSION"))

                        Case "PROCESS ARINC MESSAGE"
                            Dim rmp As New Cimp.RawMessageProcessor(t.Parameter.Value("CONNECTION"), t.Parameter.Value("FOLDER"), t.Parameter.Value("ARCHIVEFOLDER"))
                            rmp.ProcessFiles(t.Parameter.Value("EXTENSION"))

                        Case "PROCESS OUTGOING CIMP"
                            Dim rmp As New Cimp.OutgoingCimpProcessor(t.Parameter.Value("CONNECTION"))
                            rmp.ProcessOutgoingCimp()

                        Case "PROCESS DESCARTES AIR AMS"
                            Dim airAms As New Cimp.AirAmsProcessor(t.Parameter.Value("CONNECTION"), t.Parameter.Value("FOLDER"), t.Parameter.Value("ARCHIVEFOLDER"))
                            airAms.Process()
                        Case "SAVE MORPHO SCREENING RESULT"
                            Dim itdxServer As ITDX.Server = New ITDX.Server(t.Parameter.Value("HOSTIP"), t.Parameter.Value("ITDXIP"), t.Parameter.Value("TCPPORT"), t.Parameter.Value("UDPPORT,"), t.Parameter.Value("CONNECTION"))
                            itdxServer.StartServer()
                        Case Else

                            Throw New Exception("Function #" & t.TaskId & " Not Found!")
                    End Select

                Case Else
                    Throw New Exception("Function Type #" & t.TaskType & " Not Found!")
            End Select

            'BACK TO PENDING
            t.Trace = ""
            t.Status = Statuses.Pending
            t.Progress = "Last Run Status: Succeeded (" & Now & ")"
            t.ErrorMessage = ""
            _Thread.ReportProgress(0, t)
        Catch ex As Exception
            t.Trace = ex.StackTrace
            t.Status = Statuses.Failed
            t.Progress = "Last Run Status: Failed (" & Now & ") " & ex.Message
            t.ErrorMessage = ex.Message
            t.ErrorStackTrace = ex.Source + Environment.NewLine + ex.StackTrace
            _Thread.ReportProgress(0, t)
        End Try
    End Sub

    Private Sub mWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles _Thread.ProgressChanged
        Try
            Dim tmpChanged As Boolean = False
            _Task = CType(e.UserState, Task)

            If (_Task.Status = Statuses.Pending) Then
                Select Case _Task.TimerType.ToUpper
                    Case "SEC", "SECS"
                        _Task.NextDate = Now.AddSeconds(_Task.Timer)
                    Case "MIN", "MINS"
                        _Task.NextDate = Now.AddMinutes(_Task.Timer)
                    Case "HOUR", "HOURS"
                        _Task.NextDate = _Task.NextDate.AddHours(_Task.Timer)
                    Case "DAY", "DAYS"
                        _Task.NextDate = _Task.NextDate.AddDays(_Task.Timer)
                    Case "MONTH", "MONTHS"
                        _Task.NextDate = _Task.NextDate.AddMonths(_Task.Timer)
                    Case "YEAR", "YEARS"
                        _Task.NextDate = _Task.NextDate.AddYears(_Task.Timer)
                    Case Else
                        _Task.NextDate = Now.AddMinutes(_Task.Timer)
                End Select
            ElseIf (_Task.Status = Statuses.Failed) Then
                If _Task.MaxRetries.HasValue Then
                    If (_Task.Retries.HasValue AndAlso _Task.Retries.Value < _Task.MaxRetries.Value) OrElse (Not _Task.Retries.HasValue) Then
                        _Task.Retries = If(_Task.Retries.HasValue, _Task.Retries.Value + 1, 1)
                        tmpChanged = True
                    End If
                End If

                If Not tmpChanged Then
                    Select Case _Task.TimerType.ToUpper
                        Case "SEC", "SECS"
                            _Task.NextDate = Now.AddSeconds(_Task.Timer)
                        Case "MIN", "MINS"
                            _Task.NextDate = Now.AddMinutes(_Task.Timer)
                        Case "HOUR", "HOURS"
                            _Task.NextDate = _Task.NextDate.AddHours(_Task.Timer)
                        Case "DAY", "DAYS"
                            _Task.NextDate = _Task.NextDate.AddDays(_Task.Timer)
                        Case "MONTH", "MONTHS"
                            _Task.NextDate = _Task.NextDate.AddMonths(_Task.Timer)
                        Case "YEAR", "YEARS"
                            _Task.NextDate = _Task.NextDate.AddYears(_Task.Timer)
                        Case Else
                            _Task.NextDate = Now.AddMinutes(_Task.Timer)
                    End Select
                End If
            End If

            Using com As New SqlClient.SqlCommand("UPDATE    HostPlus_SessionTasks   SET Error=@Error,  Trace=@Trace, Status=@Status, Progress=@Progress, NextDate =@NextDate, LastDate = @LastDate WHERE (RecID = @RecID)", New SqlClient.SqlConnection(Settings.Connection))
                com.Parameters.Add("@Error", SqlDbType.NVarChar).Value = Task.ErrorMessage
                com.Parameters.Add("@Trace", SqlDbType.NVarChar).Value = Task.Trace
                com.Parameters.Add("@Status", SqlDbType.NVarChar).Value = Task.Status.ToString
                com.Parameters.Add("@Progress", SqlDbType.NVarChar).Value = Task.Progress
                com.Parameters.Add("@NextDate", SqlDbType.DateTime).Value = Task.NextDate
                com.Parameters.Add("@LastDate", SqlDbType.DateTime).Value = Now
                com.Parameters.Add("@RecID", SqlDbType.Int).Value = Task.Id
                com.Connection.Open() : com.ExecuteNonQuery() : com.Connection.Close()
            End Using


            If Not String.IsNullOrEmpty(Task.Email) And Not String.IsNullOrEmpty(Task.ErrorMessage) Then
                Using com As New SqlClient.SqlCommand("INSERT INTO HostPlus_Emails (RecDate, UserID,MailForm, MailTo,Body,Subject,Gateway) VALUES(GETDATE(), 'H+ SERVICE', 'Automail@CargoMatrix.com', @MailTo, @Body, @Subject, @Gateway)", New SqlClient.SqlConnection(_Task.Connection))
                    com.Parameters.Add("@MailTo", SqlDbType.NVarChar).Value = Task.Email
                    com.Parameters.Add("@Body", SqlDbType.NVarChar).Value = Task.SessionName + "." + Task.TaskName + " encountered following error:" + Environment.NewLine + Task.ErrorMessage + Environment.NewLine + "Stack Trace: " + Environment.NewLine + Task.ErrorStackTrace
                    com.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = "HostPlus.Service Error notification"
                    com.Parameters.Add("@Gateway", SqlDbType.NVarChar).Value = Task.Location
                    com.Connection.Open() : com.ExecuteNonQuery() : com.Connection.Close()
                End Using
            End If


        Catch ex As Exception
            If Not Settings.Log("Process Error: " & ex.Message) Then
                Diagnostics.EventLog.WriteEntry("HostPlus", "Process Error: " & ex.Message, EventLogEntryType.Error)
            End If
        End Try
    End Sub

    Private Sub SaveLastDate(ByVal lastDate As DateTime)
        Using com As New SqlClient.SqlCommand("UPDATE HostPlus_SessionTaskParameters SET Value=@LastDate WHERE (Name ='STARTFROMDATE' and SessionTaskId = @SessionTaskId)", New SqlClient.SqlConnection(Settings.Connection))
            com.Parameters.Add("@LastDate", SqlDbType.NVarChar).Value = lastDate.ToString
            com.Parameters.Add("@SessionTaskId", SqlDbType.Int).Value = Task.Id
            com.Connection.Open() : com.ExecuteNonQuery() : com.Connection.Close()
        End Using
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
