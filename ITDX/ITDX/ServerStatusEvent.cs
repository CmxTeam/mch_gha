﻿using System;

namespace ITDX
{
    public class ServerStatusEvent : EventArgs
    {
        public ServerStatusEvent(ServerStatus status)
        {
            Status = status;                    
        }

        public ServerStatus Status { get; private set; }
        public string Message { get; set; }
    }

    public enum ServerStatus
    {
        Error = 0,
        ServerStarted = 1,
        ConnectionFailed = 2,
        ServerStopped = 3,
        DeviceConnected = 4,
        DeviceDisconnected = 5,
        NewDeviceDataReceived = 6        
    }
}