﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;

namespace ITDX
{
    class MCHWebService
    {
        private MCHWebService()
        { 
         
        }

        public static bool ExecuteNonQuery(string sql)
        {
            WsMCH.ScannerMCHServiceSoap ws = new WsMCH.ScannerMCHServiceSoapClient("ScannerMCHServiceSoap");
            bool result = ws.ExecuteNonQuery(ConfigurationManager.AppSettings["station"].ToString(), sql);
            return result;
        }


        public static DataTable ExecuteQuery(string sql)
        {
            WsMCH.ScannerMCHServiceSoap ws = new WsMCH.ScannerMCHServiceSoapClient("ScannerMCHServiceSoap");
            DataTable result = ws.ExecuteQuery(ConfigurationManager.AppSettings["station"].ToString(), sql);
            return result;
        }

 

    }


}
