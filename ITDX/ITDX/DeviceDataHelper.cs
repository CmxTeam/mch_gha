﻿using System.Collections.Generic;

namespace ITDX
{
    public class DeviceDataHelper
    {
        private readonly DatabaseManager _databaseManager;
        private readonly Dictionary<string, string> _deviceData;
        
        public DeviceDataHelper(string hostIp, string itdxIp, string tcpPort, string udpPort, string connectionString)
        {
            _deviceData = new Dictionary<string, string>
            {
                {"HostIP", hostIp},
                {"ItdxIP", itdxIp},
                {"TcpPort", tcpPort},
                {"UdpPort", udpPort}
            }; 
            _databaseManager = new DatabaseManager(connectionString);
        }

        public void AddValue(string key, string value)
        {
            key = key.Replace(" ", "");
            if (_deviceData.ContainsKey(key))
                _deviceData.Remove(key);

            _deviceData.Add(key, value);
        }

        public void SaveValues()
        {
            var deviceId = _databaseManager.GetDeviceId(_deviceData["SerialNumber"]);

            if (!deviceId.HasValue)
                _databaseManager.InsertNewDevice(_deviceData);
            else
                _databaseManager.UpdateDevice(_deviceData, deviceId.Value);
        }
    }
}
