﻿using System;
using RemoteConnect;

namespace ITDX
{
    public class Observer : RCInterface
    {
        public event EventHandler<ServerStatusEvent> StatusChanged;

        private Device _device = null;
        private readonly string _hostIp;
        private readonly string _itdxIp;
        private readonly string _tcpPort;
        private readonly string _udpPort;
        private readonly string _connectionString;

        public Observer(string hostIp, string itdxIp, string tcpPort, string udpPort, string connectionString)
        {
            _hostIp = hostIp;
            _itdxIp = itdxIp;
            _tcpPort = tcpPort;
            _udpPort = udpPort;
            _connectionString = connectionString;
        }

        /// <summary>
        /// Signals the RC Server start event
        /// </summary>
        public override void OnServerStarted()
        {
            RaiseEvent(ServerStatus.ServerStarted);
        }

        /// <summary>
        /// Signals the RC Server stop event
        /// </summary>
        public override void OnServerStopped()
        {
            RaiseEvent(ServerStatus.ServerStopped);
        }

        /// <summary>
        /// Signals the device connect event
        /// </summary>        
        public override void OnDeviceConnected(Device device)
        {
            _device = device;
            RaiseEvent(ServerStatus.DeviceConnected);
        }

        /// <summary>
        /// Signals device disconnect event
        /// </summary>        
        public override void OnDeviceDisconnected(Device device)
        {
            if (_device == device)
            {
                _device = null;
                RaiseEvent(ServerStatus.DeviceDisconnected);
            }
        }

        /// <summary>
        /// Indicates new data has been received from the device */
        /// </summary>        
        public override void OnDeviceReceive(Device device)
        {
            if (_device != device) return;
            var dataHelper = new DeviceDataHelper(_hostIp, _itdxIp, _tcpPort, _udpPort, _connectionString);

            foreach (var se in _device.DeviceData.StatusEvents)
                dataHelper.AddValue(se.Name, se.Value);

            dataHelper.SaveValues();
            RaiseEvent(ServerStatus.NewDeviceDataReceived);
        }

        /// <summary>
        /// Indicates the end of a file transfer from the device
        /// </summary>        
        public override void OnFileReceive(Device device, string path)
        {
        }

        /// <summary>
        /// Indicates the end of a file transmit from server to device
        /// </summary>        
        public override void OnFileSent(Device device, string fileName, string path)
        {

        }

        /// <summary>
        /// Indicates the end of file transmission to device. When multiple files
        /// are selected, this function will be invoked after the last file has
        /// been transferred.
        /// </summary>        
        public override void OnFilesSent(Device device, int totalFiles)
        {
        }

        public override void OnError(string error)
        {
            RaiseEvent(ServerStatus.Error, error);
        }

        public bool IsConnected()
        {
            return (_device != null);
        }

        public void Disconnect()
        {
            if (_device != null)
            {
                _device.StopCommunication();
            }
        }

        private void RaiseEvent(ServerStatus status)
        {
            if (StatusChanged != null)
                StatusChanged(null, new ServerStatusEvent(status));
        }

        private void RaiseEvent(ServerStatus status, string message)
        {
            if (StatusChanged != null)
                StatusChanged(null, new ServerStatusEvent(status){Message = message});
        }
    }
}
