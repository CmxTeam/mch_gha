﻿using System;
using System.Net;
using RemoteConnect;

namespace ITDX
{
    public class Server
    {
        public event EventHandler<ServerStatusEvent> StatusChanged;

        private readonly Observer _observer;
        private readonly TransferProtocol _protocol;

        private readonly int _tcpPort;
        private readonly int _udpPort;
        private readonly IPAddress _hostIp;
        private readonly IPAddress _itdxIp;

        public Server(string hostIp, string itdxIp, string tcpPort, string udpPort, string connectionString)
        {
            _hostIp = IPAddress.Parse(hostIp);
            _itdxIp = IPAddress.Parse(itdxIp);
            _tcpPort = int.Parse(tcpPort);
            _udpPort = int.Parse(udpPort);
            _observer = new Observer(hostIp, itdxIp, tcpPort, udpPort, connectionString);
            _protocol = new TransferProtocol(_observer);

            _observer.StatusChanged += ObserverStatusChanged;
        }

        private void ObserverStatusChanged(object sender, ServerStatusEvent e)
        {
            RaiseStatusEvent(e.Status, e.Message);
        }

        public void StartServer()
        {
            try
            {
                _protocol.Start(_hostIp, _tcpPort, _udpPort);
                _protocol.Connect(_itdxIp, _tcpPort);
            }
            catch (Exception exception)
            {
                RaiseStatusEvent(ServerStatus.ConnectionFailed, exception.Message);
            }
        }

        public void StopServer()
        {
            _protocol.Stop();
            if (_observer.IsConnected())
                _observer.Disconnect();
        }

        private void RaiseStatusEvent(ServerStatus status, string message)
        {
            if (StatusChanged != null)
                StatusChanged(null, new ServerStatusEvent(status) { Message = message });
        }
    }
}