﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ITDX
{
    public class DatabaseManager
    {
        private readonly string _connectionString;

        public DatabaseManager(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void UpdateDevice(Dictionary<string, string> values, long deviceId)
        {
            var updateList = values.Select(i => string.Format("{0}='{1}'", i.Key, i.Value)).ToArray();
            var sql = string.Format("UPDATE  ITDX SET {0},LastConnected=getutcdate() WHERE  (DeviceId = {1})", string.Join(",", updateList), deviceId);

            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();
                    command.ExecuteNonQuery();
                    if (connection.State != ConnectionState.Closed)
                        connection.Close();
                }
            }
        }

        public void InsertNewDevice(Dictionary<string, string> values)
        {
            var sql = string.Format("INSERT INTO ITDX ({0}) VALUES ({1})", 
                string.Join(",", values.Keys.ToArray()),
                string.Join(",", values.Values.Select(v => "'" + v + "'").ToArray()));

            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();
                    command.ExecuteNonQuery();
                    if (connection.State != ConnectionState.Closed)
                        connection.Close();
                }
            }
        }

        public long? GetDeviceId(string serialNumber)
        {
            object result;

            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(string.Format("SELECT DeviceId FROM ITDX WHERE (SerialNumber = '{0}')", serialNumber), connection))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();
                    result = command.ExecuteScalar();
                    if (connection.State != ConnectionState.Closed)
                        connection.Close();
                }
            }

            return result == null ? null : (long?)long.Parse(result.ToString());
        }
    }
}