﻿using System;
using System.Configuration;
using System.Windows.Forms;
using ITDX;

namespace ItdxTester
{
    public partial class Form1 : Form
    {
        private Server _server;

        public Form1()
        {
            InitializeComponent();
        }

        private void CreateServerButtonClick(object sender, EventArgs e)
        {
            _server = new Server(ConfigurationManager.AppSettings["host_ip"], ConfigurationManager.AppSettings["itdx_ip"],
                ConfigurationManager.AppSettings["tcp_port"], ConfigurationManager.AppSettings["udp_port"],
                ConfigurationManager.ConnectionStrings["Mch"].ConnectionString);

            _server.StatusChanged += ServerStatusChanged;
        }

        public void ServerStatusChanged(object sender, ServerStatusEvent e)
        {
            Invoke(new MethodInvoker(() => statusViewer.Items.Insert(0, e.Status.ToString())));            
        }

        private void StartServerButtonClick(object sender, EventArgs e)
        {
            _server.StartServer();
        }

        private void StopServerButtonClick(object sender, EventArgs e)
        {
            _server.StopServer();
        }
    }
}
