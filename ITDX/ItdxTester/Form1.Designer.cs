﻿namespace ItdxTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createServerButton = new System.Windows.Forms.Button();
            this.startServerButton = new System.Windows.Forms.Button();
            this.stopServerButton = new System.Windows.Forms.Button();
            this.statusViewer = new System.Windows.Forms.ListView();
            this.Message = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // createServerButton
            // 
            this.createServerButton.Location = new System.Drawing.Point(12, 12);
            this.createServerButton.Name = "createServerButton";
            this.createServerButton.Size = new System.Drawing.Size(260, 23);
            this.createServerButton.TabIndex = 0;
            this.createServerButton.Text = "Create Server";
            this.createServerButton.UseVisualStyleBackColor = true;
            this.createServerButton.Click += new System.EventHandler(this.CreateServerButtonClick);
            // 
            // startServerButton
            // 
            this.startServerButton.Location = new System.Drawing.Point(12, 56);
            this.startServerButton.Name = "startServerButton";
            this.startServerButton.Size = new System.Drawing.Size(122, 23);
            this.startServerButton.TabIndex = 1;
            this.startServerButton.Text = "Start Server";
            this.startServerButton.UseVisualStyleBackColor = true;
            this.startServerButton.Click += new System.EventHandler(this.StartServerButtonClick);
            // 
            // stopServerButton
            // 
            this.stopServerButton.Location = new System.Drawing.Point(140, 56);
            this.stopServerButton.Name = "stopServerButton";
            this.stopServerButton.Size = new System.Drawing.Size(132, 23);
            this.stopServerButton.TabIndex = 2;
            this.stopServerButton.Text = "Stop Server";
            this.stopServerButton.UseVisualStyleBackColor = true;
            this.stopServerButton.Click += new System.EventHandler(this.StopServerButtonClick);
            // 
            // statusViewer
            // 
            this.statusViewer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Message});
            this.statusViewer.FullRowSelect = true;
            this.statusViewer.GridLines = true;
            this.statusViewer.Location = new System.Drawing.Point(12, 102);
            this.statusViewer.Name = "statusViewer";
            this.statusViewer.Size = new System.Drawing.Size(260, 148);
            this.statusViewer.TabIndex = 3;
            this.statusViewer.UseCompatibleStateImageBehavior = false;
            this.statusViewer.View = System.Windows.Forms.View.List;
            // 
            // Message
            // 
            this.Message.Width = 260;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.statusViewer);
            this.Controls.Add(this.stopServerButton);
            this.Controls.Add(this.startServerButton);
            this.Controls.Add(this.createServerButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button createServerButton;
        private System.Windows.Forms.Button startServerButton;
        private System.Windows.Forms.Button stopServerButton;
        private System.Windows.Forms.ListView statusViewer;
        private System.Windows.Forms.ColumnHeader Message;
    }
}

