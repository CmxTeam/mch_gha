﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;



namespace MCH.Communication
{
    public class QAS_PrepareCart
    {
        private static QAS_PrepareCart instance;
        private QAS_PrepareCart() {}

        public static QAS_PrepareCart Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new QAS_PrepareCart();
                }
                return instance;
            }
        }

        public long[]  WarehouseStatusids = {(int)enmCartStatus.PENDING,(int)enmCartStatus.READY_TO_FILL,(int)enmCartStatus.CLOSED,(int)enmCartStatus.WEIGHED,(int)enmCartStatus.STAGED_NO_GATE,(int)enmCartStatus.STAGED_NO_FLIGHT,(int)enmCartStatus.READY_FOR_PICKUP,(int)enmCartStatus.DROPPED_AT_INBOUND_STAGE,(int)enmCartStatus.EMPTIED};
        public long[] RunnerStatusids = {(int)enmCartStatus.READY_FOR_PICKUP,(int)enmCartStatus.IN_TRANSIT,(int)enmCartStatus.DROPPED,(int)enmCartStatus.CONFIRMED_LOADED,(int)enmCartStatus.DEPARTED,(int)enmCartStatus.NOT_LOADED_AT_GATE,(int)enmCartStatus.NOT_LOADED_RETRIVED_BY_RUNNER,(int)enmCartStatus.RETRIEVED_BY_RUNNER,(int)enmCartStatus.INBOUND_INFLIGHT,(int)enmCartStatus.DROPPED_AT_XFER_GATE,(int)enmCartStatus.InboundNewCart};
        public long[] InMyPossessionStatusids = {(int)enmCartStatus.IN_TRANSIT,(int)enmCartStatus.NOT_LOADED_RETRIVED_BY_RUNNER,(int)enmCartStatus.RETRIEVED_BY_RUNNER};


        public Airports GetDestinations()
        {
            Airports result = new Airports();

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("onlyActive", true.ToString());
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/common/GetDestinations");
        
            result = WebService.Instance.JsonGetMethod<Airports>(url,parameters);
            return result;  
        }


        public QASPrepareCartTasks GetActiveCarts(GetCartsParam model)
        {
            QASPrepareCartTasks result = new QASPrepareCartTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/cart/GetActiveCarts");
            result = WebService.Instance.JsonPostMethod<QASPrepareCartTasks>(url,model);
            return result;  
        }

 
        public CommunicationTransaction UpdateCartStatus(long cartId,enmCartStatus statusId, long userId, string deviceId, GPSLocationData gps, long warehouseId)
        {
            
            CartStatusUpdateParam model = new CartStatusUpdateParam();
            model.CartId = cartId;
            model.ClientDateTime = DateTime.Now;
            model.StatusId = statusId;
            model.UserId = userId;
            if (warehouseId != -1)
            {
                model.WarehouseLocationId = warehouseId;
            }
            else
            {
                model.WarehouseLocationId = null;
            }

                CoordinatesDto co = new CoordinatesDto();
                co.Altitude =  gps.Altitude;
                co.Latitude = gps.Latitude;
                co.Longitude = gps.Longitude;

                GpsDataDto to = new GpsDataDto();
                to.Accuracy = gps.Accuracy;
                to.Bearing = gps.Bearing;
                to.Coordinates = co;
                to.Provider = gps.Provider;
                to.Speed = gps.Speed;

                DeviceGeoAction geo = new DeviceGeoAction();
                geo.DeviceId = deviceId;
                geo.GPSData = to;

            model.DeviceGeo = geo;
           

            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/cart/UpdateCartStatus");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;
        }

        public QASPrepareCartTask GetScanCart(string barcode)
        {

            QASPrepareCartTask result = new QASPrepareCartTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/cart/getscancart");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("barcode", barcode);
            result = WebService.Instance.JsonGetMethod<QASPrepareCartTask>(url,parameters);
            return result;   

        }

        public QASNewMasterCartTask GetCart(string barcode)  // get master cart record
        {

            QASNewMasterCartTask result = new QASNewMasterCartTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/cart/GetCart");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("barcode", barcode);
            result = WebService.Instance.JsonGetMethod<QASNewMasterCartTask>(url,parameters);
            return result;   

        }
      
        public QASPrepareCartTask PrepareNewCart(PrepareCartParam model)
        {
            QASPrepareCartTask result = new QASPrepareCartTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/ScannerOutbound/PrepareNewCart");
            result = WebService.Instance.JsonPostMethod<QASPrepareCartTask>(url,model);
            return result;
        }

        public CommunicationTransaction UpdateCartDestination(UpdateCartParams model)
        {
            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath,  "/ScannerOutbound/UpdateCartDestination");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;    

        }


        public CommunicationTransaction UpdateCartWeight(UpdateCartParams model)
        {
            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath,  "/scanneroutbound/UpdateCartWeight");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;   
        }

//        public CommunicationTransaction DropCartAtGate(long cartId,decimal latitude, decimal longitude ,long userId)
//        {
//
//            DropCartParams model = new DropCartParams();
//            model.ActionId = null;
//            model.CartId = cartId;
//            model.ClientDateTime = DateTime.Now;
//            model.Latitude = latitude;
//            model.Longitude = longitude;
//            model.UserId = userId;
//
//
//            CommunicationTransaction result = new CommunicationTransaction();           
//            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/ScannerOutbound/DropCartAtGate");
//            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
//            return result;
//
//        }


        public CommunicationTransaction DeactivateCart(long cartId,long userId, string deviceId, GPSLocationData gps)
        {
            CartStatusUpdateParam model = new CartStatusUpdateParam();
            model.CartId = cartId;
            model.ClientDateTime = DateTime.Now;
            model.UserId = userId;
                CoordinatesDto co = new CoordinatesDto();
                co.Altitude =  gps.Altitude;
                co.Latitude = gps.Latitude;
                co.Longitude = gps.Longitude;

                GpsDataDto to = new GpsDataDto();
                to.Accuracy = gps.Accuracy;
                to.Bearing = gps.Bearing;
                to.Coordinates = co;
                to.Provider = gps.Provider;
                to.Speed = gps.Speed;

                DeviceGeoAction geo = new DeviceGeoAction();
                geo.DeviceId = deviceId;
                geo.GPSData = to;

            model.DeviceGeo = geo;

            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath,  "/cart/DeactivateCart");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;

        }

      
        public UOMs GetUOMs(enmUOMTypes typeId)
        {
            UOMs result = new UOMs();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath,  "/common/getuoms");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("typeId", ((int)typeId).ToString());
            result = WebService.Instance.JsonGetMethod<UOMs>(url,parameters);
            return result;
        }


        public QASMyTasks GetTasks(bool isopen, long userId)
        {

            GetTasksParam model = new GetTasksParam();
            model.IsOpen = isopen;
            model.UserId = userId;

            QASMyTasks result = new QASMyTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/task/gettasks");
            result = WebService.Instance.JsonPostMethod<QASMyTasks>(url,model);
            return result;  

        }

    }

    public class QASNewMasterCartTask
    {
        public CartDto Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASPrepareCartTask
    {
        public CartListItem Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASPrepareCartTasks
    {
        public List<CartListItem> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class GetCartsParam
    {
        public enmCartDirections ? CartDirection { get; set; } // instead of bool ? IsInbound
        public long ? UserId { get; set; }
        public long ? HolderId { get; set; }
        public long[] StatusIds { get; set; }
        public int ? WarehouseLocationId { get; set; }
    }

    public class CartStatusUpdateParam
    {
        public long CartId{get;set;}
        public long UserId{get;set;}
        public long ? WarehouseLocationId{get;set;}

        public DateTimeOffset ClientDateTime { get; set; }
        public enmCartStatus ? StatusId { get; set; }
        public DeviceGeoAction DeviceGeo { get; set; }
    }

  

    public class PrepareCartParam
    {
        public string Barcode{get;set;}
        public string Number{get;set;}
        public long UserId {get;set;}
        public DateTimeOffset ClientDateTime { get; set; }
        public long ? CartTypeId {get;set;}

    }

    public class DropCartParams{
        public long CartId { get; set; }
        public long UserId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public long ? ActionId { get; set; }
        public DateTimeOffset ClientDateTime { get; set; }
    }
        

    public class UpdateCartParams
    {

        public long CartId{get;set;}
        public long? DestinationId { get; set; }
        public double? Weight{get;set;}
        public double? TareWeight { get; set; }
        public double? DollyWeight { get; set; }
        public int? UomId { get; set; }
        public DeviceGeoAction GpsData { get; set; }
        public long UserId { get; set; }
        public DateTimeOffset ClientDateTime { get; set; }
    }

    public class DeviceGeoAction
    {
        public string DeviceId { get; set; }
        public int? ActionId { get; set; }
        public GpsDataDto GPSData { get; set; }
    }



    public class CartFlightDetails
    {
        public long Id { get; set; }
        public string FlightFSId { get; set; }
        public string FlightNumber { get; set; }
        public string FlightOrigin { get; set; }
        public string FlightDestination { get; set; }
        public string ArrivalGate { get; set; }
        public string DepartureGate { get; set; }
        public string CarrierCode { get; set; }

        public DateTimeOffset? STA { get; set; }
        public DateTimeOffset? STD { get; set; }
        public DateTime ? STDUtc { get; set; }
        public DateTime ? STAUtc { get; set; }
    }

    public class  CartListItem
    {
        public long CartId { get; set; }
        public string CartNumber { get; set; }
        public string Cartbarcode { get; set; }
        public string RouteOrigin { get; set; }
        public string RouteDestination { get; set; }
        public string CartDestination {get; set; }
        public double ? GrossWeight {get;set;}
        public double ? TareWeight {get;set;}
        public double ? DollyWeight {get;set;}
        public double ? MailWeight {get;set;} //Mail = Gross - tare - Dolly

        public enmCartStatus StatusId { get; set; }
        public CartFlightDetails InboundFlight { get; set; }
        public CartFlightDetails OutboundFlight { get; set; }
        public WarehouseLocationDto WarehouseLocation { get; set; }
        public DateTimeOffset RegisteredTimestamp { get; set; }

        public string Reference { 
            get
            {
                if (string.IsNullOrEmpty(CartNumber))
                {
                    return Cartbarcode;
                }
                else
                {
                    return CartNumber;
                }
            }
        }


        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}",CartNumber,RouteOrigin, RouteDestination,Cartbarcode);
        }
    }

   

    public enum enmCartDirections
    {
        OUTBOUND =1,
        INBOUND = 2,
        TRANSFER = 3,
    }

    public enum enmCartStatus
    {
        ALL = -1,
        PENDING = 100, 
        READY_TO_FILL = 101,
        CLOSED = 102,
        WEIGHED = 103,
        STAGED_NO_GATE = 104, 
        STAGED_NO_FLIGHT = 105, 
        READY_FOR_PICKUP = 106,
        IN_TRANSIT = 107,
        DROPPED = 108,
        CONFIRMED_LOADED = 109, 
        DEPARTED = 110, 
        NOT_LOADED_AT_GATE = 111,
        NOT_LOADED_RETRIVED_BY_RUNNER = 112, 
        RETRIEVED_BY_RUNNER = 113, 
        DROPPED_AT_INBOUND_STAGE = 114, 
        EMPTIED = 115, 
        DEACTIVATED = 116, 
        INBOUND_INFLIGHT = 118,
        DROPPED_AT_XFER_GATE = 119,
        InboundNewCart = 120,
    }


    public enum enmUOMTypes
    {
        WEIGHT = 1,
        VOLUME = 2,
        DIMENSION = 3,
        PRODUCT = 4
    }

    public class UOMs
    {        
        public List<UomDto> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }


    public class UomDto
    {
        public int Id { get; set; }
        public string TreeCharCode { get; set; }
        public string TwoCharCode { get; set; } // show on the UI
        public string Name { get; set; }
        public int ? TypeId { get; set; }
        public bool ? IsDefault { get; set; }
    }

    public class Airports
    {
        public List<AirportDto> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class AirportDto
    {
        public long AirportId { get; set; }
        public string IATACode { get; set; }
        public string Name { get; set; }
    }


    // Task

    public class QASMyTask
    {
        public TaskListItem Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASMyTasks
    {
        public List<TaskListItem> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class TaskListItem
    {
        public long Id { get; set; }
        public DateTime RecDate { get; set; }
        public enmTaskTypes TaskTypeId { get; set; }
        public enmTaskStatuses TaskStatusId { get; set; }
        public string Title { get; set; }
        public long ? EntityId { get; set; }
        public enmEntityTypes EntityTypeId { get; set; }
        public long ? AssignedToUserId { get; set; }
        public CartListItem Cart { get; set; }
        public FlightListItem Flight { get; set; }
    }


    public class GetTasksParam
    {
        public long? UserId { get; set; }
        public enmTaskTypes? TaskTypeId { get; set; }
        public enmTaskStatuses? TaskStatusId { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public bool? IsOpen { get; set; }
    }



    public enum enmTaskTypes : int
    {
        Pickup_From_Flight = 1,
        Bring_To_Gate = 2,
        Drop_At_Warehouse = 3,
        Pickup_From_Warehouse = 4
    }

    public enum enmTaskStatuses:int
    {
        New = 700,
        Partial = 701,
        Completed = 702,
        Canceled = 703
    }

    public enum enmEntityTypes : int
    {
        Cart = 1,
        Flight = 2,
        BMC_APC = 3,
        Tug = 4,
        InboundFlightWithCart = 5,
        USPS_Truck = 6,
        Task = 7
    }

    public enum enmCartFilters
    {
        WAREHOUSE =1,
        RUNNER = 2,
        IN_MY_POSSESSION = 3,
    }

}



