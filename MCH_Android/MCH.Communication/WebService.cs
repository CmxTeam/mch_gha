﻿using System;
using System.Collections.Generic;
using System.Net;
//using System.IO;
using System.Text;
using Android.App;
using Newtonsoft.Json;
using Environment = Android.OS.Environment;
using Java.IO;
using System.Collections;
namespace MCH.Communication
{
 


    public class WebService
    {
 
        string token;
        string shellPath;
        string appPath;

        private static WebService instance;

        private WebService() {}

        public static WebService Instance
        {
          get 
          {
             if (instance == null)
             {
                instance = new WebService();
             }
             return instance;
          }
        }
 

        public string Token
        {
            get
            { 

                return token;
            }
            set
            {
                token = value;
            }
        }

        public string ShellPath
        {
            get
            { 

                return shellPath;
            }
            set
            {
                shellPath = value;
            }
        }


        public string AppPath
        {
            get
            { 

                return appPath;
            }
            set
            {
                appPath = value;
            }
        }

        public string BuildUrl(string baseUrl, string path)
        {

            if (!string.IsNullOrEmpty(path))
            {
                if (path.Substring(0,1) == "/")
                {
                    path = path.Substring(1, path.Length - 1);
                }
            }

            if (!string.IsNullOrEmpty(baseUrl))
            {
                if (baseUrl.Substring(baseUrl.Length - 1,1) != "/")
                {
                    baseUrl += "/";
                }
            }


            System.Uri apiUri = new System.Uri(baseUrl);
            System.Uri uri = new System.Uri(apiUri, path);
            return uri.ToString();
        }

        public string LogFolder {get;set;}
        public bool LogEnable {get;set;}

  


        public CommunicationTransaction PostMethod(string url, string json)
        {
          

            
            DateTime start = DateTime.UtcNow;

            //url = GetFullUrl(url);

            CommunicationTransaction t = new CommunicationTransaction();
            t.Url = url;
             
            try
            {
            HttpWebRequest request = HttpWebRequest.CreateHttp(url);
 
            request.ContentType = "application/json";
            request.Method = "POST";

            if(!string.IsNullOrEmpty(Token))
            {
                request.Headers.Add("Authorization", "Bearer " + Token); 
            }
             
            byte[] postBytes = Encoding.ASCII.GetBytes(json);
            request.ContentLength = postBytes.Length;


//            try
//            {

                System.IO.Stream postStream = request.GetRequestStream();
                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Close();

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(string.Format("Server returned status code: {0}", response.StatusCode));
                    }
                    else
                    {
                        using (System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream()))
                        {
                            var content = reader.ReadToEnd();
                            if (string.IsNullOrWhiteSpace(content))
                            {
                                    throw new Exception("Response contained empty body.");
                            }
                            else
                            {
                                t.Json = json;
                                    t.Status = true;
                                    t.Content = content;
                                    t.Error = string.Empty;
                                    WriteLog(t.Url,json,start,t.Error);
                                    return t;   
                            }
                        }
                    }
                } 
                   

            }
            catch (System.Net.WebException ex)
            {
                t.Json = json;
                t.Status = false;
                t.Content = string.Empty;
                t.Error = string.Format("Web exception error. {0}", ex.Message);
                WriteLog(t.Url,json,start,t.Error);
                return t;  
            }
            catch (Exception ex)
            {
                t.Json = json;
                t.Status = false;
                t.Content = string.Empty;
                t.Error = string.Format("Error fetching data. {0}", ex.Message);
                WriteLog(t.Url,json,start,t.Error);
                return t;  
            }

         

        }




        public CommunicationTransaction GetMethod(string url)
        {
            return GetMethod(url, null);
        }

        public CommunicationTransaction GetMethod(string url, Dictionary<string, string> parameters)
        {

            DateTime start = DateTime.UtcNow;

           

            //url = GetFullUrl(url);

            CommunicationTransaction t = new CommunicationTransaction();

        
            if (parameters != null)
            {
                StringBuilder paramList = new StringBuilder();
                foreach (KeyValuePair<string, string> kvp in parameters)
                {

                    if (paramList.ToString().Trim() == string.Empty)
                    {
                        paramList.Append(string.Format("{0}={1}", kvp.Key, kvp.Value));
                    }
                    else
                    {
                        paramList.Append(string.Format("&{0}={1}", kvp.Key, kvp.Value));
                    }

                }

                if (paramList.ToString().Trim() != string.Empty)
                {
                    url += string.Format("?{0}", paramList.ToString());
                }
            }
            try
            {
                
            var request = HttpWebRequest.Create(url);
 
            request.ContentType = "application/json";
            request.Method = "GET";
     
            if(!string.IsNullOrEmpty(Token))
            {
                request.Headers.Add("Authorization", "Bearer " + Token); 
            }

            t.Url = url;

//            try
//            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(string.Format("Server returned status code: {0}", response.StatusCode));
                    }
                    else
                    {
                        using (System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream()))
                        {
                            var content = reader.ReadToEnd();
                            if (string.IsNullOrWhiteSpace(content))
                            {
                                    throw new Exception("Response contained empty body.");
                            }
                            else
                            {
                                    t.Status = true;
                                    t.Content = content;
                                    t.Error = string.Empty;
                                    WriteLog(t.Url,string.Empty,start,t.Error);
                                    return t;   
                            }
                        }
                    }
                } 
                   

            }
            catch (System.Net.WebException ex)
            {
                t.Status = false;
                t.Content = string.Empty;
                t.Error = string.Format("Web exception error. {0}", ex.Message);
                WriteLog(t.Url,string.Empty,start,t.Error);
                return t;  
            }
            catch (Exception ex)
            {
                t.Status = false;
                t.Content = string.Empty;
                t.Error = string.Format("Error fetching data. {0}", ex.Message);
                WriteLog(t.Url,string.Empty,start,t.Error);
                return t;  
            }


        }


//        public R JsonPostMethod<R>(string url, object parameters, R defult)
//        {
//            string json = JsonConvert.SerializeObject(parameters);
//            R result = defult;
//            CommunicationTransaction transaction = WebService.Instance.PostMethod(url, json);
//            result = DeserializeObject<R>(transaction,result);
//            return result;
//        }
//
//        public R JsonGetMethod<R>(string url, Dictionary<string, string> parameters, R defult)
//        {
//            R result = defult;
//            CommunicationTransaction transaction = WebService.Instance.GetMethod(url, parameters); 
//            result = DeserializeObject<R>(transaction,result);
//            return result;
//        }

        public R JsonPostMethod<R>(string url, object parameters) where R : new()
        {
            string json = JsonConvert.SerializeObject(parameters);
            R result = new R();
            CommunicationTransaction transaction = WebService.Instance.PostMethod(url, json);
            result = DeserializeObject<R>(transaction,result);
            return result;
        }
            
        public R JsonGetMethod<R>(string url) where R : new()
        {
            R result = new R();
            CommunicationTransaction transaction = WebService.Instance.GetMethod(url); 
            result = DeserializeObject<R>(transaction,result);
            return result;
        }
        public R JsonGetMethod<R>(string url, Dictionary<string, string> parameters) where R : new()
        {
            R result = new R();
            CommunicationTransaction transaction = WebService.Instance.GetMethod(url, parameters); 
            result = DeserializeObject<R>(transaction,result);
            return result;
        }

   
        public R DeserializeObject<R> (CommunicationTransaction transaction, R defult)
        {

            R result = defult;

            if (defult.GetType().ToString().Contains("CommunicationTransaction"))
            {

                if (transaction == null)
                {
                    transaction.Status = false;
                    transaction.Error = "Transaction data is null or empty.";
                    return (R) Convert.ChangeType(transaction, typeof(R));   
                }

                if (transaction.Status)
                {
                    try
                    {
                        CommunicationTransaction t = JsonConvert.DeserializeObject<CommunicationTransaction>(transaction.Content);
                        t.Content = transaction.Content;
                        t.Url = transaction.Url;
                        return (R)Convert.ChangeType(t, typeof(R));
                    }
                    catch (Exception ex)
                    {
                        transaction.Status = false;
                        transaction.Error = ex.Message;
                        return (R)Convert.ChangeType(transaction, typeof(R));

                    }
                }
                else
                {
                    return (R)Convert.ChangeType(transaction, typeof(R));
                }
            }
            else
            {
                
                if (transaction == null)
                {
                    transaction = new CommunicationTransaction();
                    transaction.Status = false;
                    transaction.Error = "Transaction data is null or empty.";
                    var propertyInfo = defult.GetType().GetProperty("Transaction");
                    propertyInfo.SetValue(defult,transaction);
                    return defult;
                }

                if (transaction.Status)
                {
                    try
                    {
                        result = JsonConvert.DeserializeObject<R> (transaction.Content);

                        var propertyInfo = result.GetType().GetProperty("Transaction");
                        CommunicationTransaction t = (CommunicationTransaction)propertyInfo.GetValue(result, null);
                        t.Content = transaction.Content;
                        t.Url = transaction.Url; 
                        t.Json = transaction.Json; 
                        propertyInfo.SetValue(result,t); 
                        return result;
          

                    }
                    catch (Exception ex)
                    {
                        transaction.Status = false;
                        transaction.Error = ex.Message;
                        var propertyInfo = defult.GetType().GetProperty("Transaction");
                        propertyInfo.SetValue(defult,transaction);
                        return defult;

                    }
                }
                else
                {
                    var propertyInfo = defult.GetType().GetProperty("Transaction");
                    propertyInfo.SetValue(defult,transaction);
                    return defult;
                } 
            }




        }

        public bool WriteLog(string url, string json, DateTime start, string error)
        {
            if (!LogEnable)
            {
                return true;
            }
            else
            {
                return WriteLogFile(url,json,start,error);
            }

        }

        public bool WriteLogFile(string url, string json, DateTime start, string error)
        {
 
            try
            {
                //This could be user to get a personal folder 
                //using Environment = Android.OS.Environment;
                //using Java.IO;
                //string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);



                    
                File path = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures),LogFolder );
                string folder = Application.Context.GetText(Resource.String.library_name) + ".txt";
                string filename = System.IO.Path.Combine(path.ToString(),folder);


                if (!path.Exists())
                {
                    path.Mkdirs();
                }


                DateTime ended = DateTime.UtcNow;
                TimeSpan ts =   ended - start;
                double duration = ts.TotalMilliseconds;
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("URL: "  + url + "\r\n");
                if (json != string.Empty)
                {
                    sb.AppendLine("JSON: "  + json + "\r\n");
                }
                sb.AppendLine("Started: "  + start.ToString() + "\r\n");
                sb.AppendLine("Ended: "  + ended.ToString() + "\r\n");
                sb.AppendLine("TimeSpan: "  + duration.ToString() + "\r\n");
                if (error != string.Empty)
                {
                    sb.AppendLine("Error: "  + error + "\r\n");
                }
                sb.AppendLine("\r\n" + "--------------------------------" + "\r\n" + "\r\n");

                if (path.Exists())
                {
                    using (var streamWriter = new System.IO.StreamWriter(filename, true))
                    {
                        streamWriter.WriteLine(sb.ToString());
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }

        }
 

    }
}





