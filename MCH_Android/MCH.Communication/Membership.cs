﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
//using Newtonsoft.Json;
using Android.App;

namespace MCH.Communication
{

    public class Membership  //: WebServiceWrapper
    {

        private static Membership instance;
        private Membership() {}

        public static Membership Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new Membership();
             }
             return instance;
          }
        }


    

        public DeviceInfo SaveDevice(DeviceInfo device)
        {
            DeviceInfo result = new DeviceInfo();
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"device/saveDevice");
        
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("MAC", device.Data.MAC);
            parameters.Add("companyId", device.Data.CompanyId.ToString());
            //parameters.Add("deviceName", device.Data.DeviceName ?? "");
            parameters.Add("deviceName", device.Data.DeviceName);
            if (device.Data.Id > 0)
            {
                parameters.Add("Id", device.Data.Id.ToString());
            }
                
            result = WebService.Instance.JsonPostMethod<DeviceInfo>(url,parameters);
            if(result.Transaction.Status)
            {
                result.Data.CompanyName = device.Data.CompanyName;
            }

            return result;
        }

        public Companies GetCompanies()
        {
            Companies result  = new Companies();
            //string url = "mchapi/common/getcompanies";
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"common/getcompanies");
            result = WebService.Instance.JsonGetMethod<Companies>(url,null);
            return result;
        }
            
        public CompanySecuritySettings GetCompanySecuritySettings(string companyName)
        {
            CompanySecuritySettings result  = new CompanySecuritySettings();
                    string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"Membership/GetCompanySecuritySettings");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("CompanyName", companyName);
            result = WebService.Instance.JsonGetMethod<CompanySecuritySettings>(url, parameters);
            return result;
        }

        public TokenInfo GetToken(string path,int userId,string seesionId)
        {
            TokenInfo result  = new TokenInfo();
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"Membership/apitoken");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("Url", path);
            parameters.Add("UserName", userId.ToString());
            parameters.Add("Password", seesionId);
            result = WebService.Instance.JsonPostMethod<TokenInfo>(url, parameters);
            return result;
        }


        public User GetUserAuthDataByPin(PinParameters parameters)
        {

            User result = new User();

            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"Membership/GetUserAuthDataByPin");

            parameters.Location = null;

            result = WebService.Instance.JsonPostMethod<User>(url,parameters);

            if(result.Transaction.Status)
            {
                if(result.Data==null)
                {
                    result.Data = new UserData();
                    result.Data.IsAthenticated = false;
                }
                else
                {
                    result.Data.IsAthenticated = true;
                }
            }
            else
            {
                result.Data = new UserData();
                result.Data.IsAthenticated = false;
            }

            return result;

        }




        public User AuthenticateUserPin(PinParameters parameters)
        {

            User result = new User();
     
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"Membership/AuthenticateUserPin");
 
            result = WebService.Instance.JsonPostMethod<User>(url,parameters);
  
            if(result.Transaction.Status)
            {
                if(result.Data==null)
                {
                    result.Data = new UserData();
                    result.Data.IsAthenticated = false;
                }
                else
                {
                    result.Data.IsAthenticated = true;
                }
            }
            else
            {
                result.Data = new UserData();
                result.Data.IsAthenticated = false;
            }

            return result;

        }

       
        public MobileApplications GetUserAppsByCred(string username, string password)
        {

            MobileApplications result  = new MobileApplications();
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"Membership/GetUserAppsByCred");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UserName", username);
            parameters.Add("Password", password);
            result = WebService.Instance.JsonPostMethod<MobileApplications>(url, parameters);
            return result;
        }

        public MobileApplications GetUserAppsByPin(string pin)
        {
            MobileApplications result  = new MobileApplications();
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"Membership/GetUserAppsByPin");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("Pin", pin);
            result = WebService.Instance.JsonPostMethod<MobileApplications>(url, parameters);
            return result;
        }

 

        public UserSession CheckUserSession(UserSessionParameters data)
        {

            UserSession result = new UserSession();
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"Membership/CheckUserSession");
            result = WebService.Instance.JsonPostMethod<UserSession>(url,data);
            return result;
        }

        public DeviceInfo GetDeviceInfo(string deviceId)
        {
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"device/GetDeviceInfo");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("DeviceId", deviceId);
            DeviceInfo result  = new DeviceInfo();
            result = WebService.Instance.JsonGetMethod<DeviceInfo>(url, parameters);
            //result.SessionTimeout =  1;

            if (result.Transaction.Status)
            {
                if (result.Data != null)
                {
                    result.Data.MAC = deviceId;   
                }
            }

            return result;
        }
            
        public User AuthenticateAppUser(UserPasswordParameters parameters)
        {
            User result = new User();
        
            string url = WebService.Instance.BuildUrl( WebService.Instance.ShellPath,"Membership/AuthenticateAppUser");
             
 
                
            result = WebService.Instance.JsonPostMethod<User>(url,parameters);

            if(result.Transaction.Status)
            {
                if(result.Data==null)
                {
                   result.Data = new UserData();
                   result.Data.IsAthenticated = false;
                }
                else
                {
                    result.Data.IsAthenticated = true;
                }
            }
            else
            {
                result.Data = new UserData();
                result.Data.IsAthenticated = false;
            }
                
            return result;

        }


    }


    public class CompanySecuritySettings
    {
        public CompanySecuritySettingsData Data { get; set; }
        public CommunicationTransaction Transaction { get; set; }
    }


    public class DeviceInfo
    {
        public DeviceInfoData Data { get; set; }
        public CommunicationTransaction Transaction { get; set; }
    }
 
    public class TokenInfo
    {
        public string Data { get; set; }
        public CommunicationTransaction Transaction { get; set; }
    }

    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
    }


    public class UserSession
    {
        public UserSessionData Data { get; set; }
        public CommunicationTransaction Transaction { get; set; }
    }

    public class UserSessionData
    {
        public bool IsValid { get; set; }
    }

 
    public class DeviceInfoData
    {
        public int Id { get; set; }
        //public string Name { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string DeviceName { get; set; }
        public string MAC { get; set; }
    }
 
    public class CompanySecuritySettingsData
    {
        public int PassPatternId { get; set; }
        public int UserNamePatternId { get; set; }
        public int? PassExpiration { get; set; }
        public int ForgotPassLinkExpiration { get; set; }
        public int MaxPassAttempts { get; set; }
        public int MaxPassAnswerAttempts { get; set; }
        public int SessionTimeout { get; set; }
        public bool ResetPassEnabled { get; set; }
        public bool RetrievePassEnabled { get; set; }
        public bool PassSecurityQuestionRequires { get; set; }
        public int PassFormatId { get; set; }
        public MobileAuthMethods MobileAuthMethod { get; set; }
        public bool AllowRememberMe { get; set; }
        public string PassPattern { get; set; }
        public string UsernamePattern { get; set; }
        public bool IsSingleSession { get; set; }
    }


    public enum MobileAuthMethods
    {
        None = 0,
        Pin = 1,
        Password = 2,
        PinAndPassword = 3,
    }

    public class CompanyInfo
    {
        public string CompanyName{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }
 

    public class User
    {
        public UserData Data { get; set; }
        public CommunicationTransaction Transaction { get; set; }
    }

	public class UserData
    {
        //Shell User Id 
		public int UserId { get; set; }
        //MCH User Id
        //public int AppUserId { get; set; }
        public int CompanyId { get; set; }
        public string Pin { get; set; }
		public int RoleId { get; set; }
        public string UserName { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string SessionId { get; set; }
        public bool IsAdmin { get; set; }
        public UserShellSetting ShellSetting { get; set; }
        public bool IsAthenticated{ get; set; }

        public string FormatUserName
        {
            get
            {
                return  Firstname  + " " + Lastname;
            }
        }

        public CommunicationTransaction transaction{ get; set; } 
    }

	public class UserShellSetting
	{
        public string SiteIds { get; set; }
        public long ? DefaultSiteId { get; set; }
        public string WarehouseIds { get; set; }
        public long ? DefaultWarehouseId { get; set; }
        public IList<UserShellAppSettings> AppSettings { get; set; }
	}

	public class UserShellAppSettings 
    {
        public long AppId { get; set; }
        public long AppRoleId { get; set; }
        public string AppAccountIds { get; set; }
        public long? DefaultAccountId { get; set; }
        public string AppSettingIds { get; set; }
        public string AppName { get; set; }
        //use token
        public bool IsSecureApi { get; set; }
        //api root url
        public string ApiRoot { get; set; }
        public string  AppDisplayName { get; set; }
        public string  AppDescription { get; set; }

    }


    public enum ApplicationTypes
    {
        WindowsMobile = 2,
        iOS = 3,
        Android = 4,
        WindowsPhone = 5,
        Desktop = 6,
        WebService = 7,
        WindowsService = 8,
    }
  
    public class Companies
    {
        public List<CompanyItem> Data { get; set; }
        public CommunicationTransaction Transaction { get; set; } 
    }

    public class CompanyItem
    {
 
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class MobileApplications
    {
        public List<MobileApplication> Data { get; set; }
        public CommunicationTransaction Transaction { get; set; } 
    }

    public class MobileApplication
    {
 
        public int AppId { get; set; }
 
        public string Name { get; set; }
 
        public string AppDisplayName { get; set; }
 
        public string AppDescription { get; set; }
 
        public bool IsSecureApi { get; set; }
 
        public string ApiRoot { get; set; }

//        public string Path { 
//            get
//            {
//                string[] arr = ApiRoot.Split('/');
//                return arr[arr.Length -1];
//            }
//        }
    }


    public class PinParameters
    {
        public string AppName { get; set;}
        public string Pin { get; set;}
        public string DeviceId { get; set;}
        public GeoLocation Location { get; set;}
    }

    public class UserPasswordParameters
    {
        public string AppName { get; set;}
        public string UserName { get; set;}
        public string Password { get; set;}
        public string DeviceId { get; set;}
        public GeoLocation Location { get; set;}
    }


    public class UserSessionParameters
    {
        public int UserId { get; set;}
        public string SessionId { get; set;}
        public string AppName { get; set;}
        public string DeviceId  { get; set;}
        public GeoLocation Location { get; set;}
    }
     

    public class GeoLocation
    {
        public double Latitude { get; set;}
        public double Longitude { get; set;}   
    }
}

