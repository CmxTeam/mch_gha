﻿

using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class CheckList
    {
        private static CheckList instance;
        private CheckList() {}

        public static CheckList Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new CheckList();
                }
                return instance;
            }
        }

        public CheckListTasks GetCheckListTasks(long warehouseId, int taskTypeId, long userId, CheckListStatusTypes status)
        {
            CheckListTasks result = new CheckListTasks();
 
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHChecklist/GetCheckListTasks");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("menuId", taskTypeId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("status", ((int)status).ToString());
            result = WebService.Instance.JsonGetMethod<CheckListTasks>(url,parameters);


//            if (result.Data == null)
//            {
//                result.Data = new List<CheckListShipmentItem>();
//                result.Transaction = new CommunicationTransaction();
//                result.Transaction.Status = true;
//                CheckListShipmentItem i = new CheckListShipmentItem();
//                i.AwbId = 1094;
//                i.TaskId = 237;
//                i.AwbNumber = "000-00000000";
//                i.CarrierCode = "TK";
//                i.FlightNumber = "0000";
//                i.ETD = DateTime.Now;
//                i.Pieces = 0;
//                i.Locations = "Area 1, Area 2";
//                i.Progress = 99.9;
//                i.Status = CheckListStatusTypes.In_Progress;
//                result.Data.Add(i);
//
//
//                CheckListShipmentItem k = new CheckListShipmentItem();
//                k.AwbId = 1094;
//                k.TaskId = 237;
//                k.AwbNumber = "000-00000000";
//                k.CarrierCode = "EI";
//                k.FlightNumber = "0000";
//                k.ETD = DateTime.Now;
//                k.Pieces = 0;
//                k.Locations = "Area 1, Area 2";
//                k.Progress = 99.9;
//                k.Status = CheckListStatusTypes.In_Progress;
//                result.Data.Add(k);
//
//
//                CheckListShipmentItem j = new CheckListShipmentItem();
//                j.AwbId = 1094;
//                j.TaskId = 237;
//                j.AwbNumber = "000-00000000";
//                j.CarrierCode = "CX";
//                j.FlightNumber = "0000";
//                j.ETD = DateTime.Now;
//                j.Pieces = 0;
//                j.Locations = "Area 1, Area 2";
//                j.Progress = 99.9;
//                j.Status = CheckListStatusTypes.In_Progress;
//                result.Data.Add(j);
//            }


            return result;
        }

        public CheckListData GetCheckList( long taskId, long awbId, long userId)
        {
            CheckListData result  = new CheckListData();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHChecklist/GetCheckList");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("awbId", awbId.ToString());
            result = WebService.Instance.JsonPostMethod<CheckListData>(url,parameters);
            return result;
        }


        public CommunicationTransaction FinalizeCheckList(CheckListResultsParam parameters)
        {
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHChecklist/FinalizeCheckList");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction SaveCheckList(CheckListResultsParam parameters)
        {
            CommunicationTransaction result  = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHChecklist/SaveCheckList");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

    }


    public class CheckListData
    {
        public CheckListModel Data {get;set;}
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class CheckListHeader
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<CheckListItem> Items{get;set;}
    }

    public class CheckListItem 
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int ? ParentId { get; set; }
        public string Options { get; set; }
        public IEnumerable<CheckListItem> SubItems { get;set; }
        public string SelectedOption { get; set; }
        public DateTime? CreatedDate {get;set;}
        public DateTime ? LastUpdated {get;set;}
        public string Comment {get;set;}
        public int? Number { get; set; }
    }

    public class CheckListModel 
    {
        public int? AccountId { get; set; }
        public int EntityTypeId {get;set;}
        public long EntityId {get;set;}
        public string Username {get;set;}
        public string Place {get;set;}
        public DateTime ? CommentedOn {get;set;}
        public string  Comments {get;set;}
        public long ? TaskId { get; set; }
        public string ShipmentNumber { get; set; }
        public string SignatureData { get; set; }
        public List<CheckListHeader> CheckList { get; set; }
        public string Year {get;set;}
        public string Title {get;set;}
        public string Note {get;set;}
    }
        
    public class CheckListResultsParam
    {
        public long UserId { get; set; }
        public long EntityId { get; set; }
        public string SignatureData { get; set; }
        public long  ? TaskId { get; set; }
        public string Comments { get; set; }
        public List<CheckListResultItem> Items { get; set; }
    }

    public class CheckListResultItem
    {
        public long ItemId { get; set; }
        public string OptionName { get; set; }
        public CheckListResultItem(long itemId,string optionName)
        {
            this.ItemId = itemId;
            this.OptionName = optionName;
        }
    }
 
    public class CheckListTasks
    {
        public List<CheckListShipmentItem> Data {get;set;}
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class CheckListShipmentItem
    {
        public CheckListStatusTypes Status { get; set;}
        public long TaskId { get; set; }
        public long AwbId { get; set; }
        public string AwbNumber { get; set; }
        public string CarrierCode { get; set; }
        public string FlightNumber { get; set; }
        public DateTime ? ETD { get; set; }
        public int Pieces { get; set; }
        public string Locations { get; set; }
        public double Progress { get; set; }
        public override string ToString()
        {
            return AwbNumber;
        }
    }

    public enum CheckListStatusTypes
    {
        Open=0,
        Pending = 1,
        In_Progress = 2,
        Completed = 3,
    }


}  
