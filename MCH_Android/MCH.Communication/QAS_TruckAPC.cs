﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    //methords
    public class QAS_TruckAPC
    {
        private static QAS_TruckAPC instance;
        private QAS_TruckAPC() {}

        public static QAS_TruckAPC Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new QAS_TruckAPC();
                }
                return instance;
            }
        }

        public QASTruckTask CreateTruck(string truckNumber, long warehouseLocationId, long userID, DateTime clientdatetime,TruckStatuses statusId)
        {
            CreateTruckParam model = new CreateTruckParam();
            model.ClientDateTime = DateTime.Now;
            model.TruckNumber = truckNumber;           
            model.UserId = userID;
            model.WarehouseLocationId = warehouseLocationId;
            model.StatusId = statusId;

            QASTruckTask result = new QASTruckTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/CreateTruck");
            result = WebService.Instance.JsonPostMethod<QASTruckTask>(url,model);
            return result;
        }

        public QASTruckTask GetTruck(string truckNumber)
        {
            QASTruckTask result = new QASTruckTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/GetTruck");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("truckNumber", truckNumber);
            result = WebService.Instance.JsonGetMethod<QASTruckTask>(url,parameters);
            return result;   


        }

        public QASTruckAPCTask CreateNewApc(string barcode, DateTime StaleDate, long userId, long truckId)
        {
            CreateApcParam model = new CreateApcParam();
            model.Barcode = barcode;
            model.ClientDateTime = DateTime.Now;
            model.StaleDate = StaleDate;
            if (truckId != 0)
            {
                model.TruckId = truckId;
            }

            model.UserId = userId;
            model.TypeId = enmBmcApcTypes.APC;

            QASTruckAPCTask result = new QASTruckAPCTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/CreateNewApc");
            result = WebService.Instance.JsonPostMethod<QASTruckAPCTask>(url,model);
            return result;

        }

        public QASTruckAPCTasks GetApcByStatus(APCStatusTypes statusId)
        {

            QASTruckAPCTasks result = new QASTruckAPCTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/GetApcByStatus");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("statusId", ((int)statusId).ToString());
            result = WebService.Instance.JsonGetMethod<QASTruckAPCTasks>(url,parameters);
            return result;   

        }

        // need to create APC if not found APC_Pending
        public QASTruckAPCTask GetApcByCode(string barcode)
        {

            QASTruckAPCTask result = new QASTruckAPCTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/GetApcByCode");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("barcode", barcode);
            result = WebService.Instance.JsonGetMethod<QASTruckAPCTask>(url,parameters);
            return result;   


        }

        public QASTruckAPCTasks GetApcByTruck(long truckId, APCStatusTypes statusId)
        {

            QASTruckAPCTasks result = new QASTruckAPCTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/GetApcByTruck");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("truckId", truckId.ToString());
            parameters.Add("statusId", ((int)statusId).ToString());
            result = WebService.Instance.JsonGetMethod<QASTruckAPCTasks>(url,parameters);
            return result;   


        }



        public CommunicationTransaction UpdateApcStaleDate(long APCId,DateTime? mailDate, long userId)
        {
            UpdateApcParam model = new UpdateApcParam();
            model.ClientDateTime = DateTime.Now;
            model.Id = APCId;
            model.StateDate = mailDate;

            model.UserId = userId;

            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/UpdateApcStaleDate");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;


        }
        public CommunicationTransaction UpdateAPCStatus(long APCId, APCStatusTypes statusID, bool IsReturnedEmpty,long userId)
        {
            UpdateApcParam model = new UpdateApcParam();
            model.ClientDateTime = DateTime.Now;
            model.Id = APCId;
            model.IsReturnedEmpty= IsReturnedEmpty;
            model.UserId = userId;
            model.StatusId = statusID;

            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/UpdateApcStatus");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;
        }




        public CommunicationTransaction UpdateUSPSTruckStatus(TruckStatuses statusId,long truckId,long userId)
        {
            UpdateTruckStatusParam model = new UpdateTruckStatusParam();
            model.ClientDateTime = DateTime.Now;
            model.StatusId = statusId;
            model.TruckId = truckId;
            model.UserId = userId;
            model.WarehouseLocationId = null;// warehouseLocationId;

            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/UpdateUSPSTruckStatus");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;

        }

        public CommunicationTransaction LoadApcTotruck (long ApcId, DateTimeOffset clientDateTime, long userID, DateTime ? StaleDate, long USPStruckID, string barcode)
        {
            LoadApcToTruckParam model = new LoadApcToTruckParam();
            if (ApcId != 0)
            {
                model.ApcId = ApcId;
            }

            model.ClientDateTime = DateTime.Now;
            model.StaleDate = StaleDate;
            model.TruckId = USPStruckID;
            model.UserId = userID;
            model.Barcode = barcode;
            model.TypeId = enmBmcApcTypes.APC;

            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/apc/LoadApcToTruck");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;

        }
    }

    // Objects

    public class QASTruckAPCTask
    {
        public ApcItem Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASTruckAPCTasks
    {
        public List<ApcItem> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }



    public class ApcItem
    {
        public long Id { get; set; }
        public string Barcode { get; set; }
        public DateTimeOffset? DateTimeArrived { get; set; }
        public DateTimeOffset? DateTimeDeparted { get; set; }
        public long ? TruckArrivedId { get; set; }
        public long ? TruckReturnedId { get; set; }
        public DateTime ? StaleDate { get; set; }
        public StatusDto Status { get; set; }
        public override string ToString()
        {
            return string.Format("{0}",Barcode);
        }
    }



    public class QASTruckTask
    {
        public TruckItem Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class TruckItem
    {
        public long TruckID { get; set; }
        public string TruckNumber { get; set; }
        public StatusDto Status { get; set; }
        public WarehouseLocationDto Location {get;set;}

    }

    public class StatusDto  //(this class is being used in many other return objects)
    {
        public long Id { get; set; }
        public string DisplayName { get; set; }
    }



    public enum APCStatusTypes
    { 
        ALL = -1,
        RECEIVED = 300,
        PROCESSED = 301,
        READY_TO_RETURN = 302,
        LOADED = 303,
        REMOVE = 305,
    }

    public enum TruckStatuses
    {        
        ARRIVED_FOR_DROPOFF = 600,
        ARRIVED_FOR_PICKUP =601,
        CLOSED =602,
    }

    public class UpdateTruckStatusParam
    {
        public long UserId { get; set; }
        public long ? WarehouseLocationId { get; set; }
        public DateTimeOffset ClientDateTime { get; set; }
        public long TruckId { get; set; }
        public TruckStatuses StatusId { get; set; }
    }

    public class CreateApcParam 
    {
        public long UserId { get; set; }

        public DateTimeOffset ClientDateTime { get; set; }
        public string Barcode { get; set; }
        public DateTime? StaleDate { get; set; }
        public long ? TruckId { get; set; }
        public enmBmcApcTypes ? TypeId { get; set; }
    }
    public enum enmBmcApcTypes :int
    {
        APC = 2,
        BMC = 1
    }

    public class CreateTruckParam
    {
        public string TruckNumber { get; set; }
        public long UserId { get; set; }
        public long ? WarehouseLocationId { get; set; }
        public DateTimeOffset ClientDateTime { get; set; }
        public TruckStatuses StatusId { get; set; }
    }

    public class UpdateApcParam
    {
        public long UserId { get; set; }
        public DateTimeOffset ClientDateTime { get; set; }
        public long Id { get; set; }
        public DateTime ? StateDate { get; set; }
        public APCStatusTypes ? StatusId { get; set; }
        public bool? IsReturnedEmpty { get; set; }

    }

    public class LoadApcToTruckParam
    {
        public long UserId { get; set; }

        public DateTimeOffset ClientDateTime { get; set; }
        public long ? ApcId { get; set; }
        public DateTime? StaleDate { get; set; }
        public long TruckId { get; set; }
        public string Barcode { get; set; }
        public enmBmcApcTypes TypeId { get; set; }
    }
}



