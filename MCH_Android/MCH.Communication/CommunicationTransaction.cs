﻿using System;

namespace MCH.Communication
{
    public class CommunicationTransaction
    {
        public bool Status {get;set;}
        public string Content {get;set;}
        public string Error {get;set;}
        public string Url {get;set;}
        public string ErrorCode {get;set;}
        public string Message {get;set;}
        public string Json {get;set;}

        public static CommunicationTransaction GetCommunicationTransaction()
        {
            CommunicationTransaction t = new  CommunicationTransaction();
            t.Status = true;
            return t;
        }

    }



}

