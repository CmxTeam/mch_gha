﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace MCH.Communication
{
    public class ForkLift
    {
        public ForkLift()
        {
        }
    }

    public class ForkLiftView 
    {
        public List<ForkLiftViewItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class ForkLiftViewItem
    {
 
        public string Reference { get; set; }
        public ReferenceTypes ReferenceType { get; set; }
        public int TotalPieces   {get;set;}
        public int ForkliftPieces   {get;set;}
        public int ReceivedPieces   {get;set;}
        public int AvailablePieces   {get;set;}
        public string Locations   {get;set;}                 
        public long DetailId   {get;set;}
    }


    public enum ReferenceTypes
    {
        AWB = 2, 
        HWB = 3, 
        ULD = 4
    }


}

