﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class Inventory
    {
        private static Inventory instance;
        private Inventory() {}

        public static Inventory Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new Inventory();
                }
                return instance;
            }
        }

        public CommunicationTransaction FinalizeInventory(long userId, long taskId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/FinalizeInventory");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction SetLocationAsEmpty(long userId, long locationId, long taskId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/SetLocationAsEmpty");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }


        public CommunicationTransaction ScanShipment(long userId, long awbId,long hwbId,  long locationId, int pieces,long taskId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/ScanShipment");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("awbId", awbId.ToString());
            parameters.Add("hwbId", hwbId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("pieces", pieces.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;

        }

        public InventoryLocationView GetMyInventory(long userId,long taskId)
        {
            InventoryLocationView result = new InventoryLocationView();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/GetMyInventory");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<InventoryLocationView>(url,parameters);
            return result;
        }


        public InventoryShipments GetMyInventoryShipments(long userId,long locationId,long taskId)
        {
            InventoryShipments result = new InventoryShipments();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/GetMyInventoryShipments");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<InventoryShipments>(url,parameters);
            return result;
        }

        public CommunicationTransaction ResetInventory(long awbId, long hwbId,  long taskId,long userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/ResetInventory");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("awbId", awbId.ToString());
            parameters.Add("hwbId", hwbId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }


        public CommunicationTransaction RelocatePieces(long awbId ,long hwbId , long oldLocationId, long newLocationId, int pcs, long taskId, long userId)
        {
            if (awbId == 0)
            {
                return RelocateHwbPieces(hwbId, oldLocationId, newLocationId, pcs, taskId, userId);
            }
            else
            {
                return RelocateAwbPieces(awbId, oldLocationId, newLocationId, pcs, taskId, userId);
            }
        }


 

        private CommunicationTransaction RelocateAwbPieces(long awbId , long oldLocationId, long newLocationId, int pcs, long taskId, long userId)
        {
        
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/RelocateAwbPieces");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("awbId", awbId.ToString());
            parameters.Add("oldLocationId", oldLocationId.ToString());
            parameters.Add("newLocationId", newLocationId.ToString());
            parameters.Add("pcs", pcs.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());

            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }


        public CommunicationTransaction DischargePieces(long awbId ,long hwbId  , long locationId, int pcs, long taskId, long userId,string truckNumber)
            {
                if (awbId == 0)
                {
                return DischargeHwbPieces(hwbId, locationId, pcs, taskId, userId,truckNumber);
                }
                else
                {
                return DischargeAwbPieces(awbId, locationId, pcs, taskId, userId,truckNumber);
                }
            }


        private CommunicationTransaction DischargeHwbPieces(long hwbId ,  long locationId, int pcs, long taskId, long userId,string truckNumber)
        {
            return null;
        }

            private CommunicationTransaction DischargeAwbPieces(long awbId  ,long locationId, int pcs, long taskId, long userId,string truckNumber)
            {
            return null;
            }

        public InventoryItemCount  GetPiecesCountInLocation(long awbId,long hwbId, long locationId,long taskId)
        {
            if (awbId == 0)
            {
                return GetHwbPiecesCountInLocation(hwbId, locationId,taskId);
            }
            else
            {
                return GetAwbPiecesCountInLocation(awbId, locationId,taskId);
            }
        }


        private CommunicationTransaction RelocateHwbPieces(long hwbId , long oldLocationId, long newLocationId, int pcs, long taskId, long userId)
        {

            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/RelocateHwbPieces");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hwbId", hwbId.ToString());
            parameters.Add("oldLocationId", oldLocationId.ToString());
            parameters.Add("newLocationId", newLocationId.ToString());
            parameters.Add("pcs", pcs.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());

            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }


        public CommunicationTransaction EditPiecesCount(long awbId ,long hwbId , long oldWarehouseLocationId, int pcs, long taskId, long userId)
        {
            if (awbId == 0)
            {
                return EditHwbPiecesCount(hwbId, oldWarehouseLocationId, pcs, taskId, userId);
            }
            else
            {
                return EditAwbPiecesCount(awbId, oldWarehouseLocationId, pcs, taskId, userId);
            }
        }

        private CommunicationTransaction EditHwbPiecesCount(long hwbId , long oldWarehouseLocationId,  int pcs, long taskId, long userId)
        {

      
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/EditHwbPiecesCount");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hwbId", hwbId.ToString());
            parameters.Add("oldWarehouseLocationId", oldWarehouseLocationId.ToString());
            parameters.Add("pcs", pcs.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());

            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }
        private CommunicationTransaction EditAwbPiecesCount(long awbId , long oldWarehouseLocationId,  int pcs, long taskId, long userId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/EditAwbPiecesCount");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("awbId", awbId.ToString());
            parameters.Add("oldWarehouseLocationId", oldWarehouseLocationId.ToString());
            parameters.Add("pcs", pcs.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());

            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }




//            private InventoryItemCount  GetHwbPiecesCountInLocation(long hwbId, long locationId)
//            {
//
//                InventoryItemCount result = new InventoryItemCount();
//                string url = "MCHCargoReceiver/GetHwbPiecesCountInLocation";
//                Dictionary<string, string> parameters = new Dictionary<string, string>();
//                parameters.Add("hwbId", hwbId.ToString());
//                parameters.Add("locationId", locationId.ToString());
//                result = WebService.Instance.JsonGetMethod<InventoryItemCount>(url,parameters);
//                return result;
//            }
//
//            private InventoryItemCount  GetAwbPiecesCountInLocation(long awbId, long locationId)
//            {
//
//                InventoryItemCount result = new InventoryItemCount();
//                string url = "MCHCargoReceiver/GetAwbPiecesCountInLocation";
//                Dictionary<string, string> parameters = new Dictionary<string, string>();
//                parameters.Add("awbId", awbId.ToString());
//                parameters.Add("locationId", locationId.ToString());
//                result = WebService.Instance.JsonGetMethod<InventoryItemCount>(url,parameters);
//                return result;
//            }



        private InventoryItemCount  GetHwbPiecesCountInLocation(long hwbId, long locationId,long taskId)
        {

            InventoryItemCount result = new InventoryItemCount();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/GetHwbPiecesCountInLocation");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("hwbId", hwbId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<InventoryItemCount>(url,parameters);
            return result;
        }

        private InventoryItemCount  GetAwbPiecesCountInLocation(long awbId, long locationId,long taskId)
        {

            InventoryItemCount result = new InventoryItemCount();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/GetAwbPiecesCountInLocation");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("awbId", awbId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<InventoryItemCount>(url,parameters);
            return result;
        }


        public InventoryShipment ValidateShipment(  string barcode,long warehouseId,long taskId,long userId)
        {
            InventoryShipment result = new InventoryShipment();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/ValidateShipment");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("barcode", barcode.ToString());
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<InventoryShipment>(url,parameters);
            return result;
        }

        public InventoryLocationStatus IsInventoryLocationEmpty(long userId,long locationId,long taskId)
        {
            InventoryLocationStatus result = new InventoryLocationStatus();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/IsInventoryLocationEmpty");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("locationId", locationId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<InventoryLocationStatus>(url,parameters);
            return result;
        }

        public TaskId GetInventoryTaskId(long userId,long warehouseId)
        {
            TaskId result = new TaskId();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/GetInventoryTaskId");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("warehouseId", warehouseId.ToString());
            result = WebService.Instance.JsonGetMethod<TaskId>(url,parameters);
            return result;
        }


        public InventoryShipments GetLocationsHistory(long warehouseId, string barcode, long taskId, long userId)
         {
            InventoryShipments result = new InventoryShipments();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/GetLocationsHistory");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("barcode", barcode.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<InventoryShipments>(url,parameters);
            return result;
         }


        public DiscrepancyItems GetInventoryDiscrepancies (long taskId)
        {
            DiscrepancyItems result = new DiscrepancyItems();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/MCHInventory/GetInventoryDiscrepancies");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<DiscrepancyItems>(url,parameters);
            return result;



//            DiscrepancyItems result = new DiscrepancyItems();
//            result.Transaction = new CommunicationTransaction();
//            result.Transaction.Status = true;
//
//            result.Data = new List<DiscrepancyItem>();
//
//
//            DiscrepancyItem a = new DiscrepancyItem();
//            a.Awb ="1111111";
//            a.AwbId = 1111;
//            a.Destination = "XXX";
//            a.Origin ="YYY";
//            a.DiscrepancyType = "SHORT";
//            a.DiscrepancyCount = 10;
//            a.ScannedPieces = 20;
//            a.TotalPieces = 30;
//
//            result.Data.Add(a);
//
//            return result;

        }

    }






    public class DiscrepancyItems
    {
        public List<DiscrepancyItem> Data{ get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }


    public class DiscrepancyItem
    {
        public long AwbId { get; set; }
        public string Awb { get; set; }
        public long HwbId { get; set; }
        public string Hwb { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public int TotalPieces { get; set; }
        public int ScannedPieces {get;set;}
        public string DiscrepancyType { get; set; }
        public int DiscrepancyCount { get; set; }

        public string Reference 
        {
            get
            {
                if (AwbId != 0)
                {
                    return Awb;
                }
                if (HwbId != 0)
                {
                    return Hwb;
                }
                return "";
            }
        }

        public long ReferenceId 
        {
            get
            {
                if (AwbId != 0)
                {
                    return AwbId;
                }
                if (HwbId != 0)
                {
                    return HwbId;
                }
                return 0;
            }
        }

    }

    public class TaskId
    {
        public long Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class InventoryLocationStatus
    {
        public bool Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class InventoryLocationView
    {
        public List<InventoryLocationViewItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class InventoryLocationViewItem
    {
        public int ScannedPieces {get;set;}
        public LocationItem location{get;set;}
    }


    public class InventoryShipment
    {
        public InventoryShipmentItem Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class InventoryShipments
    {
        public List<InventoryShipmentItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }


    public class InventoryItemCount
    {
        public int Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class InventoryShipmentItem
    {
        public long  AwbId  {get;set;}
        public string Awb  {get;set;}
        public long  HwbId  {get;set;}
        public string Hwb  {get;set;}
        public int TotalPieces {get;set;}
        public int ScannedPieces {get;set;}
        public string Locations {get;set;}
        public string LastLocations {get;set;}
        public string Origin {get;set;}
        public string Destination {get;set;}
        public double Weight  {get;set;}
        public string WeightUOM  {get;set;}

        public string Reference 
        {
            get
            {
                if (AwbId != 0)
                {
                    return Awb;
                }
                if (HwbId != 0)
                {
                    return Hwb;
                }
                return "";
            }
        }

        public long ReferenceId 
        {
            get
            {
                if (AwbId != 0)
                {
                    return AwbId;
                }
                if (HwbId != 0)
                {
                    return HwbId;
                }
                return 0;
            }
        }

        public int AvailablePieces  
        {
            get
            {
                int n = TotalPieces - ScannedPieces;
                if (n < 0)
                {
                    return 0;
                }
                else
                {
                    return n;
                }
            }
        }


    }

}











// 
//using System;
//using System.Collections.Generic;
//using Newtonsoft.Json;
//using System.Collections;
//using System.Text;
//
//namespace MCH.Communication
//{
//    public class Inventory
//    {
//        private static Inventory instance;
//        private Inventory() {}
//
//        public static Inventory Instance
//        {
//            get 
//            {
//                if (instance == null)
//                {
//                    instance = new Inventory();
//                }
//                return instance;
//            }
//        }
//
//        public CommunicationTransaction FinalizeInventory(long userId, long taskId)
//        {
//            CommunicationTransaction result = new CommunicationTransaction();
//            result.Status =true;
//            return result;
//        }
//
//        public CommunicationTransaction SetLocationAsEmpty(long userId, long locationId, long taskId)
//        {
//
//
//            CommunicationTransaction result = new CommunicationTransaction();
//            result.Status =true;
//            return result;
//
////            CommunicationTransaction result = new CommunicationTransaction();
////            string url = "MCHInventory/SetLocationAsEmpty";
////            Dictionary<string, string> parameters = new Dictionary<string, string>();
////            parameters.Add("userId", userId.ToString());
////            parameters.Add("locationId", locationId.ToString());
////            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
////            return result;
//        }
//
//
//        public CommunicationTransaction ScanShipment(long userId, long awbId,long hwbId,  long locationId, int pieces,long taskId)
//        {
//
//            CommunicationTransaction result = new CommunicationTransaction();
//            result.Status =true;
//            return result;
//
////                        CommunicationTransaction result = new CommunicationTransaction();
////                        string url = "MCHInventory/ScanShipment";
////                        Dictionary<string, string> parameters = new Dictionary<string, string>();
////                        parameters.Add("userId", userId.ToString());
////                        parameters.Add("locationId", locationId.ToString());
////                        parameters.Add("awbId", awbId.ToString());
////                        parameters.Add("pieces", pieces.ToString());
////                        result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
////                        return result;
//            
//        }
//
//        public InventoryLocationView GetMyInventory(long userId,long taskId)
//        {
//            InventoryLocationView result = new InventoryLocationView();
//            result.Transaction = new CommunicationTransaction();
//            result.Transaction.Status = true;
//            result.Data = new List<InventoryLocationViewItem>();
//
//            InventoryLocationViewItem a = new InventoryLocationViewItem();
//            a.location = new LocationItem(1,"AAA","CAAA","B");
//            a.ScannedPieces = 4;
//            result.Data.Add(a);
//
//
//            InventoryLocationViewItem b = new InventoryLocationViewItem();
//            b.location = new LocationItem(1,"BBB","CBBB","D");
//            b.ScannedPieces = 5;
//            result.Data.Add(b);
//
//            return result; 
//        }
//
//
//        public InventoryShipments GetMyInventoryShipments(long userId,long locationId,long taskId)
//        {
//            InventoryShipments result = new InventoryShipments();
//            result.Transaction = new CommunicationTransaction();
//            result.Transaction.Status = true;
//            result.Data = new List<InventoryShipmentItem>();
//            result.Data.Add(ValidateShipment( "235-47576082",1,1,1).Data);
//
//            return result;
//        }
//
//        public InventoryShipment ValidateShipment(  string barcode,long warehouseId,long taskid,long userId)
//        {
//            
//
//            InventoryShipment result = new InventoryShipment();
//            result.Transaction = new CommunicationTransaction();
//            result.Transaction.Status = true;
//            result.Data = new InventoryShipmentItem();
//            result.Data.Awb = "11111111";
//            result.Data.AwbId = 101;
//            result.Data.Destination = "JFK";
//            result.Data.Origin = "TLV";
//            result.Data.Locations = "LOC 1";
//            result.Data.ScannedPieces = 3;
//            result.Data.TotalPieces = 10;
//           
//            result.Data.Weight = 23.3;
//            result.Data.WeightUOM = "Kg";
//
//            if (barcode == "235-47576082")
//            {
//                result.Data.Awb = "235-47576082";
//            }
//            else if (barcode == "11111111")
//            {
//                result.Data.Awb = "11111111";
//            }
//            else if (barcode == "22222222")
//            {
//                result.Data.Awb = barcode;
//                result.Data.ScannedPieces = 4;
//                result.Data.TotalPieces = 4;
//            }
//            else
//            {
//                result.Data.AwbId = 0;
//            }
//            return result; 
//
////                        InventoryShipment result = new InventoryShipment();
//            //                        string url = "MCHInventory/ValidateShipment";
////                        Dictionary<string, string> parameters = new Dictionary<string, string>();
////                        parameters.Add("userId", userId.ToString());
//            //                        parameters.Add("barcode", barcode.ToString());
////                        result = WebService.Instance.JsonGetMethod<InventoryShipment>(url,parameters);
////                        return result; 
//        }
//      
//        public InventoryLocationStatus IsInventoryLocationEmpty(long userId,long locationId,long taskId)
//        {
//            InventoryLocationStatus result = new InventoryLocationStatus();
//            result.Transaction = new CommunicationTransaction();
//            result.Transaction.Status = true;
//            result.Data = true;
//            return result;
//        }
//
//        public TaskId GetInventoryTaskId(long userId,long warehouseId)
//        {
//            TaskId result = new TaskId();
//            result.Transaction = new CommunicationTransaction();
//            result.Transaction.Status = true;
//            result.Data = 1234;
//            return result;
//        }
//
//    }
//  
//    public class TaskId
//    {
//        public long Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }
//
//    public class InventoryLocationStatus
//    {
//        public bool Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }
//
//    public class InventoryLocationView
//    {
//        public List<InventoryLocationViewItem> Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }
//
//    public class InventoryLocationViewItem
//    {
//        public int ScannedPieces {get;set;}
//        public LocationItem location{get;set;}
//    }
//  
//
//    public class InventoryShipment
//    {
//        public InventoryShipmentItem Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }
//
//    public class InventoryShipments
//    {
//        public List<InventoryShipmentItem> Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }
//
//    public class InventoryShipmentItem
//    {
//        public long  AwbId  {get;set;}
//        public string Awb  {get;set;}
//        public long  HwbId  {get;set;}
//        public string Hwb  {get;set;}
//        public int TotalPieces {get;set;}
//        public int ScannedPieces {get;set;}
//        public string Locations {get;set;}
//        public string Origin {get;set;}
//        public string Destination {get;set;}
//        public double Weight  {get;set;}
//        public string WeightUOM  {get;set;}
//
//        public string Reference 
//        {
//            get
//            {
//                if (AwbId != 0)
//                {
//                    return Awb;
//                }
//                if (HwbId != 0)
//                {
//                    return Hwb;
//                }
//                return "";
//            }
//        }
//
//        public int AvailablePieces  
//        {
//            get
//            {
//                int n = TotalPieces - ScannedPieces;
//                if (n < 0)
//                {
//                    return 0;
//                }
//                else
//                {
//                    return n;
//                }
//            }
//        }
//
//
//    }
//
//}
//
//
//
//
// 