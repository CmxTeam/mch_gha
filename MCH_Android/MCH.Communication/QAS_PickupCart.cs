﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    //methords
    public class QAS_PickupCart
    {
        private static QAS_PickupCart instance;
        private QAS_PickupCart() {}

        public static QAS_PickupCart Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new QAS_PickupCart();
                }
                return instance;
            }
        }

        // Flight
        public QASPickupFlightTasks GetFlights(bool assignedToUser)
        {            
            GetFlightsParam model = new GetFlightsParam();
            model.AssignedToUser = assignedToUser;

            QASPickupFlightTasks result = new QASPickupFlightTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/scannerinbound/getflights");
            result = WebService.Instance.JsonPostMethod<QASPickupFlightTasks>(url,model);
            return result; 

        }


        public QASPickupCartTasks GetFlightCarts(long flightId)
        {
            QASPickupCartTasks result = new QASPickupCartTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/scannerinbound/GetFlightcarts");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("flightId", flightId.ToString());
            result = WebService.Instance.JsonGetMethod<QASPickupCartTasks>(url,parameters);
            return result;   


        }
       

        // CompleteStatus (partial =501, complete=502), Cart statusID will be changed to RETRIEVED_BY_RUNNER 113
        public CommunicationTransaction RetrieveCartsFromFlight(long UserID, List<string>  CartBarcodes, long FlightId, decimal latitude, decimal longitude)
        {
            Coordinates coor = new Coordinates();
            coor.Latitude = latitude;
            coor.Longitude = longitude;

            RetrieveCartsFromFlightParam model = new RetrieveCartsFromFlightParam();
            model.UserId = UserID;
            model.CartBarcodes = CartBarcodes;
            model.FlightId = FlightId;
            model.Coordinates = coor;           
            model.ClientDatetime = DateTime.Now;

            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/scannerinbound/RetreiveCartsFromFlight");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;
        }

        public CommunicationTransaction UpdateFlightCartStatus(FlightStatusTypes FlightCartStatus,long flightId, long userId)
        {

            UpdateFlightCartStatusParam model = new UpdateFlightCartStatusParam();
            model.ClientDateTime = DateTime.Now;
            model.FlightId = flightId;
            model.StatusId = FlightCartStatus;
            model.UserId = userId;

            CommunicationTransaction result = new CommunicationTransaction();           
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/flightactions/updateflightcartstatus");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;
        }


      
    }

    // Objects

    public class QASPickupCartTask
    {
        public CartListItem Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASPickupCartTasks
    {
        public List<CartListItem> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class GetPickupCartsParam
    {
        public enmCartDirections ? CartDirection { get; set; } // instead of bool ? IsInbound
        public long ? UserId { get; set; }
        public long[] StatusIds { get; set; }
        public int ? WarehouseLocationId { get; set; }
    }

    public class PickupCartStatusUpdateParam
    {
        public long CartId{get;set;}
        public long UserId{get;set;}
        public DateTimeOffset ClientDateTime { get; set; }
        public enmCartStatus ? StatusId { get; set; }
    }

    public class RetrieveCartsFromFlightParam
    {
        public long UserId { get; set; }
        public List<string>  CartBarcodes { get; set; }
        public long FlightId { get; set; }
        public DateTimeOffset ClientDatetime { get; set; }
        public Coordinates Coordinates { get; set; }
    }


    public class Coordinates
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }



    // Flight Objects
    public enum FlightStatusTypes
    {
        Inbound_Pending = 500,  
        Inbound_PickupPartial = 501,
        Inbound_PickupComplete = 502,
    }

    public class QASPickupFlightTasks
    {
        public List<FlightListItem> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASPickupFlightTask
    {
        public FlightListItem Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class  FlightListItem
    {

        public long FlightId { get; set; }
        public string FlightNumber { get; set; }
        public AirportDto Departure { get; set; }
        public AirportDto Arrival { get; set; } 
        public DateTimeOffset ? ArrivalTimeLocal { get; set; }
        public string ArrivalGate { get; set; }
        public string CarrierCode { get; set; }
        public long ? RunnerId { get; set; }
        public long ? RunnerShellId { get; set; }      
        public long ? StatusId { get; set; }
        public FlightStatusTypes Status { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}",FlightNumber, Departure.IATACode, ArrivalGate);
        }
    }
 

    public class UpdateFlightCartStatusParam
    {
        public long UserId { get; set; }
        public long FlightId { get; set; }
        public FlightStatusTypes StatusId { get; set; }
        public DateTimeOffset ClientDateTime { get; set; }
    }

    public class GetFlightsParam
    {
        public bool  AssignedToUser { get; set; }
       
    }
}



