﻿
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    //methords
    public class QAS_Tug
    {
        private static QAS_Tug instance;
        private QAS_Tug() {}

        public static QAS_Tug Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new QAS_Tug();
                }
                return instance;
            }
        }




        public QASTugTask GetTug(string barcode)
        {

            QASTugTask result = new QASTugTask();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/tug/gettug");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("barcode", barcode);
            result = WebService.Instance.JsonGetMethod<QASTugTask>(url,parameters);
            return result;   

        }

       
        public QASTugTasks GetTugs(long ? userId, TugStatusTypes ? StatusId)
        {
            GetTugsParams model = new GetTugsParams();

            if (StatusId == TugStatusTypes.AVAILABLE)
            {
                model.UserId = null;
            }
            else
            {
                model.UserId = userId;
            }

            model.StatusId = StatusId;

            QASTugTasks result = new QASTugTasks();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/tug/gettugs");
            result = WebService.Instance.JsonPostMethod<QASTugTasks>(url,model);
            return result;



        }


 
        public CommunicationTransaction UpdateTugStatus (long TugID, long UserID, TugStatusTypes NewStatusID)
        {

            UpdateTugParams model = new UpdateTugParams();
            model.TugId = TugID;
            model.UserId = UserID;
            model.NewStatusId = NewStatusID;
            model.ClientDateTime = DateTime.Now;

            CommunicationTransaction result = new CommunicationTransaction();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/tug/UpdateTugStatus");
            result = WebService.Instance.JsonPostMethod<CommunicationTransaction>(url,model);
            return result;
        }

        public QASTugGPSInterval GPSinterval()
        {
            QASTugGPSInterval result = new QASTugGPSInterval();
            string url = WebService.Instance.BuildUrl( WebService.Instance.AppPath, "/common/getapplicationsetting");
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("settingName", "GPSinterval");
            result = WebService.Instance.JsonGetMethod<QASTugGPSInterval>(url,parameters);
            return result;    
        }
      
    }

    // Objects

    public class QASTugTask
    {
        public TugListItem Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASTugTasks
    {
        public List<TugListItem> Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class QASTugGPSInterval
    {
        public string Data { get; set; }
        public CommunicationTransaction Transaction{ get; set; }
    }

    public class GetTugsParams
    {
        public long ? UserId { get; set; }
        public TugStatusTypes ? StatusId { get; set; } // if StatusId is null, means All, as you can see there is no “ALL” item in enmTugStatusTypes enum, instead of it we better use nullable values.
    }

    public class UpdateTugParams
    {
        public long TugId { get; set; }
        public long UserId { get; set; }
        public TugStatusTypes NewStatusId { get; set; }
        public DateTimeOffset ClientDateTime { get; set; }
    }

    public class  TugListItem
    {

        public string TugNumber { get; set; }
        public string TugBarcode { get; set; }
        public long TugId { get; set; }
        public TugStatusTypes  StatusId { get; set; }
        public string Reference { 
            get
            {
                if (string.IsNullOrEmpty(TugNumber))
                {
                    return TugBarcode;
                }
                else
                {
                    return TugNumber;
                }
            }
        }
        public override string ToString()
        {
            return string.Format("{0} {1}",TugNumber,TugBarcode);
        }
    }



    public enum TugStatusTypes
    {
//        ALL = -1,
        CHECKED_OUT = 400,
        AVAILABLE = 401,
        OUT_OF_SERVICE = 402,
    }


}



