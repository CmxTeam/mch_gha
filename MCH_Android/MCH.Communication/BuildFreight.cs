﻿ 
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

namespace MCH.Communication
{
    public class BuildFreight
    {
        private static BuildFreight instance;
        private BuildFreight() {}

        public static BuildFreight Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new BuildFreight();
             }
             return instance;
          }
        }

        public CommunicationTransaction PrintUldTag(long userId, long flightManifestId, long uldId)
        {
            CommunicationTransaction result = new CommunicationTransaction();
            string url = "MCHBuildFreight/UpdateWeight";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction UpdateWeight(long userId, long taskId, long flightManifestId, long uldId, double weight, string weightUOM)
        {
 

            CommunicationTransaction result = new CommunicationTransaction();
                        string url = "MCHBuildFreight/UpdateWeight";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("weight", weight.ToString());
            parameters.Add("weightUOM", weightUOM.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction FinalizeBuild(long taskId, long flightManifestId, long userId)
        {

     

            CommunicationTransaction result = new CommunicationTransaction();
                        string url = "MCHBuildFreight/FinalizeBuild";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("userId", userId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public BuildFreightShipment ScanShipment(long userId, long taskId,long flightManifestId, string shipmentNumber)
        {



            BuildFreightShipment result = new BuildFreightShipment();
            string url = "MCHBuildFreight/ScanShipment";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("shipmentNumber", shipmentNumber.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            result = WebService.Instance.JsonGetMethod<BuildFreightShipment>(url,parameters);
            return result;
        }


        public CommunicationTransaction RemovePiecesFromUld(long userId, long uldId, long  detailId, int pieces)
        {
        

            CommunicationTransaction result = new CommunicationTransaction();
                        string url = "MCHBuildFreight/RemoveShipmentFromUld";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("pieces", pieces.ToString());
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("detailId", detailId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }
 


        public CommunicationTransaction DropForkliftPieces(long taskId, long userId,long uldId, long locationId, long detailId)
        {

            CommunicationTransaction result = new CommunicationTransaction();
            result.Status =true;
            return result;

//            CommunicationTransaction result = new CommunicationTransaction();
            //            string url = "MCHBuildFreight/DropForkliftPieces";
//            Dictionary<string, string> parameters = new Dictionary<string, string>();
//            parameters.Add("taskId", taskId.ToString());
//            parameters.Add("userId", userId.ToString());
//            parameters.Add("locationId", locationId.ToString());
//            parameters.Add("uldId", uldId.ToString());
//            parameters.Add("detailId", detailId.ToString());
//            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
//            return result;
        }



        public CommunicationTransaction DropAllForkliftPieces(long taskId, long userId,long uldId, long locationId)
        {

            CommunicationTransaction result = new CommunicationTransaction();
            result.Status =true;
            return result;

//            CommunicationTransaction result = new CommunicationTransaction();
            //            string url = "MCHBuildFreight/DropForkliftPieces";
//            Dictionary<string, string> parameters = new Dictionary<string, string>();
//            parameters.Add("taskId", taskId.ToString());
//            parameters.Add("userId", userId.ToString());
//            parameters.Add("locationId", locationId.ToString());
//            parameters.Add("uldId", uldId.ToString());
//            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
//            return result;
        }



        public CommunicationTransaction AddForkliftPieces(long detailsId, long taskId, long flightManifestId,int pieces, long userid)
        {


            CommunicationTransaction result = new CommunicationTransaction();
            result.Status =true;
            return result;

//            CommunicationTransaction result = new CommunicationTransaction();
            //            string url = "MCHBuildFreight/AddForkliftPieces";
//            Dictionary<string, string> parameters = new Dictionary<string, string>();
//            parameters.Add("detailsId", detailsId.ToString());
//            parameters.Add("taskId", taskId.ToString());
//            parameters.Add("pieces", pieces.ToString());
//            parameters.Add("userid", userid.ToString());
//            parameters.Add("flightManifestId", flightManifestId.ToString());
//            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
//            return result;
        }


        public CommunicationTransaction RemoveItemsFromForklift(long forkliftDetailsId, int count, long locationId)
        {

            CommunicationTransaction result = new CommunicationTransaction();
            result.Status =true;
            return result;

//            CommunicationTransaction result = new CommunicationTransaction();
            //            string url = "MCHBuildFreight/RemoveItemsFromForklift";
//            Dictionary<string, string> parameters = new Dictionary<string, string>();
//            parameters.Add("forkliftDetailsId", forkliftDetailsId.ToString());
//            parameters.Add("count", count.ToString());
            //            parameters.Add("locationId", locationId.ToString());
//            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
//            return result;
        }


        public ForkLiftView GetForkliftView(long taskId, long userId)
        {

            ForkLiftView result = new ForkLiftView();
            result.Transaction = new CommunicationTransaction();
            result.Transaction.Status = true;

            result.Data = new List<ForkLiftViewItem>();
         
            ForkLiftViewItem a = new ForkLiftViewItem();
            a.DetailId  = 111;
            a.Reference  ="172-12121212";
            a.ReferenceType = ReferenceTypes.AWB;
            a.TotalPieces   = 6;
            a.ForkliftPieces = 2;
            a.ReceivedPieces  = 1;
            a.AvailablePieces= 3;
            a.Locations  ="";           
         
            result.Data.Add(a);
             



            ForkLiftViewItem b = new ForkLiftViewItem();
            b.DetailId  = 222;
            b.ReferenceType = ReferenceTypes.AWB;
            b.Reference  ="172-2000000";
            b.TotalPieces   = 2;
            b.ForkliftPieces = 1;
            b.ReceivedPieces  = 1;
            b.AvailablePieces= 0;
            b.Locations  ="";           
            
            result.Data.Add(b);



            return result; 




//            ForkLiftView result = new ForkLiftView();
            //            string url = "MCHBuildFreight/GetForkliftView";
//            Dictionary<string, string> parameters = new Dictionary<string, string>();
//            parameters.Add("taskId", taskId.ToString());
//            parameters.Add("userId", userId.ToString());
//            result = WebService.Instance.JsonGetMethod<ForkLiftView>(url,parameters);
//            return result; 
        }


        public ForkLiftCount GetForkliftCount(long userId, long taskId)
        {
  
            ForkLiftCount result = new ForkLiftCount();
            string url = "mchapi/MCHCommonController/GetForkliftCount";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
  
            result = WebService.Instance.JsonGetMethod<ForkLiftCount>(url,parameters);
            return result;
        }

        public BuildFreightShipmentItems GetShipments(long userId, long flightManifestId, long taskId, BuildFreightStatusTypes status)
        {
 

            BuildFreightShipmentItems result = new BuildFreightShipmentItems();
            string url = "mchapi/MCHBuildFreight/GetShipments";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("status", ((int)status).ToString());
            result = WebService.Instance.JsonGetMethod<BuildFreightShipmentItems>(url,parameters);
            return result;
        }



//        public BuildFreightULDView GetBuildULDView(long detailId)
//        {
//            BuildFreightULDView result = new BuildFreightULDView();
//            string url = "MCHBuildFreight/GetBuildULDView";
//            Dictionary<string, string> parameters = new Dictionary<string, string>();
//            parameters.Add("detailId", detailId.ToString());
// 
//            result = WebService.Instance.JsonGetMethod<BuildFreightULDView>(url,parameters);
//            return result;
//        }
//
        public BuildFreightShipmentItems GetShipmentsByUld(long userId, long flightManifestId, long taskId, long uldId)
        {
 
            BuildFreightShipmentItems result = new BuildFreightShipmentItems();
            string url = "mchapi/MCHBuildFreight/GetShipmentsByUld";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<BuildFreightShipmentItems>(url,parameters);
            return result;
        }

        public BuildFreightTasks GetBuildFreightTasks(long warehouseId, long userId, BuildFreightStatusTypes status)
        {

            BuildFreightTasks result = new BuildFreightTasks();
            string url = "mchapi/MCHBuildFreight/GetBuildFreightTasks";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("warehouseId", warehouseId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("status", ((int)status).ToString());
            result = WebService.Instance.JsonGetMethod<BuildFreightTasks>(url,parameters);
            return result; 
        }

        public BuildFreightTask GetBuildFreightTask(long taskId, long flightManifestId)
        {
 

            BuildFreightTask result = new BuildFreightTask();
            string url = "mchapi/MCHBuildFreight/GetBuildFreightTask";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            result = WebService.Instance.JsonGetMethod<BuildFreightTask>(url,parameters);
            return result; 
        }




        public CommunicationTransaction DeleteUld(long userId, long flightManifestId,long uldId)
        {
 
            CommunicationTransaction result = new CommunicationTransaction();
            string url = "mchapi/MCHBuildFreight/DeleteUld";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("uldId", uldId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }


        public CommunicationTransaction StageLocationTask(long taskId, long userId, long locationId)
        {
 
            CommunicationTransaction result = new CommunicationTransaction();
            string url = "mchapi/MCHBuildFreight/StageLocationTask";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("locationId", locationId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction StageBuildULD(long uldId, long userId, long flightManifestId, long locationId)
        {
 

            CommunicationTransaction result = new CommunicationTransaction();
            string url = "mchapi/MCHBuildFreight/StageBuildULD";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("locationId", locationId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction EditUld(long uldId, long userId, long flightManifestId, long uldUnitTypeId, string serialNumber, string carrierCode)
        {
 

            CommunicationTransaction result = new CommunicationTransaction();
            string url = "mchapi/MCHBuildFreight/EditUld";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("uldUnitTypeId", uldUnitTypeId.ToString());
            parameters.Add("carrierCode", carrierCode.ToString());
            parameters.Add("serialNumber", serialNumber.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

        public CommunicationTransaction AddUld(long userId, long flightManifestId, long uldUnitTypeId, string serialNumber, string carrierCode, long taskId)
        {

 
            CommunicationTransaction result = new CommunicationTransaction();
            string url = "mchapi/MCHBuildFreight/AddUld";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("uldUnitTypeId", uldUnitTypeId.ToString());
            parameters.Add("carrierCode", carrierCode.ToString());
            parameters.Add("serialNumber", serialNumber.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }


        public BuildFreightUld GetBuildFreightUld(long userId, long flightManifestId, long uldId,long taskId)
        {
            BuildFreightUld result = new BuildFreightUld();
            string url = "mchapi/MCHBuildFreight/GetBuildFreightUld";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("taskId", taskId.ToString());
            result = WebService.Instance.JsonGetMethod<BuildFreightUld>(url,parameters);
            return result;
        }

        public BuildFreightUldListItems GetBuildFreightUldListItems(long userId, long flightManifestId,BuildFreightStatusTypes status, long taskId)
        {

 


            BuildFreightUldListItems result = new BuildFreightUldListItems();
            string url = "mchapi/MCHBuildFreight/GetBuildFreightUldListItems";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", userId.ToString());
            parameters.Add("flightManifestId", flightManifestId.ToString());
            parameters.Add("status", ((int)status).ToString());
            parameters.Add("taskId", taskId.ToString());

            result = WebService.Instance.JsonGetMethod<BuildFreightUldListItems>(url,parameters);
            return result;
        }

        public CommunicationTransaction SwitchBUPMode(long uldId, long userId, long manifestId,long taskId)
        {
 

            CommunicationTransaction result = new CommunicationTransaction();
            string url = "mchapi/MCHBuildFreight/SwitchBUPMode";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("taskId", taskId.ToString());
            parameters.Add("uldId", uldId.ToString());
            parameters.Add("userId", userId.ToString());
            parameters.Add("manifestId", manifestId.ToString());
            result = WebService.Instance.JsonGetMethod<CommunicationTransaction>(url,parameters);
            return result;
        }

    }
 
    public enum BuildFreightStatusTypes
    {

        All = 1,
        Completed = 2,
        In_Progress = 3, 
        Open = 4, 
        Pending = 5,

    }
 

    public class BuildFreightTask
    {
        public BuildFreightTaskItem Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class BuildFreightTasks
    {
        public List<BuildFreightTaskItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }
  
    public class BuildFreightTaskItem
     {
            public string CarrierCode{get;set;} 
            public string FlightNumber{get;set;}
            public DateTime  ETD{get;set;}
            public string Destination{get;set;}
            public int TotalUlds{get;set;}
            public int TotalAwbs{get;set;}
            public int TotalPieces{get;set;}
            public int ScannedUlds{get;set;}
            public int ScannedAwbs{get;set;}
            public int ScannedPieces{get;set;}
            public long TaskId{get;set;}
            public long FlightManifestId{get;set;}
//            public string StageLocation{get;set;}
//            public string StagedBy{get;set;}
//            public DateTime ? StagedDate{get;set;}
            public double PercentProgress {get;set;}
            public long Flag{get;set;}
            public string Locations {get;set;}
            public BuildFreightStatusTypes Status{get;set;}  

            public override string ToString()
            {
                return string.Format("{0} {1} {2}", CarrierCode, FlightNumber,  Destination);
            }

     }




 
//    public class BuildFreightULDView
//    {
//        public List<BuildFreightULDViewItem> Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }
//
//    public class BuildFreightULDViewItem
//    {
//        public long DetailId {get;set;}
//        public long AwbId {get;set;}
//        public int LoadCount {get;set;}
//        public string AWB {get;set;}
//    }

    public class BuildFreightShipment
    {
        public BuildFreightShipmentItem Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class BuildFreightShipmentItems
    {
        public List<BuildFreightShipmentItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class BuildFreightUld
    {
        public BuildFreightUldListItem Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class BuildFreightUldListItems
    {
        public List<BuildFreightUldListItem> Data{get;set;}
        public CommunicationTransaction Transaction{get;set;} 
    }

    public class BuildFreightUldListItem
    {
        public long UldId {get;set;}
        public string UldType {get;set;}
        public string Uld {get;set;}
        public string UldSerialNo {get;set;}
        public string UldPrefix{get;set;}
        public string CarrierCode {get;set;}
        public string Locations {get;set;}
        public bool IsBUP {get;set;}
        public int TotalPieces{get;set;}
        public int TotalAwbs{get;set;}
        public double Weight{get;set;}
        public double TareWeight{get;set;}
        public string WeightUOM{get;set;}
        public string StageLocation{get;set;}
        public string StagedBy{get;set;}
        public DateTime ? StagedDate{get;set;}
        public long Flag {get;set;}
        public BuildFreightStatusTypes Status{get;set;}


        public bool IsLoose
        {
            get
            {
                if (UldPrefix.ToLower() == "all")
                {
                    return true;
                }
                else
                {
                    return (UldPrefix.ToLower() == "loose") ? true : false;
                }

            
            }
        
        }



        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(UldPrefix + UldSerialNo);

            return sb.ToString();
        }
 

    }



    public class BuildFreightShipmentItem
    {
        public List<string> References  {get;set;}
        public long Flag  {get;set;}
        public long  AwbId  {get;set;}
        public long DetailId {get;set;}
        public string AWB  {get;set;}
        public int TotalPieces {get;set;}
        //public int ForkliftPieces  {get;set;}
        public int ScannedPieces {get;set;}
        //public int AvailablePieces {get;set;}
        public string Locations {get;set;}
     
        public string Origin {get;set;}
        public string Destination {get;set;}
        public BuildFreightStatusTypes Status{get;set;}
        public double PercentProgress {get;set;}
 
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(AWB + " ");
             
            if (References != null)
            {
                foreach (string s in References)
                {
                    sb.Append(s + " ");
                }
            }

            return sb.ToString();
        }

    }


//    public class ForkLiftView 
//    {
//        public List<ForkLiftViewItem> Data{get;set;}
//        public CommunicationTransaction Transaction{get;set;} 
//    }
//
//    public class ForkLiftViewItem
//    {
//        public long AwbId   {get;set;}
//        public long UldId   {get;set;}
//        public string Uld   {get;set;}
//        public string AWB   {get;set;}
//        public int TotalPieces   {get;set;}
//        public int ForkliftPieces   {get;set;}
//        public int ReceivedPieces   {get;set;}
//        public int AvailablePieces   {get;set;}
//        public string Locations   {get;set;}                 
//        public long DetailId   {get;set;}
//        //public int Counter   {get;set;}
//    }

}


 
