﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MCH.Communication;

namespace MCH
{

    public partial class RecoverFlight_CheckInUldActivity : BaseActivity
    {

        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;
        EditText search;
        TextView titleLabel; 
        ImageView optionButton;
        ImageView backButton;

        ImageView imageHeader;
        ImageButton DropDownCheck;
        ListView listView;
        ImageButton DropDownButton;
        TextView DropDownText;
        LinearLayout DropDownBox;

        TextView Flight;
        TextView Eta;
        TextView Reference;
        TextView Counters;
        TextView StatusDescription;

        TextView Progress;
        ProgressBar ProgressBar;
        ImageView Status;

        private void Initialize()
        {

            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            search = FindViewById<EditText>(Resource.Id.SearchText);
            EditTextListener = new EditTextEventListener(search);
            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
                {
                    string data = e.Data;
                    if (e.IsBarcode)
                    {
                        //Parse Barcode Data
                    }
                    RefreshData(data,e.IsBarcode);
                };

            DropDownText = FindViewById<TextView>(Resource.Id.DropDownText);
            ClearSearch = FindViewById<ImageButton>(Resource.Id.ClearSearchButton);
            SearchButton = FindViewById<ImageButton>(Resource.Id.SearchButton);
            titleLabel = FindViewById<TextView>(Resource.Id.HeaderText);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            imageHeader = FindViewById<ImageView>(Resource.Id.HeaderImage);
            DropDownCheck = FindViewById<ImageButton>(Resource.Id.DropDownCheck);
            DropDownButton= FindViewById<ImageButton>(Resource.Id.DropDownButton);
            DropDownBox= FindViewById<LinearLayout>(Resource.Id.DropDownBox);
            listView = FindViewById<ListView>(Resource.Id.GridControl);

            //titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + " - Check-In";
            titleLabel.Text = "Check-In";

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            ClearSearch.Click += OnClearSearch_Click;
            DropDownCheck.Click += OnCheckStatusButton_Click;
            DropDownButton.Click += OnDropDownButton_Click;
            DropDownBox.Click += OnDropDownButton_Click;
            DropDownText.Click += OnDropDownButton_Click;

            SearchButton.Click += OnSearchButton_Click;

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                imageHeader.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }


            //Find here new controls

            Flight = FindViewById<TextView>(Resource.Id.Flight);
            Eta = FindViewById<TextView>(Resource.Id.Eta);
            Reference = FindViewById<TextView>(Resource.Id.Reference);
            Counters = FindViewById<TextView>(Resource.Id.Counters);
            StatusDescription = FindViewById<TextView>(Resource.Id.StatusDescription);

            Progress = FindViewById<TextView>(Resource.Id.Progress);
            ProgressBar = FindViewById<ProgressBar>(Resource.Id.ProgressBar);
            Status= FindViewById<ImageView>(Resource.Id.Status);
        }

        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            RefreshData(string.Empty,false);
            search.Text = string.Empty;
            search.RequestFocus ();
        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {
            RefreshData(search.Text,false);
        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnCheckStatusButton_Click(object sender, EventArgs e)
        {

        }

        private void OnDropDownButton_Click(object sender, EventArgs e)
        {

            UldCheckInStatusTypes[] values = (UldCheckInStatusTypes[])Enum.GetValues(typeof(UldCheckInStatusTypes));
            PopupMenu menu = new PopupMenu(this, DropDownText);

            foreach (UldCheckInStatusTypes n in values)
            {
                UldCheckInStatusTypes en = (UldCheckInStatusTypes)Enum.Parse(typeof(UldCheckInStatusTypes), n.ToString());
                menu.Menu.Add(0,(int)en, (int)en, en.ToString().Replace("_"," "));
            }

            menu.MenuInflater.Inflate(Resource.Menu.PopupMenu, menu.Menu);
            menu.MenuItemClick += (s1, arg1) =>
                {
                    DropDownText.Text = arg1.Item.ToString();
                    search.Text = string.Empty;
                    RefreshData(string.Empty,false);
                };
            menu.Show(); 
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.Finalize),OptionActions.Finalize, Resource.Drawable.Validate));
            options.Add(new OptionItem(GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
            options.Add(new OptionItem(GetText(Resource.String.Refresh),OptionActions.Refresh, Resource.Drawable.Refresh));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Refresh:
                    search.Text = string.Empty;
                    RefreshData(string.Empty,false);
                    break;
                case  OptionActions.Finalize:
                    search.Text = string.Empty;
                    Finalize();
                    break;
                case  OptionActions.Camera:
                    GoToCamera(typeof(RecoverFlight_CheckInUldActivity),typeof( RecoverFlight_CheckInUldActivity),RecoverFlight_SessionState.CurrentFlightTask.TaskId);
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}



