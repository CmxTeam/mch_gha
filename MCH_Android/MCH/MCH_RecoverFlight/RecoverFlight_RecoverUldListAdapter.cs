﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{

 
    public class RecoverFlight_RecoverUldListAdapter : BaseAdapter<RecoverFlightUldItem> {

        List<RecoverFlightUldItem> items;
        Activity context;


        public RecoverFlight_RecoverUldListAdapter(Activity context, List<RecoverFlightUldItem> items  ): base()
        {


            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override RecoverFlightUldItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFlight_RecoverUldRow, null);

 
            TextView RowText = view.FindViewById<TextView>(Resource.Id.RowText);
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
 

            RowText.Text = string.Format("Uld: {0} - {1}", item.UldType, item.UldNumber);
             
          
            switch (item.Status)
            {
                case RecoverStatusTypes.Not_Recovered:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case RecoverStatusTypes.Recovered:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
            }

            return view;
        }






    }
}

