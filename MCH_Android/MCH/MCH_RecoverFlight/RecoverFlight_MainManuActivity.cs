﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_MainManuActivity : BaseActivity
    {

        RecoverFlightTasks recoverFlightTasks;
       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_MainManuLayout);
            Initialize();

            RefreshData(string.Empty,false);
        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
 
          
            try
            {
                GoToTask(recoverFlightTasks.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(RecoverFlightItem task)
        {

            //Toast.MakeText(this, task.Flight, ToastLength.Long).Show();

            RecoverFlight_SessionState.CurrentFlightTask=task;
         
           
            var nextScreen = new Intent(this, typeof(RecoverFlight_ActionSelectionActivity));
            //nextScreen.PutExtra("Title", taskTitle);
            //nextScreen.PutExtra("Icon", ApplicationSessionState.SelectedMenuItem.IconKey);
            StartActivity(nextScreen);
            this.Finish();


        }

        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {
 

                    TaskStatusTypes en = (TaskStatusTypes)Enum.Parse(typeof(TaskStatusTypes), DropDownText.Text.Replace(" ","_"));

                    recoverFlightTasks = MCH.Communication.RecoverFlight.Instance.GetRecoverFlightTasks(ApplicationSessionState.User.Data.UserId,en);

                    RunOnUiThread (delegate {
   
             


                        if(recoverFlightTasks.Transaction.Status && recoverFlightTasks.Data!=null)
                            {

                                if(searchData!=string.Empty)
                                {
                                    List<RecoverFlightItem> taskList = new List<RecoverFlightItem>();
                                    foreach (var task in recoverFlightTasks.Data)
                                    {
                                        if (task.ToString().ToLower().Contains(searchData.ToLower()))
                                        {
                                            taskList.Add(task);   
                                        }
                                    }
                                    recoverFlightTasks.Data =taskList;
                                }

                                if( recoverFlightTasks.Data.Count == 1 && isBarcode)
                                {
                                GoToTask( recoverFlightTasks.Data[0]);
                                }

                            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",recoverFlightTasks.Data.Count);
                            listView.Adapter = new RecoverFlight_MainManuListAdapter(this, recoverFlightTasks.Data);
                                listView.ItemClick -= OnListItemClick;
                                listView.ItemClick += OnListItemClick;  

                            }
                        else
                        {
                            MessageBox m = new MessageBox(this);
                            m.ShowMessage(recoverFlightTasks.Transaction.Error);
                        }
                    
                        if(isBarcode)
                        {
                            search.Text = string.Empty;
                        }
                        search.RequestFocus ();

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }

    }
}

