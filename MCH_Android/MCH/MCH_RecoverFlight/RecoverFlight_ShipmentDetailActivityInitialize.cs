﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCH
{

    public partial class RecoverFlight_ShipmentDetailActivity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;

        //Add here new controls
        Spinner txtState;
        Spinner txtCountry;
        Spinner txtService;

        TextView txtShipmentNumber;
        TextView txtName;
        TextView txtCity;
        TextView txtPostalCode;
        TextView txtAddress1;
        TextView txtAddress2;
        TextView txtContactName;
        TextView txtContactPhone;

        Button btnPrint;
        Button btnSave;
        Button btnPackages;

        ArrayAdapter stateAdapter;
        ArrayAdapter countryAdapter;
        ArrayAdapter serviceAdapter;

        private void Initialize()
        {
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            headerText.Text = "Edit Shipment";
            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }


            //Find here new controls

            txtState = FindViewById<Spinner> (Resource.Id.txtState);
            txtCountry = FindViewById<Spinner> (Resource.Id.txtCountry);
            txtService = FindViewById<Spinner> (Resource.Id.txtService);

            stateAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.United_States_Array, Android.Resource.Layout.SimpleSpinnerItem);
            stateAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
            txtState.Adapter = stateAdapter;

            countryAdapter = ArrayAdapter.CreateFromResource (this, Resource.Array.Country_Names_Array, Android.Resource.Layout.SimpleSpinnerItem);
            stateAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
            txtCountry.Adapter = countryAdapter;

            txtShipmentNumber= FindViewById<TextView> (Resource.Id.txtShipmentNumber);
            txtName= FindViewById<TextView> (Resource.Id.txtName);
            txtCity= FindViewById<TextView> (Resource.Id.txtCity);
            txtPostalCode= FindViewById<TextView> (Resource.Id.txtPostalCode);
            txtAddress1= FindViewById<TextView> (Resource.Id.txtAddress1);
            txtAddress2= FindViewById<TextView> (Resource.Id.txtAddress2);
            txtContactName= FindViewById<TextView> (Resource.Id.txtContactName);
            txtContactPhone= FindViewById<TextView> (Resource.Id.txtContactPhone);
        

            btnSave = FindViewById<Button> (Resource.Id.btnSave);
            btnSave.Click += OnbtnSave_Click;

            btnPackages = FindViewById<Button> (Resource.Id.btnPackages);
            btnPackages.Click += OnbtnPackages_Click;

            btnPrint = FindViewById<Button> (Resource.Id.btnPrint);
            btnPrint.Click += OnbtnPrint_Click;

        }




   
        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.Printers),OptionActions.Printers, Resource.Drawable.Printer));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Printers:
                    ShowPrinters();
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}


