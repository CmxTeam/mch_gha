﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_PackageActivity : BaseActivity
    {

        Packages packages;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_PackageLayout);
            Initialize();

            RefreshData(string.Empty,false);
        }


        private void DoBack()
        {
            StartActivity (typeof(RecoverFlight_CheckInUldActivity));
            this.Finish();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
      

            try
            {
                GoToTask(packages.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(PackageItem task)
        {

            MessageBox m;

            RecoverFlight_SessionState.CurrentPackageTask = task;

            if (task.Status == CheckInStatusTypes.CheckedIn)
            {
                m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {

                        if(result)
                        {
                            EditShipment();
                        }

                        RefreshData(string.Empty,false);
                    };
            
                m.ShowConfirmationMessage(Resource.String.RecoverFlight_Package_Already_CheckedIn_Message,Resource.String.Edit,Resource.String.Cancel);
               return;
            }

//            m = new MessageBox(this);
//            m.OnConfirmationClick += (bool result) => 
//                {
//                    if(result)
//                    {
//                      
//                         
//
//                        PackageStatus scanPackage = MCH.Communication.RecoverFlight.Instance.ScanPackage(task.Id,ApplicationSessionState.User.Data.UserId,RecoverFlight_SessionState.CurrentFlightTask.FlightId);
//                        if(scanPackage.Transaction.Status)
//                        {
//                            if(scanPackage.Data.IsHawbReady)
//                            {
//                                Generate(task.HawbId);
//                            }
//                            else
//                            {
//                                RefreshData(string.Empty,false);
//                            }
//                        }
//                        else
//                        {
//                            MessageBox msg = new MessageBox(this);
//                            msg.ShowAlertMessage(scanPackage.Transaction.Error);
//                        }
//
//                    }
//                };
//            m.ShowConfirmationMessage(Resource.String.RecoverFlight_Confirm_CheckIn_Piece);


 

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle(Resource.String.app_name);
            builder.SetMessage(Resource.String.RecoverFlight_Confirm_CheckIn_Piece);
            builder.SetIcon (Resource.Drawable.Icon);
            builder.SetCancelable(false);
            builder.SetNeutralButton (Resource.String.Edit,delegate
                {
                    StartActivity (typeof(RecoverFlight_ShipmentDetailActivity));
                    this.Finish();
                });
            builder.SetPositiveButton(Resource.String.Yes,delegate
                {
                    
                    PackageStatus scanPackage = MCH.Communication.RecoverFlight.Instance.ScanPackage(task.Id,ApplicationSessionState.User.Data.UserId,RecoverFlight_SessionState.CurrentFlightTask.FlightId);

                    if(scanPackage.Transaction.Status)
                    {
                        if(scanPackage.Data.IsHawbReady)
                        {
                            
                            Generate(task.HawbId);
                        }
                        else
                        {
                            EditTextListener.EnableEnter = false;
                            search.Text = string.Empty;
                            EditTextListener.EnableEnter = true;
                            RefreshData(string.Empty,false);
                        }
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowMessage(scanPackage.Transaction.Error);
                    }
                });
            builder.SetNegativeButton(Resource.String.No,delegate 
                {
                    EditTextListener.EnableEnter = false;
                    search.Text = string.Empty;
                    EditTextListener.EnableEnter = true;
                    RefreshData(string.Empty,false);
                });

            AlertDialog dialog = builder.Create();
            dialog.Show();














        }



private void OnEditClickAction(bool  action)
        {
            if (action)
            {
                StartActivity (typeof(RecoverFlight_ShipmentDetailActivity));
            this.Finish();
            }


}

        private void Generate(int hawbId)
        {
 
 

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.RecoverFlight_Creating_Shipment), true);
            new Thread(new ThreadStart(delegate
                {
 
                    // Thread.Sleep(1000);

 
                    PrintStatus result = MCH.Communication.RecoverFlight.Instance.GenerateShipment(hawbId,ApplicationSessionState.User.Data.UserId,RecoverFlight_SessionState.CurrentFlightTask.FlightId);
                    RunOnUiThread (delegate {
                        OnPrinted(result);
                     });

                    RunOnUiThread (delegate {
                        RefreshData(string.Empty,false);
                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();
 
        }


        private void OnPrinted(PrintStatus result)
        {
            if (!result.Transaction.Status)
            {
//                        RunOnUiThread (delegate {
//                            OnPrintingError(result.Transaction.Error);
//                        });
                        
                Action<bool> EditClickAction = OnEditClickAction;
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new RecoverFlight_PrintStatusDialogActivity(this, EditClickAction, GetText(Resource.String.RecoverFlight_Print_Status), GetText(Resource.String.RecoverFlight_Shipment_Printing_Failed), RecoverFlight_SessionState.CurrentPackageTask.Hawb, RecoverFlight_SessionState.CurrentPackageTask.Pcs, "Error:\n" + result.Transaction.Error, true);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;

            }
            else
            {

                string resulttext = "Tracking Numbers:";
                foreach (string n in result.Data.ShipmentNumbers)
                {
                    resulttext +=  "\n" + n;
                }
                        
                        Action<bool> EditClickAction = OnEditClickAction;
                        var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new RecoverFlight_PrintStatusDialogActivity(this,EditClickAction, GetText(Resource.String.RecoverFlight_Print_Status), GetText(Resource.String.RecoverFlight_Print_Successful),RecoverFlight_SessionState.CurrentPackageTask .Hawb,RecoverFlight_SessionState.CurrentPackageTask.Pcs,resulttext,false);
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                        return;

                    }
        }


//        private void OnPrintingError(string error)
//        {
//            MessageBox m = new MessageBox(this);
//            m.OnConfirmationClick += (bool result) => 
//            {
//
//                if(result)
//                {
//                        EditShipment();
//                }
// 
//                RefreshData(string.Empty,false);
//            };
//            m.ShowConfirmationMessage(GetText(Resource.String.RecoverFlight_Shipment_Printing_Failed) + ". " + error ,Resource.String.Edit,Resource.String.Cancel);
//        }


        private void EditShipment()
        {
            StartActivity (typeof(RecoverFlight_ShipmentDetailActivity));
            this.Finish();
        }

        private void LoadData()
        {



         
            if (RecoverFlight_SessionState.CurrentUldTask.Id > 0)
            {
                CheckInUlds t = MCH.Communication.RecoverFlight.Instance.GetCheckInUld(RecoverFlight_SessionState.CurrentUldTask.Id);


                if (t.Transaction.Status == false)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(t.Transaction.Error);
                    return;
                }
                else
                {

                    try
                    {
                        RecoverFlight_SessionState.CurrentUldTask = t.Data[0];
                    }
                    catch (Exception ex)
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowMessage(ex.Message);
                        return;
                    }

                }
                UldNumber.Text = string.Format("Uld: {0}", RecoverFlight_SessionState.CurrentUldTask.UldNumber);
                Pieces.Text = string.Format("Pcs: {0} of {1}", RecoverFlight_SessionState.CurrentUldTask.CheckedInPcs, RecoverFlight_SessionState.CurrentUldTask.Pcs);
            }
            else
            {
                RecoverFlightTasks t= MCH.Communication.RecoverFlight.Instance.GetRecoverFlightTask( RecoverFlight_SessionState.CurrentFlightTask.FlightId);
               


                if (t.Transaction.Status == false)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(t.Transaction.Error);
                    return;
                }
                else
                {

                    try
                    {
                        RecoverFlight_SessionState.CurrentFlightTask = t.Data[0];
                    }
                    catch
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowMessage(t.Transaction.Error);
                        return;
                    }

                }


                UldNumber.Text = string.Format("Uld: {0}", "ALL");
                Pieces.Text = string.Format("Pcs: {0} of {1}",   RecoverFlight_SessionState.CurrentFlightTask.CheckedInPcs , RecoverFlight_SessionState.CurrentFlightTask.Pcs);

            }

    

 
        }


        //If found > 1 in isbarcode = false then clear search
        private void RefreshData(string searchData,bool isBarcode)
        {
            LoadData();

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {


                    CheckInStatusTypes en = (CheckInStatusTypes)Enum.Parse(typeof(CheckInStatusTypes), DropDownText.Text.Replace(" ","_"));

                    packages = MCH.Communication.RecoverFlight.Instance.GetPackages(RecoverFlight_SessionState.CurrentUldTask.Id,en,RecoverFlight_SessionState.CurrentFlightTask.FlightId);

                    RunOnUiThread (delegate {

                        if(packages.Transaction.Status && packages.Data!=null)
                        {

                          
                           

                            if(searchData!=string.Empty)
                            {
                                List<PackageItem> taskList = new List<PackageItem>();
                                foreach (var task in packages.Data)
                                {
                                    if (task.ToString().ToLower().Contains(searchData.ToLower()))
                                    {
                                        taskList.Add(task);   
                                    }
                                }
                                packages.Data =taskList;
                            }

                            if( packages.Data.Count == 1 && isBarcode)
                            {
                                MediaSounds.Instance.Beep(this);
                                GoToTask( packages.Data[0]);
                                EditTextListener.EnableEnter = false;
                                search.Text = string.Empty;
                                EditTextListener.EnableEnter = true;
                                return;
                            }
                            else if( packages.Data.Count == 1 && searchData!=string.Empty)
                            {
                                GoToTask( packages.Data[0]);
                                EditTextListener.EnableEnter = false;
                                search.Text = string.Empty;
                                EditTextListener.EnableEnter = true;
                                return;
                            }

                            titleLabel.Text  = string.Format("Packages ({0})",packages.Data.Count);
                            listView.Adapter = new RecoverFlight_PackageListAdapter(this, packages.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;  

                        }

                        if(isBarcode)
                        {
                            search.Text = string.Empty;
                        }
//                        else
//                        {
//                            if( packages.Data.Count == 0)
//                            {
//                                EditTextListener.EnableEnter = false;
//                                search.Text = string.Empty;
//                                EditTextListener.EnableEnter = true;
//                            }
//                        }



                        search.RequestFocus ();

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }




        private void Finalize()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {


                        CommunicationTransaction t =   MCH.Communication.RecoverFlight.Instance.FinalizeCheckIn(RecoverFlight_SessionState.CurrentFlightTask.FlightId,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {
                        
                            StartActivity (typeof(RecoverFlight_ActionSelectionActivity));
                            this.Finish();
                        }
                        else
                        {http://jsonviewer.stack.hu/
                            MessageBox mb = new MessageBox(this);
                            mb.ShowMessage(t.Error);
                        }

                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);

        }

    }
}




