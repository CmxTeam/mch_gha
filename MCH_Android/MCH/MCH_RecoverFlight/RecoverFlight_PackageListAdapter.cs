﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{


    public class RecoverFlight_PackageListAdapter : BaseAdapter<PackageItem> {

        List<PackageItem> items;
        Activity context;


        public RecoverFlight_PackageListAdapter(Activity context, List<PackageItem> items  ): base()
        {


            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override PackageItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFlight_PackageRow, null);


            TextView PackageNumber = view.FindViewById<TextView>(Resource.Id.PackageNumber);
            TextView HawbNumber = view.FindViewById<TextView>(Resource.Id.HawbNumber);
            TextView Pieces = view.FindViewById<TextView>(Resource.Id.Pieces);
            TextView ShipmentNumber  = view.FindViewById<TextView>(Resource.Id.ShipmentNumber);
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
 

            PackageNumber.Text = string.Format("Pkg: {0} Type: {1}", item.PackageNumber,item.PackageType,item.ServiceType);
            HawbNumber.Text = string.Format("Hawb: {0} Pcs: {1} of {2}", item.Hawb,item.CheckedInPcs, item.Pcs);
            Pieces.Text = string.Format("Service: {0}",item.ServiceType);

            if (item.ShipmentNumber == string.Empty)
            {
                ShipmentNumber.Text = "";
            }
            else
            {
                ShipmentNumber.Text = string.Format("Shp#: {0}",item.ShipmentNumber);
            }

            switch (item.Status)
            {
                case CheckInStatusTypes.Not_CheckedIn:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case CheckInStatusTypes.CheckedIn:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case CheckInStatusTypes.CheckedIn_Errors:
                    RowIcon.SetImageResource (Resource.Drawable.Exclamation);
                    break;
            }

            return view;
        }






    }
}


