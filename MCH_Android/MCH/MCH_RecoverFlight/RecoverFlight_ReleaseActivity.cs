﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_ReleaseActivity : BaseActivity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_ReleaseLayout);
            Initialize();
            LoadData();
        }


        private void DoBack()
        {
            StartActivity (typeof(RecoverFlight_ActionSelectionActivity));
            this.Finish();
        }

        private void SaveData()
        {
 
           

           
             



            if (!ApplicationSessionState.User.Data.IsAdmin)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.No_Privilege_Error);
                return;
            }

            if (ClearOption.Checked == false &&  OnHoldOption.Checked == false)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.RecoverFlight_Invalid_Release_Type);
                return;
            }

            RecoverFlightReleaseTypes releaseTypes;
            if (ClearOption.Checked)
            {
                releaseTypes = RecoverFlightReleaseTypes.Clear;
            }
            else if (OnHoldOption.Checked)
            {
                releaseTypes = RecoverFlightReleaseTypes.On_Hold;
            }
            else
            {
                releaseTypes = RecoverFlightReleaseTypes.NA;
            }


            if (releaseTypes == RecoverFlightReleaseTypes.On_Hold && Notes.Text.Trim() == string.Empty)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.RecoverFlight_Missing_Notes);
                return;
            }


            CommunicationTransaction t =   MCH.Communication.RecoverFlight.Instance.SaveReleaseInfo(RecoverFlight_SessionState.CurrentFlightTask.FlightId, Notes.Text, releaseTypes,ApplicationSessionState.User.Data.UserId);
            if (t.Status)
            {
                DoBack();
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(t.Error);
            }
        }

        private void LoadData()
        {
          
            Flight.Text = string.Format("Flight: {0}", RecoverFlight_SessionState.CurrentFlightTask.Flight);
            Eta.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", RecoverFlight_SessionState.CurrentFlightTask.ETA);
            Reference.Text = string.Format("Reference: {0}", RecoverFlight_SessionState.CurrentFlightTask.Reference);
            Counters.Text = string.Format("Ulds: {0} Pcs: {1}", RecoverFlight_SessionState.CurrentFlightTask.Ulds,  RecoverFlight_SessionState.CurrentFlightTask.Pcs);
            StatusDescription.Text =  string.Format("Status: {0}", RecoverFlight_SessionState.CurrentFlightTask.StatusDescription);

            Progress.Text = string.Format("{0:0}%", RecoverFlight_SessionState.CurrentFlightTask.Progress);
            ProgressBar.Progress = (int)RecoverFlight_SessionState.CurrentFlightTask.Progress;

            switch (RecoverFlight_SessionState.CurrentFlightTask.Status)
            {
                case TaskStatusTypes.Pending:
                    Status.SetImageResource (Resource.Drawable.Pending);
                    break;
                case TaskStatusTypes.In_Progress:
                    Status.SetImageResource (Resource.Drawable.InProgress);
                    break;
                case TaskStatusTypes.Completed:
                    Status.SetImageResource (Resource.Drawable.Completed);
                    break;
            }

            RecoverFlightReleaseInfo releaseInfo =  MCH.Communication.RecoverFlight.Instance.GetRecoverFlightReleaseInfo(RecoverFlight_SessionState.CurrentFlightTask.FlightId);
            if (releaseInfo.Transaction.Status && releaseInfo.Data != null)
            {
                if (releaseInfo.Data.Notes != string.Empty)
                {

                    try
                    {
                        string[] notes = releaseInfo.Data.Notes.Split('|');
                        if(notes.Length == 2)
                        {
                            Notes.Text = notes[1].Trim();
                        }
                        else
                        {
                            Notes.Text = string.Empty;
                        }
                    }
                    catch
                    {
                        Notes.Text = string.Empty;
                    }

                }

                if (releaseInfo.Data.ReleaseType == RecoverFlightReleaseTypes.Clear)
                {
                    ClearOption.Checked = true;
                    OnHoldOption.Checked = false;
                }
                else if (releaseInfo.Data.ReleaseType == RecoverFlightReleaseTypes.On_Hold)
                {
                    ClearOption.Checked = false;
                    OnHoldOption.Checked = true;
                }
                else
                {
                    ClearOption.Checked = false;
                    OnHoldOption.Checked = false;
                }
            }

     

        }

    }
}



