﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class RecoverFlight_MainManuListAdapter : BaseAdapter<RecoverFlightItem> {

        List<RecoverFlightItem> items;
        Activity context;
   

        public RecoverFlight_MainManuListAdapter(Activity context, List<RecoverFlightItem> items  ): base()
        {

 
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override RecoverFlightItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFlight_MainManuRow, null);


            TextView Flight = view.FindViewById<TextView>(Resource.Id.Flight);
            TextView Eta = view.FindViewById<TextView>(Resource.Id.Eta);
            TextView Reference = view.FindViewById<TextView>(Resource.Id.Reference);
            TextView Counters = view.FindViewById<TextView>(Resource.Id.Counters);
            TextView StatusDescription = view.FindViewById<TextView>(Resource.Id.StatusDescription);

            TextView Progress = view.FindViewById<TextView>(Resource.Id.Progress);
            ProgressBar ProgressBar = view.FindViewById<ProgressBar>(Resource.Id.ProgressBar);
            ImageView Status= view.FindViewById<ImageView>(Resource.Id.Status);

 

 
            Flight.Text = string.Format("Flight: {0}", item.Flight);
            Eta.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", item.ETA);
            Reference.Text = string.Format("Reference: {0}", item.Reference);
            Counters.Text = string.Format("Ulds: {0} Pcs: {1}", item.Ulds,  item.Pcs);
            StatusDescription.Text =  string.Format("Status: {0}", item.StatusDescription);
            Progress.Text = string.Format("{0:0}%", item.Progress);
            ProgressBar.Progress = (int)item.Progress;
 
            switch (item.Status)
            {
                case TaskStatusTypes.Pending:
                    Status.SetImageResource (Resource.Drawable.Pending);
                    break;
                case TaskStatusTypes.In_Progress:
                    Status.SetImageResource (Resource.Drawable.InProgress);
                    break;
                case TaskStatusTypes.Completed:
                    Status.SetImageResource (Resource.Drawable.Completed);
                    break;
            }


            return view;
        }

 




    }
}

