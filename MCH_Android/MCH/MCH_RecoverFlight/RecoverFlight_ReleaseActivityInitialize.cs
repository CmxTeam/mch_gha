﻿  
   

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MCH.Communication;

namespace MCH
{

    public partial class RecoverFlight_ReleaseActivity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;

        RadioButton ClearOption;
        RadioButton OnHoldOption;
        EditText Notes;

        TextView Flight;
        TextView Eta;
        TextView Reference;
        TextView Counters;
        TextView StatusDescription;

        TextView Progress;
        ProgressBar ProgressBar;
        ImageView Status;

        Button OkButton;

        private void Initialize()
        {
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            //headerText.Text = ApplicationSessionState.SelectedMenuItem.Name + " - Release";
            headerText.Text = "Customs Release";
            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }


            //Find here new controls
            Flight = FindViewById<TextView>(Resource.Id.Flight);
            Eta = FindViewById<TextView>(Resource.Id.Eta);
            Reference = FindViewById<TextView>(Resource.Id.Reference);
            Counters = FindViewById<TextView>(Resource.Id.Counters);
            StatusDescription = FindViewById<TextView>(Resource.Id.StatusDescription);

            Progress = FindViewById<TextView>(Resource.Id.Progress);
            ProgressBar = FindViewById<ProgressBar>(Resource.Id.ProgressBar);
            Status= FindViewById<ImageView>(Resource.Id.Status);
 

            ClearOption = FindViewById<RadioButton>(Resource.Id.ClearOption);
            OnHoldOption = FindViewById<RadioButton>(Resource.Id.OnHoldOption);
            Notes = FindViewById<EditText>(Resource.Id.Notes);
 
            OkButton = FindViewById<Button>(Resource.Id.OkButton);
            OkButton.Click += OnOkButton_Click;

        }


        private void OnOkButton_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void OnStageLocationClickAction(Barcode  barcode)
        {

        }

        private void OnTruckLocationClickAction(Barcode  barcode)
        {

        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Camera:
                    GoToCamera(typeof( RecoverFlight_ReleaseActivity ),typeof(  RecoverFlight_ReleaseActivity ),RecoverFlight_SessionState.CurrentFlightTask.TaskId);
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}


