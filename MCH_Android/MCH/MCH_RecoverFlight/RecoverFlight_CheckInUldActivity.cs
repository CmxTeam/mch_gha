﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_CheckInUldActivity : BaseActivity
    {

        CheckInUlds checkInUlds;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
             
            SetContentView (Resource.Layout.RecoverFlight_CheckInUldLayout);
            Initialize();

            RefreshData(string.Empty,false);
        }


        private void DoBack()
        {
            StartActivity (typeof(RecoverFlight_ActionSelectionActivity));
            this.Finish();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {

            try
            {
                GoToTask(checkInUlds.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(CheckInUldItem task)
        {
            //Toast.MakeText(this, task.UldNumber, ToastLength.Long).Show();
            RecoverFlight_SessionState.CurrentUldTask = task;

            if (task.Status == UldCheckInStatusTypes.Not_CheckedIn)
            {

                StartActivity (typeof(RecoverFlight_PackageActivity));
                this.Finish();
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {
                        StartActivity (typeof(RecoverFlight_PackageActivity));
                        this.Finish();
                    };
                m.ShowMessage(Resource.String.RecoverFlight_ULD_Already_CheckedIn_Message);
                //search.Text = string.Empty;
                //RefreshData(string.Empty,false);
            }



        }


 
        private void LoadData()
        {


            RecoverFlightTasks t= MCH.Communication.RecoverFlight.Instance.GetRecoverFlightTask( RecoverFlight_SessionState.CurrentFlightTask.FlightId);



            if (t.Transaction.Status == false)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(t.Transaction.Error);
                return;
            }
            else
            {

                try
                {
                    RecoverFlight_SessionState.CurrentFlightTask = t.Data[0];
                }
                catch
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(t.Transaction.Error);
                    return;
                }

            }


//            RecoverFlight_SessionState.CurrentFlightTask = MCH.Communication.RecoverFlight.Instance.GetRecoverFlightTask( RecoverFlight_SessionState.CurrentFlightTask.FlightId);
//            //need to refresh
            Flight.Text = string.Format("Flight: {0}", RecoverFlight_SessionState.CurrentFlightTask.Flight);
            Eta.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", RecoverFlight_SessionState.CurrentFlightTask.ETA);
            Reference.Text = string.Format("Reference: {0}", RecoverFlight_SessionState.CurrentFlightTask.Reference);
            Counters.Text = string.Format("Ulds: {0} of {1} Pcs: {2} of {3}", RecoverFlight_SessionState.CurrentFlightTask.RecoveredUlds,RecoverFlight_SessionState.CurrentFlightTask.Ulds,RecoverFlight_SessionState.CurrentFlightTask.CheckedInPcs,RecoverFlight_SessionState.CurrentFlightTask.Pcs);
            StatusDescription.Text =  string.Format("Status: {0}", RecoverFlight_SessionState.CurrentFlightTask.StatusDescription);


            Progress.Text = string.Format("{0:0}%", RecoverFlight_SessionState.CurrentFlightTask.Progress);
            ProgressBar.Progress = (int)RecoverFlight_SessionState.CurrentFlightTask.Progress;

            switch (RecoverFlight_SessionState.CurrentFlightTask.Status)
            {
                case TaskStatusTypes.Pending:
                    Status.SetImageResource (Resource.Drawable.Pending);
                    break;
                case TaskStatusTypes.In_Progress:
                    Status.SetImageResource (Resource.Drawable.InProgress);
                    break;
                case TaskStatusTypes.Completed:
                    Status.SetImageResource (Resource.Drawable.Completed);
                    break;
            }
        }

        private void Finalize()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
 

                        CommunicationTransaction t =   MCH.Communication.RecoverFlight.Instance.FinalizeCheckIn(RecoverFlight_SessionState.CurrentFlightTask.FlightId,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {
                            DoBack();
                        }
                        else
                        {http://jsonviewer.stack.hu/
                            MessageBox mb = new MessageBox(this);
                            mb.ShowMessage(t.Error);
                        }

                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);

        }
        private void RefreshData(string searchData,bool isBarcode)
        {
            LoadData();

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {


                    UldCheckInStatusTypes en = (UldCheckInStatusTypes)Enum.Parse(typeof(UldCheckInStatusTypes), DropDownText.Text.Replace(" ","_"));
                    
                    checkInUlds = MCH.Communication.RecoverFlight.Instance.GetCheckInUlds(RecoverFlight_SessionState.CurrentFlightTask.FlightId,en);

                    RunOnUiThread (delegate {


                
                    


                        if(checkInUlds.Transaction.Status && checkInUlds.Data!=null)
                        {

                            if(searchData!=string.Empty)
                            {
                                List<CheckInUldItem> taskList = new List<CheckInUldItem>();
                                foreach (var task in checkInUlds.Data)
                                {
                                    if (task.ToString().ToLower().Contains(searchData.ToLower()))
                                    {
                                        taskList.Add(task);   
                                    }
                                }
                                checkInUlds.Data =taskList;
                            }
                            else
                            {
                                List<CheckInUldItem> taskList = new List<CheckInUldItem>();

                                CheckInUldItem all = new CheckInUldItem();
                                all.Id = 0;
                                all.UldNumber = "ALL";
                                all.UldType = "";
                                all.Pcs = 0;
                                all.CheckedInPcs = 0;
                                all.Status = UldCheckInStatusTypes.Not_CheckedIn;

                                taskList.Add(all); 

                                foreach (var task in checkInUlds.Data)
                                {
                                    taskList.Add(task);   
                                }
                                checkInUlds.Data =taskList;
                            }

                            if( checkInUlds.Data.Count == 1 && isBarcode)
                            {
                                GoToTask( checkInUlds.Data[0]);
                            }

                            titleLabel.Text = string.Format("Check-In ({0})",checkInUlds.Data.Count);
                            listView.Adapter = new RecoverFlight_CheckInUldListAdapter(this, checkInUlds.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;  

                        }

                        if(isBarcode)
                        {
                            search.Text = string.Empty;
                        }
                        search.RequestFocus ();

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }

    }
}




