﻿ 


using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFlight_PackageDimsActivity : BaseActivity
    {
        PackageTypes packageTypes;
        Packages pks;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_ShipmentDimsLayout);
            Initialize();


            LoadData();


        }



        private int GetPackageTypeId(string packageType)
        {
            foreach (var p in packageTypes.Data)
            {
                if (p.PackageType.ToLower() == packageType.ToLower())
                {
                    return p.PackageTypeId;
                }
            }
            return 0;
        }

        private string GetPackageType(int packageTypeId)
        {
            foreach (var p in packageTypes.Data)
            {
                if (p.PackageTypeId == packageTypeId)
                {
                    return p.PackageType;
                }
            }
            return string.Empty;
        }

        private void LoadData()
        {



            packageTypes = MCH.Communication.RecoverFlight.Instance.GetPackageTypes(RecoverFlight_SessionState.CurrentPackageTask.HawbId);
            if (!packageTypes.Transaction.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(packageTypes.Transaction.Error);
            }
            else
            {

                packageAdapter = new ArrayAdapter(this,Android.Resource.Layout.SimpleSpinnerItem);
                foreach (var p in packageTypes.Data)
                {
                    packageAdapter.Add(p.PackageType);
                }
 
                txtPackageType.Adapter = packageAdapter;
            }

            string packageType =GetPackageType(RecoverFlight_SessionState.CurrentPackageDim.PackageTypeId);
            txtPackageType.SetSelection(GetPosition(packageAdapter, packageType));


            //  txtPackageType.SetSelection(GetPosition(packageAdapter, RecoverFlight_SessionState.CurrentPackageDim.PackageType));


            //update this every time
            //RecoverFlight_SessionState.CurrentPackageDim
            txtPackageNumber.Text = RecoverFlight_SessionState.CurrentPackageDim.PackageNumber;

          

            txtWeight.Text =string.Format("{0:0.0}", RecoverFlight_SessionState.CurrentPackageDim.Dims.Weight);
            txtUOM.SetSelection(GetPosition(uomAdapter, RecoverFlight_SessionState.CurrentPackageDim.Dims.Uom));
            txtLength.Text =string.Format("{0}", RecoverFlight_SessionState.CurrentPackageDim.Dims.Length);
            txtWidth.Text = string.Format("{0}",RecoverFlight_SessionState.CurrentPackageDim.Dims.Width);
            txtHeight.Text =string.Format("{0}", RecoverFlight_SessionState.CurrentPackageDim.Dims.Height);
            txtDimUOM.SetSelection(GetPosition(dimUomAdapter,RecoverFlight_SessionState.CurrentPackageDim.Dims.DimUom));
 

        }

        private double ConvertToDouble(string value)
        {
            try
            {
                return double.Parse(value);
            }
            catch
            {
                return 0;
            }
        }

        private int ConvertToInt(string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch
            {
                return 0;
            }
        }

        private void OnbtnSave_Click(object sender, EventArgs e)
        {

            if (RecoverFlight_SessionState.CurrentPackageTask.ShipmentNumber != string.Empty)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.RecoverFlight_Shipment_Has_Been_Generated);
                return;
            }

            try
            {
 
                RecoverFlight_SessionState.CurrentPackageDim.PackageTypeId =  GetPackageTypeId( txtPackageType.SelectedItem.ToString());
                RecoverFlight_SessionState.CurrentPackageDim.PackageType =  txtPackageType.SelectedItem.ToString();  
                RecoverFlight_SessionState.CurrentPackageDim.Dims.Weight = ConvertToDouble(txtWeight.Text);
                RecoverFlight_SessionState.CurrentPackageDim.Dims.Length = ConvertToInt(txtLength.Text);
                RecoverFlight_SessionState.CurrentPackageDim.Dims.Width  = ConvertToInt(txtWidth.Text);
                RecoverFlight_SessionState.CurrentPackageDim.Dims.Height  = ConvertToInt(txtHeight.Text);
                RecoverFlight_SessionState.CurrentPackageDim.Dims.Uom  =  txtUOM.SelectedItem.ToString();  ;
                RecoverFlight_SessionState.CurrentPackageDim.Dims.DimUom =  txtDimUOM.SelectedItem.ToString();  
 
                CommunicationTransaction transaction =   MCH.Communication.RecoverFlight.Instance.UpdatePackageDims(RecoverFlight_SessionState.CurrentPackageDim.Id,ApplicationSessionState.User.Data.UserId,RecoverFlight_SessionState.CurrentPackageDim.Dims,RecoverFlight_SessionState.CurrentPackageDim.PackageTypeId); 
                if(transaction.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(Resource.String.Data_Successfully_Saved);

                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(Resource.String.Error_Saving_Data);
                }
 

            }
            catch
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.Error_Saving_Data);
            }

        }

        private void DoBack()
        {
 
            StartActivity (typeof(RecoverFlight_ShipmentPackagesActivity));
            //StartActivity (typeof(RecoverFlight_ShipmentDetailActivity));
            this.Finish();


          
            this.Finish();
        }


        private int GetPosition(ArrayAdapter adapter, string value)
        {
            if (value != null)
            {
                for(int i=0 ; i<adapter.Count ; i++){
                    var obj = adapter.GetItem(i);
                    if (obj.ToString().ToUpper() == value.ToUpper())
                    {
                        return i;
                    }
                }
            }


            return 0;
        }


//    private int GetPosition(ArrayAdapter adapter, string value)
//    {
//        for(int i=0 ; i<adapter.Count ; i++){
//            var obj = adapter.GetItem(i);
//            if (obj.ToString().ToUpper() == value.ToUpper())
//            {
//                return i;
//            }
//        }
//
//        return 0;
//    }

 
 


    }
}



 