﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using  MCH.Communication;
using Android.Graphics;

namespace MCH
{

    public partial class Inventory_PiecesActivity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;

        //EditTextEventListener EditTextListener;


        TextView txtLocation;
        Button btnOk;
        ImageButton btnOkSnap;
        Button btnCancel;

        TextView txtAwb;
        TextView txtWeight;
        TextView txtLocations;
        TextView txtPieces;
        TextView txtTotalPieces;

        TextView  txtHidden;
        NumberPicker np;

        private void Initialize()
        {
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            headerText.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper();
            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }


            //Find here new controls

   
            txtLocation=FindViewById<TextView>(Resource.Id.txtLocation);
            btnOkSnap = FindViewById<ImageButton>(Resource.Id.btnOkSnap);
            btnOk = FindViewById<Button>(Resource.Id.btnOk);
            btnCancel = FindViewById<Button>(Resource.Id.btnCancel);

            btnOkSnap.Click += btnOkSnap_Click;
            btnOk.Click += btnOk_Click;
            btnCancel.Click += btnCancel_Click;


            txtAwb=FindViewById<TextView>(Resource.Id.txtAwb);
            txtWeight=FindViewById<TextView>(Resource.Id.txtWeight);
            txtLocations=FindViewById<TextView>(Resource.Id.txtLocations);
            txtPieces=FindViewById<TextView>(Resource.Id.txtPieces);
            txtTotalPieces=FindViewById<TextView>(Resource.Id.txtTotalPieces);

            txtHidden = FindViewById<TextView>(Resource.Id.txtHidden);
//            EditTextListener = new EditTextEventListener(txtPieces);
//            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
//                {
//                    ScanShipment(false);
//                };


            np = (NumberPicker) FindViewById(Resource.Id.npId);
           // np.WrapSelectorWheel = false;
            
            np.ValueChanged += (object sender, NumberPicker.ValueChangeEventArgs e) => 
            {
                    //ShowSelectedPcs(e.NewVal);
            };  
//            np.KeyPress+= (object sender, View.KeyEventArgs e) => 
//                {
//                    ShowSelectedPcs(np.Value);
//                };
            np.MaxValue = 100;
            np.MinValue = 0;

        }

//        void ShowSelectedPcs(int pcs)
//        {
//            btnOk.Text = string.Format("APPLY ({0})", pcs);
//        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();

            //options.Add(new OptionItem("Relocate",OptionActions.Location , Resource.Drawable.Map));
            options.Add(new OptionItem("Edit Piece Count",OptionActions.Edit , Resource.Drawable.Pen));
            options.Add(new OptionItem("View Details",OptionActions.Info , Resource.Drawable.Info));
            options.Add(new OptionItem("Print Label",OptionActions.Print , Resource.Drawable.Barcode));
            options.Add(new OptionItem("History",OptionActions.History , Resource.Drawable.Clock));
            //options.Add(new OptionItem(GetText(Resource.String.Overpack),OptionActions.OverPack , Resource.Drawable.shipment));
            options.Add(new OptionItem(GetText(Resource.String.Printers)  + GetDefautPrinterHeader(), OptionActions.Printers, Resource.Drawable.Printer));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            //dialogFragment.SetStyle(DialogFragmentStyle.Normal, Resource.Style.Theme_Dialog);
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;

                case  OptionActions.Search:

                    break;
                case  OptionActions.Info:
                  
                    break;
                case  OptionActions.Print:
                    PrintLabel();
                    break;
                case  OptionActions.Location:
                    Relocate();
                    break;
                case  OptionActions.Printers:
                    ShowPrinters();
                    break;
                case  OptionActions.OverPack:
                    this.GoToOverpack(typeof(Inventory_PiecesActivity), Inventory_SessionState.CurrentTaskId,  Inventory_SessionState.CurrentShipment.Data.Reference);
                    break;
                case  OptionActions.History:
                    History();
                    break;
                case  OptionActions.Edit:
                    Edit();
                    break;
            }

        }






        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}


