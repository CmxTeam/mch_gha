﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using  MCH.Communication;
using Android.Graphics;

namespace MCH
{

    public partial class Inventory_MainActivity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;

        EditTextEventListener EditTextListener;


        TextView txtLocation;
        Button btnLocation;
        Button btnEmpty;

        TextView txtShipment;
        TextView txtPieces;
        TextView lblShipment;
        TextView lblPieces;

        TableLayout tb;

        EditText txtBarcode;
        ImageButton btnClear;

        private void Initialize()
        {
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            headerText.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper();
            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }


            //Find here new controls

     
            tb=FindViewById<TableLayout>(Resource.Id.tb);

            txtShipment=FindViewById<TextView>(Resource.Id.txtShipment);
            txtPieces=FindViewById<TextView>(Resource.Id.txtPieces);
            lblShipment=FindViewById<TextView>(Resource.Id.lblShipment);
            lblPieces=FindViewById<TextView>(Resource.Id.lblPieces);
 
            txtBarcode=FindViewById<EditText>(Resource.Id.txtBarcode);

            txtLocation=FindViewById<TextView>(Resource.Id.txtLocation);
            btnLocation = FindViewById<Button>(Resource.Id.btnLocation);
            btnEmpty= FindViewById<Button>(Resource.Id.btnEmpty);

            btnClear = FindViewById<ImageButton>(Resource.Id.btnClear);

            btnEmpty.Click += btnEmpty_Click;
            btnLocation.Click += btnLocation_Click;
            btnClear.Click += btnClear_Click;

            EditTextListener = new EditTextEventListener(txtBarcode);
            EditTextListener.OnEnterEvent += DoScan;



        }



        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();


            options.Add(new OptionItem("My Inventory",OptionActions.View , "Icons/Inventory"));
            options.Add(new OptionItem("Report Empty Location",OptionActions.Update , Resource.Drawable.Empty));
            options.Add(new OptionItem("Change Location",OptionActions.Location , Resource.Drawable.Map));
            options.Add(new OptionItem(GetText(Resource.String.Overpack),OptionActions.OverPack , Resource.Drawable.shipment));
            options.Add(new OptionItem(GetText(Resource.String.Printers) + GetDefautPrinterHeader(), OptionActions.Printers, Resource.Drawable.Printer));
            options.Add(new OptionItem(GetText(Resource.String.Finalize), OptionActions.Finalize, Resource.Drawable.Validate));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;

                case  OptionActions.View:
                    LoadMyInventory();
                    break;
                case  OptionActions.Update:
                    ConfirmLocationAsEmpty();
                    break;
                case  OptionActions.Location:
                    SelectLocation();
                    break;
                case  OptionActions.Finalize:
                    DoFinalize();
                    break;
                case  OptionActions.Printers:
                    ShowPrinters();
                    break;
                case  OptionActions.OverPack:
                    this.GoToOverpack(typeof(Inventory_MainActivity), Inventory_SessionState.CurrentTaskId, string.Empty);
                    break;
     


            }

        }


        private void DoFinalize()
        {
            Inventory_SessionState.DiscrepancyReport = Inventory.Instance.GetInventoryDiscrepancies(Inventory_SessionState.CurrentTaskId);
            if (!Inventory_SessionState.DiscrepancyReport.Transaction.Status)
            {
                MessageBox ms = new MessageBox(this);
                ms.ShowAlert(Inventory_SessionState.DiscrepancyReport.Transaction.Error, MessageBox.AlertType.Error);
                return;
            }


            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {


                        if(Inventory_SessionState.DiscrepancyReport.Data != null && Inventory_SessionState.DiscrepancyReport.Data.Count>0)
                        {
                            var transaction = FragmentManager.BeginTransaction();
                            var dialogFragment = new Inventory_DiscrepancyDialog(  this, Inventory_SessionState.DiscrepancyReport.Data);
                            dialogFragment.Cancelable = false;
                            dialogFragment.OkClicked += () => 
                                {
                                    CommunicationTransaction t =  Inventory.Instance.FinalizeInventory(ApplicationSessionState.User.Data.UserId, Inventory_SessionState.CurrentTaskId );
                                    if (t.Status)
                                    {

                                        GotoMainMenu();

                                    }
                                    else
                                    {
                                        MessageBox ms = new MessageBox(this);
                                        ms.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                    } 
                                };
                            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                            return;
                        }
                        else
                        {
                            CommunicationTransaction t =  Inventory.Instance.FinalizeInventory(ApplicationSessionState.User.Data.UserId, Inventory_SessionState.CurrentTaskId );
                            if (t.Status)
                            {

                                GotoMainMenu();

                            }
                            else
                            {
                                MessageBox ms = new MessageBox(this);
                                ms.ShowAlert(t.Error, MessageBox.AlertType.Error);
                            }
                        }
        





                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);

        }


        private void LoadMyInventory()
        {
            InventoryLocationView view =  Inventory.Instance.GetMyInventory(ApplicationSessionState.User.Data.UserId,Inventory_SessionState.CurrentTaskId );
            if (view.Transaction.Status && view.Data != null)
            {
                List<LocationItem> locations = new List<LocationItem>();
                foreach (var item in view.Data)
                {
                    item.location.LocationPrefix = "B";
                    if (item.ScannedPieces == 0)
                    {
                        locations.Add(new LocationItem(item.location.LocationId ,string.Format("{0} (Empty)",item.location.Location) ,item.location.Location ,item.location.LocationPrefix ));
                    }
                    else
                    {
                        locations.Add(new LocationItem(item.location.LocationId ,string.Format("{0} ({1})",item.location.Location,item.ScannedPieces ) ,item.location.Location ,item.location.LocationPrefix ));
                    }

                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new LocationListDialogActivity("My Inventory", this,locations,  false);
                dialogFragment.Cancelable = false;
                dialogFragment.OkClicked += (List<LocationItem> selection) => 
                    {
                        foreach (var r in selection)
                        {
                            InventoryShipments shipments = Inventory.Instance.GetMyInventoryShipments(ApplicationSessionState.User.Data.UserId, r.LocationId, Inventory_SessionState.CurrentTaskId);
                            if (shipments.Transaction.Status && shipments.Data != null)
                            {
                               
                                if (shipments.Data.Count > 0)
                                {
                                    List< MCH.Communication.SelectionItem> shipmentList = new List< MCH.Communication.SelectionItem>();

                                    foreach (var s in shipments.Data)
                                    {

                                        shipmentList.Add(new MCH.Communication.SelectionItem(string.Format("{0} ({1} of {2})",s.Reference ,s.ScannedPieces ,s.TotalPieces ) ,s.ReferenceId, Resource.Drawable.shipment, false));

                                    }
                                    LoadAwbs(shipmentList,r.Location);
                                    return;
                                }
                                else
                                {
                                    MessageBox ms = new MessageBox(this);
                                    ms.ShowAlert("Location " + r.LocationBarcode + " is empty",MessageBox.AlertType.Information);
                                    ms.OnConfirmationClick+= (bool result) => 
                                        {
                                            LoadMyInventory();
                                        };
                                    
                                }

                            }
                            else
                            {
                                MessageBox ms = new MessageBox(this);
                                ms.ShowAlert(shipments.Transaction.Error, MessageBox.AlertType.Error); 
                            }


                            //LoadAwbs(r.LocationId,r.Location);
         
                        }
                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }

    


        }


        private void LoadAwbs( List< MCH.Communication.SelectionItem> shipmentList, string location)
        {

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("LOC: " + location, this, shipmentList, null, SelectionListAdapter.SelectionMode.Row, false);
            dialogFragment.Cancelable = false;
            dialogFragment.AllowNoSelection = true;
            dialogFragment.CancelButtonText = GetText(Resource.String.Close);
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
                {
                    LoadMyInventory();
                };
     
       

            
        }

        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}


