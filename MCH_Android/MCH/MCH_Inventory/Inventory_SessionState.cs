﻿using System;

namespace MCH
{
    public class Inventory_SessionState
    {
        public Inventory_SessionState()
        {
        }
        public static long CurrentTaskId;
        public static MCH.Communication.InventoryShipment CurrentShipment;
        public static long CurrentLocationId;
        public static string CurrentLocation;
        public static  string LastScanShipment;
        public static  int LastScanPieces;
        public static MCH.Communication.DiscrepancyItems DiscrepancyReport;
    }
}

