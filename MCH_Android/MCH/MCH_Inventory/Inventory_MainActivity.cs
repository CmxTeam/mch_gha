﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Inventory_MainActivity : BaseActivity
    {
        

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Inventory_MainLayout);
            Initialize();
  


            TaskId task = Inventory.Instance.GetInventoryTaskId(ApplicationSessionState.User.Data.UserId, ApplicationSessionState.SelectedWarehouseId);
            if (task.Transaction.Status)
            {
                Inventory_SessionState.CurrentTaskId = task.Data;
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick += (bool result) => 
                    {
                        DoBack();
                    };
                msg.ShowAlert(task.Transaction.Error, MessageBox.AlertType.Error);
                return;  
            }


            if (Inventory_SessionState.CurrentLocationId == 0)
            {
                SelectLocation();
            }
            else
            {
                SetLocation(Inventory_SessionState.CurrentLocationId,Inventory_SessionState.CurrentLocation);
            }

            DisplayLastScan();
        }
 
      

        private void DisplayLastScan()
        {
            if (string.IsNullOrEmpty(Inventory_SessionState.LastScanShipment))
            {
                tb.Visibility = ViewStates.Gone;
            }
            else
            {
                txtShipment.Text =  Inventory_SessionState.LastScanShipment;
                txtPieces.Text =  Inventory_SessionState.LastScanPieces.ToString();
                tb.Visibility = ViewStates.Visible;
            }


            DisplayReportEmpty();

        }
        private void DisplayReportEmpty()
        {
            InventoryShipments shipments = Inventory.Instance.GetMyInventoryShipments(ApplicationSessionState.User.Data.UserId, Inventory_SessionState.CurrentLocationId, Inventory_SessionState.CurrentTaskId);
            if (shipments.Transaction.Status && shipments.Data != null)
            {

                if (shipments.Data.Count == 0)
                {
                    btnEmpty.Visibility = ViewStates.Visible;
                }
                else
                {
                    btnEmpty.Visibility = ViewStates.Gone;
                }
            }
        }

        private void DoBack()
        {
            this.GotoMainMenu();
        }
 
 
        private void btnClear_Click(object sender, EventArgs e)
        {
            EditTextListener.Text = string.Empty;
        }
  



        private void btnEmpty_Click(object sender, EventArgs e)
        {
            ConfirmLocationAsEmpty();
        }

        private void btnLocation_Click(object sender, EventArgs e)
        {
            SelectLocation();
        }

        private void SelectLocation()
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new LocationDialogActivity(this, "Location","Select Location",true, LocationTypes.Area,LocationTypes.Door,LocationTypes.Truck,LocationTypes.ScreeningArea);
            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked+= (Barcode barcode) => 
                {
                    SetLocation(barcode.Id,barcode.Location);
                };
            dialogFragment.CancelClicked+= () => 
                {
                    if (Inventory_SessionState.CurrentLocationId == 0)
                    {
                        DoBack();
                    }
                };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void SetLocation(long LocationId, string Location)
        {
            Inventory_SessionState.CurrentLocationId = LocationId;
            Inventory_SessionState.CurrentLocation = Location;
            txtLocation.Text = "CURRENT LOCATION: " + Inventory_SessionState.CurrentLocation;
            DisplayReportEmpty();
        }


        private void DoScan(object sender, EditTextEventArgs e)
        {
            string data = e.Data;
            if (e.IsBarcode)
            {
                if (e.BarcodeData.BarcodeType == BarcodeTypes.Area ||
                    e.BarcodeData.BarcodeType == BarcodeTypes.Door ||
                    e.BarcodeData.BarcodeType == BarcodeTypes.Truck ||
                    e.BarcodeData.BarcodeType == BarcodeTypes.ScreeningArea
                )
                {

                    Location l = Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, e.BarcodeData.BarcodeSuffix);
                    if (l.Transaction.Status && l.Data != null)
                    {
                        EditTextListener.EnableBarcode = false;
                        if (Inventory_SessionState.CurrentLocationId > 0 && Inventory_SessionState.CurrentLocationId != l.Data.LocationId)
                        {
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick+= (bool result) => 
                                {
                                    EditTextListener.EnableBarcode = true;
                                    if(result)
                                    {
                                        SetLocation(l.Data.LocationId, l.Data.Location); 
                                        return;
                                    }
                                };
                            m.ShowConfirmationMessage("Are you sure you want to change location from "+ Inventory_SessionState.CurrentLocation +" to "+ l.Data.Location +"?", "Yes", "No");
                        }





                    }

                }
                else
                {
                    ValidateShipmentScan(e.Data);
                }

            }
            else
            {
                ValidateShipmentScan(e.Data);
            }
        }

        private void SetLocationAsEmpty(long locationId)
        {
            CommunicationTransaction t =  Inventory.Instance.SetLocationAsEmpty(ApplicationSessionState.User.Data.UserId,locationId,Inventory_SessionState.CurrentTaskId );
            if (!t.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
        }

        private void ConfirmLocationAsEmpty()
        {
            if (Inventory_SessionState.CurrentLocationId == 0)
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert("There is not location selected.", MessageBox.AlertType.Information);
                return;
            }

            InventoryLocationStatus status = MCH.Communication.Inventory.Instance.IsInventoryLocationEmpty(ApplicationSessionState.User.Data.UserId, Inventory_SessionState.CurrentLocationId,Inventory_SessionState.CurrentTaskId );
            if (status.Transaction.Status)
            {
                if (!status.Data)
                {
                    MessageBox msg = new MessageBox(this);
                    msg.ShowAlert("Location "+ Inventory_SessionState.CurrentLocation  +" is not empty.", MessageBox.AlertType.Information);
                    return;  
                }


                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                        SetLocationAsEmpty(Inventory_SessionState.CurrentLocationId);
                    }
                };
                m.ShowConfirmationMessage("Are you sure you want to set location " + Inventory_SessionState.CurrentLocation + " as empty?", "Yes", "No");
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(status.Transaction.Error, MessageBox.AlertType.Error);
            
            }

        }

//        private void ScanShipment(int pieces)
//        {
//            CommunicationTransaction t =  Inventory.Instance.ScanShipment(ApplicationSessionState.User.Data.AppUserId,Inventory_SessionState.CurrentShipment.Data.AwbId,Inventory_SessionState.CurrentLocationId,pieces);
//            if (t.Status)
//            {
//                Inventory_SessionState.LastScanText = Inventory_SessionState.CurrentShipment.Data.Origin +"-"+ Inventory_SessionState.CurrentShipment.Data.AWB +"-"+ Inventory_SessionState.CurrentShipment.Data.Destination + " Pcs: " + pieces.ToString();
//                DisplayLastScan();
//
//            }
//            else
//            {
//                MessageBox m = new MessageBox(this);
//                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
//            }
//
//        }
        private void ValidateShipmentScan(string reference)
        {
            EditTextListener.EnableBarcode = false;
            if (Inventory_SessionState.CurrentLocationId == 0)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("You must select a location.", MessageBox.AlertType.Information);
                m.OnConfirmationClick += (bool result) =>
                {
                        EditTextListener.EnableBarcode = true;
                    SelectLocation();
                };
                
                return;
            }

            Inventory_SessionState.CurrentShipment = MCH.Communication.Inventory.Instance.ValidateShipment(reference, ApplicationSessionState.SelectedWarehouseId, Inventory_SessionState.CurrentTaskId, ApplicationSessionState.User.Data.UserId);

            if (Inventory_SessionState.CurrentShipment.Transaction.Status)
            {

                EditTextListener.Text = string.Empty;

                if (Inventory_SessionState.CurrentShipment.Data != null)
                {
                    
                    if (Inventory_SessionState.CurrentShipment.Data.ReferenceId > 0)
                    {
//                        if ((Inventory_SessionState.CurrentShipment.Data.TotalPieces - Inventory_SessionState.CurrentShipment.Data.ScannedPieces)  > 0)
//                        {
                            this.GoToScreen(typeof(Inventory_PiecesActivity));
//     
//                        }
//                        else
//                        {
//                            MessageBox m = new MessageBox(this);
//                            m.ShowAlert("There are no pieces available.", MessageBox.AlertType.Information);
//                        }
                    }
                    else
                    {
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) => 
                            {
                                EditTextListener.EnableBarcode = true;
                            };
                        m.ShowAlert("Unable to validate shipment.", MessageBox.AlertType.Information);
                    }





                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            EditTextListener.EnableBarcode = true;
                        };
                    m.ShowAlert("Unable to validate shipment.", MessageBox.AlertType.Information);
                }



            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {
                        EditTextListener.EnableBarcode = true;
                    };
                m.ShowAlert(Inventory_SessionState.CurrentShipment.Transaction.Error, MessageBox.AlertType.Error);
            }

        }

    

    }
}


