﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MCH.Communication;

namespace MCH
{

    public partial class Overpack_MainActivity : BaseActivity
    {

     
        TextView titleLabel; 
        ImageView optionButton;
        ImageView backButton;
        ImageView imageHeader;


        ListView listView;


        TextView txtReference;
        TextView txtCounts;
        ImageView btnAdd;
        Button btnPrint;
        Button btnDone;

        private void Initialize()
        {

            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);

            titleLabel = FindViewById<TextView>(Resource.Id.HeaderText);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            imageHeader = FindViewById<ImageView>(Resource.Id.HeaderImage);

            listView = FindViewById<ListView>(Resource.Id.GridControl);

            titleLabel.Text = GetText(Resource.String.Overpack).ToUpper();
            imageHeader.SetImageResource (Resource.Drawable.shipment);


            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;
 

            //Find here new controls
   
            txtReference= FindViewById<TextView>(Resource.Id.txtReference);
            txtCounts= FindViewById<TextView>(Resource.Id.txtCounts);
            btnAdd= FindViewById<ImageView>(Resource.Id.btnAdd);
            btnPrint= FindViewById<Button>(Resource.Id.btnPrint);
            btnDone= FindViewById<Button>(Resource.Id.btnDone);

            btnDone.Click += btnDone_Click;
            btnPrint.Click += btnPrint_Click;
            btnAdd.Click += btnAdd_Click;
        }
 

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }


        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem("ADD SKID", OptionActions.Add, Resource.Drawable.Plus));
            options.Add(new OptionItem(GetText(Resource.String.Printers) + GetDefautPrinterHeader(), OptionActions.Printers, Resource.Drawable.Printer));
            options.Add(new OptionItem(GetText(Resource.String.Refresh),OptionActions.Refresh, Resource.Drawable.Refresh));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Refresh:
    
                    RefreshAll();

                    break;
                case  OptionActions.Printers:
                    ShowPrinters();
                    break;
                case  OptionActions.Add:
                    AddSkid();
                    break;
            }

        }




        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}

