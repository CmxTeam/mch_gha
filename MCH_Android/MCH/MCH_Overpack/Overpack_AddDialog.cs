﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class Overpack_AddDialog: DialogFragment
    {

        Button btnCancel;
        Button btnOk;
   

        Activity context;
        string reference;
     
        int maxValue;
        int minValue;
        int value;
        long typeId;


        TextView txtOfTotalPieces;
        NumberPicker np;
        TextView txtType;
        Button btnType;
        RadioButton qas;
        RadioButton customer;
        TextView  txtHidden;
        List<SelectionItem> list = new List<SelectionItem>();
        OverPackItem currentItem;

        public delegate void OkClickActionEventHandler();
        public event OkClickActionEventHandler OkClicked;

        public Overpack_AddDialog (Activity context, string reference,  int maxValue  ,int minValue ,int value)
        {
            this.reference = reference;
            this.context = context;
            this.maxValue = maxValue;
            this.minValue = minValue;
            this.value = value;
            this.currentItem = null;
        }

        public Overpack_AddDialog (Activity context, string reference,  int maxValue  ,int minValue ,int value,OverPackItem currentItem)
        {
            this.reference = reference;
            this.context = context;
            this.maxValue = maxValue;
            this.minValue = minValue;
            this.value = value;
            this.currentItem = currentItem;
        }



        //ADD this for error when rotating
        public Overpack_AddDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.Overpack_AddDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);


            btnCancel =DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 
            btnOk.Click += OnOk_Click;


            np = (NumberPicker) DialogInstance.FindViewById(Resource.Id.npId);
            np.ValueChanged += (object sender, NumberPicker.ValueChangeEventArgs e) => 
                {
                    //ShowSelectedPcs(e.NewVal);
                };  

            //ShowSelectedPcs(remainingPieces);
            np.MaxValue = this.maxValue;
            np.MinValue = this.minValue;
            np.Value = this.value;


            txtHidden = DialogInstance.FindViewById<TextView>(Resource.Id.txtHidden);

            txtOfTotalPieces=DialogInstance.FindViewById<TextView>(Resource.Id.txtOfTotalPieces);
            txtOfTotalPieces.Text = GetText(Resource.String.Pieces_of).ToUpper() + " " +  this.maxValue.ToString();

            txtType=DialogInstance.FindViewById<TextView>(Resource.Id.txtType);

            btnType=DialogInstance.FindViewById<Button>(Resource.Id.btnType);

            qas=DialogInstance.FindViewById<RadioButton>(Resource.Id.qas);

            customer=DialogInstance.FindViewById<RadioButton>(Resource.Id.customer);

            txtOfTotalPieces=DialogInstance.FindViewById<TextView>(Resource.Id.txtOfTotalPieces);

            btnType.Click += btnType_Click;

            GetOverpackTypes();

            if (currentItem != null)
            {
                txtType.Text = currentItem.OverpackType;
                typeId= currentItem.OverpackTypeId;

                if (currentItem.SkidOwnerType == SkidOwnerTypes.CUSTOMER)
                {
                    customer.Checked = true;
                }
                else
                {
                    qas.Checked = true;
                }
 
            }


            return DialogInstance;
        }

//        void ShowSelectedPcs(int pcs)
//        {
//            btnOk.Text = string.Format("APPLY ({0})", pcs);
//        }



        private void GetOverpackTypes( )
        {
            OverpackTypes types = MCH.Communication.Overpack.Instance.GetOverpackTypes(ApplicationSessionState.SelectedWarehouseId);
            if (types.Transaction.Status)
            {
                if (types.Data != null)
                {
                   list  = new List<SelectionItem>();

                    foreach (var n in types.Data)
                    {

                        if (n.IsDefault)
                        {
                            txtType.Text =n.OverpackType;
                            typeId=n.OverpackTypeId;
                        }


                        if (n.OverpackType.ToLower() != "loose")
                        {
                            list.Add(new SelectionItem(n.OverpackType,n.OverpackTypeId,Resource.Drawable.ULD));
                        }
                       
                    }
                }
  
 
            }
        }
        private void btnType_Click(object sender, EventArgs e)
        {
            if (list.Count>0)
            {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity("Overpack Types", this.context, list, SelectionListAdapter.SelectionMode.SingleSelection, true);
                dialogFragment.Cancelable = false;
                dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
                    {
                        foreach (var r in selection)
                        {
                            txtType.Text =r.Name;
                            typeId=r.Id;

                        }

                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }

 

        }

        private void OnOk_Click(object sender, EventArgs e)
        {
            txtHidden.RequestFocus();
            Validate();
        }
        private void Validate()
        {
            SkidOwnerTypes skidtype;
            if (customer.Checked)
            {
                skidtype = SkidOwnerTypes.CUSTOMER;
            }
            else if (qas.Checked)
            {
                skidtype = SkidOwnerTypes.QAS;
            }
            else
            {
                MessageBox m = new MessageBox(this.context);
                m.ShowAlert("Please select Overpack type.", MessageBox.AlertType.Information);
                return;
            }

            if (typeId == 0)
            {
                MessageBox m = new MessageBox(this.context);
                m.ShowAlert("Please select SKID owner type.", MessageBox.AlertType.Information);
                return;
            }

            CommunicationTransaction t;
            if (currentItem == null)
            {
                t = MCH.Communication.Overpack.Instance.AddOverpack(Overpack_SessionState.CurrentOverpackTask.OverPackTaskId,ApplicationSessionState.User.Data.UserId,typeId,skidtype,np.Value);
            }
            else
            {
                t = MCH.Communication.Overpack.Instance.EditOverpack(Overpack_SessionState.CurrentOverpackTask.OverPackTaskId,currentItem.OverPackItemId,ApplicationSessionState.User.Data.UserId,typeId,skidtype,np.Value);
            }

                       

            if (t.Status)
                        {
                                if (OkClicked != null)
                                    OkClicked();

                                Dismiss();
                        }
                        else
                        {
                            MessageBox m = new MessageBox(this.context);
                            m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }


  




        }



        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            Validate();
        }

        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            if (string.IsNullOrEmpty(this.reference))
            {
                Dialog.Window.SetTitle( GetText( Resource.String.Pieces));    
            }
            else
            {
                Dialog.Window.SetTitle(this.reference);
            }

            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }






    }
}





