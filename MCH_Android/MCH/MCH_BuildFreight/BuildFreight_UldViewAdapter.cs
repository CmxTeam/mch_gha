﻿ 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.Res;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;

namespace MCH
{

 


    public class BuildFreight_UldViewAdapter : BaseAdapter<ForkLiftSelectionItem> {
        List<ForkLiftSelectionItem>  originalItems;
        List<ForkLiftSelectionItem> items;
        Activity context;
   
        public delegate void OkDeleteEventHandler();
        public event OkDeleteEventHandler OnDelete;

        public BuildFreight_UldViewAdapter(Activity context,  List<ForkLiftSelectionItem> items, List<ForkLiftSelectionItem> originalItems): base()
        {
            this.context = context;
            this.items = items;
            this.originalItems = originalItems;

        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override ForkLiftSelectionItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }

        void OnDelete_Click(object sender, EventArgs e)
            {
 
            MessageBox m = new MessageBox(this.context);
            m.OnConfirmationClick+= (bool result) => 
                {
                    if(result)
                    {
                        ImageView img = (ImageView)sender;
                        ForkLiftSelectionItem rowitem = items[int.Parse(img.Tag.ToString())];
 
 
                        CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.RemovePiecesFromUld(ApplicationSessionState.User.Data.UserId,BuildFreight_SessionState.CurrentUld.UldId, rowitem.DetailId ,rowitem.Pieces);
                        if(t.Status)
                        {
                            items.RemoveAt(int.Parse(img.Tag.ToString()));
                            this.NotifyDataSetChanged();
//                            if(items.Count==0)
//                            {
//                                
//                            }
                            if (OnDelete != null)
                                OnDelete.Invoke();

                        }
   


                    }
                };
            m.ShowConfirmationMessage("Are you sure you want to remove this item from " + BuildFreight_SessionState.CurrentUld.Uld + "?");
            }
        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;


            if (view == null) // no view to re-use, create new
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.BuildFreight_UldViewRow, null);
             }

            TextView txtPieces= view.FindViewById<TextView>(Resource.Id.txtPieces);
                CheckBox selectionCheckBox = view.FindViewById<CheckBox>(Resource.Id.SelectionName);
            selectionCheckBox.Text = string.Format("AWB: {0}", item.Reference) ;
            txtPieces.Text = string.Format("PCS: {0}", item.Pieces) ;


                selectionCheckBox.CheckedChange -= OnCheckedChange;
                selectionCheckBox.Checked = item.Selected;
                selectionCheckBox.CheckedChange += OnCheckedChange;
                selectionCheckBox.Tag = position;



            ImageView pic = view.FindViewById<ImageView>(Resource.Id.SelectionIcon);
            pic.Click -= OnDelete_Click;
            pic.Click += OnDelete_Click;
        

            pic.Tag = position.ToString();
            //pic.Tag = item.Id.ToString();
            return view;
        }

        private void OnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            CheckBox selectionCheckBox  = (CheckBox)sender;
            int position = int.Parse(selectionCheckBox.Tag.ToString());
            var item = items[position];
            item.Selected = !item.Selected;
        }





    }
}



 

