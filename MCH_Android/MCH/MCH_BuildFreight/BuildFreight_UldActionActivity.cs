﻿//BuildFreight_UldActionActivity

using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class BuildFreight_UldActionActivity : BaseActivity
    {

        BuildFreightUldListItems Ulds;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.BuildFreight_UldActionLayout);
            Initialize();
            LoadData();
 
        }

 

        private void LoadData()
        {

            BuildFreightUld uld = MCH.Communication.BuildFreight.Instance.GetBuildFreightUld(BuildFreight_SessionState.CurrentBuildTask.TaskId, BuildFreight_SessionState.CurrentBuildTask.FlightManifestId, BuildFreight_SessionState.CurrentUld.UldId,BuildFreight_SessionState.CurrentBuildTask.TaskId );
            if (uld.Transaction.Status)
            {
                BuildFreight_SessionState.CurrentUld = uld.Data;
            }
 

            BuildFreightTask data=  MCH.Communication.BuildFreight.Instance.GetBuildFreightTask (BuildFreight_SessionState.CurrentBuildTask.TaskId, BuildFreight_SessionState.CurrentBuildTask.FlightManifestId);
            if (data.Transaction.Status)
            {
                BuildFreight_SessionState.CurrentBuildTask = data.Data;
            }
 

            LoadDetail();
        }

        private void LoadDetail()
        {
            txtFlight.Text = string.Format("{0} {1} {2}     {3:dd-MMM}", BuildFreight_SessionState.CurrentBuildTask.Destination, BuildFreight_SessionState.CurrentBuildTask.CarrierCode, BuildFreight_SessionState.CurrentBuildTask.FlightNumber, BuildFreight_SessionState.CurrentBuildTask.ETD);
            txtCounts.Text = string.Format("PCS: {0} of {1}     ULDS: {2} of {3}     AWBS: {4} of {5}", BuildFreight_SessionState.CurrentBuildTask.ScannedPieces, BuildFreight_SessionState.CurrentBuildTask.TotalPieces, BuildFreight_SessionState.CurrentBuildTask.ScannedUlds, BuildFreight_SessionState.CurrentBuildTask.TotalUlds, BuildFreight_SessionState.CurrentBuildTask.ScannedAwbs, BuildFreight_SessionState.CurrentBuildTask.TotalAwbs);



            try
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png", BuildFreight_SessionState.CurrentBuildTask.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            }
            catch
            {
                airlineImage.SetImageResource(Resource.Drawable.Airline);
            }


            switch (BuildFreight_SessionState.CurrentBuildTask.Status)
            {
                case BuildFreightStatusTypes.Pending:
                    RowIcon.SetImageResource(Resource.Drawable.Pending);
                    break;
                case BuildFreightStatusTypes.Completed:
                    RowIcon.SetImageResource(Resource.Drawable.Completed);
                    break;
                case BuildFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource(Resource.Drawable.InProgress);
                    break;
                default:
                    RowIcon.SetImageResource(Resource.Drawable.Pending);
                    break;
            }
 
 
//            if (BuildFreight_SessionState.CurrentUld.IsLoose)
//            {
//                txtUld.Text = string.Format("{0}", BuildFreight_SessionState.CurrentUld.UldPrefix);
//            }
//            else
//            {
//                txtUld.Text = string.Format("{0}{1}", BuildFreight_SessionState.CurrentUld.UldPrefix, BuildFreight_SessionState.CurrentUld.UldSerialNo);
//            }

            txtUld.Text =BuildFreight_SessionState.CurrentUld.Uld;


            if (!string.IsNullOrEmpty(BuildFreight_SessionState.CurrentUld.Locations))
            {
                btnStage.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ValidateWhite, 0, 0, 0);
            }
            else
            {
                btnStage.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.NotValidatedWhite, 0, 0, 0);
            }

            if (BuildFreight_SessionState.CurrentUld.Weight > 0)
            {
                btnWeightCheck.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ValidateWhite, 0, 0, 0);
            }
            else
            {
                btnWeightCheck.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.NotValidatedWhite, 0, 0, 0);
            }

            if (BuildFreight_SessionState.CurrentUld.IsBUP)
            {
                btnView.Visibility = ViewStates.Gone;
            }
            else
            {
                btnView.Visibility = ViewStates.Visible;
            }
 


        }


        private void DoBack()
        {
            GoToScreen(typeof(BuildFreight_ULDActivity));
        }


        private void DoStageFlight()
        {
 
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new LocationDialogActivity(this, OnStageFlight, "Location","Select Location",true, LocationTypes.Area);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
 
        }

        private void OnStageFlight(Barcode  barcode)
        {
 
            CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.StageBuildULD(BuildFreight_SessionState.CurrentUld.UldId, BuildFreight_SessionState.CurrentBuildTask.TaskId ,ApplicationSessionState.User.Data.UserId  ,  barcode.Id );
            if (t.Status)
            {
                LoadData();
            }
            else
            {
                MessageBox mb = new MessageBox(this);
                mb.ShowMessage(t.Error);
                return;
            }

        }
    

        private void btnStage_Click(object sender, EventArgs e)
        {
            DoStageFlight();
        }

        private void btnWeightCheck_Click(object sender, EventArgs e)
        {
            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new BuildFreight_WeightDialog(this,"Weight Check",BuildFreight_SessionState.CurrentUld.Uld,BuildFreight_SessionState.CurrentUld.Weight,BuildFreight_SessionState.CurrentUld.TareWeight,BuildFreight_SessionState.CurrentUld.WeightUOM);
            dialogFragment.OkClicked  += (double grossWeight, string UOM) => 
            {
                    CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.UpdateWeight(ApplicationSessionState.User.Data.UserId, BuildFreight_SessionState.CurrentBuildTask.TaskId ,  BuildFreight_SessionState.CurrentBuildTask.FlightManifestId,BuildFreight_SessionState.CurrentUld.UldId,grossWeight,UOM );
                        if (!t.Status)
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }   
            };
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            List<ForkLiftSelectionItem > fl = new List< ForkLiftSelectionItem>();



           // BuildFreightULDView v = MCH.Communication.BuildFreight.Instance.GetBuildULDView(BuildFreight_SessionState.CurrentUld.UldId);
                


            BuildFreightShipmentItems  awbs =   MCH.Communication.BuildFreight.Instance.GetShipmentsByUld(ApplicationSessionState.User.Data.UserId,BuildFreight_SessionState.CurrentBuildTask.FlightManifestId, BuildFreight_SessionState.CurrentBuildTask.TaskId, BuildFreight_SessionState.CurrentUld.UldId );
            if (awbs.Transaction.Status && awbs.Data!=null)
            {

                foreach (var i in awbs.Data)
                {
                    fl.Add(new ForkLiftSelectionItem( i.AWB,i.DetailId,i.ScannedPieces ,MCH.Communication.ReferenceTypes.AWB));
                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new BuildFreight_UldViewDialog( this, fl,false);
                dialogFragment.Cancelable = false;
                dialogFragment.OkClicked += (List<ForkLiftSelectionItem> selection) => 
                    {

                        if(selection != null && selection.Count>0)
                        {
                            foreach(var awb in selection)
                            {

                                CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.RemovePiecesFromUld(ApplicationSessionState.User.Data.UserId,BuildFreight_SessionState.CurrentUld.UldId, awb.DetailId ,awb.Pieces);
                                if(!t.Status)
                                {
                                    
                                }

                            }
                        }


                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;

            }
        }



    }
}

