﻿ 
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class BuildFreight_BuildActivity : BaseActivity
    {

        BuildFreightShipmentItems shipments;
 
         
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.BuildFreight_BuildLayout);
            Initialize();
            RefreshAll();

            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;

        }

        private void RefreshAll()
        {
            LoadData();
            RefreshData(string.Empty,false);
            search.RequestFocus ();
        }

        private void LoadData()
        {
            BuildFreightTask data=  MCH.Communication.BuildFreight.Instance.GetBuildFreightTask (BuildFreight_SessionState.CurrentBuildTask.TaskId, BuildFreight_SessionState.CurrentBuildTask.FlightManifestId);
            if (data.Transaction.Status)
            {
                BuildFreight_SessionState.CurrentBuildTask = data.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(data.Transaction.Error, MessageBox.AlertType.Error);
            }

            LoadDetail();
        }

        private void LoadDetail()
        {
            txtFlight.Text =string.Format("{0} {1} {2}     {3:dd-MMM}",BuildFreight_SessionState.CurrentBuildTask.Destination,BuildFreight_SessionState.CurrentBuildTask.CarrierCode, BuildFreight_SessionState.CurrentBuildTask.FlightNumber, BuildFreight_SessionState.CurrentBuildTask.ETD);
            txtCounts.Text = string.Format("PCS: {0} of {1}     ULDS: {2} of {3}     AWBS: {4} of {5}", BuildFreight_SessionState.CurrentBuildTask.ScannedPieces ,BuildFreight_SessionState.CurrentBuildTask.TotalPieces , BuildFreight_SessionState.CurrentBuildTask.ScannedUlds,BuildFreight_SessionState.CurrentBuildTask.TotalUlds ,BuildFreight_SessionState.CurrentBuildTask.ScannedAwbs,BuildFreight_SessionState.CurrentBuildTask.TotalAwbs);



            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",BuildFreight_SessionState.CurrentBuildTask.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }


            switch (BuildFreight_SessionState.CurrentBuildTask.Status)
            {
                case BuildFreightStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case BuildFreightStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case BuildFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }
 
            txtForkLift.Text = GetForkLiftCount().ToString();

            CheckFinilize();

        }




        private void DoBack()
        {
 
            StartActivity (typeof(BuildFreight_ULDActivity));
 
            this.Finish();
        }

        private void DropForkliftIntoLooseLocation(long uldId )
        {
            var transaction = FragmentManager.BeginTransaction();
                                var dialogFragment = new LocationDialogActivity(this, "Location","Select Location",true, LocationTypes.Area);
                                dialogFragment.Cancelable = false;
                                dialogFragment.OkClicked+= (Barcode barcode) => 
                                {
                    CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.DropAllForkliftPieces(BuildFreight_SessionState.CurrentBuildTask.TaskId,ApplicationSessionState.User.Data.UserId,uldId,barcode.Id);
                                        if(!t.Status)
                                        {
                                            MessageBox m = new MessageBox(this);
                                            m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                        }
                                        RefreshAll();
                                };
                                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }

        private void  DropForkLift()
        {
            BuildFreightUldListItems ulds = MCH.Communication.BuildFreight.Instance.GetBuildFreightUldListItems(ApplicationSessionState.User.Data.UserId, BuildFreight_SessionState.CurrentBuildTask.TaskId, BuildFreightStatusTypes.All,BuildFreight_SessionState.CurrentBuildTask.TaskId );
                if (ulds.Transaction.Status && ulds.Data != null)
                {
   
                    List<SelectionItem> list = new List<SelectionItem>();
 
                    foreach (var u in ulds.Data)
                    {
                        if (u.IsLoose)
                        {
                            list.Add(new SelectionItem(u.Uld,u.UldId,Resource.Drawable.shipment));
                        }
                        else
                        {
                            list.Add(new SelectionItem(u.Uld,u.UldId,Resource.Drawable.ULD));
                        }
                 
                    }

                        var transaction = FragmentManager.BeginTransaction();
                        var dialogFragment = new SelectionDialogActivity("Select Uld", this, list, SelectionListAdapter.SelectionMode.SingleSelection, true);
                        dialogFragment.Cancelable = false;
                        dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
                        {
                                foreach (var r in selection)
                                {
                                     if(r.Name.ToLower()=="loose")
                                     {
                                        DropForkliftIntoLooseLocation(r.Id);
                                     }
                                     else
                                     {
                                        
                                CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.DropAllForkliftPieces(BuildFreight_SessionState.CurrentBuildTask.TaskId,ApplicationSessionState.User.Data.UserId,r.Id,0);
                                        if(!t.Status)
                                        {
                                            MessageBox m = new MessageBox(this);
                                            m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                        }
                                        RefreshAll();
                                     }
                                     return;
                                }

                        };
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);




                       
                }
        }
        private void btnDrop_Click(object sender, EventArgs e)
        {
            if (GetForkLiftCount() == 0)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);
                return;
            }
            else
            {
                DropForkLift();
            }
        }


 


        private int GetForkLiftCount()
        {
 

            try
            {
                ForkLiftCount fl = MCH.Communication.BuildFreight.Instance.GetForkliftCount(ApplicationSessionState.User.Data.UserId, BuildFreight_SessionState.CurrentBuildTask.TaskId);
                txtForkLift.Text = fl.Data.ToString();
                return fl.Data;
            }
            catch
            {
                txtForkLift.Text = "0";
                return 0;
            }
           
        }

        private void btnForkLift_Click(object sender, EventArgs e)
        {
            if (GetForkLiftCount() == 0)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);
                return;
            }
 
 


            List<ForkLiftSelectionItem > fl = new List< ForkLiftSelectionItem>();
        
            ForkLiftView fv =   MCH.Communication.BuildFreight.Instance.GetForkliftView(BuildFreight_SessionState.CurrentBuildTask.TaskId, ApplicationSessionState.User.Data.UserId );
            if (fv.Transaction.Status && fv.Data!=null)
            {

                foreach (var i in fv.Data)
                {
                    fl.Add(new ForkLiftSelectionItem( i.Reference,i.DetailId,i.ForkliftPieces,MCH.Communication.ReferenceTypes.AWB ));
                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new BuildFreight_ForkLiftDialog(  this, fl);
                dialogFragment.Cancelable = false;
                dialogFragment.OkClicked += (List<ForkLiftSelectionItem> selection,long locationId) => 
                    {
 
                        DropForkLiftPieces(selection,locationId);
 
                                    
                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;

            }

        }


        void DropForkLiftPiecesIntoLocation(List<ForkLiftSelectionItem> awbs,long uld)
        {
            var transaction = FragmentManager.BeginTransaction();
                                var dialogFragment = new LocationDialogActivity(this, "Location","Select Location",true, LocationTypes.Area);
                                dialogFragment.Cancelable = false;
                                dialogFragment.OkClicked+= (Barcode barcode) => 
                                {
                                        foreach (var awb in awbs)
                                                    {
                        CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.DropForkliftPieces(BuildFreight_SessionState.CurrentBuildTask.TaskId,ApplicationSessionState.User.Data.UserId,uld,barcode.Id, awb.DetailId);
                                                        if(!t.Status)
                                                            {
                                                                MessageBox m = new MessageBox(this);
                                                                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                                            }
                                                    }
                                        RefreshAll();
                                };
                                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        void DropForkLiftPiecesIntoUldOrLocation(List<ForkLiftSelectionItem> awbs,long uld, long locationId)
        {
            foreach (var awb in awbs)
                                {
                CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.DropForkliftPieces(BuildFreight_SessionState.CurrentBuildTask.TaskId,ApplicationSessionState.User.Data.UserId,uld,locationId, awb.DetailId);
                                    if(!t.Status)
                                        {
                                            MessageBox m = new MessageBox(this);
                                            m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                        }
                                }
            RefreshAll();

        }

        void DropForkLiftPieces(List<ForkLiftSelectionItem> awbs, long locationId)
        {
            if (awbs==null)
            {
                RefreshAll();
                return;
            }

            if (awbs!=null && awbs.Count == 0)
            {
                RefreshAll();
                return;
            }

            BuildFreightUldListItems ulds = MCH.Communication.BuildFreight.Instance.GetBuildFreightUldListItems(ApplicationSessionState.User.Data.UserId, BuildFreight_SessionState.CurrentBuildTask.TaskId, BuildFreightStatusTypes.All,BuildFreight_SessionState.CurrentBuildTask.TaskId );
                if (ulds.Transaction.Status && ulds.Data != null)
                {
   
                    List<SelectionItem> list = new List<SelectionItem>();
 
                    foreach (var u in ulds.Data)
                    {
                        if (u.IsLoose)
                        {
                            list.Add(new SelectionItem(u.Uld,u.UldId,Resource.Drawable.shipment));
                        }
                        else
                        {
                            list.Add(new SelectionItem(u.Uld,u.UldId,Resource.Drawable.ULD));
                        }
                 
                    }

                        var transaction = FragmentManager.BeginTransaction();
                        var dialogFragment = new SelectionDialogActivity("Select Uld", this, list, SelectionListAdapter.SelectionMode.SingleSelection, true);
                        dialogFragment.Cancelable = false;
                        dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
                        {
                                foreach (var r in selection)
                                {
                                     if(r.Name.ToLower()=="loose")
                                     {
                                            DropForkLiftPiecesIntoLocation(awbs,r.Id);
                                     }
                                     else
                                     {
                                            DropForkLiftPiecesIntoUldOrLocation(awbs,r.Id,0);
                                     }
                                     return;
                                }

                        };
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);




                       
                }

 }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            BuildFreightShipmentItem item = shipments.Data[e.Position];
            ProcessShipment(item);
            
        }


        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {


                    BuildFreightStatusTypes status =    EnumHelper.GetEnumItem<BuildFreightStatusTypes>(DropDownText.Text);
 
                    shipments = MCH.Communication.BuildFreight.Instance.GetShipments(ApplicationSessionState.User.Data.UserId,BuildFreight_SessionState.CurrentBuildTask.FlightManifestId , BuildFreight_SessionState.CurrentBuildTask.TaskId,status);

                    RunOnUiThread (delegate {

                        if(shipments.Transaction.Status & shipments.Data!=null)
                        {
                            List<BuildFreightShipmentItem> list =  LinqHelper.Query<BuildFreightShipmentItem>(shipments.Data,searchData);

                            if(isBarcode)
                            {
                                if(list.Count == 1)
                                {
                                    ProcessShipment(list[0]);
                                }
                                else if(list.Count == 0)
                                {
                                    LoadGrid(shipments.Data);
                                }
                                else
                                {
                                    LoadGrid(list);
                                }
                            }
                            else
                            {
                                LoadGrid(list);
                            }

                        }
                        else
                        {
                            LoadGrid(null);
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }


        private void ProcessShipment(BuildFreightShipmentItem awb)
        {

            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new PiecesDialog(this,awb.AWB , awb.TotalPieces, awb.TotalPieces - awb.ScannedPieces,true );
                        dialogFragment.OkClicked += (int pieces) => 
                            {

                    CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.AddForkliftPieces(awb.DetailId,  BuildFreight_SessionState.CurrentBuildTask.TaskId ,  BuildFreight_SessionState.CurrentBuildTask.FlightManifestId, pieces,ApplicationSessionState.User.Data.UserId);
                        if (!t.Status)
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }

                                RefreshAll();
                            };
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
 
       
        }

        private void LoadGrid(List<BuildFreightShipmentItem> list)
        {
            if (list == null)
            {
                list = new List<BuildFreightShipmentItem>();
            }

            shipments.Data  = list;
            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",list.Count).ToUpper();
            listView.Adapter = new BuildFreight_BuildAdapter(this, list);
            listView.ItemClick -= OnListItemClick;
            listView.ItemClick += OnListItemClick; 
        }



        private void CheckFinilize()
        {
            if (BuildFreight_SessionState.CurrentBuildTask.Status != BuildFreightStatusTypes.Completed && BuildFreight_SessionState.CurrentBuildTask.ScannedPieces == BuildFreight_SessionState.CurrentBuildTask.TotalPieces)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DoFinalize();

                    }
                };
                m.ShowConfirmationMessage("All pieces were scanned. Do you want to finalize task?", Resource.String.Yes, Resource.String.No);
            }
        }



        private void Finalize()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DoFinalize();

                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);

        }


        private void DoFinalize()
        {
            CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.FinalizeBuild( BuildFreight_SessionState.CurrentBuildTask.TaskId ,  BuildFreight_SessionState.CurrentBuildTask.FlightManifestId,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            StartActivity (typeof(BuildFreight_MainActivity));
                            this.Finish();
                        }
                        else
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
        }

    }
}




