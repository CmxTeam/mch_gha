﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using Android.Views.InputMethods;
using MCH.Communication;
namespace MCH
{



    public class BuildFreight_UldViewDialog: DialogFragment
    {
        ListView listView;
        List<ForkLiftSelectionItem> originalSelectionList;
        List<ForkLiftSelectionItem> choiseList;
         
        Activity context;
        bool allowMultipleDelete;

        EditText SearchText;
        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;


        Button btnCancel;
        Button btnDelete;
  

        public delegate void OkClickActionEventHandler(List<ForkLiftSelectionItem> selection);
        public event OkClickActionEventHandler OkClicked;

        public BuildFreight_UldViewDialog (Activity context, List<ForkLiftSelectionItem> selectionList , bool allowMultipleDelete)
        {
            this.context = context;
            //this.selectionClickAction = selectionClickAction;
            this.originalSelectionList =  selectionList;
            this.allowMultipleDelete = allowMultipleDelete;
        }

        //ADD this for error when rotating
        public BuildFreight_UldViewDialog()
        {
            Dismiss();
        }



        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.BuildFreight_UldViewLayout, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            listView = DialogInstance.FindViewById<ListView>(Resource.Id.SelectionListView); 

            btnDelete = DialogInstance.FindViewById<Button>(Resource.Id.btnDelete);
            btnDelete.Click += OnDeleteClick;
   
            btnCancel = DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;



                SearchText = DialogInstance.FindViewById<EditText>(Resource.Id.SeachText);
                ClearSearch = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearSearch);
                SearchButton = DialogInstance.FindViewById<ImageButton>(Resource.Id.SearchButton);
                EditTextListener = new EditTextEventListener(SearchText);
                EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
                    {
    
                            UpdateOriginalList();
                            RefreshData(e.Data);
             
                    SearchText.RequestFocus();
                    };
                ClearSearch.Click += OnClearSearch_Click;
                SearchButton.Click += OnSearchButton_Click;

            RefreshData(string.Empty);
            SearchText.RequestFocus();

            if (allowMultipleDelete)
            {
                btnDelete.Visibility = ViewStates.Visible; 
            }
            else
            {
                btnDelete.Visibility = ViewStates.Gone;
            }

            return DialogInstance;
        }


        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            UpdateOriginalList();
            SearchText.Text = string.Empty;
            RefreshData(string.Empty);
            SearchText.RequestFocus ();
        }


        private void UpdateOriginalList()
        {
            if (choiseList != null)
            {

                foreach (var originallist in  this.originalSelectionList)
                {
                    foreach (var seachlist in  this.choiseList)
                    {

                        if (seachlist.DetailId == originallist.DetailId)
                        {
                            originallist.Selected = seachlist.Selected;
                        }

                    }
                }
            }

        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {

            UpdateOriginalList();


            RefreshData(SearchText.Text);
            SearchText.RequestFocus();
        }

        void RefreshData(string search)
        {
            if (originalSelectionList.Count == 0)
            {
                if (OkClicked != null)
                    OkClicked(null);
                Dismiss();
                return;
            }



            this.choiseList = new List<ForkLiftSelectionItem>();
            foreach (var choise in originalSelectionList)
            {   



                if (search == string.Empty)
                {
                    
                    this.choiseList.Add(new ForkLiftSelectionItem(choise.Reference , choise.DetailId , choise.Pieces,MCH.Communication.ReferenceTypes.AWB));

                }
                else
                {
                    if (choise.Reference.ToLower().Contains(search.ToLower()))
                    {
  
                        this.choiseList.Add(new ForkLiftSelectionItem(choise.Reference , choise.DetailId , choise.Pieces,MCH.Communication.ReferenceTypes.AWB));
            
                         
                    }
                }

            }



            var adapter  = new BuildFreight_UldViewAdapter(this.context, choiseList, originalSelectionList);
            adapter.OnDelete += () => 
                {

                    ReloadData();
                    RefreshData(string.Empty);
                    SearchText.RequestFocus();
                    return;
                };
            listView.Adapter = adapter;
            listView.RequestFocus();
            InputMethodManager manager = (InputMethodManager)context.GetSystemService(Context.InputMethodService);

 
                manager.HideSoftInputFromWindow(SearchText.WindowToken, 0);
          





        }

        void ReloadData()
        {
//            List<ForkLiftSelectionItem > fl = new List< ForkLiftSelectionItem>();
//            ForkLiftView fv =  MCH.Communication.BuildFreight.Instance.GetForkliftView( BuildFreight_SessionState.CurrentBuildTask.TaskId, ApplicationSessionState.User.Data.AppUserId );
//            if (fv.Transaction.Status)
//            {
//
//                foreach (var i in fv.Data)
//                {
//                    fl.Add(new ForkLiftSelectionItem( i.AWB,i.DetailId,i.ForkliftPieces ));
//                }
//                this.originalSelectionList = fl;
//            }
//


            List<ForkLiftSelectionItem > fl = new List< ForkLiftSelectionItem>();

            BuildFreightShipmentItems  awbs =   MCH.Communication.BuildFreight.Instance.GetShipmentsByUld(ApplicationSessionState.User.Data.UserId,BuildFreight_SessionState.CurrentBuildTask.FlightManifestId, BuildFreight_SessionState.CurrentBuildTask.TaskId, BuildFreight_SessionState.CurrentUld.UldId );
            if (awbs.Transaction.Status && awbs.Data!=null)
            {

                foreach (var i in awbs.Data)
                {
                    fl.Add(new ForkLiftSelectionItem( i.AWB,i.DetailId,i.ScannedPieces,MCH.Communication.ReferenceTypes.AWB ));
                }
                this.originalSelectionList = fl;
            }



        }

        void OnCancelClick(object sender, EventArgs e)
        {
            if (OkClicked != null)
                OkClicked(null);
            Dismiss();
        }


     
        void OnDeleteClick(object sender, EventArgs e)
        {
            DeleteSelected();
        }

        void DeleteSelected()
        {
            UpdateOriginalList();

            List<ForkLiftSelectionItem> result = new List<ForkLiftSelectionItem>();
            foreach (var selection in originalSelectionList)
            {
                if (selection.Selected)
                {
                    result.Add(selection);
                    //selection.Selected = false;
                }
            }

            if (result.Count == 0)
            {
                MessageBox m = new MessageBox(context);
                m.ShowMessage(GetText(Resource.String.Selection_Dialog_No_Items_Selected_Error));
            }
            else
            {
  
                if (OkClicked != null)
                    OkClicked(result);
                
                Dismiss();
            }

        }
 

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
   
            if (ApplicationSessionState.IsTest(this.context))
            {
   
                Dialog.Window.SetTitle (BuildFreight_SessionState.CurrentUld.Uld +  " (" + ApplicationSessionState.Mode(context).ToString() + ")");

            }
            else
            {
        
              Dialog.Window.SetTitle (BuildFreight_SessionState.CurrentUld.Uld);

            }


            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }





    }
}


