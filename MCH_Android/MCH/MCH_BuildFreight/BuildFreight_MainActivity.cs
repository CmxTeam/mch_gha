﻿
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class BuildFreight_MainActivity : BaseActivity
    {

        BuildFreightTasks Tasks;
       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.BuildFreight_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();
            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;
        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                GoToTask(Tasks.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(BuildFreightTaskItem task)
        {
            BuildFreight_SessionState.CurrentBuildTask = task;
            if (!string.IsNullOrEmpty(BuildFreight_SessionState.CurrentBuildTask.Locations))
            {
                StartActivity (typeof(BuildFreight_ULDActivity));
                this.Finish();
            }
            else
            {
                DoStageFlight();
            }

        }



        private void DoStageFlight()
        {
 
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new LocationDialogActivity(this, OnStageFlight, "Location","Select Location",true, LocationTypes.Area);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
 
        }

        private void OnStageFlight(Barcode  barcode)
        {
 
            CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.StageLocationTask(BuildFreight_SessionState.CurrentBuildTask.TaskId ,ApplicationSessionState.User.Data.UserId  ,  barcode.Id );
            if (t.Status)
            {
                StartActivity (typeof(BuildFreight_ULDActivity));
                this.Finish();
            }
            else
            {
                MessageBox mb = new MessageBox(this);
                mb.ShowMessage(t.Error);
                RefreshData(string.Empty,false);
                return;
            }

        }



        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
 

                BuildFreightStatusTypes status =    EnumHelper.GetEnumItem<BuildFreightStatusTypes>(DropDownText.Text);
                      
                    Tasks = MCH.Communication.BuildFreight.Instance.GetBuildFreightTasks(ApplicationSessionState.SelectedWarehouseId, ApplicationSessionState.User.Data.UserId, status);

                RunOnUiThread (delegate {
                    
                    if(Tasks.Transaction.Status & Tasks.Data!=null)
                    {
                            List<BuildFreightTaskItem> list =  LinqHelper.Query<BuildFreightTaskItem>(Tasks.Data,searchData);

                            if(isBarcode)
                            {
                                if(list.Count == 1)
                                {
                                    GoToTask( list[0]);
                                }
                                else if(list.Count == 0)
                                {
                                    LoadGrid(Tasks.Data);
                                }
                                else
                                {
                                    LoadGrid(list);
                                }
                            }
                            else
                            {
                                LoadGrid(list);
                            }

                    }
                    else
                    {
                            LoadGrid(null);
                    }
   
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }


        private void LoadGrid(List<BuildFreightTaskItem> list)
        {
            if (list == null)
            {
                list = new List<BuildFreightTaskItem>();
            }

            Tasks.Data  = list;
            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper() + string.Format(" - ({0})",list.Count);
            listView.Adapter = new BuildFreight_MainAdapter(this, list);
            listView.ItemClick -= OnListItemClick;
            listView.ItemClick += OnListItemClick; 
        }
 
 
    }
}

