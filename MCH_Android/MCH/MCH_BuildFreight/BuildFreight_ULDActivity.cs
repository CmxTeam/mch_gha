﻿ 
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class BuildFreight_ULDActivity : BaseActivity
    {

        BuildFreightUldListItems Ulds;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.BuildFreight_UldsLayout);
            Initialize();

            RefreshAll();
            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;
        }

        private void RefreshAll()
        {
            LoadData();
            RefreshData(string.Empty,false);
            search.RequestFocus ();
        }


        private void LoadData()
        {
            BuildFreightTask data=  MCH.Communication.BuildFreight.Instance.GetBuildFreightTask (BuildFreight_SessionState.CurrentBuildTask.TaskId, BuildFreight_SessionState.CurrentBuildTask.FlightManifestId);
            if (data.Transaction.Status)
            {
                BuildFreight_SessionState.CurrentBuildTask = data.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(data.Transaction.Error, MessageBox.AlertType.Error);
            }

            LoadDetail();
        }

        private void LoadDetail()
        {
            txtFlight.Text =string.Format("{0} {1} {2}     {3:dd-MMM}",BuildFreight_SessionState.CurrentBuildTask.Destination,BuildFreight_SessionState.CurrentBuildTask.CarrierCode, BuildFreight_SessionState.CurrentBuildTask.FlightNumber, BuildFreight_SessionState.CurrentBuildTask.ETD).ToUpper();
            txtCounts.Text = string.Format("PCS: {0} of {1} - ULDS: {2} of {3} - AWBS: {4} of {5}", BuildFreight_SessionState.CurrentBuildTask.ScannedPieces ,BuildFreight_SessionState.CurrentBuildTask.TotalPieces , BuildFreight_SessionState.CurrentBuildTask.ScannedUlds,BuildFreight_SessionState.CurrentBuildTask.TotalUlds ,BuildFreight_SessionState.CurrentBuildTask.ScannedAwbs,BuildFreight_SessionState.CurrentBuildTask.TotalAwbs);



            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",BuildFreight_SessionState.CurrentBuildTask.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }


            switch (BuildFreight_SessionState.CurrentBuildTask.Status)
            {
                case BuildFreightStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case BuildFreightStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case BuildFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }
 
            if (BuildFreight_SessionState.CurrentBuildTask.PercentProgress >= 100 || BuildFreight_SessionState.CurrentBuildTask.Status == BuildFreightStatusTypes.Completed)
            {
                imgBuild.SetImageResource (Resource.Drawable.ValidateWhite);
            }
            else
            {
                imgBuild.SetImageResource (Resource.Drawable.NotValidatedWhite);
            }



        }


        private void DoBack()
        {
            GoToScreen(typeof(BuildFreight_MainActivity));
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
   
            BuildFreight_SessionState.CurrentUld =  Ulds.Data[e.Position];  
 

            this.GoToScreen(typeof(BuildFreight_UldActionActivity)); 
 
        }

 
 



        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {


                    BuildFreightStatusTypes status =    EnumHelper.GetEnumItem<BuildFreightStatusTypes>(DropDownText.Text);
 
                    Ulds = MCH.Communication.BuildFreight.Instance.GetBuildFreightUldListItems(ApplicationSessionState.User.Data.UserId,BuildFreight_SessionState.CurrentBuildTask.FlightManifestId,status,BuildFreight_SessionState.CurrentBuildTask.TaskId );

                    RunOnUiThread (delegate {

                        if(Ulds.Transaction.Status & Ulds.Data!=null)
                        {
                            List<BuildFreightUldListItem> list =  LinqHelper.Query<BuildFreightUldListItem>(Ulds.Data,searchData);

                            if(isBarcode)
                            {
                                if(list.Count == 1)
                                {
                                    LoadGrid( list);
                                }
                                else if(list.Count == 0)
                                {
                                    LoadGrid(Ulds.Data);
                                }
                                else
                                {
                                    LoadGrid(list);
                                }
                            }
                            else
                            {
                                LoadGrid(list);
                            }

                        }
                        else
                        {
                            LoadGrid(null);
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());

     

                })).Start();

        }


        private void LoadGrid(List<BuildFreightUldListItem> list)
        {
            if (list == null)
            {
                list = new List<BuildFreightUldListItem>();
            }

            Ulds.Data  = list;
            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper() + string.Format(" - ({0})",list.Count);
            listView.Adapter = new BuildFreight_ULDAdapter(this, list,DeleteUld);
            listView.ItemClick -= OnListItemClick;
            listView.ItemClick += OnListItemClick; 
        }


        private void DeleteUld(int position)
        {
            CommunicationTransaction t = MCH.Communication.BuildFreight.Instance.DeleteUld(ApplicationSessionState.User.Data.UserId, BuildFreight_SessionState.CurrentBuildTask.FlightManifestId, Ulds.Data[position].UldId);
            if (t.Status)
            {
                RefreshAll();   
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
        }


        private void Finalize()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        //DoFinalize();

                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);

        }

    }
}

