﻿ 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class BuildFreight_BuildAdapter : BaseAdapter<BuildFreightShipmentItem> {

        List<BuildFreightShipmentItem> items;
        Activity context;
        BuildFreightShipmentItem selecteditem;

        public BuildFreight_BuildAdapter(Activity context, List<BuildFreightShipmentItem> items): base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override BuildFreightShipmentItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {




            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new
            view = context.LayoutInflater.Inflate(Resource.Layout.BuildFreight_BuildRow, null);

            TextView txtAwb= view.FindViewById<TextView>(Resource.Id.txtAwb);
            TextView txtULD= view.FindViewById<TextView>(Resource.Id.txtULD);
            TextView txtPieces= view.FindViewById<TextView>(Resource.Id.txtPieces);
            TextView txtLocations= view.FindViewById<TextView>(Resource.Id.txtLocations);
 
            LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);
            

            txtAwb.Text =string.Format("Awb: {0} {1} {2}", item.Origin ,item.AWB,item.Destination  );
            txtPieces.Text =string.Format("Pcs: {0} of {1}",item.ScannedPieces, item.TotalPieces);
            txtLocations.Text =string.Format("Loc: {0}",item.Locations);

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);

 
            return view;
        }

  

    }
}





 