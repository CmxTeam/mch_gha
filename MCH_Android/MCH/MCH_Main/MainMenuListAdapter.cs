﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.Res;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;

namespace MCH
{
 

    public class MainMenuListAdapter : BaseAdapter<MenuItem> {
        
        List<MenuItem> items;
        Activity context;
        Action<int> OnImageClickAction;

        public MainMenuListAdapter(Activity context, List<MenuItem> items, Action<int> onImageClickAction): base()
        {
            this.OnImageClickAction = onImageClickAction;
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override MenuItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }

       
        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.MainMenuRow, null);

//            if (item.OpenAssignedTaskCount == 0)
//            {
                view.FindViewById<TextView>(Resource.Id.txtTask).Text = string.Format("{0}", item.Name.ToUpper());
//            }
//            else
//            {
//                view.FindViewById<TextView>(Resource.Id.txtTask).Text = string.Format("{0} ({1})", item.Name, item.OpenAssignedTaskCount);
//            }
			
            if (item.OpenAssignedTaskCount == 0)
            {
                view.FindViewById<TextView> (Resource.Id.txtCount).Text = "";
            }
            else
            {
                view.FindViewById<TextView> (Resource.Id.txtCount).Text = string.Format("({0})", item.OpenAssignedTaskCount);
            }




			ImageView pic = view.FindViewById<ImageView> (Resource.Id.taskIcon);

			 





			try 
			{
				System.IO.Stream ims = context.Assets.Open(string.Format(@"Icons/{0}.png",item.IconKey));
				Drawable d = Drawable.CreateFromStream(ims, null);
				pic.SetImageDrawable(d);
			} 
			catch 
			{
				pic.SetImageResource (Resource.Drawable.Clock);
			}


            pic.Tag = position.ToString();
            pic.Click -= OnImageClick;
            pic.Click += OnImageClick;
 
            return view;
        }

 
        public void OnImageClick(object sender, EventArgs e) 
        {
            ImageView img = (ImageView)sender;
            OnImageClickAction.Invoke (int.Parse(img.Tag.ToString()));
        }
 
 

    }
}

