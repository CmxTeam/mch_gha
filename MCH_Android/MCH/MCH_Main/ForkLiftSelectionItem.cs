﻿using System;
using MCH.Communication;
namespace MCH
{
    public class ForkLiftSelectionItem
    {

        public bool Selected { get; set; }
        public string Reference { get; set; }
        public long DetailId { get; set; }
        public int Pieces { get; set; }
        public ReferenceTypes ReferenceType { get; set; }
        public ForkLiftSelectionItem(string reference ,long detailId ,int pieces,ReferenceTypes referenceType)
        {
            this.Reference = reference;
            this.DetailId = detailId;
            this.Pieces = pieces;
            this.Selected  =false;  
            this.ReferenceType  =referenceType;  
        }
    }
}

