﻿ 
using Android.App; using Android.OS; using Android.Views; using Android.Widget; using Android.Graphics; using System.Threading;
using System;
 using System.Collections.Generic;

using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Graphics;

using Android.Content;
using MCH.Communication;



 namespace MCH {

    [Activity (ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]          
    public class Camera_AnnotationPortraitActivity : Camera_AnnotationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
        }
    }

    [Activity (ScreenOrientation = Android.Content.PM.ScreenOrientation.Landscape, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]          
    public class Camera_AnnotationLandscapeActivity : Camera_AnnotationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
        }
    }

    //ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,     //[Activity ( Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]       public partial class Camera_AnnotationActivity : BaseActivity , View.IOnTouchListener, View.IOnLongClickListener, View.IOnClickListener      {         //static         private float _viewX;         private float _viewY;
        private  int ImageNumber;
        Button LastButton;         private const int BUTTON_HEIGHT = 150;
        private const float PRESURE = 0.83f;
        private const float TOOL_MAYOR = 40;        
        List<MCH.Communication.SelectionItem> conditions;
         protected override void OnCreate(Bundle bundle)
        {             base.OnCreate(bundle);             SetContentView(Resource.Layout.Camera_AnnotationLayout);             Initialize(GetText(Resource.String.CargoSnapShot) + " - Annotations", Resource.Drawable.CameraWhite);             
            canvas.SetOnLongClickListener(this);             canvas.SetOnClickListener(this);
            canvas.SetOnTouchListener(this);

            conditions = MCH.Communication.Miscellaneous.Instance.GetConditions();

            string passedData = Intent.GetStringExtra("ImageNumber") ?? "";
            if (int.TryParse(passedData, out ImageNumber))
            {
                DisplayImage();
            }

            ThreadPool.QueueUserWorkItem (o => LoadAnnotations ());
         } 
       

        private void OnAddButton_Click(object sender, EventArgs e)
        {
     
//            Action<MCH.Communication.SelectionItem> SelectionAction = OnSelectionAction;
//            var transaction = FragmentManager.BeginTransaction();
//            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Conditions_List_Title),this,conditions, SelectionAction);
//            dialogFragment.Cancelable = false;
//            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//            return;


            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Conditions_List_Title),this,conditions, SelectionAction, SelectionListAdapter.SelectionMode.CheckBoxSeletion,true);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            return;
        }


//        private void OnSelectionAction(MCH.Communication.SelectionItem  result) //        { //            AddAnotation(result.Name,result.Id,0,0); //        }


        private void OnSelectionAction(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                AddAnotation(r.Name,r.Id,0,0,false); 
            }
            RePositionAllButtons();
        }


        private void LoadAnnotations()
        {
            Thread.Sleep (1000);


            int isNewCounter = 0;


            int i = 0;
            if (ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Count > 0)
            {
                foreach (var a in ImageGallery.Instance.ImageItems[ImageNumber].Annotations)
                {
                   
                    RunOnUiThread (delegate 
                    {
                                if(a.IsNew)
                                {
                                AddButton(a.Annotation,i,a.X,a.Y+ (BUTTON_HEIGHT * isNewCounter));
                                    isNewCounter++;
                                }
                                else
                                {
                                    AddButton(a.Annotation,i,a.X,a.Y);
                                }

                                i++;
                    });

                }
            }

        }

        private  void DeleteAllAnnotations()
        {
            ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Clear();
            if (canvas.ChildCount > 1)
            {
                canvas.RemoveViews(1, canvas.ChildCount - 1);
            }
        }


        private  void  DisplayImage()
        {

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {
 
                    RunOnUiThread (delegate {

 

                        File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);
                        File file = new File(dir, String.Format("{0}.jpg", ImageGallery.Instance.ImageItems[ImageNumber].FileName));

                        if (file.Exists())
                        {
                  
                            
                          
//                            if(ImageGallery.Instance.ImageItems[ImageNumber].IsLandscape)
//                            {
//                                BitmapHelper.LoadImage(file.Path,imageView,320,240,ImageGallery.Instance.ImageItems[ImageNumber].Rotation);
//                                imageView.SetScaleType (ImageView.ScaleType.CenterCrop);
//                            }
//                            else
//                            {
//                                BitmapHelper.LoadImage(file.Path,imageView,320,240,ImageGallery.Instance.ImageItems[ImageNumber].Rotation);
//  
////                                if(IsLandscape())
////                                {
////                                    imageView.SetScaleType (ImageView.ScaleType.FitCenter);
////                                }
////                                else
////                                {
//                                    imageView.SetScaleType (ImageView.ScaleType.FitXy);
////                                }
//                               
//                            }




                            if (!IsLandscape())
                            {
                                if (!ImageGallery.Instance.ImageItems[ImageNumber].IsLandscape)
                                {
                                    imageView.SetScaleType(ImageView.ScaleType.FitXy);
                                }
                                else
                                {
                                    imageView.SetScaleType(ImageView.ScaleType.FitCenter);
                                }
                            }
                            else
                            {
                                if (!ImageGallery.Instance.ImageItems[ImageNumber].IsLandscape)
                                {
                                    imageView.SetScaleType(ImageView.ScaleType.FitCenter);
                                }
                                else
                                {
                                    imageView.SetScaleType(ImageView.ScaleType.FitXy);
                                }
                            }
                            BitmapHelper.LoadImage(file.Path, imageView, 320, 240, ImageGallery.Instance.ImageItems[ImageNumber].Rotation);




                        }




                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();


        }


        private bool IsLandscape()
        {
            var surfaceOrientation = WindowManager.DefaultDisplay.Rotation;
            if (surfaceOrientation == SurfaceOrientation.Rotation0)
            {
                return false;
            }

            if (surfaceOrientation == SurfaceOrientation.Rotation180)
            {
                return false;
            }

            if (surfaceOrientation == SurfaceOrientation.Rotation90)
            {
                return true;
            }
            if (surfaceOrientation == SurfaceOrientation.Rotation270)
            {
                return true;
            }
   

            return false;

        }

        private void AddButton(string text, int index, float x, float y)
        {

 
            Button bt = new Button(this);
            bt.LongClickable = true;
            bt.SetBackgroundResource(Resource.Drawable.BubbleLong);
            bt.SetOnTouchListener(this);
            bt.SetTextColor(global::Android.Graphics.Color.Red);
            bt.SetTypeface(Typeface.SansSerif, TypefaceStyle.Bold);
            bt.SetTextSize(global::Android.Util.ComplexUnitType.Sp, 15f);
            bt.Text = text;
            bt.Tag = index.ToString();
                        
          
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
            layoutParams.Height = BUTTON_HEIGHT;

            canvas.AddView(bt, layoutParams);
    
            if (x > 0 || y > 0)
            {
                bt.Visibility = ViewStates.Invisible;
                ThreadPool.QueueUserWorkItem(o => PositionButton(bt, x, y));
            }
            else
            {
//                if (LastButton != null)
//                {
//        
//                    layoutParams.AddRule(LayoutRules.Below,Resource.Id.AddButton);
//                  
//                }
//                LastButton = bt;
            } 
        }


        private void PositionButton(Button bt,float x, float y)         {             Thread.Sleep (100);             RunOnUiThread (delegate                  {

                    var left = (int)(x);
                    var right = (left + bt.Width);

                    var top = ((int)(y));
                    var buttom = (top + bt.Height);


                    if (x > 0 || y > 0)
                    {
                        bt.Layout(left, top, right, buttom);
                    }                    
                    //headerText.Text = string.Format("l{0},t{1},r{2},b{3},H{4},W{5}",left,top,right,buttom,bt.Height,bt.Width);
     
                    bt.Visibility = ViewStates.Visible;                 });         }

         private  void DoBack()
        {
            //Toast.MakeText(this,"DoBack", ToastLength.Long).Show();
            //RePositionAllButtons();


            var nextScreen = new Intent(this, typeof(Camera_FragmentActivity));
            nextScreen.PutExtra("ImageNumber", ImageNumber.ToString());
                nextScreen.PutExtra("Title", this.Title);
                StartActivity(nextScreen);
                this.Finish();


        }


        private void RePositionAllButtons()
        {
            if (canvas.ChildCount > 1)
            {
                canvas.RemoveViews(1, canvas.ChildCount - 1);
            }
            LoadAnnotations();
        }
              public void OnClick(View v)         {          }
         public bool OnLongClick(View v)         {
            
            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnLongClickSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Conditions_List_Title),this,conditions, SelectionAction, SelectionListAdapter.SelectionMode.SingleSelection,true);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            return true;

//            Action<MCH.Communication.SelectionItem> SelectionAction = OnLongClickSelectionAction;
//            var transaction = FragmentManager.BeginTransaction();
//            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Conditions_List_Title),this,conditions, SelectionAction);
//            dialogFragment.Cancelable = false;
//            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//             
//
// //            return true;         } 
        private void OnLongClickSelectionAction(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                AddAnotation(r.Name,r.Id,_viewX-(BUTTON_HEIGHT/2),_viewY-BUTTON_HEIGHT,false);
            }
            RePositionAllButtons();
        }

//        private void OnLongClickSelectionAction(MCH.Communication.SelectionItem  result)
//        {
//            AddAnotation(result.Name,result.Id,_viewX-(BUTTON_HEIGHT/2),_viewY-BUTTON_HEIGHT,true);
//        }

        private void AddAnotation(string anotation, long id, float x, float y, bool reposition)
        {
            int index = ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Count;
            ImageItemAnnotation a = new ImageItemAnnotation(anotation, id, x, y);
            ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Add(a);
            AddButton(a.Annotation, index, x, y);
            if (reposition)
            {
                RePositionAllButtons();
            }
        }
         public bool OnTouch(View v, MotionEvent e)
        {
            if (!v.GetType().ToString().Contains("Android.Widget.Button"))
            {

                switch (e.Action)
                {
                    case MotionEventActions.Down:
                    _viewX = e.GetX();
                    _viewY = e.GetY();
                    break;
                }

                return false;
            }
 
            Button bt = (Button)v;
            int index = int.Parse(bt.Tag.ToString());             switch (e.Action)
            {                  case MotionEventActions.Up:
                    bt.SetBackgroundResource(Resource.Drawable.BubbleLong);                     break;                 case MotionEventActions.Down:                     _viewX = e.GetX();                     _viewY = e.GetY();                     break;                 case MotionEventActions.Move: 
                        var left = (int)(e.RawX - _viewX);
                        var right = (left + v.Width);

                        var top = ((int)(e.RawY - _viewY));
                        var buttom = (top + v.Height);

                        top = top - (v.Height / 2) - 23;
                        buttom = buttom - (v.Height / 2) - 23;

                        if (e.Pressure == 1)
                        {
                            if (e.ToolMajor > TOOL_MAYOR)
                            {
                                bt.SetBackgroundResource(Resource.Drawable.BubbleLongWhite);


                            bt.SetOnTouchListener(null);
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                                {
                                    if(result)
                                    {

                                        canvas.RemoveView(bt);
                                        bt=null;
                                        ImageGallery.Instance.ImageItems[ImageNumber].Annotations.RemoveAt(index);
                                        RePositionAllButtons();
                                    }
                                    else
                                    {
                                        bt.SetBackgroundResource(Resource.Drawable.BubbleLong);
                                        bt.SetOnTouchListener(this);
                                    }
                                };

                                m.ShowConfirmationMessage(Resource.String.Confirm_Delete_Anotation);



                                return true;
                            }
                            else
                            {
                                bt.SetBackgroundResource(Resource.Drawable.BubbleLong);
                            }
                        }
                        else
                        {
                            if (e.Pressure >= PRESURE)
                            {
                                bt.SetBackgroundResource(Resource.Drawable.BubbleLongWhite);
         
        
                            bt.SetOnTouchListener(null);
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                                {
                                    if(result)
                                    {

                                        canvas.RemoveView(bt);
                                        bt=null;
                                        ImageGallery.Instance.ImageItems[ImageNumber].Annotations.RemoveAt(index);
                                        RePositionAllButtons();
                                    }
                                    else
                                    {
                                        bt.SetBackgroundResource(Resource.Drawable.BubbleLong);
                                        bt.SetOnTouchListener(this);
                                    }
                                };

                                m.ShowConfirmationMessage(Resource.String.Confirm_Delete_Anotation);



                                return true;
                            }
                            else
                            {
                                bt.SetBackgroundResource(Resource.Drawable.BubbleLong);
                            }
                        }


                        bt.Layout(left, top, right, buttom);


                        //int index = int.Parse(bt.Tag.ToString());
                        //ImageGallery.Instance.ImageItems[ImageNumber].Anotations[index].IsNew = false;
                        ImageGallery.Instance.ImageItems[ImageNumber].Annotations[index].X = left;
                        ImageGallery.Instance.ImageItems[ImageNumber].Annotations[index].Y = top;

                        //headerText.Text = string.Format("l{0},t{1},r{2},b{3},H{4},W{5}",left,top,right,buttom,bt.Height,bt.Width);
                            break;                               }              return true;         }     } }