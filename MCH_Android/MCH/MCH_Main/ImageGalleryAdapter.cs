﻿  
 
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using System.Collections.Generic;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Graphics;

namespace MCH
{

    public class ImageGalleryAdapter : BaseAdapter
    {
        Context context;
        List<string> fileNames;

        public ImageGalleryAdapter (Context context, List<string> fileNames)
        {
            this.fileNames  = fileNames;
            this.context = context;
        }

        public override int Count
        {
            get { return fileNames.Count; }
        }

        public override Java.Lang.Object GetItem (int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return 0;
        }
            
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            ImageView imageView = new ImageView (context);
            File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures),ApplicationSessionState.ImageFolder);
            File file  = new File(dir, String.Format("{0}.jpg",fileNames[position]));
            BitmapHelper.LoadImage(file.Path,imageView,53,70,90);
            imageView.LayoutParameters = new Gallery.LayoutParams (110, 150);
            imageView.SetScaleType (ImageView.ScaleType.FitXy);
            return imageView;
        }


    }

}

