﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;

namespace MCH
{



    public class OptionDialog: DialogFragment
    {
        ListView listView;
        List<OptionItem> options;
        Action<OptionItem> optionClickAction;
        Activity context;
        string title = string.Empty;


        public delegate void SelectionEventHandler(OptionItem selection);
        public event SelectionEventHandler OnSelectionClick;



        public OptionDialog(Activity context, List<OptionItem> options, int title)
        {
            try
            {
                this.title = Application.Context.GetText(title);
            }
            catch
            {
                this.title = string.Empty;
            }
 
            this.context = context;
            this.optionClickAction = null;
            this.options = options;
        }

        public OptionDialog (Activity context, List<OptionItem> options, string title)
        {
            this.title = title;
            this.context = context;
            this.optionClickAction = null;
            this.options = options;
        }

        public OptionDialog (Activity context, List<OptionItem> options)
        {
            this.title = string.Empty;
            this.context = context;
            this.optionClickAction = null;
            this.options = options;
        }



        public OptionDialog(Activity context, List<OptionItem> options, Action<OptionItem> optionClickAction, int title)
        {
            try
            {
                this.title = Application.Context.GetText(title);
            }
            catch
            {
                this.title = string.Empty;
            }
 
            this.context = context;
            this.optionClickAction = optionClickAction;
            this.options = options;
        }

        public OptionDialog (Activity context, List<OptionItem> options, Action<OptionItem> optionClickAction, string title)
        {
            this.title = title;
            this.context = context;
            this.optionClickAction = optionClickAction;
            this.options = options;
        }

        public OptionDialog (Activity context, List<OptionItem> options, Action<OptionItem> optionClickAction)
        {
            this.title = string.Empty;
            this.context = context;
            this.optionClickAction = optionClickAction;
            this.options = options;
        }

        //ADD this for error when rotating
        public OptionDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.OptionsLayout, container, false);
       
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            listView = DialogInstance.FindViewById<ListView>(Resource.Id.OptionListView); 
            listView.Adapter = new OptionListAdapter(this.context, options);
            listView.ItemClick += OnListItemClick;  

            

            TextView userLabel = DialogInstance.FindViewById<TextView>(Resource.Id.CurrentUser); 
            //userLabel.Text = string.Format("{0}: {1} {2} / {3}", GetText(Resource.String.Current_User), ApplicationSessionState.User.Data.Firstname, ApplicationSessionState.User.Data.Lastname, ApplicationSessionState.DeviceInfo.Data.CompanyName).ToUpper();
            userLabel.Text = string.Format("{0} {1} / {2} / {3}",  ApplicationSessionState.User.Data.Firstname, ApplicationSessionState.User.Data.Lastname, ApplicationSessionState.DeviceInfo.Data.CompanyName,ApplicationSessionState.SelectedWarehouse).ToUpper();




            return DialogInstance;
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (optionClickAction != null)
            {
                optionClickAction.Invoke(options[e.Position]);
            }

            if (OnSelectionClick != null)
            {
                OnSelectionClick.Invoke(options[e.Position]);
            }
   
            Dismiss();
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
          

            if (ApplicationSessionState.IsTest(this.context))
            {
                if(string.IsNullOrEmpty(title))
                {
                    Dialog.Window.SetTitle (GetString(Resource.String.Options).ToUpper() + " (" + ApplicationSessionState.Mode(context).ToString() + ")");
                }
                else
                {
                    Dialog.Window.SetTitle (title.ToUpper() + " (" + ApplicationSessionState.Mode(context).ToString() + ")");
                }
            }
            else
            {
                if(string.IsNullOrEmpty(title))
                {
                    Dialog.Window.SetTitle (GetString(Resource.String.Options).ToUpper());
                }
                else
                {
                    Dialog.Window.SetTitle (title.ToUpper());
                }
            }


 
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);

            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);

            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }
     
 
     


    }
}

