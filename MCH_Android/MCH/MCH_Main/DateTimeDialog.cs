﻿//using System; // //using Android.App; //using Android.Content; //using Android.Runtime; //using Android.Views; //using Android.Widget; //using Android.OS;
//
//namespace MCH
//{
//    public enum DateTimeMode 
//    {
//        Date=0,
//        Time=1,
//    }
//
//    public class DateTimeDialog : Activity
//    {
//
//        
//        Activity context;
//        DateTime datetime;
//      
//
//        public delegate void OkClickActionEventHandler(DateTime datetime);
//        public event OkClickActionEventHandler OkClicked;
//
//        public DateTimeDialog (Activity context)
//        {
//            this.context = context;
//        }
//
//        public void Show (DateTimeMode mode, DateTime datetime)
//        {
//            this.datetime = datetime;
//            ShowDialog((int)mode);
//        }
//
//
//        protected override Dialog OnCreateDialog (int id)
//        {
//            if (((DateTimeMode)id) == DateTimeMode.Time)
//                return new TimePickerDialog (this, TimePickerCallback, datetime.Hour, datetime.Minute, false);
//
//            if (((DateTimeMode)id) == DateTimeMode.Date)
//                return new DatePickerDialog (this, DatePickerCallback, datetime.Year, datetime.Month -1, datetime.Day); 
//
//            return null;
//        }
//
//        void DatePickerCallback (object sender, DatePickerDialog.DateSetEventArgs e)
//        { //            this.datetime = DateTime.Parse(string.Format("{0:yyyy}-{0:mm}-{0:dd} {1:HH}:{1:mm}",e.Date,this.datetime  ));
//            //this.datetime  = e.Date;
//
//            if (OkClicked != null)
//                OkClicked(this.datetime);
//        }
//
//
//        private void TimePickerCallback (object sender, TimePickerDialog.TimeSetEventArgs e)
//        { //            this.datetime = DateTime.Parse(string.Format("{0:yyyy}-{0:mm}-{0:dd} {1:HH}:{2:mm}",this.datetime, e.HourOfDay,e.Minute  ));
//             
//            if (OkClicked != null)
//                OkClicked(this.datetime);
//        }
//
//    }
//}
//
//
//
