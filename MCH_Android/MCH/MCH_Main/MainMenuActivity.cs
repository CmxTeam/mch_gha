﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MCH.Communication;

namespace MCH
{
 
    [Activity(Theme = "@style/Theme.NoTitle",Label = "@string/app_name" ,  Icon = "@drawable/icon" ,ConfigurationChanges=Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]		
    public class MainMenuActivity : BaseActivity
	{      
        Categories categories;
 
		TextView titleLabel; 
        ImageView optionButton;
        ImageView backButton;
		ListView listView;
        TextView userLabel;
        MenuList menulist;
 

        ImageButton CategoryOptionButton;
        LinearLayout CategoryView;
        ImageButton CheckCategoryButton;
        TextView txtCategory;


        public override bool OnCreateOptionsMenu (IMenu menu)
    {
            LoadOptions();
            return true;
    }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);


 
        }

		protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.MainMenuLayout);


  
            listView = FindViewById<ListView>(Resource.Id.menu); 
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            titleLabel = FindViewById<TextView>(Resource.Id.txtHeader);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
             
            userLabel = FindViewById<TextView>(Resource.Id.CurrentUser); 

             CategoryOptionButton= FindViewById<ImageButton>(Resource.Id.CategoryOptionButton);
             CategoryView= FindViewById<LinearLayout>(Resource.Id.CategoryView);
             CheckCategoryButton= FindViewById<ImageButton>(Resource.Id.CheckCategoryButton);
             txtCategory= FindViewById<TextView>(Resource.Id.txtCategory);

 



//            if (ApplicationSessionState.IsTest(this))
//            {
//                titleLabel.Text = GetText(Resource.String.Main_Menu) + " (" + GetString(Resource.String.Connection_Mode_Test) + ")";
//            }
//            else
//            {
//                titleLabel.Text = GetText(Resource.String.Main_Menu);
//            }

	

            backButton.Click += (object sender, EventArgs e) =>
            {

                ConfirmLogOut();
            };

            optionButton.Click += (object sender, EventArgs e) =>
            {

                    LoadOptions();
 

            };

 



            CategoryOptionButton.Click+=OnCategory_Click;
            CategoryView.Click+=OnCategory_Click;
            CheckCategoryButton.Click+=OnCategory_Click;
            txtCategory.Click+=OnCategory_Click;

 
 

            LoadCategories();
     
          
 
            RefreshData();

 

		}

        private void LoadOptions()
        {
            List<OptionItem> options = new List<OptionItem>();
                    options.Add(new OptionItem(GetText(Resource.String.Refresh),OptionActions.Refresh ,Resource.Drawable.Refresh));
            options.Add(new OptionItem("SITES",OptionActions.Location ,Resource.Drawable.Home));
            options.Add(new OptionItem(GetText(Resource.String.Settings),OptionActions.Settings ,Resource.Drawable.Settings));
            options.Add(new OptionItem(GetText(Resource.String.System_Information), OptionActions.Info, Resource.Drawable.Info));
                    options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
                    options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));
                   Action<OptionItem> OptionClickAction = OnOptionClickAction;
                var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            

            //dialogFragment.SetStyle(DialogFragmentStyle.NoTitle, Android.Resource.Style.ThemeHoloLight);
            //dialogFragment.SetStyle(DialogFragmentStyle.Normal,Resource.Style.DialogStyleNew);
                dialogFragment.Cancelable = true;
                 dialogFragment.Show(transaction,ApplicationSessionState.ApplicationName);

        }


        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
            case OptionActions.Refresh:
                    RefreshData();
                    break;
            case  OptionActions.Logout:
                ConfirmLogOut();
                break;
            case  OptionActions.Exit:
                ConfirmExit();
                break;
            case  OptionActions.Settings:
                    LoadSettings();
                break;
                case  OptionActions.Info:
                    LoadInfo();
                break;
            case  OptionActions.Location:
                    ChangeLocation();
                    break;
 

            }
    
        }

        private void OnChangeLocation(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {

            foreach (var r in result)
            {

                ApplicationSessionState.SelectedWarehouseId = r.Id;
                ApplicationSessionState.SelectedWarehouse = r.Name;
                RefreshData();
            }

        }
        private void ChangeLocation()
        {
            List< MCH.Communication.SelectionItem> locationList = new List< MCH.Communication.SelectionItem>();


            bool isDefaultSelected = false;
            locationList.Add(new MCH.Communication.SelectionItem("ALL SITES", 0, Resource.Drawable.Home, false));


            if (ApplicationSessionState.UserWarehouses.Transaction.Status && ApplicationSessionState.UserWarehouses.Data != null)
            {
 
                for (int i = 0; i < ApplicationSessionState.UserWarehouses.Data.Count; i++)
                {
                    bool isDefault = false;
                    if (ApplicationSessionState.SelectedWarehouseId == ApplicationSessionState.UserWarehouses.Data[i].Id)
                    {
                        isDefault = true;
                        isDefaultSelected = true;
                    }
                    locationList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.UserWarehouses.Data[i].Code.ToUpper(), ApplicationSessionState.UserWarehouses.Data[i].Id, Resource.Drawable.Home, isDefault));
                }
            

            }
          
            if (!isDefaultSelected)
            {
                locationList[0].Selected = true;
            }

             

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("SITES", this, locationList, OnChangeLocation, SelectionListAdapter.SelectionMode.RadioButtonSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

 

        }

        private void LoadCategories()
        {
            categories = MCH.Communication.Menu.Instance.GetMenuCategories(ApplicationSessionState.SelectedApplication.Name, ApplicationSessionState.User.Data.RoleId, ApplicationSessionState.User.Data.UserId, ApplicationSessionState.SelectedWarehouseId);
            if (categories.Transaction.Status && categories.Data != null)
            {


                if (ApplicationSessionState.CurrentCategoryId == null)
                {
                    foreach (var c in categories.Data)
                    {
                        if (c.IsDefault)
                        {

                            SelectCategory(c.CategoryId);

                            return;
                        }
                    }
                }
                else
                {
                    SelectCategory(ApplicationSessionState.CurrentCategoryId);
                    return;
                }


            }
        
            SelectCategory(0);

        }

        private void OnCategory_Click(object sender, EventArgs e)
        {
            View view = (View)sender;
            PopupMenu menu = new PopupMenu(this,view);
            int order = 0;

            menu.Menu.Add(0, 0, order, "ALL"); 

          
            if (categories.Transaction.Status && categories.Data != null)
            {
                foreach (var c in categories.Data)
                {
   

                    order += 1;
                    IMenuItem m = menu.Menu.Add(0, (int)c.CategoryId,order, c.CategoryName.ToUpper()); 
                    m.SetIcon(Resource.Drawable.Camera);
                   

                }
            }
 

       


            
            menu.MenuInflater.Inflate(Resource.Menu.PopupMenu, menu.Menu);
            menu.MenuItemClick += (s1, arg1) =>
                {
                    SelectCategory(arg1.Item.ItemId); 
                    RefreshData();

                };
            menu.Show(); 

        }

        public void SelectCategory(long ? categotyId)
        {
  
        
                if (categories.Transaction.Status && categories.Data != null)
                {
                    foreach (var c in categories.Data)
                    {
                        if (c.CategoryId == categotyId)
                        {
                        ApplicationSessionState.CurrentCategoryId = c.CategoryId;
                            txtCategory.Text =c.CategoryName.ToUpper();
                            return;
                        }
                    }
                }


            ApplicationSessionState.CurrentCategoryId = 0;
            txtCategory.Text = "ALL";

        }

 
        public string GetWarehouseCode(long id)
        {
            if (ApplicationSessionState.UserWarehouses.Data == null)
            {
                return "ALL SITES";
            }
            else
            {
                foreach (UserWarehouse w in ApplicationSessionState.UserWarehouses.Data)
                {
                    if (w.Id == id)
                    {
                        return w.Code;
                    }
                }
            }


            return "ALL SITES";
        }


        private void RefreshData()
        {

            ApplicationSessionState.SelectedWarehouse = GetWarehouseCode(ApplicationSessionState.SelectedWarehouseId);

            if (ApplicationSessionState.IsTest(this))
            {
                titleLabel.Text = GetText(Resource.String.Main_Menu) + " (" + ApplicationSessionState.Mode(this).ToString() + ")";
            }
            else
            {
                titleLabel.Text = GetText(Resource.String.Main_Menu);
            }



            //userLabel.Text = string.Format("{0}: {1} {2} / {3}", GetText(Resource.String.Current_User), ApplicationSessionState.User.Data.Firstname, ApplicationSessionState.User.Data.Lastname, ApplicationSessionState.DeviceInfo.Data.CompanyName).ToUpper();
            userLabel.Text = string.Format("{0} {1} / {2} / {3}",  ApplicationSessionState.User.Data.Firstname, ApplicationSessionState.User.Data.Lastname, ApplicationSessionState.DeviceInfo.Data.CompanyName,ApplicationSessionState.SelectedWarehouse).ToUpper();

 
            //titleLabel.Text = GetText(Resource.String.Main_Menu) + " (" + ApplicationSessionState.SelectedWarehouse + ")";
   

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
 
 
         
            new Thread(new ThreadStart(delegate
            {
 
                Action<int> ImageClickAction = OnImageClickAction;

                    //menulist = MCH.Communication.Menu.Instance.GetAppRoleMenu(Application.Context.GetText(Resource.String.ApplicationCode),ApplicationSessionState.User.Data.RoleId,ApplicationSessionState.User.Data.AppUserId,ApplicationSessionState.SelectedWarehouseId);


                    menulist = Invoke<MenuList>(delegate {
                        return MCH.Communication.Menu.Instance.GetAppRoleMenus(ApplicationSessionState.SelectedApplication.Name,ApplicationSessionState.User.Data.RoleId,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.SelectedWarehouseId); 
                    });


                    List<MenuItem> newlist = new List<MenuItem>();

                    if(menulist.Transaction.Status)
                    {
                        if(menulist.Data!=null)
                        {
                            if(ApplicationSessionState.CurrentCategoryId==0)
                            {
                                foreach(var item in menulist.Data)
                                {
                                    if(categories.Transaction.Status && categories.Data!=null)
                                    {
                                        foreach(var c in categories.Data)
                                        {
                                            if(c.CategoryId == item.CategoryId)
                                            {
                                                newlist.Add(item);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        newlist.Add(item);
                                    }


                                }
                                //all
                                //newlist = menulist.Data;
                            }
                            else
                            {

                                var query = 
                                    from l in menulist.Data  
                                        where l.CategoryId == ApplicationSessionState.CurrentCategoryId
                                    select l;  

                                foreach (var item in query)
                                {
                                    newlist.Add(item);
                                }

                            }
                        }
                    }
                    else
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowMessage(menulist.Transaction.Error);
                        RunOnUiThread(() => progressDialog.Hide());
                        return;
                    }
                   
 

                RunOnUiThread (delegate {


                        menulist.Data = newlist;

                        listView.Adapter = new MainMenuListAdapter(this, newlist,ImageClickAction);
                        listView.ItemClick -= OnListItemClick;
                        listView.ItemClick += OnListItemClick;  


                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();
        }


		private void OnImageClickAction(int position)
		{
			//Toast.MakeText (this, "OnImageClickAction" + position.ToString(), ToastLength.Long).Show ();
		}


		void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {



            try
            {
                


            if (!menulist.Transaction.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(menulist.Transaction.Error);
                return;
            }

            if (ApplicationSessionState.SelectedWarehouseId == 0 && ApplicationSessionState.UserWarehouses.Data !=null)
            {
                this.ShowAlertMessage(Resource.String.Select_Location);
                return;
            }
            else
            {


                ApplicationSessionState.SelectedMenuItem = menulist.Data[e.Position];
                switch (ApplicationSessionState.SelectedMenuItem.NavigationPath.ToLower())
                {
                    //IPAX
                    case "recover_flight":
                        StartActivity (typeof(RecoverFlight_MainManuActivity));
                        this.Finish();
                        break;
                    //QUANTEM
                    case "cargo_accept":
                        StartActivity (typeof(AcceptFreight_MainManuActivity));
                        this.Finish();
                        break;
                    case "cargo_loader":

                        StartActivity (typeof(BuildFreight_MainActivity));
                        this.Finish();

                        break;
                    case "cargo_tender":
                        break;
                    case "cargo_receiver":
                        StartActivity (typeof(RecoverFreight_MainActivity));
                        this.Finish();
 
                        break;
                    case "cargo_discharge":
                        StartActivity (typeof(CargoDischarge_MainActivity));
                        this.Finish();
                        break;

//                        Inventory_SessionState.CurrentLocationId = 0;
//                        StartActivity (typeof(Checkout_MainActivity));
//                        this.Finish();
//                        break;
                    case "cargo_inventory":
                        Inventory_SessionState.CurrentLocationId = 0;
                        StartActivity (typeof(Inventory_MainActivity));
                        this.Finish();
                        break;
                    case "lithium_checklist":
                    case "avi_checklist":
                    case "val_checklist":
                    case "dgr_checklist":
                        StartActivity (typeof(CheckList_MainListActivity));
                        this.Finish();
                        break;
                    case "cargo_screener":
                        StartActivity (typeof(Screener_MainMenuActivity));
                        this.Finish();
                        break;
                    case "cargo_snapshot":
                        GoToCamera(typeof( MainMenuActivity),typeof( MainMenuActivity ),0);
                        break;

                    case "cargo_overpack":
                        GoToOverpack(typeof( MainMenuActivity),0,string.Empty);
                        break;
                    case "cargo_lookup":
                        GoToLookup(typeof( MainMenuActivity),string.Empty,ApplicationSessionState.SelectedMenuItem.Name);
                        break;
    
//                        case "qas_outbound_preparecart":
//                        case "qas_outbound_closecart":
//                        case "qas_outbound_weightcart":
//                        case "qas_outbound_stage":
//                        case "qas_outbound_pickupfromstage":
//                        case "qas_outbound_dropgate":
//                        case "qas_outbound_retrievefromgate":
//                            StartActivity(typeof(QAS_PrepareCart_MainActivity));
//                            this.Finish();
//                            break;



//                        case "qas_inbound_dropcart":
//                        case "qas_inbound_unloadcart":
//                                StartActivity(typeof(QAS_PickupCart_MainActivity));
//                                this.Finish();
//                                break;

                        case "qas_inbound_pickupcart":
                            StartActivity(typeof(QAS_PickupFlight_MainActivity));
                            this.Finish();
                            break;

                        case "qas_tug_checkout":
                        case "qas_tug_checkin":
                            StartActivity(typeof(QAS_Tug_MainActivity));
                            this.Finish();
                            break;

                        case "qas_apc_unload":
                        case "qas_apc_process":
                        case "qas_apc_build":
                        case "qas_apc_load":
                            StartActivity(typeof(QAS_TruckAPC_MainActivity));
                            this.Finish();
                            break;

                        case "qas_admin_addcart":
                            StartActivity(typeof(QAS_Admin_AddCart));
                            this.Finish();
                            break;

                        case "qas_warehouse_allcart":
                        case "qas_runner_allcart":
                            StartActivity(typeof(QAS_Cart_Activity));
                            this.Finish();
                            break;

                        case "qas_mytask":
                            StartActivity(typeof(QAS_MyTask_Activity));
                            this.Finish();
                            break;

                }


            }


            }
            catch(Exception ex)
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(ex.Message, MessageBox.AlertType.Error);
            }



		}

      
		protected override void OnRestart()
		{
            RefreshData();
			base.OnRestart ();
		}



		public override void OnBackPressed ()
		{
			ConfirmLogOut();

		}

	}
}

//        protected override void OnRestart()
//        {
//            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
//            Refresh();
//            base.OnRestart ();
//        }
//
//        protected override void OnStart()
//        {
//            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();
// 
//            base.OnStart();
//        }
//        protected override void OnResume()
//        {
//
//            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();
// 
//            base.OnResume();
//        }
//        protected override void OnPause()
//        {
//            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
//            base.OnPause();
//        }
//        protected override void OnStop()
//        {
//            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
//            base.OnStop();
//        }
//        protected override void OnDestroy ()
//        {
//
//            base.OnDestroy ();
//            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
//        }