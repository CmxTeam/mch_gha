﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCH
{
        
        public partial class Camera_FragmentActivity : BaseFragmentActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;
  
        ImageView TagButton;
        ImageView DeleteButton;
        //ImageView imageView;
        ImageView SnapButton;
        TextView  TagCounter;

        private void Initialize()
        {
            Initialize(string.Empty,string.Empty);
        }

        private void Initialize(string title, int icon)
        {
            Initialize(title, string.Empty);

            if (title != string.Empty)
            {
                if (icon == 0)
            {
                headerImage.Visibility = ViewStates.Gone;
            }
            else
            {
                try 
                {
                    headerImage.SetImageResource (icon);
                } 
                catch 
                {
                    headerImage.SetImageResource (Resource.Drawable.Clock);
                }
                headerImage.Visibility = ViewStates.Visible;
            }
            }


        }

        private void Initialize(string title, string icon)
        {
            headerImage = FindViewById<ImageView>(Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;
 
       
            headerText.Text = title.ToUpper() + "\n#" + ImageGallery.ImageGalleryReference.ToUpper(); 
        

            if (icon == string.Empty)
            {
                headerImage.Visibility = ViewStates.Gone;
            }
            else
            {
                try 
                {
                    System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",icon));
                    Drawable d = Drawable.CreateFromStream(ims, null);
                    headerImage.SetImageDrawable(d);
                } 
                catch 
                {
                    headerImage.SetImageResource (Resource.Drawable.Clock);
                }
                headerImage.Visibility = ViewStates.Visible;
            }

            TagButton = FindViewById<ImageView>(Resource.Id.TagButton);
            TagButton.Click += OnTagButton_Click;

            DeleteButton = FindViewById<ImageView>(Resource.Id.DeleteButton);
            DeleteButton.Click += OnDeleteButton_Click;

            SnapButton  = FindViewById<ImageView>(Resource.Id.SnapButton);
            SnapButton.Click += OnSnapButton_Click;

            //imageView = FindViewById<ImageView> (Resource.Id.GalleryImagePreview);

            TagCounter = FindViewById<TextView> (Resource.Id.TagCounter);
            TagCounter.Text = string.Empty;

        }


        public void AskForDeleteAll()
        {
            MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DeleteAllFiles();
                        DoBack();
                    }
                };
            m.ShowConfirmationMessage(Resource.String.DeleteAll_Question);
        }


        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
    
            options.Add(new OptionItem(GetText(Resource.String.Gallery_Option),OptionActions.Gallery, Resource.Drawable.Galery));
            options.Add(new OptionItem(GetText(Resource.String.Save_And_Upload),OptionActions.Finalize, Resource.Drawable.Validate));
            options.Add(new OptionItem(GetText(Resource.String.DeleteAll_Option),OptionActions.DeleteAll, Resource.Drawable.Delete));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    

                    if (ImageGallery.Instance.Count > 0)
                    {
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) =>
                            {
                                if (result)
                                {
                                    DeleteAllFiles();
                                    DoLogOut();
                                }
                            };
                        m.ShowConfirmationMessage(Resource.String.CaptureSealWithImageCancel);
                    }
                    else
                    {
                        ConfirmLogOut();
                    }

                    break;
                case  OptionActions.Exit:
                    

                    if (ImageGallery.Instance.Count > 0)
                    {
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) =>
                            {
                                if (result)
                                {
                                    DeleteAllFiles();
                                    this.Finish();
                                }
                            };
                        m.ShowConfirmationMessage(Resource.String.CaptureSealWithImageCancel);
                    }
                    else
                    {
                        ConfirmExit();
                    }

                    break;
                case  OptionActions.MainMenu:
                   

                    if (ImageGallery.Instance.Count > 0)
                    {
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) =>
                            {
                                if (result)
                                {
                                    DeleteAllFiles();
                                    GotoMainMenu();
                                }
                            };
                        m.ShowConfirmationMessage(Resource.String.CaptureSealWithImageCancel);
                    }
                    else
                    {
                        GotoMainMenu();
                    }

                    break;
                case  OptionActions.Gallery:
                    var nextScreen = new Intent(this, typeof(Camera_GalleryActivity));
                    StartActivity(nextScreen);
                    this.Finish();
                    break;
                case  OptionActions.Finalize:
                    DoNext();
                    break;
                case  OptionActions.DeleteAll:
                    AskForDeleteAll();
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {

            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();
 
            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();
 
            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();


            base.OnPause();
        }
        protected override void OnStop()
        {
            //check this from the close button
            //StartActivity (typeof(Camera_Activity));
            //this.Finish();


            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            ImageNumber = -1;
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}
