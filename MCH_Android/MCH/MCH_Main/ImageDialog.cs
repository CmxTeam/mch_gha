﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using Android.Graphics;
namespace MCH
{
 
    public class ImageDialog: DialogFragment
    {
        Byte[] image;
        Activity context;
        ImageView imageView;

 
        public ImageDialog (Activity context, string image)
        {
            this.context = context;
            this.image =  System.Convert.FromBase64String(image);

        }

        public ImageDialog (Activity context, Byte[] image)
        {
            this.context = context;
            this.image = image;

        }
 
        public ImageDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.ImageLayout, container, false);

            ImageView imageView = DialogInstance.FindViewById<ImageView>(Resource.Id.ImagePreview); 
            

            try
            {
                if (image != null)
                {
                    imageView.SetImageBitmap(BitmapFactory.DecodeByteArray(image, 0, image.Length));
                }
                else
                {
                    imageView.SetImageResource(Resource.Drawable.person);

                }
            }
            catch
            {
                imageView.SetImageResource(Resource.Drawable.person);
            }
 

 
            return DialogInstance;
        }

 

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (string.Empty);
            Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }
     
 
     


    }
}