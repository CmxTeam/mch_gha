﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.Res;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;

namespace MCH
{

  


    public class SelectionListAdapter : BaseAdapter<SelectionItem> {
        List<SelectionItem>  originalItems;
        List<SelectionItem> items;
        Activity context;
        SelectionMode selectionMode;

     
        public enum SelectionMode
        {
            SingleSelection=0,
            CheckBoxSeletion=1,
            RadioButtonSelection=2,
            Row=4,
        }

        public SelectionListAdapter(Activity context, List<SelectionItem> items, List<SelectionItem> originalItems, SelectionMode selectionMode): base()
        {
            this.context = context;
            this.items = items;
            this.selectionMode = selectionMode;
            this.originalItems = originalItems;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override SelectionItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }

       
        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;


            if (selectionMode == SelectionMode.CheckBoxSeletion)
            {
                if (view == null) // no view to re-use, create new
                {
                    view = context.LayoutInflater.Inflate(Resource.Layout.SelectionCheckRow, null);
                }

                CheckBox selectionCheckBox = view.FindViewById<CheckBox>(Resource.Id.SelectionName);
                selectionCheckBox.Text = item.Name.ToUpper();
                selectionCheckBox.CheckedChange -= OnCheckedChange;
                selectionCheckBox.Checked = item.Selected;
                selectionCheckBox.CheckedChange += OnCheckedChange;
                selectionCheckBox.Tag = position;





            }
            else if (selectionMode == SelectionMode.RadioButtonSelection)
            {
                if (view == null) // no view to re-use, create new
                {
                    view = context.LayoutInflater.Inflate(Resource.Layout.SelectionRadioRow, null);
                }

                RadioButton selectionRadioButton = view.FindViewById<RadioButton>(Resource.Id.SelectionName);
                selectionRadioButton.Text = item.Name.ToUpper();
                selectionRadioButton.CheckedChange -= OnRadionChange;
                selectionRadioButton.Checked = item.Selected;
                selectionRadioButton.CheckedChange += OnRadionChange;
                selectionRadioButton.Tag = position;
 
            }
            else if (selectionMode == SelectionMode.Row)
            {
                if (view == null) // no view to re-use, create new
                {
                    view = context.LayoutInflater.Inflate(Resource.Layout.SelectionRowPlain, null);
                }
                view.FindViewById<TextView>(Resource.Id.SelectionName).Text = item.Name.ToUpper();
            }
            else
            {
                if (view == null) // no view to re-use, create new
                {
                    view = context.LayoutInflater.Inflate(Resource.Layout.SelectionRow, null);
                }
                view.FindViewById<TextView>(Resource.Id.SelectionName).Text = item.Name.ToUpper();
            }



            ImageView pic = view.FindViewById<ImageView>(Resource.Id.SelectionIcon);

            if (item.IconId > 0)
            {   
                try
                {
                    pic.SetImageResource(item.IconId);
                }
                catch
                {
                    pic.SetImageResource (Resource.Drawable.Menu);
                }
            }
            else
            {
                if (item.IconName != null && item.IconName.ToString().Trim() != string.Empty)
                {
                    try
                    {
                        System.IO.Stream ims = context.Assets.Open(string.Format(@"{0}.png", item.IconName));
                        Drawable d = Drawable.CreateFromStream(ims, null);
                        pic.SetImageDrawable(d);
                    }
                    catch
                    {
                        pic.SetImageResource(Resource.Drawable.Menu);
                    }
                }
            }
   
            //pic.Tag = position.ToString();
            pic.Tag = item.Id.ToString();
            return view;
        }

        private void OnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            CheckBox selectionCheckBox  = (CheckBox)sender;
            int position = int.Parse(selectionCheckBox.Tag.ToString());
            var item = items[position];
            item.Selected = !item.Selected;
        }

        private void OnRadionChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {


            foreach (var i in items)
            {
                i.Selected = false;
            }

            RadioButton selectionCheckBox = (RadioButton)sender;
            int position = int.Parse(selectionCheckBox.Tag.ToString());
            var item = items[position];
            item.Selected = true;



            foreach (var originalitem in  this.originalItems)
            {
                if (originalitem.Id == item.Id)
                {
                    originalitem.Selected = true;
                }
                else
                {
                    originalitem.Selected = false;
                }
                   
            }

            this.NotifyDataSetChanged();
        }
 




    }
}

 
