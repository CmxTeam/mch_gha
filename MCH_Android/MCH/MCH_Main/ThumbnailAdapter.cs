﻿ 
using MCH.Graphics;
//using Uri = Android.Net.Uri;
using System.Collections.Generic;
//using Android.Content;
//using Android.Views;
//using Android.Widget;
//using Java.Lang;
using Environment = Android.OS.Environment;
using Java.IO;
using Android.Content;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace MCH
{

    public class ThumbnailAdapter : BaseAdapter
    {
        private readonly Context context;
        List<ImageItem> files;

        public ThumbnailAdapter(Context context, List<ImageItem> files)
    {
            this.files = files;
            this.context = context;
    }

    public override int Count
    {
            get { return files.Count; }
    }

    public override Object GetItem(int position)
    {
        return null;
    }

    public override long GetItemId(int position)
    {
        return 0;
    }
 

    public override View GetView(int position, View convertView, ViewGroup parent)
    {
        ImageView imageView;

        if (convertView == null)
        {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(context);
            imageView.LayoutParameters = new AbsListView.LayoutParams(100, 100);
            imageView.SetScaleType(ImageView.ScaleType.CenterCrop);
            imageView.SetPadding(8, 8, 8, 8);

        }
        else
        {
            imageView = (ImageView) convertView;
        }


           File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures),ApplicationSessionState.ImageFolder);
           string fileName = files[position].FileName + ".jpg";
           File file  = new File(dir, fileName);

           BitmapHelper.LoadImage(file.Path,imageView,100, 100,files[position].Rotation);

 
        return imageView;
    }

 
    }
}
