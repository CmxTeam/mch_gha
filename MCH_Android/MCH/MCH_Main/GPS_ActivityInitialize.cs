﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;

namespace MCH
{
            
    public partial class GPS_Activity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;
        TextView txtLatitude;
        TextView txtLongitude;
        TextView txtAltitude;
        TextView txtAccuracy;
        TextView txtSpeed;
 
        private void Initialize()
        {
            Initialize(string.Empty,string.Empty);
        }

        private void Initialize(string title, int icon)
        {
            Initialize(title, string.Empty);

            if (title != string.Empty)
            {
                if (icon == 0)
            {
                headerImage.Visibility = ViewStates.Gone;
            }
            else
            {
                try 
                {
                    headerImage.SetImageResource (icon);
                } 
                catch 
                {
                    headerImage.SetImageResource (Resource.Drawable.Clock);
                }
                headerImage.Visibility = ViewStates.Visible;
            }
            }


        }

        private void Initialize(string title, string icon)
        {
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);


            txtLatitude = FindViewById<TextView>(Resource.Id.txtLatitude);
            txtLongitude = FindViewById<TextView>(Resource.Id.txtLongitude);
            txtAltitude = FindViewById<TextView>(Resource.Id.txtAltitude);
            txtAccuracy = FindViewById<TextView>(Resource.Id.txtAccuracy);
            txtSpeed = FindViewById<TextView>(Resource.Id.txtSpeed);


//            txtLatitude.Text = Android.Locations. location.Latitude.ToString();
//            txtLongitude.Text = location.Longitude.ToString();
//            txtAltitude.Text = location.Altitude.ToString();
//            txtAccuracy.Text = location.Accuracy.ToString();
//            txtSpeed.Text = location.Speed.ToString();

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;
 
            if (title == string.Empty)
            {
                header.Visibility = ViewStates.Gone;
            }
            else
            {
                headerText.Text = title;
            }

            if (icon == string.Empty)
            {
                headerImage.Visibility = ViewStates.Gone;
            }
            else
            {
                try 
                {
                    System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",icon));
                    Drawable d = Drawable.CreateFromStream(ims, null);
                    headerImage.SetImageDrawable(d);
                } 
                catch 
                {
                    headerImage.SetImageResource (Resource.Drawable.Clock);
                }
                headerImage.Visibility = ViewStates.Visible;
            }


            //Find here new controls

        }

 

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            MessageBox m = new MessageBox(this);
            m.ShowMessage("my message", "my title");
            m.OnConfirmationClick += (bool result) => 
                {
                    
                };
            DoBack();


            m.ShowMessage("my message");
            m.ShowMessage(GetText(Resource.String.Confirm_Finalize));
            m.ShowMessage(Resource.String.Confirm_Finalize, Resource.String.app_name);

            m.ShowAlert("heyyyyyyyy!", MessageBox.AlertType.Information);


            m.ShowConfirmationMessage("are you sure ....","Sure","Nah");

            m.OnConfirmationClick += (bool result) => 
                {
                    if(result)
                    {
                        GotoMainMenu();
                    }
                    else
                    {
                        //do nothing
                    }
                };

            //DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();
 
            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();
 
            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}

