﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;

using Camera = Android.Hardware.Camera;
 
using Environment = Android.OS.Environment;

using MCH.Graphics;
using Java.IO;
using Android.Locations;
using MCH.Communication;
namespace MCH
{
    [Activity()]			
    public class BaseActivity : Activity , ILocationListener
    {

        LocationManager locMgr;
        private bool IsLoginDialogOpen = false; 
        BackgroundWorker bgWorker;
        

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            Window.SetSoftInputMode (SoftInput.StateAlwaysHidden);
 
            if (ApplicationSessionState.CompanySecuritySettings.Data.IsSingleSession)
            {
                this.bgWorker = new BackgroundWorker();
                this.bgWorker.WorkerSupportsCancellation = true;
                this.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);

                StartWorker();
            }

        }
 
        public void OnLocationChanged(Android.Locations.Location location)
        {
            try
            {
                ApplicationSessionState.Latitude = location.Latitude;
                ApplicationSessionState.Longitude = location.Longitude;
                ApplicationSessionState.Altitude = location.Altitude;
                ApplicationSessionState.Accuracy = location.Accuracy;
                ApplicationSessionState.Bearing = location.Bearing;
                ApplicationSessionState.Speed = location.Speed;
                ApplicationSessionState.Provider = location.Provider;


                Miscellaneous.Instance.SetGPSLocation(
                    ApplicationSessionState.DeviceInfo.Data.MAC,
                    ApplicationSessionState.User.Data.UserId,
                    location.Latitude,
                    location.Longitude,
                    location.Altitude,
                    location.Accuracy,
                    location.Bearing,
                    location.Speed,
                    location.Provider);

               

            }
            catch
            {
            }
        }

        public static GPSLocationData GetGPSData()
        {
            GPSLocationData data = new GPSLocationData();
            data.Accuracy = ApplicationSessionState.Accuracy;
            data.Altitude = ApplicationSessionState.Altitude;
            data.Bearing = ApplicationSessionState.Bearing;
            data.Latitude = ApplicationSessionState.Latitude;
            data.Longitude = ApplicationSessionState.Longitude;
            data.Provider = ApplicationSessionState.Provider;  
            data.Speed = ApplicationSessionState.Speed;  
            return data;
        }

        public void OnProviderDisabled (string provider)
        {
             
        }
        public void OnProviderEnabled (string provider)
        {
             
        }
        public void OnStatusChanged (string provider, Availability status, Bundle extras)
        {
            
        }

        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            StopWorker();
        }
 
        protected override void OnPause ()
        {
            base.OnPause ();

            ApplicationSessionState.LastUseTime = DateTime.UtcNow;

            locMgr.RemoveUpdates (this);
        }




        public int Duration()
        {
            TimeSpan ts =  DateTime.UtcNow - ApplicationSessionState.LastUseTime;
            int duration = ts.Minutes;
              //duration = ts.Seconds;
            return duration;
        }

        protected override void OnResume()
        {
            base.OnResume();

            string fing = Build.Fingerprint;
            bool isEmulator=false;
            if (fing != null) {
                isEmulator =  fing.Contains("vbox") || fing.Contains("generic");
            }

//            if(isEmulator)
//                Toast.MakeText(this,"Emulator",ToastLength.Long).Show();
//            else
//                Toast.MakeText(this,"Device",ToastLength.Long).Show();


            locMgr = GetSystemService (Context.LocationService) as LocationManager;
                if (locMgr.AllProviders.Contains (LocationManager.NetworkProvider)
                    && locMgr.IsProviderEnabled (LocationManager.NetworkProvider)) {
                    locMgr.RequestLocationUpdates (LocationManager.NetworkProvider, 30000, 1, this);
                } else {
                    //Toast.MakeText (this, "The Network Provider does not exist or is not enabled!", ToastLength.Long).Show ();
                    

                if (!ApplicationSessionState.IsTest(this) && !isEmulator)
                {

              
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick +=  (bool result) => 
                            {
                                DoLogOut();
                            };
//                        m.OnConfirmationClick += delegate(bool result)
//                        {
//                            
//                        };
                        m.ShowMessage(Resource.String.Android_Location_Services_Error);
              

                
                
                }
                    



                }




            int duration = Duration();
 

            if (!ApplicationSessionState.User.Data.IsAthenticated || (ApplicationSessionState.CompanySecuritySettings.Data.SessionTimeout > 0 && duration > ApplicationSessionState.CompanySecuritySettings.Data.SessionTimeout))
            {

                DoTimeOut();

            }
            else
            {
                if (!CheckUserSession())
                {
                    DoLogOut();
                }
            }
   

        }

        public void LoadConditions()
        {
            if (ApplicationSessionState.ConditionList == null)
                {
                    ApplicationSessionState.ConditionList = MCH.Communication.CargoSnapShot.Instance.GetConditionTypes(ApplicationSessionState.User.Data.CompanyId);
                }
                if (ApplicationSessionState.ConditionList.Data != null)
                {
                    foreach (var condition in ApplicationSessionState.ConditionList.Data)
                    {
                        if (!condition.IsDamage)
                        {
                            ImageGallery.GoodConditionId = condition.ConditionId;
                        }
               
                    }
                }
        }


        public void GoToLookup(Type backScreen, string barcode, string title)
        {
            Lookup_SessionState.BackScreen = backScreen;
            Lookup_SessionState.Title = title.ToUpper();
            if (barcode == string.Empty)
            {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new Lookup_ScanDialog(this,Lookup_SessionState.Title);
 
                dialogFragment.OkClicked += (RelocateTaskItem task) => 
                    {
                        Lookup_SessionState.CurrentRelocateTask = task;
                        GoToScreen(typeof(Lookup_MainActivity));
                    };

                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }
            else
            {
                RelocateTask task = MCH.Communication.Relocate.Instance.GetRelocateTask(ApplicationSessionState.SelectedWarehouseId,  ApplicationSessionState.User.Data.UserId,barcode);
                if (task.Transaction.Status == true && task.Data != null)
                {
                    Lookup_SessionState.CurrentRelocateTask = task.Data;
                    GoToScreen(typeof(Lookup_MainActivity));
                }
            }

        }


        public void GoToOverpack(Type backScreen, long currentTaskId, string barcode)
        {
            Overpack_SessionState.BackScreen = backScreen;
            Overpack_SessionState.CurrentTaskId = currentTaskId;
            if (barcode == string.Empty)
            {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new Overpack_ScanDialog(this);
                dialogFragment.OkClicked += (OverPackTaskItem task) => 
                    {
                        Overpack_SessionState.CurrentOverpackTask = task;
                        GoToScreen(typeof(Overpack_MainActivity));
                    };
 
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }
            else
            {
                OverPackTask task = MCH.Communication.Overpack.Instance.GetOverPackTask(ApplicationSessionState.SelectedWarehouseId,currentTaskId,  ApplicationSessionState.User.Data.UserId,barcode);
                if (task.Transaction.Status == true && task.Data != null)
                {
                    Overpack_SessionState.CurrentOverpackTask = task.Data;
                    GoToScreen(typeof(Overpack_MainActivity));
                }
            }
             
        }

        public void GoToCamera(Type backScreen, Type nextScreen, long currentTaskId)
        {
            GoToCamera(backScreen, nextScreen, currentTaskId, true);
        }

        public void GoToCamera(Type backScreen, Type nextScreen, long currentTaskId, bool confirmSkip)
        {
 
            ImageGallery.Instance.ImageItems.Clear();
                ImageGallery.CurrentTaskId = currentTaskId;
                ImageGallery.BackScreen = backScreen;
                ImageGallery.NextScreen = nextScreen;

                LoadConditions();
      
                var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new Camera_ScanDialogActivity(this);
            dialogFragment.OkClicked+= (SnapShotTaskItem task) => 
                {
                    ImageGallery.ImageGalleryTaskId = task.SnapShotTaskId;
                    ImageGallery.ImageGalleryReference = task.SnapShotReference;
                    ImageGallery.Instance.Clear();
                    var cameraScreen = new Intent(this, typeof(Camera_GalleryActivity));
                    cameraScreen.PutExtra("ConfirmSkip", confirmSkip.ToString());
                    StartActivity(cameraScreen);
                    this.Finish(); 
                };

                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
     
 
        }
 

        public void GoToCamera(Type backScreen, Type nextScreen,long taskId, string barcode)
        {
            GoToCamera(backScreen,nextScreen,taskId,barcode,true);
        }

        public void GoToCamera(Type backScreen, Type nextScreen,long taskId, string barcode, bool confirmSkip)
        {

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Please_Wait), true);
            new Thread(new ThreadStart(delegate
                {

                    if (barcode != string.Empty)
                    {

                        SnapShotTask task =  MCH.Communication.CargoSnapShot.Instance.GetSnapShotTask(ApplicationSessionState.SelectedWarehouseId, taskId,ApplicationSessionState.User.Data.UserId, barcode);


                        if(task.Transaction.Status)
                        {

                            LoadConditions();
                            ImageGallery.BackScreen = backScreen;
                            ImageGallery.NextScreen = nextScreen;
                            ImageGallery.ImageGalleryTaskId = task.Data.SnapShotTaskId;
                            ImageGallery.ImageGalleryReference = task.Data.SnapShotReference;
                            ImageGallery.Instance.Clear();
                            var cameraScreen = new Intent(this, typeof(Camera_GalleryActivity));
                            cameraScreen.PutExtra("ConfirmSkip", confirmSkip.ToString());
                            StartActivity(cameraScreen);
                            this.Finish(); 

 
                        }
                        else
                        {
                            MessageBox m = new MessageBox(this);
                            m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
                        }
 
                    }       
                    else
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
                    }

                    this.RunOnUiThread(() => progressDialog.Hide());
                })).Start();

 
        }

//        private void OnBarcodeClickAction(MCH.Communication.SnapShotTaskItem  task)
//        {
// 
//            ImageGallery.ImageGalleryTaskId = task.SnapShotTaskId;
//            ImageGallery.ImageGalleryReference = task.SnapShotReference;
//            ImageGallery.Instance.Clear();
//            var cameraScreen = new Intent(this, typeof(Camera_GalleryActivity));
//            //nextScreen.PutExtra("Title", taskTitle);
//            //nextScreen.PutExtra("Icon", ApplicationSessionState.SelectedMenuItem.IconKey);
//            StartActivity(cameraScreen);
//            this.Finish(); 
//        }



        public void GoToScreen(Type screen)
        {
            StartActivity (screen);
            this.Finish();
        }


        public void GotoMainMenu()
        {
            StartActivity (typeof(MainMenuActivity));
            this.Finish();
        }

        private void OnPinClickAction(MCH.Communication.User  user)
        {
            IsLoginDialogOpen = false;
            if (user!=null)
            {
                if (user.Data.UserId == ApplicationSessionState.User.Data.UserId)
                {
                    ApplicationSessionState.User.Data.IsAthenticated =true;
                    ApplicationSessionState.LastUseTime = DateTime.UtcNow;
                }
                else
                {
                    ApplicationSessionState.User = user;

                    ApplicationSessionState.UserWarehouses =   MCH.Communication.Menu.Instance.GetUserWarehouses(ApplicationSessionState.User.Data.UserId,ApplicationSessionState.User.Data.ShellSetting.WarehouseIds);
                    if (ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId != null)
                    {
                        ApplicationSessionState.SelectedWarehouseId=long.Parse(ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId.ToString());
                    }
                    else
                    {
                        ApplicationSessionState.SelectedWarehouseId =0;
                    }

                    GotoMainMenu();
                }

            }
            else
            {
                DoLogOut();
            }
        }



        public void DoLogOut()
        {
            ApplicationSessionState.User.Data.IsAthenticated =false;
            if (ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.Pin || ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.PinAndPassword)
            {
                var intent = new Intent (this, typeof (PinActivity));
                intent.SetFlags (ActivityFlags.ClearTop);
                StartActivity(intent);
            }
            else
            {
                var intent = new Intent (this, typeof (LoginActivity));
                intent.SetFlags (ActivityFlags.ClearTop);
                StartActivity(intent);
            }
            this.Finish();
        }

        private void DoTimeOut()
        {
            ApplicationSessionState.User.Data.IsAthenticated =false;
            if (ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.Pin || ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.PinAndPassword)
            {
                if (!IsLoginDialogOpen)
                {
                    IsLoginDialogOpen = true;
                    Action<MCH.Communication.User> PinClickAction = OnPinClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new PinDialogActivity(this,false, PinClickAction,Resource.String.Session_Time_Out,Resource.String.Session_Time_Out_Pin_Message);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                }
 
            }
            else
            {
                DoLogOut();
            }

        }


        public void DoSupervisorOverride(Action<MCH.Communication.User>  supervisorOverrideAction)
        {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new PinDialogActivity(this,true, supervisorOverrideAction,Resource.String.Supervisor_Override,Resource.String.Supervisor_Override_Message,Resource.String.Supervisor_Override_Accept,Resource.String.Supervisor_Override_Reject);
                dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

 
        public void ShowAlertMessage(int message)
        {
            MessageBox m = new MessageBox(this);
            m.ShowMessage(message);
        }

        public void ShowAlertMessage(string message)
        {
            MessageBox m = new MessageBox(this);
            m.ShowMessage(message);
        }


        public void ConfirmLogOut()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) => 
                {
                    if(result)
                    {
                        DoLogOut();
                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Logout);

 
        }


        public void ConfirmExit()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) => 
                {
                    if(result)
                    {
                        this.Finish();
                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Exit_Application);


 
        }


        private int ConvertPixelsToDp(float pixelValue)
        {
            var dp = (int) ((pixelValue)/Resources.DisplayMetrics.Density);
            return dp;
        }


        public int GetScreenWidth()
        {
            var metrics = Resources.DisplayMetrics;
            var widthInDp = ConvertPixelsToDp(metrics.WidthPixels);
            return widthInDp;
        }

        public int GetScreenHeight()
        {
            var metrics = Resources.DisplayMetrics;
            var heightInDp = ConvertPixelsToDp(metrics.HeightPixels);
            return heightInDp;
        }


        private void StartWorker()
        {
            if (ApplicationSessionState.CompanySecuritySettings.Data.IsSingleSession)
            {
                if (this.bgWorker != null && !this.bgWorker.IsBusy)
                    this.bgWorker.RunWorkerAsync();
            }

        }

        private void StopWorker()
        {
            if (ApplicationSessionState.CompanySecuritySettings.Data.IsSingleSession)
            {
                if (this.bgWorker != null && this.bgWorker.IsBusy)
                    this.bgWorker.CancelAsync();
            }
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (this.bgWorker.CancellationPending) {
                    
                    //RunOnUiThread (() => labelDisplay.Text = "CancellationPending");
                    break;
                }
                else
                {

            
                    if (!CheckUserSession())
                    {
                        DoLogOut();
                        break;
                    }

                    //RunOnUiThread(() => labelDisplay.Text = counter.ToString());
                }

                Thread.Sleep (ApplicationSessionState.CheckSessionInterval);




            }
        }


        private bool CheckUserSession()
        {

             MCH.Communication.UserSessionParameters parameters = new MCH.Communication.UserSessionParameters();
            parameters.Location = new MCH.Communication.GeoLocation();
            parameters.Location.Longitude = ApplicationSessionState.Longitude;
            parameters.Location.Latitude = ApplicationSessionState.Latitude;
            parameters.AppName = ApplicationSessionState.SelectedApplication.Name;
            parameters.DeviceId = ApplicationSessionState.GetMac(this);
            parameters.SessionId = ApplicationSessionState.User.Data.SessionId;
            parameters.UserId = ApplicationSessionState.User.Data.UserId;

            bool result = false;
            MCH.Communication.UserSession userSession =  MCH.Communication.Membership.Instance.CheckUserSession(parameters);
            if (userSession.Transaction.Status)
            {
                result = userSession.Data.IsValid;
            }
            else
            {
                result = false;
            }

//            if (!result)
//            {
//                MessageBox m = new MessageBox(this);
//                m.OnConfirmationClick += (bool e) => 
//                    {
//                        DoLogOut();
//                    };
//                m.ShowAlertMessage("Session has expired");
//            }
//
 
            return result;
        }


        public void DoUpload()
        {
            //ImageGallery.Instance


            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Uploading_Images), true);
            new Thread(new ThreadStart(delegate
                {

                    //Thread.Sleep(1000);
    

                    foreach(var i in ImageGallery.Instance.ImageItems)
                    {
                        if(i.Uploaded)
                        {
                            continue;
                        }

                        MCH.Communication.SnapShotImageItem image = new MCH.Communication.SnapShotImageItem();
                        image.SnapShotImageConditionItems = new List<MCH.Communication.SnapShotImageConditionItem>();


                        if(i.Annotations.Count > 0)
                        {
                            foreach (var a in i.Annotations)
                        {
                            MCH.Communication.SnapShotImageConditionItem c = new MCH.Communication.SnapShotImageConditionItem();
                            c.ConditionId = a.AnnotationId;
                            c.X = a.X;
                            c.Y = a.Y;
                            image.SnapShotImageConditionItems.Add(c);
                        }
                        }
                        else
                        {
                            MCH.Communication.SnapShotImageConditionItem c = new MCH.Communication.SnapShotImageConditionItem();
                            c.ConditionId =  ImageGallery.GoodConditionId;
                            c.X = 0;
                            c.Y = 0;
                        
                            image.SnapShotImageConditionItems.Add(c);
                        }

            

                        image.Latitude = ApplicationSessionState.Latitude;
                        image.Longitude = ApplicationSessionState.Longitude;

                        //image.Rotation = i.Rotation;
                        image.SnapshotTaskId =  ImageGallery.ImageGalleryTaskId;
                        image.UserId = ApplicationSessionState.User.Data.UserId;
 

                      

                        File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);
                        File file = new File(dir, String.Format("{0}.jpg", i.FileName));

                        //File file = new File(dir, String.Format("{0}.png", "Damaged"));


                        if (file.Exists())
                        {
                            byte[] binaryData = BitmapHelper.GetBytesFromImage(file.AbsolutePath);


                            if(i.Rotation>0)
                            {
                                try
                                {
                                    binaryData = BitmapHelper.RotateImage(binaryData,i.Rotation);
                                }
                                catch(Exception ex)
                                {
                                    MessageBox m = new MessageBox(this);
                                    m.ShowMessage(ex.Message + ". " + GetText(Resource.String.Camera_Resolution_Error));
                                    RunOnUiThread(() => progressDialog.Hide());
                                    return;
                                }

                             
                            }

                            string base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);
                            binaryData = null;
                            image.SnapshotImage = "data:image/jpg;base64," + base64String;

                            base64String = null;
                            image.PieceCount = 1;
                            MCH.Communication.CommunicationTransaction t =  MCH.Communication.CargoSnapShot.Instance.UploadSnapShotImage(image);
                            if(t.Status)
                            {
                                i.Uploaded = true;
                            }
                            else
                            {
                                i.UploadError = t.Error;
                            }

                            image.SnapshotImage =null;
                             
                        }
                         
                      

                    }



                    foreach(var i in ImageGallery.Instance.ImageItems)
                    {
                        if(!i.Uploaded)
                        {
                             MessageBox m = new MessageBox(this);
                            m.ShowMessage(i.UploadError);
                             RunOnUiThread(() => progressDialog.Hide());
                             return;
                        }
                    }



                    RunOnUiThread (delegate {
                        DeleteAllFiles();
                     });
 


                    


                    StartActivity(ImageGallery.NextScreen);
                    this.Finish();

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }


        
        public void DeleteAllFiles()
        {



            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Deleting_Images), true);
            new Thread(new ThreadStart(delegate
                {

                 

                    foreach (var image in ImageGallery.Instance.ImageItems)
                    {
                        DeleteFile(image.FileName);

                    }
                    ImageGallery.Instance.Clear();

                    RunOnUiThread (delegate {
           
                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();




        }

        public void DeleteFile(string fileName)
        {
            File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);
            File file = new File(dir, String.Format("{0}.jpg", fileName));

            if (file.Exists())
            {   
                file.Delete();
            }
  
        }



        public List< MCH.Communication.SelectionItem> GetCameraResolutions()
        {
            List< MCH.Communication.SelectionItem> resolutionList = new List< MCH.Communication.SelectionItem>();
            try
            {
                Camera camera;
                camera = Android.Hardware.Camera.Open();
                var p = camera.GetParameters();
                IList<Camera.Size> sizes = p.SupportedPictureSizes;



                int resolution = 0;
                if (ApplicationSessionState.GetString(this, "Resolution") == string.Empty)
                {
                    resolution = (((sizes.Count - (sizes.Count % 2)) / 2) + (sizes.Count % 2)) - 1;
                    ApplicationSessionState.SaveString(this,"Resolution",resolution.ToString());
                }
                else
                {
                    resolution = ApplicationSessionState.GetInt(this, "Resolution");
                }


         
                for (int i = 0; i < sizes.Count; i++)
                {   
                    bool isDefault = false;
                    if (resolution == i)
                    {
                        isDefault = true;
                    }
                    resolutionList.Add(new MCH.Communication.SelectionItem(string.Format("{0}x{1}",sizes[i].Width,sizes[i].Height),i,Resource.Drawable.Galery,isDefault));

                }

                camera.Release();     
            }
            catch
            {
                //do nothing 
            }

           
            return resolutionList;

        }

        public void ShowCameraResolutions()
        {
            List< MCH.Communication.SelectionItem> resolutionList = GetCameraResolutions();
            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnResolutionClickSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Camera_Resolution), this, resolutionList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }
 
        private void OnResolutionClickSelectionAction(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                ApplicationSessionState.SaveString(this,"Resolution",r.Id.ToString());
            }
        }


        private void ResetDevice()
        {
                   // MCH.Communication.DeviceInfo device 
                    ApplicationSessionState.DeviceInfo.Data.MAC = "00:00:00:00:00:00";
                    ApplicationSessionState.DeviceInfo = MCH.Communication.Membership.Instance.SaveDevice(ApplicationSessionState.DeviceInfo);
                    if (!ApplicationSessionState.DeviceInfo.Transaction.Status)
                    {
                        MessageBox m = new MessageBox(this);
                m.ShowMessage(ApplicationSessionState.DeviceInfo.Transaction.Error);
                        return;
                    }
                    else
                    {
//                        var intent = new Intent (this, typeof (SplashActivity));
//                        intent.SetFlags (ActivityFlags.ClearTop);
//                        StartActivity(intent);
//                        this.Finish();
                        RestartApplication();
                        return;
                    }
        }

        public void RestartApplication()
        {
            var intent = new Intent (this, typeof (MainActivity));
            intent.SetFlags (ActivityFlags.ClearTop);
            StartActivity(intent);
            this.Finish();
        }

        private void OnSettingsClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case OptionActions.Device:
                    


                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) => 
                    {
                            if(result)
                            {
                                ResetDevice();
                            }
                    };
                    m.ShowConfirmationMessage(Resource.String.Reset_Device_Confirmation);


                    break;
                case OptionActions.Connection:
                    ShowConnectionModes();    
                    break;
                case OptionActions.Camera:
                    ShowCameraResolutions();
                    break;
                case OptionActions.Gallery:
                    ShowLimit();
                    break;
                case OptionActions.Log:
                    ShowLogSetting();
                    break;
                case OptionActions.Info:
                    LoadInfo();
                    break;
                case OptionActions.Printers:
                    ShowPrinters();
                    break;
            }
    
        }

        public void LoadInfo()
        {
            List< MCH.Communication.SelectionItem> modeList = new List< MCH.Communication.SelectionItem>();
 
            modeList.Add(new MCH.Communication.SelectionItem("Company: Cargomatrix Inc.", 0, Resource.Drawable.Icon, false));
            modeList.Add(new MCH.Communication.SelectionItem("Phone: +1 516-792-0400", 0, Resource.Drawable.Phone, false));
            modeList.Add(new MCH.Communication.SelectionItem("Email: support@cargomatrix.com", 0, Resource.Drawable.Mail, false));
            modeList.Add(new MCH.Communication.SelectionItem("Application Name: " + ApplicationSessionState.ApplicationName, 0, Resource.Drawable.Device, false));
            modeList.Add(new MCH.Communication.SelectionItem("Version: " + ApplicationSessionState.GetVersion(this), 0, Resource.Drawable.Device, false));
            modeList.Add(new MCH.Communication.SelectionItem("Account: " + ApplicationSessionState.DeviceInfo.Data.CompanyName, 0, Resource.Drawable.Home, false));
            modeList.Add(new MCH.Communication.SelectionItem("User: " + ApplicationSessionState.User.Data.FormatUserName, 0, Resource.Drawable.User, false));

            modeList.Add(new MCH.Communication.SelectionItem("User Id: " + ApplicationSessionState.User.Data.UserId.ToString(), 0, Resource.Drawable.User, false));
 

            modeList.Add(new MCH.Communication.SelectionItem("Mac: " + ApplicationSessionState.GetMac(this), 0, Resource.Drawable.Device, false));
            modeList.Add(new MCH.Communication.SelectionItem("IP: " + ApplicationSessionState.GetIp(this), 0, Resource.Drawable.Cloud, false));
            modeList.Add(new MCH.Communication.SelectionItem("Device Type: " + ApplicationSessionState.GetDeviceName(), 0, Resource.Drawable.Device, false));
            modeList.Add(new MCH.Communication.SelectionItem("Device Number: " + ApplicationSessionState.GetDeviceId(this), 0, Resource.Drawable.Device, false));
            modeList.Add(new MCH.Communication.SelectionItem("Device Id: " + ApplicationSessionState.DeviceInfo.Data.Id, 0, Resource.Drawable.Device, false));
            modeList.Add(new MCH.Communication.SelectionItem("Serial: " + ApplicationSessionState.GetDeviceSerial(), 0, Resource.Drawable.Device, false));
            modeList.Add(new MCH.Communication.SelectionItem("OS: " + ApplicationSessionState.GetDeviceOS(), 0, Resource.Drawable.Android, false));
            modeList.Add(new MCH.Communication.SelectionItem("Session: " + ApplicationSessionState.User.Data.SessionId, 0, Resource.Drawable.Device, false));
            modeList.Add(new MCH.Communication.SelectionItem("Session Timeout (min): " + ApplicationSessionState.CompanySecuritySettings.Data.SessionTimeout.ToString(), 0, Resource.Drawable.Clock, false));
            modeList.Add(new MCH.Communication.SelectionItem("Latitude: " + ApplicationSessionState.Latitude.ToString(), 0, Resource.Drawable.Location, false));
            modeList.Add(new MCH.Communication.SelectionItem("Longitude: " + ApplicationSessionState.Longitude.ToString(), 0, Resource.Drawable.Location, false));
            modeList.Add(new MCH.Communication.SelectionItem("Language: " + GetText(Resource.String.Language), 0, Resource.Drawable.World, false));
 
            if (ApplicationSessionState.User.Data.IsAdmin)
            {
                modeList.Add(new MCH.Communication.SelectionItem("URL: " + MCH.Communication.WebService.Instance.ShellPath, 0, Resource.Drawable.Cloud, false)); 
                modeList.Add(new MCH.Communication.SelectionItem("APP URL: " + MCH.Communication.WebService.Instance.AppPath, 0, Resource.Drawable.Cloud, false)); 
            }

            modeList.Add(new MCH.Communication.SelectionItem("Mode: " + ApplicationSessionState.Mode(this).ToString(), 0, Resource.Drawable.Security, false)); 


            int limit = ApplicationSessionState.MaxImageCount(this);
            modeList.Add(new MCH.Communication.SelectionItem("Picture Limit: " + (limit == 0  ?  GetText(Resource.String.NoLimit) : limit.ToString()), 0, Resource.Drawable.Galery, false));
 

            List< MCH.Communication.SelectionItem> resolutionList = GetCameraResolutions();
            foreach (var r in resolutionList)
            {
                if (r.Selected)
                {
                    modeList.Add(new MCH.Communication.SelectionItem("Picture Resolution: " + r.Name, 0, Resource.Drawable.Camera, false));
                    break;
                }
            }

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.System_Information), this, modeList, null, SelectionListAdapter.SelectionMode.Row, false);
            dialogFragment.Cancelable = false;
            dialogFragment.CancelButtonText = GetText(Resource.String.Close);
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        public void ShowLogSetting()
        {
             


            List< MCH.Communication.SelectionItem> modeList = new List< MCH.Communication.SelectionItem>();
            if (ApplicationSessionState.LogEnable(this))
            {
                modeList.Add(new MCH.Communication.SelectionItem(GetText(Resource.String.On),1,Resource.Drawable.Galery,true));
                modeList.Add(new MCH.Communication.SelectionItem(GetText(Resource.String.Off),0,Resource.Drawable.Galery,false));
            }
            else
            {
                modeList.Add(new MCH.Communication.SelectionItem(GetText(Resource.String.On),1,Resource.Drawable.Galery,false));
                modeList.Add(new MCH.Communication.SelectionItem(GetText(Resource.String.Off),0,Resource.Drawable.Galery,true));
            }
 
            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnSettingClickSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Enable_Data_Logging), this, modeList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
             
        }

        private void OnSettingClickSelectionAction(List<MCH.Communication.SelectionItem>  result, bool isExtra)
        {
            foreach (var r in result)
            {
                if (r.Id == 0)
                {
                    ApplicationSessionState.UpdateLogEnable(this,false);
                }
                else
                {
                    ApplicationSessionState.UpdateLogEnable(this,true);
                }

            }
        }


        public void ShowLimit()
        {
            int max = int.Parse(GetText(Resource.String.MaxImageCount));


            List< MCH.Communication.SelectionItem> modeList = new List< MCH.Communication.SelectionItem>();
            for (int i = 0; i <= max; i++)
            {
                bool isDefault = false;
                if (ApplicationSessionState.MaxImageCount(this) == i)
                {
                    isDefault = true;
                }
 
                modeList.Add(new MCH.Communication.SelectionItem(i == 0  ?  GetText(Resource.String.NoLimit) : i.ToString(),i,Resource.Drawable.Galery,isDefault));
            }
 
            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnLimitClickSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Set_Image_Limit), this, modeList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
             
        }

        private void OnLimitClickSelectionAction(List<MCH.Communication.SelectionItem>  result, bool isExtra)
        {
            foreach (var r in result)
            {
                ApplicationSessionState.UpdateMaxImageCount(this,(int)r.Id);
            }
        }


        public void ShowConnectionModes()
        {
            List< MCH.Communication.SelectionItem> modeList = new List< MCH.Communication.SelectionItem>();

            switch (ApplicationSessionState.Mode(this))
            {
                case ApplicationSessionState.ConnectionType.PROD:
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.PROD.ToString(),(int)ApplicationSessionState.ConnectionType.PROD,Resource.Drawable.Security,true));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.DEV.ToString(),(int)ApplicationSessionState.ConnectionType.DEV,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.QA.ToString(),(int)ApplicationSessionState.ConnectionType.QA,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.UAT.ToString(),(int)ApplicationSessionState.ConnectionType.UAT,Resource.Drawable.Security,false));
                    break;
                case ApplicationSessionState.ConnectionType.DEV:
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.PROD.ToString(),(int)ApplicationSessionState.ConnectionType.PROD,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.DEV.ToString(),(int)ApplicationSessionState.ConnectionType.DEV,Resource.Drawable.Security,true));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.QA.ToString(),(int)ApplicationSessionState.ConnectionType.QA,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.UAT.ToString(),(int)ApplicationSessionState.ConnectionType.UAT,Resource.Drawable.Security,false));
                    break;
                case ApplicationSessionState.ConnectionType.UAT:
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.PROD.ToString(),(int)ApplicationSessionState.ConnectionType.PROD,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.DEV.ToString(),(int)ApplicationSessionState.ConnectionType.DEV,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.QA.ToString(),(int)ApplicationSessionState.ConnectionType.QA,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.UAT.ToString(),(int)ApplicationSessionState.ConnectionType.UAT,Resource.Drawable.Security,true));
                    break;
                case ApplicationSessionState.ConnectionType.QA:
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.PROD.ToString(),(int)ApplicationSessionState.ConnectionType.PROD,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.DEV.ToString(),(int)ApplicationSessionState.ConnectionType.DEV,Resource.Drawable.Security,false));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.QA.ToString(),(int)ApplicationSessionState.ConnectionType.QA,Resource.Drawable.Security,true));
                    modeList.Add(new MCH.Communication.SelectionItem(ApplicationSessionState.ConnectionType.UAT.ToString(),(int)ApplicationSessionState.ConnectionType.UAT,Resource.Drawable.Security,false));
                    break;
            }


 
            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnModeClickSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Connection_Mode), this, modeList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
             
        }
        private void OnModeClickSelectionAction(List<MCH.Communication.SelectionItem>  result, bool isExtra)
        {
            
            foreach (var r in result)
            {

                ApplicationSessionState.UpdateConnection(this, (ApplicationSessionState.ConnectionType)r.Id);
                RestartApplication();
 
            }
    
        }

        public void LoadSettings()
        {

            if (!ApplicationSessionState.User.Data.IsAdmin)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.No_Privilege_Error);
                return;
            }
            else
            {
                List<OptionItem> options = new List<OptionItem>();
                options.Add(new OptionItem(GetText(Resource.String.Reset_Device), OptionActions.Device, Resource.Drawable.Device));
                options.Add(new OptionItem(GetText(Resource.String.Printers) + GetDefautPrinterHeader(), OptionActions.Printers, Resource.Drawable.Printer));
                options.Add(new OptionItem(GetText(Resource.String.Connection_Mode), OptionActions.Connection, Resource.Drawable.Security));
                options.Add(new OptionItem(GetText(Resource.String.Camera_Resolution), OptionActions.Camera, Resource.Drawable.Camera));
                options.Add(new OptionItem(GetText(Resource.String.Enable_Data_Logging), OptionActions.Log, Resource.Drawable.Pen));
                options.Add(new OptionItem(GetText(Resource.String.Set_Image_Limit), OptionActions.Gallery, Resource.Drawable.Galery));
                options.Add(new OptionItem(GetText(Resource.String.System_Information), OptionActions.Info, Resource.Drawable.Info));
                Action<OptionItem> SettingsClickAction = OnSettingsClickAction;
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new OptionDialog(this, options, SettingsClickAction, Resource.String.Settings);
                dialogFragment.Cancelable = true;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }

 
        }

        public string GetDefautPrinterHeader()
        {
            return string.Format(" ({0})",GetDefautPrinter());
        }
        public string GetDefautPrinter()
        {
           
            MCH.Communication.Printers printers = MCH.Communication.Miscellaneous.Instance.GetPrinters(ApplicationSessionState.User.Data.UserId, MCH.Communication.PrinterTypes.Label);
            if (printers.Transaction.Status)
            {


                if (printers.Data != null)
                {

                    foreach (var printer in printers.Data)
                    {
                        if (printer.IsDefault)
                        {
                            return printer.PrinterName;
                        }

                    
                    }
 
                }

            }

            return string.Empty;
        }

        public void ShowPrinters()
        {
            List< MCH.Communication.SelectionItem> printersList = new List< MCH.Communication.SelectionItem>();
            MCH.Communication.Printers printers = MCH.Communication.Miscellaneous.Instance.GetPrinters(ApplicationSessionState.User.Data.UserId, MCH.Communication.PrinterTypes.Label);
            if (printers.Transaction.Status)
            {


                if (printers.Data != null)
                {

                    if (printers.Data.Count == 0)
                    {
                        MessageBox ms = new MessageBox(this);
                        ms.ShowAlert("There are not priters available", MessageBox.AlertType.Information);
                        return;
                    }


                    foreach (var printer in printers.Data)
                    {
                        printersList.Add(new MCH.Communication.SelectionItem(printer.PrinterName, printer.PrinterId, @"Icons/Printer", printer.IsDefault));
                    }

                    bool allowSearch = (printersList.Count > 10) ? true : false;
                    Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnPrinterClickSelectionAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Printers), this, printersList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection, allowSearch);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                }
                else
                {
                    MessageBox ms = new MessageBox(this);
                    ms.ShowAlert("There are not priters available", MessageBox.AlertType.Information);
                    return;
                }
            }
            else
            {
                MessageBox ms = new MessageBox(this);
                ms.ShowAlert (printers.Transaction.Error, MessageBox.AlertType.Error);
                return;
            }

        }

        private void OnPrinterClickSelectionAction(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {

            foreach (var r in result)
            {
                MCH.Communication.Miscellaneous.Instance.SetUserDefault(ApplicationSessionState.User.Data.UserId,(int)r.Id);
            }

        }
 
        public   R Invoke<R>(Func<R> action)
        {
            R result = default(R);
            result =  action();
            return result;
        }

        public void Invoke(Action action)
        {
            action();
            return;
        }

//*******Sample***********
//        Func<MenuList> f = () =>
//            {
//                return MCH.Communication.Menu.Instance.GetAppRoleMenu(Application.Context.GetText(Resource.String.ApplicationCode),ApplicationSessionState.User.Data.RoleId,ApplicationSessionState.User.Data.AppUserId,ApplicationSessionState.SelectedWarehouseId);
//            };
//        menulist = Invoke<MenuList>(f);
//
//        menulist = null;
//
//        menulist = Invoke<MenuList>(() => 
//            MCH.Communication.Menu.Instance.GetAppRoleMenu(Application.Context.GetText(Resource.String.ApplicationCode),ApplicationSessionState.User.Data.RoleId,ApplicationSessionState.User.Data.AppUserId,ApplicationSessionState.SelectedWarehouseId)
//        );
//
//        menulist = null;
//
//        menulist = Invoke<MenuList>(delegate {
//            return MCH.Communication.Menu.Instance.GetAppRoleMenu(Application.Context.GetText(Resource.String.ApplicationCode),ApplicationSessionState.User.Data.RoleId,ApplicationSessionState.User.Data.AppUserId,ApplicationSessionState.SelectedWarehouseId); 
//        });

    }
}

