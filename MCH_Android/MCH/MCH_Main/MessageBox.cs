﻿using System;
using Android.App;
using System.Threading.Tasks;
using System.Threading;
namespace MCH
{
    public class MessageBox
    {

        public enum AlertType
        {
            Alert,
            Error,
            Information,
            None,
        }

        private Activity context;
        public delegate void ConfirmationClickEventHandler(bool result);
        public event ConfirmationClickEventHandler OnConfirmationClick;
   

        public delegate void NeutralClickEventHandler();
        public event NeutralClickEventHandler OnNeutalClick;



        public MessageBox(Activity context)
        {
            this.context = context;
        }
 
        public void ShowConfirmationMessage(int message)
        {
            ShowConfirmationMessage(Application.Context.GetText(message), Resource.String.Yes, Resource.String.No);
        }

        public void ShowConfirmationMessage(string message)
        {
            ShowConfirmationMessage(message, Resource.String.Yes, Resource.String.No);
        }

        public void ShowConfirmationMessage(int message,int positiveButtonText,int negativeButtonText)
        {
            ShowConfirmationMessage(Application.Context.GetText(message), Application.Context.GetText(positiveButtonText), Application.Context.GetText(negativeButtonText));
        }

        public void ShowConfirmationMessage(string message,int positiveButtonText,int negativeButtonText)
        {
            ShowConfirmationMessage(message, Application.Context.GetText(positiveButtonText), Application.Context.GetText(negativeButtonText));
        }

        public void ShowConfirmationMessage(int message,string positiveButtonText,string negativeButtonText)
        {
            ShowConfirmationMessage(Application.Context.GetText(message), positiveButtonText,negativeButtonText);
        }



        public void ShowConfirmationMessage(string message,string positiveButtonText,string negativeButtonText ,string neutralButtonText)
        {

            context.RunOnUiThread(delegate
                {

                    //AlertDialog.Builder builder = new AlertDialog.Builder(context,Resource.Style.Theme_Dialog);
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    //builder.SetTitle(Resource.String.app_name);


                    if (ApplicationSessionState.IsTest(context))
                    {
                        builder.SetTitle(Application.Context.GetText(Resource.String.app_name).ToUpper() + " (" + ApplicationSessionState.Mode(context).ToString() + ")");
                    }
                    else
                    {
                        builder.SetTitle(Resource.String.app_name);
                    }


                    builder.SetMessage(message);
                    builder.SetIcon (Resource.Drawable.QuestionIcon);
                    builder.SetCancelable(false);

                    builder.SetPositiveButton(positiveButtonText.ToUpper(),delegate
                        {
                            if(OnConfirmationClick!=null)
                            {
                                OnConfirmationClick.Invoke(true);
                                OnConfirmationClick = null;
                            }
                        });
                    builder.SetNegativeButton(negativeButtonText.ToUpper(),delegate 
                        {
                            if(OnConfirmationClick!=null)
                            {
                                OnConfirmationClick.Invoke(false);
                                OnConfirmationClick = null;
                            }
                        });

                    builder.SetNeutralButton(neutralButtonText.ToUpper(),delegate 
                        {
                            if(OnNeutalClick!=null)
                            {
                                OnNeutalClick.Invoke();
                                OnNeutalClick = null;
                            }
                        });

                    AlertDialog dialog = builder.Create();
                    dialog.Show();


                });

            MediaSounds.Instance.Confirm(this.context);
            return;

        }


        public void ShowConfirmationMessage(string message,string positiveButtonText,string negativeButtonText,AlertType alertType)
        {

            context.RunOnUiThread(delegate
                {

                    //AlertDialog.Builder builder = new AlertDialog.Builder(context,Resource.Style.Theme_Dialog);
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    //builder.SetTitle(Resource.String.app_name);


                    if (ApplicationSessionState.IsTest(context))
                    {
                        builder.SetTitle(Application.Context.GetText(Resource.String.app_name).ToUpper() + " (" + ApplicationSessionState.Mode(context).ToString() + ")");
                    }
                    else
                    {
                        builder.SetTitle(Resource.String.app_name);
                    }


                    builder.SetMessage(message);
                    builder.SetIcon (Resource.Drawable.QuestionIcon);


                    switch(alertType)
                    {
                        case AlertType.Alert:
                            builder.SetIcon(Resource.Drawable.AlertIcon);
                            break;
                        case AlertType.Error:
                            builder.SetIcon(Resource.Drawable.ErrorIcon);
                            break;
                        case AlertType.Information:
                            builder.SetIcon(Resource.Drawable.InfoIcon);
                            break;
                        case AlertType.None:
                        default:
                            builder.SetIcon(Resource.Drawable.QuestionIcon);
                            break;
                    }


                    builder.SetCancelable(false);

                    builder.SetPositiveButton(positiveButtonText.ToUpper(),delegate
                        {
                            if(OnConfirmationClick!=null)
                            {
                                OnConfirmationClick.Invoke(true);
                                OnConfirmationClick = null;
                            }
                        });
                    builder.SetNegativeButton(negativeButtonText.ToUpper(),delegate 
                        {
                            if(OnConfirmationClick!=null)
                            {
                                OnConfirmationClick.Invoke(false);
                                OnConfirmationClick = null;
                            }
                        });

                    AlertDialog dialog = builder.Create();
                    dialog.Show();


                });

            MediaSounds.Instance.Confirm(this.context);
            return;

        }

        public void ShowConfirmationMessage(string message,string positiveButtonText,string negativeButtonText)
        {

            context.RunOnUiThread(delegate
                {

                    //AlertDialog.Builder builder = new AlertDialog.Builder(context,Resource.Style.Theme_Dialog);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            //builder.SetTitle(Resource.String.app_name);


                    if (ApplicationSessionState.IsTest(context))
                    {
                        builder.SetTitle(Application.Context.GetText(Resource.String.app_name).ToUpper() + " (" + ApplicationSessionState.Mode(context).ToString() + ")");
                    }
                    else
                    {
                        builder.SetTitle(Resource.String.app_name);
                    }


            builder.SetMessage(message);
            builder.SetIcon (Resource.Drawable.QuestionIcon);
            builder.SetCancelable(false);

                    builder.SetPositiveButton(positiveButtonText.ToUpper(),delegate
                {
                    if(OnConfirmationClick!=null)
                    {
                        OnConfirmationClick.Invoke(true);
                        OnConfirmationClick = null;
                    }
                });
                    builder.SetNegativeButton(negativeButtonText.ToUpper(),delegate 
                {
                    if(OnConfirmationClick!=null)
                    {
                        OnConfirmationClick.Invoke(false);
                        OnConfirmationClick = null;
                    }
                });

            AlertDialog dialog = builder.Create();
            dialog.Show();


                });

            MediaSounds.Instance.Confirm(this.context);
            return;

        }

        public void ShowMessage(string message)
        {
            ShowMessage(message,Application.Context.GetText(Resource.String.app_name));
        }

        public void ShowMessage(int message, int tile)
        {
            ShowMessage(Application.Context.GetText(message),Application.Context.GetText(tile));
        }

        public void ShowMessage(int message)
        {
            ShowMessage(Application.Context.GetText(message),Application.Context.GetText(Resource.String.app_name));
        }

        public void ShowMessage(string message, string title)
        {

            context.RunOnUiThread(delegate
                {
                    //AlertDialog.Builder builder = new AlertDialog.Builder(context,Resource.Style.Theme_Dialog);
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    AlertDialog dialog = builder.Create();
                    dialog.SetIcon(Resource.Drawable.Icon);
                   

                    if (ApplicationSessionState.IsTest(context))
                    {
                        dialog.SetTitle(title.ToUpper() + " (" + ApplicationSessionState.Mode(context).ToString() + ")");
                    }
                    else
                    {
                        dialog.SetTitle(title.ToUpper());
                    }

 

                    dialog.SetMessage(message);
                    dialog.SetCancelable(false);
                    dialog.SetButton(Application.Context.GetText(Resource.String.Ok).ToUpper(), delegate
                        {
                            if(OnConfirmationClick!=null)
                            {
                                OnConfirmationClick.Invoke(true);
                                OnConfirmationClick = null;
                            }
                        });
                    dialog.Show();






                });

            MediaSounds.Instance.Alert(this.context);
            return;



        }
  

 

        public void ShowAlert(string message,AlertType alertType)
        {
            ShowAlert(message,Application.Context.GetText(Resource.String.app_name),alertType);
        }

        public void ShowAlert(int message, int tile,AlertType alertType)
        {
            ShowAlert(Application.Context.GetText(message),Application.Context.GetText(tile),alertType);
        }

        public void ShowAlert(int message,AlertType alertType)
        {
            ShowAlert(Application.Context.GetText(message),Application.Context.GetText(Resource.String.app_name),alertType);
        }

        public void ShowAlert(string message, string title,AlertType alertType)
        {

            context.RunOnUiThread(delegate
                {
                    //AlertDialog.Builder builder = new AlertDialog.Builder(context,Resource.Style.Theme_Dialog);
                   AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    AlertDialog dialog = builder.Create();


                    switch(alertType)
                    {
                        case AlertType.Alert:
                            dialog.SetIcon(Resource.Drawable.AlertIcon);
                            break;
                        case AlertType.Error:
                            dialog.SetIcon(Resource.Drawable.ErrorIcon);
                            break;
                        case AlertType.Information:
                            dialog.SetIcon(Resource.Drawable.InfoIcon);
                            break;
                        case AlertType.None:
                         default:
                            dialog.SetIcon(Resource.Drawable.Icon);
                            break;
                    }


                    if (ApplicationSessionState.IsTest(context))
                    {
                        dialog.SetTitle(title.ToUpper() + " (" + ApplicationSessionState.Mode(context).ToString() + ")");
                    }
                    else
                    {
                        dialog.SetTitle(title.ToUpper());
                    }


                    dialog.SetMessage(message);
                    dialog.SetCancelable(false);
                    dialog.SetButton(Application.Context.GetText(Resource.String.Ok).ToUpper(), delegate
                        {
                            if(OnConfirmationClick!=null)
                            {
                                OnConfirmationClick.Invoke(true);
                                OnConfirmationClick = null;
                            }
                        });
                    dialog.Show();






                });



            switch(alertType)
            {
                case AlertType.Alert:
                    MediaSounds.Instance.Alert(this.context);
                    break;
                case AlertType.Error:
                    MediaSounds.Instance.Error(this.context);
                    break;
                case AlertType.Information:
                    MediaSounds.Instance.Alert(this.context);
                    break;
                default:
                    MediaSounds.Instance.Alert(this.context);
                    break;
            }

            return;



        }



    }
}

 