﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

namespace MCH
{
    [Activity (ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]
    public class LoginActivity : Activity
    {
        Button forgotuser;
        Button forgotpassword;
        TextView userName;
        TextView password;
        Button buttonLogin;
		Button pin;
        TextView version;
		TextView rights;
        Switch rememeberSwitch;
        TextView title;
 

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
 
			SetContentView (Resource.Layout.Login);

			version = FindViewById<TextView> (Resource.Id.vesion);
			buttonLogin = FindViewById<Button> (Resource.Id.login);
			userName = FindViewById<TextView> (Resource.Id.userName);
			password = FindViewById<TextView> (Resource.Id.password);
			pin = FindViewById<Button> (Resource.Id.pin);
			rights = FindViewById<TextView> (Resource.Id.rights);

            forgotuser = FindViewById<Button> (Resource.Id.forgotuser);
            forgotpassword = FindViewById<Button> (Resource.Id.forgotpassword);



            title = FindViewById<TextView> (Resource.Id.title);
            if (ApplicationSessionState.IsTest(this))
            {
                title.Text = GetText(Resource.String.ApplicationName) + " (" + ApplicationSessionState.Mode(this).ToString() + ")";
            }
            else
            {
                title.Text = GetText(Resource.String.ApplicationName);
            }
 
 

            forgotuser.Click += (object sender, EventArgs e) => 
                {

            
                    Toast.MakeText (this, "forgotuser", ToastLength.Long).Show ();
                };


            forgotpassword.Click += (object sender, EventArgs e) => 
                {

                    Toast.MakeText (this, "forgotpassword", ToastLength.Long).Show ();
                };

			//password.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;


            if (ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.PinAndPassword) {
				pin.Click += (object sender, EventArgs e) => 
				{
				StartActivity(typeof(PinActivity));
				this.Finish();
				};
			
			}
			else 
			{
				pin.Visibility = ViewStates.Gone;
			}

        

            password.KeyPress += OnKeyPress;

            rememeberSwitch = FindViewById<Switch> (Resource.Id.remember);
            if (ApplicationSessionState.CompanySecuritySettings.Data.AllowRememberMe)
            {
               
                rememeberSwitch.Enabled = true;

                if (ApplicationSessionState.GetString(this, "RememberMe") == "YES")
                {
                    rememeberSwitch.Checked = true;
                    if(userName.Text == string.Empty)
                    {

                        userName.Text = ApplicationSessionState.GetString(this, "LastUser");
                    }

      
                }
                else
                {
                    rememeberSwitch.Checked = false;
                }


                rememeberSwitch.CheckedChange += delegate(object sender, CompoundButton.CheckedChangeEventArgs e) {
                       

                    if(e.IsChecked)
                    {
                        ApplicationSessionState.SaveString(this,"RememberMe","YES");
                        userName.Text = ApplicationSessionState.GetString(this, "LastUser");
                        password.Text = string.Empty;
                    }
                    else
                    {
                        ApplicationSessionState.SaveString(this,"RememberMe","NO");
                        userName.Text = string.Empty;
                        password.Text = string.Empty;
                    }




                };
            }
            else
            {
                userName.Text = string.Empty;
                rememeberSwitch.Enabled = false;

            }





            if(ApplicationSessionState.IsTest(this))
            {
                    userName.Text = GetText(Resource.String.DefaultUser);
                    password.Text = GetText(Resource.String.DefaultPassword);                            
            }


            buttonLogin.Click += (object sender, EventArgs e) =>
            {

                    DoLogin();

            };
 
           
			rights.Text  =string.Format(GetText(Resource.String.All_Rights_Reserved),DateTime.Now.Year);
            version.Text = string.Format(GetText(Resource.String.Version) , ApplicationSessionState.GetVersion(this));




            ImageView  logo = FindViewById<ImageView>(Resource.Id.logo);
            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Companies/{0}.png",ApplicationSessionState.DeviceInfo.Data.CompanyName.ToUpper().Replace(" ","_")));
                Drawable d = Drawable.CreateFromStream(ims, null);
                logo.SetImageDrawable(d);
            } 
            catch 
            {
                //logo.SetImageResource (Resource.Drawable.Cargomatrix);
            }




        }

 


        private void OnKeyPress(object sender, View.KeyEventArgs e) 
        {
 
            string data = pin.Text;
      
            e.Handled = false;
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {     
 

                DoLogin();

                e.Handled = true;
            }
                                

        }

		public override void OnBackPressed ()
		{
            if (ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.PinAndPassword) {
				StartActivity (typeof(PinActivity));
			
			}
			this.Finish ();
		}

        private void DoLogin()
        {
            
            MCH.Communication.UserPasswordParameters parameters = new MCH.Communication.UserPasswordParameters();
            parameters.Location = new MCH.Communication.GeoLocation();
            parameters.Location.Longitude = ApplicationSessionState.Longitude;
            parameters.Location.Latitude = ApplicationSessionState.Latitude;
            parameters.Password = password.Text;
            parameters.UserName  = userName.Text;
            parameters.DeviceId = ApplicationSessionState.GetMac(this);
            parameters.AppName = "ApplicationSessionState.SelectedApplication.Name    find this from a list"; 


            ApplicationSessionState.User =  MCH.Communication.Membership.Instance.AuthenticateAppUser(parameters);
            if(ApplicationSessionState.User.Transaction.Status)
                {
                if(ApplicationSessionState.User.Data.IsAthenticated)
                        {

                            
                    ApplicationSessionState.UserWarehouses =   MCH.Communication.Menu.Instance.GetUserWarehouses(ApplicationSessionState.User.Data.UserId,ApplicationSessionState.User.Data.ShellSetting.WarehouseIds);
                    if (ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId != null)
                            {
                        ApplicationSessionState.SelectedWarehouseId=long.Parse(ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId.ToString());
                            }
                            else
                            {
                        ApplicationSessionState.SelectedWarehouseId =0;
                            }
    
               
                     
                    ApplicationSessionState.SaveString(this, "LastUser", userName.Text);
             
                    if (ApplicationSessionState.DeviceInfo.Data.Id == 0)
                    {
                        ApplicationSessionState.DeviceInfo = MCH.Communication.Membership.Instance.SaveDevice(ApplicationSessionState.DeviceInfo);
                        if (!ApplicationSessionState.DeviceInfo.Transaction.Status)
                        {
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                            {
                                    this.Finish();
                                    return;
                            };
                            m.ShowMessage(ApplicationSessionState.DeviceInfo.Transaction.Error);
                            return;
                        }
                    }

                    ApplicationSessionState.LastUseTime = DateTime.UtcNow;
                    ApplicationSessionState.CurrentCategoryId = null;
                    StartActivity(typeof(MainMenuActivity));
                            this.Finish();

                        }
                        else
                        {

                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(Resource.String.Login_Failed);


                        }

                }
                else
                {

                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.Login_Failed);


                }
        }
 
    }
}

