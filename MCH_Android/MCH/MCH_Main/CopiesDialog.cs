﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class CopiesDialog: DialogFragment
    {

        Button btnCancel;
        Button btnOk;
 
        Activity context;
        string reference;
        string printer;
        string message;
        TextView txtMessage;
        int maxCopies;
        TextView txtReference;
        NumberPicker np;
         
        
        public delegate void OkClickActionEventHandler(int pieces);
        public event OkClickActionEventHandler OkClicked;
 
        public CopiesDialog (Activity context, string reference,string printer,  int maxCopies   )
        {
            this.reference = reference;
            this.context = context;
            this.maxCopies = maxCopies;
            this.printer = printer;
        }

        public CopiesDialog (Activity context, string reference,string printer,  int maxCopies, string message   )
        {
            this.reference = reference;
            this.context = context;
            this.maxCopies = maxCopies;
            this.printer = printer;
            this.message = message;
        }
 

        //ADD this for error when rotating
        public CopiesDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.CopiesDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);
  
            txtReference=DialogInstance.FindViewById<TextView>(Resource.Id.txtReference);
            txtMessage=DialogInstance.FindViewById<TextView>(Resource.Id.txtMessage);
            btnCancel =DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;

            
            if (!string.IsNullOrEmpty(this.message))
            {
                txtMessage.Text = this.message.ToUpper();
            }

            if (!string.IsNullOrEmpty(this.printer))
            {
                txtReference.Text = "REF#: " + this.reference.ToUpper(); 
                txtReference.Visibility = ViewStates.Visible;
            }
            else
            {
                txtReference.Visibility = ViewStates.Gone;
            }

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 
            btnOk.Click += OnOk_Click;

 
            np = (NumberPicker) DialogInstance.FindViewById(Resource.Id.npId);
            np.ValueChanged += (object sender, NumberPicker.ValueChangeEventArgs e) => 
                {
                    //tv.Text = e.NewVal.ToString();
                };  

        
                np.MaxValue = maxCopies;
                np.MinValue = 1;
                np.Value = 1;
        
 
            return DialogInstance;
        }

        private void OnOk_Click(object sender, EventArgs e)
        {
            Validate();
        }
        private void Validate()
        {

        
                if (OkClicked != null)
                OkClicked(np.Value);
        
                Dismiss();
 



        }

   

        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            Validate();
        }

        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            if (string.IsNullOrEmpty("PRINTER: " + this.printer))
            {
                Dialog.Window.SetTitle( GetText( Resource.String.Pieces));    
            }
            else
            {
                Dialog.Window.SetTitle("PRINTER: " + this.printer);
            }

            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }



 


    }
}





