﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class DateDialog: DialogFragment
    {
        TextView txtMessage;
        Button btnCancel;
        Button btnOk;
        
       
        NumberPicker npMonth;
        NumberPicker npDay;
        NumberPicker npYear;


        Activity context;
        string title;
        string message;
 
        DateTime ? date;

        public delegate void OkClickActionEventHandler(DateTime  date);
        public event OkClickActionEventHandler OkClicked;

        public DateDialog (Activity context,string title ,string message, DateTime ? date)
        {

            this.title = title.ToUpper();
            this.message = message.ToUpper();
            this.context = context;
            this.date = date;
        }



        //ADD this for error when rotating
        public DateDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.DateDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);
 

            npMonth= DialogInstance.FindViewById<NumberPicker>(Resource.Id.npMonth);
            npDay= DialogInstance.FindViewById<NumberPicker>(Resource.Id.npDay);
            npYear= DialogInstance.FindViewById<NumberPicker>(Resource.Id.npYear);

            txtMessage = DialogInstance.FindViewById<TextView>(Resource.Id.txtMessage);
            txtMessage.Text = this.message;

            btnCancel = DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 
 
            btnOk.Click += OnOk_Click;
            btnCancel.Click += OnCancelClick;
 
            LoadDate();


            return DialogInstance;
        }


        private void LoadDate()
        {
            DateTime temp = DateTime.Now;
            if (this.date != null)
            {
                temp = (DateTime)this.date;
            }
 
            npMonth.MaxValue = 12;
            npMonth.MinValue = 1;
            npMonth.Value = temp.Month;

            npDay.MaxValue = 31;
            npDay.MinValue = 1;
            npDay.Value = temp.Day;


            string year = string.Format("{0:yy}", temp);

            npYear.MaxValue = int.Parse(year) + 1;
            npYear.MinValue = int.Parse(year) - 1;
            npYear.Value = int.Parse(year);
        }

        private void OnOk_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime temp = new DateTime(2000+npYear.Value, npMonth.Value, npDay.Value);
                if(OkClicked!=null)
                {
                    OkClicked.Invoke(temp);
                }

                Dismiss();  
            }
            catch
            {
                MessageBox m = new MessageBox(this.context);
                m.ShowAlert("Invalid date.", MessageBox.AlertType.Error);
            }
        }
 

        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {

            Dialog.Window.SetTitle( this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;

        }



 

    }
}




