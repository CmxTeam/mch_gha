﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using Android.Views.InputMethods;

namespace MCH
{



    public class LocationListDialogActivity: DialogFragment
    {
        ListView listView;
        List<MCH.Communication.LocationItem> originalLocationList;
        List<MCH.Communication.LocationItem> choiseList;
        Action<List<MCH.Communication.LocationItem>> locationClickAction;
        Activity context;
        string title;
 
        bool allowSeach;

        EditText SearchText;
        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;

        public delegate void OkClickActionEventHandler(List<MCH.Communication.LocationItem> selection);
        public event OkClickActionEventHandler OkClicked;


        public LocationListDialogActivity (string title, Activity context, List<MCH.Communication.LocationItem> locationList,  bool allowSeach)
        {
            this.context = context;
            this.locationClickAction = null;
            this.originalLocationList =  locationList;

            this.title = title;
            this.allowSeach = allowSeach;
        }

        public LocationListDialogActivity (string title, Activity context, List<MCH.Communication.LocationItem> locationList, Action<List<MCH.Communication.LocationItem>> locationClickAction, bool allowSeach)
        {
            this.context = context;
            this.locationClickAction = locationClickAction;
            this.originalLocationList =  locationList;

            this.title = title;
            this.allowSeach = allowSeach;
        }

        //ADD this for error when rotating
        public LocationListDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.LocationListDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);
            listView = DialogInstance.FindViewById<ListView>(Resource.Id.SelectionListView); 
 
            Button close = DialogInstance.FindViewById<Button>(Resource.Id.close);
            close.Click += OnCloseClick;

 
                if (locationClickAction != null)
                {
                listView.ItemClick -= OnListItemClick;
                    listView.ItemClick += OnListItemClick;
                }

            if (OkClicked != null)
            {
                listView.ItemClick -= OnListItemClick;
                listView.ItemClick += OnListItemClick;
            }
  
            if (!allowSeach)
            {
                LinearLayout SearchBar = DialogInstance.FindViewById<LinearLayout>(Resource.Id.SearchBar);
                SearchBar.Visibility = ViewStates.Gone;
            }
            else
            {

                SearchText = DialogInstance.FindViewById<EditText>(Resource.Id.SeachText);
                ClearSearch = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearSearch);
                SearchButton = DialogInstance.FindViewById<ImageButton>(Resource.Id.SearchButton);
                EditTextListener = new EditTextEventListener(SearchText);
                EditTextListener.OnEnterEvent += OnEnterEvent;
//                EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
//                    {
//                        string data = e.Data;
//                        if (!e.IsBarcode)
//                        {
//                            RefreshData(data);
//                        }
//
//                    };
                ClearSearch.Click += OnClearSearch_Click;
                SearchButton.Click += OnSearchButton_Click;

            }
            RefreshData(string.Empty);

            if (allowSeach)
            {
                SearchText.RequestFocus ();
            }

//            try
//            {
//                SearchText.RequestFocus ();
//            }
//            catch(Exception ex)
//            {
//                string e;
//                e = ex.Message;
//            }
          

            return DialogInstance;
        }

        private void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            string data = e.Data;
            if (!e.IsBarcode)
            {
                RefreshData(data);
            }
            else
            {
                if (e.BarcodeData.BarcodeType == BarcodeTypes.NA)
                {
                    RefreshData(data);
                }
                else
                {
                    RefreshData(e.BarcodeData.Location);
                }
            }
        }

        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            SearchText.Text = string.Empty;
            RefreshData(string.Empty);
            SearchText.RequestFocus ();
        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {
            RefreshData(SearchText.Text);
        }

        void RefreshData(string search)
        {
            this.choiseList = new List<MCH.Communication.LocationItem>();
            foreach (var choise in originalLocationList)
            {   
                if (search == string.Empty)
                {
                    this.choiseList.Add(new MCH.Communication.LocationItem(choise.LocationId,choise.Location,choise.LocationBarcode,choise.LocationPrefix));
                }
                else
                {
                    if (choise.ToString().ToLower().Contains(search.ToLower()))
                    {
                        this.choiseList.Add(new MCH.Communication.LocationItem(choise.LocationId,choise.Location,choise.LocationBarcode,choise.LocationPrefix));
                    }
                }
            }

            listView.Adapter = new LocationListAdapter(this.context, choiseList);
   

            listView.RequestFocus();
            InputMethodManager manager = (InputMethodManager)context.GetSystemService(Context.InputMethodService);

            if (allowSeach)
            {
                manager.HideSoftInputFromWindow(SearchText.WindowToken, 0);
            }
 
        }

        void OnCloseClick(object sender, EventArgs e)
        {
            Dismiss();
        }
 
        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {

            List<MCH.Communication.LocationItem> result = new List<MCH.Communication.LocationItem>();
            result.Add(choiseList[e.Position]);
            if (locationClickAction != null)
            {
                locationClickAction.Invoke (result);
            }

            if (OkClicked != null)
            {
                OkClicked.Invoke (result);
            }
                

            Dismiss();

        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (title.ToUpper());
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }





    }
}


