﻿using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Graphics;

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Camera_GalleryActivity : BaseActivity
    {

  
        bool confirmSkip =false;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Camera_GalleryLayout);
 
            string extra = Intent.GetStringExtra("ConfirmSkip");
            if (!string.IsNullOrEmpty(extra))
            {
                confirmSkip = bool.Parse(extra);
            }


            Initialize(GetText( Resource.String.CargoSnapShot) + " - Gallery",Resource.Drawable.CameraWhite);
  
            gridview.ItemClick += DisplayImage;
            DisplayImages();
 


        }
 
  
        private void DoBack()
        {
            if (ImageGallery.Instance.Count > 0)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                            DeleteAllFiles();
                            StartActivity(ImageGallery.BackScreen);
                            this.Finish();
                    }
                };
                m.ShowConfirmationMessage(Resource.String.CaptureSealWithImageCancel);

            }
            else
            {
                StartActivity(ImageGallery.BackScreen);
                this.Finish();
            }
        }
 
        private void DisplayImages()
        {
            if (ImageGallery.Instance.Count > 0)
            {
                gridview.Adapter = new ThumbnailAdapter (this,ImageGallery.Instance.ImageItems );
            }
            else
            {
                var nextScreen = new Intent(this, typeof(Camera_Activity));
                nextScreen.PutExtra("ConfirmSkip", confirmSkip.ToString());
                StartActivity(nextScreen);
                this.Finish();
            }
        }

        private void DisplayImage(object sender, AdapterView.ItemClickEventArgs e)
        {
             
            if (ImageGallery.Instance.Count > 0)
            {
                var nextScreen = new Intent(this, typeof(Camera_FragmentActivity));
                nextScreen.PutExtra("ImageNumber", e.Position.ToString());
                nextScreen.PutExtra("Title", this.Title);
                StartActivity(nextScreen);
                this.Finish();
            }
 
        }

        private void OnSnapButton_Click(object sender, EventArgs e)
        {
//            StartActivity (typeof(Camera_Activity));
//            this.Finish();

            var nextScreen = new Intent(this, typeof(Camera_Activity));
            nextScreen.PutExtra("ConfirmSkip", confirmSkip.ToString());
            StartActivity(nextScreen);
            this.Finish();
        }
 
        private void DoNext()
        {
            if (ImageGallery.Instance.Count > 0)
            {
                DoUpload();
            }
            else
            {
                StartActivity(ImageGallery.NextScreen);
                this.Finish();
            }
        }
        private void OnNextButton_Click(object sender, EventArgs e)
        {
            DoNext();
 


        }

        public void AskForDeleteAll()
        {
            MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
//                        foreach (var image in ImageGallery.Instance.ImageItems)
//                        {
//                            DeleteFile(image.FileName);
//
//                        }
//                        ImageGallery.Instance.Clear();
                        DeleteAllFiles();
                        DisplayImages();
                    }
                };
            m.ShowConfirmationMessage(Resource.String.DeleteAll_Question);
        }



    }
}

