﻿using System;
using System.Collections.Generic;
 
using Android.Content;

namespace MCH
{
    public class ImageGallery
    {
        private static ImageGallery instance;
//        public static Intent BackScreen;
//        public static Intent NextScreen ;
        public static Type BackScreen;
        public static Type NextScreen ;
      
        public static long CurrentTaskId = 0 ;
        public static long GoodConditionId ;
        public static long ImageGalleryTaskId ;
        public static string ImageGalleryReference ;
         



        private ImageGallery() {}


        private List<ImageItem> imageItems;

        public static ImageGallery Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new ImageGallery();

                    instance.imageItems = new List<ImageItem>();

             }
             return instance;
          }
            
        }

        ~ImageGallery()  // destructor
        {
 
        }
 
        public List<ImageItem> ImageItems
        {
            get
            { 
                return instance.imageItems;
            }
        }

        public int Count
        {
            get
            { 
                return instance.imageItems.Count;
            }
        }

       
    

        public void Add(string fileName, int rotation,bool isLandscape)
        {
            imageItems.Add(new ImageItem(fileName,rotation,isLandscape));
        }

        public void Stack(string fileName, int rotation,bool isLandscape)
        {
             
            List<ImageItem> newImageList = new List<ImageItem>();
            newImageList.Add(new ImageItem(fileName,rotation,isLandscape));

            foreach(ImageItem i in   imageItems )
            {
                newImageList.Add(i);
            }
            imageItems = newImageList;

        }

        public void RemoveAt(int index)
        {
            imageItems.RemoveAt(index);
        }

        public void Clear()
        {
            imageItems.Clear();
        }

    }

    public class ImageItem
    {
        public bool Uploaded
        {
            get; set;
        }

        public string UploadError
        {
            get; set;
        }

        public string FileName
        {
            get; set;
        }
        public int Rotation
        {
            get; set;
        }

        public bool IsLandscape
        {
            get; set;
        }

        public List<ImageItemAnnotation> Annotations = new List<ImageItemAnnotation>();

        public ImageItem(string fileName, int rotation, bool isLandscape)
        {
            this.IsLandscape = isLandscape;
            this.FileName = fileName;
            this.Rotation = rotation;
        }


    }

    public class ImageItemAnnotation
    {

        public string Annotation
        {
            get; set;
        }

//        public int Left
//        {
//            get; set;
//        }
//
//        public int Top
//        {
//            get; set;
//        }
//
//        public int Right
//        {
//            get; set;
//        }
//
//        public int Buttom
//        {
//            get; set;
//        }
 
        public bool IsNew
        {
            get
            { 
                if (X == 0 && Y == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public long AnnotationId
        {
            get; set;
        }


        public float X
        {
            get; set;
        }

        public float Y
        {
            get; set;
        }

//        public ImageItemAnotation(string anotation, int left , int top,int right, int buttom)
//        {
//            this.Anotation = anotation;
//            this.Left = left;
//            this.Top = top;
//            this.Right = right;
//            this.Buttom = buttom;
//            this.IsNew = false;
//        }

        public ImageItemAnnotation(string annotation,long annotationId, float x , float y)
        {
            this.AnnotationId = annotationId;
            this.Annotation = annotation;
            this.X = x;
            this.Y = y;
        }

    }
}

