﻿ 

//using System;
//using System.Collections.Generic;
//using Android.App;
//using Android.Content;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Android.OS;




using System;
using System.Collections.Generic;
using Android.Graphics;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
 
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Graphics;

using Android.Support.V4.View;
using Android.Support.V4.App;
using MCH.Communication;

namespace MCH
{ //MainLauncher = true,
  
    //ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,

     [Activity( Theme = "@style/android:Theme.Holo.Light",  NoHistory = true,  ConfigurationChanges=Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    //[Activity( Theme = "@style/Theme.NoTitle",  NoHistory = true,  ConfigurationChanges=Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public partial class Camera_FragmentActivity : BaseFragmentActivity 
    {
 


        public bool fromCamera = false;
        private   int ImageNumber  = -1;
        //static
           
        protected override void OnCreate(Bundle bundle)
        {



////Test
//            MCH.Communication.CompanyInfo company = MCH.Communication.Membership.Instance.GetCompanyName(ApplicationSessionState.GetDeviceId(this));
//            ApplicationSessionState.CompanyName = company.CompanyName;
//            ApplicationSessionState.CompanySecuritySettings = MCH.Communication.Membership.Instance.GetCompanySecuritySettings(ApplicationSessionState.CompanyName);
//            ApplicationSessionState.User = MCH.Communication.Membership.Instance.AuthenticateUserPin(GetText(Resource.String.ApplicationCode), "111111");
//            ApplicationSessionState.LastUseTime = DateTime.UtcNow;
//            ApplicationSessionState.User.IsAthenticated = true;
//  
//            ImageGallery.Instance.Add("babce147-2517-4ba3-ba1d-6dcc59b23e6d", 90, false);
//            ImageGallery.Instance.Add("64882041-a6de-4dfd-9f99-cd0c3a756af8", 0, true);
//            ImageGallery.Instance.Add("68b4a9bb-d4a9-4419-83bd-881e589c568e", 90, false);
//            ImageGallery.Instance.Add("69e07300-f9e0-4fb0-aaa6-9c4ca416d543", 90, false);
//            ImageGallery.Instance.Add("243f1d69-b19c-46f9-aa39-cf650a88bcb6", 90, false);
//            ImageGallery.Instance.Add("531091bc-0768-4410-b5d8-b2e0fdea2cfb", 0, true);
//
////Test


            base.OnCreate(bundle);
 
            SetContentView(Resource.Layout.Camera_PreviewViewPagerLayout);
  
            
           
            string passedData = Intent.GetStringExtra("ImageNumber") ?? "";
            if (ImageNumber < 0)
            {
                if (!int.TryParse(passedData, out ImageNumber))
                {
                    ImageNumber = 0;
                }
            }

            if (Intent.GetStringExtra("FromCamera") != null)
            {
                fromCamera = bool.Parse(Intent.GetStringExtra("FromCamera"));
            }
          

            Initialize(GetText(Resource.String.CargoSnapShot), Resource.Drawable.CameraWhite);
  
            RefreshData();
            //string x = ActionBar.SelectedTab.Text;

        }

        public override void OnConfigurationChanged (Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged (newConfig);
            var nextScreen = new Intent(this, typeof(Camera_FragmentActivity));
            nextScreen.PutExtra("ImageNumber", ImageNumber.ToString());
                nextScreen.PutExtra("Title", this.Title);
                StartActivity(nextScreen);
                this.Finish();
     
        }

        private void RefreshData()
        {

            headerText.Text = GetText(Resource.String.CargoSnapShot) + string.Format(" ({0} of {1})", ImageNumber + 1, ImageGallery.Instance.Count)+ "\n#" + ImageGallery.ImageGalleryReference;
 
            ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
            ActionBar.Hide();
            var pager = FindViewById<ViewPager>(Resource.Id.pager);
            var adaptor = new GenericFragmentPagerAdaptor(SupportFragmentManager);
            for (int index = 0; index < ImageGallery.Instance.Count; index++)
            {
                adaptor.AddFragmentView(index, (position, i, v, b) =>
                    {
                        var view = i.Inflate(Resource.Layout.Camera_PreviewViewPagerFrameLayout, v, false);
 
                        var imageView = view.FindViewById<ImageView>(Resource.Id.ImagePreview);
 
                        File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);
                        File file = new File(dir, String.Format("{0}.jpg", ImageGallery.Instance.ImageItems[position].FileName));

                        if (file.Exists())
                        {
 
//                            if (ImageGallery.Instance.ImageItems[position].IsLandscape)
//                            {
//                                BitmapHelper.LoadImage(file.Path, imageView, 320, 240, ImageGallery.Instance.ImageItems[position].Rotation);
//                                imageView.SetScaleType(ImageView.ScaleType.CenterCrop);
//                            }
//                            else
//                            {
//                                BitmapHelper.LoadImage(file.Path, imageView, 320, 240, ImageGallery.Instance.ImageItems[position].Rotation);
//  
//                                if (IsLandscape())
//                                {
//                                    imageView.SetScaleType(ImageView.ScaleType.FitCenter);
//                                }
//                                else
//                                {
//                                    imageView.SetScaleType(ImageView.ScaleType.FitXy);
//                                }
//                               
//                            }


                            if (!IsLandscape())
                            {
                                if (!ImageGallery.Instance.ImageItems[position].IsLandscape)
                                {
                                    imageView.SetScaleType(ImageView.ScaleType.FitXy);
                                }
                                else
                                {
                                    imageView.SetScaleType(ImageView.ScaleType.FitCenter);
                                }
                            }
                            else
                            {
                                if (!ImageGallery.Instance.ImageItems[position].IsLandscape)
                                {
                                    imageView.SetScaleType(ImageView.ScaleType.FitCenter);
                                }
                                else
                                {
                                    imageView.SetScaleType(ImageView.ScaleType.FitXy);
                                }
                            }
                            BitmapHelper.LoadImage(file.Path, imageView, 320, 240, ImageGallery.Instance.ImageItems[position].Rotation);

                        }
 
                        return view;
                    }
                );
            }


            Action<int> pageSelectAction = OnPageSelectAction;

            pager.Adapter = adaptor;
            pager.SetOnPageChangeListener(new ViewPageListenerForActionBar(ActionBar, pageSelectAction));


            for (int index = 0; index < ImageGallery.Instance.Count; index++)
            {
                ActionBar.AddTab(pager.GetViewPageTab(ActionBar, index.ToString()));
            }
 
            ActionBar.SelectTab(ActionBar.GetTabAt(ImageNumber));
            ActionBar.Tab tab = ActionBar.SelectedTab;

//            if (ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Count > 0)
//            {
//                TagCounter.Text  = ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Count.ToString();
//            }
//            else
//            {
//                TagCounter.Text =string.Empty;
//            }
//           
            ShowAnnotationCounter();
        }

        private void ShowAnnotationCounter()
        {
            if (ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Count > 0)
            {
                TagCounter.Text  = ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Count.ToString();
            }
            else
            {
                TagCounter.Text =string.Empty;
            }
        }

        private void OnPageSelectAction(int  position)
        {
            //GetText(Resource.String.CargoSnapShot)

            headerText.Text =   GetText(Resource.String.CargoSnapShot) + string.Format(" {0} of {1}",position+1,ImageGallery.Instance.Count) + "\n#: " + ImageGallery.ImageGalleryReference;

//            if (ImageGallery.Instance.ImageItems[position].Annotations.Count > 0)
//            {
//                TagCounter.Text  = ImageGallery.Instance.ImageItems[position].Annotations.Count.ToString();
//            }
//            else
//            {
//                TagCounter.Text =string.Empty;
//            }
           
            ImageNumber =position;
            ShowAnnotationCounter();
        }

        private bool IsLandscape()
        {
            var surfaceOrientation = WindowManager.DefaultDisplay.Rotation;
            if (surfaceOrientation == SurfaceOrientation.Rotation0)
            {
                return false;
            }

            if (surfaceOrientation == SurfaceOrientation.Rotation180)
            {
                return false;
            }

            if (surfaceOrientation == SurfaceOrientation.Rotation90)
            {
                return true;
            }
            if (surfaceOrientation == SurfaceOrientation.Rotation270)
            {
                return true;
            }
   

            return false;

        }

        private void OnSnapButton_Click(object sender, EventArgs e)         {             StartActivity (typeof(Camera_Activity));             this.Finish();         }          private void DoBack()         {             ImageNumber = -1;             if (fromCamera)             {                 StartActivity (typeof(Camera_Activity));                 this.Finish();             }             else             {                 if (ImageGallery.Instance.Count > 0)                 {                     var nextScreen = new Intent(this, typeof(Camera_GalleryActivity));                     //nextScreen.PutExtra("Title", "Camera");                     StartActivity(nextScreen);                     this.Finish();                 }                 else                 {                     StartActivity (typeof(Camera_Activity));                     this.Finish();                 }             }          }


        private void OnDeleteButton_Click(object sender, EventArgs e)
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) => 
                {
                    if(result)
                    {

                        DeleteFile(ImageGallery.Instance.ImageItems[ImageNumber].FileName);

                        ImageGallery.Instance.RemoveAt(ImageNumber);

                        if(ImageGallery.Instance.Count == 0)
                        {
                            ImageNumber = -1;
                            DoBack();
                        }
                        else
                        {
                            ImageNumber = 0;
                            RefreshData();
                        }
                     
                    }
                };

            m.ShowConfirmationMessage(Resource.String.Confirm_Delete_Image);

        }

        private void DeleteFile(string fileName)
        {
            File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);
            File file = new File(dir, String.Format("{0}.jpg", fileName));

            if (file.Exists())
            {   
                file.Delete();
            }
 

        }


        private void OnConditionClickSelectionAction(List<MCH.Communication.SelectionItem>  result, bool isExtra)
        {
            if (isExtra)
            {
                for (int i = 0; i < ImageGallery.Instance.ImageItems.Count; i++)
                {
                    ImageGallery.Instance.ImageItems[i].Annotations.Clear();
                    foreach (var r in result)
                    {
         
                        int index = ImageGallery.Instance.ImageItems[i].Annotations.Count;
                        ImageItemAnnotation a = new ImageItemAnnotation(r.Name , r.Id, 0, 0);
                        ImageGallery.Instance.ImageItems[i].Annotations.Add(a);
                    
                    }
                }
            }
            else
            {
                ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Clear();
                foreach (var r in result)
                {
     
                    int index = ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Count;
                    ImageItemAnnotation a = new ImageItemAnnotation(r.Name , r.Id, 0, 0);
                    ImageGallery.Instance.ImageItems[ImageNumber].Annotations.Add(a);
                
                }
            }

            ShowAnnotationCounter();
        }

        private void OnTagButton_Click(object sender, EventArgs e)
        {

 

            List< MCH.Communication.SelectionItem> conditionList = new List< MCH.Communication.SelectionItem>();
            if (ApplicationSessionState.ConditionList.Data != null)
            {
                foreach (var condition in ApplicationSessionState.ConditionList.Data)
                {
                    if (condition.IsDamage)
                    {
                        bool isSelected = false;
                        foreach (var anotation in ImageGallery.Instance.ImageItems[ImageNumber].Annotations)
                        {
                            if (anotation.AnnotationId == condition.ConditionId)
                            {
                                isSelected = true;
                            }

                        }
                        conditionList.Add(new MCH.Communication.SelectionItem(condition.Condition, condition.ConditionId, @"Icons/Damage", isSelected));
                    }
           
                }

 
                Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnConditionClickSelectionAction;
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Conditions), this, conditionList, SelectionAction, SelectionListAdapter.SelectionMode.CheckBoxSeletion, false);
                dialogFragment.ExtraButtonText = GetText(Resource.String.Apply_All);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }
 
            
  
                      }

        private void OnTagButton_Click_old(object sender, EventArgs e)
        {

                        if (ImageGallery.Instance.ImageItems[ImageNumber].IsLandscape)
                        {
                       
                            var nextScreen = new Intent(this, typeof(Camera_AnnotationLandscapeActivity));
                        nextScreen.PutExtra("ImageNumber", ImageNumber.ToString());
                        StartActivity(nextScreen);
                        this.Finish();
                        }
                        else
                        {
                             
                            var nextScreen = new Intent(this, typeof(Camera_AnnotationPortraitActivity));
                        nextScreen.PutExtra("ImageNumber", ImageNumber.ToString());
                        StartActivity(nextScreen);
                        this.Finish();
                        }
        } 
        private void DoNext()
        {
            if (ImageGallery.Instance.Count > 0)
            {
                DoUpload();
            }
            else
            {
                StartActivity(ImageGallery.NextScreen);
                this.Finish();
            }
        }



    }
}


