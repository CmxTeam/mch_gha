﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
 
using Android.Support.V4.View;
using Android.Support.V4.App;

using System.ComponentModel;
using System.Threading;

using Camera = Android.Hardware.Camera;
using Environment = Android.OS.Environment;

using MCH.Graphics;
using Java.IO;

namespace MCH
{
    [Activity()]            
    public class BaseFragmentActivity : FragmentActivity
    {

        private bool IsLoginDialogOpen = false; 
//        public string Title;
//        public string Icon;
        BackgroundWorker bgWorker;

        protected override void OnCreate(Bundle bundle)
        {
  

//            Title = Intent.GetStringExtra ("Title") ?? ""; 
//            Icon = Intent.GetStringExtra ("Icon") ?? ""; 

                base.OnCreate(bundle);

            Window.SetSoftInputMode(SoftInput.StateAlwaysHidden);

            if (ApplicationSessionState.CompanySecuritySettings.Data.IsSingleSession)
            {
                this.bgWorker = new BackgroundWorker();
                this.bgWorker.WorkerSupportsCancellation = true;
                this.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                StartWorker();
            }

        }
 
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            StopWorker();
        }

        protected override void OnPause ()
        {
            base.OnPause ();

            ApplicationSessionState.LastUseTime = DateTime.UtcNow;

        }

        public int Duration()
        {
            TimeSpan ts =  DateTime.UtcNow - ApplicationSessionState.LastUseTime;
            int duration = ts.Minutes;
 
            return duration;
        }

        protected override void OnResume()
        {
            base.OnResume();
            int duration = Duration();
 

            if (!ApplicationSessionState.User.Data.IsAthenticated || (ApplicationSessionState.CompanySecuritySettings.Data.SessionTimeout > 0 && duration > ApplicationSessionState.CompanySecuritySettings.Data.SessionTimeout)) 
            {

                DoTimeOut();

            }
            else
            {
                if (!CheckUserSession())
                {
                    DoLogOut();
                }
            }
         
    


        }

        public void GoToScreen(Type screen)
        {
            StartActivity (screen);
            this.Finish();
        }

        public void GotoMainMenu()
        {
            StartActivity (typeof(MainMenuActivity));
            this.Finish();
        }

        private void OnPinClickAction(MCH.Communication.User  user)
        {
            IsLoginDialogOpen = false;
            if (user!=null)
            {
                if (user.Data.UserId == ApplicationSessionState.User.Data.UserId)
                {
                    ApplicationSessionState.User.Data.IsAthenticated =true;
                    ApplicationSessionState.LastUseTime = DateTime.UtcNow;
                }
                else
                {
                    ApplicationSessionState.User = user;

                    ApplicationSessionState.UserWarehouses =   MCH.Communication.Menu.Instance.GetUserWarehouses(ApplicationSessionState.User.Data.UserId,ApplicationSessionState.User.Data.ShellSetting.WarehouseIds);
                    if (ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId != null)
                    {
                        ApplicationSessionState.SelectedWarehouseId=long.Parse(ApplicationSessionState.User.Data.ShellSetting.DefaultWarehouseId.ToString());
                    }
                    else
                    {
                        ApplicationSessionState.SelectedWarehouseId =0;
                    }

                    GotoMainMenu();
                }

            }
            else
            {
                DoLogOut();
            }
        }



        public void DoLogOut()
        {
            ApplicationSessionState.User.Data.IsAthenticated =false;
            if (ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.Pin || ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.PinAndPassword)
            {
                var intent = new Intent (this, typeof (PinActivity));
                intent.SetFlags (ActivityFlags.ClearTop);
                StartActivity(intent);
            }
            else
            {
                var intent = new Intent (this, typeof (LoginActivity));
                intent.SetFlags (ActivityFlags.ClearTop);
                StartActivity(intent);
            }
            this.Finish();
        }

        private void DoTimeOut()
        {
            ApplicationSessionState.User.Data.IsAthenticated =false;
            if (ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.Pin || ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.PinAndPassword)
            {
                if (!IsLoginDialogOpen)
                {
                    IsLoginDialogOpen = true;
                    Action<MCH.Communication.User> PinClickAction = OnPinClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new PinDialogActivity(this,false, PinClickAction,Resource.String.Session_Time_Out,Resource.String.Session_Time_Out_Pin_Message);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                }
 
            }
            else
            {
                DoLogOut();
            }

        }


        public void DoSupervisorOverride(Action<MCH.Communication.User>  supervisorOverrideAction)
        {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new PinDialogActivity(this,true, supervisorOverrideAction,Resource.String.Supervisor_Override,Resource.String.Supervisor_Override_Message,Resource.String.Supervisor_Override_Accept,Resource.String.Supervisor_Override_Reject);
                dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

 
        public void ShowAlertMessage(int message)
        {
            MessageBox m = new MessageBox(this);
            m.ShowMessage(message);
        }

        public void ShowAlertMessage(string message)
        {
            MessageBox m = new MessageBox(this);
            m.ShowMessage(message);
        }


        public void ConfirmLogOut()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) => 
                {
                    if(result)
                    {
                        DoLogOut();
                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Logout);

 
        }


        public void ConfirmExit()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) => 
                {
                    if(result)
                    {
                        this.Finish();
                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Exit_Application);


 
        }


        private int ConvertPixelsToDp(float pixelValue)
        {
            var dp = (int) ((pixelValue)/Resources.DisplayMetrics.Density);
            return dp;
        }


        public int GetScreenWidth()
        {
            var metrics = Resources.DisplayMetrics;
            var widthInDp = ConvertPixelsToDp(metrics.WidthPixels);
            return widthInDp;
        }

        public int GetScreenHeight()
        {
            var metrics = Resources.DisplayMetrics;
            var heightInDp = ConvertPixelsToDp(metrics.HeightPixels);
            return heightInDp;
        }


        private void StartWorker()
        {
            if (ApplicationSessionState.CompanySecuritySettings.Data.IsSingleSession)
            {
                if (this.bgWorker != null && !this.bgWorker.IsBusy)
                    this.bgWorker.RunWorkerAsync();
            }

        }

        private void StopWorker()
        {
            if (ApplicationSessionState.CompanySecuritySettings.Data.IsSingleSession)
            {
                if (this.bgWorker != null && this.bgWorker.IsBusy)
                    this.bgWorker.CancelAsync();
            }
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (this.bgWorker.CancellationPending) {

                    //RunOnUiThread (() => labelDisplay.Text = "CancellationPending");
                    break;
                }
                else
                {


                    if (!CheckUserSession())
                    {
                        DoLogOut();
                        break;
                    }

                    //RunOnUiThread(() => labelDisplay.Text = counter.ToString());
                }

                Thread.Sleep (ApplicationSessionState.CheckSessionInterval);

            }
        }


        private bool CheckUserSession()
        {
  
            bool result = false;

            MCH.Communication.UserSessionParameters parameters = new MCH.Communication.UserSessionParameters();
            parameters.Location = new MCH.Communication.GeoLocation();
            parameters.Location.Longitude = ApplicationSessionState.Longitude;
            parameters.Location.Latitude = ApplicationSessionState.Latitude;
            parameters.AppName = ApplicationSessionState.SelectedApplication.Name;
            parameters.DeviceId = ApplicationSessionState.GetMac(this);
            parameters.SessionId = ApplicationSessionState.User.Data.SessionId;
            parameters.UserId = ApplicationSessionState.User.Data.UserId;

            MCH.Communication.UserSession userSession =  MCH.Communication.Membership.Instance.CheckUserSession(parameters);
            if (userSession.Transaction.Status)
            {
                result = userSession.Data.IsValid;
            }
            else
            {
                result = false;
            }

            //            if (!result)
            //            {
            //                MessageBox m = new MessageBox(this);
            //                m.OnConfirmationClick += (bool e) => 
            //                    {
            //                        DoLogOut();
            //                    };
            //                m.ShowAlertMessage("Session has expired");
            //            }
            //
            return result;
        }



        public void DoUpload()
        {
            //ImageGallery.Instance


            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Uploading_Images), true);
            new Thread(new ThreadStart(delegate
                {

                    //Thread.Sleep(1000);
    

                    foreach(var i in ImageGallery.Instance.ImageItems)
                    {
                        if(i.Uploaded)
                        {
                            continue;
                        }

                        MCH.Communication.SnapShotImageItem image = new MCH.Communication.SnapShotImageItem();
                        image.SnapShotImageConditionItems = new List<MCH.Communication.SnapShotImageConditionItem>();


                        if(i.Annotations.Count > 0)
                        {
                            foreach (var a in i.Annotations)
                        {
                            MCH.Communication.SnapShotImageConditionItem c = new MCH.Communication.SnapShotImageConditionItem();
                            c.ConditionId = a.AnnotationId;
                            c.X = a.X;
                            c.Y = a.Y;
                            image.SnapShotImageConditionItems.Add(c);
                        }
                        }
                        else
                        {
                            MCH.Communication.SnapShotImageConditionItem c = new MCH.Communication.SnapShotImageConditionItem();
                            c.ConditionId =  ImageGallery.GoodConditionId;
                            c.X = 0;
                            c.Y = 0;
                            image.SnapShotImageConditionItems.Add(c);
                        }

            
                        image.Latitude = ApplicationSessionState.Latitude;
                        image.Longitude = ApplicationSessionState.Longitude;

                        //image.Rotation = i.Rotation;
                        image.SnapshotTaskId =  ImageGallery.ImageGalleryTaskId;
                        image.UserId = ApplicationSessionState.User.Data.UserId;
 

                      

                        File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);
                        File file = new File(dir, String.Format("{0}.jpg", i.FileName));

                        //File file = new File(dir, String.Format("{0}.png", "Damaged"));


                        if (file.Exists())
                        {
                            byte[] binaryData = BitmapHelper.GetBytesFromImage(file.AbsolutePath);


                            if(i.Rotation>0)
                            {
                                try
                                {
                                    binaryData = BitmapHelper.RotateImage(binaryData,i.Rotation);
                                }
                                catch(Exception ex)
                                {
                                    MessageBox m = new MessageBox(this);
                                    m.ShowMessage(ex.Message + ". " + GetText(Resource.String.Camera_Resolution_Error));
                                    RunOnUiThread(() => progressDialog.Hide());
                                    return;
                                }
                               
                            }




                            string base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);
                            binaryData = null;
                            image.SnapshotImage = "data:image/jpg;base64," + base64String;
                            base64String = null;
                            image.PieceCount = 1;
                            MCH.Communication.CommunicationTransaction t =  MCH.Communication.CargoSnapShot.Instance.UploadSnapShotImage(image);
                            if(t.Status)
                            {
                                i.Uploaded = true;
                            }
                            else
                            {
                                i.UploadError = t.Error;
                            }

                             
                        }
                         
                      

                    }



                    foreach(var i in ImageGallery.Instance.ImageItems)
                    {
                        if(!i.Uploaded)
                        {
                             MessageBox m = new MessageBox(this);
                            m.ShowMessage(i.UploadError);
                             RunOnUiThread(() => progressDialog.Hide());
                             return;
                        }
                    }



                    RunOnUiThread (delegate {
                        DeleteAllFiles();
                     });
 


                    


                    StartActivity(ImageGallery.NextScreen);
                    this.Finish();

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }


        
        public void DeleteAllFiles()
        {
            foreach (var image in ImageGallery.Instance.ImageItems)
                        {
                            DeleteFile(image.FileName);

                        }
                        ImageGallery.Instance.Clear();
        }

        public void DeleteFile(string fileName)
        {
            File dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), ApplicationSessionState.ImageFolder);
            File file = new File(dir, String.Format("{0}.jpg", fileName));

            if (file.Exists())
            {   
                file.Delete();
            }
  
        }


        private void ShowCameraResolutions()
        {
            int resolution = ApplicationSessionState.GetInt(this, "Resolution");
            Camera camera;
            camera = Android.Hardware.Camera.Open();
            var p = camera.GetParameters();
            IList<Camera.Size> sizes = p.SupportedPictureSizes;

            List< MCH.Communication.SelectionItem> resolutionList = new List< MCH.Communication.SelectionItem>();
            for (int i = 0; i < sizes.Count; i++)
            {   
                bool isDefault = false;
                if (resolution == i)
                {
                    isDefault = true;
                }
                resolutionList.Add(new MCH.Communication.SelectionItem(string.Format("{0}x{1}",sizes[i].Width,sizes[i].Height),i,Resource.Drawable.Galery,isDefault));
            
            }
            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnResolutionClickSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Camera_Resolution), this, resolutionList, SelectionAction, SelectionListAdapter.SelectionMode.RadioButtonSelection, false);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            camera.Release();

        }
        private void OnResolutionClickSelectionAction(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            
            foreach (var r in result)
            {
                ApplicationSessionState.SaveString(this,"Resolution",r.Id.ToString());
            }
    
        }


    }
}

