﻿using System;
//using System.IO;
using System.Collections.Generic;
using System.Collections;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Net;
using Android.Net.Wifi;
 
using Java.IO;
 
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
 

namespace MCH
{
    public class ApplicationSessionState
    {
        private static ApplicationSessionState instance;
 

        private ApplicationSessionState() {}

        public static ApplicationSessionState Instance
        {
          get 
          {
             if (instance == null)
             {
                    instance = new ApplicationSessionState();
             }
             return instance;
          }
            
        }

        ~ApplicationSessionState()  // destructor
        {
 
        }
 
        private static long ? currentCategoryId = null;
        public static long ? CurrentCategoryId
        {
            get 
            { 
                return currentCategoryId;
            }
            set
            { 
                currentCategoryId = value;
            }
        }

        private static int checkSessionInterval;
        public static int CheckSessionInterval
        {
            get 
            {
                if (checkSessionInterval == 0)
                {
                    try
                    {
                        checkSessionInterval = int.Parse(Application.Context.GetText(Resource.String.CheckSessionInterval)) * (1000 * 60);
                    }
                    catch
                    {
                        checkSessionInterval = 5 * (1000 * 60);
                    }
                }
                return checkSessionInterval;
            }

        }
 

        public static DateTime LastUseTime;
        public static MCH.Communication.CompanySecuritySettings CompanySecuritySettings {get;set;}
        public static MCH.Communication.User User {get;set;}
  
        public static MCH.Communication.MobileApplication SelectedApplication {get;set;}
        public static MCH.Communication.MobileApplications ApplicationList {get;set;}
		//public static string CompanyName;

        public static MCH.Communication.DeviceInfo DeviceInfo;

		public static MCH.Communication.UserWarehouses UserWarehouses;
        public static long SelectedWarehouseId;
        public static string SelectedWarehouse;
        public static MCH.Communication.MenuItem SelectedMenuItem;
        public static MCH.Communication.ConditionTypes ConditionList;


		private static string deviceId = string.Empty;

        public static string GetDeviceName()
        {
            //string name =  Android.OS.Build.Display;
            //Android.OS.Build.Device
            return Android.OS.Build.Manufacturer + " " + Android.OS.Build.Model;
        }

        public static string GetDeviceSerial()
        {
            return Android.OS.Build.Serial;
        }


        public static string GetDeviceOS()
        {
            return Android.OS.Build.VERSION.Release;
        }

        public static string GetDeviceId (Activity actvity)
		{
   

			if (deviceId == string.Empty) {
				return Android.Provider.Settings.Secure.GetString(actvity.ContentResolver,Android.Provider.Settings.Secure.AndroidId);
			} else {
				return deviceId;
			}
        }

        public static string GetVersion(Activity actvity)
        {
            Context context = actvity.ApplicationContext;
            return  context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
        }
   
		public static string GetAllRightsReserved ()
		{
            return string.Format("{0} 1999-{1}",Application.Context.GetText(Resource.String.All_Rights_Reserved),   DateTime.Now.Year);
		}
 

        public static void DisplayErrorMessage(Activity actvity, string message)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(actvity);
                AlertDialog dialog = builder.Create();
                dialog.SetIcon (Resource.Drawable.Icon);
                dialog.SetTitle(Application.Context.GetText(Resource.String.app_name));
                dialog.SetMessage(message);
                dialog.SetCancelable(false);
                dialog.SetButton (Application.Context.GetText(Resource.String.Ok),delegate(object o, DialogClickEventArgs e) {
                    //
                });
                dialog.Show();
        }


        public static string ImageFolder
        {
            get
            {
                return Application.Context.GetText(Resource.String.ImageFolder);
            }
        }

        public static string ApplicationName
        {
            get
            {
                return Application.Context.GetText(Resource.String.app_name);
            }
        }


        public static int GetInt(Activity actvity, string key)
        {
            try
            {
                return int.Parse(GetString(actvity,key));
            }
            catch
            {
                return 0;
            }
        }

        public static bool GetBool(Activity actvity, string key)
        {
            try
            {
                string val = GetString(actvity,key);
                bool result =bool.Parse(val);
                return result;
            }
            catch
            {
                return false;
            }
        }

         //GPS
        public static double Latitude {get;set;}
        public static double Longitude {get;set;}
        public static double Altitude {get;set;}
        public static double Accuracy {get;set;}
        public static double Bearing {get;set;}
        public static double Speed {get;set;}
        public static string Provider {get;set;}


        public static string GetString(Activity actvity, string key)
        {
            var prefs = actvity.GetSharedPreferences(actvity.PackageName, FileCreationMode.Private);
            return prefs.GetString(key, string.Empty);
        }



        public static void SaveString(Activity actvity, string key, string value)
        {

            var prefs = actvity.GetSharedPreferences(actvity.PackageName, FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString(key, value);
            prefEditor.Commit();
        }

        public static string GetIp(Activity actvity)
        {
            try
            {
                WifiManager wifiMan = (WifiManager) actvity.GetSystemService(Context.WifiService);
                int ip =  wifiMan.ConnectionInfo.IpAddress;
                string addr = System.Net.IPAddress.Parse(ip.ToString()).ToString();
                return addr;
            }
            catch
            {
                return "127.0.0.1";
            }

        }
        public static string GetMac(Activity actvity)
        {

            try
            {
                WifiManager wifiMan = (WifiManager) actvity.GetSystemService(Context.WifiService);
                String macAddr =  wifiMan.ConnectionInfo.MacAddress;
                 
                return macAddr;
            }
            catch
            {
                return "00:00:00:00:00:00";
            }

        }



        public static int MaxImageCount(Activity actvity)
        {
            return GetInt(actvity,"MaxImageCount");
        }

        public static void UpdateMaxImageCount(Activity actvity, int maxImageCount)
        {
            SaveString(actvity ,"MaxImageCount",maxImageCount.ToString());
        }


        public static bool LogEnable(Activity actvity)
        {
            return GetBool(actvity,"LogEnable");
        }

        public static void UpdateLogEnable(Activity actvity, bool logEnable)
        {
            SaveString(actvity ,"LogEnable",logEnable.ToString());
            MCH.Communication.WebService.Instance.LogEnable = logEnable;
        }

      
 

        public static void InitializeApplication(Activity actvity)
        {  

            if(GetString(actvity,"MaxImageCount") == string.Empty)
            {
                SaveString(actvity ,"MaxImageCount",Application.Context.GetText(Resource.String.MaxImageCount));
            }

            if(GetString(actvity,"LogFolder") == string.Empty)
            {
                SaveString(actvity ,"LogFolder",ApplicationSessionState.ImageFolder);
                SaveString(actvity ,"LogEnable",false.ToString());
            }

            MCH.Communication.WebService.Instance.LogFolder = GetString(actvity,"LogFolder"); 
            MCH.Communication.WebService.Instance.LogEnable = GetBool(actvity,"LogEnable");

 
                SaveString(actvity ,"PROD_WebServiceUrl",Application.Context.GetText(Resource.String.PROD_WebServiceUrl));
   
                SaveString(actvity ,"DEV_WebServiceUrl",Application.Context.GetText(Resource.String.DEV_WebServiceUrl));
          
                SaveString(actvity ,"UAT_WebServiceUrl",Application.Context.GetText(Resource.String.UAT_WebServiceUrl));
     
                SaveString(actvity ,"QA_WebServiceUrl",Application.Context.GetText(Resource.String.QA_WebServiceUrl));
         

            if (GetString(actvity, "ConnectionMode") == string.Empty)
            {
 
                SaveString(actvity,"ConnectionMode",Application.Context.GetText(Resource.String.ConnectionMode));
              
            }

            string mode = GetString(actvity, "ConnectionMode");
            MCH.Communication.WebService.Instance.ShellPath = GetString(actvity,mode + "_WebServiceUrl");


 
 

        }

        public static bool IsTest(Activity actvity)
        {
            if (Mode(actvity) == ConnectionType.PROD)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public static ConnectionType  Mode(Activity actvity)
        {
       
            string mode = GetString(actvity, "ConnectionMode");
 
   

            if (mode == ConnectionType.PROD.ToString())
            {
                return ConnectionType.PROD;
            }
            else if (mode == ConnectionType.UAT.ToString())
            {
                return ConnectionType.UAT;
            }
            else if (mode == ConnectionType.QA.ToString())
            {
                return ConnectionType.QA;
            }
            else
            {
                return ConnectionType.DEV;
            }

        }

         
        public static void UpdateConnection(Activity actvity, ConnectionType mode)
        {
            SaveString(actvity ,"ConnectionMode",mode.ToString());
        }


        public enum ConnectionType : int
        {
            PROD = 0,
            QA = 1,
            UAT = 2,
            DEV = 3,
        }
    }
}

