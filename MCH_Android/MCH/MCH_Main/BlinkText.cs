﻿ 
using System;

using Android.Widget;
using Android.Views;
using Android.App;
namespace MCH
{
 

    public class BlinkText
    {
        private Activity context;
        private System.Timers.Timer t;
        private TextView editText;
        public BlinkText(Activity context, TextView editText)
        {
            this.editText = editText;
            this.context = context;
        }


        ~BlinkText()  // destructor
        {
            Stop();
            t = null;
        }

        public void Start()
        {
            if (t == null)
            {
                t = new System.Timers.Timer();
                t.Interval = 1000;
                t.Elapsed += new System.Timers.ElapsedEventHandler(t_Elapsed);
                 
            }
            t.Start(); 
        }

        public void Stop()
        {
            if (t != null)
            {
                editText.Visibility = ViewStates.Visible ; 
                t.Interval = 1000;
                t.Stop();  
            }

        }
        protected void t_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
 
            context.RunOnUiThread(delegate
                {
                    if(editText.Visibility == ViewStates.Invisible)
                    {
                        editText.Visibility = ViewStates.Visible ; 
                        t.Interval = 1000;
                    }
                    else
                    {
                        editText.Visibility = ViewStates.Invisible ; 
                        t.Interval = 250;
                    }

                });

 
        }
    }
 

}















 
