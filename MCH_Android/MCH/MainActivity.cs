﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using MCH.Communication;
using Android.Locations;



 

namespace MCH
{
    //MainLauncher = true,
    [Activity( Theme = "@style/Theme.Splash",     NoHistory = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity , ILocationListener
    {
        LocationManager locMgr;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            ApplicationSessionState.InitializeApplication(this);

     
            //Test
//                    StartActivity(typeof(test));
//                       return;
            //Test

          
            //MCH_Inventory.



            SetContentView (Resource.Layout.Splash);


            var tv = FindViewById<TextView> (Resource.Id.systemUiFlagTextView);
            tv.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;

            ThreadPool.QueueUserWorkItem (o => LoadActivity ());

            this.Window.AddFlags (Android.Views.WindowManagerFlags.Fullscreen);

        }


        protected override void OnResume()
        {
            base.OnResume();




            locMgr = GetSystemService(Context.LocationService) as LocationManager;
            if (locMgr.AllProviders.Contains(LocationManager.NetworkProvider)
                && locMgr.IsProviderEnabled(LocationManager.NetworkProvider))
            {
                locMgr.RequestLocationUpdates(LocationManager.NetworkProvider, 30000, 1, this);
            }

        }

        public void OnLocationChanged(Android.Locations.Location location)
        {
            try
            {
                ApplicationSessionState.Latitude = location.Latitude;
                ApplicationSessionState.Longitude = location.Longitude;
            }
            catch
            {
            }
        }

        public void OnProviderDisabled (string provider)
        {

        }
        public void OnProviderEnabled (string provider)
        {

        }
        public void OnStatusChanged (string provider, Availability status, Bundle extras)
        {

        }


        private void LoadActivity()
        {

            string device = ApplicationSessionState.GetMac(this);
            ApplicationSessionState.DeviceInfo = MCH.Communication.Membership.Instance.GetDeviceInfo(device);

            if (!ApplicationSessionState.DeviceInfo.Transaction.Status)
            {



                if (ApplicationSessionState.DeviceInfo.Transaction.Error.ToLower() == "Invalid Device".ToLower())
                {
                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            if(result)
                            {
                                SelectCompany();
                                return;
                            }
                            else
                            {
                                this.Finish();
                                return;
                            }
                        };
                    m.ShowConfirmationMessage(GetText(Resource.String.Device_Not_Registered) + ". MAC: " + device + ". " + GetText(Resource.String.Device_Registration_Question));
                    return;  
                }
                else
                {
                    RunOnUiThread(delegate
                        {
                            MessageBox m = new MessageBox(this);
                            m.OnConfirmationClick += (bool result) => 
                                {
                                    this.Finish();
                                    return;
                                };
                            m.ShowMessage(ApplicationSessionState.DeviceInfo.Transaction.Error + ". MAC: " + device);
                        });
                    return;
                }


            }
            else
            {
                if (ApplicationSessionState.DeviceInfo.Data == null)
                {



                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) => 
                        {
                            if(result)
                            {
                                SelectCompany();
                                return;
                            }
                            else
                            {
                                this.Finish();
                                return;
                            }
                        };
                    m.ShowConfirmationMessage(GetText(Resource.String.Device_Not_Registered) + ". MAC: " + device + ". " + GetText(Resource.String.Device_Registration_Question));
                    return;

                }
            }


            GoToLoginScreen(ApplicationSessionState.DeviceInfo.Data.CompanyName);

        }

        private void OnCompanySelectionAction(List<MCH.Communication.SelectionItem>  result, bool isExtra)
        {
            if (result == null || result.Count == 0)
            {
                this.Finish();
                return;              
            }
            else
            {
                ApplicationSessionState.DeviceInfo = new DeviceInfo();
                ApplicationSessionState.DeviceInfo.Data = new DeviceInfoData();
                ApplicationSessionState.DeviceInfo.Data.CompanyId = (int)result[0].Id;
                ApplicationSessionState.DeviceInfo.Data.CompanyName = result[0].Name;
                ApplicationSessionState.DeviceInfo.Data.Id = 0;
                ApplicationSessionState.DeviceInfo.Data.DeviceName = ApplicationSessionState.GetDeviceName();
                ApplicationSessionState.DeviceInfo.Data.MAC= ApplicationSessionState.GetMac(this);
                GoToLoginScreen(ApplicationSessionState.DeviceInfo.Data.CompanyName);

            }
        }

        private void SelectCompany()
        {
            List<SelectionItem> companyList = new List<SelectionItem>();
            Companies companies = Membership.Instance.GetCompanies();
            if (companies.Transaction.Status)
            {


                foreach (var company in companies.Data)
                {
                    companyList.Add(new SelectionItem(company.Name, company.Id, @"Icons/Home"));
                }

                Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnCompanySelectionAction;
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity(GetText(Resource.String.Companies), this, companyList, SelectionAction, SelectionListAdapter.SelectionMode.SingleSelection, false);
                dialogFragment.CancelButtonText = GetText(Resource.String.Cancel);
                dialogFragment.Cancelable = false;
                dialogFragment.AllowNoSelection = true;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick+= (bool result) => 
                    {
                        this.Finish();
                        return; 
                    };
                m.ShowMessage(companies.Transaction.Error);
            }

        }

        private async void GoToLoginScreen(string companyName)
        {
            ApplicationSessionState.CompanySecuritySettings = Membership.Instance.GetCompanySecuritySettings(companyName);

            if (ApplicationSessionState.CompanySecuritySettings.Transaction.Status)
            {

                // Simulate a long pause
                //bool result = await WaitAsynchronouslyAsync ();
                bool result = await WaitSynchronously ();

                if (ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MobileAuthMethods.Pin || ApplicationSessionState.CompanySecuritySettings.Data.MobileAuthMethod == MCH.Communication.MobileAuthMethods.PinAndPassword)
                {
                    RunOnUiThread (() => StartActivity (typeof(PinActivity)));

                    this.Finish();

                }
                else
                {
                    RunOnUiThread (() => StartActivity (typeof(LoginActivity)));

                    this.Finish();
                }

            }
            else
            {
                RunOnUiThread (delegate 
                    {

                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) => 
                            {
                                this.Finish();
                                return;
                            };
                        m.ShowMessage(ApplicationSessionState.CompanySecuritySettings.Transaction.Error);
                    });

            } 
        }

        public async Task<bool> WaitAsynchronouslyAsync()
        {
            await Task.Delay(1000);
            return true;
        }

        public async Task<bool> WaitSynchronously()
        {
            // Add a using directive for System.Threading.
            Thread.Sleep(1000);
            return true;
        }

    }


}


 