﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class QAS_WeightDialog: DialogFragment
    {
        TextView textViewMessage;
        Button cancel;
        Button ok;
        EditText txtGrossWeight;
        EditText txtTareWeight;
        EditText txtWeight;
      
        Activity context;
        string title;
        string message;
        ImageButton ClearGrossButton;
        ImageButton ClearTareButton;
        ImageButton ClearDollyButton;

       

        public delegate void OkClickActionEventHandler(bool  value);
        public event OkClickActionEventHandler OkClicked;

        public QAS_WeightDialog (Activity context,string title ,string message)
        {
            
            this.title = title.ToUpper();
            this.message = message.ToUpper();
            this.context = context;
                  
        }

 

        //ADD this for error when rotating
        public QAS_WeightDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.QAS_WeightDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            ok = DialogInstance.FindViewById<Button>(Resource.Id.ok); 

            txtGrossWeight = DialogInstance.FindViewById<EditText>(Resource.Id.txtGrossWeight);
            txtTareWeight = DialogInstance.FindViewById<EditText>(Resource.Id.txtTareWeight);
            //Dolly weight
            txtWeight = DialogInstance.FindViewById<EditText>(Resource.Id.txtWeight);

            textViewMessage = DialogInstance.FindViewById<TextView>(Resource.Id.message);
            textViewMessage.Text = this.message;

            LoadWeight();


            cancel =DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            cancel.Click += OnCancelClick;


            ok.Click += OnOk_Click;

            ClearGrossButton = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearGrossButton);
            ClearGrossButton.Click += OnClearGrossButton_Click;

            ClearTareButton = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearTareButton);
            ClearTareButton.Click += OnClearTareButton_Click;

            ClearDollyButton = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearDollyButton);
            ClearDollyButton.Click += OnClearDollyButton_Click;



            return DialogInstance;
        }

        private void OnClearGrossButton_Click(object sender, EventArgs e)
        {
            txtGrossWeight.Text = string.Empty;
            txtGrossWeight.RequestFocus ();
        }

        private void OnClearTareButton_Click(object sender, EventArgs e)
        {
            txtTareWeight.Text = string.Empty;
            txtTareWeight.RequestFocus ();
        }

        private void OnClearDollyButton_Click(object sender, EventArgs e)
        {
            txtWeight.Text = string.Empty;
            txtWeight.RequestFocus ();
        }

        private void LoadWeight()
        {
            //clear session
            QAS_PrepareCart_SessionState.GrossWeight = 0;
            QAS_PrepareCart_SessionState.TareWeight = 0;
            QAS_PrepareCart_SessionState.DollyWeight = 0;

            if (QAS_PrepareCart_SessionState.QASPrepareCartTask.GrossWeight != null && QAS_PrepareCart_SessionState.QASPrepareCartTask.GrossWeight >= 0)
            {
                txtGrossWeight.Text = string.Format("{0:0.00}", QAS_PrepareCart_SessionState.QASPrepareCartTask.GrossWeight);
            }
            else
            {
                txtGrossWeight.Text = string.Empty;
            } 

            if (QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight != null && QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight >= 0)
            {
                txtTareWeight.Text = string.Format("{0:0.00}", QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight);
            }
            else
            {
                txtTareWeight.Text = string.Empty;
            } 

            if (QAS_PrepareCart_SessionState.QASPrepareCartTask.DollyWeight != null && QAS_PrepareCart_SessionState.QASPrepareCartTask.DollyWeight >= 0)
            {
                txtWeight.Text = string.Format("{0:0.00}", QAS_PrepareCart_SessionState.QASPrepareCartTask.DollyWeight);
            }
            else
            {
                txtWeight.Text = string.Empty;
            } 
        }


        private void OnOk_Click(object sender, EventArgs e)
        {


            if(txtGrossWeight.Text == string.Empty || Double.Parse(txtGrossWeight.Text)< 0.1)
            {
                MessageBox msg = new MessageBox(Activity);
                msg.ShowAlert(GetText(Resource.String.Gross_Weight_Not_Valid), MessageBox.AlertType.Information);
                txtGrossWeight.RequestFocus();
                return;
            }
            if(txtTareWeight.Text == string.Empty || Double.Parse(txtTareWeight.Text)< 0.1)
            {
                MessageBox msg = new MessageBox(Activity);
                msg.ShowAlert(GetText(Resource.String.Tare_Weight_Not_Valid), MessageBox.AlertType.Information);
                txtTareWeight.RequestFocus();
                return;
            }
            if(txtWeight.Text == string.Empty)
            {
                txtWeight.Text = "0";
            }

            QAS_PrepareCart_SessionState.QASPrepareCartTask.GrossWeight = Double.Parse(txtGrossWeight.Text);
            QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight = Double.Parse(txtTareWeight.Text);
            QAS_PrepareCart_SessionState.QASPrepareCartTask.DollyWeight = Double.Parse(txtWeight.Text);


            if(OkClicked!=null)
            {
                OkClicked.Invoke(true);
            }

            Dismiss();


        }



        void OnCancelClick(object sender, EventArgs e)
        {
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {

            Dialog.Window.SetTitle( this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;

        }



    }
}



