﻿using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class QAS_Cart_Activity : BaseActivity
    {


       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.QAS_Cart_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();

        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
//            listView.ItemClick -= OnListItemClick;
            QAS_PrepareCart_SessionState.QASPrepareCartTask = QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data[e.Position];
            ShowActionDialog();
//            TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId.ToString()));
        }

        public void ShowActionDialog()
        {
            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new QAS_CartActionDialog(this,"Cart");

            dialogFragment.Cancelable = false;
            dialogFragment.OnActionClick += (result) => {
                
                switch (result)
                {
                   
                    case QAS_CartActionDialog.CartActionResultType.Task:
                        TaskSwitcher();
                     break;
                    case QAS_CartActionDialog.CartActionResultType.OtherTask:
                        GoToOtherTask();
                    break;
                    case QAS_CartActionDialog.CartActionResultType.Detail:
                        GoToTask_CartDetails();
                        break;
                }
     
            };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

       
        public void ShowWeightDialog()
        {
            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new QAS_WeightDialog(this,"Cart Weights","Please enter cart weights");

            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked += (result) => {

                GoToTask_WeightCart();

            };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void TitleSwitch()
        {
            string title = "";
            if (DropDownText.Text == enmCartFilters.WAREHOUSE.ToString())
            {
                title = enmCartFilters.WAREHOUSE.ToString();

            }
            else if (DropDownText.Text == enmCartFilters.RUNNER.ToString())
            {
                title = enmCartFilters.RUNNER.ToString();
            }
            else if (DropDownText.Text == enmCartFilters.IN_MY_POSSESSION.ToString().Replace("_", " "))
            {
                title = enmCartFilters.IN_MY_POSSESSION.ToString().Replace("_", " ");
            }

            titleLabel.Text = title;

        }

        private void RefreshData(string searchData,bool isBarcode)
        {
//            QAS_PrepareCart_SessionState.SearchedBarcode = searchData;

            if(searchData.Length >100)
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(GetText(Resource.String.Enter_Exceed_Limit), MessageBox.AlertType.Information);
                return;
            }

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
                    
                    GetCartsParam parameters = new GetCartsParam();

                    if(DropDownText.Text == enmCartFilters.WAREHOUSE.ToString())
                    {    
                        if(QAS_PrepareCart_SessionState.SelectedStatusFilter == null)
                        {
                            long[]  statusids = MCH.Communication.QAS_PrepareCart.Instance.WarehouseStatusids; 
                            parameters.StatusIds= statusids;
                        }
                        else
                        {
                            parameters.StatusIds= QAS_PrepareCart_SessionState.SelectedStatusFilter;
                        }
                       
                    }
                    else if(DropDownText.Text == enmCartFilters.RUNNER.ToString())
                    {
                        if(QAS_PrepareCart_SessionState.SelectedStatusFilter == null)
                        {
                            long[] statusids = MCH.Communication.QAS_PrepareCart.Instance.RunnerStatusids; 
                            parameters.StatusIds= statusids;
                        }
                        else
                        {
                            parameters.StatusIds= QAS_PrepareCart_SessionState.SelectedStatusFilter;
                        }

                    }
                    else if(DropDownText.Text == enmCartFilters.IN_MY_POSSESSION.ToString().Replace("_"," "))
                    {
                        // Only pass holderID no filter, pass one if filter selected
                        if(QAS_PrepareCart_SessionState.SelectedStatusFilter != null)
                        {
                            parameters.StatusIds= QAS_PrepareCart_SessionState.SelectedStatusFilter;
                        }

                        parameters.HolderId = ApplicationSessionState.User.Data.UserId;

                    }

                   
                    QAS_PrepareCart_SessionState.QASPrepareCartTasks = MCH.Communication.QAS_PrepareCart.Instance.GetActiveCarts(parameters);
               
                    RunOnUiThread (delegate {
                    
                    if(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Transaction.Status)
                    {
                        if(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data!=null)
                        {
                           QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data =  LinqHelper.Query<CartListItem>( QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data,searchData);
//                            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count);
                            TitleSwitch();
                            listView.Adapter = new QAS_Cart_Adapter(this, QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;   

                        }
                    }
                    else
                    {
                            if(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Transaction.Error.Contains("ConnectFailure"))
                            {
                                MessageBox m = new MessageBox(this);
                                m.OnConfirmationClick += (bool result) => 
                                    {
                                        DoLogOut();
                                    };
                                m.ShowConfirmationMessage(Resource.String.NoInternet_Logout);
                            }
                            else
                            {
                                MessageBox msg = new MessageBox(this);
                                msg.ShowAlert(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Transaction.Error, MessageBox.AlertType.Error);  

                            }

                    }
          
                    if(isBarcode)
                    {    
                        //find cart from scan cart
                        try
                        {
                                
                            QAS_PrepareCart_SessionState.QASPrepareCartTask =  MCH.Communication.QAS_PrepareCart.Instance.GetScanCart(searchData).Data;
                            if(QAS_PrepareCart_SessionState.QASPrepareCartTask != null)
                             {
                                ShowActionDialog();
                             }
                            else
                            {
                                GoToTask_PrepareNewcart(searchData);
                            }

                        }
                        catch(Exception ex)
                        {
                            MessageBox m = new MessageBox(this);
                            m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
                        }
                    }
                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

       
        public void TaskSwitcher()
        {

            switch (QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId)
            {
                //outbound actions
                case enmCartStatus.PENDING:
                    GoToTask_AddLocation();
                    break;

                case enmCartStatus.READY_TO_FILL:
                case enmCartStatus.CLOSED:
                    ShowWeightDialog();
                    break;

                case enmCartStatus.WEIGHED:
                    GoToTask_ReadyToStage();
                    break;
                
                case enmCartStatus.DROPPED:
                case enmCartStatus.STAGED_NO_GATE:
                case enmCartStatus.STAGED_NO_FLIGHT:
                case enmCartStatus.READY_FOR_PICKUP:
                case enmCartStatus.IN_TRANSIT:
                    SelectLocation();

                    break;

                case enmCartStatus.CONFIRMED_LOADED:
                case enmCartStatus.DEPARTED:
                case enmCartStatus.NOT_LOADED_AT_GATE:
                case enmCartStatus.DROPPED_AT_XFER_GATE:
                    GoToTask_RetrieveCart();
                    break;

                case enmCartStatus.NOT_LOADED_RETRIVED_BY_RUNNER:
                    GoToTask_ReadyToStage();
                    break;

                    //inbound actions
                case enmCartStatus.RETRIEVED_BY_RUNNER:
                    DropAtInboundStage();
                    break;

                case enmCartStatus.DROPPED_AT_INBOUND_STAGE:
                    GoToTask_UnloadCart();
                    break;

                case enmCartStatus.INBOUND_INFLIGHT:
                case enmCartStatus.InboundNewCart:
                    GoToTask_RETRIEVED_BY_RUNNER();
                    break;

                default:
                    GoToTask_Default();
                    break;
            }
        }



        public void TaskOptionClickAction(OptionItem  option)
        {

            switch (option.OptionAction)
            {
                //outbound
                case  OptionActions.Remove:
                    GoToTask_RemoveCart();                   
                    break;

                case  OptionActions.Reopen:
                    GoToTask_AddLocation();
                    break;

                case  OptionActions.Weight:
                    ShowWeightDialog();
//                    GoToTask_WeightCart();
                    break;

                case  OptionActions.Move:
                    GoToTask_MoveToStage();
                    break;
                case  OptionActions.DropCart:
                    GoToTask_DropCart();
                    break;
                case  OptionActions.DropAtStage:
                    GoToTask_DropOutboundStage();
                    break;
                case OptionActions.Info:
                    GoToTask_CartDetails();
                    break;

                    //inbound
                case  OptionActions.DropAtGate:
                    //DropAtGate();    
                    SelectLocation();
                    break;
                case  OptionActions.DropAtOutboundStage:
                    DropAtOutboundStage();
                    break;
                case  OptionActions.DropAtInboundStage:
                    DropAtInboundStage();
                    break;
                case  OptionActions.Exit:
                    break;

                case  OptionActions.View:
                    
                    break;
            }

        }

        private void SelectLocation()
        {
            var transaction = FragmentManager.BeginTransaction();

            var dialogFragment = new QAS_LocationDialogActivity(this, "Location","Select Location",true, enmWarehouseLocationTypes.CartBoneyard);
            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked+= (Barcode barcode) => 
                {
                    if(QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId == enmCartStatus.IN_TRANSIT)
                    {
                        GoToTask_DropCartAtGate(barcode.Id);
                    }
                    else
                    {
                        GoToTask_EnrouteToGate(barcode.Id);
                    }
                    
                };
            dialogFragment.CancelClicked+= () => 
                {
                    DoBack();
                    //                    if (QAS_TruckAPC_SessionState.QASTruckLocationTask.Data.LocationId == 0)
                    //                    {
                    //                        DoBack();
                    //                    }
                };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }


        #region Go to task and other tasks

        public void GoToTask_Default()
        {
            List<OptionItem> taskOptions = new List<OptionItem>();
            taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

            Action<OptionItem> OptionClickAction = TaskOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        public void GoToTask_AddLocation()
        {


            List<SelectionItem> list  = new List<SelectionItem>();
            Airports airport = QAS_PrepareCart.Instance.GetDestinations();
            if (airport.Transaction.Status)
            {
                if (airport.Data != null)
                {
                    foreach (var n in airport.Data)
                    {
                        list.Add(new SelectionItem(n.IATACode, n.AirportId, Resource.Drawable.Airport));
                    }
                }
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(airport.Transaction.Error, MessageBox.AlertType.Error);
                return;
            }

            if (list.Count == 0)
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert("Unable to retrieve locations.", MessageBox.AlertType.Information);
                return;
            }

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Destinations", this, list, SelectionListAdapter.SelectionMode.SingleSelection, true);
            dialogFragment.Cancelable = false;

            dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
                {
                    foreach (var r in selection)
                    {

                        UpdateCartParams model = new UpdateCartParams();
                        model.CartId = QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId;
                        model.DestinationId = r.Id;
                        model.ClientDateTime = DateTime.Now;

                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartDestination(model);
                        if (t.Status)
                        {
                            t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_TO_FILL,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
                            if (t.Status)
                            {
                                MessageBox msg1 = new MessageBox(this);
                                msg1.OnConfirmationClick+= (bool result) => 
                                    {  
                                        RefreshData(search.Text, false);
                                    };
                                //                                msg1.ShowAlert("Hello",MessageBox.AlertType.Information);
                                msg1.ShowAlert(string.Format("{0} destination has been assigned to Cart# {1}.",r.Name,QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference ),MessageBox.AlertType.Information);

                                //Cart #<reference> Destination is set to <code>
                            }


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }


                    }

                };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);


        }

        public void GoToTask_RETRIEVED_BY_RUNNER()
        {
            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.RETRIEVED_BY_RUNNER,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
            if (t.Status)
            {


                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {   
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(string.Format("Cart# {0} has been retrieved.",  QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference),MessageBox.AlertType.Information);


            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
        }

        public void GoToTask_CloseCart()
        {
            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.CLOSED,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
            if (t.Status)
            {


                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {   
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(string.Format("Cart# {0} has been closed.",  QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference),MessageBox.AlertType.Information);


            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
        }


        public void GoToOtherTask()
        {
            List<OptionItem> taskOptions = new List<OptionItem>();

            switch (QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId)
            {
                case  enmCartStatus.PENDING:
                case  enmCartStatus.DROPPED_AT_INBOUND_STAGE:
                case  enmCartStatus.EMPTIED:
                case  enmCartStatus.DEACTIVATED:
                case  enmCartStatus.INBOUND_INFLIGHT:
                case  enmCartStatus.InboundNewCart:
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                    break;

                case  enmCartStatus.READY_TO_FILL:
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Assign_Destination),OptionActions.Reopen, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                    break;

                case  enmCartStatus.CLOSED:
                case enmCartStatus.DROPPED:
                case enmCartStatus.CONFIRMED_LOADED:
                case enmCartStatus.DEPARTED:
                case enmCartStatus.NOT_LOADED_AT_GATE:
                case enmCartStatus.DROPPED_AT_XFER_GATE:
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu)); 
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));  
                break;

                case  enmCartStatus.WEIGHED:
                case enmCartStatus.STAGED_NO_GATE:
                case enmCartStatus.STAGED_NO_FLIGHT:
                case enmCartStatus.READY_FOR_PICKUP:
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu)); 
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Weight_Cart),OptionActions.Weight, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));  
                    break;

                case enmCartStatus.IN_TRANSIT:
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu)); 
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Weight_Cart),OptionActions.Weight, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_Stage),OptionActions.DropAtStage, Resource.Drawable.Menu)); 
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));  
                    break;

                case enmCartStatus.NOT_LOADED_RETRIVED_BY_RUNNER:
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu)); 
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_Cart_at_Gate),OptionActions.DropAtGate, Resource.Drawable.Menu)); 
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));  
                    break;

                case enmCartStatus.RETRIEVED_BY_RUNNER:
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_Cart_at_Gate),OptionActions.DropAtGate, Resource.Drawable.Menu)); 
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));  
                    break;

               
            }

           
            taskOptions.Add(new OptionItem(GetText(Resource.String.Cancel),OptionActions.Exit, Resource.Drawable.Menu)); 


            Action<OptionItem> OptionClickAction = TaskOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }



        public void GoToTask_PrepareNewcart(string barcode)
        {
            string title = "Enter details for cart " + barcode;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment2 = new QAS_NewCartDialog(this,ApplicationSessionState.SelectedMenuItem.Name.ToUpper(),title,barcode);
            dialogFragment2.OkClicked+= () =>  
                {
                    // QAS_PrepareCart_SessionState.PrepareNewCartNumber = cartNumber.BarcodeText;
                    DoPrepareCart(barcode);
                };
            dialogFragment2.CancelClicked += () =>
                {
                    RefreshData(string.Empty,false);
//                    DoPrepareCart(barcode);
                };
            dialogFragment2.Cancelable = false;
            dialogFragment2.Show(transaction, ApplicationSessionState.ApplicationName);
        }



        public void DoPrepareCart(string barcode)
        {
            try
            {
                PrepareCartParam model = new PrepareCartParam();
                model.Barcode = barcode;
                if(QAS_PrepareCart_SessionState.PrepareNewCartNumber != null)
                {
                    model.Number = QAS_PrepareCart_SessionState.PrepareNewCartNumber;
                }
                if(QAS_PrepareCart_SessionState.PrepareNewCartTypeId != 0)
                {
                    model.CartTypeId = QAS_PrepareCart_SessionState.PrepareNewCartTypeId;
                }
                model.ClientDateTime = DateTime.Now;
                model.UserId = ApplicationSessionState.User.Data.UserId;

                QASPrepareCartTask task=MCH.Communication.QAS_PrepareCart.Instance.PrepareNewCart(model);  
                if(task.Transaction.Status)
                {
                    if(task.Data!=null)
                    {
                        QAS_PrepareCart_SessionState.QASPrepareCartTask =  task.Data;
                        EditTextListener.Text = string.Empty;
                        search.RequestFocus ();
                        ShowActionDialog();
                        //                                            TaskSwitcher(task.Data.StatusId);
                        RefreshData(string.Empty,false);
                    }
                    else
                    {
                        //do nothign
                    }
                }
                else
                {

                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(task.Transaction.Error,MessageBox.AlertType.Error);
                    return;
                }
                return;
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
            }
        }

        public void GoToTask_RemoveCart() // change to deactivate cart
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {      
                        
                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.DeactivateCart(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());
                        if (t.Status)
                        {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {             
                                    RefreshData(search.Text, false);                                 
                                };
                            msg.ShowAlert(string.Format("Cart# {0} has been removed." , QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString())  ,MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };


            m.ShowConfirmationMessage(QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference+" - "+QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination+ "\n\n"+ GetText(Resource.String.Confirm_Remove),GetText(Resource.String.Yes), GetText(Resource.String.No));

        }


        private void GoToTask_WeightCart()
        {
            UpdateCartParams model = new UpdateCartParams();
            model.CartId = QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId;
            model.Weight = QAS_PrepareCart_SessionState.QASPrepareCartTask.GrossWeight;
            model.TareWeight = QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight;
            model.DollyWeight = QAS_PrepareCart_SessionState.QASPrepareCartTask.DollyWeight;
            model.ClientDateTime = DateTime.Now;

            //1. First, it should save the weights and update the status to weighed (103)
            //2. Then, it should perform another status update to Ready for Pickup (106)
            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartWeight(model);
            if (t.Status)
            {

                t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.WEIGHED,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
                if (t.Status)
                {

                    t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_FOR_PICKUP,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
                    if (t.Status)
                    {

                        MessageBox msg = new MessageBox(this);
                        msg.OnConfirmationClick+= (bool r) => 
                            {            
                                RefreshData(search.Text, false);                                  
                            };
                        msg.ShowAlert(string.Format("Cart# {0} has been weighed.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString() ),MessageBox.AlertType.Information);



                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }

                }
                else
                {
                    MessageBox msg = new MessageBox(this);
                    msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }

            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
        }


        public void GoToTask_ReadyToStage()
        {

            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_FOR_PICKUP,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);

            if (t.Status)
            {


                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);

                    };
                msg.ShowAlert(string.Format("Cart# {0} is ready to be brought to the gate.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);


            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }


        public void GoToTask_EnrouteToGate(long warehouseId)
        {

            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.IN_TRANSIT,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),warehouseId);

            if (t.Status)
            {
                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {    
                        RefreshData(search.Text, false);                         
                    };
                msg.ShowAlert(string.Format("Cart# {0} is in your possession now.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);

            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }



        private void GoToTask_DropOutboundStage()
        {

            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_FOR_PICKUP,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);

            if (t.Status)
            {



                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);

                    };
                msg.ShowAlert(string.Format("Cart# {0} is dropped in stage.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);


            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }

        public void GoToTask_DropCartAtGate(long warehouseId)
        {
            
            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.DROPPED,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),warehouseId);
            if (t.Status)
            {


                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(string.Format("Cart# {0} has been dropped.",QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);

            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }


        public void GoToTask_RetrieveCart()  
        {

            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.NOT_LOADED_RETRIVED_BY_RUNNER,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
            if (t.Status)
            {


                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(string.Format("Cart# {0} has been retrieved.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);


            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }


        private void GoToTask_MoveToStage()
        {
            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_FOR_PICKUP ,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
            if (t.Status)
            {


                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(string.Format("Cart# {0} has been moved to stage.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);



            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }         

        }
        public void GoToTask_CartDetails()
        {

            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new QAS_CartDetailDialog(this,"Cart Details");

            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);


        }

        public void GoToTask_DropCart()
        {
            List<OptionItem> taskOptions = new List<OptionItem>();
            taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_Cart_at_Gate),OptionActions.DropCart, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_OutboundStage),OptionActions.DropAtOutboundStage, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_InboundStage),OptionActions.DropAtInboundStage, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Cancel),OptionActions.Exit, Resource.Drawable.Menu));

            Action<OptionItem> OptionClickAction = TaskOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }


        public void GoToTask_UnloadCart() 
        {
            
            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.EMPTIED, ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
            if (t.Status)
            {
                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(String.Format("{0} has been emptied.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);

            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }

//        private void DropAtGate(long warehouseId)
//        {
//            
//            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.DROPPED,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),warehouseId);
//            if (t.Status)
//            {
//               
//                MessageBox msg = new MessageBox(this);
//                msg.OnConfirmationClick+= (bool r) => 
//                    {
//                        RefreshData(search.Text, false);
//                    };
//                msg.ShowAlert(String.Format( "{0} has been dropped.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);
//            }
//            else
//            {
//                MessageBox msg = new MessageBox(this);
//                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
//            }
//
//        }

        private void DropAtOutboundStage()
        {
           
            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_FOR_PICKUP,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
            if (t.Status)
            {
                //                            Toast.MakeText (Activity, QAS_PrepareCart_SessionState.QASPrepareCartTask.CartNumber.ToString() + " has been dropped at stage.", ToastLength.Long).Show ();
                //                            RefreshData("", false);

                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(String.Format( "{0} has been dropped at outbound stage.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }

        private void DropAtInboundStage()
        {
            
            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.DROPPED_AT_INBOUND_STAGE,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData(),-1);
            if (t.Status)
            {
                //                            Toast.MakeText (Activity, QAS_PrepareCart_SessionState.QASPrepareCartTask.CartNumber.ToString() + " has been dropped at stage.", ToastLength.Long).Show ();
                //                            RefreshData("", false);

                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        RefreshData(search.Text, false);
                    };
                msg.ShowAlert(String.Format( "{0} has been dropped at inbound stage.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }

        }

        #endregion
    }
}

