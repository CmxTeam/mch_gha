﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class QAS_MyTask_Adapter : BaseAdapter<TaskListItem> {

        List<TaskListItem> items;
        Activity context;
        


        public QAS_MyTask_Adapter(Activity context, List<TaskListItem> items  ): base()
        {
 
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override TaskListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.QAS_MyTask_Row, null);

            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            TextView txtTaskType = view.FindViewById<TextView>(Resource.Id.txtTaskType);
            TextView txtFlightInfo = view.FindViewById<TextView>(Resource.Id.txtFlightInfo);
            TextView txtEntity = view.FindViewById<TextView>(Resource.Id.txtEntity);
            TextView txtGate = view.FindViewById<TextView>(Resource.Id.txtGate);

 
            txtTaskType.Text = item.Title;

            if (item.Flight != null && item.TaskTypeId == enmTaskTypes.Pickup_From_Flight )
            {
                txtEntity.Text = string.Format("{0}{1}  {2}-{3}", item.Flight.CarrierCode, item.Flight.FlightNumber, item.Flight.Departure.IATACode, item.Flight.Arrival.IATACode);
                txtGate.Text = item.Flight.ArrivalGate;
                txtFlightInfo.Text = string.Format("Arr: {0:MMM-dd HH:mm}", item.Flight.ArrivalTimeLocal);
            }
            else if (item.Cart != null && (item.TaskTypeId ==enmTaskTypes.Bring_To_Gate || item.TaskTypeId ==enmTaskTypes.Pickup_From_Warehouse) )
            {
                txtEntity.Text = string.Format("{0}  {1}",item.Cart.Reference,item.Cart.CartDestination);
                if (item.Cart.OutboundFlight != null)
                {
                    txtGate.Text = item.Cart.OutboundFlight.DepartureGate;
                    txtFlightInfo.Text = string.Format("{0}{1} {2}-{3} Dep:{4:MMM-dd HH:mm}", item.Cart.OutboundFlight.CarrierCode, item.Cart.OutboundFlight.FlightNumber, item.Cart.OutboundFlight.FlightOrigin, item.Cart.OutboundFlight.FlightDestination, item.Cart.OutboundFlight.STD);
                }

            }
            else if (item.Cart != null && item.TaskTypeId ==enmTaskTypes.Drop_At_Warehouse )
            {
                txtEntity.Text = string.Format("{0}  {1}",item.Cart.Reference,item.Cart.CartDestination);
                if (item.Cart.InboundFlight != null)
                {
                    txtGate.Text = "";
                    txtFlightInfo.Text = string.Format("{0}{1}  {2}-{3}", item.Cart.InboundFlight.CarrierCode, item.Cart.InboundFlight.FlightNumber, item.Cart.InboundFlight.FlightOrigin, item.Cart.InboundFlight.FlightDestination);
                }

            }

            if (item.Cart != null)
            {
                switch (item.Cart.StatusId)
                {
                    case enmCartStatus.PENDING:
                        RowIcon.SetImageResource(Resource.Drawable.Pending);
                        break;
                    case enmCartStatus.READY_TO_FILL:
                        RowIcon.SetImageResource(Resource.Drawable.Check);
                        break;
                    case enmCartStatus.CLOSED:
                        RowIcon.SetImageResource(Resource.Drawable.Cube);
                        break;    
                    case enmCartStatus.WEIGHED:
                        RowIcon.SetImageResource(Resource.Drawable.Filing);
                        break;  
                    case enmCartStatus.READY_FOR_PICKUP:
                        RowIcon.SetImageResource(Resource.Drawable.Completed);
                        break;  
                    case enmCartStatus.IN_TRANSIT:
                        RowIcon.SetImageResource(Resource.Drawable.Plane);
                        break; 
                    case enmCartStatus.NOT_LOADED_AT_GATE:
                        RowIcon.SetImageResource(Resource.Drawable.Empty);
                        break;

                    case enmCartStatus.RETRIEVED_BY_RUNNER:
                        RowIcon.SetImageResource(Resource.Drawable.Unload);
                        break;
                    case enmCartStatus.DROPPED_AT_INBOUND_STAGE:
                        RowIcon.SetImageResource(Resource.Drawable.Remove);
                        break;

                    default:
                        RowIcon.SetImageResource(Resource.Drawable.Pending);
                        break;
                }
            }
            else if (item.Flight != null)
            {
                RowIcon.SetImageResource(Resource.Drawable.Plane);
            }
            else
            {
                RowIcon.SetImageResource(Resource.Drawable.Pending);
            }

            return view;
        }


    }
}


