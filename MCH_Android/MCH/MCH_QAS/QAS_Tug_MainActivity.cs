﻿using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class QAS_Tug_MainActivity : BaseActivity
    {


       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.QAS_Tug_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();

        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {

            try
            {
            QAS_Tug_SessionState.QASTugTask = QAS_Tug_SessionState.QASTugTasks.Data[e.Position];
            TaskSwitcher(EnumHelper.GetEnumItem<TugStatusTypes>(QAS_Tug_SessionState.QASTugTask.StatusId.ToString()));
            }
            catch
            {
                
            }
        }


        private void TaskSwitcher(TugStatusTypes status)
        {

            switch (status)
            {
               
                case TugStatusTypes.AVAILABLE:
                    GoToTask_CheckOut();
                break;

                case TugStatusTypes.CHECKED_OUT:
                    GoToTask_CheckIn();
                break;

            }


        }

        private long GetCheckedOutTugId()
        {
           
            long tugid = 0;
            QAS_Tug_SessionState.QASTugTasks = MCH.Communication.QAS_Tug.Instance.GetTugs(ApplicationSessionState.User.Data.UserId,TugStatusTypes.CHECKED_OUT);


            if(QAS_Tug_SessionState.QASTugTasks.Transaction.Status)
            {
                    if (QAS_Tug_SessionState.QASTugTasks.Data.Count != 0)
                    {
                    tugid = QAS_Tug_SessionState.QASTugTasks.Data[0].TugId;
                    }

            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(QAS_Tug_SessionState.QASTugTasks.Transaction.Error, MessageBox.AlertType.Error);
            }
            return tugid;
        }

        private void GoToTask_CheckOut() 
        {
            if (GetCheckedOutTugId() == 0)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {                       
                        CommunicationTransaction t = MCH.Communication.QAS_Tug.Instance.UpdateTugStatus(QAS_Tug_SessionState.QASTugTask.TugId, ApplicationSessionState.User.Data.UserId, TugStatusTypes.CHECKED_OUT);
                        if (t.Status)
                        {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick += (bool r) =>
                            {
                                        GotoMainMenu();
                            };
                            msg.ShowAlert(String.Format("Tug#{0} has been checked out.", QAS_Tug_SessionState.QASTugTask.Reference), MessageBox.AlertType.Information);
                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }
                    else
                    {
                        RefreshData(search.Text, false);
                    }

                };


                m.ShowConfirmationMessage("Tug# " + QAS_Tug_SessionState.QASTugTask.Reference.ToString() + "\n\n" + GetText(Resource.String.Confirm_Tug_CheckOut), GetText(Resource.String.Yes), GetText(Resource.String.No));
            }
            else
            {
                RefreshData(search.Text,false);
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(GetText(Resource.String.CheckOut_One_Tug), MessageBox.AlertType.Information);
               

            }
        }

        private void GoToTask_CheckIn() 
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {                       
                        CommunicationTransaction t = MCH.Communication.QAS_Tug.Instance.UpdateTugStatus(QAS_Tug_SessionState.QASTugTask.TugId,ApplicationSessionState.User.Data.UserId,TugStatusTypes.AVAILABLE);
                        if (t.Status)
                        {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {
//                                    RefreshData(search.Text, false);
                                    GotoMainMenu();
                                };
                            msg.ShowAlert(String.Format("Tug# {0} has been checked in.", QAS_Tug_SessionState.QASTugTask.Reference.ToString()),MessageBox.AlertType.Information);
                                
                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }
                    else
                    {
                        RefreshData(search.Text,false);
                    }

                };


            m.ShowConfirmationMessage("Tug# "+QAS_Tug_SessionState.QASTugTask.Reference.ToString()+ "\n\n"+ GetText(Resource.String.Confirm_Tug_CheckIn),GetText(Resource.String.Yes), GetText(Resource.String.No));

        }

//        private string GPSInterval()
//        {
//
//            QASTugGPSInterval interval = MCH.Communication.QAS_Tug.Instance.GPSinterval();
//            if (interval.Transaction.Status)
//            {
//                if (interval.Data != null)
//                {
//                    return interval.Data;                   
//                }
//            }
//
//            return string.Empty;
//        }

        private void LoadtitleLabel(TugStatusTypes status)
        {
            switch (status)
            {
                case TugStatusTypes.AVAILABLE:
                    titleLabel.Text =GetText(Resource.String.Check_Out_Tug).ToUpper() + string.Format(" - ({0})", QAS_Tug_SessionState.QASTugTasks.Data.Count);
                    break;
                case TugStatusTypes.CHECKED_OUT:
                    titleLabel.Text = GetText(Resource.String.Check_In_Tug).ToUpper() + string.Format(" - ({0})", QAS_Tug_SessionState.QASTugTasks.Data.Count);
                    break;    
                
                default:
                    titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper() + string.Format(" - ({0})",QAS_Tug_SessionState.QASTugTasks.Data.Count);
                    break;
            }

        }
       

        private void RefreshData(string searchData,bool isBarcode)
        {
//            string interval= GPSInterval();

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
 
                    TugStatusTypes status =    EnumHelper.GetEnumItem<TugStatusTypes>(DropDownText.Text);
                    QAS_Tug_SessionState.QASTugTasks = MCH.Communication.QAS_Tug.Instance.GetTugs(ApplicationSessionState.User.Data.UserId,status);

                    RunOnUiThread (delegate {

                    if(QAS_Tug_SessionState.QASTugTasks.Transaction.Status)
                    {
                            if(QAS_Tug_SessionState.QASTugTasks.Data!=null)
                        {
                                QAS_Tug_SessionState.QASTugTasks.Data =  LinqHelper.Query<TugListItem>( QAS_Tug_SessionState.QASTugTasks.Data,searchData);
//                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",QAS_Pickup_DropCart_SessionState.QASTugTasks.Data.Count);
                                LoadtitleLabel(status);
                                listView.Adapter = new QAS_Tug_Adapter(this, QAS_Tug_SessionState.QASTugTasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;                          
                        }
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(QAS_Tug_SessionState.QASTugTasks.Transaction.Error, MessageBox.AlertType.Error);
                    }

                        if(isBarcode)
                        {    

                            QAS_Tug_SessionState.QASTugTasks.Data =  LinqHelper.Query<TugListItem>(QAS_Tug_SessionState.QASTugTasks.Data,searchData);
                            if(QAS_Tug_SessionState.QASTugTasks.Data!=null && QAS_Tug_SessionState.QASTugTasks.Data.Count==1)
                            {
                                QAS_Tug_SessionState.QASTugTask = QAS_Tug_SessionState.QASTugTasks.Data[0];
                                TaskSwitcher(EnumHelper.GetEnumItem<TugStatusTypes>(QAS_Tug_SessionState.QASTugTask.StatusId.ToString()));   
                            }
                            else
                            {
                                try
                                {

                                    QASTugTask task=MCH.Communication.QAS_Tug.Instance.GetTug(searchData);  
                                    if(task.Transaction.Status)
                                    {
                                        if(task.Data!=null)
                                        {
                                            QAS_Tug_SessionState.QASTugTask =  task.Data;
                                            EditTextListener.Text = string.Empty;
                                            search.RequestFocus ();

                                            TaskSwitcher(task.Data.StatusId);
//                                            TugStatusTypes temstatus = TugStatusTypes.ALL;                                           
//                                            if(task.Data.StatusId != null)
//                                            {                                                
//                                                temstatus = (TugStatusTypes)task.Data.StatusId;
//                                            }
//                                            TaskSwitcher(temstatus);

                                            RefreshData(string.Empty,false);
                                        }
                                        else
                                        {
                                            //do nothign
                                        }
                                    }
                                    else
                                    {
                                        RunOnUiThread(() => progressDialog.Hide());
                                        //show msg ...... task.Transaction.Error

                                        MessageBox m = new MessageBox(this);
                                        m.ShowAlert(task.Transaction.Error,MessageBox.AlertType.Error);
                                        return;
                                    }
                                    return;
                                }
                                catch(Exception ex)
                                {
                                    MessageBox m = new MessageBox(this);
                                    m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
                                }
                            }



                        }
          
//                    if(isBarcode)
//                    {                  
//                            QASTugTask task=MCH.Communication.QAS_Tug.Instance.GetTug(searchData);  
//                        
//                            QAS_Tug_SessionState.QASTugTask =  task.Data;
//                        
//                            // todo no transaction in QASTugTask
//                    
//                                EditTextListener.Text = string.Empty;
//                                search.RequestFocus ();
//
//                                TaskSwitcher(task.Data.StatusId);
//                          
//
//                           //RunOnUiThread(() => progressDialog.Hide());
//                        return;
//
//                    }
                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

       

    }
}

