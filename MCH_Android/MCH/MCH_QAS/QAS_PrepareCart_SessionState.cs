﻿
using System;
using System.Collections.Generic;
namespace MCH
{
    public class QAS_PrepareCart_SessionState
    {
        private QAS_PrepareCart_SessionState()
        {
        }


        ~QAS_PrepareCart_SessionState()  // destructor
        {
 
        }
       
        //Carts
        public static MCH.Communication.QASPrepareCartTasks QASPrepareCartTasks;
        public static MCH.Communication.CartListItem QASPrepareCartTask;   
        public static MCH.Communication.QASNewMasterCartTask QASNewMasterCartTask;  

        // Tasks
        public static MCH.Communication.QASMyTasks QASMyTasks;
        public static MCH.Communication.TaskListItem QASMyTask;  

        // Other
//        public static string SearchedBarcode;
        public static double GrossWeight;
        public static double TareWeight;
        public static double DollyWeight;
        public static long[] SelectedStatusFilter;
        public static string PrepareNewCartNumber;
        public static int PrepareNewCartTypeId;

    }
}
 