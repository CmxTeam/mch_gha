﻿ 

using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class QAS_PickupFlight_CartActivity : BaseActivity
    {
         
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

//            SetContentView (Resource.Layout.QAS_Pickup_CartLayout);
            Initialize();
            RefreshAll();



            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;

        }

        private void RefreshAll()
        {

            LoadDetail();
            RefreshData(string.Empty,false);
//            search.RequestFocus (); 
//            QAS_PickupFlight_SessionState.BarcodeList = new List<string>();

        }


        private void DoBack()
        {
            StartActivity (typeof(QAS_PickupFlight_MainActivity));
 
            this.Finish();
        }


        private void btnDone_Click(object sender, EventArgs e)         {
           
            if (QAS_PickupFlight_SessionState.BarcodeList.Count != 0)
            {

                PickupFlightStatusTypes completeStatus = new PickupFlightStatusTypes();
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                            completeStatus = PickupFlightStatusTypes.Inbound_PickupPartial;                   
                    }
                    else
                    {
                            completeStatus = PickupFlightStatusTypes.Inbound_PickupComplete;   
                    }

                        CommunicationTransaction t = MCH.Communication.QAS_PickupCart.Instance.RetrieveCartsFromFlight(ApplicationSessionState.User.Data.UserId,QAS_PickupFlight_SessionState.BarcodeList,completeStatus,QAS_PickupFlight_SessionState.QASPickupFlightTask.FlightId,(decimal)ApplicationSessionState.Latitude,(decimal)ApplicationSessionState.Longitude);
                    if (t.Status)
                    {

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {
                                    GotoMainMenu();
                                };
                            msg.ShowAlert(GetText(Resource.String.Confirm_CartPickup),MessageBox.AlertType.Information);

                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        return;
                    }
                };

                m.ShowConfirmationMessage(GetText(Resource.String.Confirm_MoreCart), GetText(Resource.String.Yes), GetText(Resource.String.No));


            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(GetText(Resource.String.Please_scan_cart_first), MessageBox.AlertType.Error);
                return;
            }           } 


        private void btnCancel_Click(object sender, EventArgs e)         {                                     StartActivity (typeof(QAS_PickupFlight_MainActivity));
         }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            //            listView.ItemClick -= OnListItemClick;
            QAS_PickupCart_SessionState.QASPickupCartTask = QAS_PickupCart_SessionState.QASPickupCartTasks.Data[e.Position];
            TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PickupCart_SessionState.QASPickupCartTask.StatusId.ToString()));
        }

        private void TaskSwitcher(enmCartStatus status)
        {

            switch (status)
            {

                case enmCartStatus.RETRIEVED_BY_RUNNER:
//                    GoToTask_DropStage();
                    break;

                case enmCartStatus.DROPPED_AT_INBOUND_STAGE:
//                    GoToTask_UnloadCart();
                    break;

            }


        }



        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {

                    enmCartStatus StatusId =    enmCartStatus.RETRIEVED_BY_RUNNER;
                    GetPickupCartsParam parameters = new GetPickupCartsParam();
                    parameters.IsInbound= true;
                    parameters.StatusIds = new int[1];

                    if(!isBarcode)
                    {
                        parameters.StatusIds[0] = (int)StatusId;
                    }
                    else
                    {
                        parameters.StatusIds[0] = (int)enmCartStatus.ALL;
                    }

                    QAS_PickupCart_SessionState.QASPickupCartTasks = MCH.Communication.QAS_PickupCart.Instance.GetActiveCarts(parameters);

                    RunOnUiThread (delegate {

                        if(QAS_PickupCart_SessionState.QASPickupCartTasks.Transaction.Status)
                        {
                            if(QAS_PickupCart_SessionState.QASPickupCartTasks.Data!=null)
                            {
                                QAS_PickupCart_SessionState.QASPickupCartTasks.Data =  LinqHelper.Query<CartListItem>( QAS_PickupCart_SessionState.QASPickupCartTasks.Data,searchData);
                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count);
//                                LoadtitleLabel(StatusId);
                                listView.Adapter = new QAS_PickupCart_Adapter(this, QAS_PickupCart_SessionState.QASPickupCartTasks.Data);
                                listView.ItemClick -= OnListItemClick;
                                listView.ItemClick += OnListItemClick;                          
                            }
                        }

                        if(isBarcode)
                        {    

                            QAS_PickupCart_SessionState.QASPickupCartTasks.Data =  LinqHelper.Query<CartListItem>(QAS_PickupCart_SessionState.QASPickupCartTasks.Data,searchData);
                            if(QAS_PickupCart_SessionState.QASPickupCartTasks.Data!=null && QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count==1)
                            {
                                QAS_PickupCart_SessionState.QASPickupCartTask = QAS_PickupCart_SessionState.QASPickupCartTasks.Data[0];
                                TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PickupCart_SessionState.QASPickupCartTask.StatusId.ToString()));   
                            }
                            else
                            {
                                try
                                {

                                    QASPickupCartTask task=MCH.Communication.QAS_PickupCart.Instance.GetScanCart(searchData);  
                                    if(task.Transaction.Status)
                                    {
                                        if(task.Data!=null)
                                        {
                                            QAS_PickupCart_SessionState.QASPickupCartTask =  task.Data;
                                            EditTextListener.Text = string.Empty;
                                            search.RequestFocus ();

                                            TaskSwitcher(task.Data.StatusId);

                                            RefreshData(string.Empty,false);
                                        }
                                        else
                                        {
                                            //do nothign
                                        }
                                    }
                                    else
                                    {
                                        RunOnUiThread(() => progressDialog.Hide());
                                        //show msg ...... task.Transaction.Error

                                        MessageBox m = new MessageBox(this);
                                        m.ShowAlert(task.Transaction.Error,MessageBox.AlertType.Error);
                                        return;
                                    }
                                    return;
                                }
                                catch(Exception ex)
                                {
                                    MessageBox m = new MessageBox(this);
                                    m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
                                }
                            }



                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }

 
//        private void RefreshData(string searchData,bool isBarcode)
//        {
////            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
////            new Thread(new ThreadStart(delegate
////                {                   
//                   
//                    QAS_PickupCart_SessionState.QASPickupCartTasks = MCH.Communication.QAS_PickupCart.Instance.GetFlightCarts(QAS_PickupFlight_SessionState.QASPickupFlightTask.FlightId,true);
//
////                    RunOnUiThread (delegate {
//
//                        if(QAS_PickupCart_SessionState.QASPickupCartTasks.Transaction.Status)
//                        {
//                            if(QAS_PickupCart_SessionState.QASPickupCartTasks.Data!=null)
//                            {
//                                QAS_PickupCart_SessionState.QASPickupCartTasks.Data =  LinqHelper.Query<CartListItem>( QAS_PickupCart_SessionState.QASPickupCartTasks.Data,searchData);
//                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count);
////                                LoadtitleLabel(StatusId);
//                                listView.Adapter = new QAS_PickupCart_Adapter(this, QAS_PickupCart_SessionState.QASPickupCartTasks.Data);
//                                listView.ItemClick -= OnListItemClick;
//                                listView.ItemClick += OnListItemClick;                          
//                            }
//                        }
//
//                        if(isBarcode)
//                        {    
//
//                            QAS_PickupCart_SessionState.QASPickupCartTasks.Data =  LinqHelper.Query<CartListItem>(QAS_PickupCart_SessionState.QASPickupCartTasks.Data,searchData);
//                            if(QAS_PickupCart_SessionState.QASPickupCartTasks.Data!=null && QAS_PickupCart_SessionState.QASPickupCartTasks.Data.Count==1)
//                            {
//                                QAS_PickupCart_SessionState.QASPickupCartTask = QAS_PickupCart_SessionState.QASPickupCartTasks.Data[0];
//                                TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PickupCart_SessionState.QASPickupCartTask.StatusId.ToString()));   
//                            }
//                            else
//                            {
//                                try
//                                {
//
//                                    QASPickupCartTask task=MCH.Communication.QAS_PickupCart.Instance.GetScanCart(searchData);  
//                                    if(task.Transaction.Status)
//                                    {
//                                        if(task.Data!=null)
//                                        {
//                                            QAS_PickupCart_SessionState.QASPickupCartTask =  task.Data;
//                                            EditTextListener.Text = string.Empty;
//                                            search.RequestFocus ();
//
//                                            TaskSwitcher(task.Data.StatusId);
//
//                                            RefreshData(string.Empty,false);
//                                        }
//                                        else
//                                        {
//                                            //do nothign
//                                        }
//                                    }
//                                    else
//                                    {
////                                        RunOnUiThread(() => progressDialog.Hide());
//                                        //show msg ...... task.Transaction.Error
//
//                                        MessageBox m = new MessageBox(this);
//                                        m.ShowAlert(task.Transaction.Error,MessageBox.AlertType.Error);
//                                        return;
//                                    }
//                                    return;
//                                }
//                                catch(Exception ex)
//                                {
//                                    MessageBox m = new MessageBox(this);
//                                    m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
//                                }
//                            }
//
//
//
//                        }
//
////                    });
////
////                    RunOnUiThread(() => progressDialog.Hide());
////                })).Start();
//
//        }


//        private void RefreshData(string searchData,bool isBarcode)
//        {
//            
//            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
//            new Thread(new ThreadStart(delegate
//                {
//
//
//                    RunOnUiThread (delegate {
//
//                        if(isBarcode)
//                        {                  
//                            txtBarcode.Text = string.Empty;
//
//                            if(QAS_PickupFlight_SessionState.BarcodeList.Count < 4)
//                            {
//                                if(!QAS_PickupFlight_SessionState.BarcodeList.Contains(searchData))
//                                {
//                                    QAS_PickupFlight_SessionState.BarcodeList.Add(searchData);   
//                                }
//                                                            
//                            }
//                            else
//                            {
//                                MessageBox m = new MessageBox(this);
//                                m.ShowAlert(GetText(Resource.String.Confirm_Max4Carts), MessageBox.AlertType.Error);
//                               
//                            }
//                                                       
//                            for (int i = 0; i < QAS_PickupFlight_SessionState.BarcodeList.Count; i++)
//                            {                               
//                                txtBarcode.Text += QAS_PickupFlight_SessionState.BarcodeList[i].ToString() + "\n";
//                            }
//
//                           
//                            // add barcode to row ??
//                            EditTextListener.Text = string.Empty;
//                            search.RequestFocus ();
//                                                       
//                            return;
//                        }
//
//                    });
//
//                    RunOnUiThread(() => progressDialog.Hide());
//                })).Start();
//        }
//
//
//
    }



}



