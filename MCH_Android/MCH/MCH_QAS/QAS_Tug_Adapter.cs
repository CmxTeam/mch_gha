﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class QAS_Tug_Adapter : BaseAdapter<TugListItem> {

        List<TugListItem> items;
        Activity context;
        


        public QAS_Tug_Adapter(Activity context, List<TugListItem> items  ): base()
        {
 
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override TugListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.QAS_Tug_MainRow, null);

            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            TextView txtTug = view.FindViewById<TextView>(Resource.Id.txtTug);
//            TextView txtFlightInfo = view.FindViewById<TextView>(Resource.Id.txtFlightInfo);

            txtTug.Text = item.Reference;

           
            //todo  need to change to correct status images
            switch (item.StatusId)
            {
                
                case TugStatusTypes.AVAILABLE:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case TugStatusTypes.CHECKED_OUT:
                    RowIcon.SetImageResource (Resource.Drawable.Check);
                    break;
               
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            return view;
        }


    }
}


