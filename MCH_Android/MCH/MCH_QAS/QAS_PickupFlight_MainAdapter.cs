﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class QAS_PickupFlight_MainManuAdapter : BaseAdapter<PickupFlightListItem> {

        List<PickupFlightListItem> items;
        Activity context;
        


        public QAS_PickupFlight_MainManuAdapter(Activity context, List<PickupFlightListItem> items  ): base()
        {
 
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override PickupFlightListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.QAS_PickupFlight_MainRow, null);

            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            TextView txtFlight = view.FindViewById<TextView>(Resource.Id.txtFlight);
            TextView txtETA = view.FindViewById<TextView>(Resource.Id.txtETA);
            TextView txtPlace = view.FindViewById<TextView>(Resource.Id.txtPlace);
            ImageView airlineImage= view.FindViewById<ImageView>(Resource.Id.airlineImage);

            txtFlight.Text = string.Format("{0} {1}", item.Departure.IATACode, item.FlightNumber);
            txtETA.Text = string.Format("ETA:{0:MMM-dd-yy HH:mm}", item.ArrivalTimeLocal);
//            txtPlace.Text = "GATE: " +item.GateNumber.ToString();
            txtPlace.Text = string.Format("GATE:{0}", item.ArrivalGate); 
            RowIcon.SetImageResource(Resource.Drawable.InProgress);

            try 
            {
                System.IO.Stream ims = context.Assets.Open(string.Format(@"Airlines/{0}.png",item.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            return view;
        }


    }
}


