﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{
    [Activity (ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]
    public class QAS_Admin_AddCart : BaseActivity
    {
        TextView titleLabel; 
        ImageView optionButton;
        ImageView backButton;
        ImageView imageHeader;
        TextView title;

        Button btnOk;
        Button btnAddAnother;
        Button btnCancel;
        EditText txtBarcode;
        EditText txtCartNumber;
        EditText txtTareWeight;
        EditText txtCartType;

        int typeId;
        Button options;
        List<SelectionItem> list = new List<SelectionItem>();



        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
 
            SetContentView (Resource.Layout.QAS_Admin_AddCart);

            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            titleLabel = FindViewById<TextView>(Resource.Id.HeaderText);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            imageHeader = FindViewById<ImageView>(Resource.Id.HeaderImage);
            options = FindViewById<Button> (Resource.Id.options);
            btnOk = FindViewById<Button> (Resource.Id.btnOk);
            btnAddAnother = FindViewById<Button> (Resource.Id.btnAddAnother);
            btnCancel = FindViewById<Button> (Resource.Id.btnCancel);
            txtBarcode = FindViewById<EditText> (Resource.Id.txtBarcode);
            txtCartNumber = FindViewById<EditText> (Resource.Id.txtCartNumber);
            txtTareWeight = FindViewById<EditText> (Resource.Id.txtTareWeight);
            txtCartType = FindViewById<EditText> (Resource.Id.txtCartType);
            txtCartType.Focusable = false;
            title = FindViewById<TextView>(Resource.Id.title);        

            txtBarcode.RequestFocus();
//            title.Text = "Add Cart";
            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name;
            txtCartType = FindViewById<EditText>(Resource.Id.txtCartType);

            GetCartTypes();
            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;
            btnCancel.Click += OnBackButton_Click;
            btnOk.Click += OnOkButton_Click;
            btnAddAnother.Click += btnAddAnotherButton_Click;
            options.Click += btnType_Click;
            txtCartType.Click += btnType_Click;

 
        }

        private void GetCartTypes()
        {
            QASCartTypeTasks types = MCH.Communication.QAS_Admin.Instance.GetCartType();
            if (types.Transaction.Status)
            {
                if (types.Data != null)
                {
                    list  = new List<SelectionItem>();

                    foreach (var n in types.Data)
                    {
                        list.Add(new SelectionItem(n.Name,n.Id,Resource.Drawable.ULD));
                      
                    }
                  list.Add(new SelectionItem("Other",0,Resource.Drawable.ULD));
                }


            }
        }

        private void btnType_Click(object sender, EventArgs e)
        {
            if (list.Count>0)
            {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity("Cart Types", this, list, SelectionListAdapter.SelectionMode.SingleSelection, true);
                dialogFragment.Cancelable = false;
                dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
                    {
                        foreach (var r in selection)
                        {
                            txtCartType.Text =r.Name;
                            typeId=int.Parse(r.Id.ToString());
                        }

                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }

        }

        private void OnOkButton_Click(object sender, EventArgs e)
        {
            GoNext("Save");
        }

        private void btnAddAnotherButton_Click(object sender, EventArgs e)
        {
            GoNext("SaveAnother");
        }

        bool existed = false;
        long cartId;
        private void GoNext(string type)
        {
            string barcode = txtBarcode.Text.Replace(" ","");
            string cartnumber = txtCartNumber.Text.Replace(" ","");

            try
            {              
                double tare= -1;
                if (barcode == string.Empty)
                {
                    MessageBox msg = new MessageBox(this);
                    msg.ShowAlert(GetText(Resource.String.Enter_Barcode), MessageBox.AlertType.Information);
                    txtBarcode.Text="";
                    txtBarcode.RequestFocus();
                    return;
                }
                else if(barcode.Length > 100)
                {
                    MessageBox msg = new MessageBox(this);
                    msg.ShowAlert(GetText(Resource.String.Enter_Exceed_Limit), MessageBox.AlertType.Information);
                    txtBarcode.RequestFocus();
                    return;
                }
                else if(cartnumber.Length > 100)
                {
                    MessageBox msg = new MessageBox(this);
                    msg.ShowAlert(GetText(Resource.String.Enter_Exceed_Limit), MessageBox.AlertType.Information);
                    txtCartNumber.RequestFocus();
                    return;
                }

                else if(txtTareWeight.Text != string.Empty && !DataTypeHelper.IsDouble(txtTareWeight.Text))
                {
                    MessageBox msg2 = new MessageBox(this);
                    msg2.ShowAlert(GetText(Resource.String.Enter_Correct_Weight), MessageBox.AlertType.Information);
                    txtTareWeight.RequestFocus();
                    return;
                }
                else if (txtTareWeight.Text != string.Empty)
                {
                    if(double.Parse(txtTareWeight.Text)< 1 || double.Parse(txtTareWeight.Text) > 9999999)
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(GetText(Resource.String.Weight_Not_Valid), MessageBox.AlertType.Information);
                        txtTareWeight.RequestFocus();
                        return;
                    }
                    else
                    {
                        tare = double.Parse(txtTareWeight.Text);
                    }

                }


                if(existed)
                {
                    
                    CommunicationTransaction t = MCH.Communication.QAS_Admin.Instance.UpdateCart(cartId,barcode,cartnumber, typeId, tare);
                    if (t.Status)
                    {
                        btnOk.Visibility = ViewStates.Invisible;
                        btnAddAnother.Visibility = ViewStates.Invisible;

                        MessageBox msg = new MessageBox(this);
                        msg.OnConfirmationClick+= (bool r) => 
                            {                                       
                                if (type == "Save")
                                {
                                    GotoMainMenu(); 
                                }
                                else
                                {
                                    StartActivity(typeof(QAS_Admin_AddCart));       
                                }                                 
                            };
                        msg.ShowAlert(string.Format("Cart# {0} has been updated.", txtBarcode.Text ),MessageBox.AlertType.Information);

                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }
                }
                else
                {
                    QASAddCartTask t;
                    t = MCH.Communication.QAS_Admin.Instance.AddNewCart(barcode,cartnumber, typeId, tare);
                    if (t.Transaction.Status)
                    {
                        btnOk.Visibility = ViewStates.Invisible;
                        btnAddAnother.Visibility = ViewStates.Invisible;

                        MessageBox msg = new MessageBox(this);
                        msg.OnConfirmationClick+= (bool r) => 
                            {                                       
                                if (type == "Save")
                                {
                                    GotoMainMenu(); 
                                }
                                else
                                {
                                    StartActivity(typeof(QAS_Admin_AddCart));       
                                }                                 
                            };
                        msg.ShowAlert(string.Format("Cart# {0} has been added.", txtBarcode.Text ),MessageBox.AlertType.Information);


                    }
                    else
                    {
                        if(t.Transaction.ErrorCode == "1005")
                        {
                            existed = true;
                            UpdateCart();
                        }
                        else
                        {
                            existed = false;
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Transaction.Error, MessageBox.AlertType.Error);
                        }

                    }
                }



            }
            catch
            {
            }
           
        }


        private void UpdateCart()
        {
            title.Text = "Edit Cart";
            MessageBox msg = new MessageBox(this);
            msg.ShowAlert(GetText(Resource.String.Confirm_CartExists), MessageBox.AlertType.Information);
            
            QASNewMasterCartTask t = MCH.Communication.QAS_PrepareCart.Instance.GetCart(txtBarcode.Text);
            if (t.Transaction.Status)
            {
                
                cartId = t.Data.Id;
                txtCartNumber.Text = t.Data.Number;
                if (t.Data.CartType != null)
                {
                    txtCartType.Text = t.Data.CartType.Name;
                    typeId = (int)t.Data.CartType.Id;
                }
                else
                {
                    txtCartType.Text = "";
                }
                txtTareWeight.Text = t.Data.TareWeight.ToString();
            }
            else
            {
                MessageBox msg1 = new MessageBox(this);
                msg1.ShowAlert(t.Transaction.Error, MessageBox.AlertType.Error);
            }
        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            GotoMainMenu();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();

             options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
               

            }

        }
    }
}

