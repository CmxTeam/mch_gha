﻿using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class QAS_PrepareCart_MainActivity : BaseActivity
    {


       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.QAS_PrepareCart_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();

        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
//            listView.ItemClick -= OnListItemClick;
            QAS_PrepareCart_SessionState.QASPrepareCartTask = QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data[e.Position];
            TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId.ToString()));
        }

        private void TaskSwitcher(enmCartStatus status)
        {
            
            switch (status)
            {
                case enmCartStatus.PENDING:
                    GoToTask_AddLocation( QAS_PrepareCart_SessionState.QASPrepareCartTask);
                break;

                case enmCartStatus.READY_TO_FILL:
                    GoToTask_CloseCart(QAS_PrepareCart_SessionState.QASPrepareCartTask);                   
                break;

                case enmCartStatus.CLOSED:
                    GoToTask_WeightCart();
                   break;

                case enmCartStatus.WEIGHED:
                    GoToTask_ReadyToStage();
                    break;

                case enmCartStatus.READY_FOR_PICKUP:
                    GoToTask_EnrouteToGate();
                    break;

                case enmCartStatus.IN_TRANSIT:
                    GoToTask_DropCart();
                    break;

                case enmCartStatus.NOT_LOADED_AT_GATE:
                    GoToTask_RetrieveCart();
                    break;
                case enmCartStatus.STAGED_NO_FLIGHT:
                    GoToTask_StagedNOFlight();
                    break;

                case enmCartStatus.STAGED_NO_GATE:
                    GoToTask_StagedNOGate();
                    break;
                case enmCartStatus.DROPPED:
                    GoToTask_DropOtherTask();
                    break;
                case enmCartStatus.NOT_LOADED_RETRIVED_BY_RUNNER:
                    GoToTask_NotLoade_RetrievedTask();

                    break;
                default:
                    GoToTask_Default();
                    break;
            }
        }

        private void TaskOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Remove:
                    GoToTask_RemoveCart();                   
                    break;

                case  OptionActions.Reopen:
                    GoToTask_AddLocation(QAS_PrepareCart_SessionState.QASPrepareCartTask);
                    break;

                case  OptionActions.Weight:
                   // GoToTask_WeightCart();
                    DoWeight();
                    break;

                case  OptionActions.Move:
                    GoToTask_MoveToStage();
                    break;
                case  OptionActions.DropAtGate:
                    GoToTask_DropCart();
                    break;
                case  OptionActions.DropAtStage:
                    GoToTask_DropOutboundStage();
                    break;
                case OptionActions.Info:
                    GoToTask_CartDetails();
                    break;
            }

        }



        private void GoToTask_Default()
        {
            List<OptionItem> taskOptions = new List<OptionItem>();
            taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
            taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

            Action<OptionItem> OptionClickAction = TaskOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void GoToTask_AddLocation(CartListItem task)
        {

 
            List<SelectionItem> list  = new List<SelectionItem>();
            Airports airport = QAS_PrepareCart.Instance.GetDestinations();
            if (airport.Transaction.Status)
            {
                if (airport.Data != null)
                {
                    foreach (var n in airport.Data)
                    {
                        list.Add(new SelectionItem(n.IATACode, n.AirportId, Resource.Drawable.Airport));
                    }
                }
            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(airport.Transaction.Error, MessageBox.AlertType.Error);
                return;
            }
 
            if (list.Count == 0)
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert("Unable to retrieve locations.", MessageBox.AlertType.Information);
                return;
            }

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Destinations", this, list, SelectionListAdapter.SelectionMode.SingleSelection, true);
            dialogFragment.Cancelable = false;
 
            dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
                {
                    foreach (var r in selection)
                    {
               
                        UpdateCartParams model = new UpdateCartParams();
                        model.CartId = task.CartId;
                        model.DestinationId = r.Id;
         
                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartDestination(model);
                        if (t.Status)
                        {
                            t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_TO_FILL,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());
                            if (t.Status)
                            {
                                MessageBox msg = new MessageBox(this);
                                msg.OnConfirmationClick+= (bool result) => 
                                    {                                        
                                       RefreshData(search.Text, false);                                        
                                    };
                                msg.ShowAlert(string.Format("{0} destination has been assigned to Cart# {1}.",r.Name,task.Reference ),MessageBox.AlertType.Information);

                                //Cart #<reference> Destination is set to <code>
                            }

   
                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }


                    }

                };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);


        }

        private void GoToTask_CloseCart(CartListItem task)
        {
            
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
 

                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.CLOSED,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());
                        if (t.Status)
                        {
        

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {                                    
                                 RefreshData(search.Text, false);                                   
                                };
                            msg.ShowAlert(string.Format("Cart# {0} has been closed.",  task.Reference),MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }
//                    listView.ItemClick += OnListItemClick;
                };
            m.OnNeutalClick += () =>
                {

                    List<OptionItem> taskOptions = new List<OptionItem>();
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

                    Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+ task.Reference);
                    dialogFragment.Cancelable = true;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                   
//                    listView.ItemClick += OnListItemClick;
                };

            m.ShowConfirmationMessage(task.Reference + " - "+ task.CartDestination+ "\n\n"+GetText(Resource.String.Confirm_Close_Cart),GetText(Resource.String.Yes), GetText(Resource.String.No),GetText(Resource.String.Other_Tasks));
        }

        private void GoToTask_RemoveCart() // change to deactivate cart
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {      
                        
                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.DeactivateCart(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());
                        if (t.Status)
                        {
 
                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {                                   
                                   RefreshData(search.Text, false);                                    
                                };
                            msg.ShowAlert(string.Format("Cart# {0} has been removed." , QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString())  ,MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };


            m.ShowConfirmationMessage(QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference+" - "+QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination+ "\n\n"+ GetText(Resource.String.Confirm_Remove),GetText(Resource.String.Yes), GetText(Resource.String.No));

        }


        private void DoWeight()
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new WeightDialog(this,ApplicationSessionState.SelectedMenuItem.Name.ToUpper(),"Enter Weight.",QAS_PrepareCart_SessionState.QASPrepareCartTask.GrossWeight);
//                var dialogFragment = new WeightDialog(this,ApplicationSessionState.SelectedMenuItem.Name.ToUpper(),"Enter Weight.",QAS_PrepareCart_SessionState.QASPrepareCartTask.Weight.ToString());

            dialogFragment.OkClicked+= (Double value) =>  
                {
                    if(value< 0.1 || value > 9999999)
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(GetText(Resource.String.Weight_Not_Valid), MessageBox.AlertType.Information);
                        return;
                    }
                    if(DataTypeHelper.IsDouble(value.ToString()))
                    {
                        UpdateCartParams model = new UpdateCartParams();
                        model.CartId = QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId;
                        model.Weight = value;

                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartWeight(model);
                        if (t.Status)
                        {

                            t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.WEIGHED,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());
                            if (t.Status)
                            {

                                MessageBox msg = new MessageBox(this);
                                msg.OnConfirmationClick+= (bool r) => 
                                    {                                       
                                       RefreshData(search.Text, false);                                       
                                    };
                                msg.ShowAlert(string.Format("Cart# {0} has been weighed.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString() ),MessageBox.AlertType.Information);



                            }
                            else
                            {
                                MessageBox msg = new MessageBox(this);
                                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                            }

                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    

                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(GetText(Resource.String.Invalid_Weight), MessageBox.AlertType.Error);
                    }

                };


            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }


        private void GoToTask_WeightCart()
        {
            MessageBox m1 = new MessageBox(this);
            m1.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                        DoWeight();

                    }

                };
            m1.OnNeutalClick += () =>
                {

                    List<OptionItem> taskOptions = new List<OptionItem>();
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

                    Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+ QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference);
                    dialogFragment.Cancelable = true;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                };

            m1.ShowConfirmationMessage(QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference+" - "+QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination+  "\n\n"+GetText(Resource.String.Confirm_Weight_Cart),GetText(Resource.String.Yes), GetText(Resource.String.No),GetText(Resource.String.Other_Tasks));

        }

        private void GoToTask_ReadyToStage()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
   

                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_FOR_PICKUP,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());

                        if (t.Status)
                        {
 

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {
                                    
                                   RefreshData(search.Text, false);

                                };
                            msg.ShowAlert(string.Format("Cart# {0} is ready to be brought to the gate.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };
            m.OnNeutalClick += () =>
                {

                    List<OptionItem> taskOptions = new List<OptionItem>();
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Weight_Cart),OptionActions.Weight, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

                    Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
                    dialogFragment.Cancelable = true;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                };

            m.ShowConfirmationMessage(QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference+" - "+QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination+  "\n\n"+ GetText(Resource.String.Confirm_Gate),GetText(Resource.String.Yes), GetText(Resource.String.No),GetText(Resource.String.Other_Tasks));


        }

        private void GoToTask_EnrouteToGate()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
 
                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.IN_TRANSIT,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());
                                              
                        if (t.Status)
                        {
 


                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {                                   
                                   RefreshData(search.Text, false);                                   
                                };
                            msg.ShowAlert(string.Format("Cart# {0} is in your possession now.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };
            m.OnNeutalClick += () =>
                {

                    List<OptionItem> taskOptions = new List<OptionItem>();
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Weight_Cart),OptionActions.Weight, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

                    Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
                    dialogFragment.Cancelable = true;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                };

            m.ShowConfirmationMessage(QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference+" - "+QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination+  "\n\n"+ GetText(Resource.String.Confirm_Bring_To_Gate),GetText(Resource.String.Yes), GetText(Resource.String.No),GetText(Resource.String.Other_Tasks));


        }

        private void GoToTask_DropOutboundStage()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_FOR_PICKUP,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());

                        if (t.Status)
                        {



                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {
                                    
                                  RefreshData(search.Text, false);
                                   
                                };
                            msg.ShowAlert(string.Format("Cart# {0} is dropped in stage.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };
            m.ShowConfirmationMessage(QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference+" - "+QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination+  "\n\n"+ GetText(Resource.String.Confirm_Drop_Stage),GetText(Resource.String.Yes), GetText(Resource.String.No));

        }

        private void GoToTask_DropCart()
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.DropCartAtGate(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,(decimal)ApplicationSessionState.Latitude,(decimal)ApplicationSessionState.Longitude,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {


                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {

                                    RefreshData(search.Text, false);

                                };
                            msg.ShowAlert(string.Format("Cart# {0} has been dropped.",QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);

                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };
            m.OnNeutalClick += () =>
                {

                    List<OptionItem> taskOptions = new List<OptionItem>();
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Weight_Cart),OptionActions.Weight, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Move_to_stage),OptionActions.Move, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                    taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));


                    Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
                    dialogFragment.Cancelable = true;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                };

            m.ShowConfirmationMessage(QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference+" - "+QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination+  "\n\n"+ GetText(Resource.String.Confirm_Drop_Gate),GetText(Resource.String.Yes), GetText(Resource.String.No),GetText(Resource.String.Other_Tasks));


        }
 
        private void GoToTask_StagedNOFlight()
        {

                        List<OptionItem> taskOptions = new List<OptionItem>();
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Weight_Cart),OptionActions.Weight, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Move_to_stage),OptionActions.Move, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));
    
                        Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                        var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
                        dialogFragment.Cancelable = true;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }

        private void GoToTask_StagedNOGate()
        {

                        List<OptionItem> taskOptions = new List<OptionItem>();
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Weight_Cart),OptionActions.Weight, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Move_to_stage),OptionActions.Move, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

                        Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                        var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
                        dialogFragment.Cancelable = true;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }

        private void GoToTask_NotLoade_RetrievedTask()
        {

                        List<OptionItem> taskOptions = new List<OptionItem>();
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_Cart_at_Gate),OptionActions.DropAtGate, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Drop_Cart_at_Stage),OptionActions.DropAtStage, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

                        Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                        var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
                        dialogFragment.Cancelable = true;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }

        private void GoToTask_DropOtherTask()
        {

                        List<OptionItem> taskOptions = new List<OptionItem>();
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Reopen_Cart),OptionActions.Reopen, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Weight_Cart),OptionActions.Weight, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Move_to_stage),OptionActions.Move, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Remove_Cart),OptionActions.Remove, Resource.Drawable.Menu));
                        taskOptions.Add(new OptionItem(GetText(Resource.String.Details),OptionActions.Info, Resource.Drawable.Menu));

                        Action<OptionItem> OptionClickAction = TaskOptionClickAction;
                        var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, taskOptions, OptionClickAction,"Cart# "+QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString());
                        dialogFragment.Cancelable = true;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }

        private void GoToTask_RetrieveCart()  
        {
            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
  

                        CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.NOT_LOADED_RETRIVED_BY_RUNNER,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());
                        if (t.Status)
                        {
   

                            MessageBox msg = new MessageBox(this);
                            msg.OnConfirmationClick+= (bool r) => 
                                {
                                   
                                  RefreshData(search.Text, false);
                                   
                                };
                            msg.ShowAlert(string.Format("Cart# {0} has been retrieved.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);


                        }
                        else
                        {
                            MessageBox msg = new MessageBox(this);
                            msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }

                };


            m.ShowConfirmationMessage(QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference+" - "+QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination+  "\n\n"+ GetText(Resource.String.Confirm_Leave_Cart),GetText(Resource.String.Yes), GetText(Resource.String.No));


        }


       

        private void GoToTask_MoveToStage()
        {
 

            CommunicationTransaction t = MCH.Communication.QAS_PrepareCart.Instance.UpdateCartStatus(QAS_PrepareCart_SessionState.QASPrepareCartTask.CartId,enmCartStatus.READY_FOR_PICKUP ,ApplicationSessionState.User.Data.UserId,ApplicationSessionState.DeviceInfo.Data.MAC,GetGPSData());
            if (t.Status)
            {
 

                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick+= (bool r) => 
                    {
                        
                      RefreshData(search.Text, false);
                       
                    };
                msg.ShowAlert(string.Format("Cart# {0} has been moved to stage.", QAS_PrepareCart_SessionState.QASPrepareCartTask.Reference.ToString()),MessageBox.AlertType.Information);



            }
            else
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }         

        }
        private void GoToTask_CartDetails()
        {

            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new QAS_CartDetailDialog(this,"Cart Details");

            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);


        }



        private void LoadtitleLabel(enmCartStatus status)
        {
            switch (status)
            {
                case enmCartStatus.READY_TO_FILL:
                    titleLabel.Text =GetText(Resource.String.Close_Cart).ToUpper() + string.Format(" - ({0})", QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count);
                    break;
                case enmCartStatus.CLOSED:
                    titleLabel.Text = GetText(Resource.String.Weight_Cart).ToUpper() + string.Format(" - ({0})", QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count);
                    break;    
                case enmCartStatus.WEIGHED:
                    titleLabel.Text = GetText(Resource.String.Move_to_stage).ToUpper() + string.Format(" - ({0})", QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count);
                    break;  
                case enmCartStatus.READY_FOR_PICKUP:
                    titleLabel.Text = GetText(Resource.String.Pickup_Cart_from_Stage).ToUpper()  + string.Format(" - ({0})", QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count);
                    break;  
                case enmCartStatus.IN_TRANSIT:
                    titleLabel.Text =  GetText(Resource.String.Drop_Cart_at_Gate).ToUpper() + string.Format(" - ({0})", QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count);
                    break;  
                case enmCartStatus.NOT_LOADED_AT_GATE:
                    titleLabel.Text = GetText(Resource.String.Retrieve_Cart_from_Gate).ToUpper() + string.Format(" - ({0})", QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count);
                    break;  
                default:
                    titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper() + string.Format(" - ({0})",QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count);
                    break;
            }

        }


        private void RefreshData(string searchData,bool isBarcode)
        {
            if(searchData.Length >100)
            {
                MessageBox msg = new MessageBox(this);
                msg.ShowAlert(GetText(Resource.String.Enter_Exceed_Limit), MessageBox.AlertType.Information);
                return;
            }

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
 
                   enmCartStatus StatusId =    EnumHelper.GetEnumItem<enmCartStatus>(DropDownText.Text);
                    GetCartsParam parameters = new GetCartsParam();
                    parameters.CartDirection = null;
                    parameters.StatusIds = new long[1];

                    if(!isBarcode)
                    {
                        parameters.StatusIds[0] = (long)StatusId;
                    }
                    else
                    {
                        parameters.StatusIds[0] = (long)enmCartStatus.ALL;
                    }
                  
                   
                    QAS_PrepareCart_SessionState.QASPrepareCartTasks = MCH.Communication.QAS_PrepareCart.Instance.GetActiveCarts(parameters);
               
                    RunOnUiThread (delegate {
                    
                    if(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Transaction.Status)
                    {
                        if(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data!=null)
                        {
                           QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data =  LinqHelper.Query<CartListItem>( QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data,searchData);
                            LoadtitleLabel(StatusId);
                            listView.Adapter = new QAS_PrepareCart_MainManuAdapter(this, QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;   

                        }
                    }
                    else
                    {
                        MessageBox msg = new MessageBox(this);
                        msg.ShowAlert(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Transaction.Error, MessageBox.AlertType.Error);
                    }
          
                    if(isBarcode)
                    {    

                        QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data =  LinqHelper.Query<CartListItem>(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data,searchData);
                            if(QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data!=null && QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data.Count==1)
                         {
                                QAS_PrepareCart_SessionState.QASPrepareCartTask = QAS_PrepareCart_SessionState.QASPrepareCartTasks.Data[0];
                                TaskSwitcher(EnumHelper.GetEnumItem<enmCartStatus>(QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId.ToString()));   
                         }
                            else
                            {
                                try
                                {
                                    
                                    PrepareCartParam model = new PrepareCartParam();
                                    model.Barcode = searchData;
                                    model.ClientDateTime = DateTime.Now;
                                    model.UserId = ApplicationSessionState.User.Data.UserId;

                                    QASPrepareCartTask task=MCH.Communication.QAS_PrepareCart.Instance.PrepareNewCart(model);  
                                    if(task.Transaction.Status)
                                    {
                                        if(task.Data!=null)
                                        {
                                            QAS_PrepareCart_SessionState.QASPrepareCartTask =  task.Data;
                                            EditTextListener.Text = string.Empty;
                                            search.RequestFocus ();
                                            TaskSwitcher(task.Data.StatusId);
                                            RefreshData(string.Empty,false);
                                        }
                                        else
                                        {
                                            //do nothign
                                        }
                                    }
                                    else
                                    {
                                        RunOnUiThread(() => progressDialog.Hide());
                                        //show msg ...... task.Transaction.Error

                                        MessageBox m = new MessageBox(this);
                                        m.ShowAlert(task.Transaction.Error,MessageBox.AlertType.Error);
                                        return;
                                    }
                                    return;
                                }
                                catch(Exception ex)
                                {
                                    MessageBox m = new MessageBox(this);
                                    m.ShowAlert(ex.Message,MessageBox.AlertType.Error);
                                }
                            }


    
                    }
                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

       

    }
}

