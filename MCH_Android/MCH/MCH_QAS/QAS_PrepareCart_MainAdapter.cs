﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class QAS_PrepareCart_MainManuAdapter : BaseAdapter<CartListItem> {

        List<CartListItem> items;
        Activity context;
        


        public QAS_PrepareCart_MainManuAdapter(Activity context, List<CartListItem> items  ): base()
        {
 
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CartListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.QAS_PrepareCart_MainRow, null);

            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            TextView txtCart = view.FindViewById<TextView>(Resource.Id.txtCart);
            TextView txtFlightInfo = view.FindViewById<TextView>(Resource.Id.txtFlightInfo);

 
            txtCart.Text = string.Format("{0} {1} ", item.Reference, item.CartDestination);

           
            if (item.OutboundFlight != null)
            {
                txtFlightInfo.Text = string.Format("{0} {1} {2:MMM-dd-yy HH:mm} {3}", item.RouteDestination, item.OutboundFlight.FlightNumber, item.OutboundFlight.STD,item.OutboundFlight.DepartureGate);
            }
            else
            {
                txtFlightInfo.Text = string.Format("{0} {1}", item.RouteDestination, " Unassigned");
            }
           

//            try 
//            {
//                System.IO.Stream ims = context.Assets.Open(string.Format(@"Icons/{0}.png",item.IconKey));
//                Drawable d = Drawable.CreateFromStream(ims, null);
//                pic.SetImageDrawable(d);
//            } 
//            catch 
//            {
//                pic.SetImageResource (Resource.Drawable.Clock);
//            }


            //todo  need to change to correct status images
            switch (item.StatusId)
            {
                case enmCartStatus.PENDING:
                    RowIcon.SetImageResource(Resource.Drawable.Pending);
                    break;
                case enmCartStatus.READY_TO_FILL:
                    RowIcon.SetImageResource (Resource.Drawable.Check);
                    break;
                case enmCartStatus.CLOSED:
                    RowIcon.SetImageResource (Resource.Drawable.Cube);
                    break;    
                case enmCartStatus.WEIGHED:
                    RowIcon.SetImageResource (Resource.Drawable.Filing);
                    break;  
                case enmCartStatus.READY_FOR_PICKUP:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;  
                case enmCartStatus.IN_TRANSIT:
                    RowIcon.SetImageResource (Resource.Drawable.Plane);
                    break; 
                case enmCartStatus.NOT_LOADED_AT_GATE:
                    RowIcon.SetImageResource (Resource.Drawable.Empty);
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }
//
            if (item.StatusId != enmCartStatus.READY_TO_FILL && item.StatusId != enmCartStatus.PENDING)
            {
                if (item.OutboundFlight != null && item.OutboundFlight.FlightNumber == string.Empty)
                {
                    view.SetBackgroundColor(Android.Graphics.Color.LightCyan);

                }
                //else if (item.ETD.AddHours(1) > DateTime.Now && item.ETD < DateTime.Now.AddHours(1))
                else if (item.OutboundFlight != null && item.OutboundFlight.STD < DateTime.Now.AddHours(1))
                {
                    view.SetBackgroundColor(Android.Graphics.Color.LightPink);
                }

            }
           

            return view;
        }


    }
}


