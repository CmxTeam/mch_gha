﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.Res;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;

namespace MCH
{




    public class QAS_LocationListAdapter : BaseAdapter<LocationItem> {

        List<LocationItem> items;
        Activity context;

        public QAS_LocationListAdapter(Activity context, List<LocationItem> items): base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override LocationItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
 
                if (view == null) // no view to re-use, create new
                {
                view = context.LayoutInflater.Inflate(Resource.Layout.LocationRow, null);
                }

            view.FindViewById<TextView>(Resource.Id.LocationName).Text = item.Location.ToUpper();
       


            ImageView pic = view.FindViewById<ImageView>(Resource.Id.LocationIcon);

            string icon = string.Empty;
            switch (item.LocationPrefix)
            {
                case "A":
                case "B":
                case "C":
                case "R":
                    icon = "Bin";
                    break;
                case "D":
                    icon = "Door";
                    break;
                case "T":
                    icon = "Truck";
                    break;
                case "S":
                    icon = "Inspection";
                    break;
                default:
                    icon = string.Empty;
                    break;
            }


            if (icon != string.Empty)
            {
                try 
                {
                    System.IO.Stream ims = context.Assets.Open(string.Format(@"Locations/{0}.png",icon));
                    Drawable d = Drawable.CreateFromStream(ims, null);
                    pic.SetImageDrawable(d);
                } 
                catch 
                {
                    pic.SetImageResource (Resource.Drawable.Menu);
                }
            }

 

     
            pic.Tag = item.LocationId.ToString();
            return view;
        }
 


    }
}


