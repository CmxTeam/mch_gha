﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class QAS_NewCartDialog: DialogFragment
    {
        TextView textViewMessage;
        Button cancel;
        Button ok;
        EditText txtCartNumber;
        EditTextEventListener EditTextListener;
        //        //Action<SnapShotTaskItem> okClickAction;
        Activity context;
        string title;
        string message;
        ImageButton ClearSearch;
        EditText txtCartType;
        string barcode;
        int typeId =0;
        Button options;
        List<SelectionItem> list = new List<SelectionItem>();

        public delegate void OkClickActionEventHandler();
        public event OkClickActionEventHandler OkClicked;

        public delegate void CancelClickActionEventHandler();
        public event CancelClickActionEventHandler CancelClicked;

        public QAS_NewCartDialog (Activity context,string title ,string message, string barcode )
        {

            this.title = title.ToUpper();
            this.message = message.ToUpper();
            this.context = context;
            this.barcode = barcode;
        }

 

        //ADD this for error when rotating
        public QAS_NewCartDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.QAS_NewCartDialog, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            ok = DialogInstance.FindViewById<Button>(Resource.Id.ok); 

            txtCartNumber = DialogInstance.FindViewById<EditText>(Resource.Id.txtCartNumber);
            txtCartType = DialogInstance.FindViewById<EditText>(Resource.Id.txtCartType);
            txtCartType.Focusable = false;
            options = DialogInstance.FindViewById<Button> (Resource.Id.options);
            textViewMessage = DialogInstance.FindViewById<TextView>(Resource.Id.message);
            textViewMessage.Text = this.message;
           

            cancel =DialogInstance.FindViewById<Button>(Resource.Id.cancel);
            cancel.Click += OnCancelClick;

//            EditTextListener = new EditTextEventListener(txtCartNumber);
//            EditTextListener.OnEnterEvent += OnEnterEvent;
            GetCartTypes();
            options.Click += btnType_Click;
            txtCartType.Click += btnType_Click;

            ok.Click += OnOk_Click;


            ClearSearch = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearSearchButton);
            ClearSearch.Click += OnClearSearch_Click;

            QAS_PrepareCart_SessionState.PrepareNewCartNumber = null;
            QAS_PrepareCart_SessionState.PrepareNewCartTypeId= 0;

            QAS_PrepareCart_SessionState.QASNewMasterCartTask = MCH.Communication.QAS_PrepareCart.Instance.GetCart(barcode);

            if (QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data != null)
                {
               

                    txtCartNumber.Text = QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data.Number;
                if (QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data.CartType != null)
                {
                    txtCartType.Text = QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data.CartType.Name.ToString();
                    typeId = (int)QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data.CartType.Id;
                }
                else
                {
                    txtCartType.Text = "";
                }


                }
                else
                {
                    txtCartNumber.Text = "";
                    txtCartType.Text = "";
                }

//            QAS_PrepareCart_SessionState.QASNewMasterCartTask = MCH.Communication.QAS_PrepareCart.Instance.GetCart(barcode);
//            if (QAS_PrepareCart_SessionState.QASNewMasterCartTask.Transaction.Status)
//            {
//                if (QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data != null)
//                {
//                    txtCartNumber.Text = QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data.Number;
//                    txtCartType.Text = QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data.CartTypeId.ToString();
//                }
//                else
//                {
//                    txtCartNumber.Text = "";
//                    txtCartType.Text = "";
//                }
//            }
//            else
//            {
//                MessageBox msg = new MessageBox(Activity);
//                msg.ShowAlert(QAS_PrepareCart_SessionState.QASNewMasterCartTask.Transaction.Error, MessageBox.AlertType.Error);
//            }

            return DialogInstance;
        }

        private void GetCartTypes()
        {
            QASCartTypeTasks types = MCH.Communication.QAS_Admin.Instance.GetCartType();
            if (types.Transaction.Status)
            {
                if (types.Data != null)
                {
                    list  = new List<SelectionItem>();

                    foreach (var n in types.Data)
                    {
                        list.Add(new SelectionItem(n.Name,n.Id,Resource.Drawable.ULD));

                    }
                    list.Add(new SelectionItem("Other",0,Resource.Drawable.ULD));
                }


            }
        }

        private void btnType_Click(object sender, EventArgs e)
        {
            if (list.Count>0)
            {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity("Cart Types", Activity, list, SelectionListAdapter.SelectionMode.SingleSelection, true);
                dialogFragment.Cancelable = false;
                dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
                    {
                        foreach (var r in selection)
                        {
                            txtCartType.Text =r.Name;
                            typeId=int.Parse(r.Id.ToString());
                        }

                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }

        }

        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            txtCartNumber.Text = string.Empty;
            txtCartNumber.RequestFocus ();
        }

        private void DoOKWork()
        {
            if (typeId != 0)
            {
                QAS_PrepareCart_SessionState.PrepareNewCartTypeId = typeId;
            }

            if (txtCartNumber.Text.Length > 45)
            {
                MessageBox msg = new MessageBox(context);
                msg.ShowAlert(GetText(Resource.String.Enter_Exceed_Limit), MessageBox.AlertType.Error);
                return;
            }


            if (txtCartNumber.Text != string.Empty)
            {
                QAS_PrepareCart_SessionState.PrepareNewCartNumber = txtCartNumber.Text;

            }

            if (QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data != null)
            {
                CommunicationTransaction t = MCH.Communication.QAS_Admin.Instance.UpdateCart(QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data.Id,barcode,QAS_PrepareCart_SessionState.PrepareNewCartNumber, typeId, QAS_PrepareCart_SessionState.QASNewMasterCartTask.Data.TareWeight);
                if (!t.Status)
                {
                    MessageBox msg = new MessageBox(Activity);
                    msg.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    return;
                }
            }


            if(OkClicked!=null)
            {
                OkClicked.Invoke();
            }

            Dismiss();
        }

        private void OnOk_Click(object sender, EventArgs e)
        {
            DoOKWork();

        }

        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            DoOKWork();
        }


        void OnCancelClick(object sender, EventArgs e)
        {

            if(CancelClicked!=null)
            {
                CancelClicked();
                CancelClicked = null;
            }
            Dismiss();
          
        }



    }
}



