﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class QAS_CartActionDialog: DialogFragment
    {
        TextView txtCartNumber;
        TextView txtBarcode;
        TextView txtRoute;
        TextView txtLocation;
        TextView txtStatus;
        TextView txtOutboundFlight;
        TextView txtInboundFlight;
        Button btnMain;
        Button btnOtherTask;
        Button btnDetail;
        Button btnCancel;
        Activity context;
        string title;
       

        public delegate void OkClickActionEventHandler(DateTime  date);
//        public event OkClickActionEventHandler OkClicked;

        public QAS_CartActionDialog (Activity context, string title)
        {

            this.title = title.ToUpper();
//            this.message = message.ToUpper();
            this.context = context;
           
        }

        public delegate void ActionClickEventHandler(CartActionResultType result);
        public event ActionClickEventHandler OnActionClick;

        public enum CartActionResultType
        {
            Task = 1,
            OtherTask = 2,
            Detail = 3,
        }


        //ADD this for error when rotating
        public QAS_CartActionDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.QAS_CartAction, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);
 
            txtCartNumber = DialogInstance.FindViewById<TextView>(Resource.Id.txtCartNumber);
            txtBarcode = DialogInstance.FindViewById<TextView>(Resource.Id.txtBarcode);
            txtRoute = DialogInstance.FindViewById<TextView>(Resource.Id.txtRoute);
            txtLocation = DialogInstance.FindViewById<TextView>(Resource.Id.txtLocation);
            txtStatus = DialogInstance.FindViewById<TextView>(Resource.Id.txtStatus);
            txtOutboundFlight = DialogInstance.FindViewById<TextView>(Resource.Id.txtOutboundFlight);
            txtInboundFlight = DialogInstance.FindViewById<TextView>(Resource.Id.txtInboundFlight);

            btnMain = DialogInstance.FindViewById<Button>(Resource.Id.btnMain); 
            btnOtherTask = DialogInstance.FindViewById<Button>(Resource.Id.btnOtherTask); 
            btnDetail = DialogInstance.FindViewById<Button>(Resource.Id.btnDetail); 
            btnCancel = DialogInstance.FindViewById<Button>(Resource.Id.btnCancel); 

            btnMain.Click += btnMain_Click;
            btnCancel.Click += btnCancel_Click;
            btnDetail.Click += btnDetail_Click;
            btnOtherTask.Click += btnOtherTask_Click;

            CartDetails();
            DisplayMainBtn();

            return DialogInstance;
        }

        private void DisplayMainBtn()
        {
            if (QAS_PrepareCart_SessionState.QASPrepareCartTask != null)
            {
                
                switch (QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId)
                {
                    //outbound actions
                    case enmCartStatus.PENDING:
                        btnMain.Text = "Assign Destination";
                        break;

                    case enmCartStatus.READY_TO_FILL:
                    case enmCartStatus.CLOSED:
                        btnMain.Text = "Weigh Cart";  
                        break;

                    case enmCartStatus.WEIGHED:
                        btnMain.Text = "Stage Cart";  
                        break;

                   

                    case enmCartStatus.IN_TRANSIT:
                        btnMain.Text = "Drop Cart​";  
                        break;

                    case enmCartStatus.DROPPED:
                    case enmCartStatus.STAGED_NO_FLIGHT:
                    case enmCartStatus.STAGED_NO_GATE:
                    case enmCartStatus.READY_FOR_PICKUP:
                        btnMain.Text = "Pick up Cart​"; 
                        break;

                    
                    case enmCartStatus.CONFIRMED_LOADED:
                    case enmCartStatus.DEPARTED:
                    case enmCartStatus.NOT_LOADED_AT_GATE:
                    case enmCartStatus.DROPPED_AT_XFER_GATE:
                        btnMain.Text = "Retrieve cart​​"; 
                        break;

                    case enmCartStatus.NOT_LOADED_RETRIVED_BY_RUNNER:
                    case enmCartStatus.RETRIEVED_BY_RUNNER:
                        btnMain.Text = "Drop at stage​​"; 
                        break;

                    case enmCartStatus.DROPPED_AT_INBOUND_STAGE:
                    case enmCartStatus.EMPTIED:
                        btnMain.Text = "Empty Cart​​"; 
                        break;

                    case enmCartStatus.DEACTIVATED:
                        btnMain.Text = "Remove Cart​​"; 
                        break;

                    case enmCartStatus.INBOUND_INFLIGHT:
                    case enmCartStatus.InboundNewCart:
                        btnMain.Text = "Pickup from flight​​"; 
                        break;

                    default:
                        btnMain.Text = "Default​​"; 
                        break;
                }
                    
            }
        }


        private void btnMain_Click(object sender, EventArgs e)
        {
            if(OnActionClick!=null)
            {
                OnActionClick(CartActionResultType.Task);
                OnActionClick = null;
            }
            Dismiss();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dismiss();  
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
            if(OnActionClick!=null)
            {
                OnActionClick(CartActionResultType.Detail);
                OnActionClick = null;
            }
            Dismiss();
        }

        private void btnOtherTask_Click(object sender, EventArgs e)
        {
            if(OnActionClick!=null)
            {
                OnActionClick(CartActionResultType.OtherTask);
                OnActionClick = null;
            }
            Dismiss();
        }


        public override void OnActivityCreated( Bundle savedInstanceState)
        {

            Dialog.Window.SetTitle( this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;

        }



        private void CartDetails()
        {

                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.CartNumber != null)
                {
                    txtCartNumber.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.CartNumber;
                }               
                txtStatus.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId.ToString().Replace("_"," ");
                txtBarcode.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.Cartbarcode;
                txtRoute.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination;
                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.WarehouseLocation != null)
                {
                    txtLocation.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.WarehouseLocation.Location;
                }
                
                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight != null)
                {
                    txtOutboundFlight.Text = string.Format("{0}{1} {2}-{3}\n{4:HH:mm}-{5:HH:mm}", QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.CarrierCode, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.FlightNumber, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.FlightOrigin, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.FlightDestination, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.STD,QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.STA);

                }
                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight != null)
                {
//                string flighttime = string.Format("{0:HH:mm}-{1:HH:mm}", QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.STD, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.STA);
//                txtInboundFlight.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.CarrierCode+""+ QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightNumber+" "+QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightOrigin+"-"+ QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightDestination+"\n"+ flighttime;

                  txtInboundFlight.Text = string.Format("{0}{1} {2}-{3}\n{4:HH:mm}-{5:HH:mm}", QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.CarrierCode, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightNumber, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightOrigin, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightDestination, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.STD,QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.STA);

                }

        }



    }
}




