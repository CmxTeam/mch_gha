﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class QAS_CartDetailDialog: DialogFragment
    {
        TextView txtCartNumber;
        TextView txtBarcode;
        TextView txtDestination;
        TextView txtCreateDate;
        TextView txtMailWeight;
        TextView txtTareWeight;
        TextView txtStatus;
        TextView txtOutboundFlight;
        TextView txtInboundFlight;
        Button btnOk;
        Activity context;
        string title;
       
//        enmCartDirections dir = enmCartDirections.OUTBOUND;
      

        public delegate void OkClickActionEventHandler(DateTime  date);
//        public event OkClickActionEventHandler OkClicked;

        public QAS_CartDetailDialog (Activity context, string title)
        {

            this.title = title.ToUpper();
//            this.message = message.ToUpper();
            this.context = context;

        }




        //ADD this for error when rotating
        public QAS_CartDetailDialog()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.QAS_CartDetail, container, false);
            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);
 
            txtCartNumber = DialogInstance.FindViewById<TextView>(Resource.Id.txtCartNumber);
            txtBarcode = DialogInstance.FindViewById<TextView>(Resource.Id.txtBarcode);
            txtDestination = DialogInstance.FindViewById<TextView>(Resource.Id.txtDestination);
            txtCreateDate = DialogInstance.FindViewById<TextView>(Resource.Id.txtCreateDate);
            txtMailWeight = DialogInstance.FindViewById<TextView>(Resource.Id.txtMailWeight);
            txtTareWeight = DialogInstance.FindViewById<TextView>(Resource.Id.txtTareWeight);
            txtStatus = DialogInstance.FindViewById<TextView>(Resource.Id.txtStatus);
            txtOutboundFlight = DialogInstance.FindViewById<TextView>(Resource.Id.txtOutboundFlight);
            txtInboundFlight = DialogInstance.FindViewById<TextView>(Resource.Id.txtInboundFlight);

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 
 
            btnOk.Click += OnOk_Click;

            CartDetails();

            return DialogInstance;
        }


      
        private void OnOk_Click(object sender, EventArgs e)
        {
            Dismiss();  
        }
 

        public override void OnActivityCreated( Bundle savedInstanceState)
        {

            Dialog.Window.SetTitle( this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;

        }



        private void CartDetails()
        {
            
                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.CartNumber != null)
                {
                    txtCartNumber.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.CartNumber;
                }               
                txtStatus.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.StatusId.ToString().Replace("_", " ");
                txtBarcode.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.Cartbarcode;
                txtDestination.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.CartDestination;
                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.RegisteredTimestamp.Year > 2000)
                {
                    txtCreateDate.Text = string.Format("{0:MM/dd/yyyy}", QAS_PrepareCart_SessionState.QASPrepareCartTask.RegisteredTimestamp);

                }
                
           
            if (QAS_PrepareCart_SessionState.QASPrepareCartTask.GrossWeight != null && QAS_PrepareCart_SessionState.QASPrepareCartTask.DollyWeight != null && QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight != null)
                {
                txtMailWeight.Text = (QAS_PrepareCart_SessionState.QASPrepareCartTask.GrossWeight - QAS_PrepareCart_SessionState.QASPrepareCartTask.DollyWeight - QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight).ToString();
                }
                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight != null)
                {
                    txtTareWeight.Text = QAS_PrepareCart_SessionState.QASPrepareCartTask.TareWeight.ToString();
                }


                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight != null && QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.FlightNumber != string.Empty)
                {
                    txtOutboundFlight.Text = string.Format("{0}{1} {2}-{3}\nDep: {4:MMM-dd HH:mm}\n         Gate: {5}\nArr:  {6:MMM-dd HH:mm}\n         Gate: {7}", QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.CarrierCode, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.FlightNumber, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.FlightOrigin, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.FlightDestination, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.STD, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.DepartureGate, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.STA, QAS_PrepareCart_SessionState.QASPrepareCartTask.OutboundFlight.ArrivalGate);

                }

                if (QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight != null && QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightNumber != string.Empty)
                {
                    txtInboundFlight.Text = string.Format("{0}{1} {2}-{3}\nDep: {4:MMM-dd HH:mm}\n         Gate: {5}\nArr:  {6:MMM-dd HH:mm}\n         Gate: {7}", QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.CarrierCode, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightNumber, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightOrigin, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.FlightDestination, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.STD, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.DepartureGate, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.STA, QAS_PrepareCart_SessionState.QASPrepareCartTask.InboundFlight.ArrivalGate);

                }


        }

    }
}




