﻿
using System;
using System.Collections.Generic;
namespace MCH
{
    public class QAS_PickupFlight_SessionState
    {
        private QAS_PickupFlight_SessionState()
        {
        }


        ~QAS_PickupFlight_SessionState()  // destructor
        {
 
        }
 
        public static MCH.Communication.QASPickupFlightTasks QASPickupFlightTasks;
        public static MCH.Communication.FlightListItem QASPickupFlightTask;
        public static List<string> BarcodeList;
        public static string FlightCartNextActivity;
    }
}
 