﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class QAS_Cart_Adapter : BaseAdapter<CartListItem> {

        List<CartListItem> items;
        Activity context;
        


        public QAS_Cart_Adapter(Activity context, List<CartListItem> items  ): base()
        {
 
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CartListItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.QAS_Cart_Row, null);

            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            TextView txtCart = view.FindViewById<TextView>(Resource.Id.txtCart);
            TextView txtFlightInfo = view.FindViewById<TextView>(Resource.Id.txtFlightInfo);
            TextView txtStatus = view.FindViewById<TextView>(Resource.Id.txtStatus);
 
            txtCart.Text = string.Format("{0} {1} ", item.Cartbarcode,item.CartDestination);

            if (item.WarehouseLocation == null)
            {
                txtStatus.Text = "Status: " + item.StatusId.ToString().Replace("_", " ");
            }
            else
            {
                txtStatus.Text = string.Format("Status: {0} ({1})", item.StatusId.ToString().Replace("_", " "), item.WarehouseLocation.Location);
            }
           

            if (item.OutboundFlight != null)
            {
                txtFlightInfo.Text = string.Format("{0}{1} {2}-{3} {4:MMM-dd HH:mm}", item.OutboundFlight.CarrierCode, item.OutboundFlight.FlightNumber, item.OutboundFlight.FlightOrigin, item.OutboundFlight.FlightDestination, item.OutboundFlight.STD);

            }
            else if (item.InboundFlight != null)
            {
                txtFlightInfo.Text = string.Format("{0}{1} {2}-{3} {4:MMM-dd HH:mm}", item.InboundFlight.CarrierCode, item.InboundFlight.FlightNumber, item.InboundFlight.FlightOrigin, item.InboundFlight.FlightDestination, item.InboundFlight.STA);

            }
            else
            {
                txtFlightInfo.Text = "Unassigned";
            }
           
            //todo  need to change to correct status images
            switch (item.StatusId)
            {
                case enmCartStatus.PENDING:
                    RowIcon.SetImageResource(Resource.Drawable.Pending);
                    break;
                case enmCartStatus.READY_TO_FILL:
                    RowIcon.SetImageResource (Resource.Drawable.Check);
                    break;
                case enmCartStatus.CLOSED:
                    RowIcon.SetImageResource (Resource.Drawable.Cube);
                    break;    
                case enmCartStatus.WEIGHED:
                    RowIcon.SetImageResource (Resource.Drawable.Filing);
                    break;  
                case enmCartStatus.READY_FOR_PICKUP:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;  
                case enmCartStatus.IN_TRANSIT:
                    RowIcon.SetImageResource (Resource.Drawable.Plane);
                    break; 
                case enmCartStatus.NOT_LOADED_AT_GATE:
                    RowIcon.SetImageResource (Resource.Drawable.Empty);
                    break;

                case enmCartStatus.RETRIEVED_BY_RUNNER:
                    RowIcon.SetImageResource (Resource.Drawable.Unload);
                    break;
                case enmCartStatus.DROPPED_AT_INBOUND_STAGE:
                    RowIcon.SetImageResource (Resource.Drawable.Remove);
                    break;

                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            // background highlight will need else for all possibility, Otherwise it will be crazy
            if (item.OutboundFlight != null && item.OutboundFlight.FlightNumber == string.Empty)
            {
                view.SetBackgroundColor(Android.Graphics.Color.LightCyan);

            }
            else if (item.OutboundFlight != null && item.OutboundFlight.STD != null)
            {
//                    DateTimeOffset now = DateTimeOffset.Parse("2015-09-23T05:00:30");

                DateTime UTCnow =  DateTime.UtcNow;
                DateTime stdUTC = (DateTime)item.OutboundFlight.STDUtc;


                // STD closer to 90 mins or passed
                if ((stdUTC - UTCnow).TotalMinutes < 91)
                {
                    view.SetBackgroundColor(Android.Graphics.Color.LightPink);
                }
                else
                {
                    view.SetBackgroundColor(Android.Graphics.Color.White);
                }
               
            }
            else
            {
                view.SetBackgroundColor(Android.Graphics.Color.White);
            }
                            

          
           

            return view;
        }


    }
}


