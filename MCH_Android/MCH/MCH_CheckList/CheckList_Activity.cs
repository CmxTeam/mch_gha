﻿ 
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
using System.Text;
using MCH.Graphics;

namespace MCH
{
    //ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
    [Activity (  Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class CheckList_Activity : BaseActivity
    {
        bool isDirty = false;
        List<CheckListItem> items;

        public static Intent BackScreen;
        public static Intent NextScreen ;
        //public static CheckListShipmentItem CheckListTask;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.CheckList_Layout);
            Initialize();

            LoadData();
        }

        private void DoNext()
        {
            StartActivity(CheckList_Activity.NextScreen);
            this.Finish();
        }
        private void DoBack()
        {
            if (isDirty)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {
                        if(result)
                        {
                            StartActivity(CheckList_Activity.BackScreen);
                            this.Finish();
                        }
  
                    };
                m.ShowConfirmationMessage(Resource.String.Confirm_Cancel_Task);
            }
            else
            {
                StartActivity(CheckList_Activity.BackScreen);
                this.Finish();
            }

        }
 

        private string FindSammaryComments(string[] lines)
        {
            if (lines == null)
            {
                return string.Empty;
            }

            if (lines.Length == 0)
            {
                return string.Empty;
            }

            if(lines[0].Contains(")"))
            {
                return string.Empty;
            }
            else
            {
                return lines[0];
            }
        }

        private string FindComments(string[] lines, string question)
        {
            if (lines == null)
            {
                return string.Empty;
            }

            string value = question + ")";
            var result = 
                from l in lines 
                    where l.StartsWith(value)
                select l;  

            StringBuilder sb = new StringBuilder();
            foreach (var r in result) 
            { 
                sb.Append(r.Replace(value,string.Empty));
            } 
            return sb.ToString();

        }
 
        private void LoadData()
        {
       
             
            shipment.Text = string.Format("{0}: {1}",GetText(Resource.String.Shipment_Number) , CheckList_SessionState.CheckListTask.ToString());

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {
    
 
                    CheckList_SessionState.CurrentCheckListData = CheckList.Instance.GetCheckList(CheckList_SessionState.CheckListTask.TaskId,CheckList_SessionState.CheckListTask.AwbId,ApplicationSessionState.User.Data.UserId);
                    if(!CheckList_SessionState.CurrentCheckListData.Transaction.Status || CheckList_SessionState.CurrentCheckListData.Data == null)
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowAlert(CheckList_SessionState.CurrentCheckListData.Transaction.Error,MessageBox.AlertType.Error);
                        m.OnConfirmationClick+= (bool result) => 
                            {
                                DoBack();
                            };

                        //RunOnUiThread(() => progressDialog.Hide());
                        return;
                    }

                    string[] lines;
                    if(!string.IsNullOrEmpty(CheckList_SessionState.CurrentCheckListData.Data.Comments))
                    {
                        lines = CheckList_SessionState.CurrentCheckListData.Data.Comments.Split('\n');
                    }
                    else
                    {
                        lines  = null;
                    }
 


                    items = new List<CheckListItem>();


                    CheckListTitle title = new CheckListTitle();
                    title.Year = CheckList_SessionState.CurrentCheckListData.Data.Year;
                    title.Title = CheckList_SessionState.CurrentCheckListData.Data.Title;
                    title.Note = CheckList_SessionState.CurrentCheckListData.Data.Note;




                    items.Add(new CheckListItem(title));


                    foreach (var a in CheckList_SessionState.CurrentCheckListData.Data.CheckList)
                    {
                        items.Add(new CheckListItem(a.Id, "","", a.Name, "", "",""));

                        foreach (var b in a.Items)
                        {
                            b.Comment  = FindComments(lines,b.Number.ToString());
                            items.Add(new CheckListItem(b.Id ,b.Number.ToString(),"", b.Text, b.Options, b.SelectedOption,b.Comment));

                            int counter = 0;
                            foreach (var c in b.SubItems)
                            {
                                counter++;
                                c.Comment = FindComments(lines,b.Number.ToString() + "-" + counter.ToString());
                                items.Add(new CheckListItem(c.Id ,counter.ToString(),b.Number.ToString(), c.Text, c.Options, c.SelectedOption,c.Comment ));
                            }

                        }

                    }

                    //clear data for test
//                    foreach (var i in items)
//                    {
//                        i.Answer = string.Empty;
//                    }

                    CheckListSummary summary = new CheckListSummary();
                    summary.Comments =  FindSammaryComments(lines);
                    //summary.Comments = CheckList_SessionState.CurrentCheckListData.Data.Comments;
                    summary.UserName = CheckList_SessionState.CurrentCheckListData.Data.Username;
                    summary.Place = CheckList_SessionState.CurrentCheckListData.Data.Place;
                    summary.EntityId = CheckList_SessionState.CurrentCheckListData.Data.EntityId;
                    summary.TaskId = CheckList_SessionState.CurrentCheckListData.Data.TaskId;
                    items.Add(new CheckListItem(summary));

                    Action<int> ItemChange = OnItemChange;
                    Action<bool> ItemSave = OnItemSave;
                    RunOnUiThread (delegate {
                        listView.DividerHeight = 0;
                        listView.Adapter = new CheckList_Adapter(this, items, ItemChange,ItemSave);
  



                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }

        private bool Validate()
        {
            int post = 0;
            int counter = 0;
            int total = 0;
            foreach (var i in items)
            {
                if (!string.IsNullOrEmpty(i.Buttons) && string.IsNullOrEmpty(i.Answer))
                {
                    total++;
                    i.IsIncomplete = true;
                    if (post == 0)
                    {
                        post = counter;
                    }
                }
                else
                {
                    i.IsIncomplete = false;
                }
                counter++;
            }

            if (post > 0)
            {
                //listView.SmoothScrollToPosition(counter);
                RunOnUiThread(delegate
                    {
                        listView.SetSelection(post);
                        Toast.MakeText (this,string.Format(GetText(Resource.String.Incomplete_Questions),total), ToastLength.Long).Show ();

                    });
                return false;
            }
            else
            {
                return true;
            }
        }

        private void OnItemSave(bool isFinalize)
        {

            if (!Validate())
            {
                return;
            }
             
         
            CheckListResultsParam parameters = GetResult(string.Empty);
            parameters.UserId = ApplicationSessionState.User.Data.UserId;
            MCH.Communication.CommunicationTransaction t = CheckList.Instance.SaveCheckList(parameters);
            if (!t.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(GetText(Resource.String.Error_Saving_Data) + ". " + t.Error);
                return;
            }


            isDirty = false;
            if (isFinalize)
            {
                Action<Bitmap> SignatureAction = OnSignatureAction;
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SignatureDialogActivity(this, SignatureAction);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

                return;

            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(GetText(Resource.String.Data_Successfully_Saved));
            }

        }

  

        private CheckListResultsParam GetResult(string signatureData)
        {
            CheckListResultsParam result = new CheckListResultsParam();
            result.Items = new List<CheckListResultItem>();

            StringBuilder sb = new StringBuilder();

            foreach (var i in items)
            {
                if (i.Id == 0)
                {
                    
                    if (i.Summary != null)
                    {
                        result.UserId = ApplicationSessionState.User.Data.UserId;
                        result.Comments = i.Summary.Comments;
                        result.EntityId = i.Summary.EntityId;
                        result.SignatureData = signatureData;
                        result.TaskId = i.Summary.TaskId; 
                    }
 
                }
                else
                {
                    if (!string.IsNullOrEmpty(i.Answer))
                    {
                        result.Items.Add(new CheckListResultItem(i.Id,i.Answer));

                        if (!string.IsNullOrEmpty(i.Comments))
                        {
                            if (i.ParentNumber != string.Empty)
                            {
                                sb.Append(string.Format("\n{0}-{1}){2}",i.ParentNumber,i.Number,i.Comments));
                            }
                            else
                            {
                                sb.Append(string.Format("\n{0}){1}",i.Number,i.Comments));
                            }

                        }
 

                    }
               
                }

            }

            result.Comments += sb.ToString();
            return result;
        }
 

    private void OnSignatureAction(Bitmap  result)
    {
        if (result!= null)
        {
                
                byte[] binaryData = BitmapHelper.BitmapToArray(result);
                string signatureData = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);
                signatureData = "data:image/png;base64," + signatureData;
                CheckListResultsParam parameters = GetResult(signatureData);
                parameters.UserId = ApplicationSessionState.User.Data.UserId;
                MCH.Communication.CommunicationTransaction t = CheckList.Instance.FinalizeCheckList(parameters);
                if (!t.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(GetText(Resource.String.Error_Saving_Data) + ". " + t.Error);
                    return;
                }

                DoNext();
        }

    }


        private void OnItemChange(int position)
        {
            isDirty = true;

        }
 

    }
}





