﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class CheckList_MainListActivity : BaseActivity
    {

        CheckListTasks checkListTasks;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFlight_MainManuLayout);
            Initialize();

            RefreshData(string.Empty,false);
        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {


            try
            {
                GoToTask(checkListTasks.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(CheckListShipmentItem task)
        {
 
            CheckList_SessionState.CheckListTask = task;
            CheckList_Activity.BackScreen = new Intent(this, typeof( CheckList_MainListActivity ));
            CheckList_Activity.NextScreen = new Intent(this, typeof( CheckList_MainListActivity ));
            StartActivity (typeof(CheckList_Activity));
            this.Finish();
 
        }

        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {


                    CheckListStatusTypes en = (CheckListStatusTypes)Enum.Parse(typeof(CheckListStatusTypes), DropDownText.Text.Replace(" ","_"));

                    checkListTasks = MCH.Communication.CheckList.Instance.GetCheckListTasks(ApplicationSessionState.SelectedWarehouseId,int.Parse(ApplicationSessionState.SelectedMenuItem.MenuParam),  ApplicationSessionState.User.Data.UserId,en);

                    RunOnUiThread (delegate {

 

                        if(checkListTasks.Transaction.Status && checkListTasks.Data!=null)
                        {

                            if(searchData!=string.Empty)
                            {
                                List<CheckListShipmentItem> taskList = new List<CheckListShipmentItem>();
                                foreach (var task in checkListTasks.Data)
                                {
                                    if (task.ToString().ToLower().Contains(searchData.ToLower()))
                                    {
                                        taskList.Add(task);   
                                    }
                                }
                                checkListTasks.Data =taskList;
                            }

                            if( checkListTasks.Data.Count == 1 && isBarcode)
                            {
                                GoToTask( checkListTasks.Data[0]);
                            }

                            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Text + string.Format(" ({0})",checkListTasks.Data.Count);
                            listView.Adapter = new CheckList_MainListAdapter(this, checkListTasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;  

                        }
                        else
                        {
                            MessageBox m = new MessageBox(this);
                            m.ShowMessage(checkListTasks.Transaction.Error);
                        }

                        if(isBarcode)
                        {
                            search.Text = string.Empty;
                        }
                        search.RequestFocus ();

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();



        }

    }
}



