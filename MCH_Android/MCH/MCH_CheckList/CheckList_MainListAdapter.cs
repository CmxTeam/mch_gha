﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class CheckList_MainListAdapter : BaseAdapter<CheckListShipmentItem> {

        List<CheckListShipmentItem> items;
        Activity context;


        public CheckList_MainListAdapter(Activity context, List<CheckListShipmentItem> items  ): base()
        {


            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CheckListShipmentItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.CheckList_MainListRow, null);


            TextView ShipmentNumber = view.FindViewById<TextView>(Resource.Id.ShipmentNumber);
            TextView FlightNumber = view.FindViewById<TextView>(Resource.Id.FlightNumber);
            TextView ETD = view.FindViewById<TextView>(Resource.Id.ETD);
            TextView Pieces = view.FindViewById<TextView>(Resource.Id.Pieces);
            TextView Locations = view.FindViewById<TextView>(Resource.Id.Locations);

            TextView Progress = view.FindViewById<TextView>(Resource.Id.Progress);
            ProgressBar ProgressBar = view.FindViewById<ProgressBar>(Resource.Id.ProgressBar);
            ImageView Status= view.FindViewById<ImageView>(Resource.Id.Status);
            ImageView AirlineImage= view.FindViewById<ImageView>(Resource.Id.AirlineImage);

            try 
            {
                System.IO.Stream ims = context.Assets.Open(string.Format(@"Airlines/{0}.png",item.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                AirlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                AirlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            ShipmentNumber.Text = item.AwbNumber;
            FlightNumber.Text = string.Format("Flight: {0}", item.FlightNumber);
            ETD.Text = string.Format("ETD: {0:MM/dd/yyyy HH:mm}", item.ETD);
            Pieces.Text = string.Format("Pcs: {0}", item.Pieces);
            Locations.Text = string.Format("Loc: {0}", item.Locations);

            Progress.Text = string.Format("{0:0}%", item.Progress);
            ProgressBar.Progress = (int)item.Progress;

            switch (item.Status)
            {
                case CheckListStatusTypes.Pending:
                    Status.SetImageResource (Resource.Drawable.Pending);
                    break;
                case CheckListStatusTypes.In_Progress:
                    Status.SetImageResource (Resource.Drawable.InProgress);
                    break;
                case CheckListStatusTypes.Completed:
                    Status.SetImageResource (Resource.Drawable.Completed);
                    break;
            }


            return view;
        }






    }
}


  