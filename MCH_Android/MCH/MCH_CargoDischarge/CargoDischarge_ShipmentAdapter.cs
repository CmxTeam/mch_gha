﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class CargoDischarge_ShipmentAdapter : BaseAdapter<CargoDischargeShipmentItem> {

        List<CargoDischargeShipmentItem> items;
        Activity context;
        CargoDischargeShipmentItem selecteditem;

        public CargoDischarge_ShipmentAdapter(Activity context, List<CargoDischargeShipmentItem> items): base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CargoDischargeShipmentItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {




            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new
            view = context.LayoutInflater.Inflate(Resource.Layout.CargoDischarge_ShipmentRow, null);

            TextView txtAwb= view.FindViewById<TextView>(Resource.Id.txtAwb);
            TextView txtULD= view.FindViewById<TextView>(Resource.Id.txtULD);
            TextView txtPieces= view.FindViewById<TextView>(Resource.Id.txtPieces);
            TextView txtLocations= view.FindViewById<TextView>(Resource.Id.txtLocations);
            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);

        
                txtAwb.Text =string.Format("{0}: {1} {2} {3}",item.ReferenceType.ToString(), item.Origin ,item.Reference,item.Destination  );
                     
     

            if (item.ScannedPieces > 0 && item.TotalPieces > item.ScannedPieces)
            {
                txtPieces.SetTextColor(Color.Red);
            }
            else
            {
                txtPieces.SetTextColor(Color.Black);
            }

            txtPieces.Text =string.Format("PCS: {0} of {1}",item.ScannedPieces, item.TotalPieces);
            txtLocations.Text =string.Format("LOC: {0}",item.Locations);

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);


            if (item.Flag == 0)
            {
                //Indicator.Visibility = ViewStates.Gone;
            }
            else
            {
                //Indicator.Visibility = ViewStates.Visible;
            }

            switch (item.Status)
            {
                case CargoDischargeStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case CargoDischargeStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case CargoDischargeStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress);
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }


            return view;
        }



    }
}





