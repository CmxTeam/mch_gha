﻿ 
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
using MCH.Graphics;
 
namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class CargoDischarge_ShipmentActivity : BaseActivity
    {
 
        CargoDischargeShipmentItems shipments;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.CargoDischarge_ShipmentLayout);
            Initialize();
            RefreshAll();

            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;

        }

        private void RefreshAll()
        {
            LoadData();
            RefreshData(string.Empty,false);
            search.RequestFocus ();
        }

        private void LoadData()
        {
            CargoDischargeTask data=  MCH.Communication.CargoDischarge.Instance.GetCargoDischargeTask (CargoDischarge_SessionState.CurrentDischargeTask.TaskId);
            if (data.Transaction.Status)
            {
                CargoDischarge_SessionState.CurrentDischargeTask = data.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(data.Transaction.Error, MessageBox.AlertType.Error);
            }

            LoadDetail();
        }

        private void LoadDetail()
        {
            CargoDischargeTaskItem item = CargoDischarge_SessionState.CurrentDischargeTask;

            txtCounts.Text = string.Format("SHIP: {0} of {1} - PCS: {2} of {3}",  item.ScannedShipments, item.TotalShipments ,item.ScannedPieces, item.TotalPieces);

            progressBar.Progress = (int)item.PercentProgress;
            txtLocations.Text = "LOC: " + item.Locations;
            txtProgress.Text = string.Format("{0:0}%", item.PercentProgress);
            txtDriver.Text = item.Driver.ToUpper();
            txtCompany.Text = item.Company.ToUpper();

          


            try
            {
                if (!string.IsNullOrEmpty(item.DriverPhoto)) {

                    Byte[] image  = System.Convert.FromBase64String(item.DriverPhoto);
                    personImage.SetImageBitmap (BitmapFactory.DecodeByteArray (image, 0, image.Length));
                }
                else
                {
                    personImage.SetImageResource (Resource.Drawable.person);

                }
            }
            catch
            {
                personImage.SetImageResource (Resource.Drawable.person);
            }


            switch (item.Status)
            {
                case CargoDischargeStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case CargoDischargeStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case CargoDischargeStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            txtForkLift.Text = GetForkLiftCount().ToString();

            CheckFinilize();

        }




        private void DoBack()
        {

            StartActivity (typeof(CargoDischarge_MainActivity));

            this.Finish();
        }



        private void  DropForkLift(long locationId)
        {
            CommunicationTransaction t = MCH.Communication.CargoDischarge.Instance.DropAllForkliftPieces(CargoDischarge_SessionState.CurrentDischargeTask.TaskId,ApplicationSessionState.User.Data.UserId,locationId);
            if(!t.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
            RefreshAll();
        }

        private void btnDrop_Click(object sender, EventArgs e)
        {
            if (GetForkLiftCount() == 0)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);
                return;
            }
            else
            {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new LocationDialogActivity(this, "Location","Select Location",true, LocationTypes.Door);
                dialogFragment.Cancelable = false;
                dialogFragment.OkClicked+= (Barcode barcode) => 
                    {
                        DropForkLift(barcode.Id);
                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

               
            }
        }


        private int GetForkLiftCount()
        {


            try
            {
                ForkLiftCount fl = MCH.Communication.CargoDischarge.Instance.GetForkliftCount(ApplicationSessionState.User.Data.UserId, CargoDischarge_SessionState.CurrentDischargeTask.TaskId);
                txtForkLift.Text = fl.Data.ToString();
                return fl.Data;
            }
            catch
            {
                txtForkLift.Text = "0";
                return 0;
            }

        }

        private void btnForkLift_Click(object sender, EventArgs e)
        {
            if (GetForkLiftCount() == 0)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);
                return;
            }




            List<ForkLiftSelectionItem > fl = new List< ForkLiftSelectionItem>();

            ForkLiftView fv =   MCH.Communication.CargoDischarge.Instance.GetForkliftView(CargoDischarge_SessionState.CurrentDischargeTask.TaskId, ApplicationSessionState.User.Data.UserId );
            if (fv.Transaction.Status && fv.Data!=null)
            {

                foreach (var i in fv.Data)
                {
                    fl.Add(new ForkLiftSelectionItem( i.Reference,i.DetailId,i.ForkliftPieces,i.ReferenceType ));
                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new CargoDischarge_ForkLiftDialog(  this, fl);
                dialogFragment.Cancelable = false;
                dialogFragment.OkClicked += (List<ForkLiftSelectionItem> selection,long locationId) => 
                    {

                        DropForkLiftPiecesIntoLocation(selection,locationId);


                    };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;

            }

        }

 
        void DropForkLiftPiecesIntoLocation(List<ForkLiftSelectionItem> awbs,long locationId)
        {
            if (awbs == null)
            {
                return;
            }

            if (locationId == 0)
            {
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new LocationDialogActivity(this, "Location", "Select Location", true, LocationTypes.Door);
                dialogFragment.Cancelable = false;
                dialogFragment.OkClicked += (Barcode barcode) =>
                {
                    if (barcode.Id == 0)
                    {
                        return;
                    }
                    else
                    {
                        DropForkLiftPiecesIntoLocation(awbs, barcode.Id);
                        return;
                    }
                };
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);  
            }
            else
            {
                foreach (var awb in awbs)
                {
                    CommunicationTransaction t = MCH.Communication.CargoDischarge.Instance.DropForkliftPieces(CargoDischarge_SessionState.CurrentDischargeTask.TaskId,ApplicationSessionState.User.Data.UserId,locationId, awb.DetailId);
                    if(!t.Status)
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }
                }
                RefreshAll();
            }
           

 
        }



 

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            CargoDischargeShipmentItem item = shipments.Data[e.Position];
            ProcessShipment(item);

        }


        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {


                    CargoDischargeStatusTypes status =    EnumHelper.GetEnumItem<CargoDischargeStatusTypes>(DropDownText.Text);

                    shipments = MCH.Communication.CargoDischarge.Instance.GetShipments(ApplicationSessionState.User.Data.UserId,CargoDischarge_SessionState.CurrentDischargeTask.TaskId ,status);

                    RunOnUiThread (delegate {

                        if(shipments.Transaction.Status & shipments.Data!=null)
                        {
                            List<CargoDischargeShipmentItem> list =  LinqHelper.Query<CargoDischargeShipmentItem>(shipments.Data,searchData);

                            if(isBarcode)
                            {
                                if(list.Count == 1)
                                {
                                    ProcessShipment(list[0]);
                                }
                                else if(list.Count == 0)
                                {
                                    LoadGrid(shipments.Data);
                                }
                                else
                                {
                                    LoadGrid(list);
                                }
                            }
                            else
                            {
                                LoadGrid(list);
                            }

                        }
                        else
                        {
                            LoadGrid(null);
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }


        private void ProcessShipment(CargoDischargeShipmentItem ship)
        {



            var transaction = this.FragmentManager.BeginTransaction();
            var dialogFragment = new PiecesDialog(this,ship.Reference , ship.TotalPieces, ship.TotalPieces - ship.ScannedPieces,true );
            dialogFragment.OkClicked += (int pieces) => 
                {

                    CommunicationTransaction t = MCH.Communication.CargoDischarge.Instance.AddForkliftPieces(ship.DetailId,  CargoDischarge_SessionState.CurrentDischargeTask.TaskId , pieces,ApplicationSessionState.User.Data.UserId);
                    if (!t.Status)
                    {
                        MessageBox mb = new MessageBox(this);
                        mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                    }

                    RefreshAll();
                };
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 


        }

        private void LoadGrid(List<CargoDischargeShipmentItem> list)
        {
            if (list == null)
            {
                list = new List<CargoDischargeShipmentItem>();
            }

            shipments.Data  = list;
            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",list.Count).ToUpper();
            listView.Adapter = new CargoDischarge_ShipmentAdapter(this, list);
            listView.ItemClick -= OnListItemClick;
            listView.ItemClick += OnListItemClick; 
        }



        private void CheckFinilize()
        {
            if (CargoDischarge_SessionState.CurrentDischargeTask.Status != CargoDischargeStatusTypes.Completed && CargoDischarge_SessionState.CurrentDischargeTask.ScannedPieces == CargoDischarge_SessionState.CurrentDischargeTask.TotalPieces)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                    {
                        if (result)
                        {

                            DoFinalize();

                        }
                    };
                m.ShowConfirmationMessage("All pieces were scanned. Do you want to finalize task?", Resource.String.Yes, Resource.String.No);
            }
        }



        private void FinalizeTask()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DoFinalize();

                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);

        }


        private void DoFinalize()
        {

            Action<Bitmap> SignatureAction = OnSignatureAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SignatureDialogActivity(this, SignatureAction);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

            return;



        }

        private void OnSignatureAction(Bitmap  result)
        {
            if (result!= null)
            {

                byte[] binaryData = BitmapHelper.BitmapToArray(result);
                string signatureData = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);
                signatureData = "data:image/png;base64," + signatureData;
                
   
                CommunicationTransaction t = MCH.Communication.CargoDischarge.Instance.FinalizeCargoDischarge( CargoDischarge_SessionState.CurrentDischargeTask.TaskId ,  ApplicationSessionState.User.Data.UserId, signatureData);
                if (t.Status)
                {

                    StartActivity (typeof(CargoDischarge_MainActivity));
                    this.Finish();
                }
                else
                {
                    MessageBox mb = new MessageBox(this);
                    mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }


            

          
            }

        }



    }
}




