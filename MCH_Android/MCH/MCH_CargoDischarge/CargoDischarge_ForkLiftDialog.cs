﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using Android.Views.InputMethods;
using MCH.Communication;
namespace MCH
{



    public class CargoDischarge_ForkLiftDialog: DialogFragment
    {
        ListView listView;
        List<ForkLiftSelectionItem> originalSelectionList;
        List<ForkLiftSelectionItem> choiseList;

        Activity context;


        EditText SearchText;
        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;


        Button btnCancel;
        Button btnDrop;
        Button btnDropAll;

        public delegate void OkClickActionEventHandler(List<ForkLiftSelectionItem> selection,long locationId);
        public event OkClickActionEventHandler OkClicked;

        public CargoDischarge_ForkLiftDialog (Activity context, List<ForkLiftSelectionItem> selectionList )
        {
            this.context = context;
            //this.selectionClickAction = selectionClickAction;
            this.originalSelectionList =  selectionList;

        }

        //ADD this for error when rotating
        public CargoDischarge_ForkLiftDialog()
        {
            Dismiss();
        }



        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView(inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.ForkLiftLayout, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            listView = DialogInstance.FindViewById<ListView>(Resource.Id.SelectionListView); 

            btnDrop = DialogInstance.FindViewById<Button>(Resource.Id.btnDrop);
            btnDrop.Click += OnDropClick;
            btnDropAll = DialogInstance.FindViewById<Button>(Resource.Id.btnDropAll);
            btnDropAll.Click += OnDropAllClick;
            btnCancel = DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;



            SearchText = DialogInstance.FindViewById<EditText>(Resource.Id.SeachText);
            ClearSearch = DialogInstance.FindViewById<ImageButton>(Resource.Id.ClearSearch);
            SearchButton = DialogInstance.FindViewById<ImageButton>(Resource.Id.SearchButton);
            EditTextListener = new EditTextEventListener(SearchText);
            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
                {
                    string data = e.Data;
                    if (!e.IsBarcode)
                    {
                        UpdateOriginalList();
                        RefreshData(data);
                    }
                    else
                    {
                        if(e.BarcodeData.BarcodeType == BarcodeTypes.Door)
                        {
                            DropSelectedIntoLocation(e.BarcodeData.BarcodeSuffix);

                        }
                    }
                    SearchText.RequestFocus();
                };
            ClearSearch.Click += OnClearSearch_Click;
            SearchButton.Click += OnSearchButton_Click;

            RefreshData(string.Empty);
            SearchText.RequestFocus();
            return DialogInstance;
        }


        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            UpdateOriginalList();
            SearchText.Text = string.Empty;
            RefreshData(string.Empty);
            SearchText.RequestFocus ();
        }


        private void UpdateOriginalList()
        {
            if (choiseList != null)
            {

                foreach (var originallist in  this.originalSelectionList)
                {
                    foreach (var seachlist in  this.choiseList)
                    {

                        if (seachlist.DetailId == originallist.DetailId)
                        {
                            originallist.Selected = seachlist.Selected;
                        }

                    }
                }
            }

        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {

            UpdateOriginalList();


            RefreshData(SearchText.Text);
            SearchText.RequestFocus();
        }

        void RefreshData(string search)
        {
            if (originalSelectionList.Count == 0)
            {
                if (OkClicked != null)
                    OkClicked(null,0);
                Dismiss();
                return;
            }



            this.choiseList = new List<ForkLiftSelectionItem>();
            foreach (var choise in originalSelectionList)
            {   



                if (search == string.Empty)
                {

                    this.choiseList.Add(new ForkLiftSelectionItem(choise.Reference , choise.DetailId , choise.Pieces,choise.ReferenceType));

                }
                else
                {
                    if (choise.Reference.ToLower().Contains(search.ToLower()))
                    {

                        this.choiseList.Add(new ForkLiftSelectionItem(choise.Reference , choise.DetailId , choise.Pieces,choise.ReferenceType));


                    }
                }

            }



            var adapter  = new CargoDischarge_ForkLiftAdapter(this.context, choiseList, originalSelectionList);
            adapter.OnDelete += () => 
                {

                    ReloadData();
                    RefreshData(string.Empty);
                    SearchText.RequestFocus();
                    return;
                };
            listView.Adapter = adapter;
            listView.RequestFocus();
            InputMethodManager manager = (InputMethodManager)context.GetSystemService(Context.InputMethodService);


            manager.HideSoftInputFromWindow(SearchText.WindowToken, 0);






        }

        void ReloadData()
        {
            List<ForkLiftSelectionItem > fl = new List< ForkLiftSelectionItem>();
            ForkLiftView fv =  MCH.Communication.CargoDischarge.Instance.GetForkliftView( CargoDischarge_SessionState.CurrentDischargeTask.TaskId, ApplicationSessionState.User.Data.UserId );
            if (fv.Transaction.Status)
            {

                foreach (var i in fv.Data)
                {
                    fl.Add(new ForkLiftSelectionItem( i.Reference,i.DetailId,i.ForkliftPieces,i.ReferenceType ));
                }
                this.originalSelectionList = fl;
            }
        }

        void OnCancelClick(object sender, EventArgs e)
        {
            if (OkClicked != null)
                OkClicked(null,0);
            Dismiss();
        }




        void OnDropAllClick(object sender, EventArgs e)
        {
            if (OkClicked != null)
                OkClicked(originalSelectionList,0);

            Dismiss();
        }


        void OnDropClick(object sender, EventArgs e)
        {
            DropSelected(0);
        }

        void DropSelected(long locationId)
        {
            UpdateOriginalList();

            List<ForkLiftSelectionItem> result = new List<ForkLiftSelectionItem>();
            foreach (var selection in originalSelectionList)
            {
                if (selection.Selected)
                {
                    result.Add(selection);
                    //selection.Selected = false;
                }
            }

            if (result.Count == 0)
            {
                MessageBox m = new MessageBox(context);
                m.ShowMessage(GetText(Resource.String.Selection_Dialog_No_Items_Selected_Error));
            }
            else
            {

                if (OkClicked != null)
                    OkClicked(result,locationId);

                Dismiss();
            }

        }

        public void DropSelectedIntoLocation(string  barcode)
        {
            MCH.Communication.Location l = MCH.Communication.Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, barcode );
            if (l.Transaction.Status && l.Data != null)
            {
                DropSelected(l.Data.LocationId);
            }
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {

            if (ApplicationSessionState.IsTest(this.context))
            {

                Dialog.Window.SetTitle ("FORKLIFT" + " (" + ApplicationSessionState.Mode(context).ToString() + ")");

            }
            else
            {

                Dialog.Window.SetTitle ("FORKLIFT");

            }


            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }





    }
}


 