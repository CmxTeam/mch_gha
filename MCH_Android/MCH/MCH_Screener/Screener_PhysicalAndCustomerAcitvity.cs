﻿ 
using System.Collections;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
using System.ComponentModel;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Screener_PhysicalAndCustomerAcitvity : BaseActivity
    {
        public static ScreenerMenuItem.ScreenerMenuType Mode;
        long ReferenceId = 0;
        long CCSFId = 0;
        ScreeningDeviceTypes SelectedScreeningMode;
        string Reference = string.Empty;
        public static string LastReference = string.Empty;
        public static int LastReferencePieces = 0;
        int ReferencePieces = 0;
        string barcode  = string.Empty;
        ScreeningTask currentScreeningTask;
 
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Screener_PhysicalAndCustomerLayout);
            Initialize();
   
            ShowLastScanned();

            txtMessage.Text = Mode.ToString().Replace('_',' ');
 
        }

    
    
        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
 
            if (txtBarcode.Text == string.Empty)
            {
                return;
            }
 

            if (e.IsBarcode)
            {
   
                DoScan(e.BarcodeData);
    

            }
            else
            {
                Barcode b = new Barcode();
                b.BarcodeType = BarcodeTypes.NA;
                b.BarcodeText = txtBarcode.Text;
                DoScan(b);
            }
        }
 

        private void DoScan(Barcode barcode)
        {
 
            ReferenceId = 0;
            Reference = string.Empty;
 
            if (barcode.BarcodeText != string.Empty)
            {
 
                currentScreeningTask = Morpho.Instance.GetScreeningTask(ApplicationSessionState.SelectedWarehouseId,0, ApplicationSessionState.User.Data.UserId, barcode.BarcodeText);

                if (currentScreeningTask.Transaction.Status)
                {

                    this.barcode = barcode.BarcodeText;
                    ReferenceId = currentScreeningTask.Data.Hwb ?? (currentScreeningTask.Data.Awb ?? 0);
                    Reference  = currentScreeningTask.Data.ShipmentReference ?? string.Empty; 
                    EditTextListener.Text = string.Empty;

                    if (currentScreeningTask.Data.HasAlarm)
                    {
                        MessageBox m = new MessageBox(this);
                        m.OnConfirmationClick += (bool result) => 
                        {
                                if(result)
                                {

                                    Action<int> PiecesClickAction = OnPiecesClickAction;
                                    var transaction = FragmentManager.BeginTransaction();
                                    var dialogFragment = new PiecesDialog(this,currentScreeningTask.Data.ShipmentReference, PiecesClickAction,currentScreeningTask.Data.TotalPieces, currentScreeningTask.Data.TotalPieces - currentScreeningTask.Data.ScreenedPieces , false );
                                    dialogFragment.Cancelable = false;
                                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 

                                }
                                else
                                {
                                    return;
                                }
                        };
                        m.ShowConfirmationMessage("Shipment Number " + currentScreeningTask.Data.ShipmentReference + " has an 'Alarm'. Do you want to clear the alarm?" ,"Yes","No");
                    }
                    else
                    {
                        
                                    Action<int> PiecesClickAction = OnPiecesClickAction;
                                    var transaction = FragmentManager.BeginTransaction();
                        var dialogFragment = new PiecesDialog(this,currentScreeningTask.Data.ShipmentReference, PiecesClickAction,currentScreeningTask.Data.TotalPieces, currentScreeningTask.Data.TotalPieces - currentScreeningTask.Data.ScreenedPieces , false );
                                    dialogFragment.Cancelable = false;
                                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 

                    }        

                }
                else
                {
                     
                    MessageBox m = new MessageBox(this);
                    m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
                }


            }       
            else
            {
            
                MessageBox m = new MessageBox(this);
                m.ShowMessage(GetText(Resource.String.Invalid) + " " + GetText(Resource.String.Barcode) + ".");
            }


        }

        private void OnPiecesClickAction(int pieces)
        {
            if (pieces > 0)
            {
                ReferencePieces = pieces;
                 
                RunOnUiThread(() => txtBarcode.Text = string.Empty);
 

                if (Mode == ScreenerMenuItem.ScreenerMenuType.Physical_Inspection)
                {
                    Action<string,string> ClickAction = OnRemarkClickAction;
                    var transaction = this.FragmentManager.BeginTransaction();
                    var dialogFragment = new Screener_PassFailDialogActivity(this, ClickAction,txtMessage.Text, Reference,ReferencePieces);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                    return;
                }
                else if (Mode == ScreenerMenuItem.ScreenerMenuType.Verify_Customer_Screening)
                {
                    
                    Action<bool,long> BarcodeClickAction = OnCCSFClickAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new Screener_CCSFDialogActivity(this, BarcodeClickAction,"CCSF#",Reference);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                    return;
                }

  

            }

        }

        private void OnCCSFClickAction(bool result,long ccsfid)
        {
         
            if (!result)
            {
                return;
            }

            this.CCSFId=ccsfid;
 
             
            List<SelectionItem> screeninglist  = new List<SelectionItem>();
            ScreeningDeviceTypes[] values = (ScreeningDeviceTypes[])Enum.GetValues(typeof(ScreeningDeviceTypes));
            foreach (ScreeningDeviceTypes n in values)
            {
                ScreeningDeviceTypes en = (ScreeningDeviceTypes)Enum.Parse(typeof(ScreeningDeviceTypes), n.ToString());
                screeninglist.Add(new SelectionItem( en.ToString().Replace("_"," "),(long)en));
            }

            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnClickSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Screening Types", this, screeninglist, SelectionAction, SelectionListAdapter.SelectionMode.SingleSelection,false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
 
 

        }


        private void OnClickSelectionAction(List<MCH.Communication.SelectionItem>  result, bool isExtra)
        {
            foreach (var r in result)
            {
                SelectedScreeningMode = (ScreeningDeviceTypes)r.Id;
                Action<string,string> ClickAction = OnRemarkClickAction;
                var transaction = this.FragmentManager.BeginTransaction();
                var dialogFragment = new Screener_CommentDialogActivity(this, ClickAction,txtMessage.Text, Reference,ReferencePieces);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;

            }

            return;

        }


        private void OnRemarkClickAction(string result, string comments)
        {
            if (result == string.Empty)
            {
         
                return;
            }

            ScreeningTransactionModel screeningTransactionItem = new ScreeningTransactionModel();

            if (Mode == ScreenerMenuItem.ScreenerMenuType.Physical_Inspection)
            {
               
                screeningTransactionItem.Comment = comments;
                screeningTransactionItem.Pieces = ReferencePieces;
                screeningTransactionItem.Result = result.ToLower() == "pass" ? ScreeningResult.PASS : ScreeningResult.ALARM;
                screeningTransactionItem.PrescreenedCCSFId = 0;
                screeningTransactionItem.SampleNumber = string.Empty;
                screeningTransactionItem.SubstancesFound = string.Empty;
                screeningTransactionItem.TaskId = currentScreeningTask.Data.TaskId;
                screeningTransactionItem.UserId = ApplicationSessionState.User.Data.UserId;
                screeningTransactionItem.Data = new ScreeningDeviceItem();
                screeningTransactionItem.Data.device = ScreeningDeviceTypes.PHYSICAL;
                screeningTransactionItem.Data.DeviceId = 0;
                screeningTransactionItem.Data.SerialNumber = string.Empty;
            }
            else if (Mode == ScreenerMenuItem.ScreenerMenuType.Verify_Customer_Screening)
            {
                
                screeningTransactionItem.Comment = comments;
                screeningTransactionItem.Pieces = ReferencePieces;
                screeningTransactionItem.Result = ScreeningResult.PASS; 
                screeningTransactionItem.PrescreenedCCSFId = CCSFId;
                screeningTransactionItem.SampleNumber = string.Empty;
                screeningTransactionItem.SubstancesFound = string.Empty;
                screeningTransactionItem.TaskId = currentScreeningTask.Data.TaskId;
                screeningTransactionItem.UserId = ApplicationSessionState.User.Data.UserId;
                screeningTransactionItem.Data = new ScreeningDeviceItem();
                screeningTransactionItem.Data.device = SelectedScreeningMode;
                screeningTransactionItem.Data.DeviceId = 0;
                screeningTransactionItem.Data.SerialNumber = string.Empty;
            }



 





            CommunicationTransaction t = Morpho.Instance.SaveScreeningTransaction(screeningTransactionItem);
            if (!t.Status)
            {
  
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += delegate
                {
                    return; 
                };
                m.ShowAlert(t.Error, MessageBox.AlertType.Alert);
                return;
            }
            else
            {
   
                GoToCamera(typeof( Screener_PhysicalAndCustomerAcitvity ),typeof(  Screener_PhysicalAndCustomerAcitvity ),currentScreeningTask.Data.TaskId ,this.barcode);

            }

            LastReferencePieces = ReferencePieces;
            LastReference = Reference;
            ShowLastScanned();


        }

        void ShowLastScanned()
        {
            RunOnUiThread(delegate
                        {
                    if(LastReference == string.Empty)
                    {
                        LastScan.Text = string.Empty;
                    }
                    else
                    {
                        LastScan.Text = "Last scan: " + LastReference + " Pcs: " + LastReferencePieces.ToString();
                    }
                            
                        });
        }

    }
}





