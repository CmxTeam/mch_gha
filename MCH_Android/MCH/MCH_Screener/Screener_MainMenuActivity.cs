﻿//Screener_MainMenuActivity
using System.Collections;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class Screener_MainMenuActivity : BaseActivity
    {

        List<ScreenerMenuItem> menulist;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Screener_MainMenuLayout);
            Initialize();



            RefreshData();
        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            switch (menulist[e.Position].Menu)
            {
                case ScreenerMenuItem.ScreenerMenuType.ETD_Screening:
                    //Screener_MorphoActivity.Mode = ScreenerMenuItem.ScreenerMenuType.ETD_Screening;
                    Screener_MorphoActivity.CurrentDeviceId = 0;
                    StartActivity(typeof(Screener_MorphoActivity));
                    this.Finish();
                    break;
                case ScreenerMenuItem.ScreenerMenuType.Physical_Inspection:
                    Screener_PhysicalAndCustomerAcitvity.Mode = ScreenerMenuItem.ScreenerMenuType.Physical_Inspection;
                    //Screener_PhysicalAndCustomerAcitvity.CurrentDeviceId = 0;
                    StartActivity(typeof(Screener_PhysicalAndCustomerAcitvity));
                    this.Finish();
                    break;
                case ScreenerMenuItem.ScreenerMenuType.Verify_Customer_Screening:
                    Screener_PhysicalAndCustomerAcitvity.Mode = ScreenerMenuItem.ScreenerMenuType.Verify_Customer_Screening;
                    //Screener_PhysicalAndCustomerAcitvity.CurrentDeviceId = 0;
                    StartActivity(typeof(Screener_PhysicalAndCustomerAcitvity));
                    this.Finish();
                    break;
            }
 
        }
 

 

        private void OnPiecesClickAction(int pieces)
        {
            Toast.MakeText (this, pieces.ToString(), ToastLength.Long).Show ();
        }






        private void RefreshData()
        {

            menulist = new List<ScreenerMenuItem>();
            menulist.Add(new ScreenerMenuItem(ScreenerMenuItem.ScreenerMenuType.Physical_Inspection,typeof( MainMenuActivity),ScreenerMenuItem.ScreenerMenuType.Physical_Inspection.ToString()));
            menulist.Add(new ScreenerMenuItem(ScreenerMenuItem.ScreenerMenuType.Verify_Customer_Screening,typeof( MainMenuActivity),ScreenerMenuItem.ScreenerMenuType.Verify_Customer_Screening.ToString()));
            menulist.Add(new ScreenerMenuItem(ScreenerMenuItem.ScreenerMenuType.ETD_Screening,typeof( MainMenuActivity),ScreenerMenuItem.ScreenerMenuType.ETD_Screening.ToString()));
 

            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {
  
                    RunOnUiThread (delegate {
  
                                listView.Adapter = new Screener_MainMenuListAdapter(this, menulist);
                                listView.ItemClick -= OnListItemClick;
                                listView.ItemClick += OnListItemClick;  
        
                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();
        }


 

    }
}





