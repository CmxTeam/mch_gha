﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class Screener_PassFailDialogActivity: DialogFragment
    {
        
        Button btnCancel;
        Button btnOk;
        TextView txtReference;
        TextView txtPieces;
        Button btnComments;
        TextView txtComments;
        RadioButton radioPass;
        RadioButton radioFail;

        Action<string,string> okClickAction;
        Activity context;
        string title;
        int pieces; 
        string reference;
        public Screener_PassFailDialogActivity (Activity context, Action<string,string> okClickAction, string title, string reference, int pieces)
        {

            this.title = title;
            this.context = context;
            this.okClickAction = okClickAction;
            this.reference = reference;
            this.pieces = pieces;
        }

 

        //ADD this for error when rotating
        public Screener_PassFailDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView (inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.Screener_PassFailDialog, container, false);

            Dialog.RequestWindowFeature((int)WindowFeatures.LeftIcon);

            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 
            btnOk.Click += OnOk_Click;


            btnCancel =DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;

            txtReference = DialogInstance.FindViewById<TextView>(Resource.Id.txtReference);
            txtComments = DialogInstance.FindViewById<TextView>(Resource.Id.txtComments);
            txtPieces = DialogInstance.FindViewById<TextView>(Resource.Id.txtPieces);
          
            btnComments = DialogInstance.FindViewById<Button>(Resource.Id.btnComments); 
            btnComments.Click += OnbtnComments_Click; 
 
            radioPass = DialogInstance.FindViewById<RadioButton>(Resource.Id.radioPass); 
            radioFail = DialogInstance.FindViewById<RadioButton>(Resource.Id.radioFail); 

            txtReference.Text = "Reference: " + reference;
            txtPieces.Text = "Pieces: " + pieces.ToString();

            return DialogInstance;
        }

        private void OnbtnComments_Click(object sender, EventArgs e)
        {
        
            List<SelectionItem> remarklist  = new List<SelectionItem>();
            ScreeningInspectionRemarks remarks = Morpho.Instance.GetScreeningInspectionRemarks(ApplicationSessionState.SelectedWarehouseId);
            if (!remarks.Transaction.Status)
            {
                MessageBox m = new MessageBox(context);
                m.ShowAlert(remarks.Transaction.Error, MessageBox.AlertType.Error);
                return;
            }
            else
            {
                if (remarks.Data !=null )
                {
                    foreach (ScreeningInspectionRemarkItem r in remarks.Data)
                    {
                        remarklist.Add(new SelectionItem(r.Remark,r.Id));
                    }
                }
            }

           

            Action<List<MCH.Communication.SelectionItem>,bool> SelectionAction = OnClickSelectionAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Remarks", context, remarklist, SelectionAction, SelectionListAdapter.SelectionMode.CheckBoxSeletion,false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);


        }

 
        private void OnClickSelectionAction(List<MCH.Communication.SelectionItem>  result, bool isExtra)
        {
            foreach (var r in result)
            {
                if (txtComments.Text.Trim() == string.Empty)
                {
                    txtComments.Text = r.Name; 
                }
                else
                {
                    txtComments.Text += ", " + r.Name; 
                }
            }
        }

        private void OnOk_Click(object sender, EventArgs e)
        {

            if (!radioFail.Checked && !radioPass.Checked)
            {
                MessageBox m = new MessageBox(context);
                m.ShowMessage("Please make a selection.");
                return;
            }


            if (radioFail.Checked)
            {
                if (txtComments.Text == string.Empty)
                {
                    MessageBox m = new MessageBox(context);
                    m.ShowMessage("Please enter comments.");
                    return;
                }
            }

            if (radioPass.Checked)
            {
                okClickAction.Invoke ("PASS",txtComments.Text);
            }
            else
            {
                okClickAction.Invoke ("FAIL",txtComments.Text);
            }
 
            Dismiss();
        }

  
        void OnCancelClick(object sender, EventArgs e)
        {
            okClickAction.Invoke (string.Empty,string.Empty);
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title);
 
         
            base.OnActivityCreated (savedInstanceState);


            Dialog.SetFeatureDrawableResource((int)WindowFeatures.LeftIcon , Resource.Drawable.Icon);

            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }


   

    }
}




