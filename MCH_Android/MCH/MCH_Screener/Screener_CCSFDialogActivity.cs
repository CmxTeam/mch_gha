﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using System.Threading;
using MCH.Communication;


namespace MCH
{

    public class Screener_CCSFDialogActivity: DialogFragment
    {
         
        Button btnCancel;
        Button btnOk;
        TextView txtReference;
        TextView txtCCSF;
        EditTextEventListener EditTextListener;
        Action<bool,long> okClickAction;
        Activity context;
        string title;
        string reference;

 

        public Screener_CCSFDialogActivity (Activity context, Action<bool,long> okClickAction, string title , string reference)
        {

            this.title = title;
            this.reference = reference;
            this.context = context;
            this.okClickAction = okClickAction;

        }

        //ADD this for error when rotating
        public Screener_CCSFDialogActivity()
        {
            Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            base.OnCreateView (inflater, container, savedInstanceState);
            View DialogInstance = inflater.Inflate(Resource.Layout.Screener_CCSFDialog, container, false);


            btnOk = DialogInstance.FindViewById<Button>(Resource.Id.btnOk); 

            txtReference = DialogInstance.FindViewById<TextView>(Resource.Id.txtReference);
            txtCCSF = DialogInstance.FindViewById<TextView>(Resource.Id.txtCCSF);

            btnCancel =DialogInstance.FindViewById<Button>(Resource.Id.btnCancel);
            btnCancel.Click += OnCancelClick;
 

            btnOk.Click += OnOk_Click;
 
            txtReference.Text = "Reference: " + reference;


            EditTextListener = new EditTextEventListener(txtCCSF);
            EditTextListener.OnEnterEvent += OnEnterEvent;

            return DialogInstance;
        }

        void OnEnterEvent(object sender, EditTextEventArgs e)
        {
            VerifyCCSF();
        }

        private void OnOk_Click(object sender, EventArgs e)
        {

            VerifyCCSF();

        }

        void VerifyCCSF()
        {
            if (txtCCSF.Text != string.Empty)
            {

                CCSF ccsf = Morpho.Instance.ValidateCertificationNumber(txtCCSF.Text);
                if (ccsf.Transaction.Status)
                {
                    if (ccsf.Data != null)
                    {
                        okClickAction.Invoke (true,ccsf.Data.CCSFId);
                        Dismiss();
                    }
                    else
                    {
                        MessageBox m = new MessageBox(context);
                        m.ShowAlert(GetText(Resource.String.Invalid) + " " + title + ".", MessageBox.AlertType.Alert);
                    }
                }
                else
                {
                    MessageBox m = new MessageBox(context);
                    m.ShowAlert(ccsf.Transaction.Error, MessageBox.AlertType.Error);
                }

            }
            else
            {
                        MessageBox m = new MessageBox(context);
                        m.ShowAlert(GetText(Resource.String.Invalid) + " " + title + ".", MessageBox.AlertType.Alert);
            }
        }


        void OnCancelClick(object sender, EventArgs e)
        {
            okClickAction.Invoke (false,0);
            Dismiss();
        }

        public override void OnActivityCreated( Bundle savedInstanceState)
        {
            Dialog.Window.SetTitle (this.title);
            //Dialog.Window.RequestFeature (WindowFeatures.NoTitle);
            base.OnActivityCreated (savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.DialogAnimation;
        }


        
 

    }
}



