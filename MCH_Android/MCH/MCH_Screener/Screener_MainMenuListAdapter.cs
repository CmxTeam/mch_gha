﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Content.Res;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;

namespace MCH
{
    public class ScreenerMenuItem
    {
        public enum ScreenerMenuType
        {
            Physical_Inspection,
            ETD_Screening,
            Verify_Customer_Screening,
        }
        public ScreenerMenuItem(ScreenerMenuType menu, Type screen,string iconKey)
        {
            this.Screen = screen;
            this.Menu = menu;
            this.IconKey  =iconKey;
        }
 
        public Type Screen { get; set;}
        public ScreenerMenuType Menu { get; set;}
        public string IconKey { get; set;}

    }

    public class Screener_MainMenuListAdapter : BaseAdapter<ScreenerMenuItem> {

        List<ScreenerMenuItem> items;
        Activity context;

        public Screener_MainMenuListAdapter(Activity context, List<ScreenerMenuItem> items): base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override ScreenerMenuItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView (int position, View convertView, ViewGroup parent)
        {

            var item = items [position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate (Resource.Layout.Screener_MainMenuRow, null);

 
            view.FindViewById<TextView> (Resource.Id.txtTask).Text = string.Format ("{0}", item.Menu.ToString().Replace('_',' ')).ToUpper();
 

            ImageView pic = view.FindViewById<ImageView> (Resource.Id.taskIcon);


            try 
            {
                System.IO.Stream ims = context.Assets.Open(string.Format(@"Icons/{0}.png",item.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                pic.SetImageDrawable(d);
            } 
            catch 
            {
                pic.SetImageResource (Resource.Drawable.Clock);
            }



            return view;
        }






    }
}

