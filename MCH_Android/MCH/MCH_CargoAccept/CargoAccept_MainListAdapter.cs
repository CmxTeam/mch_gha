﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{

  

    public class CargoAccept_MainListAdapter : BaseAdapter<CargoAcceptTaskItem> {
        
        List<CargoAcceptTaskItem> items;
        Activity context;
        Action<int> OnImageClickAction;
       
 
        public CargoAccept_MainListAdapter(Activity context, List<CargoAcceptTaskItem> items, Action<int> onImageClickAction ): base()
        {
 
            this.OnImageClickAction = onImageClickAction;
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override CargoAcceptTaskItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }

       
        public  override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = items[position];
            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.CargoAccept_MainListRow, null);

 
            TextView FullName = view.FindViewById<TextView>(Resource.Id.FullName);

            TextView Company = view.FindViewById<TextView>(Resource.Id.Company);
            TextView Counts = view.FindViewById<TextView>(Resource.Id.Counts);
            TextView Locations = view.FindViewById<TextView>(Resource.Id.Locations);
            TextView Date = view.FindViewById<TextView>(Resource.Id.Date);
            TextView ProgressText = view.FindViewById<TextView>(Resource.Id.Progress);

            ProgressBar bar = view.FindViewById<ProgressBar>(Resource.Id.TaskProgressBar);

            ImageView imageIcon = view.FindViewById<ImageView>(Resource.Id.taskIcon);
            ImageView AirlineImage= view.FindViewById<ImageView>(Resource.Id.AirlineImage);




            bar.Progress = (int)item.Progress;
            FullName.Text = item.Name;
            Company.Text = item.Company;
            Counts.Text = string.Format("AWBs: {0}, Pcs: {1}", item.Awbs, item.Pcs);
            Locations.Text = "Location: " + item.Locations;
            Date.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", item.Date);
            ProgressText.Text = string.Format("{0:0}%", item.Progress);





            try 
            {
                System.IO.Stream ims = context.Assets.Open(string.Format(@"Airlines/{0}.png",item.Airline));
                Drawable d = Drawable.CreateFromStream(ims, null);
                AirlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                AirlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            try
            {
                if (item.TaskImage != null) {
                    imageIcon.SetImageBitmap (BitmapFactory.DecodeByteArray (item.TaskImage, 0, item.TaskImage.Length));
            }
            else
            {
                    imageIcon.SetImageResource (Resource.Drawable.person);

            }
            }
            catch
            {
                imageIcon.SetImageResource (Resource.Drawable.person);
            }




            imageIcon.Tag = position.ToString();
            imageIcon.Click -= OnImageClick;
            imageIcon.Click += OnImageClick;
 
            return view;
        }

        public void OnImageClick(object sender, EventArgs e) 
        {
            ImageView img = (ImageView)sender;
            OnImageClickAction.Invoke (int.Parse(img.Tag.ToString()));
        }

 
 

    }
}
