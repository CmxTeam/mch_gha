﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Graphics;

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public class CargoAccept_ValidateSealActivity : BaseActivity
    {

      
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;

        EditTextEventListener EditTextListener;

        TextView sealnumber;
        TextView FullName;
        TextView Company;
        TextView Counts;
        TextView Locations;
        TextView Date;
        TextView ProgressText;
        ProgressBar bar;
        ImageView imageIcon;
        ImageView AirlineImage;
        Button overrideseal;
        Button validateseal;
 
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.CargoAccept_ValidateSealLayout);
 
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);

            headerText.Text = ApplicationSessionState.SelectedMenuItem.Name;
            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                headerImage.SetImageResource (Resource.Drawable.Clock);
            }

            FullName = FindViewById<TextView>(Resource.Id.FullName);
            Company = FindViewById<TextView>(Resource.Id.Company);
            Counts = FindViewById<TextView>(Resource.Id.Counts);
            Locations = FindViewById<TextView>(Resource.Id.Locations);
            Date = FindViewById<TextView>(Resource.Id.Date);
            ProgressText = FindViewById<TextView>(Resource.Id.Progress);
            bar = FindViewById<ProgressBar>(Resource.Id.TaskProgressBar);
            imageIcon = FindViewById<ImageView>(Resource.Id.taskIcon);
            AirlineImage= FindViewById<ImageView>(Resource.Id.AirlineImage);
            overrideseal= FindViewById<Button>(Resource.Id.overrideseal);
            validateseal= FindViewById<Button>(Resource.Id.validateseal);

            validateseal.Click += OnValidateSealClick;
            overrideseal.Click += OnOverrideSealClick;

            sealnumber = FindViewById<TextView>(Resource.Id.sealnumber);
            EditTextListener = new EditTextEventListener(sealnumber,true);
            EditTextListener.OnEnterEvent +=  OnSealNumber;
  

            RefreshData();

        }
 
        private void OnSealNumber(object sender, EditTextEventArgs e)
        {
            string data = e.Data;
                    if (e.IsBarcode)
                    {
                        //Parse Barcode Data
                        //sealnumber.Text = e.Data.ToString();
                    }
                     
                    ValidateSealNumber(e.Data);
        }

        private void OnValidateSealClick(object sender, EventArgs e)
        {
            ValidateSealNumber(sealnumber.Text);
        }

        private void OnOverrideSealClick(object sender, EventArgs e)
        {
            if (sealnumber.Text == string.Empty)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.Enter_SEAL_Number_Error);
                sealnumber.RequestFocus();
                return;
            }
            Action<MCH.Communication.User>  supervisorOverrideAction  = OnDoSupervisorOverride;
            DoSupervisorOverride(supervisorOverrideAction);
        }
 
        private void OnDoSupervisorOverride(MCH.Communication.User Supervisor)
        {
            if (Supervisor != null)
            {
//                CargoAccept_SessionState.CurrentCargoAcceptTask.SealNumber = sealnumber.Text;
//                CargoAccept_SessionState.CurrentCargoAcceptTask.OverrideSeal = true;
//                StartActivity(typeof(CargoAccept_SealOverrideActivity));
//                this.Finish();


                var nextScreen = new Intent (this, typeof(CargoAccept_SealOverrideActivity));
                nextScreen.PutExtra ("SealNumber", sealnumber.Text);
                StartActivity (nextScreen);
                this.Finish();


            }
               
        }



        private void ValidateSealNumber(string seal)
        {
            if (seal == string.Empty)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.Enter_SEAL_Number_Error);
                sealnumber.RequestFocus();
                return;
            }


            bool isSealValid = MCH.Communication.CargoAccept.Instance.ValidateSealNumber(seal);
 

            if (CargoAccept_SessionState.CurrentCargoAcceptTask.SealNumber == seal && CargoAccept_SessionState.CurrentCargoAcceptTask.OverrideSeal)
            {
                isSealValid = true;
            }

            if (isSealValid)
            {
                CargoAccept_SessionState.CurrentCargoAcceptTask.SealNumber = seal;
                StartActivity(typeof(EmptyActivity));
                this.Finish();
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.InvalidSealMessage);
                sealnumber.RequestFocus();
            }
                
        }

        private void RefreshData()
        {

            

            bar.Progress = (int)CargoAccept_SessionState.CurrentCargoAcceptTask.Progress;
            FullName.Text = CargoAccept_SessionState.CurrentCargoAcceptTask.Name;
            Company.Text = CargoAccept_SessionState.CurrentCargoAcceptTask.Company;
            Counts.Text = string.Format("AWBs: {0}, Pcs: {1}", CargoAccept_SessionState.CurrentCargoAcceptTask.Awbs, CargoAccept_SessionState.CurrentCargoAcceptTask.Pcs);
            Locations.Text = "Location: " + CargoAccept_SessionState.CurrentCargoAcceptTask.Locations;
            Date.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", CargoAccept_SessionState.CurrentCargoAcceptTask.Date);
            ProgressText.Text = string.Format("{0:0}%", CargoAccept_SessionState.CurrentCargoAcceptTask.Progress);
 

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",CargoAccept_SessionState.CurrentCargoAcceptTask.Airline));
                Drawable d = Drawable.CreateFromStream(ims, null);
                AirlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                AirlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            try
            {
                if (CargoAccept_SessionState.CurrentCargoAcceptTask.TaskImage != null) {
                        imageIcon.SetImageBitmap (BitmapFactory.DecodeByteArray (CargoAccept_SessionState.CurrentCargoAcceptTask.TaskImage, 0, CargoAccept_SessionState.CurrentCargoAcceptTask.TaskImage.Length));
                }
                else
                {
                        imageIcon.SetImageResource (Resource.Drawable.person);

                }
            }
            catch
            {
                imageIcon.SetImageResource (Resource.Drawable.person);
            }

            imageIcon.Click -= OnImageClick;
            imageIcon.Click += OnImageClick;

            EditTextListener.EnableEnter = false;
            sealnumber.Text = CargoAccept_SessionState.CurrentCargoAcceptTask.SealNumber;
            EditTextListener.EnableEnter = true;

            sealnumber.RequestFocus ();

        }
 

        public void OnImageClick(object sender, EventArgs e) 
        {
            //ImageView img = (ImageView)sender;
            //OnImageClickAction.Invoke (int.Parse(img.Tag.ToString()));
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new ImageDialog(this,CargoAccept_SessionState.CurrentCargoAcceptTask.TaskImage );
                dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

            sealnumber.RequestFocus ();

        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

  
        private void DoBack()
        {
            //StartActivity (typeof(CargoAccept_CaptureSealImageActivity));
            //this.Finish();

            //new
            //string taskTitle =string.Format("{0}\n{1}", CargoAccept_SessionState.CurrentCargoAcceptTask.Name,CargoAccept_SessionState.CurrentCargoAcceptTask.Company);
            var nextScreen = new Intent(this, typeof(Camera_GalleryActivity));
            //nextScreen.PutExtra("Title", taskTitle);
            nextScreen.PutExtra("Icon", ApplicationSessionState.SelectedMenuItem.IconKey);
            StartActivity(nextScreen);
            this.Finish();
        }
 
        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();
 
            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();
 
            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }

    }
}

