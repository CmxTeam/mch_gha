﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Graphics;

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class CargoAccept_SealOverrideActivity : BaseActivity
    {


        string sealNumberData;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView (Resource.Layout.CargoAccept_SealOverrideLayout);
            Initialize(ApplicationSessionState.SelectedMenuItem.Name);

            sealNumberData = Intent.GetStringExtra ("SealNumber") ?? "";

            RefreshData();

        }
 
        private void OnValidateSealClick(object sender, EventArgs e)
        {

            if (!ReScreenOption.Checked && !NoScreenOption.Checked)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(Resource.String.Select_Seal_Option_Error);
                return;
            }

                        Action<Bitmap> SignatureAction = OnSignatureAction;
                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new SignatureDialogActivity(this, SignatureAction);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

                    return;




        }

        private void OnSignatureAction(Bitmap  result)
        {
            if (result!= null)
            {
                CargoAccept_SessionState.CurrentCargoAcceptTask.SealNumber =  sealNumberData;
                CargoAccept_SessionState.CurrentCargoAcceptTask.OverrideSeal = true;
                StartActivity(typeof(EmptyActivity));
                this.Finish();
            }
           
        }





        private void RefreshData()
        {

            

            bar.Progress = (int)CargoAccept_SessionState.CurrentCargoAcceptTask.Progress;
            FullName.Text = CargoAccept_SessionState.CurrentCargoAcceptTask.Name;
            Company.Text = CargoAccept_SessionState.CurrentCargoAcceptTask.Company;
            Counts.Text = string.Format("AWBs: {0}, Pcs: {1}", CargoAccept_SessionState.CurrentCargoAcceptTask.Awbs, CargoAccept_SessionState.CurrentCargoAcceptTask.Pcs);
            Locations.Text = "Location: " + CargoAccept_SessionState.CurrentCargoAcceptTask.Locations;
            Date.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", CargoAccept_SessionState.CurrentCargoAcceptTask.Date);
            ProgressText.Text = string.Format("{0:0}%", CargoAccept_SessionState.CurrentCargoAcceptTask.Progress);
 

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",CargoAccept_SessionState.CurrentCargoAcceptTask.Airline));
                Drawable d = Drawable.CreateFromStream(ims, null);
                AirlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                AirlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            try
            {
                if (CargoAccept_SessionState.CurrentCargoAcceptTask.TaskImage != null) {
                        imageIcon.SetImageBitmap (BitmapFactory.DecodeByteArray (CargoAccept_SessionState.CurrentCargoAcceptTask.TaskImage, 0, CargoAccept_SessionState.CurrentCargoAcceptTask.TaskImage.Length));
                }
                else
                {
                        imageIcon.SetImageResource (Resource.Drawable.person);

                }
            }
            catch
            {
                imageIcon.SetImageResource (Resource.Drawable.person);
            }

            imageIcon.Click -= OnImageClick;
            imageIcon.Click += OnImageClick;

        }
 

        public void OnImageClick(object sender, EventArgs e) 
        {

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new ImageDialog(this,CargoAccept_SessionState.CurrentCargoAcceptTask.TaskImage );
                dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }
 
        private void DoBack()
        {
            StartActivity (typeof(CargoAccept_ValidateSealActivity));
            this.Finish();
        }
 


    }
}


 