﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MCH
{
     		
    public partial class AcceptFreight_ScreeningActivity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;
 

        
        Spinner SealTypeSpinner;
        Button btnNext;

        TextView txtName;
        TextView txtCompany;
        TextView txtCounts;
        TextView txtLocations;
        TextView txtDate;
        TextView txtProgress;
        TextView txtShipper;
        ProgressBar progressBar;
        ImageView personImage;
        ImageView airlineImage;
        TextView txtSealNumber;

        private void Initialize()
        {
            string title = Intent.GetStringExtra ("Title") ?? ""; 
            Initialize(title);
        }

        private void Initialize(int title)
        {
            Initialize(Application.Context.GetText(title));
        }

        private void Initialize(string tilte)
        {
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;

   
            headerText.Text = ApplicationSessionState.SelectedMenuItem.Name;
            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }

            //Find here new controls
            txtName = FindViewById<TextView>(Resource.Id.txtName);
            txtCompany = FindViewById<TextView>(Resource.Id.txtCompany);
            txtCounts = FindViewById<TextView>(Resource.Id.txtCounts);
            txtLocations = FindViewById<TextView>(Resource.Id.txtLocations);
            txtDate = FindViewById<TextView>(Resource.Id.txtDate);
            txtProgress = FindViewById<TextView>(Resource.Id.txtProgress);
            txtShipper = FindViewById<TextView>(Resource.Id.txtShipper);
            progressBar = FindViewById<ProgressBar>(Resource.Id.progressBar);
            personImage = FindViewById<ImageView>(Resource.Id.personImage);
            airlineImage= FindViewById<ImageView>(Resource.Id.airlineImage);

            txtSealNumber = FindViewById<TextView>(Resource.Id.txtSealNumber);



            progressBar.Progress = (int)AcceptFreight_SessionState.CurrentAcceptFreightTask.Progress;
            txtName.Text = AcceptFreight_SessionState.CurrentAcceptFreightTask.DriverName;
            txtCompany.Text = AcceptFreight_SessionState.CurrentAcceptFreightTask.DriverCompany;
            txtCounts.Text = string.Format("AWBs: {0}, Pcs: {1}", AcceptFreight_SessionState.CurrentAcceptFreightTask.Awbs, AcceptFreight_SessionState.CurrentAcceptFreightTask.Pcs);
            txtLocations.Text = "Location: " + AcceptFreight_SessionState.CurrentAcceptFreightTask.Locations;
            txtDate.Text = string.Format("Date: {0:MM/dd/yyyy HH:mm}", AcceptFreight_SessionState.CurrentAcceptFreightTask.Date);
            txtProgress.Text = string.Format("{0:0}%", AcceptFreight_SessionState.CurrentAcceptFreightTask.Progress);
            txtShipper.Text = "Shipper: " + AcceptFreight_SessionState.CurrentAcceptFreightTask.Shipper;

            SealTypeSpinner = FindViewById<Spinner>(Resource.Id.txtSealType);
            btnNext = FindViewById<Button>(Resource.Id.btnNext);
            btnNext.Click += OnNextButton_Click;

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",AcceptFreight_SessionState.CurrentAcceptFreightTask.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }

            try
            {
                if (!string.IsNullOrEmpty(AcceptFreight_SessionState.CurrentAcceptFreightTask.DriverImageThumbnail)) {

                    Byte[] image  = System.Convert.FromBase64String(AcceptFreight_SessionState.CurrentAcceptFreightTask.DriverImageThumbnail);
                    personImage.SetImageBitmap (BitmapFactory.DecodeByteArray (image, 0, image.Length));
                }
                else
                {
                    personImage.SetImageResource (Resource.Drawable.person);

                }
            }
            catch
            {
                personImage.SetImageResource (Resource.Drawable.person);
            }
 

           
 

        }


        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem(GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Camera:
                    GoToCamera(typeof( AcceptFreight_SealActivity),typeof( AcceptFreight_SealActivity ),AcceptFreight_SessionState.CurrentAcceptFreightTask.TaskId);
                    break;
            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();
 
            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();
 
            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}

