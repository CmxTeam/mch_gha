﻿using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class AcceptFreight_MainManuActivity : BaseActivity
    {

        AcceptFreightTasks AcceptFreightTasks;
       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.AcceptFreight_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();
        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                GoToTask(AcceptFreightTasks.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(AcceptFreightTaskItem task)
        {
            AcceptFreight_SessionState.CurrentAcceptFreightTask = task;
            StartActivity (typeof(AcceptFreight_ActionActivity));
            this.Finish();
        }

        private void OnImageClickAction(int position)
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new ImageDialog(this,AcceptFreightTasks.Data[position].DriverImageThumbnail );
                dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            search.RequestFocus ();
        }

        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
 
                Action<int> ImageClickAction = OnImageClickAction;
                AcceptFreightStatusTypes status =    EnumHelper.GetEnumItem<AcceptFreightStatusTypes>(DropDownText.Text);
                AcceptFreightTasks = MCH.Communication.AcceptFreight.Instance.GetAcceptFreightTasks(ApplicationSessionState.SelectedWarehouseId, ApplicationSessionState.User.Data.UserId, status);

                RunOnUiThread (delegate {
                    
                    if(AcceptFreightTasks.Transaction.Status)
                    {
                        if(AcceptFreightTasks.Data!=null)
                        {
                            AcceptFreightTasks.Data =  LinqHelper.Query<AcceptFreightTaskItem>(AcceptFreightTasks.Data,searchData);
                                titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",AcceptFreightTasks.Data.Count);
                            listView.Adapter = new AcceptFreight_MainManuAdapter(this, AcceptFreightTasks.Data,ImageClickAction);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;                          
                        }
                    }

                    if(isBarcode)
                    {
                        EditTextListener.Text = string.Empty;
                        search.RequestFocus ();
                        if( AcceptFreightTasks.Data.Count == 1)
                        {
                            GoToTask( AcceptFreightTasks.Data[0]);
                            return;
                        }
                    }
                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

    }
}

