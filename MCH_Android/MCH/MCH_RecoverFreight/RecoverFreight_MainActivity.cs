﻿
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFreight_MainActivity : BaseActivity
    {

        RecoverTaskList Tasks;
       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFreight_MainLayout);
            Initialize();

            RefreshData(string.Empty,false);
            search.RequestFocus ();
            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;
        }


        private void DoBack()
        {
            GotoMainMenu();
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                GoToTask(Tasks.Data[e.Position]);  
            }
            catch(Exception ex)
            {
                MessageBox m = new MessageBox(this);
                m.ShowMessage(ex.Message);
            }
        }

        private void GoToTask(RecoverTaskItem task)
        {
            RecoverFreight_SessionState.CurrentRecoverTask =task;
            StartActivity (typeof(RecoverFreight_RecoverActivity));
            this.Finish();
        }



        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
            {
 

                RecoverFreightStatusTypes status =    EnumHelper.GetEnumItem<RecoverFreightStatusTypes>(DropDownText.Text);
 
                 


                    Tasks =    MCH.Communication.RecoverFreight.Instance.GetCargoReceiverFlights(ApplicationSessionState.SelectedWarehouse, ApplicationSessionState.User.Data.UserId, status);

                RunOnUiThread (delegate {
                    
                    if(Tasks.Transaction.Status && Tasks.Data!=null)
                    {

                            Tasks.Data =  LinqHelper.Query<RecoverTaskItem>(Tasks.Data,searchData);
                            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper() + string.Format(" - ({0})",Tasks.Data.Count);
                            listView.Adapter = new RecoverFreight_MainAdapter(this, Tasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;                          
                       
                    }
                    else
                    {
                        Tasks.Data = new List<RecoverTaskItem>();
                            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper() + string.Format(" - ({0})",Tasks.Data.Count);
                        listView.Adapter = new RecoverFreight_MainAdapter(this, Tasks.Data);
                        listView.ItemClick -= OnListItemClick;
                        listView.ItemClick += OnListItemClick;     
                    }



                        if( Tasks.Data.Count == 1)
                        {

                            if(isBarcode)
                            {
                                EditTextListener.Text = string.Empty;
                                search.RequestFocus ();
                                GoToTask( Tasks.Data[0]);
                            } 
                            return;
                        }
                            else  if( Tasks.Data.Count == 0)
                            {
                             


                            RecoverTaskList list=  MCH.Communication.RecoverFreight.Instance.GetFlightManifestByShipment(ApplicationSessionState.SelectedWarehouseId, searchData ,  ApplicationSessionState.User.Data.UserId);
                                if (list.Transaction.Status)
                                {
                                    if(list.Data != null && list.Data.Count > 0 )
                                    {
                                        Tasks.Data =  list.Data;

                                        titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",Tasks.Data.Count);
                                        listView.Adapter = new RecoverFreight_MainAdapter(this, Tasks.Data);
                                        listView.ItemClick -= OnListItemClick;
                                        listView.ItemClick += OnListItemClick; 


                                        if(list.Data.Count == 1)
                                        {
                                            //GoToTask(Tasks.Data[0]);
                                            if(isBarcode)
                                            {
                                                EditTextListener.Text = string.Empty;
                                                search.RequestFocus ();
                                                GoToTask( Tasks.Data[0]);
                                            } 
                                            return;
                                        }


                                         
                                    }
                                    else
                                    {
                                    if(isBarcode)
                                    {
                                        RefreshData(string.Empty,false);
                                    }

                                    }
                                }
                      



                            }
                





                        
                });
 
                RunOnUiThread(() => progressDialog.Hide());
            })).Start();

        }

 
        public void FindFlight()
        {
            List< MCH.Communication.SelectionItem> carriers = new List< MCH.Communication.SelectionItem>();

            Carriers cs =  MCH.Communication.RecoverFreight.Instance.GetAvailableCarriers(ApplicationSessionState.SelectedWarehouse, ApplicationSessionState.User.Data.UserId);
            if (!cs.Transaction.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(cs.Transaction.Error, MessageBox.AlertType.Error);
                return;
            }


            foreach (var c in cs.Data)
            {
                carriers.Add(new SelectionItem(c.Name,c.Id,@"Airlines/" + c.Name));
            }

            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Select a Carrier", this, carriers, OnCarrierSelection, SelectionListAdapter.SelectionMode.SingleSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnCarrierSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                List< MCH.Communication.SelectionItem> etas = new List< MCH.Communication.SelectionItem>();

                Etas es =  MCH.Communication.RecoverFreight.Instance.GetAvailableEtas(r.Id, ApplicationSessionState.SelectedWarehouse, ApplicationSessionState.User.Data.UserId);
                if (!es.Transaction.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(es.Transaction.Error, MessageBox.AlertType.Error);
                    return;
                }


                foreach (var e in es.Data)
                {
                    etas.Add(new SelectionItem(e.ToString(),r.Id,Resource.Drawable.Clock));
                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity("Select a ETA", this, etas, OnEtaSelection, SelectionListAdapter.SelectionMode.SingleSelection, false);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;
            }
        }

        private void OnEtaSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                List< MCH.Communication.SelectionItem> flights = new List< MCH.Communication.SelectionItem>();

                FlightTasks fs =  MCH.Communication.RecoverFreight.Instance.GetAvailableFlightsByEta(r.Id,DateTime.Parse(r.Name), ApplicationSessionState.SelectedWarehouse, ApplicationSessionState.User.Data.UserId);
                if (!fs.Transaction.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(fs.Transaction.Error, MessageBox.AlertType.Error);
                    return;
                }

                foreach (var f in fs.Data)
                {
                    flights.Add(new SelectionItem(f.Name,f.TaskId,Resource.Drawable.Plane));
                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity("Select Flight", this, flights, OnFlightSelection, SelectionListAdapter.SelectionMode.SingleSelection, false);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;
            }
        }

        private void OnFlightSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.LinkTaskToUserId(r.Id, ApplicationSessionState.User.Data.UserId);
                if (!t.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }
  
                Tasks =    MCH.Communication.RecoverFreight.Instance.GetCargoReceiverFlights(ApplicationSessionState.SelectedWarehouse, ApplicationSessionState.User.Data.UserId,RecoverFreightStatusTypes.Open);
                if (Tasks.Transaction.Status)
                {
                    foreach(var task in Tasks.Data)
                    {
                        if (task.TaskId == r.Id)
                        {
                            GoToTask(task);  
                            return;
                        }
                    }
                }




                return;
            }
        }
    }
}

