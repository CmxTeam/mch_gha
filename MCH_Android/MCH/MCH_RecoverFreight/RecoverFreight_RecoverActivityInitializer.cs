﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using  MCH.Communication;
using Android.Graphics;

namespace MCH
{

    public partial class RecoverFreight_RecoverActivity : BaseActivity
    {
        TextView headerText; 
        ImageView optionButton;
        ImageView backButton;
        ImageView headerImage;
        LinearLayout header;



        TextView txtFlight;
        TextView txtETA;
        TextView txtAwbs;
        TextView txtUlds;
        TextView txtPcs;
        TextView txtRecover;
        TextView txtRecoverBy;
        LinearLayout Indicator;
        ImageView RowIcon;
        ImageView airlineImage;

        //Button btnEdit;
        Button btnRecover;
 
        private void Initialize()
        {
            headerImage = FindViewById<ImageView> (Resource.Id.HeaderImage);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            headerText = FindViewById<TextView>(Resource.Id.HeaderText);
            header = FindViewById<LinearLayout>(Resource.Id.Header);

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


           //headerText.Text = ApplicationSessionState.SelectedMenuItem.Name;
            headerText.Text = "RECOVER FLIGHT";

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                headerImage.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }

 
            //Find here new controls
 
            txtFlight=FindViewById<TextView>(Resource.Id.txtFlight);
            txtETA=FindViewById<TextView>(Resource.Id.txtETA);
            txtAwbs=FindViewById<TextView>(Resource.Id.txtAwbs);
            txtUlds=FindViewById<TextView>(Resource.Id.txtUlds);
            txtPcs=FindViewById<TextView>(Resource.Id.txtPcs);
            txtRecover=FindViewById<TextView>(Resource.Id.txtRecover);
            txtRecoverBy=FindViewById<TextView>(Resource.Id.txtRecoverBy);
            RowIcon= FindViewById<ImageView>(Resource.Id.RowIcon);
            airlineImage= FindViewById<ImageView>(Resource.Id.airlineImage);
            Indicator = FindViewById<LinearLayout>(Resource.Id.Indicator);
 
            //btnEdit = FindViewById<Button>(Resource.Id.btnEdit);
            btnRecover = FindViewById<Button>(Resource.Id.btnRecover);



  

        }



        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem("Summary",OptionActions.Info , Resource.Drawable.Info));
            options.Add(new OptionItem(GetText(Resource.String.Finalize),OptionActions.Finalize, Resource.Drawable.Validate));
            options.Add(new OptionItem("Update ETA",OptionActions.Update, Resource.Drawable.Calendar));
            options.Add(new OptionItem(GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Camera:
                    GoToCamera(typeof( RecoverFreight_RecoverActivity),typeof( RecoverFreight_RecoverActivity ),RecoverFreight_SessionState.CurrentRecoverTask.TaskId);
                    break;
                case OptionActions.Search:
                    
                    StartActivity (typeof(RecoverFreight_SearchActivity));
                    this.Finish();
                    break;
                case OptionActions.Update:
                    UpdateEta();
                    break;
                case OptionActions.Finalize:
                    Finalize();
                    break;
                case OptionActions.Info:

                    RecoverFreight_SummaryActivity.BackScreen = typeof(RecoverFreight_RecoverActivity);
                    StartActivity (typeof(RecoverFreight_SummaryActivity));
                    this.Finish();


                    break;




            }

        }

        private const int ETA_TIME = 1;
        private const int ETA_DATE = 2;
        private void UpdateEta()
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem("Update ETA Date",OptionActions.Date, Resource.Drawable.Calendar));
            options.Add(new OptionItem("Update ETA Time",OptionActions.Time, Resource.Drawable.Clock));
 
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OnUpdateEta, "Update ETA");
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void Finalize()         {              MessageBox m = new MessageBox(this);             m.OnConfirmationClick += (bool result) =>                 {                     if (result)                     {                          DoFinalize();                      }                 };             m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);          }


        private void DoFinalize()
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.FinalizeReceiver( RecoverFreight_SessionState.CurrentRecoverTask.TaskId, RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            StartActivity (typeof(RecoverFreight_MainActivity));
                            this.Finish();
                        }
                        else
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowMessage(t.Error);
                        }
        }



        private void OnUpdateEta(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Time:
                    ShowDialog(ETA_TIME);
                    break;
                case  OptionActions.Date:
                    ShowDialog(ETA_DATE);
                    break;
            }

        }



        protected override Dialog OnCreateDialog (int id)
        {
            DateTime eta = RecoverFreight_SessionState.CurrentRecoverTask.GetEta();
            if (id == ETA_TIME)
                return new TimePickerDialog (this, TimePickerCallback, eta.Hour,  eta.Minute, false);

            if (id == ETA_DATE)
                return new DatePickerDialog (this, DatePickerCallback, eta.Year, eta.Month -1, eta.Day); 

            return null;
        }

        void DatePickerCallback (object sender, DatePickerDialog.DateSetEventArgs e)
        {
            DateTime eta = RecoverFreight_SessionState.CurrentRecoverTask.GetEta();
            string datetimestring = string.Format("{0:yyyy}-{0:MM}-{0:dd} {1:HH}:{1:mm}", e.Date, eta);
            eta = DateTime.Parse(datetimestring);
            UpdateEta(eta);
        }


        private void TimePickerCallback (object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            DateTime eta = RecoverFreight_SessionState.CurrentRecoverTask.GetEta();
            string datetimestring = string.Format("{0:yyyy}-{0:MM}-{0:dd} {1:00}:{2:00}", eta, e.HourOfDay, e.Minute);
            eta = DateTime.Parse(datetimestring);
            UpdateEta(eta);
        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}


