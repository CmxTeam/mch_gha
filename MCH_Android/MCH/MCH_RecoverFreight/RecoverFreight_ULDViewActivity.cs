﻿ 

using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFreight_ULDViewActivity : BaseActivity
    {

        ShipmentUldView ULDview;
        ValidateScanData scandata;
        public static  string LastScanText;
         
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFreight_ULDViewLayout);
            Initialize();
            RefreshAll();



            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;

        }

        private void RefreshAll()
        {
            LoadData();
            LoadDetail();
            RefreshData(string.Empty,false);
            search.RequestFocus (); 
        }

        private void LoadData()
        {
            RecoverTask data=  MCH.Communication.RecoverFreight.Instance.GetFlightManifestById(RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId);
            if (data.Transaction.Status)
            {
                RecoverFreight_SessionState.CurrentRecoverTask = data.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(data.Transaction.Error, MessageBox.AlertType.Error);
            }
        }

        private void DoBack()
        {
            //StartActivity (typeof(RecoverFreight_ScanActivity));
            StartActivity (typeof(RecoverFreight_ULDActivity));
 
            this.Finish();
        }


        private void btnDrop_Click(object sender, EventArgs e)         {             if (GetForkLiftCount() == 0)             {                 MessageBox m = new MessageBox(this);                 m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);                 return;             }                         MessageBox mb = new MessageBox(this);             mb.OnConfirmationClick += (bool result) =>                  {                     if(result)                     {                               var transaction = FragmentManager.BeginTransaction();                         var dialogFragment = new LocationDialogActivity(this, DropForkliftPieces, "Location","Select Location",true, LocationTypes.Area);                             dialogFragment.Cancelable = false;                             dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);                       }                 };             mb.ShowConfirmationMessage("Are you sure you want to drop all items from your forklift?");           }         private void DropForkliftPieces(Barcode  barcode)         {             CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.DropForkliftPieces(RecoverFreight_SessionState.CurrentRecoverTask.TaskId ,ApplicationSessionState.User.Data.UserId  ,barcode.Id );             if (t.Status)             {                 RefreshAll();               }             else             {                 MessageBox mb = new MessageBox(this);                 mb.ShowAlert(t.Error, MessageBox.AlertType.Error);             }          } 
        private int GetForkLiftCount()
        {
 

            try
            {
                ForkLiftCount fl = MCH.Communication.RecoverFreight.Instance.GetForkliftCount(ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId);
                btnFork.Text = fl.Data.ToString();
                return fl.Data;
            }
            catch
            {
                btnFork.Text = "0";
                return 0;
            }
           
        }

        private void btnFork_Click(object sender, EventArgs e)         {             if (GetForkLiftCount() == 0)             {                 MessageBox m = new MessageBox(this);                 m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);                 return;             }              List<ForkLiftSelectionItem > fl = new List< ForkLiftSelectionItem>();                      ForkLiftView fv =  MCH.Communication.RecoverFreight.Instance.GetForkliftView( RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId );             if (fv.Transaction.Status && fv.Data!=null)             {                  foreach (var i in fv.Data)                 {                     fl.Add(new ForkLiftSelectionItem( i.Reference,i.DetailId,i.ForkliftPieces,MCH.Communication.ReferenceTypes.AWB ));                 }                  var transaction = FragmentManager.BeginTransaction();                 var dialogFragment = new RecoverFreight_ForkLiftDialog(  this, fl);                 dialogFragment.Cancelable = false;                 dialogFragment.OkClicked += (List<ForkLiftSelectionItem> selection,long locationId) =>                      {                          if(selection==null)                         {                             RefreshAll();                         }                         else                         {                             if(locationId==0)                             {                                  DropSelectedPieces(selection);                             }                             else                             {                                  foreach(var s in selection)                                 {                                     CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.DropPiecesToLocation(RecoverFreight_SessionState.CurrentRecoverTask.TaskId ,ApplicationSessionState.User.Data.UserId ,locationId,s.DetailId,s.Pieces);                                     if (!t.Status)                                     {                                         MessageBox mb = new MessageBox(this);                                         mb.ShowAlert(t.Error, MessageBox.AlertType.Error);                                     }                                 }                                 RefreshAll();                              }                          }                                                          };                 dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);                 return;              }                               } 
        private void DropSelectedPieces(List<ForkLiftSelectionItem> selection)
        {
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new LocationDialogActivity(this, null, "Location","Select Location",true, LocationTypes.Area);
            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked += (Barcode barcode) => 
                {
                    foreach(var s in selection)
                    {
                        CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.DropPiecesToLocation(RecoverFreight_SessionState.CurrentRecoverTask.TaskId ,ApplicationSessionState.User.Data.UserId ,barcode.Id,s.DetailId,s.Pieces);
                        if (!t.Status)
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                        }
                    }
                    RefreshAll(); 
                };
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

          
        }

        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ShipmentUldViewItem item = ULDview.Data[e.Position];


//            var newScreen = new Intent(this, typeof(RecoverFreight_ScanActivity));
//            newScreen.PutExtra("Barcode", item.AWB  );
//            StartActivity(newScreen);
//            this.Finish();


            ValidateShipmentScan (item.AWB, item.UldId);

               
            
        }

        private static string CurrentSelectedReference = string.Empty;
        private static long CurrentSelectedUldId = 0;
        private void DoRecoverUld(Barcode  barcode)
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.RecoverUld(CurrentSelectedUldId ,ApplicationSessionState.User.Data.UserId  , RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId,barcode.Id );
            if (t.Status)
            {

                if (RecoverFreight_SessionState.CurrentUld.IsBUP)
                {
                   
                    RefreshAll();
                }
                else
                {

                    ValidateShipmentScan (CurrentSelectedReference , CurrentSelectedUldId);
                    //GoToUld();
                }
 
            }
            else
            {
                MessageBox mb = new MessageBox(this);
                mb.ShowMessage(t.Error);
                RefreshAll();
                return;
            }
        }

        private void DoRecover(  )
        {
            
                var transaction = FragmentManager.BeginTransaction();
             var dialogFragment = new LocationDialogActivity(this, DoRecoverUld, "Location","Select Location",true, LocationTypes.Area);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
 
        }

        private void ValidateShipmentScan(string reference, long uldId)
        {


            if (RecoverFreight_SessionState.CurrentUld.UldPrefix.ToLower() == "all")
            {
             
                FlightUldView uld = MCH.Communication.RecoverFreight.Instance.GetFlightULD(uldId , RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId);
                try
                {
                    if(!uld.Data.Ulds[0].IsRecovered)
                    {
                        MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) =>
                    {
                        if (result)
                        {
                            CurrentSelectedUldId = uldId;
                            CurrentSelectedReference =reference;
                            DoRecover();
                        }
                        else
                        {
                                    RefreshData(string.Empty,false);
                        }
                    };
                    m.ShowConfirmationMessage("Are you sure you want to recover this ULD?.");
                        return;
                    }
 
                }
                catch
                {
                } 

            }
 

            scandata =  MCH.Communication.RecoverFreight.Instance.ValidateShipment(ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, uldId, reference);
 
            if (scandata.Transaction.Status)
            {

                EditTextListener.Text = string.Empty;

                if (scandata.Data.DetailId > 0)
                {
                    if (scandata.Data.AvailablePieces > 0)
                    {

                        var transaction = FragmentManager.BeginTransaction();
                        var dialogFragment = new PiecesDialog(this,scandata.Data.Awb,0 ,scandata.Data.AvailablePieces );
                        dialogFragment.Cancelable = false;
                        dialogFragment.OkClicked += OnPiecesClickAction;
                        //dialogFragment.OverpackClicked  += OnOverpackClickAction;

                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 

                    }
                    else
                    {
                        MessageBox m = new MessageBox(this);
                        m.ShowAlert("There are no pieces available.", MessageBox.AlertType.Information);
                        RefreshData(string.Empty,false);
                    }
                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert("Unable to validate shipment.", MessageBox.AlertType.Information);
                    RefreshData(string.Empty,false);
                }



            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(scandata.Transaction.Error, MessageBox.AlertType.Error);
            }

        }


//        private void OnOverpackClickAction(int pieces)
//        {
//            if (pieces > 0)
//            {
//                OverPackTask task = MCH.Communication.Overpack.Instance.GetOverPackTask(ApplicationSessionState.SelectedWarehouseId,0,  ApplicationSessionState.User.Data.AppUserId,scandata.Data.Awb);
//                if (!task.Transaction.Status)
//                {
//                    MessageBox mb = new MessageBox(this);
//                    mb.ShowAlert(task.Transaction.Error, MessageBox.AlertType.Error);
//                }
//                else
//                {
//                    OverPackItems items = MCH.Communication.Overpack.Instance.GetOverPackItems(task.Data.OverPackTaskId);
//                    if (items.Transaction.Status == true && items.Data != null)
//                    {
//
//                        if (items.Data.Count == 0)
//                        {
//                            //go to add overpack
//                        }
//                        else
//                        {
//                            List<SelectionItem> list  = new List<SelectionItem>();
//                            list.Add(new SelectionItem("ADD OVERPACK" ,0,Resource.Drawable.Plus));
//
//
//                            foreach (var n in items.Data)
//                            {
//                                list.Add(new SelectionItem(String.Format("{0} {1}     PCS: {2}",  n.OverpackType, n.OverPackItemNumber,n.Pieces ) ,n.OverPackItemId,Resource.Drawable.Cube));
//                            }
//
//                            var transaction = FragmentManager.BeginTransaction();
//                            var dialogFragment = new SelectionDialogActivity("REF#: " + scandata.Data.Awb, this, list, SelectionListAdapter.SelectionMode.SingleSelection, false);
//                            dialogFragment.Cancelable = false;
//                            dialogFragment.OnSelectionClick += (List<SelectionItem> selection, bool isExtra) => 
//                                {
//                                    foreach (var r in selection)
//                                    {
//                                        CommunicationTransaction t;
//                                        if(r.Id==0)
//                                        {
//                                            //go to add overpack
//                                        }
//                                        else
//                                        {
//
//
//                                            foreach (var n in items.Data)
//                                            {
//                                                if(n.OverPackItemId == r.Id)
//                                                {
//                                                    t = MCH.Communication.Overpack.Instance.EditOverpack(task.Data.OverPackTaskId,n.OverPackItemId,ApplicationSessionState.User.Data.AppUserId,n.OverpackTypeId,n.SkidOwnerType,n.Pieces + pieces);
//
//
//                                                    if (t.Status)
//                                                    {
//                                                        OnPiecesClickAction(pieces);
//                                                        return;
//                                                    }
//                                                    else
//                                                    {
//                                                        MessageBox m = new MessageBox(this);
//                                                        m.ShowAlert(t.Error, MessageBox.AlertType.Error);
//                                                    }
//                                                }
//                                            }
//                                     
//
//
//                                        }
// 
//                                    }
//
//                                };
//                            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
//                        }
//
//
//
//
//
//
//                    }
//                    else
//                    {
//                        MessageBox mb = new MessageBox(this);
//                        mb.ShowAlert(items.Transaction.Error, MessageBox.AlertType.Error);
//                    }
//                }
//            }
//
//        }

        private void OnPiecesClickAction(int pieces)
        {
            if (pieces > 0)
            {
    
                CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.AddForkliftPieces(scandata.Data.DetailId , RecoverFreight_SessionState.CurrentRecoverTask.TaskId, pieces, ApplicationSessionState.User.Data.UserId);

                if (!t.Status)
                {
                    MessageBox mb = new MessageBox(this);
                    mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }
                else
                {
                    MediaSounds.Instance.Beep(this);
                }
                LastScanText = scandata.Data.Awb;
                RefreshAll();  
            }

        }

        public void PrintItems()
        {
            OverPackItems list;
            list = MCH.Communication.Overpack.Instance.GetOverPackItems(Overpack_SessionState.CurrentOverpackTask.OverPackTaskId);

 
            if (list.Transaction.Status == true && list.Data != null)
            {
 


            int loose = Overpack_SessionState.CurrentOverpackTask.TotalPieces - Overpack_SessionState.CurrentOverpackTask.TotalSkidPieces;
            if (loose > 0)
            {

                    string str = string.Empty;
                    foreach (var s in list.Data)
                    {
                        if (str == string.Empty)
                        {
                            str = s.OverpackType.ToString() + s.OverPackItemNumber; 
                        }
                        else
                        {
                            str = ", " + s.OverpackType.ToString() + s.OverPackItemNumber; 
                        }
                    }

                    string r = string.Empty;
                    if (str == string.Empty)
                    {
                        r = Overpack_SessionState.CurrentOverpackTask.Reference; 
                    }
                    else
                    {
                        r = Overpack_SessionState.CurrentOverpackTask.Reference + "\n" + str; 
                    }

                var transaction = this.FragmentManager.BeginTransaction();
                var dialogFragment = new CopiesDialog(this, r, GetDefautPrinter(), loose, "ENTER NUMBER OF LOOSE COPIES:");
                dialogFragment.OkClicked += (int pieces) =>
                {


                    foreach (OverPackItem o in list.Data)
                    {
                        int copies = 0;
                        if (o.OverpackType.ToUpper() == "LOOSE")
                        {
                            copies = pieces;
                        }
                        else
                        {
                            copies = 1;
                        }

                                CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Overpack_SessionState.CurrentOverpackTask.HwbId, Overpack_SessionState.CurrentOverpackTask.AwbId, ApplicationSessionState.User.Data.UserId, o.OverPackItemId, copies);
                        if (!t.Status)
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                            return;
                        }


                    }
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert("Labels printed successfully.", MessageBox.AlertType.Information);




                };
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 



            }
            else
            {


                MessageBox msg = new MessageBox(this);
                msg.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {
                        foreach (OverPackItem o in list.Data)
                        {
                                    CommunicationTransaction t = MCH.Communication.Miscellaneous.Instance.PrintLabel(Overpack_SessionState.CurrentOverpackTask.HwbId, Overpack_SessionState.CurrentOverpackTask.AwbId, ApplicationSessionState.User.Data.UserId, o.OverPackItemId, 1);
                            if (!t.Status)
                            {
                                MessageBox mb = new MessageBox(this);
                                mb.ShowAlert(t.Error, MessageBox.AlertType.Error);
                                return;
                            }
                        }
                        MessageBox m = new MessageBox(this);
                        m.ShowAlert("Labels printed successfully.", MessageBox.AlertType.Information);
                    }

                };
                msg.ShowConfirmationMessage("Are you sure you want to print all labels", "YES", "NO");



            }




        }
        else
        {
            //error
        }

        }
        private void Print(ShipmentUldViewItem items)
        {
            OverPackTask task = MCH.Communication.Overpack.Instance.GetOverPackTask(ApplicationSessionState.SelectedWarehouseId,0,  ApplicationSessionState.User.Data.UserId,items.AWB);
            if (task.Transaction.Status == true && task.Data != null)
            {
                Overpack_SessionState.CurrentOverpackTask = task.Data;
                PrintItems();
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(task.Transaction.Error, MessageBox.AlertType.Error);
            }

        }


        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {

                    RecoverFreightStatusTypes status =  EnumHelper.GetEnumItem<RecoverFreightStatusTypes>(DropDownText.Text);

                    ULDview = MCH.Communication.RecoverFreight.Instance.GetUldView(RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentUld.UldId,status);
                    RunOnUiThread (delegate {

                        if(ULDview.Transaction.Status)
                        {
                            if(ULDview.Data!=null)
                            {
                                //ShipmentUldViewItem
                                ULDview.Data =  LinqHelper.Query<ShipmentUldViewItem>(ULDview.Data,searchData);
                                titleLabel.Text = "RECOVER AWBS" + string.Format(" - ({0})",ULDview.Data.Count);
                                var adapter = new RecoverFreight_ULDViewAdapter(this, ULDview.Data,status);
                                adapter.OkOverPack  += (ShipmentUldViewItem items) => 
                                    {
                                        GoToOverpack(typeof(RecoverFreight_ULDViewActivity),0,items.AWB);
                                    };
                                adapter.OnPrint += Print;
                                listView.Adapter = adapter;
                                listView.ItemClick -= OnListItemClick;
                                listView.ItemClick += OnListItemClick;                          
                            }
                        }
                        else
                        {
                            ULDview.Data = new List<ShipmentUldViewItem>();
                            titleLabel.Text = "RECOVER AWBS" + string.Format(" - ({0})",ULDview.Data.Count);
                             
                            var adapter = new RecoverFreight_ULDViewAdapter(this, ULDview.Data,status);
                            adapter.OkOverPack  += (ShipmentUldViewItem items) => 
                                {
                                    GoToOverpack(typeof(RecoverFreight_ULDViewActivity),0,items.AWB);
                                };
                            adapter.OnPrint += Print;
                            listView.Adapter = adapter;
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;     
                        }

                        if(isBarcode)
                        {
                            EditTextListener.Text = string.Empty;
                            search.RequestFocus ();
                            if( ULDview.Data.Count == 1)
                            {

                                ValidateShipmentScan (ULDview.Data[0].AWB, ULDview.Data[0].UldId);
                        

                                return;
                            }
                            else if( ULDview.Data.Count == 0)
                            {
                                FlightView  ulds =    MCH.Communication.RecoverFreight.Instance.GetShipmentUldView(RecoverFreight_SessionState.CurrentRecoverTask.TaskId,RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId, searchData );
                                if(ulds.Data !=null && ulds.Transaction.Status )
                                {
                                    if(ulds.Data.Count == 1)
                                    {

                                        if(RecoverFreight_SessionState.CurrentUld.UldId == 0 )
                                        {
                                            ValidateShipmentScan (ulds.Data[0].AWB, ulds.Data[0].UldId);
                                        }
                                        else
                                        {
                                            if(RecoverFreight_SessionState.CurrentUld.UldId == ulds.Data[0].UldId)
                                            {
                                                    ValidateShipmentScan (ulds.Data[0].AWB, ulds.Data[0].UldId);
                                            }
                                            else
                                            {
                                                    RefreshData(string.Empty,false);
                                            }
                                        }

                                     
                                    }
                                    else
                                    {
                                        RefreshData(string.Empty,false);
                                    }
                                }
 

                                return;
                            }
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }



    }
}



