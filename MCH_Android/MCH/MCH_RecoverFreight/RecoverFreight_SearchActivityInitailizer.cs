﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MCH.Communication;

namespace MCH
{

    public partial class RecoverFreight_SearchActivity : BaseActivity
    {

        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;
        EditText search;
        TextView titleLabel; 
        ImageView optionButton;
        ImageView backButton;
        ImageView imageHeader;

 
        ListView listView;
  
        TextView txtFlight;
        TextView txtETA;
        TextView txtAwbs;
        TextView txtUlds;
        TextView txtPcs;
        TextView txtRecover;
        TextView txtRecoverBy;
        LinearLayout Indicator;
        ImageView RowIcon;
        ImageView airlineImage;

 

        private void Initialize()
        {

            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            search = FindViewById<EditText>(Resource.Id.SearchText);
            EditTextListener = new EditTextEventListener(search);
            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
                {
                    string data = e.Data;
                    if (e.IsBarcode)
                    {
                        ValidateShipment(data);
                    }
                    else
                    {
                        RefreshData(data,e.IsBarcode); 
                    }
                  
                };
 
            ClearSearch = FindViewById<ImageButton>(Resource.Id.ClearSearchButton);
            SearchButton = FindViewById<ImageButton>(Resource.Id.SearchButton);
            titleLabel = FindViewById<TextView>(Resource.Id.HeaderText);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            imageHeader = FindViewById<ImageView>(Resource.Id.HeaderImage);
 
            listView = FindViewById<ListView>(Resource.Id.GridControl);

            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name.ToUpper();

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;


            ClearSearch.Click += OnClearSearch_Click;

            SearchButton.Click += OnSearchButton_Click;

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                imageHeader.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }

            //Find here new controls
 
            txtFlight=FindViewById<TextView>(Resource.Id.txtFlight);
            txtETA=FindViewById<TextView>(Resource.Id.txtETA);
            txtAwbs=FindViewById<TextView>(Resource.Id.txtAwbs);
            txtUlds=FindViewById<TextView>(Resource.Id.txtUlds);
            txtPcs=FindViewById<TextView>(Resource.Id.txtPcs);
            txtRecover=FindViewById<TextView>(Resource.Id.txtRecover);
            txtRecoverBy=FindViewById<TextView>(Resource.Id.txtRecoverBy);

            txtFlight.Text =string.Format("{0} {1} {2}",RecoverFreight_SessionState.CurrentRecoverTask.Origin,RecoverFreight_SessionState.CurrentRecoverTask.CarrierCode, RecoverFreight_SessionState.CurrentRecoverTask.FlightNumber);
            txtETA.Text = string.Format("ETA: {0:dd-MMM-yy HH:mm}",RecoverFreight_SessionState.CurrentRecoverTask.ETA).ToUpper();
            txtAwbs.Text = string.Format("AWBS: {0}", RecoverFreight_SessionState.CurrentRecoverTask.AWBCount);
            txtUlds.Text= string.Format("ULDS: {0} of {1}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredULDs,RecoverFreight_SessionState.CurrentRecoverTask.ULDCount);
            txtPcs.Text= string.Format("PCS: {0} of {1}", RecoverFreight_SessionState.CurrentRecoverTask.ReceivedPieces ,RecoverFreight_SessionState.CurrentRecoverTask.TotalPieces );
            txtRecover.Text= string.Format("RECOVERED: {0:dd-MMM-yy HH:mm}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredDate).ToUpper();
            txtRecoverBy.Text= string.Format("BY: {0}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredBy).ToUpper();

            RowIcon= FindViewById<ImageView>(Resource.Id.RowIcon);
            airlineImage= FindViewById<ImageView>(Resource.Id.airlineImage);
            Indicator = FindViewById<LinearLayout>(Resource.Id.Indicator);
 
            IndicatorAdapter indicatorObj = new IndicatorAdapter(this, Indicator);
            indicatorObj.Load(RecoverFreight_SessionState.CurrentRecoverTask.Flag);

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",RecoverFreight_SessionState.CurrentRecoverTask.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }


            switch (RecoverFreight_SessionState.CurrentRecoverTask.Status)
            {
                case RecoverFreightStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case RecoverFreightStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case RecoverFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

  
        }

        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            RefreshData(string.Empty,false);
            EditTextListener.Text = string.Empty;
            search.RequestFocus ();
        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {
            RefreshData(search.Text,false);
        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

  
  

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();

         
            options.Add(new OptionItem(GetText(Resource.String.Refresh),OptionActions.Refresh, Resource.Drawable.Refresh));
            options.Add(new OptionItem(GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Refresh:
                    search.Text = string.Empty;
                    RefreshData(string.Empty, false);
                    search.RequestFocus();
                    break;
                case  OptionActions.Camera:
                    GoToCamera(typeof( RecoverFreight_SearchActivity),typeof( RecoverFreight_SearchActivity ),RecoverFreight_SessionState.CurrentRecoverTask.TaskId);
                    break;


            }

        }


        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}

