﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class RecoverFreight_ULDAdapter : BaseAdapter<UldViewItem> {

        List<UldViewItem> items ;
        Activity context;

        public delegate void OkRefreshEventHandler();
        public event OkRefreshEventHandler OkRefresh;

        public RecoverFreight_ULDAdapter(Activity context, List<UldViewItem> items)
            : base()
        {
            this.context = context;



            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override UldViewItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {


            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new


            if (item.UldPrefix.ToLower() == "all")
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFreight_AllRow, null);
                return view;
            }

            view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFreight_ULDRow, null);

     
            TextView txtUld= view.FindViewById<TextView>(Resource.Id.txtUld);
            TextView txtPieces= view.FindViewById<TextView>(Resource.Id.txtPieces);
            TextView txtLocation= view.FindViewById<TextView>(Resource.Id.txtLocation);
 
            LinearLayout btnBup= view.FindViewById<LinearLayout>(Resource.Id.btnBup);
            ImageView imgBup= view.FindViewById<ImageView>(Resource.Id.imgBup);
            TextView txtBup= view.FindViewById<TextView>(Resource.Id.txtBup);

            ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);


            if (item.IsLoose)
            {
                txtUld.Text =string.Format("{0}",item.UldPrefix);
            }
            else
            {
                txtUld.Text =string.Format("{0}{1}",item.UldPrefix,item.UldSerialNo);
            }


            txtPieces.Text =string.Format("PCS: {0} of {1}",item.ReceivedPieces, item.TotalPieces);
            if (item.ReceivedPieces > 0 && item.TotalPieces > item.ReceivedPieces)
            {
                txtPieces.SetTextColor(Color.Red);
            }
            else
            {
                txtPieces.SetTextColor(Color.Black);
            }


            txtLocation.Text =string.Format("LOC: {0}",item.Location);

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);
            if (item.Flag == 0)
            {
                Indicator.Visibility = ViewStates.Gone;
            }
            else
            {
                Indicator.Visibility = ViewStates.Visible;
            }

            if (item.IsBUP)
            {
                txtBup.Text = "BUP";
                imgBup.SetImageResource (Resource.Drawable.ValidateWhite);
            }
            else
            {
                txtBup.Text = "BUP";
                imgBup.SetImageResource (Resource.Drawable.NotValidatedWhite);
            }


            switch (item.Status)
            {
                case UldStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case UldStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case UldStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress);
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            if (item.IsLoose)
            {
                btnBup.Visibility = ViewStates.Gone;  
            }
            else
            {
                btnBup.Visibility = ViewStates.Visible;  
                btnBup.Tag = position.ToString();
                btnBup.Click -= btnBup_Click;
                btnBup.Click += btnBup_Click;  
            }

         
 
            return view;
        }


        public void btnBup_Click(object sender, EventArgs e) 
        {
            LinearLayout btn = (LinearLayout)sender;
            //OnImageClickAction.Invoke (int.Parse(img.Tag.ToString()));
            UldViewItem item = items[int.Parse(btn.Tag.ToString())];



            if (item.IsLoose)
            {
                return;
            }

//            if (item.Location.ToLower() != "n/a" && !string.IsNullOrEmpty(item.Location))
//            {
//                return;
//            }

            if (item.ReceivedPieces > 0)
            {
                return;
            }


            string msg;
            if (item.IsBUP)
            {
                msg = "Are you sure you want to change from BUP to Breakdown?";
            }
            else
            {
                msg = "Are you sure you want to change from Breakdown to BUP?";
            }
                MessageBox m = new MessageBox(this.context);
                m.OnConfirmationClick+= (bool result) => 
                    {
                        if(result)
                        {

                        CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.SwitchBUPMode(ApplicationSessionState.SelectedWarehouse,item.UldId ,ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId   );
                            if (!t.Status)
                            {
                                MessageBox m1 = new MessageBox(this.context);
                                m1.ShowAlert(t.Error, MessageBox.AlertType.Error);
                            }
                            else
                            {
                                item.IsBUP =  !item.IsBUP;
                                this.NotifyDataSetChanged();
                           


                            if (item.IsBUP && item.Location.ToLower() != "n/a" && !string.IsNullOrEmpty(item.Location))
                                        {
                                if (OkRefresh != null)
                                    OkRefresh.Invoke();
                                        }

                 

                            }

                            

                        }
                    };
                m.ShowConfirmationMessage(msg);
       


        }



    }
}





 

