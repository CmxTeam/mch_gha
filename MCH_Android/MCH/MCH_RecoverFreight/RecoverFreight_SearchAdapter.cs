﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class RecoverFreight_SearchAdapter : BaseAdapter<FlightViewItem> {

        List<FlightViewItem> items;
        Activity context;



        public RecoverFreight_SearchAdapter(Activity context, List<FlightViewItem> items): base()
        {

            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override FlightViewItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {




            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new
            view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFreight_SearchRow, null);

            TextView txtAwb= view.FindViewById<TextView>(Resource.Id.txtAwb);
            TextView txtULD= view.FindViewById<TextView>(Resource.Id.txtULD);
            TextView txtPieces= view.FindViewById<TextView>(Resource.Id.txtPieces);
            TextView txtLocations= view.FindViewById<TextView>(Resource.Id.txtLocations);

            //ImageView RowIcon= view.FindViewById<ImageView>(Resource.Id.RowIcon);
            LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);
             

            txtAwb.Text =string.Format("AWB: {0} {1} {2}",item.Origin , item.AWB, item.Destination);
            txtULD.Text =string.Format("ULD: {0}",item.Uld);
            txtPieces.Text =string.Format("PCS: {0} of {1}",item.ReceivedPieces, item.TotalPieces);
            txtLocations.Text =string.Format("LOC: {0}",item.Locations);

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);

            if (item.Flag == 0)
            {
                Indicator.Visibility = ViewStates.Gone;
            }
            else
            {
                Indicator.Visibility = ViewStates.Visible;
            }

  

            return view;
        }






    }
}




