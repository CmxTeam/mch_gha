﻿ 
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFreight_RecoverActivity : BaseActivity
    {
         
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFreight_RecoverLayout);
            Initialize();
            LoadData();
            LoadDetail();
        }

        private void LoadData()
        {
            RecoverTask task=  MCH.Communication.RecoverFreight.Instance.GetFlightManifestById(RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId);
            if (task.Transaction.Status)
            {
                RecoverFreight_SessionState.CurrentRecoverTask = task.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(task.Transaction.Error, MessageBox.AlertType.Error);
            }
        }


        private void LoadDetail()
        {
            txtFlight.Text =string.Format("{0} {1} {2}",RecoverFreight_SessionState.CurrentRecoverTask.Origin,RecoverFreight_SessionState.CurrentRecoverTask.CarrierCode, RecoverFreight_SessionState.CurrentRecoverTask.FlightNumber);
            txtETA.Text = string.Format("{0:dd-MMM-yy HH:mm}",RecoverFreight_SessionState.CurrentRecoverTask.ETA).ToUpper();
            txtAwbs.Text = string.Format("{0}", RecoverFreight_SessionState.CurrentRecoverTask.AWBCount);
            txtUlds.Text= string.Format("{0} of {1}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredULDs,RecoverFreight_SessionState.CurrentRecoverTask.ULDCount);
            txtPcs.Text= string.Format("{0} of {1}", RecoverFreight_SessionState.CurrentRecoverTask.ReceivedPieces ,RecoverFreight_SessionState.CurrentRecoverTask.TotalPieces );
            txtRecover.Text= string.Format("{0:dd-MMM-yy HH:mm}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredDate).ToUpper();
            txtRecoverBy.Text= string.Format("{0}", RecoverFreight_SessionState.CurrentRecoverTask.RecoveredBy).ToUpper();
 
            IndicatorAdapter indicatorObj = new IndicatorAdapter(this, Indicator);
            indicatorObj.Load(RecoverFreight_SessionState.CurrentRecoverTask.Flag);

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",RecoverFreight_SessionState.CurrentRecoverTask.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }

 
            switch (RecoverFreight_SessionState.CurrentRecoverTask.Status)
            {
                case RecoverFreightStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.NotValidatedWhite);
                    break;
                case RecoverFreightStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.ValidateWhite);
                    break;
                case RecoverFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgressWhite );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.NotValidatedWhite);
                    break;
            }
 
                //btnEdit.Click += btnEdit_Click;
                btnRecover.Click += btnRecover_Click;

            if (string.IsNullOrEmpty(RecoverFreight_SessionState.CurrentRecoverTask.RecoveredBy))
            {
                btnRecover.Text = "RECOVER";
                btnRecover.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.NotValidatedWhite, 0, 0, 0);
            }
            else
            {
                btnRecover.Text = "CONTINUE";
                btnRecover.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ValidateWhite, 0, 0, 0);
            }


            CheckFinilize();


        }

        private void CheckFinilize()
        {
            if (RecoverFreight_SessionState.CurrentRecoverTask.Status != RecoverFreightStatusTypes.Completed && RecoverFreight_SessionState.CurrentRecoverTask.ReceivedPieces == RecoverFreight_SessionState.CurrentRecoverTask.TotalPieces)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DoFinalize();

                    }
                };
                m.ShowConfirmationMessage("All pieces were scanned. Do you want to finalize task?", Resource.String.Yes, Resource.String.No);
            }
        }


        private void DoBack()
        {
            StartActivity (typeof(RecoverFreight_MainActivity));
            this.Finish();
        }

        private void btnRecover_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(RecoverFreight_SessionState.CurrentRecoverTask.RecoveredBy))
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {
                        if(result)
                        {
                            DoRecover();
                            return;    
                        }

                    };
                m.ShowConfirmationMessage("Are you sure you want to recover this flight?");
            }
            else
            {
                GoToUlds();
            }


        }


        private void GoToUlds()
        {
            StartActivity (typeof(RecoverFreight_ULDActivity));
            this.Finish();
        }

        private void DoRecover()
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.RecoverFlight(ApplicationSessionState.SelectedWarehouse,RecoverFreight_SessionState.CurrentRecoverTask .TaskId, ApplicationSessionState.User.Data.UserId);
            if (!t.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
            else
            {
                GoToUlds();
            }
        }

//        private void btnEdit_Click(object sender, EventArgs e)
//        {
//            DateTime eta = DateTime.Now;
//            if(RecoverFreight_SessionState.CurrentRecoverTask.ETA != null)
//            {
//                eta = DateTime.Parse(RecoverFreight_SessionState.CurrentRecoverTask.ETA.ToString());
//            }
//    
//            var transaction = FragmentManager.BeginTransaction();
//            var dialogFragment = new RecoverFreight_EtaDialog(this, UpdateEta,eta );
//            dialogFragment.Cancelable = false;
//            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
//        }

        private void UpdateEta(DateTime eta)
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.UpdateFlightETA(ApplicationSessionState.SelectedWarehouse,RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId ,RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId, eta);
            if (!t.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
            }
            else
            {
                RecoverFreight_SessionState.CurrentRecoverTask.ETA = eta;
               // txtETA.Text = string.Format("ETA: {0:dd-MMM-yy HH:mm}",RecoverFreight_SessionState.CurrentRecoverTask.ETA);
                LoadDetail();
            }
 
        }

    }
}


