﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using MCH.Communication;

namespace MCH
{



    public class RecoverFreight_ULDViewAdapter : BaseAdapter<ShipmentUldViewItem> {

        List<ShipmentUldViewItem> items;
        Activity context;
        RecoverFreightStatusTypes status;
        ShipmentUldViewItem selecteditem;

        public delegate void OkOverPackEventHandler(ShipmentUldViewItem items);
        public event OkOverPackEventHandler OkOverPack;


        public delegate void OkPrintEventHandler(ShipmentUldViewItem items);
        public event OkPrintEventHandler OnPrint;

        public RecoverFreight_ULDViewAdapter(Activity context, List<ShipmentUldViewItem> items, RecoverFreightStatusTypes status): base()
        {
            this.status = status;
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        public override ShipmentUldViewItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }


        public  override View GetView(int position, View convertView, ViewGroup parent)
        {




            var item = items[position];
            View view = convertView;
            //if (view == null) // no view to re-use, create new
            view = context.LayoutInflater.Inflate(Resource.Layout.RecoverFreight_ULDViewRow, null);

            TextView txtAwb= view.FindViewById<TextView>(Resource.Id.txtAwb);
            TextView txtULD= view.FindViewById<TextView>(Resource.Id.txtULD);
            TextView txtPieces= view.FindViewById<TextView>(Resource.Id.txtPieces);
            TextView txtLocations= view.FindViewById<TextView>(Resource.Id.txtLocations);

            
            LinearLayout  Indicator = view.FindViewById<LinearLayout>(Resource.Id.Indicator);

            //Button btnOption = view.FindViewById<Button>(Resource.Id.options);

            LinearLayout btnRelocate= view.FindViewById<LinearLayout>(Resource.Id.btnRelocate); 
            btnRelocate.Click -= OnOptionButton_Click;
            btnRelocate.Click += OnOptionButton_Click;  
            btnRelocate.Tag = position.ToString();
 


            txtAwb.Text =string.Format("AWB: {0} {1} {2}", item.Origin ,item.AWB,item.Destination  );
            txtULD.Text =string.Format("ULD: {0}",item.Uld);
            txtPieces.Text =string.Format("PCS: {0} of {1}",item.ReceivedPieces, item.TotalPieces);

            if (item.ReceivedPieces > 0 && item.TotalPieces > item.ReceivedPieces)
            {
                txtPieces.SetTextColor(Color.Red);
            }
            else
            {
                txtPieces.SetTextColor(Color.Black);
            }

            txtLocations.Text =string.Format("LOC: {0}",item.Locations);

            IndicatorAdapter indicatorObj = new IndicatorAdapter(context, Indicator);
            indicatorObj.Load(item.Flag);
            if (item.Flag == 0)
            {
                Indicator.Visibility = ViewStates.Gone;
            }
            else
            {
                Indicator.Visibility = ViewStates.Visible;
            }
            //btnOption.Tag = position.ToString();
           // btnOption.Click += OnOptionButton_Click;



            return view;
        }

 

        private void RelocateAwbPieces(long oldLocationId, int pcs)
        {
           

        var transaction = context.FragmentManager.BeginTransaction();
            var dialogFragment = new LocationDialogActivity(this.context,null, "Location","Select Location",true, LocationTypes.Area);
            dialogFragment.Cancelable = false;
            dialogFragment.OkClicked += (Barcode barcode) => 
                {
     

                    CommunicationTransaction t = MCH.Communication.Relocate.Instance.RelocatePieces(selecteditem.AwbId,ReferenceTypes.AWB,oldLocationId,barcode.Id,pcs,RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId);
    


//                    CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.RelocateAwbPieces(selecteditem.AwbId,oldLocationId,barcode.Id ,pcs,RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.AppUserId);
                    if(t.Status)
                    {
                        ShipmentUldView ULDview = MCH.Communication.RecoverFreight.Instance.GetUldView(RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentUld.UldId, this.status);
                        if(ULDview.Transaction.Status)
                        {
                            items =  ULDview.Data;
                        }

                        this.NotifyDataSetChanged();
                    }
                }; 
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);

        }

        private void OnLocationSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
      
            foreach (var r in result)
            {
                string location;
                string[] locarray = r.Name.Split('(');
                if (locarray.Length > 1)
                {
                    location = locarray[0].Trim();
                }
                else
                {
                    location = r.Name;
                }

                Location l = Miscellaneous.Instance.GetLocationIdByLocationBarcode(ApplicationSessionState.SelectedWarehouse, location);
                if (l.Transaction.Status && l.Data != null)
                {
                    LocationItemCount count = MCH.Communication.RecoverFreight.Instance.GetAwbPiecesCountInLocation(selecteditem.AwbId, l.Data.LocationId);
                    if (count.Transaction.Status)
                    {
                        var transaction = context.FragmentManager.BeginTransaction();
                        var dialogFragment = new PiecesDialog(this.context,selecteditem.AWB,null,0 ,count.Data );
                        dialogFragment.OkClicked += (int pieces) => 
                            {

                                RelocateAwbPieces(l.Data.LocationId,pieces);


                            };
                        dialogFragment.Cancelable = false;
                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
                    }  
                }

//                    LocationItemId locationid =  MCH.Communication.RecoverFreight.Instance.GetLocationId(r.Name, ApplicationSessionState.SelectedWarehouse);
//                    if (locationid.Transaction.Status)
//                    {
//
//                    LocationItemCount count = MCH.Communication.RecoverFreight.Instance.GetAwbPiecesCountInLocation(selecteditem.AwbId, locationid.Data);
//                            if (count.Transaction.Status)
//                            {
//                        var transaction = context.FragmentManager.BeginTransaction();
//                        var dialogFragment = new PiecesDialog(this.context,null,0 ,count.Data );
//                        dialogFragment.OkClicked += (int pieces) => 
//                            {
//                                
//                                RelocateAwbPieces(locationid.Data,pieces);
//
//
//                            };
//                        dialogFragment.Cancelable = false;
//                        dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName); 
//                            }
//
//  
//
//
//                    }
           


                return;
            }

        }


  
            private void OnOptionButton_Click(object sender, EventArgs e)
            {

            LinearLayout btn = (LinearLayout)sender;
            selecteditem = items[int.Parse(btn.Tag.ToString())];


//            Button btn = (Button)sender;
//            selecteditem = items[int.Parse(btn.Tag.ToString())];

            List<OptionItem> options = new List<OptionItem>();
            if (string.IsNullOrEmpty(selecteditem.Locations))
            {
                //
            }
            else
            {
                options.Add(new OptionItem("RELOCATE",OptionActions.Location, Resource.Drawable.Map));
           
            }

            //options.Add(new OptionItem(this.context.GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
            options.Add(new OptionItem("PRINT",OptionActions.Print, Resource.Drawable.Printer));
            options.Add(new OptionItem("OVERPACK",OptionActions.OverPack, Resource.Drawable.Cube));
  
 
                Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = context.FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this.context, options, OptionClickAction,"REF# " + selecteditem.AWB);
                dialogFragment.Cancelable = true;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            }
     



        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Location:
                    Relocate();
                    break;
                case  OptionActions.Camera:
//                    GoToCamera(typeof( RecoverFreight_MainActivity),typeof( RecoverFreight_MainActivity ),0);
                    break;
                case  OptionActions.OverPack:
                    if (OkOverPack != null)
                        OkOverPack.Invoke(selecteditem);
                    break;
                case  OptionActions.Print:
                    if (OnPrint!= null)
                        OnPrint.Invoke(selecteditem);
                    break;
                    break;
            }

        }

        public void Relocate() 
        {
        
        
           

            List< MCH.Communication.SelectionItem> locationlist = new List< MCH.Communication.SelectionItem>();
            string[] locations = selecteditem.Locations.Split(',');
            for (int i = 0; i < locations.Length; i++)
            {

                locationlist.Add(new SelectionItem(locations[i].Trim() ,selecteditem.AwbId,Resource.Drawable.Map));

            }


            var transaction = context.FragmentManager.BeginTransaction();
            var dialogFragment = new SelectionDialogActivity("Relocate", this.context, locationlist,OnLocationSelection , SelectionListAdapter.SelectionMode.SingleSelection, false);
            dialogFragment.Cancelable = false;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
            return;

        }


    }
}




