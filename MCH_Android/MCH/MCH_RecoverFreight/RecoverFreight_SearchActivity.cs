﻿ 

using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;


namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFreight_SearchActivity : BaseActivity
    {
        ScanData lastscan;
        FlightView Tasks;

         
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFreight_SearchLayout);
            Initialize();
            LoadData();
            RefreshData(string.Empty,false);
            search.RequestFocus ();
            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;
        }

        private void LoadData()
        {
            RecoverTask data=  MCH.Communication.RecoverFreight.Instance.GetFlightManifestById(RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId);
            if (data.Transaction.Status)
            {
                RecoverFreight_SessionState.CurrentRecoverTask = data.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(data.Transaction.Error, MessageBox.AlertType.Error);
            }
        }

        private void DoBack()
        {
            StartActivity (typeof(RecoverFreight_RecoverActivity));
            this.Finish();
        }

        private void OnUldSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                foreach (var f in lastscan.Data)
                {
                    if (r.Id == f.Uld.UldId)
                    {
                        SelectUld(f.Uld);
                        return;
                    }
                }
                return;
            }

        }
        void ValidateShipment(string reference)
        {
            lastscan = MCH.Communication.RecoverFreight.Instance.ScanShipment(ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId,reference);
            if (lastscan.Transaction.Status)
            {

                if (lastscan.Data.Count > 1)
                {
                    List< MCH.Communication.SelectionItem> ulds = new List< MCH.Communication.SelectionItem>();
                    foreach (var f in lastscan.Data)
                    {
                        if (f.Uld.IsLoose)
                        {
                            ulds.Add(new SelectionItem(f.Uld.UldPrefix  ,f.Uld.UldId,Resource.Drawable.Cube));
                        }
                        else
                        {
                            ulds.Add(new SelectionItem(f.Uld.UldPrefix + f.Uld.UldSerialNo ,f.Uld.UldId,Resource.Drawable.Cube));
                        }

                    }

                    var transaction = FragmentManager.BeginTransaction();
                    var dialogFragment = new SelectionDialogActivity("Select Uld", this, ulds,OnUldSelection , SelectionListAdapter.SelectionMode.SingleSelection, false);
                    dialogFragment.Cancelable = false;
                    dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                    return;


                }
                else if (lastscan.Data.Count == 1)
                {
                    SelectUld(lastscan.Data[0].Uld);

                }
                else
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert("Unable to validate shipment.", MessageBox.AlertType.Information);
                }

            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(lastscan.Transaction.Error, MessageBox.AlertType.Error);
            }
        }


        void SelectUld(UldViewItem uld)
        {
            RecoverFreight_SessionState.CurrentUld = uld;
            if (string.IsNullOrEmpty(RecoverFreight_SessionState.CurrentRecoverTask.RecoveredBy))
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) => 
                    {
                        if(result)
                        {
                            DoRecover();
                            return;    
                        }

                    };
                m.ShowConfirmationMessage("Are you sure you want to recover this flight and uld?");
            }
            else
            {
                GoToUlds();
            }
        }
        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
           FlightViewItem item = Tasks.Data[e.Position];
           ValidateShipment(item.AWB);
 
        }



        private void GoToUlds()
        {



            if (RecoverFreight_SessionState.CurrentUld.IsRecovered)
            {
                StartActivity (typeof(RecoverFreight_ScanActivity));
                this.Finish();  
            }
            else
            {
                var newScreen = new Intent(this, typeof(RecoverFreight_ULDActivity));
                newScreen.PutExtra("UldId",  RecoverFreight_SessionState.CurrentUld.UldId.ToString());
                StartActivity(newScreen);
                this.Finish();    
            }

        }

        private void DoRecover()
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.RecoverFlight(ApplicationSessionState.SelectedWarehouse,RecoverFreight_SessionState.CurrentRecoverTask .TaskId, ApplicationSessionState.User.Data.UserId);
            if (!t.Status)
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                return;
            }
          
            GoToUlds();
            
        }



        private void RefreshData(string searchData,bool isBarcode)
        {
            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {

 

                    Tasks =    MCH.Communication.RecoverFreight.Instance.GetFlightView(RecoverFreight_SessionState.CurrentRecoverTask.TaskId,RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId );

                    RunOnUiThread (delegate {

                        if(Tasks.Transaction.Status)
                        {
                            if(Tasks.Data!=null)
                            {

                                Tasks.Data =  LinqHelper.Query<FlightViewItem>(Tasks.Data,searchData);
                                titleLabel.Text = "Search" + string.Format(" - ({0})",Tasks.Data.Count);
                                listView.Adapter = new RecoverFreight_SearchAdapter(this, Tasks.Data);
                                listView.ItemClick -= OnListItemClick;
                                listView.ItemClick += OnListItemClick;                          
                            }
                        }
                        else
                        {
                            Tasks.Data = new List<FlightViewItem>();
                            titleLabel.Text = "Search" + string.Format(" - ({0})",Tasks.Data.Count);
                            listView.Adapter = new RecoverFreight_SearchAdapter(this, Tasks.Data);
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;     
                        }

                        if(isBarcode)
                        {
                            EditTextListener.Text = string.Empty;
                            search.RequestFocus ();
                            if( Tasks.Data.Count == 1)
                            {
                                ValidateShipment(Tasks.Data[0].AWB);
                                //CheckIfRecover();
                                //GoToTask( Tasks.Data[0]);
                                return;
                            }
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }




        private void OnCarrierSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                List< MCH.Communication.SelectionItem> etas = new List< MCH.Communication.SelectionItem>();

                Etas es =  MCH.Communication.RecoverFreight.Instance.GetAvailableEtas(r.Id, ApplicationSessionState.SelectedWarehouse, ApplicationSessionState.User.Data.UserId);
                if (!es.Transaction.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(es.Transaction.Error, MessageBox.AlertType.Error);
                    return;
                }


                foreach (var e in es.Data)
                {
                    etas.Add(new SelectionItem(e.ToString(),r.Id,Resource.Drawable.Clock));
                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity("Select a ETA", this, etas, OnEtaSelection, SelectionListAdapter.SelectionMode.SingleSelection, false);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;
            }
        }

        private void OnEtaSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                List< MCH.Communication.SelectionItem> flights = new List< MCH.Communication.SelectionItem>();

                FlightTasks fs =  MCH.Communication.RecoverFreight.Instance.GetAvailableFlightsByEta(r.Id,DateTime.Parse(r.Name), ApplicationSessionState.SelectedWarehouse, ApplicationSessionState.User.Data.UserId);
                if (!fs.Transaction.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(fs.Transaction.Error, MessageBox.AlertType.Error);
                    return;
                }

                foreach (var f in fs.Data)
                {
                    flights.Add(new SelectionItem(f.Name,f.TaskId,Resource.Drawable.Plane));
                }

                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new SelectionDialogActivity("Select Flight", this, flights, OnFlightSelection, SelectionListAdapter.SelectionMode.SingleSelection, false);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
                return;
            }
        }

        private void OnFlightSelection(List<MCH.Communication.SelectionItem>  result,bool isExtra)
        {
            foreach (var r in result)
            {
                CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.LinkTaskToUserId(r.Id, ApplicationSessionState.User.Data.UserId);
                if (!t.Status)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert(t.Error, MessageBox.AlertType.Error);
                }

                RefreshData(string.Empty,false);
                search.RequestFocus ();
                return;
            }
        }
    }
}



