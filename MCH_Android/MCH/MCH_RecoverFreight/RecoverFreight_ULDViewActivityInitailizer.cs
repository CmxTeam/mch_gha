﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics.Drawables;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MCH.Communication;

namespace MCH
{

    public partial class RecoverFreight_ULDViewActivity : BaseActivity
    {

        ImageButton ClearSearch;
        ImageButton SearchButton;
        EditTextEventListener EditTextListener;
        EditText search;
        TextView titleLabel; 
        ImageView optionButton;
        ImageView backButton;
        ImageView imageHeader;
   
        ImageButton DropDownCheck;
        ListView listView;
        ImageButton DropDownButton;
        TextView DropDownText;
        LinearLayout DropDownBox;
 
    
  
        TextView txtFlight;
        TextView txtCounts;
        TextView txtUld;

        ImageView RowIcon;
        ImageView airlineImage;

        Button btnDrop;         Button btnFork;

        private void Initialize()
        {

            optionButton = FindViewById<ImageView>(Resource.Id.OptionButton);
            search = FindViewById<EditText>(Resource.Id.SearchText);
            EditTextListener = new EditTextEventListener(search);
            EditTextListener.OnEnterEvent += (object sender, EditTextEventArgs e) => 
                {
                    string data = e.Data;
                    if (e.IsBarcode)
                    {

                        if (e.BarcodeData.BarcodeType == BarcodeTypes.Area ||
                            e.BarcodeData.BarcodeType == BarcodeTypes.Door ||
                            e.BarcodeData.BarcodeType == BarcodeTypes.Truck ||
                            e.BarcodeData.BarcodeType == BarcodeTypes.ScreeningArea
                        )
                        {
                            if (GetForkLiftCount() == 0)
                            {
                                MessageBox m = new MessageBox(this);
                                m.ShowAlert("Your forklift is empty.", MessageBox.AlertType.Information);
                                return;
                            }

                            MessageBox mb = new MessageBox(this);
                            mb.OnConfirmationClick += (bool result) => 
                                {
                                    if(result)
                                    {
                                      


                                        DropForkliftPieces(e.BarcodeData);

                                    }
                                };
                            mb.ShowConfirmationMessage("Are you sure you want to drop all items from your forklift into this location?");
                            return;
                        }
                        else
                        {

//                            ScanData scandata = MCH.Communication.RecoverFreight.Instance.ScanShipment(ApplicationSessionState.User.Data.AppUserId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId,data);
//                            if (scandata.Transaction.Status && scandata.Data!=null)
//                            {
// 
//                            }

                            ValidateScanData validateScanData =  MCH.Communication.RecoverFreight.Instance.ValidateShipment(ApplicationSessionState.User.Data.UserId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, RecoverFreight_SessionState.CurrentUld.UldId, data);
                            if (validateScanData.Transaction.Status && validateScanData.Data!=null) 
                            {
                                if(validateScanData.Data.DetailId>0)
                                {
                                    data = validateScanData.Data.Awb;
                                }
                            } 
                        }

                       

                    }
                
                    RefreshData(data,e.IsBarcode);
                };

            DropDownBox= FindViewById<LinearLayout>(Resource.Id.DropDownBox);
            DropDownText = FindViewById<TextView>(Resource.Id.DropDownText);
            ClearSearch = FindViewById<ImageButton>(Resource.Id.ClearSearchButton);
            SearchButton = FindViewById<ImageButton>(Resource.Id.SearchButton);
            titleLabel = FindViewById<TextView>(Resource.Id.HeaderText);
            backButton = FindViewById<ImageView>(Resource.Id.BackButton);
            imageHeader = FindViewById<ImageView>(Resource.Id.HeaderImage);
            DropDownCheck = FindViewById<ImageButton>(Resource.Id.DropDownCheck);
            DropDownButton= FindViewById<ImageButton>(Resource.Id.DropDownButton);
            listView = FindViewById<ListView>(Resource.Id.GridControl);

            titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name;

            backButton.Click += OnBackButton_Click;
            optionButton.Click += OnOptionButton_Click;
 
            ClearSearch.Click += OnClearSearch_Click;
            DropDownCheck.Click += OnCheckStatusButton_Click;
            DropDownButton.Click += OnDropDownButton_Click;
            DropDownBox.Click += OnDropDownButton_Click;
            DropDownText.Click += OnDropDownButton_Click;
            SearchButton.Click += OnSearchButton_Click;

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Icons/{0}.png",ApplicationSessionState.SelectedMenuItem.IconKey));
                Drawable d = Drawable.CreateFromStream(ims, null);
                imageHeader.SetImageDrawable(d);
            } 
            catch 
            {
                //imageHeader.SetImageResource (Resource.Drawable.Icon);
            }

            //Find here new controls
 
            txtFlight = FindViewById<TextView>(Resource.Id.txtFlight);
            txtCounts = FindViewById<TextView>(Resource.Id.txtCounts);
            txtUld = FindViewById<TextView>(Resource.Id.txtUld);

            btnDrop = FindViewById<Button>(Resource.Id.btnDrop);             btnFork = FindViewById<Button>(Resource.Id.btnFork);
            btnFork.Click += btnFork_Click;              btnDrop.Click += btnDrop_Click;
  
        }
        private void LoadDetail()
        {
            txtFlight.Text =string.Format("{0} {1} {2}     {3:dd-MMM}",RecoverFreight_SessionState.CurrentRecoverTask.Origin,RecoverFreight_SessionState.CurrentRecoverTask.CarrierCode, RecoverFreight_SessionState.CurrentRecoverTask.FlightNumber, RecoverFreight_SessionState.CurrentRecoverTask.ETA).ToUpper();
            txtCounts.Text = string.Format("PCS: {0} of {1} ULDS: {2} of {3} AWBS: {4}", RecoverFreight_SessionState.CurrentRecoverTask.ReceivedPieces ,RecoverFreight_SessionState.CurrentRecoverTask.TotalPieces , RecoverFreight_SessionState.CurrentRecoverTask.RecoveredULDs,RecoverFreight_SessionState.CurrentRecoverTask.ULDCount ,RecoverFreight_SessionState.CurrentRecoverTask.AWBCount);


                if(RecoverFreight_SessionState.CurrentUld.IsLoose)
                {
                txtUld.Text = "ULD: " + RecoverFreight_SessionState.CurrentUld.UldPrefix;
                }
                else
                {
                txtUld.Text = "ULD: "  + RecoverFreight_SessionState.CurrentUld.UldPrefix + RecoverFreight_SessionState.CurrentUld.UldSerialNo;
                }

            RowIcon= FindViewById<ImageView>(Resource.Id.RowIcon);
            airlineImage= FindViewById<ImageView>(Resource.Id.airlineImage);
 

            try 
            {
                System.IO.Stream ims = this.Assets.Open(string.Format(@"Airlines/{0}.png",RecoverFreight_SessionState.CurrentRecoverTask.CarrierCode));
                Drawable d = Drawable.CreateFromStream(ims, null);
                airlineImage.SetImageDrawable(d);
            } 
            catch 
            {
                airlineImage.SetImageResource (Resource.Drawable.Airline);
            }


            switch (RecoverFreight_SessionState.CurrentRecoverTask.Status)
            {
                case RecoverFreightStatusTypes.Pending:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
                case RecoverFreightStatusTypes.Completed:
                    RowIcon.SetImageResource (Resource.Drawable.Completed);
                    break;
                case RecoverFreightStatusTypes.In_Progress:
                    RowIcon.SetImageResource (Resource.Drawable.InProgress );
                    break;
                default:
                    RowIcon.SetImageResource (Resource.Drawable.Pending);
                    break;
            }

            btnFork.Text = GetForkLiftCount().ToString();

            CheckFinilize();
        }


        private void CheckFinilize()
        {
            if (RecoverFreight_SessionState.CurrentRecoverTask.Status != RecoverFreightStatusTypes.Completed && RecoverFreight_SessionState.CurrentRecoverTask.ReceivedPieces == RecoverFreight_SessionState.CurrentRecoverTask.TotalPieces)
            {
                MessageBox m = new MessageBox(this);
                m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DoFinalize();

                    }
                };
                m.ShowConfirmationMessage("All pieces were scanned. Do you want to finalize task?", Resource.String.Yes, Resource.String.No);
            }
        }


        private void OnCheckStatusButton_Click(object sender, EventArgs e)
        {

        }


        private void OnDropDownButton_Click(object sender, EventArgs e)
        {

            PopupMenu menu = new PopupMenu(this, DropDownText);

            Dictionary<int, string> result = EnumHelper.GetEnumToList<RecoverFreightStatusTypes>();
            foreach (KeyValuePair<int, string> kvp in result)
            {
                menu.Menu.Add(0,kvp.Key,kvp.Key,kvp.Value);
            }

            menu.MenuInflater.Inflate(Resource.Menu.PopupMenu, menu.Menu);
            menu.MenuItemClick += (s1, arg1) =>
                {
                    DropDownText.Text = arg1.Item.ToString();
                    search.Text = string.Empty;
                    RefreshData(string.Empty,false);
                };
            menu.Show(); 
        }

        private void OnClearSearch_Click(object sender, EventArgs e)
        {
            RefreshData(string.Empty,false);
            EditTextListener.Text = string.Empty;
            search.RequestFocus ();
        }

        private void OnSearchButton_Click(object sender, EventArgs e)
        {
            RefreshData(search.Text,false);
        }

        private void OnBackButton_Click(object sender, EventArgs e)
        {
            DoBack();
        }

  
  

        private void OnOptionButton_Click(object sender, EventArgs e)
        {
            List<OptionItem> options = new List<OptionItem>();
            options.Add(new OptionItem("Summary",OptionActions.Info , Resource.Drawable.Info));
            options.Add(new OptionItem(GetText(Resource.String.Finalize),OptionActions.Finalize, Resource.Drawable.Validate));
            options.Add(new OptionItem(GetText(Resource.String.Refresh),OptionActions.Refresh, Resource.Drawable.Refresh));
            options.Add(new OptionItem(GetText(Resource.String.CargoSnapShot),OptionActions.Camera , Resource.Drawable.Camera));
            options.Add(new OptionItem(GetText(Resource.String.Main_Menu),OptionActions.MainMenu, Resource.Drawable.Menu));
            options.Add(new OptionItem(GetText(Resource.String.Logout),OptionActions.Logout, Resource.Drawable.Logout));
            options.Add(new OptionItem(GetText(Resource.String.Exit_Application),OptionActions.Exit, Resource.Drawable.Exit));

            Action<OptionItem> OptionClickAction = OnOptionClickAction;
            var transaction = FragmentManager.BeginTransaction();
            var dialogFragment = new OptionDialog(this, options, OptionClickAction);
            dialogFragment.Cancelable = true;
            dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
        }

        private void OnOptionClickAction(OptionItem  option)
        {
            switch (option.OptionAction)
            {
                case  OptionActions.Logout:
                    ConfirmLogOut();
                    break;
                case  OptionActions.Exit:
                    ConfirmExit();
                    break;
                case  OptionActions.MainMenu:
                    GotoMainMenu();
                    break;
                case  OptionActions.Refresh:
                    search.Text = string.Empty;
                    RefreshData(string.Empty, false);
                    search.RequestFocus();
                    break;
                case  OptionActions.Camera:
                    GoToCamera(typeof( RecoverFreight_ULDViewActivity),typeof( RecoverFreight_ULDViewActivity ),RecoverFreight_SessionState.CurrentRecoverTask.TaskId);
                    break;
                case OptionActions.Finalize:
                    Finalize();
                    break;
                case OptionActions.Info :                     RecoverFreight_SummaryActivity.BackScreen = typeof(RecoverFreight_ULDViewActivity );
                    StartActivity (typeof(RecoverFreight_SummaryActivity));
                    this.Finish();                     break; 
            }

        }

        private void Finalize()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DoFinalize();

                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);

        }


        private void DoFinalize()
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.FinalizeReceiver( RecoverFreight_SessionState.CurrentRecoverTask.TaskId, RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            StartActivity (typeof(RecoverFreight_MainActivity));
                            this.Finish();
                        }
                        else
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowMessage(t.Error);
                        }
        }

        public override void OnBackPressed ()
        {
            DoBack();
        }

        protected override void OnRestart()
        {
            //Toast.MakeText (this, "OnRestart called, App is Restart", ToastLength.Long).Show ();
            // Refresh Data Here
            base.OnRestart ();
        }

        protected override void OnStart()
        {
            //Toast.MakeText (this, "OnStart called, App is Active", ToastLength.Long).Show ();

            base.OnStart();
        }
        protected override void OnResume()
        {

            //Toast.MakeText (this, "OnResume called, app is ready to interact with the user", ToastLength.Long).Show ();

            base.OnResume();
        }
        protected override void OnPause()
        {
            //Toast.MakeText (this, "OnPause called, App is moving to background", ToastLength.Long).Show ();
            base.OnPause();
        }
        protected override void OnStop()
        {
            //Toast.MakeText (this, "OnStop called, App is in the background", ToastLength.Long).Show ();
            base.OnStop();
        }
        protected override void OnDestroy ()
        {
            base.OnDestroy ();
            //Toast.MakeText (this, "OnDestroy called, App is Terminating", ToastLength.Long).Show ();
        }
    }
}

