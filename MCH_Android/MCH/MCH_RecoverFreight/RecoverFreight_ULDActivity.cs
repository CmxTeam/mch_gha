﻿
using System.Linq;
using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;
using Java.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Camera = Android.Hardware.Camera;
using Android.Hardware;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using MCH.Communication;
 

namespace MCH
{
    [Activity ( ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.NoTitle", Label =  "@string/app_name", Icon = "@drawable/icon")]           
    public partial class RecoverFreight_ULDActivity : BaseActivity
    {

        FlightUldView Tasks;
        int selectedUldId = 0;
       
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.RecoverFreight_ULDLayout);
            Initialize();
            RefreshAll();

            FindSelecedUldId();
            search.RequestFocus ();
            search.SystemUiVisibility = (StatusBarVisibility)View.SystemUiFlagHideNavigation;
        }

        public void FindSelecedUldId()
        {
            try
            {
                selectedUldId = int.Parse( Intent.GetStringExtra("UldId"));


                if(RecoverFreight_SessionState.CurrentUld.UldId == selectedUldId)
                {
                    CheckSelectedUld();
                }


            }
            catch{}
        }

        private void RefreshAll()
        {
            LoadData();
            RefreshData(string.Empty,false);
            search.RequestFocus ();
        }

        private void LoadData()
        {
            RecoverTask data=  MCH.Communication.RecoverFreight.Instance.GetFlightManifestById(RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId, RecoverFreight_SessionState.CurrentRecoverTask.TaskId, ApplicationSessionState.User.Data.UserId);
            if (data.Transaction.Status)
            {
                RecoverFreight_SessionState.CurrentRecoverTask = data.Data;
            }
            else
            {
                MessageBox m = new MessageBox(this);
                m.ShowAlert(data.Transaction.Error, MessageBox.AlertType.Error);
            }
            LoadDetail();
        }



        private void DoBack()
        {
            StartActivity (typeof(RecoverFreight_RecoverActivity));
            this.Finish();
        }


       
        void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            RecoverFreight_SessionState.CurrentUld = Tasks.Data.Ulds[e.Position];
            CheckSelectedUld();
        }
        void CheckSelectedUld()
        {
 
            if (RecoverFreight_SessionState.CurrentUld.UldPrefix.ToLower() == "all")
            {
                GoToUld();
            }
            else
            {
                if (!RecoverFreight_SessionState.CurrentUld.IsRecovered)
            {
                if (selectedUldId == 0)
                {
                    MessageBox m = new MessageBox(this);
                    m.OnConfirmationClick += (bool result) =>
                    {
                        if (result)
                        {
                            DoRecover();
                        }

                    };
                    m.ShowConfirmationMessage("Are you sure you want to recover this ULD?.");
                }
                else
                {
                    selectedUldId = 0;
                    DoRecover();
                }
            }
            else
            {
                if ( RecoverFreight_SessionState.CurrentUld.IsBUP)
                {
                    MessageBox m = new MessageBox(this);
                    m.ShowAlert("This ULDs has been already recovered.", MessageBox.AlertType.Information);
                }
                else
                {
                    GoToUld();
                }
            }
            } 




        }

        private void DoRecover()
        {
 
                var transaction = FragmentManager.BeginTransaction();
                var dialogFragment = new LocationDialogActivity(this, ShowBarcodeId, "Location","Select Location",true, LocationTypes.Area);
                dialogFragment.Cancelable = false;
                dialogFragment.Show(transaction, ApplicationSessionState.ApplicationName);
 
        }

        private void ShowBarcodeId(Barcode  barcode)
        {


            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.RecoverUld(RecoverFreight_SessionState.CurrentUld.UldId ,ApplicationSessionState.User.Data.UserId  , RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId,barcode.Id );
            if (t.Status)
            {

                if (RecoverFreight_SessionState.CurrentUld.IsBUP)
                {
                    //GoToCamera(typeof( RecoverFreight_ULDActivity ),typeof(  RecoverFreight_ULDActivity ),RecoverFreight_SessionState.CurrentRecoverTask.TaskId  ,"???????" );
                    RefreshAll();
                }
                else
                {
                    //GoToCamera(typeof( RecoverFreight_ScanLayout ),typeof(  RecoverFreight_ScanLayout ),RecoverFreight_SessionState.CurrentRecoverTask.TaskId  ,"???????" );
                    GoToUld();
                }
 
            }
            else
            {
                MessageBox mb = new MessageBox(this);
                mb.ShowMessage(t.Error);
                RefreshAll();
                return;
            }

        }


        private void GoToUld()
        {

           // StartActivity (typeof(RecoverFreight_ScanActivity));
            StartActivity (typeof(RecoverFreight_ULDViewActivity));
            this.Finish();  
        }




        private List<UldViewItem> AddAllToList(List<UldViewItem> list)
        {
            UldViewItem all = new UldViewItem();
            all.UldPrefix = "All";
            List<UldViewItem> result = new List<UldViewItem>();
            result.Add(all);
            foreach (var item in list)
            {
                result.Add(item);
            }
            titleLabel.Text = "RECOVER ULDS" + string.Format(" - ({0})",result.Count -1);
            return result;
        }


        private List<UldViewItem> AddSelectedToList(List<UldViewItem> list, List<FlightViewItem>  ulds)
        {
            UldViewItem all = new UldViewItem();
            all.UldPrefix = "All";
            List<UldViewItem> result = new List<UldViewItem>();
            result.Add(all);
            foreach (var item in list)
            {
                
                foreach (var uld in ulds)
                {
                    if (item.UldId == uld.UldId)
                    {
                        result.Add(item);
                    }
                }

               
            }

            titleLabel.Text = "RECOVER ULDS" + string.Format(" - ({0})",result.Count -1);
            return result;
        }


        private void RefreshData(string searchData,bool isBarcode)
        {



            var progressDialog = ProgressDialog.Show(this, GetText(Resource.String.Please_Wait), GetText(Resource.String.Loading), true);
            new Thread(new ThreadStart(delegate
                {

                    UldStatusTypes status =    EnumHelper.GetEnumItem<UldStatusTypes>(DropDownText.Text);



                    //MCH.Communication.RecoverFreight.Instance.GetCargoReceiverUlds(ApplicationSessionState.User.Data.AppUserId, ApplicationSessionState.SelectedWarehouseId,status);


                    Tasks = MCH.Communication.RecoverFreight.Instance.GetFlightULDs(RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId,status);

                     
                    RunOnUiThread (delegate {

                        if(Tasks.Transaction.Status)
                        {
                            if(Tasks.Data!=null)
                            {

                                Tasks.Data.Ulds =  LinqHelper.Query<UldViewItem>(Tasks.Data.Ulds,searchData);


                               

                                //titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",Tasks.Data.Ulds.Count);
                                //titleLabel.Text = "Recover Ulds" + string.Format(" - ({0})",Tasks.Data.Ulds.Count);
                                Tasks.Data.Ulds = AddAllToList(Tasks.Data.Ulds);
                        
                                var adapter   = new RecoverFreight_ULDAdapter(this, Tasks.Data.Ulds);
                                adapter.OkRefresh += () => 
                                    {
                                        RefreshData(string.Empty,false);  
                                    };
                                listView.Adapter = adapter;
                                listView.ItemClick -= OnListItemClick;
                                listView.ItemClick += OnListItemClick;                          
                            }
                        }
                        else
                        {
                            Tasks.Data.Ulds = new List<UldViewItem>();
                            //titleLabel.Text = ApplicationSessionState.SelectedMenuItem.Name + string.Format(" - ({0})",Tasks.Data.Ulds.Count);
                            //titleLabel.Text = "Recover Ulds"  + string.Format(" - ({0})",Tasks.Data.Ulds.Count);
                            Tasks.Data.Ulds = AddAllToList(Tasks.Data.Ulds);

                            var adapter   = new RecoverFreight_ULDAdapter(this, Tasks.Data.Ulds);
                            adapter.OkRefresh += () => 
                                {
                                    RefreshData(string.Empty,false);  
                                };
                            listView.Adapter = adapter;

              
                            listView.ItemClick -= OnListItemClick;
                            listView.ItemClick += OnListItemClick;     
                        }

                        if(isBarcode)
                        {
                            EditTextListener.Text = string.Empty;
                            search.RequestFocus ();
                            if( Tasks.Data.Ulds.Count == 1)
                            {
                                

                                FlightView  ulds =    MCH.Communication.RecoverFreight.Instance.GetShipmentUldView(RecoverFreight_SessionState.CurrentRecoverTask.TaskId,RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId, searchData );
                                if(ulds.Data !=null && ulds.Transaction.Status)
                                {
                                    if(ulds.Data.Count>0)
                                    {
                                        Tasks = MCH.Communication.RecoverFreight.Instance.GetFlightULDs(RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId,status);
                                        if(Tasks.Data !=null && Tasks.Transaction.Status)
                                        {
                                            Tasks.Data.Ulds = AddSelectedToList(Tasks.Data.Ulds,ulds.Data);
                                        
                                            var adapter   = new RecoverFreight_ULDAdapter(this, Tasks.Data.Ulds);
                                            adapter.OkRefresh += () => 
                                                {
                                                    RefreshData(string.Empty,false);  
                                                };
                                            listView.Adapter = adapter;
 
                                        listView.ItemClick -= OnListItemClick;
                                        listView.ItemClick += OnListItemClick;  

                                        }
                                    }
                                    else
                                    {
                                        RefreshData(string.Empty,false);
                                    }

           
                                }
 

                                return;
                            }
                        }

                    });

                    RunOnUiThread(() => progressDialog.Hide());
                })).Start();

        }

 
        private void Finalize()
        {

            MessageBox m = new MessageBox(this);
            m.OnConfirmationClick += (bool result) =>
                {
                    if (result)
                    {

                        DoFinalize();

                    }
                };
            m.ShowConfirmationMessage(Resource.String.Confirm_Finalize, Resource.String.Yes, Resource.String.No);

        }


        private void DoFinalize()
        {
            CommunicationTransaction t = MCH.Communication.RecoverFreight.Instance.FinalizeReceiver( RecoverFreight_SessionState.CurrentRecoverTask.TaskId, RecoverFreight_SessionState.CurrentRecoverTask.FlightManifestId,ApplicationSessionState.User.Data.UserId);
                        if (t.Status)
                        {

                            StartActivity (typeof(RecoverFreight_MainActivity));
                            this.Finish();
                        }
                        else
                        {
                            MessageBox mb = new MessageBox(this);
                            mb.ShowMessage(t.Error);
                        }
        }


    }
}

