﻿using System;
using System.Linq;
using CargoMatrix.Communication.WSCargoDischarge;
using CargoMatrix.Communication.Common;
using CargoMatrix.Communication.DTO;
using System.Collections.Generic;

namespace CargoMatrix.Communication.CargoDischarge
{
    public class CargoDischarge : CMXServiceWrapper
    {
        private WsCargoDischarge service;
        private static CargoDischarge _instance;
        private string asmxName = "WsCargoDischarge.asmx";


        public static CargoDischarge Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CargoDischarge();
                }

                return _instance;
            }
        }

        private CargoDischarge()
        {
            service = new WsCargoDischarge();
            service.Url = Settings.Instance.URLPath + asmxName;
        }


        public DischargeHousebillItem[] GetCargoDischargeHouseBills(HousebillFilterCriteria filterCriteria,
                                                                    HouseBillSortCriteria sortCriteria)
        {
            Func<DischargeHousebillItem[]> f = () =>
            {

                WSCargoDischarge.HouseBillItem filterItem = new WSCargoDischarge.HouseBillItem();
                filterItem.Status = filterCriteria.ToWSCargoDischargeStatus();

                HouseBillFields sortingValue = sortCriteria.ToWsCargoDischargeFileds();

                return (
                            from item in this.service.GetCargoDischargeHouseBillList(this.connectionName, filterItem, sortingValue)
                            select new CargoMatrix.Communication.CargoDischarge.DischargeHousebillItem(item)
                        ).ToArray();

            };
            return Invoke<DischargeHousebillItem[]>(f, 300001, new DischargeHousebillItem[0]);
        }



        public DischargeHousebillItem[] GetCargoDischargeHouseBillsByNumber(params string[] houseBillNumbers)
        {
            Func<DischargeHousebillItem[]> f = () =>
            {

                var houseBills = this.service.GetCargoDischargeHouseBills(this.connectionName, houseBillNumbers);

                return (
                            from item in houseBills
                            select new DischargeHousebillItem(item)
                       ).ToArray();
            };
            return Invoke<DischargeHousebillItem[]>(f, 300004, new DischargeHousebillItem[0]);
        }


        public DischargeHousebillItem GetCargoDischargeHouseBillByNumber(string houseBillNumber)
        {
            return this.GetCargoDischargeHouseBillsByNumber(houseBillNumber)[0];
        }

        public int GetForkliftItemsCount(int userID, int forkliftID)
        {
            Func<int> f = () =>
            {
                return this.service.GetForkLiftHouseBillsCount(this.connectionName, userID, forkliftID);
            };
            return Invoke<int>(f, 300005, 0);
        }

        public int GetForkliftItemsCount(int forkliftID)
        {
            return this.GetForkliftItemsCount(userId, forkliftID);
        }

        public DischargeHousebillItem[] GetForkLiftHouseBills(int userID, HouseBillSortCriteria sortCriteria, int forkliftID)
        {
            Func<DischargeHousebillItem[]> f = () =>
            {

                HouseBillFields sortingValue = sortCriteria.ToWsCargoDischargeFileds();

                return (
                            from item in this.service.GetForkLiftHouseBills(this.connectionName, userID, sortingValue, forkliftID)
                            select new DischargeHousebillItem(item)
                       ).ToArray();
            };
            return Invoke<DischargeHousebillItem[]>(f, 300006, new DischargeHousebillItem[0]);
        }

        public DischargeHousebillItem[] GetForkLiftHouseBills(HouseBillSortCriteria sortCriteria, int forkliftID)
        {
            return this.GetForkLiftHouseBills(userId, sortCriteria, forkliftID);
        }

        public TransactionStatus RemovePiecesFromForklift(IDischargeHouseBillItem housebillItem, int pieceID, int userID, int forkliftID, CargoMatrix.Communication.DTO.ScanModes scanMode)
        {
            Func<TransactionStatus> f = () =>
            {
                var scnMode = ToWSCargoDoschargeScanModes(scanMode);

                return this.service.RemovePieceFromForkLift(this.connectionName, housebillItem.HousebillId, pieceID, userID, scnMode, forkliftID, housebillItem.TaskID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 300007, defRet);
        }

        public TransactionStatus RemovePiecesFromForklift(IDischargeHouseBillItem housebillItem, int pieceID, int forkliftID, CargoMatrix.Communication.DTO.ScanModes scanMode)
        {
            return this.RemovePiecesFromForklift(housebillItem, pieceID, userId, forkliftID, scanMode);
        }

        public TransactionStatus RemoveHousebillFromForklift(IDischargeHouseBillItem housebillItem, int userID, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.RemoveHousebillFromForklift(this.connectionName, housebillItem.HousebillId, housebillItem.TaskID, userID, forkliftID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 300008, defRet);
        }

        public TransactionStatus RemoveHousebillFromForklift(IDischargeHouseBillItem housebillItem, int forkliftID)
        {
            return this.RemoveHousebillFromForklift(housebillItem, userId, forkliftID);
        }

        public TransactionStatus ClearForklift(int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.RemoveAllPiecesFromForklift(this.connectionName, userId, forkliftID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 300009, defRet);
        }

        public CargoMatrix.Communication.ScannerUtilityWS.TaskSettings GetTaskSettings()
        {
            return ScannerUtility.Instance.GetTaskSettings(CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.CargoDischarge);
        }

        public DischargeHousebillItem ScanPieceCountIntoForkLift(IDischargeHouseBillItem houseBillItem, int pieces, int slac, int userID, ScanTypeEnum scanType, int forkliftID)
        {
            Func<DischargeHousebillItem> f = () =>
            {
                WSCargoDischarge.ScanTypes scnType = ToWSCargoDischargeScanTypes(scanType);

                var houseBill = this.service.ScanPieceCountIntoForkLift(this.connectionName, houseBillItem.HousebillId, pieces, slac, houseBillItem.TaskID, userID, scnType, forkliftID);

                return new DischargeHousebillItem(houseBill);
            };
            var defRet = new DischargeHousebillItem(new CargoMatrix.Communication.WSCargoDischarge.HouseBillItem());
            return Invoke<DischargeHousebillItem>(f, 300011, defRet);

        }

        public DischargeHousebillItem ScanPieceCountIntoForkLift(IDischargeHouseBillItem houseBillItem, int pieces, int slac, ScanTypeEnum scanType, int forkliftID)
        {
            return this.ScanPieceCountIntoForkLift(houseBillItem, pieces, slac, userId, scanType, forkliftID);
        }

        public DischargeHousebillItem ScanPieceNumberIntoForkLift(string houseBillNumber, int pieseNumber, int slac, int userId, ScanTypeEnum scanType, int forkliftID)
        {
            Func<DischargeHousebillItem> f = () =>
            {

                WSCargoDischarge.ScanTypes scnType = ToWSCargoDischargeScanTypes(scanType);

                var houseBill = this.service.ScanPieceNumberIntoForkLift(this.connectionName, houseBillNumber, pieseNumber, slac, userId, scnType, forkliftID);

                return new DischargeHousebillItem(houseBill);
            };
            var defRet = new DischargeHousebillItem(new CargoMatrix.Communication.WSCargoDischarge.HouseBillItem());
            return Invoke<DischargeHousebillItem>(f, 300015, defRet);
        }

        public DischargeHousebillItem ScanPieceNumberIntoForkLift(string houseBillNumber, int pieseNumber, int slac, ScanTypeEnum scanType, int forkliftID)
        {
            return this.ScanPieceNumberIntoForkLift(houseBillNumber, pieseNumber, slac, userId, scanType, forkliftID);
        }

        public DischargeHousebillItem ScanPiecesIntoForkLift(IDischargeHouseBillItem houseBillItem, int[] piecesNumbers, int slac, int userID, ScanTypeEnum scanType, int forkliftID)
        {
            Func<DischargeHousebillItem> f = () =>
            {

                WSCargoDischarge.ScanTypes wsScanType = ToWSCargoDischargeScanTypes(scanType);

                var wsDischargeItem = this.service.ScanPiecesIntoForkLift(this.connectionName, houseBillItem.HousebillNumber,
                                                                                      piecesNumbers,
                                                                                      slac,
                                                                                      userID,
                                                                                      wsScanType,
                                                                                      forkliftID);
                return new DischargeHousebillItem(wsDischargeItem);
            };
            var defRet = new DischargeHousebillItem(new CargoMatrix.Communication.WSCargoDischarge.HouseBillItem());
            return Invoke<DischargeHousebillItem>(f, 300016, defRet);
        }

        public DischargeHousebillItem ScanPiecesIntoForkLift(IDischargeHouseBillItem houseBillItem, int[] piecesNumbers, int slac, int forkliftID)
        {
            return this.ScanPiecesIntoForkLift(houseBillItem, piecesNumbers, slac, userId, ScanTypeEnum.Manual, forkliftID);
        }

        public byte[] GetDriverImage(int taskID)
        {
            Func<byte[]> f = () =>
            {

                string result = this.service.GetDriverImage(this.connectionName, taskID);

                if (string.IsNullOrEmpty(result)) return null;

                return Convert.FromBase64String(result);
            };
            return Invoke<byte[]>(f, 300017, new byte[0]);
        }

        public TransactionStatus FinalizeCargoDischarge(PieceItem[] pieces, int userID, string doorName, int forkliftID, byte[] signature)
        {
            Func<TransactionStatus> f = () =>
            {
                string strSignature = Convert.ToBase64String(signature);

                return this.service.FinalizeCargoDischarge(this.connectionName, pieces, userID, doorName, strSignature, forkliftID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 300018, defRet);
        }

        public TransactionStatus FinalizeCargoDischarge(PieceItem[] pieces, string doorName, int forkliftID, byte[] signature)
        {
            return this.FinalizeCargoDischarge(pieces, userId, doorName, forkliftID, signature);
        }

        public TransactionStatus DropPiecesIntoLocation(PieceItem[] pieces, int taskID, int userID, ScanTypes scanType, string locationName, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.DropPiecesIntoLocation(this.connectionName, pieces, taskID, userID, scanType, locationName, forkliftID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 300019, defRet);
        }

        public TransactionStatus DropPiecesIntoLocation(PieceItem[] pieces, int taskID, ScanTypes scanType, string locationName, int forkliftID)
        {
            return this.DropPiecesIntoLocation(pieces, taskID, userId, scanType, locationName, forkliftID);
        }



        public TransactionStatus CancelCargoDischarge(int[] taskIDs, int userID, int fokliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.CancelCargoDischarge(this.connectionName, taskIDs, userID, fokliftID);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 300020, defRet);
        }

        public TransactionStatus CancelCargoDischarge(int[] taskIDs, int forkliftID)
        {
            return this.CancelCargoDischarge(taskIDs, userId, forkliftID);
        }

        private static WSCargoDischarge.ScanTypes ToWSCargoDischargeScanTypes(ScanTypeEnum scanType)
        {
            switch (scanType)
            {
                case ScanTypeEnum.Automatic: return ScanTypes.Automatic;

                case ScanTypeEnum.Manual: return ScanTypes.Manual;

                default: throw new NotImplementedException();

            }
        }

        public static WSCargoDischarge.ScanModes ToWSCargoDoschargeScanModes(CargoMatrix.Communication.DTO.ScanModes scanMode)
        {
            switch (scanMode)
            {
                case CargoMatrix.Communication.DTO.ScanModes.Count: return CargoMatrix.Communication.WSCargoDischarge.ScanModes.Count;

                case CargoMatrix.Communication.DTO.ScanModes.NA: return CargoMatrix.Communication.WSCargoDischarge.ScanModes.NA;

                case CargoMatrix.Communication.DTO.ScanModes.Piece: return CargoMatrix.Communication.WSCargoDischarge.ScanModes.Piece;

                case CargoMatrix.Communication.DTO.ScanModes.Slac: return CargoMatrix.Communication.WSCargoDischarge.ScanModes.Slac;

                default: throw new NotImplementedException();
            }

        }

    }
}
