﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication.ScannerUtilityWS
{
    partial class MasterBillItem : IMasterBillItem
    {

        #region IMasterBillItem Members


        int IMasterBillItem.Weight
        {
            get { return (int)this.weightField; }
        }

        DateTime IMasterBillItem.CutOff
        {
            get { return this.cutOffField ?? DateTime.MinValue; }
        }

        MasterbillStatus IMasterBillItem.Status
        {
            get
            {
                switch (this.scanStatusField)
                {
                    case ScanStatuses.NotScanned:
                        return MasterbillStatus.Open;
                    case ScanStatuses.Scanned:
                        return MasterbillStatus.Completed;
                    case ScanStatuses.Shortage:
                    case ScanStatuses.TakeOff:
                    case ScanStatuses.Removed:
                    case ScanStatuses.All:
                    default:
                        return MasterbillStatus.All;
                }
            }
        }

        #endregion

        #region IMasterBillItem Members


        CargoMatrix.Communication.WSLoadConsol.MOT IMasterBillItem.Mot
        {
            get { return (CargoMatrix.Communication.WSLoadConsol.MOT)Enum.Parse(typeof(CargoMatrix.Communication.WSLoadConsol.MOT), this.motField.ToString(), true); }
        }

        #endregion
    }
}
