﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSPieceScan;
using CargoMatrix.Communication.DTO;
using System.Xml.Serialization;
using System.IO;
using CargoMatrix.Communication.Common;



namespace CargoMatrix.Communication
{



    public class CMXScannerWebService : ScannerWebService.ScannerWebService
    {
        string asmxName = "CMXScannerWebService_v11.asmx";

        public CMXScannerWebService()
        {
            Url = Settings.Instance.URLPath + asmxName; 
        }

    }
    public class Context { }
    public class WebServiceManager : IWebServiceClient
    {
        private static WebServiceManager instance;
        public CargoMatrix.Communication.DTO.User m_user;
        CMXScannerWebService webservice;
        public string m_connectionString;
        public string m_gateway;

        private WebServiceManager()
        {

            webservice = new CMXScannerWebService();

            m_connectionString = Settings.Instance.ConnectionName;
            m_gateway = Settings.Instance.Gateway;
        }

        ~WebServiceManager()
        {
            if (webservice != null)
                webservice.Dispose();
        }

        public static WebServiceManager Instance()
        {
            if (instance == null)
            {
                instance = new WebServiceManager();
            }

            return instance;

        }
        public static void SetUserID(int userID, string name, UserTypes userType)
        {
            if (instance == null)
                instance = new WebServiceManager();
            instance.m_user.UserName = name;
            instance.m_user.userID = userID;
            //int x = (int)userType;
            instance.m_user.GroupID = userType;// (UserTypes)x;
        }




        public static string GetConnectionString()
        {
            if (instance != null)
            {

                return instance.m_connectionString;
            }
            return "";
        }


        public static string GetUserID()
        {
            if (instance != null)
            {

                return instance.m_user.UserName;
            }
            return null;
        }


        public static int UserID()
        {
            if (instance != null)
            {

                return instance.m_user.userID;
            }
            return 0;
        }


        public static int GetUserIDTemp()
        {

            return -1;
        }

        public int LoginByPin(string pin)
        {
            //return SecurityManager.Instance.LoginByPin(pin);

            return ScannerMCHServiceManager.Instance.LoginByPin(pin);
        }

     

        public bool GetMainManuMCH(out CargoMatrix.Communication.DTO.MainMenuItem[] mainMenuItems, out bool newVersionAvailable)
        {
            WSScannerMCHService.MainMenuItem[] tempItems = null;
            try
            {



                int latestversion=0;

                tempItems = ScannerMCHServiceManager.Instance.GetMainMenuMCH(m_user.userID, m_gateway, m_connectionString);

 

                newVersionAvailable = Utilities.AppCurrentVersion < latestversion ? true : false;

                mainMenuItems = new CargoMatrix.Communication.DTO.MainMenuItem[tempItems.Length];
                for (int i = 0; i < tempItems.Length; i++)
                {
                    mainMenuItems[i] = new CargoMatrix.Communication.DTO.MainMenuItem();
                    mainMenuItems[i].ActionID = tempItems[i].ActionID;
                    mainMenuItems[i].TotalCount = tempItems[i].TotalCount;
                    mainMenuItems[i].NotCompleted = tempItems[i].NotCompleted;
                    mainMenuItems[i].NotAssigned = tempItems[i].NotAssigned;
                    mainMenuItems[i].Completed = tempItems[i].Completed;
                    mainMenuItems[i].Icon = tempItems[i].MobileResource;
                   
                    //if (tempItems[i].ActionID == 1)
                    //{
                    //    mainMenuItems[i].Icon = "Checkin_Manifest";
                    //}

                    mainMenuItems[i].Label = tempItems[i].Label;
                    mainMenuItems[i].TaskCountByUser = tempItems[i].TotalCountByUser;
                    mainMenuItems[i].NotCompletedTaskCountByUserID = tempItems[i].NotCompletedTaskCountByUserID.ToString();
                }
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10006)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                mainMenuItems = null;
                newVersionAvailable = false;
                return false;
            }
            return true;
        }


        public CargoMatrix.Communication.ScannerUtilityWS.LocationItem[] GetWarehouseLocationsMCH(WSScannerMCHService.LocationType locationType)
        {
            WSScannerMCHService.WarehouseLocation[] tempItems = null;
            CargoMatrix.Communication.ScannerUtilityWS.LocationItem[] locationItems;
            
            try
            {


                tempItems = ScannerMCHServiceManager.Instance.GetWarehouseLocationsMCH(m_user.userID, m_gateway, m_connectionString,locationType);
                locationItems = new CargoMatrix.Communication.ScannerUtilityWS.LocationItem[tempItems.Length];
                for (int i = 0; i < tempItems.Length; i++)
                {
                    locationItems[i] = new CargoMatrix.Communication.ScannerUtilityWS.LocationItem();
                    locationItems[i].Location = tempItems[i].Location;
                    locationItems[i].LocationBarcode = tempItems[i].LocationBarcode;
                    locationItems[i].LocationId = (int)tempItems[i].LocationId;
                    locationItems[i].LocationType = (CargoMatrix.Communication.ScannerUtilityWS.LocationTypes)tempItems[i].LocationType;
                }

                return locationItems;
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10006)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                return null;
                 
            }
    
        }

        public long GetLocationIdByLocationBarcodeMCH(string location)
        {
            try
            {
                return ScannerMCHServiceManager.Instance.GetLocationIdByLocationBarcodeMCH(m_user.userID, m_gateway, location);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                return 0;
            }

        }


        public WSCargoReceiverMCHService.ValidatedShipment ValidateShipmentMCH(long taskId, long uldId, string shipment)
        {
            try
            {
                return CargoReceiverMCHService.Instance.ValidateShipmentMCH(m_user.userID, taskId, uldId, shipment);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                return null;
            }
        }

        public WSCargoReceiverMCHService.TransactionStatus AddForkliftPiecesMCH(long detailId, long taskId, int pcs)
        {
            try
            {
                return CargoReceiverMCHService.Instance.AddForkliftPiecesMCH(detailId, taskId, pcs, (long)m_user.userID);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                return null;
            }
        }


        public bool RecoverFlightMCH(long taskId)
        { 
            try {
                return CargoReceiverMCHService.Instance.RecoverFlightMCH(m_user.userID, taskId);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                return false;
            }

        }

        public bool DropPiecesToLocationMCH(long taskId, int locationId, long forkLiftDetailId, int pieces)
        {
            try
            {
                return CargoReceiverMCHService.Instance.DropPiecesToLocationMCH(taskId, m_user.userID, locationId, forkLiftDetailId, pieces);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                return false;
            }

        }

  

        public bool RecoverUldMCH(long uldId, long locationId, long manifestId)
        {
            try
            {
                return CargoReceiverMCHService.Instance.RecoverUldMCH(m_user.userID, uldId, locationId, manifestId);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                return false;
            }

        }
        
     

        public bool GetFlightsMCH(out CargoMatrix.Communication.DTO.FlightItem[] flightItems, CargoMatrix.Communication.DTO.FlightStatus status, string carrierNo, string origin, string flightNo, CargoMatrix.Communication.DTO.FlightSort sortBy)
        {
            WSCargoReceiverMCHService.CargoReceiverFlight[] tempItems = null;
            try
            {


                tempItems = CargoReceiverMCHService.Instance.GetFlightsMCH(m_user.userID, m_gateway, m_connectionString, status, carrierNo, origin, flightNo, sortBy);


                flightItems = new CargoMatrix.Communication.DTO.FlightItem[tempItems.Length];


                for (int i = 0; i < tempItems.Length; i++)
                {
                    flightItems[i] = new CargoMatrix.Communication.DTO.FlightItem();
                    flightItems[i].FlightNumber = tempItems[i].FlightNumber;
                    flightItems[i].AWBCount = tempItems[i].AWBCount;
                    flightItems[i].CarrierCode = tempItems[i].CarrierCode;
                    flightItems[i].CarrierName = tempItems[i].CarrierName;
                    try
                    {
                        flightItems[i].ETA = DateTime.Parse(tempItems[i].ETA.ToString());
                    }
                    catch
                    {
                        flightItems[i].ETA = DateTime.Now;
                    }
                   
                    flightItems[i].Origin = tempItems[i].Origin;
                    flightItems[i].ULDCount = tempItems[i].ULDCount;
                    flightItems[i].TotalPieces = tempItems[i].TotalPieces;
                    flightItems[i].TaskId = tempItems[i].TaskId;

                    flightItems[i].RecoveredULDs = tempItems[i].RecoveredULDs;
                    flightItems[i].ReceivedPieces = tempItems[i].ReceivedPieces;
                    flightItems[i].FlightManifestId = tempItems[i].FlightManifestId;
                    try
                    {
                        flightItems[i].RecoveredDate = DateTime.Parse(tempItems[i].RecoveredDate.ToString());
                        flightItems[i].RecoveredBy = tempItems[i].RecoveredBy;
                    }
                    catch
                    {
                        flightItems[i].RecoveredDate = DateTime.Now;
                        flightItems[i].RecoveredBy = string.Empty;
                    }


                    switch (tempItems[i].Status)
                    {
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.All:
                            flightItems[i].Status = FlightStatus.All;
                            break;
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.Completed:
                            flightItems[i].Status = FlightStatus.Completed;
                            break;
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.InProgress:
                            flightItems[i].Status = FlightStatus.InProgress;
                            break;
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.Open:
                            flightItems[i].Status = FlightStatus.Open;
                            break;
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.Pending:
                            flightItems[i].Status = FlightStatus.Pending;
                            break;
                        default:
                            flightItems[i].Status = FlightStatus.All;
                            break;
                    }


                    //switch (tempItems[i].Status)
                    //{
                    //    case "All":
                    //        flightItems[i].Status = FlightStatus.All;
                    //        break;
                    //    case "Completed":
                    //        flightItems[i].Status = FlightStatus.Completed;
                    //        break;
                    //    case "InProgress":
                    //        flightItems[i].Status = FlightStatus.InProgress;
                    //        break;
                    //    case "Open":
                    //        flightItems[i].Status = FlightStatus.Open;
                    //        break;
                    //    case "Pending":
                    //        flightItems[i].Status = FlightStatus.Pending;
                    //        break;
                    //    default:
                    //        flightItems[i].Status = FlightStatus.All;
                    //        break;
                    //}



                }
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                flightItems = null;
                return false;
            }
            return true;
        }


        public  CargoMatrix.Communication.DTO.FlightItem GetFlightMCH( long manifestId,long taskId)
        {
            WSCargoReceiverMCHService.CargoReceiverFlight tempItem = null;
            try
            {


                tempItem = CargoReceiverMCHService.Instance.GetFlightMCH(manifestId,taskId,m_user.userID);


                CargoMatrix.Communication.DTO.FlightItem flightItem = new CargoMatrix.Communication.DTO.FlightItem();


                flightItem = new CargoMatrix.Communication.DTO.FlightItem();
                flightItem.FlightNumber = tempItem.FlightNumber;
                flightItem.AWBCount = tempItem.AWBCount;
                flightItem.CarrierCode = tempItem.CarrierCode;
                flightItem.CarrierName = tempItem.CarrierName;
                    try
                    {
                        flightItem.ETA = DateTime.Parse(tempItem.ETA.ToString());
                    }
                    catch
                    {
                        flightItem.ETA = DateTime.Now;
                    }

                    flightItem.Origin = tempItem.Origin;
                    flightItem.ULDCount = tempItem.ULDCount;
                    flightItem.TotalPieces = tempItem.TotalPieces;
                    flightItem.TaskId = tempItem.TaskId;

                    flightItem.RecoveredULDs = tempItem.RecoveredULDs;
                    flightItem.ReceivedPieces = tempItem.ReceivedPieces;
                    flightItem.FlightManifestId = tempItem.FlightManifestId;
                    flightItem.Flags = (int)tempItem.Flag;
                    try
                    {
                        flightItem.RecoveredDate = DateTime.Parse(tempItem.RecoveredDate.ToString());
                        flightItem.RecoveredBy = tempItem.RecoveredBy;
                    }
                    catch
                    {
                        flightItem.RecoveredDate = DateTime.Now;
                        flightItem.RecoveredBy = string.Empty;
                    }


                    switch (tempItem.Status)
                    {
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.All:
                            flightItem.Status = FlightStatus.All;
                            break;
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.Completed:
                            flightItem.Status = FlightStatus.Completed;
                            break;
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.InProgress:
                            flightItem.Status = FlightStatus.InProgress;
                            break;
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.Open:
                            flightItem.Status = FlightStatus.Open;
                            break;
                        case CargoMatrix.Communication.WSCargoReceiverMCHService.TaskStatuses.Pending:
                            flightItem.Status = FlightStatus.Pending;
                            break;
                        default:
                            flightItem.Status = FlightStatus.All;
                            break;
                    }


                    return flightItem;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                return null;
            }
 
        }



        public string GetLocationMCH()
        {

            return CargoReceiverMCHService.Instance.GetLocationMCH();

        }

        public bool FinalizeReceiverMCH(long taskId, long manifestId)
        {

            try
            {


                return CargoReceiverMCHService.Instance.FinalizeReceiverMCH((long)m_user.userID,   taskId, manifestId);



            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);

                return false;
            }

        }

        public bool DropForkliftPiecesMCH(long taskId,int locationId)
        {

            try
            {


                return CargoReceiverMCHService.Instance.DropForkliftPiecesMCH(m_user.userID, m_gateway, taskId, locationId);



            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);

                return false;
            }

        }

        //public bool UpdateFlightETAMCH(int userId, string gateway, long taskId, long manifestId, DateTime eta)
        //{
        //    try
        //    {

        //        string currentGateway = string.Empty;
        //        if (MCHSecurityManager.connectionName == string.Empty)
        //        {
        //            currentGateway = gateway;
        //        }
        //        else
        //        {
        //            currentGateway = MCHSecurityManager.gateway;
        //        }



        //        MchScannerService.TransactionStatus t = wsSecurity.UpdateFlightETA(currentGateway, manifestId, taskId, uldId, userId, eta);
        //        return t.Status;

        //    }
        //    catch
        //    {
        //        return false;
        //    }




        //}

        public bool UpdateFlightETAMCH(long taskId, long manifestId, DateTime eta)
        {

            try
            {

                return CargoReceiverMCHService.Instance.UpdateFlightETAMCH(m_user.userID, taskId, manifestId, eta);



            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);

                return false;
            }

        }
        public bool BUPChangeMCH(long manifestId, long uldId)
        {
             
            try
            {


                return CargoReceiverMCHService.Instance.BUPChangeMCH(m_user.userID , manifestId, uldId);


                
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
             
                return false;
            }
         
        }


        public CargoMatrix.Communication.DTO.ShipmentItem[] GetFlightViewMCH(long taskId, long manifestId)
        {

            try
            {

                WSCargoReceiverMCHService.UldView[] tempItems = CargoReceiverMCHService.Instance.GetFlightViewMCH(taskId, manifestId);
 
                ShipmentItem[] forkliftItems = new CargoMatrix.Communication.DTO.ShipmentItem[tempItems.Length];
                for (int i = 0; i < tempItems.Length; i++)
                {
                    forkliftItems[i] = new CargoMatrix.Communication.DTO.ShipmentItem();
                    forkliftItems[i].IsForkLift = false;
                    //forkliftItems[i].Hwb = tempItems[i].Hwb;
                    forkliftItems[i].AWB = tempItems[i].AWB;
                    forkliftItems[i].TotalPieces = tempItems[i].TotalPieces;
                    forkliftItems[i].ForkliftPieces = tempItems[i].ForkliftPieces;
                    forkliftItems[i].ReceivedPieces = tempItems[i].ReceivedPieces;
                    forkliftItems[i].AvailablePieces = tempItems[i].AvailablePieces;
                    forkliftItems[i].Locations = tempItems[i].Locations;
                    forkliftItems[i].DetailId = tempItems[i].DetailId;
                    forkliftItems[i].Uld = tempItems[i].Uld;
                    

                    StringBuilder references = new StringBuilder(string.Empty);
                    try
                    {
                        foreach (string reference in tempItems[i].References)
                        {
                            references.Append(" " + reference);
                        }
                    }
                    catch { }
         
                    forkliftItems[i].References = references.ToString();
                    forkliftItems[i].Flags = (int)tempItems[i].Flag;

                }
                return forkliftItems;
                 
 


            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);

                return null;
            }

        }


        public CargoMatrix.Communication.DTO.ShipmentItem[] GetUldViewMCH(long taskId, long uldId)
        {

            try
            {

                WSCargoReceiverMCHService.UldView[] tempItems = CargoReceiverMCHService.Instance.GetUldViewMCH(m_user.userID, taskId, uldId);
                 

                //if (tempItems != null && tempItems.Length > 0)
                //{
                    ShipmentItem[] forkliftItems = new CargoMatrix.Communication.DTO.ShipmentItem[tempItems.Length];
                    for (int i = 0; i < tempItems.Length; i++)
                    {
                        forkliftItems[i] = new CargoMatrix.Communication.DTO.ShipmentItem();
                        forkliftItems[i].IsForkLift = false;
                        //forkliftItems[i].Hwb = tempItems[i].Hwb;
                        forkliftItems[i].AWB = tempItems[i].AWB;
                        forkliftItems[i].TotalPieces = tempItems[i].TotalPieces;
                        forkliftItems[i].ForkliftPieces = tempItems[i].ForkliftPieces;
                        forkliftItems[i].ReceivedPieces = tempItems[i].ReceivedPieces;
                        forkliftItems[i].AvailablePieces = tempItems[i].AvailablePieces;
                        forkliftItems[i].Locations = tempItems[i].Locations;
                        forkliftItems[i].DetailId = tempItems[i].DetailId;
                        forkliftItems[i].Uld = tempItems[i].Uld;
                        StringBuilder references = new StringBuilder(string.Empty);
                        try
                        {
                            foreach (string reference in tempItems[i].References)
                            {
                                references.Append(" " + reference);
                            }
                        }
                        catch { }
                        forkliftItems[i].References = references.ToString();
                        forkliftItems[i].Flags = (int)tempItems[i].Flag;

                    }
                    return forkliftItems;
                //}
                //else
                //{
                //    ShipmentItem[] forkliftItems = new CargoMatrix.Communication.DTO.ShipmentItem[1];

                //    forkliftItems[0] = new CargoMatrix.Communication.DTO.ShipmentItem();
                //    forkliftItems[0].IsForkLift = true;
                //    forkliftItems[0].Hwb = "AAAAA";
                //    forkliftItems[0].AWB = "BBBBB";
                //    forkliftItems[0].TotalPieces = 1;
                //    forkliftItems[0].ForkliftPieces = 2;
                //    forkliftItems[0].ReceivedPieces = 3;
                //    forkliftItems[0].AvailablePieces = 4;
                //    forkliftItems[0].Locations = "CCCCCCC";
                //    forkliftItems[0].DetailId = 1;
                //    return forkliftItems;
                //}




            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);

                return null;
            }

        }

        public CargoMatrix.Communication.DTO.ShipmentItem[] GetForkliftViewMCH(long taskId)
        {

            try
            {

                WSCargoReceiverMCHService.ForkliftView[] tempItems = CargoReceiverMCHService.Instance.GetForkliftViewMCH(m_user.userID, taskId);


                //if (tempItems != null && tempItems.Length > 0)
                //{
                    ShipmentItem[] forkliftItems = new CargoMatrix.Communication.DTO.ShipmentItem[tempItems.Length];
                    for (int i = 0; i < tempItems.Length; i++)
                    {
                        forkliftItems[i] = new CargoMatrix.Communication.DTO.ShipmentItem();
                        forkliftItems[i].IsForkLift = true;
                        //forkliftItems[i].Hwb = tempItems[i].Hwb;
                        forkliftItems[i].AWB = tempItems[i].AWB;
                        forkliftItems[i].TotalPieces = tempItems[i].TotalPieces;
                        forkliftItems[i].ForkliftPieces = tempItems[i].ForkliftPieces;
                        forkliftItems[i].ReceivedPieces = tempItems[i].ReceivedPieces;
                        forkliftItems[i].AvailablePieces = tempItems[i].AvailablePieces;
                        forkliftItems[i].Locations = tempItems[i].Locations;
                        forkliftItems[i].DetailId =  tempItems[i].DetailId;
                        forkliftItems[i].References = string.Empty;
                        forkliftItems[i].Flags = 0;
                        forkliftItems[i].Counter = tempItems[i].Counter;
                     
                    }
                    return forkliftItems;
                //}
                //else
                //{
                //    ShipmentItem[] forkliftItems = new CargoMatrix.Communication.DTO.ShipmentItem[1];

                //    forkliftItems[0] = new CargoMatrix.Communication.DTO.ShipmentItem();
                //        forkliftItems[0].IsForkLift = true;
                //        forkliftItems[0].Hwb = "AAAAA";
                //        forkliftItems[0].AWB = "BBBBB";
                //        forkliftItems[0].TotalPieces = 1;
                //        forkliftItems[0].ForkliftPieces = 2;
                //        forkliftItems[0].ReceivedPieces = 3;
                //        forkliftItems[0].AvailablePieces = 4;
                //        forkliftItems[0].Locations = "CCCCCCC";
                //        forkliftItems[0].DetailId = 1;
                //    return forkliftItems;
                //}




            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);

                return null;
            }

        }


        public WSCargoReceiverMCHService.Uld GetFlightULDMCH(long uldId)
        {

            try
            {


                return CargoReceiverMCHService.Instance.GetFlightULDMCH(uldId);



            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);

                return null;
            }

        }

        public int GetForkliftCountMCH(long userId, long taskId)
        {

            try
            {


                return CargoReceiverMCHService.Instance.GetForkliftCountMCH(userId, taskId);



            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);

                return 0;
            }

        }



        public bool RemoveItemsFromForkliftMCH(long forkliftDetailsId, int piece)
        {
            return CargoReceiverMCHService.Instance.RemoveItemsFromForkliftMCH(forkliftDetailsId, piece);
        }
        public bool GetFlightULDsMCH(out CargoMatrix.Communication.DTO.UldItem [] uldItems, CargoMatrix.Communication.DTO.RecoverStatuses status, long manifestId)
        {
            WSCargoReceiverMCHService.FlightUldInfo flightInfo = null;
            try
            {


                flightInfo = CargoReceiverMCHService.Instance.GetFlightULDsMCH(m_user.userID, m_gateway, m_connectionString, status, manifestId);


                uldItems = new CargoMatrix.Communication.DTO.UldItem[flightInfo.Ulds.Length];


                for (int i = 0; i < uldItems.Length; i++)
                {
                    uldItems[i] = new CargoMatrix.Communication.DTO.UldItem();
                    uldItems[i].IsBUP = flightInfo.Ulds[i].IsBUP;
                    uldItems[i].Location = flightInfo.Ulds[i].Location;
                    uldItems[i].Status = (CargoMatrix.Communication.DTO.RecoverStatuses)flightInfo.Ulds[i].Status;
                    uldItems[i].TotalPieces = flightInfo.Ulds[i].TotalPieces;
                    uldItems[i].ReceivedPieces = flightInfo.Ulds[i].ReceivedPieces;
                    uldItems[i].UldSerialNo = flightInfo.Ulds[i].UldSerialNo;
                    uldItems[i].UldType = flightInfo.Ulds[i].UldType;
                    uldItems[i].UldId = flightInfo.Ulds[i].UldId;
                    uldItems[i].UldPrefix = flightInfo.Ulds[i].UldPrefix;
                    uldItems[i].FlightManifestId = flightInfo.FlightManifestId;

                    StringBuilder references = new StringBuilder(string.Empty);
                    try
                    {
                        foreach (string reference in flightInfo.Ulds[i].References)
                        {
                            references.Append(" " + reference);
                        }
                    }
                    catch { }
                    uldItems[i].References = references.ToString();
                     
                }
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                uldItems = null;
                return false;
            }
            return true;
        }

        public int LoginByUserID(string username, string password)
        {
            return SecurityManager.Instance.LoginByUserID(username, password);

        }

        private void Send(string data)
        {
        }
        private string Receive()
        {
            return null;
        }
        public bool CheckGuid()
        {

            int result = 0;
            try
            {

                result = webservice.CheckGuid(GuidManager.GetGuid().ToString(), m_user.UserName, m_gateway, m_connectionString);

            }
            catch (Exception e)
            {

                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10003)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation); 
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10003);

            }
            if (result != 1)
            {
                if (result == 0)
                    CargoMatrix.UI.CMXMessageBox.Show("User session is expired on this device", "Error!" + " (10004)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                else
                    CargoMatrix.UI.CMXMessageBox.Show("An Unexpected error has occured", "Error!" + " (10005)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                CargoMatrix.UI.CMXAnimationmanager.DoLogout();
            }

            return true;
        }
        public bool GetMainMenu(out CargoMatrix.Communication.DTO.MainMenuItem[] mainMenuItems, out bool newVersionAvailable)
        {
            CargoMatrix.Communication.ScannerWebService.MainMenuItem[] tempItems = null;
            try
            {

            

                int latestversion;
                
               

                //tempItems = webservice.GetMainMenu(GuidManager.GetGuid().ToString(), m_user.UserName, m_gateway, m_connectionString, out latestversion);
                tempItems = webservice.GetMainMenuItems(GuidManager.GetGuid().ToString(), m_user.UserName, m_gateway, m_connectionString, out latestversion);

               

                
                
                newVersionAvailable = Utilities.AppCurrentVersion < latestversion ? true : false;

                mainMenuItems = new CargoMatrix.Communication.DTO.MainMenuItem[tempItems.Length];
                for (int i = 0; i < tempItems.Length; i++)
                {
                    mainMenuItems[i] = new CargoMatrix.Communication.DTO.MainMenuItem();
                    mainMenuItems[i].ActionID = tempItems[i].ActionID;
                    mainMenuItems[i].TotalCount = tempItems[i].TotalCount;
                    mainMenuItems[i].NotCompleted = tempItems[i].NotCompleted; ;// tempItems[i].NotCompleted;
                    mainMenuItems[i].NotAssigned = tempItems[i].NotAssigned;
                    mainMenuItems[i].Completed = tempItems[i].Completed;
                    mainMenuItems[i].Icon = tempItems[i].Icon;
                    mainMenuItems[i].Label = tempItems[i].Label;
                    mainMenuItems[i].TaskCountByUser = tempItems[i].TotalCountByUser;
                }
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10006)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10006);
                mainMenuItems = null;
                newVersionAvailable = false;
                return false;
            }
            return true;
        }

        public bool GetFreightPhotoCaptureTasks(out CargoMatrix.Communication.DTO.FreightPhotoCapture[] tasks, string filter)
        {
            CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture[] tempTasks = null;
            //using (ScannerWebService.ScannerWebService webservice = new ScannerWebService.ScannerWebService())
            {
                try
                {
                    //if(filter.ToUpper() == ("Not Assigned").ToUpper())  //special case for not assigned
                    //    tempTasks = webservice.GetFreightPhotoCaptureTasks(GuidManager.GetGuid().ToString(), "", filter, m_gateway, m_connectionString);
                    //else                        
                    tempTasks = webservice.GetFreightPhotoCaptureTasks(GuidManager.GetGuid().ToString(), m_user.UserName, filter, m_gateway, m_connectionString);

                }
                catch (Exception e)
                {
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10007);
                    //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    tasks = null;

                    return false;

                }
                if (tempTasks == null)
                {
                    tasks = null;
                    return false;
                }
            }
            List<DTO.FreightPhotoCapture> taskList = new List<DTO.FreightPhotoCapture>();
            //tasks = new CargoMatrix.Communication.Data.TaskItem[tempTasks.Length];
            for (int i = 0; i < tempTasks.Length; i++)
            {
                taskList.Add(FPCConvertFromWS(tempTasks[i]));

            }
            tasks = taskList.ToArray();
            return true;


        }

        public bool GetTasks(out CargoMatrix.Communication.DTO.TaskItem[] tasks, CargoMatrix.Communication.DTO.TaskType taskType)
        {

            CargoMatrix.Communication.ScannerWebService.TaskItem[] tempTasks = null;
            //using (ScannerWebService.ScannerWebService webservice = new ScannerWebService.ScannerWebService())
            {
                try
                {

                    tempTasks = webservice.GetTasks(GuidManager.GetGuid().ToString(), m_user.UserName, m_gateway, m_connectionString);

                }
                catch (Exception e)
                {

                    //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10008)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10008);
                    tasks = null;

                    return false;

                }
                if (tempTasks == null)
                {
                    tasks = null;
                    return false;
                }
            }
            List<DTO.TaskItem> taskList = new List<DTO.TaskItem>();
            //tasks = new CargoMatrix.Communication.Data.TaskItem[tempTasks.Length];
            for (int i = 0; i < tempTasks.Length; i++)
            {
                if ((int)taskType == (int)tempTasks[i].taskType)
                {
                    switch (tempTasks[i].taskType)
                    {
                        case CargoMatrix.Communication.ScannerWebService.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
                            DTO.FreightPhotoCapture photoCapture = new DTO.FreightPhotoCapture();
                            photoCapture.reference = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).reference;
                            photoCapture.line2 = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).line2;
                            photoCapture.line3 = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).line3;
                            photoCapture.taskPicture = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).taskPicture;
                            int x = (int)(tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).taskType;
                            photoCapture.taskType = (DTO.TaskType)x;

                            CargoMatrix.Communication.ScannerWebService.FreightPhotoCaptureItem[] items = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).itemsList;
                            photoCapture.itemsList = new List<DTO.FreightPhotoCaptureItem>();
                            for (int j = 0; j < items.Length; j++)
                            {
                                photoCapture.itemsList.Add(new DTO.FreightPhotoCaptureItem(items[j].reason, null, 0));
                            }
                            taskList.Add(photoCapture);
                            break;

                        case CargoMatrix.Communication.ScannerWebService.TaskType.UNKNOWN:
                            DTO.TaskItem unknownTaskItem = new DTO.TaskItem();
                            unknownTaskItem.taskPicture = tempTasks[i].taskPicture;
                            x = (int)tempTasks[i].taskType;
                            unknownTaskItem.taskType = (DTO.TaskType)x;
                            taskList.Add(unknownTaskItem);

                            break;



                    }
                }


            }
            tasks = taskList.ToArray();
            return true;

        }

        public bool GetTasks(out CargoMatrix.Communication.DTO.TaskItem[] tasks)
        {

            CargoMatrix.Communication.ScannerWebService.TaskItem[] tempTasks = null;
            //using (ScannerWebService.ScannerWebService webservice = new ScannerWebService.ScannerWebService())
            {
                try
                {

                    tempTasks = webservice.GetTasks(GuidManager.GetGuid().ToString(), m_user.UserName, m_gateway, m_connectionString);

                }
                catch (Exception e)
                {

                    //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10009)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10009);
                    tasks = null;

                    return false;

                }
                if (tempTasks == null)
                {
                    tasks = null;
                    return false;
                }
            }
            List<DTO.TaskItem> taskList = new List<DTO.TaskItem>();
            //tasks = new CargoMatrix.Communication.Data.TaskItem[tempTasks.Length];
            for (int i = 0; i < tempTasks.Length; i++)
            {
                switch (tempTasks[i].taskType)
                {
                    case CargoMatrix.Communication.ScannerWebService.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
                        DTO.FreightPhotoCapture photoCapture = new DTO.FreightPhotoCapture();
                        //photoCapture.bin = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).bin;
                        //photoCapture.description = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).description;
                        photoCapture.reference = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).reference;
                        photoCapture.line2 = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).line2;
                        photoCapture.line3 = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).line3;
                        //photoCapture.pieceMode = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).pieceMode;
                        //photoCapture.location = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).location;
                        //photoCapture.piece = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).piece;
                        //photoCapture.rack = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).rack;
                        //photoCapture.task = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).task;
                        photoCapture.taskPicture = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).taskPicture;
                        int x = (int)(tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).taskType;
                        photoCapture.taskType = (DTO.TaskType)x;

                        CargoMatrix.Communication.ScannerWebService.FreightPhotoCaptureItem[] items = (tempTasks[i] as CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture).itemsList;
                        photoCapture.itemsList = new List<DTO.FreightPhotoCaptureItem>();
                        for (int j = 0; j < items.Length; j++)
                        {
                            photoCapture.itemsList.Add(new DTO.FreightPhotoCaptureItem(items[j].reason, null, 0));
                        }
                        taskList.Add(photoCapture);
                        break;

                    case CargoMatrix.Communication.ScannerWebService.TaskType.UNKNOWN:
                        DTO.TaskItem unknownTaskItem = new DTO.TaskItem();
                        //unknownTaskItem.description = tempTasks[i].description;
                        //unknownTaskItem.task = tempTasks[i].task;
                        unknownTaskItem.taskPicture = tempTasks[i].taskPicture;
                        x = (int)tempTasks[i].taskType;
                        unknownTaskItem.taskType = (DTO.TaskType)x;
                        taskList.Add(unknownTaskItem);

                        break;



                }


            }
            tasks = taskList.ToArray();
            return true;

        }

        public bool GetReasons(string taskID, out  CargoMatrix.Communication.DTO.FreightPhotoCapture photoCaptureData)
        {

            photoCaptureData = new CargoMatrix.Communication.DTO.FreightPhotoCapture();
            try
            {
                CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture temp = webservice.GetPhotoCaptureReasons(GuidManager.GetGuid().ToString(), m_user.UserName, System.Convert.ToInt32(taskID), m_gateway, m_connectionString);
                if (temp != null)
                {
                    //photoCaptureData.bin = temp.bin;
                    photoCaptureData.reference = temp.reference;
                    photoCaptureData.line2 = temp.line2;
                    photoCaptureData.line3 = temp.line3;
                    //photoCaptureData.pieceMode = temp.pieceMode;

                    photoCaptureData.itemsList = new List<CargoMatrix.Communication.DTO.FreightPhotoCaptureItem>();
                    {
                        for (int i = 0; i < temp.itemsList.Length; i++)
                        {
                            photoCaptureData.itemsList.Add(new CargoMatrix.Communication.DTO.FreightPhotoCaptureItem(temp.itemsList[i].reason, null, 0)); // imagelist is null in the begining
                        }

                    }

                    //photoCaptureData.location = temp.location;
                    //photoCaptureData.piece = temp.piece;
                    //photoCaptureData.rack = temp.rack;
                    return true;

                }
                else
                {
                    CheckGuid();
                    return false;

                }

            }
            catch (Exception e)
            {

                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10011)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10011);
                photoCaptureData = null;
                return false;


            }

        }
        public bool UploadPhotoCapture(string taskID, CargoMatrix.Communication.DTO.FreightPhotoCapture photoCaptureData)
        {
            CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture tempPhotoCapture = new CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture();
            //tempPhotoCapture.bin = photoCaptureData.bin;
            tempPhotoCapture.reference = photoCaptureData.reference;
            tempPhotoCapture.line2 = photoCaptureData.line2;
            tempPhotoCapture.line3 = photoCaptureData.line3;
            //tempPhotoCapture.pieceMode = photoCaptureData.pieceMode;
            //tempPhotoCapture.location = photoCaptureData.location;
            //tempPhotoCapture.piece = photoCaptureData.piece;
            //tempPhotoCapture.rack = photoCaptureData.rack;
            tempPhotoCapture.taskID = taskID;
            tempPhotoCapture.itemsList = new CargoMatrix.Communication.ScannerWebService.FreightPhotoCaptureItem[photoCaptureData.itemsList.Count];
            for (int i = 0; i < photoCaptureData.itemsList.Count; i++)
            {
                tempPhotoCapture.itemsList[i] = new CargoMatrix.Communication.ScannerWebService.FreightPhotoCaptureItem();
                tempPhotoCapture.itemsList[i].reason = photoCaptureData.itemsList[i].reason;
                if (photoCaptureData.itemsList[i].imagelist != null)
                {
                    tempPhotoCapture.itemsList[i].imagelist = new CargoMatrix.Communication.ScannerWebService.ImageObject[photoCaptureData.itemsList[i].imagelist.Count];
                    for (int j = 0; j < photoCaptureData.itemsList[i].imagelist.Count; j++)
                    {
                        tempPhotoCapture.itemsList[i].imagelist[j] = new CargoMatrix.Communication.ScannerWebService.ImageObject();
                        tempPhotoCapture.itemsList[i].imagelist[j].m_rawImage = photoCaptureData.itemsList[i].imagelist[j].m_rawImage;//.ToArray();
                        tempPhotoCapture.itemsList[i].imagelist[j].m_time = photoCaptureData.itemsList[i].imagelist[j].m_time;
                        tempPhotoCapture.itemsList[i].imagelist[j].m_index = photoCaptureData.itemsList[i].imagelist[j].m_index;
                        tempPhotoCapture.itemsList[i].imagelist[j].m_pieceNo = photoCaptureData.itemsList[i].imagelist[j].m_pieceNo;
                    }
                }

            }
            bool result = false;
            try
            {
                result = webservice.UploadPhotoCapture(GuidManager.GetGuid().ToString(), m_user.UserName, tempPhotoCapture, m_gateway, m_connectionString);
            }
            catch (Exception e)
            {

                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10012)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10012);


                return false;

            }

            return result;
        }

        public void TaskInProgress(string taskID)
        {
            if (taskID != null)
            {
                try
                {
                    webservice.OpenTask(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, taskID, m_gateway, m_connectionString);//.UpdateTask("In Progress", "104", taskID, m_user.UserID, DateTime.Now.ToShortDateString(), "", m_gateway, m_connectionString);
                }
                catch (Exception e)
                {

                    //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10013)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10013);


                }
            }
        }
        public void CloseTask(string taskID)
        {
            if (taskID != null)
            {
                try
                {
                    webservice.CloseTask(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, taskID, m_gateway, m_connectionString);//.UpdateTask("In Progress", "104", taskID, m_user.UserID, DateTime.Now.ToShortDateString(), "", m_gateway, m_connectionString);
                }
                catch (Exception e)
                {

                     // CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10024);


                }
            }
        }

        public bool GetExistingImage(ref DTO.FreightPhotoCapture photoCapture)
        {
            try
            {
                ScannerWebService.FreightPhotoCapture tempTask = FPCConvertToWS(photoCapture);
                webservice.GetExistingImages(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, ref tempTask, m_gateway, m_connectionString);
                photoCapture = FPCConvertFromWS(tempTask);

            }
            catch (Exception e)
            {

                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10014)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10014);


                throw e;

            }
            if (photoCapture != null)
            {
                if (photoCapture.itemsList != null)
                {
                    for (int i = 0; i < photoCapture.itemsList.Count; i++)
                    {
                        if (photoCapture.itemsList[i].HasData)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;


        }
        private ScannerWebService.FreightPhotoCapture FPCConvertToWS(CargoMatrix.Communication.DTO.FreightPhotoCapture item)
        {
            ScannerWebService.FreightPhotoCapture converted = new CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture();

            //converted.bin = item.bin;
            //converted.description = item.description;
            converted.origin = item.origin;
            converted.destination = item.destination;
            converted.reference = item.reference;
            converted.line2 = item.line2;
            converted.line3 = item.line3;
            converted.actualBill = item.actualBill;
            converted.carrier = item.carrier;
            //converted.itemsList = item.itemsList;
            //converted.location = item.location;
            //converted.piece = item.piece;
            //converted.rack = item.rack;
            converted.status = item.status;
            converted.statusCode = item.statusCode;
            //converted.task = item.task;
            converted.taskID = item.taskID;
            converted.taskPrefix = item.taskPrefix;
            converted.taskPicture = item.taskPicture;
            converted.logo = item.logo;
            //converted.pieceMode = item.pieceMode;

            //converted.taskType = item.taskType;

            int x = (int)item.taskType;
            converted.taskType = (ScannerWebService.TaskType)x;

            if (item.itemsList != null)
            {
                converted.itemsList = new CargoMatrix.Communication.ScannerWebService.FreightPhotoCaptureItem[item.itemsList.Count];// List<FreightPhotoCaptureItem>();
                for (int j = 0; j < item.itemsList.Count; j++)
                {
                    converted.itemsList[j] = new CargoMatrix.Communication.ScannerWebService.FreightPhotoCaptureItem();
                    converted.itemsList[j].reason = item.itemsList[j].reason;
                    converted.itemsList[j].ReasonType = item.itemsList[j].ReasonType;


                    //converted.itemsList[j].time = item.itemsList[j].time;
                    if (item.itemsList[j].imagelist != null)
                    {
                        for (int k = 0; k < item.itemsList[j].imagelist.Count; k++)
                        {
                            converted.itemsList[j].imagelist[k].m_rawImage = item.itemsList[j].imagelist[k].m_rawImage;
                            converted.itemsList[j].imagelist[k].m_time = item.itemsList[j].imagelist[k].m_time;
                            converted.itemsList[j].imagelist[k].m_location = item.itemsList[j].imagelist[k].m_location;
                            converted.itemsList[j].imagelist[k].m_userid = item.itemsList[j].imagelist[k].m_userid;
                            converted.itemsList[j].imagelist[k].m_reason = item.itemsList[j].imagelist[k].m_reason;
                        }
                    }
                }
            }

            return converted;


        }

        private CargoMatrix.Communication.DTO.FreightPhotoCapture FPCConvertFromWS(ScannerWebService.FreightPhotoCapture item)
        {
            CargoMatrix.Communication.DTO.FreightPhotoCapture converted = new DTO.FreightPhotoCapture();
            //converted.bin = item.bin;
            //converted.description = item.description;
            converted.origin = item.origin;
            converted.destination = item.destination;
            converted.reference = item.reference;
            converted.actualBill = item.actualBill;
            converted.carrier = item.carrier;
            converted.line2 = item.line2;
            converted.line3 = item.line3;

            //converted.itemsList = item.itemsList;
            //converted.location = item.location;
            //converted.piece = item.piece;
            //converted.rack = item.rack;
            converted.status = item.status;
            converted.statusCode = item.statusCode;
            //converted.task = item.task;
            converted.taskID = item.taskID;
            converted.taskPrefix = item.taskPrefix;
            converted.taskPicture = item.taskPicture;
            converted.logo = item.logo;

            //converted.mode = item.pieceMode;


            int x = (int)item.taskType;
            converted.taskType = (DTO.TaskType)x;

            if (item.itemsList != null)
            {
                converted.itemsList = new List<DTO.FreightPhotoCaptureItem>();// CargoMatrix.Communication.ScannerWebService.FreightPhotoCaptureItem[item.itemsList.Count];// List<FreightPhotoCaptureItem>();
                for (int j = 0; j < item.itemsList.Length; j++)
                {
                    FreightPhotoCaptureItem fpcItem = new FreightPhotoCaptureItem();
                    fpcItem.reason = item.itemsList[j].reason;
                    fpcItem.ReasonType = item.itemsList[j].ReasonType;
                    converted.itemsList.Add(fpcItem);
                    //converted.itemsList[j].time = item.itemsList[j].time;
                    if (item.itemsList[j].imagelist != null)
                    {
                        converted.itemsList[j].imagelist = new List<ImageObject>();
                        for (int k = 0; k < item.itemsList[j].imagelist.Length; k++)
                        {
                            ImageObject tempImg = new ImageObject();
                            tempImg.m_rawImage = item.itemsList[j].imagelist[k].m_rawImage;
                            tempImg.m_time = item.itemsList[j].imagelist[k].m_time;
                            tempImg.m_index = item.itemsList[j].imagelist[k].m_index;
                            tempImg.m_location = item.itemsList[j].imagelist[k].m_location;
                            tempImg.m_userid = item.itemsList[j].imagelist[k].m_userid;
                            tempImg.m_reason = item.itemsList[j].imagelist[k].m_reason;
                            tempImg.m_pieceNo = item.itemsList[j].imagelist[k].m_pieceNo;

                            converted.itemsList[j].imagelist.Add(tempImg);

                        }
                    }
                    //converted.itemsList[j].imagelist = item.itemsList[j].imagelist;
                }
            }

            return converted;


        }
        private ScannerWebService.ImageObject ImageObjectConvertToWS(CargoMatrix.Communication.DTO.ImageObject item)
        {
            ScannerWebService.ImageObject converted = new CargoMatrix.Communication.ScannerWebService.ImageObject();
            converted.m_rawImage = item.m_rawImage;
            converted.m_time = item.m_time;
            converted.m_location = item.m_location;
            converted.m_userid = item.m_userid;
            converted.m_reason = item.m_reason;
            return converted;
        }
        private CargoMatrix.Communication.DTO.ImageObject ImageObjectConvertFromWS(ScannerWebService.ImageObject item)
        {
            ImageObject tempImg = new ImageObject();
            tempImg.m_rawImage = item.m_rawImage;
            tempImg.m_time = item.m_time;
            tempImg.m_index = item.m_index;
            tempImg.m_location = item.m_location;
            tempImg.m_userid = item.m_userid;
            tempImg.m_reason = item.m_reason;
            tempImg.m_pieceNo = item.m_pieceNo;
            return tempImg;

        }
        public bool GetAllHouseBillReasons(out string[] reasons)
        {

            try
            {
                reasons = webservice.GetAllHouseBillReasons(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, m_gateway, m_connectionString);
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10015)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10015);
                reasons = null;
                return false;

            }
            return true;
        }

        public bool GetAllMasterBillReasons(out string[] reasons)
        {

            try
            {
                reasons = webservice.GetAllMasterBillReasons(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, m_gateway, m_connectionString);
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10015)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10015);
                reasons = null;
                return false;

            }
            return true;
        }
        public DTO.ImageObject GetFullImage(string housebill, int index)
        {
            ImageObject result = new ImageObject();
            CargoMatrix.Communication.ScannerWebService.ImageObject obj = null;

            //try
            {
                obj = webservice.GetImage(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, housebill, index, m_gateway, m_connectionString);

            }
            //catch (Exception e)
            //{
            //    //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10016)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);                
            //    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10016);
            //}
            if (obj != null)
            {
                result.m_index = obj.m_index;
                result.m_rawImage = obj.m_rawImage;
                result.m_time = obj.m_time;
                result.m_location = obj.m_location;
                result.m_userid = obj.m_userid;
                result.m_reason = obj.m_reason;
                result.m_pieceNo = obj.m_pieceNo;
            }
            return result;
        }
        public bool HouseBillExists(string houseBill)
        {

            return webservice.HouseBillExists(houseBill, m_connectionString);


        }
        public bool MasterBillExists(string carrier, string masterbill)
        {
            return webservice.MasterBillExists(carrier, masterbill, m_connectionString);
        }
        public bool TaskAssignedToUser(string houseBill, out CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {
            photoCapture = null;
            CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture tempPhotoCapture;
            bool result = false;
            try
            {
                // need to send userid here
                result = webservice.TaskAssignedToUser(GuidManager.GetGuid().ToString(), this.m_user.UserName, houseBill, m_gateway, m_connectionString, out tempPhotoCapture);
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10018)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10018);

                return false;

            }
            if (tempPhotoCapture != null)
            {
                photoCapture = FPCConvertFromWS(tempPhotoCapture);
            }
            return result;
            //webservice
        }

        public bool VerifySupervisor(string pin)
        {
            bool result = false;
            try
            {

                result = webservice.VerifySupervisor(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, pin, m_gateway, m_connectionString);
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10020)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10020);
                throw e;
                //return false;

            }
            return result;

        }

        public int GetLastFPCHouseBillTask(string housebill, out CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {
            photoCapture = null;
            string userID;
            int result = 0;
            CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture tempPhotoCapture;
            try
            {

                if (webservice.GetLastFPCHouseBillTask(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, housebill, m_gateway, m_connectionString, out userID, out tempPhotoCapture))//(pin, m_gateway, m_connectionString);
                {
                    if (userID == null || userID == "")
                    {
                        result = 3;
                    }
                    else if (userID.ToUpper() == m_user.UserName.ToUpper())
                    {
                        result = 1;
                    }
                    else
                        result = 2;
                }

            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10021)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10021);

                return -1;

            }
            if (tempPhotoCapture != null)
            {
                photoCapture = FPCConvertFromWS(tempPhotoCapture);
            }
            return result;
        }

        public int GetLastFPCMasterBillTask(string carrier, string masterbill, out CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {
            photoCapture = null;
            string userID;
            int result = 0;
            CargoMatrix.Communication.ScannerWebService.FreightPhotoCapture tempPhotoCapture;

            try
            {
                if (webservice.GetLastFPCMasterBillTask(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, carrier, masterbill, m_gateway, m_connectionString, out userID, out tempPhotoCapture))//(pin, m_gateway, m_connectionString);
                {
                    if (string.IsNullOrEmpty(userID))
                        result = 3;

                    else
                    {
                        if (userID.Equals(m_user.UserName, StringComparison.OrdinalIgnoreCase))
                            result = 1;

                        else
                            result = 2;
                    }
                }

            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10025);
                return -1;
            }

            if (tempPhotoCapture != null)
            {
                photoCapture = FPCConvertFromWS(tempPhotoCapture);
            }

            return result;
        }

        public bool AssignUser(string taskRecID)
        {
            bool result = false;
            try
            {

                result = webservice.AssignUser(CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, taskRecID, m_gateway, m_connectionString);
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10022)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10022);

                return false;

            }
            return result;

        }
        public bool CreateAndAssignNewHouseBillTask(string housebill, string origin, string destination, ref CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {
            FreightPhotoCapture tempFPC;
            bool result = false;
            try
            {

                string taskID = webservice.CreateAndAssignNewHouseBillTask("104", CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, housebill, origin, destination, m_gateway, m_connectionString);
                if (taskID == null)
                    return false;
                GetLastFPCHouseBillTask(housebill, out tempFPC);
                if (tempFPC == null)
                    return false;


                photoCapture.taskID = taskID;
                result = true;
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10023)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10023);

                return false;

            }
            return result;
        }

        public bool CreateAndAssignNewMasterBillTask(string carrier, string masterbill, string origin, string destination, ref CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {

            bool result = false;
            try
            {

                string taskID = webservice.CreateAndAssignNewMasterBillTask("104", CargoMatrix.Communication.GuidManager.GetGuid().ToString(), m_user.UserName, carrier, masterbill, origin, destination, m_gateway, m_connectionString);
                if (taskID == null)
                    return false;
                photoCapture.taskID = taskID;
                result = true;
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10023)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10023);

                return false;

            }
            return result;
        }

        public ShellData GetHousebillDetails(string carrier, string housebill)
        {
            ShellData hbData = new ShellData();
            try
            {
                CargoMatrix.Communication.ScannerWebService.ShellData wsData;
                if (carrier == null)
                    wsData = webservice.GetHousebillDetails(housebill, m_gateway, m_connectionString);
                else
                    wsData = webservice.GetMasterbillDetails(carrier, housebill, m_gateway, m_connectionString);

                if (wsData == null)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Unable to locate details for this shipment. Check if this shipment exists in system.", "Shipment details not found!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return null;

                }


                hbData.Name = wsData.Name;
                hbData.Reference = wsData.Reference;
                //ShellItem shellItem;
                for (int i = 0; i < wsData.m_pages.Length; i++)
                {
                    ShellPage shellPage = new ShellPage();
                    shellPage.Name = wsData.m_pages[i].Name;
                    shellPage.Icon = wsData.m_pages[i].Icon;
                    for (int j = 0; j < wsData.m_pages[i].m_items.Length; j++)
                    {
                        int x = (int)wsData.m_pages[i].m_items[j].m_type;



                        ShellItem shellItem = new ShellItem((ShellItemType)(int)wsData.m_pages[i].m_items[j].m_type,
                            wsData.m_pages[i].m_items[j].m_heading,
                            wsData.m_pages[i].m_items[j].m_description,
                            wsData.m_pages[i].m_items[j].m_image);
                        shellPage.AddItem(shellItem);
                    }
                    hbData.AddPage(shellPage);
                }

            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10026);
                return null;

            }
            return hbData;

        }

        public System.Drawing.Image GetImageFromServer(string location)
        {
            System.Drawing.Image image = null;
            try
            {
                byte[] bytes = webservice.GetImageBinaryString(location);
                if (bytes != null)
                {
                    System.IO.MemoryStream stream = null;
                    try
                    {
                        stream = new System.IO.MemoryStream(bytes);
                        image = new System.Drawing.Bitmap(stream);
                        stream.Close();
                    }
                    catch (Exception ex)
                    {
                        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 10027);
                    }
                    finally
                    {
                        if (stream != null)
                        {
                            stream.Close();
                        }

                    }
                }

            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10028);
            }

            return image;

        }

        public List<DTO.ImageObject> GetShellViewerImages(string reference)
        {
            List<ImageObject> list = new List<ImageObject>();

            try
            {
                ScannerWebService.ImageObject[] tempList;
                tempList = webservice.GetShellViewerImages(reference, m_gateway, m_connectionString);
                if (tempList != null)
                {
                    for (int i = 0; i < tempList.Length; i++)
                    {
                        list.Add(ImageObjectConvertFromWS(tempList[i]));
                    }
                }


            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10029);
                return null;

            }
            return list;

        }

        public bool DeleteTask(string taskRecID)
        {
            try
            {
                webservice.DeleteTask(taskRecID, m_user.UserName, m_gateway, m_connectionString);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10030);
                return false;

            }
            return true;

        }



        public CargoMatrix.Communication.WSCargoReceiverMCHService.ScannedShipmentInfo[] ScanShipmentMCH(long taskId, string shipmentNumber)
        {
            try
            {
                return CargoReceiverMCHService.Instance.ScanShipmentMCH(m_user.userID, taskId, shipmentNumber);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10030);
                return null;

            }

        }


        public CargoMatrix.Communication.WSCargoReceiverMCHService.Alert[] GetFlightAlertsMCH(  long manifestId)
        {
            try
            {
                return CargoReceiverMCHService.Instance.GetFlightAlertsMCH( manifestId);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10030);
                return null;

            }
   
        }
        public CargoMatrix.Communication.WSCargoReceiverMCHService.Alert[] GetUldAlertsMCH( long uldId)
        {
            try
            {
                return CargoReceiverMCHService.Instance.GetUldAlertsMCH(uldId);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10030);
                return null;

            }
        }

        public bool RenameReference(string hawbNo, string org, string dest)
        {
            try
            {
                return webservice.RenameReference(m_connectionString, hawbNo, org, dest);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10031);
                return false;
            }
        }



        public CargoMatrix.Communication.WSCargoReceiverMCHService.AvailableItmes[] GetAvailableCarriers(   )
        {
            try
            {
                return CargoReceiverMCHService.Instance.GetAvailableCarriers(  m_user.userID);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10030);
                return null;

            }
        }


        public CargoMatrix.Communication.WSCargoReceiverMCHService.AvailableFlight[] GetAvailableFlights(long carrierId, DateTime etd)
        {
            try
            {
                return CargoReceiverMCHService.Instance.GetAvailableFlights(carrierId, etd, m_user.userID);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10030);
                return null;

            }
        }



        public CargoMatrix.Communication.WSCargoReceiverMCHService.AvailableFlight[] GetAvailableFlightsByEta(long carrierId, DateTime eta)
        {
            try
            {
                return CargoReceiverMCHService.Instance.GetAvailableFlightsByEta(carrierId, eta, m_user.userID);
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10030);
                return null;

            }
        }

 

        public DateTime[] GetAvailableEtds(long carrierId)
        {
            return CargoReceiverMCHService.Instance.GetAvailableEtds(carrierId, m_user.userID);
        }

        public DateTime[] GetAvailableEtas(long carrierId)
        {
            return CargoReceiverMCHService.Instance.GetAvailableEtas(carrierId, m_user.userID);
        }


        public bool LinkTaskToUserId(long taskId)
        {
            return CargoReceiverMCHService.Instance.LinkTaskToUserId(taskId, m_user.userID);
        }


        public CargoMatrix.Communication.WSCargoReceiverMCHService.FlightProgress GetFlightProgress(long manifestId, long taskId)
        {
            return CargoReceiverMCHService.Instance.GetFlightProgress(manifestId, taskId);
        }

        public int GetAwbPiecesCountInLocation(long awbId, long locationId)
        {
            return CargoReceiverMCHService.Instance.GetAwbPiecesCountInLocation(awbId, locationId);
        }


        public bool RelocateAwbPieces(long awbId, int locationId, int newlocationId, int pcs, long taskid)
        {
            return CargoReceiverMCHService.Instance.RelocateAwbPieces(awbId, locationId, newlocationId, pcs, taskid, m_user.userID);
        }


        public int GetLocationId(string locationName)
        {
            return CargoReceiverMCHService.Instance.GetLocationId(locationName);
        }

     
 
    }
}
