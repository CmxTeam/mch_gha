﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.WarehouseManager
{
    public interface IRegion
    {
        /// <remarks/>
        int RecID
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        int TypeID
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        string Code
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        string Name
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        string Description
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        int WarehouseID
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        string WarehouseName
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        int GatewayID
        {
            get;
            
            set;
        }

        /// <remarks/>
        string GatewayCode
        {
            get;
            
            set;
            
        }
    }
}
