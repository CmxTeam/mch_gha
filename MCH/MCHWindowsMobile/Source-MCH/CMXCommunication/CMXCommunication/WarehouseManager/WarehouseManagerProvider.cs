﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSWarehouseManager;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication.WarehouseManager
{
    public class WarehouseManagerProvider
    {
        private CargoWarehouseManager service;
        private static WarehouseManagerProvider _instance;
        private string asmxName = "CargoWarehouseManager.asmx";
      
        private string ConnectionName
        {
            get
            {
                return WebServiceManager.Instance().m_connectionString;
            }
        }

        public CargoMatrix.Communication.DTO.User CurrentUser
        {
            get
            {
                return WebServiceManager.Instance().m_user;
            }

        }

        public string GateWayCode
        {
            get
            {
                return WebServiceManager.Instance().m_gateway;
            }
        }

        private int GateWayID
        {
            get
            {
                string code = GateWayCode;
                
                int? gateWayID = this.service.GetGatewayID(code);

                if (gateWayID.HasValue == false) throw new ApplicationException("Unable to locate gateway ID for " + code);

                return gateWayID.Value;
            }
        }


        public static WarehouseManagerProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WarehouseManagerProvider();
                }

                return _instance;
            }
        }

        private WarehouseManagerProvider()
        {
            this.service = new CargoWarehouseManager();
            this.service.Url = Settings.Instance.URLPath + asmxName;
        }


        public IWarehouse[] GetWareHoulseList()
        {
            Func<IWarehouse[]> func = () => this.service.SearchWarehouseForDevice(null, GateWayID.ToString(), null, null, null, null, null, null, null, null, null);

            return Utilities.Invoke<IWarehouse[]>(func, 70001, new IWarehouse[0]);
        }

        public IWarehouse AddWareHouse(IWarehouse wareHouse, Action<SoapExceptionManager.SoapExceptionDetails> errorAction)
        {
            Func<IWarehouse> func = () => this.service.CreateWarehouseForDevice(CurrentUser.UserName, GateWayID, wareHouse.Code, wareHouse.Name);

            return Utilities.Invoke<IWarehouse>(func, errorAction, 70002, null);
        }

        public IRegion[] GetRegionList(IWarehouse wareHouse)
        {
            Func<IRegion[]> func = () => this.service.WarehouseRegionListForDevice(GateWayID, wareHouse.RecID);

            return Utilities.Invoke<IRegion[]>(func, 70003, new IRegion[0]);
        }

        public IRegion AddRegion(IRegion region, Action<SoapExceptionManager.SoapExceptionDetails> errorAction)
        {
            Func<IRegion> func = () => this.service.CreateWarehouseRegionForDevice(GateWayCode, CurrentUser.UserName, region.WarehouseID, region.Code, region.Name, region.Description);

            return Utilities.Invoke(func, errorAction, 70004, null);
        }

        public ILocation AddLocation(ILocation location, Action<SoapExceptionManager.SoapExceptionDetails> errorAction)
        {
            Func<ILocation> func = () => this.service.CreateWarehouseLocationForDevice(GateWayCode, GateWayID, CurrentUser.UserName, (int)location.LocationType, location.WarehouseID, location.RegionTypeID, location.Code, location.Name);

            return Utilities.Invoke(func, errorAction, 70005, null);
        }

        public ILocation[] GetLocationList(IRegion region, LocationTypeEnum locationType)
        {
            Func<ILocation[]> func = () => this.service.SearchWarehouseLocationForDevice(GateWayID, region.WarehouseID, region.RecID, (int)locationType);

            return Utilities.Invoke<ILocation[]>(func, 70006, new ILocation[0]);
        }

    }
}
