﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.WarehouseManager
{
    public interface IWarehouse
    {
        int RecID
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        string ModifyBy
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        System.DateTime ModifyOn
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        string GatewayID
        {
            get;
            
            set;
            
        }

        /// <remarks/>
        string GatewayCode
        {
            get;

            set;

        }

        /// <remarks/>
        string Code
        {
            get;
            set;
            
        }

        /// <remarks/>
        string Name
        {
            get;

            set;

        }

        /// <remarks/>
        string Street1
        {
            get;

            set;

        }

        /// <remarks/>
        string Street2
        {
            get;

            set;

        }

        /// <remarks/>
        string City
        {
            get;
           
            set;
            
        }

        /// <remarks/>
        string StateCode
        {
            get;

            set;

        }

        /// <remarks/>
        string CountryCode
        {
            get;

            set;

        }

        /// <remarks/>
        string ZipCode
        {
            get;

            set;

        }
    }
}
