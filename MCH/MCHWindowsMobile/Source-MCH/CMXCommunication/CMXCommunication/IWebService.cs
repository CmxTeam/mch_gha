﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication
{
    public interface IWebServiceClient
    {
        int LoginByPin(string pin);
        int LoginByUserID(string userID, string password);
        //void SyncTime();
        bool CheckGuid();
        

    }
}
