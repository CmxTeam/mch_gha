﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml.Serialization;
using System.Linq;
using CargoMatrix.Communication.WSPieceScan;
using CargoMatrix.Communication.DTO;
using System.Xml;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication
{
    public class HostPlusIncomming : CMXServiceWrapper
    {
        WSPieceScan.WSPieceScan wsHostPlusIncoming;
        ScannerWebService.ScannerWebService wsScanner;
        //Context wsUserProfile;
        static HostPlusIncomming m_instance = null;
        string asmxName = "WSPieceScan.asmx";
        //private string failMessage = "Operation Failed";
        //private string connectionName { get { return CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString; } }
        //private string gateway { get { return CargoMatrix.Communication.WebServiceManager.Instance().m_gateway; } }
        //private string username { get { return CargoMatrix.Communication.WebServiceManager.Instance().m_user.UserName; } }

        public static HostPlusIncomming Instance
        {
            get
            {
                if (m_instance == null)
                    m_instance = new HostPlusIncomming();
                return m_instance;

            }
        }
        private HostPlusIncomming()
        {
            wsHostPlusIncoming = new WSPieceScan.WSPieceScan();
            wsHostPlusIncoming.Url = Settings.Instance.URLPath + asmxName;
            wsScanner = new CargoMatrix.Communication.ScannerWebService.ScannerWebService();
        }

        public IHouseBillItem[] GetHousebillList(int actionId, EnumStatus status, WSPieceScan.HBSortFields sort, string direction, string carrierNo, string reference, string mbDestination)
        {
            Func<IHouseBillItem[]> f = () =>
            {
                var results = wsHostPlusIncoming.HouseBillList(WebServiceManager.Instance().m_connectionString, WebServiceManager.Instance().m_gateway, actionId, status, WebServiceManager.Instance().m_user.UserName,
                    sort, string.Empty, carrierNo, reference, mbDestination);
                var ret = from item in results
                          select ConvertHAWBToIHousebillItem(item);
                return ret.ToArray<IHouseBillItem>();
            };
            return Invoke<CargoMatrix.Communication.DTO.IHouseBillItem[]>(f, 11201, new IHouseBillItem[0]);
        }

        private CargoMatrix.Communication.DTO.IHouseBillItem ConvertHAWBToIHousebillItem(HouseBillListItem item)
        {
            return new HouseBillitem(item);
        }


        public bool VerifySupervisorPIN(string pin)
        {
            Func<bool> f = () =>
            {
                string result = wsHostPlusIncoming.GetAdminUserName(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, pin);
                return !string.IsNullOrEmpty(result);

            };
            return Invoke<bool>(f, 11012, false);
        }

        public int GetULDCount(int masterbillRecID)
        {
            Func<int> f = () =>
            {
                return wsScanner.GetULDCount(masterbillRecID);

            };
            return Invoke<int>(f, 11021, -1);
        }

        public ScannerWebService.ULD[] GetULDDetails(int masterbillRecID)
        {
            Func<ScannerWebService.ULD[]> f = () =>
                        {
                            return wsScanner.GetULDDetails(masterbillRecID);

                        };
            return Invoke<ScannerWebService.ULD[]>(f, 11022, null);

        }

        private CargoMatrix.Communication.DTO.TaskItem ConvertHAWBToTaskItem(HouseBillListItem item)
        {
            if (item == null)
                return null;
            CargoMatrix.Communication.DTO.TaskItem taskItem = new CargoMatrix.Communication.DTO.TaskItem();
            taskItem.taskID = item.HouseBillID.ToString();
            taskItem.reference = item.Line1;
            taskItem.actualBill = item.HouseBillNo;
            taskItem.line2 = item.Line2;
            taskItem.line3 = item.Line3;
            taskItem.line4 = item.Line4;
            taskItem.totalPieces = item.NoOfPieces;
            taskItem.Flags = item.Flags;
            taskItem.mode = item.Mode;
            taskItem.CutOffTime = item.CutoffTime;
            taskItem.destination = item.Destination;
            taskItem.MawbDestination = item.MBDestination;
            taskItem.NoofPieces = item.Piece;
            taskItem.taskType = CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL;
            taskItem.origin = item.Origin;
            taskItem.TotalWeight = item.Weight;
            //taskItem.screeningStatusDescription = item.ScreeningStatusDescription;
            //taskItem.screeningStatusID = item.ScreeningStatusID;
            //switch (item.Mode)
            //{
            //    case Modes.Count:
            //        taskItem.mode = 0;
            //        break;
            //    case Modes.Piece:
            //        taskItem.mode = 1;
            //        break;
            //    case Modes.Slac:
            //        taskItem.mode = 2;
            //        break;
            //}
            //WSScanner.EnumStatus sts = item.Status;
            //WSScanner.EnumStatus sts = (WSScanner.EnumStatus)z;
            switch (item.Status)
            {
                //case CargoMatrix.Communication.WSPieceScan.HouseBillStatuses.NotCompleted:
                case EnumStatus.Open:
                    taskItem.statusCode = 'N';
                    break;
                case EnumStatus.InProgress:
                    taskItem.statusCode = 'I';
                    break;
                case EnumStatus.Completed:
                    taskItem.statusCode = 'C';
                    break;


            }
            return taskItem;
        }

        public bool VisualInspection(string housebillNo, int pieces, string remark)
        {
            Func<bool> f = () =>
            {
                wsHostPlusIncoming.VisualInspection(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, housebillNo, userName, pieces, remark);
                return true;

            };
            return Invoke<bool>(f, 11029, false);
        }

        //[Obsolete("use ResetInspection with UserName parameter")]
        //public bool ResetInspection(string housebillNo, int supervisorPIN)
        //{
        //    bool result = false;

        //    try
        //    {
        //        string connectionName = CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString;
        //        string username = wsHostPlusIncoming.GetAdminUserName(connectionName, supervisorPIN.ToString());
        //        if (string.IsNullOrEmpty(username))
        //            throw new Exception("Invalid Admin Pin Number");

        //        wsHostPlusIncoming.ResetScreening(connectionName, housebillNo, username);
        //        result = true;

        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11030);
        //        return false;
        //    }
        //    return result;

        //}

        //public bool CancelScreening(string housebillNo, string supervisorPIN)
        //{
        //    bool result = false;

        //    try
        //    {
        //        string connectionName = CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString;
        //        string username = wsHostPlusIncoming.GetAdminUserName(connectionName, supervisorPIN);
        //        if (string.IsNullOrEmpty(username))
        //            CargoMatrix.UI.CMXMessageBox.Show("Invalid Admin Pin Number", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //        wsHostPlusIncoming.CancelScreening(connectionName, housebillNo, username);
        //        result = true;

        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11032);
        //        return false;
        //    }
        //    return result;

        //}

        public bool IsScreeningCertified()
        {
            Func<bool> f = () =>
            {
                string certifiedName = wsHostPlusIncoming.GetScreeningCertifiedUserName(connectionName, CargoMatrix.Communication.WebServiceManager.Instance().m_user.pin);
                return string.Equals(userName, certifiedName, StringComparison.OrdinalIgnoreCase);
            };
            return Invoke<bool>(f, 11033, false);
        }

        public string VerifyVisualInspectionSupervisorPIN(string pin)
        {
            Func<string> f = () =>
            {
                return wsHostPlusIncoming.GetScreeningCertifiedUserName(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, pin);
            };
            return Invoke<string>(f, 11033, null);
        }

        public bool HAWBQualifiedForScreening(string housebillNo)
        {
            Func<bool> f = () =>
            {
                return wsHostPlusIncoming.IsFSPCompletedByHawb(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, housebillNo);
            };
            return Invoke<bool>(f, 11034, false);
        }

        public InventoryLocationGroup[] GetInventoryLocationReport(string location)
        {
            InventoryLocationGroup[] result = new InventoryLocationGroup[0];
            Func<InventoryLocationGroup[]> f = () =>
            {
                return wsHostPlusIncoming.GetInventoryLocationGroupList(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, location);
            };
            return Invoke<InventoryLocationGroup[]>(f, 11035, new InventoryLocationGroup[0]);
        }

        public InventoryLocationGroup[] GetCurrentInventoryReport(int taskId)
        {
            Func<InventoryLocationGroup[]> f = () =>
            {
                return wsHostPlusIncoming.GetInventoryLocationGroupListByTask(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, taskId);
            };
            return Invoke<InventoryLocationGroup[]>(f, 11036, new InventoryLocationGroup[0]);
        }

        //public int GetForkliftID()
        //{
        //    try
        //    {
        //        return wsHostPlusIncoming.GetForkliftID(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString);

        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11037);
        //    }
        //    return 0;
        //}

        //public MoveHBPiecesResponse MoveHouseBilltoForklift(string hawbNo, int actionId, int forkliftID, ScanTypes scanType)
        //{
        //    try
        //    {
        //        return wsHostPlusIncoming.MoveHouseBilltoForklift(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, CargoMatrix.Communication.WebServiceManager.Instance().m_gateway, actionId, hawbNo, forkliftID, CargoMatrix.Communication.WebServiceManager.Instance().m_user.userID, scanType);
        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11038);
        //    }
        //    return new MoveHBPiecesResponse { IsSuccess = false, Message = failMessage };
        //}
        //public MoveHBPiecesResponse MoveHouseBillPiecestoForklift(string hawbNo, int actionId, int forkliftID, int NoOfPieces, ScanTypes scanType)
        //{
        //    try
        //    {
        //        return wsHostPlusIncoming.MoveHouseBillPiecestoForklift(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, CargoMatrix.Communication.WebServiceManager.Instance().m_gateway, actionId, hawbNo, forkliftID, CargoMatrix.Communication.WebServiceManager.Instance().m_user.userID, scanType, NoOfPieces);
        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11039);
        //    }
        //    return new MoveHBPiecesResponse { IsSuccess = false, Message = failMessage };
        //}

        //public MoveHBPiecesResponse DropForkliftIntoLocation(int actionId, int forkliftID, string location, ScanTypes scanType)
        //{
        //    try
        //    {   //.DropHouseBillPiecesFromForklifttoETDLocation
        //        return wsHostPlusIncoming.DropFromForklifttoLocation(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, CargoMatrix.Communication.WebServiceManager.Instance().m_gateway, actionId, forkliftID, location, CargoMatrix.Communication.WebServiceManager.Instance().m_user.userID, scanType);
        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11040);
        //    }
        //    return new MoveHBPiecesResponse() { IsSuccess = false, Message = failMessage };
        //}
        //public FSPTaskSummaryItem GetFreightScreeningTaskDetails()
        //{
        //    try
        //    {
        //        return wsHostPlusIncoming.GetFSPTaskSummaryItem(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString);
        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11041);
        //    }
        //    return new FSPTaskSummaryItem();
        //}

        //public MoveHBPiecesResponse DropPiecesIntoLocation(int hawbId, int[] trackIds, int actionId, int forkliftID, string location)
        //{
        //    try
        //    {   //.DropHouseBillPiecesFromForklifttoETDLocation
        //        return wsHostPlusIncoming.DropHouseBillPiecesFromForklifttoLocation(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, CargoMatrix.Communication.WebServiceManager.Instance().m_gateway, actionId, forkliftID, location, CargoMatrix.Communication.WebServiceManager.Instance().m_user.userID, ScanTypes.Manual, hawbId, trackIds);
        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11042);
        //    }
        //    return new MoveHBPiecesResponse() { IsSuccess = false, Message = failMessage };
        //}


        public HouseBill GetHousebillInfo(string hawbNo)
        {
            Func<HouseBill> f = () =>
            {   //.DropHouseBillPiecesFromForklifttoETDLocation
                return wsHostPlusIncoming.GetHouseBill(CargoMatrix.Communication.WebServiceManager.Instance().m_connectionString, hawbNo, 105);
            };
            return Invoke<HouseBill>(f, 11043, null);
        }

        public bool CanineInspection(string housebillNo, int pieces, string remark, string cartNo)
        {
            Func<bool> f = () =>
            {
                wsHostPlusIncoming.CanineInspection(connectionName, housebillNo, userName, pieces, remark, cartNo);
                return true;

            };
            return Invoke<bool>(f, 11044, false);
        }

        public bool VerifyCustomerScreening(string housebillNo, int pieces, string remark, string ccsfNo)
        {
            Func<bool> f = () =>
            {
                wsHostPlusIncoming.VerifyCustomerScreening(connectionName, housebillNo, userName, pieces, remark, ccsfNo);
                return true;
            };
            return Invoke<bool>(f, 11045, false);
        }

        public bool XRayInspection(string housebillNo, int pieces, bool passState, string serialNo)
        {
            Func<bool> f = () =>
            {
                wsHostPlusIncoming.XRayInspection(connectionName, housebillNo, userName, pieces, passState, serialNo);
                return true;

            };
            return Invoke<bool>(f, 11046, false);

        }


        public bool ScreeningResult(string housebillNo, int pieces, bool passState, string serialNo,string remark ,int sampleNo,string screeningMethod)
        {
            Func<bool> f = () =>
            {
                wsHostPlusIncoming.ScreeningResult(connectionName, housebillNo, userName, pieces, passState, serialNo, remark, sampleNo, "", screeningMethod);
                return true;

            };
            return Invoke<bool>(f, 11046, false);

        }


        public InventoryLocationGroup[] GetForkliftInventoryReport(int taskId)
        {
            Func<InventoryLocationGroup[]> f = () =>
            {
                var result = wsHostPlusIncoming.GetInventoryLocationGroupListByTask(connectionName, taskId);
                var r = from rep in result
                        where string.Equals(rep.LocationName, "FORKLIFT", StringComparison.OrdinalIgnoreCase)
                        select rep;
                return r.ToArray<InventoryLocationGroup>();
            };

            return Invoke<InventoryLocationGroup[]>(f, 11047, new InventoryLocationGroup[0]);
        }

        public bool ResetInspection(string housebillNo)
        {
            Func<bool> f= ()=>
            {
                wsHostPlusIncoming.ResetScreening(connectionName, housebillNo, userName);
                return true;
            };
            return Invoke<bool>(f, 11048, false);
        }

        public string GetFSPDeviceNumber(string barcode, InspectionMode mode)
        {
            Func<string> f = ()=>
            {
                return wsHostPlusIncoming.GetScreeningDeviceSerialNo(mode.ToString(), barcode, connectionName);
            };

            return Invoke<string>(f,11048,string.Empty);
        }

        public void ClearAlarm(string housebillNo)
        {
            Action f = () =>
            {
                wsHostPlusIncoming.ClearAlarm(connectionName, gateway, housebillNo, userName);
            };
            Invoke(f,11049);
        }

        public Inspection[] GetInspectionReport(InspectionMode mode)
        {
            Func<Inspection[]> f = ()=>
            {
                return wsHostPlusIncoming.GetInspectionReport(connectionName, userName, mode.ToString());
            };
            return Invoke<Inspection[]>(f,11050,new Inspection[0]);
        }

        public void CreateHouseBillSkeleton(string hawbNo, int pcs, int slac, string origin, string dest, double weight, string carrier, string MawbNo)
        {
            Action f = ()=>
            {//wsHostPlusIncoming.CreateHouseBillSkeleton(connectionName, hawbNo, pcs, slac, origin, dest, weight, carrier, MawbNo);
                wsHostPlusIncoming.CreateHouseBillSkeleton(connectionName, hawbNo, pcs, slac, origin, dest);
            };
            Invoke(f,11051);
        }

        public FspDevice[] GetScrreningDevices(InspectionMode mode)
        {
            Func<FspDevice[]> f = ()=>
            {
                return wsHostPlusIncoming.GetFspDevices(mode.ToString(), connectionName);
            };
            return Invoke<FspDevice[]>(f, 11052, new FspDevice[0]);
        }

        public class HouseBillitem : IHouseBillItem
        {
            private WSPieceScan.HouseBillListItem hawb;
            internal HouseBillitem(CargoMatrix.Communication.WSPieceScan.HouseBillListItem item)
            {
                hawb = item;
            }

            #region IHouseBillItem Members

            public string HousebillNumber
            {
                get { return hawb.HouseBillNo; }
            }

            public int HousebillId
            {
                get { return hawb.HouseBillID; }
            }

            public string Origin
            {
                get { return hawb.Origin; }
            }

            public string Destination
            {
                get { return hawb.Destination; }
            }

            public int TotalPieces
            {
                get { return hawb.NoOfPieces; }
            }

            public int ScannedPieces
            {
                get { return hawb.Piece; }
            }

            public int Slac
            {
                get { return hawb.Slac; }
            }

            public int Weight
            {
                get { return (int)hawb.Weight; }
            }

            public string Location
            {
                get { return hawb.Line4; }
            }

            public int Flags
            {
                get { return hawb.Flags; }
            }

            public ScanModes ScanMode
            {
                get { return (ScanModes)Enum.Parse(typeof(ScanModes), hawb.Mode.ToString(), true); }
            }

            public HouseBillStatuses status
            {
                get { return (HouseBillStatuses)Enum.Parse(typeof(HouseBillStatuses), hawb.Status.ToString(), true); }
            }

            public RemoveModes RemoveMode
            {
                get { return RemoveModes.NA; }
            }

            #endregion
        }
        public class ReportLoc
        {
            public string Location { get; set; }
            public ReportHawb[] Hawbs { get; set; }
        }
        public class ReportHawb
        {
            public string Hawb { get; set; }
            public DateTime LastScan { get; set; }

        }
    }
    public enum InspectionMode { Physical, Customer, XRay, Canine, ETD }
}
