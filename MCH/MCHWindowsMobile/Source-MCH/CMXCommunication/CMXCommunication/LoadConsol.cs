﻿using System;
using System.Linq;
using System.Collections.Generic;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.LoadConsoleCom;
using CargoMatrix.Communication.Common;
using CargoMatrix.Communication.CargoTruckLoad;
using CargoMatrix.Communication.CargoTender;
namespace CargoMatrix.Communication
{
    public class LoadConsol : CMXServiceWrapper
    {
        protected WSLoadConsol.WsLoadConsol service;
        private static LoadConsol _instance;
        private string asmxName = "WsLoadConsol.asmx";
        public static LoadConsol Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new LoadConsol();
                return _instance;
            }
        }

        private LoadConsol()
        {
            service = new WSLoadConsol.WsLoadConsol();
            service.Url = Settings.Instance.URLPath + asmxName;
        }

        /// <summary>
        /// returns all ULDs attached to consol with Loose ULD at the top
        /// </summary>
        /// <param name="mawbID"></param>
        /// <returns></returns>
        public List<ULD> GetAttachedULDs(int mawbID, MOT mot)
        {
            Func<List<ULD>> f = () =>
            {
                List<ULD> ulds = new List<ULD>();
                UldItem looseUld = null;
                foreach (var item in service.GetAttachedUlds(connectionName, mawbID, mot))
                {
                    if (string.Equals(item.Uld, "loose", StringComparison.OrdinalIgnoreCase))
                        looseUld = item;
                    else
                        ulds.Add(new ULD(item));
                }
                if (looseUld != null)
                    ulds.Insert(0, new ULD(looseUld));
                return ulds;
            };
            return Invoke<List<ULD>>(f, 11036, new List<ULD>());
        }

        public TransactionStatus AssignULDToMawb(int mawbID, int typeId, string number, bool force, MOT mot, int hawbID)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.AttachUld(connectionName, mawbID, typeId, number, force, mot, hawbID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11038, defRet);
        }

        public bool UpdateULD(int uldId, int uldTypeId, string uldNo, MOT mot, int? hawbId)
        {
            Func<bool> f = () =>
            {
                return service.UpdateAttachedUld(connectionName, uldId, uldTypeId, uldNo, mot, hawbId);
            };
            return Invoke<bool>(f, 11039, false);
        }

        public TransactionStatus DeleteULD(int id, MOT mot, int? hawbId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DeleteAttachedUld(connectionName, id, mot, hawbId);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11039, defRet);
        }

        public CargoMatrix.Communication.DTO.IMasterBillItem[] GetMAWBList(string filter, string sort, MOT mot)
        {

            Func<CargoMatrix.Communication.DTO.IMasterBillItem[]> f = () =>
            {
                //CargoMatrix.Communication.DTO.MasterBillItem[] mawbs = new CargoMatrix.Communication.DTO.MasterBillItem[0];
                //
                MasterBillStatuses status;
                switch (filter)
                {
                    case CargoMatrix.Communication.DTO.MawbFilter.NOT_STARTED:
                        status = MasterBillStatuses.Pending;
                        break;
                    case CargoMatrix.Communication.DTO.MawbFilter.IN_PROGRESS:
                        status = MasterBillStatuses.InProgress;
                        break;
                    case CargoMatrix.Communication.DTO.MawbFilter.COMPLETED:
                        status = MasterBillStatuses.Completed;
                        break;
                    case CargoMatrix.Communication.DTO.MawbFilter.NOT_COMPLETED:
                        status = MasterBillStatuses.Open;
                        break;
                    default:
                        status = MasterBillStatuses.All;
                        break;
                }
                MasterBillFields sortfield;
                switch (sort)
                {
                    case CargoMatrix.Communication.DTO.MawbSort.CUTOFF:
                        sortfield = MasterBillFields.CutOff;
                        break;
                    case CargoMatrix.Communication.DTO.MawbSort.MASTERBILL:
                        sortfield = MasterBillFields.MasterBillNumber;
                        break;
                    case CargoMatrix.Communication.DTO.MawbSort.ORIGIN:
                        sortfield = MasterBillFields.Origin;
                        break;
                    case CargoMatrix.Communication.DTO.MawbSort.WEIGHT:
                        sortfield = MasterBillFields.Weight;
                        break;
                    case CargoMatrix.Communication.DTO.MawbSort.CARRIER:
                        sortfield = MasterBillFields.CarrierNumber;
                        break;
                    default:
                        sortfield = MasterBillFields.NA;
                        break;
                }
                MasterBillItem filterMawb = new MasterBillItem() { Status = status, UserId = userId };
                return service.GetLoadConsolMasterBills(connectionName, filterMawb, sortfield, mot);
            };
            return Invoke<CargoMatrix.Communication.DTO.IMasterBillItem[]>(f, 11006, new CargoMatrix.Communication.DTO.IMasterBillItem[0]);
        }

        public HouseBillItem[] GetHawbList(int mawbId, int taskId, ScanStatuses filter, HouseBillFields sort, bool isLoad, MOT mot)
        {
            Func<HouseBillItem[]> f = () =>
            {
                var summary = service.GetLoadConsolHouseBills(connectionName, mawbId, taskId, filter, sort, isLoad, mot);
                return summary.HouseBills;
            };
            return Invoke(f, 11044, null);
        }

        public LocationItem[] GetMasterbillLocations(int mawbId, string filter, bool isLoad, MOT mot)
        {
            Func<LocationItem[]> f = () =>
            {
                var locs = service.GetMasterbillLocations(connectionName, mawbId, isLoad, filter, mot);
                return locs;//.Length > 0 ? locs : new LocationItem[] { new LocationItem() };
            };
            return Invoke<LocationItem[]>(f, 11045, new LocationItem[] { new LocationItem() });
        }

        public HousebillSummary GetHousebill(int curenthawbId, ListDirection direction, int mawbId, int taskId, string filter, string sort, string location, int forkliftId, bool isLoad, MOT mot)
        {
            Func<HousebillSummary> f = () =>
            {
                ScanStatuses filterField = getFilter(filter);
                HouseBillFields sortField = getSort(sort);
                var response = service.GetLoadConsolHouseBill(connectionName, mawbId, curenthawbId, direction, taskId, filterField, sortField, location, userId, forkliftId, isLoad, mot);
                return new HousebillSummary(response);
            };
            var defRet = new HousebillSummary();
            return Invoke<HousebillSummary>(f, 11046, defRet);
        }

        private ScanStatuses getFilter(string filter)
        {
            switch (filter)
            {
                case DTO.HawbFilter.NOT_SCANNED:
                    return ScanStatuses.NotScanned;
                case DTO.HawbFilter.SCANNED:
                    return ScanStatuses.Scanned;
                case DTO.HawbFilter.SHORT:
                    return ScanStatuses.Shortage;
                case DTO.HawbFilter.TAKEOFF:
                    return ScanStatuses.TakeOff;
                case DTO.HawbFilter.REMOVED:
                    return ScanStatuses.Removed;
                case DTO.HawbFilter.LOCKED:
                    return ScanStatuses.Locked;
                default:
                    return ScanStatuses.All;
            }
        }
        private HouseBillFields getSort(string sort)
        {
            switch (sort)
            {
                case CargoMatrix.Communication.DTO.HawbSort.DESTINATION:
                    return HouseBillFields.Destination;
                case CargoMatrix.Communication.DTO.HawbSort.HAWB:
                    return HouseBillFields.HousebillNumber;
                case CargoMatrix.Communication.DTO.HawbSort.LOCATION:
                    return HouseBillFields.Location;
                case CargoMatrix.Communication.DTO.HawbSort.ORIGIN:
                    return HouseBillFields.Origin;
                case CargoMatrix.Communication.DTO.HawbSort.PIECES:
                    return HouseBillFields.TotalPieces;
                case CargoMatrix.Communication.DTO.HawbSort.SCAN:
                    return HouseBillFields.ScannedPieces;
                case CargoMatrix.Communication.DTO.HawbSort.WEIGHT:
                    return HouseBillFields.Weight;
                case CargoMatrix.Communication.DTO.HawbSort.UNKNOWN:
                    return HouseBillFields.UnknownShipper;
                case CargoMatrix.Communication.DTO.HawbSort.WGHT_LOC:
                    return HouseBillFields.Location_Weight;
                default:
                    return HouseBillFields.NA;
            }
        }

        public LoadConsoleShipmentConditionType[] GetShipmentConditionTypes()
        {
            ShipmentConditionType[] conditons = null;
            Func<LoadConsoleShipmentConditionType[]> f = () =>
            {
                conditons = service.GetShipmentConditionList(connectionName);
                return (
                            from item in conditons
                            select new LoadConsoleShipmentConditionType(item)
                       ).ToArray();
            };
            return Invoke<LoadConsoleShipmentConditionType[]>(f, 11047, new LoadConsoleShipmentConditionType[0]);
        }

        public LoadConsoleShipmentConditionSummary[] GetShipmentConditionSummary(int hawbId, MOT mot)
        {
            Func<LoadConsoleShipmentConditionSummary[]> f = () =>
            {
                var shpSummaries = service.GetShipmentConditionSummary(connectionName, hawbId, mot);
                return (from item in shpSummaries
                        select new LoadConsoleShipmentConditionSummary(item)
                        ).ToArray();
            };
            return Invoke<LoadConsoleShipmentConditionSummary[]>(f, 11048, new LoadConsoleShipmentConditionSummary[0]);
        }

        public ScanItem GetScanDetail(ScanItem barcode, int taskId, MOT mot)
        {
            Func<ScanItem> f = () =>
            {
                return service.GetScanDetail(connectionName, barcode, taskId, mot);
            };
            return Invoke<ScanItem>(f, 11049, barcode);
        }


        public bool UpdateShippmentCondition(int houseBillId,
                                             int userId,
                                             CargoMatrix.Communication.DTO.ScanModes scanMode,
                                             CargoMatrix.Communication.Common.ShipmentCondition[] shipmentConditions,
                                             int taskID, string remark, MOT mot)
        {
            Func<bool> f = () =>
                {
                    var scnMode = ToWSLoadConsolScanModes(scanMode);

                    var shpConditions = from item in shipmentConditions
                                        select CargoMatrix.Communication.Common.ShipmentCondition.ToWSLoadConsolShipmentCondition(item);

                    return this.service.UpdateShipmentConditions(connectionName, houseBillId, userId, scnMode, shpConditions.ToArray(), taskID, remark, mot);
                };
            return Invoke<bool>(f, 11050, false);

        }

        public bool UpdateShippmentCondition(int houseBillId,
                                             CargoMatrix.Communication.DTO.ScanModes scanMode,
                                             CargoMatrix.Communication.Common.ShipmentCondition[] shipmentConditions,
                                             int taskID, string remark, MOT mot)
        {
            return this.UpdateShippmentCondition(houseBillId, userId, scanMode, shipmentConditions, taskID, remark, mot);
        }

        private ScanModes ToWSLoadConsolScanModes(CargoMatrix.Communication.DTO.ScanModes scanMode)
        {
            switch (scanMode)
            {
                case CargoMatrix.Communication.DTO.ScanModes.Count: return ScanModes.Count;

                case CargoMatrix.Communication.DTO.ScanModes.NA: return ScanModes.NA;

                case CargoMatrix.Communication.DTO.ScanModes.Piece: return ScanModes.Piece;

                case CargoMatrix.Communication.DTO.ScanModes.Slac: return ScanModes.Slac;

                default: throw new NotImplementedException();
            }
        }


        public List<ShipmentAlert> GetMasterbillAlerts(int mawbId, int taskId)
        {
            Func<List<ShipmentAlert>> f = () =>
            {
                return service.GetMaterbillBillsAlerts(connectionName, mawbId, taskId, userName).ToList<ShipmentAlert>();
            };

            return Invoke<List<ShipmentAlert>>(f, 11051, new List<ShipmentAlert>());
        }
        //public List<ShipmentAlert> GetHousebillAlerts(int hawbId, int taskId)
        //{
        //    List<ShipmentAlert> alerts = new List<ShipmentAlert>();
        //    try
        //    {
        //        alerts.AddRange(service.GetHouseBillsAlerts(connectionName, hawbId, taskId, userName));
        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11057);
        //    }
        //    return alerts;
        //}


        public HousebillSummary ScanPiecesIntoForklift(PieceItem[] pieceIds, int mawbId, int taskId, int forkliftId, string filter, string sort, string location, bool isLoad, MOT mot)
        {
            Func<HousebillSummary> f = () =>
            {
                ScanStatuses filterField = getFilter(filter);
                HouseBillFields sortField = getSort(sort);
                var response = service.ScanPiecesIntoForkLift(connectionName, pieceIds, mawbId, taskId, userId, ScanTypes.Manual, forkliftId, userName, filterField, sortField, location, isLoad, mot);
                return new HousebillSummary(response);
            };
            var defRet = new HousebillSummary();
            return Invoke<HousebillSummary>(f, 11052, defRet);

        }

        public HousebillSummary ScanHousebillIntoForkliftPiece(int hawbID, int mawbId, int pieceId, int taskId, int forkliftId, ScanTypes scanType, string filter, string sort, string location, bool isLoad, MOT mot)
        {
            Func<HousebillSummary> f = () =>
            {
                ScanStatuses filterField = getFilter(filter);
                HouseBillFields sortField = getSort(sort);

                var response = service.ScanPieceIntoForkLift(connectionName, hawbID, mawbId, pieceId, taskId, userId, ScanModes.Piece, scanType, forkliftId, userName, filterField, sortField, location, isLoad, mot);
                return new HousebillSummary(response);
            };
            var defRet = new HousebillSummary();
            return Invoke<HousebillSummary>(f, 11053, defRet);
        }

        public HousebillSummary ScanHousebillIntoForkliftCount(int hawbID, int mawbId, int pieceCount, int taskId, int forkliftId, ScanTypes scanType, string filter, string sort, string location, bool isLoad, MOT mot)
        {
            Func<HousebillSummary> f = () =>
            {
                ScanStatuses filterField = getFilter(filter);
                HouseBillFields sortField = getSort(sort);

                var response = service.ScanPieceIntoForkLift(connectionName, hawbID, mawbId, pieceCount, taskId, userId, ScanModes.Count, scanType, forkliftId, userName, filterField, sortField, location, isLoad, mot);
                return new HousebillSummary(response);
            };
            var defRet = new HousebillSummary();
            return Invoke<HousebillSummary>(f, 11054, defRet);
        }

        //public TransactionStatus LoadPieceIntoULDCount(int hawbID, int uldId, int pieceCount, int taskId, ScanTypes scanType)
        //{
        //    Func<TransactionStatus> f = () =>
        //    {
        //        return service.DropPieceIntoConsol(connectionName, hawbID, pieceCount, taskId, userId, ScanModes.Count, scanType, 0, uldId);
        //    };
        //    var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
        //    return Invoke<TransactionStatus>(f, 11055, defRet);
        //}

        public TransactionStatus LoadPiecesIntoULDPiece(int uldId, PieceItem[] pieceIds, int taskId, ScanTypes scanType, int forkliftId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                // if its a ULD send 0 as LocationId
                return service.DropPiecesIntoConsol(connectionName, pieceIds, taskId, userId, ScanTypes.Manual, 0, uldId, forkliftId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11056, defRet);
        }

        public TransactionStatus LoadPiecesIntoLocation(int uldId, int locId, PieceItem[] pieceIds, int taskId, ScanTypes scanType, int forkliftId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropPiecesIntoConsol(connectionName, pieceIds, taskId, userId, scanType, locId, uldId, forkliftId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11057, defRet);
        }





        public HouseBillItem[] GetForkliftContent(int mawbId, int taskId, int fliftId, MOT mot)
        {
            return GetForkliftContent(mawbId, taskId, HouseBillFields.HousebillNumber, fliftId, mot);
        }

        public HouseBillItem[] GetForkliftContent(int mawbId, int taskId, HouseBillFields sort, int fliftId, MOT mot)
        {
            Func<HouseBillItem[]> f = () =>
            {
                return service.GetForkLiftHouseBills(connectionName, mawbId, taskId, userId, sort, fliftId, mot);
            };
            return Invoke<HouseBillItem[]>(f, 11058, new HouseBillItem[0]);
        }

        public TransactionStatus RemovePieceFromForklift(int hawbId, int pieceId, int taskId, ScanModes mode, int forkliftId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemovePieceFromForkLift(connectionName, hawbId, pieceId, taskId, userId, mode, forkliftId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11059, defRet);
        }

        //public TransactionStatus RemoveHawbFromForklift(int hawbId,int pieceId,int taskId)
        //{
        //    try
        //    {
        //        return service.RemovePieceFromForkLift(connectionName, hawbId, pieceId, taskId, userId);
        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11059);
        //    }
        //    return null;
        //}

        public HouseBillItem GetHousebillInfo(int hawbId, int taskId)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.GetHouseBillInfo(connectionName, hawbId, taskId);
            };
            return Invoke<HouseBillItem>(f, 11060, new HouseBillItem());
        }

        public int GetForkliftItemsCount(int taskId, int fliftId, MOT mot)
        {
            Func<int> f = () =>
            {
                return service.GetForkLiftHouseBillsCount(connectionName, taskId, userId, fliftId, mot);
            };
            return Invoke<int>(f, 11061, 0);
        }

        public UserItem[] GetWarehouseUsers(int taskID)
        {
            Func<UserItem[]> f = () =>
            {
                return service.GetWarehouseUsers(connectionName, taskID);
            };
            return Invoke<UserItem[]>(f, 11062, new UserItem[0]);
        }

        public TransactionStatus ShareLoadConsolTask(int taskId, int userId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.ShareLoadConsolTask(connectionName, taskId, userId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11063, defRet);
        }

        public TransactionStatus DropPiecesIntoLocation(PieceItem[] pieceIds, int taskId, int locId, int forkliftId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropPiecesIntoLocation(connectionName, pieceIds, taskId, userId, ScanTypes.Manual, locId, forkliftId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11064, defRet);
        }

        public TransactionStatus AttachHousebillToMasterbill(int hawbId, int taskId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.AttachHousebillToMasterbill(connectionName, hawbId, taskId, userId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11065, defRet);
        }

        public TransactionStatus DropAllPiecesIntoLocation(int taskId, int locationId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropAllPiecesIntoLocation(connectionName, taskId, userId, locationId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11066, defRet);
        }

        public TransactionStatus LoadAllPiecesIntoULD(int taskId, int uldId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropAllPiecesIntoConsol(connectionName, taskId, userId, 0, uldId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11067, defRet);
        }

        public TransactionStatus LoadAllPiecesIntoLocation(int taskId, int uldId, int locId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropAllPiecesIntoConsol(connectionName, taskId, userId, locId, uldId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11067, defRet);
        }

        public UldItem MawbHasLooseULD(int mawbID, MOT mot)
        {
            Func<UldItem> f = () =>
            {
                foreach (var uld in service.GetAttachedUlds(connectionName, mawbID, mot))
                    if (string.Equals(uld.Uld, "loose", StringComparison.OrdinalIgnoreCase))
                        return uld;
                return null;
            };
            return Invoke<UldItem>(f, 11068, null);
        }

        public HouseBillItem[] GetULDContent(int uldId, int mawbId, int taskId, MOT mot)
        {
            Func<HouseBillItem[]> f = () =>
            {
                return service.GetUldHouseBills(connectionName, mawbId, taskId, uldId, HouseBillFields.HousebillNumber, mot);
            };
            return Invoke<HouseBillItem[]>(f, 11069, new HouseBillItem[0]);
        }

        public TransactionStatus RemovePieceFromULD(int hawbId, int pieceId, int taskId, int uldId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemovePieceFromUld(connectionName, hawbId, pieceId, taskId, uldId, userId, ScanModes.Piece, ScanTypes.Manual, mot);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11070, defRet);
        }

        public TransactionStatus IsLoadConsolReady(int taskId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.IsLoadConsolReady(connectionName, taskId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11070, defRet);
        }


   

        public TransactionStatus FinalizeConsol(int taskId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.FinalizeLoadConsol(connectionName, taskId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11071, defRet);
        }

        public TransactionStatus DropULDIntoLocation(int taskId, int uldId, int locId, string sealNo, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropUldIntoLocation(connectionName, taskId, uldId, locId, sealNo, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11072, defRet);
        }

        //public HousebillSummary ScanRemovedPieceIntoForklift(int hawbID, int mawbId, int pieceCountorID, int taskId, int forkliftId, ScanModes mode, ScanTypes scanType, string filter, string sort, string location, bool isLoad)
        //{
        //    Func<HousebillSummary> f = () =>
        //    {
        //        ScanStatuses filterField = getFilter(filter);
        //        HouseBillFields sortField = getSort(sort);

        //        var response = service.ScanRemovedPieceIntoForkLift(connectionName, hawbID, mawbId, pieceCountorID, taskId, userId, mode, scanType, forkliftId, userName, filterField, sortField, location, isLoad);
        //        return new HousebillSummary(response);
        //    };

        //    var defRet = new HousebillSummary();
        //    return Invoke<HousebillSummary>(f, 11073, defRet);
        //}

        //public CargoMatrix.Communication.DTO.RemoveModes GetHouseBillAllowedRemoveModes(int hawbID, int mawbId)
        //{
        //    Func<CargoMatrix.Communication.DTO.RemoveModes> f = () =>
        //    {
        //        string remMode = service.GetHouseBillRemoveModes(connectionName, hawbID, mawbId).ToString();
        //        return (CargoMatrix.Communication.DTO.RemoveModes)Enum.Parse(typeof(CargoMatrix.Communication.DTO.RemoveModes), remMode, true);
        //    };
        //    return Invoke<CargoMatrix.Communication.DTO.RemoveModes>(f, 11074, CargoMatrix.Communication.DTO.RemoveModes.NA);
        //}

        public TransactionStatus ShortOrTakeOffHawb(int hawbID, int mawbId, bool isShortage, int taskId)
        {
            Func<TransactionStatus> f = () =>
             {
                 return service.ShortOrTakeOff(connectionName, hawbID, mawbId, isShortage, taskId);
             };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11075, defRet);
        }

        public RemoveReasonItem[] GetRemoveReasons()
        {
            Func<RemoveReasonItem[]> f = () =>
            {
                return service.GetRemoveReasons(connectionName);
            };
            return Invoke<RemoveReasonItem[]>(f, 11076, new RemoveReasonItem[0]);
        }

        public int GetSupervisorIDByPin(int pin)
        {
            Func<int> f = () =>
            {
                return service.IsSupervisor(connectionName, pin);
            };
            return Invoke<int>(f, 11077, 0);
        }

        /// <summary>
        /// removes hawb from consol and returns it to specified location
        /// </summary>
        /// <param name="hawbId"></param>
        /// <param name="taskId"></param>
        /// <param name="locId"></param>
        /// <param name="reasonId"></param>
        /// <param name="scanType"></param>
        /// <returns></returns>
        public TransactionStatus RemoveHawbFromConsol(int hawbId, int superID, string location, int reasonId, ScanTypes scanType)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveHousebillFromConsolFinal(connectionName, hawbId, userId, superID, location, reasonId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11078, defRet);
        }

        public TransactionStatus UnShareLoadConsolTask(int taskId, int userId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.UnShareLoadConsolTask(connectionName, taskId, userId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11079, defRet);
        }

        public UserItem[] GetTaskUsers(int taskId)
        {
            Func<UserItem[]> f = () =>
            {
                return service.ShareLoadConsolTaskList(connectionName, taskId);
            };
            return Invoke<UserItem[]>(f, 11080, new UserItem[0]);
        }

        public CargoMatrix.Communication.ScannerUtilityWS.TaskSettings GetTaskSettings()
        {
            return ScannerUtility.Instance.GetTaskSettings(CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.LoadConsol);
        }

        public TransactionStatus ReturnHawbIntoConsol(int hawbID)
        {
            Func<TransactionStatus> f = () =>
            {
                TransactionStatus status = new TransactionStatus { TransactionStatus1 = false };
                status = service.ReturnHousebillIntoConsol(connectionName, hawbID);
                return status;
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11082, defRet);

        }
        public TransactionStatus RemoveHousebillFromForklift(int hawbId, int taskId, int forkliftId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveHousebillFromForklift(connectionName, hawbId, taskId, userId, forkliftId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11083, defRet);
        }

        //public TransactionStatus ReturnAllPiecesIntoLocation(int taskId, int locId)
        //{
        //    Func<TransactionStatus> f = () =>
        //    {
        //        return service.ReturnAllPiecesIntoLocation(connectionName, taskId, userId, locId);
        //    };
        //    var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
        //    return Invoke<TransactionStatus>(f, 11084, defRet);
        //}

        public TransactionStatus RemoveHousebillFromUld(int hawbId, int taskId, int uldId, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveHousebillFromUld(connectionName, hawbId, taskId, uldId, userId, mot);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11085, defRet);
        }
        public LockedConsolHouseBillItem GetLockedHawbs(int mawbId, int taskId, HouseBillFields sort, bool isload)
        {
            Func<LockedConsolHouseBillItem> f = () =>
            {
                return service.GetLockedHousebills(connectionName, mawbId, taskId, sort, userId, isload);
            };
            var defRet = new LockedConsolHouseBillItem();
            defRet.Transaction = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            defRet.HouseBills = new LockedHouseBillItem[0];
            return Invoke<LockedConsolHouseBillItem>(f, 11086, defRet);

        }

        public bool IsPullEnabled()
        {
            Func<bool> f = () =>
            {
                return service.IsFeatureEnabled(connectionName, "Stage");
            };

            return Invoke<bool>(f, 11087, false);
        }


        public TransactionStatus DropHousebillIntoLocation(string houseBillNumber,int taskId ,int locationId)
        {
            Func<TransactionStatus> f = () =>
            {
                var user = WebServiceManager.Instance().m_user;

                return service.DropHousebillIntoLocation(connectionName, houseBillNumber, taskId, user.userID, locationId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11088, defRet);
        }

        //private CargoMatrix.Communication.DTO.MasterBillItem ConvertMAWBToIMasterbillItem(MasterBillItem item)
        //{
        //    if (item == null)
        //        return null;
        //    return new CargoMatrix.Communication.DTO.MasterBillItem(item);
        //}



        #region Truck Load



        public ITruckLoadMasterBillItem[] GetLoadTruckMasterBills(MasterBillFilterCriteria filter, MasterBillSortCriteria sort)
        {
            Func<ITruckLoadMasterBillItem[]> f = () =>
            {
                MasterBillItem filterItem = new MasterBillItem() { Status = filter.ToWsMasterBillStatuses() };

                return service.GetLoadTruckMasterBills(this.connectionName, filterItem, sort.ToWsMasterBillFields());

                //return (
                //            from item in items
                //            select new TruckLoadMasterBillItem(item)
                //       ).ToArray();
            };
            return Invoke<ITruckLoadMasterBillItem[]>(f, 41001, new ITruckLoadMasterBillItem[0]);
        }

        public CargoMatrix.Communication.DTO.IULD[] GetLoadTruckMasterBillUlds(int masterBillID, int taskID, int forkliftID, out TransactionStatus transactionStatus)
        {
            transactionStatus = null;

            try
            {
                var user = WebServiceManager.Instance().m_user;
                var uldList = this.service.GetLoadTruckMasterBillUlds(this.connectionName, masterBillID, taskID, user.userID, forkliftID);

                transactionStatus = uldList.Transaction;

                return (
                            from item in uldList.Ulds
                            select new ULD(item)
                       ).ToArray();
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 41002);
                return new CargoMatrix.Communication.DTO.IULD[0];
            }
        }

        public TransactionStatus FinalizeLoadTruck(int taskID, int userID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.FinalizeLoadTruck(this.connectionName, taskID, userID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41003, defRet);
        }

        public TransactionStatus FinalizeLoadTruck(int taskID)
        {
            var user = WebServiceManager.Instance().m_user;

            return this.FinalizeLoadTruck(taskID, user.userID);
        }

        public TransactionStatus UnloadUldIntoLocation(int masterBillID, int taksID, int uldID, string location, int userID, int truckID, int forkliftID, out CargoMatrix.Communication.DTO.IULD[] ulds)
        {
            ulds = null;

            try
            {
                var uldList = this.service.UnloadUldIntoLocation(this.connectionName, masterBillID, taksID, uldID, location, userID, truckID, forkliftID);

                if (uldList.Transaction.TransactionStatus1 == true)
                {
                    ulds = (
                                from item in uldList.Ulds
                                select new ULD(item)
                           ).ToArray();
                }

                return uldList.Transaction;

            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 41004);
                return new TransactionStatus() { TransactionStatus1 = false };
            }
        }

        public TransactionStatus UnloadUldIntoLocation(int masterBillID, int taksID, int uldID, string location, int truckID, int forkliftID, out CargoMatrix.Communication.DTO.IULD[] ulds)
        {
            ulds = null;

            var user = WebServiceManager.Instance().m_user;

            return this.UnloadUldIntoLocation(masterBillID, taksID, uldID, location, user.userID, truckID, forkliftID, out ulds);
        }

        public CargoMatrix.Communication.DTO.HouseBillItem[] GetTruckHouseBills(int masterBillID, int uldID, HouseBillFields sortBy)
        {
            Func<CargoMatrix.Communication.DTO.HouseBillItem[]> f = () =>
            {
                var items = this.service.GetTruckHouseBills(this.connectionName, masterBillID, uldID, sortBy);

                return (
                        from item in items
                        select new CargoMatrix.Communication.DTO.HouseBillItem(item)
                       ).ToArray();

            };
            return Invoke<CargoMatrix.Communication.DTO.HouseBillItem[]>(f, 41005, new CargoMatrix.Communication.DTO.HouseBillItem[0]);
        }

        public int? GetMasterBillId(string houseBillNumber)
        {
            Func<int?> f = () =>
            {
                int? result = null;
                var id = this.service.GetMasterBillId(this.connectionName, houseBillNumber);

                if (id != 0)
                {
                    result = id;
                }

                return result;

            };
            return Invoke<int?>(f, 41006, null);
        }


        public int? GetMasterBillIdByULDNumber(string uldNumber)
        {
            int result = this.service.GetMasterBillIdByULDNumber(this.connectionName, uldNumber);

            if (result == 0) return null;

            return result;

        }

        public ICargoTruckLoadDoor[] GetMasterBillDoors(int masterbillID)
        {
            Func<ICargoTruckLoadDoor[]> f = () =>
            {
                var items = this.service.GetMasterBillDoors(this.connectionName, masterbillID);

                return (
                            from item in items
                            select new CargoTruckLoadDoor(item)
                       ).ToArray();

            };
            return Invoke<ICargoTruckLoadDoor[]>(f, 41007, new ICargoTruckLoadDoor[0]);
        }


        public TransactionStatus AssigneDoorTruck(int masterbillID, int taskID, ICargoTruckLoadDoor door)
        {
            Func<TransactionStatus> f = () =>
            {
                var user = WebServiceManager.Instance().m_user;

                return this.service.AssignedDoorTruck(this.connectionName, masterbillID, door.Name, door.Truck.Location, user.userID, taskID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41008, defRet);
        }

        public TransactionStatus AssigneNewDoor(int masterBillID, int taskID, ICargoTruckLoadDoor door)
        {
            Func<TransactionStatus> f = () =>
            {
                var user = WebServiceManager.Instance().m_user;

                return this.service.AssignedNewDoor(this.connectionName, masterBillID, door.Name, user.userID, taskID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41009, defRet);
        }

        public TransactionStatus ReAssignDoor(int masterbillID, ICargoTruckLoadDoor currentDoor, int taskID, ICargoTruckLoadDoor newDoor)
        {
            Func<TransactionStatus> f = () =>
            {
                var user = WebServiceManager.Instance().m_user;

                return this.service.ReAssignedDoor(this.connectionName, masterbillID, currentDoor.Name, newDoor.Name, user.userID, taskID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41010, defRet);
        }

        public CargoMatrix.Communication.ScannerUtilityWS.TaskSettings GetTaskSettingsTruckLoad()
        {
            return ScannerUtility.Instance.GetTaskSettings(CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.LoadTruck);
        }

        public int GetTruckLoadForkliftItemsCount(int taskID, int forkliftID)
        {
            Func<int> f = () =>
            {
                var user = WebServiceManager.Instance().m_user;
                return this.service.GetLoadTruckForkLiftHouseBillsCount(this.connectionName, taskID, user.userID, forkliftID);
            };
            return Invoke<int>(f, 41012, 0);
        }

        public TransactionStatus ScanULDIntoLoadTruckForkLift(int masterBillID, int uldID, int taskId, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                var user = WebServiceManager.Instance().m_user;

                return this.service.ScanULDIntoLoadTruckForkLift(this.connectionName, uldID, masterBillID, taskId, user.userID, forkliftID);

            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41013, defRet);
        }

        public CargoMatrix.Communication.CargoTruckLoad.UldList ScanPiecesIntoLoadTruckForkLift(PieceItem[] pieces, int masterBillID, int taskID, int forkliftID)
        {
            return this.ScanPiecesIntoLoadTruckForkLift(pieces, masterBillID, taskID, ScanTypes.Automatic, forkliftID);
        }

        public CargoMatrix.Communication.CargoTruckLoad.UldList ScanPiecesIntoLoadTruckForkLift(PieceItem[] pieces, int masterBillID, int taskID, ScanTypes scanType, int forkliftID)
        {
            Func<CargoMatrix.Communication.CargoTruckLoad.UldList> f = () =>
            {
                var user = WebServiceManager.Instance().m_user;
                var res = this.service.ScanPiecesIntoLoadTruckForkLift(this.connectionName, pieces, masterBillID, taskID, user.userID, scanType, forkliftID);
                return new CargoMatrix.Communication.CargoTruckLoad.UldList(res);
            };
            return Invoke<CargoMatrix.Communication.CargoTruckLoad.UldList>(f, 41014, new CargoMatrix.Communication.CargoTruckLoad.UldList());
        }

        public CargoMatrix.Communication.DTO.HouseBillItem[] GetLoadTruckUldContent(int uldId, int forkliftID, ScanStatuses scanStatus)
        {
            Func<CargoMatrix.Communication.DTO.HouseBillItem[]> f = () =>
            {
                var res = this.service.GetLoadTruckForkLiftHouseBills(this.connectionName, uldId, forkliftID, scanStatus);

                return (from item in res
                        select new CargoMatrix.Communication.DTO.HouseBillItem(item)).ToArray();
            };
            return Invoke<CargoMatrix.Communication.DTO.HouseBillItem[]>(f, 41015, new CargoMatrix.Communication.DTO.HouseBillItem[0]);
        }

        public int? GetLoadTruckUldByHouseBillNumber(string houseBillNumber)
        {
            Func<int?> f = () =>
            {
                int res = this.service.GetLoadTruckUldIdByHousebill(this.connectionName, houseBillNumber);

                return res == 0 ? null : (int?)res;
            };
            return Invoke<int?>(f, 41016, null);
        }

        public TransactionStatus CanLoadTruckStart(int masterBillID)
        {
            Func<TransactionStatus> f = () =>
            {
                var user = WebServiceManager.Instance().m_user;
                return this.service.CanLoadTruckStart(this.connectionName, masterBillID, user.userID);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41017, defRet);
        }

        public TransactionStatus GetLoadTruckForkliftContent(int masterBillId, int taskID, int userID, int forkliftID, out LoadTruckULD[] ulds)
        {
            ulds = null;

            try
            {
                var uldList = this.service.GetLoadTruckForkliftUlds(this.connectionName, masterBillId, taskID, userID, forkliftID);

                if (uldList.Transaction.TransactionStatus1 == false) return uldList.Transaction;

                ulds = (from item in uldList.Ulds
                        select new LoadTruckULD(item)).ToArray();

                return uldList.Transaction;
            }
            catch (System.Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 41018);
                return new TransactionStatus { TransactionStatus1 = false };
            }

        }

        public TransactionStatus GetLoadTruckForkliftContent(int masterBillId, int taskID, int forkliftID, out LoadTruckULD[] ulds)
        {
            var user = WebServiceManager.Instance().m_user;
            return this.GetLoadTruckForkliftContent(masterBillId, taskID, taskID, user.userID, out ulds);
        }

        public TransactionStatus RemoveUldFromForklift(int uldID, int taskID, int userID, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.RemoveUldFromForklift(this.connectionName, uldID, taskID, userID, forkliftID);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41019, defRet);
        }

        public TransactionStatus RemoveUldFromForklift(int uldID, int taskID, int forkliftID)
        {
            var user = WebServiceManager.Instance().m_user;
            return this.RemoveUldFromForklift(uldID, taskID, user.userID, forkliftID);
        }

        public TransactionStatus GetLoadTruckContent(int masterbillID, int taskID, int truckID, int userID, int forkliftID, out LoadTruckULD[] ulds)
        {
            ulds = null;

            try
            {
                var uldList = this.service.GetLoadTruckUlds(this.connectionName, masterbillID, truckID, taskID, userID, forkliftID);

                if (uldList.Transaction.TransactionStatus1 == false) return uldList.Transaction;

                ulds = (from item in uldList.Ulds
                        select new LoadTruckULD(item)).ToArray();

                return uldList.Transaction;
            }
            catch (System.Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 41020);
                return new TransactionStatus { TransactionStatus1 = false };
            }
        }

        public TransactionStatus GetLoadTruckContent(int masterbillID, int taksID, int truckID, int forkliftID, out LoadTruckULD[] ulds)
        {
            var user = WebServiceManager.Instance().m_user;
            return GetLoadTruckContent(masterbillID, taksID, truckID, user.userID, forkliftID, out ulds);

        }

        public TransactionStatus RemovePiecesFromTruck(int truckID, PieceItem[] pieces, int taskID, int userID, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.RemovePiecesFromTruck(this.connectionName, pieces, truckID, taskID, userID, forkliftID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41021, defRet);
        }

        public TransactionStatus RemovePiecesFromTruck(int truckID, PieceItem[] pieces, int taskID, int forkliftID)
        {
            var user = WebServiceManager.Instance().m_user;
            return this.RemovePiecesFromTruck(truckID, pieces, taskID, user.userID, forkliftID);
        }

        public TransactionStatus RemoveUldFromTruck(int uldId, int truckId, int taskID, int userID, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.RemoveUldFromTruck(this.connectionName, uldId, truckId, taskID, userID, forkliftID);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41022, defRet);
        }

        public TransactionStatus RemoveUldFromTruck(int uldId, int truckId, int taskID, int forkliftID)
        {
            var user = WebServiceManager.Instance().m_user;
            return RemoveUldFromTruck(uldId, truckId, taskID, user.userID, forkliftID);
        }


        public TransactionStatus RemoveLoadTruckHousebillFromForklift(int houseBillId, int taskId, int userId, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.RemoveLoadTruckHousebillFromForklift(this.connectionName, houseBillId, taskId, userId, forkliftID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41023, defRet);
        }

        public TransactionStatus RemoveLoadTruckHousebillFromForklift(int houseBillId, int taskId, int forkliftID)
        {
            var user = WebServiceManager.Instance().m_user;
            return this.RemoveLoadTruckHousebillFromForklift(houseBillId, taskId, user.userID, forkliftID);

        }

        public TransactionStatus IsLoadTruckReady(int taskID)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.IsLoadTruckReady(this.connectionName, taskID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41024, defRet);

        }

        public TransactionStatus ScanPiecesIntoLoadTruck(int masterbillID, int taskID, int userID, ScanTypes scanType, int forkliftID, int truckId, PieceItem[] pieces)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.ScanPiecesIntoLoadTruck(this.connectionName, pieces, masterbillID, taskID, userID, scanType, forkliftID, truckId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41025, defRet);

        }

        public TransactionStatus ScanItemsIntoTruck(int[] uldIds, PieceItem[] pieces, int masterbillID, int taskID, int forkliftID, int truckId)
        {
            Func<TransactionStatus> f = () =>
            {
                TransactionStatus status = new TransactionStatus { TransactionStatus1 = false, TransactionError = "No items provided" };

                var user = WebServiceManager.Instance().m_user;

                if (pieces.Count() > 0)
                {
                    status = this.service.ScanPiecesIntoLoadTruck(this.connectionName, pieces, masterbillID, taskID, user.userID, ScanTypes.Automatic, forkliftID, truckId);

                    if (status.TransactionStatus1 == false)
                    {
                        return status;
                    }
                }
                if (uldIds.Count() > 0)
                {
                    status = this.service.ScanULDsIntoLoadTruck(this.connectionName, uldIds, masterbillID, taskID, user.userID, forkliftID, truckId);
                }

                return status;

            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 41026, defRet);
        }

        public TransactionStatus CanScanIntoLoadTruckForkLift(string masterbillNumber, string carrierNumber, int forkliftId, string houseBillNumber, int pieceNumber, out DTO.HouseBillItem houseBill)
        {
            houseBill = null;

            try
            {
                var houseBillItem = this.service.CanScanIntoLoadTruckForkLift(this.connectionName, houseBillNumber, pieceNumber, carrierNumber, masterbillNumber, forkliftId);

                if (houseBillItem.Transaction.TransactionStatus1 == true)
                {
                    houseBill = new CargoMatrix.Communication.DTO.HouseBillItem(houseBillItem);
                }

                return houseBillItem.Transaction;


            }
            catch (System.Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 41027);

                return new TransactionStatus { TransactionStatus1 = false };
            }

        }

        public TransactionStatus CanScanIntoLoadTruck(string masterbillNumber, string carrierNumber, int forkliftId, string houseBillNumber, int pieceNumber, out DTO.HouseBillItem houseBill)
        {
            houseBill = null;

            try
            {
                var houseBillItem = this.service.CanScanIntoLoadTruck(this.connectionName, houseBillNumber, pieceNumber, carrierNumber, masterbillNumber, forkliftId);

                if (houseBillItem.Transaction.TransactionStatus1 == true)
                {
                    houseBill = new CargoMatrix.Communication.DTO.HouseBillItem(houseBillItem);
                }
                return houseBillItem.Transaction;
            }
            catch (System.Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 41028);

                return new TransactionStatus { TransactionStatus1 = false };
            }
        }

        public CargoMatrix.Communication.DTO.IHouseBillItem[] GetLoadContainerHousebills(int mawbId)
        {
            Func<CargoMatrix.Communication.DTO.IHouseBillItem[]> f = () =>
            {
                return service.GetLoadContainerHousebills(connectionName, mawbId);

            };
            var defRet = new CargoMatrix.Communication.DTO.IHouseBillItem[0];
            return Invoke<CargoMatrix.Communication.DTO.IHouseBillItem[]>(f, 41027, defRet);

        }

        #endregion


        #region Cargo Tender

        public CargoMatrix.Communication.DTO.IULD[] GetCargTenderULDList(ICargTenderMasterBillItem masterBill)
        {
            Random r = new Random();
            int uldCount = r.Next(10);
            CargoMatrix.Communication.DTO.IULD[] result = new CargoMatrix.Communication.DTO.IULD[uldCount];

            for (int i = 0; i < uldCount; i++)
            {
                var uldItem = new CargoMatrix.Communication.WSLoadConsol.UldItem();
                uldItem.ScannedPieces = r.Next(3);
                uldItem.Pieces = r.Next(3, 10);
                uldItem.Uld = r.Next(100) < 50 ? "LOOSE" : "";
                uldItem.UldId = (int)System.DateTime.Now.Ticks;
                uldItem.Weight = r.Next(100) * 1.457;
                uldItem.Location = new LocationItem();
                uldItem.Location.Location = "Test Location";
                uldItem.UldNumber = uldItem.Uld.Equals("LOOSE") ? string.Empty : "1234567";

                var uld = new CargoMatrix.Communication.LoadConsol.ULD(uldItem);
                result[i] = uld;
            }


            return result;
        }

        #endregion


        public class ULD : CargoMatrix.Communication.DTO.IULD, CargoMatrix.Communication.DTO.IULDItem
        {
            CargoMatrix.Communication.WSLoadConsol.UldItem uld;
            internal ULD(CargoMatrix.Communication.WSLoadConsol.UldItem uld)
            {
                this.uld = uld;
            }

            public CargoMatrix.Communication.WSLoadConsol.UldItem WsULD
            {
                get
                {
                    return this.uld;
                }
            }

            #region IULD Members

            public string ULDType
            {
                get
                {
                    return uld.Uld;
                }
                set
                {
                    uld.Uld = value;
                }
            }
            public int ULDTypeID
            {
                get { return uld.UldTypeId; }
                set { uld.UldTypeId = value; }
            }
            public string ULDNo
            {
                get
                {
                    return uld.UldNumber;
                }
                set
                {
                    uld.UldNumber = value;
                }
            }
            public int ID
            {
                get
                {
                    return uld.UldId;
                }
            }
            public bool IsDefault
            {
                get
                {
                    return uld.IsDefault;
                }
                set
                {
                    uld.IsDefault = value;
                }
            }
            public int Pieces { get { return (int)uld.Pieces; } }
            public double Weight { get { return uld.Weight; } }
            public int TypeID
            {
                get { return uld.UldTypeId; }
                set { uld.UldTypeId = value; }
            }

            public int ScannedPieces
            {
                get { return this.uld.ScannedPieces; }
            }

            public LocationItem Location
            {
                get { return this.uld.Location; }
            }

            #endregion

            #region IULDItem Members


            string CargoMatrix.Communication.DTO.IULDItem.Location
            {
                get
                {
                    return this.Location.Location;
                }
                set
                {
                    this.Location.Location = value;
                }
            }

            #endregion

            #region IULDType Members

            public string ULDName
            {
                get { return this.uld.Uld; }
                set { this.uld.Uld = value; }
            }

            public bool IsLoose
            {
                get { return this.ULDType.Equals("loose", StringComparison.OrdinalIgnoreCase); }
            }

            #endregion

            public HouseBillItem FirstHousebill
            {
                get
                {
                    var hb = this.uld.Housebills.FirstOrDefault();
                    if (hb == null)
                        hb = new HouseBillItem();
                    return hb;
                }
            }
        }

    }
}


public class HousebillSummary
{
    public TransactionStatus Status { get { return housebill.Transaction; } internal set { housebill.Transaction = value; } }
    private CargoMatrix.Communication.WSLoadConsol.HouseBillItem housebill;
    public CargoMatrix.Communication.DTO.IHouseBillItem HouseBill { get { return housebill; } }
    public HousebillSummary(CargoMatrix.Communication.WSLoadConsol.HouseBillItem hawb)
    {
        housebill = hawb;
    }
    public HousebillSummary()
    {
        this.housebill = new HouseBillItem();
        Status = new TransactionStatus() { TransactionStatus1 = false, TransactionError = "Operation Failed" };
    }
}


