﻿using System;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSPieceScan;
using CargoMatrix.Communication.WSLoadConsol;

namespace CargoMatrix.Communication.DTO
{
    public class DataTransferObjects
    {

    }

    public struct User
    {

        //public string username;
        public string pin;
        public string password;
        public string firstName;
        public string lastName;
        public string UserName;
        public int userID;
        public CargoMatrix.Communication.WSPieceScan.UserTypes GroupID;
        public int errorCode;
        public UserSettings settings;


    }
    public struct UserSettings
    {
        public string location;
        public int timeOut; // in minutes
        public int timeDifference;

    }
    public struct ImageObject
    {
        //public byte m_flag;
        public byte[] m_rawImage;
        //public System.DateTime m_time;
        public string m_time;
        public string m_location;
        public string m_userid;
        public string m_reason;
        public int m_index;
        public int m_pieceNo;
        public ImageObject(byte[] img, string time, string location, string userid, string reason)
        {
            m_rawImage = img;
            m_time = time;
            //m_flag = flag;
            m_index = -1;
            m_location = location;
            m_userid = userid;
            m_reason = reason;
            m_pieceNo = 0;

        }
        public ImageObject(byte[] img, string time, string location, string userid, string reason, int index)
        {
            m_rawImage = img;
            m_time = time;
            //m_flag = flag;
            m_index = index;
            m_location = location;
            m_userid = userid;
            m_reason = reason;
            m_pieceNo = 0;
        }
    }
    public enum TaskType
    {
        FREIGHT_PHOTO_CAPTURE_HOUSEBILL,
        FREIGHT_PHOTO_CAPTURE_MASTERBILL,
        UNKNOWN
    }

    public class TaskItem
    {
        public string taskPicture;
        public int direction;
        public System.Nullable<int> totalPieces;
        public Modes mode;
        public TaskType taskType;
        public string taskPrefix;
        public string taskID;
        public string status;
        public char statusCode;

        public byte[] logo;  // for masterbill
        public string origin;
        public string destination;
        public string carrier; // for masterbill
        public string actualBill;  // actual house bill
        public string reference;
        public string line2, line3, line4;
        public DateTime? CutOffTime;
        public int NoofPieces;
        public int NoOfHawbs;
        public int NoOfSLACs;
        public int NoOfULDs;
        public int Flags;
        public double TotalWeight;
        public string MawbDestination;

        public TaskItem()
        {
            taskType = TaskType.UNKNOWN;
            mode = Modes.NA;
            totalPieces = 0;
            //isVisuallyInspected = false;
        }

    }

    public class FreightPhotoCapture : TaskItem
    {

        //public string piece;
        //public string location;
        //public string rack;
        //public string bin;
        //public bool isAlreadyExists;
        public List<FreightPhotoCaptureItem> itemsList;



        public FreightPhotoCapture()
        {
            //isAlreadyExists = false;
        }
        public bool HasData
        {
            get
            {
                if (itemsList != null)
                {
                    for (int i = 0; i < itemsList.Count; i++)
                    {
                        if (itemsList[i].HasData)
                            return true;
                    }
                }
                return false;
            }
        }


    }

    public class FreightPhotoCaptureItem
    {
        public FreightPhotoCaptureItem()
        {
        }
        public FreightPhotoCaptureItem(string Reason, List<ImageObject> ImageList, int reasonType)
        {
            reason = Reason;
            imagelist = ImageList;
            ReasonType = reasonType;
        }
        public bool HasData
        {
            get
            {
                if (imagelist != null)
                {
                    if (imagelist.Count > 0)
                        return true;

                }
                return false;
            }
        }
        public string reason;
        public List<ImageObject> imagelist;
        public int ReasonType;
        //public System.DateTime time;
        //public System.Windows.Forms.ImageList imagelist;


    }

    public class MainMenuItem
    {
        public string Label;
        public int ActionID;
        public string TotalCount;
        public string Icon;
        public string NotCompleted;
        public string NotAssigned;
        public string Completed;
        public string TaskCountByUser;
        public string NotCompletedTaskCountByUserID;

    }

 
    public enum FlightSort { Carrier, FlightNo, Origin ,Destination}

    public enum FlightStatus { All, Completed, InProgress, Open, Pending }
    public class FlightItem //: IFlightItem
    {
        public string CarrierLogo;
        public string CarrierCode;
        public string CarrierName;
        public string FlightNumber;
        public DateTime ETA;
        public string Origin;
        public int ULDCount;
        public int AWBCount;
        public int TotalPieces;
        public long TaskId;
        public long FlightManifestId;
        public string RecoverLocation;
        public DateTime RecoveredDate;
        public string RecoveredBy;
        public int RecoveredULDs;
        public int ReceivedPieces;
        public FlightStatus Status;
        public int Flags;
     
    }



    public enum RecoverStatuses { All, Pending, InProgress, Complete, NotCompleted, NA }
    public class UldItem  
    {
        public bool IsBUP;
        public string Location;
        public RecoverStatuses Status;
        public int TotalPieces;
        public int ReceivedPieces;
        public string UldSerialNo;
        public string UldType;
        public long UldId;
        public long FlightManifestId;
        public string References;
        public string UldPrefix;
    }

    public class ShipmentItem
    {
        public bool IsForkLift;
        //public string Hwb;
        public string AWB;
        public string Locations;
        public int TotalPieces;
        public int ReceivedPieces;
        public string Uld;
        //public string UldType;
        public int ForkliftPieces;
        public int AvailablePieces;
        public long DetailId;
        public string References;
        public int Flags;
        public int Counter;
    }

 






    #region housebill region
    public class AddressObject
    {
        public string address;
        public string addressType;


    }
    public class HousebillData
    {
        public string housebill;
        public int totalPieces;
        public string[] locations;
        public string[] locationsDetails;
        public string[] dimensions;
        public string senderName;
        //public string senderAddress;
        public string receiverName;
        //public string receiverAddress;
        public string description;
        public string[] history;
        public string weight;
        public AddressObject[] addresses;

    }
    public class ShellData
    {
        public string Name;
        public string Reference;
        public List<ShellPage> m_pages;

        public ShellData()
        {
            m_pages = new List<ShellPage>();
        }
        public void AddPage(ShellPage page)
        {
            if (page != null)
                m_pages.Add(page);
        }

    }
    public class ShellPage
    {
        public string Name;
        public string Icon;
        public List<ShellItem> m_items;

        public ShellPage()
        {
            m_items = new List<ShellItem>();
        }
        public void AddItem(ShellItem item)
        {
            if (item != null)
                m_items.Add(item);

        }

    }
    public class ShellItem
    {
        public ShellItemType m_type;
        public string m_heading;
        public string m_description;
        public string m_image;
        public ShellItem()
        {

        }
        public ShellItem(ShellItemType type, string heading, string description, string image)
        {
            m_type = type;
            m_heading = heading;
            m_description = description;
            m_image = image;

        }
    }
    public enum ShellItemType
    {
        CONTACT,
        SHELL_ITEM_A,
        SHELL_ITEM_B,
        SHELL_ITEM_C,
        SHELL_IMAGE_ITEM_A,
        SHELL_IMAGE_ITEM_B,
        SHELL_IMAGE_ITEM_C,
        SHELL_ITEM_PHOTO
    }
    #endregion


    internal class MasterBillItem : IMasterBillItem
    {
        private WSLoadConsol.MasterBillItem mawb;

        internal WSLoadConsol.MasterBillItem MasterBill
        {
            get { return mawb; }
            set { mawb = value; }
        }



        internal MasterBillItem(WSLoadConsol.MasterBillItem mb)
        {
            mawb = mb;
        }

        #region IMasterBillItem Members

        public bool AllowShare
        {
            get { return mawb.AllowShare; }
        }

        public string MasterBillNumber
        {
            get { return mawb.MasterBillNumber; }
        }

        public string CarrierNumber
        {
            get { return mawb.CarrierNumber; }
        }

        public string Origin
        {
            get { return mawb.Origin; }
        }

        public string Destination
        {
            get { return mawb.Destination; }
        }

        public string FlightNumber
        {
            get { return mawb.FlightNumber; }
        }

        public int Weight
        {
            get { return (int)mawb.Weight; }
        }

        public int Pieces
        {
            get { return mawb.Pieces; }
        }

        public int ScannedPieces
        {
            get { return mawb.ScannedPieces; }
        }

        public int Slac
        {
            get { return mawb.Slac; }
        }

        public int HouseBills
        {
            get { return mawb.HouseBills; }
        }

        public int Ulds
        {
            get { return mawb.Ulds; }
        }

        public int Flags
        {
            get { return mawb.Flags; }
        }

#warning refactor to handle min value
        public DateTime CutOff
        {
            get { return mawb.CutOff ?? DateTime.MinValue; }
        }

        public int MasterBillId
        {
            get { return mawb.MasterBillId; }
        }
        public int TaskId
        { get { return mawb.TaskId; } }


        public MasterbillStatus Status
        {
            get { return (MasterbillStatus)Enum.Parse(typeof(MasterbillStatus), mawb.Status.ToString(), true); }
        }

        #endregion

        #region IMasterBillItem Members


        public MOT Mot
        {
            get { return this.mawb.Mot; }
        }

        #endregion
    }


    public class HouseBillItem : IHouseBillItem
    {
        private WSLoadConsol.HouseBillItem hawb;
        public HouseBillItem(WSLoadConsol.HouseBillItem item)
        {
            hawb = item;
        }
        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get { return hawb.HousebillNumber; }
        }

        public int HousebillId
        {
            get { return hawb.HousebillId; }
        }

        public string Origin
        {
            get { return hawb.Origin; }
        }

        public string Destination
        {
            get { return hawb.Destination; }
        }

        public int TotalPieces
        {
            get { return hawb.TotalPieces; }
        }

        public int ScannedPieces
        {
            get { return hawb.ScannedPieces; }
        }

        public int Slac
        {
            get { return hawb.TotalSlac; }
        }

        public int Weight
        {
            get { return (int)hawb.Weight; }
        }

        public string Location
        {
            get { return hawb.LastLocation; }
        }

        public int Flags
        {
            get { return hawb.Flags; }
        }

        public ScanModes ScanMode
        {
            get { return (ScanModes)Enum.Parse(typeof(ScanModes), hawb.ScanMode.ToString(), true); }
        }

        public HouseBillStatuses status
        {
            get { return (HouseBillStatuses)Enum.Parse(typeof(HouseBillStatuses), hawb.Status.ToString(), true); }
        }

        public RemoveModes RemoveMode
        {
            get { return (RemoveModes)Enum.Parse(typeof(RemoveModes), hawb.RemoveMode.ToString(), true); }
        }

        public PieceItem[] Pieces
        {
            get
            {
                return this.hawb.Pieces;
            }
        }

        #endregion
    }
    public interface IULDType
    {
        string ULDName { get; set; }
        int TypeID { get; set; }
        bool IsLoose { get; }
    }

    public interface IULDItem : IULDType
    {
        string ULDNo { get; set; }
        int ID { get; }
        double Weight { get; }
        int Pieces { get; }
        string Location { get; set; }
    }

    public interface IULD
    {
        string ULDType { get; set; }
        string ULDNo { get; set; }
        int TypeID { get; set; }
        int Pieces { get; }
        int ScannedPieces { get; }
        LocationItem Location { get; }
        double Weight { get; }
        int ID { get; }
        bool IsDefault { get; }
    }

}
