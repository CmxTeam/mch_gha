﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSOnHand;
using CargoMatrix.Communication.CargoMeasurements;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication.OnHand
{
    public class OnHand : CMXServiceWrapper
    {
        private wsCargoOnHand service;
        private static OnHand _instance;
        private string asmxName = "wsCargoOnHand.asmx";

        private OnHand()
        {
            service = new WSOnHand.wsCargoOnHand();
            service.Url = Settings.Instance.URLPath + asmxName;
        }

        public static OnHand Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new OnHand();
                }

                return _instance;
            }
        }

        public CMXAccount[] GetCustomerAccounts()
        {
            Func<CMXAccount[]> f = () =>
            {
                return service.GetPickupCustomers(gateway);
            };
            var defRet = new CMXAccount[0];
            return Invoke<CMXAccount[]>(f, 80001, defRet);
        }

        public OnHandTaskItem[] GetOnhandTasks(string filter, string sort)
        {
            Func<OnHandTaskItem[]> f = () =>
            {
                var filterEnum = GetFilter(filter);
                var sortEnum = GetSort(sort);
                return service.GetOnHandTasks(gateway, userName, filterEnum, sortEnum);
            };
            var defRet = new OnHandTaskItem[0];
            return Invoke<OnHandTaskItem[]>(f, 80002, defRet);
        }

        #region string To Enum converters
        private ShipmentStatus GetFilter(string filter)
        {
            switch (filter)
            {
                case "Not Started":
                    return ShipmentStatus.NotStarted;
                case "Not Completed":
                    return ShipmentStatus.NotCompleted;
                case "Completed":
                    return ShipmentStatus.Complete;
                case "Not Assigned":
                    return ShipmentStatus.NotAssigned;
                case "Assigned":
                    return ShipmentStatus.Assigned;
                case "In Progress":
                    return ShipmentStatus.Inprogress;
                default:
                    return ShipmentStatus.NotCompleted;
            };
        }
        private TaskSortBy GetSort(string sort)
        {
            switch (sort)
            {
                case "Pickup Reference":
                    return TaskSortBy.PICKUPREF;
                case "PO Number":
                    return TaskSortBy.PONUMBER;
                case "Vendor Name":
                    return TaskSortBy.VENDORNAME;
                case "Customer Name":
                    return TaskSortBy.CUSTOMERNAME;
                case "Origin":
                    return TaskSortBy.ORIGIN;
                case "Destination":
                    return TaskSortBy.DESTINATION;
                default:
                    return TaskSortBy.PICKUPREF;
            };
        }
        #endregion

        public CMXVendor[] GetVendors(string vendorSearch)
        {
            Func<CMXVendor[]> f = () =>
            {
                return service.GetVendorsList(vendorSearch);
            };
            var defRet = new CMXVendor[0];
            return Invoke<CMXVendor[]>(f, 80003, defRet);
        }

        public MODItem[] GetMOTList()
        {
            Func<MODItem[]> f = () =>
            {
                return service.GetMOTList(connectionName, gateway);
            };
            var defRet = new MODItem[0];
            return Invoke<MODItem[]>(f, 80004, defRet);
        }

        public CMXCarrier[] GetTruckingCompanyList(string searchString)
        {
            Func<CMXCarrier[]> f = () =>
            {
                return service.GetTruckingCompanyList(searchString);
            };
            var defRet = new CMXCarrier[0];
            return Invoke<CMXCarrier[]>(f, 80005, defRet);
        }

        public string[] GetTruckerNameList(string company)
        {
            Func<string[]> f = () =>
            {
                return service.GetTruckerNameList(connectionName, gateway, company);
            };
            var defRet = new string[0];
            return Invoke<string[]>(f, 80006, defRet);
        }

        public CMXPickup[] SearchPickup(string pickupRef, string customer, string vendor, string poNumber)
        {
            Func<CMXPickup[]> f = () =>
            {
                return service.SearchPickup(gateway, pickupRef, customer, vendor, poNumber);
            };
            var defRet = new CMXPickup[0];
            return Invoke<CMXPickup[]>(f, 80007, defRet);
        }

        public CMXPickup GetPickupDetails(string pickupRef)
        {
            Func<CMXPickup> f = () =>
            {
                return service.GetPickupDetails(pickupRef);
            };
            var defRet = new CMXPickup();
            return Invoke<CMXPickup>(f, 80008, defRet);
        }

        public CMXPickup CreateNewPickupSkel(string pickupRef, string vendor, string customer, string dest, string mot)
        {
            Func<CMXPickup> f = () =>
            {
                return service.CreateNewPickupSkel(gateway, pickupRef, vendor, customer, dest, mot);
            };
            var defRet = new CMXPickup();
            return Invoke<CMXPickup>(f, 80009, defRet);
        }

        public TransactionStatus RecoverPickup(string pickupRef, long pickupID, string truckCompanyName, string truckerName, string pro, int totalPcs, string door, string truck, byte[] signature)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RecoverPickupFromTrucker(gateway, pickupID, pickupRef, pro, truckCompanyName, truckerName, totalPcs, door, truck, signature, userName);
            };
            var defRet = new TransactionStatus { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80010, defRet);
        }

        //public string GetPickupReferences(int pickupID)
        //{
        //    Func<string> f = () =>
        //    {
        //        return service.GetPickupReferences(connectionName, gateway, pickupID);
        //    };
        //    var defRet = string.Empty;
        //    return Invoke<string>(f, 80011, defRet);
        //}

        public CMXAccount[] GetAllCustomersList()
        {
            Func<CMXAccount[]> f = () =>
            {
                return service.GetAllCustomersList();
            };
            var defRet = new CMXAccount[0];
            return Invoke<CMXAccount[]>(f, 80012, defRet);
        }

        public CMXShipment GetOnHandDetails(string pickupRef)
        {
            Func<CMXShipment> f = () =>
            {
                return service.GetOnHandDetails(gateway, null, pickupRef, userName);
            };
            return Invoke<CMXShipment>(f, 80013, null);
        }

        public TransactionStatus StagePickupAtLocation(long pickupID, string location)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.StagePickupInLocation(gateway, pickupID, location, userName);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80014, defRet);
        }

        public TransactionStatus AddOnHandPackage(long onhandId, CMXDomesticPackage dompackage)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.SaveOnhandPackage(userName, onhandId, dompackage, gateway);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80015, defRet);
        }

        public TransactionStatus RemoveOnHandPackage(long packageId, long onhandId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveOnhandPackage(userName, packageId, onhandId, gateway);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80016, defRet);
        }

        public CMXPackageType[] GetPackageTypes()
        {
            Func<CMXPackageType[]> f = () =>
            {
                return service.GetPackageTypes();
            };
            var defRet = new CMXPackageType[0];
            return Invoke<CMXPackageType[]>(f, 80017, defRet);
        }

        public CMXPackageCondition[] GetConditions()
        {
            Func<CMXPackageCondition[]> f = () =>
            {
                return service.GetConditions(gateway);
            };
            var defRet = new CMXPackageCondition[0];
            return Invoke<CMXPackageCondition[]>(f, 80018, defRet);
        }

        public TransactionStatus StartPickupRecovery(long pickupID)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.StartPickupRecovery(gateway, pickupID, userName);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80019, defRet);
        }

        public CMXAttribute[] GetAttributes(long onhandID)
        {
            Func<CMXAttribute[]> f = () =>
            {
                return service.GetOnHandAttributes(gateway, onhandID);
            };
            var defRet = new CMXAttribute[0];
            return Invoke<CMXAttribute[]>(f, 80020, defRet);
        }

        /// <summary>
        /// returns first line item if itemNo is -1
        /// </summary>
        /// <param name="pickupID"></param>
        /// <param name="orderNo"></param>
        /// <param name="itemNo"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public CMXInvoiceOrder GetOrderItemDetails(long pickupID, string orderNo, int itemNo, string account)
        {
            Func<CMXInvoiceOrder> f = () =>
            {
                return service.GetOrderItemDetails(pickupID, orderNo, itemNo, account);
            };
            var defRet = new CMXInvoiceOrder();
            return Invoke<CMXInvoiceOrder>(f, 80021, defRet);
        }

        public TransactionStatus SavePackageItemQuantity(long? lineItemId, long onhandID, long pickupID, long packageID, string orderNo, double receiveQty, int itemNo, string account)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.SavePackageItemQuantity(lineItemId, onhandID, pickupID, packageID, orderNo, receiveQty, itemNo, account, gateway, userName);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80022, defRet);
        }

        public CMXInvoiceOrder[] GetPickupOrderList(long pickupID, string orderNo, int? itemNo, string account)
        {
            Func<CMXInvoiceOrder[]> f = () =>
            {
                return service.GetPickupOrderItemDetails(pickupID, orderNo, itemNo, account);
            };
            var defRet = new CMXInvoiceOrder[0];
            return Invoke<CMXInvoiceOrder[]>(f, 80023, defRet);
        }

        public TransactionStatus AddShipmentReference(long onhandID, CMXReference reference)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.AddShipmentReference(onhandID, reference);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80024, defRet);
        }

        public TransactionStatus EditShipmentReference(CMXReference reference)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.EditShipmentReference(reference);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80025, defRet);
        }

        public TransactionStatus RemoveShipmentReference(CMXReference reference)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveShipmentReference(reference, userName);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80026, defRet);
        }

        public CMXReference[] GetShipmentReferences(long onhandID)
        {
            Func<CMXReference[]> f = () =>
            {
                return service.GetShipmentReferences(onhandID);
            };
            var defRet = new CMXReference[0];
            return Invoke<CMXReference[]>(f, 80027, defRet);
        }

        public CMXReference[] GetReferenceTypes(string account)
        {
            Func<CMXReference[]> f = () =>
            {
                return service.GetAccountReferenceTypes(account);
            };
            var defRet = new CMXReference[0];
            return Invoke<CMXReference[]>(f, 80028, defRet);
        }

        public TransactionStatus CompleteOnhand(long onhandID, string location)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.SetShipmentOnHandComplete(gateway, onhandID, userName, location);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80029, defRet);
        }

        public TransactionStatus UpdateOnhandAttributes(long onhandID, CMXAttribute[] attributes)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.UpdateOnHandAttributes(gateway, onhandID, userName, attributes);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80029, defRet);
        }

        public TransactionStatus PrintOnhandLabel(string printerName, long onhandID)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.PrintOnHandLabel(gateway, printerName, userName, onhandID);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80030, defRet);
        }

        public TransactionStatus PrintOnhandLabel(string printerName, long onhandID, int pieceNo)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.PrintOnHandLabelByPieceNumber(gateway, printerName, userName, onhandID, pieceNo);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80031, defRet);
        }

        public TransactionStatus PrintPackageLabel(string printerName, long onhandID, int pieceNo)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.PrintPackageLabelByPieceNumber(gateway, printerName, userName, onhandID, pieceNo);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80032, defRet);
        }

        public TransactionStatus PrintPackageLabel(string printerName, long onhandID, string pieceNos)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.PrintOnHandLabelsByMultiplePieceNumbers(gateway, printerName, userName, onhandID, pieceNos);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80033, defRet);
        }

        public TransactionStatus ScanPiecesToForklift(int[] pieceIds, long onhandID, int taskId, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.ScanPiecesToForkLift(connectionName, (int)onhandID, pieceIds, taskId, userId, ScanTypes.Manual, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80034, defRet);
        }

        public TransactionStatus DropAllPiecesToLocation(int taskId, string location, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropAllPiecesToLocation(connectionName, taskId, userId, location, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80035, defRet);
        }

        public TransactionStatus DropPiecesToLocation(PieceItem[] pieceIds, long onhandID, int taskId, string location, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropPiecesToLocation(connectionName, pieceIds, taskId, userId, ScanTypes.Manual, location, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80036, defRet);
        }

        public PieceItem[] GetForkliftPieces(int forkliftId)
        {
            Func<PieceItem[]> f = () =>
            {
                return service.GetForkliftPieces(connectionName, forkliftId, userId);
            };
            var defRet = new PieceItem[0];
            return Invoke<PieceItem[]>(f, 80037, defRet);
        }

        public TransactionStatus RemovePieceFromForklift(int onhandID, int pieceId, int taskId, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemovePieceFromForkLift(connectionName, onhandID, pieceId, userId, ScanModes.Piece, forkliftId, taskId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 80038, defRet);
        }


        public PieceItem[] GetOnhandPieces(int taskId)
        {
            Func<PieceItem[]> f = () =>
            {
                return service.GetOnhandPieces(connectionName, taskId, ScanStatuses.NotScanned, HouseBillFields.HousebillNumber);
            };
            var defRet = new PieceItem[0];
            return Invoke<PieceItem[]>(f, 80039, defRet);
        }

        public PackageItem ScanPieceToForklift(string onhandNo, int pieceNo, int taskId, int forkliftId)
        {
            Func<PackageItem> f = () =>
            {
                return service.ScanOnhandtoForklift(connectionName, onhandNo, pieceNo, taskId, userId, forkliftId);
            };
            var defRet = new PackageItem();
            defRet.TransactionStatus = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<PackageItem>(f, 80040, defRet);
        }

        public OnHandTaskItem GetOnhandTask(string pickupRef)
        {
            Func<OnHandTaskItem> f = () =>
            {
                return service.GetOnHandTask(gateway, pickupRef);
            };
            var defRet = new OnHandTaskItem();
            return Invoke<OnHandTaskItem>(f, 80041, defRet);
        }

        public TransactionStatus DropToLocationCompleted(long onhandId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropToLocationCompleted(gateway, onhandId, userName);
            };
            var defRet = new TransactionStatus();
            return Invoke<TransactionStatus>(f, 80042, defRet);
        }

        public TransactionStatus RemoveOnhandPackageBatch(long[] packageIds)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveOnhandPackageBatch(userName, packageIds, gateway);
            };
            var defRet = new TransactionStatus();
            return Invoke<TransactionStatus>(f, 80043, defRet);
        }

        public TransactionStatus SaveOnhandPackageBatch(long onhandId, CMXDomesticPackage package, int numOfPackages, long[] packageIds)
        {
            Func<TransactionStatus> f = () =>
            {
                int[] ids = null;
                if (packageIds != null && packageIds.Length > 0)
                    ids = (from l in packageIds select (int)l).ToArray();

                return service.SaveOnhandPackageBatch(userName, onhandId, package, numOfPackages, ids, gateway);
            };
            var defRet = new TransactionStatus();
            return Invoke<TransactionStatus>(f, 80044, defRet);
        }

        public CargoMatrix.Communication.ScannerUtilityWS.TaskSettings GetTaskSettings()
        {
            return ScannerUtility.Instance.GetTaskSettings(CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.Inventoy);
        }
    }
}
