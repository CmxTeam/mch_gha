﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.OnHand;

namespace CargoMatrix.Communication.WSOnHand
{
    public partial class CMXReference : IReference
    {
        public string ReferenceNumber
        {
            get
            {
                return this.referenceValueField;
            }
            set
            {
                this.referenceValueField = value; ;
            }
        }

        long IReference.ID
        {
            get
            {
                return this.domesticRecIDField;
            }
            set
            {
                this.domesticRecIDField = value;
            }
        }

    }
}
