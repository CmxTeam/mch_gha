﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.ScannerUtilityWS;
using CargoMatrix.Communication.Utility;
using CargoMatrix.Communication.DTO;
using System.Windows.Forms;
using CargoMatrix.Communication.Common;


namespace CargoMatrix.Communication
{
    public class ScannerUtility : CMXServiceWrapper
    {
        private ScannerUtilityWS.WsScannerUtility service;
        private static ScannerUtility _instance;
        private string asmxName = "WsScannerUtility.asmx";
        public static ScannerUtility Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ScannerUtility();
                return _instance;
            }
        }

        public CargoMatrix.Communication.DTO.User CurrentUser
        {
            get
            {
                return CargoMatrix.Communication.WebServiceManager.Instance().m_user;
            }
        }


        private ScannerUtility()
        {
            service = new ScannerUtilityWS.WsScannerUtility();
            service.Url = Settings.Instance.URLPath + asmxName;

        }

        public LocationItem[] GetLocations(LocationTypes locs)
        {
            Func<LocationItem[]> f = () =>
            {
                return service.GetLocations(connectionName, locs);
            };
            return Invoke<LocationItem[]>(f, 150001, new LocationItem[0]);
        }

        public UtilityShipmentCondition[] GetMasterBillShipmentConditions(int taskID)
        {
            Func<UtilityShipmentCondition[]> f = () =>
            {
                var conditions = this.service.GetConsolConditions(this.connectionName, taskID);

                return (
                            from item in conditions
                            select new UtilityShipmentCondition(item)
                        ).ToArray();
            };
            return Invoke<UtilityShipmentCondition[]>(f, 150002, null);
        }

        public UtilityShipmentConditionType[] GetMasterBillShipmentConditionTypes()
        {
            Func<UtilityShipmentConditionType[]> f = () =>
            {
                var conditionTypes = this.service.GetConsolConditionList(this.connectionName);

                return (
                            from item in conditionTypes
                            select new UtilityShipmentConditionType(item)
                       ).ToArray();
            };
            return Invoke<UtilityShipmentConditionType[]>(f, 150003, new UtilityShipmentConditionType[0]);
        }

        public bool UpdateMasterBillConditions(int taksID,
                                               int userID,
                                               CargoMatrix.Communication.Common.ShipmentCondition[] shipmentConditions)
        {
            Func<bool> f = () =>
            {

                var shpConditions = from item in shipmentConditions
                                    select CargoMatrix.Communication.Common.ShipmentCondition.ToWSScannerUtilityShipmentCondition(item);


                return this.service.UpdateConsolConditions(this.connectionName, taksID, userID, shpConditions.ToArray());
            };
            return Invoke<bool>(f, 150004, false);
        }

        public bool UpdateMasterBillConditions(int taksID,
                                               CargoMatrix.Communication.Common.ShipmentCondition[] shipmentConditions)
        {
            var user = this.CurrentUser;

            return this.UpdateMasterBillConditions(taksID, user.userID, shipmentConditions);
        }


      

        public UtilityShipmentConditionSummary[] GetMasterBillShipmentConditionsSummarys(int taskID)
        {
            Func<UtilityShipmentConditionSummary[]> f = () =>
            {
                var conditionSummaries = this.service.GetConsolConditionSummary(this.connectionName, taskID);
                if (conditionSummaries == null)
                    return new UtilityShipmentConditionSummary[0];

                return (
                            from item in conditionSummaries
                            select new UtilityShipmentConditionSummary(item)
                       ).ToArray();
            };
            return Invoke<UtilityShipmentConditionSummary[]>(f, 150005, new UtilityShipmentConditionSummary[0]);
        }

        public LocationItem[] GetPiecesByLocation(int hawbId, int masterbillId, MOT mot)
        {
            Func<LocationItem[]> f = () =>
            {
                return service.GetPiecesByLocation(connectionName, hawbId, masterbillId, mot);
            };
            return Invoke<LocationItem[]>(f, 150006, new LocationItem[0]);
        }

        public TransactionStatus ChangeHawbMode(int hawbId, int taskId, CargoMatrix.Communication.DTO.ScanModes changeToMode)
        {
            Func<TransactionStatus> f = () =>
            {
                CargoMatrix.Communication.ScannerUtilityWS.ScanModes newMode = (CargoMatrix.Communication.ScannerUtilityWS.ScanModes)Enum.Parse(typeof(CargoMatrix.Communication.ScannerUtilityWS.ScanModes), changeToMode.ToString(), true);
                return service.ChangeScanMode(connectionName, hawbId, WebServiceManager.Instance().m_user.userID, taskId, newMode);
            };
            return Invoke<TransactionStatus>(f, 150007, new TransactionStatus() { TransactionError = failMessage, TransactionStatus1 = false });
        }



        public bool PrintULDLabel(int uldId, int printerId, int copies)
        {
            Func<bool> f = () =>
            {
                var user = CargoMatrix.Communication.WebServiceManager.Instance().m_user;

                return service.PrintUldLabel(connectionName, uldId, printerId, copies, user.userID);
            };
            return Invoke<bool>(f, 150010, false);
        }
        public LabelPrinter[] GetPrinters(LabelTypes LblType)
        {
            Func<LabelPrinter[]> f = () =>
            {
                return service.GetLabelPrinters(connectionName, LblType, userName);
            };
            return Invoke<LabelPrinter[]>(f, 150011, new LabelPrinter[] { new LabelPrinter() });
        }

        public LabelPrinter GetDefaultPrinter(LabelTypes LblType)
        {
            Func<LabelPrinter> f = () =>
            {
                var prtrs = service.GetLabelPrinters(connectionName, LblType, userName);
                if (prtrs.Length < 1)
                    return new LabelPrinter();
                else
                    return prtrs.FirstOrDefault(p => p.IsDefault) ?? prtrs[0];

            };
            return Invoke<LabelPrinter>(f, 150012, new LabelPrinter());
        }

        public bool PrintMasterBillLabel(string carrier, string mawbNo, int printerId, int copies, string reason)
        {
            Func<bool> f = () =>
            {
                return service.PrintMasterBillLabel(connectionName, carrier, mawbNo, printerId, copies, reason, userName);
            };
            return Invoke<bool>(f, 150013, false);
        }

        public UtilityShipmentConditionType[] GetHouseBillShipmentConditionTypes()
        {
            Func<UtilityShipmentConditionType[]> f = () =>
            {
                var conditons = service.GetShipmentConditionList(connectionName);

                return (
                            from item in conditons
                            select new UtilityShipmentConditionType(item)
                       ).ToArray();
            };
            return Invoke<UtilityShipmentConditionType[]>(f, 150014, new UtilityShipmentConditionType[0]);
        }

        public UtilityShipmentConditionSummary[] GetHouseBillShipmentConditionSummary(int hawbId)
        {
            Func<UtilityShipmentConditionSummary[]> f = () =>
            {
                var conditinSumaries = service.GetShipmentConditionSummary(connectionName, hawbId);

                return (
                            from item in conditinSumaries
                            select new UtilityShipmentConditionSummary(item)
                        ).ToArray();
            };
            return Invoke<UtilityShipmentConditionSummary[]>(f, 150015, new UtilityShipmentConditionSummary[0]);
        }

        public bool UpdateHouseBillShipmentConditions(int houseBillID, Communication.DTO.ScanModes scanMode, Communication.Common.ShipmentCondition[] conditions, int taksID, TaskTypes taskType, string remark, int slac)
        {
            Func<bool> f = () =>
            {
                int userID = WebServiceManager.Instance().m_user.userID;

                var scnMode = ToWSWsScannerUtilityScanModes(scanMode);

                var utlityShipmentConditions = from item in conditions
                                               select Communication.Common.ShipmentCondition.ToWSScannerUtilityShipmentCondition(item);

                return service.UpdateShipmentConditions(connectionName, houseBillID, userID, scnMode, utlityShipmentConditions.ToArray(), taksID, taskType, remark, slac);
            };
            return Invoke<bool>(f, 150016, false);
        }

        public TaskSettings GetTaskSettings(TaskTypes task)
        {
            Func<TaskSettings> f = () =>
            {
                return service.GetTaskSettings(connectionName, WebServiceManager.Instance().m_user.userID, 0, task);
            };
            return Invoke<TaskSettings>(f, 150018, new TaskSettings() { Transaction = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage } });
        }

        public bool IsLocationValid(string loc)
        {
            Func<bool> f = () =>
            {
                return service.GetLocationId(connectionName, loc) > 0;
            };
            return Invoke<bool>(f, 150019, false);
        }

        public bool PrintTruckLabels(int[] truckIDs, int printerID, int numberOfCopies)
        {
            Func<bool> f = () =>
            {
                var user = CargoMatrix.Communication.WebServiceManager.Instance().m_user;

                return this.service.PrintTruckLabel(this.connectionName, truckIDs, printerID, numberOfCopies, user.userID);
            };
            return Invoke<bool>(f, 150020, false);
        }

        public IULDType[] GetULDTypes(bool getloose, MOT mot)
        {
            Func<IULDType[]> f = () =>
            {
                var res = this.service.GetUldTypes(connectionName, mot);

                if (getloose)
                    return res;
                else
                    return res.Where(uld => uld.IsLoose == false).ToArray();
            };
            return Invoke<IULDType[]>(f, 150021, new IULDType[0]);
        }

        public LocationItem[] GetPiecesByLocation(string hawbNo)
        {
            Func<LocationItem[]> f = () =>
            {
                return service.GetPiecesByLocationByNumber(connectionName, hawbNo);
            };
            return Invoke<LocationItem[]>(f, 150022, new LocationItem[0]);
        }

        public bool UpdateMasterBillConditions(string carrier, string mawbNo, CargoMatrix.Communication.Common.ShipmentCondition[] shipmentConditions)
        {
            Func<bool> f = () =>
            {
                var shpConditions = from item in shipmentConditions
                                    select CargoMatrix.Communication.Common.ShipmentCondition.ToWSScannerUtilityShipmentCondition(item);
                return this.service.UpdateConsolConditionsByMasterbill(this.connectionName, carrier, mawbNo, CurrentUser.userID, shpConditions.ToArray());
            };
            return Invoke<bool>(f, 150023, false);
        }


   


        public CargoMatrix.Communication.ScannerUtilityWS.MasterBillItem GetMasterBillByNumber(string carrierNumber, string masterbillNumber)
        {
            Func<CargoMatrix.Communication.ScannerUtilityWS.MasterBillItem> f = () =>
                {
                    return this.service.GetMasterBillByNumber(this.connectionName, carrierNumber, masterbillNumber);
                };
            return Invoke<CargoMatrix.Communication.ScannerUtilityWS.MasterBillItem>(f, 150024, null);
        }

        public CargoMatrix.Communication.ScannerUtilityWS.HouseBillItem GetHouseBillByNumber(string houseBillNumber)
        {
            Func<CargoMatrix.Communication.ScannerUtilityWS.HouseBillItem> f = () =>
            {
                return this.service.GetHouseBillByNumber(this.connectionName, houseBillNumber);
            };
            return Invoke<CargoMatrix.Communication.ScannerUtilityWS.HouseBillItem>(f, 150025, null);
        }

        public int? GetUldIdByNumber(string uldNumber)
        {
            Func<int?> f = () =>
                        {
                            int uldId = this.service.GetUldIdByNumber(this.connectionName, uldNumber);

                            return uldId == 0 ? (int?)null : uldId;
                        };
            return Invoke<int?>(f, 150026, null);
        }

        public bool PrintLocationLabel(string location, int printerId, int numberOfCopies)
        {
            Func<bool> f = () =>
            {
                string userName = CurrentUser.UserName;

                return this.service.PrintLocationLabel(this.connectionName, location, printerId, numberOfCopies, userName);
            };
            return Invoke<bool>(f, 150027, false);
        }

        public int? GetLocationId(string location)
        {
            Func<int?> f = () =>
                        {
                            int result = this.service.GetLocationId(this.connectionName, location);

                            if (result == 0) return null;

                            return result;
                        };

            return Invoke<int?>(f, 150028, null);
        }

        public LabelReason[] GetPrintReasons(bool isMasterbill)
        {
            Func<LabelReason[]> f = () =>
            {
                return service.GetPrintLabelReasons(connectionName, isMasterbill ? ShipmentType.MasterBill : ShipmentType.HouseBill);
            };
            return Invoke<LabelReason[]>(f, 150029, new LabelReason[0]);
        }

        public bool CapturePrintReason(string awbNo, string carrier, string reason, bool isMasterbill, int copies, int printerId)
        {
            Func<bool> f;
            if (isMasterbill)
                f = () => { return service.CreatePrintLabelLog(connectionName, string.Empty, carrier, awbNo, copies, reason, printerId, userName); };
            else
                f = () => { return service.CreatePrintLabelLog(connectionName, awbNo, string.Empty, string.Empty, copies, reason, printerId, userName); };

            return Invoke<bool>(f, 150030, false);
        }

        public PieceOwner[] GetHawbPiecesInForklift(string hawbNo)
        {
            Func<PieceOwner[]> f = () =>
            {
                return service.GetHawbPiecesInForklift(connectionName, hawbNo);
            };
            return Invoke<PieceOwner[]>(f, 150031, new PieceOwner[0]);

        }

        public bool ReleasePieces(string pieces)
        {
            Func<bool> f = () =>
            {
                return service.ReleasePiece(connectionName, pieces);
            };
            return Invoke<bool>(f, 150032, false);
        }

        public TransactionStatus PrintHouseBillLabels(int hawbId, int printerId, string pieceNumbers, string reason)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.PrintHouseBillLabelByPieceNumber(connectionName, printerId, hawbId, pieceNumbers, reason, userName);
            };
            return Invoke<TransactionStatus>(f, 150033, new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage });

        }

        public TransactionStatus PrintAllHousebillLabels(int printerId, string carrierNo, string mawbNo, string reason)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.PrintHBLabelByMB(connectionName, printerId, carrierNo, mawbNo, reason, userName);
            };
            return Invoke<TransactionStatus>(f, 150034, new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage });
        }

        public HouseBill[] GetHouseBillByMasterBill(string carrierNo, string mawbNo)
        {
            Func<HouseBill[]> f = () =>
            {
                return service.GetHouseBillByMasterBill(connectionName, carrierNo, mawbNo);
            };
            return Invoke<HouseBill[]>(f, 150035, new HouseBill[0]);
        }

        public bool CapturePrintReasonForConsole(string carrierNo, string mawbNo, string reason, int printerId)
        {
            Func<bool> f = () =>
            {
                return service.CreatePrintLabelLogForHBs(connectionName, carrierNo, mawbNo, reason, printerId, userName);
            };
            return Invoke<bool>(f, 150036, false);
        }

        public bool UpdateHawbDetails(int hawbId, string origin, string dest, int noOfPcs)
        {
            Func<bool> f = () =>
            {
                return service.UpdateHouseBill(connectionName, connectionName, hawbId, origin, dest, noOfPcs);
            };
            return Invoke<bool>(f, 150037, false);
        }

        public CarrierItem GetCarrierInfo(string carrier)
        {
            Func<CarrierItem> f = () =>
            {
                return service.GetCarrierItem(connectionName, carrier);

            };
            return Invoke<CarrierItem>(f, 150038, new CarrierItem());
        }

        public ScannerUtilityWS.User[] GetUsersByDepartment(string departmentCode)
        {
            Func<ScannerUtilityWS.User[]> f = () =>
                {
                    return this.service.GetUsersByDepartment(this.connectionName, departmentCode);
                };

            return this.Invoke<ScannerUtilityWS.User[]>(f, 150039, new ScannerUtilityWS.User[0]);
        }


        public string GetUserDepartment()
        {
            Func<string> f = () =>
                {
                    return this.service.GetUserDepartment(this.connectionName, this.CurrentUser.UserName);
                };

            return this.Invoke<string>(f, 150040, null);
        }

        public ScannerUtilityWS.User[] GetSameDepartmentUsers()
        {
            Func<ScannerUtilityWS.User[]> f = () =>
            {
                return this.service.GetSameDepartmentUsers(this.connectionName, this.CurrentUser.UserName);
            };

            return this.Invoke<ScannerUtilityWS.User[]>(f, 150041, new ScannerUtilityWS.User[0]);
        }

        public void SaveScannerLog(int errorCode, string stackTrace)
        {
            try
            {
                this.service.SaveExceptionLog(connectionName, userName, errorCode, stackTrace);

            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 150042);
            }
        }

        public PackageType[] GetPackageTypeList()
        {
            Func<PackageType[]> f = () =>
            {
                return this.service.GetPackageTypes(gateway);
            };

            return this.Invoke<PackageType[]>(f, 150043, new PackageType[0]);
        }


        public string[] GetGatewayReference(string referenceType)
        {
            Func<string[]> f = () =>
            {
                return this.service.GetGatewayReference(gateway, referenceType);
            };

            return this.Invoke<string[]>(f, 150043, null);
        }

        public ULD GetUldByNumber(string uldNumber)
        {
            Func<ULD> f = () =>
                {
                    return this.service.GetUldByNumber(this.connectionName, uldNumber);
                };

            return this.Invoke<ULD>(f, 150044, null);
        }

        public TransactionStatus SetDefaultPrinter(LabelPrinter printer)
        {
            Func<TransactionStatus> f = () =>
                {
                    return this.service.SetDefaultPrinter(printer, connectionName, userName);
                };

            return this.Invoke<TransactionStatus>(f, 150045, new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage });
        }

        private static ScannerUtilityWS.ScanModes ToWSWsScannerUtilityScanModes(CargoMatrix.Communication.DTO.ScanModes scanMode)
        {
            switch (scanMode)
            {
                case CargoMatrix.Communication.DTO.ScanModes.Count: return ScannerUtilityWS.ScanModes.Count;

                case CargoMatrix.Communication.DTO.ScanModes.NA: return ScannerUtilityWS.ScanModes.NA;

                case CargoMatrix.Communication.DTO.ScanModes.Piece: return ScannerUtilityWS.ScanModes.Piece;

                case CargoMatrix.Communication.DTO.ScanModes.Slac: return ScannerUtilityWS.ScanModes.Slac;

                default: return ScannerUtilityWS.ScanModes.NA;
            }

        }

        public TransactionStatus UpdateHouseBillDimensions(int houseBillID, ScannerUtilityWS.ScanModes scanModes, int pieceNumber, string packingMethod, double weight, int length, int width, int height, bool isManual)
        {
            Func<TransactionStatus> f = () =>
            {
                return this.service.UpdateHouseBillDimensions(this.connectionName, houseBillID, scanModes, pieceNumber, packingMethod, weight, length, width, height, this.CurrentUser.userID, isManual);
            };
            return Invoke<TransactionStatus>(f, 150046, new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage });
        }

    }
}
