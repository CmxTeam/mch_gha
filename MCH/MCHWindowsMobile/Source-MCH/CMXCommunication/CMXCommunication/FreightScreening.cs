﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSFreightScreening;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication
{
    public class FreightScreening : CMXServiceWrapper
    {
        private WsFreightScreening service;
        private static FreightScreening _instance;
        private string asmxName = "WsFreightScreening.Asmx";


        public static FreightScreening Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new FreightScreening();
                return _instance;
            }
        }
        private FreightScreening()
        {
            service = new WsFreightScreening();
            service.Url = Settings.Instance.URLPath + asmxName;

        }

        public HouseBillItem[] GetForkLiftHouseBills(int forkliftId)
        {
            Func<HouseBillItem[]> f = () =>
            {
                return service.GetForkLiftHouseBills(connectionName, userId, HouseBillFields.NA, forkliftId);
            };
            return Invoke<HouseBillItem[]>(f, 11301, new HouseBillItem[0]);
        }

        public int GetForkLiftHouseBillsCount(int forkliftId)
        {
            Func<int> f = () =>
            {
                return service.GetForkLiftHouseBillsCount(connectionName, userId, forkliftId);
            };
            return Invoke<int>(f, 11302, 0);
        }

        public TransactionStatus RemoveHousebillFromForklift(int hawbId, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveHousebillFromForklift(connectionName, hawbId, userId, forkliftId);
            };
            return Invoke<TransactionStatus>(f, 11303, new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage });
        }

        public TransactionStatus RemovePiecesFromForkLift(int hawbId, int[] pieceIds, int forkliftId, ScanModes mode)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemovePiecesFromForkLift(connectionName, hawbId, pieceIds, userId, mode, forkliftId);
            };
            return Invoke<TransactionStatus>(f, 11304, new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage });
        }

        public TransactionStatus RemovePieceFromForkLift(int hawbId, int pieceId, int forkliftId, ScanModes mode)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemovePieceFromForkLift(connectionName, hawbId, pieceId, userId, mode, forkliftId);
            };
            return Invoke<TransactionStatus>(f, 11305, new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage });

        }
    }
}
