﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public class MasterBillFilterCriteria
    {
        FilteringFieldsEnum filteringField;

        public FilteringFieldsEnum Value
        {
            get { return filteringField; }
            set { filteringField = value; }
        }

        public static FilteringFieldsEnum DefaultValue
        {
            get
            {
                return FilteringFieldsEnum.NOT_COMPLETED;
            }
        }

        public static FilteringFieldsEnum NoSortingValue
        {
            get
            {
                return FilteringFieldsEnum.ALL;
            }
        }

        public MasterBillFilterCriteria(FilteringFieldsEnum filteringField)
        {
            this.filteringField = filteringField;
        }

        public MasterBillFilterCriteria()
            : this(MasterBillFilterCriteria.DefaultValue)
        {
        }

        public static string ToString(FilteringFieldsEnum filteringField)
        {
            switch (filteringField)
            {
                case FilteringFieldsEnum.ALL: return "All";

                case FilteringFieldsEnum.COMPLETED: return "Complete";

                case FilteringFieldsEnum.IN_PROGRESS: return "In Progress";

                case FilteringFieldsEnum.NOT_COMPLETED: return "Not Complete";

                case FilteringFieldsEnum.NOT_STARTED: return "Not Started";

                default: throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return MasterBillFilterCriteria.ToString(this.filteringField);
        }


        public WSLoadConsol.MasterBillStatuses ToWsMasterBillStatuses()
        {
            switch (this.filteringField)
            {
                case FilteringFieldsEnum.ALL:  return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.All;

                case FilteringFieldsEnum.COMPLETED: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.Completed;

                case FilteringFieldsEnum.IN_PROGRESS: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.InProgress;

                case FilteringFieldsEnum.NOT_COMPLETED: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.Open;

                case FilteringFieldsEnum.NOT_STARTED: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.Pending;

                default: throw new NotImplementedException();

            }
        }

    }
}
