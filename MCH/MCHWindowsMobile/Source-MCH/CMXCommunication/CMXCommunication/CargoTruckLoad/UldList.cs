﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSLoadConsol;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public class UldList
    {
        CargoMatrix.Communication.DTO.IULD[] ulds;
        CargoMatrix.Communication.WSLoadConsol.UldList uldList;
        
        internal UldList(CargoMatrix.Communication.WSLoadConsol.UldList uldList)
        {
            this.uldList = uldList;

            this.ulds = (from item in uldList.Ulds
                         select new LoadConsol.ULD(item)).ToArray();
        }


        internal UldList()
        {
        }
        
        public CargoMatrix.Communication.DTO.IULD[] Ulds
        {
            get { return this.ulds; }
        }
        
        public TransactionStatus TransactionStatus
        {
            get
            {
                if (this.uldList == null) return new TransactionStatus { TransactionStatus1 = false };
                
                return uldList.Transaction; 
            }
        }
    }
}
