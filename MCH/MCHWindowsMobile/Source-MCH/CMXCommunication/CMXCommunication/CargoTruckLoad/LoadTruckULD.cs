﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public class LoadTruckULD : CargoMatrix.Communication.LoadConsol.ULD, ILoadTruckULD
    {
        WSLoadConsol.UldItem uldItem;
        
        public WSLoadConsol.HouseBillItem[] HouseBills
        {
            get
            {
                return this.uldItem.Housebills;
            
            }
        }
        
        public LoadTruckULD(WSLoadConsol.UldItem uldItem)
            : base(uldItem)
        {
            this.uldItem = uldItem;
        }
    }
}
