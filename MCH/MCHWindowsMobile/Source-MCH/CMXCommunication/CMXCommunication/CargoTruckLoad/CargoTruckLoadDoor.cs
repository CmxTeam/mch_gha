﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSLoadConsol;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public class CargoTruckLoadDoor : ICargoTruckLoadDoor
    {
        WSLoadConsol.LoadTruckLocationItem loadTruckLocationItem;
        WSLoadConsol.LocationItem truck;

        public CargoTruckLoadDoor(WSLoadConsol.LoadTruckLocationItem locationItem)
        {
            this.loadTruckLocationItem = locationItem;

            if(this.loadTruckLocationItem.TruckId != 0)
            {
                this.truck = new WSLoadConsol.LocationItem();
                this.truck.LocationId = this.loadTruckLocationItem.TruckId;
                this.truck.Location = this.loadTruckLocationItem.Truck;
            }
        }
        
        #region ICargoTruckLoadDoor Members

        public int ID
        {
            get { return this.loadTruckLocationItem.DoorId; }
        }

        public string Name
        {
            get { return this.loadTruckLocationItem.Door; }
        }

      
        public CargoMatrix.Communication.WSLoadConsol.LocationTypes locationType
        {
            get { return LocationTypes.Door; }
        }

        public WSLoadConsol.LocationItem Truck 
        {
            get
            {
                return this.truck;
            }
            set
            {
                this.truck = value;
            }
        }

        #endregion
    }
}
