﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSLoadConsol;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public interface ITruckLoadMasterBillItem : IMasterBillItem
    {
        LoadTruckLocationItem[] Doors { get; }

    }
}
