﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSLoadConsol;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public interface ICargoLoadMasterBill : IMasterBillItem
    {
        CarrierItem CarrierInfo { get; }

        IULD[] UldItems { get; }
    }
}
