﻿using System;
using CargoMatrix.Communication.ScannerUtilityWS;
namespace CargoMatrix.Communication.DTO
{
    public enum MasterbillStatus { All, Completed, InProgress, Open, Pending }

    //public enum FlightStatus { All, Completed, InProgress, Open, Pending }

    //public interface IFlightItem
    //{

    //    string CarrierLogo { get; }
    //    string CarrierCode { get; }
    //    string CarrierName { get; }
    //    string FlightNumber { get; }
    //    DateTime ETA { get; }
    //    string Origin { get; }
    //    int ULDCount { get; }
    //    int AWBCount { get; }
    //    int TotalPieces { get; }
    //    long TaskId { get; }
    //    long FlightManifestId { get; }
    //    string RecoverLocation { get; }
    //    DateTime RecoveredDate { get; }
    //    int RecoveredULDs { get; }
    //    int ReceivedPieces { get; }
    //     FlightStatus Status { get; }


    //}


    public interface IMasterBillItem
    {
        bool AllowShare { get; }
        string MasterBillNumber { get; }
        string CarrierNumber { get; }
        string Origin { get; }
        string Destination { get; }
        string FlightNumber { get; }
        int Weight { get; }
        int Pieces { get; }
        int ScannedPieces { get; }
        int Slac { get; }
        int HouseBills { get; }
        int Ulds { get; }
        int Flags { get; }
        int TaskId { get; }
        DateTime CutOff { get; }
        int MasterBillId { get; }
        MasterbillStatus Status { get; }
        CargoMatrix.Communication.WSLoadConsol.MOT Mot { get; }
    }

    public enum ScanModes { Count, NA, Piece, Slac }
    public enum HouseBillStatuses { All, Completed, InProgress, Open, Pending, OnHold, Cancelled, Bypass }
    public enum RemoveModes { All, NA, Shortage, TakeOff }
    public interface IHouseBillItem
    {
        string HousebillNumber { get; }
        int HousebillId { get; }
        string Origin { get; }
        string Destination { get; }
        int TotalPieces { get; }
        int ScannedPieces { get; }
        int Slac { get; }
        int Weight { get; }
        string Location { get; }
        int Flags { get; }
        ScanModes ScanMode { get; }
        HouseBillStatuses status { get; }
        RemoveModes RemoveMode { get; }
        //string PackingMethod { get; }
    }



    public class FlightFilters
    {
        public const string COMPLETED = "Completed";
        public const string IN_PROGRESS = "In Progress";
        public const string NOT_COMPLETED = "Not Completed";
        public const string NOT_STARTED = "Not Started";
    }
 
    public class FlightSorts
    {
        public const string FLIGHTNO = "Flight Number";
        public const string ORIGIN = "Origin";
        public const string CARRIER = "Carrier Number";
        public const string DESTINATION = "Destination";
    }
 
    public class MawbFilter
    {
        public const string COMPLETED = "Completed";
        public const string IN_PROGRESS = "In Progress";
        public const string NOT_COMPLETED = "Not Completed";
        public const string NOT_STARTED = "Not Started";
    }
    public class MawbSort
    {
        public const string MASTERBILL = "Masterbill";
        public const string WEIGHT = "Weight";
        public const string ORIGIN = "Origin";
        public const string CUTOFF = "Cutoff Time";
        public const string HAWBS = "Housebills";
        public const string CARRIER = "Carrier Number";
    }
    public class HawbFilter
    {
        public const string NOT_SCANNED = "NotScanned";
        public const string SCANNED = "Scanned";
        public const string SHORT = "Shortage";
        public const string TAKEOFF = "TakeOff";
        public const string REMOVED = "Removed";
        public const string LOCKED = "Locked";
    }
    public class HawbSort
    {//Destination, Security, Location, Scan
        public const string HAWB = "Housebill";
        public const string WEIGHT = "Weight";
        public const string PIECES = "Pieces";
        public const string ORIGIN = "Origin";
        public const string DESTINATION = "Destination";
        public const string LOCATION = "Location";
        public const string SCAN = "Scan";
        public const string UNKNOWN = "Unknown/Known";
        public const string WGHT_LOC = "Weight-Location";

    }
}
