﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.ScannerUpdateWS;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication
{
    public class ScannerUpdate:CMXServiceWrapper
    {
        private CMXScannerUpdateWS service;
        private static ScannerUpdate _instance;
        private string asmxName = "CMXScannerUpdateWS.asmx";

        public static ScannerUpdate Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ScannerUpdate();
                return _instance;
            }
        }

        private ScannerUpdate()
        {
            service = new CMXScannerUpdateWS();
            service.Url = Settings.Instance.URLPath + asmxName;
        }

        public bool IsNewerVersionAvailable()
        {
            Func<bool> f =()=>
            {
                return service.GetUpdateInfo(Utilities.AppCurrentVersion, true, connectionName).IsAvailable;
            };
            return Invoke<bool>(f, 160001, false);
        }
    }
}
