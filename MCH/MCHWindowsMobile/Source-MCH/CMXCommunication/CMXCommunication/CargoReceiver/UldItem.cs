﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication.WSCargoReceiver
{
    public partial class UldItem : IULDItem
    {

        public string ULDName
        {
            get { return this.uldField; }
            set { this.uldField = value; }
        }

        public string ULDNo
        {
            get { return this.uldNumberField; }
            set { this.uldNumberField = value; }
        }

        public int TypeID
        {
            get { return this.uldTypeIdField; }
            set { this.uldTypeIdField = value; }
        }

        public int ID
        {
            get { return this.uldIdField; }
        }

        public bool IsLoose
        {
            get { return string.Equals(this.uldField, "loose", StringComparison.OrdinalIgnoreCase); }
        }

        string IULDItem.Location
        {
            get
            {
                return this.locationField.Location;
            }
            set
            {
                this.locationField.Location = value;
            }
        }

    }
}
