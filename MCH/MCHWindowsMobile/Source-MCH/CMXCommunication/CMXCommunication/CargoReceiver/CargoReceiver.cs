﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
//using CargoMatrix.ExceptionManager;
using CargoMatrix.Communication.WSCargoReceiver;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication
{
    public class CargoReceiver : CMXServiceWrapper
    {
        private WsCargoReceiver service;
        private static CargoReceiver _instance;
        private string asmxName = "WSCargoReceiver.asmx";

        public static CargoReceiver Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new CargoReceiver();
                return _instance;
            }
        }
        private CargoReceiver()
        {
            service = new CargoMatrix.Communication.WSCargoReceiver.WsCargoReceiver();
            service.Url = Settings.Instance.URLPath + asmxName;

        }

        /// <summary>
        /// Returns Receiver Masterbills 
        /// </summary>
        /// <param name="direction">Import, Domestic, NA</param>
        /// <param name="sort"></param>
        /// <param name="filter"></param>
        /// <param name="assigned">return assigned tasks</param>
        /// <returns></returns>
        public CargoMatrix.Communication.DTO.IMasterBillItem[] GetMasterbillList(MasterBillDirection direction, string sort, string filter, bool assigned)
        {
            Func<CargoMatrix.Communication.DTO.IMasterBillItem[]> f = () =>
            {
                MasterBillStatuses status = MasterBillStatuses.All;
                switch (filter)
                {
                    case CargoMatrix.Communication.DTO.MawbFilter.NOT_STARTED:
                        status = MasterBillStatuses.Pending;
                        break;
                    case CargoMatrix.Communication.DTO.MawbFilter.IN_PROGRESS:
                        status = MasterBillStatuses.InProgress;
                        break;
                    case CargoMatrix.Communication.DTO.MawbFilter.COMPLETED:
                        status = MasterBillStatuses.Completed;
                        break;
                    case CargoMatrix.Communication.DTO.MawbFilter.NOT_COMPLETED:
                        status = MasterBillStatuses.Open;
                        break;
                    default:
                        status = MasterBillStatuses.All;
                        break;
                }
                CargoReceiverMasterBillsSortField sortfield = CargoReceiverMasterBillsSortField.Consol;
                switch (sort)
                {
                    case CargoMatrix.Communication.DTO.MawbSort.ORIGIN:
                        sortfield = CargoReceiverMasterBillsSortField.Origin;
                        break;
                    case CargoMatrix.Communication.DTO.MawbSort.CARRIER:
                        sortfield = CargoReceiverMasterBillsSortField.Carrier;
                        break;
                    case CargoMatrix.Communication.DTO.MawbSort.HAWBS:
                        sortfield = CargoReceiverMasterBillsSortField.Shipments;
                        break;
                    default:
                        sortfield = CargoReceiverMasterBillsSortField.Consol;
                        break;
                }
                MasterBillItem filterMawb = new MasterBillItem() { Status = status };

                string userid = assigned ? WebServiceManager.Instance().m_user.UserName : string.Empty;

                if (assigned)
                {
                    filterMawb.UserId = CargoMatrix.Communication.WebServiceManager.Instance().m_user.userID;
                    direction = MasterBillDirection.NA;
                }
                var items = service.GetCargoReceiverMasterBills2(connectionName, userid, status, string.Empty, string.Empty, string.Empty, direction, sortfield);
                return items;
            };
            return Invoke<CargoMatrix.Communication.DTO.IMasterBillItem[]>(f, 11401, new CargoMatrix.Communication.DTO.IMasterBillItem[0]);
        }

        public MasterBillItem GetMasterbillInfo(string carrierNo, string mawbNo, string hawbNo)
        {
            Func<MasterBillItem> f = () =>
            {
                return service.GetMasterBillInfo(connectionName, carrierNo, mawbNo, hawbNo);
            };
            var defRet = new MasterBillItem() { Transaction = new TransactionStatus { TransactionStatus1 = false, TransactionError = failMessage } };
            return Invoke<MasterBillItem>(f, 11402, defRet);
        }

        public TransactionStatus RecoverConsol(int taskId, string location, string truck, int partId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RecoverConsol(connectionName, taskId, userId, location, truck, partId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11403, defRet);
        }

        public HouseBillItem[] GetForkLiftHouseBills(int forkliftid, int taskId)
        {
            Func<HouseBillItem[]> f = () =>
            {
                return service.GetForkLiftHouseBills(connectionName, userId, HouseBillFields.HousebillNumber, forkliftid, taskId);
            };
            return Invoke<HouseBillItem[]>(f, 11404, new HouseBillItem[0]);
        }

        public HouseBillItem ScanHawbIntoForklift(string hawbNo, int pieceNumber, int taskId, int forkliftId)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.ScanPieceNumberIntoForkLift(connectionName, hawbNo, pieceNumber, taskId, userId, ScanTypes.Automatic, forkliftId);
            };
            var defRet = new HouseBillItem { Transaction = new TransactionStatus { TransactionStatus1 = false, TransactionError = failMessage } };

            return Invoke<HouseBillItem>(f, 11405, defRet);
        }

        public HouseBillItem ScanHawbCountIntoForklift(int hawbId, int pieceCount, int taskId, int forkliftId, ScanTypes scanType)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.ScanPieceCountIntoForkLift(connectionName, hawbId, pieceCount, taskId, userId, scanType, forkliftId);
            };
            var defRet = new HouseBillItem { Transaction = new TransactionStatus { TransactionStatus1 = false, TransactionError = failMessage } };
            return Invoke<HouseBillItem>(f, 11406, defRet);
        }

        public TransactionStatus GetForkLiftHouseBillsCount(int forkliftId, int taskId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.GetTransactionInfo(connectionName, userId, forkliftId, taskId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11407, defRet);
        }

        public CargoMatrix.Communication.ScannerUtilityWS.TaskSettings GetTaskSettings()
        {
            return ScannerUtility.Instance.GetTaskSettings(CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.CargoReceiver);
        }

        public TransactionStatus RemoveHawbFromForkLift(int hawbId, int taskId, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveHousebillFromForklift(connectionName, hawbId, taskId, userId, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11409, defRet);
        }

        public TransactionStatus RemovePieceFromForkLift(int hawbId, int pieceId, int taskId, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemovePieceFromForkLift(connectionName, hawbId, pieceId, userId, ScanModes.Piece, forkliftId, taskId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11410, defRet);
        }

        public TransactionStatus DropForkliftIntoLocation(int taskId, string locationName, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropAllPiecesIntoLocation(connectionName, taskId, userId, locationName, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11411, defRet);
        }

        public TransactionStatus DropPiecesIntoLocation(PieceItem[] pieces, int taskId, string locname, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropPiecesIntoLocation(connectionName, pieces, taskId, userId, ScanTypes.Manual, locname, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11412, defRet);
        }

        //public LocationItem[] GetPiecesByLocation(int hawbId)
        //{
        //    try
        //    {
        //        return service.GetPiecesByLocation(connectionName, hawbId);
        //    }
        //    catch (Exception ex)
        //    {
        //        CMXExceptionManager.DisplayException(ex, 11413);
        //    }
        //    return new LocationItem[0];
        //}

        public HouseBillItem ScanHawbPiecesIntoForklift(string hawbNo, int[] pieceNumbers, int taskId, int forkliftId)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.ScanPiecesIntoForkLift(connectionName, hawbNo, pieceNumbers, taskId, userId, ScanTypes.Manual, forkliftId);
            };
            return Invoke<HouseBillItem>(f, 11414, new HouseBillItem());
        }

        public HouseBillItem[] GetHousebillList(int taskId, ScanStatuses filter, HouseBillFields sort)
        {
            Func<HouseBillItem[]> f = () =>
            {
                return service.GetCargoReceiverHouseBills(connectionName, taskId, filter, sort);
            };
            return Invoke<HouseBillItem[]>(f, 11416, new HouseBillItem[0]);
        }

        public HouseBillItem[] GetHousebillListWithFSP(int taskId, ScanStatuses filter, HouseBillFields sort, bool getFSP)
        {
            Func<HouseBillItem[]> f = () =>
            {
                return service.GetCargoReceiverHouseBillsWithFSP(connectionName, taskId, filter, sort, getFSP);
            };
            return Invoke<HouseBillItem[]>(f, 11416, new HouseBillItem[0]);
        }

        public HouseBillItem GetHousebillInfo(int hawbId, int taskId)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.GetHouseBillInfo(connectionName, hawbId, taskId);
            };
            return Invoke<HouseBillItem>(f, 11417, new HouseBillItem());
        }


        public TransactionStatus FinalizeCargoReceiver(int taskId, bool overrideFlag)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.FinalizeCargoReceiver(connectionName, taskId, userId, overrideFlag);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11418, defRet);
        }

        public SealTypeItem[] GetSealTypes()
        {
            Func<SealTypeItem[]> f = () =>
            {
                return service.GetSealTypes(connectionName, "V");
            };
            return Invoke<SealTypeItem[]>(f, 11419, new SealTypeItem[0]);
        }

        public bool SaveSealInformation(string carrier, string consol, int sealTypeId, string sealRef, string remark, string sealApproval)
        {
            Func<bool> f = () =>
            {
                return service.SaveSealInformation(connectionName, carrier, consol, sealTypeId.ToString(), sealRef, WebServiceManager.Instance().m_user.UserName, WebServiceManager.Instance().m_gateway, remark, sealApproval);
            };
            return Invoke<bool>(f, 11420, false);
        }


        public CargoMatrix.Communication.DTO.IULDItem[] GetMasterbillULDs(int mawbId)
        {
            Func<CargoMatrix.Communication.DTO.IULDItem[]> f = () =>
            {
                return service.GetMasterBillUlds(connectionName, mawbId);
            };
            return Invoke<CargoMatrix.Communication.DTO.IULDItem[]>(f, 11421, new CargoMatrix.Communication.DTO.IULDItem[0]);
        }

        public TransactionStatus AttachULD(int mawbId, int typeId, string number, string location, bool force)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.AttachUld(connectionName, mawbId, typeId, number, location, force);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11422, defRet);
        }

        public TransactionStatus DeleteULD(int uldId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DeleteAttachedUld(connectionName, uldId);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11410, defRet);
        }


        public TransactionStatus CreateHousebill(string hawbNo,string mawbNo,string carrierNo,  string origin, string dest, int pieces, int taskId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.CreateHousebillSkel(connectionName, origin, hawbNo, mawbNo, carrierNo, dest, pieces, taskId, userId);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11426, defRet);
        }


        public bool UpdateULD(CargoMatrix.Communication.DTO.IULDItem uld)
        {
            Func<bool> f = () =>
            {
                return service.UpdateAttachedUld(connectionName, uld.ID, uld.TypeID, uld.ULDNo, uld.Location);
            };
            return Invoke<bool>(f, 11424, false);
        }

        public TransactionStatus StageMasterBill(int mawbId, int uldId, string location)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.StageMasterBill(connectionName, mawbId, uldId, location, userId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11425, defRet);
        }
    }
}
