﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Reflection;
using System.IO;

namespace CargoMatrix.Communication
{
    [XmlRoot("CMXConfig", IsNullable = false)]
    public class CMXConfig
    {
        public string connectionName;
        public string gateway;
        public string ScannerWSURL;
        public string UpdaterWSURL;
        //public int camera;
    }

    public class Settings
    {
        string Url, urlPath, connectionName, gateway,AppUrl, appUrlPath;
        //int camera;
        //public int CameraType
        //{ get { return camera; } }
        private static Settings instance;
        private Settings()
        {
            string ConfigFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\CMXScannerConfig.xml";
            //string ConfigFilename = @"CMXScannerConfig.xml";
            //instance = new Settings();
             System.Xml.XmlDocument xmlFile = new System.Xml.XmlDocument();
             xmlFile.Load(ConfigFile);
             System.Xml.XmlNode root = xmlFile.SelectSingleNode("CMXConfig");
             if (root != null)
             {
                 urlPath = root.SelectSingleNode("WSURL").InnerText;
                 appUrlPath = root.SelectSingleNode("APPWSURL").InnerText;
                 connectionName = root.SelectSingleNode("connectionName").InnerText;
                 //camera = Convert.ToInt32(root.SelectSingleNode("camera").InnerText);
                 gateway = root.SelectSingleNode("gateway").InnerText;

                 if (urlPath != null)
                 {
                     if (urlPath[urlPath.Length - 1] == '/')
                     {
                         Url = urlPath;// +"CMXScannerWebService.asmx";

                     }
                     else
                     {
                         Url = urlPath + @"/";
                     
                     }
                 }


                 if (appUrlPath != null)
                 {
                     if (appUrlPath[appUrlPath.Length - 1] == '/')
                     {
                         AppUrl = appUrlPath;// +"CMXScannerWebService.asmx";

                     }
                     else
                     {
                         AppUrl = appUrlPath + @"/";

                     }
                 }


             
             }

        }
        public static Settings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Settings();
                }

                return instance;
            }

        }
       
        public string URLPath
        {
            get { return urlPath; }
        }

        public string AppURLPath
        {
            get { return AppUrl; }
        }

        public string ConnectionName
        {
            get { return connectionName; }
        }
        public string Gateway
        {
            get { return gateway; }
        }
    }
}
