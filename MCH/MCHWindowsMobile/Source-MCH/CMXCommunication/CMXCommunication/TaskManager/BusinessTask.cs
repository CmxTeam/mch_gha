﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.TaskManager
{
    public class BusinessTask
    {
        private List<string> userIDs;
        private List<string> ulds;


        public string ID { get; set; }
        public string MasterBillNumber { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string CarrierNumber { get; set; }
        public string Reference { get; set; }
        public string Weight { get; set; }
        public string Pieces { get; set; }
        public string Description { get; set; }
        public string Percent { get; set; }

        //public DateTime? CutOffTime { get; set; }

        public string HouseBillNumber { get; set; }

        public TaskTypeEnum TaskType
        {
            get
            {
                try
                {
                    int id = int.Parse(this.ActionID);

                    return id == 97 ? TaskTypeEnum.HouseBill : TaskTypeEnum.MasterBill;
                }
                catch
                {
                    return TaskTypeEnum.Unknown;
                }
            }
        }
        public TaskProgressTypeEnum? Status { get; set; }
        public string ActionID { get; set; }
        public string ProcessID { get; set; }


        public List<string> UserIDs
        {
            get { return this.userIDs; }
        }

        public List<string> Ulds
        {
            get { return ulds; }

        }


        public bool IsAssigned
        {
            get
            {
                return this.userIDs.Count() > 0;
            }
        }

        public string GeneralReference
        {
            get
            {
                switch (this.TaskType)
                {
                    case TaskTypeEnum.HouseBill: return string.Format("{0}-{1}-{2}", this.Origin, this.HouseBillNumber, this.Destination);

                    case TaskTypeEnum.MasterBill: return this.Reference;

                    default: return string.Empty;
                }

                return this.Reference;
            }
        }

        public string GeneralDescription
        {
            get
            {
                switch (this.TaskType)
                {
                    case TaskTypeEnum.HouseBill: return string.Format("Psc: {0}   Wgt: {1}", this.Pieces, this.Weight);

                    case TaskTypeEnum.MasterBill: return this.Description;

                    default: return string.Empty;
                }

                return this.Reference;
            }
        }



        public BusinessTask(string iD)
        {
            this.ID = iD;
            this.userIDs = new List<string>();
            this.ulds = new List<string>();
        }

        public BusinessTask(string iD, string[] userIDs)
            : this(iD)
        {
            var iDs = from userId in userIDs
                      where string.IsNullOrEmpty(userId) == false
                      select userId;

            this.userIDs.AddRange(iDs);
        }


        public BusinessTask(string iD, string[] userIDs, string[] ulds)
            : this(iD, userIDs)
        {
            var iDs = from uld in ulds
                      where string.IsNullOrEmpty(uld) == false
                      select uld;

            this.Ulds.AddRange(iDs);
        }


        public static TaskProgressTypeEnum? ToBusinessTaskStatus(string statusStr)
        {
            try
            {
                switch (statusStr.ToUpper())
                {
                    case "NOT STARTED": return TaskProgressTypeEnum.NotStarted;
                    case "NOT ASSIGNED": return TaskProgressTypeEnum.NotAssigned;
                    case "IN PROGRESS": return TaskProgressTypeEnum.InProgress;
                    case "COMPLETED": return TaskProgressTypeEnum.Completed;
                }

                return (TaskProgressTypeEnum)Enum.Parse(typeof(TaskProgressTypeEnum), statusStr, true);
            }
            catch
            {
                return null;
            }
        }


        public enum TaskTypeEnum
        {
            MasterBill,
            HouseBill,
            Unknown
        }
    }
}
