﻿using System;
using System.Linq;
using System.Collections.Generic;
using CargoMatrix.Communication.InventoryWS;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication
{
    public class Relocator : CMXServiceWrapper
    {
        private WsInventory service;
        private static Relocator _instance;
        private string asmxName = "WsInventory.asmx";

        public static Relocator Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Relocator();
                return _instance;
            }
        }

        private Relocator()
        {
            service = new CargoMatrix.Communication.InventoryWS.WsInventory();
            service.Url = Settings.Instance.URLPath + asmxName;

        }

        public HouseBillItem ScanPieceIntoForklift(string hawbNo, int pieceNo, int taskID, int forkliftID, MOT mot)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.RelocationScanPieceIntoForklift(connectionName, hawbNo, pieceNo, taskID, userId, ScanTypes.Automatic, forkliftID, mot);
            };

            return Invoke<HouseBillItem>(f, 50001, null);
        }

        public TransactionStatus DropPiecesIntoLocation(RelocationPieceItem[] pieces, int taskID, string locationName, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RelocationDropPiecesIntoLocation(connectionName, pieces, taskID, userId, ScanTypes.Manual, locationName, forkliftID);
            };
            TransactionStatus defReturn = new TransactionStatus { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 50002, defReturn);
        }

        public TransactionStatus DropAllPiecesIntoLocation(int taskID, string locationName, int forkliftID)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RelocationDropAllPiecesIntoLocation(connectionName, taskID, userId, locationName, forkliftID);
            };
            TransactionStatus defReturn = new TransactionStatus { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 50003, defReturn);
        }

        public RelocationPieceItem[] GetForkliftPieces(int forkliftID, int taskID)
        {
            Func<RelocationPieceItem[]> f = () =>
            {
                return service.RelocationGetForkliftPieces(connectionName, forkliftID, userId, taskID);
            };
            RelocationPieceItem[] defReturn = new RelocationPieceItem[0];
            return Invoke<RelocationPieceItem[]>(f, 50004, defReturn);
        }

        public int GetForkliftPiecesCount(int forkliftID, int taskID)
        {
            Func<int> f = () =>
            {
                return service.RelocationGetForkliftPiecesCount(connectionName, forkliftID, userId, taskID);
            };
            int defReturn = 0;
            return Invoke<int>(f, 50005, defReturn);
        }

        public TransactionStatus RemoveHousebillFromForklift(int housebillID, int taskID, int forkliftID, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RelocationRemoveHousebillFromForklift(connectionName, housebillID, taskID, userId, forkliftID, mot);
            };
            TransactionStatus defReturn = new TransactionStatus { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 50006, defReturn);
        }

        public TransactionStatus RemovePieceFromForkLift(int housebillID, int pieceIDorNumber, int taskID, ScanModes scanMode, int forkliftID, MOT mot)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RelocationRemovePieceFromForkLift(connectionName, housebillID, pieceIDorNumber, taskID, userId, scanMode, forkliftID, mot);
            };
            TransactionStatus defReturn = new TransactionStatus { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 50007, defReturn);
        }

        public CargoMatrix.Communication.ScannerUtilityWS.TaskSettings GetTaskSettings()
        {
            return ScannerUtility.Instance.GetTaskSettings(CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.Relocation);
        }
    }
}
