﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common
{
    public class ShipmentCondition : IShipmentCondition
    {
        IShipmentConditionType[] conditionTypes;
        int pieces;
        int pieceID;

        #region IShipmentCondition Members

        public IShipmentConditionType[] Conditions
        {
            get 
            {
                return this.conditionTypes;
            }
            set
            {
                this.conditionTypes = value;
            }
        }

        public int Pieces
        {
            get 
            {
                return this.pieces;
            }
            set
            {
                this.pieces = value;
            }
        }

        public int PieceId
        {
            get 
            {
                return this.pieceID;
            }
            set
            {
                this.pieceID = value;
            }
        }

        #endregion


        public static ScannerUtilityWS.ShipmentCondition ToWSCargoDischargeShipmentCondition(ShipmentCondition shipmentCondition)
        {
            ScannerUtilityWS.ShipmentCondition result = new CargoMatrix.Communication.ScannerUtilityWS.ShipmentCondition();

            result.PieceId = shipmentCondition.PieceId;
            result.Pieces = shipmentCondition.Pieces;

            result.Conditions = (
                                    from item in shipmentCondition.Conditions
                                    select new ScannerUtilityWS.ShipmentConditionType()
                                    {
                                        ConditionTypeCode = item.ConditionTypeCode,
                                        ConditionTypeId = item.ConditionTypeId,
                                        ConditionTypeName = item.ConditionTypeName
                                    }
                                ).ToArray();

            return result;

        }


        public static WSLoadConsol.ShipmentCondition ToWSLoadConsolShipmentCondition(ShipmentCondition shipmentCondition)
        {
            WSLoadConsol.ShipmentCondition result = new CargoMatrix.Communication.WSLoadConsol.ShipmentCondition();

            result.PieceId = shipmentCondition.PieceId;
            result.Pieces = shipmentCondition.Pieces;

            result.Conditions = (
                                    from item in shipmentCondition.Conditions
                                    select new WSLoadConsol.ShipmentConditionType()
                                    {
                                        ConditionTypeCode = item.ConditionTypeCode,
                                        ConditionTypeId = item.ConditionTypeId,
                                        ConditionTypeName = item.ConditionTypeName
                                    }
                                ).ToArray();

            return result;

        }

        public static ScannerUtilityWS.ShipmentCondition ToWSScannerUtilityShipmentCondition(ShipmentCondition shipmentCondition)
        {
            ScannerUtilityWS.ShipmentCondition result = new CargoMatrix.Communication.ScannerUtilityWS.ShipmentCondition();

            result.PieceId = shipmentCondition.PieceId;
            result.Pieces = shipmentCondition.Pieces;


            result.Conditions = (
                                    from item in shipmentCondition.Conditions
                                    select new ScannerUtilityWS.ShipmentConditionType()
                                    {
                                        ConditionTypeCode = item.ConditionTypeCode,
                                        ConditionTypeId = item.ConditionTypeId,
                                        ConditionTypeName = item.ConditionTypeName
                                    }
                                ).ToArray();
            return result;
        }
    
    }
}
