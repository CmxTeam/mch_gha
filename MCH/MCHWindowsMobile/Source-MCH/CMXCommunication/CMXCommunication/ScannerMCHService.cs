﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using CargoMatrix.Communication.WSScannerMCHService;
using System.Data;
using CargoMatrix.Communication.Common;
using CargoMatrix.Communication.Json.Managers;
using CargoMatrix.Communication.Common.DTO.Membership;
using CargoMatrix.Communication.Common.Exceptions;
namespace CargoMatrix.Communication
{
    public class WSScannerMCHServiceObj : CargoMatrix.Communication.WSScannerMCHService.ScannerMCHService
    {
        //string path = "http://10.0.0.235/ScannerServices/";
        string asmxName = "ScannerMCHService.asmx";
        public WSScannerMCHServiceObj()
        {
            //Do not Update from here
            //if (path == string.Empty)
            //{
                Url = Settings.Instance.AppURLPath + asmxName;
            //}
            //else
            //{
            //    Url = path + asmxName;
            //}

        }
        public string URL
        {
            get { return Url; }
        }

    }

    public class ScannerMCHServiceManager
    {

        //private static string gateway = "BOS";
        //private static string connectionName = "BOS";

        private static ScannerMCHServiceManager instance;
        WSScannerMCHServiceObj ws;
        private ScannerMCHServiceManager()
        {
            ws = new WSScannerMCHServiceObj();
        }
        public static ScannerMCHServiceManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ScannerMCHServiceManager();
                }

                return instance;
            }

        }

        public string GetLocationMCH()
        {
            //string currentConnectionName = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentConnectionName = Settings.Instance.ConnectionName;
            //}
            //else
            //{
            //    currentConnectionName = ScannerMCHServiceManager.connectionName;
            //}
            //return currentConnectionName;
            return GetConnectionName();
        }


        public string GetConnectionName()
        {

            //if (CargoLoaderMCHService.connectionName == string.Empty)
            //{
            return Settings.Instance.ConnectionName;
            //}
            //else
            //{
            //    return CargoLoaderMCHService.connectionName;
            //}

        }


      



		//public int LoginByPin(string pin)
		//{
		//    try
		//    {
 
		//        //string currentConnectionName = string.Empty;
		//        //if (ScannerMCHServiceManager.connectionName == string.Empty)
		//        //{
		//        //    currentConnectionName = Settings.Instance.ConnectionName;
		//        //}
		//        //else
		//        //{
		//        //    currentConnectionName = ScannerMCHServiceManager.connectionName;
		//        //}


		//        UserSession session = ws.LoginByPin(GetConnectionName(), pin);

		//        if (session == null) { return 1; }
                

		//        OpenNETCF.WindowsCE.DateTimeHelper.LocalTime = session.LocalTime;



		//        WSPieceScan.UserTypes userTypes;
		//        if (session.GroupID == UserTypes.Admin)
		//        {
		//            userTypes = CargoMatrix.Communication.WSPieceScan.UserTypes.Admin;
		//        }
		//        else
		//        {
		//            userTypes = CargoMatrix.Communication.WSPieceScan.UserTypes.User;
		//        }


		//        WebServiceManager.SetUserID((int)session.UserID, session.UserName, userTypes);
		//        CargoMatrix.Communication.WebServiceManager.Instance().m_user.firstName = session.FirstName;
		//        CargoMatrix.Communication.WebServiceManager.Instance().m_user.lastName = session.LastName;
		//        CargoMatrix.Communication.WebServiceManager.Instance().m_user.pin = pin;
                
		//        if (session.Timeout == null)
		//            session.Timeout = 30;

		//        GuidManager.SetGuid(session.SessionGuid.ToString(), (int)session.Timeout);
            
		//    }
		//    catch (SoapException ex)
		//    {
		//        if (ex.Code.Name.ToUpper() == "Server".ToUpper())
		//        {
		//            return -1;
		//        }
		//        else
		//        {
		//            CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11002);

		//            return -2;
		//        }
		//    }
		//    catch (Exception e)
		//    {
		//        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 11001);

		//        return -2;

		//    }


		//    return 0;


		//}
		public int LoginByPin(string pin)
		{

			try
			{
				MembershipManager jmgr = MembershipManager.Instance;
				UserMobileInfo res = jmgr.AuthenticateUserPin(new AuthAppUserModel
				{
					AppName = jmgr.GetApplicationName(),
					Pin = pin
				});
				UserSession session = new UserSession
				{
					FirstName = res.Firstname,
					LastName = res.Lastname,
					UserName = res.UserName,
					Pin = res.Pin,
					GatewayName = null,
					GroupID = res.IsAdmin ? UserTypes.Admin : UserTypes.User,
					UserID = (int)res.UserId,
					//SessionGuid = 
				};

				WSPieceScan.UserTypes userTypes;
				if (session.GroupID == UserTypes.Admin)
				{
					userTypes = CargoMatrix.Communication.WSPieceScan.UserTypes.Admin;
				}
				else
				{
					userTypes = CargoMatrix.Communication.WSPieceScan.UserTypes.User;
				}

				//OpenNETCF.WindowsCE.DateTimeHelper.LocalTime = session.LocalTime;

				WebServiceManager.SetUserID((int)session.UserID, session.UserName, userTypes);
				CargoMatrix.Communication.WebServiceManager.Instance().m_user.firstName = session.FirstName;
				CargoMatrix.Communication.WebServiceManager.Instance().m_user.lastName = session.LastName;
				if (session.Timeout == null)
					session.Timeout = 30;
				GuidManager.SetGuid(session.SessionGuid.ToString(), (int)session.Timeout);
			}
			catch (CommunicationException)
			{
				return -1;
			}
			catch (ServerSideExcetion ex)
			{
				CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11002);

				return -2;
			}
			catch (ConnectionRetryRejectedException ex)
			{
				return -2;
			}
			catch (Exception e)
			{
				CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 11003);

				return -2;

			}

			return 0; // user found
		}




        public long GetLocationIdByLocationBarcodeMCH(int userId, string gateway, string location)
        {
            try
            {

   
                //string currentGateway = string.Empty;
                //if (ScannerMCHServiceManager.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = ScannerMCHServiceManager.gateway;
                //}

                return ws.GetLocationIdByLocationBarcode(GetConnectionName(), location);

            }
            catch
            {
                return 0;
            }




        }


        public WSScannerMCHService.WarehouseLocation[] GetWarehouseLocationsMCH(int userId, string gateway, string connection, LocationType locationType)
        {

            //string currentConnectionName = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentConnectionName = connection;
            //}
            //else
            //{
            //    currentConnectionName = ScannerMCHServiceManager.connectionName;
            //}


            //string currentGateway = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentGateway = gateway;
            //}
            //else
            //{
            //    currentGateway = ScannerMCHServiceManager.gateway;
            //}


            WSScannerMCHService.WarehouseLocation[] locationitems = ws.GetWarehouseLocations(GetConnectionName(), locationType);


            return locationitems;
        }


        public WSScannerMCHService.MainMenuItem[] GetMainMenuMCH(int userId, string gateway, string connection)
        {

            //string currentConnectionName = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentConnectionName = connection;
            //}
            //else
            //{
            //    currentConnectionName = ScannerMCHServiceManager.connectionName;
            //}


            //string currentGateway = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentGateway = gateway;
            //}
            //else
            //{
            //    currentGateway = ScannerMCHServiceManager.gateway;
            //}


             int v = 0;
             WSScannerMCHService.MainMenuItem[] menuitems = ws.GetMainMenu("", userId, GetConnectionName(), GetConnectionName(), out v);
            

            return menuitems;
        }


        public bool AddTaskSnapshotReference(long taskId, string reference)
        {

            try
            { 
               ws.AddTaskSnapshotReference(taskId, reference);

            return true;
            }
            catch
            {
                return false;
            }
         
        }



        public bool ExecuteNonQuery(string sql)
        {

            try
            {
                return ws.ExecuteNonQuery(GetConnectionName(), sql);
            }
            catch
            {
                return false;
            }

        }


        public DataTable ExecuteQuery(string sql)
        {

            try
            {
                return ws.ExecuteQuery(GetConnectionName(), sql);
            }
            catch
            {
                return null;
            }

        }

        public AwbInfo GetAwbInfo(string shipment)
        {

            try
            {
                return ws.GetAwbInfo(GetConnectionName(), shipment);
            }
            catch
            {
                return null;
            }

        }


        //*********** 




        public DataTable Accept_GetTasks()
        {

            try
            {
                string  userid = CargoMatrix.Communication.WebServiceManager.UserID().ToString();
                string sql = "Exec dbo.GetCargoAcceptTasks '" + GetConnectionName() + "',null,null," + userid + ",0,0";
                DataTable dt = ExecuteQuery(sql);
                return dt;
            }
            catch
            {
                return null;
            }

        }
        

        
        public bool Accept_StartAcceptTask(long  taskId,int locationId,string truckNumber,long sealTypeId,string sealNumber)
        {

            try
            {
                string  userid = CargoMatrix.Communication.WebServiceManager.UserID().ToString();
                string sql = string.Format("Exec dbo.StartAcceptTask {0},{1},{2},'{3}',{4},'{5}'",userid,taskId,locationId,truckNumber,sealTypeId,sealNumber);
                ExecuteNonQuery(sql);
                return true;
            }
            catch
            {
                return false;
            }

        }



            public DataTable Accept_GetForkliftAWBsList(long taskId)
        {

            try
            {
                string  userid = CargoMatrix.Communication.WebServiceManager.UserID().ToString();
                string sql = "Exec dbo.GetForkliftAWBsList " + taskId  + "," + userid ;
                DataTable dt = ExecuteQuery(sql);
                return dt;
            }
            catch
            {
                return null;
            }

        }

            public int Accept_GetForkliftCounter(long taskId)
        {

            try
            {
                string  userid = CargoMatrix.Communication.WebServiceManager.UserID().ToString();
                string sql = "Exec dbo.GetForkliftCounter " + taskId + "," + userid;
                DataTable dt = ExecuteQuery(sql);
                return int.Parse(dt.Rows[0][0].ToString());
            }
            catch
            {
                return 0;
            }

        }




          public bool Accept_DropAllAcceptForkliftPieces(long taskId,int locationId)
        {

            try
            {
                string  userid = CargoMatrix.Communication.WebServiceManager.UserID().ToString();
                string sql = "Exec dbo.DropAllAcceptForkliftPieces " + userid  + "," + taskId + "," + locationId;
                  ExecuteNonQuery(sql);
                return  true;
            }
            catch
            {
                return false;
            }

        }



          public bool Accept_DropSelectedAcceptForkliftPieces(long taskId, int locationId, long forkliftDetailId)
          {
      
              try
              {
                  string userid = CargoMatrix.Communication.WebServiceManager.UserID().ToString();
                  string sql = "Exec dbo.DropSelectedAcceptForkliftPieces " + userid + "," + taskId + "," + locationId + "," + forkliftDetailId;
                  ExecuteNonQuery(sql);
                  return true;
              }
              catch
              {
                  return false;
              }

          }




          public bool Accept_FinalizeAccept(long taskId, string signature)
          {

              try
              {
                  string userid = CargoMatrix.Communication.WebServiceManager.UserID().ToString();
                  string sql = "Exec dbo.FinalizeAccept  " + taskId + "," + userid + ",'" + signature + "'";
                  ExecuteNonQuery(sql);
                  return true;
              }
              catch
              {
                  return false;
              }

          }



          public bool Accept_AddCargoAcceptAWBDims(long taskId,string awbIds,int uldTypeId,int prefixTypeId, string uldNumber,int pcs, double weight, bool isScreened)
          {

              int isScreenedNumber = 0;
              if(isScreened){isScreenedNumber = 1;}


              try
              {
                  string userid = CargoMatrix.Communication.WebServiceManager.UserID().ToString();
                 string sql =  string.Format("AddCargoAcceptAWBDims {0},{1},'{2}',{3},{4},'{5}',{6},{7},{8},{9},null,null,null,null",taskId,userid,awbIds,uldTypeId,prefixTypeId,uldNumber,pcs,weight,4,isScreenedNumber);
                  ExecuteNonQuery(sql);
                  return true;
              }
              catch
              {
                  return false;
              }


   


          }


    }
}
