﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements
{
    public enum MesurementsUnitEnum
    {
        Lb,
        In,
        Cm,
        Kg
    }
}
