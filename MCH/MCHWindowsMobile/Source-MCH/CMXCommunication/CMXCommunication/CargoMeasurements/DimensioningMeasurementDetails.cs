﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.ScannerUtilityWS;

namespace CargoMatrix.Communication.CargoMeasurements
{
    public class DimensioningMeasurementDetails
    {
        public MettlerMeasurements Measurement { get; set; }

        public CMXBarcode.BarcodeTypes BarcodeType { get; set; }
        public IULDType ULDType { get; set; }
        public PackageType PackageType { get; set; }
        public string PackageNumber { get; set; }
        public string FullReferenceNumber { get; set; }
        public string MasterbillNumber { get; set; }
        public int? PieceNumber { get; set; }
        
    }
}
