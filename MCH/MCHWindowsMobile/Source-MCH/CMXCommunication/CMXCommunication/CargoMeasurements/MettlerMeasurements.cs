﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.ScannerUtilityWS;

namespace CargoMatrix.Communication.CargoMeasurements
{
    public class MettlerMeasurements
    {
        #region Fields
        double weight;
        double length;
        double height;
        double width;
        MetricSystemTypeEnum metricSystemType = MettlerMeasurements.DefaultMetricSystem;
        MeasurementsOriginEnum origin = MeasurementsOriginEnum.Device;

        #endregion

        #region Properties
        /// <summary>
        /// Gets or Sets package weight
        /// </summary>
        public double Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        /// <summary>
        /// Gets or Sets package Length
        /// </summary>
        public double Length
        {
            get { return length; }
            set { length = value; }
        }

        /// <summary>
        /// Gets or Sets package height
        /// </summary>
        public double Height
        {
            get { return height; }
            set { height = value; }
        }

        /// <summary>
        /// Gets or Sets package width
        /// </summary>
        public double Width
        {
            get { return width; }
            set { width = value; }
        }

        public string PackageID { get; set; }
        
       
        /// <summary>
        /// Gets metric system type
        /// </summary>
        public MetricSystemTypeEnum MetricSystemType
        {
            get
            {
                return metricSystemType;
            }
        }

        public MeasurementsOriginEnum Origin
        {
            get { return origin; }
            set { this.origin = value; }
        }

        public static MetricSystemTypeEnum DefaultMetricSystem
        {
            get
            {
                return MetricSystemTypeEnum.Imperial;
            }
        }

        #endregion

        protected MettlerMeasurements() { }

        public static MettlerMeasurements CreateFromSystemType(MetricSystemTypeEnum metricSystemType, MeasurementsOriginEnum origin)
        {
            MettlerMeasurements result = new MettlerMeasurements();
            result.metricSystemType = metricSystemType;
            result.origin = origin;

            return result;
        }

        public MettlerMeasurements ConvertToMetricSystem(MetricSystemTypeEnum metricSystemType)
        {
            MettlerMeasurements result = new MettlerMeasurements();
            result.metricSystemType = metricSystemType;
            result.PackageID = this.PackageID;
            result.origin = this.origin;

            double weightCoefficient = 0.45359237d;
            double lengthCoefficient = 2.54d;

            if (this.metricSystemType == metricSystemType)
            {
                weightCoefficient = 1.0d;
                lengthCoefficient = 1.0d;
            }
            else
            {
                if (this.metricSystemType == MetricSystemTypeEnum.Metric &&  // from Metric to Imperial
                    metricSystemType == MetricSystemTypeEnum.Imperial)
                {
                    weightCoefficient = 1.0d / weightCoefficient;
                    lengthCoefficient = 1.0d / lengthCoefficient;
                }
                else
                {
                    if (this.metricSystemType == MetricSystemTypeEnum.Imperial && // from Imperial to Metric
                        metricSystemType == MetricSystemTypeEnum.Metric)
                    {
                        // do nothing ...
                    }
                    else
                    {
                        throw new ArgumentException("Unknown metric system types");
                    }
                }
            }

            result.weight = this.weight * weightCoefficient;
            result.width = this.width * lengthCoefficient;
            result.height = this.height * lengthCoefficient;
            result.length = this.length * lengthCoefficient;

            return result;
        }


        /// <summary>
        /// Parses string measurements
        /// </summary>
        /// <param name="data">Measurements to parse.</param>
        /// <param name="dataSeparator">Values separator</param>
        /// <returns>MettlerMeasurements</returns>
        public static MettlerMeasurements Parse(MetricSystemTypeEnum metricSystemType, string data, char separator)
        {
            if (data == null) throw new ArgumentNullException("data");

            string[] values = data.Split(new char[] { separator });

            double weight = 0.0;
            double length = 0.0;
            double width = 0.0;
            double height = 0.0;
            string packageID = null;

            try
            {
                weight = double.Parse(values[0]);
                length = double.Parse(values[1]);
                width = double.Parse(values[2]);
                height = double.Parse(values[3]);
            }
            catch
            {
                throw new FormatException();
            }

            try
            {
                if (values.Length == 5)
                {
                    packageID = values[4];
                }
            }
            catch { }

            MettlerMeasurements result = new MettlerMeasurements();

            result.metricSystemType = metricSystemType;
            result.weight = weight;
            result.length = length;
            result.width = width;
            result.height = height;
            result.PackageID = packageID;
            result.origin = MeasurementsOriginEnum.Device;

            return result;
        }

        /// <summary>
        /// Parses string measurements with default values comma separator
        /// </summary>
        /// <param name="data">Measurements to parse. Format : '{0},{1},{2},{3},{4}'</param>
        /// <returns>MettlerMeasurements</returns>
        public static MettlerMeasurements Parse(MetricSystemTypeEnum metricSystemType, string data)
        {
            return MettlerMeasurements.Parse(metricSystemType, data, ',');
        }

        /// <summary>
        /// Overridden.
        /// </summary>
        /// <returns>Weight : {0},  Length : {1},  Width : {2},  Height : {3}, PID : {4}</returns>
        public override string ToString()
        {
            string weightMetrics = this.metricSystemType == MetricSystemTypeEnum.Imperial ? "lb" : "kg";
            string legthMetrics = this.metricSystemType == MetricSystemTypeEnum.Imperial ? "in" : "cm";

            StringBuilder strBuilder = new StringBuilder();
            strBuilder.AppendFormat("Weight : {0} {1},", this.weight, weightMetrics);
            strBuilder.AppendFormat("Length : {0} {1},", this.length, legthMetrics);
            strBuilder.AppendFormat("Width : {0} {1},", this.width, legthMetrics);
            strBuilder.AppendFormat("Height : {0} {1},", this.height, legthMetrics);
            strBuilder.AppendFormat("PID : {0}", this.PackageID);

            return strBuilder.ToString();
        }

        public MesurementsUnitEnum LengthMesurementsUnit
        {
            get
            {
                return GetLengthMeasurementUnit(this.metricSystemType);
            }
        }

        public MesurementsUnitEnum WeightMesurementsUnit
        {
            get
            {
                return GetWeightMeasurementUnit(this.metricSystemType);
            }
        }

        public static MesurementsUnitEnum GetWeightMeasurementUnit(MetricSystemTypeEnum metricSystemType)
        {
            switch (metricSystemType)
            {
                case MetricSystemTypeEnum.Imperial: return MesurementsUnitEnum.Lb;

                case MetricSystemTypeEnum.Metric: return MesurementsUnitEnum.Kg;

                default: return MesurementsUnitEnum.Lb;
            }
        }

        public static MesurementsUnitEnum GetLengthMeasurementUnit(MetricSystemTypeEnum metricSystemType)
        {
            switch (metricSystemType)
            {
                case MetricSystemTypeEnum.Imperial: return MesurementsUnitEnum.In;

                case MetricSystemTypeEnum.Metric: return MesurementsUnitEnum.Cm;

                default: return MesurementsUnitEnum.In;
            }
        }
    }
}
