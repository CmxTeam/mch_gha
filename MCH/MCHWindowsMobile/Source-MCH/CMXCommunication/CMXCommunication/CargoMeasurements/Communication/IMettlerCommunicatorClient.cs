﻿using System;
using CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions;


namespace CargoMatrix.Communication.CargoMeasurements.Communication
{
    public interface IMettlerCommunicatorClient
    {
        #region Properties
        int BytesToRead { get; set; }
        bool Connected { get; }
        string MettlerDeviceIP { get; }
        string ServerIP { get; }
        int ServerPort { get; } 
        #endregion

        #region Events
        event EventHandler OnAliveRequestSent;
        event EventHandler OnConnected;
        event EventHandler<ExceptionEventArgs> OnError;
        event EventHandler OnMettlerIPSent;
        event EventHandler<ResponseReceivedEventArgs> OnResponseReceived; 
        #endregion

        #region Methods
        void Connect();
        void ConnectAsync();
        void Disconnect();
        void SendAliveCommand();
        void SendAliveCommandAsync();
        void SendMettlerIP();
        void SendMettlerIPAsync(); 
        #endregion
    }
}
