﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;


namespace CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions
{
    public class ExceptionEventArgs : EventArgs
    {
        CommunicationException exception;

        /// <summary>
        /// Gets or Sets the exception fired by the listener
        /// </summary>
        public CommunicationException Exception
        {
            get { return exception; }
            set { exception = value; }
        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="exception"></param>
        public ExceptionEventArgs(CommunicationException exception)
        {
            this.exception = exception;
        }
    }
}
