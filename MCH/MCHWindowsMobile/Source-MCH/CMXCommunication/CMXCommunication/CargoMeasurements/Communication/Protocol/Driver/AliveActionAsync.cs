﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver
{
    internal class AliveActionAsync : CommunicatorAction
    {
        public AliveActionAsync(IMettlerCommunicatorClient communicationClient)
            : base(communicationClient)
        { }

        public override CommunicationStateEnum Perform()
        {
            this.CommunicationClient.SendAliveCommandAsync();

            return CommunicationStateEnum.Connecting;
        }
    }
}
