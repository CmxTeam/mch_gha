﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;


namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol
{
    static class CommandsParser
    {
        private static string commandFormat = "{0}:{1}:{2}";

        public static string FormatClientCommand(CommandValues command)
        {
            return string.Format(commandFormat, command.CommunicatorType.ToString().ToUpper(),
                                                command.CommnadType.ToString().ToUpper(),
                                                command.Data);
        }

        public static CommandValues ParseServerCommand(string command)
        {
            if (string.IsNullOrEmpty(command) == true) throw new ArgumentException("command");

            string[] values = command.Split(new char[] { ':' });

            if (values.Length != 3) throw new ArgumentException("command");

            CommandValues result = new CommandValues();

            result.Data = values[2];
            result.CommnadType = (CommandTypesEnum)Enum.Parse(typeof(CommandTypesEnum), values[1], true);
            result.CommunicatorType = (CommunicatorTypesEnum)Enum.Parse(typeof(CommunicatorTypesEnum), values[0], true);

            return result;
        }

        public static byte[] ToByteArray(string str)
        {
            return System.Text.Encoding.ASCII.GetBytes(str);
        }

        public static string ToString(byte[] array)
        {
            return System.Text.Encoding.ASCII.GetString(array, 0, array.Length);
        }
    
    }
}
