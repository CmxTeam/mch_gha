﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver
{
    public abstract class CommunicatorAction
    {
        protected IMettlerCommunicatorClient CommunicationClient { get; private set; }

        public CommunicatorAction Next { get; set; }

        public CommunicatorAction(IMettlerCommunicatorClient communicationClient)
        {
            this.CommunicationClient = communicationClient;
        }

        public abstract CommunicationStateEnum Perform();
    }
}
