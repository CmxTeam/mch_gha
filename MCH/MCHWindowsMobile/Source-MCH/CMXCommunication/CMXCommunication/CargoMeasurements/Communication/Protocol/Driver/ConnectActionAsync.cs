﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver
{
    internal class ConnectActionAsync : CommunicatorAction
    {
        public ConnectActionAsync(IMettlerCommunicatorClient communicationClient)
            : base(communicationClient)
        { }

        public override CommunicationStateEnum Perform()
        {
            this.CommunicationClient.ConnectAsync();

            return CommunicationStateEnum.Connecting;
        }
    }
}
