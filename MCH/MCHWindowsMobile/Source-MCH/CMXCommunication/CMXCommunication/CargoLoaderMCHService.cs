﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication
{
    public class CargoLoaderMCHServiceObj : CargoMatrix.Communication.WSCargoLoaderMCHService.CargoLoaderMCHService
    {
        //string path = "http://10.0.0.235/ScannerServices/";
        string asmxName = "CargoLoaderMCHService.asmx";
        public CargoLoaderMCHServiceObj()
        {
            //Do not Update from here
            //if (path == string.Empty)
            //{
                Url = Settings.Instance.AppURLPath + asmxName;
            //}
            //else
            //{
            //    Url = path + asmxName;
            //}
     

            //string currentUrl = Url;

        }
        public string URL
        {
            get { return Url; }
        }

    }

    public class CargoLoaderMCHService
    {
 
        //private static string connectionName = "BOS";

        private static CargoLoaderMCHService instance;
        CargoLoaderMCHServiceObj ws;
        private CargoLoaderMCHService()
        {
            ws = new CargoLoaderMCHServiceObj();
            
        }
        public static CargoLoaderMCHService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CargoLoaderMCHService();
                }

                return instance;
            }

        }

        public string GetConnectionName()
        {
          
            //if (CargoLoaderMCHService.connectionName == string.Empty)
            //{
                return Settings.Instance.ConnectionName;
            //}
            //else
            //{
            //    return CargoLoaderMCHService.connectionName;
            //}
     
        }

        public CargoLoaderFlight[] GetCargoLoaderFlights(TaskStatuses status,string carrierNo,string destination,string flightNo,ReceiverSortFields sort)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetCargoLoaderFlights(GetConnectionName(),(int)CargoMatrix.Communication.WebServiceManager.UserID(), status, carrierNo, destination, flightNo, sort);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }


        public CargoLoaderFlight GetCargoLoaderFlight(long manifestId, long taskId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetCargoLoaderFlight(manifestId, taskId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }

        public CargoLoaderFlightLeg[] GetCargoLoaderFlightLegs(long manifestId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetCargoLoaderFlightLegs(manifestId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }

  
        public CargoLoaderFlightLegUld[] AvailableULDsForBuild(long flightId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.AvailableULDsForBuild(flightId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }


        public CargoLoaderFlightLegUld[] GetCargoLoaderFlightUlds(long flightId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetCargoLoaderFlightUlds( flightId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }

        public CargoLoaderFlightLeg GetCargoLoaderFlightLeg(long legId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetCargoLoaderFlightLeg(legId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }


        public bool UpdateFlightLegLocation(long legId, long locationId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                 TransactionStatus t =  ws.UpdateFlightLegLocation(legId, locationId, CargoMatrix.Communication.WebServiceManager.UserID());
                 return t.Status;
            
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }

        public Alert[] GetFlightAlerts(long manifestId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetFlightAlerts(manifestId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }

        public Alert[] GetLegAlerts(long legId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetLegAlerts(legId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }


        //public bool DeleteCargoLoaderFlightLegUld(long uldlegId)
        //{
        //    //CargoMatrix.Communication.WSCargoLoaderMCHService.
        //    try
        //    {
        //        TransactionStatus t = ws.DeleteCargoLoaderFlightLegUld(uldlegId);
        //        return t.Status;
        //    }
        //    catch (Exception e)
        //    {

        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
        //        return false;
        //    }
        //}


        public bool UpdateUldWeight(long uldId, decimal value)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.UpdateUldWeight(uldId, value);
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }

        public bool UpdateUldTareWeight(long uldId, decimal value)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.UpdateUldTareWeight(uldId, value);
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }


        public bool FinalizeCargoLoader(long taskId, long flightId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.FinalizeCargoLoader(taskId, flightId, CargoMatrix.Communication.WebServiceManager.UserID());
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }

   
        public bool UpdateFlightLegUldLocation(long uldId, int locationId, long taskId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.UpdateFlightLegUldLocation(uldId, locationId, taskId, CargoMatrix.Communication.WebServiceManager.UserID());
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }



        public bool UpdateCargoLoaderFlightLegUld(long uldlegId, string uldSerialNo, string prefix, long taskId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.UpdateCargoLoaderFlightLegUld(uldlegId,uldSerialNo, prefix ,CargoMatrix.Communication.WebServiceManager.UserID(), taskId );
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }

        public bool AddCargoLoaderFlightLegUld(long uldTypeId,string prefix, string serial ,long taskId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.AddCargoLoaderFlightLegULD(uldTypeId, prefix, serial, taskId, CargoMatrix.Communication.WebServiceManager.UserID());
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }

        public bool DeleteCargoLoaderFlightLegUld(long uldLegId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.DeleteCargoLoaderFlightLegUld(uldLegId, CargoMatrix.Communication.WebServiceManager.UserID());
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }


        public  UldType[] GetUldTypeList()
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetUldTypes();
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }


        public string[] GetUldPrefixList()
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetUldPrefix();
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }


        public CargoLoaderFlightLegUld[] GetFlightDestinationBUPsNotAttached(long legId, string uldType)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetFlightDestinationBUPs(legId, FlightDestinationTypes.NotAttached, uldType);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }

        //public CargoLoaderShipment[] GetCargoLoaderShipments(long legId,long flightId)
        //{
        //    //CargoMatrix.Communication.WSCargoLoaderMCHService.
        //    try
        //    {
        //        return ws.GetCargoLoaderShipments(legId, flightId);
        //    }
        //    catch (Exception e)
        //    {

        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
        //        return null;
        //    }
        //}


        // , DropPiecesIntoForklift


        public bool DropPiecesIntoForklift(long taskId, long awbId, int pieces)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {

                TransactionStatus t =ws.DropPiecesIntoForklift(CargoMatrix.Communication.WebServiceManager.UserID(), taskId, awbId, pieces);
                return t.Status;
 
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }

        public AwbModel[] GetLoadingPlan(long fligthId, long taskId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetLoadingPlan(fligthId, taskId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }

        public  UldViewItem[] GetUldView(long fligthId, long uldId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetUldView(fligthId,  uldId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }

        public AwbModel GetLoadingPlanForAwb(long awbId, long taskId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
           
            try
            {
                return ws.GetLoadingPlanForAwb(awbId, taskId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }

        }


        public ValidatedShipment ValidateLoaderShipment(long fligthId, long taskId, string shipment)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.ValidateLoaderShipment(fligthId, taskId, shipment);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }



        public bool RemovePiecesFromForklift(long detailId,int pieces)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {

                TransactionStatus t = ws.RemovePiecesFromForklift(detailId, pieces);
                return t.Status;

            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }




        public LoadForkliftViewItem[] GetForkliftView(long taskId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetForkliftView(taskId, CargoMatrix.Communication.WebServiceManager.UserID());
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return null;
            }
        }


        public int GetForkLiftCount(long taskId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                return ws.GetForkLiftCount(CargoMatrix.Communication.WebServiceManager.UserID(), taskId);
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return 0;
            }
        }

 

        public bool DropAllForkliftPiecesIntoLocation(long taskId, int locationId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.DropAllForkliftPiecesIntoLocation(taskId, CargoMatrix.Communication.WebServiceManager.UserID(), locationId);
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }


        public bool DropAllForkliftPiecesIntoULD(long taskId, long uldId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.DropAllForkliftPiecesIntoULD(taskId, CargoMatrix.Communication.WebServiceManager.UserID(), uldId);
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }

        public bool DropSelectedItemsIntoULD(long taskId, long uldId, long[] detailIds)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.DropSelectedItemsIntoULD(taskId, CargoMatrix.Communication.WebServiceManager.UserID(), uldId, detailIds);
                return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }
        
       public bool DropSelectedItemsIntoLocation(long taskId,int locationId, long[] detailIds )
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.DropSelectedItemsIntoLocation(taskId, CargoMatrix.Communication.WebServiceManager.UserID(), locationId, detailIds);
                    return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }


       public bool RemoveAwbFromUld(long detailId, long uldId, long taskId, int locationId)
        {
            //CargoMatrix.Communication.WSCargoLoaderMCHService.
            try
            {
                TransactionStatus t = ws.RemoveAwbFromUld(detailId, uldId, taskId, CargoMatrix.Communication.WebServiceManager.UserID(), locationId);
                    return t.Status;
            }
            catch (Exception e)
            {

                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
                return false;
            }
        }



       public bool UpdateFlightBuildLocation(long flightId,   long taskId, int locationId)
       {
           //CargoMatrix.Communication.WSCargoLoaderMCHService.
           try
           {
               TransactionStatus t = ws.UpdateFlightBuildLocation(flightId, CargoMatrix.Communication.WebServiceManager.UserID(),taskId, locationId);
               return t.Status;
           }
           catch (Exception e)
           {

               CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 10000);
               return false;
           }
       }

        

    }
}
