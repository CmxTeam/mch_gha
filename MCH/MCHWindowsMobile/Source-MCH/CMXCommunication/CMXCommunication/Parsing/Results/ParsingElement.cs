﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Parsing.Results
{
    public class ParsingElement
    {
        public string Name { get; set; }
        public string Regex { get; set; }
        public int Order { get; set; }
        public string Value { get; set; }

        public ParsingElement() { }
    }
}
