﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Parsing.Results
{
    public class ParsingPattern
    {
        ResultTypeEnum resultType;
        List<PatternValue> values;
        bool isLocation;

        public ResultTypeEnum ResultType
        {
            get { return resultType; }
        }

        public IEnumerable<PatternValue> Values
        {
            get { return values; }
        }

        public bool IsLocation
        {
            get { return isLocation; }
        }

        public ParsingPattern(ResultTypeEnum resultType, bool isLocation, List<PatternValue> values)
        {
            this.resultType = resultType;
            this.isLocation = isLocation;
            this.values = values;
        }

        public class PatternValue
        {
            string regex;
            ParsingElement[] elements;
            
            public ParsingElement[] Elements
            {
                get { return elements; }
            }

            public string Regex
            {
                get { return regex; }
            }

            public PatternValue(string regex, ParsingElement[] elements)
            {
                this.regex = regex;
                this.elements = elements;
            }
        }
    }
}
