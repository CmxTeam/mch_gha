﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication.InventoryCom
{
    public class InventoryHouseBilItem : IHouseBillItem
    {
        InventoryWS.HouseBillItem wsInventoryHousebillItem;

        public InventoryHouseBilItem(InventoryWS.HouseBillItem wsInventoryHousebillItem)
        {
            this.wsInventoryHousebillItem = wsInventoryHousebillItem;
        }


        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get 
            {
                return this.wsInventoryHousebillItem.HousebillNumber;     
            }
        }

        public int HousebillId
        {
            get 
            {
                return this.wsInventoryHousebillItem.HousebillId;     
            }
        }

        public string Origin
        {
            get 
            {
                return this.wsInventoryHousebillItem.Origin;
            }
        }

        public string Destination
        {
            get 
            {
                return this.wsInventoryHousebillItem.Description;
            }
        }

        public int TotalPieces
        {
            get 
            {
                return this.wsInventoryHousebillItem.TotalPieces;     
            }
        }

        public int ScannedPieces
        {
            get 
            {
                return this.wsInventoryHousebillItem.ScannedPieces;
            }
        }

        public int Slac
        {
            get 
            {
                return this.wsInventoryHousebillItem.TotalSlac;
            }
        }

        public int Weight
        {
            get 
            {
                return (int)this.wsInventoryHousebillItem.Weight;
            }
        }

        public string Location
        {
            get 
            {
                return this.wsInventoryHousebillItem.LastLocation;     
            }
        }

        public int Flags
        {
            get 
            {
                return this.wsInventoryHousebillItem.Flags;
            }
        }

        public ScanModes ScanMode
        {
            get 
            {
                switch (this.wsInventoryHousebillItem.ScanMode)
                {
                    case CargoMatrix.Communication.InventoryWS.ScanModes.Count: return ScanModes.Count;

                    case CargoMatrix.Communication.InventoryWS.ScanModes.NA: return ScanModes.NA;

                    case CargoMatrix.Communication.InventoryWS.ScanModes.Piece: return ScanModes.Piece;

                    case CargoMatrix.Communication.InventoryWS.ScanModes.Slac: return ScanModes.Slac;

                    default: throw new NotImplementedException();
                }
            }
        }

        public HouseBillStatuses status
        {
            get 
            {
                switch (this.wsInventoryHousebillItem.Status)
                {
                    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.All: return HouseBillStatuses.All;

                    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Completed: return HouseBillStatuses.Completed;

                    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.InProgress: return HouseBillStatuses.InProgress;

                    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Open: return HouseBillStatuses.Open;

                    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Pending: return HouseBillStatuses.Pending;

                    default: throw new NotImplementedException();
                }
            }
        }

        public RemoveModes RemoveMode
        {
            get 
            {
                switch (this.wsInventoryHousebillItem.RemoveMode)
                {
                    case CargoMatrix.Communication.InventoryWS.RemoveModes.All: return RemoveModes.All;

                    case CargoMatrix.Communication.InventoryWS.RemoveModes.NA: return RemoveModes.NA;

                    case CargoMatrix.Communication.InventoryWS.RemoveModes.Shortage: return RemoveModes.Shortage;

                    case CargoMatrix.Communication.InventoryWS.RemoveModes.TakeOff: return RemoveModes.TakeOff;

                    default: throw new NotImplementedException();
    
                }
            }
        }

        #endregion
    }
}
