﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication.Utility
{
    public class UtilityShipmentCondition : IShipmentCondition
    {
        IShipmentConditionType[] conditions;
        ScannerUtilityWS.ShipmentCondition wsShipmentCondition;

        public UtilityShipmentCondition(ScannerUtilityWS.ShipmentCondition wsShipmentCondition)
        {
            this.wsShipmentCondition = wsShipmentCondition;
        }


        #region IShipmentCondition Members

        public IShipmentConditionType[] Conditions
        {
            get 
            {
                var cnds = this.conditions;

                if (cnds == null)
                {
                    this.conditions = (
                                            from item in this.wsShipmentCondition.Conditions
                                            select new UtilityShipmentConditionType(item)
                                       ).ToArray();
                }


                return this.conditions;
            }
        }

        public int Pieces
        {
            get 
            {
                return this.wsShipmentCondition.Pieces;
            }
        }

        public int PieceId
        {
            get 
            {
                return this.wsShipmentCondition.PieceId;
            }
        }

        #endregion
    }
}
