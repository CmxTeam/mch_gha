﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.ExceptionManager
{
    public static class Logger
    {
        private static readonly object logLock = new object();

        public static void Log(Exception ex, int locationCode)
        {
            lock (logLock)
            {
                using (System.IO.FileStream fs = new System.IO.FileStream("cargomatrix_exception.txt", System.IO.FileMode.Append))
                {
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fs))
                    {
                        sw.WriteLine("-----------------------------");
                        sw.WriteLine("Location: " + locationCode);
                        sw.WriteLine(DateTime.Now.ToString());
                        sw.Write(ex);
                        sw.WriteLine(" ");

                        sw.Flush();
                        fs.Flush();
                    }
                }
            }
    
        }
    }
}
