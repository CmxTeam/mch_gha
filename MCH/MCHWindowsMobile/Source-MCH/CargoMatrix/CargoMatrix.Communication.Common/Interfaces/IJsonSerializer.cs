﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.Common.Interfaces
{
	public interface IJsonSerializer
	{
		T DeserializeObject<T>(string value);
		string SerializeObject(object value);
	}
}
