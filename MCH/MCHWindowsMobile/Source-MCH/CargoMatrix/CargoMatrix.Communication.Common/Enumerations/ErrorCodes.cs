﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.Enumerations
{
	public enum ErrorCodes : int
	{
		Not_Unique_Username = 101,
		User_Not_Found = 102,
		Invalid_User_Settings = 103,
		Invalid_User = 104,
		Could_Not_Create_User = 105,
		Username_Required = 106,

		Application_Not_Found = 201,

		Profile_Field_Not_Found = 301,

		Incorrect_Passord_Answer = 401,
		Incorrect_Passord = 402,
		Not_Unique_Mobile_Pin = 403,
		Invalid_Password = 404,
		Missmatch_Password = 405,
		Password_Required = 406,

		Invalid_Argument = 501,


		Menu_Not_Found = 601,


		Company_Not_Found = 701,
		Company_Required = 702,
		Company_Is_In_Use = 703,


		Role_Not_Found = 801,
		Role_Has_Users = 802,
		Not_Unique_Role = 803,

		Not_Unique_Account = 901

	}
}
