﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.Enumerations
{
	public enum MobileAuthMethod
	{
		None = 0,
		Pin = 1,
		Password = 2,
		PinAndPassword = 3,
	}
}
