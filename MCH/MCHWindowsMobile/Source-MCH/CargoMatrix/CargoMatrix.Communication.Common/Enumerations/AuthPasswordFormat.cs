﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.Enumerations
{
	public enum AuthPasswordFormat
	{
		PlainText = 0,
		HashText = 1,
		EncryptedText = 2
	}
}
