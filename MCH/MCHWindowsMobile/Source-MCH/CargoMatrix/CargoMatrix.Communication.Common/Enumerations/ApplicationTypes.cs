﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.Enumerations
{
	public enum ApplicationTypes
	{
		Web = 1,
		WindowsMobile = 2,
		iOS = 3,
		Android = 4,
		WindowsPhone = 5,
		Desktop = 6,
		WebService = 7,
		WindowsService = 8
	}
}
