﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Method, Inherited=false, AllowMultiple=false)]
    public class ServiceMethodAttribute: Attribute
    {
        public const string GET = "GET";
        public const string POST = "POST";

        /// <summary>
        /// Initializes a new instance
        /// </summary>
        /// <param name="path">Relative path of the web method</param>
        /// <param name="method">GET or POST method</param>
        public ServiceMethodAttribute(string path, string method): base()
        {
			Path = path;
			Method = method;
        }

        /// <summary>
        /// Relative path of the web method
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// GET or POST method
        /// </summary>
        public string Method { get; private set; }
    }
}
