﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.Enumerations;

namespace CargoMatrix.Communication.Common.Exceptions
{
	public class ServerSideExcetion: Exception
	{
		public ServerSideExcetion() : base() { }
		public ServerSideExcetion(string message) : base(message) { }
		public ServerSideExcetion(string message, Exception innerException) : base(message, innerException) { }

		public ErrorCodes? ErrorCode { get; set; }
	}
}
