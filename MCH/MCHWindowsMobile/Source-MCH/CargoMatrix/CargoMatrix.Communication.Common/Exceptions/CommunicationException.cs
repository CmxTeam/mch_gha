﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace CargoMatrix.Communication.Common.Exceptions
{
	public class CommunicationException: Exception
	{
		public CommunicationException() : base() { }
		public CommunicationException(string message) : base(message) { }
		public CommunicationException(string message, Exception innerException) : base(message, innerException) { }

		public HttpStatusCode Status { get; set; }
	}
}
