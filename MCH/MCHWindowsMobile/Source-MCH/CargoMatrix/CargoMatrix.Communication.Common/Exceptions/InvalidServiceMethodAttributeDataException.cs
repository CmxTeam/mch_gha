﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.Exceptions
{
    public class InvalidServiceMethodAttributeDataException: Exception
    {
        public InvalidServiceMethodAttributeDataException() : base() { }
        public InvalidServiceMethodAttributeDataException(string message) : base(message) { }
        public InvalidServiceMethodAttributeDataException(string message, Exception innerException) : base(message, innerException) { }
    }
}
