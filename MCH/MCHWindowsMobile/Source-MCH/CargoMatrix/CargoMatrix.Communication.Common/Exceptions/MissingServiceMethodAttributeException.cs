﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.Exceptions
{
    public class MissingServiceMethodAttributeException: Exception
    {
        public MissingServiceMethodAttributeException() : base() { }
        public MissingServiceMethodAttributeException(string message) : base(message) { }
        public MissingServiceMethodAttributeException(string message, Exception innerException) : base(message, innerException) { }
    }
}
