﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.Exceptions
{
    public class ConnectionRetryRejectedException : Exception
    {
        public ConnectionRetryRejectedException() : base() { }
        public ConnectionRetryRejectedException(string message) : base(message) { }
        public ConnectionRetryRejectedException(string message, Exception innerException) : base(message, innerException) { }
    }
}
