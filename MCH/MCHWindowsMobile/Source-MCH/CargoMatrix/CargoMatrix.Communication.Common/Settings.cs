﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Reflection;
using System.IO;

namespace CargoMatrix.Communication.Common
{
    [XmlRoot("CMXConfig", IsNullable = false)]
    public class CMXConfig
    {
		public string connectionName { get; set; }
		public string gateway { get; set; }
		public string ScannerWSURL { get; set; }
		public string UpdaterWSURL { get; set; }
		public string ScannerRestURL { get; set; }
		public string AppName { get; set; }
		public string CompanyName { get; set; }
        //public int camera;
    }

    public class Settings
    {
		string Url = string.Empty, urlPath = string.Empty, connectionName = string.Empty, gateway = string.Empty, AppUrl = string.Empty, appUrlPath = string.Empty, restPath = string.Empty, appName = string.Empty, companyName = string.Empty;
        //int camera;
        //public int CameraType
        //{ get { return camera; } }
        private static Settings instance;
        private Settings()
        {
            string ConfigFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\CMXScannerConfig.xml";
            //string ConfigFilename = @"CMXScannerConfig.xml";
            //instance = new Settings();
             System.Xml.XmlDocument xmlFile = new System.Xml.XmlDocument();
             xmlFile.Load(ConfigFile);
             System.Xml.XmlNode root = xmlFile.SelectSingleNode("CMXConfig");
             if (root != null)
             {
				 System.Xml.XmlNode node = root.SelectSingleNode("WSURL");
                 urlPath = node != null? node.InnerText: string.Empty;
				 node = root.SelectSingleNode("APPWSURL");
				 appUrlPath = node != null ? node.InnerText : string.Empty;
				 node = root.SelectSingleNode("connectionName");
				 connectionName = node != null ? node.InnerText : string.Empty;
                 //camera = Convert.ToInt32(root.SelectSingleNode("camera").InnerText);
				 node = root.SelectSingleNode("gateway");
				 gateway = node != null ? node.InnerText : string.Empty;
				 node = root.SelectSingleNode("RestURL");
				 restPath = node != null ? node.InnerText : string.Empty;
				 node = root.SelectSingleNode("APPLICATIONNAME");
				 appName = node != null ? node.InnerText : string.Empty;
				 node = root.SelectSingleNode("COMPANYNAME");
				 companyName = node != null ? node.InnerText : string.Empty;
                 if (!string.IsNullOrEmpty(urlPath))
                 {
                     if (urlPath[urlPath.Length - 1] == '/')
                     {
                         Url = urlPath;// +"CMXScannerWebService.asmx";

                     }
                     else
                     {
                         Url = urlPath + @"/";
                     
                     }
                 }


                 if (!string.IsNullOrEmpty(appUrlPath))
                 {
                     if (appUrlPath[appUrlPath.Length - 1] == '/')
                     {
                         AppUrl = appUrlPath;// +"CMXScannerWebService.asmx";

                     }
                     else
                     {
                         AppUrl = appUrlPath + @"/";

                     }
                 }


             
             }

        }
        public static Settings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Settings();
                }

                return instance;
            }

        }
       
        public string URLPath
        {
            get { return urlPath; }
        }

        public string AppURLPath
        {
            get { return AppUrl; }
        }

        public string ConnectionName
        {
            get { return connectionName; }
        }
        public string Gateway
        {
            get { return gateway; }
        }

        public string RestURLPath
        {
            get { return restPath; }
        }

		public string AppName
        {
			get { return appName; }
        }

		public string CompanyName
        {
			get { return companyName; }
        }
    }
}
