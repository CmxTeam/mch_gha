﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.Enumerations;

namespace CargoMatrix.Communication.Common.Data
{
	public class CommunicationTransaction
	{
		public bool Status { get; set; }
		public string Content { get; set; }
		public string Error { get; set; }
		public string Url { get; set; }
		public ErrorCodes? ErrorCode { get; set; }
	}
}
