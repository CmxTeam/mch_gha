﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.Common.Data
{
	public interface ITransactional
	{
		CommunicationTransaction Transaction { get; set; }
	}
}
