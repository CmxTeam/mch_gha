﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.Data;
using CargoMatrix.Communication.Common.Enumerations;

namespace CargoMatrix.Communication.Common.DTO.Membership
{
	public class CompanySecurityConfigDTO 
	{
		public int? PassPatternId { get; set; }
		public int? UserNamePatternId { get; set; }
		public int? PassExpiration { get; set; }
		public int ForgotPassLinkExpiration { get; set; }
		public int? MaxPassAttempts { get; set; }
		public int? MaxPassAnswerAttempts { get; set; }
		public int? SessionTimeout { get; set; }
		public bool ResetPassEnabled { get; set; }
		public bool RetrievePassEnabled { get; set; }
		public bool PassSecurityQuestionRequires { get; set; }
		public AuthPasswordFormat PassFormatId { get; set; }
		public MobileAuthMethod MobileAuthMethod { get; set; }
		public bool AllowRememberMe { get; set; }		
	}
}
