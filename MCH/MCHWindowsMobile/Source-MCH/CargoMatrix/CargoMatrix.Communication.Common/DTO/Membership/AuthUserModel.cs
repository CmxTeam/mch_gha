﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.DTO.Membership
{
	public class AuthUserModel
	{
		public string AppName { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
