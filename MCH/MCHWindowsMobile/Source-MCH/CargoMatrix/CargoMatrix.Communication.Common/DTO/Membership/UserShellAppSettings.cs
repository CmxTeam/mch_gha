﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.DTO.Membership
{
	public class UserShellAppSettings
	{
		public long AppId { get; set; }
		public long AppRoleId { get; set; }
		public string AppAccountIds { get; set; }
		public long? DefaultAccountId { get; set; }
		public string AppSettingIds { get; set; }
	}
}
