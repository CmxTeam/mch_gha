﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.Data;

namespace CargoMatrix.Communication.Common.DTO.Membership
{
	public class CompanySecurityConfigDTOPackage : ITransactional, IDataContainer<CompanySecurityConfigDTO>
	{
		#region ITransactional Members
		public CommunicationTransaction Transaction { get; set; }
		#endregion

		#region IDataContainer<CompanySecurityConfigDTO> Members

		public CompanySecurityConfigDTO Data { get; set; }

		#endregion
	}
}
