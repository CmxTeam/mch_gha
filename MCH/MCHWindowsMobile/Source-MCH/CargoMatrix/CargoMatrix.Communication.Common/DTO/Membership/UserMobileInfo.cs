﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.DTO.Membership
{
	public class UserMobileInfo
	{
		public long UserId { get; set; }
		public int CompanyId { get; set; }
		public string Pin { get; set; }
		public long RoleId { get; set; }
		public string UserName { get; set; }
		public string Lastname { get; set; }
		public string Firstname { get; set; }
		public bool IsAdmin { get; set; }
		public UserShellSetting ShellSetting { get; set; }
	}
}
