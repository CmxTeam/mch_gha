﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.DTO.Membership
{
	public class AuthAppUserModel
	{
		public string AppName { get; set; }
		public string Pin { get; set; }
	}
}
