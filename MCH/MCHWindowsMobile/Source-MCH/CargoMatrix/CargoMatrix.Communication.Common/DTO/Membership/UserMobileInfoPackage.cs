﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.Data;

namespace CargoMatrix.Communication.Common.DTO.Membership
{
	public class UserMobileInfoPackage : ITransactional, IDataContainer<UserMobileInfo>
	{
		#region ITransactional Members
		public CommunicationTransaction Transaction { get; set; }
		#endregion

		#region IDataContainer<UserMobileInfo> Members

		public UserMobileInfo Data { get; set; }

		#endregion
	}
}
