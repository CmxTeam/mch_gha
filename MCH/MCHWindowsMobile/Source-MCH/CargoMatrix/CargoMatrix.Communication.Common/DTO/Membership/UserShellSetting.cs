﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.DTO.Membership
{
	public class UserShellSetting
	{
		public string SiteIds { get; set; }
		public long? DefaultSiteId { get; set; }
		public string WarehouseIds { get; set; }
		public long? DefaultWarehouseId { get; set; }
		public List<UserShellAppSettings> AppSettings { get; set; }
	}
}
