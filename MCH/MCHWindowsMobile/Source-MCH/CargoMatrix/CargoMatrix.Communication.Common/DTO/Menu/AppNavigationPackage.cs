﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.Data;

namespace CargoMatrix.Communication.Common.DTO.Menu
{
	public class AppNavigationPackage : ITransactional, IDataContainer<List<AppNavigation>>
	{
		#region ITransactional Members
		public CommunicationTransaction Transaction { get; set; }
		#endregion

		#region IDataContainer<List<AppNavigation>> Members

		public List<AppNavigation> Data { get; set; }

		#endregion
	}
}
