﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.Enumerations;

namespace CargoMatrix.Communication.Common.DTO.Menu
{
	public class AppRoleMenuModel
	{
		public string AppName { get; set; }
		public long RoleId { get; set; }
		public ApplicationTypes ApplicationTypeId { get; set; }
	}
}
