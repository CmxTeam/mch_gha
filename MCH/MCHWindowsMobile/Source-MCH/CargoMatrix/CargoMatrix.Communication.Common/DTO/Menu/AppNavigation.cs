﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common.DTO.Menu
{
	public class AppNavigation
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public string Text { get; set; }
		public List<AppNavigation> ChildItems { get; set; }
		public int Order { get; set; }
		public string IconKey { get; set; }
		public string NavigationPath { get; set; }
		public string IconBase64 { get; set; }
		public bool IsDefault { get; set; }
		public string MenuParam { get; set; }
		public List<long> Restrictions { get; set; }
		public bool HasChildren { get; set; }
	}
}
