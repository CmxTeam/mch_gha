﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.DamageCapture
{
    public class ShipmentConditionsEventArgs : EventArgs
    {
        IShipmentConditionType[] shipmentConditions;

        public IShipmentConditionType[] ShipmentConditions
        {
            get { return shipmentConditions; }
            set { shipmentConditions = value; }
        }
    }
}
