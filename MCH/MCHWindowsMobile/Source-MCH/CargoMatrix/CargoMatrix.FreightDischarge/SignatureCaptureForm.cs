﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GraphicsResources = Resources.Graphics;
using System.IO;

namespace CargoMatrix.FreightDischarge
{
    public partial class SignatureCaptureForm : Form
    {
        string header;
     
        public string DriversName
        {
            get
            {
                return this.lblDriversName.Text;
            }
            set
            {
                this.lblDriversName.Text = value;
            }
        }

        public string CompanyName
        {
            get
            {
                return this.lblCompanyName.Text;
            }
            set
            {
                this.lblCompanyName.Text = value;
            }
        }

        public Image DriverPhoto
        {
            get
            {
                return this.pcxPhoto.Image;
            }
            set
            {
                this.pcxPhoto.Image = value;
            }
        }

        public int Pieces
        {
            set
            {
                this.lblPieces.Text = value.ToString();
            }
        }

        public byte[] DriverPhotoAsArray
        {
            get
            {
                using(MemoryStream ms = new MemoryStream())
                {
                    this.pcxPhoto.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    return ms.ToArray();
                }
            }
            set
            {
                using (MemoryStream ms = new MemoryStream(value))
                {
                    this.pcxPhoto.Image = new Bitmap(ms);
                }
            }
        }

        public string SubHeader
        {
            get { return this.lblSignatureRequired.Text; }
            set { this.lblSignatureRequired.Text = value; }
        }
     
        /// <summary>
        /// Gets or Sets form's header text
        /// </summary>
        public string Header
        {
            get { return header; }
            set { header = value; }
        }

        /// <summary>
        /// Gets or sets signature as byte array (point(x,y))
        /// </summary>
        public byte[] SignatureAsBytes
        {
            get
            {
                Bitmap bmp = this.SignatureAsBitmap;

                byte[] signature;
                using (MemoryStream ms = new MemoryStream())
                {
                    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    signature = ms.ToArray();
                    ms.Flush();
                }

                return signature;
            }
        }

        /// <summary>
        /// Gets the signature as a bitmap
        /// </summary>
        public Bitmap SignatureAsBitmap
        {
            get
            {
                return this.signatureControl.ToBitmap();
            }
        }

        
        public SignatureCaptureForm()
        {
            InitializeComponent();
            this.pcxPhoto.AutoscaleDimantions = this.AutoScaleDimensions;
        }

        void pcxHeader_Paint(object sender, PaintEventArgs e)
        {
            using(Brush brush = new SolidBrush(Color.White))
            {
                e.Graphics.DrawString(this.Header, 
                                      new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold),
                                      brush, 5, 3);
            }
        }

        private void InitializeFields()
        {
            this.Header = "Cargomatrix";

            this.pcxPhoto.Image = CargoMatrix.Resources.Skin.photo_not_aval; //CargoMatrix.Resources.Skin.worker;
            this.pcxPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
          
            this.signatureControl.BackgroundBitmap = CargoMatrix.Resources.Skin.SignatureBackground;

            this.pcxHeader.Image = GraphicsResources.Skin.popup_header;
            this.pcxFooter.Image = GraphicsResources.Skin.nav_bg;
            this.pcxFooter.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pcxHeader.Paint += new PaintEventHandler(pcxHeader_Paint);

            this.buttonCancel.Image = GraphicsResources.Skin.nav_cancel;  //CargoMatrix.Resources.Skin.button_default;
            this.buttonCancel.PressedImage = GraphicsResources.Skin.nav_cancel_over; //CargoMatrix.Resources.Skin.button_pressed;//   ;
            this.buttonCancel.Click += new EventHandler(buttonCancel_Click);

            this.buttonOk.Image = GraphicsResources.Skin.nav_ok; //CargoMatrix.Resources.Skin.button_default; 
            this.buttonOk.PressedImage = GraphicsResources.Skin.nav_ok_over; //CargoMatrix.Resources.Skin.button_pressed;
            this.buttonOk.Text = "OK";
            this.buttonCancel.BackColor = Color.White;
            this.buttonOk.Click += new EventHandler(buttonOk_Click);

            this.buttonClear.Image = CargoMatrix.Resources.Skin.eraser_btn; //GraphicsResources.Skin.nav_cancel; //CargoMatrix.Resources.Skin.button_default; /;
            this.buttonClear.PressedImage = CargoMatrix.Resources.Skin.eraser_button_over; //GraphicsResources.Skin.nav_cancel_over; // CargoMatrix.Resources.Skin.button_pressed;
            this.buttonClear.Text = "Clear";
            this.buttonCancel.BackColor = Color.White;
            this.buttonClear.Click += new EventHandler(buttonClear_Click);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            byte[] signature = this.signatureControl.GetSignatureEx();

            if (signature.Length <= 4)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Signature is required.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Message, MessageBoxButtons.OK, DialogResult.OK);

                return;
            }
            
            DialogResult result = CargoMatrix.UI.CMXMessageBox.Show("Confirm the signature is valid.", "Signature confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, DialogResult.Yes);

            if (result == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            this.signatureControl.Clear();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
            
        }

    }
}