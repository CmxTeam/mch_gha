﻿namespace CargoMatrix.FreightDischarge
{
    partial class DischargeItemDisplayControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DischargeItemDisplayControl));
            this.labelPieces = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.labelLoc = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDamage = new CargoMatrix.UI.CMXPictureButton();
            this.labelSender = new System.Windows.Forms.Label();
            this.labelReceiver = new System.Windows.Forms.Label();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.labelDescr = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblAliasNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSlac = new System.Windows.Forms.Label();
            
            this.SuspendLayout();
            // 
            // labelPieces
            // 
            this.labelPieces.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPieces.Location = new System.Drawing.Point(76, 50);
            this.labelPieces.Name = "labelPieces";
            this.labelPieces.Size = new System.Drawing.Size(121, 13);
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(44, 8);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(190, 14);
            // 
            // labelLoc
            // 
            this.labelLoc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLoc.Location = new System.Drawing.Point(76, 86);
            this.labelLoc.Name = "labelLoc";
            this.labelLoc.Size = new System.Drawing.Size(121, 13);
            this.labelLoc.Text = "Not Recovered";
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(7, 2);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(24, 24);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDetails.Image = ((System.Drawing.Image)(resources.GetObject("buttonDetails.Image")));
            this.buttonDetails.Location = new System.Drawing.Point(202, 28);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.PressedImage = ((System.Drawing.Image)(resources.GetObject("buttonDetails.PressedImage")));
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.BackColor = System.Drawing.SystemColors.Control;
            this.buttonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("buttonBrowse.Image")));
            this.buttonBrowse.Location = new System.Drawing.Point(202, 104);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.PressedImage = ((System.Drawing.Image)(resources.GetObject("buttonBrowse.PressedImage")));
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // buttonDamage
            // 
            this.buttonDamage.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDamage.Image = ((System.Drawing.Image)(resources.GetObject("buttonDamage.Image")));
            this.buttonDamage.Location = new System.Drawing.Point(202, 66);
            this.buttonDamage.Name = "buttonDamage";
            this.buttonDamage.PressedImage = ((System.Drawing.Image)(resources.GetObject("buttonDamage.PressedImage")));
            this.buttonDamage.Size = new System.Drawing.Size(32, 32);
            this.buttonDamage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDamage.TransparentColor = System.Drawing.Color.White;
            this.buttonDamage.Click += new System.EventHandler(this.buttonDamage_Click);
            // 
            // labelSender
            // 
            this.labelSender.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelSender.Location = new System.Drawing.Point(76, 104);
            this.labelSender.Name = "labelSender";
            this.labelSender.Size = new System.Drawing.Size(121, 13);
            // 
            // labelReceiver
            // 
            this.labelReceiver.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelReceiver.Location = new System.Drawing.Point(76, 122);
            this.labelReceiver.Name = "labelReceiver";
            this.labelReceiver.Size = new System.Drawing.Size(121, 13);
            // 
            // panelIndicators
            // 
            this.panelIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIndicators.Location = new System.Drawing.Point(3, 157);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            this.panelIndicators.TabIndex = 19;
            // 
            // labelDescr
            // 
            this.labelDescr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDescr.Location = new System.Drawing.Point(76, 140);
            this.labelDescr.Name = "labelDescr";
            this.labelDescr.Size = new System.Drawing.Size(159, 13);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(7, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.Text = "Pieces :";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(7, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.Text = "Location :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(7, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.Text = "Sender :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(7, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.Text = "Receiver :";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(7, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.Text = "Descr. :";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(7, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.Text = "Reference :";
            // 
            // lblAliasNumber
            // 
            this.lblAliasNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblAliasNumber.Location = new System.Drawing.Point(76, 32);
            this.lblAliasNumber.Name = "lblAliasNumber";
            this.lblAliasNumber.Size = new System.Drawing.Size(121, 13);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(7, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.Text = "Slac :";
            // 
            // lblSlac
            // 
            this.lblSlac.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblSlac.Location = new System.Drawing.Point(76, 68);
            this.lblSlac.Name = "lblSlac";
            this.lblSlac.Size = new System.Drawing.Size(121, 13);
            // 
            // DischargeItemDisplayControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblSlac);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblAliasNumber);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelPieces);
            this.Controls.Add(this.labelDescr);
            this.Controls.Add(this.labelReceiver);
            this.Controls.Add(this.labelSender);
            this.Controls.Add(this.labelLoc);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonDamage);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.panelIndicators);
            this.Name = "DischargeItemDisplayControl";
            this.Size = new System.Drawing.Size(240, 174);
            
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label labelPieces;
        private System.Windows.Forms.Label labelLoc;
        private System.Windows.Forms.Label labelSender;
        private System.Windows.Forms.Label labelReceiver;
        private System.Windows.Forms.Label labelDescr;
        private CustomUtilities.IndicatorPanel panelIndicators;
        private CargoMatrix.UI.CMXPictureButton buttonDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private CargoMatrix.UI.CMXPictureButton buttonBrowse;
        private CargoMatrix.UI.CMXPictureButton buttonDamage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblAliasNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSlac;
    }
}