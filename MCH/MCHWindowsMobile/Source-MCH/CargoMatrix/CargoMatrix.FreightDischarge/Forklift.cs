﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using System.Drawing;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.CargoDischarge;
using CargoMatrix.Communication.WSCargoDischarge;
using CMXBarcode;
using CustomUtilities;
using CustomListItems;
using CMXExtensions;


namespace CargoMatrix.FreightDischarge
{
    class Forklift : CargoMatrix.Utilities.MessageListBox
    {
        private string releaseDoorName;

        List<IDischargeHouseBillItem> houseBillItems;
        private static Forklift instance;
        private ChangeCauseEnum lastContentChangeType;
        private List<int> removedHouseBillsIds;
        private List<int> releasedHouseBillsIds;
        private int itemsCount;
        int id;
      
        CargoMatrix.Communication.ScannerUtilityWS.StepsTypes scanningType;
        CargoMatrix.UI.BarcodeReader barcode;

        public event EventHandler<ContentChangedEventArgs> OnContentChanged;

        #region Properties
        public static Forklift Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Forklift();
                }

                if (instance.IsLoaded == false)
                {
                    instance.LoadAppSettings();
                }

                return instance;
            }
        }

        public bool IsLoaded { get; private set; }


        public int ID
        {
            get
            {
                return this.id;
            }

        }

        public CargoMatrix.Communication.ScannerUtilityWS.StepsTypes ScanningType
        {
            get { return scanningType; }
        }

        public ChangeCauseEnum LastContentChangeType
        {
            get { return lastContentChangeType; }
        }

        public int ItemsCountDB
        {
            get
            {
                return CargoDischarge.Instance.GetForkliftItemsCount(this.ID);
            }
        }

        public int ItemsCountList
        {
            get
            {
                return this.itemsCount;
            }
            set
            {
                this.itemsCount = value;
            }

        }

        public List<IDischargeHouseBillItem> HouseBillItems
        {
            get { return houseBillItems; }
            set { houseBillItems = value; }
        }

        public List<int> RemovedHouseBillsIds
        {
            get { return removedHouseBillsIds; }
        }

        public List<int> ReleasedHouseBillsIds
        {
            get { return releasedHouseBillsIds; }
        }

        public string ReleaseDoorName
        {
            get { return releaseDoorName; }
            set { releaseDoorName = value; }
        }
        
        #endregion

        #region Constructors
        private Forklift()
        {
            smoothListBoxBase1.MultiSelectEnabled = false;
            this.OneTouchSelection = false;
            this.HeaderText = "Forklift View";

            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(Forklift_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(Forklift_ListItemClicked);
           
            Cursor.Current = Cursors.Default;

            lastContentChangeType = ChangeCauseEnum.Closed;
            this.removedHouseBillsIds = new List<int>();
            this.releasedHouseBillsIds = new List<int>();
            this.houseBillItems = new List<IDischargeHouseBillItem>();

            this.barcode = new CargoMatrix.UI.BarcodeReader();
            this.barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(Barcode_ReadNotify);
        } 
        #endregion

        #region Handlers
        void Forklift_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var item = (ComboBox<DischargeHousebillItemState>)listItem;

            //item.SelectAll(isSelected);

            if (isSelected == false)
            {
                this.UnScanHousebill(item);

                item.SelectedChanged(false);
                return;
            }

            if (item.Data.Item.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
            {
                this.SelectInPiecesMode(item);
            }
            else
            {
                this.SelectInCountMode(item);
            }
        }

        void Barcode_ReadNotify(string barcodeData)
        {
            ScanObject scanObject = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            this.barcode.StartRead();

            switch (scanObject.BarcodeType)
            {
                case BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    this.Scan(scanObject.HouseBillNumber);

                    break;

                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);

                    CargoMatrix.UI.CMXMessageBox.Show("Ivalid barcode scanned", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    break;
            }

        }

        void Forklift_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            PopulateForkliftContent();

            this.barcode.StartRead();
        }

        void Combo_LayoutChanged(CustomListItems.ComboBox sender, int subItemId, bool comboIsEmpty)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (comboIsEmpty)
            {
                smoothListBoxBase1.RemoveItem(sender as Control);
            }

            smoothListBoxBase1.LayoutItems();

            var combo = (CustomListItems.ComboBox<DischargeHousebillItemState>)sender;


            TransactionStatus finalStatus = new TransactionStatus() { TransactionStatus1 = true };
            IDischargeHouseBillItem houseBillItem = combo.Data.Item;// this.GetHousebillByID(sender.ID);

            if (subItemId == -1)
            {
                finalStatus = CargoDischarge.Instance.RemoveHousebillFromForklift(houseBillItem, this.ID);

                if (finalStatus.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(finalStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }
         
            if (smoothListBoxBase1.ItemsCount == 0)
            {
                DialogResult = DialogResult.Cancel;
            }

            if (finalStatus.TransactionStatus1 == true)
            {
                this.lastContentChangeType = ChangeCauseEnum.Remove;
                this.removedHouseBillsIds.Add(houseBillItem.HousebillId);
                this.RemoveFromHouseBill(houseBillItem);

                this.itemsCount = finalStatus.TransactionRecords;

            }

            Cursor.Current = Cursors.Default;

            this.OkEnabled = this.GetOKEnabled();
        }

        void Combo_SelectionChanged(object sender, EventArgs e)
        {
            var combo = (CustomListItems.ComboBox<DischargeHousebillItemState>)sender;
            int selectedItemsCount = combo.SelectedIds.Count();

            if (selectedItemsCount == combo.ItemsCount)
            {
                this.CompletelyScanHouseBill(combo);
            }
            else
            {
                combo.Data.ScannedPeces = selectedItemsCount;

                if (selectedItemsCount > 0)
                {
                    var selectedItemsDatas = from item in combo.Items
                                             where (from ID in combo.SelectedIds
                                                    select ID).Contains(item.id)
                                             select item;


                    this.PartiallyScanHouseBill(combo, selectedItemsDatas);
                }
                else
                {
                    this.UnScanHousebill(combo);
                    return;
                }

                this.OkEnabled = this.GetOKEnabled();
            }


            var deSelectedItemsDatas = from item in combo.Items
                                     where !(from ID in combo.SelectedIds
                                            select ID).Contains(item.id)
                                     select item;

            foreach (var item in deSelectedItemsDatas)
            {
                item.isChecked = false;
            }

        } 
        #endregion

        #region Overrides
        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            bool partialScanDetected = false;

            var partiallyScannedHouseBiills = from item in this.smoothListBoxBase1.Items.OfType<CustomListItems.ComboBox<DischargeHousebillItemState>>()
                                              where item.Data.Item.ReleaseStatus != ReleaseStatuses.Completed
                                              select (IDischargeHouseBillItem)item.Data.Item;

            if (partiallyScannedHouseBiills.Count() > 0)
            {
                var dialogResult = CargoMatrix.UI.CMXMessageBox.Show("Release partilally scanned housebills?", "Question", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.OK);

                if (dialogResult != DialogResult.OK) return;

                partialScanDetected = true;
            }

            if (partialScanDetected == false)
            {
                partiallyScannedHouseBiills = from item in this.smoothListBoxBase1.Items.OfType<CustomListItems.ComboBox<DischargeHousebillItemState>>()
                                              where item.Data.State == DischargeHousebillItemStateEnum.PartiallyScanned
                                              select (IDischargeHouseBillItem)item.Data.Item;

                if (partiallyScannedHouseBiills.Count() > 0)
                {
                    var dialogResult = CargoMatrix.UI.CMXMessageBox.Show("Release partilally scanned housebills?", "Question", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.OK);

                    if (dialogResult != DialogResult.OK) return;
                }
            }

            string dropDoor;
            bool result = this.GetDoorFromUser(out dropDoor);

            this.releaseDoorName = string.Empty;

            if (result == false) return;

            IEnumerable<IDischargeHouseBillItem> houseBills;
            IEnumerable<PieceItem> pieces;
            this.GetReleaseItems(out houseBills, out pieces);

            result = DischargeItemFunctionallity.ReleaseHousebills(dropDoor, houseBills, pieces);

            if (result == true)
            {
                this.barcode.StopRead();

                MoveUnreleasedPieces();

                this.lastContentChangeType = ChangeCauseEnum.Release;

                this.Close();
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            this.barcode.StopRead();

            this.releaseDoorName = string.Empty;

            base.OnClosing(e);
        }
        #endregion
        
        #region Additional Methods

        private void GetReleaseItems(out IEnumerable<IDischargeHouseBillItem> houseBills, out IEnumerable<PieceItem> pieces)
        {
            var housebillsToRelease = from item in this.smoothListBoxBase1.Items.OfType<CustomListItems.ComboBox<DischargeHousebillItemState>>()
                                      where item.Data.State != DischargeHousebillItemStateEnum.NonScanned
                                      select new
                                      {
                                          HouseBill = item.Data.Item,
                                          Combo = item,
                                          ScannedPieces = item.Data.ScannedPeces
                                      };

            houseBills = from item in housebillsToRelease
                             select item.HouseBill;

            List<PieceItem> piecesList = new List<PieceItem>();

            foreach (var item in housebillsToRelease)
            {
                var houseBill = item.HouseBill;
                IEnumerable<PieceItem> pItems = 

                item.HouseBill.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Count ?
                
                    from piece in houseBill.Pieces.Take(item.ScannedPieces)
                    select DischargeItemFunctionallity.CreateReleasePieceItem(piece, houseBill)
                    :
                    from p in houseBill.Pieces
                    where (from dataItem in item.Combo.Items
                           where dataItem.isChecked == true
                           select dataItem.id).Contains(p.PieceId)
                    select DischargeItemFunctionallity.CreateReleasePieceItem(p, houseBill);
           
               piecesList.AddRange(pItems);
            }

            pieces = piecesList;
        }
        
        void SelectInPiecesMode(CustomListItems.ComboBox<DischargeHousebillItemState> item)
        {
            if (item.Data.State == DischargeHousebillItemStateEnum.CompletlyScanned)
            {
                this.UnScanHousebill(item);
                item.SelectedChanged(false);
                return;
            }

            this.CompletelyScanHouseBill(item);
            item.SelectedChanged(true);
        }

        void SelectInCountMode(CustomListItems.ComboBox<DischargeHousebillItemState> item)
        {
            if (item.Data.State != DischargeHousebillItemStateEnum.NonScanned)
            {
                this.UnScanHousebill(item);
                item.SelectedChanged(false);
                return;
            }

            int maxPieces = item.Data.TotalPieces - item.Data.ScannedPeces;
            bool isReadonly = maxPieces == 1;

            int? piecesCount = DischargeItemFunctionallity.GetPiecesCountFromUser(item.Data.Item.Reference(), "Number of pieces to release", maxPieces, isReadonly);

            if (piecesCount.HasValue == false)
            {
                item.SelectedChanged(false);
                return;
            }

            if (piecesCount == maxPieces)
            {
                this.CompletelyScanHouseBill(item);
                item.SelectedChanged(true);
                return;
            }

            this.PartiallyScanHouseBill(item, piecesCount.Value);

            this.OkEnabled = true;

            return;
        }

        private IDischargeHouseBillItem GetHousebillByID(int houseBillID)
        {
            return
                (
                    from item in this.houseBillItems
                    where item.HousebillId == houseBillID
                    select item
                ).FirstOrDefault();
        }

        private void PopulateForkliftContent()
        {
            smoothListBoxBase1.RemoveAll();
            int piecesCount = 0;
            foreach (var hawb in CargoDischarge.Instance.GetForkLiftHouseBills(new HouseBillSortCriteria(), this.ID))
            {
                List<CustomListItems.ComboBoxItemData> pieces = new List<CustomListItems.ComboBoxItemData>();
                piecesCount += hawb.Pieces.Count();


                if (hawb.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
                {
                    foreach (var hawbPiece in hawb.Pieces)
                    {
                        var pieceItem = new CustomListItems.ComboBoxItemData(string.Format("Piece {0} ({1})", hawbPiece.PieceNumber, hawbPiece.Slac), "Loc " + hawbPiece.Location, hawbPiece.PieceId);
                        pieceItem.isReadonly = true;

                        pieces.Add(pieceItem);
                    }
                }

                DischargeHousebillItemState status = new DischargeHousebillItemState(hawb, DischargeHousebillItemStateEnum.NonScanned);
                status.TotalPieces = hawb.ScannedPieces;

                Image icon = hawb.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode :
                                                                                              CargoMatrix.Resources.Skin.countMode;

                string line2 = this.GetLine2Text(hawb, status.ScannedPeces);

                CustomListItems.ComboBox<DischargeHousebillItemState> combo = new CustomListItems.ComboBox<DischargeHousebillItemState>(status, hawb.HousebillId, hawb.Reference(), line2, icon, pieces);
                combo.ContainerName = "Forklift";
                combo.Expandable = hawb.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece;
                combo.IsSelectable = true;
                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(Combo_LayoutChanged);
                combo.SelectionChanged += new EventHandler(Combo_SelectionChanged);

                smoothListBoxBase1.AddItem(combo);
            }

            OkEnabled = false;

            if (smoothListBoxBase1.ItemsCount == 0)
            {
                DialogResult = DialogResult.Cancel;
            }

            lastContentChangeType = ChangeCauseEnum.Closed;

        }

        private void Scan(string housebillNumber)
        {
            var dischargeHouseBillCombo = (
                                            from item in this.Items.OfType<CustomListItems.ComboBox<DischargeHousebillItemState>>()
                                            where string.Compare(item.Data.Item.HousebillNumber, housebillNumber) == 0
                                            select item
                                         ).FirstOrDefault();

            if (dischargeHouseBillCombo == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Housebill is not in the list", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            var itemStateData = dischargeHouseBillCombo.Data;

            if (itemStateData.State == DischargeHousebillItemStateEnum.CompletlyScanned)
            {
                //var dResult = CargoMatrix.UI.CMXMessageBox.Show("Undo scan?", "Question", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.OK);

                //if (dResult != DialogResult.OK) return;

                //this.UnScanHousebill(dischargeHouseBillCombo);

                //dischargeHouseBillCombo.SelectedChanged(false);

                CargoMatrix.UI.CMXMessageBox.Show("Housebill is scanned", "Message", CargoMatrix.UI.CMXMessageBoxIcon.Message);

                return;

            }

            int maxPieces = itemStateData.TotalPieces - itemStateData.ScannedPeces;
            bool isReadonly = maxPieces == 1;
            int? piecesCount = DischargeItemFunctionallity.GetPiecesCountFromUser(itemStateData.Item.Reference(), "Number of pieces to release", maxPieces, isReadonly);

            if (piecesCount.HasValue == false)
            {
                dischargeHouseBillCombo.SelectedChanged(false);
                return;
            }

            itemStateData.ScannedPeces += piecesCount.Value;

            if (itemStateData.ScannedPeces == itemStateData.Item.ScannedPieces)
            {
                this.CompletelyScanHouseBill(dischargeHouseBillCombo);

                dischargeHouseBillCombo.SelectedChanged(true);
            }
            else
            {
                var notSelectedItems = (from item in dischargeHouseBillCombo.Items
                                      where item.isChecked == false
                                      select item).Take(piecesCount.Value);

                this.PartiallyScanHouseBill(dischargeHouseBillCombo, notSelectedItems);
            }

            this.OkEnabled = true;
        }

        public static void MoveUnreleasedPieces()
        {
            var houseBiillItems = from item in CargoDischarge.Instance.GetForkLiftHouseBills(new HouseBillSortCriteria(), Forklift.Instance.ID)
                                  where item.status == CargoMatrix.Communication.DTO.HouseBillStatuses.Completed
                                  select item;

            if (houseBiillItems.Count() <= 0) return;

            DischargeItemFunctionallity.MoveUnreleasedPieces(houseBiillItems.OfType<IDischargeHouseBillItem>());
        }

        private void FireContentChanged(ContentChangedEventArgs args)
        {
            var contentChanged = this.OnContentChanged;

            if (contentChanged != null)
            {
                contentChanged(this, args);
            }
        }

        private void RemoveFromHouseBill(IDischargeHouseBillItem houseBillItem)
        {

            var hbItem = (from item in this.houseBillItems
                          where item.HousebillId == houseBillItem.HousebillId
                          select item).FirstOrDefault();

            if (hbItem != null)
            {
                this.houseBillItems.Remove(hbItem);
            }
        }

        private string GetLine2Text(IDischargeHouseBillItem houseBill, int secondScannedPieces)
        {
            return string.Format("Pieces: {0} of {1}", secondScannedPieces, houseBill.AvailablePieces + houseBill.ScannedPieces);
        }

        bool GetOKEnabled()
        {
            var houseBiill = this.smoothListBoxBase1.Items.OfType<CustomListItems.ComboBox<DischargeHousebillItemState>>().
                                    FirstOrDefault(item => item.Data.State != DischargeHousebillItemStateEnum.NonScanned);

            return houseBiill != null;
        }

        private void UnScanHousebill(CustomListItems.ComboBox<DischargeHousebillItemState> listItem)
        {
            var itemStateData = listItem.Data;
            
            itemStateData.State = DischargeHousebillItemStateEnum.NonScanned;
            itemStateData.ScannedPeces = 0;
            listItem.TextColor = Color.Black;
            listItem.Line2Color = Color.Black;
            listItem.Line2 = this.GetLine2Text(itemStateData.Item, itemStateData.ScannedPeces);

            listItem.UpdateSubItemsIcon(false, itemStateData.TotalPieces);

            this.OkEnabled = this.GetOKEnabled();
        }

        private void CompletelyScanHouseBill(CustomListItems.ComboBox<DischargeHousebillItemState> listItem)
        {
            listItem.Data.State = DischargeHousebillItemStateEnum.CompletlyScanned;
            listItem.Data.ScannedPeces = listItem.Data.TotalPieces;
            listItem.Line2 = this.GetLine2Text(listItem.Data.Item, listItem.Data.ScannedPeces);
            listItem.TextColor = Color.Green;
            listItem.Line2Color = Color.Green;
            listItem.UpdateSubItemsIcon(true, listItem.Data.ScannedPeces);

            OkEnabled = true;
        }

        private void PartiallyScanHouseBill(CustomListItems.ComboBox<DischargeHousebillItemState> listItem, IEnumerable<CustomListItems.ComboBoxItemData> scannnedPiecesItemss)
        {
            listItem.Data.State = DischargeHousebillItemStateEnum.PartiallyScanned;
            listItem.Line2 = this.GetLine2Text(listItem.Data.Item, listItem.Data.ScannedPeces);
            listItem.TextColor = Color.Red;
            listItem.Line2Color = Color.Red;
            listItem.UpdateSubItemsIcon(true, scannnedPiecesItemss);
        }

        private void PartiallyScanHouseBill(CustomListItems.ComboBox<DischargeHousebillItemState> listItem, int itemsCoumt)
        {
            listItem.Data.State = DischargeHousebillItemStateEnum.PartiallyScanned;
            listItem.Data.ScannedPeces = itemsCoumt;
            listItem.Line2 = this.GetLine2Text(listItem.Data.Item, listItem.Data.ScannedPeces);
            listItem.TextColor = Color.Red;
            listItem.Line2Color = Color.Red;
            listItem.UpdateSubItemsIcon(true, itemsCoumt);
        }

        private bool GetDoorFromUser(out string doorName)
        {
            doorName = null;

            if (string.IsNullOrEmpty(this.releaseDoorName) == false)
            {
                doorName = this.releaseDoorName;
                return true;
            }
            
            this.barcode.StopRead();

            string scannedText;
            string locationName;

            bool result = DischargeItemFunctionallity.GetLocationFromUser("Release door", "Scan/select door",
                                                            CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Door,
                                                            out scannedText, out locationName);

            this.barcode.StartRead();

            if (result == false) return false;
           
            doorName = locationName;
            return true;
        }

        private void LoadAppSettings()
        {
            var tempSettings = CargoMatrix.Communication.CargoDischarge.CargoDischarge.Instance.GetTaskSettings();
            if (tempSettings.Transaction.TransactionStatus1 == true)
            {
                this.id = tempSettings.ForkliftId;
                this.scanningType = tempSettings.ScanningSteps;

                if (this.scanningType == CargoMatrix.Communication.ScannerUtilityWS.StepsTypes.OneScan)
                {
                    this.IsSelectable = true;
                    this.HeaderText2 = "Select/Scan HAWBs to release";
                }
                else
                {
                    this.IsSelectable = false;
                    this.HeaderText2 = "Scan HAWBs to release";
                }

                this.IsLoaded = true;
            }
            else
            {
                this.IsLoaded = false;
                CargoMatrix.UI.CMXMessageBox.Show("Error has occured while downloading task settings. Refresh or restart the application", "Unable to connect", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }

        #endregion
    }
}
