﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.FreightDischarge
{
    public class ContentChangedEventArgs : EventArgs
    {
        ChangeCauseEnum changeCause;

        public ChangeCauseEnum ChangeCause
        {
            get { return changeCause; }
            set { changeCause = value; }
        } 
        
        int[] houseBillsIDs;

        public int[] HouseBillsIDs
        {
            get { return houseBillsIDs; }
            set { houseBillsIDs = value; }
        }
    }

    public enum ChangeCauseEnum
    {
        Remove,
        Release,
        Closed
    }
}
