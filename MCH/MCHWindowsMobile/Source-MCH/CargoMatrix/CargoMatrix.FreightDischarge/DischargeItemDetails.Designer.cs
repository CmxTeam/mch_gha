﻿namespace CargoMatrix.FreightDischarge
{
    partial class DischargeItemDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
    
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonScannedList = new CargoMatrix.UI.CMXCounterIcon();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblDriver = new System.Windows.Forms.Label();
            this.topLabel = new CustomUtilities.CMXBlinkingLabel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.topPanel = new System.Windows.Forms.Panel();
            this.topPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonScannedList
            // 
            this.buttonScannedList.Location = new System.Drawing.Point(198, 4);
            this.buttonScannedList.Name = "buttonScannedList";
            this.buttonScannedList.Image = CargoMatrix.Resources.Skin.Forklift_btn;
            this.buttonScannedList.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over;
            this.buttonScannedList.Size = new System.Drawing.Size(36, 36);
            this.buttonScannedList.TabIndex = 1;
            this.buttonScannedList.Click += new System.EventHandler(this.buttonScannedList_Click);

            
            //
            // lblCompany
            //
            this.lblCompany.BackColor = System.Drawing.Color.White;
            this.lblCompany.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblCompany.ForeColor = System.Drawing.Color.Black;
            this.lblCompany.Location = new System.Drawing.Point(3, 2);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(190, 15);

            //
            // lblDriver
            //
            this.lblDriver.BackColor = System.Drawing.Color.White;
            this.lblDriver.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblDriver.ForeColor = System.Drawing.Color.Black;
            this.lblDriver.Location = new System.Drawing.Point(3, 19);
            this.lblDriver.Name = "lblDriver";
            this.lblDriver.Size = new System.Drawing.Size(190, 15);

            // 
            // topLabel
            // 
            this.topLabel.BackColor = System.Drawing.Color.White;
            this.topLabel.Blink = true;
            this.topLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.topLabel.ForeColor = System.Drawing.Color.Red;
            this.topLabel.Location = new System.Drawing.Point(3, 36);
            this.topLabel.Name = "topLabel";
            this.topLabel.Size = new System.Drawing.Size(190, 15);
            
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 53);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(240, 1);
            // 
            // topPanel
            // 
            this.topPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.topPanel.Controls.Add(this.lblDriver);
            this.topPanel.Controls.Add(this.lblCompany);
            this.topPanel.Controls.Add(this.topLabel);
            this.topPanel.Controls.Add(this.buttonScannedList);
            this.topPanel.Controls.Add(this.splitter1);
            
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(240, 53);
            // 
            // DischargeItemDetails
            // 
            this.components = new System.ComponentModel.Container();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.Controls.Add(this.topPanel);
            this.Name = "DischargeItemDetails";
            this.Text = "DischargeItemDetails";
            this.topPanel.ResumeLayout(false);
            this.ResumeLayout(false);
   
        }

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.Label lblDriver;
        private CustomUtilities.CMXBlinkingLabel topLabel;
        private CargoMatrix.UI.CMXCounterIcon buttonScannedList;
        private System.Windows.Forms.Panel topPanel;

        #endregion
    }
}