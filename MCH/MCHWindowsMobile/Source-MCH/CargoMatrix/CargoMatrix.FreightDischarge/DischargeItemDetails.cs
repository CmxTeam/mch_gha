﻿using System;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoDischarge;
using CargoMatrix.Communication.Common;
using CMXExtensions;

namespace CargoMatrix.FreightDischarge
{
    public partial class DischargeItemDetails : SmoothListbox.SmoothListbox
    {
        IDischargeHouseBillItem[] houseBillItems;
        DischargeItemDisplayControl hawbDetailsControl;
        
        public DischargeItemDetails(int forkliftItemsCount)
        {
            InitializeComponent();

            panelHeader2.Height = smoothListBoxMainList.Bottom - panelHeader2.Top;

            this.topLabel.Text = "SCAN HOUSEBILL...";
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeScanned);

            buttonScannedList.Value = forkliftItemsCount; // Forklift.Instance.ItemsCountDB;
            Forklift.Instance.ItemsCountList = buttonScannedList.Value;

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;
        }

        

        private void UpdateHouseBillsList(IDischargeHouseBillItem[] houseBillItems, IDischargeHouseBillItem item)
        {
            for (int i = 0; i < houseBillItems.Length; i++)
            {
                if (houseBillItems[i].HousebillNumber.Equals(item.HousebillNumber))
                {
                    houseBillItems[i] = item;
                    break;
                }
            }
        }


        public void DisplayHouseBill(IDischargeHouseBillItem[] houseBillItems)
        {
            Cursor.Current = Cursors.WaitCursor;

            this.houseBillItems = houseBillItems;

            if (this.houseBillItems.Length == 1)
            {
                if (hawbDetailsControl == null)
                {
                    hawbDetailsControl = new DischargeItemDisplayControl() { Location = new Point(0, this.topPanel.Bottom + 1) };
                    this.panelHeader2.Controls.Add(hawbDetailsControl);
                }
                
                var hbItem = this.houseBillItems[0];

                hawbDetailsControl.DisplayHousebill(hbItem);
                this.TitleText = hbItem.Reference();
            }
           
            this.lblCompany.Text = houseBillItems[0].CompanyName;
            this.lblDriver.Text = houseBillItems[0].DriverFullName;

            Cursor.Current = Cursors.Default;
        }
       
        void BarcodeScanned(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

          
            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    Scan(scanItem.HouseBillNumber, scanItem.PieceNumber);
                    break;
                
                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                    break;
            }

            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
            
        }

        private bool ScanPiecesMode(IDischargeHouseBillItem selectedHouseBill, int pieceNumber, out DischargeHousebillItem houseBiullItem)
        {
            houseBiullItem = null;
            int piecesCount = 1;
            int slacNumber = piecesCount;

            if (selectedHouseBill.ReleseSlac == selectedHouseBill.ScannedSlac)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Housebill is already scannned", "Message", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                return false;
            }
            
            int remainigPieces = selectedHouseBill.TotalPieces - selectedHouseBill.ScannedPieces;

            if (remainigPieces == piecesCount)
            {
                slacNumber = selectedHouseBill.ReleseSlac - selectedHouseBill.ScannedSlac;
            }
            else
            {
                if (selectedHouseBill.TotalPieces != selectedHouseBill.Slac)
                {
                    //int maxReleseSlac = this.CalculateMaxReleaseSlac(selectedHouseBill, piecesCount, remainigPieces);
                    //int? enteredSlac = this.GetSlacFromUser(1, maxReleseSlac);

                    //if (enteredSlac.HasValue == false) return false;

                    //slacNumber = enteredSlac.Value;

                    bool result = this.ScanCountMode(selectedHouseBill, pieceNumber, out houseBiullItem);

                    if (result == false) return false;

                }
            }

            houseBiullItem = CargoDischarge.Instance.ScanPieceNumberIntoForkLift(selectedHouseBill.HousebillNumber,
                                                                                 pieceNumber,
                                                                                 slacNumber,
                                                                                 ScanTypeEnum.Automatic,
                                                                                 Forklift.Instance.ID);

            if (houseBiullItem.TransactionStatus.TransactionStatus1 == true)
            {
                buttonScannedList.Value = houseBiullItem.TransactionStatus.TransactionRecords;
                return true;
            }

            if (houseBiullItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Count)
            {
                return this.ScanCountMode(houseBiullItem, pieceNumber, out houseBiullItem);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(houseBiullItem.TransactionStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
            return false;
        }

        private bool ScanCountMode(IDischargeHouseBillItem selectedHouseBill, int pieceNumber, out DischargeHousebillItem houseBiullItem)
        {
            houseBiullItem = null;

            int remainigPieces = this.CalculateMaxPieces(selectedHouseBill);

            if (remainigPieces == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Housebill is already scannned", "Message", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                return false;
            }

            int piecesEntered = remainigPieces;

            if (remainigPieces != 1)
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
                cntMsg.HeaderText = "Confirm count";
                cntMsg.LabelDescription = "Enter number of pieces";
                cntMsg.LabelReference = selectedHouseBill.Reference();
                cntMsg.PieceCount = remainigPieces;

                if (DialogResult.OK != cntMsg.ShowDialog()) return false;

                piecesEntered = cntMsg.PieceCount;
            }
            
            int slacNumber = piecesEntered;

            if (remainigPieces == piecesEntered)
            {
                slacNumber = selectedHouseBill.ReleseSlac - selectedHouseBill.ScannedSlac;
            }
            else
            {
                if (selectedHouseBill.TotalPieces != selectedHouseBill.Slac)
                {
                    int maxReleseSlac = this.CalculateMaxReleaseSlac(selectedHouseBill, piecesEntered, remainigPieces);
                    int? enteredSlac = this.GetSlacFromUser(piecesEntered, maxReleseSlac);

                    if (enteredSlac.HasValue == false) return false;

                    slacNumber = enteredSlac.Value;
                }
            }

            houseBiullItem = CargoDischarge.Instance.ScanPieceCountIntoForkLift(selectedHouseBill,
                                                                               piecesEntered,
                                                                               slacNumber,
                                                                               ScanTypeEnum.Automatic,
                                                                               Forklift.Instance.ID);

            if (houseBiullItem.TransactionStatus.TransactionStatus1 == true)
            {
                buttonScannedList.Value = houseBiullItem.TransactionStatus.TransactionRecords;
                return true;
            }

            CargoMatrix.UI.CMXMessageBox.Show(houseBiullItem.TransactionStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            return false;
        }


        private void Scan(string housebillNumber, int pieceNumber)
        {
            var selectedHouseBill = (
                                        from item in this.houseBillItems
                                        where item.HousebillNumber.Equals(housebillNumber)
                                        select item
                                     ).FirstOrDefault();

            if (selectedHouseBill == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Wrong housebill number", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            DischargeHousebillItem houseBillItem;
            bool result = false;

            switch (selectedHouseBill.ScanMode)
            {
                case CargoMatrix.Communication.DTO.ScanModes.Piece:
                    result = this.ScanPiecesMode(selectedHouseBill, pieceNumber, out houseBillItem);
                    break;

                case CargoMatrix.Communication.DTO.ScanModes.Count:
                    result = this.ScanCountMode(selectedHouseBill, pieceNumber, out houseBillItem);
                    break;

                default: throw new NotImplementedException();
            }

            if (result == true)
            {
                this.UpdateHouseBillsList(this.houseBillItems, houseBillItem);

                if (this.hawbDetailsControl != null &&
                   this.hawbDetailsControl.HouseBillItem.HousebillId == houseBillItem.HousebillId)
                {
                    this.hawbDetailsControl.DisplayHousebill(houseBillItem);
                }
            }
        }

        private int CalculateMaxReleaseSlac(IDischargeHouseBillItem houseBillItem, int piecesCountUser, int maxPieces)
        {
            //int remainingPieces = houseBillItem.TotalPieces - houseBillItem.ScannedPieces;

            //return houseBillItem.ReleseSlac - houseBillItem.ScannedSlac - (remainingPieces - piecesCount);

            return (houseBillItem.ReleseSlac - houseBillItem.ScannedSlac) - (maxPieces - piecesCountUser);
        }

        private int CalculateMaxPieces(IDischargeHouseBillItem houseBillItem)
        {
            int result = 0;
            
            if(houseBillItem.TotalPieces != houseBillItem.Slac)
            {
                if(houseBillItem.Slac - (houseBillItem.ReleseSlac + houseBillItem.ScannedSlac) == 0)
                {
                    if(houseBillItem.ReleseSlac == houseBillItem.ScannedSlac)
                    {
                        result = 0;
                    }
                    else
                    {
                        result = houseBillItem.TotalPieces - houseBillItem.ScannedPieces;
                    }
                }
                else
                {
                    if(houseBillItem.TotalPieces > houseBillItem.ScannedPieces)
                    {
                        if(houseBillItem.Slac == houseBillItem.ReleseSlac)
                        {
                             result = houseBillItem.TotalPieces - houseBillItem.ScannedPieces;

                        }
                        else
                        {
                            result = houseBillItem.TotalPieces - houseBillItem.ScannedPieces - 1;
                        }
                        //result = houseBillItem.TotalPieces - houseBillItem.ScannedPieces - 1;
                    }
                    else
                    {
                        result = 0;
                    }
                }

                if(result > houseBillItem.ReleseSlac)
                {
                    result = houseBillItem.ReleseSlac;
                }
            }

            return result;
        }


        private int? GetSlacFromUser(int piecesCount, int maxReleseSlac)
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = "Confirm slac";
            cntMsg.LabelDescription = "Enter the slac";
            cntMsg.LabelReference = string.Format("Number of pieces: {0}", piecesCount);
            cntMsg.PieceCount = maxReleseSlac;

            if (DialogResult.OK != cntMsg.ShowDialog()) return null;

            return cntMsg.PieceCount;
        }

        

        void buttonScannedList_Click(object sender, System.EventArgs e)
        {
            if (buttonScannedList.Value <= 0) return;
            
            Cursor.Current = Cursors.WaitCursor;

            BarcodeEnabled = false;
            BarcodeEnabled = true;

            Forklift instance = Forklift.Instance;
           
            instance.ItemsCountList = buttonScannedList.Value; 
          
            instance.ShowDialog();

            buttonScannedList.Value = instance.ItemsCountList;

            switch (instance.LastContentChangeType)
            {
                case ChangeCauseEnum.Closed:
                    break;

                case ChangeCauseEnum.Remove:

                    this.houseBillItems = DischargeItemFunctionallity.RefreshHouseBills(this.houseBillItems);
                    this.DisplayHouseBill(this.houseBillItems);

                    break;

                case ChangeCauseEnum.Release:

                    //int[] selectedIDS = instance.SelectedHouseBillsIDs;
                    //var unReleasedItems = from item in this.houseBillItems
                    //                      where !(from id in selectedIDS
                    //                              select id).Contains(item.HousebillId)
                    //                      select item;

                    //if (unReleasedItems.Count() > 0)
                    //{
                    //    this.DisplayHouseBill(unReleasedItems.ToArray());
                    //}
                    //else
                    //{
                    //    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                    //}
              
                    break;

                default: throw new NotImplementedException();
            }
            
        }
    }
}