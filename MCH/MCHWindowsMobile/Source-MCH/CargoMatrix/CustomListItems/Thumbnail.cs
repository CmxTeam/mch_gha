﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CustomListItems
{
    public partial class Thumbnail : SmoothListbox.ListItems.ListItem
    {
        Guid m_guid;
        string m_time;
        Byte m_flag;
        int m_ID;
        private void init()
        {
            InitializeComponent();
            pictureBox1.Left = pictureBox1.Top = 1;
            //pictureBox1.Width = Width - 2;
            //pictureBox1.Height = Height - 4;
            SelectionWidth(2);
            selectionColor = Color.Gold;
            panelBaseLine.Visible = false;//.BackColor = Color.Black;
            m_ID = -1;
            
        }

        public Thumbnail()
        {
            init();
            
        }
        public Thumbnail(CargoMatrix.Communication.DTO.ImageObject imageObject)
        {
            init();
            System.IO.MemoryStream imageStream;
            imageStream = new System.IO.MemoryStream(imageObject.m_rawImage);
            
            try
            {
                System.GC.Collect();
                image = new Bitmap(imageStream);
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (70001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 70001);
            }
            m_time = imageObject.m_time;
            m_ID = imageObject.m_index;
            Location = imageObject.m_location;
            userID = imageObject.m_userid;
            Reason = imageObject.m_reason;
            
            imageStream.Close();
            
        
        }
        public Image image
        {
            set
            {
                pictureBox1.Image = value;
            }
            get
            {
                return pictureBox1.Image;   
            }
        }
        public string Time
        {
            set { m_time = value; }
            get { return m_time; }
        }
        
        public int ID
        { 
            get { return m_ID; }
            set { m_ID = value; }
        }
        public Guid GUID
        {
            set { m_guid = value; }
            get { return m_guid; }
        }
        public string Location
        {
            get;
            set;
        }

        public string userID
        {
            get;
            set;
        }
        public string Reason
        {
            get;
            set;
        }

        public override void Focus(bool focused)
        {
            //base.Focus(focused);
        }

        private void Thumbnail_Resize(object sender, EventArgs e)
        {
            if(Width == 240)
                Width = 68;

        }
    }
}
