﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomListItems
{
    public partial class HeaderItem : UserControl
    {
        public string Label1 { get { return label1.Text; } set { label1.Text = value; } }
        public string Label2 { get { return label2.Text; } set { label2.Text = value; } }
        public string Label3
        {
            get { return label3.Text; }
            set { label3.Text = value; }//label3.EnableScrolling = true;}
        }
        private string LabelProgress { set { labelProgress.Text = value; } }
        public int Counter
        {
            set { ButtonAdd.Value = value; }
            get { return ButtonAdd.Value; }
        }
        public Image Logo { get { return pictureBoxLogo.Image; } set { pictureBoxLogo.Image = value; } }
        public Image ButtonImage { set { ButtonAdd.Image = value; } }
        public Image ButtonPressedImage { get { return ButtonAdd.PressedImage; } set { ButtonAdd.PressedImage = value; } }
        public bool ButtonVisible { get { return ButtonAdd.Visible; } set { ButtonAdd.Visible = value;} }
        public event EventHandler ButtonClick;
        public event EventHandler ItemClick;

        public HeaderItem()
        {
            InitializeComponent();

            this.Click += new EventHandler(HeaderItem_Click);
           
            this.ButtonAdd.Image = CargoMatrix.Resources.Skin.add_btn;
            this.ButtonAdd.PressedImage = CargoMatrix.Resources.Skin.add_btn_over;

            this.label3.Blink = true;

        }

        void HeaderItem_Click(object sender, EventArgs e)
        {
            if (this.ItemClick != null)
                this.ItemClick(this, e);
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null)
                ButtonClick(this, e);
        }

    }
}
