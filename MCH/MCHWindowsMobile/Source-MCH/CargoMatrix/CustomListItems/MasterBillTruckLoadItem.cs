﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using CargoMatrix.Communication.CargoTruckLoad;

namespace CustomListItems
{
    public partial class MasterBillTruckLoadItem : CustomListItems.ExpandableListItem
    {
        ITruckLoadMasterBillItem masterBill;
        
        public event EventHandler OnDamageViewClick;
        public event EventHandler OnMoreClick;

        public ITruckLoadMasterBillItem MasterBill 
        {
            get
            {
                return this.masterBill;
            }
            set
            {
                this.masterBill = value;
            }
        }

        public MasterBillTruckLoadItem(ITruckLoadMasterBillItem masterBill)
        {
            InitializeComponent();

            this.masterBill = masterBill;
            
            this.labelLine3.Location = new System.Drawing.Point(41, 33);

            panelCutoff.Visible = false;

            this.title.Text = masterBill.Reference();
            labelLine2.Text = masterBill.Line2();
            labelLine3.Text = masterBill.Line3();

            if(masterBill.Doors.Length > 0)
            {
                string more = masterBill.Doors.Length > 1 ? ", ..." : string.Empty;

                this.line4.Text = string.Format("DOORs: {0}{1}", masterBill.Doors[0].Door, more);
            }

            

            switch (masterBill.Status)
            {
                case CargoMatrix.Communication.DTO.MasterbillStatus.Open:
                case CargoMatrix.Communication.DTO.MasterbillStatus.Pending:
                    Logo = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case CargoMatrix.Communication.DTO.MasterbillStatus.InProgress:
                    Logo = CargoMatrix.Resources.Skin.status_history;
                    break;
                case CargoMatrix.Communication.DTO.MasterbillStatus.Completed:
                    Logo = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;

            }
            this.panelCutoff.Visible = false;
            this.LabelConfirmation = "Enter mawb number";
            panelIndicators.Flags = masterBill.Flags;
            panelIndicators.PopupHeader = masterBill.Reference();
            this.previousHeight = this.Height;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

        }


        protected override bool ButtonEnterValidation()
        {
            return string.Equals(this.masterBill.MasterBillNumber, TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        protected void pcxDamage_Click(object sender, System.EventArgs e)
        {
            var damageView = this.OnDamageViewClick;

            if (damageView != null)
            {
                damageView(this, EventArgs.Empty);
            }
        }

        void btnMore_Click(object sender, System.EventArgs e)
        {
            var moreClick = this.OnMoreClick;

            if (moreClick != null)
            {
                moreClick(this, EventArgs.Empty);
            }
        }
    }
}
