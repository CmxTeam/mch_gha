﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomListItems
{
    public partial class BottomListItem : UserControl
    {
        string captionLine1;
        string captionLine2;

        public string CaptionLine1
        {
            get { return captionLine1; }
            set 
            {
                captionLine1 = value; 
                this.pcxImage.Refresh();
                this.Scale();
            }
        }

        public string CaptionLine2
        {
            get { return captionLine2; }
            set
            {
                captionLine2 = value;
                this.pcxImage.Refresh();
            }
        }
        
        public BottomListItem()
        {
            this.captionLine1 = string.Empty;
            InitializeComponent();
        }

        void pcxImage_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            this.PaintContent(e);
        }

        protected virtual void PaintContent(System.Windows.Forms.PaintEventArgs e)
        {
            using (Brush brush = new SolidBrush(Color.White))
            {
                var font = new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold);

                
                
                e.Graphics.DrawString(this.CaptionLine1, font, brush, 5, 3);

                e.Graphics.DrawString(this.CaptionLine2, font, brush, 5, (this.pcxImage.Height / 2) + 3);

                font.Dispose();
            }
        }

        public void Scale()
        {
            this.Scale(new SizeF(this.AutoScaleDimensions.Width / 96F, this.AutoScaleDimensions.Height / 96F));
        }
    }
}
