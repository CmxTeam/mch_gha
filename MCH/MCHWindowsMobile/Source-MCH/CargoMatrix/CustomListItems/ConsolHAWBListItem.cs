﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using SmoothListbox.ListItems;
using SmoothListbox;
using CMXExtensions;

namespace CustomListItems
{
    public partial class ConsolHAWBListItem : SmoothListbox.ListItems.ListItem
    {
        public event EventHandler ButtonConditionClick;
        public event EventHandler ButtonBrowseClick;
        public event EventHandler OnViewerClick;
        public event EventHandler OnEnterClick;
        private CargoMatrix.Communication.DTO.IHouseBillItem _hawbItem;
        public CargoMatrix.Communication.DTO.IHouseBillItem HawbItem { get { return _hawbItem; } set { _hawbItem = value; DisplayData(); } }
        protected int previousHeight;
        private Panel panelManualEntry;
        private CargoMatrix.UI.CMXTextBox cmxTextBox1;
        private CargoMatrix.UI.CMXPictureButton buttonEnter;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private System.Windows.Forms.Label labelConfirm;

        bool m_firstTimeSelection = false;

        public string LabelConfirmation { get; set; }


        public ConsolHAWBListItem(CargoMatrix.Communication.DTO.IHouseBillItem hawbItem)
        {
            InitializeComponent();
            this.MouseDown += new MouseEventHandler(ConsolHAWBListItem_MouseDown);
            this.MouseUp += new MouseEventHandler(ConsolHAWBListItem_MouseUp);
            HawbItem = hawbItem;
            //this.labelLine4.Visible = true;
            //this.panelIndicators.Location = new Point(3, this.labelLine4.Bottom);
            //this.panelIndicators.Size = new Size(this.Width - 6, 16);
            //this.Height += panelIndicators.Height;
            this.previousHeight = this.Height;
            this.LabelConfirmation = "Enter housebill No to confirm";
            DisplayData();


        }

        void ConsolHAWBListItem_MouseUp(object sender, MouseEventArgs e)
        {

            if (this.buttonBrowse.Bounds.Contains(e.X, e.Y))
                MessageBox.Show("");
        }

        void ConsolHAWBListItem_MouseDown(object sender, MouseEventArgs e)
        {

        }

        public void DisplayData()
        {
            this.title.Text = HawbItem.Reference();
            labelLine2.Text = HawbItem.Line2();
            labelLine3.Text = HawbItem.Line3();
            if (HawbItem.ScannedPieces > 0 && HawbItem.ScannedPieces != HawbItem.TotalPieces)
                labelLine3.ForeColor = Color.Red;
            else
                labelLine3.ForeColor = Color.Black;
            labelLine4.Text = HawbItem.Line4();
            switch (HawbItem.status)
            {
                case HouseBillStatuses.Open:
                case HouseBillStatuses.Pending:
                    itemPicture.Image = CustomListItemsResource.Clipboard;
                    break;
                case HouseBillStatuses.InProgress:
                    itemPicture.Image = CustomListItemsResource.History;
                    break;
                case HouseBillStatuses.Completed:
                    itemPicture.Image = CustomListItemsResource.Clipboard_Check;
                    break;

            }

            panelIndicators.Flags = HawbItem.Flags;
            panelIndicators.PopupHeader = HawbItem.Reference();
            //this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

        }
        void initializeImages()
        {
            buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonCondition.Image = CargoMatrix.Resources.Skin.damage;
            this.buttonCondition.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = Resources.Graphics.Skin._3dots_over;
        }
        void buttonCondition_Click(object sender, System.EventArgs e)
        {
            if (null != ButtonConditionClick)
                ButtonConditionClick(this, e);
        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            if (null != ButtonBrowseClick)
                ButtonBrowseClick(this, e);
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            if (OnViewerClick != null)
            {
                Cursor.Current = Cursors.WaitCursor;
                OnViewerClick(this, e);
            }
        }
        private void buttonEnter_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok_over;
            buttonEnter.Refresh();

            char[] whiteSpaces = " ".ToCharArray();
            cmxTextBox1.Text = cmxTextBox1.Text.TrimEnd(whiteSpaces);
            cmxTextBox1.Text = cmxTextBox1.Text.TrimEnd(whiteSpaces);
            cmxTextBox1.Text = cmxTextBox1.Text.ToUpper();
            if (HawbItem.HousebillNumber == cmxTextBox1.Text)
            {
                Cursor.Current = Cursors.Default;
                if (OnEnterClick != null)
                    OnEnterClick(this, e);
                buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok;
                buttonEnter.Refresh();

            }
            else
            {

                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Text entered does not match. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok;
                buttonEnter.Refresh();
                cmxTextBox1.SelectAll();
                cmxTextBox1.Focus();
            }


        }
        private void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (this.m_selected)
                        buttonEnter_Click(sender, EventArgs.Empty);
                    break;
            }


        }

        public override void SelectedChanged(bool isSelected)
        {
            m_selected = isSelected;

            base.SelectedChanged(isSelected);

            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
            {


                if (isSelected)
                {
                    /////////////////////////////
                    if (m_firstTimeSelection == false)
                    {
                        m_firstTimeSelection = true;

                        this.SuspendLayout();
                        this.panelManualEntry = new Panel();
                        this.panelManualEntry.Size = new Size(240, 43);
                        this.panelManualEntry.Location = new Point(0, 78);
                        panelManualEntry.SuspendLayout();

                        this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
                        this.buttonEnter = new CargoMatrix.UI.CMXPictureButton();
                        this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
                        this.labelConfirm = new System.Windows.Forms.Label();
                        // cmxTextBox1
                        // 
                        this.cmxTextBox1.Location = new System.Drawing.Point(3, 14);
                        //this.cmxTextBox1.Name = "cmxTextBox1";
                        this.cmxTextBox1.Size = new System.Drawing.Size(147, 28);
                        this.cmxTextBox1.TabIndex = 2;
                        this.cmxTextBox1.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
                        this.cmxTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmxTextBox1_KeyDown);
                        this.cmxTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
                        // 
                        // buttonEnter
                        // 
                        this.buttonEnter.Location = new System.Drawing.Point(152, 14);
                        //this.buttonEnter.Name = "buttonEnter";
                        this.buttonEnter.Size = new System.Drawing.Size(40, 28);
                        this.buttonEnter.Click += new System.EventHandler(this.buttonEnter_Click);
                        this.buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok;
                        this.buttonEnter.PressedImage = Resources.Graphics.Skin.manual_entry_ok_over;
                        // 
                        // buttonCancel
                        // 
                        this.buttonCancel.Location = new System.Drawing.Point(195, 14);
                        //this.buttonEnter.Name = "buttonEnter";
                        this.buttonCancel.Size = new System.Drawing.Size(40, 28);
                        this.buttonCancel.Click += new EventHandler(buttonCancel_Click);
                        this.buttonCancel.Image = Resources.Graphics.Skin.manual_entry_cancel;
                        this.buttonCancel.PressedImage = Resources.Graphics.Skin.manual_entry_cancel_over;
                        // 
                        // labelConfirm
                        // 
                        this.labelConfirm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
                        this.labelConfirm.ForeColor = System.Drawing.Color.Black;
                        this.labelConfirm.Location = new System.Drawing.Point(3, 0);
                        //this.labelConfirm.Name = "labelConfirm";
                        this.labelConfirm.Size = new System.Drawing.Size(234, 12);
                        this.labelConfirm.Text = LabelConfirmation;



                        this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

                        this.panelManualEntry.Controls.Add(this.labelConfirm);
                        this.panelManualEntry.Controls.Add(this.cmxTextBox1);
                        this.panelManualEntry.Controls.Add(this.buttonEnter);
                        this.panelManualEntry.Controls.Add(this.buttonCancel);

                        this.Controls.Add(panelManualEntry);
                        this.panelManualEntry.ResumeLayout();
                        this.ResumeLayout();
                    }
                    ////////////////////////////////
                    labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = true;

                    Cursor.Current = Cursors.Default;

                    this.Height = panelManualEntry.Bottom + 4;
                    this.panelManualEntry.BackColor = this.BackColor;
                    cmxTextBox1.SelectAll();
                    cmxTextBox1.Focus();
                    //buttonEnterClicked = false;
                }
                else
                {
                    //if (this.ClientRectangle.Width == 480)
                    //    this.Height = 112;// previousHeight;
                    //else
                    //    this.Height = 56;
                    Height = previousHeight;

                    if (m_firstTimeSelection)
                        labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = false;
                }

            }
            for (Control control = this.Parent; control != null; control = control.Parent)
            {
                if (control is SmoothListbox.SmoothListBoxBase)
                {
                    (control as SmoothListbox.SmoothListBoxBase).RefreshScroll();
                    break;
                }
            }
        }
        void buttonCancel_Click(object sender, EventArgs e)
        {
            cmxTextBox1.Text = string.Empty;
            //SelectedChanged(false);
        }
    }
}
