﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using SmoothListbox;
using System.Threading;
using System.Linq;

namespace CustomListItems
{
    public class ComboBoxEventArgs : EventArgs
    {
        public ComboBoxEventArgs(int id, bool selected)
        {
            SubItemID = id;
            IsSelected = selected;
        }
        public int SubItemID;
        public bool IsSelected;
        public bool Cancel = false;
    }
    public partial class ComboBox : ListItem
    {
        protected List<ComboBoxItemData> m_items;
        public List<ComboBoxItemData> Items { get { return m_items; } }
        bool m_firstTimeSelection = false;
        public event EventHandler<ComboBoxEventArgs> SubItemSelecting;
        protected SmoothListBoxBase m_smoothList;
        protected int m_previousHeight;
        public delegate void ComboBoxLayoutChangedHandler(ComboBox sender, int subItemId, bool comboIsEmpty);
        public event ComboBoxLayoutChangedHandler LayoutChanged;
        public event EventHandler SelectionChanged;
        public string ContainerName { get; set; }

        public bool IsSelectable
        {
            get { return m_smoothList.IsSelectable; }
            set { m_smoothList.IsSelectable = value; }
        }

        public bool ReadOnly
        {
            set
            {
                if (value)
                {
                    //IsSelectable = false;
                    buttonDelte.Visible = false;
                    buttonCollapse.Location = buttonDelte.Location;
                    foreach (var item in m_smoothList.Items)
                    {
                        (item as ComboBoxItem).ReadOnly = value;
                    }
                }
                else
                {
                    //IsSelectable = true;
                    buttonDelte.Visible = true;
                    buttonCollapse.Left -= 34;
                    foreach (var item in m_smoothList.Items)
                    {
                        (item as ComboBoxItem).ReadOnly = value;
                    }
                }
            }
        }
        public IEnumerable<int> PieceIds
        {
            get
            {
                foreach (var item in m_items)
                    yield return item.id;
            }
        }
        public IEnumerable<int> SelectedIds
        {
            get
            {
                if (m_firstTimeSelection == false)
                    if (IsSelected)
                        foreach (var item in m_items)
                            yield return item.id;
                    else yield break;
                else
                {
                    foreach (var item in m_smoothList.SelectedItems)
                        if (item is ComboBoxItem)
                            yield return (item as ComboBoxItem).ID;
                }

            }
        }

        public IEnumerable<int> CheckedIds
        {
            get
            {
                //if (m_firstTimeSelection == false)
                //{
                foreach (var item in m_items)
                    if (item.isChecked)
                        yield return item.id;
                //}
                //else
                //{
                //    foreach (var item in m_smoothList.SelectedItems)
                //        if (item is ComboBoxItem)
                //            yield return (item as ComboBoxItem).ID;
                //}

            }
        }
        private bool isExpanded = false;
        public bool SubItemSelected { get; set; }
        public bool SubItemChecked
        {
            get
            {
                foreach (var item in m_items)
                    if (item.isChecked)
                        return true;
                return false;
            }
        }

        public int ItemsCount { get { return this.m_smoothList.ItemsCount; } }

        public string ComboText
        {
            get
            {
                return labelHeading.Text;
            }
        }

        public Color TextColor
        {
            get
            {
                return this.labelHeading.ForeColor;
            }
            set
            {
                this.labelHeading.ForeColor = value;
            }
        }

        public Color Line2Color
        {
            get
            {
                return this.title.ForeColor;
            }
            set
            {
                this.title.ForeColor = value;
            }
        }

        public string Line2
        {
            get
            {
                return this.title.Text;
            }
            set
            {
                this.title.Text = value;
            }
        }

        public bool Expandable
        {
            get
            {
                return this.buttonCollapse.Visible;
            }
            set
            {
                this.buttonCollapse.Visible = value;
            }
        }


        private SmoothListBoxBase parentListBox;
        public ComboBox(int id, string text, string line2, Image image, List<ComboBoxItemData> items)
        {

            InitializeComponent();
            ContainerName = "forklift";
            this.ID = id;
            m_previousHeight = this.Height;
            m_items = items;
            itemPicture.Image = image;
            labelHeading.Text = text;
            this.title.Text = line2;
            pictureBoxDottedLine.Image = CustomListItemsResource.DottedLineVertical;
            this.selectionColor = Color.Gainsboro;
            SubItemSelected = false;
            m_smoothList = new SmoothListBoxBase();
            //FirstTimeLoad();
        }

        public void Unload()
        {
            foreach (var item in m_smoothList.SelectedItems)
            {
                RemoveSubItem(item);
            }
        }

        void m_smoothList_ListItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem == null)
                return;
            ComboBoxEventArgs args = new ComboBoxEventArgs((listItem as ComboBoxItem).ID, isSelected);

            if (this.SubItemSelecting != null)
            {
                this.SubItemSelecting(this, args);
            }

            if (args.Cancel == true &&
                isSelected == true)
            {
                this.m_smoothList.selectedItemsMap[listItem] = false;
                ((ComboBoxItem)listItem).SelectedChanged(false);
                return;
            }

            this.m_smoothList.selectedItemsMap[listItem] = isSelected;
            ((ComboBoxItem)listItem).SelectedChanged(isSelected);

            if (m_smoothList.SelectedItems.Count == m_smoothList.ItemsCount)
            {
                SubItemSelected = true;
                this.SelectedChanged(true);
                //if (parentListBox == null)
                //    FindParentListBox();
                //parentListBox.selectedItemsMap[this] = true;
                if (SelectionChanged != null)
                    SelectionChanged(this, new EventArgs());
                return;
            }
            if (m_smoothList.SelectedItems.Count == 0)
            {
                SubItemSelected = false;
                this.SelectedChanged(false);
                if (parentListBox == null)
                    FindParentListBox();
                parentListBox.selectedItemsMap[this] = false;

                if (SelectionChanged != null)
                    SelectionChanged(this, new EventArgs());
                return;
            }
            if (m_smoothList.SelectedItems.Count > 0)
            {
                SubItemSelected = true;

                base.Focus(false);

                if (SelectionChanged != null)
                    SelectionChanged(this, new EventArgs());
            }
        }


        public override void SelectedChanged(bool isSelected)
        {
            base.Focus(isSelected);
            base.SelectedChanged(isSelected);
            selectionFixer(isSelected);

            if (isSelected)
            {
                SubItemSelected = true;
                m_smoothList.SelectAll();
            }
            else
            {
                SubItemSelected = false;
                m_smoothList.SelectNone();
            }
            Application.DoEvents();
            //if (SelectionChanged != null)
            //   SelectionChanged(this, new EventArgs());
        }
        private void selectionFixer(bool selected)
        {
            if (parentListBox == null)
                FindParentListBox();
            if (parentListBox == null)
                return;

            parentListBox.selectedItemsMap[this] = selected;

        }
        private void Collapse()
        {
            //pictureBoxCombo.Image = CustomListItemsResource.Expand;
            //Height = m_previousHeight;
            AnimateCombo(Height, m_previousHeight);

            pictureBoxDottedLine.Visible = false;
            this.buttonCollapse.Image = CustomListItemsResource.Collapse_Down;
            this.buttonCollapse.PressedImage = CustomListItemsResource.Collapse_Down;
        }

        private void AnimateCombo(int start, int end)
        {
            int step = (end - start) / 4;
            for (int i = 0; i < 4; i++)
            {
                this.Height += step;
                RefershLayout();
                Application.DoEvents();
            }
            this.Height = end;
        }

        private void Expand()
        {
            if (m_firstTimeSelection == false)
                FirstTimeLoad();
            //pictureBoxCombo.Image = CustomListItemsResource.Collapse;
            //Height = m_previousHeight + m_smoothList.Height + 5;
            AnimateCombo(Height, m_previousHeight + m_smoothList.Height + 5);
            this.buttonCollapse.Image = CustomListItemsResource.Collapse_Up;
            this.buttonCollapse.PressedImage = CustomListItemsResource.Collapse_Up;
            pictureBoxDottedLine.Visible = true;
            ScrollIntoView();
        }

        void buttonCollapse_Click(object sender, System.EventArgs e)
        {

            isExpanded = !isExpanded;
            if (isExpanded)
                Expand();
            else
                Collapse();
            RefershLayout();
        }

        private void RefershLayout()
        {
            if (parentListBox == null)
                FindParentListBox();
            parentListBox.LayoutItems();
            parentListBox.RefreshScroll();
        }

        private void FindParentListBox()
        {
            Control control = this.Parent;
            while (control != null)
            {
                if (control is SmoothListBoxBase)
                {
                    if ((control as SmoothListBoxBase).selectedItemsMap.ContainsKey(this))
                    {
                        parentListBox = control as SmoothListBoxBase;
                        break;
                    }
                }
                control = control.Parent;
            }
        }
        private void ScrollIntoView()
        {
            if (parentListBox == null)
                FindParentListBox();
            parentListBox.MoveControlToTop(this);
        }

        protected void FirstTimeLoad()
        {
            if (m_items != null)
            {
                if (m_firstTimeSelection == false)
                {
                    m_firstTimeSelection = true;

                    this.SuspendLayout();
                    Cursor.Current = Cursors.WaitCursor;
                    this.m_smoothList.ListItemClicked += new ListItemClickedHandler(m_smoothList_ListItemClicked);
                    m_smoothList.MultiSelectEnabled = true;
                    m_smoothList.UnselectEnabled = true;

                    bool isItemSelectable = true;

                    if (m_items.Count > 0)
                    {
                        int i = 0;
                        for (; i < m_items.Count - 1; i++)
                        {
                            isItemSelectable = m_items[i].IsSelectable == false ? false : IsSelectable;
                            ComboBoxItem item = new ComboBoxItem(m_items[i].name, m_items[i].location, m_items[i].id, false, isItemSelectable, m_items[i].isChecked);
                            //item.ReadOnly = !buttonDelte.Visible;

                            item.ReadOnly = m_items[i].isReadonly;

                            //if (buttonDelte.Visible)
                            //{

                            //}
                            //else
                            //{
                            //    item.ReadOnly = m_items[i].isReadonly;
                            //}

                            item.ButtonClick += new EventHandler(item_ButtonClick);
                            m_smoothList.AddItem2(item);
                            if (IsSelectable)
                            {
                                if (m_items[i].isChecked == true)
                                {
                                    item.SelectedChanged(true);
                                }
                                else

                                    item.SelectedChanged(IsSelected);
                            }
                        }

                        isItemSelectable = m_items[i].IsSelectable == false ? false : IsSelectable;
                        ComboBoxItem lastitem = new ComboBoxItem(m_items[i].name, m_items[i].location, m_items[i].id, true, isItemSelectable, m_items[i].isChecked);


                        //if (buttonDelte.Visible)
                        //    lastitem.ReadOnly = m_items[i].isReadonly;
                        //else
                        //    lastitem.ReadOnly = true;

                        lastitem.ReadOnly = m_items[i].isReadonly;

                        lastitem.ButtonClick += new EventHandler(item_ButtonClick);
                        m_smoothList.AddItem2(lastitem);
                        if (IsSelectable)
                        {
                            if (m_items[i].isChecked == true)
                            {
                                lastitem.SelectedChanged(true);
                            }
                            else

                                lastitem.SelectedChanged(IsSelected);
                        }
                    }



                    m_smoothList.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                    m_smoothList.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;



                    m_smoothList.Width = ClientSize.Width - 12;
                    m_smoothList.Left = 16;
                    m_smoothList.Top = m_previousHeight;

                    m_smoothList.LayoutItems();

                    m_smoothList.Height = m_smoothList.itemsPanel.Height;

                    Controls.Add(m_smoothList);

                    this.ResumeLayout();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        void item_ButtonClick(object sender, EventArgs e)
        {
            string message = string.Format("Are you sure you want to remove {0} from {1}?", (sender as ComboBoxItem).Name, ContainerName);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(message, "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.YesNo, DialogResult.No))
            {
                RemoveSubItem(sender);
            }
        }

        private void RemoveSubItem(object sender)
        {
            m_smoothList.Height -= (sender as Control).Height;
            m_smoothList.RemoveItem(sender as Control);
            this.Height -= (sender as Control).Height;

            //removeItem Data
            m_items.RemoveAll(itemData => itemData.id == (sender as ComboBoxItem).ID);

            if (m_smoothList.ItemsCount == 0)
            {
                if (LayoutChanged != null)
                {
                    m_smoothList_ListItemClicked(null, null, false);
                    LayoutChanged(this, (sender as ComboBoxItem).ID, true);
                }
            }
            else
            {
                if ((sender as ComboBoxItem).IsLast)
                    (m_smoothList.Items[m_smoothList.ItemsCount - 1] as ComboBoxItem).IsLast = true;
                if (LayoutChanged != null)
                {
                    LayoutChanged(this, (sender as ComboBoxItem).ID, false);
                    m_smoothList_ListItemClicked(null, null, false);
                }
            }
        }
        public void RemoveSubItem(int id)
        {
            ComboBoxItem matchItem = null;
            foreach (ComboBoxItem item in m_smoothList.Items)
                if (item.ID == id)
                {
                    matchItem = item;
                    break;
                }
            if (matchItem == null)
                return;
            //removeItem Data
            m_items.RemoveAll(itemData => itemData.id == id);

            m_smoothList.Height -= matchItem.Height;
            m_smoothList.RemoveItem(matchItem);
            this.Height -= matchItem.Height;
            if (m_smoothList.ItemsCount == 0)
            {
                if (LayoutChanged != null)
                {
                    m_smoothList_ListItemClicked(null, null, false);
                    LayoutChanged(this, matchItem.ID, true);
                }
            }
            else
            {
                if (matchItem.IsLast)
                    (m_smoothList.Items[m_smoothList.ItemsCount - 1] as ComboBoxItem).IsLast = true;
                if (LayoutChanged != null)
                {
                    LayoutChanged(this, matchItem.ID, false);
                    m_smoothList_ListItemClicked(null, null, false);
                }
            }
        }
        private void ComboBox_Resize(object sender, EventArgs e)
        {
            if (m_smoothList != null)
            {
                m_smoothList.Width = ClientSize.Width - 8;
                m_smoothList.Left = 4;
            }
        }

        private void buttonDelte_Click(object sender, EventArgs e)
        {
            string message = string.Format("Are you sure you want to remove {0} from {1}?", labelHeading.Text, ContainerName);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(message, "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.YesNo, DialogResult.No))
            {
                if (LayoutChanged != null)
                    LayoutChanged(this, -1, true);
            }

        }

        public void UpdateSubItemText(string text, int subItemID)
        {
            var subItem = (
                                from item in this.m_smoothList.Items.OfType<ComboBoxItem>()
                                where item.ID == subItemID
                                select item
                           ).First();

            subItem.Text = text;
        }

        public void UpdateSubItemIcon(int subItemID, bool isChecked)
        {
            var subItem = (
                                from item in this.m_smoothList.Items.OfType<ComboBoxItem>()
                                where item.ID == subItemID
                                select item
                           ).First();

            subItem.IsChecked = isChecked;
            subItem.RefreshIcon();
        }

        public void UpdateSubItemsIcon(bool isChecked)
        {
            foreach (ComboBoxItem item in this.m_smoothList.Items)
            {
                item.IsChecked = isChecked;
                item.RefreshIcon();
            }

            foreach (ComboBoxItemData item in this.m_items)
            {
                item.isChecked = isChecked;
            }
        }

        public void UpdateSubItemsIcon(bool isChecked, IEnumerable<CustomListItems.ComboBoxItemData> items)
        {
            var its = from dataItem in items
                      join comboItem in this.m_smoothList.Items.OfType<ComboBoxItem>() on dataItem.id equals comboItem.ID into combos
                      from combo in combos.DefaultIfEmpty()
                      select new
                      {
                          ComboItem = combo,
                          DataItem = dataItem
                      };

            foreach (var item in its)
            {
                item.DataItem.isChecked = isChecked;

                if (item.ComboItem != null)
                {
                    item.ComboItem.IsChecked = isChecked;
                    item.ComboItem.SelectedChanged(isChecked);
                    item.ComboItem.RefreshIcon();
                }
            }
        }

        public void UpdateSubItemsIconsSelectable(bool selectable, IEnumerable<CustomListItems.ComboBoxItemData> items)
        {
            var its = from dataItem in items
                      join comboItem in this.m_smoothList.Items.OfType<ComboBoxItem>() on dataItem.id equals comboItem.ID into combos
                      from combo in combos.DefaultIfEmpty()
                      select new
                      {
                          ComboItem = combo,
                          DataItem = dataItem
                      };

            foreach (var item in its)
            {
                item.DataItem.IsSelectable = selectable;

                if (item.ComboItem != null)
                {
                    item.ComboItem.Selectable = selectable;
                    item.ComboItem.RefreshIcon();
                }
            }
        }

        public void UpdateSubItemsIcon(bool isChecked, int itemsCount)
        {
            var items = from dataItem in this.m_items.Take(itemsCount)
                        join comboItem in this.m_smoothList.Items.OfType<ComboBoxItem>() on dataItem.id equals comboItem.ID into combos
                        from combo in combos.DefaultIfEmpty()
                        select new
                        {
                            ComboItem = combo,
                            DataItem = dataItem
                        };

            foreach (var item in items)
            {
                item.DataItem.isChecked = isChecked;
                if (item.ComboItem != null)
                {
                    item.ComboItem.IsSelected = isChecked;
                    item.ComboItem.SelectedChanged(isChecked);
                    item.ComboItem.RefreshIcon();
                }
            }
        }

        public void UpdateSubItemState<R>(int subItemID, bool isChecked) where R : ComboBoxItemData
        {
            int checkedItemsCount = 0;
            foreach (R item in this.m_items)
            {
                if (item.id == subItemID)
                {
                    item.isChecked = isChecked;

                }
                if (item.isChecked)
                    checkedItemsCount++;
            }

            if (checkedItemsCount == m_items.Count)
                TextColor = Color.Green;
            else if (checkedItemsCount == 0)
                TextColor = Color.Black;
            else
                TextColor = Color.Red;

            foreach (ComboBoxItem item in this.m_smoothList.Items)
                if (item.ID == subItemID)
                {
                    item.IsChecked = isChecked;
                    item.RefreshIcon();
                    break;
                }
        }

        public void SelectSubitems(int count)
        {
            if (count == Items.Count)
            {
                this.SelectedChanged(true);
                SubItemSelected = true;
                return;
            }
            if(count == 0)
            {
                this.SelectedChanged(false);
                    return;
            }
            if (m_firstTimeSelection == false)
                FirstTimeLoad();
            foreach (var item in m_smoothList.Items.OfType<ComboBoxItem>().Take<ComboBoxItem>(count))
            {
                item.SelectedChanged(true);
            }
            SubItemSelected = true;
        }
    }

    public class ComboBox<T> : ComboBox
    {
        T data;

        public T Data
        {
            get { return data; }
            set { data = value; }
        }

        public ComboBox(T data, int id, string text, string line2, Image image, List<ComboBoxItemData> items)
            : base(id, text, line2, image, items)
        {
            this.data = data;
        }
    }


    public class ComboBoxItemData
    {
        public ComboBoxItemData(string Name, string loc, int id, bool isreadonly)
        {
            name = Name;
            location = loc;
            this.id = id;
            this.isReadonly = isreadonly;
            this.isChecked = false;
        }
        public ComboBoxItemData(string Name, string loc, int id)
            : this(Name, loc, id, false) { }
        public ComboBoxItemData(string Name, string loc)
            : this(Name, loc, -1, false) { }

        public string name;
        public string location;
        public int id;
        public bool isReadonly;
        public bool isChecked = false;
        public int locationId = 0;
        public bool IsSelectable = true;

    }
    public class ComboBoxItemData<T> : ComboBoxItemData
    {
        public T Data { get; set; }
        public ComboBoxItemData(T data, string Name, string loc, int id, bool isreadonly)
            : base(Name, loc, id, isreadonly)
        {
            Data = data;
        }
        public ComboBoxItemData(T data, string Name, string loc, int id)
            : base(Name, loc, id, false) { Data = data; }
        public ComboBoxItemData(T data, string Name, string loc)
            : base(Name, loc, -1, false) { Data = data; }
    }
}
