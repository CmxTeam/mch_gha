﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CustomListItems;

namespace CustomListItems
{
    public partial class ReportItem : ListItem, IExpandItemData
    {
        public ReportItem()
        {
            InitializeComponent();
            IsSelectable = false;
            IsReadonly = true;
            panelBaseLine.Visible = false;
            itemPicture.Image = CustomListItemsResource.DottedLineFullDisabled;

        }
        public ReportItem(string line1):this()
        {
            TitleLine = line1;
        }

        #region IExpandItemData Members


        public string TitleLine
        {
            get
            {
                return label1.Text;
            }
            set
            {
                label1.Text = value;
            }
        }

        public string DescriptionLine
        {
            get
            {
                return null;
            }
            set
            {
            }
        }
        public bool IsSelectable{get;set;}

        public bool IsReadonly
        {
            get;
            set;
        }

        public bool IsChecked
        {
            get;
            set;
        }
        private bool isLast;
        public bool IsLast
        {
            get { return isLast; }
            set
            {
                isLast = value;
                if (value)
                    itemPicture.Image = CustomListItemsResource.DottedLineHalfDisabled;
                else
                    itemPicture.Image = CustomListItemsResource.DottedLineFullDisabled;
            }

        }

        public bool Expandable
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IExpandItemData ParentItemData
        {
            get;
            set;
        }

        public void RefreshView()
        {
            
        }


        public new bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                
            }
        }

        #endregion
    }
}