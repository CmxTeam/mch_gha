﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomListItems
{
    [Obsolete("use ULDListItem_new")]
    public partial class ULDListItem : SmoothListbox.ListItems.ListItem
    {
        public event EventHandler ButtonEditClick;
        public event EventHandler ButtonDeleteClick;
        public event EventHandler ButtonEnterClick;
        public event EventHandler ButtonBrowseClick;
        public bool Expandable { get; set; }
        public string LabelConfirm { set { labelConfirm.Text = value; } }
        public string UldType
        {
            get { return uld.ULDType; }

        }
        public int UldTypeID
        {
            get { return uld.TypeID; }
        }
        public int ULDID { get { return uld.ID; } }
        public void SetULDType(int typeId, string type)
        {
            uld.TypeID = typeId;
            uld.ULDType = type;
            pictureBox1.Image = CargoMatrix.Resources.Skin.Freight_Car;
            FormatLine1();
            CheckForLooseULD();

        }
        public string UldNumber
        {
            get { return uld.ULDNo; }
            set
            {
                uld.ULDNo = value;
                FormatLine1();
            }
        }
        public string UldLocation { get { return uldLoc; } set { uldLoc = value; if (value != string.Empty)label2.Text = string.Format("LOC: {0} ({1})", uldLoc, uldPart); } }
        public string UldPart { get { return uldPart; } set { uldPart = value; if (value != string.Empty)label2.Text = string.Format("LOC: {0} ({1})", uldLoc, uldPart); } }
        public Image Logo { get { return pictureBox1.Image; } set { pictureBox1.Image = value; } }
        public ULDListItem(CargoMatrix.Communication.DTO.IULD uld)
            : this(uld, false)
        {
        }
        public ULDListItem(CargoMatrix.Communication.DTO.IULD uld, bool expandable)
        {
            InitializeComponent();
            this.uld = uld;
            FormatLine1();
            this.Expandable = expandable;
        }
        public string Line2 { set { label2.Text = value; } }
        public string Line1 { get { return label1.Text; } set { label1.Text = value; } }
        public override void SelectedChanged(bool isSelected)
        {
            base.SelectedChanged(isSelected);

            if (Expandable)
            {
                Expand(isSelected);
            }

        }

        protected CargoMatrix.UI.CMXTextBox cmxTextBox1;
        protected CargoMatrix.UI.CMXPictureButton buttonEnter;
        protected CargoMatrix.UI.CMXPictureButton buttonCancel;
        protected CargoMatrix.UI.CMXPictureButton buttonBrowse;
        protected System.Windows.Forms.Label labelConfirm;
        protected bool firstTimeSelection = true;
        protected CargoMatrix.Communication.DTO.IULD uld;
        //private string uldType = string.Empty, uldNumber = string.Empty; 

        private string uldLoc = string.Empty, uldPart = string.Empty;
        private void CheckForLooseULD()
        {
            if (uld.ULDType.Equals(CustomListItemsResource.Text_Loose, StringComparison.OrdinalIgnoreCase))
            {
                if (null != cmxTextBox1)
                {
                    cmxTextBox1.Enabled = false;
                    cmxTextBox1.TextBoxBackColor = Color.LightGray;
                    this.labelConfirm.Text = CustomListItemsResource.Text_LooseConfirm;

                }
            }
            else
                if (null != cmxTextBox1)
                {
                    cmxTextBox1.Enabled = true;
                    cmxTextBox1.TextBoxBackColor = Color.White;

                    this.labelConfirm.Text = CustomListItemsResource.Text_UldConfirm;
                }
        }
        SmoothListbox.SmoothListBoxBase _parentList;
        private SmoothListbox.SmoothListBoxBase parentList
        {
            get
            {
                if (_parentList == null)
                    for (Control control = this.Parent; control != null; control = control.Parent)
                        if (control is SmoothListbox.SmoothListBoxBase)
                        {
                            _parentList = (control as SmoothListbox.SmoothListBoxBase);
                            break;
                        }
                return _parentList;
            }
        }
        private void FormatLine1()
        {
            if (uld.ULDNo != string.Empty)
            {

                if (uld.ULDType.Length > 8 || uld.ULDNo.Length > 8 || uld.ULDType.Length + uld.ULDNo.Length > 16)
                {
                    label1.Text = uld.ULDType + " - " + Environment.NewLine + uld.ULDNo;
                    label1.Height = 26;
                    label1.Top = 2;
                    label2.Top = 28;
                }
                else
                {
                    label1.Text = uld.ULDType + " - " + uld.ULDNo;
                    label1.Height = 16;
                    label1.Top = 7;
                    label2.Top = 23;
                }
            }
            else
            {
                label1.Text = uld.ULDType;
                label1.Height = 16;
                label1.Top = 7;
                label2.Top = 23;
            }
            AutoScaleDimensions = new SizeF(96F, 96F);
            AutoScaleMode = AutoScaleMode.Dpi;

        }
        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (ButtonEditClick != null)
                ButtonEditClick(this, e);
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ButtonDeleteClick != null)
                ButtonDeleteClick(this, e);
        }
        private void Expand(bool isSelected)
        {
            if (isSelected)
            {
                if (firstTimeSelection)
                {

                    InitializeAdditionalComponents();
                    firstTimeSelection = false;

                }
                ////////////////////////////////
                labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = true;

                Cursor.Current = Cursors.Default;

                this.Height = 86;
                cmxTextBox1.SelectAll();
                cmxTextBox1.Focus();

                parentList.MoveControlToTop(this);
                parentList.RefreshScroll();
            }
            else
            {
                Height = 44;
                if (!firstTimeSelection)
                    labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = false;
            }
            AutoScaleDimensions = new SizeF(96F, 96F);
            AutoScaleMode = AutoScaleMode.Dpi;
        }

        private void InitializeAdditionalComponents()
        {
            this.SuspendLayout();

            this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
            this.buttonEnter = new CargoMatrix.UI.CMXPictureButton();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.labelConfirm = new System.Windows.Forms.Label();
            // cmxTextBox1
            // 
            this.cmxTextBox1.Location = new System.Drawing.Point(4, 54);
            this.cmxTextBox1.Size = new System.Drawing.Size(106, 28);
            this.cmxTextBox1.TabIndex = 2;
            this.cmxTextBox1.KeyDown += new KeyEventHandler(cmxTextBox1_KeyDown);
            this.cmxTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            CheckForLooseULD();

            // 
            // buttonEnter
            // 
            this.buttonEnter.Location = new System.Drawing.Point(112, 54);
            //this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(40, 28);
            this.buttonEnter.Click += new EventHandler(buttonEnter_Click);
            this.buttonEnter.SizeMode = PictureBoxSizeMode.StretchImage;
            this.buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok;
            this.buttonEnter.PressedImage = Resources.Graphics.Skin.manual_entry_ok_over;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(154, 54);
            //this.buttonEnter.Name = "buttonEnter";
            this.buttonCancel.Size = new System.Drawing.Size(40, 28);
            this.buttonCancel.Click += new EventHandler(buttonCancel_Click);
            this.buttonCancel.SizeMode = PictureBoxSizeMode.StretchImage;
            this.buttonCancel.Image = Resources.Graphics.Skin.manual_entry_cancel;
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.manual_entry_cancel_over;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(196, 54);
            //this.buttonBrowse.Name = "buttonEnter";
            this.buttonBrowse.Size = new System.Drawing.Size(40, 28);
            this.buttonBrowse.Click += new EventHandler(buttonBrowse_Click);
            this.buttonBrowse.SizeMode = PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = Resources.Graphics.Skin._3dots_over;
            // 
            // labelConfirm
            // 
            this.labelConfirm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelConfirm.ForeColor = System.Drawing.Color.Black;
            this.labelConfirm.Location = new System.Drawing.Point(3, 40);
            this.labelConfirm.Size = new System.Drawing.Size(232, 12);

            this.Controls.Add(this.labelConfirm);
            this.Controls.Add(this.cmxTextBox1);
            this.Controls.Add(this.buttonEnter);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonBrowse);


            this.ResumeLayout();
            parentList.RefreshScroll();

        }

        void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonEnter_Click(null, null);
            }
        }

        void buttonBrowse_Click(object sender, EventArgs e)
        {
            if (ButtonBrowseClick != null)
                ButtonBrowseClick(this, e);
        }

        void buttonCancel_Click(object sender, EventArgs e)
        {
            this.SelectedChanged(false);
            parentList.Reset();
        }

        void buttonEnter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            char[] whiteSpaces = " ".ToCharArray();
            cmxTextBox1.Text = cmxTextBox1.Text.TrimEnd(whiteSpaces);
            cmxTextBox1.Text = cmxTextBox1.Text.TrimStart(whiteSpaces);
            cmxTextBox1.Text = cmxTextBox1.Text.ToUpper();
            if (UldNumber.ToUpper() == cmxTextBox1.Text)
            {
                Cursor.Current = Cursors.Default;
                if (ButtonEnterClick != null)
                    ButtonEnterClick(this, e);
                cmxTextBox1.Text = string.Empty;

            }
            else
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Text entered does not match. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                cmxTextBox1.SelectAll();
                cmxTextBox1.Focus();
            }
        }
    }
}
