﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.CargoTruckLoad;
using System.Drawing;
using CMXExtensions;

namespace CustomListItems
{
    public class TruckLoadListItemRendered : ExpandableRenderListitem<ITruckLoadMasterBillItem>, SmoothListbox.ISmartListItem
    {
        private CargoMatrix.UI.CMXPictureButton btnDamageCapture;
        private CargoMatrix.UI.CMXPictureButton btnMoreDetails;
        private CustomUtilities.IndicatorPanel pnlIndicators;
        private CargoMatrix.UI.CMXPictureButton btnDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 pcxLogo;

        public event EventHandler OnViewerClick;
        public event EventHandler OnDamageCaptureClick;
        public event EventHandler OnMoreDetailsClick;
        
        public TruckLoadListItemRendered(ITruckLoadMasterBillItem item)
            : base(item)
        {
            this.SuspendLayout();

            this.BackColor = Color.White;

            this.ResumeLayout();

            this.RetentionLevel = 2;
        }
        

        protected override void InitializeControls()
        {
            this.btnDetails = new CargoMatrix.UI.CMXPictureButton();
            this.pnlIndicators = new CustomUtilities.IndicatorPanel();
            this.pcxLogo = new OpenNETCF.Windows.Forms.PictureBox2();
            this.btnMoreDetails = new CargoMatrix.UI.CMXPictureButton();
            this.btnDamageCapture = new CargoMatrix.UI.CMXPictureButton();

            var rHeight = this.CurrentAutoScaleDimensions.Height;
            var rWidth = this.CurrentAutoScaleDimensions.Width;


            // 
            // pictureBox1
            // 

            this.pcxLogo.Location = new System.Drawing.Point((int)(3 * rWidth / 96F), (int)(3 * rHeight / 96F));
            this.pcxLogo.Name = "pcxLogo";
            this.pcxLogo.Size = new System.Drawing.Size((int)(24 * rWidth / 96F), (int)(24 * rHeight / 96F));
            this.pcxLogo.TransparentColor = System.Drawing.Color.White;
            this.pcxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            // 
            // pnlIndicators
            // 
            this.pnlIndicators.Location = new System.Drawing.Point((int)(3 * rWidth / 96F), (int)(99 * rHeight / 96F));
            this.pnlIndicators.Name = "pnlIndicators";
            this.pnlIndicators.Size = new System.Drawing.Size((int)(237 * rWidth / 96F), (int)(16 * rHeight / 96F));

            // 
            // btnDetails
            // 
            this.btnDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.btnDetails.Location = new System.Drawing.Point((int)(3 * rWidth / 96F), (int)(49 * rHeight / 96F));
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.btnDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            this.btnDetails.Size = new System.Drawing.Size((int)(32 * rWidth / 96F), (int)(32 * rHeight / 96F));
            this.btnDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetails.TransparentColor = System.Drawing.Color.White;
            this.btnDetails.Click += new EventHandler(btnDetails_Click);

            // 
            // btnDamageCapture
            // 
            this.btnDamageCapture.Image = CargoMatrix.Resources.Skin.damage;
            this.btnDamageCapture.Location = new System.Drawing.Point((int)(204 * rWidth / 96F), (int)(21 * rHeight / 96F));
            this.btnDamageCapture.Name = "btnDamageCapture";
            this.btnDamageCapture.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.btnDamageCapture.Size = new System.Drawing.Size((int)(32 * rWidth / 96F), (int)(32 * rHeight / 96F));
            this.btnDamageCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDamageCapture.TransparentColor = System.Drawing.Color.White;
            this.btnDamageCapture.Click += new EventHandler(btnDamageCapture_Click);

            // 
            // btnMoreDetails
            // 
            this.btnMoreDetails.Image = CargoMatrix.Resources.Skin._3dots_btn;
            this.btnMoreDetails.Location = new System.Drawing.Point((int)(204 * rWidth / 96F), (int)(59 * rHeight / 96F));
            this.btnMoreDetails.Name = "btnMoreDetails";
            this.btnMoreDetails.PressedImage = CargoMatrix.Resources.Skin._3dots_btn_over;
            this.btnMoreDetails.Size = new System.Drawing.Size((int)(32 * rWidth / 96F), (int)(32 * rHeight / 96F));
            this.btnMoreDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMoreDetails.TransparentColor = System.Drawing.Color.White;
            this.btnMoreDetails.Click += new EventHandler(btnMoreDetails_Click);

            this.Controls.Add(this.pcxLogo);
            this.Controls.Add(this.btnDamageCapture);
            this.Controls.Add(this.btnMoreDetails);
            this.Controls.Add(this.btnDetails);
            this.Controls.Add(this.pnlIndicators);
        }

       
        
        
        protected override bool ButtonEnterValidation()
        {
            return string.Equals(this.ItemData.MasterBillNumber, TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        protected override void Draw(System.Drawing.Graphics gOffScreen)
        {
            this.SuspendLayout();

            int fontSize = 9;
            var rHeight = this.CurrentAutoScaleDimensions.Height;
            var rWidth = this.CurrentAutoScaleDimensions.Width;

            var masterBill = this.ItemData;

            using (Brush brush = new SolidBrush(Color.Black))
            {
                gOffScreen.DrawString(masterBill.Reference(),
                                     new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), brush, 41 * rWidth / 96F, 3 * rHeight / 96F);
                gOffScreen.DrawString(masterBill.Line2(),
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), brush, 41 * rWidth / 96F, 19 * rHeight / 96F);
                gOffScreen.DrawString(masterBill.Line3(),
                                    new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), brush, 41 * rWidth / 96F, 33 * rHeight / 96F);

                string doors = "DOORs:";

                if(masterBill.Doors.Length > 0)
                {
                    doors = masterBill.Doors.Length == 1 ?
                        string.Format("{0} {1}", doors, masterBill.Doors[0])
                        :
                        string.Format("{0} {1} ...", doors, masterBill.Doors[0]);
                }

                gOffScreen.DrawString(doors, new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular), brush, 41 * rWidth / 96F, 53 * rHeight / 96F);

                switch (masterBill.Status)
                {
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Open:
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Pending:
                        pcxLogo.Image = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case CargoMatrix.Communication.DTO.MasterbillStatus.InProgress:
                        pcxLogo.Image = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Completed:
                        pcxLogo.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;
                }

            }

            using (Pen pen = new Pen(Color.Gainsboro))
            {
                gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
            }

            this.pnlIndicators.Flags = masterBill.Flags;
            this.pnlIndicators.PopupHeader = masterBill.Reference();
            this.pnlIndicators.BackColor = this.BackColor;

            this.ResumeLayout(false);
        }


        void btnMoreDetails_Click(object sender, EventArgs e)
        {
            var click = OnMoreDetailsClick;

            if (click != null)
            {
                OnMoreDetailsClick(this, EventArgs.Empty);
            }
        }

        void btnDamageCapture_Click(object sender, EventArgs e)
        {
            var click = OnDamageCaptureClick;

            if (click != null)
            {
                OnDamageCaptureClick(this, EventArgs.Empty);
            }
        }
        
        void btnDetails_Click(object sender, EventArgs e)
        {
            var detailsClick = this.OnViewerClick;

            if(detailsClick != null)
            {
                detailsClick(this, EventArgs.Empty);
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string textToMatch = text.TrimEnd(new char[] { ' ' }).ToUpper();

            if (this.ItemData.Reference().ToUpper().Contains(textToMatch)) return true;

            var door = (from item in this.ItemData.Doors
                        where item.Door.ToUpper().Contains(textToMatch)
                        select item).FirstOrDefault();

            return door != null;
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
