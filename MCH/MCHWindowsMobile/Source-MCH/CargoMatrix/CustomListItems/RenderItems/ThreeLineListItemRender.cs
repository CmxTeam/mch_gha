﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using SmoothListbox;

namespace CustomListItems.RenderItems
{
    public class ThreeLineListItem<T> : ListItemWithButton<T>, ISmartListItem
    {
        protected string line3;
        public ThreeLineListItem(string title, string line2, string line3, Image picture, T data)
            : base(title, line2, picture, data)
        {
            this.line3 = line3;
            float vScale = CurrentAutoScaleDimensions.Height / 96F;
            this.Height = (int)(50 * vScale);
        }
        protected override void InitializeControls()
        {
            base.InitializeControls();
            int logoY = (int)((this.Height - (32 * CurrentAutoScaleDimensions.Height / 96F)) / 2);
            this.button.Top = logoY;
        }
        protected override void Draw(Graphics gOffScreen)
        {
            base.Draw(gOffScreen);
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 36 * vScale, 170 * hScale, 13 * vScale));
                }
            }
        }
        #region ISmartListItem Members

        bool ISmartListItem.Contains(string text)
        {
            return line1.Contains(text) || line2.Contains(text);
        }

        #endregion
    }
}
