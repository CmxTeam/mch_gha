﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CustomListItems.RenderItems
{
    public partial class RenderButtonTest : SmoothListbox.ListItems.RenderItemBase
    {
        private SizeF Scaleratio { get { return new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F); } }
        private Rectangle buttonRect;
        public string Line1 = "Line 1";// { get; set; }
        public string Line2 = "Line 1";//{ get; set; }
        public string Line3 = "Line 1";//{ get; set; }
        public Image Logo = CargoMatrix.Resources.Icons.Door;//{ get; set; }
        public Image ButtonImage = CargoMatrix.Resources.Skin.printerButton;//{ get; set; }
        public Image ButtonPressedImage = CargoMatrix.Resources.Skin.printerButton_over;//{ get; set; }
        private bool buttonPressed = false;
        public event EventHandler ButtonClicked;
        public RenderButtonTest()
        {
            this.AutoScaleDimensions = new SizeF(96F, 96F);
            this.BackColor = Color.White;
            this.Size = new Size(240, 40);
            this.MouseDown += new MouseEventHandler(RenderButtonTest_MouseDown);
            this.MouseUp += new MouseEventHandler(RenderButtonTest_MouseUp);

            buttonRect = new Rectangle((int)(204 * Scaleratio.Width), (int)(4 * Scaleratio.Height), (int)(36 * Scaleratio.Width), (int)(36 * Scaleratio.Height));
        }
        //public override void Focus(bool isFocused)
        //{
        //    if (buttonPressed != true)
        //        base.Focus(isFocused);
        //}
        public override void SelectedChanged(bool isSelected)
        {
            if (buttonPressed != true)
                base.SelectedChanged(isSelected);
        }
        void RenderButtonTest_MouseUp(object sender, MouseEventArgs e)
        {
            if (buttonRect.Contains(e.X, e.Y))
            {
                buttonPressed = false;
                Dirty = true;
                Render();
                if (ButtonClicked != null)
                    ButtonClicked(this, EventArgs.Empty);
            }
        }

        void RenderButtonTest_MouseDown(object sender, MouseEventArgs e)
        {
            if (buttonRect.Contains(e.X, e.Y))
            {
                buttonPressed = true;
                Dirty = true;
                Render();
            }
        }

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    gOffScreen.DrawString(Line1, fnt, brush, new RectangleF(30 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {

                    gOffScreen.DrawString(Line2, fnt, brush, new RectangleF(30 * hScale, 20 * vScale, 170 * hScale, 13 * vScale));
                    gOffScreen.DrawString(Line3, fnt, brush, new RectangleF(30 * hScale, 33 * vScale, 170 * hScale, 13 * vScale));
                }

                Image btnImage = buttonPressed ? ButtonPressedImage : ButtonImage;

                gOffScreen.DrawImage(Logo, new Rectangle((int)(4 * hScale), (int)(4 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, Logo.Width, Logo.Height), GraphicsUnit.Pixel);
                gOffScreen.DrawImage(btnImage, new Rectangle((int)(204 * hScale), (int)(4 * vScale), (int)(32 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, btnImage.Width, btnImage.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
                }
            }
        }
    }
}
