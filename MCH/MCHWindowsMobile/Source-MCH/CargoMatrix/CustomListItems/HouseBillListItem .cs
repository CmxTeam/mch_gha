﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using System.Drawing;
using CMXExtensions;
using System.Windows.Forms;

namespace CustomListItems
{
    public class HouseBillListItem : RenderItemBase<IHouseBillItem>
    {
        public event EventHandler ButtonConditionClick;
        public event EventHandler ButtonBrowseClick;
        public event EventHandler OnViewerClick;
        public event EventHandler OnEnterClick;
        protected int previousHeight;
        private Panel panelManualEntry;
        private CargoMatrix.UI.CMXTextBox cmxTextBox1;
        private CargoMatrix.UI.CMXPictureButton buttonEnter;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private System.Windows.Forms.Label labelConfirm;
        bool m_firstTimeSelection = false;
        public string LabelConfirmation { get; set; }

        public HouseBillListItem(IHouseBillItem item)
            : base(item)
        {
            InitializeComponent();
        }
        public override void Render()
        {
            try
            {
                base.Render();

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    gOffScreen.DrawString(ItemData.Reference(), fnt, brush, new RectangleF(43 * hScale, 3 * vScale, 180 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {

                    gOffScreen.DrawString(ItemData.Line2(), fnt, brush, new RectangleF(43 * hScale, 33 * vScale, 166 * hScale, 13 * vScale));
                    gOffScreen.DrawString(ItemData.Line4(), fnt, brush, new RectangleF(43 * hScale, 47 * vScale, 166 * hScale, 13 * vScale));
                    if (ItemData.ScannedPieces > 0 && ItemData.ScannedPieces != ItemData.TotalPieces)
                        gOffScreen.DrawString(ItemData.Line3(), fnt, redBrush, new RectangleF(43 * hScale, 19 * vScale, 166 * hScale, 13 * vScale));
                    else
                        gOffScreen.DrawString(ItemData.Line3(), fnt, brush, new RectangleF(43 * hScale, 19 * vScale, 166 * hScale, 13 * vScale));
                }
                Image img = null;
                switch (ItemData.status)
                {
                    case HouseBillStatuses.Open:
                    case HouseBillStatuses.Pending:
                        img = CustomListItemsResource.Clipboard;
                        break;
                    case HouseBillStatuses.InProgress:
                        img = CustomListItemsResource.History;
                        break;
                    case HouseBillStatuses.Completed:
                        img = CustomListItemsResource.Clipboard_Check;
                        break;

                }
                gOffScreen.DrawImage(img, new Rectangle((int)(3 * hScale), (int)(3 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
                }
            }
        }

        void buttonCondition_Click(object sender, System.EventArgs e)
        {
            if (null != ButtonConditionClick)
                ButtonConditionClick(this, e);
        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            if (null != ButtonBrowseClick)
                ButtonBrowseClick(this, e);
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            if (OnViewerClick != null)
            {
                Cursor.Current = Cursors.WaitCursor;
                OnViewerClick(this, e);
            }
        }
        private void buttonEnter_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok_over;
            buttonEnter.Refresh();

            char[] whiteSpaces = " ".ToCharArray();
            cmxTextBox1.Text = cmxTextBox1.Text.TrimEnd(whiteSpaces);
            cmxTextBox1.Text = cmxTextBox1.Text.TrimEnd(whiteSpaces);
            cmxTextBox1.Text = cmxTextBox1.Text.ToUpper();
            if (ItemData.HousebillNumber == cmxTextBox1.Text)
            {
                Cursor.Current = Cursors.Default;
                if (OnEnterClick != null)
                    OnEnterClick(this, e);
                buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok;
                buttonEnter.Refresh();

            }
            else
            {

                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Text entered does not match. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok;
                buttonEnter.Refresh();
                cmxTextBox1.SelectAll();
                cmxTextBox1.Focus();
            }


        }
        private void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (this.IsSelected)
                        buttonEnter_Click(sender, EventArgs.Empty);
                    break;
            }


        }

        public override void SelectedChanged(bool isSelected)
        {
            IsSelected = isSelected;

            base.SelectedChanged(isSelected);
            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
            {


                if (isSelected)
                {
                    /////////////////////////////
                    if (m_firstTimeSelection == false)
                    {
                        m_firstTimeSelection = true;

                        this.SuspendLayout();
                        this.panelManualEntry = new Panel();
                        this.panelManualEntry.Size = new Size(240, 43);
                        this.panelManualEntry.Location = new Point(0, 78);
                        panelManualEntry.SuspendLayout();

                        this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
                        this.buttonEnter = new CargoMatrix.UI.CMXPictureButton();
                        this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
                        this.labelConfirm = new System.Windows.Forms.Label();
                        // cmxTextBox1
                        // 
                        this.cmxTextBox1.Location = new System.Drawing.Point(3, 14);
                        //this.cmxTextBox1.Name = "cmxTextBox1";
                        this.cmxTextBox1.Size = new System.Drawing.Size(147, 28);
                        this.cmxTextBox1.TabIndex = 2;
                        this.cmxTextBox1.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
                        this.cmxTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmxTextBox1_KeyDown);
                        this.cmxTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
                        // 
                        // buttonEnter
                        // 
                        this.buttonEnter.Location = new System.Drawing.Point(152, 14);
                        //this.buttonEnter.Name = "buttonEnter";
                        this.buttonEnter.Size = new System.Drawing.Size(40, 28);
                        this.buttonEnter.Click += new System.EventHandler(this.buttonEnter_Click);
                        this.buttonEnter.Image = Resources.Graphics.Skin.manual_entry_ok;
                        this.buttonEnter.PressedImage = Resources.Graphics.Skin.manual_entry_ok_over;
                        // 
                        // buttonCancel
                        // 
                        this.buttonCancel.Location = new System.Drawing.Point(195, 14);
                        //this.buttonEnter.Name = "buttonEnter";
                        this.buttonCancel.Size = new System.Drawing.Size(40, 28);
                        this.buttonCancel.Click += new EventHandler(buttonCancel_Click);
                        this.buttonCancel.Image = Resources.Graphics.Skin.manual_entry_cancel;
                        this.buttonCancel.PressedImage = Resources.Graphics.Skin.manual_entry_cancel_over;
                        // 
                        // labelConfirm
                        // 
                        this.labelConfirm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
                        this.labelConfirm.ForeColor = System.Drawing.Color.Black;
                        this.labelConfirm.Location = new System.Drawing.Point(3, 0);
                        //this.labelConfirm.Name = "labelConfirm";
                        this.labelConfirm.Size = new System.Drawing.Size(234, 12);
                        this.labelConfirm.Text = LabelConfirmation;



                        this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

                        this.panelManualEntry.Controls.Add(this.labelConfirm);
                        this.panelManualEntry.Controls.Add(this.cmxTextBox1);
                        this.panelManualEntry.Controls.Add(this.buttonEnter);
                        this.panelManualEntry.Controls.Add(this.buttonCancel);

                        this.Controls.Add(panelManualEntry);
                        this.panelManualEntry.ResumeLayout();
                        this.ResumeLayout();
                    }
                    ////////////////////////////////
                    labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = true;

                    Cursor.Current = Cursors.Default;
                    previousHeight = this.Height;
                    this.Height = panelManualEntry.Bottom + 4;
                    this.Dirty = true;
                    this.panelManualEntry.BackColor = this.BackColor;
                    cmxTextBox1.SelectAll();
                    cmxTextBox1.Focus();
                    //buttonEnterClicked = false;
                }
                else
                {
                    //if (this.ClientRectangle.Width == 480)
                    //    this.Height = 112;// previousHeight;
                    //else
                    //    this.Height = 56;
                    Height = previousHeight;

                    if (m_firstTimeSelection)
                        labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = false;
                }

            }
            for (Control control = this.Parent; control != null; control = control.Parent)
            {
                if (control is SmoothListbox.SmoothListBoxBase)
                {
                    (control as SmoothListbox.SmoothListBoxBase).LayoutItems();
                    (control as SmoothListbox.SmoothListBoxBase).RefreshScroll();
                    break;
                }
            }
        }
        void buttonCancel_Click(object sender, EventArgs e)
        {
            cmxTextBox1.Text = string.Empty;
            //SelectedChanged(false);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCondition = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.SuspendLayout();
            // 
            // buttonCondition
            // 
            this.buttonCondition.Location = new System.Drawing.Point(167, 3);
            this.buttonCondition.Name = "buttonCondition";
            this.buttonCondition.Size = new System.Drawing.Size(32, 32);
            this.buttonCondition.Click += new System.EventHandler(this.buttonCondition_Click);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(205, 3);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // panelIndicators
            // 
            this.panelIndicators.Location = new System.Drawing.Point(3, 62);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            this.panelIndicators.TabIndex = 2;
            // 
            // buttonDetails
            // 
            this.buttonDetails.Location = new System.Drawing.Point(3, 29);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            buttonDetails.Click += new System.EventHandler(buttonDetails_Click);
            // 
            // ConsolHAWBListItem
            // 
            initializeImages();
            ///
            this.Controls.Add(this.buttonCondition);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.buttonDetails);

            this.Controls.Add(this.panelIndicators);
            this.Name = "HouseBillListItem";
            this.BackColor = Color.White;
            this.Size = new System.Drawing.Size(240, 81);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }
        void initializeImages()
        {
            buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonCondition.Image = CargoMatrix.Resources.Skin.damage;
            this.buttonCondition.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.buttonBrowse.Image = global::Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.panelIndicators.Flags = ItemData.Flags;
            this.panelIndicators.PopupHeader = ItemData.Reference();
        }
        #endregion

        private CargoMatrix.UI.CMXPictureButton buttonCondition;
        private CargoMatrix.UI.CMXPictureButton buttonBrowse;
        private CustomUtilities.IndicatorPanel panelIndicators;
        private CargoMatrix.UI.CMXPictureButton buttonDetails;
    }
}
