﻿namespace CustomListItems
{
    partial class ConsolHAWBListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCondition = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.labelLine4 = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.labelLine2 = new System.Windows.Forms.Label();
            this.labelLine3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCondition
            // 
            this.buttonCondition.Location = new System.Drawing.Point(167, 3);
            this.buttonCondition.Name = "buttonCondition";
            this.buttonCondition.Size = new System.Drawing.Size(32, 32);
            this.buttonCondition.Click += new System.EventHandler(this.buttonCondition_Click);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(205, 3);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // panelIndicators
            // 
            this.panelIndicators.Location = new System.Drawing.Point(3, 62);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            this.panelIndicators.TabIndex = 2;
            // 
            // labelLine4
            // 
            this.labelLine4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine4.Location = new System.Drawing.Point(41, 47);
            this.labelLine4.Name = "labelLine4";
            this.labelLine4.Size = new System.Drawing.Size(166, 13);
            this.labelLine4.Text = "XOY1234:ky";
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(41, 3);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(180, 14);
            this.title.Text = "ORD-12345678-ORD";
            // 
            // itemPicture
            // 
            this.itemPicture.Location = new System.Drawing.Point(3, 3);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(24, 24);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDetails
            // 
            this.buttonDetails.Location = new System.Drawing.Point(3, 29);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            buttonDetails.Click += new System.EventHandler(buttonDetails_Click);
            // 
            // labelLine2
            // 
            this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine2.Location = new System.Drawing.Point(41, 19);
            this.labelLine2.Name = "labelLine2";
            this.labelLine2.Size = new System.Drawing.Size(166, 13);
            this.labelLine2.Text = "<labelDescription>";
            // 
            // labelLine3
            // 
            this.labelLine3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLine3.Location = new System.Drawing.Point(41, 33);
            this.labelLine3.Name = "labelLine3";
            this.labelLine3.Size = new System.Drawing.Size(166, 13);
            this.labelLine3.Text = "<labelHouseBill>";
            // 
            // ConsolHAWBListItem
            // 
            initializeImages();
            ///
            this.Controls.Add(this.buttonCondition);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.labelLine4);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.labelLine2);
            this.Controls.Add(this.labelLine3);
            this.Controls.Add(this.panelIndicators);
            this.Name = "ConsolHAWBListItem";
            this.Size = new System.Drawing.Size(240, 81);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }

        #endregion

        private CargoMatrix.UI.CMXPictureButton buttonCondition;
        private CargoMatrix.UI.CMXPictureButton buttonBrowse;
        private CustomUtilities.IndicatorPanel panelIndicators;
        protected System.Windows.Forms.Label labelLine4;
        public System.Windows.Forms.Label title;
        public OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private CargoMatrix.UI.CMXPictureButton buttonDetails;
        protected System.Windows.Forms.Label labelLine2;
        protected System.Windows.Forms.Label labelLine3;
    }
}
