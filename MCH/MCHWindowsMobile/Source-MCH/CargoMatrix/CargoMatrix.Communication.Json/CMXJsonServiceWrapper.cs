﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.Exceptions;
using System.Windows.Forms;

namespace CargoMatrix.Communication.Json
{
	public class CMXJsonServiceWrapper
	{
		private const int NoOfTrys = 1;

		public void Invoke(Action action, int excCode, bool throwOnReconnectionReject, bool rethrowCommonException)
		{
			int i = 0;
			bool rejected = false;
			while (i < NoOfTrys)
			{
				i++;
				try
				{
					action();
					return;
				}
				catch (CommunicationException exc)
				{
					if (i < NoOfTrys)
					{
						continue;
					}
					else
					{
						Cursor.Current = Cursors.Default;
						if (System.Windows.Forms.DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Unable to connect... do you want to retry ?", "Retry", CargoMatrix.UI.CMXMessageBoxIcon.Question, System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.DialogResult.Yes))
						{
							i = 0;
							continue;
						}
						else
						{
							rejected = true;
							break;
						}
					}
				}
				catch (Exception ex)
				{
					//TODO: Implement for json
					//CargoMatrix.Communication.ScannerUtility.Instance.SaveScannerLog(excCode, ex.ToString());
					if (rethrowCommonException)
					{
						throw;
					}
					else
					{
						CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, excCode);
						break;
					}
				}
				finally
				{
					Cursor.Current = Cursors.Default;
				}
			}
			if (rejected && throwOnReconnectionReject)
			{
				throw new ConnectionRetryRejectedException(connectionFailed);
			}
		}

		public R Invoke<R>(Func<R> action, int excCode, R defaultReturn, bool throwOnReconnectionReject, bool rethrowCommonException)
		{
			int i = 0;
			bool rejected = false;
			while (i < NoOfTrys)
			{
				i++;
				try
				{
					try
					{
						Cursor.Current = Cursors.WaitCursor;
						return action();
					}
					catch (CommunicationException exc)
					{
						if ((i < NoOfTrys))
						{
							continue;
						}
						else
						{
							Cursor.Current = Cursors.Default;
							if (System.Windows.Forms.DialogResult.OK ==
								CargoMatrix.UI.CMXMessageBox.Show("Unable to connect... do you want to retry ?", "Retry",
									CargoMatrix.UI.CMXMessageBoxIcon.Question,
									System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.DialogResult.Yes))
							{
								i = 0;
								continue;
							}
							else
							{
								rejected = true;
								break;
							}
						}
					}
					//catch (XmlException exc)
					//{

					//    if ((exc.Message.Contains("The remote name could not be resolved") ||
					//         exc.Message.Contains("Unable to read data from the transport connection")) &&
					//        (i < NoOfTrys))
					//    {
					//        continue;
					//    }
					//    else
					//    {
					//        Cursor.Current = Cursors.Default;
					//        if (System.Windows.Forms.DialogResult.OK ==
					//            CargoMatrix.UI.CMXMessageBox.Show("Unable to connect... do you want to retry ?", "Retry",
					//                CargoMatrix.UI.CMXMessageBoxIcon.Question,
					//                System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.DialogResult.Yes))
					//        {
					//            i = 0;
					//            continue;
					//        }
					//        else
					//        {
					//            rejected = true;
					//            break;
					//        }
					//    }
					//}					
				}
				catch (Exception ex)
				{
					// TODO FLUOR
					//CargoMatrix.Communication.ScannerUtility.Instance.SaveScannerLog(excCode, ex.ToString());
					if (rethrowCommonException)
					{
						throw;
					}
					else
					{
						CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, excCode);
						break;
					}
				}
				finally
				{
					Cursor.Current = Cursors.Default;
				}
			}
			if (rejected && throwOnReconnectionReject)
			{
				throw new ConnectionRetryRejectedException(connectionFailed);
			}
			return defaultReturn;
		}

		protected string connectionFailed = "Connection failed";
	}
}
