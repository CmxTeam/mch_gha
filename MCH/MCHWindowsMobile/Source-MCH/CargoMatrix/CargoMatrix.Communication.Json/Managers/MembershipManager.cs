﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.DTO.Membership;
using CargoMatrix.Communication.Json.ServiceReferences;
using CargoMatrix.Communication.Common.Exceptions;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication.Json.Managers
{
	public class MembershipManager : CMXJsonServiceWrapper
	{
		//Fault code base: 12000xx
		private static MembershipManager instance;

		public UserMobileInfo User { get; private set; }

		static MembershipManager()
		{
			instance = new MembershipManager();
		}

		public static MembershipManager Instance
		{
			get
			{
				return instance;
			}
		}

		public UserMobileInfo AuthenticateUserPin(AuthAppUserModel model)
		{
			Func<UserMobileInfoPackage> f = () =>
			{
				return MembershipService.Instance.AuthenticateUserPin(model);
			};
			UserMobileInfoPackage result = Invoke<UserMobileInfoPackage>(f, 1200001, null, true, true);
			if (result.Transaction != null && !result.Transaction.Status)
				throw new ServerSideExcetion(result.Transaction.Error) { ErrorCode = result.Transaction.ErrorCode };
			UserMobileInfo retval = result.Data;
			User = retval;
			return retval;
		}

		public UserMobileInfo AuthenticateAppUser(AuthUserModel model)
		{
			Func<UserMobileInfoPackage> f = () =>
			{
				return MembershipService.Instance.AuthenticateAppUser(model);
			};
			UserMobileInfoPackage result = Invoke<UserMobileInfoPackage>(f, 1200002, null, true, true);
			if (result.Transaction != null && !result.Transaction.Status)
				throw new ServerSideExcetion(result.Transaction.Error) { ErrorCode = result.Transaction.ErrorCode };
			UserMobileInfo retval = result.Data;
			User = retval;
			return retval;
		}

		public CompanySecurityConfigDTO GetCompanySecuritySettings(string companyName)
		{
			Func<CompanySecurityConfigDTOPackage> f = () =>
			{
				return MembershipService.Instance.GetCompanySecuritySettings(companyName);
			};
			CompanySecurityConfigDTOPackage result = Invoke<CompanySecurityConfigDTOPackage>(f, 1200003, null, true, true);
			if (result.Transaction != null && !result.Transaction.Status)
				throw new ServerSideExcetion(result.Transaction.Error) { ErrorCode = result.Transaction.ErrorCode };
			CompanySecurityConfigDTO retval = result.Data;

			return retval;
		}

		public CompanySecurityConfigDTO GetCompanySecurityById(int companyId)
		{
			Func<CompanySecurityConfigDTOPackage> f = () =>
			{
				return MembershipService.Instance.GetCompanySecurityById(companyId);
			};
			CompanySecurityConfigDTOPackage result = Invoke<CompanySecurityConfigDTOPackage>(f, 1200003, null, true, true);
			if (result.Transaction != null && !result.Transaction.Status)
				throw new ServerSideExcetion(result.Transaction.Error) { ErrorCode = result.Transaction.ErrorCode };
			CompanySecurityConfigDTO retval = result.Data;

			return retval;
		}

		public string GetApplicationName()
		{
			return Settings.Instance.AppName;
		}
	}
}
