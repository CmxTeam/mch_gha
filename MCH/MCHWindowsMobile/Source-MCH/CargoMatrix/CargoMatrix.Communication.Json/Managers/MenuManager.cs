﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common.DTO.Menu;
using CargoMatrix.Communication.Json.ServiceReferences;
using CargoMatrix.Communication.Common.Exceptions;

namespace CargoMatrix.Communication.Json.Managers
{
	public class MenuManager : CMXJsonServiceWrapper
	{
		//Fault code base: 13000xx
		private static MenuManager instance;

		static MenuManager()
		{
			instance = new MenuManager();
		}

		public static MenuManager Instance
		{
			get
			{
				return instance;
			}
		}

		public List<AppNavigation> GetAppRoleMenus(AppRoleMenuModel model)
		{
			Func<AppNavigationPackage> f = () =>
			{
				return MenuService.Instance.GetAppRoleMenus(model);
			};
			AppNavigationPackage result =Invoke<AppNavigationPackage>(f, 1300001, null, true, true); 
			if (result.Transaction != null && !result.Transaction.Status)
				throw new ServerSideExcetion(result.Transaction.Error) { ErrorCode = result.Transaction.ErrorCode };
			List<AppNavigation> retval = result.Data;
			return retval;
		}
	}
}
