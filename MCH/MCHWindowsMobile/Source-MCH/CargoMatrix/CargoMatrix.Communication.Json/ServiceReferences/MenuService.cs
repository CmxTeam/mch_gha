﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;
using CargoMatrix.Communication.Common.DTO.Menu;
using CargoMatrix.Communication.Common.Attributes;

namespace CargoMatrix.Communication.Json.ServiceReferences
{
	public class MenuService : RESTClientProtocol
	{
		private MenuService() { }

		private static MenuService instance;

		static MenuService()
		{
			instance = new MenuService() { Processor = RestServiceProcessor.GetService(Settings.Instance.RestURLPath) };
		}

		public static MenuService Instance
		{
			get
			{
				return instance;
			}
		}

		[ServiceMethod("Menu/GetAppRoleMenus", ServiceMethodAttribute.POST)]
		public AppNavigationPackage GetAppRoleMenus(AppRoleMenuModel model)
		{
			return this.Processor.ExecuteMethod<AppNavigationPackage>(typeof(MenuService).GetMethod("GetAppRoleMenus"), model);
		}
	}
}
