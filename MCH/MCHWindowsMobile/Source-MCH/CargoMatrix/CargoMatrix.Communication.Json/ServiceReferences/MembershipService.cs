﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;
using CargoMatrix.Communication.Common.DTO.Membership;
using CargoMatrix.Communication.Common.Attributes;

namespace CargoMatrix.Communication.Json.ServiceReferences
{
	public class MembershipService : RESTClientProtocol
	{
		private MembershipService() { }

		private static MembershipService instance;

		static MembershipService()
		{
			instance = new MembershipService() { Processor = RestServiceProcessor.GetService(Settings.Instance.RestURLPath) };		}

		public static MembershipService Instance
		{
			get
			{
				return instance;
			}
		}

		[ServiceMethod("Membership/GetCompanySecuritySettings", ServiceMethodAttribute.GET)]
		public CompanySecurityConfigDTOPackage GetCompanySecuritySettings(string companyName)
		{
			return this.Processor.ExecuteMethod<CompanySecurityConfigDTOPackage>(typeof(MembershipService).GetMethod("GetCompanySecuritySettings"), companyName);
		}

		[ServiceMethod("Membership/GetCompanySecurityById", ServiceMethodAttribute.GET)]
		public CompanySecurityConfigDTOPackage GetCompanySecurityById(int companyId)
		{
			return this.Processor.ExecuteMethod<CompanySecurityConfigDTOPackage>(typeof(MembershipService).GetMethod("GetCompanySecurityById"), companyId);
		}

		[ServiceMethod("Membership/AuthenticateUserPin", ServiceMethodAttribute.POST)]
		public UserMobileInfoPackage AuthenticateUserPin(AuthAppUserModel model)
		{
			return this.Processor.ExecuteMethod<UserMobileInfoPackage>(typeof(MembershipService).GetMethod("AuthenticateUserPin"), model);
		}

		[ServiceMethod("Membership/AuthenticateAppUser", ServiceMethodAttribute.POST)]
		public UserMobileInfoPackage AuthenticateAppUser(AuthUserModel model)
		{
			return this.Processor.ExecuteMethod<UserMobileInfoPackage>(typeof(MembershipService).GetMethod("AuthenticateAppUser"), model);
		}
	}
}
