﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;
using System.Net;
using System.IO;
using CargoMatrix.Communication.Common.Data;
using System.Reflection;
using CargoMatrix.Communication.Common.Attributes;
using CargoMatrix.Communication.Common.Exceptions;
using Newtonsoft.Json;

namespace CargoMatrix.Communication.Json
{
    public class RestServiceProcessor
    {

        Uri baseUri;

		Uri BaseUri
		{
			get { return baseUri; }
		}

        private RestServiceProcessor() { }

        public static RestServiceProcessor GetService(string serviceUrl)
        {
            RestServiceProcessor instance = new RestServiceProcessor();
			instance.baseUri = new Uri(serviceUrl);
            return instance;
        }

        private string GetFullUrl(string functionName)
        {
            Uri uri = new Uri(baseUri, functionName);
            return uri.ToString();
        }
		
		private string RetrievePostMethodResponse(string url, string json)
		{
			url = GetFullUrl(url);

			CommunicationTransaction t = new CommunicationTransaction();
			t.Url = url;

			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			byte[] postBytes = Encoding.ASCII.GetBytes(json);
			request.ContentLength = postBytes.Length;
			Stream postStream = request.GetRequestStream();
			postStream.Write(postBytes, 0, postBytes.Length);
			postStream.Close();

			using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
			{
				if (response.StatusCode != HttpStatusCode.OK)
				{
					throw new CommunicationException(string.Format("Server returned status code: {0}", response.StatusCode)) { Status = response.StatusCode };
				}
				else
				{
					using (StreamReader reader = new StreamReader(response.GetResponseStream()))
					{
						var content = reader.ReadToEnd();
						if (string.IsNullOrEmpty(content) || string.IsNullOrEmpty(content.Trim()))
						{
							throw new Exception("Response contained empty body.");
						}
						else
						{
							return content;
						}
					}
				}
			}
		}

		private string RetrieveGetMethodResponse(string url, Dictionary<string, string> parameters)
		{
			url = GetFullUrl(url);

			CommunicationTransaction t = new CommunicationTransaction();
			t.Url = url;

			if (parameters != null)
			{
				StringBuilder paramList = new StringBuilder();
				foreach (KeyValuePair<string, string> kvp in parameters)
				{

					if (paramList.ToString().Trim() == string.Empty)
					{
						paramList.Append(string.Format("{0}={1}", kvp.Key, kvp.Value));
					}
					else
					{
						paramList.Append(string.Format("&{0}={1}", kvp.Key, kvp.Value));
					}

				}

				if (paramList.ToString().Trim() != string.Empty)
				{
					url += string.Format("?{0}", paramList.ToString());
				}
			}

			var request = HttpWebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "GET";


			using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
			{
				if (response.StatusCode != HttpStatusCode.OK)
				{
					throw new CommunicationException(string.Format("Server returned status code: {0}", response.StatusCode)) { Status = response.StatusCode };
				}
				else
				{
					using (StreamReader reader = new StreamReader(response.GetResponseStream()))
					{
						var content = reader.ReadToEnd();
						if (string.IsNullOrEmpty(content) || string.IsNullOrEmpty(content.Trim()))
						{
							throw new Exception("Response contained empty body.");
						}
						else
						{
							return content;
						}
					}
				}
			}
		}

		private string TryExecuteMethod(MethodInfo method, params object[] parameters)
		{
			object[] attribs = method.GetCustomAttributes(typeof(ServiceMethodAttribute), true);
			if (attribs == null || attribs.Length == 0)
				throw new MissingServiceMethodAttributeException("No service method information provided");
			ServiceMethodAttribute attr = (ServiceMethodAttribute)attribs[0];
			if (string.IsNullOrEmpty(attr.Method) && string.IsNullOrEmpty(attr.Method.Trim()))
				throw new InvalidServiceMethodAttributeDataException("No method provided");
			if (string.IsNullOrEmpty(attr.Path) && string.IsNullOrEmpty(attr.Path.Trim()))
				throw new InvalidServiceMethodAttributeDataException("No method URI provided");
			ParameterInfo[] methodParams = method.GetParameters();
			string result = null;
			if (attr.Method == ServiceMethodAttribute.GET)
			{
				Dictionary<string, string> getParams = new Dictionary<string, string>();
				if (methodParams != null && methodParams.Length > 0 && parameters != null && parameters.Length > 0)
					for (int i = 0; i < Math.Max(methodParams.Length, parameters.Length); i++)
					{
						getParams.Add(methodParams[i].Name, parameters[i] == null ? string.Empty : parameters[i].ToString());
					}
				result = RetrieveGetMethodResponse(attr.Path, getParams);
			}
			else
			{
				string serialized = string.Empty;
				if (parameters != null && parameters.Length > 0 && parameters[0] != null)
				{
					serialized = JsonConvert.SerializeObject(parameters[0]);
				}
				result = RetrievePostMethodResponse(attr.Path, serialized);
			}
			return result;
		}

        public void ExecuteMethod(MethodInfo method, params object[] parameters)
        {
			TryExecuteMethod(method, parameters);
        }

		public TRes ExecuteMethod<TRes>(MethodInfo method, params object[] parameters)
		{
			string result = TryExecuteMethod(method, parameters);
			TRes retval = default(TRes);
			retval = Newtonsoft.Json.JsonConvert.DeserializeObject<TRes>(result);
			return retval;
		}
    }
}
