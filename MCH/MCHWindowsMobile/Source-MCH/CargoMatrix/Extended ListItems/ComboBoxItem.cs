﻿using System;

using System.Collections.Generic;
using System.Text;

namespace SmoothListBox.UI.ListItems
{
    class ComboBoxItem : ListItem
    {
        public System.Windows.Forms.Label title;
        public OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        public OpenNETCF.Windows.Forms.PictureBox2 pictureboxIcon;
        //bool m_isLast;
    
        public ComboBoxItem(string text, System.Drawing.Image Icon, bool isLast)
        {
            InitializeComponent();
            panelBaseLine.Visible = false;
            //focusedColor = System.Drawing.Color.Ivory;
            //title.Font = new System.Drawing.Font(title.Font.Name, 8, System.Drawing.FontStyle.Regular);
            title.Text = text;
            Name = text;
            if (isLast == false)
                itemPicture.Image = ListItemsResource.DottedLineFull;
            else
                itemPicture.Image = ListItemsResource.DottedLineHalf;

            pictureboxIcon.Image = Icon;
        }

        public override void SelectedChanged(bool isSelected)
        {
            //if (isSelected)
            //{
            //    //pictureBoxCheck.Image = ListItemsResource.Symbol_Check_2;
            //    checkBox.Image = ListItemsResource.Check_In_16;
            //}
            //else
            //{
            //    checkBox.Image = ListItemsResource.DottedLineHorizontal;// null;// ListItemsResource.Notepad_Information;
            //    //pictureBoxCheck.Image = null;

            //}

            ////base.SelectedChanged(isSelected);
        }
        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.pictureboxIcon = new OpenNETCF.Windows.Forms.PictureBox2();
            //((System.ComponentModel.ISupportInitialize)(this.itemPicture)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.checkBox)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.title.Location = new System.Drawing.Point(70, 12);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(164, 15);
            this.title.Text = "<title>";
            // 
            // itemPicture
            // 
            this.itemPicture.Location = new System.Drawing.Point(1, 0);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(40, 40);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // pictureboxIcon
            // 
            this.pictureboxIcon.Location = new System.Drawing.Point(42, 8);
            this.pictureboxIcon.Name = "pictureboxIcon";
            this.pictureboxIcon.Size = new System.Drawing.Size(24, 24);
            this.pictureboxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureboxIcon.TransparentColor = System.Drawing.Color.White;
            // 
            // ComboBoxItem
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.pictureboxIcon);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Name = "ComboBoxItem";
            this.Size = new System.Drawing.Size(238, 40);
            //((System.ComponentModel.ISupportInitialize)(this.itemPicture)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.checkBox)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
