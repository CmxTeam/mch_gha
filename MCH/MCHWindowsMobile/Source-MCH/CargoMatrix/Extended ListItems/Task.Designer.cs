﻿namespace SmoothListBox.UI.ListItems
{
    partial class Task
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.description = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.taskPicture = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // description
            // 
            this.description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.description.Location = new System.Drawing.Point(74, 27);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(135, 18);
            this.description.Text = "<description>";
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(74, 4);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(135, 18);
            this.title.Text = "<title>";
            // 
            // taskPicture
            // 
            this.taskPicture.Location = new System.Drawing.Point(3, 3);
            this.taskPicture.Name = "taskPicture";
            this.taskPicture.Size = new System.Drawing.Size(64, 64);
            this.taskPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // Task
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.description);
            this.Controls.Add(this.title);
            this.Controls.Add(this.taskPicture);
            this.Name = "Task";
            this.Size = new System.Drawing.Size(212, 72);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label title;
        private System.Windows.Forms.PictureBox taskPicture;
        protected System.Windows.Forms.Label description;
    }
}
