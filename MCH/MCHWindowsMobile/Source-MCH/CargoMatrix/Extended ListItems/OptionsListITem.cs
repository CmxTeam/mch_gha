﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;


namespace SmoothListBox.UI.ListItems
{
    public partial class OptionsListITem : StandardListItem, IOptionsListItem// UserControl
    {
        public enum OptionItemID
        {
            EXIT = 101,
            LOGOUT,
            REFRESH,
            MAIN_MENU,
            ABOUT,
            COMPLETE_TASK,
            COMPLETE_TASK_LATER,
            EXIT_WITHOUT_SAVING,
            MANUAL_LOCATION,
            FILTER,
            SELECT_ALL,
            SELECT_NONE,
            REMOVE_ALL,
            DELETE_TASK,
            MANUAL_PEICE_ENTRY,
            UNKNOWN,
            PRINT_HAWB_LABEL,
            PRINT_MAWB_LABEL,
            STAGE_ULD,
            PRINT_ULD_LABEL,
            VISUAL_INSPECTION,
            RESET_SCREENING,
            CANCEL_SCREENING,
            FIND,
            SCREENING_DIRECTION


        }
        public OptionsListITem( OptionItemID optionID)
        {
            InitializeComponent();
            ID = optionID;
            switch (optionID)
            { 
                case OptionItemID.ABOUT:
                    this.itemPicture.Image = ListItemsResource.Analyze;
                    this.title.Text = ListItemsResource.Text_About;
                    break;
                case OptionItemID.COMPLETE_TASK:
                    //this.itemPicture.Image = ListItemsResource.Symbol_Check_2;
                    this.title.Text = ListItemsResource.Text_CompleteTask;
                    break;
                case OptionItemID.COMPLETE_TASK_LATER:
                    //this.itemPicture.Image = ListItemsResource.Save;
                    this.title.Text = ListItemsResource.Text_CompleteTaskLater;
                    break;
                case OptionItemID.EXIT:
                    this.title.Text = ListItemsResource.Text_Exit;
                    this.itemPicture.Image = ListItemsResource.Logout;
                    break;
                case OptionItemID.EXIT_WITHOUT_SAVING:
                    //this.itemPicture.Image = ListItemsResource.Symbol_Delete_2;
                    this.title.Text = ListItemsResource.Text_ExitWithoutSaving;
                    break;
                case OptionItemID.LOGOUT:
                    this.title.Text = ListItemsResource.Text_Logout;
                    this.LabelUser.Text = CargoMatrix.Communication.WebServiceManager.Instance().m_user.firstName
                         + " " + CargoMatrix.Communication.WebServiceManager.Instance().m_user.lastName
                         + " (" + CargoMatrix.Communication.WebServiceManager.Instance().m_user.UserName + ")";
                    this.itemPicture.Image = ListItemsResource.Logout;
                    break;
                case OptionItemID.MAIN_MENU:
                    this.title.Text = ListItemsResource.Text_MainMenu;
                    this.itemPicture.Image = ListItemsResource.Menu;
                    break;
                case OptionItemID.MANUAL_LOCATION:
                    this.title.Text = ListItemsResource.Text_ManualLocation;
                    this.itemPicture.Image = ListItemsResource.Symbol_Edit;
                    break;
                case OptionItemID.REFRESH:
                    this.title.Text = ListItemsResource.Text_Refresh;
                    this.itemPicture.Image = ListItemsResource.Refresh;
                    break;
                case OptionItemID.FILTER:
                    this.title.Text = ListItemsResource.Text_Filter;
                    itemPicture.Image = ListItemsResource.Filter;
                    break;
                case OptionItemID.SELECT_ALL:
                    this.title.Text = ListItemsResource.Text_Select_ALL;
                    itemPicture.Image = ListItemsResource.Selection;
                    break;
                case OptionItemID.SELECT_NONE:
                    this.title.Text = ListItemsResource.Text_Select_None;
                    itemPicture.Image = ListItemsResource.Selection_Delete;
                    break;
                case OptionItemID.REMOVE_ALL:
                    this.title.Text = ListItemsResource.Text_Remove_ALL;
                    itemPicture.Image = ListItemsResource.Trash_Can;
                    break;

                case OptionItemID.DELETE_TASK:
                    this.title.Text = ListItemsResource.Text_Delete_Task;
                    itemPicture.Image = ListItemsResource.Trash_Can;
                    break;

                case OptionItemID.MANUAL_PEICE_ENTRY:
                    this.title.Text = ListItemsResource.Text_Manual_Piece_Entry;
                    itemPicture.Image = ListItemsResource.Assembly;
                    break;
                case OptionItemID.PRINT_HAWB_LABEL:
                    this.title.Text = ListItemsResource.Text_Print_HAWB_Label;
                    itemPicture.Image = ListItemsResource.Printer_Laser;
                    break;
                case OptionItemID.PRINT_MAWB_LABEL:
                    this.title.Text = ListItemsResource.Text_Print_MAWB_Label;
                    itemPicture.Image = ListItemsResource.Printer_Laser;
                    break;
                case OptionItemID.STAGE_ULD:
                    this.title.Text = ListItemsResource.Text_Stage_ULD;
                    itemPicture.Image = ListItemsResource.Objects;
                    break;
                case OptionItemID.PRINT_ULD_LABEL:
                    this.title.Text = ListItemsResource.Text_Print_ULD;
                    itemPicture.Image = ListItemsResource.Printer_Laser;
                    break;

                case OptionItemID.VISUAL_INSPECTION:
                    itemPicture.Image = ListItemsResource.Visual_Inspection_32x32;
                    title.Text = ListItemsResource.Text_Visual_Inspection;
                    break;
                case OptionItemID.RESET_SCREENING:
                    itemPicture.Image = ListItemsResource.Reset;
                    title.Text = ListItemsResource.Text_Reset_Screening;
                    break;
                case OptionItemID.CANCEL_SCREENING:
                    itemPicture.Image = ListItemsResource.Symbol_Delete_2;
                    title.Text = ListItemsResource.Text_Cancel_Screening;
                    break;
                case OptionItemID.FIND:
                    itemPicture.Image = ListItemsResource.Symbol_Find;
                    title.Text = ListItemsResource.Text_Find;
                    break;
                case OptionItemID.SCREENING_DIRECTION:
                    itemPicture.Image = ListItemsResource.Symbol_Find;
                    break;
                    

                    
            }

            Init();
            
           
            
        }
        //public OptionsListITem(string titlee, Image picture, OptionItemID optionID)
        //{
        //    InitializeComponent();
        //    this.titlee.Text = titlee;
        //    this.Name = titlee;
        //    this.itemPicture.Image = picture;

        //    Init();


        //}

        //public OptionsListITem(string titlee, Image picture, Color bkColor, OptionItemID optionID)
        //{
        //    InitializeComponent();
        //    this.titlee.Text = titlee;
        //    this.Name = titlee;
        //    this.itemPicture.Image = picture;
        //    m_backColor = bkColor;
        //    Init();


        //}
        //public OptionsListITem(string titlee, string image, OptionItemID optionID)
        //{ 
        //    InitializeComponent();
        //    this.titlee.Text = titlee;
        //    this.Name = titlee;
        //    //this.itemPicture.Image = picture;
        //    if (image != null)
        //        this.itemPicture.Image = (Image)ListItemsResource.ResourceManager.GetObject(image);
            
        //    Init();

        
        //}
        private void Init()
        {
            //this.FocusedColor = Color.WhiteSmoke; // Color.Silver;//.IndianRed;//.Khaki;//.LightYellow;
            this.backColor = m_backColor;// Color.White;
            //if (itemPicture.Image != null)
            {
                //Bitmap bmp = new Bitmap(itemPicture.Image);
                itemPicture.TransparentColor = m_backColor;// Color.White; //bmp.GetPixel(0, 0);
                //bmp.Dispose();
            }

        }
        public Image Picture
        {
            set
            {
                itemPicture.Image = value;
            }
        }
        public new OptionItemID ID
        {
            set
            {
                base.ID = (int)value;
            }
            get
            {
                return (OptionItemID)base.ID;
            }
        }

        private void OptionsListITem_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled)
            {
                switch (ID)
                {
                    case OptionItemID.ABOUT:
                        this.itemPicture.Image = ListItemsResource.Analyze;
                        break;
                    case OptionItemID.COMPLETE_TASK:
                        this.itemPicture.Image = ListItemsResource.Symbol_Check_2;
                        break;
                    case OptionItemID.COMPLETE_TASK_LATER:
                        this.itemPicture.Image = ListItemsResource.Save;
                        break;
                    case OptionItemID.EXIT:
                        this.itemPicture.Image = ListItemsResource.Logout;
                        break;
                    case OptionItemID.EXIT_WITHOUT_SAVING:
                        this.itemPicture.Image = ListItemsResource.Symbol_Delete_2;
                        break;
                    case OptionItemID.LOGOUT:
                        this.itemPicture.Image = ListItemsResource.Logout;
                        break;
                    case OptionItemID.MAIN_MENU:
                        this.itemPicture.Image = ListItemsResource.Menu;
                        break;
                    case OptionItemID.MANUAL_LOCATION:
                        this.itemPicture.Image = ListItemsResource.Symbol_Edit;
                        break;
                    case OptionItemID.REFRESH:
                        this.itemPicture.Image = ListItemsResource.Refresh;
                        break;
                    case OptionItemID.STAGE_ULD:
                        itemPicture.Image = ListItemsResource.Objects;
                        break;
                    case OptionItemID.MANUAL_PEICE_ENTRY:
                        itemPicture.Image = ListItemsResource.Assembly;
                        break;
                    case OptionItemID.PRINT_ULD_LABEL:
                        itemPicture.Image = ListItemsResource.Printer_Laser;
                        break;
                    case OptionItemID.VISUAL_INSPECTION:
                        itemPicture.Image = ListItemsResource.Visual_Inspection_32x32;
                        break;
                    case OptionItemID.RESET_SCREENING:
                        itemPicture.Image = ListItemsResource.Reset;
                        break;
                    case OptionItemID.CANCEL_SCREENING:
                        itemPicture.Image = ListItemsResource.Symbol_Delete_2;
                        break;
                }
            }
            else
            {
                switch (ID)
                {
                    case OptionItemID.ABOUT:
                        //this.itemPicture.Image = ListItemsResource.Analyze;
                        break;
                    case OptionItemID.COMPLETE_TASK:
                        this.itemPicture.Image = ListItemsResource.Symbol_Check_2_dis;
                        break;
                    case OptionItemID.COMPLETE_TASK_LATER:
                        this.itemPicture.Image = ListItemsResource.Save_Dis;
                        break;
                    case OptionItemID.EXIT:
                        //this.itemPicture.Image = ListItemsResource.Logout;
                        break;
                    case OptionItemID.EXIT_WITHOUT_SAVING:
                        this.itemPicture.Image = ListItemsResource.Symbol_Delete_2_dis;
                        break;
                    case OptionItemID.LOGOUT:
                        //this.itemPicture.Image = ListItemsResource.Logout;
                        break;
                    case OptionItemID.MAIN_MENU:
                        this.itemPicture.Image = ListItemsResource.Menu;
                        break;
                    case OptionItemID.MANUAL_LOCATION:
                        this.itemPicture.Image = ListItemsResource.Symbol_Edit_dis;
                        break;
                    case OptionItemID.REFRESH:
                        //this.itemPicture.Image = ListItemsResource.Refresh;
                        break;
                    case OptionItemID.STAGE_ULD:
                        itemPicture.Image = ListItemsResource.Objects_dis;
                        break;
                    case OptionItemID.MANUAL_PEICE_ENTRY:
                        itemPicture.Image = ListItemsResource.Assembly_dis;
                        break;
                    case OptionItemID.PRINT_ULD_LABEL:
                        itemPicture.Image = ListItemsResource.Printer_Laser_dis;
                        break;
                    case OptionItemID.VISUAL_INSPECTION:
                        itemPicture.Image = ListItemsResource.Visual_Inspection_32x32_dis;
                        break;
                    case OptionItemID.RESET_SCREENING:
                        itemPicture.Image = ListItemsResource.Reset_dis;
                        break;
                    case OptionItemID.CANCEL_SCREENING:
                        itemPicture.Image = ListItemsResource.Symbol_Delete_2_dis;
                        break;

                }
                

            }

        }
    }
}
