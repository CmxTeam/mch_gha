﻿namespace SmoothListBox.UI.ListItems
{
    partial class ViewMasterbillItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
            this.buttonEnter = new OpenNETCF.Windows.Forms.Button2();
            this.textBoxHousebill = new CargoMatrix.UI.CMXTextBox();
            this.SuspendLayout();
            // 
            // cmxTextBox1
            // 
            this.cmxTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBox1.Location = new System.Drawing.Point(3, 12);
            this.cmxTextBox1.Name = "cmxTextBox1";
            this.cmxTextBox1.Size = new System.Drawing.Size(66, 28);
            this.cmxTextBox1.TabIndex = 32;
            // 
            // buttonEnter
            // 
            this.buttonEnter.ActiveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonEnter.ActiveBorderColor = System.Drawing.Color.DarkRed;
            this.buttonEnter.ActiveForeColor = System.Drawing.Color.Black;
            this.buttonEnter.BackColor = System.Drawing.Color.White;
            this.buttonEnter.BorderColor = System.Drawing.Color.White;
            this.buttonEnter.DisabledBackColor = System.Drawing.Color.White;
            this.buttonEnter.DisabledBorderColor = System.Drawing.Color.White;
            this.buttonEnter.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonEnter.Location = new System.Drawing.Point(196, 11);
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(41, 27);
            this.buttonEnter.TabIndex = 31;
            this.buttonEnter.Text = "Enter";
            this.buttonEnter.TransparentImage = true;
            this.buttonEnter.Click += new System.EventHandler(this.buttonEnter_Click_1);
            // 
            // textBoxHousebill
            // 
            this.textBoxHousebill.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxHousebill.Location = new System.Drawing.Point(84, 12);
            this.textBoxHousebill.Name = "textBoxHousebill";
            this.textBoxHousebill.Size = new System.Drawing.Size(95, 28);
            this.textBoxHousebill.TabIndex = 30;
            // 
            // ViewMasterbillItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.cmxTextBox1);
            this.Controls.Add(this.buttonEnter);
            this.Controls.Add(this.textBoxHousebill);
            this.Name = "ViewMasterbillItem";
            this.Size = new System.Drawing.Size(240, 50);
            this.ResumeLayout(false);

        }

        #endregion

        private CargoMatrix.UI.CMXTextBox cmxTextBox1;
        private OpenNETCF.Windows.Forms.Button2 buttonEnter;
        private CargoMatrix.UI.CMXTextBox textBoxHousebill;

    }
}
