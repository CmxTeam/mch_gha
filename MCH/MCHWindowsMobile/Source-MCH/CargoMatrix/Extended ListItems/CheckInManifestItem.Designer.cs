﻿namespace SmoothListBox.UI.ListItems
{
    partial class CheckInManifestItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Terminal = new System.Windows.Forms.Label();
            this.labelLocation = new System.Windows.Forms.Label();
            this.Weight = new System.Windows.Forms.Label();
            this.pictureBoxAlert = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // Terminal
            // 
            this.Terminal.Location = new System.Drawing.Point(23, 7);
            this.Terminal.Name = "Terminal";
            this.Terminal.Size = new System.Drawing.Size(100, 20);
            this.Terminal.Text = "<Terminal>";
            // 
            // Location
            // 
            this.labelLocation.Location = new System.Drawing.Point(25, 37);
            this.labelLocation.Name = "Location";
            this.labelLocation.Size = new System.Drawing.Size(100, 20);
            this.labelLocation.Text = "<location>";
            // 
            // Weight
            // 
            this.Weight.Location = new System.Drawing.Point(161, 5);
            this.Weight.Name = "Weight";
            this.Weight.Size = new System.Drawing.Size(76, 20);
            this.Weight.Text = "<weight>";
            // 
            // pictureBoxAlert
            // 
            this.pictureBoxAlert.Location = new System.Drawing.Point(205, 26);
            this.pictureBoxAlert.Name = "pictureBoxAlert";
            this.pictureBoxAlert.Size = new System.Drawing.Size(32, 32);
            // 
            // CheckInManifestItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.pictureBoxAlert);
            this.Controls.Add(this.Weight);
            this.Controls.Add(this.labelLocation);
            this.Controls.Add(this.Terminal);
            this.Name = "CheckInManifestItem";
            this.Size = new System.Drawing.Size(240, 60);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Terminal;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Label Weight;
        private System.Windows.Forms.PictureBox pictureBoxAlert;
    }
}
