﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    
    public partial class OptionsListNode : StandardListItem , IOptionsListNode// UserControl
    {
        public delegate void ApplyNodeClickedHandler(string nodeName, bool[] values);
        public event ApplyNodeClickedHandler ApplyNodeClicked;
        
        SmoothListBoxWithOptionsApply m_list;
        bool[] m_values = null;
        ApplyListItem m_applyItem;
        public enum ListNodeTye { SINGLE, MULTIPLE, SINGLE_WITH_APPLY_BUTTON };
        ListNodeTye m_type = ListNodeTye.SINGLE;
        public OptionsListNode()
        {
            this.title.Width = 175;
            InitializeComponent();
            m_list = new SmoothListBoxWithOptionsApply();
            
            pictureBoxExpand.Image = ListItemsResource.Collapse_Right;
            
        }
        public OptionsListNode(string title, string image)
        {
            this.title.Width = 175;
            InitializeComponent();
            m_list = new SmoothListBoxWithOptionsApply();
            m_list.ListItemClicked += new ListItemClickedHandler(m_list_ListItemClicked);


            pictureBoxExpand.Image = ListItemsResource.Collapse_Right;


            if (image != null)
                this.itemPicture.Image = (Image)ListItemsResource.ResourceManager.GetObject(image);
            Title = title;
            m_applyItem = new ApplyListItem();
            m_applyItem.buttonApply.Click += new EventHandler(buttonApply_Click);

            //this.FocusedColor = Color.WhiteSmoke; // Color.Silver;//.IndianRed;//.Khaki;//.LightYellow;
            this.backColor = Color.White;
            //if (itemPicture.Image != null)
            {
                //Bitmap bmp = new Bitmap(itemPicture.Image);
                itemPicture.TransparentColor = Color.White; ;// bmp.GetPixel(0, 0);
                //bmp.Dispose();
            }

            pictureBoxExpand.TransparentColor = Color.White;
        
        }
        public OptionsListNode(string title, Image picture)
        {
            this.title.Width = 175;
            InitializeComponent();
            m_list = new SmoothListBoxWithOptionsApply();
            m_list.ListItemClicked += new ListItemClickedHandler(m_list_ListItemClicked);
            
            
            pictureBoxExpand.Image = ListItemsResource.Collapse_Right;
            

            itemPicture.Image = picture;
            Title = title;
            m_applyItem = new ApplyListItem();
            m_applyItem.buttonApply.Click += new EventHandler(buttonApply_Click);

            //this.FocusedColor = Color.WhiteSmoke; // Color.Silver;//.IndianRed;//.Khaki;//.LightYellow;
            this.backColor = Color.White;
            //if (itemPicture.Image != null)
            {
                //Bitmap bmp = new Bitmap(itemPicture.Image);
                itemPicture.TransparentColor = Color.White; ;// bmp.GetPixel(0, 0);
                //bmp.Dispose();
            }

            pictureBoxExpand.TransparentColor = Color.White;
            
        }

        void m_list_ListItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (m_type == ListNodeTye.SINGLE)
            {
                buttonApply_Click(this, EventArgs.Empty);
                
            }
            //throw new NotImplementedException();
        }

        void buttonCancel_Click(object sender, EventArgs e)
        {
            CancelSelection();
            if (ApplyNodeClicked != null)
                ApplyNodeClicked(this.title.Text, null);

        }
        public void CancelSelection()
        {
            for (int i = 0; i < m_list.selectedItemsMap.Count; i++)
            {
                m_list.selectedItemsMap[m_list.itemsPanel.Controls[i]] = m_values[i];
                if (m_list.itemsPanel.Controls[i] is IExtendedListItem)
                {
                    (m_list.itemsPanel.Controls[i] as IExtendedListItem).SelectedChanged(m_values[i]);
                    (m_list.itemsPanel.Controls[i] as IExtendedListItem).Focus(false);
                    
                }
                
            }


        
        }
        void buttonApply_Click(object sender, EventArgs e)
        {
            m_list.selectedItemsMap.Values.CopyTo(m_values, 0);
            

            if (ApplyNodeClicked != null)
                ApplyNodeClicked(this.title.Text, m_values);

            
        }
        public SmoothListBoxBase GetList()
        {
            CancelSelection();
            if (m_type == ListNodeTye.MULTIPLE)
            {
                
                m_list.AddOptionsApplyControl(m_applyItem);
                m_list.MultiSelectEnabled = true;
                m_list.UnselectEnabled = true;
            }
            else if (m_type == ListNodeTye.SINGLE)
            {

                //m_list.AddOptionsApplyControl(m_applyItem);
                m_list.MultiSelectEnabled = false;
                m_list.UnselectEnabled = false;

            }
            else if (m_type == ListNodeTye.SINGLE_WITH_APPLY_BUTTON)
            {
                
                m_list.AddOptionsApplyControl(m_applyItem);
                m_list.MultiSelectEnabled = false;
                m_list.UnselectEnabled = false;
                
            }

            
            return m_list;
        
        }
        public void AddItem(Control control)
        {
            m_list.AddItem(control);
            m_values = new bool[m_list.selectedItemsMap.Count];
            m_list.selectedItemsMap.Values.CopyTo(m_values, 0);
        }

        public void AddItem(Control control, bool isChecked)
        {
            m_list.AddItem(control);
            m_list.selectedItemsMap[control] = isChecked;
            m_values = new bool[m_list.selectedItemsMap.Count];
            m_list.selectedItemsMap.Values.CopyTo(m_values, 0);

            if (control is IExtendedListItem)
            {
                (control as IExtendedListItem).SelectedChanged(isChecked);
                (control as IExtendedListItem).Focus(false);
            }
        }
        public string Title
        {
            get { return title.Text; }
            set 
            { 
                title.Text = value;
                m_list.m_titleText = title.Text;
            }
        }
        public ListNodeTye ListType
        {
            set { m_type = value; }
            get { return m_type; }
        }
    }
}
