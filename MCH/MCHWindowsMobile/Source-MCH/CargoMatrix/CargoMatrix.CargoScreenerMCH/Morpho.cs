﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using System.Data;
using CargoMatrix.Communication.WSScannerMCHService;
using System.Collections;
using System.Threading;

namespace CargoMatrix.CargoScreener


{
    public partial class Morpho : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;
  
        protected CargoMatrix.UI.CMXTextBox searchBox;
        private int currentuserid = 0;
        private string devicenumber = string.Empty;
        private string shipmentnumber = string.Empty;
        private int pieces = 0;
        private string lastshipment = string.Empty;
        private string filename = string.Empty;

        System.Threading.Thread myThread = null;
        bool isRunning = false;

        public Morpho()
        {
 
            this.Visible = false;

            InitializeComponent();

            searchBox.WatermarkText = "Scan/Enter shipment";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            DoBarcodeEnabled(false);


        }

  
 

        void buttonScannedList_Click(object sender, System.EventArgs e)
        {

 

        }


        void buttonSearch_Click(object sender, System.EventArgs e)
        {
            DoBarcodeEnabled(false);
            ValidateShipment(searchBox.Text);
            DoBarcodeEnabled(true);
        }


        void UldViewButton_Click(object sender, EventArgs e)
        {
            DoStart();
        }

        void DoStop()
        {

            


            shipmentnumber = "";
            if (myThread != null)
            {
                isRunning = false;
                try
                {
                    myThread.Abort();
                }
                catch { }
                this.UldButton.Text = "START";
                this.label2.Text = "";
                this.label3.Text = "";
                this.label4.Text = "";
                myThread = null;
            }
            LoadControl();
        }

        void DoStart()
        {


            this.label2.Text = "";
            this.label3.Text = "";
            this.label4.Text = "";

            if (myThread == null)
            {
  
                if (shipmentnumber == string.Empty)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Please scan or enter shipment.", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    LoadControl();
                    return;
                }

                if (IsDeviceReady())
                {
                    myThread = new System.Threading.Thread(new System.Threading.ThreadStart(DoScreening));
                    myThread.Start();
                    this.UldButton.Text = "STOP";
                }
                else
                {

                    //string msg = GetSystemMessage(devicenumber);
                    this.label2.Text = "Message: Device not ready";
                    //this.label3.Text = "Results: " + msg;
                    this.label3.Text = "";
                    this.label4.Text = "";


        



                    CargoMatrix.UI.CMXMessageBox.Show("Device not ready.", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                }

            }
            else
            {
               
                isRunning = false;
                try
                {
                    myThread.Abort();
                }
                catch { }
                this.UldButton.Text = "START";
                this.label2.Text = "";
                this.label3.Text = "";
                this.label4.Text = "";
                myThread = null;
            }
           

        }


        bool IsDeviceReady()
        {
            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery(" SELECT        SystemMessage FROM            ITDX  WHERE        (SerialNumber = '" + devicenumber + "')");
            try
            {

                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SystemMessage"].ToString().ToLower() == "no alarm - ready")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }

                else
                {
                    return false;
                }


            }
            catch
            {
                return false;

            }
        }

        void DoScreening()
        {



            bool isNew = false;
            isRunning = true;
            this.Invoke(new EventHandler(delegate
            {

                
                while (isRunning)
                {





                    DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery(" SELECT    SubstancesFound,   FileName, SystemMessage, ScreeningResults FROM            ITDX  WHERE      (UserId=" + currentuserid + ") and  (SerialNumber = '" + devicenumber + "')");
            try
            {

                if (dt.Rows.Count > 0)
                {
                    CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteNonQuery("UPDATE    ITDX SET                LastConnected = GETDATE() WHERE        (SerialNumber = '" + devicenumber + "') AND (UserId = " + currentuserid + ")");

                    this.label2.Text = "Message: " + dt.Rows[0]["SystemMessage"].ToString();
                    //this.label3.Text = "Results: " + dt.Rows[0]["ScreeningResults"].ToString();


                    if (dt.Rows[0]["SystemMessage"].ToString().ToLower() == "sampling...")
                    {
                        isNew = true;
                    }


                    if (dt.Rows[0]["FileName"].ToString() != "")
                    {
                        if (isNew)
                        {
                            filename = dt.Rows[0]["FileName"].ToString();
                            this.label3.Text = "Results: " + dt.Rows[0]["ScreeningResults"].ToString();
                            this.label4.Text = "File: " + filename;
                            isRunning = false;
                            try
                            {
                                myThread.Abort();
                            }
                            catch { }
                            this.UldButton.Text = "START";
                            shipmentnumber = "";
                            pieces = 0;

                            if (dt.Rows[0]["ScreeningResults"].ToString().ToLower() == "alarm")
                            {
                                this.label2.Text = "Message: " + dt.Rows[0]["SystemMessage"].ToString() + " - " + dt.Rows[0]["SubstancesFound"].ToString();
                                CargoMatrix.UI.CMXMessageBox.Show("* * * A L A R M * * *\n\n" + dt.Rows[0]["SystemMessage"].ToString() + " \nSubstances: " + dt.Rows[0]["SubstancesFound"].ToString(), "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                            }
                            else
                            {
                                CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
 

                            }

                            UldButton.Enabled = false;

                            myThread = null;
                            return;
                        }
                        else
                        {
                            filename = "";
                            this.label4.Text = "";
                            this.label3.Text = "";
                        }
                    }
                    else
                    {
                        filename = "";
                        this.label4.Text = "";
                        this.label3.Text = "";
                    }


                }

                else
                {
                    //devicenumber = "";
                    //this.label1.Text = "Device: " + devicenumber;
                    DoStop();
                    this.label2.Text ="Message: " + "Unavailable";
                    this.label3.Text = "";
                    this.label4.Text = "";
                }
 
   
            }
            catch
            {
                this.label2.Text = "Message: " + "Error";
                this.label3.Text = "";
                this.label4.Text = "";

            }



                  
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(1000);
                    Application.DoEvents();
                }
 
          
            }));
        }

        void ProceedButton_Click(object sender, EventArgs e)
        {
            //DoBarcodeEnabled(false);
            isRunning = false;
            try
            {
                myThread.Abort();
            }
            catch { }
    
            myThread = null;
            CargoMatrix.UI.CMXAnimationmanager.GoBack();

            //DoBarcodeEnabled(true);
        }
        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MORPHO_DEVICE));
        }


        public override void LoadControl()
        {


            Cursor.Current = Cursors.WaitCursor;
            this.SuspendLayout();

            DoBarcodeEnabled(false);
            DoBarcodeEnabled(true);

           
     

              currentuserid = CargoMatrix.Communication.WebServiceManager.UserID();


            if (devicenumber == string.Empty)
            {
                this.devicenumber = SelectDevice();
            }

      


            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
 

            this.pictureBox2.Image = global::Resources.Graphics.Skin.nav_bg;

            //ProceedButton.BackgroundImage = Resources.Skin.button;
            //ProceedButton.ActiveBackgroundImage = Resources.Skin.button_over;

            //UldButton.BackgroundImage = Resources.Skin.button;
            //UldButton.ActiveBackgroundImage = Resources.Skin.button_over;

            if (shipmentnumber != string.Empty)
            {
                this.TitleText = "Cargo Screener - " + shipmentnumber + "(" + pieces.ToString() + ")";
                
                UldButton.BackgroundImage = Resources.Skin.button;
                UldButton.ActiveBackgroundImage = Resources.Skin.button_over;
                UldButton.Enabled = true;
            }
            else
            {
                this.TitleText = "Cargo Screener";
                UldButton.Enabled = false;
                UldButton.BackgroundImage = Resources.Skin.btn_dis;
                UldButton.ActiveBackgroundImage = Resources.Skin.btn_dis;
                UldButton.DisabledBackgroundImage = Resources.Skin.btn_dis;
               
            }




            itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            this.label1.Text = "Device: " + devicenumber ;

            this.label2.Text = "";
            this.label3.Text = "";
            this.label4.Text = "";

            if (lastshipment == string.Empty)
            {
                this.label6.Text = "Last Scanned: N/A";
            }
            else
            {
                this.label6.Text = "Last Scanned: " + lastshipment + " Pcs: " + pieces.ToString();
            }
           
            this.buttonScannedList.Value = 0;



            this.searchBox.Visible = true;
            this.label6.Visible = true;
            this.ProceedButton.Visible = true;
            this.UldButton.Visible = true;
            this.buttonScannedList.Visible = false;
            this.itemPicture.Visible = false;


            this.Refresh();
            this.ResumeLayout(false);
            this.Visible = true;
            Cursor.Current = Cursors.Default;

        }

        private void DoBarcodeEnabled(bool enable)
        {
            if (enable)
            {
                if (!BarcodeEnabled)
                {
                    BarcodeEnabled = false;
                    BarcodeEnabled = true;
                }
            }
            else
            {
                BarcodeEnabled = false;
            }
        }



        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
        
        }


        protected void searchBox_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:

                    e.Handled = true;
                    break;
                case Keys.Enter:
                    ValidateShipment(searchBox.Text);
                    e.Handled = true;
                    break;

                case Keys.Up:

                    e.Handled = true;
                    break;
            }



        }


        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            DoBarcodeEnabled(false);
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.MORPHO_DEVICE:
                        DoStop();
                        this.devicenumber = SelectDevice();
                        LoadControl();
                        break;
                }

            DoBarcodeEnabled(false);
            DoBarcodeEnabled(true);
        }


        void MawbList_BarcodeReadNotify(string barcodeData)
        {
            

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (IsDevice(barcodeData))
            {
                if (IsDiviceAvailable(barcodeData))
                {
                    LinkDevice(barcodeData, currentuserid);
                    this.devicenumber = barcodeData;
                    LoadControl();
                }
                else
                {
                    if (DialogResult.Cancel == CargoMatrix.UI.CMXMessageBox.Show("This device is being used by a different user. Do you want to force connection?", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        LoadControl();
                    }
                    else
                    {
                        this.devicenumber = barcodeData;
                        LoadControl();
                    }
                }
            }
            else
            {
                ValidateShipment(barcodeData);
            }


            Cursor.Current = Cursors.Default;
            //DoBarcodeEnabled(true);
        }

        string SelectDevice()
        {


            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Devices";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "Cargo Screener";


            actPopup.RemoveAllItems();


            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("SELECT    Id, SerialNumber FROM            ITDX WHERE     (DATEDIFF(hh, LastConnected, GETDATE()) = 0) AND    (IsActive = 1) ORDER BY SerialNumber");
            if (dt != null)
            {

                if (dt.Rows.Count == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("There are no devices available.", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return string.Empty;
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(dt.Rows[i]["SerialNumber"].ToString(), CargoMatrix.Resources.Icons.detector, int.Parse(dt.Rows[i]["Id"].ToString())));
                }
            }
       
 

            if (DialogResult.OK == actPopup.ShowDialog())
            {
        
                if (IsDiviceAvailable(actPopup.SelectedItems[0].Name))
                {
                    LinkDevice(actPopup.SelectedItems[0].Name, currentuserid);
                    return actPopup.SelectedItems[0].Name;
                }
                else
                {
                    if (DialogResult.Cancel == CargoMatrix.UI.CMXMessageBox.Show("This device is being used by a different user. Do you want to force connection?", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        return this.devicenumber;
                    }
                    else
                    {
                        LinkDevice(actPopup.SelectedItems[0].Name, currentuserid);
                        return actPopup.SelectedItems[0].Name;
                    }
                }
            }
            else
            {
                return this.devicenumber;
            }
    
        }




       bool IsDevice (string device)
        {

            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("SELECT        Id, SerialNumber, UserId FROM            ITDX WHERE        (IsActive = 1) AND                          (SerialNumber = '" + device + "') ORDER BY SerialNumber");
            try
            {
                if (dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }



       string GetSystemMessage(string device)
       {
           DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery(" SELECT        SystemMessage FROM            ITDX  WHERE        (SerialNumber = '" + devicenumber + "')");
           try
           {

               if (dt.Rows.Count > 0)
               {
                   return dt.Rows[0]["SystemMessage"].ToString();

               }

               else
               {
                   return String.Empty;
               }


           }
           catch
           {
               return String.Empty;

           }
       }


        bool IsDiviceAvailable(string device)
        {

            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("SELECT        Id, SerialNumber, UserId FROM            ITDX WHERE        (IsActive = 1) AND                          (SerialNumber = '" + device + "') ORDER BY SerialNumber");
            try
            {
                int dbuser = int.Parse(dt.Rows[0]["UserId"].ToString());
                if (dbuser > 0)
                {
                    if (dbuser == currentuserid)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
             
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        void LinkDevice(string device, int userid)
        {
            try
            {
                CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteNonQuery("UPDATE       ITDX SET    FileName='', SystemMessage='', SystemReadiness='', ScreeningResults='',      UserId = " + userid.ToString() + " WHERE        (SerialNumber = '" + device + "')");
            }
            catch
            {
         
            }
        }

        void ValidateShipment(string shipment)
        {

            DoStop();

             
            shipmentnumber = string.Empty;
            pieces = 0;
            if (devicenumber == string.Empty)
            {
                CargoMatrix.UI.CMXMessageBox.Show("You are currently not connected to any device. Please select a device.", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                LoadControl();
                return;
            }



            AwbInfo awb =   CargoMatrix.Communication.ScannerMCHServiceManager.Instance.GetAwbInfo(shipment);
            if (awb == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid Shipment.", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                LoadControl();
                return;
            }

            if (awb.HasAlarm)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("This shipment has an Alarm. Do you want to clear it?", "Cargo Screener", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                   // MessageBox.Show("alarm cleared");
                }
            }

            if (awb.Alert != null)
            {
                foreach (Alert item in awb.Alert)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + shipment, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                }
            }

            
            int remaningPieces = awb.Pieces;
            int enteredPieces = 0;
            if (DoPieces(shipment, remaningPieces, out enteredPieces, false))
            {

                pieces = enteredPieces;
                shipmentnumber = shipment;
                lastshipment = shipment;
                LoadControl();
                DoStart();
            }
            else
            {
                LoadControl();
            }
 


        }


        bool DoPieces(string reference, int remaningPieces, out int enteredPieces, bool allowOverage)
        {

            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.PieceCount = remaningPieces;
            CntMsg.LabelReference = reference;
            CntMsg.AllowOverage = allowOverage;
            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                enteredPieces = CntMsg.PieceCount;
                return true;
            }
            else
            {
                enteredPieces = 0;
                return false;
            }

        }

    }

}
