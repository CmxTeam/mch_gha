﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using System.Reflection;
namespace CargoMatrixScanner
{
    public partial class LoginMainScreen : SmoothListbox.SmoothListbox// CargoMatrix.UI.CMXUserControl
    {
        public object m_activeApp;
        SmoothListbox.SmoothListbox login = null;// = new Login();

        public LoginMainScreen()
        {
            InitializeComponent();
            this.LoadOptionsMenu += new EventHandler(LoginMainScreen_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(LoginMainScreen_MenuItemClicked);
            smoothListBoxMainList.Visible = false;
            panelTitle.Height = 0;
            labelTitleDescription.Text = CargoMatrixScannerResource.labelTitleDescription;
            pictureBox21.Image = CargoMatrixScannerResource.User_Group_Labor_48;
            //pictureBox2.Image = CargoMatrixScannerResource.Contact_Card_48;
            labelLogin.Text = CargoMatrixScannerResource.Text_Login;
            labelScanCard.Text = CargoMatrixScannerResource.Text_Scan_card;


            // manual login button
            button21.Text = CargoMatrixScannerResource.Text_Manual_Login;
            button21.Image = CargoMatrixScannerResource.manual_scan;
            button21.PressedImage = CargoMatrixScannerResource.manual_scan_over;

            button22.Text = CargoMatrixScannerResource.Text_Scan_card;
            button22.Image = CargoMatrixScannerResource.barcode_scan_over;
            button22.PressedImage = CargoMatrixScannerResource.barcode_scan;

            button21.ForeColor = Color.White;
            button22.ForeColor = Color.White;
        }

        //void button21_MouseDown(object sender, MouseEventArgs e)
        //{

        //    button22.Image = CargoMatrixScannerResource.barcode_scan;
        //    button22.Refresh();
        //    //throw new NotImplementedException();
        //}
        public override void LoadControl()
        {
            //if (CargoMatrix.Communication.ScannerUpdate.Instance.IsNewerVersionAvailable() &&
            //   DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(CargoMatrixScannerResource.Text_NewVersion, "Update Available", CargoMatrix.UI.CMXMessageBoxIcon.Hand, MessageBoxButtons.OK, DialogResult.OK))
            //    CargoMatrix.UI.CMXAnimationmanager.ExitForm();
        }

 

        void LoginMainScreen_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.EXIT)
                {
                    //if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(CargoMatrixScannerResource.ExitDialog_Text,
                    //    CargoMatrixScannerResource.ExitDialog_Title, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                    //    MessageBoxButtons.OKCancel, DialogResult.Cancel))
                    {
                        //RDF
                         
                        CargoMatrix.UI.CMXAnimationmanager.ExitForm();
                        
                         
                    }
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.ABOUT)
                {
                    CMXAboutUs.ShowAboutUs();
                }


            }

        }

        void LoginMainScreen_LoadOptionsMenu(object sender, EventArgs e)
        {

            smoothListBoxOptions.AddItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ABOUT) { DescriptionLine = CargoMatrixScannerResource.Text_Version });
            smoothListBoxOptions.AddItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.EXIT));

            //throw new NotImplementedException();
        }

        //private void cmxPictureButtonUserID_Click(object sender, EventArgs e)
        //{
        //    if(login == null)
        //        login = new Login();

        //    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(login);

        //}

        private void cmxPictureButtonExit_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(CargoMatrixScannerResource.ExitDialog_Text,
                        CargoMatrixScannerResource.ExitDialog_Title, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                        MessageBoxButtons.OKCancel, DialogResult.Cancel))
            {
                CargoMatrix.UI.CMXAnimationmanager.ExitForm();
            }
        }

        private void LoginMainScreen_BarcodeReadNotify(string barcodeData)
        {
            CargoMatrix.UI.CMXSound.Play(CargoMatrixScannerResource.Pass);
            CargoMatrix.Communication.WebServiceManager wsm = CargoMatrix.Communication.WebServiceManager.Instance();
            if (wsm == null)
                return;

      
            Cursor.Current = Cursors.WaitCursor;
            if (wsm.LoginByPin(barcodeData) == 0)
            {
                //wsm.SyncTime();
                CargoMatrix.UI.CMXAnimationmanager.GetParent().DoLogin();

            }
            else
            {
                CargoMatrix.UI.CMXSound.Play(CargoMatrixScannerResource.Fail);
                this.barcode.StartRead();
            }
            Cursor.Current = Cursors.Default;

        }


        //int animationIdex = 0;
        //private void AnimationTimer_Tick(object sender, EventArgs e)
        //{
        //    //switch (animationIdex)
        //    //{ 
        //    //    case 0:
        //    //        pictureBox2.Height -= 2;
        //    //        pictureBox2.Width -= 2;
        //    //        pictureBox2.Top += 1;
        //    //        pictureBox2.Left += 1;
        //    //        break;
        //    //    case 1:
        //    //        pictureBox2.Height += 2;
        //    //        pictureBox2.Width += 2;
        //    //        pictureBox2.Top -= 1;
        //    //        pictureBox2.Left -= 1;
        //    //        break;
        //    //}

        //    //animationIdex++;
        //    //animationIdex %= 2;

        //}

        private void LoginMainScreen_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled)
            {
                //AnimationTimer.Enabled = true;
                button22.Image = CargoMatrixScannerResource.barcode_scan_over;
                //button22.DisabledBackgroundImage = CargoMatrixScannerResource.barcode_scan_over;
            }
            //else
            //    AnimationTimer.Enabled = false;
        }




        private void button21_Click(object sender, EventArgs e)
        {



            Cursor.Current = Cursors.WaitCursor;
            //if (login == null)
            {
                try
                {


					CargoMatrix.Communication.Json.Managers.MembershipManager mbrMgr = CargoMatrix.Communication.Json.Managers.MembershipManager.Instance;
					var res = mbrMgr.GetCompanySecuritySettings(CargoMatrix.Communication.Common.Settings.Instance.CompanyName);
					switch (res.MobileAuthMethod)
					{
						case CargoMatrix.Communication.Common.Enumerations.MobileAuthMethod.None:
							break;
						case CargoMatrix.Communication.Common.Enumerations.MobileAuthMethod.Pin:
							login = new LoginByPin();
							break;
						case CargoMatrix.Communication.Common.Enumerations.MobileAuthMethod.Password:
							login = new Login();
							break;
						case CargoMatrix.Communication.Common.Enumerations.MobileAuthMethod.PinAndPassword:
							//TODO: provide login method selection
							login = new LoginByPin();
							break;
						default:
							break;
					}

					//switch (CargoMatrix.Communication.SecurityManager.Instance.LoginType())
					//{
					//    case CargoMatrix.Communication.WSPieceScan.LoginTypes.Pin:
					//        login = new LoginByPin();
					//        break;
					//    case CargoMatrix.Communication.WSPieceScan.LoginTypes.Password:
					//        login = new Login();
					//        break;
					//    case CargoMatrix.Communication.WSPieceScan.LoginTypes.ActiveDirectory:
					//        break;
					//}

                }
                catch
                {
                    Cursor.Current = Cursors.Default;
                    return;
                }

            }
            button22.Image = CargoMatrixScannerResource.barcode_scan;
            button22.Refresh();
            button21.Refresh();
			if (login != null)
				CargoMatrix.UI.CMXAnimationmanager.DisplayForm(login);
            Cursor.Current = Cursors.Default;

        }

        private void button21_MouseDown(object sender, MouseEventArgs e)
        {
            button22.Image = CargoMatrixScannerResource.barcode_scan;
            button22.Refresh();
        }
    }
}
