﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
//using SmoothListbox;
using CargoMatrix.Communication.DTO;// CustomListItems.MainMenuItem;
using System.Reflection;
using CargoMatrix.TaskManager;
using CargoMatrix.Communication.Json.Managers;
using CargoMatrix.Communication.Common.DTO.Menu;
using CargoMatrix.Communication.Common;



namespace CargoMatrixScanner
{
    public partial class MainMenu : SmoothListbox.SmoothListbox// UserControl
    {

        //private CargoMatrix.Communication.Data.User currentUser;
        public object m_activeApp;
        //CustomListItems.MainMenuItem.ViewHouseBillItem viewHouseBill;
        //CustomListItems.MainMenuItem.ViewMasterbillItem viewMasterBill;
        public MainMenu()
        {
            InitializeComponent();
            smoothListBoxMainList.UnselectEnabled = true;
            TitleText = "Main Menu";
            Top = CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight;
            panelSoftkeys.Enabled = true;
            pictureBoxMenu.Click += new EventHandler(pictureBoxMenu_Click);
        }

        void pictureBoxMenu_Click(object sender, EventArgs e)
        {
        }


  
        void MainMenu_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        {
            if (listItem is SmoothListbox.ListItems.ListItemWithButton)
                switch ((listItem as CustomListItems.MainMenuItem).ActionID)
                {
                    #region Cargo Photo Capture
                    case 104: // freight photo capture
                        {
                            if (CargoMatrix.Communication.Utilities.CameraPresent)
                            {
                                Cursor.Current = Cursors.WaitCursor;
                                try
                                {
                                    if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.FreightPhotoCapture.TaskList"))
                                    {
                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {

                                        Assembly SampleAssembly;
                                        SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                                        // Obtain a reference to a method known to exist in assembly.
                                        Type myType;

                                        myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                                        if (myType != null)
                                        {
                                            m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                            (m_activeApp as UserControl).Location = new Point(Left, Top);
                                            (m_activeApp as UserControl).Size = new Size(Width, Height);

                                            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                        }
                                        else
                                        {
                                            CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //CargoMatrix.UI.CMXMessageBox.Show(ex.Message, "Error!" + " (60002)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60002);
                                }
                            }
                            else
                            {
                                CargoMatrix.UI.CMXMessageBox.Show("Cannot open Freight Phototo Capture. Camera is not found on this device. Contact Administrator to configure the settings of this device.", "Camera Not Found", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            }
                        }
                        break;
                    #endregion

                    #region  domestic check in
                    case 36: // domestic check in
                        {

                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.FreightHandler.DomesticCheckInList"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightHandler.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.FreightHandler.DomesticCheckInList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Domestic Check In Module", "Error!" + " (60003)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60004);
                            }
                        }
                        break;
                    #endregion

                    #region import check in
                    case 38: // import check in
                        {

                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.FreightHandler.ImportCheckInList"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightHandler.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.FreightHandler.ImportCheckInList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Import Check In Module", "Error!" + " (60005)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60006);
                            }
                        }
                        break;
                    #endregion

                    #region Cargo Receiver
                    case 46: // Cargo Receiver
                        {

                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoReceiver.CargoReceiver"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoReceiver.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.CargoReceiver.TaskList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoReceiver Module", "Error!" + " (60007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60008);
                            }
                        }
                        break;
                    #endregion

                    #region Cargo Receiver MCH
                    case 1: // Cargo Receiver
                        {



                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoReceiver.CargoReceiverMCH"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoReceiverMCH.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.CargoReceiver.TaskList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoReceiver Module", "Error!" + " (60007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60008);
                            }
                        }
                        break;
                    #endregion

                    #region Cargo Loader MCH
                    case 5: // Cargo Receiver
                        {



                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoReceiver.CargoLoaderMCH"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoLoaderMCH.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.CargoLoader.TaskList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoLoader Module", "Error!" + " (60007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60008);
                            }
                        }
                        break;
                    #endregion

                    #region Cargo Screening MCH
                    case 7: // Cargo Receiver
                        {



                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoMatrix.CargoScreenerMCH"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoScreenerMCH.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.CargoScreener.MainMenu");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoScreener Module", "Error!" + " (60007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60008);
                            }
                        }
                        break;
                    #endregion



                    #region Cargo Accept MCH
                    case 9: // Cargo Receiver
                        {



                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoMatrix.CargoAcceptMCH"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoAcceptMCH.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.CargoAccept.TaskList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoAccept Module", "Error!" + " (60007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60008);
                            }
                        }
                        break;
                    #endregion

                    #region freight screeining
                    case 105: // freight screeining
                        if (CargoMatrix.Communication.HostPlusIncomming.Instance.IsScreeningCertified())
                        {

                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {

                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.FreightScreening.MainMenu"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightScreening.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.FreightScreening.MainMenu");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Screening Module", "Error!" + " (60009)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60010);
                            }
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("You are not a TSA certified user.", "Access Denied", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        }
                        break;
                    #endregion

                    #region cargoLoader
                    case 35: // domestic check in Load Consol
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.LoadConsol.ConsolList"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.LoadConsol.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.LoadConsol.ConsolList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Laod Console Module", "Error!" + " (60011)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60012);
                            }
                        }
                        break;
                    #endregion

                    #region Inventory
                    case 40: // Inventory
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.Inventory.InventoryMenu"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.Inventory.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.Inventory.InventoryMenu");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Inventory Module", "Error!" + " (60014)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60014);
                            }
                        } break;
                    #endregion

                    #region CargoDischarge
                    case 97: // Discharge
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.FreightDischarge.DischargeList"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightDischarge.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.FreightDischarge.DischargeList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoMatrix.FreightDischarge Module", "Error!" + " (60015)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60015);
                            }
                        }
                        break;
                    #endregion

                    #region TruckLoad
                    case 44: // Cargo Load Truck
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoTruckLoad.MawbList"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoTruckLoad.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.CargoTruckLoad.MawbList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoMatrix.CargoTruckLoad Module", "Error!" + " (60016)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60016);
                            }
                        }
                        break;
                    #endregion

                    #region Cargo Measurements
                    case 114: // Cargo Measurements
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoMeasurements.MeasurementsCapture.RedeemDimensions"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoMeasurements.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.CargoMeasurements.MeasurementsCapture.RedeemDimensions");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoMatrix.MeasurementsCapture.RedeemDimensions Module", "Error!" + " (60017)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60017);
                            }
                        }
                        break;

                    #endregion

                    #region Cargo Utilities
                    case 115: // Cargo Utilities
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoUtilities.UtilitiesList"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {
                                    Assembly sampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoUtilities.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType = sampleAssembly.GetType("CargoMatrix.CargoUtilities.UtilitiesList");

                                    if (myType != null)
                                    {
                                        m_activeApp = sampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoMatrix.CargoUtilities Module", "Error!" + " (60018)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60018);
                            }
                        }
                        break;
                    #endregion

                    #region AQM
                    case 16:
                        Cursor.Current = Cursors.WaitCursor;
                        try
                        {
                            if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.AQM.AQMList"))
                            {
                                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                            }
                            else
                            {
                                Assembly sampleAssembly = Assembly.LoadFrom("CargoMatrix.AQM.dll");
                                // Obtain a reference to a method known to exist in assembly.
                                Type myType = sampleAssembly.GetType("CargoMatrix.AQM.AQMList");

                                if (myType != null)
                                {
                                    m_activeApp = sampleAssembly.CreateInstance(myType.FullName);

                                    (m_activeApp as UserControl).Location = new Point(Left, Top);
                                    (m_activeApp as UserControl).Size = new Size(Width, Height);

                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {
                                    CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoMatrix.AQM Module", "Error!" + " (60019)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60019);
                        }
                        break;
                    #endregion

                    #region Cargo Tender
                    case 117: // Cargo Utilities
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoTender.MasterbillList"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {
                                    Assembly sampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoTender.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType = sampleAssembly.GetType("CargoMatrix.CargoTender.MasterbillList");

                                    if (myType != null)
                                    {
                                        m_activeApp = sampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoMatrix.CargoTender.MasterbillList Module", "Error!" + " (60020)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60020);
                            }
                        }
                        break;
                    #endregion

                    #region Receiver OHL
                    case 121: // Cargo Receiver OHL
                        {

                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.CargoReceiverOHL.CargoReceiver"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    //string str = m_activeApp.ToString();
                                    //Type type = m_activeApp.GetType();
                                }
                                else
                                {

                                    Assembly SampleAssembly;
                                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.CargoReceiverOHL.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType;

                                    myType = SampleAssembly.GetType("CargoMatrix.CargoReceiverOHL.ManifestList");
                                    if (myType != null)
                                    {
                                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load CargoReceiver OHL Module", "Error!" + " (60021)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60021);
                            }
                        }
                        break;
                    #endregion
                    /*
                    #region Test App
                    case 120: // Test App
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "SmartDeviceTest.TestApp.TestListBox"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {
                                    Assembly sampleAssembly = Assembly.LoadFrom("SmartDeviceTest.exe");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType = sampleAssembly.GetType("SmartDeviceTest.TestApp.TestListBox");

                                    if (myType != null)
                                    {
                                        m_activeApp = sampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load SmartDeviceTest Module", "Error!" + " (60021)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60021);
                            }
                        }
                        break;
                    #endregion
                        */
                    #region OnHand
                    case 118:
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.OnHand.OnHandTasksList"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {
                                    Assembly sampleAssembly = Assembly.LoadFrom("CargoMatrix.OnHand.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType = sampleAssembly.GetType("CargoMatrix.OnHand.OnHandTasksList");

                                    if (myType != null)
                                    {
                                        m_activeApp = sampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load OnHand Module", "Error!" + " (60022)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60022);
                            }
                        }
                        break;
                    #endregion

                    #region Load Container
                    case 120:
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.LoadContainer.LoadContainerTasks"))
                                {
                                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }
                                else
                                {
                                    Assembly sampleAssembly = Assembly.LoadFrom("CargoMatrix.LoadContainer.dll");
                                    // Obtain a reference to a method known to exist in assembly.
                                    Type myType = sampleAssembly.GetType("CargoMatrix.LoadContainer.LoadContainerTasks");

                                    if (myType != null)
                                    {
                                        m_activeApp = sampleAssembly.CreateInstance(myType.FullName);

                                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                    }
                                    else
                                    {
                                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Load Container Module", "Error!" + " (60023)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60023);
                            }
                        }
                        break;
                    #endregion
                    default:
                        break;
                }
        }

        private void LoadConsol()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if ((m_activeApp != null) && (m_activeApp.GetType().FullName == "CargoMatrix.LoadConsol.ConsolList"))
                {
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                    //string str = m_activeApp.ToString();
                    //Type type = m_activeApp.GetType();
                }
                else
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.LoadConsol.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    myType = SampleAssembly.GetType("CargoMatrix.LoadConsol.ConsolList");
                    if (myType != null)
                    {
                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Laod Console Module", "Error!" + " (60009)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                }

            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60004);
            }
        }

        private void MainMenu_EnabledChanged(object sender, EventArgs e)
        {

            //base.OnEnabledChanged(e);

            if (Enabled == false)
            {
                smoothListBoxMainList.RemoveAll();//.itemsPanel.Controls.Clear();
                LayoutItems();

                return;
            }
            if (m_activeApp != null)
            {
                (m_activeApp as UserControl).Dispose();
                m_activeApp = null;
            }
            ReloadList();
        }

        private void MainMenu_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.LOGOUT)
                {
                    //if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to logout?",
                    //   "Confirm Logout", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                    //   MessageBoxButtons.OKCancel, DialogResult.Cancel))
                    if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                        CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.REFRESH)
                {
                    ReloadList();
                }
            }
        }

        protected override void SetBackButton()
        {
            pictureBoxBack.Enabled = true;

        }
        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            if (m_bOptionsMenu)
                base.pictureBoxBack_Click(sender, e);
            else
            {
                pictureBoxBack.Refresh();
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(CargoMatrixScannerResource.LogoutDialog_Text,
                            CargoMatrixScannerResource.LogoutDialog_Title, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                            MessageBoxButtons.OKCancel, DialogResult.Cancel))
                    CargoMatrix.UI.CMXAnimationmanager.DoLogout();
            }

        }

        private void MainMenu_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
        }

        void ReloadList()
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxMainList.RemoveAll();

            CargoMatrix.Communication.DTO.MainMenuItem[] mainMenuItems = null;
            bool newVersionAvailable;

            //if (CargoMatrix.Communication.WebServiceManager.Instance().GetMainMenu(out mainMenuItems, out newVersionAvailable))
			List<AppNavigation> menuLst = null;
			try
			{
				menuLst = MenuManager.Instance.GetAppRoleMenus(new AppRoleMenuModel { AppName = Settings.Instance.AppName, RoleId = MembershipManager.Instance.User.RoleId, ApplicationTypeId = CargoMatrix.Communication.Common.Enumerations.ApplicationTypes.Android });
				List<MainMenuItem> mainMenuItemsLst = new List<MainMenuItem>();
				if (menuLst != null)
				{
					foreach (var item in menuLst)
					{
						mainMenuItemsLst.Add(new MainMenuItem 
						{
							 ActionID = (int)item.Id,							  
							 Label = item.Text,
							  
						});
					}
				}
			}
			catch (Exception ex)
			{
			}
			List<AppNavigation> res = menuLst;
			newVersionAvailable = false;
            if (CargoMatrix.Communication.WebServiceManager.Instance().GetMainManuMCH(out mainMenuItems, out newVersionAvailable))
            {

                if (newVersionAvailable && DialogResult.OK ==
                    CargoMatrix.UI.CMXMessageBox.Show(CargoMatrixScannerResource.Text_NewVersion, "Update Available", CargoMatrix.UI.CMXMessageBoxIcon.Hand, MessageBoxButtons.OK, DialogResult.OK))
                {
                    CargoMatrix.UI.CMXAnimationmanager.ExitForm();
                    return;
                }
                //AddMainListItem(new CustomListItems.MainMenuItem.CounterListItem(0));
                //AddMainListItem(new CustomListItems.MainMenuItem.CounterListItem(9));
                //AddMainListItem(new CustomListItems.MainMenuItem.CounterListItem(10));
                //AddMainListItem(new CustomListItems.MainMenuItem.CounterListItem(100));

                bool isAdmin = CargoMatrix.Communication.Utilities.IsAdmin;

                for (int i = 0; i < mainMenuItems.Length; i++)
                {
                    var menuItem = mainMenuItems[i];

                    bool buttonEnabled = isAdmin &&
                            (menuItem.ActionID == 117 ||  // CARGO TENDER
                             menuItem.ActionID == 110 ||  // PULL CONSOL
                             menuItem.ActionID == 109 ||  // SHARE PULL AND LOAD
                             menuItem.ActionID == 105 ||  // CARGO SCREENER
                             menuItem.ActionID == 104 ||  // CARGO PHOTO CAPTURE
                             menuItem.ActionID == 103 ||  // FREIGHT RECEIVE
                             menuItem.ActionID == 46 ||  // CARGO RECEIVER
                             menuItem.ActionID == 44 ||  // CARGO TRUCK LOAD
                             menuItem.ActionID == 40 ||  // CARGO INVENTORY
                             menuItem.ActionID == 35 ||  // CARGO LOADER
                             menuItem.ActionID == 97);    // CARGO DISCHARGE

                    var item = new CustomListItems.MainMenuItem(mainMenuItems[i], buttonEnabled);

                    if (buttonEnabled)
                    {
                        item.OnButtonClick += new EventHandler(MainMenuItem_OnButtonClick);
                    }

                    AddMainListItem(item);
                }
            }

 
            //var screening = new CustomListItems.MainMenuItem(GetScreener(), false);
            //AddMainListItem(screening);



            Cursor.Current = Cursors.Default;
        }

        //CargoMatrix.Communication.DTO.MainMenuItem GetScreener()
        //{
        //    CargoMatrix.Communication.DTO.MainMenuItem screener = new CargoMatrix.Communication.DTO.MainMenuItem();
        //    screener.ActionID = 100;
        //    screener.Completed = "";
        //    screener.Icon = "Freight_Screening";
        //    screener.Label = "Cargo Screener";
        //    screener.NotAssigned = "0";
        //    screener.NotCompleted = "0";
        //    screener.NotCompletedTaskCountByUserID = "0";
        //    screener.TaskCountByUser = "0";
        //    screener.TotalCount = "0";
             
        //    return screener;
        //}

        void MainMenuItem_OnButtonClick(object sender, EventArgs e)
        {
            var item = (CustomListItems.MainMenuItem)sender;
            TaskList taskPopup = new TaskList(item.ActionID.ToString(), item.MenuItem.Label);
            taskPopup.ShowDialog();
            ReloadList();
        }
    }
}
