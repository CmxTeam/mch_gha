﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.UI;
using OpenNETCF.Windows.Forms;

namespace CargoMatrixScanner
{
    public partial class Login : SmoothListbox.SmoothListbox//CargoMatrix.UI.CMXUserControl
    {
        bool needFocus = true;
        public Login()
        {
            InitializeComponent();

            this.LoadOptionsMenu += new EventHandler(Login_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(Login_MenuItemClicked);
            smoothListBoxMainList.Visible = false;
            panelTitle.Height = 0;

            textBoxUserID.Focus();
            
            this.textBoxPassword.PasswordChar = '*';

            labelUserID.Text = CargoMatrixScannerResource.Text_UserID;
            labelPassword.Text = CargoMatrixScannerResource.Text_Password;
            labelTitleDescription.Text = CargoMatrixScannerResource.Text_Login_Caption;

            // login button
            buttonLogin.BackgroundImage = CargoMatrix.Resources.Skin.button_default;
            buttonLogin.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;
            buttonLogin.Text = CargoMatrixScannerResource.Text_Login;
            //buttonLogin.Image = CargoMatrixScannerResource.Image_Login;
            //buttonLogin.ImageAlign = OpenNETCF.Drawing.ContentAlignment2.MiddleLeft;
            buttonLogin.TextAlign = OpenNETCF.Drawing.ContentAlignment2.MiddleCenter;
            buttonLogin.TransparentImage = true;

            //clear button

            buttonClear.BackgroundImage = CargoMatrix.Resources.Skin.button_default;
            buttonClear.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;
            buttonClear.Text = CargoMatrixScannerResource.Text_Clear;
            //buttonClear.Image = CargoMatrixScannerResource.Image_cleanup;
            //buttonClear.ImageAlign = OpenNETCF.Drawing.ContentAlignment2.MiddleLeft;
            buttonClear.TextAlign = OpenNETCF.Drawing.ContentAlignment2.MiddleCenter;
            buttonClear.TransparentImage = true;

            //// back button
            //buttonBack.Text = CargoMatrixScannerResource.Text_Back;
            //buttonBack.Image = CargoMatrixScannerResource.GoBack;
            //buttonBack.ImageAlign = OpenNETCF.Drawing.ContentAlignment2.TopCenter;
            //buttonBack.TextAlign = OpenNETCF.Drawing.ContentAlignment2.BottomCenter;
            //buttonBack.TransparentImage = true;

            //// back exit
            //buttonExit.Text = CargoMatrixScannerResource.Text_Exit;
            //buttonExit.Image = CargoMatrixScannerResource.Logout;
            //buttonExit.ImageAlign = OpenNETCF.Drawing.ContentAlignment2.TopCenter;
            //buttonExit.TextAlign = OpenNETCF.Drawing.ContentAlignment2.BottomCenter;
            //buttonExit.TransparentImage = true;

           
        }

        void Login_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.EXIT)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(CargoMatrixScannerResource.ExitDialog_Text,
                        CargoMatrixScannerResource.ExitDialog_Title, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                        MessageBoxButtons.OKCancel, DialogResult.Cancel))
                    {

                        CargoMatrix.UI.CMXAnimationmanager.ExitForm();
                    }
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.ABOUT)
                {
                    CMXAboutUs.ShowAboutUs();
                }


            }

        }

        void Login_LoadOptionsMenu(object sender, EventArgs e)
        {
            //smoothListBoxOptions.AddItem(new CustomListItems.OptionsListITem(CargoMatrixScannerResource.Text_Back, CargoMatrixScannerResource.GoBack));

            smoothListBoxOptions.AddItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ABOUT) { DescriptionLine = CargoMatrixScannerResource.Text_Version});
            smoothListBoxOptions.AddItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.EXIT));
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            SendLoginRequest();
        }
        private void SendLoginRequest()
        {
            
            WebServiceManager wsm = WebServiceManager.Instance();

            if (wsm == null)
                return;
            
            Cursor.Current = Cursors.WaitCursor;
            int err = wsm.LoginByUserID(textBoxUserID.Text, textBoxPassword.Text);
            switch(err)
            {
                case 0:
                    //wsm.SyncTime();
                    CargoMatrix.UI.CMXAnimationmanager.GetParent().DoLogin();
                    break;
                case -1:

                    CargoMatrix.UI.CMXMessageBox.Show(CargoMatrixScannerResource.Text_Invalid_Username, CargoMatrixScannerResource.Text_Error, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    textBoxPassword.SelectAll();
                    textBoxPassword.Focus();
                    break;
                case 1:

                    CargoMatrix.UI.CMXMessageBox.Show(CargoMatrixScannerResource.Text_Invalid_Password, CargoMatrixScannerResource.Text_Error, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    textBoxPassword.SelectAll();
                    textBoxPassword.Focus();
                    break;

                case -2:
                default:
                    textBoxPassword.SelectAll();
                    textBoxPassword.Focus();
                    break;
            }
            Cursor.Current = Cursors.Default;
            
                        
        }

        private void Login_BarcodeReadNotify(string barcodeData)
        {
            CargoMatrix.UI.CMXSound.Play(CargoMatrixScannerResource.Pass);
            WebServiceManager wsm = WebServiceManager.Instance();
            if (wsm == null)
                return;


            

            Cursor.Current = Cursors.WaitCursor;
            if (wsm.LoginByPin(barcodeData)==0)
            {
                //wsm.SyncTime();
                CargoMatrix.UI.CMXAnimationmanager.GetParent().DoLogin();
                
            }
            else
            {
                CargoMatrix.UI.CMXSound.Play(CargoMatrixScannerResource.Fail);
                this.barcode.StartRead();

                ClearFields();
            }
            Cursor.Current = Cursors.Default;

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            CargoMatrix.UI.CMXAnimationmanager.GoBack();
        }

        private void cmxPictureButtonBack_Click(object sender, EventArgs e)
        {
            CargoMatrix.UI.CMXAnimationmanager.GetParent().GoBack();
        }

        private void cmxPictureButtonExit_Click_1(object sender, EventArgs e)
        {
            if (DialogResult.OK == CMXMessageBox.Show(CargoMatrixScannerResource.ExitDialog_Text,
                        CargoMatrixScannerResource.ExitDialog_Title, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                        MessageBoxButtons.OKCancel, DialogResult.Cancel))
            {

                CargoMatrix.UI.CMXAnimationmanager.ExitForm();
            }
        }

        private void Login_EnabledChanged(object sender, EventArgs e)
        {


            ClearFields();
            needFocus = Enabled;
            
        }

        private void cmxPictureButtonGoBack_Click(object sender, EventArgs e)
        {
            CargoMatrix.UI.CMXAnimationmanager.GetParent().GoBack();
        }

        private void textBoxUserID_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            { 
                case Keys.Tab:
                case Keys.Down:
                case Keys.Enter:
                    textBoxPassword.Focus();
                    e.Handled = true;
                    break;
            }   
            
                
        }

        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    buttonLogin.Focus();
                    e.Handled = true;
                    break;
                case Keys.Enter:
                    buttonLogin.Focus();
                    buttonLogin.Refresh();
                    SendLoginRequest();
                    e.Handled = true;
                    break;

                case Keys.Up:
                    textBoxUserID.Focus();
                    e.Handled = true;
                    break;
            }
            
            

        }

        private void textBoxUserID_GotFocus(object sender, EventArgs e)
        {
            textBoxUserID.SelectAll();
        }

        private void textBoxPassword_GotFocus(object sender, EventArgs e)
        {
            textBoxPassword.SelectAll();
        }

        private void textBoxUserID_EnabledChanged(object sender, EventArgs e)
        {
            //textBoxUserID.Focus();
        }

        private void ClearFields()
        {
            textBoxUserID.Text = "";
            textBoxPassword.Text = "";
            textBoxUserID.Focus();
        
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            SendLoginRequest();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            CargoMatrix.UI.CMXAnimationmanager.GoBack();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == CMXMessageBox.Show(CargoMatrixScannerResource.ExitDialog_Text,
                        CargoMatrixScannerResource.ExitDialog_Title, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                        MessageBoxButtons.OKCancel, DialogResult.Cancel))
            {
                CargoMatrix.UI.CMXAnimationmanager.ExitForm();
            }
        }

        private void textBoxPassword_Click(object sender, EventArgs e)
        {

        }

        private void Login_GotFocus(object sender, EventArgs e)
        {
            try
            {
                textBoxUserID.Focus();
            }
            catch (ObjectDisposedException) { }

        }

        private void Login_Paint(object sender, PaintEventArgs e)
        {
            if (this.Enabled)
            {
                if (needFocus)
                {
                    needFocus = false;
                    textBoxUserID.Focus();

                }
            }
        }

    }
}
