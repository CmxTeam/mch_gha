﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Xml;


namespace CargoMatrixScanner
{
    
    public partial class ConnectionForm : Form
    {
        
        public ConnectionForm()
        {
            InitializeComponent();
            comboBoxCamera.SelectedIndex = 0;

            if (File.Exists(CargoMatrix.Communication.WebServiceManager.ConfigFilename) == true)
            {
                CargoMatrix.Communication.CMXConfig config;
                XmlSerializer s = new XmlSerializer(typeof(CargoMatrix.Communication.CMXConfig));

                TextReader r = new StreamReader(CargoMatrix.Communication.WebServiceManager.ConfigFilename);
                config = (CargoMatrix.Communication.CMXConfig)s.Deserialize(r);
                r.Close();
                textBoxConnectionString.Text = config.connectionName;
                textBoxGateway.Text = config.gateway;
                textBoxUpdaterWS.Text = config.UpdaterWSURL;
                textBoxURL.Text = config.ScannerWSURL;
                comboBoxCamera.SelectedIndex = config.camera;
                
            }

            
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            //string []str = new string[4];
            //str[0] = textBoxConnectionString.Text;
            //str[1] = textBoxGateway.Text;
            //str[2] = textBoxURL.Text;
            //str[3] = Convert.ToString(comboBoxCamera.SelectedIndex);
            
            //XmlSerializer s = new XmlSerializer(typeof(string[]));
            //TextWriter w = new StreamWriter(@"ConnectionString.xml");
            //s.Serialize(w, str);
            

            WriteXML();

            this.Close();
        }

        void WriteXML()
        {
            try
            {
                //pick whatever filename with .xml extension
                //string filename = "CMXScannerConfig.xml";

                CargoMatrix.Communication.CMXConfig config = new CargoMatrix.Communication.CMXConfig();
                config.camera = comboBoxCamera.SelectedIndex;
                config.connectionName = textBoxConnectionString.Text;
                config.gateway = textBoxGateway.Text;
                config.ScannerWSURL = textBoxURL.Text;
                config.UpdaterWSURL = textBoxUpdaterWS.Text;

                XmlSerializer s = new XmlSerializer(typeof(CargoMatrix.Communication.CMXConfig));

                XmlTextWriter writer = new XmlTextWriter(CargoMatrix.Communication.WebServiceManager.ConfigFilename, null);
                writer.Formatting = Formatting.Indented;
                s.Serialize(writer, config);
                writer.Close();
                


             
            }
            catch (Exception ex)
            {
                //WriteError(ex.ToString());
                CargoMatrix.UI.CMXMessageBox.Show(ex.Message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
        }

    }
    
}