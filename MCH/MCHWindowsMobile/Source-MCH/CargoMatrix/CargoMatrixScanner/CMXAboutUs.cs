﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrixScanner
{
    
    public partial class CMXAboutUs : SmoothListbox.SmoothListbox
    {
        public CMXAboutUs()
        {
            InitializeComponent();
            panelTitle.Height = 0;
            smoothListBoxMainList.Visible = false;
            labelVersion.Text = "Version: " + CargoMatrixScannerResource.Text_Version;
            labelProduct.Text = "Product: " + CargoMatrixScannerResource.Text_Product;
            pictureBoxMenu.Enabled = false;
        }
        public static void ShowAboutUs()
        {
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new CMXAboutUs());
        }
    }
}
