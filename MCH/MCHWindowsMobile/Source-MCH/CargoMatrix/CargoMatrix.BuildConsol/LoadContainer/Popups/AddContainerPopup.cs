﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using SmoothListbox.ListItems;
using CargoMatrix.LoadConsol;

namespace CargoMatrix.LoadContainer
{
    public partial class AddContainerPopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        private IULDType containerType;
        private string containerNumber;
        private int hawbId;

        public int HawbId
        {
            get { return hawbId; }
        }

        public IULDType ContainerType
        {
            get { return containerType; }
        }

        public string ContainerNumber
        {
            get { return containerNumber; }
        }

        public AddContainerPopup(IULDItem container)
        {
            InitializeComponent();
            this.Title = container == null ? "Add New Container" : "Edit Container";
            if (null != container)
            {
                containerType = container;
                textBoxContainerNo.Text = container.ULDNo;
                textBoxContainerType.Text = container.ULDName;
                if (container is Communication.LoadConsol.ULD)
                {
                    var hb = (container as Communication.LoadConsol.ULD).FirstHousebill;
                    this.hawbId = hb.HousebillId;
                    this.textBoxHawbNo.Text = hb.HousebillNumber;
                }
            }
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            buttonOk.Enabled = !string.IsNullOrEmpty(textBoxContainerNo.Text) &&
                !string.IsNullOrEmpty(textBoxContainerType.Text) &&
                !string.IsNullOrEmpty(textBoxHawbNo.Text);
        }

        void buttonContainerType_Click(object sender, System.EventArgs e)
        {
            var uld = SharedMethods.DisplayULDTypes(false,CargoMatrix.Communication.ScannerUtilityWS.MOT.Ocean);
            if (uld != null)
            {
                this.containerType = uld;
                this.textBoxContainerType.Text = uld.ULDName;
            }
        }


        public IHouseBillItem DisplayHawbList()
        {
            var hawbList = new CustomUtilities.SearchMessageListBox();
            hawbList.HeaderText = "Select Housebill";
            hawbList.MultiSelectListEnabled = false;
            hawbList.OneTouchSelection = true;

            foreach (var hawb in Communication.LoadConsol.Instance.GetLoadContainerHousebills(ForkLiftViewer.Instance.Mawb.MasterBillId))
            {
                hawbList.smoothListBoxBase1.AddItem2(new StandardListItem<IHouseBillItem>(hawb.HousebillNumber, null, hawb));
            }
            hawbList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == hawbList.ShowDialog())
            {
                return (hawbList.SelectedItems[0] as StandardListItem<IHouseBillItem>).ItemData;
            }
            else return null;
        }

        void buttonHawb_Click(object sender, System.EventArgs e)
        {
            var hawb = DisplayHawbList();
            if (hawb != null)
            {
                textBoxHawbNo.Text = hawb.HousebillNumber;
                this.hawbId = hawb.HousebillId;
            }
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            this.containerNumber = textBoxContainerNo.Text;
            DialogResult = DialogResult.OK;
        }

    }
}
