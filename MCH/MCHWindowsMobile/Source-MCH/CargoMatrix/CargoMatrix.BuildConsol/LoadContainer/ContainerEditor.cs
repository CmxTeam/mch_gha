﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CustomListItems;
using CustomUtilities;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.UI;
using System.Linq;
using CargoMatrix.LoadConsol;
using CMXExtensions;

namespace CargoMatrix.LoadContainer
{
    public partial class ContainerEditor : CargoUtilities.SmoothListBox2Options
    {

        private CustomListItems.HeaderItem headerItem;
        private CargoMatrix.Utilities.MessageListBox uldContentList;

        public ContainerEditor()
        {

            InitializeComponent();
            headerItem = new CustomListItems.HeaderItem();
            headerItem.Label1 = ForkLiftViewer.Instance.Mawb.Reference();
            headerItem.Label2 = ForkLiftViewer.Instance.Mawb.Line2();
            headerItem.Label3 = "ADD/EDIT ULD";

            headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            headerItem.Location = panel1.Location;
            panel1.Visible = true;
            this.panel1.Controls.Add(headerItem);
            headerItem.BringToFront();
            panel1.Height = headerItem.Height;
            panelHeader2.Height = headerItem.Height;

            this.TitleText = "CargoLoader ULD Setup";

            smoothListBoxMainList.ListItemClicked += new SmoothListbox.ListItemClickedHandler(smoothListBoxMainList_ListItemClicked);
            LoadOptionsMenu += new EventHandler(ULDBuilder_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ULDBuilder_MenuItemClicked);
            this.ButtonFinalizeText = "Next";
            headerItem.Logo = ForkLiftViewer.Instance.CarrierLogo;

        }

        void ULDBuilder_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.PRINT_ULD_LABEL:
                        populatePrintableUlds();
                        break;
                    case OptionsListITem.OptionItemID.PRINT_MAWB_LABEL:
                        BarcodeEnabled = false;
                        CustomUtilities.MawbLabelPrinter.Show(ForkLiftViewer.Instance.Mawb);
                        BarcodeEnabled = true;
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL:
                        SharedMethods.FinalizeAndTakePictures();
                        break;
                }
            }
        }

        private void DisplayExistingULDS()
        {
            smoothListBoxMainList.RemoveAll();
            var existingUlds = CargoMatrix.Communication.LoadConsol.Instance.GetAttachedULDs(ForkLiftViewer.Instance.Mawb.MasterBillId, MOT.Ocean);
            foreach (var item in existingUlds)
            {
                string line1 = string.Format("{0} - {1}", item.ULDName, item.ULDNo);
                string linte2 = string.Format("Wgt {0} : Hbs {1}", Decimal.Round(Convert.ToDecimal(item.Weight), 2), item.WsULD.Housebills.Count());
                var uld = new ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>(line1, linte2, item);
                uld.ButtonDeleteClick += new EventHandler(Ulditem_ButtonDeleteClick);
                uld.ButtonEditClick += new EventHandler(Ulditem_ButtonEditClick);
                smoothListBoxMainList.AddItem(uld);
            }
        }

        void ULDBuilder_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_ULD_LABEL));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_MAWB_LABEL));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL));
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }

        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            var ulds = smoothListBoxMainList.Items.OfType<ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>>();

            if (ulds.Any(uld => (!string.IsNullOrEmpty(uld.ItemData.ULDNo))))
            {
                Cursor.Current = Cursors.WaitCursor;

                OnhandLoader loadHwabForm = new OnhandLoader();

                (loadHwabForm as UserControl).Location = new Point(Left, Top);
                (loadHwabForm as UserControl).Size = new Size(Width, Height);

                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(loadHwabForm as CargoMatrix.UI.CMXUserControl);
                smoothListBoxMainList.Reset();
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_UldNoNumberAssigned, "ULD Number", CMXMessageBoxIcon.Hand);
            }
        }

        #region ULDLabelPrinting
        private void populatePrintableUlds()
        {
            CargoMatrix.Utilities.MessageListBox printableUlds = new CargoMatrix.Utilities.MessageListBox();
            printableUlds.ButtonVisible = true;
            printableUlds.ButtonImage = CargoMatrix.Resources.Skin.printerButton;
            printableUlds.ButtonPressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            var defPrinter = CargoMatrix.Communication.ScannerUtility.Instance.GetDefaultPrinter(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD);
            printableUlds.HeaderText = defPrinter.PrinterName;
            printableUlds.HeaderText2 = "Select ULDs to print labels";

            printableUlds.ButtonClick += delegate(object sender, EventArgs e)
            {
                if (CustomUtilities.PrinterUtilities.PopulateAvailablePrinters(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD, ref defPrinter))
                    printableUlds.HeaderText = defPrinter.PrinterName;

            };



            var uldsCheckItems = from uld in smoothListBoxMainList.Items.OfType<ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>>()
                                 where !uld.ItemData.IsLoose
                                 select new CustomListItems.CkeckItem<IULDItem>(uld.Line1, uld.ID, uld.ItemData);

            if (uldsCheckItems.Count() > 1)
            {
                var selectAllItem = new CkeckItem<int>("Select All", 0, 0);
                printableUlds.AddItem(selectAllItem);
                printableUlds.ListItemClicked += (sender, item, isSelected) =>
                {
                    if (item is CkeckItem<int>)
                    {
                        if (isSelected)
                            printableUlds.smoothListBoxBase1.SelectAll();
                        else
                            printableUlds.smoothListBoxBase1.SelectNone();
                    }
                    else
                    {
                        if (isSelected && printableUlds.Items.OfType<CkeckItem<IULDItem>>().All(ci => ci.IsSelected))
                            selectAllItem.SelectedChanged(true);
                        else
                        {
                            selectAllItem.SelectedChanged(false);
                            printableUlds.smoothListBoxBase1.selectedItemsMap[selectAllItem] = false;
                        }
                    }
                };
            }

            printableUlds.AddItems(uldsCheckItems.OfType<Control>().ToArray());

            if (DialogResult.OK == printableUlds.ShowDialog())
            {
                var uldsToPrint = printableUlds.SelectedItems.OfType<CkeckItem<IULDItem>>().ToList<CkeckItem<IULDItem>>();
                SendPrintingJobToPrinter(uldsToPrint, defPrinter);
            }
        }

        private void SendPrintingJobToPrinter(IEnumerable<CkeckItem<IULDItem>> list, CargoMatrix.Communication.ScannerUtilityWS.LabelPrinter selectedPrinter)
        {
            if (selectedPrinter.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileLabel ||
    selectedPrinter.PrinterType == CargoMatrix.Communication.ScannerUtilityWS.PrinterType.MobileWireless)
            {
                /// Capture printing reason if printed from mobile printer

                var mbilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(selectedPrinter);
                foreach (var item in list.OfType<CustomListItems.CkeckItem<IULDItem>>())
                    mbilePrinter.UldPrint(item.ItemData.ULDNo);
            }
            else            /// if network printer
            {

                foreach (var item in list.OfType<CustomListItems.CkeckItem<IULDItem>>())
                    CargoMatrix.Communication.ScannerUtility.Instance.PrintULDLabel(item.ItemData.ID, selectedPrinter.PrinterId, 1);
                string message = string.Format("Printing has been scheduled on {0}", selectedPrinter.PrinterName);
                CargoMatrix.UI.CMXMessageBox.Show(message, "Print", CargoMatrix.UI.CMXMessageBoxIcon.Success);

            }
        }

        #endregion


        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            var popup = new AddContainerPopup(null);
            if (DialogResult.OK == popup.ShowDialog())
            {
                TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.AssignULDToMawb(ForkLiftViewer.Instance.Mawb.MasterBillId, popup.ContainerType.TypeID, popup.ContainerNumber, true, MOT.Ocean, popup.HawbId);
                if (status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

            }
            LoadControl();
        }

        public override void LoadControl()
        {
            BarcodeEnabled = false;
            base.LoadControl();
            DisplayExistingULDS();

            Cursor.Current = Cursors.Default;
            Application.DoEvents();
            if (smoothListBoxMainList.ItemsCount == 0)
            {
                ShowContinueButton(false);
                Application.DoEvents();
                //headerItem_ButtonClick(null, EventArgs.Empty);
            }
            else
            {
                ShowContinueButton(false);
                ShowContinueButton(true);
            }
        }


        void Ulditem_ButtonEditClick(object sender, EventArgs e)
        {
            if (!(sender is ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>))
                return;

            var item = (sender as ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>).ItemData;
            var popup = new AddContainerPopup(item);
            if (DialogResult.OK == popup.ShowDialog())
            {
                item.ULDNo = popup.ContainerNumber;
                item.TypeID = popup.ContainerType.TypeID;
                item.ULDName = popup.ContainerType.ULDName;
                Communication.LoadConsol.Instance.UpdateULD(item.ID, item.TypeID, item.ULDNo, MOT.Ocean, popup.HawbId);
                LoadControl();
            }
        }



        void Ulditem_ButtonDeleteClick(object sender, EventArgs e)
        {
            if (!(sender is ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>))
                return;

            var uld = (sender as ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>).ItemData;
            string msg = string.Format("Are you sure you want to delete ULD {0} ?", uld.ULDNo);
            if (DialogResult.OK == UI.CMXMessageBox.Show(msg, "Confirm Delete", CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
            {
                var hb = (uld as Communication.LoadConsol.ULD).FirstHousebill;
                var status = CargoMatrix.Communication.LoadConsol.Instance.DeleteULD(uld.ID, MOT.Ocean, hb.HousebillId);
                if (status.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CMXMessageBoxIcon.Hand);
                }
                LoadControl(); // if delete confirmed reload the uld items

                if (smoothListBoxMainList.ItemsCount == 0)
                    ShowContinueButton(false);
            }
        }

        private void PopulateUldContent(IULDItem uld)
        {
            Cursor.Current = Cursors.WaitCursor;

            uldContentList = new CargoMatrix.Utilities.MessageListBox();
            uldContentList.LabelEmpty = "ULD is Empty";
            uldContentList.HeaderText = string.Format("{0} - {1}", uld.ULDName, uld.ULDNo);
            uldContentList.HeaderText2 = "ULD Content";
            uldContentList.OkEnabled = false;
            uldContentList.IsSelectable = false;

            uldContentList.AddItems(GetULDContent(uld.ID));

            Cursor.Current = Cursors.Default;
            uldContentList.ShowDialog();
            DisplayExistingULDS();

        }


        int selecteduldId;
        void smoothListBoxMainList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            selecteduldId = (listItem as ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>).ItemData.ID;
            PopulateUldContent((listItem as ULDListItem<CargoMatrix.Communication.LoadConsol.ULD>).ItemData);

        }

        private Control[] GetULDContent(int uldId)
        {
            List<Control> uldContnent = new List<Control>();
            foreach (var hawb in CargoMatrix.Communication.LoadConsol.Instance.GetULDContent(uldId, ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.Mawb.Mot))
            {
                List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();
                foreach (var piece in hawb.Pieces)
                    items.Add(new CustomListItems.ComboBoxItemData(string.Format("Piece {0}", piece.PieceNumber), String.Empty /*piece.Location*/, piece.PieceId));
                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawb.HousebillId, hawb.HousebillNumber, string.Format("PCS: {0} of {1}", hawb.ScannedPieces, hawb.TotalPieces), hawb.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode, items);
                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(combo_LayoutChanged);
                combo.IsSelectable = false;
                combo.ContainerName = "ULD";
                uldContnent.Add(combo);
            }

            return uldContnent.ToArray();
        }
        void combo_LayoutChanged(CustomListItems.ComboBox sender, int subItemId, bool comboIsEmpty)
        {
            if (comboIsEmpty)
                uldContentList.RemoveItem(sender as Control);
            TransactionStatus status;
            if (subItemId == -1)
            {
                status = CargoMatrix.Communication.LoadConsol.Instance.RemoveHousebillFromUld(sender.ID, ForkLiftViewer.Instance.Mawb.TaskId, selecteduldId, ForkLiftViewer.Instance.Mawb.Mot);
            }
            else
            {
                status = CargoMatrix.Communication.LoadConsol.Instance.RemovePieceFromULD(sender.ID, subItemId, ForkLiftViewer.Instance.Mawb.TaskId, selecteduldId, ForkLiftViewer.Instance.Mawb.Mot);
                if (status.TransactionStatus1)
                {
                    UpdateDescriptionLine(sender as CustomListItems.ComboBox);
                }
            }

            if (status.TransactionStatus1 == false)
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

            if (uldContentList.smoothListBoxBase1.ItemsCount == 0)
                uldContentList.DialogResult = DialogResult.Cancel;
        }

        void item_OnButtonClick(object sender, EventArgs e)
        {
            SmoothListbox.ListItems.ListItemWithButton item = sender as SmoothListbox.ListItems.ListItemWithButton;
            Control parent = item.Parent;
            parent.Controls.Remove(item);
            parent.Refresh();
        }

        private void UpdateDescriptionLine(CustomListItems.ComboBox sender)
        {
            if (sender is CustomListItems.ComboBox)
            {
                CustomListItems.ComboBox item = sender as CustomListItems.ComboBox;
                string[] splitArray = item.Line2.Split(' ');
                sender.Line2 = string.Format("PCS: {0} of {1}", item.Items.Count.ToString(), splitArray.Last());
            }
        }
    }
}
