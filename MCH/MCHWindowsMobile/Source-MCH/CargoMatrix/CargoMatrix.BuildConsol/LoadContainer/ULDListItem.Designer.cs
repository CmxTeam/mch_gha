﻿namespace CargoMatrix.LoadContainer
{
    partial class ULDListItem<T>
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonEdit = new CargoMatrix.UI.CMXPictureButton();
            this.ButtonDelete = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBox1 = new OpenNETCF.Windows.Forms.PictureBox2();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(42, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 26);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(42, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 12);
            // 
            // ButtonEdit
            // 
            this.ButtonEdit.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonEdit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ButtonEdit.Image = CargoMatrix.Resources.Skin.clipboardButton;
            this.ButtonEdit.Location = new System.Drawing.Point(170, 5);
            this.ButtonEdit.Name = "ButtonEdit";
            this.ButtonEdit.PressedImage = CargoMatrix.Resources.Skin.clipboardButton_over;
            this.ButtonEdit.Size = new System.Drawing.Size(32, 32);
            this.ButtonEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ButtonEdit.TabIndex = 3;
            this.ButtonEdit.TabStop = false;
            this.ButtonEdit.TransparentColor = System.Drawing.Color.White;
            this.ButtonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonDelete.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ButtonDelete.Image = CargoMatrix.Resources.Skin.cc_trash;
            this.ButtonDelete.Location = new System.Drawing.Point(204, 5);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.PressedImage = CargoMatrix.Resources.Skin.cc_trash_over;
            this.ButtonDelete.Size = new System.Drawing.Size(32, 32);
            this.ButtonDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ButtonDelete.TabIndex = 2;
            this.ButtonDelete.TabStop = false;
            this.ButtonDelete.TransparentColor = System.Drawing.Color.White;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pictureBox1.Location = new System.Drawing.Point(4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.TransparentColor = System.Drawing.Color.White;
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // ULDListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ButtonDelete);
            this.Controls.Add(this.ButtonEdit);
            this.Controls.Add(this.label2);
            this.Name = "ULDListItem";
            this.Size = new System.Drawing.Size(240, 44);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private CargoMatrix.UI.CMXPictureButton ButtonEdit;
        private CargoMatrix.UI.CMXPictureButton ButtonDelete;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBox1;
    }
}
