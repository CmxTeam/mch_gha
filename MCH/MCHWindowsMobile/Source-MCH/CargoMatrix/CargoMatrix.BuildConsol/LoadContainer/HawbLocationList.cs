﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CustomListItems;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication;
using CMXExtensions;
using SmoothListbox.ListItems;
using CargoMatrix.LoadConsol;

namespace CargoMatrix.LoadContainer
{
    public partial class HawbLocationList : CargoMatrix.Utilities.MessageListBox
    {
        private HouseBillItem[] housebills;
        private bool buttonTreeMode = false;
        private ScanStatuses filter;
        private HouseBillFields sort;

        public HouseBillItem SelectedHousebill { get; private set; }
        public HawbLocationList(string fltr, string srt)
        {
            EvaluateFilterAndSort(fltr, srt);
            InitializeComponent();
            this.HeaderText = ForkLiftViewer.Instance.Mawb.Reference();
            this.smoothListBoxBase1.IsSelectable = false;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(HawbLocationList_ListItemClicked);



            housebills = CargoMatrix.Communication.LoadConsol.Instance.GetHawbList(ForkLiftViewer.Instance.Mawb.MasterBillId,
                ForkLiftViewer.Instance.Mawb.TaskId, filter, sort, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);

            DisplayHousebills();
        }

        public HawbLocationList(string fltr, string srt, string location)
        {
            EvaluateFilterAndSort(fltr, srt);
            InitializeComponent();
            this.HeaderText = location;
            this.smoothListBoxBase1.IsSelectable = true;
            this.smoothListBoxBase1.MultiSelectEnabled = false;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(HawbLocationList_ListItemClicked);


            HouseBillItem[] hbs = CargoMatrix.Communication.LoadConsol.Instance.GetHawbList(ForkLiftViewer.Instance.Mawb.MasterBillId,
                            ForkLiftViewer.Instance.Mawb.TaskId, filter, sort, ForkLiftViewer.Instance.Mode == ConsolMode.Load, MOT.Ocean);

            housebills = (hbs.Where(hb => hb.PieceLocations.Any(p => p.LocationName.Equals(location, StringComparison.OrdinalIgnoreCase)))).ToArray();

            DisplayHousebills();
        }
        void HawbLocationList_ButtonClick(object sender, EventArgs e)
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Hawb);
            fp.Filter = filter.ToString();
            fp.Sort = sort.ToString();
            if (DialogResult.OK == fp.ShowDialog())
            {
                EvaluateFilterAndSort(fp.Filter, fp.Sort);

                housebills = CargoMatrix.Communication.LoadConsol.Instance.GetHawbList(ForkLiftViewer.Instance.Mawb.MasterBillId,
                                    ForkLiftViewer.Instance.Mawb.TaskId, filter, sort, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);

                DisplayHousebills();
            }
        }

        private void EvaluateFilterAndSort(string fltr, string srt)
        {
            try
            {
                filter = (ScanStatuses)Enum.Parse(typeof(ScanStatuses), fltr, true);

            }
            catch (Exception)
            {

                filter = ScanStatuses.NotScanned;
            }
            try
            {
                sort = (HouseBillFields)Enum.Parse(typeof(HouseBillFields), srt, true);

            }
            catch (Exception)
            {
                sort = HouseBillFields.HousebillNumber;
            }
        }

        // toggle button functionality to switch between list mode and grouped by location mode
        void HawbLocationList_ButtonClick_Toggle(object sender, EventArgs e)
        {
            if (buttonTreeMode)
            {
                this.ButtonImage = Resources.Skin.btn_treeView;
                this.ButtonPressedImage = Resources.Skin.btn_treeView_over;
            }
            else
            {
                this.ButtonImage = Resources.Skin.btn_listView;
                this.ButtonPressedImage = Resources.Skin.btn_listView_over;
            }
            buttonTreeMode = !buttonTreeMode;
            DisplayHousebills();
        }

        private void DisplayHousebills()
        {
            Cursor.Current = Cursors.WaitCursor;
            this.HeaderText2 = string.Format("{0}/{1}", filter, sort);
            smoothListBoxBase1.RemoveAll();
            if (buttonTreeMode) // tree mode
            {
                this.MultiSelectListEnabled = false;
                this.IsSelectable = false;


                var list = from hb in housebills
                           group hb by hb.LastLocation into g
                           select new LocationHousebills() { Location = g.Key, Housebills = g.OrderByDescending(hb => hb.Weight).ToArray() };

                foreach (var Loc in list)
                {
                    var subItems = from hb in Loc.Housebills
                                   select new ExpenListItemData(hb.HousebillId, string.Format("{0}-{1}-{2}", hb.Origin, hb.HousebillNumber, hb.Destination),
                                                                string.Format("WGT {0} LBS : Pcs {1}", hb.Weight, hb.TotalPieces)) { IsSelectable = true, IsReadonly = true };
                    var headerData = new ExpenListItemData(0, Loc.Location, "Housebills: " + Loc.Housebills.Length) { IsReadonly = true, IsSelectable = true };
                    var LocationItem = new ExpandListItem(headerData, Resources.Icons.Door);
                    LocationItem.AddExpandItemsRange(subItems);
                    LocationItem.SubItemSelected += new EventHandler<ExpandItemEventArgs>(LocationItem_SubItemSelected);
                    smoothListBoxBase1.AddItem2(LocationItem);
                }
                smoothListBoxBase1.LayoutItems();
                smoothListBoxBase1.RefreshScroll();
            }
            else // list mode
            {

                this.MultiSelectListEnabled = false;
                this.IsSelectable = true;
                this.UnselectListEnabled = true;
                foreach (var item in housebills)
                {
                    Image icon = item.ScanMode == ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode;
                    TwoLineListItem<HouseBillItem> li = new TwoLineListItem<HouseBillItem>(string.Format("{0}-{1}-{2}", item.Origin, item.HousebillNumber, item.Destination),
                        icon, item.HousebillId, string.Format("WGT {0} LBS : Pcs {1}", item.Weight, item.TotalPieces), item);
                    smoothListBoxBase1.AddItem2(li);
                }
                smoothListBoxBase1.LayoutItems();
                smoothListBoxBase1.RefreshScroll();
            }
            Cursor.Current = Cursors.Default;
        }

        void LocationItem_SubItemSelected(object sender, ExpandItemEventArgs e)
        {
            if (!e.IsSelected)
                return;

            var hb = housebills.FirstOrDefault(h => h.HousebillId == e.ItemID);
            if (hb != null)
                SelectedHousebill = hb;
            DialogResult = DialogResult.OK;
        }
        void HawbLocationList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is TwoLineListItem<HouseBillItem>)
                SelectedHousebill = (listItem as TwoLineListItem<HouseBillItem>).ItemData;
            DialogResult = DialogResult.OK;
        }
    }
    public class LocationHousebills
    {
        public string Location { get; set; }
        public HouseBillItem[] Housebills { get; set; }
    }
}
