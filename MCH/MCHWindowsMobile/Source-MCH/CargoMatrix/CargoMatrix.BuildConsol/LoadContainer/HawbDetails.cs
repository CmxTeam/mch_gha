﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
namespace CargoMatrix.LoadContainer
{
    public partial class HawbDetails : UserControl
    {
        internal IHouseBillItem Hawb { get; private set; }

        public HawbDetails()
        {
            InitializeComponent();
        }
        private void InitializeImages()
        {
            this.buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            this.buttonBrowse.Image = global::Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.buttonDamage.Image = CargoMatrix.Resources.Skin.damage;
            this.buttonDamage.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.buttonLeft.Image = global::Resources.Graphics.Skin.gen_btn_left_over;
            this.buttonLeft.PressedImage = global::Resources.Graphics.Skin.gen_btn_left;
            this.buttonRight.Image = global::Resources.Graphics.Skin.gen_btn_right_over;
            this.buttonRight.PressedImage = global::Resources.Graphics.Skin.gen_btn_right;
            this.buttonHeader.Image = global::Resources.Graphics.Skin.gen_dropdown_oposite_over;
        }

        void buttonHeader_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.buttonHeader.Image = global::Resources.Graphics.Skin.gen_dropdown_oposite;
        }
        internal void DisplayHousebill(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            if (hawb != null && hawb.HousebillId != 0)
            {
                this.Hawb = hawb;
                if (panelHousebillInfo.Visible == false)
                {
                    panelHousebillInfo.Visible = true;
                }
                title.Text = Hawb.Reference();

                labelPieces.Text = string.Format("{0} of {1}", Hawb.ScannedPieces, Hawb.TotalPieces);
                // Set Label Color;
                if (Hawb.ScannedPieces > 0 && Hawb.ScannedPieces < Hawb.TotalPieces)
                    labelPieces.ForeColor = Color.Red;
                else labelPieces.ForeColor = Color.Black;

                labelWeight.Text = Hawb.Weight + " LBS";
                labelSlac.Text = Hawb.Slac.ToString();

                panelIndicators.Flags = Hawb.Flags;
                panelIndicators.PopupHeader = title.Text;
                switch (Hawb.status)
                {
                    case CargoMatrix.Communication.DTO.HouseBillStatuses.Open:
                    case CargoMatrix.Communication.DTO.HouseBillStatuses.Pending:
                        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case CargoMatrix.Communication.DTO.HouseBillStatuses.InProgress:
                        itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case CargoMatrix.Communication.DTO.HouseBillStatuses.Completed:
                        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;
                }

            }
            else
            {
                this.Hawb = null;
                panelHousebillInfo.Visible = false;
            }
        }
    }
}
