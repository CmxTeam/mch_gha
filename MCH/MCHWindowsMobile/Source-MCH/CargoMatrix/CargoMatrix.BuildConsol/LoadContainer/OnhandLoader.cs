﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CustomListItems;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using CargoMatrix.UI;
using CMXBarcode;
using CargoMatrix.LoadConsol;

namespace CargoMatrix.LoadContainer
{
    public partial class OnhandLoader : CargoUtilities.SmoothListBoxOptions
    {
        private CustomListItems.HeaderItem headerItem;
        private string filter = HawbFilter.NOT_SCANNED;
        private string sort = HawbSort.WEIGHT;
        private LocationItem currentLoc = new LocationItem();
        private LocationItem[] locations;
        private HawbDetails hawbDetails;
        private bool m_bLocationsList = false;
        private bool locationListLoaded = false;
        private SmoothListbox.SmoothListBoxBase smoothListLocations = new SmoothListbox.SmoothListBoxBase();
        private string lastScan;
        private string progress;
        private CustomListItems.OptionsListITem filterOption;

        internal string LastScan
        {
            get
            {
                return "Last Scan: " + lastScan;
            }
            set { lastScan = value; pictureBoxBottomPane.Refresh(); }//headerItem.Label3 = LastScan; }
        }
        internal string Progress
        {
            get { return progress; }
            set { progress = value; pictureBoxBottomPane.Refresh(); }
        }

        private LocationItem CurrentLocation
        {
            get { return currentLoc; }
            set
            {
                currentLoc = value;
                if (hawbDetails != null) hawbDetails.buttonHeader.Refresh();
            }
        }
        private void SetCurrentLocation(string loc)
        {
            var cLoc = locations.FirstOrDefault(l => string.Equals(loc, l.Location, StringComparison.OrdinalIgnoreCase));
            if (cLoc != null)
                CurrentLocation = cLoc;
            else
                CurrentLocation = new LocationItem() { Location = loc };

        }

        private void SetCurrentLocation(string[] locs)
        {
            /// if list is empty or contains current location don't do anything.
            if (locs == null || locs.Contains(CurrentLocation.Location))
                return;
            else
            {
                string lastLoc = locs.Length > 0 ? locs[0] : string.Empty;
                var cLoc = locations.FirstOrDefault(l => string.Equals(lastLoc, l.Location, StringComparison.OrdinalIgnoreCase));
                if (cLoc != null)
                    CurrentLocation = cLoc;
                else
                    CurrentLocation = new LocationItem() { Location = lastLoc };
            }
        }
        public OnhandLoader()
        {
            InitializeComponent();



            this.panelHeader2.Controls.Add(hawbDetails);
            headerItem = new CustomListItems.HeaderItem();
            headerItem.Label1 = ForkLiftViewer.Instance.Mawb.Reference();
            headerItem.Label2 = ForkLiftViewer.Instance.Mawb.Line2();


            headerItem.Label3 = string.Format(LoadConsolResources.Text_HawbListBlink, ForkLiftViewer.Instance.Mode.ToString().ToUpper());
            headerItem.ButtonImage = CargoMatrix.Resources.Skin.Forklift_btn; //CargoMatrix.Resources.Skin.buffer;
            headerItem.ButtonPressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over; //CargoMatrix.Resources.Skin.buffer_over;
            this.headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            this.panelHeader2.Controls.Add(headerItem);

            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(LoadHawbs_MenuItemClicked);

            //if (ForkLiftViewer.Instance.ScanHawbToULD)
            //    this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(LoadHawbs_BarcodeReadNotify2);
            //else
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(LoadHawbs_BarcodeReadNotify);
            this.Scrollable = false;
            headerItem.Logo = ForkLiftViewer.Instance.CarrierLogo;
            ForkLiftViewer.Instance.ItemCountChanged += new EventHandler(Forklift_ItemCountChanged);

            hawbDetails = new HawbDetails() { Location = new Point(0, headerItem.Height) };
            hawbDetails.buttonBrowse.Click += HawbItem_BrowseClick;
            hawbDetails.buttonDamage.Click += HawbItem_DamageClick;
            hawbDetails.buttonDetails.Click += HawbItem_ViewerClick;
            hawbDetails.buttonLeft.Click += buttonLeft_Click;
            hawbDetails.buttonRight.Click += buttonRight_Click;
            hawbDetails.buttonHeader.Paint += buttonHeader_Paint;
            hawbDetails.buttonHeader.Click += buttonHeader_Click;
            this.panelHeader2.Controls.Add(hawbDetails);

            if (ForkLiftViewer.Instance.DoubleScanEnabled)
            {
                buttonDrop.Visible = true;
            }
        }
        private void Forklift_ItemCountChanged(object sender, EventArgs e)
        {
            headerItem.Counter = ForkLiftViewer.Instance.ItemsCount;
        }


        private void LoadHawbs_BarcodeReadNotify(string barcodeData)
        {
            int forkliftPieces = ForkLiftViewer.Instance.ItemsCount;

            var scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            /// get scan Ids from backend 
            var scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(ForkLiftViewer.getScanItem(scanObj), ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.Mawb.Mot);
            if (scanItem.Transaction.TransactionStatus1 == false)
            {
                scanObj.BarcodeType = CMXBarcode.BarcodeTypes.NA;
            }

            #region PULL MODE
            if (ForkLiftViewer.Instance.Mode == ConsolMode.Pull)
            {
                switch (scanObj.BarcodeType)
                {
                    /// Drop to Location
                    case CMXBarcode.BarcodeTypes.Door:
                    case CMXBarcode.BarcodeTypes.Area:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);

                        TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DropAllPiecesIntoLocation(ForkLiftViewer.Instance.Mawb.TaskId, scanItem.LocationId, ForkLiftViewer.Instance.Mawb.Mot);
                        if (status.TransactionStatus1)
                        {
                            //CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullDrop, forkliftPieces, scanItem.Location), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            ForkLiftViewer.Instance.ItemsCount = status.TransactionRecords;
                            LoadControl();
                        }
                        else
                            ShowFailMessage(status.TransactionError);
                        break;

                    case CMXBarcode.BarcodeTypes.Uld:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchMode, scanItem.BarcodeType.ToString()), LoadConsolResources.Text_SwitchModeTitle, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                            /// show ULD BUilder
                            if (scanItem.UldId <= 0)
                            {
                                DisplayUldBuilder(string.Format(LoadConsolResources.Text_ULDNotAttached, scanItem.UldNumber));
                                return;

                            }
                            else
                            {
                                LoadPiecesIntoULD(scanItem.UldId, scanItem.UldNumber);
                            }
                        }
                        break;

                    case CMXBarcode.BarcodeTypes.User:
                    case CMXBarcode.BarcodeTypes.NA:
                    case CMXBarcode.BarcodeTypes.MasterBill:
                    case CMXBarcode.BarcodeTypes.HouseBill:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        ShowFailMessage(LoadConsolResources.Text_InvalidBarcode);
                        break;
                    default:
                        break;

                }

            }
            #endregion

            #region LOAD MODE
            else
            {
                switch (scanObj.BarcodeType)
                {
                    case CMXBarcode.BarcodeTypes.Uld:
                        if (scanItem.UldId == 0)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_ULDNotAttached, scanItem.UldNumber), "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                        }
                        else
                        {
                            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                            LoadPiecesIntoULD(scanItem.UldId, scanItem.UldNumber);
                        }
                        break;

                    case CMXBarcode.BarcodeTypes.Truck:
                    case CMXBarcode.BarcodeTypes.Door:
                    case CMXBarcode.BarcodeTypes.Area:
                    case CMXBarcode.BarcodeTypes.User:
                    case CMXBarcode.BarcodeTypes.NA:
                    case CMXBarcode.BarcodeTypes.MasterBill:
                    case CMXBarcode.BarcodeTypes.HouseBill:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        ShowFailMessage(LoadConsolResources.Text_InvalidBarcode);
                        break;
                    default:
                        break;
                }
            }
            #endregion

            /// HAWB Scanned
            if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.OnHand)
            {
                /// HAWB from consol
                if (scanItem.MasterBillId == ForkLiftViewer.Instance.Mawb.MasterBillId)
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    addHawbToForkLift(scanItem);
                }
                /// HAWB from other Consol
                else
                {
                    string msg = string.Format("Onhand {0} doesn't belong to the booking.", scanItem.HouseBillNumber);
                    CargoMatrix.UI.CMXMessageBox.Show(msg, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                }
            }
            //if (!BarcodeEnabled)
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private void LoadPiecesIntoULD(int uldId, string uldNo)
        {
            TransactionStatus loadStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoULD(ForkLiftViewer.Instance.Mawb.TaskId, uldId, ForkLiftViewer.Instance.Mawb.Mot);
            if (loadStatus.TransactionStatus1)
            {
                ForkLiftViewer.Instance.ItemsCount = loadStatus.TransactionRecords;
                LoadControl();
            }
            else
                ShowFailMessage(loadStatus.TransactionError);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg">shows a messagebos if message is provided</param>
        private void DisplayUldBuilder(string message)
        {
            if (!string.IsNullOrEmpty(message))
                CargoMatrix.UI.CMXMessageBox.Show(message, "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
            if (CMXAnimationmanager.GetPreviousControl() is ContainerEditor)
                CMXAnimationmanager.GoBack();
            else
            {
                Cursor.Current = Cursors.WaitCursor;
                ContainerEditor uldbuilder = new ContainerEditor();
                uldbuilder.Location = new Point(Left, Top);
                uldbuilder.Size = new Size(Width, Height);
                this.smoothListBoxMainList.Reset();
                CargoMatrix.UI.CMXAnimationmanager.GoBack(uldbuilder as CargoMatrix.UI.CMXUserControl);
            }
        }

        #region add Hawb into F/L
        private void addHawbToForkLift(ScanItem scanItem)
        {
            switch (scanItem.ScanMode)
            {
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Count:
                    AddToForkliftInCountMode(scanItem);
                    break;
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Slac:
                    AddToForkliftInSlacMode(scanItem);
                    break;
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece:
                default:
                    AddToForkliftInPieceMode(scanItem);
                    break;
            }
        }
        private void AddToForkliftInSlacMode(ScanItem hawb)
        {
            Control[] actions = new Control[]
                {   
                    new SmoothListbox.ListItems.StandardListItem("PIECE ID",null,1),
                    new SmoothListbox.ListItems.StandardListItem("COUNT",null,2),
                };
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.AddItems(actions);
            actPopup.HeaderText2 = string.Format("Total Pieces: {0} SLAC: {1}", hawb.TotalPieces, "N/A");
            actPopup.HeaderText = hawb.MasterBillNumber;
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1:
                        AddToForkliftInPieceMode(hawb);
                        break;
                    case 2:
                        AddToForkliftInCountMode(hawb);
                        break;
                    default:
                        break;
                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();
        }
        private void AddToForkliftInPieceMode(ScanItem hawb)
        {
            Cursor.Current = Cursors.WaitCursor;
            var response = CargoMatrix.Communication.LoadConsol.Instance.ScanHousebillIntoForkliftPiece(hawb.HouseBillId, ForkLiftViewer.Instance.Mawb.MasterBillId, hawb.PieceId, ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, ScanTypes.Automatic, filter, sort, CurrentLocation.Location, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
            if (!response.Status.TransactionStatus1)
                ShowFailMessage(response.Status.TransactionError);
            else
            {
                lastScan = hawb.HouseBillNumber;
                //PopulateHousebillDetails(response); #warning needs to be refactored
                LoadControl();
            }
            Cursor.Current = Cursors.Default;
        }
        private void AddToForkliftInCountMode(ScanItem hawb)
        {
            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.PieceCount = hawb.TotalPieces - hawb.ScannedPieces;
            if (CntMsg.PieceCount == 0)
            {
                string msg = string.Format(LoadConsolResources.Text_HawbAlreadyScanned, hawb.HouseBillNumber);
                ShowFailMessage(msg);
                return;
            }

            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces to load";
            CntMsg.LabelReference = hawb.HouseBillNumber;

            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                var response = CargoMatrix.Communication.LoadConsol.Instance.ScanHousebillIntoForkliftCount(hawb.HouseBillId, ForkLiftViewer.Instance.Mawb.MasterBillId, CntMsg.PieceCount, ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, ScanTypes.Automatic, filter, sort, CurrentLocation.Location, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
                if (!response.Status.TransactionStatus1)
                    ShowFailMessage(response.Status.TransactionError);
                else
                {
                    lastScan = hawb.HouseBillNumber;
                    //PopulateHousebillDetails(response); #warning needs to be refactored
                    LoadControl();
                }
            }
        }
        #endregion

        private static void ShowFailMessage(string message)
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.UI.CMXMessageBox.Show(message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            ShowNavButtons(true);
            this.buttonUp.BringToFront();
            this.buttonDown.BringToFront();
            this.TitleText = string.Format("Load Container - {0} - ({1}/{2})", ForkLiftViewer.Instance.Mode.ToString(), filter, sort);


            locations = CargoMatrix.Communication.LoadConsol.Instance.GetMasterbillLocations(ForkLiftViewer.Instance.Mawb.MasterBillId, filter, ForkLiftViewer.Instance.Mode == ConsolMode.Load, MOT.Ocean);
            if (locations.Length > 0)
            {
                // if the previous location is in the list find it and keep as a new currentLocation
                CurrentLocation = locations.FirstOrDefault<LocationItem>(l => l.Location == CurrentLocation.Location);

                // if the previous location not in the list take the first one as currentLocation
                if (null == CurrentLocation)
                    CurrentLocation = locations[0];
            }
            else
                CurrentLocation = new LocationItem();


            BarcodeEnabled = false;
            BarcodeEnabled = true;
            DisplayHawb(ListDirection.Current);
            Cursor.Current = Cursors.Default;
        }

        private void ShowNavButtons(bool flag)
        {
            this.buttonDown.Visible = flag;
            this.buttonUp.Visible = flag;
            this.pictureBoxDown.Visible = !flag;
            this.pictureBoxUp.Visible = !flag;
        }

        private void headerItem_ButtonClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            ForkLiftViewer.Instance.ShowDialog();
            BarcodeEnabled = true;
            LoadControl();
        }

        private void HawbItem_DamageClick(object sender, EventArgs e)
        {
            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(hawbDetails.Hawb);

            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionSummary(args.HouseBillID, ForkLiftViewer.Instance.Mawb.Mot);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.LoadConsol.Instance.UpdateShippmentCondition(args.HouseBillId, args.ScanModes, args.ShipmentConditions, ForkLiftViewer.Instance.Mawb.TaskId, string.Empty, ForkLiftViewer.Instance.Mawb.Mot);
            };

            damageCapture.GetHouseBillPiecesIDs = () =>
            {
                return
                    (
                        from item in CargoMatrix.Communication.LoadConsol.Instance.GetHousebillInfo(hawbDetails.Hawb.HousebillId, ForkLiftViewer.Instance.Mawb.TaskId).Pieces
                        select item.PieceId
                    ).ToArray();
            };

            damageCapture.Size = this.Size;
            BarcodeEnabled = false;
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(damageCapture);

        }
        private void HawbItem_BrowseClick(object sender, EventArgs e)
        {
            Control[] actions = new Control[]
            {
                new SmoothListbox.ListItems.StandardListItem("SHOW LOCATIONS",null,5),
                new SmoothListbox.ListItems.StandardListItem("PRINT LABELS",null,4),
            };



            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.OneTouchSelection = true;
            actPopup.MultiSelectListEnabled = false;
            actPopup.HeaderText = hawbDetails.Hawb.Reference();
            actPopup.HeaderText2 = "Select action to continue";
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 4:
                        PrintLabels(hawbDetails.Hawb);
                        break;
                    case 5:
                        ShowLocations(hawbDetails.Hawb);
                        break;
                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();
        }
        private void HawbItem_ViewerClick(object sender, EventArgs e)
        {

        }
        private void ShowLocations(IHouseBillItem hawb)
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxMainList.Reset();
            BarcodeEnabled = false;
            HawbViewer hawbViewer = new HawbViewer(hawb, filter, sort, CurrentLocation.Location);//
            LoadControl();
        }

        private void LoadHawbs_LoadOptionsMenu(object sender, System.EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.SHOW_LOCATIONS) { Title = "View Shipment List" });
            //this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.VIEW_HAWB_LIST) { Title = "Locked Housebills" });

            this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.PRINT_MAWB_LABEL));

            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.SHARE_TASK) { Enabled = ForkLiftViewer.Instance.Mawb.AllowShare });
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL) { Enabled = ForkLiftViewer.Instance.Mode == ConsolMode.Load });
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }



        private void LoadHawbs_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            if (listItem is CustomListItems.OptionsListITem)
            {
                FilterPopup fp;
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {

                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        fp = new FilterPopup(FilteringMode.Hawb);
                        fp.Filter = filter;
                        fp.Sort = sort;
                        if (DialogResult.OK == fp.ShowDialog())
                        {
                            sort = fp.Sort;
                            filter = fp.Filter;
                            if (filterOption != null)
                                filterOption.DescriptionLine = filter + " / " + sort;
                            Cursor.Current = Cursors.WaitCursor;
                            LoadControl();
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case OptionsListITem.OptionItemID.PRINT_MAWB_LABEL:
                        CustomUtilities.MawbLabelPrinter.Show(ForkLiftViewer.Instance.Mawb);
                        if (!BarcodeEnabled)
                        {
                            BarcodeEnabled = true;
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.SHARE_TASK:
                        ShareTask.ShowDialog(ForkLiftViewer.Instance.Mawb);
                        if (!BarcodeEnabled)
                        {
                            BarcodeEnabled = true;
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL:
                        bool finalizeWindowConfirmed = SharedMethods.FinalizeAndTakePictures();
                        // Reenable the Barcode if Cancel has been clicked.
                        // Otherwise don't reenable as an ObjectDisposedException will pop.
                        if (!finalizeWindowConfirmed)
                        {
                            if (!BarcodeEnabled)
                            {
                                BarcodeEnabled = true;
                            }
                        }
                        break;
                    case OptionsListITem.OptionItemID.SHOW_LOCATIONS:
                        HawbLocationList locViewer = new HawbLocationList(filter, sort);
                        locViewer.ShowDialog();
                        if (locViewer.SelectedHousebill != null)
                        {
                            SetCurrentLocation(locViewer.SelectedHousebill.LastLocation);
                            hawbDetails.DisplayHousebill(new CargoMatrix.Communication.DTO.HouseBillItem(locViewer.SelectedHousebill));
                        }
                        if (!BarcodeEnabled)
                        {
                            BarcodeEnabled = true;
                        }
                        break;
                    case OptionsListITem.OptionItemID.VIEW_HAWB_LIST:
                        new LockedHawbList().ShowDialog();
                        break;
                    default:
                        break;
                }
                ShowNavButtons(true);
            }
        }

        internal void PrintLabels(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            var defPrinter = CargoMatrix.Communication.ScannerUtility.Instance.GetDefaultPrinter(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD);

            if (CustomUtilities.PrinterUtilities.PopulateAvailablePrinters(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD, ref defPrinter))
            {
                var status = Communication.OnHand.OnHand.Instance.PrintOnhandLabel(defPrinter.PrinterName, hawb.HousebillId);
                if (false == status.TransactionStatus1)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CMXMessageBoxIcon.Hand);
                else
                {
                    string msg = string.Concat("Printing has been scheduled on ", defPrinter.PrinterName);
                    CargoMatrix.UI.CMXMessageBox.Show(msg, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                }
            }
        }

        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            if (m_bLocationsList)
                ShowLocationList(false);
            base.pictureBoxMenu_Click(sender, e);
            if (m_bOptionsMenu)
            {
                ShowNavButtons(false);
            }
            else
            {
                ShowNavButtons(true);
            }
        }
        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            if (ForkLiftViewer.Instance.Mode == ConsolMode.Load && !(CMXAnimationmanager.GetPreviousControl() is ContainerEditor))
                DisplayUldBuilder(string.Empty);
            else
                base.pictureBoxBack_Click(sender, e);

            if (m_bOptionsMenu)
            {
                ShowNavButtons(false);
            }
            else
            {
                ShowNavButtons(true);
            }
        }



        #region Navigation
        protected override void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxUp_MouseUp(sender, e);

            else
            {
                if (m_bLocationsList)
                    smoothListLocations.UpButtonPressed = false;

                if (pictureBoxUp.Enabled)
                    UpButtonPressed(false);

            }
        }
        protected override void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxUp_MouseDown(sender, e);

            else
            {
                if (m_bLocationsList)
                    smoothListLocations.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);

                if (pictureBoxDown.Enabled)
                    UpButtonPressed(true);
            }
        }
        protected override void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseDown(sender, e);

            else
            {
                if (m_bLocationsList)
                    smoothListLocations.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);

                if (pictureBoxDown.Enabled)
                    DownButtonPressed(true);
            }
        }
        protected override void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseUp(sender, e);

            else
            {
                if (m_bLocationsList)
                    smoothListLocations.DownButtonPressed = false;
                if (pictureBoxDown.Enabled)
                    DownButtonPressed(false);

            }
        }
        private void smoothListLocations_AutoScroll(SmoothListbox.SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            if (m_bLocationsList == true && m_bOptionsMenu == false)
            {
                switch (direction)
                {
                    case SmoothListbox.SmoothListBoxBase.DIRECTION.UP:
                        pictureBoxUp.Enabled = enable;
                        if (pictureBoxUp.Enabled == false)
                            smoothListLocations.UpButtonPressed = false;
                        break;
                    case SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN:
                        //pictureBoxBookMarkNext.Enabled = enable;
                        pictureBoxDown.Enabled = enable;
                        if (pictureBoxDown.Enabled == false)
                            smoothListLocations.DownButtonPressed = false;
                        break;
                }

            }

        }

        void pictureBoxBottomPane_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            using (Font ft = new Font("Tahoma", 9F, FontStyle.Bold))
            {
                SizeF textScanSize = e.Graphics.MeasureString(LastScan, ft);
                SizeF textProgressSize = e.Graphics.MeasureString(Progress, ft);
                int y1 = (int)(pictureBoxBottomPane.Height - 2 * textScanSize.Height) / 2;
                int y2 = (int)(textScanSize.Height + (2 * y1));
                int x2 = (int)(pictureBoxBottomPane.Width - textProgressSize.Width) / 2;
                e.Graphics.DrawString(LastScan, ft, new SolidBrush(Color.White), 10, y1);
                e.Graphics.DrawString(Progress, ft, new SolidBrush(Color.White), x2, y2);
            }
        }


        private void DisplayHawb(ListDirection dir)
        {
            int currentHawbid = 0;
            if (hawbDetails.Hawb != null)
                currentHawbid = hawbDetails.Hawb.HousebillId;
            var hb = CargoMatrix.Communication.LoadConsol.Instance.GetHousebill(currentHawbid, dir, ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.TaskId, filter, sort, CurrentLocation.Location, ForkLiftViewer.Instance.ForkliftID, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
            if (hb.Status.TransactionStatus1 == false && hb.Status.TransactionError != "No records found.")
            {
                ShowFailMessage(hb.Status.TransactionError);
                if (hb.HouseBill == null && hawbDetails.Hawb != null)
                {
                    string[] locs = new string[0];
                    if ((hb.HouseBill is CargoMatrix.Communication.WSLoadConsol.HouseBillItem))
                    {
                        var pieceLocs = (hb.HouseBill as CargoMatrix.Communication.WSLoadConsol.HouseBillItem).PieceLocations;
                        locs = (from ploc in pieceLocs
                                select ploc.LocationName).ToArray<string>();
                    }

                    SetCurrentLocation(locs);
                }
                else
                    PopulateHousebillDetails(hb);
            }
            else
                PopulateHousebillDetails(hb);
        }

        private void PopulateHousebillDetails(HousebillSummary hb)
        {
            ForkLiftViewer.Instance.ItemsCount = hb.Status.TransactionRecords;

            Progress = string.Format("Progress : {0}%", (int)hb.Status.TaskProgress);
            //if (hb.Status.TaskProgress == 100)
            //{
            //    if (ForkLiftViewer.Instance.FinalizeReady)
            //    {
            //        bool success = ForkLiftViewer.Instance.FinalizeConsol();
            //        if (success)
            //            return;
            //    }
            //}

            string[] locs = new string[0];
            if ((hb.HouseBill is CargoMatrix.Communication.WSLoadConsol.HouseBillItem))
            {
                var pieceLocs = (hb.HouseBill as CargoMatrix.Communication.WSLoadConsol.HouseBillItem).PieceLocations;
                locs = (from ploc in pieceLocs
                        select ploc.LocationName).ToArray<string>();
            }

            SetCurrentLocation(locs);
            if (hb.HouseBill != null)
            {
                foreach (var item in (hb.HouseBill as CargoMatrix.Communication.WSLoadConsol.HouseBillItem).ShipmentAlerts)
                    CargoMatrix.UI.CMXMessageBox.Show(item.AlertMessage + Environment.NewLine + item.AlertSetBy + Environment.NewLine + item.AlertDate.ToString("MM/dd HH:mm"), "Alert: " + hb.HouseBill.HousebillNumber, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
            hawbDetails.DisplayHousebill(hb.HouseBill);
            if (hb.HouseBill.HousebillId>0)
            {
                // TODO ANI
                //headerItem.Label3Visibility = true;
            }
            else
            {
                // TODO ANI
                //headerItem.Label3Visibility = false;
            }

            /// Pull proccess is finished
            if (ForkLiftViewer.Instance.Mode == ConsolMode.Pull && hb.Status.TaskProgress == 100)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_SwitchToLoad, "Switch to Load", CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.OK))
                {
                    ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                    pictureBoxBack_Click(null, EventArgs.Empty);
                }
            }
        }

        void buttonRight_Click(object sender, EventArgs e)
        {
            if (locations.Length > 1)
            {
                int newindex = (currenLocationIndex + 1) % locations.Length;
                CurrentLocation = locations[newindex];
                if (m_bLocationsList)
                    ShowLocationList(false);
                DisplayHawb(ListDirection.Current);
            }
        }

        void buttonLeft_Click(object sender, EventArgs e)
        {
            if (locations.Length > 1)
            {
                int newindex = (currenLocationIndex + locations.Length - 1) % locations.Length;
                CurrentLocation = locations[newindex];
                if (m_bLocationsList)
                    ShowLocationList(false);
                DisplayHawb(ListDirection.Current);
            }
        }
        private int currenLocationIndex
        {
            get
            {
                for (int i = 0; i < locations.Length; i++)
                    if (locations[i] == CurrentLocation)
                        return i;

                return 0;
            }
        }


        void LocationItem_ButtonClick(object sender, EventArgs e)
        {
            BarcodeEnabled = false;
            string loc = (sender as ListItemCheckWithButton<LocationItem>).ItemData.Location;
            //string filter = FilterPopup.GetFilter();
            var locViewer = new HawbLocationList(filter, sort, loc);
            ShowLocationList(false);
            if (locViewer.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                this.Refresh();
                if (locViewer.SelectedHousebill != null)
                {
                    SetCurrentLocation(locViewer.SelectedHousebill.LastLocation);
                    hawbDetails.DisplayHousebill(new CargoMatrix.Communication.DTO.HouseBillItem(locViewer.SelectedHousebill));
                }
            }
            BarcodeEnabled = true;
        }

        void smoothListLocations_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            CurrentLocation = (listItem as ListItemCheckWithButton<LocationItem>).ItemData;
            ShowLocationList(false);
            DisplayHawb(ListDirection.Current);
        }

        void buttonHeader_Click(object sender, EventArgs e)
        {
            ShowLocationList(!m_bLocationsList);
        }
        private void CreateLocationList()
        {
            locationListLoaded = true;
            smoothListLocations.Width = this.Width;
            smoothListLocations.Height = hawbDetails.Height - hawbDetails.buttonHeader.Height + pictureBoxBottomPane.Height;
            smoothListLocations.Location = new Point(0, hawbDetails.Top + hawbDetails.buttonHeader.Height + panelTitle.Height);
            smoothListLocations.Visible = false;
            smoothListLocations.UnselectEnabled = false;
            smoothListLocations.MultiSelectEnabled = false;
            smoothListLocations.ListItemClicked += new SmoothListbox.ListItemClickedHandler(smoothListLocations_ListItemClicked);
            this.Controls.Add(smoothListLocations);
            smoothListLocations.BringToFront();

        }

        private void ShowLocationList(bool show)
        {
            if (show)
            {
                m_bLocationsList = true;
                if (locationListLoaded == false)
                    CreateLocationList();
                smoothListLocations.RemoveAll();

                locations = CargoMatrix.Communication.LoadConsol.Instance.GetMasterbillLocations(ForkLiftViewer.Instance.Mawb.MasterBillId, filter, ForkLiftViewer.Instance.Mode == ConsolMode.Load, MOT.Ocean);
                foreach (var item in locations.Where<LocationItem>(l => !string.IsNullOrEmpty(l.Location)))
                {
                    var cntrl = new ListItemCheckWithButton<LocationItem>(item, item.Location, string.Format("Pieces: {0} WGT: {1} LBS", item.TotalPieces, item.TotalWeight.ToString("0.##")));
                    cntrl.OnButtonClick += new EventHandler(LocationItem_ButtonClick);
                    smoothListLocations.AddItem(cntrl);
                }
                selectCurrentLocation();
                smoothListLocations.Visible = true;
                this.hawbDetails.buttonHeader.Image = global::Resources.Graphics.Skin.gen_dropdown_oposite;
                smoothListBoxMainList.AutoScroll -= smoothListBoxMainList_AutoScroll;
                smoothListLocations.AutoScroll += new SmoothListbox.AutoScrollHandler(smoothListLocations_AutoScroll);
                ShowNavButtons(false);
                smoothListLocations.RefreshScroll();
            }
            else
            {
                m_bLocationsList = false;
                smoothListLocations.Visible = false;
                smoothListBoxMainList.AutoScroll += smoothListBoxMainList_AutoScroll;
                smoothListLocations.AutoScroll -= new SmoothListbox.AutoScrollHandler(smoothListLocations_AutoScroll);
                this.hawbDetails.buttonHeader.Image = global::Resources.Graphics.Skin.gen_dropdown_oposite_over;
                ShowNavButtons(true);
            }
        }

        private void selectCurrentLocation()
        {
            smoothListLocations.SelectNone();
            foreach (ListItemCheckWithButton<LocationItem> item in smoothListLocations.Items)
            {
                if (CurrentLocation == item.ItemData)
                {
                    item.SelectedChanged(true);
                    break;
                }
            }
        }

        void buttonHeader_Paint(object sender, PaintEventArgs e)
        {
            if (CurrentLocation == null || string.IsNullOrEmpty(CurrentLocation.Location))
                return;
            using (Font ft = new Font("Tahoma", 9F, FontStyle.Bold))
            {
                using (Brush b = new SolidBrush(Color.White))
                {
                    string text = string.Format("{0} ({1})", CurrentLocation.Location, CurrentLocation.TotalPieces);
                    SizeF textSize = e.Graphics.MeasureString(text, ft);
                    int x = (hawbDetails.buttonHeader.Width - (int)textSize.Width) / 2;
                    int y = (hawbDetails.buttonHeader.Height - (int)textSize.Height) / 2;
                    e.Graphics.DrawString(text, ft, b, x, y);
                }
            }
        }

        void buttonDown_Click(object sender, System.EventArgs e)
        {
            DisplayHawb(ListDirection.NextItem);
        }

        void buttonUp_Click(object sender, System.EventArgs e)
        {
            DisplayHawb(ListDirection.PreviousItem);
        }
        void buttonDrop_Click(object sender, System.EventArgs e)
        {
            ForkLiftViewer.Instance.ShowDialog();
            LoadControl();
        }
        #endregion

        #region Switch Mode

        private bool SwitchMode(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            bool retValue = false;
            /// piece Mode -> Count
            if (hawb.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
            {
                string msg = string.Format(LoadConsolResources.Text_ChangeModeToCount, hawb.Reference());
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, ForkLiftViewer.Instance.Mawb.TaskId, CargoMatrix.Communication.DTO.ScanModes.Count);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_ChangeCountConfirm, hawb.Reference()), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            // Count Mode -> Piece
            else
            {
                string msg = string.Format(LoadConsolResources.Text_ChangeModeToPiece, hawb.Reference());
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (!ChangeModeAllowed(hawb))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchNotAllowed, hawb.Reference()), "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        retValue = false;
                    }
                    else
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, ForkLiftViewer.Instance.Mawb.TaskId, CargoMatrix.Communication.DTO.ScanModes.Piece);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_ChangePieceConfirm, hawb.Reference()), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            return retValue;
        }

        private bool ChangeModeAllowed(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            if (ForkLiftViewer.Instance.ChangeHawbScanModeAllowed ||
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin ||
                DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                return true;
            else
                return false;
        }

        #endregion
    }
}
