﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSLoadConsol;
using CMXExtensions;
using SmoothListbox.ListItems;
using CMXBarcode;
using CargoMatrix.LoadConsol;

namespace CargoMatrix.LoadContainer
{
    public partial class HawbViewer : CargoMatrix.Utilities.MessageListBox
    {
        //private CustomListItems.HeaderItem headerItem;
        private CargoMatrix.Communication.DTO.IHouseBillItem hawbItem;
        private string filter;
        private string sort;
        private string location;
        public HawbViewer(CargoMatrix.Communication.DTO.IHouseBillItem hawb, string filter, string sort, string location)
        {
            InitializeComponent();
            this.hawbItem = hawb;
            this.filter = filter;
            this.sort = sort;
            this.location = location;
            this.HeaderText = hawb.Reference();
            this.HeaderText2 = LoadConsolResources.Text_HawbViewTitle;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(HawbViewer_ListItemClicked);
            {
                LoadControl();
                this.ShowDialog();
            }
        }

        void HawbViewer_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem>)
            {
                TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem> lItem = (listItem as TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem>);
                CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
                cntMsg.PieceCount = lItem.ItemData.Piece.Length;
                cntMsg.HeaderText = "Confirm count";
                cntMsg.LabelDescription = "Enter number of pieces";
                cntMsg.LabelReference = lItem.ItemData.Location;
                if (DialogResult.OK == cntMsg.ShowDialog())
                {
                    var response = CargoMatrix.Communication.LoadConsol.Instance.ScanHousebillIntoForkliftCount(hawbItem.HousebillId,
                        ForkLiftViewer.Instance.Mawb.MasterBillId, cntMsg.PieceCount, ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, ScanTypes.Manual, string.Empty, string.Empty, string.Empty, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
                    if (!response.Status.TransactionStatus1)
                        CargoMatrix.UI.CMXMessageBox.Show(response.Status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    else
                        ForkLiftViewer.Instance.ItemsCount = response.Status.TransactionRecords;

                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    sender.Reset();
                    OkEnabled = false;
                }
            }
        }
        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            List<PieceItem> pieceItems = new List<PieceItem>();
            foreach (var item in smoothListBoxBase1.Items.OfType<CustomListItems.ComboBox>())
            {
                foreach (var pieceId in item.SelectedIds)
                    pieceItems.Add(new PieceItem() { HouseBillId = item.ID, PieceId = pieceId });
            }

            var response = CargoMatrix.Communication.LoadConsol.Instance.ScanPiecesIntoForklift(pieceItems.ToArray<PieceItem>(),
            ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, filter, sort, location, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
            if (!response.Status.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(response.Status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            else
                ForkLiftViewer.Instance.ItemsCount = response.Status.TransactionRecords;

            this.DialogResult = DialogResult.OK;
            Cursor.Current = Cursors.Default;
        }

        public void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;

            smoothListBoxBase1.RemoveAll();
            if (hawbItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)//? CargoMatrix.Resources.Skin.countMode : CargoMatrix.Resources.Skin.PieceMode)
            {
                foreach (var loc in CargoMatrix.Communication.ScannerUtility.Instance.GetPiecesByLocation(hawbItem.HousebillId,ForkLiftViewer.Instance.Mawb.MasterBillId, CargoMatrix.Communication.ScannerUtilityWS.MOT.Ocean))
                {

                    List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();
                    foreach (var pc in loc.Piece)

                        items.Add(new CustomListItems.ComboBoxItemData("Piece " + pc.PieceNumber, pc.Location, pc.PieceId, true));

                    CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawbItem.HousebillId, loc.Location, string.Format("Pieces: {0} of {1}", loc.Piece.Length, hawbItem.TotalPieces), CargoMatrix.Resources.Skin.PieceMode, items);
                    combo.IsSelectable = true;
                    combo.ReadOnly = true;
                    combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                    smoothListBoxBase1.AddItem(combo);
                }
            }
            else
            {
                foreach (var loc in CargoMatrix.Communication.ScannerUtility.Instance.GetPiecesByLocation(hawbItem.HousebillId,ForkLiftViewer.Instance.Mawb.MasterBillId, CargoMatrix.Communication.ScannerUtilityWS.MOT.Ocean))
                {
                    TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem> item = new TwoLineListItem<CargoMatrix.Communication.ScannerUtilityWS.LocationItem>(loc.Location, CargoMatrix.Resources.Skin.countMode, loc.LocationId, string.Format("Pieces: {0} of {1}", loc.Piece.Length, hawbItem.TotalPieces), loc);
                    smoothListBoxBase1.AddItem(item);
                }

            }
            Cursor.Current = Cursors.Default;
        }

        void combo_SelectionChanged(object sender, EventArgs e)
        {
            if ((sender as CustomListItems.ComboBox).SubItemSelected)
                OkEnabled = true;
            else
                OkEnabled = false;
        }
    }
}
