﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.LoadContainer
{
    public partial class ULDListItem<T> : SmoothListbox.ListItems.ListItem<T>
    {
        public event EventHandler ButtonEditClick;
        public event EventHandler ButtonDeleteClick;
        public Image Logo { get { return pictureBox1.Image; } set { pictureBox1.Image = value; } }
        public string Line1 { get { return label1.Text; } set { label1.Text = value; } }
        public string Line2 { get { return label2.Text; } set { label2.Text = value; } }

        public ULDListItem(string line1, string line2, T uld)
            : this(line1, line2, CargoMatrix.Resources.Skin.Freight_Car, uld)
        {
        }
        public ULDListItem(string line1, string line2, Image logo, T uld)
            : base(uld)
        {
            InitializeComponent();
            label1.Text = line1;
            label2.Text = line2;
            ItemData = uld;
            Logo = logo;
        }
        public override void OnDataItemChanged()
        {
            FormatLine1();
        }

        private void FormatLine1()
        {
            if (ItemData is IULD)
            {
                var uld = ItemData as IULD;
                if (uld.ULDNo != string.Empty)
                {
                    if (uld.ULDType.Length > 8 || uld.ULDNo.Length > 8 || uld.ULDType.Length + uld.ULDNo.Length > 16)
                    {
                        label1.Text = uld.ULDType + " - " + Environment.NewLine + uld.ULDNo;
                        label1.Height = 26;
                        label1.Top = 2;
                        label2.Top = 28;
                    }
                    else
                    {
                        label1.Text = uld.ULDType + " - " + uld.ULDNo;
                        label1.Height = 16;
                        label1.Top = 7;
                        label2.Top = 23;
                    }
                }
                else
                {
                    label1.Text = uld.ULDType;
                    label1.Height = 16;
                    label1.Top = 7;
                    label2.Top = 23;
                }
                AutoScaleDimensions = new SizeF(96F, 96F);
                AutoScaleMode = AutoScaleMode.Dpi;
            }
        }
        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (ButtonEditClick != null)
                ButtonEditClick(this, e);
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ButtonDeleteClick != null)
                ButtonDeleteClick(this, e);
        }
    }
}
