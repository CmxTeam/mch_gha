﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CustomListItems;
using CustomUtilities;
using CargoMatrix.Communication.WSLoadConsol;
using CMXExtensions;

namespace CargoMatrix.LoadConsol
{
    class ULDEditor
    {
        private static ULDEditor instance;

        internal bool AddULDItem(bool showLoose)
        {
            if (ForkLiftViewer.Instance.AllowULDEdit == false && !CargoMatrix.Communication.Utilities.IsAdmin)
                if (DialogResult.OK != CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                    return false;

            AddULDPopup addUldPopup = new AddULDPopup(null);
            addUldPopup.ShowLoose = showLoose;
            if (DialogResult.OK == addUldPopup.ShowDialog())
            {
                TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.AssignULDToMawb(ForkLiftViewer.Instance.Mawb.MasterBillId, addUldPopup.ContainerType.TypeID, addUldPopup.ContainerNumber, false, MOT.Air, 0);
                if (status.TransactionStatus1 == false && status.NeedOverride == true)
                {
                    status = CargoMatrix.Communication.LoadConsol.Instance.AssignULDToMawb(ForkLiftViewer.Instance.Mawb.MasterBillId, addUldPopup.ContainerType.TypeID, addUldPopup.ContainerNumber, true, MOT.Air, 0);
                }

                if (status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return true;
            }
            return false;
        }

        internal void EditUldItem(ULDListItem_new Ulditem, bool showLoose)
        {
            AddULDPopup addUldPopup = new AddULDPopup(Ulditem.ItemData);
            addUldPopup.ShowLoose = showLoose;
            addUldPopup.Title = ForkLiftViewer.Instance.Mawb.Reference();
            if (DialogResult.OK == addUldPopup.ShowDialog())
            {

                CargoMatrix.Communication.LoadConsol.Instance.UpdateULD(Ulditem.ItemData.ID, addUldPopup.ContainerType.TypeID, addUldPopup.ContainerNumber, MOT.Air, null);
            }

        }
        internal bool DeleteULDItem(ULDListItem_new Ulditem)
        {
            if (CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_DeleteUld, "Confirm Delete", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
            {
                if (ForkLiftViewer.Instance.AllowULDEdit == false && !Communication.Utilities.IsAdmin)
                    if (DialogResult.OK != CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                        return false;

                TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DeleteULD(Ulditem.ItemData.ID, MOT.Air, null);
                if (null != status && status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return true;
            }
            return false;
        }

        internal static ULDEditor Instance
        {
            get
            {
                if (instance == null)
                    instance = new ULDEditor();
                return instance;
            }
        }
    }
}
