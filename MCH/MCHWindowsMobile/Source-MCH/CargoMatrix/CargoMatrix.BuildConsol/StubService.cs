﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSPieceScan;

namespace CargoMatrix.LoadConsol
{
    public static class StubService
    {
        static StubService()
        {
            ULDs = new List<MawbULD>();
        }
        public static List<MawbULD> ULDs
        {
            get;set;
        }
    }
}
