﻿using System;
namespace CargoMatrix.LoadConsol
{
    partial class HawbLocationList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ButtonVisible = true;
            this.ButtonImage = Resources.Skin.filter_btn; //Resources.Skin.btn_treeView;
            this.ButtonPressedImage = Resources.Skin.filter_btn_over; //Resources.Skin.btn_treeView_over;
            this.Height += 20;
            this.ButtonClick += new EventHandler(HawbLocationList_ButtonClick);

            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        }

        #endregion
    }
}
