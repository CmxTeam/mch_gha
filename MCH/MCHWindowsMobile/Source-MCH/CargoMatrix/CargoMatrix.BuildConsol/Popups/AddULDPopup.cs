﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using SmoothListbox.ListItems;
using CargoMatrix.LoadConsol;
using CargoMatrix.LoadContainer;

namespace CargoMatrix.LoadConsol
{
    public partial class AddULDPopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        private IULDType containerType;
        private string containerNumber;
        private UI.BarcodeReader barcodereader;
        public IULDType ContainerType
        {
            get { return containerType; }
        }

        public string ContainerNumber
        {
            get { return containerNumber; }
        }
        public bool ShowLoose { get; set; }

        public AddULDPopup(IULDItem uld)
        {
            InitializeComponent();
            barcodereader = new CargoMatrix.UI.BarcodeReader();
            barcodereader.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcodereader_BarcodeReadNotify);
            this.Closing += (o, e) =>
            {
                barcodereader.StopRead();
            };
            if (null != uld)
            {
                string uldRef = uld.IsLoose ? uld.ULDName : uld.ULDNo;
                this.Title = "Edit Uld - " + uldRef;
                containerType = uld;
                textBoxContainerNo.Text = uld.ULDNo;
                textBoxContainerType.Text = uld.ULDName;
                EnableControls(uld);
            }
            else
            {
                this.Title = "Add New ULD";
                barcodereader.StartRead();

            }
        }

        void barcodereader_BarcodeReadNotify(string barcodeData)
        {
            var scan = Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scan.BarcodeType == CMXBarcode.BarcodeTypes.Uld)
            {
                UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                textBoxContainerNo.Text = scan.UldNumber;
            }
            else
            {
                UI.CMXMessageBox.Show("Invalid Barcode Scan", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
            barcodereader.StartRead();
        }

        void buttonContainerType_Click(object sender, System.EventArgs e)
        {
            var uld = SharedMethods.DisplayULDTypes(ShowLoose, CargoMatrix.Communication.ScannerUtilityWS.MOT.Air);
            if (uld != null)
            {
                this.containerType = uld;
                this.textBoxContainerType.Text = uld.ULDName;
                EnableControls(uld);
            }
        }

        private void EnableControls(IULDType uld)
        {
            if (uld.IsLoose)
            {
                textBoxContainerNo.Enabled = false;
                textBoxContainerNo.WatermarkText = string.Empty;
                textBoxContainerNo.Text = string.Empty;
                barcodereader.StopRead();
            }
            else
            {
                textBoxContainerNo.Enabled = true;
                barcodereader.StopRead();
                barcodereader.StartRead();
            }
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            this.containerNumber = textBoxContainerNo.Text;
            DialogResult = DialogResult.OK;
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            buttonOk.Enabled =
                // Uld is not loose and both fields are populated
                (!string.IsNullOrEmpty(textBoxContainerType.Text) && !ContainerType.IsLoose &&
                !string.IsNullOrEmpty(textBoxContainerNo.Text))
                ||
                // uld is loose
                (!string.IsNullOrEmpty(textBoxContainerType.Text) && ContainerType.IsLoose);
        }
    }
}
