﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSLoadConsol;
using CMXExtensions;
using CustomListItems;

namespace CargoMatrix.LoadConsol
{
    public partial class LockedHawbList : CargoMatrix.Utilities.MessageListBox
    {
        public LockedHawbList()
        {
            InitializeComponent();
            this.HeaderText2 = "Forklift/Short/Takeoff Housebills";
            this.HeaderText = ForkLiftViewer.Instance.Mawb.Reference();
            DisplayHousebills();
        }


        private void DisplayHousebills()
        {
            var housebills = CargoMatrix.Communication.LoadConsol.Instance.GetLockedHawbs(ForkLiftViewer.Instance.Mawb.MasterBillId,
                ForkLiftViewer.Instance.Mawb.TaskId, CargoMatrix.Communication.WSLoadConsol.HouseBillFields.HousebillNumber, ForkLiftViewer.Instance.Mode == ConsolMode.Load);


            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxBase1.RemoveAll();

            this.MultiSelectListEnabled = false;
            this.IsSelectable = true;
            this.UnselectListEnabled = true;
            foreach (var item in housebills.HouseBills)
            {
                Image icon = item.ScanMode == ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode;
                string secLine = string.Format(item.LockReason + " : " + item.UserName);
                ThreeLineItem<LockedHouseBillItem> li = new ThreeLineItem<LockedHouseBillItem>(item,
                    string.Format("{0}-{1}-{2}", item.Origin,item.HousebillNumber, item.Destination),
                    item.LockReason + " : " + item.UserName,"Task : " + item.Task, icon);
                li.ButtonVisible = false;
                smoothListBoxBase1.AddItem2(li);
            }
            smoothListBoxBase1.LayoutItems();
            smoothListBoxBase1.RefreshScroll();
            smoothListBoxBase1.IsSelectable = false;
            Cursor.Current = Cursors.Default;
        }
    }
}
