﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSLoadConsol;

namespace CargoMatrix.LoadConsol
{
    public enum FilteringMode
    {
        Hawb, Mawb
    }
    public partial class FilterPopup : MessageListBox
    {
        private CustomListItems.ChoiceListItem filterItem;
        private CustomListItems.ChoiceListItem sortItem;
        private MessageListBox optionalChoiceList;
        private string filter = LoadConsolResources.TextAll;
        private string sort = MawbSort.MASTERBILL;
        private FilteringMode mode;
        public string Filter
        {
            get { return filter; }
            set { filter = value; filterItem.LabelLine2.Text = value; }
        }
        public string Sort
        {
            get { return sort; }
            set { sort = value; sortItem.LabelLine2.Text = value; }
        }
        public int SortValue
        {
            get;
            set;
        }
        private static FilterPopup _isntance = new FilterPopup();
        private FilterPopup()
        {
            filter = "Not Scanned";
        }
        public FilterPopup(FilteringMode mode)
        {
            InitializeComponent();
            this.HeaderText = "Select filters";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = true;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FilterPopup_ListItemClicked);
            TopPanel = false;
            filterItem = new CustomListItems.ChoiceListItem("Filter", CargoMatrix.Resources.Skin.Filter, !string.IsNullOrEmpty(filter) ? filter : LoadConsolResources.TextAll);
            AddItem(filterItem);
            this.mode = mode;
            //if (mode == FilteringMode.Hawb)
            {
                sortItem = new CustomListItems.ChoiceListItem("Sort", CargoMatrix.Resources.Skin.Sort, !string.IsNullOrEmpty(sort) ? sort : LoadConsolResources.TextAll);
                AddItem(sortItem);
            }
        }


        void FilterPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            // first time initialization
            if (null == this.optionalChoiceList)
            {
                optionalChoiceList = new MessageListBox();
                optionalChoiceList.MultiSelectListEnabled = false;
                optionalChoiceList.OneTouchSelection = true;
                optionalChoiceList.TopPanel = false;
            }
            // if filter
            if (listItem == filterItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = LoadConsolResources.Text_FilterBy;



                optionalChoiceList.AddItems(GetFilters());

                // select current selected status
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == filter)
                    {
                        optionalChoiceList.SelectControl(item);
                        break;
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    filter = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    filterItem.LabelLine2.Text = filter;
                }
            }
            //if sort 
            if (listItem == sortItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = LoadConsolResources.Text_Sortby;
                optionalChoiceList.AddItems(GetSorts());

                // select current selected direction
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == sort)
                    {
                        optionalChoiceList.SelectControl(item);
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    sort = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    sortItem.LabelLine2.Text = sort;
                    SortValue = (optionalChoiceList.SelectedItems[0] as StandardListItem).ID;
                }
            }
            sender.Reset();
        }

        public static string GetFilter()
        {
            // first time initialization
            if (null == _isntance.optionalChoiceList)
            {
                _isntance.optionalChoiceList = new MessageListBox();
                _isntance.optionalChoiceList.MultiSelectListEnabled = false;
                _isntance.optionalChoiceList.OneTouchSelection = true;
                _isntance.optionalChoiceList.TopPanel = false;
            }
            _isntance.optionalChoiceList.RemoveAllItems();
            _isntance.optionalChoiceList.HeaderText = LoadConsolResources.Text_FilterBy;



            _isntance.optionalChoiceList.AddItems(_isntance.GetFilters());

            // select current selected status
            foreach (Control item in _isntance.optionalChoiceList.Items)
                if (item is StandardListItem && (item as StandardListItem).title.Text == _isntance.filter)
                {
                    _isntance.optionalChoiceList.SelectControl(item);
                    break;
                }


            if (_isntance.optionalChoiceList.ShowDialog() == DialogResult.OK)
                return (_isntance.optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
            else return _isntance.filter;
        }

        private Control[] GetFilters()
        {
            if (mode == FilteringMode.Hawb)
                return new Control[] 
                {
                    new StandardListItem(ScanStatuses.NotScanned.ToString(), null),
                    new StandardListItem(ScanStatuses.Scanned.ToString(), null),
                    new StandardListItem(ScanStatuses.Shortage.ToString(), null),
                    new StandardListItem(ScanStatuses.TakeOff.ToString(), null),
                    //new StandardListItem(ScanStatuses.Removed.ToString(), null) // TFS#6463
                    ////new StandardListItem(LoadConsolResources.TextAll, null), // TFS#3447
                    //new StandardListItem(HawbFilter.NOT_SCANNED, null),
                    ////new StandardListItem(HawbFilter.SCANNED, null), //TFS #3447
                    //new StandardListItem(HawbFilter.SHORT, null), 
                    //new StandardListItem(HawbFilter.TAKEOFF, null),
                    //new StandardListItem(HawbFilter.REMOVED, null)
                };
            else
                return new Control[] {
                    new StandardListItem(LoadConsolResources.TextAll, null),
                    new StandardListItem(MawbFilter.NOT_STARTED, null),
                    new StandardListItem(MawbFilter.IN_PROGRESS, null),
                    new StandardListItem(MawbFilter.NOT_COMPLETED, null),
                    new StandardListItem(MawbFilter.COMPLETED, null) };
        }

        private Control[] GetSorts()
        {
            if (mode == FilteringMode.Hawb)
                return new Control[] {
                    new StandardListItem(LoadConsolResources.TextAll, null),
                    new StandardListItem(HawbSort.HAWB, null),
                    new StandardListItem(HawbSort.WEIGHT, null),
                    new StandardListItem(HawbSort.WGHT_LOC, null),
                    new StandardListItem(HawbSort.PIECES, null),
                    new StandardListItem(HawbSort.ORIGIN, null),
                    new StandardListItem(HawbSort.DESTINATION, null),
                    new StandardListItem(HawbSort.UNKNOWN, null),
                    new StandardListItem(HawbSort.LOCATION, null),
                    new StandardListItem(HawbSort.SCAN, null)};
            else
                return new Control[]
                { 
                new StandardListItem(LoadConsolResources.TextAll, null,(int)CargoMatrix.Communication.WSLoadConsol.MasterBillFields.NA),
                new StandardListItem(MawbSort.CARRIER, null),
                new StandardListItem(MawbSort.MASTERBILL, null),
                new StandardListItem(MawbSort.WEIGHT, null),
                new StandardListItem(MawbSort.ORIGIN, null),
                new StandardListItem(MawbSort.HAWBS, null),
                new StandardListItem(MawbSort.CUTOFF, null)
                };
        }
    }
}
