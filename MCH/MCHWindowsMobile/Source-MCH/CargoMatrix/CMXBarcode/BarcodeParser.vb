﻿
Imports System.Text.RegularExpressions

Partial Public Class BarcodeParser

    Public Function Parse(ByVal barcodeData As String) As ScanObject

        Dim scan As New ScanObject
        scan.BarcodeData = barcodeData


        Try

            Dim MatchObj As Match

            MatchObj = Regex.Match(barcodeData, "^O(.?)*\+[Y|S][0-9]{4}\+$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.OnHand

                Dim s() As String = Split(barcodeData, "+")
                scan.OnHandNumber = s(0).Remove(0, 1)

                'Dim pieceNumber As String = s(1).Remove(s(1).Length - 1, 1)
                Dim pieceNumber As String = s(1).Remove(0, 1)

                If IsNumeric(pieceNumber) Then
                    scan.PieceNumber = pieceNumber
                End If


                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^O\w{8}\+[Y|S][0-9]{4}\+$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.OnHand

                Dim s() As String = Split(barcodeData, "+")
                scan.OnHandNumber = s(0).Remove(0, 1)

                Dim pieceNumber As String = s(1).Remove(0, 1)

                If IsNumeric(pieceNumber) Then
                    scan.PieceNumber = pieceNumber
                End If


                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "[A-Z][A-Z][P][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9]", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                If Mid(barcodeData.Trim, 10, 1) = "0" Then
                    scan.UldNumber = Mid(barcodeData.Trim, 3, 7).Trim & Mid(barcodeData.Trim, 1, 2).Trim
                Else
                    scan.UldNumber = Mid(barcodeData.Trim, 3, 8).Trim & Mid(barcodeData.Trim, 1, 2).Trim
                End If
                scan.BarcodeType = BarcodeTypes.Uld
                Return scan
            End If



            MatchObj = Regex.Match(barcodeData, "(\w{7}|H\w{7})(\s)*\+[Y|S][0-9][0-9][0-9][0-9](\+)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.HouseBill

                Dim s() As String = Split(barcodeData.Trim, "+")
                scan.HouseBillNumber = s(0).Trim

                If Len(scan.HouseBillNumber) > 7 And Mid(scan.HouseBillNumber, 1, 1) = "H" Then
                    scan.HouseBillNumber = Mid(scan.HouseBillNumber, 2, Len(scan.HouseBillNumber) - 1)
                End If

                If IsNumeric(s(1).Remove(0, 1).Trim) Then
                    scan.PieceNumber = s(1).Remove(0, 1)
                End If
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^H\w{7}(\+)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.HouseBill
                scan.HouseBillNumber = Mid(barcodeData, 2, 7)
                scan.PieceNumber = 0
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^P[-|*](.?)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.Printer
                scan.PrinterName = Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^A[-|*](.?)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.Area
                scan.Location = Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^B[-|*](.?)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.Area
                scan.Location = Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^C[-|*](.?)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.Area
                scan.Location = Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^D[-|*](.?)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.Door
                scan.Location = Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^S[-|*](.?)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.ScreeningArea
                scan.Location = Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^T[-|*](.?)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.Truck
                scan.Location = Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^L[-|*](.?)*", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.Uld
                scan.UldNumber = Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^U[-|*]\d+", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                If MatchObj.Value.Trim.Length <= 10 Then
                    scan.BarcodeType = BarcodeTypes.User
                    scan.UserNumber = CInt(Mid(MatchObj.Value.Trim, 3, MatchObj.Value.Trim.Length - 2))
                    Return scan
                End If
            End If

            MatchObj = Regex.Match(barcodeData, "^(T999)\w+", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                Dim temp As String = MatchObj.Value.Trim
                If temp.Length <= 11 Then
                    scan.CarrierNumber = "T999"
                    scan.MasterBillNumber = Mid(temp, 5, temp.Length - 4)
                    scan.BarcodeType = BarcodeTypes.MasterBill
                    Return scan
                End If
            End If

            MatchObj = Regex.Match(barcodeData, "^([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                Dim temp As String = MatchObj.Value.Trim
                If temp.Length <= 11 Then
                    scan.CarrierNumber = Mid(temp, 1, 3)
                    scan.MasterBillNumber = Mid(temp, 4, 11)
                    scan.BarcodeType = BarcodeTypes.MasterBill
                    Return scan
                End If
            End If



            MatchObj = Regex.Match(barcodeData, "^([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                Dim temp As String = MatchObj.Value.Trim
                If temp.Length = 16 Then
                    scan.CarrierNumber = Mid(temp, 1, 3)
                    scan.MasterBillNumber = Mid(temp, 4, 8)
                    scan.BarcodeType = BarcodeTypes.MasterBill
                    Return scan
                End If
            End If

            MatchObj = Regex.Match(barcodeData, "^([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                Dim temp As String = MatchObj.Value.Trim
                If temp.Length = 15 Then
                    scan.CarrierNumber = Mid(temp, 1, 3)
                    scan.MasterBillNumber = Mid(temp, 4, 8)
                    scan.BarcodeType = BarcodeTypes.MasterBill
                    Return scan
                End If
            End If

            MatchObj = Regex.Match(barcodeData, "^(\w\w\w-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-\w\w\w)$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                Dim temp As String = MatchObj.Value.Trim
                If temp.Length <= 11 Then
                    scan.CarrierNumber = Mid(temp, 4, 3)
                    scan.MasterBillNumber = Mid(temp, 9, 8)
                    scan.BarcodeType = BarcodeTypes.MasterBill
                    Return scan
                End If
            End If




            MatchObj = Regex.Match(barcodeData, "^[A-Z][A-Z][A-Z]\w{7}[0-9][0-9][0-9][0-9][0-9]", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.HouseBill
                scan.HouseBillNumber = Mid(barcodeData, 4, 7)
                scan.PieceNumber = Mid(barcodeData, 11, 5)
                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "^O(.)*$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.OnHand

                scan.OnHandNumber = barcodeData.Remove(0, 1)
                scan.PieceNumber = 0


                Return scan
            End If

            MatchObj = Regex.Match(barcodeData, "\w{7}", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                scan.BarcodeType = BarcodeTypes.HouseBill
                scan.HouseBillNumber = IIf(Mid(barcodeData, 1, 1) = "H", Mid(barcodeData, 2, 7), Mid(barcodeData, 1, 7))
                scan.PieceNumber = 0
                Return scan
            End If


   
            MatchObj = Regex.Match(barcodeData, "^[0-9]*\t[0-9]*\t[0-9]*\t[0-9]*\.[0-9]\s*$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                Dim spliteddata As String() = Regex.Split(barcodeData, "\t", RegexOptions.IgnoreCase)
                If spliteddata.Length = 5 Then
                    If IsNumeric(spliteddata(0)) And IsNumeric(spliteddata(1)) And IsNumeric(spliteddata(2)) And IsNumeric(spliteddata(3)) Then
                        scan.Length = spliteddata(0)
                        scan.Width = spliteddata(1)
                        scan.Height = spliteddata(2)
                        scan.Weight = spliteddata(3)
                        scan.BarcodeType = BarcodeTypes.Dimensions
                        Return scan
                    End If
                End If
            End If

            'This is for testing only
            MatchObj = Regex.Match(barcodeData, "^[0-9]*\-[0-9]*\-[0-9]*\-[0-9]*\.[0-9]\-$", RegexOptions.IgnoreCase)
            If MatchObj.Success = True Then
                Dim spliteddata As String() = Regex.Split(barcodeData, "-", RegexOptions.IgnoreCase)
                If spliteddata.Length = 5 Then
                    If IsNumeric(spliteddata(0)) And IsNumeric(spliteddata(1)) And IsNumeric(spliteddata(2)) And IsNumeric(spliteddata(3)) Then
                        scan.Length = spliteddata(0)
                        scan.Width = spliteddata(1)
                        scan.Height = spliteddata(2)
                        scan.Weight = spliteddata(3)
                        scan.BarcodeType = BarcodeTypes.Dimensions
                        Return scan
                    End If
                End If
            End If
      





            scan.BarcodeType = BarcodeTypes.NA
            Return scan
        Catch ex As Exception
            scan.BarcodeType = BarcodeTypes.NA
            Return scan
        End Try

    End Function



    Public Const PrefixHousebill As String = "H"
    Public Const PrefixULD As String = "L"
    Public Const PrefixArea As String = "B"
    Public Const PrefixDoor As String = "D"
    Public Const PrefixScreeningArea As String = "S"
    Public Const PrefixTruck As String = "T"
    Public Const PrefixUser As String = "U"
    Public Const PrefixPrinter As String = "P"
    Public Const PrefixOnHand As String = "O"


End Class





Public Class ScanObject

    Public BarcodeData As String

    Public BarcodeType As BarcodeTypes

    Public HouseBillNumber As String

    Public MasterBillNumber As String

    Public CarrierNumber As String

    Public UldNumber As String

    Public Location As String

    Public PieceNumber As Integer

    Public PinNumber As Integer

    Public UserNumber As Integer

    Public PrinterName As String

    Public OnHandNumber As String

    Public Weight As Double

    Public Width As Integer

    Public Length As Integer

    Public Height As Integer


End Class

Public Enum BarcodeTypes

    NA

    HouseBill

    MasterBill

    Uld

    Door

    Area

    Truck

    User

    ScreeningArea

    Printer

    OnHand

    Dimensions

End Enum

