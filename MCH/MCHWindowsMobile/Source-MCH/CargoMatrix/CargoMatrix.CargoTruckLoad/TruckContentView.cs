﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoTruckLoad;
using CustomListItems.SelectHighlightListItem;
using CustomListItems;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.CargoTruckLoad
{
    public partial class TruckContentView : CargoMatrix.Utilities.MessageListBox
    {
        ICargoTruckLoadDoor door;
        IMasterBillItem masterBill;
        
        public TruckContentView(IMasterBillItem masterBill, ICargoTruckLoadDoor door)
        {
            this.masterBill = masterBill;
            this.door = door;

            InitializeComponent();

            smoothListBoxBase1.MultiSelectEnabled = false;
            this.OneTouchSelection = false;
            this.HeaderText = "Truck View";
           
            this.HeaderText2 = door.Truck != null ? door.Truck.Location : string.Empty;

            this.PopulateTruckContent();
        }

        private void PopulateTruckContent()
        {
            SelectHighlightItem combo = new SelectHighlightItem(0, CargoMatrix.Resources.Skin.TruckView_32x32);
            combo.DescriptionLine = string.Format("Pieces: {0}", 4);
            combo.TitleLine = "Test Truck";
            combo.IsReadonly = true;

            List<ExpenListItemData> items = new List<ExpenListItemData>();

            
            ExpenListItemData itemData = new ExpenListItemData(0, "LOOSE", string.Format("Pieces : {0} of {1}", 3, 5));

            itemData.IsReadonly = false;
            itemData.IsSelectable = true;
            items.Add(itemData);

            itemData = new ExpenListItemData(1, "L - 1234567", string.Format("Pieces : {0}", 4));

            itemData.IsReadonly = false;
            itemData.IsSelectable = true;
            items.Add(itemData);

            combo.AddExpandItemsRange(items);
           
            smoothListBoxBase1.AddItem(combo);
        }
    }
}