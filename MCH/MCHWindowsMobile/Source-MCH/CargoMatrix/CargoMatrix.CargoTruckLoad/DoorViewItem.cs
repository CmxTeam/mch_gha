﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoTruckLoad;

namespace CargoMatrix.CargoTruckLoad
{
    public partial class DoorViewItem : CustomListItems.ExpandableListItem<ICargoTruckLoadDoor>
    {
        public event EventHandler OnEditClick;
        public event EventHandler OnBrowseClick;
        
        public DoorViewItem(ICargoTruckLoadDoor cargoTruckLoadDoor)
         : base(cargoTruckLoadDoor)
        {
            InitializeComponent();

            this.DisplayItem();
        }

        private void DisplayItem()
        {
            this.title.Text = this.ItemData.Name;

            if (this.ItemData.Truck != null)
            {
                this.labelLine2.Text = string.Format("Truck : {0}", this.ItemData.Truck.Location);
            }
        }

        void btnEdit_Click(object sender, System.EventArgs e)
        {
            var editClick = OnEditClick;

            if (editClick == null) return;

            editClick(this, EventArgs.Empty);
        }

        void btnBrowse_Click(object sender, System.EventArgs e)
        {
            var browseClick = OnBrowseClick;

            if (browseClick == null) return;

            browseClick(this, EventArgs.Empty);
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Compare(this.ItemData.Name, TextBox1.Text, StringComparison.OrdinalIgnoreCase) == 0;
        }

        public override void OnDataItemChanged()
        {
            this.DisplayItem();
        }

        public void ClearEnter()
        {
            this.TextBox1.Text = string.Empty;
        }
    }
}
