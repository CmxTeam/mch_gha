﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Utilities;
using CMXBarcode;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.CargoTruckLoad;
using CargoMatrix.Communication.WSLoadConsol;
using CMXExtensions;
using CargoMatrix.UI;

namespace CargoMatrix.CargoTruckLoad
{
    public partial class ULDList : CargoUtilities.SmoothListBoxAsync2Options<IULD>
    {
        IMasterBillItem masterBill;
        IEnumerable<ICargoTruckLoadDoor> doors;
      
        #region Constructors
        public ULDList(IMasterBillItem masterBill, IEnumerable<ICargoTruckLoadDoor> doors)
        {
            InitializeComponent();

            this.masterBill = masterBill;
            this.doors = doors;

            var carrier = CargoMatrix.Communication.ScannerUtility.Instance.GetCarrierInfo(masterBill.CarrierNumber);
            this.pcxLogo.Image = CargoMatrix.Communication.Utilities.ConvertToImage(carrier.CarrierLogo);

            this.TitleText = "CargoTruckLoad";

            this.pcxLogo.Image = CargoMatrix.Communication.Utilities.ConvertToImage(carrier.CarrierLogo);

            this.labelReference.Text = masterBill.Reference();
            this.lblLine2.Text = masterBill.Line2();
            this.labelBlink.Text = "SCAN/SELECT ULD";
            this.labelBlink.Blink = true;

            this.ShowContinueButton(true);
            this.ButtonFinalizeText = "Load";
            this.panel1.Height = 0;

            this.btnForkliftScanned.Value = 0;
            this.EnableFinalizeButton();

            this.FinalizeButtonEnabled = false;

            this.LoadOptionsMenu += new EventHandler(ULDList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ULDList_MenuItemClicked);


            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(ULDList_BarcodeReadNotify);
            
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            Forklift.Instance.MasterBill = this.masterBill;
            
        }
        #endregion

        #region Handlers
        void ULDList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;

                default: return;
            }

        }

        void ULDList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void ULDList_BarcodeReadNotify(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Uld:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    string uldNumber = scanItem.UldNumber.StartsWith("L-") ? scanItem.UldNumber.Remove(0, 2) : scanItem.UldNumber;

                    var uldListItem = (
                                        from item in this.smoothListBoxMainList.Items.OfType<ULDViewItem>()
                                        where string.Compare(item.Uld.ULDNo, uldNumber, true) == 0 &&
                                        item.Uld.IsLoose() == false
                                        select item
                                      ).FirstOrDefault();

                    if (uldListItem == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtLooseBillNotList, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        break;
                    }

                    this.ScanULDToForklift(uldListItem);

                    break;

                case CMXBarcode.BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    CargoMatrix.Communication.DTO.HouseBillItem housebill;
                    var status = CargoMatrix.Communication.LoadConsol.Instance.CanScanIntoLoadTruckForkLift(this.masterBill.MasterBillNumber, this.masterBill.CarrierNumber, Forklift.Instance.ID,  scanItem.HouseBillNumber, scanItem.PieceNumber, out housebill);
                    
                   if(status.TransactionStatus1 == false)
                   {
                       CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                       break; 
                   }

                   var piece = (from item in housebill.Pieces
                                where item.PieceNumber == scanItem.PieceNumber
                                select item).FirstOrDefault();

                    if (piece == null && housebill.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtWrongPieceNumber, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        break;
                    }

                    this.ScanLooseToForklift(housebill, piece);

                    break;

                case CMXBarcode.BarcodeTypes.Truck:

                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    if (this.btnForkliftScanned.Value <= 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtScanPieces, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        break;
                    }

                    this.ContinueToLoad(scanItem.Location);

                    break;


                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);

                    CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtWrongBarcodeType, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                    break;
            }

            BarcodeEnabled = false;
            BarcodeEnabled = true;

            this.Refresh();

            this.EnableFinalizeButton();

            Cursor.Current = Cursors.Default;
        }

        void uld_ButtonEnterClick(object sender, EventArgs e)
        {
            if ((sender is ULDViewItem) == false) return;

            var uldListItem = (ULDViewItem)sender;

            var uld = uldListItem.Uld;

            if (uld.IsLoose())
            {
                this.PopulateUldContent(uldListItem, false);
            }
            else
            {
                this.ScanULDToForklift(uldListItem);
            }

            this.EnableFinalizeButton();
        }

        void uld_ButtonBrosweClick(object sender, EventArgs e)
        {
            ULDViewItem listItem = (ULDViewItem)sender;

            IULD uld = listItem.Uld;

            MessageListBox actPopup = new MessageListBox();

            var printLabesListItem = new SmoothListbox.ListItems.StandardListItem("PRINT LABELS", null, 2);
            printLabesListItem.Enabled = !uld.IsLoose();

            Control[] actions = new Control[] 
            { 
                new SmoothListbox.ListItems.StandardListItem("VIEW CONTENT", null, 1),
                printLabesListItem
                
            };

            string headerPart = uld.IsLoose() ? "LOOSE" : uld.ULDNo;

            actPopup.AddItems(actions);
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = headerPart;
            actPopup.HeaderText2 = string.Format("Select action for {0}", headerPart);

            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1: // View uld content

                        this.PopulateUldContent(listItem);

                        break;

                    case 2: // Print Labels

                        CommonMethods.PrintUldLabels(uld);

                        break;
                }
            }

            actPopup.ResetSelection();


        }

        void btnForkliftScanned_Click(object sender, System.EventArgs e)
        {
            var getTrucks = CommonMethods.Func(() => CommonMethods.GetUtilityTrucks(this.doors));
            this.OpenForkliftView(null, getTrucks);
        }

        #endregion

        #region Overrides
        protected override Control InitializeItem(IULD item)
        {
            ULDViewItem uld = new ULDViewItem(item, true);
            uld.ButtonBrosweClick += new EventHandler(uld_ButtonBrosweClick);
            uld.ButtonEnterClick += new EventHandler(uld_ButtonEnterClick);
            return uld;
        }
        
        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            this.BarcodeEnabled = false;

            var forklift = Forklift.Instance;

            if (forklift.IsLoaded == false) return;

            CargoMatrix.Communication.WSLoadConsol.TransactionStatus transactionStatus;
            var ulds = CargoMatrix.Communication.LoadConsol.Instance.GetLoadTruckMasterBillUlds(this.masterBill.MasterBillId,
                                                                                                this.masterBill.TaskId,
                                                                                                forklift.ID,
                                                                                                out transactionStatus);
         
            this.TitleText = this.GetMainHeaderText(ulds.Count());
            
            this.btnForkliftScanned.Value = transactionStatus.TransactionRecords;
            this.EnableFinalizeButton();

            this.ReloadItemsAsync(ulds);
            
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            this.TryFinalizeLoadTruck();

            this.ShowContinueButton(true);

            Cursor.Current = Cursors.Default;
        }

        public void LoadControl(CargoMatrix.Communication.CargoTruckLoad.UldList uldList)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            this.BarcodeEnabled = false;

            this.TitleText = this.GetMainHeaderText(uldList.Ulds.Count());

            this.btnForkliftScanned.Value = uldList.TransactionStatus.TransactionRecords;
            this.EnableFinalizeButton();

            this.ReloadItemsAsync(uldList.Ulds);

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            this.TryFinalizeLoadTruck();

            Cursor.Current = Cursors.Default;
        }

        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            base.buttonContinue_Click(sender, e);


            this.ContinueToLoad(null);
            
        }
        
        #endregion

        #region Additional Functionallity

        
        private void OpenForkliftView(CargoMatrix.Communication.ScannerUtilityWS.LocationItem truck, Func<CargoMatrix.Communication.ScannerUtilityWS.LocationItem[]> getTrucks)
        {
            if (this.btnForkliftScanned.Value <= 0) return;

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;

            var forklift = Forklift.Instance;
            forklift.TruckToLoad = truck;
            forklift.GetTrucks = getTrucks;

            forklift.ShowDialog();

            forklift.TruckToLoad = null;
            this.Refresh();

            switch (forklift.ActionPerformed)
            {
                case Forklift.ActionType.NoAction:

                    this.BarcodeEnabled = false;
                    this.BarcodeEnabled = true;

                    break;

                case Forklift.ActionType.Delete:
                case Forklift.ActionType.MovedToTruck:

                    this.LoadControl();

                    break;
            }
        }

        private void ContinueToLoad(string truckName)
        {
            string truckToLoad = truckName;

            CargoMatrix.Communication.ScannerUtilityWS.LocationItem truck = null;
            var getTrucks = CommonMethods.Func(() => CommonMethods.GetUtilityTrucks(this.doors));

            if (string.IsNullOrEmpty(truckToLoad) == true)
            {
                this.BarcodeEnabled = false;
                this.BarcodeEnabled = false;

                string scannedText;
                string locationName;
                var result = CommonMethods.GetTruckFromUser(getTrucks, out scannedText, out locationName);

                this.BarcodeEnabled = false;
                this.BarcodeEnabled = true;

                if (result == true)
                {
                    truckToLoad = locationName;
                }
                else
                {
                    this.EnableFinalizeButton();
                    return;
                }
            }

            truck = CommonMethods.GetTruckByName(truckToLoad, getTrucks);

            if (truck == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtScannedBarcodeNotinList, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            Forklift forklift = Forklift.Instance;

            if (forklift.ScanningType == CargoMatrix.Communication.ScannerUtilityWS.StepsTypes.OneScan)
            {
                Cursor.Current = Cursors.WaitCursor;
                
                LoadTruckULD[] ulds;
                var status = CargoMatrix.Communication.LoadConsol.Instance.GetLoadTruckForkliftContent(this.masterBill.MasterBillId, this.masterBill.TaskId, forklift.ID, out ulds);

                if(status.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    Cursor.Current = Cursors.Default;
                    return;
                }

                var uldIds = from item in ulds
                             where item.IsLoose() == false
                             select item.ID;

                var housebills = from item in ulds
                                 from houseBill in item.HouseBills
                                 where item.IsLoose() == true
                                 select houseBill;

                var pieces = this.CreateMoveToTruckParameter(housebills);

                status = CargoMatrix.Communication.LoadConsol.Instance.ScanItemsIntoTruck(uldIds.ToArray(), pieces.ToArray(), this.masterBill.MasterBillId, this.masterBill.TaskId, forklift.ID, truck.LocationId);


                Cursor.Current = Cursors.Default;
                
                if (status.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }

                this.btnForkliftScanned.Value = 0;

                this.EnableFinalizeButton();

                this.TryFinalizeLoadTruck();
            }
            else
            {
                this.OpenForkliftView(truck, getTrucks);
            }
        }

        List<CargoMatrix.Communication.WSLoadConsol.PieceItem> CreateMoveToTruckParameter(IEnumerable<CargoMatrix.Communication.WSLoadConsol.HouseBillItem> housebills)
        {
            var pieces = new List<CargoMatrix.Communication.WSLoadConsol.PieceItem>();

            foreach (var housebill in housebills)
            {
                if (housebill.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece)
                {
                    foreach (var p in housebill.Pieces)
                    {
                        var piece = new CargoMatrix.Communication.WSLoadConsol.PieceItem();
                        piece.HouseBillId = housebill.HousebillId;
                        piece.PieceId = p.PieceId;
                        piece.scanMode = CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece;
                        piece.TaskId = this.masterBill.TaskId;

                        pieces.Add(piece);
                    }
                }
                else
                {
                    var piece = new CargoMatrix.Communication.WSLoadConsol.PieceItem();
                    piece.PieceId = housebill.Pieces.Length;
                    piece.scanMode = CargoMatrix.Communication.WSLoadConsol.ScanModes.Count;
                    piece.TaskId = this.masterBill.TaskId;
                    piece.HouseBillId = housebill.HousebillId;
                    pieces.Add(piece);
                }
            }

            return pieces;

        }

        
        private void PopulateUldContent(ULDViewItem uldListItem)
        {
            this.PopulateUldContent(uldListItem, true);
        }

        private void PopulateUldContent(ULDViewItem uldListItem, bool previewOnly)
        {
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;

            UldContent uldContent = new UldContent(this.masterBill, uldListItem.Uld, previewOnly);
            uldContent.HeaderText = uldListItem.Line1.Replace(Environment.NewLine, string.Empty);
            uldContent.HeaderText2 = previewOnly == true ? "ULD Content" : "Move pieces to forklift";

            uldContent.ShowDialog();
            this.Refresh();

            if (uldContent.ViewAction == UldContent.UldContentViewAction.Scan)
            {
                this.LoadControl(uldContent.UldList);
            }
            else
            {
                this.BarcodeEnabled = false;
                this.BarcodeEnabled = true;
            }
        }

        private void ScanLooseToForklift(CargoMatrix.Communication.DTO.HouseBillItem houseBill, PieceItem piece)
        {
            var forklift = Forklift.Instance;
            List<PieceItem> pieceItems = new List<PieceItem>();

            switch (houseBill.ScanMode)
            {
                case  CargoMatrix.Communication.DTO.ScanModes.Count:
                    
                    int? piecesCount = CommonMethods.GetPiecesCountFromUser(houseBill.Reference(), "Number of pieces", houseBill.Pieces.Length, false);

                    if (piecesCount.HasValue == false) return;

                        var pieceItem = new CargoMatrix.Communication.WSLoadConsol.PieceItem();
                        pieceItem.HouseBillId = houseBill.HousebillId;
                        pieceItem.PieceId = piecesCount.Value;
                        pieceItem.scanMode = CargoMatrix.Communication.WSLoadConsol.ScanModes.Count;
                        pieceItem.TaskId = this.masterBill.TaskId;

                        pieceItems.Add(pieceItem);
                   

                    break;

                case CargoMatrix.Communication.DTO.ScanModes.Piece:

                    piece.TaskId = this.masterBill.TaskId;
                    piece.scanMode = CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece;
                    piece.HouseBillId = houseBill.HousebillId;
                    pieceItems.Add(piece);

                    break;
            }

            var uldList = CargoMatrix.Communication.LoadConsol.Instance.ScanPiecesIntoLoadTruckForkLift(pieceItems.ToArray(), this.masterBill.MasterBillId, this.masterBill.TaskId, forklift.ID);

            this.LoadControl(uldList);
        }

        private void ScanULDToForklift(ULDViewItem uldListItem)
        {
            Cursor.Current = Cursors.WaitCursor;

            var forklift = Forklift.Instance;

            var status = CargoMatrix.Communication.LoadConsol.Instance.ScanULDIntoLoadTruckForkLift(this.masterBill.MasterBillId, uldListItem.Uld.ID, this.masterBill.TaskId, forklift.ID);

            Cursor.Current = Cursors.Default;

            if (status.TransactionStatus1 == true)
            {
                this.btnForkliftScanned.Value = status.TransactionRecords;

                this.smoothListBoxMainList.RemoveItem(uldListItem);

                this.smoothListBoxMainList.itemsPanel.Top = 0;

                this.LayoutItems();
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Message);
            }
        }

        private void TryFinalizeLoadTruck()
        {
            var status = CargoMatrix.Communication.LoadConsol.Instance.IsLoadTruckReady(this.masterBill.TaskId);

            if (status.TransactionStatus1 == true)
            {
                var dResult = CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtShipmentsLoaded, "Question", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Yes);

                if (dResult == DialogResult.Cancel)
                {
                    CMXAnimationmanager.GoBack();
                    return;
                }

                bool result = CommonMethods.Finalize(this.masterBill);

                if (result == true)
                {
                    CMXAnimationmanager.GoBack(2);
                }
            }
        }

        private string GetMainHeaderText(int itemsCount)
        {
            return string.Format("CargoTruckLoad ({0})", itemsCount);
        }

        private void EnableFinalizeButton()
        {
            this.FinalizeButtonEnabled = this.btnForkliftScanned.Value > 0;
        }

        


        #endregion

    }

        
}

