﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;

namespace CargoMatrix.CargoTruckLoad
{
    public partial class ULDViewItem : SmoothListbox.ListItems.ListItem
    {
        SmoothListbox.SmoothListBoxBase _parentList;
        public event EventHandler ButtonBrosweClick;
        public event EventHandler ButtonEnterClick;

        protected CargoMatrix.UI.CMXTextBox cmxTextBox1;
        protected CargoMatrix.UI.CMXPictureButton buttonEnter;
        protected CargoMatrix.UI.CMXPictureButton buttonCancel;
        protected CargoMatrix.UI.CMXPictureButton buttonBrowse;
        protected System.Windows.Forms.Label labelConfirm;
        protected bool firstTimeSelection = true;
        private IULD uld;

        public IULD Uld
        {
            get { return uld; }
            set 
            { 
                uld = value;

                this.DisplayULD(uld, this.Expandable);
            }
        }

        public string Line1
        {
            get
            {
                return this.lblLine1.Text;
            }
        }

        
        public bool Expandable { get; set; }
        
        public ULDViewItem(IULD uld)
            : this(uld, false)
        {

        }
        
        public ULDViewItem(IULD uld, bool expandable)
        {
            InitializeComponent();
            this.uld = uld;

            this.DisplayULD(uld, expandable);
           
        }

        private void DisplayULD(IULD uld, bool expandable)
        {
            FormatLine1();
            this.Expandable = expandable;

            this.ID = uld.ID;
            this.pcxLogo.Image = CargoMatrix.Resources.Skin.Freight_Car;
            //this.lblWeight.Text = string.Format("WT: {0}KG", (int)uld.Weight);

            if(uld.IsLoose())
            {
                this.lblPieces.Text = string.Format("{0} of {1}", uld.Pieces, uld.Pieces + uld.ScannedPieces);
                
                if(uld.ScannedPieces == 0)
                {
                    this.lblPieces.ForeColor = Color.Black;
                }
                else
                {
                    this.lblPieces.ForeColor = uld.Pieces == uld.ScannedPieces ? Color.Green : Color.Red;
                }
            }
            else
            {
                this.lblPieces.Text = uld.Pieces.ToString();
            }

            this.lblLine3.Text = string.Format("LOC: {0}", uld.Location.Location);
        }

        public override void SelectedChanged(bool isSelected)
        {
            base.SelectedChanged(isSelected);

            if (Expandable)
            {
                Expand(isSelected);
            }

        }

        
        private void CheckForLooseULD()
        {
            if (uld.IsLoose() == true)
            {
                if (null != cmxTextBox1)
                {
                    cmxTextBox1.Enabled = false;
                    cmxTextBox1.TextBoxBackColor = Color.LightGray;
                    this.labelConfirm.Text = "Click Check to Confirm";

                }
            }
            else
                if (null != cmxTextBox1)
                {
                    cmxTextBox1.Enabled = true;
                    cmxTextBox1.TextBoxBackColor = Color.White;
                    this.labelConfirm.Text = "Confirm ULD Number";
                }
        }
        
        
        private SmoothListbox.SmoothListBoxBase parentList
        {
            get
            {
                if (_parentList == null)
                    for (Control control = this.Parent; control != null; control = control.Parent)
                        if (control is SmoothListbox.SmoothListBoxBase)
                        {
                            _parentList = (control as SmoothListbox.SmoothListBoxBase);
                            break;
                        }
                return _parentList;
            }
        }
        
        private void FormatLine1()
        {
            if (uld.ULDNo != string.Empty)
            {

                if (uld.ULDType.Length > 8 || uld.ULDNo.Length > 8 || uld.ULDType.Length + uld.ULDNo.Length > 16)
                {
                    lblLine1.Text = string.Format("{0} - {1}{2}", uld.ULDType, Environment.NewLine, uld.ULDNo);
                    lblLine1.Height = 26;
                    lblLine1.Top = 2;
                    lblPiecesHdr.Top = 31;
                    lblPieces.Top = 31;
                    //lblWeight.Top = 31;
                    lblLine3.Top = 47;
                }
                else
                {
                    lblLine1.Text = string.Format("{0} - {1}", uld.ULDType, uld.ULDNo);
                    lblLine1.Top = 7;
                    lblLine1.Height = 16;
                    lblPiecesHdr.Top = 23;
                    lblPieces.Top = 23;
                    //lblWeight.Top = 23;
                    lblLine3.Top = 38;
                }
            }
            else
            {
                lblLine1.Text = uld.ULDType;
                lblLine1.Top = 7;
                lblLine1.Height = 16;
                lblPiecesHdr.Top = 25;
                lblPieces.Top = 25;
                //lblWeight.Top = 25;
                lblLine3.Top = 42;

            }
            AutoScaleDimensions = new SizeF(96F, 96F);
            AutoScaleMode = AutoScaleMode.Dpi;

        }
        
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ButtonBrosweClick != null)
                ButtonBrosweClick(this, e);
        }
        
        private void Expand(bool isSelected)
        {
            if (isSelected)
            {
                if (firstTimeSelection)
                {

                    InitializeAdditionalComponents();
                    firstTimeSelection = false;

                }
                ////////////////////////////////
                labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = true;

                Cursor.Current = Cursors.Default;

                this.Height = 107;
                cmxTextBox1.SelectAll();
                cmxTextBox1.Focus();

                parentList.MoveControlToTop(this);
                parentList.RefreshScroll();
            }
            else
            {
                Height = 63;
                if (!firstTimeSelection)
                    labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = false;
            }
                AutoScaleDimensions = new SizeF(96F, 96F);
                AutoScaleMode = AutoScaleMode.Dpi;
        }

        private void InitializeAdditionalComponents()
        {
            this.SuspendLayout();

            this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
            this.buttonEnter = new CargoMatrix.UI.CMXPictureButton();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.labelConfirm = new System.Windows.Forms.Label();
            // cmxTextBox1
            // 
            this.cmxTextBox1.Location = new System.Drawing.Point(4, 75);
            this.cmxTextBox1.Size = new System.Drawing.Size(145, 28);
            this.cmxTextBox1.TabIndex = 2;
            this.cmxTextBox1.KeyDown += new KeyEventHandler(cmxTextBox1_KeyDown);
            this.cmxTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            CheckForLooseULD();

            // 
            // buttonEnter
            // 
            this.buttonEnter.Location = new System.Drawing.Point(153, 75);
            //this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(40, 28);
            this.buttonEnter.Click += new EventHandler(buttonEnter_Click);
            this.buttonEnter.SizeMode = PictureBoxSizeMode.StretchImage;
            this.buttonEnter.Image = global::Resources.Graphics.Skin.manual_entry_ok;
            this.buttonEnter.PressedImage = global::Resources.Graphics.Skin.manual_entry_ok_over;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(195, 75);
            //this.buttonEnter.Name = "buttonEnter";
            this.buttonCancel.Size = new System.Drawing.Size(40, 28);
            this.buttonCancel.Click += new EventHandler(buttonCancel_Click);
            this.buttonCancel.SizeMode = PictureBoxSizeMode.StretchImage;
            this.buttonCancel.Image = global::Resources.Graphics.Skin.manual_entry_cancel;
            this.buttonCancel.PressedImage = global::Resources.Graphics.Skin.manual_entry_cancel_over;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(196, 75);
            //this.buttonBrowse.Name = "buttonEnter";
            this.buttonBrowse.Size = new System.Drawing.Size(40, 28);
            this.buttonBrowse.SizeMode = PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.Image = global::Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            // 
            // labelConfirm
            // 
            this.labelConfirm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelConfirm.ForeColor = System.Drawing.Color.Black;
            this.labelConfirm.Location = new System.Drawing.Point(3, 60);
            this.labelConfirm.Size = new System.Drawing.Size(232, 12);

            this.Controls.Add(this.labelConfirm);
            this.Controls.Add(this.cmxTextBox1);
            this.Controls.Add(this.buttonEnter);
            this.Controls.Add(this.buttonCancel);
            this.ResumeLayout();
            parentList.RefreshScroll();

        }

        void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonEnter_Click(null, null);
            }
        }

        void buttonCancel_Click(object sender, EventArgs e)
        {
            this.SelectedChanged(false);
            parentList.Reset();
        }

        void buttonEnter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            char[] whiteSpaces = new char[] { ' ' };
            cmxTextBox1.Text = cmxTextBox1.Text.TrimEnd(whiteSpaces);
            cmxTextBox1.Text = cmxTextBox1.Text.TrimStart(whiteSpaces);
            cmxTextBox1.Text = cmxTextBox1.Text.ToUpper();
            if (this.uld.ULDNo == cmxTextBox1.Text)
            {
                Cursor.Current = Cursors.Default;

                cmxTextBox1.Text = string.Empty;
                
                if (ButtonEnterClick != null)
                {
                    ButtonEnterClick(this, e);
                }


            }
            else
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Text entered does not match. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                cmxTextBox1.SelectAll();
                cmxTextBox1.Focus();
            }
        }
    }
}
