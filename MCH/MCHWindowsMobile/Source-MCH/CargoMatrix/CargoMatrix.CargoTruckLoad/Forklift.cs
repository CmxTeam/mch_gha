﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CustomListItems;
using CustomListItems.SelectHighlightListItem;
using CMXBarcode;
using CMXExtensions;
using CargoMatrix.Communication.CargoTruckLoad;


namespace CargoMatrix.CargoTruckLoad
{
    internal partial class Forklift : CargoMatrix.Utilities.MessageListBox
    {
        private static Forklift instance;

        CargoMatrix.Communication.DTO.IMasterBillItem masterBill;

        int iD;
        CargoMatrix.Communication.ScannerUtilityWS.StepsTypes scanningType;
        ActionType actionPerformed;
        CargoMatrix.UI.BarcodeReader barcode;
        CargoMatrix.Communication.ScannerUtilityWS.LocationItem truckToLoad;
        Func<CargoMatrix.Communication.ScannerUtilityWS.LocationItem[]> getTrucks;

        public Func<CargoMatrix.Communication.ScannerUtilityWS.LocationItem[]> GetTrucks
        {
            get { return getTrucks; }
            set { getTrucks = value; }
        }

        public CargoMatrix.Communication.ScannerUtilityWS.LocationItem TruckToLoad
        {
            get { return truckToLoad; }
            set { truckToLoad = value; }
        }

        public ActionType ActionPerformed
        {
            get { return actionPerformed; }
            set { actionPerformed = value; }
        }

        public static Forklift Instance
        {
            get
            {
                if (instance == null)
                    instance = new Forklift();
                if (instance.IsLoaded == false)
                    instance.LoadAppSettings();
                return instance;
            }
        }

        public bool IsLoaded { get; private set; }

        public int ID
        {
            get { return iD; }
        }

        public CargoMatrix.Communication.ScannerUtilityWS.StepsTypes ScanningType
        {
            get { return scanningType; }
        }

        public CargoMatrix.Communication.DTO.IMasterBillItem MasterBill
        {
            get { return masterBill; }
            set { masterBill = value; }
        }

        private Forklift()
        {
            InitializeComponent();
            smoothListBoxBase1.MultiSelectEnabled = true;
            this.HeaderText = "Forklift View";
            this.HeaderText2 = "Select pieces to drop into truck";
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(Forklift_LoadListEvent);
            this.OKClicked += new EventHandler(Forklift_OKClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(Forklift_ListItemClicked);

            this.actionPerformed = ActionType.NoAction;
            this.OneTouchSelection = false;

            this.barcode = new CargoMatrix.UI.BarcodeReader();
            this.barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcode_BarcodeReadNotify);
        }

        void Forklift_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>)
            {
                var combo = (SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>)listItem;

                var houseBill = combo.CustomData;

                if (houseBill.ScanMode != CargoMatrix.Communication.WSLoadConsol.ScanModes.Count)
                {
                    combo.HighlightAll();

                    this.EnableOKButon();

                    return;
                }

                if (isSelected == false)
                {
                    combo.HighlightAll(false);
                }
                else
                {
                    combo.HighlightItemsByUserInput();
                }
            }
            else
            {
                if (listItem is SelectHighlightItem<LoadTruckULD>)
                {
                    var combo = (SelectHighlightItem<LoadTruckULD>)listItem;

                    combo.HighlightAll();
                }
            }


            this.EnableOKButon();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.barcode.StopRead();

            base.OnClosing(e);
        }

        void barcode_BarcodeReadNotify(string barcodeData)
        {
            ScanObject scanObject = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            switch (scanObject.BarcodeType)
            {
                case BarcodeTypes.Uld:

                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);

                    var uldItem = (from item in this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<LoadTruckULD>>()
                                   where string.Compare(item.CustomData.ULDNo, scanObject.UldNumber, true) == 0
                                   select item).FirstOrDefault();

                    if (uldItem == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Uld is not in the list", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        break;
                    }

                    uldItem.IsSelected = true;

                    uldItem.HighlightAll();


                    break;

                case BarcodeTypes.HouseBill:

                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    CargoMatrix.Communication.DTO.HouseBillItem housebill;
                    var status = CargoMatrix.Communication.LoadConsol.Instance.CanScanIntoLoadTruck(this.masterBill.MasterBillNumber, this.masterBill.CarrierNumber, this.ID, scanObject.HouseBillNumber, scanObject.PieceNumber, out housebill);

                    if (status.TransactionStatus1 == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        break;
                    }



                    var housebillItem = (from item in this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>>()
                                         where string.Compare(item.CustomData.HousebillNumber, scanObject.HouseBillNumber, true) == 0
                                         select item).FirstOrDefault();

                    if (housebillItem == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Housebill is not in the list", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        break;
                    }

                    var piece = (from item in housebillItem.CustomData.Pieces
                                 where item.PieceNumber == scanObject.PieceNumber
                                 select item).FirstOrDefault();

                    if (piece == null && housebillItem.CustomData.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Scanned piece is not in the list", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        break;
                    }


                    if (housebillItem.CustomData.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece)
                    {
                        housebillItem.HighlightPieces(new int[] { piece.PieceId }, true);
                    }
                    else
                    {
                        housebillItem.HighlightItemsByUserInput();

                    }

                    if (housebillItem.ItemsHeighlightCount == housebillItem.MaxItemsSelectHighlightCount)
                    {
                        housebillItem.SelectedChanged(true);
                    }



                    break;

                default:

                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);

                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    break;
            }

            this.barcode.StartRead();

            this.EnableOKButon();
        }


        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            if (this.truckToLoad == null)
            {
                this.barcode.StopRead();

                string scannedText;
                string locationName;
                var result = CommonMethods.GetTruckFromUser(getTrucks, out scannedText, out locationName);

                this.barcode.StartRead();

                if (result == false) return;


                truckToLoad = CommonMethods.GetTruckByName(locationName, getTrucks);

                if (truckToLoad == null)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtScannedBarcodeNotinList, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }
            }

            var uldIDs = from item in this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<LoadTruckULD>>()
                         where item.SelectState != SelectStateEnum.NotSelected
                         select item.CustomData.ID;

            var houseBillItems = from item in this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>>()
                                 where item.SelectState != SelectStateEnum.NotSelected
                                 select item;

            if (uldIDs.Count() <= 0 && houseBillItems.Count() <= 0) return;

            Cursor.Current = Cursors.WaitCursor;

            var pieceItems = this.CreateMoveToTruckParameter(houseBillItems);

            var status = CargoMatrix.Communication.LoadConsol.Instance.ScanItemsIntoTruck(uldIDs.ToArray(), pieceItems.ToArray(), this.masterBill.MasterBillId, this.masterBill.TaskId, this.ID, this.truckToLoad.LocationId);

            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            this.actionPerformed = ActionType.MovedToTruck;

            this.PopulateForkliftContent();
        }

        void Forklift_OKClicked(object sender, EventArgs e)
        {
            //if(this.truckToLoad == null)
            //{
            //    this.barcode.StopRead();

            //    string scannedText;
            //    string locationName;
            //    var result = CommonMethods.GetTruckFromUser(getTrucks, out scannedText, out locationName);

            //    this.barcode.StartRead();

            //    if (result == false) return;


            //    truckToLoad = CommonMethods.GetTruckByName(locationName, getTrucks);

            //    if (truckToLoad == null)
            //    {
            //        CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtScannedBarcodeNotinList, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            //        return;
            //    }
            //}

            //var uldIDs = from item in this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<LoadTruckULD>>()
            //                   where item.SelectState != SelectStateEnum.NotSelected
            //                   select item.CustomData.ID;

            //var houseBillItems = from item in this.smoothListBoxBase1.Items.OfType<SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>>()
            //                     where item.SelectState != SelectStateEnum.NotSelected
            //                     select item;

            //if (uldIDs.Count() <= 0 && houseBillItems.Count() <= 0) return;

            //Cursor.Current = Cursors.WaitCursor;

            //var pieceItems = this.CreateMoveToTruckParameter(houseBillItems);

            //var status = CargoMatrix.Communication.LoadConsol.Instance.ScanItemsIntoTruck(uldIDs.ToArray(), pieceItems.ToArray(), this.masterBill.MasterBillId, this.masterBill.TaskId, this.ID, this.truckToLoad.LocationId);

            //if(status.TransactionStatus1 == false)
            //{
            //    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            //    return;
            //}

            //this.actionPerformed = ActionType.MovedToTruck;

            //this.PopulateForkliftContent();
        }

        List<CargoMatrix.Communication.WSLoadConsol.PieceItem> CreateMoveToTruckParameter(IEnumerable<SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>> housebillItems)
        {
            var pieces = new List<CargoMatrix.Communication.WSLoadConsol.PieceItem>();

            foreach (var houseBillItem in housebillItems)
            {
                var houseBill = houseBillItem.CustomData;

                if (houseBill.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece)
                {
                    IEnumerable<int> ids = from itm in houseBillItem.SelectedItems
                                           select itm.ID;

                    foreach (var id in ids)
                    {
                        var piece = new CargoMatrix.Communication.WSLoadConsol.PieceItem();
                        piece.HouseBillId = houseBill.HousebillId;
                        piece.PieceId = id;
                        piece.scanMode = CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece;
                        piece.TaskId = this.masterBill.TaskId;

                        pieces.Add(piece);
                    }
                }
                else
                {
                    var piece = new CargoMatrix.Communication.WSLoadConsol.PieceItem();
                    piece.PieceId = houseBillItem.ItemsHeighlightCount;
                    piece.scanMode = CargoMatrix.Communication.WSLoadConsol.ScanModes.Count;
                    piece.TaskId = this.masterBill.TaskId;
                    piece.HouseBillId = houseBill.HousebillId;
                    pieces.Add(piece);
                }
            }

            return pieces;

        }

        void Forklift_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.barcode.StartRead();

            PopulateForkliftContent();
        }

        private void PopulateForkliftContent()
        {
            this.smoothListBoxBase1.RemoveAll();

            var icon = CargoMatrix.Resources.Skin.Freight_Car;

            LoadTruckULD[] ulds;
            var status = CargoMatrix.Communication.LoadConsol.Instance.GetLoadTruckForkliftContent(this.masterBill.MasterBillId, this.masterBill.TaskId, this.ID, out ulds);


            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                return;
            }


            var nonLooses = from item in ulds
                            where item.IsLoose() == false
                            select item;

            this.PopulateNonLooses(nonLooses);

            var looses = from item in ulds
                         where item.IsLoose() == true
                         select item;

            this.PopulateLooses(looses);

            this.EnableOKButon();

            if (this.smoothListBoxBase1.Items.Count <= 0)
            {
                this.Close();
            }
        }

        private void PopulateNonLooses(IEnumerable<LoadTruckULD> ulds)
        {
            var icon = CargoMatrix.Resources.Skin.Freight_Car;

            foreach (var uld in ulds)
            {
                var combo = new SelectHighlightItem<LoadTruckULD>(uld, uld.ID, icon);
                combo.IsReadonly = false;
                combo.TitleLine = uld.ULDNo;
                combo.DescriptionLine = string.Format("PCs: {0}", uld.Pieces);
                combo.Expandable = false;
                combo.ButtonClick += new EventHandler(combo_ButtonClick);
                combo.MaxItemsSelectHighlightCount = 1;

                smoothListBoxBase1.AddItem(combo);
            }
        }

        private void PopulateLooses(IEnumerable<LoadTruckULD> ulds)
        {
            foreach (var uld in ulds)
            {
                foreach (var houseBill in uld.HouseBills)
                {
                    var housebillIcon = houseBill.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.countMode;

                    var combo = new SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>(houseBill, uld.ID, housebillIcon);

                    combo.IsReadonly = false;
                    combo.TitleLine = string.Format("{0}-{1}-{2}", houseBill.Origin, houseBill.HousebillNumber, houseBill.Destination);
                    combo.Expandable = houseBill.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece ? true : false;
                    combo.IsSelectable = this.IsSelectable;
                    combo.SubItemSelected += new EventHandler<ExpandItemEventArgs>(combo_SubItemSelected);
                    combo.ButtonClick += new EventHandler(combo_ButtonClick);
                    // combo.SubItemButtonClick += new EventHandler<SubItemButtonClickEventArgs>(combo_SubItemButtonClick);
                    combo.DescriptionLine = this.GetExpandItemDescription(0, houseBill.Pieces.Length);
                    combo.ManualEnterDialogHeader = string.Format("{0}-{1}-{2}", houseBill.Origin, houseBill.HousebillNumber, houseBill.Destination);
                    combo.ManualEnterDialogSubHeader = "Enter number of pieces";
                    combo.DescriptionLineUpdateText = () =>
                    {
                        return this.GetExpandItemDescription(combo.ItemsHeighlightCount, combo.CustomData.Pieces.Length);
                    };

                    if (houseBill.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece)
                    {
                        List<ExpenListItemData> items = new List<ExpenListItemData>();

                        foreach (var piece in houseBill.Pieces)
                        {
                            ExpenListItemData itemData = new ExpenListItemData(piece.PieceId,
                                                                       string.Format("Piece {0}", piece.PieceNumber),
                                                                       piece.Location);
                            itemData.IsReadonly = true;
                            itemData.IsSelectable = true;
                            itemData.IsChecked = false;
                            items.Add(itemData);
                        }
                        combo.AddExpandItemsRange(items);

                        combo.MaxItemsSelectHighlightCount = items.Count;
                    }
                    else
                    {
                        combo.MaxItemsSelectHighlightCount = combo.CustomData.Pieces.Length;
                    }


                    smoothListBoxBase1.AddItem(combo);

                }
            }
        }

        void combo_SubItemButtonClick(object sender, SubItemButtonClickEventArgs e)
        {
            if ((sender is SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>) == false) return;

            var combo = (SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>)sender;

            var piece = (from item in combo.CustomData.Pieces
                         where item.PieceId == e.SubItem.ID
                         select item).FirstOrDefault();

            if (piece == null) return;

            string message = string.Format("Are you sure you want to remove pieces {0} from truck?", piece.PieceNumber);

            var dialodResult = CargoMatrix.UI.CMXMessageBox.Show(message, "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Yes);

            if (dialodResult != DialogResult.OK) return;

            var pieces = new CargoMatrix.Communication.WSLoadConsol.PieceItem[] { piece };



            //combo.RemoveItem(e.SubItem);

            //if (combo.Items.Count() <= 0)
            //{
            //    this.smoothListBoxBase1.RemoveItem(combo);
            //}

            //if (this.smoothListBoxBase1.Items.Count <= 0)
            //{
            //    this.Close();
            //}
        }


        void combo_ButtonClick(object sender, EventArgs e)
        {
            if (sender is SelectHighlightItem<LoadTruckULD>)
            {
                var combo = (SelectHighlightItem<LoadTruckULD>)sender;

                string message = string.Format("Are you sure you want to remove {0} from forklift?", combo.CustomData.ULDNo);

                var dialodResult = CargoMatrix.UI.CMXMessageBox.Show(message, "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Yes);

                if (dialodResult != DialogResult.OK) return;

                var status = CargoMatrix.Communication.LoadConsol.Instance.RemoveUldFromForklift(combo.CustomData.ID, this.masterBill.TaskId, this.ID);

                if (status.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                    return;
                }
                else
                {
                    this.RemoveItem(combo);
                }

            }
            else
            {
                if (sender is SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>)
                {
                    var combo = (SelectHighlightItem<CargoMatrix.Communication.WSLoadConsol.HouseBillItem>)sender;

                    string message = string.Format("Are you sure you want to remove {0} from forklift?", combo.CustomData.HousebillNumber);

                    var dialodResult = CargoMatrix.UI.CMXMessageBox.Show(message, "Delete Confirmation", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Yes);

                    if (dialodResult != DialogResult.OK) return;

                    var status = CargoMatrix.Communication.LoadConsol.Instance.RemoveLoadTruckHousebillFromForklift(combo.CustomData.HousebillId, this.masterBill.TaskId, this.ID);

                    if (status.TransactionStatus1 == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                        return;
                    }
                    else
                    {
                        this.RemoveItem(combo);
                    }
                }
            }

            this.actionPerformed = ActionType.Delete;

            if (this.smoothListBoxBase1.Items.Count <= 0)
            {
                this.Close();
            }
            else
            {
                this.EnableOKButon();
            }

        }

        void combo_SubItemSelected(object sender, ExpandItemEventArgs e)
        {
            this.EnableOKButon();
        }

        private void EnableOKButon()
        {
            var scannedItem = this.smoothListBoxBase1.Items.OfType<SelectHighlightItem>().
                                FirstOrDefault(item => item.SelectState != SelectStateEnum.NotSelected);

            this.OkEnabled = scannedItem != null;
        }

        public string GetExpandItemDescription(int selectedPieces, int totalPieces)
        {
            return string.Format("PCS: {0} of {1}", selectedPieces, totalPieces);
        }


        private void LoadAppSettings()
        {
            var taskSettings = CargoMatrix.Communication.LoadConsol.Instance.GetTaskSettingsTruckLoad();
            if (taskSettings.Transaction.TransactionStatus1 == true)
            {
                this.iD = taskSettings.ForkliftId;
                this.scanningType = taskSettings.ScanningSteps;

                if (this.scanningType == CargoMatrix.Communication.ScannerUtilityWS.StepsTypes.OneScan)
                {
                    this.IsSelectable = true;
                    this.HeaderText2 = "Select/Scan ULDs to truck";
                }
                else
                {
                    this.IsSelectable = false;
                    this.HeaderText2 = "Scan ULDs to truck";
                }
                this.IsLoaded = true;
            }
            else
            {
                this.IsLoaded = false;
                CargoMatrix.UI.CMXMessageBox.Show("Error has occured while downloading task settings. Refresh or restart the application", "Unable to connect", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }

        public enum ActionType
        {
            NoAction,
            Delete,
            MovedToTruck
        }
    }
}
