﻿namespace CargoMatrix.CargoReceiver
{
    partial class MawbCargoItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // MawbCargoItem
            // 
            this.Name = "MawbCargoItem";
            //this.Controls.Add(this.panelIndicators);
            this.Size = new System.Drawing.Size(240, 44);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

        }

        #endregion

        protected CargoMatrix.UI.CMXPictureButton buttonBrowse;
        //protected CustomUtilities.IndicatorPanel panelIndicators;


    }
}
