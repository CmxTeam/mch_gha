﻿namespace CargoMatrix.CargoReceiver
{
    partial class MawbList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.buttonFilter = new CargoMatrix.UI.CMXPictureButton();
            this.panelHeader2.SuspendLayout();
            //
            // searchbox
            //
            this.searchBox = new CargoMatrix.UI.CMXTextBox();
            this.searchBox.Location = new System.Drawing.Point(4, 21);
            this.searchBox.Size = new System.Drawing.Size(196, 23);
            this.searchBox.TextChanged += new System.EventHandler(searchBox_TextChanged);
            //
            //label1
            //
            this.label1 = new System.Windows.Forms.Label();
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Size = new System.Drawing.Size(198, 15);
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            //
            //
            //
            this.buttonFilter.Location = new System.Drawing.Point(202, 10);
            this.buttonFilter.Size = new System.Drawing.Size(32, 32);
            this.buttonFilter.Image = CargoMatrix.Resources.Skin.filter_btn;
            this.buttonFilter.PressedImage = CargoMatrix.Resources.Skin.filter_btn_over;
            this.buttonFilter.Click += new System.EventHandler(buttonFilter_Click);
            //
            //
            //
            this.panelHeader2.Controls.Add(searchBox);
            this.panelHeader2.Controls.Add(buttonFilter);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
            this.panelHeader2.Controls.Add(label1);
            panelHeader2.Height = 49;
            panelHeader2.Visible = true;
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.ResumeLayout(false);

        }


        protected System.Windows.Forms.Label label1;
        protected CargoMatrix.UI.CMXPictureButton buttonFilter;
        #endregion
    }
}
