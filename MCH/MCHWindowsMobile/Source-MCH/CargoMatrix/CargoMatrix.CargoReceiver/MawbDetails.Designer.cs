﻿namespace CargoMatrix.CargoReceiver
{
    partial class MawbDetails
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelWeight = new System.Windows.Forms.Label();
            this.labelPieces = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.labelUlds = new System.Windows.Forms.Label();
            this.ProceedButton = new OpenNETCF.Windows.Forms.Button2();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDamage = new CargoMatrix.UI.CMXPictureButton();
            this.buttonPrint = new CargoMatrix.UI.CMXPictureButton();
            this.labelHawbs = new System.Windows.Forms.Label();
            this.labelFlightNo = new System.Windows.Forms.Label();
            this.labelLocation = new System.Windows.Forms.Label();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.labelDescr = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBoxCarrierLogo = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.labelSlac = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelWeight
            // 
            this.labelWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelWeight.BackColor = System.Drawing.Color.White;
            this.labelWeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelWeight.Location = new System.Drawing.Point(75, 41);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(121, 13);
            // 
            // labelPieces
            // 
            this.labelPieces.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPieces.BackColor = System.Drawing.Color.White;
            this.labelPieces.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPieces.Location = new System.Drawing.Point(75, 57);
            this.labelPieces.Name = "labelPieces";
            this.labelPieces.Size = new System.Drawing.Size(30, 13);
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.BackColor = System.Drawing.Color.White;
            this.title.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(42, 8);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(160, 20);
            // 
            // labelUlds
            // 
            this.labelUlds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelUlds.BackColor = System.Drawing.Color.White;
            this.labelUlds.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelUlds.Location = new System.Drawing.Point(75, 73);
            this.labelUlds.Name = "labelUlds";
            this.labelUlds.Size = new System.Drawing.Size(121, 13);
            // 
            // itemPicture
            // 
            this.itemPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.itemPicture.BackColor = System.Drawing.Color.White;
            this.itemPicture.Location = new System.Drawing.Point(202, 4);//new System.Drawing.Point(202, 41);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(32, 32);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.Color.White;
            this.buttonDetails.Location = new System.Drawing.Point(202, 41);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // buttonDamage
            // 
            this.buttonDamage.BackColor = System.Drawing.Color.White;
            this.buttonDamage.Location = new System.Drawing.Point(202, 78);
            this.buttonDamage.Name = "buttonDamage";
            this.buttonDamage.Size = new System.Drawing.Size(32, 32);
            this.buttonDamage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDamage.TransparentColor = System.Drawing.Color.White;
            this.buttonDamage.Click += new System.EventHandler(this.buttonDamage_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.Color.White;
            this.buttonPrint.Location = new System.Drawing.Point(202, 115);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(32, 32);
            this.buttonPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonPrint.TransparentColor = System.Drawing.Color.White;
            this.buttonPrint.Click += new System.EventHandler(buttonPrint_Click);
            // 
            // labelHawbs
            // 
            this.labelHawbs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHawbs.BackColor = System.Drawing.Color.White;
            this.labelHawbs.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelHawbs.Location = new System.Drawing.Point(75, 89);
            this.labelHawbs.Name = "labelHawbs";
            this.labelHawbs.Size = new System.Drawing.Size(121, 13);
            // 
            // labelFlightNo
            // 
            this.labelFlightNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFlightNo.BackColor = System.Drawing.Color.White;
            this.labelFlightNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelFlightNo.Location = new System.Drawing.Point(75, 105);
            this.labelFlightNo.Name = "labelFlightNo";
            this.labelFlightNo.Size = new System.Drawing.Size(121, 13);
            // 
            // labelLocation
            // 
            this.labelLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLocation.BackColor = System.Drawing.Color.White;
            this.labelLocation.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLocation.Location = new System.Drawing.Point(75, 121);
            this.labelLocation.Name = "labelLastScan";
            this.labelLocation.Size = new System.Drawing.Size(121, 13);
            // 
            // panelIndicators
            // 
            this.panelIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIndicators.Location = new System.Drawing.Point(3, 157);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            this.panelIndicators.TabIndex = 19;
            // 
            // labelDescr
            // 
            this.labelDescr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDescr.BackColor = System.Drawing.Color.White;
            this.labelDescr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDescr.Location = new System.Drawing.Point(75, 137);
            this.labelDescr.Name = "labelDescr";
            this.labelDescr.Size = new System.Drawing.Size(121, 13);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(7, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.Text = "Weight :";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(7, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.Text = "Pieces :";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(7, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.Text = "ULDs :";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(7, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.Text = "Hawbs :";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(7, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.Text = "Flight No:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(7, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.Text = "Staged At:";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(7, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            // 
            // pictureBoxCarrierLogo
            // 
            this.pictureBoxCarrierLogo.BackColor = System.Drawing.Color.White;
            this.pictureBoxCarrierLogo.Location = new System.Drawing.Point(4, 4);
            this.pictureBoxCarrierLogo.Name = "pictureBoxCarrierLogo";
            this.pictureBoxCarrierLogo.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxCarrierLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 174);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(240, 40);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(111, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.Text = "Slac :";
            // 
            // labelSlac
            // 
            this.labelSlac.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSlac.BackColor = System.Drawing.Color.White;
            this.labelSlac.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelSlac.Location = new System.Drawing.Point(152, 57);
            this.labelSlac.Name = "labelSlac";
            this.labelSlac.Size = new System.Drawing.Size(30, 13);
            //
            //ProceedButton
            //
            this.ProceedButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProceedButton.BackColor = System.Drawing.SystemColors.Control;
            this.ProceedButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProceedButton.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ProceedButton.ForeColor = System.Drawing.Color.White;
            this.ProceedButton.Location = new System.Drawing.Point(80, 176);
            this.ProceedButton.Name = "ProceedButton";
            this.ProceedButton.Size = new System.Drawing.Size(80, 32);
            this.ProceedButton.TabIndex = 17;
            this.ProceedButton.Text = "RECOVER";

            // 
            // MawbDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pictureBoxCarrierLogo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.labelSlac);
            this.Controls.Add(this.labelPieces);
            this.Controls.Add(this.labelDescr);
            this.Controls.Add(this.labelLocation);
            this.Controls.Add(this.labelFlightNo);
            this.Controls.Add(this.labelHawbs);
            this.Controls.Add(this.labelUlds);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonDamage);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.panelIndicators);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.ProceedButton);
            this.Controls.Add(this.buttonPrint);
            this.ProceedButton.BringToFront();
            this.Name = "MawbDetails";
            this.Size = new System.Drawing.Size(240, 214);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelPieces;
        private System.Windows.Forms.Label labelUlds;
        private System.Windows.Forms.Label labelHawbs;
        private System.Windows.Forms.Label labelFlightNo;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Label labelDescr;
        private CustomUtilities.IndicatorPanel panelIndicators;
        private CargoMatrix.UI.CMXPictureButton buttonDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private CargoMatrix.UI.CMXPictureButton buttonDamage;
        private CargoMatrix.UI.CMXPictureButton buttonPrint;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBoxCarrierLogo;
        private System.Windows.Forms.PictureBox pictureBox2;
        internal OpenNETCF.Windows.Forms.Button2 ProceedButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelSlac;

    }
}
