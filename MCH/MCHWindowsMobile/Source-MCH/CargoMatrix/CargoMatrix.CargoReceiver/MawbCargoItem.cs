﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;

namespace CargoMatrix.CargoReceiver
{
    public partial class MawbCargoItem : CustomListItems.ExpandableRenderListitem<IMasterBillItem>, ISmartListItem
    {

        public event EventHandler ButtonClick;

        public MawbCargoItem(IMasterBillItem mawb)
            : base(mawb)
        {
            InitializeComponent();
            //this.panelIndicators.Flags = mawb.Flags;
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.MasterBillNumber, TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this, EventArgs.Empty);
            }
        }
        protected override void InitializeControls()
        {
            this.SuspendLayout();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBrowse.Location = new System.Drawing.Point(202, 6);
            this.buttonBrowse.Name = "btnPrint";
            this.buttonBrowse.Image = Resources.Skin.printerButton;
            this.buttonBrowse.PressedImage = Resources.Skin.printerButton_over;
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            this.buttonBrowse.Click += new System.EventHandler(buttonBrowse_Click);
            this.Controls.Add(this.buttonBrowse);
            this.buttonBrowse.Scale(new SizeF(CurrentAutoScaleDimensions.Width/96F,CurrentAutoScaleDimensions.Height/96F));
            this.ResumeLayout(false);
        }

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    gOffScreen.DrawString(ItemData.Reference(), fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string stagedLoc = string.Empty;
                    if (ItemData is CargoMatrix.Communication.WSCargoReceiver.MasterBillItem)
                        stagedLoc = (ItemData as CargoMatrix.Communication.WSCargoReceiver.MasterBillItem).StageLocation;
                    string line2 = string.Format("HBs: {0} Loc: {1}", ItemData.HouseBills, stagedLoc);
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 20 * vScale, 170 * hScale, 13 * vScale));
                }

                Image img = CargoMatrix.Resources.Skin.Clipboard;
                switch (ItemData.Status)
                {
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Open:
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Pending:
                        img = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case CargoMatrix.Communication.DTO.MasterbillStatus.InProgress:
                        img = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case CargoMatrix.Communication.DTO.MasterbillStatus.Completed:
                        img = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;

                }
                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            return ItemData.Reference().ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
