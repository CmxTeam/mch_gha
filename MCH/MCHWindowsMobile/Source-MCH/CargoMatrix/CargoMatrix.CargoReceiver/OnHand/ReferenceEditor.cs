﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CustomListItems;

namespace CargoMatrix.CargoReceiver
{
    partial class ReferenceEditor : CargoMatrix.Utilities.MessageListBox
    {
        public ReferenceEditor()
        {
            InitializeComponent();
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(ReferenceEditor_LoadListEvent);
            this.HeaderText = "References - ORG-123456-DST";
            this.ButtonVisible = true;
            this.HeaderText2 = "Add/Edit Reference";
            this.WideWidth();
            this.ButtonClick += new EventHandler(ButtonAdd_Click);
            OkEnabled = true;
        }

        void ReferenceEditor_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            LoadControl();
        }

        //protected override void buttonContinue_Click(object sender, EventArgs e)
        //{
        //    //base.buttonContinue_Click(sender, e);
        //}

        void ReferenceEditor_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as OptionsListITem).ID)
            {
                case OptionsListITem.OptionItemID.REFRESH:
                    LoadControl();
                    break;
            }
        }

        //void ReferenceEditor_LoadOptionsMenu(object sender, EventArgs e)
        //{
        //    AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        //}

        public void LoadControl()
        {
            this.smoothListBoxBase1.RemoveAll();
            var item = new ReferenceListItem<string>("ref1", null, "Ref Type") { Line2 = "Ref number" };
            item.ButtonEditClick += new EventHandler(item_ButtonEditClick);
            item.ButtonDeleteClick += new EventHandler(item_ButtonDeleteClick);
            this.AddItem(item);
        }

        void item_ButtonDeleteClick(object sender, EventArgs e)
        {
            smoothListBoxBase1.RemoveItem(sender as Control);
        }

        void item_ButtonEditClick(object sender, EventArgs e)
        {

        }
        void ButtonAdd_Click(object sender, System.EventArgs e)
        {
            string reftype = string.Empty;
            if (selectRefType(ref reftype))
            {
                var item = new ReferenceListItem<string>(reftype, null, reftype) { Line2 = "Ref number" };
                item.ButtonEditClick += new EventHandler(item_ButtonEditClick);
                item.ButtonDeleteClick += new EventHandler(item_ButtonDeleteClick);
                this.AddItem(item);
            }
        }
        private void EditReference()
        {
            var editSelect = new CargoMatrix.Utilities.MessageListBox();
            editSelect.AddItem(new StandardListItem("Edit Reference Type", null, 1));
            editSelect.AddItem(new StandardListItem("Edit Reference Number", null, 2));
            if (DialogResult.OK == editSelect.ShowDialog())
            {
                switch ((editSelect.SelectedItems[0] as StandardListItem).ID)
                {
                    case 1:
                        string reftype = string.Empty;
                        selectRefType(ref reftype);
                        break;
                    case 2:
                        break;
                }
            }
        }

        private bool selectRefType(ref string refType)
        {
            var typeListBox = new CargoMatrix.Utilities.MessageListBox();
            typeListBox.HeaderText2 = "Select Reference Type";
            typeListBox.HeaderText2 = "Reference Types";
            typeListBox.MultiSelectListEnabled = false;
            typeListBox.OneTouchSelection = true;
            typeListBox.AddItem(new StandardListItem<string>("Ref Type 1", null, "Ref Type 1"));
            typeListBox.AddItem(new StandardListItem<string>("Ref Type 2", null, "Ref Type 2"));
            if (DialogResult.OK == typeListBox.ShowDialog())
            {
                refType = (typeListBox.SelectedItems[0] as StandardListItem<string>).ItemData;
                return true;
            }
            else return false;
        }

    }
}