﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;


namespace CargoMatrix.CargoReceiver
{
    public partial class MawbList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        protected string filter = MawbFilter.NOT_COMPLETED;
        protected string sort = MawbSort.CARRIER;
        private int listItemHeight;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;

        public MawbList()
        {

            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(MawbList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(MawbList_ListItemClicked);
        }

        protected virtual void MawbList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (CargoMatrix.Communication.Utilities.IsAdmin)
            {
                MAWB_Enter_Click(listItem, null);
            }
            else
            {
                smoothListBoxMainList.MoveControlToTop(listItem);
                smoothListBoxMainList.LayoutItems();
            }

        }
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            BarcodeEnabled = false;

            if (ShowFilterPopup() == false)
                BarcodeEnabled = true;
        }
        void MawbList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO:
                        string carrier, mawb, hawb;
                        if (DialogResult.OK == ManualHawbMawb.Show("Enter Manually", "Enter Mawb or Hawb Details", out carrier, out mawb, out hawb))
                        {
                            var tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(carrier, mawb, hawb);
                            ProceedWithMawb(tempMawb);
                        }
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Mawb);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter + " / " + sort;
                LoadControl();
                return true;
            }
            return false;
        }

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO) { Title = "Manual Lookup" });
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
            //Application.DoEvents();
            this.label1.Text = string.Format("{0}/{1}", filter, sort);
            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;
            var items = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillList(Forklift.Instance.ReceiveMode, sort, filter, false);
            this.TitleText = string.Format("CargoReceiver - {0} - ({1})", Forklift.Instance.ReceiveMode.ToString(), items.Length);
            Cursor.Current = Cursors.Default;
            //ReloadItemsAsync(items);

            var rItems = from i in items
                         select InitializeItem(i);

            smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());

            Cursor.Current = Cursors.Default;
        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("CargoReceiver - {0} - ({1})", Forklift.Instance.ReceiveMode.ToString(), smoothListBoxMainList.VisibleItemsCount);
        }

        protected ICustomRenderItem InitializeItem(CargoMatrix.Communication.DTO.IMasterBillItem item)
        {
            MawbCargoItem tempMAWB = new MawbCargoItem(item);
            tempMAWB.OnEnterClick += new EventHandler(MAWB_Enter_Click);
            tempMAWB.ButtonClick += new EventHandler(MAWB_OnMoreClick);
            //if (!searchBox.Focused)
            //    searchBox.Focus();
            listItemHeight = tempMAWB.Height;
            return tempMAWB;
        }

        void MAWB_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            var mawb = (sender as MawbCargoItem).ItemData;
            Cursor.Current = Cursors.Default;
            MAWB_PrintLabels(mawb);

            // Multiple options
            /*

            if (MawbOptionsList == null)
            {
                MawbOptionsList = new MessageListBox();
                MawbOptionsList.OneTouchSelection = true;
                MawbOptionsList.MultiSelectListEnabled = false;
                MawbOptionsList.HeaderText = mawb.Reference();
                MawbOptionsList.HeaderText2 = "Select Action to Continue";
                MawbOptionsList.AddItem(new StandardListItem("Damage Capture", null, 1));
                MawbOptionsList.AddItem(new StandardListItem("View Masterbill Details", null, 2));
                MawbOptionsList.AddItem(new StandardListItem("Print Labels", null, 3));
            }
            MawbOptionsList.ResetSelection();
            Cursor.Current = Cursors.Default;
            if (DialogResult.OK == MawbOptionsList.ShowDialog())
            {
                int id = (MawbOptionsList.SelectedItems[0] as StandardListItem).ID;
                switch (id)
                {
                    case 1:
                        MAWB_DamageCapture(mawb);
                        break;
                    case 2:
                        MAWB_DetailsViewer(mawb);
                        break;
                    case 3:
                        MAWB_PrintLabels(mawb);
                        break;
                }
                MawbOptionsList.ResetSelection();
            }
            */
        }
        private void MAWB_PrintLabels(IMasterBillItem item)
        {
            BarcodeEnabled = false;
            CustomUtilities.HawbPiecesLabelPrinter.Show(item.MasterBillNumber, item.CarrierNumber);
            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        private void MAWB_DamageCapture(IMasterBillItem item)
        {
            Cursor.Current = Cursors.WaitCursor;

            MssterBillAsHouseBill masterAsHouseBill = new MssterBillAsHouseBill(item);

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(masterAsHouseBill, DamageCapture.DamageCaptureApplyTypeEnum.MasterBill);

            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionsSummarys(item.TaskId);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.ScannerUtility.Instance.UpdateMasterBillConditions(item.TaskId, args.ShipmentConditions);
            };

            CMXAnimationmanager.DisplayForm(damageCapture);
            Cursor.Current = Cursors.Default;
        }

        private void MAWB_DetailsViewer(IMasterBillItem item)
        {
            CargoMatrix.Viewer.HousebillViewer billViewer = new CargoMatrix.Viewer.HousebillViewer(item.CarrierNumber, item.MasterBillNumber);
            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }

        protected virtual void MAWB_Enter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            IMasterBillItem mawb = (sender as MawbCargoItem).ItemData;
            var tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(mawb.CarrierNumber, mawb.MasterBillNumber, string.Empty);
            ProceedWithMawb(tempMawb);
        }

        private bool ProceedWithMawb(CargoMatrix.Communication.WSCargoReceiver.MasterBillItem tempMawb)
        {
            if (tempMawb == null)
                return false;
            if (tempMawb.Transaction.TransactionStatus1 == false)
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show(tempMawb.Transaction.TransactionError, "Error: " + tempMawb.MasterBillNumber, CMXMessageBoxIcon.Delete);
                return false;
            }

            Forklift.Instance.Mawb = tempMawb;

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new CargoReceiver());
            Cursor.Current = Cursors.Default;
            return true;
        }

        void MawbList_BarcodeReadNotify(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            var barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            CargoMatrix.Communication.WSCargoReceiver.MasterBillItem tempMawb = null;
            switch (barcode.BarcodeType)
            {
                case BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    Forklift.Instance.FirstScannedHousebill = barcodeData;
                    tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(string.Empty, string.Empty, barcode.HouseBillNumber);
                    break;
                case BarcodeTypes.MasterBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(barcode.CarrierNumber, barcode.MasterBillNumber, string.Empty);
                    break;
                default:
                    CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_ScanHwbMawb, "Invalid Barcode", CMXMessageBoxIcon.Hand);
                    break;
            }

            if (ProceedWithMawb(tempMawb))
                return;
            else
            {
                Cursor.Current = Cursors.Default;
                BarcodeEnabled = true;
            }
        }
    }
    public class MssterBillAsHouseBill : IHouseBillItem
    {
        IMasterBillItem masterBill;

        public MssterBillAsHouseBill(IMasterBillItem masterBill)
        {
            this.masterBill = masterBill;
        }

        #region IHouseBillItem Members

        public string HousebillNumber
        {
            get
            {
                return this.masterBill.MasterBillNumber;
            }
        }

        public int HousebillId
        {
            get
            {
                return this.masterBill.MasterBillId;
            }
        }

        public string Origin
        {
            get
            {
                return this.masterBill.Origin;
            }
        }

        public string Destination
        {
            get
            {
                return this.masterBill.Destination;
            }
        }

        public int TotalPieces
        {
            get
            {
                return this.masterBill.Pieces;
            }
        }

        public int ScannedPieces
        {
            get
            {
                return this.masterBill.ScannedPieces;
            }
        }

        public int Slac
        {
            get
            {
                return this.masterBill.Slac;
            }
        }

        public int Weight
        {
            get
            {
                return this.masterBill.Weight;
            }
        }

        public string Location
        {
            get
            {
                return string.Empty;
            }
        }

        public int Flags
        {
            get
            {
                return this.masterBill.Flags;
            }
        }

        public ScanModes ScanMode
        {
            get
            {
                return ScanModes.NA;
            }
        }

        public HouseBillStatuses status
        {
            get
            {
                switch (this.masterBill.Status)
                {
                    case MasterbillStatus.All: return HouseBillStatuses.All;

                    case MasterbillStatus.Completed: return HouseBillStatuses.Completed;

                    case MasterbillStatus.InProgress: return HouseBillStatuses.InProgress;

                    case MasterbillStatus.Open: return HouseBillStatuses.Open;

                    case MasterbillStatus.Pending: return HouseBillStatuses.Pending;

                    default: return HouseBillStatuses.Open;

                }
            }
        }

        public RemoveModes RemoveMode
        {
            get
            {
                return RemoveModes.NA;
            }
        }

        public string Reference
        {
            get { return string.Format("{0}-{1}-{2}-{3}", this.masterBill.Origin, this.masterBill.CarrierNumber, this.masterBill.MasterBillNumber, this.masterBill.Destination); }
        }

        public string Line2
        {
            get { return string.Format("Weight: {0} KGS", this.masterBill.Weight); }
        }

        public string Line3
        {
            get { return string.Format("Pieces: {0} of {1}  Slac: {2}", this.masterBill.ScannedPieces, this.masterBill.Pieces, this.masterBill.Slac); }
        }

        public string Line4
        {
            get { return string.Empty; }
        }

        #endregion
    }
}
