﻿namespace CargoMatrix.FreightScreening
{
    partial class CargoInspection
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.topLabel = new CustomUtilities.CMXBlinkingLabel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.topPanel = new System.Windows.Forms.Panel();
            this.pictureboxBackground = new System.Windows.Forms.PictureBox();
            this.xrayDeviceLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            this.topPanel.SuspendLayout();

            // 
            // topLabel
            // 
            this.topLabel.BackColor = System.Drawing.Color.White;
            this.topLabel.Blink = true;
            this.topLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.topLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.topLabel.ForeColor = System.Drawing.Color.Red;
            this.topLabel.Location = new System.Drawing.Point(2, 15);
            this.topLabel.Name = "topLabel";
            this.topLabel.Size = new System.Drawing.Size(234, 15);
            //
            // xrayDeviceLabel
            //
            this.xrayDeviceLabel.BackColor = System.Drawing.Color.White;
            this.xrayDeviceLabel.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.xrayDeviceLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.xrayDeviceLabel.ForeColor = System.Drawing.Color.Black;
            this.xrayDeviceLabel.Location = new System.Drawing.Point(10, 3);
            this.xrayDeviceLabel.Name = "xrayDeviceLabel";
            this.xrayDeviceLabel.Text = string.Empty;
            this.xrayDeviceLabel.Size = new System.Drawing.Size(220, 15);

            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 43);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(240, 1);
            //
            // pictureboxBackground
            //
            this.pictureboxBackground.Size = new System.Drawing.Size(150, 150);
            this.pictureboxBackground.Location = new System.Drawing.Point(45, 75);
            this.pictureboxBackground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureboxBackground.Image = FreightScreening.tsa;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.topLabel);
            this.topPanel.Controls.Add(this.splitter1);
            this.topPanel.Controls.Add(xrayDeviceLabel);
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(240, 44);

            this.panelHeader2.Height = topPanel.Height;
            this.panelHeader2.Controls.Add(topPanel);
            this.Controls.Add(this.pictureboxBackground);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.topLabel.ResumeLayout(false);
            this.Controls.Add(pictureboxBackground);
            //xrayPlacement();
            this.ResumeLayout(false);

        }



        #endregion
        private System.Windows.Forms.Splitter splitter1;
        private CustomUtilities.CMXBlinkingLabel topLabel;
        private System.Windows.Forms.Label xrayDeviceLabel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.PictureBox pictureboxBackground;


    }
}

