﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using CargoMatrix.Communication.WSPieceScan;

namespace CargoMatrix.FreightScreening
{
    public partial class HawbViewer : CargoMatrix.Utilities.MessageListBox
    {
        private CargoMatrix.Communication.DTO.IHouseBillItem hawbItem;
        private int taskId;

        public HawbViewer(CargoMatrix.Communication.DTO.IHouseBillItem hawb, int taskId)
        {
            InitializeComponent();
            this.taskId = taskId;
            this.hawbItem = hawb;
            this.HeaderText = FreightScreening.Text_HawbViewTitle;
            this.HeaderText2 = FreightScreening.Text_HawbViewBlink;
            LoadControl();
        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            MoveHBPiecesResponse response;
            int pieceCount = 0;
            foreach (var item in smoothListBoxBase1.Items)
                pieceCount += (item as CustomListItems.ComboBox).SelectedIds.Count();


            response = CargoMatrix.Communication.HostPlusIncomming.Instance.MoveHouseBillPiecestoForklift(hawbItem.HousebillNumber, taskId, Forklift.Instance.ForkliftID,pieceCount, CargoMatrix.Communication.WSPieceScan.ScanTypes.Manual);
            if (!response.IsSuccess)
            {
                if (!string.IsNullOrEmpty(response.Message))
                    CargoMatrix.UI.CMXMessageBox.Show(response.Message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
            else
            {
                (this.Tag as ScreeningList).buttonScannedList.Value = response.NoOfPiecesInForklift;
                //buttonScannedList.Value = 
            }
            LoadControl();
        }

        public void LoadControl()
        {

            smoothListBoxBase1.RemoveAll();

            foreach (var loc in CargoMatrix.Communication.ScannerUtility.Instance.GetPiecesByLocation(hawbItem.HousebillId))
            {

                List<CustomListItems.ComboBoxItemData> items = new List<CustomListItems.ComboBoxItemData>();
                foreach (var pc in loc.Piece)

                    items.Add(new CustomListItems.ComboBoxItemData("Piece " + pc.PieceNumber, pc.Location, pc.PieceId));

                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawbItem.HousebillId, loc.Location, string.Format("PCS: {0} of {1}", loc.Piece.Length, hawbItem.TotalPieces), hawbItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Count ? CargoMatrix.Resources.Skin.countMode : CargoMatrix.Resources.Skin.PieceMode, items);
                combo.IsSelectable = true;
                combo.ReadOnly = true;
                combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                smoothListBoxBase1.AddItem(combo);
            }

            this.smoothListBoxBase1.SelectAll();
            Cursor.Current = Cursors.Default;
        }

        void combo_SelectionChanged(object sender, EventArgs e)
        {
            if ((sender as CustomListItems.ComboBox).SubItemSelected)
                OkEnabled = true;
            else
                OkEnabled = false;
        }
    }
}
