﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using SmoothListbox.ListItems;
using CustomListItems;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.OnHand
{
    public partial class OnHandSearchTasks : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        protected string filter = "Not Completed";
        protected string sort = "PO Number";

        SearchItem searchItem;
        public OnHandSearchTasks(SearchItem search)
        {
            InitializeComponent();
            searchItem = search;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(OnHandSelectTask_ListItemClicked);
            this.LoadOptionsMenu += new EventHandler(OnHandSelectTask_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(OnHandSelectTask_MenuItemClicked);
        }

        void OnHandSelectTask_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is OptionsListITem)
                switch ((listItem as OptionsListITem).ID)
                {
                    case OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case OptionsListITem.OptionItemID.FILTER:
                        FilterSort();
                        break;
                }
        }

        private void FilterSort()
        {
            FilterSortPopup filterPopup = new FilterSortPopup(filter, sort);
            if (DialogResult.OK == filterPopup.ShowDialog())
            {
                filter = filterPopup.Filter;
                sort = filterPopup.Sort;
                LoadControl();
            }
        }

        void OnHandSelectTask_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.REFRESH));
            //this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.FILTER));
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void OnHandSelectTask_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is StaticListItem)
            {
                NewOnhandPopup newOnHand = new NewOnhandPopup();
                if (DialogResult.OK == newOnHand.ShowDialog())
                {
                    var pickup = Communication.OnHand.OnHand.Instance.CreateNewPickupSkel(newOnHand.PickupRefNo, newOnHand.Vendor, string.Empty, newOnHand.Destination, newOnHand.MOT);
                    SharedMethods.CurrentPickup = pickup;
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new OnHandDetails());
                }
                else
                {
                    smoothListBoxMainList.Reset();
                }
            }
            else
            {
                SharedMethods.CurrentPickup = (listItem as OnHandListItem).ItemData;
                if (SharedMethods.CurrentPickup.Status != CargoMatrix.Communication.WSOnHand.ShipmentStatus.Complete)
                {
                    CargoMatrix.Communication.OnHand.OnHand.Instance.StartPickupRecovery(SharedMethods.CurrentPickup.ShipmentID);
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new OnHandDetails());
                }
                else
                {
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new PiecesList());
                }
            }

        }

        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            FilterSort();
        }

        void searchBox_TextChanged(object sender, System.EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
        }

        public override void LoadControl()
        {
            this.labelFilterSort.Text = string.Format("{0}/{1}", filter, sort);
            this.smoothListBoxMainList.RemoveAll();
            searchBox.Text = string.Empty;
            this.smoothListBoxMainList.AddItem(new StaticListItem("Create New Receiving Task", Resources.Icons.Notepad_Add));
            foreach (var item in Communication.OnHand.OnHand.Instance.SearchPickup(searchItem.PickupRefNo, searchItem.Customer, searchItem.Vendor, searchItem.OrderRef))
            {
                smoothListBoxMainList.AddItem2(new OnHandListItem(item));
            }
            LayoutItems();
            this.TitleText = string.Format("OnHand Tasks ({0})", smoothListBoxMainList.ItemsCount - 1);
        }
    }
}
