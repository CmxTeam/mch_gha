﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.OnHand
{
    public partial class PODetails : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        PODetailsItem poDetailsItem;
        long packageID;
        int paackageNo;
        public PODetails(long packID, int packNo, string title)
        {
            InitializeComponent();
            this.packageID = packID;
            this.TitleText = SharedMethods.CurrentPickup.ShipmentNO + " " + title;
            this.paackageNo = packNo;
            this.poDetailsItem = new PODetailsItem();
            this.Scrollable = false;
            this.LoadOptionsMenu += new EventHandler(PODetails_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(PODetails_MenuItemClicked);
            this.panelHeader2.Height = this.panelSoftkeys.Top - panelTitle.Bottom;
            this.poDetailsItem.Location = new Point(0, labelSplitter.Bottom);
            this.panelHeader2.Controls.Add(poDetailsItem);
            this.poDetailsItem.ButtonApplyClick += new EventHandler(poDetailsItem_ButtonApplyClick);
            this.poDetailsItem.ButtonDoneClick += (object sender, EventArgs e) => { this.pictureBoxBack_Click(sender, e); };
            this.poDetailsItem.ButtonListClick += new EventHandler(poDetailsItem_ButtonListClick);
            this.poDetailsItem.ButtonPrintClick += new EventHandler(poDetailsItem_ButtonPrintClick);
        }

        void poDetailsItem_ButtonPrintClick(object sender, EventArgs e)
        {
            var defPrinter = CargoMatrix.Communication.ScannerUtility.Instance.GetDefaultPrinter(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD);

            if (CustomUtilities.PrinterUtilities.PopulateAvailablePrinters(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.ULD, ref defPrinter))
            {
                var status = Communication.OnHand.OnHand.Instance.PrintPackageLabel(defPrinter.PrinterName, SharedMethods.CurrentPickup.ShipmentID, paackageNo);
                if (false == status.TransactionStatus1)
                    SharedMethods.ShowFailMessage(status.TransactionError);
            }
        }

        void PODetails_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as CustomListItems.OptionsListITem).ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                    LoadControl();
                    break;
                default:
                    break;
            }
        }

        void PODetails_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void poDetailsItem_ButtonListClick(object sender, EventArgs e)
        {
            var invoice = SharedMethods.DisplayOrderList();
            if (invoice != null)
            {
                this.Refresh();
                this.textboxLineItemNo.Text = invoice.LineItemNumber.ToString();
                this.textboxPONumber.Text = invoice.PoNumber;
                LoadControl();
            }
        }

        public override void LoadControl()
        {
            textboxPONumber.Text = SharedMethods.CurrentPONumber;
            buttonEnter_Click(null, EventArgs.Empty);
        }

        void PiecesList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL) { Title = "Complete On-Hand" });
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void PiecesList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL:
                        break;
                }
        }

        void buttonEnter_Click(object sender, System.EventArgs e)
        {
            int itemNo = string.IsNullOrEmpty(textboxLineItemNo.Text) ? -1 : int.Parse(textboxLineItemNo.Text);
            var lineItem = Communication.OnHand.OnHand.Instance.GetOrderItemDetails(SharedMethods.CurrentPickupID, textboxPONumber.Text, itemNo, SharedMethods.CurrentPickup.CustomerCode);
            if (lineItem != null)
            {
                poDetailsItem.DisplayPODetails(lineItem);
                textboxLineItemNo.Text = lineItem.LineItemNumber.ToString();
            }
        }

        #region Event Handlers
        void textboxPONumber_TextChanged(object sender, EventArgs e)
        {
            buttonEnter.Enabled = !string.IsNullOrEmpty(textboxPONumber.Text);
        }

        void poDetailsItem_ButtonApplyClick(object sender, EventArgs e)
        {
            int itemNo;
            if (!poDetailsItem.ShipQuantity.HasValue)
            {
                SharedMethods.ShowFailMessage("Ship quantity is not specified");
                return;
            }
            try
            {
                itemNo = int.Parse(textboxLineItemNo.Text);

            }
            catch (Exception)
            {
                SharedMethods.ShowFailMessage("Item No is not specified");
                return;

            }
            Cursor.Current = Cursors.WaitCursor;
            var status = Communication.OnHand.OnHand.Instance.SavePackageItemQuantity(poDetailsItem.ID, SharedMethods.CurrentPickup.ShipmentID, SharedMethods.CurrentPickupID, this.packageID,
                SharedMethods.CurrentPONumber, poDetailsItem.ShipQuantity.Value, itemNo, SharedMethods.CurrentPickup.CustomerCode);
            Cursor.Current = Cursors.Default;
            if (!status.TransactionStatus1)
            {
                SharedMethods.ShowFailMessage(status.TransactionError);
            }
            else
            {
                if (!string.IsNullOrEmpty(status.TransactionError))
                    UI.CMXMessageBox.Show(status.TransactionError, "Warning!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                LoadControl();
            }
        }

        #endregion
    }
}
