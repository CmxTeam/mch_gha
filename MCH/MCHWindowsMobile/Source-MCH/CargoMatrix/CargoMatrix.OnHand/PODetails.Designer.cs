﻿namespace CargoMatrix.OnHand
{
    partial class PODetails
    {
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textboxPONumber = new CargoMatrix.UI.CMXTextBox();
            this.textboxLineItemNo = new CargoMatrix.UI.CMXTextBox();
            this.labelItemNo = new System.Windows.Forms.Label();
            this.labelSplitter = new System.Windows.Forms.Label();
            this.labelPONo = new System.Windows.Forms.Label();
            //this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.buttonEnter = new CargoMatrix.UI.CMXPictureButton();
            this.Size = new System.Drawing.Size(240, 292);
            this.SuspendLayout();
            // 
            // labelPONo
            // 
            this.labelPONo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPONo.BackColor = System.Drawing.Color.White;
            this.labelPONo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelPONo.Location = new System.Drawing.Point(4, 4);
            this.labelPONo.Name = "labelPONo";
            this.labelPONo.Text = "PO#:";
            this.labelPONo.Size = new System.Drawing.Size(30, 13);
            // 
            // textboxPONumber
            // 
            this.textboxPONumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxPONumber.BackColor = System.Drawing.Color.White;
            this.textboxPONumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.textboxPONumber.Location = new System.Drawing.Point(54, 2);
            this.textboxPONumber.WatermarkText = "Enter PO#";
            this.textboxPONumber.Name = "textboxPONumber";
            this.textboxPONumber.Size = new System.Drawing.Size(138, 13);
            this.textboxPONumber.TextChanged += new System.EventHandler(textboxPONumber_TextChanged);
            this.textboxPONumber.GotFocus += (s, e) => { textboxPONumber.SelectAll(); };
            // 
            // labelItemNo
            // 
            this.labelItemNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelItemNo.BackColor = System.Drawing.Color.White;
            this.labelItemNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelItemNo.Location = new System.Drawing.Point(4, 24);
            this.labelItemNo.Name = "labelItemNo";
            this.labelItemNo.Text = "Item#:";
            this.labelItemNo.Size = new System.Drawing.Size(60, 13);
            // 
            // textboxLineItemNo
            // 
            this.textboxLineItemNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxLineItemNo.BackColor = System.Drawing.Color.White;
            this.textboxLineItemNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.textboxLineItemNo.Location = new System.Drawing.Point(54, 24);
            this.textboxLineItemNo.WatermarkText = "Enter Line Item #";
            this.textboxLineItemNo.Name = "textboxPONumber";
            this.textboxLineItemNo.Size = new System.Drawing.Size(138, 13);
            this.textboxLineItemNo.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.textboxLineItemNo.GotFocus += (s, e) => { textboxLineItemNo.SelectAll(); };
            // 
            // labelSplitter
            // 
            this.labelSplitter.BackColor = System.Drawing.Color.Black;
            this.labelSplitter.Location = new System.Drawing.Point(0, 48);
            this.labelSplitter.Name = "labelSplitter";
            this.labelSplitter.Size = new System.Drawing.Size(240, 1);
            // 
            // buttonDetails
            // 
            //this.buttonDetails.BackColor = System.Drawing.Color.White;
            //this.buttonDetails.Location = new System.Drawing.Point(164, 6);
            //this.buttonDetails.Name = "buttonDetails";
            //this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            //this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            //this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            //this.buttonDetails.Click += new System.EventHandler(buttonDetails_Click);
            //this.buttonDetails.Image = global::Resources.Graphics.Skin.add_btn;
            //this.buttonDetails.PressedImage = global::Resources.Graphics.Skin.add_btn_over;
            // 
            // buttonEnter
            // 
            this.buttonEnter.BackColor = System.Drawing.Color.White;
            this.buttonEnter.Location = new System.Drawing.Point(202, 6);
            this.buttonEnter.Name = "buttonDetails";
            this.buttonEnter.Size = new System.Drawing.Size(32, 32);
            this.buttonEnter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonEnter.TransparentColor = System.Drawing.Color.White;
            this.buttonEnter.Click += new System.EventHandler(buttonEnter_Click);
            this.buttonEnter.Image = Resources.Skin.magnify_btn;
            this.buttonEnter.PressedImage = Resources.Skin.magnify_btn_over;
            this.buttonEnter.DisabledImage = Resources.Skin.magnify_btn_dis;
            // 
            // PiecesList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.panelHeader2.Controls.Add(this.textboxPONumber);
            this.panelHeader2.Controls.Add(this.textboxLineItemNo);
            this.panelHeader2.Controls.Add(this.labelSplitter);
            this.panelHeader2.Controls.Add(this.labelPONo);
            this.panelHeader2.Controls.Add(this.labelItemNo);
            //this.panelHeader2.Controls.Add(this.buttonDetails);
            this.panelHeader2.Controls.Add(this.buttonEnter);
            this.panelHeader2.Height = 72;
            this.Name = "PiecesList";
            this.Location = new System.Drawing.Point(0, 0);
            this.ResumeLayout(false);

        }




        #endregion

        private System.Windows.Forms.Label labelPONo;
        private CargoMatrix.UI.CMXTextBox textboxPONumber;
        private CargoMatrix.UI.CMXTextBox textboxLineItemNo;
        private System.Windows.Forms.Label labelItemNo;
        private System.Windows.Forms.Label labelSplitter;
        //private CargoMatrix.UI.CMXPictureButton buttonDetails;
        private CargoMatrix.UI.CMXPictureButton buttonEnter;
    }
}
