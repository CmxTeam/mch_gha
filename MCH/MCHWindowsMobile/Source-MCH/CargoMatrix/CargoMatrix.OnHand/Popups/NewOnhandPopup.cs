﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.OnHand
{
    public partial class NewOnhandPopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        public string PickupRefNo
        {
            get { return textBoxPickupRef.Text; }
            set { textBoxPickupRef.Text = value; }
        }
        public string Vendor
        {
            get { return textBoxVendor.Text; }
            set { textBoxVendor.Text = value; }
        }
        public string OrderReference
        {
            get { return textBoxOrdRef.Text; }
            set { textBoxOrdRef.Text = value; }
        }
        public string Destination
        {
            get { return textBoxDest.Text; }
            set { textBoxDest.Text = value; }
        }
        public string MOT
        {
            get { return textBoxMOT.Text; }
            set { textBoxMOT.Text = value; }
        }
        public NewOnhandPopup()
        {
            InitializeComponent();
            this.Title = "CargoOnhand - New Receiving Task";
            textBox_TextChanged(null, EventArgs.Empty);
        }

        private void textBox_TextChanged(object sender, System.EventArgs e)
        {
            // disable OK button if texboxes are empty
            // enable OK button if one of the textboxes has text
            bool enableFlag = !string.IsNullOrEmpty(textBoxPickupRef.Text.Trim());

            buttonOk.Enabled = enableFlag;
        }

        void textBoxVendor_TextChanged(object sender, System.EventArgs e)
        {
            buttonVendor.Enabled = textBoxVendor.Text.Trim().Length > 2;
        }
        void buttonVendor_Click(object sender, System.EventArgs e)
        {
            var vndr = SharedMethods.DisplayVendorsList(textBoxVendor.Text);
            if (vndr != null)
            {
                Vendor = vndr.VendorName;
            }
        }

        void buttonMOT_Click(object sender, System.EventArgs e)
        {
            string mot = SharedMethods.DisplayMOTList();
            if (!string.IsNullOrEmpty(mot))
            {
                textBoxMOT.Text = mot;
            }
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            base.buttonOk_Click(sender, e);
        }
    }
}
