﻿namespace CargoMatrix.OnHand
{
    partial class AddEditReference
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRefType = new CargoMatrix.UI.CMXTextBox();
            this.buttonRefTypeList = new CargoMatrix.UI.CMXPictureButton();
            this.textBoxRefNumber = new CargoMatrix.UI.CMXTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.Text = "Reference Type";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(12, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 13);
            this.label2.Text = "Rererence Number";
            // 
            // textBoxRefType
            // 
            this.textBoxRefType.Location = new System.Drawing.Point(12, 56);
            this.textBoxRefType.Name = "textBox1";
            this.textBoxRefType.Size = new System.Drawing.Size(177, 21);
            this.textBoxRefType.TabStop = false;
            this.textBoxRefType.TextChanged += textBox_TextChanged;
            // 
            // buttonRefTypeList
            // 
            this.buttonRefTypeList.Location = new System.Drawing.Point(198, 56);
            this.buttonRefTypeList.Name = "button1";
            this.buttonRefTypeList.Size = new System.Drawing.Size(32, 32);
            this.buttonRefTypeList.TabIndex = 2;
            this.buttonRefTypeList.Text = "buttonRefTypeList";
            this.buttonRefTypeList.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonRefTypeList.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonRefTypeList.Click += new System.EventHandler(buttonRefTypeList_Click);
            // 
            // textBoxRefNumber
            // 
            this.textBoxRefNumber.Location = new System.Drawing.Point(12, 109);
            this.textBoxRefNumber.Name = "textBox2";
            this.textBoxRefNumber.Size = new System.Drawing.Size(177, 21);
            this.textBoxRefNumber.TabIndex = 1;
            this.textBoxRefNumber.TextChanged += textBox_TextChanged;
            // 
            // AddEditReference
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.panelContent.Controls.Add(this.buttonRefTypeList);
            this.panelContent.Controls.Add(this.textBoxRefNumber);
            this.panelContent.Controls.Add(this.textBoxRefType);
            this.panelContent.Controls.Add(this.label2);
            this.panelContent.Controls.Add(this.label1);
            this.Name = "AddEditReference";
            this.panelContent.Size = new System.Drawing.Size(236, 200);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private CargoMatrix.UI.CMXTextBox textBoxRefType;
        private CargoMatrix.UI.CMXPictureButton buttonRefTypeList;
        private CargoMatrix.UI.CMXTextBox textBoxRefNumber;
    }
}
