﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.OnHand;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.OnHand
{
    public partial class AddEditReference : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        private IReference reference;
        public AddEditReference(IReference reference)
        {
            InitializeComponent();
            this.buttonOk.Enabled = false;
            this.Title = reference != null ? "Edit Reference" : "Add New Reference";
            this.Reference = reference;
        }

        public IReference Reference
        {
            get { return reference; }
            private set
            {
                if (value != null)
                {
                    reference = value;
                    this.textBoxRefNumber.Text = value.ReferenceNumber;
                    this.textBoxRefType.Text = value.ReferenceType;
                }
            }
        }

        void buttonRefTypeList_Click(object sender, System.EventArgs e)
        {
            this.Reference = SharedMethods.DisplayReferenceTypes();
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            buttonOk.Enabled = !string.IsNullOrEmpty(textBoxRefType.Text) && !string.IsNullOrEmpty(textBoxRefNumber.Text);
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            if (Reference == null)
                Reference = new CMXReference();
            Reference.ReferenceNumber = textBoxRefNumber.Text;
            DialogResult = DialogResult.OK;
        }

        protected override void buttonCancel_Click(object sender, EventArgs e)
        {
            this.reference = null;
            DialogResult = DialogResult.Cancel;
        }
    }
}
