﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SmoothListbox;
using CargoMatrix.Communication.WSAQM;
using System.Linq;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CargoMatrix.Communication;
using System.Collections.Generic;
using UIInterface;
using CustomUtilities;

namespace CargoMatrix.AQM
{
    public partial class AQMList : SmoothListbox.SmoothListbox
    {
        CustomListItems.HeaderItem headerItem;
        private bool? isMawb = null;
        private IAQMInfo aqmInfo;

        public AQMList()
        {
            InitializeComponent();
            headerItem = new CustomListItems.HeaderItem();
            headerItem.Label3 = AQMResources.Text_ScanEnter;

            headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            headerItem.Location = new Point(0, 0);
            panelHeader2.Visible = true;
            this.panelHeader2.Controls.Add(headerItem);
            headerItem.BringToFront();
            panelHeader2.Height = headerItem.Height;
            headerItem.ButtonVisible = false;
            this.TitleText = "Cargo AQM";

            LoadOptionsMenu += new EventHandler(AQMList_LoadOptionsMenu);
            this.MenuItemClicked += new ListItemClickedHandler(AQMList_MenuItemClicked);
            headerItem.Logo = CustomListItems.CustomListItemsResource.Inspection;
            BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(AQMList_BarcodeReadNotify);
        }

        void AQMList_BarcodeReadNotify(string barcodeData)
        {
            var scan = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (scan.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    isMawb = false;
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    DisplayAvailableAqm(string.Empty, scan.HouseBillNumber);
                    break;
                case CMXBarcode.BarcodeTypes.MasterBill:
                    isMawb = true;
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    DisplayAvailableAqm(scan.CarrierNumber, scan.MasterBillNumber);
                    break;
                default:
                    CargoMatrix.UI.CMXMessageBox.Show(AQMResources.Text_ScanEnter, "Invalid Barcode", CMXMessageBoxIcon.Delete);
                    isMawb = null;
                    break;
            }
            BarcodeEnabled = true;
        }

        public override void LoadControl()
        {
            RefreshList();
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            if (!isMawb.HasValue)
            {
                CargoMatrix.UI.CMXMessageBox.Show(AQMResources.Text_ScanEnter, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return;
            }

            CargoMatrix.Utilities.MessageListBox irregList = new CargoMatrix.Utilities.MessageListBox();
            irregList.OneTouchSelection = true;
            irregList.HeaderText = aqmInfo.Reference;
            irregList.HeaderText2 = "Select Action To Continue";
            irregList.MultiSelectListEnabled = false;

            foreach (var irreg in Communication.AQM.Instance.GetIrregularityCodes(isMawb ?? true))
                irregList.AddItem(new StandardListItem<Reference>(irreg.Name, null, irreg));
            if (DialogResult.OK == irregList.ShowDialog())
            {
                var irregCode = (irregList.SelectedItems[0] as StandardListItem<Reference>).ItemData.Code;
                switch (irregCode)
                {
                    case "14":
                    case "D":
                        if (aqmInfo.Type == AQMType.MAWB)
                            MawbDamageCapture();
                        else
                            HawbDamageCapture();
                        break;
                    case "O":
                    case "99":
                        CustomUtilities.RemarksMessageBox rmrks = new CustomUtilities.RemarksMessageBox("Remark:" + aqmInfo.Reference);
                        rmrks.IsMandatory = true;
                        if (DialogResult.OK == rmrks.ShowDialog())
                        {
                            bool result = false;
                            if (aqmInfo.Type == AQMType.MAWB)
                                result = Communication.AQM.Instance.CreateMawbAQM(aqmInfo.CarrierNo, aqmInfo.AwbNo, irregCode, rmrks.TextEntered);
                            else
                            {
                                int count = 1;
                                if (aqmInfo.TotalPieces > 1)
                                {
                                    CargoMatrix.Utilities.CountMessageBox cnt = new CargoMatrix.Utilities.CountMessageBox();
                                    cnt.HeaderText = "Confirm count";
                                    cnt.LabelDescription = "Enter number of pieces";
                                    //cntMsg.LabelReference = lItem.ItemData.Location;
                                    cnt.PieceCount = aqmInfo.TotalPieces;
                                    if (DialogResult.OK != cnt.ShowDialog())
                                        return;
                                    else count = cnt.PieceCount;
                                }
                                result = Communication.AQM.Instance.CreateHawbAQM(aqmInfo.AwbNo, irregCode, count, string.Empty, rmrks.TextEntered);
                            }

                            if (result)
                                CargoMatrix.UI.CMXMessageBox.Show(AQMResources.Text_AQMCreated, aqmInfo.Reference, CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            LoadControl();
                        }
                        break;
                    default:
                        {
                            bool result = false;
                            if (aqmInfo.Type == AQMType.MAWB)
                            {
                                result = Communication.AQM.Instance.CreateMawbAQM(aqmInfo.CarrierNo, aqmInfo.AwbNo, irregCode, string.Empty);
                            }
                            else
                            {
                                int count = 1;
                                if (aqmInfo.TotalPieces > 1)
                                {
                                    CargoMatrix.Utilities.CountMessageBox cnt = new CargoMatrix.Utilities.CountMessageBox();
                                    cnt.PieceCount = aqmInfo.TotalPieces;
                                    if (DialogResult.OK != cnt.ShowDialog())
                                        return;
                                    else count = cnt.PieceCount;
                                }
                                result = Communication.AQM.Instance.CreateHawbAQM(aqmInfo.AwbNo, irregCode, count, string.Empty, string.Empty);
                            }
                            if (result)
                                CargoMatrix.UI.CMXMessageBox.Show(AQMResources.Text_AQMCreated, aqmInfo.Reference, CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            LoadControl();
                        }
                        break;
                }
            }
        }

        void AQMList_MenuItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var option = listItem as CustomListItems.OptionsListITem;
            switch (option.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                    LoadControl();
                    break;
                case CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO:
                    AcquireMawbHawbDetails();
                    break;
                default:
                    break;
            }
        }

        private void AcquireMawbHawbDetails()
        {
            string carrier, mawb, hawb;
            if (DialogResult.OK == ManualHawbMawb.Show("Enter Manually", AQMResources.Text_ProvideDetails, out carrier, out mawb, out hawb))
            {
                if (hawb != string.Empty)
                {
                    isMawb = false;
                    DisplayAvailableAqm(string.Empty, hawb);
                }
                else
                {
                    isMawb = true;
                    DisplayAvailableAqm(carrier, mawb);
                }
            }
        }

        void AQMList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO));
        }

        private void RefreshList()
        {
            if (aqmInfo == null)
                return;

            if (aqmInfo.Type == AQMType.MAWB)
                DisplayAvailableAqm(aqmInfo.CarrierNo, aqmInfo.AwbNo);
            else
                DisplayAvailableAqm(string.Empty, aqmInfo.AwbNo);

        }

        private void DisplayAvailableAqm(string carrier, string number)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (isMawb.HasValue && isMawb.Value == true)
            {
                aqmInfo = CargoMatrix.Communication.AQM.Instance.GetAQMList(carrier, number, AQMType.MAWB);
                if (aqmInfo == null)
                {
                    Cursor.Current = Cursors.Default;
                    CargoMatrix.UI.CMXMessageBox.Show(AQMResources.Text_MawbNotFound, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    return;
                }
            }
            else
            {
                aqmInfo = CargoMatrix.Communication.AQM.Instance.GetAQMList(string.Empty, number, AQMType.HAWB);
                if (aqmInfo == null)
                {
                    Cursor.Current = Cursors.Default;
                    CargoMatrix.UI.CMXMessageBox.Show(AQMResources.Text_HawbNotFound, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    return;
                }
            }

            headerItem.Label3 = AQMResources.Text_AddAqm;
            headerItem.Label1 = aqmInfo.Reference;
            headerItem.Label2 = string.Format("Pcs: {0} Wgt: {1}", aqmInfo.TotalPieces, aqmInfo.Weight);
            headerItem.ButtonVisible = true;
            smoothListBoxMainList.RemoveAll();

            foreach (var aqm in aqmInfo.Queues)
            {
                List<CMXString> labels = new List<CMXString>()
                {
                    new CMXString(string.Format("{0} {1}-{2}-{3} {4}",aqm.AqmOrigin, aqm.AqmFrom, aqm.AqmNo, aqm.AqmDestination,aqm.AqmTo),true),
                    new CMXString(string.Format("{0} - {1}", (aqm.Color??string.Empty).ToUpper(), (aqm.IrregularityCode??string.Empty).ToUpper()),false),
                    new CMXString(string.Format("Pcs: {0} - {1}",aqm.ExceptionPieces, (aqm.ExceptionCode??string.Empty).ToUpper()),false),
                    new CMXString(string.Format("By: {0}  {1}", (aqm.UserID??string.Empty).ToUpper(), aqm.RecDate.ToString("MM/dd/yy hh:mm tt")),false),
                };
                Image icon = aqm.IsAutoClose ? CargoMatrix.Resources.Icons.Shipping_Box_Security : CargoMatrix.Resources.Icons.Shipping_Box_Progress;
                smoothListBoxMainList.AddItem2(new SmoothListbox.ListItems.DynamicListItem(labels, aqm, icon));
            }

            smoothListBoxMainList.LayoutItems();
            smoothListBoxMainList.RefreshScroll();
            Cursor.Current = Cursors.Default;
        }

        void MawbDamageCapture()
        {
            Cursor.Current = Cursors.WaitCursor;

            MawbItemAsHousebill masterAsHouseBill = new MawbItemAsHousebill(aqmInfo as MasterBillAqmDetail);

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(masterAsHouseBill, DamageCapture.DamageCaptureApplyTypeEnum.MasterBill);

            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionsSummarys(0);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.ScannerUtility.Instance.UpdateMasterBillConditions(aqmInfo.CarrierNo,aqmInfo.AwbNo, args.ShipmentConditions);
            };

            CMXAnimationmanager.DisplayForm(damageCapture);
            Cursor.Current = Cursors.Default;
        }

        void HawbDamageCapture()
        {
            Cursor.Current = Cursors.WaitCursor;

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(new AQMIHousebill(aqmInfo as HouseBillAqmDetail));

            damageCapture.OnLoadShipmentConditions += (sender, args) =>
            {
                args.ShipmentConditions = ScannerUtility.Instance.GetHouseBillShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sender, args) =>
            {
                args.ConditionsSummaries = ScannerUtility.Instance.GetHouseBillShipmentConditionSummary(args.HouseBillID);

            };


            damageCapture.OnUpdateShipmentConditions += (sender, args) =>
            {
                args.Result = ScannerUtility.Instance.UpdateHouseBillShipmentConditions(args.HouseBillId, args.ScanModes, args.ShipmentConditions, 0, CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.AQM, string.Empty, args.Slac);
            };

            //damageCapture.GetHouseBillPiecesIDs = () =>
            //{
            //    return new int[0];
            //};
            CMXAnimationmanager.DisplayForm(damageCapture);

            Cursor.Current = Cursors.Default;
        }
    }
}