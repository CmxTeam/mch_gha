﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.AQM
{
    public partial class ManualHawbMawb : CargoMatrix.UI.MessageBoxBase
    {
        private string title;

        private static ManualHawbMawb instance = null;

        public ManualHawbMawb()
        {
            InitializeComponent();
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = global::Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = global::Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = global::Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = global::Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = global::Resources.Graphics.Skin.nav_ok;
            this.buttonOk.DisabledImage = global::Resources.Graphics.Skin.nav_ok_dis;
            this.pictureBox1.Image = global::Resources.Graphics.Skin.nav_bg;
            this.buttonOk.Enabled = false;
        }


        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(title, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public static DialogResult Show(string title,string message, out string carrier, out string mawb, out string hawb)
        {
            if (instance == null)
                instance = new ManualHawbMawb();

            instance.textBoxHawb.Text = string.Empty;
            instance.textBoxConsol.Text = string.Empty;
            instance.textBoxCarrier.Text = string.Empty;
            instance.textBoxHawb.Enabled = true;
            instance.textBoxConsol.Enabled = true;
            instance.textBoxCarrier.Enabled = true;

            instance.title = title;
            instance.message.Text = message;
            return instance.CMXShowDialog(out carrier, out mawb, out hawb);
        }

        private DialogResult CMXShowDialog(out string carrier, out string mawb, out string hawb)
        {
            DialogResult result = ShowDialog();
            carrier = textBoxCarrier.Text;
            mawb = textBoxConsol.Text;
            hawb = textBoxHawb.Text;
            return result;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void textBoxMmawb_TextChanged(object sender, EventArgs e)
        {
            if (textBoxCarrier.Text == string.Empty && textBoxConsol.Text == string.Empty)
            {
                textBoxHawb.Enabled = true;
                buttonOk.Enabled = false;
            }
            else
            {
                textBoxHawb.Enabled = false;
                if (textBoxCarrier.Text != string.Empty && textBoxConsol.Text != string.Empty)
                    buttonOk.Enabled = true;
            }
        }

        private void textBoxHawb_TextChanged(object sender, EventArgs e)
        {
            bool flag = textBoxHawb.Text == string.Empty;
            textBoxCarrier.Enabled = textBoxConsol.Enabled = flag;
            buttonOk.Enabled = !flag;
        }
    }
}
