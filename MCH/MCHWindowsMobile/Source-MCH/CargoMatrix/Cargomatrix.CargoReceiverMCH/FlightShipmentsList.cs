﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using System.Collections.Generic;
namespace CargoMatrix.CargoReceiver
{
    public partial class FlightShipmentsList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;

        
        private FlightItem flightDetailsItem;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
        private int listItemHeight;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        //CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;
        //private long manifestId;

        //private string selectedUld = string.Empty;
        public FlightShipmentsList(FlightItem flightDetailsItem)
        {
            //this.selectedUld = selectedUld;
            this.flightDetailsItem = flightDetailsItem;
      
             
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_ListItemClicked);
        }


        void ValidateShipment(string shipment)
        {
            CargoMatrix.Communication.WSCargoReceiverMCHService.ScannedShipmentInfo[] obj = CargoMatrix.Communication.WebServiceManager.Instance().ScanShipmentMCH(flightDetailsItem.TaskId, shipment);
            

          
            List<string> ulds = new List<string>();
            try
            {
                foreach (CargoMatrix.Communication.WSCargoReceiverMCHService.ScannedShipmentInfo o in obj)
                {
                   
                    if (o.Uld.UldPrefix.ToUpper() == "LOOSE")
                    {
                        ulds.Add(o.Uld.UldPrefix);
                    }
                    else
                    {
                        ulds.Add(o.Uld.UldPrefix + o.Uld.UldSerialNo);
                    }

                }
            }
            catch(Exception ex)
            {
                if(!ulds.Contains("Loose"))
                {
                        ulds.Add("Loose");
                }
        
                string e = ex.Message;
            }

            string uld = string.Empty;
            if (ulds.Count == 1)
            {
                uld = ulds[0];
            }
            else if (ulds.Count > 1)
            {
                uld = SelectUld(ulds);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Unable to locate details for this shipment. Check if this shipment exists in system.", "Shipment details not found!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }


            if (uld != string.Empty)
            {
                FlightReceiver.scannedUld = uld;
                CargoMatrix.UI.CMXAnimationmanager.GoBack();
            }

        }
        protected virtual void UldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
        
            ShipmentCargoItem tempShipmentCargoItem = (ShipmentCargoItem)listItem;
            ShipmentItem tempShipmetItem = (ShipmentItem)tempShipmentCargoItem.ItemData;
            //AddShipmentIntoForkLift(tempShipmetItem);


      
            ValidateShipment(tempShipmetItem.AWB);

      }



        string SelectUld(List<string> ulds)
        {

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Select Ulds";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoReceiver";


            actPopup.RemoveAllItems();
            for (int i = 0; i < ulds.Count ; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(ulds[i], null, i + 1));
            }

            
             


            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
                    return actPopup.SelectedItems[0].Name;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }



        }
 
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("buttonFilter_Click");
        }

        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

 

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
 
            //this.label1.Text = string.Format("{0}", filter);
 

            this.label1.Text =string.Format("{0}  {1}  {2}", this.flightDetailsItem.Origin, this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
            this.label2.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.RecoveredULDs, this.flightDetailsItem.ULDCount);
            this.label3.Text = string.Format("PCS: {0} of {1}", this.flightDetailsItem.ReceivedPieces, this.flightDetailsItem.TotalPieces);

            switch (this.flightDetailsItem.Status)
            {
                case FlightStatus.Open:
                case FlightStatus.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case FlightStatus.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case FlightStatus.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;
                default:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
            }


            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;


            CargoMatrix.Communication.DTO.ShipmentItem[] shipmentItemsArray = CargoMatrix.Communication.WebServiceManager.Instance().GetFlightViewMCH(flightDetailsItem.TaskId, flightDetailsItem.FlightManifestId);


                     if (shipmentItemsArray!=null)
                     {
                         var rItems = from i in shipmentItemsArray
                                      select InitializeItem(i);
                         smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                     }


                     string flight = string.Format("{0}  {1}  {2}", flightDetailsItem.Origin, flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber);

                     this.TitleText = string.Format("CargoReceiver - {0} ({1})", flight, shipmentItemsArray.Length);
         
            Cursor.Current = Cursors.Default;
        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);


            string flight = string.Format("{0}  {1}  {2}", flightDetailsItem.Origin, flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber);


            this.TitleText = string.Format("CargoReceiver - {0} ({1})", flight, smoothListBoxMainList.VisibleItemsCount);
      
        }

        protected ICustomRenderItem InitializeItem(ShipmentItem item)
        {
            ShipmentCargoItem tempShipment = new ShipmentCargoItem(item);
            tempShipment.OnEnterClick += new EventHandler(ULD_Enter_Click);
            tempShipment.ButtonClick += new EventHandler(ULD_OnMoreClick);
            listItemHeight = tempShipment.Height;
            return tempShipment;
        }



        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }


        bool DoPieces(string reference, int remaningPieces, out int enteredPieces, bool allowOverage)
        {

            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.PieceCount = remaningPieces;
            CntMsg.LabelReference = reference;
            CntMsg.AllowOverage = allowOverage;
            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                enteredPieces = CntMsg.PieceCount;
                return true;
            }
            else
            {
                enteredPieces = 0;
                return false;
            }

        }

        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {


            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + "-" + hawbNo + "-" + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }




        void ULD_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            Cursor.Current = Cursors.Default;
            MessageBox.Show("Feature not available " + (sender as ShipmentCargoItem).ItemData.AWB);

        }
        //void ULD_OnMoreClick(object sender, EventArgs e)
        //{
        //    //Cursor.Current = Cursors.WaitCursor;
             
        //    //Cursor.Current = Cursors.Default;
        //    //MessageBox.Show("OnMoreClick" + (sender as ShipmentCargoItem).ItemData.AWB);


        //    ShipmentItem item = (sender as ShipmentCargoItem).ItemData;


        //    CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();

        //    actPopup.OneTouchSelection = true;
        //    actPopup.OkEnabled = false;
        //    actPopup.MultiSelectListEnabled = false;

        //    actPopup.HeaderText = "Relocate Shipment";
        //    actPopup.HeaderText2 = "Select a location";
        //    actPopup.RemoveAllItems();

        //    string[] locations = item.Locations.Split(',');


        //    for (int i = 0; i < locations.Length; i++)
        //    {
        //        actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(locations[i].Trim(), CargoMatrix.Resources.Skin.button_map, i + 1));
        //    }




        //    if (DialogResult.OK == actPopup.ShowDialog())
        //    {
        //        //TODO: Get the number of pcs for this location
        //        string oldlocation = actPopup.SelectedItems[0].Name;
        //        CargoMatrix.Communication.WSCargoReceiverMCHService.ValidatedShipment obj = CargoMatrix.Communication.WebServiceManager.Instance().ValidateShipmentMCH(flightDetailsItem.TaskId, uldDetailsItem.UldId, item.AWB);
        //        // long oldlocationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(oldlocation);


        //        long oldlocationId = (int)CargoMatrix.Communication.WebServiceManager.Instance().GetLocationId(oldlocation);

        //        int pcs = CargoMatrix.Communication.WebServiceManager.Instance().GetAwbPiecesCountInLocation(obj.AwbId, oldlocationId);

        //        int remaningPieces = pcs; // item.ReceivedPieces;
        //        int enteredPieces = 0;
        //        if (DoPieces(item.AWB, remaningPieces, out enteredPieces, false))
        //        {

        //            BarcodeEnabled = false;
        //            string enteredLocation = string.Empty;
        //            bool isFPC = false;
        //            if (DoLocation(out enteredLocation, out isFPC, item.AWB, true))
        //            {

        //                long newlocationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
        //                if (newlocationId == 0)
        //                {
        //                    CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //                    BarcodeEnabled = true;
        //                    return;
        //                }

        //                CargoMatrix.Communication.WebServiceManager.Instance().RelocateAwbPieces(obj.AwbId, (int)oldlocationId, (int)newlocationId, enteredPieces, flightDetailsItem.TaskId);

        //                //TODO: Funtion to relocate here 
        //                if (isFPC)
        //                {
        //                    ShowFreightPhotoCapture(item.AWB, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), enteredPieces, "CargoReceiver");

        //                }
        //                LoadControl();

        //                return;
        //            }
        //            BarcodeEnabled = true;


        //        }


        //    }

        //}
 
  
        protected virtual void ULD_Enter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            ShipmentItem shipment = (sender as ShipmentCargoItem).ItemData;
            ProceedWithUld(shipment);
            Cursor.Current = Cursors.Default;
        }


   
 
 
        private bool ProceedWithUld(ShipmentItem tempShipment)
        {
            //if (tempShipment == null)
            //    return false;

            //MessageBox.Show("ProceedWithUld" + tempShipment.AWB);
            ////CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightReceiver(tempFlight));
            //Cursor.Current = Cursors.Default;
            return true;
        }



        void MawbList_BarcodeReadNotify(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            ValidateShipment(barcodeData);
          
            Cursor.Current = Cursors.Default;
         
            //if (!BarcodeEnabled)
            //{
                BarcodeEnabled = true;
            //}


        }
    }

}
