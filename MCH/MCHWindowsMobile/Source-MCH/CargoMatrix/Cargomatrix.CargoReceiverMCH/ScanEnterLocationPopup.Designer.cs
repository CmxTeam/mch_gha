﻿namespace CargoMatrix.CargoReceiver
{
    partial class ScanEnterLocationPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCamera = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.textBox1 = new CargoMatrix.UI.CMXTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.labelText = new System.Windows.Forms.Label();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBoxNavBg = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.White;
            this.buttonOk.Enabled = false;
            this.buttonOk.Location = new System.Drawing.Point(13, 96);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonOk.TransparentColor = System.Drawing.Color.White;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(163, 96);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonCancel.TransparentColor = System.Drawing.Color.White;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonCamera
            // 
            this.buttonCamera.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCamera.BackColor = System.Drawing.Color.White;
            this.buttonCamera.Location = new System.Drawing.Point(89, 96);
            this.buttonCamera.Name = "buttonCancel";
            this.buttonCamera.Size = new System.Drawing.Size(52, 37);
            this.buttonCamera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonCamera.TransparentColor = System.Drawing.Color.White;
            this.buttonCamera.Click += new System.EventHandler(this.buttonOkFPC_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBox1.Location = new System.Drawing.Point(10, 44);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(171, 28);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Controls.Add(this.labelText);
            this.panel1.Controls.Add(this.buttonBrowse);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCamera);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.pictureBoxNavBg);
            this.panel1.Location = new System.Drawing.Point(5, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 136);
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(230, 20);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // labelText
            // 
            this.labelText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelText.Location = new System.Drawing.Point(10, 23);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(210, 18);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowse.BackColor = System.Drawing.Color.White;
            this.buttonBrowse.Location = new System.Drawing.Point(185, 44);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(40, 28);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // pictureBoxNavBg
            // 
            this.pictureBoxNavBg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBoxNavBg.Location = new System.Drawing.Point(0, 93);
            this.pictureBoxNavBg.Name = "pictureBoxNavBg";
            this.pictureBoxNavBg.Size = new System.Drawing.Size(230, 43);
            this.pictureBoxNavBg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // ScanEnterPopup2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "ScanEnterPopup2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ScanEnterPopup_KeyDown);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CargoMatrix.UI.CMXPictureButton buttonCamera;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private CargoMatrix.UI.CMXTextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.PictureBox pictureBoxNavBg;
        private CargoMatrix.UI.CMXPictureButton buttonBrowse;
    }
}