﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;

namespace CargoMatrix.CargoReceiver
{
    public partial class UldReceiver : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;
        private CargoMatrix.Communication.WSCargoReceiverMCHService.Uld UldData;
        private long UldId;
        FlightItem flightDetailsItem;
        UldItem uldDetailsItem;
        private int ForkLiftCount=0;
        //UldDetails uldScreenDetailsItem;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        protected MessageListBox MawbOptionsList;

        public UldReceiver( FlightItem flightDetailsItem, UldItem uldDetailsItem)
        {
            this.uldDetailsItem = uldDetailsItem;
            this.flightDetailsItem = flightDetailsItem;
            this.UldId = uldDetailsItem.UldId;
            this.Visible = false;

            InitializeComponent();
            
            searchBox.WatermarkText = "Scan/Enter shipment";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            DoBarcodeEnabled(false);

    
        }

 
        //void buttonFilter_Click(object sender, System.EventArgs e)
        //{

        //    //CargoMatrix.UI.CMXAnimationmanager.GoBack();
        

        //    if (ForkLiftCount == 0)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //    }
        //    else
        //    {
        //        ViewForkLift();
        //    }
       
        //}

        void buttonScannedList_Click(object sender, System.EventArgs e)
        {

            if (ForkLiftCount == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else
            {
                ViewForkLift();
            }

        }


        void buttonSearch_Click(object sender, System.EventArgs e)
        {
            DoBarcodeEnabled(false);
            ValidateShipment(searchBox.Text);
            DoBarcodeEnabled(true);
        }

        
            void UldViewButton_Click(object sender, EventArgs e)
            {
                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new ShipmentsList(flightDetailsItem, uldDetailsItem ));
            }
        void ProceedButton_Click(object sender, EventArgs e)
        {
            DoBarcodeEnabled(false);
            if (ForkLiftCount == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else
            {

                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop all pieces into a location?", "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                    string enteredLocation = string.Empty;
                    string reference = string.Format("{0} {1} {2}", flightDetailsItem.Origin, flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber);
                    bool isFPC = false;
                     
                    if (DoLocation(out enteredLocation, out isFPC, reference, true))
                    {
                        DropForkLift(enteredLocation);

                        if (isFPC)
                        {
                            ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), 1, "CargoReceiver");
                        }


                    }
                }

               
                

            }
            DoBarcodeEnabled(true);
        }
        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

 
        public override void LoadControl()
        {


            Cursor.Current = Cursors.WaitCursor;
            this.SuspendLayout();

            DoBarcodeEnabled(false);
            DoBarcodeEnabled(true);

            UldData = CargoMatrix.Communication.WebServiceManager.Instance().GetFlightULDMCH(UldId);

            int userid = CargoMatrix.Communication.WebServiceManager.UserID();

            ForkLiftCount = CargoMatrix.Communication.WebServiceManager.Instance().GetForkliftCountMCH((long)userid, flightDetailsItem.TaskId);
             

            this.label1.Text = string.Format("{0} {1} {2}", flightDetailsItem.Origin, flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber);

            if (UldData.UldType.ToUpper() == "LOOSE")
            {
                this.label2.Text = UldData.UldType;
            }
            else
            {
                this.label2.Text = UldData.UldPrefix + UldData.UldSerialNo;
            }

            this.label3.Text = string.Format("PCS: {0} of {1}", UldData.ReceivedPieces, UldData.TotalPieces);
            this.label4.Text = string.Format("LOC: {0}",  UldData.Location);

            this.buttonScannedList.Value = ForkLiftCount;
 

            switch (this.flightDetailsItem.Status)
            {
                case FlightStatus.Open:
                case FlightStatus.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case FlightStatus.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case FlightStatus.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;
                default:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
            }

            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            
         

 
            this.TitleText =  "CargoReceiver";
 
          
            this.pictureBox2.Image = global::Resources.Graphics.Skin.nav_bg;
            
            ProceedButton.BackgroundImage = Resources.Skin.button;
            ProceedButton.ActiveBackgroundImage = Resources.Skin.button_over;

            UldButton.BackgroundImage = Resources.Skin.button;
            UldButton.ActiveBackgroundImage = Resources.Skin.button_over;

 
            this.searchBox.Visible = true;
            this.label6.Visible = true;
            this.ProceedButton.Visible = true;
            this.UldButton.Visible = true;
            this.buttonScannedList.Visible = true;

            this.Refresh();
            this.ResumeLayout(false);
            this.Visible = true;
            Cursor.Current = Cursors.Default;
            
        }

        private void DoBarcodeEnabled(bool enable)
        {
            if (enable)
            {
                if (!BarcodeEnabled)
                {
                    BarcodeEnabled = false;
                    BarcodeEnabled = true;
                }
            }
            else
            {
                BarcodeEnabled = false;
            }
        }



        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            
        }


        protected void searchBox_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
 
                    e.Handled = true;
                    break;
                case Keys.Enter:
                    ValidateShipment(searchBox.Text);
                    e.Handled = true;
                    break;

                case Keys.Up:
          
                    e.Handled = true;
                    break;
            }



        }

 
        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            DoBarcodeEnabled(false);
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                }
             
            DoBarcodeEnabled(false);
            DoBarcodeEnabled(true);
        }
  

        void MawbList_BarcodeReadNotify(string barcodeData)
        {


            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
            {
                if (ForkLiftCount == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    DoBarcodeEnabled(false);
                }
                else
                {

                    if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop all pieces into this location?", "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        LoadControl();
                        return;
                    }

                    DropForkLift(scanItem.Location);
                }
            }
            else
            {
                ValidateShipment(barcodeData);

            }

           

            Cursor.Current = Cursors.Default;
            DoBarcodeEnabled(true);
        }

      
        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {
             

            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + "-" + hawbNo + "-" + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }

        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }



        void ValidateShipment(string shipmentNumber)
        {

            CargoMatrix.Communication.WSCargoReceiverMCHService.ValidatedShipment obj = CargoMatrix.Communication.WebServiceManager.Instance().ValidateShipmentMCH(flightDetailsItem.TaskId, uldDetailsItem.UldId, shipmentNumber);
            if (obj == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Unable to validare shipment.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                LoadControl();
                return;
            }

            if (obj.TransactionStatus.Status == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(obj.TransactionStatus.Error, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                LoadControl();
                return;
            }

            //bool isHawb = true;
            //string reference = string.Empty;
            //if (obj.Hwb != null)
            //{
            //    reference = obj.Hwb;
            //}

            //if (reference == string.Empty)
            //{
            //    reference = obj.Awb;
            //    isHawb = false;
            //}

            //TODO: GETAWBALERTS
            //foreach (CargoMatrix.Communication.WSCargoReceiverMCHService.Alert item in obj.Alerts)
            //{
            //    CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + reference, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            //}

            int remaningPieces = obj.AvailablePieces;
            //int remaningPieces = obj.TotalPieces - (obj.ForkliftPieces + obj.ReceivedPieces);
            if (remaningPieces < 0)
            {
                remaningPieces = 0;
            }


            int enteredPieces = 0;
            if (DoPieces(obj.Awb, remaningPieces, out enteredPieces, true))
            {

                if (enteredPieces == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Invalid number of pieces.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    LoadControl();
                    return;
                }

                if (enteredPieces > remaningPieces)
                {
                    if (DialogResult.Cancel == CargoMatrix.UI.CMXMessageBox.Show("The number of pieces you have entered is more than the available number pieces for this shipment. Are you sure you want to report an overage?", "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        LoadControl();
                        return;
                    }
                }

                CargoMatrix.Communication.WSCargoReceiverMCHService.TransactionStatus t = null;
                t = CargoMatrix.Communication.WebServiceManager.Instance().AddForkliftPiecesMCH(obj.DetailId, flightDetailsItem.TaskId, enteredPieces);
 
                if (t == null)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Unable to add shipment to forklift.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    LoadControl();
                    return;
                }

                if (t.Status == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(t.Error, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    LoadControl();
                    return;
                }

                this.label6.Text = "Last Scanned: " + obj.Awb;
                LoadControl();
                return;

            }
            else
            {
                LoadControl();
                return;
            }


        }

        //void ValidateShipmentOld(string shipment)
        //{

        //    CargoMatrix.Communication.MchScannerService.ValidatedShipment obj = CargoMatrix.Communication.WebServiceManager.Instance().ValidateShipmentMCH(flightDetailsItem.TaskId, uldDetailsItem.UldId, shipment);
        //    if (obj == null)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("Unable to validare shipment.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //        LoadControl();
        //        return;
        //    }

        //    if (obj.TransactionStatus.Status == false)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show(obj.TransactionStatus.Error, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //        LoadControl();
        //        return;
        //    }

        //    bool isHawb = true;
        //    string reference = string.Empty;
        //    if (obj.Hwb != null)
        //    {
        //        reference = obj.Hwb;
        //    }

        //    if (reference == string.Empty)
        //    {
        //        reference = obj.Awb;
        //        isHawb = false;
        //    }


        //    if (obj.AvailablePieces == 0)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("There are no available pieces.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //        LoadControl();
        //        return;
        //    }

        //    foreach (CargoMatrix.Communication.MchScannerService.Alert item in obj.Alerts)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + reference, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        //    }

        //    int remaningPieces = obj.AvailablePieces;
        //    int enteredPieces = 0;
        //    if (DoPieces(reference, remaningPieces, out enteredPieces))
        //    {

        //        if (enteredPieces == 0 || enteredPieces > obj.AvailablePieces)
        //        {
        //            CargoMatrix.UI.CMXMessageBox.Show("Invalid number of pieces.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //            LoadControl();
        //            return;
        //        }

        //        CargoMatrix.Communication.MchScannerService.TransactionStatus t = null;

        //        t = CargoMatrix.Communication.WebServiceManager.Instance().AddForkliftPiecesMCH(obj.DetailsId, flightDetailsItem.TaskId, enteredPieces, isHawb);



        //        if (t == null)
        //        {
        //            CargoMatrix.UI.CMXMessageBox.Show("Unable to add shipment to forklift.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //            LoadControl();
        //            return;
        //        }

        //        if (t.Status == false)
        //        {
        //            CargoMatrix.UI.CMXMessageBox.Show(t.Error, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //            LoadControl();
        //            return;
        //        }

        //        this.label6.Text = "Last Scanned: " + shipment;
        //        LoadControl();
        //        return;

        //    }
        //    else
        //    {
        //        LoadControl();
        //        return;
        //    }


        //}

        
        bool DoPieces(string reference, int remaningPieces, out int enteredPieces,bool allowOverage)
        {

            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.PieceCount = remaningPieces;
            CntMsg.LabelReference = reference;
            CntMsg.AllowOverage = allowOverage;
            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                enteredPieces = CntMsg.PieceCount;
                return true;
            }
            else
            {
                enteredPieces = 0;
                return false;
            }

        }


        void DropForkLift(string location)
        {
            long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(location);
            if (locationId == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                LoadControl();
                return;
            }
            CargoMatrix.Communication.WebServiceManager.Instance().DropForkliftPiecesMCH(flightDetailsItem.TaskId ,(int) locationId);
            LoadControl();
        }

        void ViewForkLift()
        {
    
           //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new ForkLiftList(flightDetailsItem));

           BarcodeEnabled = false;
           DialogResult dr = ForkLiftViewer.Instance.ShowDialog(flightDetailsItem);
 
           BarcodeEnabled = true;
           LoadControl();

        }


       

    }

}
