﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXBarcode;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSCargoReceiver;
using CMXExtensions;
using System.Reflection;

namespace CargoMatrix.CargoReceiver
{
    public partial class FlightReceiver : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        public object m_activeApp;

        FlightItem flightDetailsItem;
        FlightDetails flightScreenDetailsItem;
        public static string scannedUld=string.Empty;
        public static bool wasFinalized = false;

        //CustomListItems.OptionsListITem mawbListOption;
        //CustomListItems.OptionsListITem hawbListOption;
        public FlightReceiver(FlightItem flight)
        {
            flightDetailsItem = flight;


            InitializeComponent();
            this.LoadOptionsMenu += new EventHandler(CargoReceiver_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(CargoReceiver_MenuItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeReadNotify_Domestic);
        }
        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            base.pictureBoxMenu_Click(sender, e);
            if (m_bOptionsMenu)
            {
                 
            }

        }
        private void InitializeComponentHelper()
        {
            panelHeader2.Height = 231;
            this.Scrollable = false;
        }



        public override void LoadControl()
        {

            if (wasFinalized)
            {
                wasFinalized = false;
                CargoMatrix.UI.CMXAnimationmanager.GoBack();
                return;
            }


            this.TitleText =  "CargoReceiver";
            DisplayMawbDetails();
            BarcodeEnabled = false;
            BarcodeEnabled = true;


            if (scannedUld != string.Empty)
            {
                DoProceed(scannedUld);
                scannedUld = string.Empty;
            }
        }

        private void ClearMawbDetails()
        {
            if (flightScreenDetailsItem != null)
                flightScreenDetailsItem.Visible = false;
        }

        private void DisplayMawbDetails()
        {

            flightDetailsItem = CargoMatrix.Communication.WebServiceManager.Instance().GetFlightMCH(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);

            this.TitleText = "CargoReceiver";
            BarcodeEnabled = false;

            if (flightScreenDetailsItem == null)
            {
                flightScreenDetailsItem = new FlightDetails(flightDetailsItem) { Top = 20 };
                flightScreenDetailsItem.ProceedButton.Click += new EventHandler(ProceedButton_Click);
                flightScreenDetailsItem.buttonDetails.Click += new EventHandler(buttonDetails_Click);
                this.panelHeader2.Controls.Add(flightScreenDetailsItem);
            }
            else
            {
                flightScreenDetailsItem.flightDetailsItem = flightDetailsItem;
            }

            if (flightDetailsItem.Status == FlightStatus.Open || flightDetailsItem.Status == FlightStatus.Pending)
            {
                topLabel.Text = "CONFIRM RECOVERY";
                flightScreenDetailsItem.ProceedButton.Text = "RECOVER";
            }
            else
            {
                topLabel.Text = "CONTINUE RECOVERY";
                flightScreenDetailsItem.ProceedButton.Text = "CONTINUE";
            }

flightScreenDetailsItem.DisplayMasterbill();

            if (this.flightDetailsItem.Status != FlightStatus.Completed)
            {
                CargoMatrix.Communication.WSCargoReceiverMCHService.FlightProgress progress = CargoMatrix.Communication.WebServiceManager.Instance().GetFlightProgress(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);

                if (progress.Progress >= 100)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("All pieces where scanned. Do you want to finalize this task?", "Finalize", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
        
                        FinalizeTask();
                    }
                }
            }

            
        }

        void FinalizeTask()
        {

            if (!CargoMatrix.Communication.WebServiceManager.Instance().FinalizeReceiverMCH(flightDetailsItem.TaskId, flightDetailsItem.FlightManifestId))
            {
                CargoMatrix.UI.CMXMessageBox.Show("Unable to finalize this task.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                LoadControl();
            }
            else
            {
                CargoMatrix.UI.CMXAnimationmanager.GoBack();
            }
        
         
        }


        string GetOverageReason()
        { 
     
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Overage Reasons";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoReceiver";


            actPopup.RemoveAllItems();
        
            actPopup.AddItem(  new SmoothListbox.ListItems.StandardListItem("SPLIT", null, 1));
             actPopup.AddItem(  new SmoothListbox.ListItems.StandardListItem("SHORTAGE", null, 2));
                actPopup.AddItem(  new SmoothListbox.ListItems.StandardListItem("SPLIT/SHORTAGE", null, 3));
            

                 try
                 {
                             if (DialogResult.OK == actPopup.ShowDialog())
            {
                return  actPopup.SelectedItems[0].Name;
            }
            else
            {
                return string.Empty;
            }
                 }
            catch
                 {
            return string.Empty;
            }

  
        
        }



        void CargoReceiver_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)

                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZETASK:

                        if (this.flightDetailsItem.Status != FlightStatus.Completed)
                        {
                       
                          CargoMatrix.Communication.WSCargoReceiverMCHService.FlightProgress progress =   CargoMatrix.Communication.WebServiceManager.Instance().GetFlightProgress(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);


                          if (progress.Progress<100)
                            {
                                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Not all pieces were scanned. Are you sure you want to report a shortage?", "Finalize", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                                {
                                    string overageReason = GetOverageReason();
                                    FinalizeTask();
                                    if (overageReason != string.Empty)
                                    { 

                                    }
                                    return;
                                }
                            }
                            else
                            {
                                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to finalize this task?", "Finalize", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                                {
                                    FinalizeTask();
                                    return;
                                }
                            }
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("This flight was already finalized.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        }
                        
                        
                        
             
                    
                        LoadControl();
                        break;
                    //case CustomListItems.OptionsListITem.OptionItemID.VIEW_MAWB_LIST:
                    //    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightList());
                    //    break;
                    //case CustomListItems.OptionsListITem.OptionItemID.VIEW_HAWB_LIST:
                    //    //new HawbListMessageBox(Forklift.Instance.Mawb.Reference(), Forklift.Instance.Mawb.TaskId).ShowDialog();
                    //    break;
                }
        }

        void CargoReceiver_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZETASK));
            //AddOptionsListItem(this.UtilitesOptionsListItem);
        }


        string SelectUld(List<string> ulds)
        {

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Select Ulds";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoReceiver";


            actPopup.RemoveAllItems();
            for (int i = 0; i < ulds.Count; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(ulds[i], null, i + 1));
            }





            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
                    return actPopup.SelectedItems[0].Name;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }



        }
 


        void ValidateShipment(string shipment)
        {
            CargoMatrix.Communication.WSCargoReceiverMCHService.ScannedShipmentInfo[] obj = CargoMatrix.Communication.WebServiceManager.Instance().ScanShipmentMCH(flightDetailsItem.TaskId, shipment);
             
       
             

            List<string> ulds = new List<string>();
            try
            {
                foreach (CargoMatrix.Communication.WSCargoReceiverMCHService.ScannedShipmentInfo o in obj)
                {

                    if (o.Uld.UldPrefix.ToUpper() == "LOOSE")
                    {
                        ulds.Add(o.Uld.UldPrefix);
                    }
                    else
                    {
                        ulds.Add(o.Uld.UldPrefix + o.Uld.UldSerialNo);
                    }

                }
            }
            catch (Exception ex)
            {
                if (!ulds.Contains("Loose"))
                {
                    ulds.Add("Loose");
                }
                string e = ex.Message;
            }

            string uld = string.Empty;
            if (ulds.Count == 1)
            {
                uld = ulds[0];
            }
            else if (ulds.Count > 1)
            {
                uld = SelectUld(ulds);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Unable to locate details for this shipment. Check if this shipment exists in system.", "Shipment details not found!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }


            if (uld != string.Empty)
            {
                DoProceed(uld);
                scannedUld = string.Empty;
            }

        }

        void BarcodeReadNotify_Domestic(string barcodeData)
        {
            ValidateShipment(barcodeData);
            Cursor.Current = Cursors.WaitCursor;
           


            
            //ScanObject barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            //this.topLabel.SplashText("Scanned " + barcodeData);
            //MasterBillItem tempMawb = null;
            //switch (barcode.BarcodeType)
            //{
            //    case BarcodeTypes.HouseBill:
            //        CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
            //        //Forklift.Instance.FirstScannedHousebill = barcodeData;
            //        tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(string.Empty, string.Empty, barcode.HouseBillNumber);
            //        break;
            //    case BarcodeTypes.MasterBill:
            //        CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
            //        tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(barcode.CarrierNumber, barcode.MasterBillNumber, string.Empty);
            //        break;
            //    default:
            //        break;
            //}

            //if (tempMawb != null)
            //{
            //    if (tempMawb.Transaction.TransactionStatus1 == false)
            //    {
            //        CargoMatrix.UI.CMXMessageBox.Show(tempMawb.Transaction.TransactionError, "Error:", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            //    }
            //    else
            //    {
            //        //Forklift.Instance.Mawb = tempMawb;
            //        DisplayMawbDetails();
            //    }
            //}
            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }

        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {
 
            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + "-" + hawbNo + "-" + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }

        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            scannedUld = string.Empty;
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightShipmentsList(flightDetailsItem));
        }

        void ProceedButton_Click(object sender, EventArgs e)
        {
            DoProceed(string.Empty);
        }
        void DoProceed(string selectedUld)
        {


            if (flightDetailsItem.Status == FlightStatus.Open || flightDetailsItem.Status == FlightStatus.Pending)
            {
                string msg = "Are you sure you want to recover this flight?";
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {

                    //string reference = string.Format("{0}{1}{2}", this.flightDetailsItem.Origin, this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
                    //string enteredLocation;
                    //BarcodeEnabled = false;
                    //bool isFPC = false;
                    //if (DoLocation(out enteredLocation, out isFPC, reference,true))
                    //{
                    //    long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);


                        bool status = CargoMatrix.Communication.WebServiceManager.Instance().RecoverFlightMCH(flightDetailsItem.TaskId);
                        if (status)
                        {
                            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldList(flightDetailsItem, selectedUld));
                            //if (isFPC)
                            //{
                            //    ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), this.flightDetailsItem.TotalPieces, "CargoReceiver");
                            //}
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to recover this flight.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            LoadControl();
                            return;
                        }

                     
          
                    //}







                    //flightDetailsItem.Status = FlightStatus.InProgress;
                    //DisplayMawbDetails();
                }
                else
                {
                    DisplayMawbDetails();
                }
            }
            else
            {
                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new UldList(flightDetailsItem, selectedUld));
            }
             
         
            
           
  
        }

        //private void DisplayStageCheckinPopup()
        //{
        //    Control[] btns = new Control[]{
        //    new SmoothListbox.ListItems.StandardListItem("STAGE",null,1),
        //    new SmoothListbox.ListItems.StandardListItem("CHECK IN",null,2)};

        //    CargoMatrix.Utilities.MessageListBox popup = new CargoMatrix.Utilities.MessageListBox();
        //    popup.AddItems(btns);
        //    popup.MultiSelectListEnabled = false;
        //    popup.OneTouchSelection = true;
        //    popup.HeaderText = Forklift.Instance.Mawb.Reference();
        //    popup.HeaderText2 = "Select Action To Continue";
        //    popup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(popup_ListItemClicked);
        //    if (DialogResult.OK != popup.ShowDialog())
        //        BarcodeEnabled = true;
        //}

        void popup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 1:
                    //if (stageConsol())
                    //{
                    //    //Forklift.Instance.Mawb = null;
                    //    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                    //}
                    break;
                case 2:
                    //Cursor.Current = Cursors.WaitCursor;
                    //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new HawbScan());
                    break;
            }
            sender.Reset();
        }

        //private bool stageConsol()
        //{
        //    CustomUtilities.ScanEnterPopup consolStagePop = new CustomUtilities.ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
        //    consolStagePop.Title = Forklift.Instance.Mawb.Reference();
        //    consolStagePop.TextLabel = "Scan or Enter Stage Location";
        //    if (DialogResult.OK == consolStagePop.ShowDialog())
        //    {
        //        var status = CargoMatrix.Communication.CargoReceiver.Instance.StageMasterBill(Forklift.Instance.Mawb.MasterBillId, 0, consolStagePop.Text);
        //        if (status.TransactionStatus1 == false)
        //        {
        //            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //        }
        //        return status.TransactionStatus1;
        //    }
        //    else return false;
        //}
    }
}
