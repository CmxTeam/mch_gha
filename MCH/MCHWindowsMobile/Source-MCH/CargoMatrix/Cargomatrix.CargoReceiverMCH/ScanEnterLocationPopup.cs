﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.ScannerUtilityWS;
using CargoMatrix.Communication.WSCargoReceiverMCHService;
using CargoMatrix.Communication.WSScannerMCHService;
using SmoothListbox.ListItems;
using CMXExtensions;
using System.Linq;

namespace CargoMatrix.CargoReceiver
{
    public partial class ScanEnterLocationPopup : Form
    {
        CargoMatrix.UI.BarcodeReader barcode;
        public string scannedPrefix;
        public bool isFPC = false;
        public bool isFPCActive = false;
        public Func<LocationItem[]> GetLocationsMathod;
        bool shouldBelongToList;
        string notInTheListMessage;


        public bool BrowseButtonVisible
        {
            get
            {
                return this.buttonBrowse.Visible;
            }
            set
            {
                this.buttonBrowse.Visible = value;
                this.buttonBrowse.Refresh();
            }
        }

        public string NotInTheListMessage
        {
            get { return notInTheListMessage; }
            set { notInTheListMessage = value; }
        }

        public bool ShouldBelongToList
        {
            get { return shouldBelongToList; }
            set { shouldBelongToList = value; }
        }

        public string ScannedPrefix
        {
            get { return scannedPrefix; }
            set { scannedPrefix = value; }
        }
        private LocationTypes locationTypes;


        public void Reset()
        {
            textBox1.Text = string.Empty;
            labelText.Text = string.Empty;
            scannedPrefix = string.Empty;
            barcode.StopRead();
            barcode.StartRead();
        }

        public ScanEnterLocationPopup()
            : this(LocationTypes.NA)
        {
        }

        public ScanEnterLocationPopup(LocationTypes locationTypes)
        {
            InitializeComponent();
            InitialzeImages();
            this.buttonOk.Enabled = false;
            this.textBox1.TextChanged += new EventHandler(textBox1_TextChanged);
            this.barcode = new CargoMatrix.UI.BarcodeReader();
            barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcodeReader_BarcodeReadNotify);
            
            this.locationTypes = locationTypes;
            this.shouldBelongToList = false;
            this.notInTheListMessage = "Scanned barcode doesn't belong to the list";
            
            Reset();
        }

        private void InitialzeImages()
        {
            this.buttonOk.Image = global::Resources.Graphics.Skin.nav_ok_dis;//global::Resources.Graphics.Skin.nav_ok;
            this.buttonOk.PressedImage = global::Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.DisabledImage = global::Resources.Graphics.Skin.nav_ok_dis;

            this.buttonCancel.Image = global::Resources.Graphics.Skin.nav_cancel;
            this.buttonCancel.PressedImage = global::Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.DisabledImage = global::Resources.Graphics.Skin.nav_cancel_dis;

            this.buttonBrowse.Image = CargoMatrix.Resources.Skin._3dots_btn;
            this.buttonBrowse.PressedImage = CargoMatrix.Resources.Skin._3dots_btn_over;


            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                if (!isFPCActive)
                {
                    this.buttonCamera.Enabled = false;
                    this.buttonCamera.PressedImage = CargoMatrix.Resources.Skin.button_camera_dis;
                    this.buttonCamera.Image = CargoMatrix.Resources.Skin.button_camera_dis;
                    this.buttonCamera.DisabledImage = CargoMatrix.Resources.Skin.button_camera_dis;
                }
                else
                {
                    this.buttonCamera.Enabled = true;
                    this.buttonCamera.PressedImage = CargoMatrix.Resources.Skin.button_camera_over;
                    this.buttonCamera.Image = CargoMatrix.Resources.Skin.button_camera;
                    this.buttonCamera.DisabledImage = CargoMatrix.Resources.Skin.button_camera_dis;
                }
            }
            else
            {
                this.buttonCamera.Enabled = false;
                this.buttonCamera.PressedImage = CargoMatrix.Resources.Skin.button_camera_dis;
                this.buttonCamera.Image = CargoMatrix.Resources.Skin.button_camera_dis;
                this.buttonCamera.DisabledImage = CargoMatrix.Resources.Skin.button_camera_dis;
            }




            this.pictureBoxHeader.Image = global::Resources.Graphics.Skin.popup_header;
            this.pictureBoxNavBg.Image = global::Resources.Graphics.Skin.nav_bg;
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }
        void barcodeReader_BarcodeReadNotify(string barcodeData)
        {
            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            if (this.locationTypes == LocationTypes.Area ||
               this.locationTypes == LocationTypes.Door ||
               this.locationTypes == LocationTypes.Truck ||
               this.locationTypes == LocationTypes.Uld ||
               this.locationTypes == LocationTypes.Location)
            {
                LocationTypes? scannedLocation = this.TryConvetToLocationType(scanItem.BarcodeType);

                if (scannedLocation.HasValue == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Unknown barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    barcode.StartRead();
                    return;
                }

                if (this.locationTypes == LocationTypes.Location &&
                    !(scannedLocation.Value == LocationTypes.Area || scannedLocation.Value == LocationTypes.Door || scannedLocation.Value == LocationTypes.ScreeningArea))

                if (this.locationTypes != scannedLocation.Value)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    barcode.StartRead();
                    return;
                }

                if(this.shouldBelongToList == true)
                {
                    var getLocations = this.GetLocationsMathod;

                    //if (getLocations == null)
                    //{
                    //    getLocations = () => CargoMatrix.Communication.ScannerUtility.Instance.GetLocations(this.locationTypes);
                    //}

                    if (getLocations == null)
                    {
                        getLocations = () => CargoMatrix.Communication.WebServiceManager.Instance().GetWarehouseLocationsMCH(CargoMatrix.Communication.WSScannerMCHService.LocationType.Area);
                    }
                    


                    var locationsArray = getLocations();

                    var location = (from item in locationsArray
                            where string.Compare(item.LocationBarcode, scanItem.Location, true) == 0
                            select item).FirstOrDefault();

                    if (location == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(this.notInTheListMessage, "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        barcode.StartRead();
                        return;
                    }
                }
            }
             
            textBox1.Enabled = false;
            char[] separators = {'-','*'};
            int startIndex = barcodeData.IndexOfAny(separators);
            if (startIndex > -1)
            {
                scannedPrefix = barcodeData.Substring(0, startIndex).ToUpper();
                textBox1.Text = barcodeData.Substring(startIndex + 1).ToUpper();
            }
            
            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
            //DialogResult = DialogResult.OK;
            barcode.StartRead();
        }
        public event EventHandler ButtonOkClick;
        public event EventHandler ButtonCancelClick;
        public string OKText
        {
            get { return buttonOk.Text; }
            set { buttonOk.Text = value; }
        }
        public string CancelText
        {
            get { return buttonCancel.Text; }
            set { buttonCancel.Text = value; }
        }
        public string TextLabel
        {
            get { return this.labelText.Text; }
            set { this.labelText.Text = value; }
        }
        public string Title
        {
            get;
            set;
        }
        public string ScannedText
        {
            get { return scannedPrefix + '-' + textBox1.Text; }
            set { textBox1.Text = value.ToUpper(); }
        }

        public override string Text
        {
            get
            {
                return this.textBox1.Text;
            }
            set
            {
                this.textBox1.Text = value;
            }
        }

        public bool CancelEnabled
        {
            get
            {
                return this.buttonCancel.Enabled;
            }
            set
            {
                this.buttonCancel.Enabled = value;
            }
        }

 
        private void buttonOkFPC_Click(object sender, EventArgs e)
        {
            isFPC = true;

            textBox1.Text = textBox1.Text;

            barcode.StopRead();

            if (null != ButtonOkClick)
            {
                ButtonOkClick(this, e);
            }

            DialogResult = DialogResult.OK;
        }


        private void buttonOk_Click(object sender, EventArgs e)
        {
            isFPC = false;

            textBox1.Text = textBox1.Text;

            barcode.StopRead();
            
            if (null != ButtonOkClick)
            {
                ButtonOkClick(this, e);
            }
            
            DialogResult = DialogResult.OK;
        }


        public void ClosePopUp()
        {
            isFPC = false;

            try
            {
                textBox1.Text = textBox1.Text;

                barcode.StopRead();

                //if (null != ButtonOkClick)
                //{
                //    ButtonOkClick(this, e);
                //}

                DialogResult = DialogResult.OK;
            }
            catch { }


        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = string.Empty;

            barcode.StopRead();

            if (null != ButtonCancelClick)
            {
                ButtonCancelClick(this, e);
            }

            DialogResult = DialogResult.Cancel;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == string.Empty)
            {
                buttonOk.Enabled = false;
                buttonOk.Image = global::Resources.Graphics.Skin.nav_ok_dis;
                buttonCamera.Enabled = false;
                buttonCamera.Image = CargoMatrix.Resources.Skin.button_camera_dis;

            }
            else
            {
                buttonOk.Enabled = true;
                buttonOk.Image = global::Resources.Graphics.Skin.nav_ok;

                if (isFPCActive)
                {
                    buttonCamera.Enabled = true;
                    buttonCamera.Image = CargoMatrix.Resources.Skin.button_camera;
                    buttonCamera.PressedImage = CargoMatrix.Resources.Skin.button_camera_over;
                }

            }
        }

        private void ScanEnterPopup_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                buttonOk_Click(null, null);
            }

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                e.Graphics.DrawString(Title, font, new SolidBrush(Color.White), 5, 3);
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            barcode.StopRead();
            ScanEnterLocationMessageBox loc = new ScanEnterLocationMessageBox();
            loc.OneTouchSelection = true;
            var getLocations = this.GetLocationsMathod;

            //if (getLocations == null)
            //{
            //    getLocations = () => CargoMatrix.Communication.ScannerUtility.Instance.GetLocations(this.locationTypes);
            //}

            if (getLocations == null)
            {
                getLocations = () => CargoMatrix.Communication.WebServiceManager.Instance().GetWarehouseLocationsMCH(CargoMatrix.Communication.WSScannerMCHService.LocationType.Area);
            }

            loc.ReloadItemsAsync(getLocations());
            
            if (loc.ShowDialog() == DialogResult.OK)
            {
                StandardListItem<LocationItem> selectedItem = (StandardListItem<LocationItem>)loc.SelectedItems[0];

                textBox1.Text = selectedItem.ItemData.LocationBarcode;

                if (scannedPrefix == string.Empty)
                {
                    scannedPrefix = selectedItem.ItemData.Prefix();
                }
               
            }

            this.Refresh();
            barcode.StartRead();
        }


        private LocationTypes? TryConvetToLocationType(CMXBarcode.BarcodeTypes barcodeType)
        {
            switch (barcodeType)
            {
                case CMXBarcode.BarcodeTypes.Area: return LocationTypes.Area;

                case CMXBarcode.BarcodeTypes.Door: return LocationTypes.Door;

                case CMXBarcode.BarcodeTypes.Truck: return LocationTypes.Truck;

                case CMXBarcode.BarcodeTypes.Uld: return LocationTypes.Uld;

                case CMXBarcode.BarcodeTypes.ScreeningArea: return LocationTypes.ScreeningArea;

                default: return null;
            }
        }
    }
}