﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartDeviceTest
{
    public partial class Form2 : Form
    {
        int attempt = 0;
        private delegate void AssigneText(TextBox txt, string text, int attempt);
        AssigneText at = (txt, str, a) => txt.Text +=a.ToString() +". " + str;
        ConnectionManager conectionManager;
        

        public Form2()
        {
            InitializeComponent();

            this.conectionManager = new ConnectionManager();
            this.conectionManager.ConnectionFailed += new EventHandler<ConnectionManager.ConnectionEventArgs>(conectionManager_ConnectionFailed);
            this.conectionManager.BeforeConnect += new EventHandler<ConnectionManager.ConnectionEventArgs>(conectionManager_BeforeConnect);
            this.conectionManager.AfterConnect += new EventHandler<ConnectionManager.ConnectionEventArgs>(conectionManager_AfterConnect);
        }

        void conectionManager_AfterConnect(object sender, ConnectionManager.ConnectionEventArgs e)
        {
            string text = string.Format("{0}Connected {1}", Environment.NewLine, e.Destination.Description);

            if (this.textBox1.InvokeRequired)
            {
                this.textBox1.Invoke(at, new object[] { this.textBox1, text, attempt++ });
            }
            else
            {
                this.textBox1.Text += text;
            }
        }

        void conectionManager_BeforeConnect(object sender, ConnectionManager.ConnectionEventArgs e)
        {
            string text = string.Format("{0}Attempt to conect. Connection {1}", Environment.NewLine, e.Destination.Description);
            
            if (this.textBox1.InvokeRequired)
            {
                this.textBox1.Invoke(at, new object[] { this.textBox1, text, attempt++ });
            }
            else
            {
                this.textBox1.Text += text;
            }
        }

        void conectionManager_ConnectionFailed(object sender, ConnectionManager.ConnectionEventArgs e)
        {
            string text = string.Format("{0}Connection Failed", Environment.NewLine);

            if (e.Destination != null)
            {
                text += e.Destination.Description;
            }

            if (e.Exception != null)
            {
                text += e.Exception.Message;
            }

            
            if (this.textBox1.InvokeRequired)
            {
                this.textBox1.Invoke(at, new object[] { this.textBox1, text, attempt++ });
            }
            else
            {
                this.textBox1.Text += text;
            }
            
            
        }

        private void btnInternetConnect_Click(object sender, EventArgs e)
        {
            this.conectionManager.Start();
        }

        private void btnGprsConnect_Click(object sender, EventArgs e)
        {

            this.conectionManager.Stop();        
        }

        
    }
}