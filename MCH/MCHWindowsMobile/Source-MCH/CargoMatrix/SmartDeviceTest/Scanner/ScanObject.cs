﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SmartDeviceTest.Scanner
{
    public class ScanObject
    {
        public string BarcodeData;

        public BarcodeTypesEnum BarcodeType;

        public string HouseBillNumber;

        public string MasterBillNumber;

        public string CarrierNumber;

        public string UldNumber;

        public string Location;

        public int PieceNumber;

        public int PinNumber;

        public int UserNumber;
    }
}
