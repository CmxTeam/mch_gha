﻿
namespace SmartDeviceTest
{
    partial class CountMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelReference = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxPieceCount = new CMXAlphaNumTextBox();
            this.pictureBoxCancel = new System.Windows.Forms.PictureBox();
            this.pictureBoxOk = new System.Windows.Forms.PictureBox();
            this.pictureBoxMenuBack = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pictureBoxCancel);
            this.panel1.Controls.Add(this.pictureBoxOk);
            this.panel1.Controls.Add(this.pictureBoxMenuBack);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Location = new System.Drawing.Point(10, 69);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(219, 148);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.labelReference);
            this.panel2.Controls.Add(this.labelDescription);
            this.panel2.Controls.Add(this.textBoxPieceCount);
            this.panel2.Location = new System.Drawing.Point(3, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(213, 79);
            // 
            // labelReference
            // 
            this.labelReference.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelReference.Location = new System.Drawing.Point(1, 6);
            this.labelReference.Name = "labelReference";
            this.labelReference.Size = new System.Drawing.Size(209, 29);
            this.labelReference.Text = "There are 5 Pieces avaiable in Rack 5. Enter the number of peices you would like " +
                "to move.";
            // 
            // labelDescription
            // 
            this.labelDescription.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelDescription.Location = new System.Drawing.Point(3, 42);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(158, 28);
            this.labelDescription.Text = "Enter number of pieces to be moved to new location:";
            // 
            // textBoxPieceCount
            // 
            this.textBoxPieceCount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxPieceCount.Location = new System.Drawing.Point(165, 42);
            this.textBoxPieceCount.Name = "textBoxPieceCount";
            this.textBoxPieceCount.Size = new System.Drawing.Size(45, 28);
            this.textBoxPieceCount.TabIndex = 13;
            this.textBoxPieceCount.InputMode = CMXTextBoxInputMode.Numeric;
            this.textBoxPieceCount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textLocationCode_KeyDown);
            // 
            // pictureBoxCancel
            // 
            this.pictureBoxCancel.Location = new System.Drawing.Point(126, 107);
            this.pictureBoxCancel.Name = "pictureBoxCancel";
            this.pictureBoxCancel.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCancel.Click += new System.EventHandler(this.pictureBoxCancel_Click);
            this.pictureBoxCancel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCancel_MouseDown);
            this.pictureBoxCancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCancel_MouseUp);
            // 
            // pictureBoxOk
            // 
            this.pictureBoxOk.Location = new System.Drawing.Point(39, 107);
            this.pictureBoxOk.Name = "pictureBoxOk";
            this.pictureBoxOk.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxOk.Click += new System.EventHandler(this.pictureBoxOk_Click);
            this.pictureBoxOk.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxOk_MouseDown);
            this.pictureBoxOk.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxOk_MouseUp);
            // 
            // pictureBoxMenuBack
            // 
            this.pictureBoxMenuBack.Location = new System.Drawing.Point(0, 105);
            this.pictureBoxMenuBack.Name = "pictureBoxMenuBack";
            this.pictureBoxMenuBack.Size = new System.Drawing.Size(219, 42);
            this.pictureBoxMenuBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(219, 22);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
            // 
            // CountMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "CountMessageBox";
            this.Text = "MicroPhotoCaptureForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RemarksMessageBox_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxMenuBack;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.PictureBox pictureBoxCancel;
        private System.Windows.Forms.PictureBox pictureBoxOk;
        private CMXAlphaNumTextBox textBoxPieceCount;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelReference;
        private System.Windows.Forms.Label labelDescription;
    }
}