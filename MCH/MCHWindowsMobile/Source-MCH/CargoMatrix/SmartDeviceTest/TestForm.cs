﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartDeviceTest
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();

            Scanner.Scanner.BarcodeScanned += new EventHandler<SmartDeviceTest.Scanner.ScanEventArgs>(Scanner_BarcodeScanned);

        }

        void Scanner_BarcodeScanned(object sender, SmartDeviceTest.Scanner.ScanEventArgs e)
        {
            this.button1.Text = e.ScannedText;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            DialogResult = DialogResult.OK;
        }

 
    }
}