﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CustomListItems;
using CustomUtilities;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CargoMatrix.Communication.WSScannerMCHService;
using CMXExtensions;
using CMXBarcode;
using System.Reflection;

namespace CargoMatrix.CargoLoader
{
    public partial class UldViewer : CargoMatrix.Utilities.MessageListBox
    {
        public object m_activeApp;
        private CargoLoaderFlight flightDetailsItem;
        private CargoLoaderFlightLegUld uld;
        private static UldViewer instance;
        CargoMatrix.UI.BarcodeReader barcode;
 
        public static UldViewer Instance
        {
            get
            {
                if (instance == null)
                    instance = new UldViewer();
 
                return instance;
            }
        }


        private UldViewer()
        {
            InitializeComponent();
            smoothListBoxBase1.MultiSelectEnabled = true;
   
            this.HeaderText = "";
            this.HeaderText2 = "";
       
            this.LoadListEvent += new LoadSmoothList(ForkLiftViewer_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(ForkLiftViewer_ListItemClicked);
            barcode = new CargoMatrix.UI.BarcodeReader();
            barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(ForkliftScan);
        }


        void ForkLiftViewer_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

     
        }

        void ForkLiftViewer_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            LoadControl();
        }

        public void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxBase1.RemoveAll();
            
            OkEnabled = false;


             
             
            UldViewItem[] items = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetUldView(flightDetailsItem.FlightManifestId,   this.uld.UldId);
            if (items ==null)
            {
                this.DialogResult = DialogResult.OK;
                return;
            }
            
            if (items.Length == 0)
            {
                 this.DialogResult = DialogResult.OK;
                 return;
            }


            foreach (UldViewItem item in items)
            {
                UldViewerCargoItem tempItem = new UldViewerCargoItem(item);
                tempItem.ButtonClick += new EventHandler(ForkliftDelete_OnListClick);
                smoothListBoxBase1.AddItem(tempItem);
            }



        }



        void ForkliftDelete_OnListClick(object sender, EventArgs e)
        {

            UldViewerCargoItem itemControl = (sender as UldViewerCargoItem);
            UldViewItem item = (UldViewItem)itemControl.ItemData;
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to remove this item from this ULD?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
 
                string reference = string.Format("{0}{1}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
                string enteredLocation = string.Empty;
                bool isFPC = false;
                 barcode.StopRead();
                if (DoLocation(out enteredLocation, out isFPC, reference, true))
                {

                     
                    long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                    if (locationId == 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }

                    CargoMatrix.Communication.CargoLoaderMCHService.Instance.RemoveAwbFromUld(item.DetailId, uld.UldId, flightDetailsItem.TaskId, (int)locationId);
                    LoadControl();


                    if (isFPC)
                    {
                        ShowFreightPhotoCapture(reference, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), flightDetailsItem.Destinations[0], 1, "CargoLoader");
                        this.DialogResult = DialogResult.OK;
                        return;
                    }
 
                }
                barcode.StartRead();

            }
              
        }
 

  
        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {

   
           
            
        }
  

        public  DialogResult ShowDialog(CargoLoaderFlight flightDetailsItem, CargoLoaderFlightLegUld uld)
        {
            this.uld = uld;
            this.flightDetailsItem = flightDetailsItem;


            if (uld.UldSerialNo == string.Empty)
            {
                this.HeaderText = string.Format("{0}", uld.UldType);
            }
            else
            {
                if (uld.UldType == null || uld.UldType == string.Empty)
                {
                    this.HeaderText = string.Format("{0}{1}", uld.UldPrefix, uld.UldSerialNo);
                }
                else
                {
                    this.HeaderText = string.Format("{0}-{1}{2}", uld.UldType, uld.UldPrefix, uld.UldSerialNo);
                }

            }



            barcode.StartRead();
            smoothListBoxBase1.IsSelectable = false;

            DialogResult dr = (instance as MessageListBox).ShowDialog();
            barcode.StopRead();
            return dr;

        }

 
        void ForkliftScan(string barcodeData)
        {
 
        }



        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }


        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {


            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }


            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + "-" + hawbNo + "-" + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, 584);
                        //  (m_activeApp as UserControl).Size = new Size(Width, Height);
                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                  
                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }
    

 
    }
   
}
