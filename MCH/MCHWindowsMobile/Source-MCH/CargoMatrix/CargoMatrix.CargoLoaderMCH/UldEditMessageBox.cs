﻿using System;

 
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
namespace CustomUtilities
{
    public partial class UldEditMessageBox : CargoMatrix.UI.MessageBoxBase 
    {
        
        private string m_caption;
        private long flightId;
        private string uldType;
        private static UldEditMessageBox instance = null;
        public UldEditMessageBox()
        {
            InitializeComponent();
 
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = Resources.Graphics.Skin._3dots_over ;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
             
             
        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            string uldPrefix = GetUldPrefix();
            if (uldPrefix != string.Empty)
          {
              instance.cmxTextBoxPrefix.Text = uldPrefix;
          }
        }
        string GetUldPrefix()
        {
            string[] uldTypes = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetUldPrefixList();
            if (uldTypes == null)
                return string.Empty;

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Uld Prefix";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoLoader";


            actPopup.RemoveAllItems();

            for (int i = 0; i < uldTypes.Length; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(uldTypes[i], null, i + 1));


            }





            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
                    return actPopup.SelectedItems[0].Name;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }



        }

        //string GetBUPsNotAttached()
        //{
        //    CargoLoaderFlightLegUld[] ulds = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetFlightDestinationBUPsNotAttached(this.flightId, this.uldType);
        //    if (ulds == null)
        //        return string.Empty;

        //    CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
        //    actPopup.HeaderText2 = "Ulds";
        //    actPopup.OneTouchSelection = true;
        //    actPopup.OkEnabled = false;
        //    actPopup.MultiSelectListEnabled = false;

        //    actPopup.HeaderText = "CargoLoader";


        //    actPopup.RemoveAllItems();

        //    for (int i = 0; i < ulds.Length; i++)
        //    {
        //        actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(ulds[i].UldType + ulds[i].UldSerialNo, CargoMatrix.Resources.Skin.Shipping_Crate1, i + 1));
        //    }
 

        //    try
        //    {
        //        if (DialogResult.OK == actPopup.ShowDialog())
        //        {
                   

        //            for (int i = 0; i < ulds.Length; i++)
        //            {
        //                if (ulds[i].UldType + ulds[i].UldSerialNo == actPopup.SelectedItems[0].Name)
        //                {
        //                    return ulds[i].UldSerialNo;
        //                }
        //            }

        //            return string.Empty;

        //        }
        //        else
        //        {
        //            return string.Empty;
        //        }
        //    }
        //    catch
        //    {
        //        return string.Empty;
        //    }



        //}


        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (instance.cmxTextBoxPrefix.Text == string.Empty || instance.cmxTextBoxNumber.Text.Length < 5)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid ULD information.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }
            DialogResult = DialogResult.OK;
        }


        public static DialogResult Show(long flightId, string reference, string uldType, ref string uldPrefix,ref string uldNumber)
        {


            if (instance == null)
                instance = new UldEditMessageBox();


            instance.flightId = flightId;
            instance.uldType = uldType;

 
            instance.lblType.Text = "ULD Type: " + uldType;
            instance.cmxTextBoxNumber.Text = uldNumber;
            instance.cmxTextBoxPrefix.Text = uldPrefix;

            instance.m_caption = "Edit Uld - " + reference;

            //if (!enableLookUp)
            //{
            //    instance.buttonBrowse.Enabled = false;
            //    instance.buttonBrowse.Image = Resources.Graphics.Skin._3dots_dis;
            //}
            //else
            //{
            //    instance.buttonBrowse.Enabled = true;
            //    instance.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            //}


            return instance.CMXShowDialog(ref uldNumber, ref uldPrefix);
             
        }
        private DialogResult CMXShowDialog(ref   string uldNumber, ref   string uldPrefix)
        {
            DialogResult result = ShowDialog();

            uldNumber = instance.cmxTextBoxNumber.Text;
            uldPrefix = instance.cmxTextBoxPrefix.Text;
            return result;
        }
       
      
    
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
           
        }


     
    }
}