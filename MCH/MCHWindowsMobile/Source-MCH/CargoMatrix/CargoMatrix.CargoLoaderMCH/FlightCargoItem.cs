﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using System.Text;
namespace CargoMatrix.CargoLoader
{
    public partial class FlightCargoItem : CustomListItems.ExpandableRenderListitem<CargoLoaderFlight>, ISmartListItem
    {

        //public event EventHandler ButtonClick;

        public FlightCargoItem(CargoLoaderFlight flight)
            : base(flight)
        {
            InitializeComponent();
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

 
        protected override void InitializeControls()
        {
            this.SuspendLayout();


            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.panelIndicators.Anchor = System.Windows.Forms.AnchorStyles.None;
            //this.panelIndicators.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelIndicators.Location = new System.Drawing.Point(59, 70);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(180, 16);
            //this.panelIndicators.TabIndex = 19;
            this.Controls.Add(panelIndicators);



            this.panelIndicators.Flags = (int)ItemData.Flag;
            if (ItemData.Flag == 0)
            {
                this.panelIndicators.Visible = false;
            }
            else
            {
                this.panelIndicators.Visible = true;
            }
  

            this.ResumeLayout(false);
        }

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                Brush whiteBrush = new SolidBrush(Color.White);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Format("{0}{1}", ItemData.CarrierCode, ItemData.FlightNumber);
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 145 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                   StringBuilder line2 = new StringBuilder(string.Empty);
                   foreach (string dest in ItemData.Destinations)
                   {
                       if (line2.ToString() == string.Empty)
                        {
                            line2.Append(dest);
                        }
                        else
                        {
                            line2.Append("-" + dest);
                        }
                    }
                    gOffScreen.DrawString(line2.ToString(), fnt, brush, new RectangleF(40 * hScale, 17 * vScale, 145 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line3 = string.Format("ETD: {0:ddMMM}", ItemData.ETD).ToUpper();
                    line3 += string.Format(" Loc: {0}", ItemData.Location);
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 31 * vScale, 170 * hScale, 13 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line4 = string.Format("Awbs: {0}  STC: {1}  PCS: {2}", ItemData.TotalAwbs, ItemData.TotalSTC, ItemData.TotalPcs);
                    gOffScreen.DrawString(line4, fnt, brush, new RectangleF(40 * hScale, 44 * vScale, 170 * hScale, 13 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line5 = string.Format("Ulds: {0}", ItemData.TotalUlds);
                    if (ItemData.TotalLoose > 0)
                    {
                        line5 += ", Loose";
                    }
                    gOffScreen.DrawString(line5, fnt, brush, new RectangleF(40 * hScale, 57 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line6 = string.Format("{0:0}%", ItemData.Progress);
                    gOffScreen.DrawString(line6, fnt, redBrush, new RectangleF(4 * hScale, 71 * vScale, 145 * hScale, 14 * vScale));
                }

                Image img = CargoMatrix.Resources.Skin.Clipboard;
                switch (ItemData.Status)
                {
                    case  TaskStatuses.Open:
                    case TaskStatuses.Pending:
                        img = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case TaskStatuses.InProgress:
                        img = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case TaskStatuses.Completed:
                        img = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;

                }
                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(38 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
 
                object O = Resources.Airlines.GetImage(ItemData.CarrierCode);
                if (O != null)
                {
                    Image logo = (Image)O;
                    gOffScreen.DrawImage(logo, new Rectangle((int)(4 * hScale), (int)(4 * vScale), (int)(34 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
                }
                else
                {
                    Image logo = Resources.Skin.Airplane24x24 ;
                    gOffScreen.DrawImage(logo, new Rectangle((int)(4 * hScale), (int)(4 * vScale), (int)(34 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
                }


                Image imgCutoff = CargoMatrix.Resources.Skin.cutoff;
                gOffScreen.DrawImage(imgCutoff, new Rectangle((int)(190 * hScale), (int)(4 * vScale), (int)(47 * hScale), (int)(28 * vScale)), new Rectangle(0, 0, imgCutoff.Width, imgCutoff.Height), GraphicsUnit.Pixel);
 

                //this.panelIndicators.Flags = (int)ItemData.Flag;

                //this.panelIndicators.Visible = true;
 
                //using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                //{
                //    Rectangle topRect = new Rectangle((int)(190 * hScale), (int)(4 * vScale), (int)(47 * hScale), (int)((47 * hScale) / 2));
                //    Rectangle botRect = new Rectangle((int)(190 * hScale), (int)(18 * vScale), (int)(47 * hScale), (int)((47 * hScale) / 2));
                //    StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };

                //    gOffScreen.DrawString(string.Format("{0:ddd}", ItemData.CutOfTime), font, new SolidBrush(Color.White), topRect, sf);
                //    gOffScreen.DrawString(string.Format("{0:HH:mm}", ItemData.CutOfTime), font, new SolidBrush(Color.Black), botRect, sf);

                //}

                StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };



                if (ItemData.CutOfTime != null)
                {
                    using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                    {
                        string line7 = string.Format("{0:ddd}", ItemData.CutOfTime);
                        gOffScreen.DrawString(line7, fnt, whiteBrush, new RectangleF(190 * hScale, 4 * vScale, 47 * hScale, 14 * vScale), sf);
                    }

                    using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                    {
                        string line8 = string.Format("{0:HH:mm}", ItemData.CutOfTime);
                        gOffScreen.DrawString(line8, fnt, brush, new RectangleF(190 * hScale, 18 * vScale, 47 * hScale, 14 * vScale), sf);
                    }
                }
                else
                {
                    using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                    {
                        string line7 = string.Format("{0:ddd}", ItemData.ETD);
                        gOffScreen.DrawString(line7, fnt, whiteBrush, new RectangleF(190 * hScale, 4 * vScale, 47 * hScale, 14 * vScale), sf);
                    }

                    using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                    {
                        string line8 = string.Format("{0:HH:mm}", ItemData.ETD);
                        gOffScreen.DrawString(line8, fnt, brush, new RectangleF(190 * hScale, 18 * vScale, 47 * hScale, 14 * vScale), sf);
                    }
                }

 

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }

            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {

            StringBuilder destinations = new StringBuilder(string.Empty);
            foreach (string dest in ItemData.Destinations)
            {
                if (destinations.ToString() == string.Empty)
                {
                    destinations.Append(dest);
                }
                else
                {
                    destinations.Append("-" + dest);
                }
            }

            string content = ItemData.FlightNumber + ItemData.CarrierCode + destinations;
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
