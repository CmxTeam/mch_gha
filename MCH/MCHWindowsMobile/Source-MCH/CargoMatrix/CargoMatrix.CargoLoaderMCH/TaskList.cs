﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CustomListItems;
using CMXExtensions;
using System.Collections.Generic;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CMXBarcode;
using SmoothListbox;
using System.Linq;

namespace CargoMatrix.CargoLoader
{
    public class TaskList : FlightList
    {
        public TaskList()
            : base()
        {
            this.Name = "TaskList";
        }

        public override void LoadControl()
        {
            smoothListBoxMainList.RemoveAll();

            Cursor.Current = Cursors.WaitCursor;
            this.label1.Text = string.Format("{0}/{1}", filter, sort);
            this.searchBox.Text = string.Empty;
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;

            //smoothListBoxMainList.AddItemToFront(new StaticListItem("New", Resources.Icons.Notepad_Add));
            TaskStatuses status = TaskStatuses.All;
            switch (filter)
            {
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                    status = TaskStatuses.Pending;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
                    status = TaskStatuses.InProgress;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
                    status = TaskStatuses.Completed;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                    status = TaskStatuses.Open;
                    break;
                default:
                    status = TaskStatuses.All;
                    break;
            }

            ReceiverSortFields sortfield = ReceiverSortFields.Carrier;
            switch (sort)
            {
                case CargoMatrix.Communication.DTO.FlightSorts.ORIGIN:
                    sortfield = ReceiverSortFields.Carrier;
                    break;
                case CargoMatrix.Communication.DTO.FlightSorts.CARRIER:
                    sortfield = ReceiverSortFields.Carrier;
                    break;
                case CargoMatrix.Communication.DTO.FlightSorts.FLIGHTNO:
                    sortfield = ReceiverSortFields.FlightNo;
                    break;
                default:
                    sortfield = ReceiverSortFields.Carrier;
                    break;
            }

            CargoLoaderFlight[] flightItems = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlights(status, "", "", "", sortfield);
            if (flightItems != null)
            {
                var rItems = from i in flightItems
                             select InitializeItem(i);
                smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
            }
 

            this.TitleText = string.Format("CargoLoader - ({0})", flightItems.Length);
            Cursor.Current = Cursors.Default;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

     
        protected override void FlightList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is StaticListItem)
                MessageBox.Show("This feature is not available");
            else
                base.FlightList_ListItemClicked(sender, listItem, isSelected);
        }

        protected override void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("CargoLoader - ({0})", smoothListBoxMainList.VisibleItemsCount  );
        }

    }
}
