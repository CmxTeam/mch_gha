﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CustomListItems;
using System.Text;

namespace CargoMatrix.CargoLoader
{
    public partial class ShipmentList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {

        public object m_activeApp;

        CustomListItems.OptionsListITem filterOption;
        private CargoLoaderFlight flightDetailsItem;
       // protected MessageListBox MawbOptionsList;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.DESTINATION;


        public ShipmentList(CargoLoaderFlight flightDetailsItem)
        {

      
            this.flightDetailsItem = flightDetailsItem;
      
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(ShipmentList_BarcodeReadNotify);
            BarcodeEnabled = false;

            this.LoadOptionsMenu += new EventHandler(ShipmentList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ShipmentList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(ShipmentList_ListItemClicked);
            this.panelCutoff.Paint += new System.Windows.Forms.PaintEventHandler(panelCutoff_Paint);
        }


        void panelCutoff_Paint(object sender, PaintEventArgs e)
        {
           
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                Rectangle topRect = new Rectangle(0, 0, panelCutoff.Width, panelCutoff.Height / 2);
                Rectangle botRect = new Rectangle(0, panelCutoff.Height / 2, panelCutoff.Width, panelCutoff.Height / 2);
                StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
                if (flightDetailsItem.CutOfTime != null)
                {
                    e.Graphics.DrawString(string.Format("{0:ddd}", flightDetailsItem.CutOfTime), font, new SolidBrush(Color.White), topRect, sf);
                    e.Graphics.DrawString(string.Format("{0:HH:mm}", flightDetailsItem.CutOfTime), font, new SolidBrush(Color.Black), botRect, sf);

                }
                else
                {
                    e.Graphics.DrawString(string.Format("{0:ddd}", flightDetailsItem.ETD), font, new SolidBrush(Color.White), topRect, sf);
                    e.Graphics.DrawString(string.Format("{0:HH:mm}", flightDetailsItem.ETD), font, new SolidBrush(Color.Black), botRect, sf);

                }
     
            }
        }


        //void RefreshFlight()
        //{
        //    CargoLoaderFlight tempFlight = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlight(this.flightDetailsItem.FlightManifestId, this.flightDetailsItem.TaskId);
        //    if (tempFlight != null)
        //    {
        //        this.flightDetailsItem = tempFlight;
        //    }

        //}

        void ShipmentList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:

                        //RefreshFlight();
                        LoadControl();
                        
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Uld);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter;
                LoadControl();
                return true;
            }
            return false;
        }

        void ShipmentList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter };
            this.AddOptionsListItem(filterOption);
        }

        //void ShipmentList_LoadOptionsMenu(object sender, EventArgs e)
        //{
        //    this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        //}


        void buttonForkLift_Click(object sender, System.EventArgs e)
        {
            if (CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkLiftCount(flightDetailsItem.TaskId) == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;
                BarcodeEnabled = false;
               // CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new ForkLiftList(flightDetailsItem, flightLegDetailsItem));
                DialogResult dr = ForkLiftViewer.Instance.ShowDialog(flightDetailsItem);
                //if (dr == DialogResult.OK)
                //{
                   
                //}
                LoadControl();

                Cursor.Current = Cursors.Default; 
            }
        }

        private string GetLocationLine(string[] locations)
        {
            StringBuilder sb = new StringBuilder("");
            foreach (string l in locations)
            {
                if (sb.ToString() == string.Empty)
                {
                    sb.Append(l);
                }
                else
                {
                    sb.Append(", " + l);
                }
            }
            return sb.ToString();
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;

            this.flightDetailsItem = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlight(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);

            this.buttonForkLiftCounter.Value = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkLiftCount(flightDetailsItem.TaskId);
     
            this.label1.Text =string.Format("{0}{1} {2:ddMMM}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber,  this.flightDetailsItem.ETD).ToUpper();
            this.label2.Text = string.Format("Awbs: {0} STC: {1} Pcs: {2}", this.flightDetailsItem.TotalAwbs, this.flightDetailsItem.TotalSTC, this.flightDetailsItem.TotalPcs);
            this.label3.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.LoadedUlds, this.flightDetailsItem.TotalUlds);
            this.label4.Text = string.Format("{0:0}%", this.flightDetailsItem.Progress);
            //this.label5.Text = string.Format("{0}", GetLocationLine(this.flightDetailsItem.Destinations));
            this.label5.Text = string.Format("Loc: {0} Dest: {1}", this.flightDetailsItem.Location, GetLocationLine(this.flightDetailsItem.Destinations)); 
            //switch (this.flightDetailsItem.Status)
            //{
            //    case TaskStatuses.Open:
            //    case TaskStatuses.Pending:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //    case TaskStatuses.InProgress:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
            //        break;
            //    case TaskStatuses.Completed:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
            //        break;
            //    default:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //}


            //TaskStatuses status = TaskStatuses.All;
            //switch (filter)
            //{
            //    case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
            //        status = TaskStatuses.Pending;
            //        break;
            //    case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
            //        status = TaskStatuses.InProgress;
            //        break;
            //    case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
            //        status = TaskStatuses.Completed;
            //        break;
            //    case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
            //        status = TaskStatuses.Pending;
            //        break;
            //    default:
            //        status = TaskStatuses.Pending;
            //        break;
            //}


            object O = Resources.Airlines.GetImage(flightDetailsItem.CarrierCode);
            if (O != null)
            {
                Image logo = (Image)O;
                itemPicture.Image = logo;
            }
            else
            {
                itemPicture.Image = Resources.Skin.Airplane24x24;
            }


            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;

 
          AwbModel[]  items =  CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetLoadingPlan(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);

             

            //if (items != null)
            //{
            //    var rItems = from i in items
            //                 select InitializeItem(i);
            //    smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
            //}

            foreach(AwbModel item in items)
            {



                ShipmentCargoItem tempItem = null;

                switch (filter)
                {
                    case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:

                        if (item.LoadedPcs > 0 && item.LoadedPcs < item.LoadPcs)
                        {
                            tempItem = new ShipmentCargoItem(item);
                            tempItem.ButtonClick += new EventHandler(ShipmentList_OnListClick);
                            smoothListBoxMainList.AddItem(tempItem);
                        }

                        break;
                    case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:

                        if ( item.LoadedPcs >= item.LoadPcs)
                        {
                            tempItem = new ShipmentCargoItem(item);
                            tempItem.ButtonClick += new EventHandler(ShipmentList_OnListClick);
                            smoothListBoxMainList.AddItem(tempItem);
                        }
 
                        break;
                    case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                        if (item.LoadedPcs < item.LoadPcs)
                        {
                            tempItem = new ShipmentCargoItem(item);
                            tempItem.ButtonClick += new EventHandler(ShipmentList_OnListClick);
                            smoothListBoxMainList.AddItem(tempItem);
                        }
                        break;
                    case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                        if (item.LoadedPcs == 0)
                        {
                            tempItem = new ShipmentCargoItem(item);
                            tempItem.ButtonClick += new EventHandler(ShipmentList_OnListClick);
                            smoothListBoxMainList.AddItem(tempItem);
                        }
                        break;
                    default:
                        tempItem = new ShipmentCargoItem(item);
                        tempItem.ButtonClick += new EventHandler(ShipmentList_OnListClick);
                        smoothListBoxMainList.AddItem(tempItem);
                        break;
                }




            }








            this.TitleText = string.Format("Loading Plan - {0} ({1})", filter, smoothListBoxMainList.VisibleItemsCount);
            Cursor.Current = Cursors.Default;


            if (flightDetailsItem.Progress >= 100 && flightDetailsItem.Status != TaskStatuses.Completed)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("All pieces were loaded. Do you want to finalize this task?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                    CargoMatrix.Communication.CargoLoaderMCHService.Instance.FinalizeCargoLoader(flightDetailsItem.TaskId, flightDetailsItem.FlightManifestId);
                    BarcodeEnabled = false;
                    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                    return;
                }
            }


        }



        //protected ICustomRenderItem InitializeItem(AwbModel item)
        //{
        //    ShipmentCargoItem tempItem = new ShipmentCargoItem(item);
        //    tempItem.ButtonClick += new EventHandler(ShipmentList_OnListClick);
        //    return tempItem;
        //}

        bool DoPieces(string reference, int remaningPieces, out int enteredPieces, bool allowOverage)
        {

            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.PieceCount = remaningPieces;
            CntMsg.LabelReference = reference;
            CntMsg.AllowOverage = allowOverage;
            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                enteredPieces = CntMsg.PieceCount;
                return true;
            }
            else
            {
                enteredPieces = 0;
                return false;
            }

        }

        private void AddAwbIntoForkLift(ValidatedShipment item)
        {
            
            int remaningPieces = item.AvailablePieces;
            int enteredPieces = 0;
            if (DoPieces(item.Awb, remaningPieces, out enteredPieces, false))
            {
                CargoMatrix.Communication.CargoLoaderMCHService.Instance.DropPiecesIntoForklift(flightDetailsItem.TaskId, item.AwbId, enteredPieces);
                //LoadControl();
                this.buttonForkLiftCounter.Value = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkLiftCount(flightDetailsItem.TaskId);
            }
            //else
            //{
            //    BarcodeEnabled = false;
            //    BarcodeEnabled = true;
            //}

           
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        protected virtual void ShipmentList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            
            ShipmentCargoItem itemControl = (ShipmentCargoItem)listItem;
            AwbModel item = (AwbModel)itemControl.ItemData;
            item = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetLoadingPlanForAwb(item.AWBID, flightDetailsItem.TaskId);
            int remaningPieces = item.AvailablePcs;
            int enteredPieces = 0;
            if (DoPieces(item.AWBNumber, remaningPieces, out enteredPieces, false))
            {
                CargoMatrix.Communication.CargoLoaderMCHService.Instance.DropPiecesIntoForklift(flightDetailsItem.TaskId, item.AWBID, enteredPieces);
                //LoadControl();


               
            }

            this.buttonForkLiftCounter.Value = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkLiftCount(flightDetailsItem.TaskId);
        }



        void ShipmentList_OnListClick(object sender, EventArgs e)
        {
            MessageBox.Show("List");
        }


        CargoLoaderFlightLegUld ValidateUld(string uldnumber)
        {
            CargoLoaderFlightLegUld[] ulds = CargoMatrix.Communication.CargoLoaderMCHService.Instance.AvailableULDsForBuild(flightDetailsItem.FlightManifestId);
            if (ulds == null)
                return null;

            foreach(CargoLoaderFlightLegUld uld in ulds)
            {
                if ((uld.UldPrefix + uld.UldSerialNo).ToUpper() == uldnumber.ToUpper())
                {
                    return uld;
                }
            }

            return null;
        }

        void ShipmentList_BarcodeReadNotify(string barcodeData)
        {
           


            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
            {
      
                int ForkLiftCount = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkLiftCount(flightDetailsItem.TaskId );
                if (ForkLiftCount == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                }
                else
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop all pieces into this location?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                             long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(scanItem.Location);
                             if (locationId == 0)
                             {
                                 CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                                 BarcodeEnabled = true;
                                 return;
                             }

                             CargoMatrix.Communication.CargoLoaderMCHService.Instance.DropAllForkliftPiecesIntoLocation(flightDetailsItem.TaskId,(int) locationId);
                        LoadControl();
                        return;
                    }
                }

            }
            else if (scanItem.BarcodeType == BarcodeTypes.Uld)
            {
                int ForkLiftCount = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkLiftCount(flightDetailsItem.TaskId);
                if (ForkLiftCount == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                }
                else
                {
                    CargoLoaderFlightLegUld uld = ValidateUld(scanItem.UldNumber);
                    if (uld != null)
                    {
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop all pieces into this ULD?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {

                            CargoMatrix.Communication.CargoLoaderMCHService.Instance.DropAllForkliftPiecesIntoULD(flightDetailsItem.TaskId, uld.UldId);
                             
                            LoadControl();
                            return;
                        }
                    }

                }
            }
            else
            {

                try
                {
                    ValidatedShipment awb = CargoMatrix.Communication.CargoLoaderMCHService.Instance.ValidateLoaderShipment(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId, barcodeData);
                    AddAwbIntoForkLift(awb);
                    
                    return;
                }
                catch
                { }
             
                
            }

            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }


        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {
 

            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

              CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + "-" + hawbNo + "-" + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }

 

    }

}
