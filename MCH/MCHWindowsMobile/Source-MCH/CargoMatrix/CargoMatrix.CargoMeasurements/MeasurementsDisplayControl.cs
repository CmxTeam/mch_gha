﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoMeasurements;
using CargoMatrix.Communication.ScannerUtilityWS;

namespace CargoMatrix.CargoMeasurements
{
    public partial class MeasurementsDisplayControl : SmoothListbox.ListItems.ListItem
    {
        public event EventHandler DeleteClick;
        public event EventHandler PrintClick;

        DimensioningMeasurementDetails measurementDetails;

        #region Properties
        public int ID
        {
            get;
            private set;
        }

        public DimensioningMeasurementDetails Measurements
        {
            get { return this.measurementDetails; }
            set
            {
                this.measurementDetails = value;

                if (this.measurementDetails == null)
                {
                    this.Clear();
                    return;
                }

                this.Display();
            }
        }

        public bool HasCompleteMeasurements
        {
            get
            {
                if (this.measurementDetails == null) return false;

                if (string.IsNullOrEmpty(this.txtPackageReferenceNumber.Text) == true) return false;

                if (this.measurementDetails.PackageType == null) return false;

                if (this.measurementDetails.Measurement == null) return false;

                if (this.measurementDetails.Measurement.Height == 0.0d) return false;

                if (this.measurementDetails.Measurement.Length == 0.0d) return false;

                if (this.measurementDetails.Measurement.Weight == 0.0d) return false;

                if (this.measurementDetails.Measurement.Width == 0.0d) return false;

                return true;
            }
        } 
        #endregion

        #region Constructors
        public MeasurementsDisplayControl(int id)
        {
            this.ID = id;

            InitializeComponent();
        } 
        #endregion

        public void Clear()
        {
            this.txtPackageReferenceNumber.Text = string.Empty;

            this.lblWeight.Text = string.Empty;

            this.lblHeight.Text = string.Empty;
            this.lblWidth.Text = string.Empty;
            this.lblLength.Text = string.Empty;

            this.measurementDetails = null;
        }

        public void ChangeMetricSystem(MetricSystemTypeEnum metricSystemType)
        {
            if (this.measurementDetails == null) return;

            if (this.measurementDetails.Measurement == null)
            {
                this.measurementDetails.Measurement = MettlerMeasurements.CreateFromSystemType(metricSystemType, MeasurementsOriginEnum.Manual);
            }
            else
            {
                if (this.measurementDetails.Measurement.MetricSystemType != metricSystemType)
                {
                    this.measurementDetails.Measurement = this.measurementDetails.Measurement.ConvertToMetricSystem(metricSystemType); 
                }
            }

            this.Display();
        }

        #region Handlers
        void btnDelete_Click(object sender, System.EventArgs e)
        {
            var deleteClick = this.DeleteClick;

            if (deleteClick != null)
            {
                deleteClick(this, EventArgs.Empty);
            }
        }

        void btnPrintLabel_Click(object sender, System.EventArgs e)
        {
            var printClick = this.PrintClick;

            if (printClick != null)
            {
                printClick(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Additional Methods
        private void Display()
        {
            if (this.measurementDetails == null)
            {
                this.Clear();
                return;
            }

            if (measurementDetails.Measurement != null)
            {
                string strValueFormat = "{0:N2} {1}";
                string mesuresWeightSign = measurementDetails.Measurement.WeightMesurementsUnit.ToString().ToLower();
                string mesuresLengthSign = measurementDetails.Measurement.LengthMesurementsUnit.ToString().ToLower();

                this.txtPackageReferenceNumber.Text = measurementDetails.PackageNumber;

                this.lblWeight.Text = string.Format(strValueFormat, measurementDetails.Measurement.Weight, mesuresWeightSign);

                this.lblHeight.Text = string.Format(strValueFormat, measurementDetails.Measurement.Height, mesuresLengthSign);

                this.lblWidth.Text = string.Format(strValueFormat, measurementDetails.Measurement.Width, mesuresLengthSign);

                this.lblLength.Text = string.Format(strValueFormat, measurementDetails.Measurement.Length, mesuresLengthSign);
            }

            if (measurementDetails.PackageType != null)
            {
                this.lblBoxType.Text = this.measurementDetails.PackageType.name;
            }

            this.lblPkgRefHeader.Text = CommonMethods.GetHeaderText(measurementDetails);
        }
        #endregion
    }
}
