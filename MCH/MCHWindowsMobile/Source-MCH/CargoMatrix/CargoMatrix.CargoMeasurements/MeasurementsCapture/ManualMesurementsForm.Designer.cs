﻿namespace CargoMatrix.CargoMeasurements.MeasurementsCapture
{
    partial class ManualMesurementsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
      
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.txtLength = new CargoMatrix.UI.CMXTextBox();
            this.lblWeightSystem = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblWidthSystem = new System.Windows.Forms.Label();
            this.txtWeight = new CargoMatrix.UI.CMXTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtHeight = new CargoMatrix.UI.CMXTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtWidth = new CargoMatrix.UI.CMXTextBox();
            this.lblPkgType = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblStatus = new CustomUtilities.CMXBlinkingLabel();
            this.lblDevice = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPkgRef = new System.Windows.Forms.TextBox();
            this.pcxHeader = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pcxFooter = new System.Windows.Forms.PictureBox();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.txtLength);
            this.pnlMain.Controls.Add(this.lblWeightSystem);
            this.pnlMain.Controls.Add(this.label4);
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Controls.Add(this.lblWidthSystem);
            this.pnlMain.Controls.Add(this.txtWeight);
            this.pnlMain.Controls.Add(this.label6);
            this.pnlMain.Controls.Add(this.txtHeight);
            this.pnlMain.Controls.Add(this.label7);
            this.pnlMain.Controls.Add(this.txtWidth);
            this.pnlMain.Controls.Add(this.lblPkgType);
            this.pnlMain.Controls.Add(this.label8);
            this.pnlMain.Controls.Add(this.lblStatus);
            this.pnlMain.Controls.Add(this.lblDevice);
            this.pnlMain.Controls.Add(this.label5);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.txtPkgRef);
            this.pnlMain.Controls.Add(this.pcxHeader);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.pcxFooter);
            this.pnlMain.Location = new System.Drawing.Point(0, 3);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(240, 288);
            
            // 
            // txtLength
            // 
            this.txtLength.BackColor = System.Drawing.Color.White;
            this.txtLength.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtLength.Location = new System.Drawing.Point(9, 131);
            this.txtLength.Name = "txtLength";
            this.txtLength.Size = new System.Drawing.Size(61, 19);
            this.txtLength.TabIndex = 23;
            this.txtLength.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtLength.MaxLength = 7;
            // 
            // lblWeightSystem
            // 
            this.lblWeightSystem.Location = new System.Drawing.Point(77, 175);
            this.lblWeightSystem.Name = "lblWeightSystem";
            this.lblWeightSystem.Size = new System.Drawing.Size(21, 19);
            this.lblWeightSystem.Text = "lb";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(12, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.Text = "Weight";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(21, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 18);
            this.label3.Text = "L";
            // 
            // lblWidthSystem
            // 
            this.lblWidthSystem.Location = new System.Drawing.Point(212, 134);
            this.lblWidthSystem.Name = "lblWidthSystem";
            this.lblWidthSystem.Size = new System.Drawing.Size(23, 13);
            this.lblWidthSystem.Text = "in";
            // 
            // txtWeight
            // 
            this.txtWeight.BackColor = System.Drawing.Color.White;
            this.txtWeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtWeight.Location = new System.Drawing.Point(9, 172);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(67, 19);
            this.txtWeight.TabIndex = 31;
            this.txtWeight.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtWeight.MaxLength = 7;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(89, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 15);
            this.label6.Text = "W";
            // 
            // txtHeight
            // 
            this.txtHeight.BackColor = System.Drawing.Color.White;
            this.txtHeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtHeight.Location = new System.Drawing.Point(150, 131);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(61, 19);
            this.txtHeight.TabIndex = 28;
            this.txtHeight.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtHeight.MaxLength = 7;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(162, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 18);
            this.label7.Text = "H";
            // 
            // txtWidth
            // 
            this.txtWidth.BackColor = System.Drawing.Color.White;
            this.txtWidth.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtWidth.Location = new System.Drawing.Point(79, 131);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(61, 19);
            this.txtWidth.TabIndex = 26;
            this.txtWidth.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtWidth.MaxLength = 7;
            // 
            // lblPkgType
            // 
            this.lblPkgType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblPkgType.Location = new System.Drawing.Point(9, 93);
            this.lblPkgType.Name = "lblPkgType";
            this.lblPkgType.Size = new System.Drawing.Size(110, 13);
            this.lblPkgType.ReadOnly = true;
            this.lblPkgType.BackColor = System.Drawing.Color.White;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(12, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 15);
            this.label8.Text = "Package Type";
            // 
            // lblStatus
            // 
            this.lblStatus.Blink = true;
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblStatus.Location = new System.Drawing.Point(64, 218);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(119, 16);
            this.lblStatus.Text = "...";
            // 
            // lblDevice
            // 
            this.lblDevice.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDevice.Location = new System.Drawing.Point(63, 202);
            this.lblDevice.Name = "lblDevice";
            this.lblDevice.Size = new System.Drawing.Size(119, 16);
            this.lblDevice.Text = "...";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(8, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 16);
            this.label5.Text = "Status :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(6, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.Text = "Device :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPkgRef
            // 
            this.txtPkgRef.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtPkgRef.Location = new System.Drawing.Point(9, 37);
            this.txtPkgRef.Multiline = true;
            this.txtPkgRef.Name = "txtPkgRef";
            this.txtPkgRef.Size = new System.Drawing.Size(180, 34);
            this.txtPkgRef.TabIndex = 11;
            this.txtPkgRef.ReadOnly = true;
            this.txtPkgRef.BackColor = System.Drawing.Color.White;
            // 
            // pcxHeader
            // 
            this.pcxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcxHeader.Location = new System.Drawing.Point(0, 0);
            this.pcxHeader.Name = "pcxHeader";
            this.pcxHeader.Size = new System.Drawing.Size(240, 20);
            this.pcxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pcxHeader_Paint);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 16);
            this.label1.Text = "Package Ref #";
            // 
            // pcxFooter
            // 
            this.pcxFooter.BackColor = System.Drawing.Color.White;
            this.pcxFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pcxFooter.Location = new System.Drawing.Point(0, 241);
            this.pcxFooter.Name = "pcxFooter";
            this.pcxFooter.Size = new System.Drawing.Size(240, 47);
            this.pcxFooter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;


            this.InitializeFields();


            // 
            // ManualMesurementsFormAnother
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.pnlMain);
            this.Name = "ManualMesurementsFormAnother";
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.PictureBox pcxHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pcxFooter;
        private System.Windows.Forms.TextBox txtPkgRef;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private CustomUtilities.CMXBlinkingLabel lblStatus;
        private System.Windows.Forms.Label lblDevice;
        private System.Windows.Forms.TextBox lblPkgType;
        private System.Windows.Forms.Label label8;
        private CargoMatrix.UI.CMXTextBox txtLength;
        private System.Windows.Forms.Label lblWeightSystem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblWidthSystem;
        private CargoMatrix.UI.CMXTextBox txtWeight;
        private System.Windows.Forms.Label label6;
        private CargoMatrix.UI.CMXTextBox txtHeight;
        private System.Windows.Forms.Label label7;
        private CargoMatrix.UI.CMXTextBox txtWidth;
    }
}