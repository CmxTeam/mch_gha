﻿
namespace CargoMatrix.CargoMeasurements.MeasurementsCapture
{
    partial class RedeemDimensionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.lblDevice = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtWeight = new CargoMatrix.UI.CMXTextBox();
            this.txtHeight = new CargoMatrix.UI.CMXTextBox();
            this.txtWidth = new CargoMatrix.UI.CMXTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLength = new CargoMatrix.UI.CMXTextBox();
            this.txtPkgRef = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblWeightSystem = new System.Windows.Forms.Label();
            this.lblHeightSystem = new System.Windows.Forms.Label();
            this.lblStatus = new CustomUtilities.CMXBlinkingLabel();
            this.lblPkgType = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(12, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 15);
            this.label8.Text = "Package Type";
            // 
            // lblDevice
            // 
            this.lblDevice.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDevice.Location = new System.Drawing.Point(58, 194);
            this.lblDevice.Name = "lblDevice";
            this.lblDevice.Size = new System.Drawing.Size(119, 16);
            this.lblDevice.Text = "...";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(5, 210);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 16);
            this.label5.Text = "Status :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(4, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.Text = "Device :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtWeight
            // 
            this.txtWeight.BackColor = System.Drawing.Color.White;
            this.txtWeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtWeight.Location = new System.Drawing.Point(9, 159);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(67, 19);
            this.txtWeight.TabIndex = 39;
            this.txtWeight.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtWeight.MaxLength = 7;
            // 
            // txtHeight
            // 
            this.txtHeight.BackColor = System.Drawing.Color.White;
            this.txtHeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtHeight.Location = new System.Drawing.Point(148, 116);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(61, 19);
            this.txtHeight.TabIndex = 38;
            this.txtHeight.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtHeight.MaxLength = 7;
            // 
            // txtWidth
            // 
            this.txtWidth.BackColor = System.Drawing.Color.White;
            this.txtWidth.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtWidth.Location = new System.Drawing.Point(78, 116);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(61, 19);
            this.txtWidth.TabIndex = 37;
            this.txtWidth.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtWidth.MaxLength = 7;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(160, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 18);
            this.label7.Text = "H";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(88, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 18);
            this.label6.Text = "W";
            // 
            // txtLength
            // 
            this.txtLength.BackColor = System.Drawing.Color.White;
            this.txtLength.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtLength.Location = new System.Drawing.Point(9, 116);
            this.txtLength.Name = "txtLength";
            this.txtLength.Size = new System.Drawing.Size(61, 19);
            this.txtLength.TabIndex = 36;
            this.txtLength.InputMode = UI.CMXTextBoxInputMode.Decimal;
            this.txtLength.MaxLength = 7;
            // 
            // txtPkgRef
            // 
            this.txtPkgRef.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtPkgRef.Location = new System.Drawing.Point(9, 18);
            this.txtPkgRef.Multiline = true;
            this.txtPkgRef.Name = "txtPkgRef";
            this.txtPkgRef.Size = new System.Drawing.Size(180, 34);
            this.txtPkgRef.TabIndex = 35;
            this.txtPkgRef.ReadOnly = true;
            this.txtPkgRef.BackColor = System.Drawing.Color.White;

            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 16);
            this.label1.Text = "Package Ref #";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(24, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 15);
            this.label3.Text = "L";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(14, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 15);
            this.label4.Text = "Weight";
            // 
            // lblWeightSystem
            // 
            this.lblWeightSystem.Location = new System.Drawing.Point(78, 162);
            this.lblWeightSystem.Name = "lblWeightSystem";
            this.lblWeightSystem.Size = new System.Drawing.Size(17, 18);
            this.lblWeightSystem.Text = "lb";
            // 
            // lblHeightSystem
            // 
            this.lblHeightSystem.Location = new System.Drawing.Point(211, 120);
            this.lblHeightSystem.Name = "lblHeightSystem";
            this.lblHeightSystem.Size = new System.Drawing.Size(23, 13);
            this.lblHeightSystem.Text = "in";
            // 
            // lblStatus
            // 
            this.lblStatus.Blink = true;
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblStatus.Location = new System.Drawing.Point(59, 210);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(119, 16);
            this.lblStatus.Text = "...";
            // 
            // lblPkgType
            // 
            this.lblPkgType.Location = new System.Drawing.Point(9, 73);
            this.lblPkgType.Name = "lblPkgType";
            this.lblPkgType.Size = new System.Drawing.Size(110, 13);
            this.lblPkgType.BackColor = System.Drawing.Color.White;
            this.lblPkgType.ReadOnly = true;
            // 
            // RedeemDimensionControlAnother
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblPkgType);
            this.Controls.Add(this.txtWidth);
            this.Controls.Add(this.lblHeightSystem);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblWeightSystem);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblDevice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.txtPkgRef);
            this.Controls.Add(this.txtLength);
            this.Controls.Add(this.txtHeight);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Name = "RedeemDimensionControlAnother";
            this.Size = new System.Drawing.Size(240, 231);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private CustomUtilities.CMXBlinkingLabel lblStatus;
        private System.Windows.Forms.Label lblDevice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private CargoMatrix.UI.CMXTextBox txtWeight;
        private CargoMatrix.UI.CMXTextBox txtHeight;
        private CargoMatrix.UI.CMXTextBox txtWidth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private CargoMatrix.UI.CMXTextBox txtLength;
        private System.Windows.Forms.TextBox txtPkgRef;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblWeightSystem;
        private System.Windows.Forms.Label lblHeightSystem;
        private System.Windows.Forms.TextBox lblPkgType;
    }
}
