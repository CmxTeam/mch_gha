﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CustomListItems;
using CargoMatrix.Communication.CargoMeasurements;
using CustomUtilities;
using CargoMatrix.Communication;
using CargoMatrix.Communication.ScannerUtilityWS;
using CargoMatrix.Communication.WSCargoDimensioner;

namespace CargoMatrix.CargoMeasurements.MeasurementsCapture
{
    public partial class Measurements : SmoothListbox.SmoothListbox2
    {
        public Action<DimensioningMeasurementDetails[], CargoMatrix.Communication.WSCargoDimensioner.TransactionStatus[]> SubmitMeasurementsAction;

        MetricSystemTypeEnum metricSystemType;
     
        OptionsListITem metricSystemMenuItem = new OptionsListITem(OptionsListITem.OptionItemID.METRICS_SYSTEM);
     
        private bool clearAfterSubmit = true;
        private int measurmentsIndex = 0;
        LabelPrinter defaultPrinter;
        PrintingOrigin printingOrigin;

       

        #region Properties
        public bool ClearAfterSubmit
        {
            get { return clearAfterSubmit; }
            set { clearAfterSubmit = value; }
        }

        public int? InitialMeasurementsCount
        {
            get;
            private set;
        }

        private int MeasurmentsIndex
        {
            get
            {
                return this.measurmentsIndex++;
            }
        }

        #endregion

        #region Constructors
       
        private Measurements()
        {
            InitializeComponent();

            this.panel1.Height = 0;
            this.panelHeader2.Height = 0;
            this.BarcodeEnabled = false;
         
            this.LoadOptionsMenu += new EventHandler(Measurements_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(Measurements_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(Measurements_ListItemClicked);
          
            this.metricSystemType = MettlerMeasurements.DefaultMetricSystem;
          
            this.ButtonFinalizeText = "Finalize";
            this.FinalizeButtonEnabled = false;
            this.ShowContinueButton(true);

            this.SetMainScreenHeader();
        }


        public Measurements(Action<DimensioningMeasurementDetails[], CargoMatrix.Communication.WSCargoDimensioner.TransactionStatus[]> submitMeasurementsAction, int initialMeasurementsCount)
            :this()
        {
            this.SubmitMeasurementsAction = submitMeasurementsAction;
            this.InitialMeasurementsCount = initialMeasurementsCount;
        }

        public Measurements(Action<DimensioningMeasurementDetails[], CargoMatrix.Communication.WSCargoDimensioner.TransactionStatus[]> submitMeasurementsAction)
            : this()
        {
            this.SubmitMeasurementsAction = submitMeasurementsAction;
            this.InitialMeasurementsCount = null;
        }

        #endregion

        #region Handlers
        void Measurements_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var selectedItem = listItem as MeasurementsDisplayControl;
            
            this.EditMeasurement(selectedItem);
            
            if (isSelected == false)
            {
                selectedItem.Focus(false);
            }
        }

        void Measurements_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;
            DialogResult dialogResult;

            var measurements = from item in this.smoothListBoxMainList.Items.OfType<MeasurementsDisplayControl>()
                                       select item.Measurements;

            switch (lItem.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.METRICS_SYSTEM:

                    Cursor.Current = Cursors.WaitCursor;
                    
                    MetricSystemPopup metricPopup = new MetricSystemPopup(this.metricSystemType);

                    dialogResult = metricPopup.ShowDialog();

                    if (dialogResult != DialogResult.OK) return;

                    this.metricSystemType = metricPopup.MetricSystemType;

                    metricSystemMenuItem.DescriptionLine = CommonMethods.GetMetricSystemMenuItemText(this.metricSystemType);


                    foreach (MeasurementsDisplayControl item in this.smoothListBoxMainList.Items)
                    {
                        item.ChangeMetricSystem(this.metricSystemType);
                    }

                    break;

                case OptionsListITem.OptionItemID.FINALIZE_MEASUREMENTS:

                    this.OnSubmitMesurements();

                    break;

                case OptionsListITem.OptionItemID.ADD_MEASUREMENTS:

                    this.AddNewMeasurements();

                    break;
            }

        }

        void Measurements_LoadOptionsMenu(object sender, EventArgs e)
        {
            metricSystemMenuItem.DescriptionLine = CommonMethods.GetMetricSystemMenuItemText(this.metricSystemType);

            this.AddOptionsListItem(metricSystemMenuItem);

            this.AddOptionsListItem(new OptionsListITem(OptionsListITem.OptionItemID.ADD_MEASUREMENTS));
        }

        void listItem_DeleteClick(object sender, EventArgs e)
        {
            var result = CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to remove measurements?", "Question", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.OK);

            if (result != DialogResult.OK) return;

            MeasurementsDisplayControl control = (MeasurementsDisplayControl)sender;

            this.smoothListBoxMainList.RemoveItem(control);
            this.LayoutItems();

            this.EnableFinalizeButton();
            this.SetMainScreenHeader();
        }
        
        #endregion

        #region Overrides
        public override void LoadControl()
        {
            if (this.smoothListBoxMainList.Items.Count > 0 &&
                this.InitialMeasurementsCount.HasValue == false) return;
            
            this.AddNewMeasurements(this.InitialMeasurementsCount);

            this.printingOrigin = CargoMeasurementsService.Instance.GetPrinterOrigin();
            this.defaultPrinter = CargoMatrix.Communication.ScannerUtility.Instance.GetDefaultPrinter(LabelTypes.MML);
        }

        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            if (this.m_bOptionsMenu == true)
            {
                base.pictureBoxBack_Click(sender, e);
                return;
            }

            if (HasInCompleteMeasurments == true)
            {
                CargoMatrix.UI.CMXMessageBox.Show("There are incomplete measurements", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);

                return;
            }

            if (this.smoothListBoxMainList.Items.Count > 0 &&
                this.HasInCompleteMeasurments == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Finalize measurments", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);

                return;
            }

            base.pictureBoxBack_Click(sender, e);
        }

        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            this.OnSubmitMesurements();
        }

        #endregion

        #region Public Methods
        public void AddMeasurements(IEnumerable<DimensioningMeasurementDetails> measurements)
        {
            foreach (var measurement in measurements)
            {
                this.AddMeasurement(measurement);
            }

            this.SetMainScreenHeader();

            this.LayoutItems();
        }

        #endregion

        #region Additional Methods
        private void OnSubmitMesurements()
        {
            var submit = this.SubmitMeasurementsAction;

            using (var cursorChanger = CustomUtilities.CursorChanger.WaitChanger)
            {

                var measurements = (from item in this.smoothListBoxMainList.Items.OfType<MeasurementsDisplayControl>()
                                    select item.Measurements).ToArray();

                var statuses = CargoMatrix.Communication.CargoMeasurements.CargoMeasurementsService.Instance.SaveMeasurements(measurements);

                var fails = (from item in statuses
                            where item.transactionStatus == false
                            select item).ToList();

                if (fails.Count > 0)
                {
                    foreach (var item in fails)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(item.TransactionError, "Error saving : " + item.Reference, CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    if (printingOrigin == PrintingOrigin.MANUAL)
                    {
                        var result = CargoMatrix.UI.CMXMessageBox.Show("Print measurements?", "Question", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                        if (result == DialogResult.OK)
                        {
                            this.defaultPrinter = CommonMethods.PrintMeasurements(measurements, this.printingOrigin, this.defaultPrinter);
                        }
                    }
                    else
                    {
                        this.defaultPrinter = CommonMethods.PrintMeasurements(measurements, this.printingOrigin, this.defaultPrinter);
                    }
                    
                    if (this.clearAfterSubmit == true)
                    {
                        this.Clear();
                    }
                }

                if (submit != null)
                {
                    submit(measurements, statuses);
                }
            }
        }

        public void Clear()
        {
            this.smoothListBoxMainList.RemoveAll();

            this.EnableFinalizeButton();
        }

        private void EnableFinalizeButton()
        {
            if (this.smoothListBoxMainList.Items.Count <= 0)
            {
                this.FinalizeButtonEnabled = false;
                return; 
            }
            
            this.FinalizeButtonEnabled = !this.HasInCompleteMeasurments;
        }

        private bool HasInCompleteMeasurments
        {
            get
            {
                if (this.smoothListBoxMainList.Items.Count <= 0) return false;

                var result = this.smoothListBoxMainList.Items.OfType<MeasurementsDisplayControl>().ToList().Exists(c => c.HasCompleteMeasurements == false);

                return result;
            }
        }

        void SetMainScreenHeader()
        {
            int itemsCount = this.smoothListBoxMainList.Items.Count;

            this.TitleText = string.Format("CargoDimensioner ({0})", itemsCount);
        }

        void AddNewMeasurements()
        {
            this.AddNewMeasurements(null);
        }
        
        void AddNewMeasurements(int? measurementsCount)
        {
            if (measurementsCount.HasValue == false)
            {
                CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
                cntMsg.HeaderText = "Confirm number of measurements";
                cntMsg.LabelDescription = "Enter the number";
                cntMsg.LabelReference = "Number of measurements";
                cntMsg.PieceCount = int.MaxValue;
                cntMsg.MinPiecesCount = 1;
                cntMsg.Readonly = false;

                if (DialogResult.OK != cntMsg.ShowDialog()) return;

                measurementsCount = cntMsg.PieceCount;
            }

            Cursor.Current = Cursors.WaitCursor;

            ManualMesurementsForm manualMesurementsForm = new ManualMesurementsForm(this.metricSystemType, ManualMesurementsForm.ActionType.Enter, new DimensioningMeasurementDetails[measurementsCount.Value]);
            manualMesurementsForm.ShowDialog();


            var measurements = manualMesurementsForm.Measurements;

            this.InitialMeasurementsCount = this.InitialMeasurementsCount.HasValue ? this.InitialMeasurementsCount.Value + measurements.Length : measurements.Length;

            foreach (var measuremet in measurements)
            {
                this.AddMeasurement(measuremet);
            }

            this.SetMainScreenHeader();

            this.EnableFinalizeButton();

        }

        void EditMeasurements(MeasurementsDisplayControl control)
        {
            var measurements = control.Measurements;

            ManualMesurementsForm manualMesurementsForm = new ManualMesurementsForm(this.metricSystemType, measurements);
            manualMesurementsForm.ShowDialog();

            control.Measurements = manualMesurementsForm.Measurements[0];

            this.LayoutItems();
        }

        private void AddMeasurement(DimensioningMeasurementDetails measurement)
        {
            MeasurementsDisplayControl listItem = new MeasurementsDisplayControl(this.MeasurmentsIndex);
            
            listItem.DeleteClick += new EventHandler(this.listItem_DeleteClick);
            listItem.Measurements = measurement;
            listItem.ChangeMetricSystem(this.metricSystemType);

            this.AddMainListItem(listItem);

            this.smoothListBoxMainList.MoveControlToTop(listItem);
        }

        private void EditMeasurement(MeasurementsDisplayControl listItem)
        {
            DimensioningMeasurementDetails[] measurments = new DimensioningMeasurementDetails[this.smoothListBoxMainList.Items.Count];

            int currentIndex = -1;
            int i = 0;
            foreach (MeasurementsDisplayControl item in this.smoothListBoxMainList.Items)
            {
                measurments[i++] = item.Measurements;

                if (listItem.ID == item.ID)
                {
                    currentIndex = i - 1;
                }
            }

            ManualMesurementsForm manualMesurementsForm = new ManualMesurementsForm(this.metricSystemType,
                ManualMesurementsForm.ActionType.Edit, measurments, currentIndex);

            
            manualMesurementsForm.ShowDialog();

            i = 0;
            foreach (MeasurementsDisplayControl item in this.smoothListBoxMainList.Items)
            {
                item.Measurements = manualMesurementsForm.Measurements[i++];
            }

            this.EnableFinalizeButton();

            this.LayoutItems();
        }
        #endregion
    }
}
