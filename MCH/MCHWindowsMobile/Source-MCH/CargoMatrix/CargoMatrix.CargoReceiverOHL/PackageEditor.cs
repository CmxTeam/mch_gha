﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CustomListItems;
using CustomUtilities;
using CargoMatrix.Communication.WSLoadConsol;
using CMXExtensions;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.CargoReceiverOHL
{
    class PackageEditor
    {
        private static PackageEditor instance;

        internal bool AddPackage()
        {
            AddEditPackagePopup addPackagePopup = new AddEditPackagePopup();
            if (DialogResult.OK == addPackagePopup.ShowDialog())
            {
                DataProvider.Packages.Add(addPackagePopup.Package);
                return true;
            }
            return false;
        }

        internal void EditPackage(DumbPackage package)
        {
            AddEditPackagePopup addPackagePopup = new AddEditPackagePopup();
            addPackagePopup.Package = package;
            addPackagePopup.Title = DataProvider.Title;
            if (DialogResult.OK == addPackagePopup.ShowDialog())
            {
                //var findPkg = DataProvider.Packages.Find(p => p.Package.Id == p.Package.Id && p.Pieces == package.Pieces);
                //if (null != findPkg)
                //{
                //    findPkg.Package = addPackagePopup.Package.Package;
                //    findPkg.Pieces = addPackagePopup.Package.Pieces;
                //}
            }

        }
        internal bool DeletePackage(DumbPackage pkg)
        {
            string msg = "Are you sure you want to delete Package ";//); !string.IsNullOrEmpty(Ulditem.ItemData.ULDNo) ? Ulditem.ItemData.ULDNo : Ulditem.ItemData.ULDName);
            if (CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Delete", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
            {
                DataProvider.Packages.Remove(pkg);
                return true;
            }
            return false;
        }

        internal static PackageEditor Instance
        {
            get
            {
                if (instance == null)
                    instance = new PackageEditor();
                return instance;
            }
        }
    }
}
