﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using SmoothListbox.ListItems;

namespace CargoMatrix.CargoReceiverOHL
{
    public partial class AddULDPopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        private IULDType containerType;
        private string containerNumber;
        private UI.BarcodeReader barcodereader;
        public IULDType ContainerType
        {
            get { return containerType; }
        }

        public string ContainerNumber
        {
            get { return containerNumber; }
        }
        public bool ShowLoose { get; set; }

        public AddULDPopup(IULDItem uld)
        {
            InitializeComponent();
            barcodereader = new CargoMatrix.UI.BarcodeReader();
            barcodereader.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcodereader_BarcodeReadNotify);
            this.Closing += (o, e) =>
            {
                barcodereader.StopRead();
            };
            if (null != uld)
            {
                string uldRef = uld.IsLoose ? uld.ULDName : uld.ULDNo;
                this.Title = "Edit Uld - " + uldRef;
                containerType = uld;
                textBoxContainerNo.Text = uld.ULDNo;
                textBoxContainerType.Text = uld.ULDName;
                EnableControls(uld);
            }
            else
            {
                this.Title = "Add New ULD";
                barcodereader.StartRead();

            }
        }

        void barcodereader_BarcodeReadNotify(string barcodeData)
        {
            var scan = Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scan.BarcodeType == CMXBarcode.BarcodeTypes.Uld)
            {
                UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                textBoxContainerNo.Text = scan.UldNumber;
            }
            else
            {
                UI.CMXMessageBox.Show("Invalid Barcode Scan", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
            barcodereader.StartRead();
        }

        void buttonContainerType_Click(object sender, System.EventArgs e)
        {
            var uld = DisplayULDTypes(ShowLoose, CargoMatrix.Communication.ScannerUtilityWS.MOT.Air);
            if (uld != null)
            {
                this.containerType = uld;
                this.textBoxContainerType.Text = uld.ULDName;
                EnableControls(uld);
            }
        }

        private void EnableControls(IULDType uld)
        {
            if (uld.IsLoose)
            {
                textBoxContainerNo.Enabled = false;
                textBoxContainerNo.Text = string.Empty;
                textBoxContainerNo.WatermarkText = string.Empty;
                barcodereader.StopRead();
            }
            else
            {
                textBoxContainerNo.Enabled = true;
                barcodereader.StartRead();
            }
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            this.containerNumber = !containerType.IsLoose ? textBoxContainerNo.Text : string.Empty;
            DialogResult = DialogResult.OK;
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            buttonOk.Enabled =
                // Uld is not loose and both fields are populated
                (!string.IsNullOrEmpty(textBoxContainerType.Text) && !ContainerType.IsLoose &&
                !string.IsNullOrEmpty(textBoxContainerNo.Text))
                ||
                // uld is loose
                (!string.IsNullOrEmpty(textBoxContainerType.Text) && ContainerType.IsLoose);
        }
        public static IULDType DisplayULDTypes(bool showLoose, CargoMatrix.Communication.ScannerUtilityWS.MOT mot)
        {
            var typeList = new CustomUtilities.SearchMessageListBox();
            typeList.HeaderText = "Select ULD Type";
            typeList.MultiSelectListEnabled = false;
            typeList.OneTouchSelection = true;
            foreach (var uldType in Communication.ScannerUtility.Instance.GetULDTypes(showLoose, mot))
            {
                if (showLoose && uldType.IsLoose)
                    typeList.smoothListBoxBase1.AddItemToFront(new StandardListItem<IULDType>(uldType.ULDName, null, uldType));
                else
                    typeList.smoothListBoxBase1.AddItem2(new StandardListItem<IULDType>(uldType.ULDName, null, uldType));
            }
            typeList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == typeList.ShowDialog())
            {
                return (typeList.SelectedItems[0] as StandardListItem<IULDType>).ItemData;
            }
            else return null;
        }

    }
}
