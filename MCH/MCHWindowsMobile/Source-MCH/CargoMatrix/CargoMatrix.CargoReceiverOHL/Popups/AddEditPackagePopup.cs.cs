﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSOnHand;
using SmoothListbox.ListItems;

namespace CargoMatrix.CargoReceiverOHL
{
    public partial class AddEditPackagePopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        private DumbPackage package;
        public AddEditPackagePopup()
        {
            InitializeComponent();
            this.Title = DataProvider.Title;
            textBox_TextChanged(null, EventArgs.Empty);
        }

        public new DialogResult ShowDialog()
        {
            if (package == null)
                buttonPackageType_Click(null, EventArgs.Empty);
            else
            {
                textBoxPackaging.Text = package.Package.Name;
                textBoxPieces.Text = package.Pieces.ToString();
            }
            return base.ShowDialog();
        }

        public DumbPackage Package
        {
            get { return package; }
            set { package = value; }
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            buttonOk.Enabled = !string.IsNullOrEmpty(textBoxPackaging.Text)
                && !string.IsNullOrEmpty(textBoxPieces.Text);
        }

        void buttonPackageType_Click(object sender, System.EventArgs e)
        {
            var packageType = DisplayPackageTypesList();
            if (packageType != null)
            {
                if (package == null)
                {
                    package = new DumbPackage(){ Package = packageType};
                }
                package.Package = packageType;
                textBoxPackaging.Text = packageType.Name;
            }
        }

        public static CMXPackageType DisplayPackageTypesList()
        {
            var pkgTypeList = new CustomUtilities.SearchMessageListBox();
            pkgTypeList.HeaderText = "Select Package Type";
            pkgTypeList.MultiSelectListEnabled = false;
            pkgTypeList.OneTouchSelection = true;
            foreach (var pkgType in Communication.OnHand.OnHand.Instance.GetPackageTypes())
            {
                pkgTypeList.smoothListBoxBase1.AddItem2(new StandardListItem<CMXPackageType>(pkgType.Name, null, pkgType));
            }
            pkgTypeList.smoothListBoxBase1.LayoutItems();

            if (DialogResult.OK == pkgTypeList.ShowDialog())
            {
                return (pkgTypeList.SelectedItems[0] as StandardListItem<CMXPackageType>).ItemData;
            }
            else return null;
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            package.Pieces = int.Parse(textBoxPieces.Text);
            DialogResult = DialogResult.OK;
        }
    }
}
