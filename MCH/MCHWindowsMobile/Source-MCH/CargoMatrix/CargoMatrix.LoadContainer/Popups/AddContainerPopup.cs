﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.LoadContainer
{
    public partial class AddContainerPopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        public AddContainerPopup()
        {
            InitializeComponent();
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            buttonOk.Enabled = !string.IsNullOrEmpty(textBoxContainerNo.Text) &&
                !string.IsNullOrEmpty(textBoxContainerType.Text) &&
                !string.IsNullOrEmpty(textBoxHawbNo.Text);
        }

    }
}
