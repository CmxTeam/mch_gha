﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSCargoReceiver;

namespace CargoMatrix.CargoAccept
{

    public enum FilteringMode
    {
        Flight, Uld
    }

    public partial class FilterPopup : MessageListBox
    {
        private CustomListItems.ChoiceListItem filterItem;
        private CustomListItems.ChoiceListItem sortItem;
        private MessageListBox optionalChoiceList;
        private string filter = CargoMatrix.CargoAcceptMCH.Resource.TextAll;
        private string sort = MawbSort.CARRIER;
        private FilteringMode mode;
        public string Filter
        {
            get { return filter; }
            set { filter = value; filterItem.LabelLine2.Text = value; }
        }
        public string Sort
        {
            get { return sort; }
            set { sort = value; sortItem.LabelLine2.Text = value; }
        }
        public int SortValue
        {
            get;
            set;
        }
        public FilterPopup(FilteringMode mode)
        {

            InitializeComponent();
            this.mode = mode;
            this.HeaderText = "Select filters";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = true;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FilterPopup_ListItemClicked);
            TopPanel = false;
            filterItem = new CustomListItems.ChoiceListItem("Filter", CargoMatrix.Resources.Skin.Filter, !string.IsNullOrEmpty(filter) ? filter : CargoMatrix.CargoAcceptMCH.Resource.TextAll);
            AddItem(filterItem);


            sortItem = new CustomListItems.ChoiceListItem("Sort", CargoMatrix.Resources.Skin.Sort, !string.IsNullOrEmpty(sort) ? sort : CargoMatrix.CargoAcceptMCH.Resource.TextAll);
            if (mode == FilteringMode.Flight)
            {
                AddItem(sortItem);
            }
 

        }


        void FilterPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            // first time initialization
            if (null == this.optionalChoiceList)
            {
                optionalChoiceList = new MessageListBox();
                optionalChoiceList.MultiSelectListEnabled = false;
                optionalChoiceList.OneTouchSelection = true;
                optionalChoiceList.TopPanel = false;
            }
            // if filter
            if (listItem == filterItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = CargoMatrix.CargoAcceptMCH.Resource.Text_FilterBy;



                optionalChoiceList.AddItems(GetFilters());

                // select current selected status
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == filter)
                    {
                        optionalChoiceList.SelectControl(item);
                        break;
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    filter = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    filterItem.LabelLine2.Text = filter;
                }
            }
            //if sort 
            if (listItem == sortItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = CargoMatrix.CargoAcceptMCH.Resource.Text_Sortby;
                optionalChoiceList.AddItems(GetSorts());

                // select current selected direction
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == sort)
                    {
                        optionalChoiceList.SelectControl(item);
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    sort = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    sortItem.LabelLine2.Text = sort;
                    SortValue = (optionalChoiceList.SelectedItems[0] as StandardListItem).ID;
                }
            }
        }



        private Control[] GetFilters()
        {
            ////if (mode == FilteringMode.Uld)
            return new Control[] {
                    new StandardListItem(CargoMatrix.CargoAcceptMCH.Resource.TextAll, null),
                    new StandardListItem(FlightFilters.NOT_STARTED, null),
                    new StandardListItem(FlightFilters.IN_PROGRESS, null),
                    new StandardListItem(FlightFilters.NOT_COMPLETED, null),
                    new StandardListItem(FlightFilters.COMPLETED, null) };
        }

        private Control[] GetSorts()
        {
            return new Control[]
                { 
                new StandardListItem(FlightSorts.CARRIER, null),
                new StandardListItem(FlightSorts.FLIGHTNO, null),
                new StandardListItem(FlightSorts.DESTINATION, null),
                };
        }
    }
}
