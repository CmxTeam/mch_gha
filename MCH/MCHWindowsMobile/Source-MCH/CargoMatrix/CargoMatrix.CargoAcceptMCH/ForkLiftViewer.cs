﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CustomListItems;
using CustomUtilities;
 
using CMXExtensions;
using CMXBarcode;
using System.Reflection;

namespace CargoMatrix.CargoAccept
{
    public partial class ForkLiftViewer : CargoMatrix.Utilities.MessageListBox
    {
        public object m_activeApp;
        private long TaskId;
        private string AwbNumber;
        private static ForkLiftViewer instance;
        CargoMatrix.UI.BarcodeReader barcode;
 
        public static ForkLiftViewer Instance
        {
            get
            {
                if (instance == null)
                    instance = new ForkLiftViewer();
 
                return instance;
            }
        }


        private ForkLiftViewer()
        {
            InitializeComponent();
            smoothListBoxBase1.MultiSelectEnabled = true;
   
            this.HeaderText = "Forklift Content";
            this.HeaderText2 = "Select shipments to drop into location";
            this.LoadListEvent += new LoadSmoothList(ForkLiftViewer_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(ForkLiftViewer_ListItemClicked);
            barcode = new CargoMatrix.UI.BarcodeReader();
            barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(ForkliftScan);
        }


        void ForkLiftViewer_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {


            ForkLiftCargoItem itemControl = (ForkLiftCargoItem)listItem;
       
            DataRow itm = (DataRow)itemControl.ItemData;

            int Counter = int.Parse(itm["counter"].ToString());
            long DetailId = long.Parse(itm["awbid"].ToString());
            int ForkliftPieces = int.Parse(itm["pieces"].ToString());




            if (Counter == 0)
            {
                Counter = ForkliftPieces;
            }
            else
            {
                Counter = 0;
            }

            itm["counter"] = Counter;
            itm.AcceptChanges();


            sender.Reset();
        }

        void ForkLiftViewer_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            LoadControl();
        }

        public void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxBase1.RemoveAll();
            
            OkEnabled = false;



           DataTable dtFL =  CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_GetForkliftAWBsList(this.TaskId);
           if (dtFL.Rows.Count == 0)
           {
               this.DialogResult = DialogResult.OK;
               return;
           }
           for (int i = 0; i < dtFL.Rows.Count; i++)
           {
               ForkLiftCargoItem tempItem = new ForkLiftCargoItem(dtFL.Rows[i]);
                   tempItem.ButtonClick += new EventHandler(ForkliftDelete_OnListClick);
                   smoothListBoxBase1.AddItem(tempItem);
           }
         

 


        }



        void ForkliftDelete_OnListClick(object sender, EventArgs e)
        {
 
            //ForkLiftCargoItem itemControl = (sender as ForkLiftCargoItem);
            //ShipmentItem item = (ShipmentItem)itemControl.ItemData;

            //if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to remove this item from your forklift?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            //{

            //    if (CargoMatrix.Communication.WebServiceManager.Instance().RemoveItemsFromForkliftMCH(item.DetailId, item.ForkliftPieces))
            //    {
            //        LoadControl();
            //    }
            //    else
            //    {
            //        CargoMatrix.UI.CMXMessageBox.Show("Unable to remove pieces from ForkLift.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            //    }
            //    LoadControl();
            //}
    
        }
 
 




        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {

            List<int> detailIdsPcs = new List<int>();
            List<long> detailIds = new List<long>();
            for (int i = 0; i < smoothListBoxBase1.Items.Count; i++)
            {

                ForkLiftCargoItem ctr = (ForkLiftCargoItem)smoothListBoxBase1.Items[i];
                DataRow itm = (DataRow)ctr.ItemData;

                int Counter = int.Parse(itm["counter"].ToString());
                long DetailId = long.Parse(itm["fdid"].ToString());
                int ForkliftPieces = int.Parse(itm["pieces"].ToString());


                if (Counter > 0)
                {
                    detailIds.Add(DetailId);
                    detailIdsPcs.Add(ForkliftPieces);
                }
            }
            if (detailIds.Count == 0)
            {
                OkEnabled = false;
            }


            if (detailIds.Count > 0)
            {
                barcode.StopRead();


                string reference = AwbNumber;
                string enteredLocation = string.Empty;
                bool isFPC = false;

                if (DoLocation(out enteredLocation, out isFPC, reference, true))
                {

                    long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                    if (locationId == 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }



                    for (int i = 0; i < detailIds.Count; i++)
                    {
                        CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_DropSelectedAcceptForkliftPieces(TaskId, (int)locationId, detailIds[i]);
                        //CargoMatrix.Communication.WebServiceManager.Instance().DropPiecesToLocationMCH(this.TaskId, (int)locationId, detailIds[i], detailIdsPcs[i]);
                    }

 
                    if (isFPC)
                    {
                        ShowFreightPhotoCapture(reference, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), 1, "CargoAccept");
                        this.DialogResult = DialogResult.OK;
                        return;
                    }
                 

                    LoadControl();
                     
                }




                barcode.StartRead();
            }
            else
            {
                OkEnabled = false;
            }


        }

        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {


            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }


            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(TaskId, origin + "-" + hawbNo + "-" + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, 584);
                        // (m_activeApp as UserControl).Size = new Size(Width, Height);
                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }
    
 
        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

 


        public DialogResult ShowDialog(long taskId,string awbNumber)
        {
            this.AwbNumber = awbNumber;
            this.TaskId = taskId;
                barcode.StartRead();
                smoothListBoxBase1.IsSelectable = true;
 
            DialogResult dr = (instance as MessageListBox).ShowDialog();
            barcode.StopRead();
            return dr;

        }

   

        void ForkliftScan(string barcodeData)
        {


            List<int> detailIdsPcs = new List<int>();
            List<long> detailIds = new List<long>();
            for (int i = 0; i < smoothListBoxBase1.Items.Count; i++)
            {

                ForkLiftCargoItem ctr = (ForkLiftCargoItem)smoothListBoxBase1.Items[i];
                DataRow itm = (DataRow)ctr.ItemData;

                int Counter = int.Parse(itm["counter"].ToString());
                long DetailId = long.Parse(itm["fdid"].ToString());
                int ForkliftPieces = int.Parse(itm["pieces"].ToString());


                if (Counter > 0)
                {
                    detailIds.Add(DetailId);
                    detailIdsPcs.Add(ForkliftPieces);
                }
            }
            if (detailIds.Count == 0)
            {
                OkEnabled = false;
            }


            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
            {



                if (detailIds.Count > 0)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop selected pieces into this location?", "CargoAccect", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(scanItem.Location);
                        if (locationId == 0)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            barcode.StartRead();
                            return;
                        }


                        for (int i = 0; i <  detailIds.Count; i++)
                        {
                          
                            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.Accept_DropSelectedAcceptForkliftPieces(TaskId, (int)locationId, detailIds[i]);
                        
                        }

     
                        LoadControl();
                        //return;
                    }
                }


 


            }

            barcode.StartRead();
        }
 
    }
   
}
