﻿using System;

 
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
namespace CustomUtilities
{
    public partial class UldWeightMessageBox : CargoMatrix.UI.MessageBoxBase 
    {
        
        private string m_caption;
        private int remainingPcs=0;
       
        private static UldWeightMessageBox instance = null;
        public UldWeightMessageBox()
        {
            InitializeComponent();
 
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = Resources.Graphics.Skin._3dots_over ;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
             
             
        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
  
        }


        //private void buttonWeight_TextChanged(object sender, EventArgs e)
        //{
           
        //}

        private decimal ConvertToDecimal(string value)
        {
            try
            {
                return decimal.Parse(value);
            }
            catch
            {
                return 0;
            }

        }

        private int ConvertToInt(string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch
            {
                return 0;
            }

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {

            decimal weight = ConvertToDecimal(instance.txtWeight.Text);
            int pcs = ConvertToInt(instance.txtPieces.Text);
            if (pcs < 1)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Please enter a valid number of pieces.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (weight < 1)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Please enter a valid weight.", "CargoAccept", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (pcs > instance.remainingPcs)
            {
                if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show("You have enter more pices than expected. Are you sure you want to continue?", "CargoAccect", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                    return;
                }
            }

            DialogResult = DialogResult.OK;
        }


        public static DialogResult Show(string reference, string uld, ref decimal weight, ref int pcs, ref bool screened, bool isUld)
        {


            if (instance == null)
                instance = new UldWeightMessageBox();


            if (isUld)
            {
                instance.txtPieces.Enabled = false;
            }
            else
            {
                instance.txtPieces.Enabled = true;
            }

            instance.remainingPcs = pcs;

            instance.lblReference.Text = uld;
          
            instance.txtWeight.Text = string.Format("{0:0.0}",weight);
            instance.txtWeight.SelectAll();
            instance.txtPieces.Text = string.Format("{0}", pcs);
            instance.ckScreened.Checked = screened;

            instance.m_caption = reference;


            instance.txtWeight.Focus();


            return instance.CMXShowDialog(ref weight, ref pcs,ref  screened);
             
        }
        private DialogResult CMXShowDialog(ref   decimal weight, ref int pcs,ref bool screened)
        {
            DialogResult result = ShowDialog();

            weight = ConvertToDecimal(instance.txtWeight.Text);
            pcs = ConvertToInt(instance.txtPieces.Text);
            screened = instance.ckScreened.Checked;
            return result;
        }
       
      
    
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
           
        }


     
    }
}