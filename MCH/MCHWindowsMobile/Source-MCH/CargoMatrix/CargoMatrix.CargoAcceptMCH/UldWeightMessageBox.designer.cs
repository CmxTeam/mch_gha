﻿namespace CustomUtilities
{
    partial class UldWeightMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.ckScreened = new System.Windows.Forms.CheckBox();

            this.lblReference = new System.Windows.Forms.Label();

            this.lblWeight = new System.Windows.Forms.Label();
            this.lblPieces = new System.Windows.Forms.Label();
            this.txtWeight = new CargoMatrix.UI.CMXTextBox();
            this.txtPieces = new CargoMatrix.UI.CMXTextBox();


            this.panel1.SuspendLayout();
            this.SuspendLayout();
       

            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            //this.panel1.Controls.Add(this.buttonBrowse);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBoxHeader);

            this.panel1.Controls.Add(this.txtWeight);
            this.panel1.Controls.Add(this.lblReference);
       
            this.panel1.Controls.Add(this.lblWeight);
            this.panel1.Controls.Add(this.lblPieces);
            this.panel1.Controls.Add(this.txtPieces);
            this.panel1.Controls.Add(this.ckScreened);

            this.panel1.Location = new System.Drawing.Point(3, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 261);
   

            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(127, 220);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
   

            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(56, 220);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);


            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowse.Location = new System.Drawing.Point(187, 90);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(40, 28);
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
        

            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 126);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            

            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 24);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
           
 
       
     



            // 
            // label1
            // 
            this.lblReference.Location = new System.Drawing.Point(12, 36);
            this.lblReference.Name = "label1";
            this.lblReference.Size = new System.Drawing.Size(170, 20);
            this.lblReference.Text = "";



      
            this.lblWeight.Location = new System.Drawing.Point(12, 70);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(170, 16);
            this.lblWeight.Text = "Weight (KG):";
            


            this.lblPieces.Location = new System.Drawing.Point(12, 118);
            this.lblPieces.Name = "lblPieces";
            this.lblPieces.Size = new System.Drawing.Size(170, 13);
            this.lblPieces.Text = "Pcs:";





            this.ckScreened.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.ckScreened.Location = new System.Drawing.Point(12, 170);
            this.ckScreened.Name = "ckScreened";
            this.ckScreened.Size = new System.Drawing.Size(100, 20);
            this.ckScreened.TabIndex = 12;
            this.ckScreened.Text = "Screened";






            this.txtWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtWeight.Location = new System.Drawing.Point(12, 91);
            this.txtWeight.MaxLength = 10;
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(169, 26);
            this.txtWeight.TabIndex = 3;
            this.txtWeight.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Decimal;
 


            this.txtPieces.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.txtPieces.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtPieces.Location = new System.Drawing.Point(12, 133);
            this.txtPieces.MaxLength = 10;
            this.txtPieces.Name = "txtPieces";
            this.txtPieces.Size = new System.Drawing.Size(169, 26);
            this.txtPieces.TabIndex = 3;
            this.txtPieces.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            



            InitializeImages();
      
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "OriginDestinationMessageBox";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private CargoMatrix.UI.CMXPictureButton buttonBrowse;
        private System.Windows.Forms.PictureBox pictureBox1;


        private System.Windows.Forms.Label lblReference;

        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblPieces;

        public CargoMatrix.UI.CMXTextBox txtWeight;
        public CargoMatrix.UI.CMXTextBox txtPieces;
        public System.Windows.Forms.CheckBox ckScreened;

    }
}