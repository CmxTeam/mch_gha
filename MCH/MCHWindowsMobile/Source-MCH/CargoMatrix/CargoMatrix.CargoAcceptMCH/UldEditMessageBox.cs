﻿using System;

using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
namespace CustomUtilities
{
    public partial class UldEditMessageBox : CargoMatrix.UI.MessageBoxBase 
    {
        
        private string m_caption;
        private long UldPrefixId = 0;
        private string uldType;
        private static UldEditMessageBox instance = null;
        public UldEditMessageBox()
        {
            InitializeComponent();
 
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = Resources.Graphics.Skin._3dots_over ;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
             
             
        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            string uldPrefix =string.Empty;
            long uldPrefixId = 0;
            GetUldPrefix(ref uldPrefix,ref  uldPrefixId);
            if (uldPrefix != string.Empty)
          {
              instance.cmxTextBoxPrefix.Text = uldPrefix;
              instance.UldPrefixId = uldPrefixId;
          }
        }

       


        void GetUldPrefix(ref string uldPrefix, ref long uldPrefixId)
        {
            string[] uldTypes = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetUldPrefixList();
            if (uldTypes == null)
                return;

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Uld Prefix";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoAccect";


            actPopup.RemoveAllItems();

            for (int i = 0; i < uldTypes.Length; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(uldTypes[i], null, i + 1));


            }





            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {



                    DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("select id from ShipmentUnitTypes where code = '" + actPopup.SelectedItems[0].Name + "'");
                    if(dt.Rows.Count>0)
                    {
                        uldPrefix = actPopup.SelectedItems[0].Name;
                        uldPrefixId = long.Parse(dt.Rows[0]["id"].ToString());
                    }
                    else
                    {
                    uldPrefix ="";
                         uldPrefixId = 0;
                         return;
                    }
                     



                }
                else
                {
                    uldPrefix = "";
                    uldPrefixId = 0;
                    return;
                }
            }
            catch
            {
                uldPrefix = "";
                uldPrefixId = 0;
                return;
            }



        }

        

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (instance.cmxTextBoxPrefix.Text == string.Empty || instance.cmxTextBoxNumber.Text.Length < 5)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid ULD information.", "CargoAccect", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }
            DialogResult = DialogResult.OK;
        }


        public static DialogResult Show(string uldType, ref string uldPrefix, ref string uldNumber, ref   long uldPrefixId)
        {


            if (instance == null)
                instance = new UldEditMessageBox();


             
            instance.uldType = uldType;

 
            instance.lblType.Text = "ULD Type: " + uldType;
            instance.cmxTextBoxNumber.Text = uldNumber;
            instance.cmxTextBoxPrefix.Text = uldPrefix;

            instance.m_caption = "Uld";

            //if (!enableLookUp)
            //{
            //    instance.buttonBrowse.Enabled = false;
            //    instance.buttonBrowse.Image = Resources.Graphics.Skin._3dots_dis;
            //}
            //else
            //{
            //    instance.buttonBrowse.Enabled = true;
            //    instance.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            //}


            return instance.CMXShowDialog(ref uldNumber, ref uldPrefix, ref uldPrefixId);
             
        }
        private DialogResult CMXShowDialog(ref   string uldNumber, ref   string uldPrefix, ref   long uldPrefixId)
        {
            DialogResult result = ShowDialog();

            uldNumber = instance.cmxTextBoxNumber.Text;
            uldPrefix = instance.cmxTextBoxPrefix.Text;
            uldPrefixId = instance.UldPrefixId;
            return result;
        }
       
      
    
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
           
        }


     
    }
}