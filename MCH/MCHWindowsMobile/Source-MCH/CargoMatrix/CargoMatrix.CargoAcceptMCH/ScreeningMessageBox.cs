﻿using System;

using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;
 
namespace CustomUtilities
{
    public partial class ScreeningMessageBox : CargoMatrix.UI.MessageBoxBase 
    {
        
        private string m_caption;

        private static ScreeningMessageBox instance = null;
        public ScreeningMessageBox()
        {
            InitializeComponent();
 
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = Resources.Graphics.Skin._3dots_over ;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
             
             
        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            string sealType = GetSealTypes();
            if (sealType != string.Empty)
          {
              instance.cmxTextBoxSealType.Text = sealType;
              instance.cmxTextBoxSealNumber.Focus();
          }
        }
        string GetSealTypes( )
        {
            DataTable dt = CargoMatrix.Communication.ScannerMCHServiceManager.Instance.ExecuteQuery("exec dbo.GetScreeningSealtypes");

            //dbo.GetScreeningSealtypes
            
            //string[] sealTypes ={"A","B"};
            //if (sealTypes == null)
            //    return string.Empty;

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Seal Types";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoAccept";


            actPopup.RemoveAllItems();

            //for (int i = 0; i < sealTypes.Length; i++)
            //{
            //    actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(sealTypes[i], null, i + 1));


            //}

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(dt.Rows[i]["SealType"].ToString(), null, i + 1));
            }


            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
                    return actPopup.SelectedItems[0].Name;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }



        }

   
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (instance.cmxTextBoxSealNumber.Text == string.Empty || instance.cmxTextBoxSealType.Text == string.Empty)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid information.", "CargoAccect", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }
            DialogResult = DialogResult.OK;
        }


        public static DialogResult Show(ref string truckNumber, ref string sealType, ref string sealNumber)
        {


            if (instance == null)
                instance = new ScreeningMessageBox();




            instance.cmxTextBoxSealType.Text =   sealType;
            instance.cmxTextBoxTruckNumber.Text = truckNumber;
            instance.cmxTextBoxSealNumber.Text = sealNumber;

            instance.m_caption = "Verify Screening.";

            //if (!enableLookUp)
            //{
            //    instance.buttonBrowse.Enabled = false;
            //    instance.buttonBrowse.Image = Resources.Graphics.Skin._3dots_dis;
            //}
            //else
            //{
            //    instance.buttonBrowse.Enabled = true;
            //    instance.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            //}


            return instance.CMXShowDialog(ref truckNumber, ref sealType, ref sealNumber);
             
        }
        private DialogResult CMXShowDialog(ref   string truckNumber, ref   string sealType, ref   string sealNumber)
        {
            DialogResult result = ShowDialog();

            truckNumber = instance.cmxTextBoxTruckNumber.Text;
            sealType = instance.cmxTextBoxSealType.Text;
            sealNumber = instance.cmxTextBoxSealNumber.Text;
            return result;
        }
       
      
    
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
           
        }


     
    }
}