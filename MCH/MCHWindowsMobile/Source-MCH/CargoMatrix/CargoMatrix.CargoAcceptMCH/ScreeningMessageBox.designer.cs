﻿namespace CustomUtilities
{
    partial class ScreeningMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();



            this.lblSealType = new System.Windows.Forms.Label();
            this.lblTruckNumber = new System.Windows.Forms.Label();
            this.lblSealNumber = new System.Windows.Forms.Label();

            this.cmxTextBoxTruckNumber = new CargoMatrix.UI.CMXTextBox();
            this.cmxTextBoxSealNumber = new CargoMatrix.UI.CMXTextBox();
            this.cmxTextBoxSealType = new CargoMatrix.UI.CMXTextBox();
     

            this.panel1.SuspendLayout();
            this.SuspendLayout();
       

            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonBrowse);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBoxHeader);

            this.panel1.Controls.Add(this.cmxTextBoxTruckNumber);
            this.panel1.Controls.Add(this.cmxTextBoxSealNumber);
            this.panel1.Controls.Add(this.cmxTextBoxSealType);

            this.panel1.Controls.Add(this.lblSealType);
            this.panel1.Controls.Add(this.lblSealNumber);
            this.panel1.Controls.Add(this.lblTruckNumber);


            this.panel1.Location = new System.Drawing.Point(3, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 171);
   

            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(127, 130);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
   

            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(56, 130);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);


            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowse.Location = new System.Drawing.Point(172, 32);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(40, 28);
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
        

            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 126);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            

            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 24);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
           
 
       
    
 
            // 
            // label2
            // 
      


            this.lblSealType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblSealType.Location = new System.Drawing.Point(3, 36);
            this.lblSealType.Name = "lblSealType";
            this.lblSealType.Size = new System.Drawing.Size(66, 20);
            this.lblSealType.Text = "Seal Type:";
            this.lblSealType.TextAlign = System.Drawing.ContentAlignment.TopRight;


            this.lblSealNumber.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblSealNumber.Location = new System.Drawing.Point(3, 68);
            this.lblSealNumber.Name = "lblSealNumber";
            this.lblSealNumber.Size = new System.Drawing.Size(66, 20);
            this.lblSealNumber.Text = "Seal#:";
            this.lblSealNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;


            this.lblTruckNumber.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblTruckNumber.Location = new System.Drawing.Point(3, 100);
            this.lblTruckNumber.Name = "lblTruckNumber";
            this.lblTruckNumber.Size = new System.Drawing.Size(66, 20);
            this.lblTruckNumber.Text = "Truck#:";
            this.lblTruckNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;



            this.cmxTextBoxTruckNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxTruckNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxTruckNumber.Location = new System.Drawing.Point(76, 97);
            this.cmxTextBoxTruckNumber.MaxLength = 20;
            this.cmxTextBoxTruckNumber.Name = "cmxTextBoxTruckNumber";
            this.cmxTextBoxTruckNumber.Size = new System.Drawing.Size(136, 26);
            this.cmxTextBoxTruckNumber.TabIndex = 3;
            this.cmxTextBoxTruckNumber.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;


            this.cmxTextBoxSealNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxSealNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxSealNumber.Location = new System.Drawing.Point(76, 65);
            this.cmxTextBoxSealNumber.MaxLength = 20;
            this.cmxTextBoxSealNumber.Name = "cmxTextBoxSealNumber";
            this.cmxTextBoxSealNumber.Size = new System.Drawing.Size(136, 26);
            this.cmxTextBoxSealNumber.TabIndex = 2;
            this.cmxTextBoxSealNumber.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;

            this.cmxTextBoxSealType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxSealType.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxSealType.Location = new System.Drawing.Point(76, 33);
            this.cmxTextBoxSealType.MaxLength = 20;
            this.cmxTextBoxSealType.Name = "cmxTextBoxSealType";
            this.cmxTextBoxSealType.Size = new System.Drawing.Size(88, 26);
            this.cmxTextBoxSealType.TabIndex = 1;
            this.cmxTextBoxSealType.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
            this.cmxTextBoxSealType.Enabled = false;

            InitializeImages();
      
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "OriginDestinationMessageBox";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private CargoMatrix.UI.CMXPictureButton buttonBrowse;
        private System.Windows.Forms.PictureBox pictureBox1;


      
        private System.Windows.Forms.Label lblSealType;

        private System.Windows.Forms.Label lblTruckNumber;
        private System.Windows.Forms.Label lblSealNumber;

        public CargoMatrix.UI.CMXTextBox cmxTextBoxTruckNumber;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxSealNumber;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxSealType;

      

    }
}