﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.GridListBox
{
    public partial class ManualPieceEntry : Form
    {
        bool bFirstTimeLoad = false;
       
        public string HeaderText;
        //SmoothListbox.ListItems.ComboBox m_PiecesComboBox;
        //SmoothListbox.ListItems.ComboBox m_directionComboBox;
        //public int SelectedPieceNo = -1;

        public string HousebillName;
        public int[] remainingPieces;
        public ManualPieceEntry()
        {
            InitializeComponent();

            smoothListBoxBase1.AutoScroll += new SmoothListbox.AutoScrollHandler(smoothListBoxReasons_AutoScroll);
            panel1.BackColor = GridListBoxResources.thumbnailBackColor.GetPixel(0, 0);
            pictureBoxMenuBack.Image = GridListBoxResources.popup_Main_nav;
            pictureBoxHeader.Image = GridListBoxResources.popup_header;
            pictureBoxOk.Image = GridListBoxResources.nav_ok;
            pictureBoxCancel.Image = GridListBoxResources.nav_cancel;
            pictureBoxUp.Image = GridListBoxResources.nav_up;
            pictureBoxDown.Image = GridListBoxResources.nav_down;

           // List<SmoothListbox.ListItems.ComboBoxItemData> tempList = new List<SmoothListbox.ListItems.ComboBoxItemData>();

            //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Warehouse area", FreightHandlerResources.Warehouse));
            //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Door", FreightHandlerResources.Door));
            //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Truck", FreightHandlerResources.Truck));
            //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Other", FreightHandlerResources.Symbol_New));
            //m_typeComboBox = new SmoothListbox.ListItems.ComboBox(null, FreightHandlerResources.Warehouse, tempList);
            //smoothListBoxBase1.AddItem(m_typeComboBox);

            //List<SmoothListbox.ListItems.ComboBoxItemData> tempList2 = new List<SmoothListbox.ListItems.ComboBoxItemData>();
            //tempList2.Add(new SmoothListbox.ListItems.ComboBoxItemData("Import", FreightHandlerResources.Import24));
            //tempList2.Add(new SmoothListbox.ListItems.ComboBoxItemData("Export", FreightHandlerResources.Export24));
            //m_directionComboBox = new SmoothListbox.ListItems.ComboBox(null, FreightHandlerResources.Shipping_Routes, tempList2);
            //smoothListBoxBase1.AddItem(m_directionComboBox);

            //m_typeComboBox.SelectedID = 0;
            //m_directionComboBox.SelectedID = 0;
           
            //AirlineNumber = airlneno;
        }

        void smoothListBoxReasons_AutoScroll(SmoothListbox.SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            switch (direction)
            {
                case SmoothListbox.SmoothListBoxBase.DIRECTION.UP:
                    pictureBoxUp.Enabled = enable;
                    break;
                case SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN:
                    pictureBoxDown.Enabled = enable;
                    break;
            }
            //throw new NotImplementedException();
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {



                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));
                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }


            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (bFirstTimeLoad == false)
            {
                bFirstTimeLoad = true;
                HeaderText = "Manual Entry";
                
                
            }
        }
       

        private void MicroPhotoCaptureForm_Load(object sender, EventArgs e)
        {
            List<SmoothListbox.ListItems.ComboBoxItemData> tempList = new List<SmoothListbox.ListItems.ComboBoxItemData>();
            smoothListBoxBase1.RemoveAll();
            //textLocationCode.Text = "";
            //SelectedPieceNo = -1;
            for (int i = 0; i < remainingPieces.Length; i++)
            {
                smoothListBoxBase1.AddItem(new SmoothListbox.ListItems.StandardListItem("Piece: "+ remainingPieces[i],null,remainingPieces[i]));
            
                //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("" + remainingPieces[i], null));
                //tempList.Add(new SmoothListbox.ListItems.ComboBoxItemData("Door", FreightHandlerResources.Door));
                
            }
            //m_PiecesComboBox = new SmoothListbox.ListItems.ComboBox(HousebillName, FreightHandlerResources.Warehouse, tempList);
            //smoothListBoxBase1.AddItem(m_PiecesComboBox);
            //textLocationCode.Focus();
            //textLocationCode.SelectAll();

        }

     
        Font HeaderFont = new Font(FontFamily.GenericSerif, 8, FontStyle.Bold);
        SolidBrush HeaderBrush = new SolidBrush(Color.White);
        private void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawString(HeaderText, HeaderFont, HeaderBrush,3, 4);
        }

        private void pictureBoxOk_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = GridListBoxResources.nav_ok_over;
        }

        private void pictureBoxOk_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = GridListBoxResources.nav_ok;
        }

        private void pictureBoxOk_Click(object sender, EventArgs e)
        {
            pictureBoxOk.Image = GridListBoxResources.nav_ok_over;
            pictureBoxOk.Refresh();

            //smoothListBoxBase1

            DialogResult = DialogResult.OK;

            

        }

        private void pictureBoxCancel_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = GridListBoxResources.nav_cancel_over;

        }

        private void pictureBoxCancel_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = GridListBoxResources.nav_cancel;
        }

        private void pictureBoxCancel_Click(object sender, EventArgs e)
        {
            pictureBoxCancel.Image = GridListBoxResources.nav_cancel_over;
            pictureBoxCancel.Refresh();

            DialogResult = DialogResult.Cancel;
           
        }

        private void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxUp.Image = GridListBoxResources.nav_up_over;
            smoothListBoxBase1.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);

        }

        private void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxUp.Image = GridListBoxResources.nav_up;
            smoothListBoxBase1.UpButtonPressed = false;
        }

        private void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxDown.Image = GridListBoxResources.nav_down_over;
            smoothListBoxBase1.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);

        }

        private void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxDown.Image = GridListBoxResources.nav_down;
            smoothListBoxBase1.DownButtonPressed = false;
        }
               
        private void pictureBoxUp_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxUp.Enabled)
                pictureBoxUp.Image = GridListBoxResources.nav_up;
            else
            {
                pictureBoxUp.Image = GridListBoxResources.nav_up_dis;
                smoothListBoxBase1.UpButtonPressed = false;
            }

        }

        private void pictureBoxDown_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxDown.Enabled)
                pictureBoxDown.Image = GridListBoxResources.nav_down;
            else
            {
                pictureBoxDown.Image = GridListBoxResources.nav_down_dis;
                smoothListBoxBase1.DownButtonPressed = false;
            }

        }

        
       
       
       

        private void textLocationCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBoxOk.Image = GridListBoxResources.nav_ok_over;
                pictureBoxOk.Refresh();
                DialogResult = DialogResult.OK;
                pictureBoxOk.Image = GridListBoxResources.nav_ok;

            }
        }

        private void textLocationCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBoxOk.Image = GridListBoxResources.nav_ok;                

            }
        }
        public int[] SelectedPieces
        {
            get
            {
                
                List<int> selected = new List<int>();
                
                foreach (Control control in smoothListBoxBase1.SelectedItems)
                {
                    if(control is SmoothListbox.ListItems.StandardListItem)
                    {
                        selected.Add((control as SmoothListbox.ListItems.StandardListItem).ID);
                    }
                }
                return selected.ToArray();// remainingPieces[m_PiecesComboBox.SelectedID];
                
                
                
                

            }

        }
       
    }
}