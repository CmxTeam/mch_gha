﻿namespace CargoMatrix.GridListBox
{
    partial class SummaryRow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.progressBar = new OpenNETCF.Windows.Forms.ProgressBar2();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.labelMode = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.Silver;
            this.splitter2.Location = new System.Drawing.Point(0, 1);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1, 13);
            // 
            // progressBar
            // 
            this.progressBar.BackColor = System.Drawing.Color.White;
            this.progressBar.BarColor = System.Drawing.Color.Lime;
            this.progressBar.BarGradientColor = System.Drawing.Color.Green;
            this.progressBar.BorderColor = System.Drawing.Color.Black;
            this.progressBar.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold);
            this.progressBar.ForeColor = System.Drawing.Color.Black;
            this.progressBar.Location = new System.Drawing.Point(86, 2);
            this.progressBar.Maximum = 100;
            this.progressBar.Minimum = 0;
            this.progressBar.Name = "progressBar";
            this.progressBar.ShowPercentValueText = true;
            this.progressBar.ShowValueText = false;
            this.progressBar.Size = new System.Drawing.Size(151, 11);
            this.progressBar.TabIndex = 6;
            this.progressBar.Value = 48;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Silver;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(240, 1);
            // 
            // labelMode
            // 
            this.labelMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMode.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelMode.Location = new System.Drawing.Point(1, 1);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(79, 13);
            this.labelMode.Text = "<mode>";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SummaryRow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.labelMode);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.splitter1);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Name = "SummaryRow";
            this.Size = new System.Drawing.Size(240, 14);
            this.ResumeLayout(false);

        }

        #endregion

        //private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label labelMode;
        public OpenNETCF.Windows.Forms.ProgressBar2 progressBar;
    }
}
