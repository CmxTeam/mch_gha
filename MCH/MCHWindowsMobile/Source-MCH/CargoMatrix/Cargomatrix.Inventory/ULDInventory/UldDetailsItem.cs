﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.InventoryWS;
using CargoMatrix.Communication.InventoryCom;

namespace CargoMatrix.Inventory
{
    public partial class UldDetailsItem : UserControl
    {
        public void DisplayULD(UldObject uld)
        {
            title.Text = uld.UldType + "-" + uld.UldNo;
            labelCarrier.Text = uld.Carrier;
            labelWeight.Text = uld.Weight.ToString();
            labelLength.Text = uld.Length.ToString("F");
            labelWidth.Text = uld.Width.ToString("F");
            labelHeight.Text = uld.Height.ToString("F");
            labelDays.Text = uld.NumberOfDays;
            labelDays.ForeColor = uld.IsNumberOfDaysCritical ? Color.Red : Color.Black;
            if (uld.Mot == MOT.Ocean)
                panelIndicators.Flags = (int)Math.Pow(2, 19);
            //labelDescr.Text = hawb.Description;
            //labelLastScan.Text = hawb.LastScan;


            //switch (hawb.Status)
            //{
            //    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Open:
            //    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Pending:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.InProgress:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
            //        break;
            //    case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Completed:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
            //        break;

            //}
            //panelIndicators.Flags = hawb.Flags;
            //panelIndicators.PopupHeader = hawb.Reference();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Visible = true;
        }

        public UldDetailsItem()
        {
            InitializeComponent();
        }

        void buttonPrint_Click(object sender, System.EventArgs e)
        {

        }

        void buttonDamage_Click(object sender, System.EventArgs e)
        {

        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {

        }
    }
}
