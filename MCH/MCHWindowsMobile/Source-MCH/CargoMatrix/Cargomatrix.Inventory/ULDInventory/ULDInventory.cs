﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;
using CargoMatrix.Communication.InventoryWS;
using CustomListItems;
using CustomUtilities;

namespace CargoMatrix.Inventory
{
    public partial class ULDInventory : CargoUtilities.SmoothListBoxOptions
    {
        private UldDetailsItem uldDetailsItem;
        private OptionsListITem uldManulScanItem;

        public ULDInventory()
        {
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(BarcodeScanned);
            this.LoadOptionsMenu += new EventHandler(ULDInventory_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(HawbInventory_MenuItemClicked);
            Forklift.Instance.ItemCountChanged += (s, e) => { buttonScannedList.Value = Forklift.Instance.ItemsCount; };
            this.Scrollable = false;
        }

        void buttonScannedList_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            new ULDScannedList().ShowDialog();
            BarcodeEnabled = true;
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            buttonScannedList.Value = Forklift.Instance.ItemsCount;
            if (!string.IsNullOrEmpty(Forklift.Instance.ScannedLocation))
            {
                labelLocation.Text = Forklift.Instance.ScannedLocation;
                topLabel.Text = InventoryResources.Text_ScanHawb;
            }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        private void InitializeComponentHelper()
        {
            panelHeader2.Height = 231;
            this.TitleText = "ULD Inventory";
        }

        private void DisplayULD(UldObject uld)
        {
            if (uldDetailsItem == null)
            {
                uldDetailsItem = new UldDetailsItem() { Location = new Point(0, 45) };
                this.panelHeader2.Controls.Add(uldDetailsItem);
            }
            uldDetailsItem.DisplayULD(uld);
            Forklift.Instance.ItemsCount = uld.TransactionStaus.TransactionRecords;

        }
        void BarcodeScanned(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Uld:
                    /// no location has been scanned
                    if (!Forklift.Instance.IsLocationScanned)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_ScanLocation, "Scan Location", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        break;
                    }
                    ScanULD(scanItem.UldNumber);
                    break;

                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    ScanLocation(scanItem.Location);
                    break;

                default:
                    ShowInvalidLocationMessage();
                    break;
            }

            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        private static void ShowInvalidLocationMessage()
        {
            CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_InvalidBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        }

        private void SetLocation(string loc)
        {
            if (string.IsNullOrEmpty(loc))
            {
                Forklift.Instance.ScannedLocation = string.Empty;
                topLabel.Text = InventoryResources.Text_ScanLoc;
                labelLocation.Text = string.Empty;
            }
            else
            {
                Forklift.Instance.ScannedLocation = loc;
                labelLocation.Text = loc;
                topLabel.Text = InventoryResources.Text_ScanHawb;
            }
            if (uldManulScanItem != null)
                uldManulScanItem.Enabled = Forklift.Instance.IsLocationScanned;
        }

        private void ScanULD(string uldNo)
        {
            CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
            var uld = CargoMatrix.Communication.Inventory.Instance.ScanUldIntoForklift(uldNo, Forklift.Instance.TaskID, Forklift.Instance.ForkliftID, ScanTypes.Automatic);
            if (uld.TransactionStaus.TransactionStatus1 == true)
            {
                DisplayULD(uld);
            }
            else
            {
                if (uld.TransactionStaus.NeedOverride)
                {
                    CreateNewUld(uldNo);
                    return;
                }
                else if (!string.IsNullOrEmpty(uld.TransactionStaus.TransactionError))
                {
                    CargoMatrix.UI.CMXMessageBox.Show(uld.TransactionStaus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                }
            }
        }

        private void ScanLocation(string location)
        {
            CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
            // if no scanned location, forklift is empty , and location is valid => set location
            if (string.IsNullOrEmpty(Forklift.Instance.ScannedLocation) || buttonScannedList.Value == 0)
            {
                if (!CargoMatrix.Communication.ScannerUtility.Instance.IsLocationValid(location))
                    ShowInvalidLocationMessage();
                else

                    SetLocation(location);
                return;
            }

            if (!string.Equals(location, Forklift.Instance.ScannedLocation))
            {
                string msg = string.Format(InventoryResources.Text_ChangeLocation, Forklift.Instance.ScannedLocation, location);
                if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Location", CargoMatrix.UI.CMXMessageBoxIcon.Hand, MessageBoxButtons.YesNo, DialogResult.Yes))
                    return;
            }


            string mesage = string.Format(InventoryResources.Text_DropConfirm, Forklift.Instance.ItemsCount, location);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(mesage, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OKCancel, DialogResult.OK))
            {
                TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.DropForkliftUldsIntoLocation(Forklift.Instance.ForkliftID, location, Forklift.Instance.TaskID);
                if (!status.TransactionStatus1)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                else
                    Forklift.Instance.ItemsCount = status.TransactionRecords;

                if (uldDetailsItem != null)
                    uldDetailsItem.Visible = false;

                SetLocation(null);
            }
        }


        private void CreateNewUld(string uldNo)
        {
            AddULDPopup uldPopup = new AddULDPopup(uldNo);
            if (DialogResult.OK == uldPopup.ShowDialog())
            {
                var uldResponse = Communication.Inventory.Instance.CreateNewUld(uldPopup.ULDNumber, uldPopup.ULDType.TypeID, uldPopup.ULDCarrier);
                if (uldResponse.TransactionStaus.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(uldResponse.TransactionStaus.TransactionError, "Error", CMXMessageBoxIcon.Hand);
                }
            }
        }


        void ULDInventory_LoadOptionsMenu(object sender, EventArgs e)
        {
            uldManulScanItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.SHOW_LOCATIONS) { Title = "Manual ULD Entry", Enabled = Forklift.Instance.IsLocationScanned };
            this.AddOptionsListItem(uldManulScanItem);
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }

        void HawbInventory_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case OptionsListITem.OptionItemID.SHOW_LOCATIONS:
                        ScanEnterPopup_old locPopup = new ScanEnterPopup_old(BarcodeType.ULD);
                        if (DialogResult.OK == locPopup.ShowDialog())
                        {
                            ScanULD(locPopup.ScannedText);
                        }
                        break;
                    default:
                        break;
                }
        }

        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            if (Forklift.Instance.ItemsCount != 0)
            {
                this.BackButtonHold = false;
                /// forklift is not empty
                CargoMatrix.UI.CMXMessageBox.Show(InventoryResources.Text_EmptyForklift, "Confirm Location", CMXMessageBoxIcon.Hand);
            }
            else
                base.pictureBoxBack_Click(sender, e);
        }

    }
}
