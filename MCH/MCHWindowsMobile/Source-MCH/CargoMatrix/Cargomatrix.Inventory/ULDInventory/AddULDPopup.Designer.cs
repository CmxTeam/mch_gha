﻿using CargoMatrix.UI;
using System.Windows.Forms;
namespace CargoMatrix.Inventory
{
    partial class AddULDPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxContainerNo = new CMXTextBox();
            this.textBoxContainerType = new CMXTextBox();
            this.buttonContainerType = new CMXPictureButton();
            this.textBoxCarrier = new CMXTextBox();
            this.SuspendLayout();
            // 
            // textBoxContainerType
            // 
            this.textBoxContainerType.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.textBoxContainerType.Location = new System.Drawing.Point(8, 28);
            this.textBoxContainerType.Name = "textBoxContainerType";
            this.textBoxContainerType.Size = new System.Drawing.Size(181, 23);
            this.textBoxContainerType.TabIndex = 1;
            this.textBoxContainerType.WatermarkText = "Select Container Type";
            this.textBoxContainerType.Enabled = false;
            this.textBoxContainerType.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // textBoxContainerNo
            // 
            this.textBoxContainerNo.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.textBoxContainerNo.Location = new System.Drawing.Point(8, 67);
            this.textBoxContainerNo.Name = "textBoxContainerNo";
            this.textBoxContainerNo.Size = new System.Drawing.Size(181, 23);
            this.textBoxContainerNo.TabStop = false;
            this.textBoxContainerNo.WatermarkText = "Enter Container Number";
            this.textBoxContainerNo.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // textBoxCarrier
            // 
            this.textBoxCarrier.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.textBoxCarrier.Location = new System.Drawing.Point(8, 106);
            this.textBoxCarrier.Name = "textBoxCarrier";
            this.textBoxCarrier.Size = new System.Drawing.Size(181, 23);
            this.textBoxCarrier.TabStop = false;
            this.textBoxCarrier.WatermarkText = "Enter Carrier Number";
            // 
            // buttonContainerType
            // 
            this.buttonContainerType.Location = new System.Drawing.Point(196, 28);
            this.buttonContainerType.Name = "buttonContainerType";
            this.buttonContainerType.Size = new System.Drawing.Size(32, 32);
            this.buttonContainerType.TabIndex = 2;
            this.buttonContainerType.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonContainerType.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonContainerType.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            this.buttonContainerType.Click += new System.EventHandler(buttonContainerType_Click);
            // 
            // AddContainerPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.panelContent.Size = new System.Drawing.Size(236, 190);
            this.panelContent.Controls.Add(this.buttonContainerType);
            this.panelContent.Controls.Add(this.textBoxContainerType);
            this.panelContent.Controls.Add(this.textBoxContainerNo);
            this.panelContent.Controls.Add(this.textBoxCarrier);
            this.Name = "AddContainerPopup";
            this.buttonOk.Enabled = false;
            this.ResumeLayout(false);

        }

        #endregion

        private CMXTextBox textBoxContainerNo;
        private CMXTextBox textBoxContainerType;
        private CMXTextBox textBoxCarrier;
        private CMXPictureButton buttonContainerType;
    }
}
