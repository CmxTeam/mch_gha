﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;

namespace CargoMatrix.Inventory
{
    public partial class DiscrepancyReport : CustomListItems.MessageListBoxAsync<CargoMatrix.Communication.InventoryWS.HouseBillItem>
    {
        public DiscrepancyReport()
        {
            InitializeComponent();
            this.HeaderText = "Discrepancy Report";
            this.HeaderText2 = "List of housebills";
            this.smoothListBoxBase1.IsSelectable = false;
            ReloadItemsAsync(CargoMatrix.Communication.Inventory.Instance.GetDiscrepancyReport(Forklift.Instance.TaskID));
        }

        protected override Control InitializeItem(CargoMatrix.Communication.InventoryWS.HouseBillItem item)
        {
            var pieces = from p in item.Pieces
                        select new CustomListItems.ComboBoxItemData(string.Format("Piece {0}",p.PieceNumber ), "Last Location: " + p.Location,p.PieceId,true);//
            Image icon = item.ScanMode == CargoMatrix.Communication.InventoryWS.ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode:CargoMatrix.Resources.Skin.countMode;
            CustomListItems.ComboBox combo = new CustomListItems.ComboBox(item.HousebillId, item.HousebillNumber, string.Format("Pieces: {0} of {1}",item.Pieces.Count(),item.TotalPieces),icon , pieces.ToList<CustomListItems.ComboBoxItemData>());
            combo.IsSelectable = false;
            combo.ReadOnly = true;
            return combo;
        }
    }
}
