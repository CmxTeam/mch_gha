﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class MicroReasons : UserControl// SmoothListbox.SmoothListBoxBase
    {
        int m_ColorCameraResolution = 4;
        public event LoadCamera LoadCameraNotify;
        private bool m_bHasCamera = true;
        public bool m_bCameraMode = false;
        public MicroReasonsListItem selectedReason;
        private CargoMatrix.Communication.DTO.FreightPhotoCapture m_photoCaptureData;

        //bool m_isExistingTask;
        public MicroReasons()
        {
            InitializeComponent();
        }

        public MicroReasons(int ColorCameraResolution)
        {
            InitializeComponent();

            m_ColorCameraResolution = ColorCameraResolution;

           
        
        }

       
        
        public void LoadReasons()
        {

            smoothListBoxReasons.RemoveAll();
            smoothListBoxReasons.LayoutItems();
            m_bHasCamera = CargoMatrix.Communication.Utilities.CameraPresent;
            
            if (m_bHasCamera == true)
                this.smoothListBoxReasons.MultiSelectEnabled = false;
            else
                smoothListBoxReasons.MultiSelectEnabled = true;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                CargoMatrix.Communication.WebServiceManager.Instance().GetExistingImage(ref m_photoCaptureData);
            }
            catch
            {
                return;
            }
            if (m_photoCaptureData != null)
            {
                
               
                this.Update();
                if (m_photoCaptureData.itemsList != null)
                {
                    smoothListBoxReasons.progressBar1.Visible = true;

                    smoothListBoxReasons.progressBar1.Minimum = -1;
                    smoothListBoxReasons.progressBar1.Maximum = m_photoCaptureData.itemsList.Count;
                    smoothListBoxReasons.progressBar1.Value = 0;
                    UpdateList();
                    
                }

                
            }


            Cursor.Current = Cursors.Default;


            smoothListBoxReasons.LayoutItems();
            
        
        }

        //private void Reasons_EnabledChanged(object sender, EventArgs e)
        //{
        //    if (Enabled == false)
        //    {
        //        this.smoothListBoxMainList.RemoveAll();
        //        smoothListBoxMainList.LayoutItems();

                
        //        labelHouseBill.Text = "";
        //        labelLine2.Text = "";
        //        labelLine3.Text = "";
        //        GC.Collect();
        //        return;
        //    }
        //    this.panelSoftkeys.Top = this.Height - panelSoftkeys.Height;
        //    this.panelSoftkeys.Width = Width;
            
        //    this.Refresh();
        //    LoadReasons();
        //    pictureBoxBack.Enabled = true;
            
        //}

        void ReasonsListItem_CameraExitNotify(Control sender)
        {
            
            //if (HasDataChanged)
            //{
            //    pictureBoxBack.Enabled = false;

            //}

            smoothListBoxReasons.Scrollable = true;


            smoothListBoxReasons.itemsPanel.Top = prevItemsLocation;
            m_bCameraMode = false;
            selectedReason = null;
            smoothListBoxReasons.LayoutItems();
            
        }

        private void buttonCameraMode_Click(object sender, EventArgs e)
        {
            if(LoadCameraNotify != null)
                LoadCameraNotify();
        }

        
       
        public bool UploadPictures()
        {
            bool result = false;
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxReasons.progressBar1.Minimum = -1;
            smoothListBoxReasons.progressBar1.Maximum = 100;
            smoothListBoxReasons.progressBar1.Value = 5;
            smoothListBoxReasons.progressBar1.Visible = true;
            smoothListBoxReasons.progressBar1.Refresh();
            CargoMatrix.Communication.DTO.FreightPhotoCapture data = new CargoMatrix.Communication.DTO.FreightPhotoCapture();
            data = m_photoCaptureData;

            data.itemsList = new List<CargoMatrix.Communication.DTO.FreightPhotoCaptureItem>();
            CargoMatrix.Communication.DTO.FreightPhotoCaptureItem item;
            foreach (Control control in smoothListBoxReasons.itemsPanel.Controls)
            {
                if (control is MicroReasonsListItem)
                {
                    item = new CargoMatrix.Communication.DTO.FreightPhotoCaptureItem();
                    item.reason = (control as MicroReasonsListItem).title.Text;
                    item.imagelist = (control as MicroReasonsListItem).GetUploadImageList();//.GetImageList();

                    data.itemsList.Add(item);
                }
            }
            smoothListBoxReasons.progressBar1.Value = 25;
            smoothListBoxReasons.progressBar1.Refresh();
            if (CargoMatrix.Communication.WebServiceManager.Instance().UploadPhotoCapture(m_photoCaptureData.taskID, data) == true)
            {
                result = true;
                //if (ExitNotify != null)
                //   ExitNotify();
                //CargoMatrix.UI.CMXAnimationmanager.GoBack();

            }
            else
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Server unable to process the request. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                smoothListBoxReasons.progressBar1.Visible = false;
                return false ;
                
            }
            smoothListBoxReasons.progressBar1.Value = 90;
            smoothListBoxReasons.progressBar1.Refresh();
            ImagesDBManager.RemoveImageFromDB(m_photoCaptureData.reference);
            Cursor.Current = Cursors.Default;
            smoothListBoxReasons.progressBar1.Value = 100;
            smoothListBoxReasons.progressBar1.Refresh();
            smoothListBoxReasons.progressBar1.Visible = false;
            smoothListBoxReasons.progressBar1.Refresh();

            return result;
        
        }
        bool m_hasDataChanged;
        public bool HasDataChanged
        {
            get
            {
                return m_hasDataChanged;
            }
            set
            {
                m_hasDataChanged = value;
                
            }

            
        
        }
        bool HasPictures
        {
            get
            {
                bool dataFlag = false;
                foreach (Control control in smoothListBoxReasons.itemsPanel.Controls)
                {
                    if (control is ReasonsListItem)
                    {

                        if ((control as ReasonsListItem).GetImageList() != null)
                        {
                            dataFlag = true;
                            break;
                        }

                    }
                }
                return dataFlag;
            
            }
        }
        
        public CargoMatrix.Communication.DTO.FreightPhotoCapture PhotoCaptureData
        {
            set
            {
               m_photoCaptureData = value;
              

            }
            get { return m_photoCaptureData; }
        }
        
        int threadIndex = 0;
        void UpdateList()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            
            for (threadIndex = 0; threadIndex < m_photoCaptureData.itemsList.Count; threadIndex++)
            {
                this.Invoke(new EventHandler(WorkerUpdate));
                
            }
            
                

            this.Invoke(new EventHandler(WorkerEndUpdate));
            
            Cursor.Current = Cursors.Default;

        }
        public void WorkerUpdate(object sender, EventArgs e)
        {

            //for (int i = 0; i < m_tasks.Length; i++)
            {
                MicroReasonsListItem temp = new MicroReasonsListItem(m_photoCaptureData.itemsList[threadIndex].reason, this, m_photoCaptureData.itemsList[threadIndex].ReasonType, m_bHasCamera);//
                temp.Enabled = false;

                smoothListBoxReasons.AddItem2(temp);
                if (m_photoCaptureData.itemsList[threadIndex].imagelist.Count > 0)
                {
                    using (System.IO.MemoryStream stream = new System.IO.MemoryStream(m_photoCaptureData.itemsList[threadIndex].imagelist[0].m_rawImage))
                    {
                        temp.InsertOuterImage(new Bitmap(stream));
                    }
                    
                }
                temp.CameraExitNotify += new ExitEventHandler(ReasonsListItem_CameraExitNotify);

                //if (threadIndex % 5 == 0)
                {

                    smoothListBoxReasons.progressBar1.Value = threadIndex;
                    smoothListBoxReasons.LayoutItems();
                    smoothListBoxReasons.Update();
                    
                }
            }

        }

        public void WorkerEndUpdate(object sender, EventArgs e)
        {
            smoothListBoxReasons.LayoutItems();
            smoothListBoxReasons.Update();
            smoothListBoxReasons.progressBar1.Visible = false;


            for (int i = 0; i < smoothListBoxReasons.itemsPanel.Controls.Count; i++)
                smoothListBoxReasons.itemsPanel.Controls[i].Enabled = true;

            smoothListBoxReasons.RefreshScroll();

        }

        public int MainControlIndex(Control control)
        {
            if (smoothListBoxReasons.itemsPanel.Controls.Contains(control))
                return smoothListBoxReasons.itemsPanel.Controls.GetChildIndex(control);
            return -1;
        }
        public int ColorCameraResolution
        {
            set
            {
                m_ColorCameraResolution = value;
                if (selectedReason != null)
                {
                    selectedReason.microPhotoCaptureControl.SetColorCameraResolution();
                    
                }
            }
            get
            {
                return m_ColorCameraResolution;
            
            }
        }
        int prevItemsLocation;

        private void smoothListBoxReasons_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (m_bHasCamera == true)
            {
                if (m_bCameraMode == false)
                {
                    if (listItem is MicroReasonsListItem)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        CargoMatrix.UI.CMXAnimationmanager.RefreshHeader();
                        smoothListBoxReasons.Scrollable = false;
                        prevItemsLocation = smoothListBoxReasons.itemsPanel.Top;

                        smoothListBoxReasons.selectedItemsMap[listItem] = false;

                        (listItem as MicroReasonsListItem).ShowCamera();
                        //LayoutItems();

                        //smoothListBoxMainList.MoveControlToTop(listItem);
                        //listItem.ResumeLayout();

                        m_bCameraMode = true;
                        selectedReason = listItem as MicroReasonsListItem;
                        //selectedReason.threadSyncControl = pictureBoxMenu;
                        selectedReason.Focus(false);
                        //selectedReason.miniPhotoCaptureControl.FilmBackColor = thumbnailBackColor;// Color.DarkRed;
                        //this.pictureBoxBack.Enabled = true;

                        Cursor.Current = Cursors.Default;

                    }
                }

                Cursor.Current = Cursors.Default;
            }
            else
            {
                HasDataChanged = true;
            
            }
        }

        

    }
}
