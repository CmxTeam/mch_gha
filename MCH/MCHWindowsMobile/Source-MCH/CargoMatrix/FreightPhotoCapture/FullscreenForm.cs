﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class FullscreenForm : Form
    {
        
        public FullscreenForm()
        {
            InitializeComponent();
            
        }

        private void FullscreenForm_KeyDown(object sender, KeyEventArgs e)
        {
            
            
            this.Close();

        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            
            this.Close();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            DeletePicture();
            base.OnClosing(e);
        }
        public Image Image
        {
            set
            {
                DeletePicture();
                using (Bitmap bmp = new Bitmap(value))
                {
                    Bitmap rotated = OpenNETCF.Drawing.Imaging.ImageUtils.Rotate(bmp, 90);
                    pictureBox1.Image = rotated;
                }
            }
        }
        private void DeletePicture()
        {
            if (pictureBox1.Image != null)
            {
                pictureBox1.Image.Dispose();
                pictureBox1.Image = null;
            }
        }


        Font picFont = new Font(FontFamily.GenericSerif, 8, FontStyle.Regular);
        SolidBrush picBrush = new SolidBrush(Color.Red);
        string picString = PhotoCaptureResource.Text_FullScreenDialogText;
        Pen pen = new Pen(Color.Red);

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            
            SizeF size = e.Graphics.MeasureString(picString, picFont);
            e.Graphics.DrawString(picString, picFont, picBrush, (pictureBox1.Width - (int)size.Width) / 2, 1);
            
        }
    }
}