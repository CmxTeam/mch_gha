﻿using System;

using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.FreightPhotoCapture
{
    public delegate void ImageReadNotifyHandler(System.IO.MemoryStream stream);
    abstract public class CMXCameraBase : System.Windows.Forms.UserControl
    {
        public abstract event ImageReadNotifyHandler ImageReadNotify;
        public abstract System.Drawing.Image image
        {
            get;
            set;
        }
        
        public abstract void StopAcquisition();
        public abstract void StartAcquisition();
        public abstract void FullScreen(bool goFullScreen);
    }
}
