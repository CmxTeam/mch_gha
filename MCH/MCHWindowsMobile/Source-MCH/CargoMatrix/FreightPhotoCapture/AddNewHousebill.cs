﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightPhotoCapture
{
   
    public partial class AddNewHousebill : SmoothListbox.SmoothListbox// UserControl
    {
        
        //CargoMatrix.FreightPhotoCapture.PhotoCaptureControl2 photoCaptureControl;
        //ListAllReasons reasonsList;
        Reasons reasonsControl;

        bool needFocus = true;
        
        public AddNewHousebill()
        {
            InitializeComponent();
            pictureBox1.Image = PhotoCaptureResource.scan2_small;
            Location = new Point(CargoMatrix.UI.CMXAnimationmanager.GetParent().Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            Size = new Size(CargoMatrix.UI.CMXAnimationmanager.GetParent().Width,
                CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            smoothListBoxMainList.Height = 0;
            smoothListBoxMainList.Visible = false;
           

            cmxTextBox1.Focus();
            //Disposed += new EventHandler(NewPhotoCaptureTask_Disposed);
            pictureBox1.Visible = false;
            buttonClear.Visible = false;
            buttonDamage.Visible = false;
            //buttonLogin.Visible = false;
            cmxTextBox1.Visible = false;
            label1.Visible = false;
            label2.Visible = false;

            this.TitleText = PhotoCaptureResource.Text_AddNewHousebill;
            //pictureBoxMenu.Enabled = false;
                
        }

        //void NewPhotoCaptureTask_Disposed(object sender, EventArgs e)
        //{
        //    //throw new NotImplementedException();
        //}

        private void buttonClear_Click(object sender, EventArgs e)
        {
            cmxTextBox1.Text = "";
            cmxTextBox1.Focus();

        }

        //private void buttonLogin_Click(object sender, EventArgs e)
        //{
        //    if (cmxTextBox1.Text == "")
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("You must scan or enter barcode manually to proceed.", "Barcode cannot be Empty", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //        return;

        //    }
        //    Cursor.Current = Cursors.WaitCursor;
            
        //    if (photoCaptureControl == null)
        //    {
        //        photoCaptureControl = new PhotoCaptureControl2(cmxTextBox1.Text);
        //    }
        //    photoCaptureControl.ControlType = PhotoCaptureControl2.PhotoCaptureControlType.NO_REASONS;
        //    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(photoCaptureControl);
        //}

        private void NewPhotoCaptureTask_BarcodeReadNotify(string barcodeData)
        {
            //Cursor.Current = Cursors.WaitCursor;
            //if (barcodeData == null)
            //{
            //    CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode.", "Invalid Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            //    barcode.StartRead();
            //    return;
            //}
            //cmxTextBox1.Text = barcodeData;

            //CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
            //barcode.StartRead();
            //this.buttonDamage_Click(this, EventArgs.Empty);

            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
            CargoMatrix.Communication.DTO.FreightPhotoCapture tempPhotoCapture;
            Cursor.Current = Cursors.WaitCursor;
            System.Threading.Thread.Sleep(0);
            string tempBarcode;// = barcodeData;
            string carrier = null, origMB;
            CargoMatrix.Communication.DTO.TaskType taskType;
            if (TaskList.IsMasterBill(barcodeData, out carrier, out origMB))
            //if (CargoMatrix.Utilities.BarcodeParser.IsMasterBill(barcodeData))
            {
                //tempBarcode = origMB;
                //taskType = TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL;
                CargoMatrix.UI.CMXMessageBox.Show("Error! You are scanning a masterbill barcode in \'Add new Housebill\'.", "Invalid barcode.", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn())
                    barcode.StartRead();
                return;
            }
            else
            {
                tempBarcode = CMXBarcode.BarcodeParser.FixNumber(barcodeData);
                taskType = CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL;

            }
            if (tempBarcode == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode.", "Invalid Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                barcode.StartRead();
                return;
            }
            if (ReasonsLogic.CreateTask(carrier, tempBarcode, taskType, this, ref reasonsControl) == false)
            {
                if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn())
                    barcode.StartRead();
            }
        }

        private void NewPhotoCaptureTask_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled == false)
            {
                cmxTextBox1.Text = "";
                pictureBox1.Visible = false;
                buttonClear.Visible = false;
                buttonDamage.Visible = false;
                //buttonLogin.Visible = false;
                cmxTextBox1.Visible = false;
                label1.Visible = false;
                label2.Visible = false;

                
            }

            else
            {
                //if (photoCaptureControl != null)
                //{
                //    photoCaptureControl.Dispose();
                //    photoCaptureControl = null;
                //}

                if (reasonsControl!= null)
                {
                    reasonsControl.Dispose();
                    reasonsControl = null;
                }

                pictureBox1.Visible = true;
                buttonClear.Visible = true;
                buttonDamage.Visible = true;
                //buttonLogin.Visible = true;
                cmxTextBox1.Visible = true;
                label1.Visible = true;
                label2.Visible = true;
                Application.DoEvents();
                cmxTextBox1.Focus();
            }
            needFocus = Enabled;
        }

        private void cmxTextBox1_GotFocus(object sender, EventArgs e)
        {
            
            cmxTextBox1.SelectAll();
        }

        private void buttonDamage_Click(object sender, EventArgs e)
        {
            if (cmxTextBox1.Text == "")
            {
                //CargoMatrix.UI.CMXMessageBox.Show("You must scan or enter barcode manually to proceed.", "Barcode cannot be Empty", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;

            }
            Cursor.Current = Cursors.WaitCursor;
            ReasonsLogic.CreateTask("", cmxTextBox1.Text, CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL, this, ref reasonsControl);
            



        }

        private void cmxTextBox1_EnabledChanged(object sender, EventArgs e)
        {
            cmxTextBox1.Focus();
            cmxTextBox1.Invalidate();


        }

        private void AddNewHousebill_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
        }

        private void AddNewHousebill_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.LOGOUT)
            {
                if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                {

                    //AbortAllThreads();
                    BarcodeEnabled = false;
                    CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                }
            }
        }

        private void AddNewHousebill_Paint(object sender, PaintEventArgs e)
        {
            if (this.Enabled)
            {
                if (needFocus)
                {
                    needFocus = false;
                    cmxTextBox1.Focus();

                }

            }
        }
        
    }
}
