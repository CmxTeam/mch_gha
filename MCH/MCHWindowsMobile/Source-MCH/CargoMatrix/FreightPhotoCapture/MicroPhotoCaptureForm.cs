﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class MicroPhotoCaptureForm : Form
    {
        bool bFirstTimeLoad = false;
        string m_peiceInfo;
        public string HeaderText;
        //bool m_bHasCamera = false;
        
        public MicroPhotoCaptureForm(CargoMatrix.Communication.DTO.FreightPhotoCapture PhotoCapture, string pcsInfo)
        {
            InitializeComponent();
            microReasons1.smoothListBoxReasons.AutoScroll += new SmoothListbox.AutoScrollHandler(smoothListBoxReasons_AutoScroll);
            BackColor = global::Resources.Graphics.Skin.thumbnailBackColor.GetPixel(0, 0);
            pictureBoxMenuBack.Image = global::Resources.Graphics.Skin.nav_bg;
            pictureBoxHeader.Image = global::Resources.Graphics.Skin.popup_header;
            pictureBoxOk.Image = global::Resources.Graphics.Skin.nav_ok;
            pictureBoxCancel.Image = global::Resources.Graphics.Skin.nav_cancel;
            pictureBoxUp.Image = global::Resources.Graphics.Skin.nav_up;
            pictureBoxDown.Image = global::Resources.Graphics.Skin.nav_down;
            microReasons1.PhotoCaptureData = PhotoCapture;
            m_peiceInfo = pcsInfo;
        }

        void smoothListBoxReasons_AutoScroll(SmoothListbox.SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            switch (direction)
            {
                case SmoothListbox.SmoothListBoxBase.DIRECTION.UP:
                    pictureBoxUp.Enabled = enable;
                    break;
                case SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN:
                    pictureBoxDown.Enabled = enable;
                    break;
            }
            //throw new NotImplementedException();
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {



                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));
                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }


            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (bFirstTimeLoad == false)
            {
                bFirstTimeLoad = true;
                HeaderText = "HB# " + microReasons1.PhotoCaptureData.reference + ", PCS: " + m_peiceInfo;
                Application.DoEvents();
                microReasons1.LoadReasons();
            }
        }
        //private void buttonOk_Click(object sender, EventArgs e)
        //{
        //    if (microReasons1.m_bCameraMode)
        //    {
        //        microReasons1.selectedReason.GoBack();
        //    }
        //    else
        //    {
        //        if(microReasons1.UploadPictures())
        //            DialogResult = DialogResult.OK;
        //    }
        //}

        private void MicroPhotoCaptureForm_Load(object sender, EventArgs e)
        {
            //microReasons1.LoadReasons();
        }

        //private void buttonUp_KeyDown(object sender, KeyEventArgs e)
        //{
        //    microReasons1.smoothListBoxReasons.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);
            

        //}

        //private void buttonUp_KeyUp(object sender, KeyEventArgs e)
        //{
        //    microReasons1.smoothListBoxReasons.UpButtonPressed = false;

        //}

        //private void buttonDown_KeyDown(object sender, KeyEventArgs e)
        //{
        //    microReasons1.smoothListBoxReasons.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);
        //}

        //private void buttonDown_KeyUp(object sender, KeyEventArgs e)
        //{
        //    microReasons1.smoothListBoxReasons.DownButtonPressed = false;
        //}
        Font HeaderFont = new Font(FontFamily.GenericSerif, 8, FontStyle.Bold);
        SolidBrush HeaderBrush = new SolidBrush(Color.White);
        private void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawString(HeaderText, HeaderFont, HeaderBrush,3, 4);
        }

        private void pictureBoxOk_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = global::Resources.Graphics.Skin.nav_ok_over;
        }

        private void pictureBoxOk_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = global::Resources.Graphics.Skin.nav_ok;
        }

        private void pictureBoxOk_Click(object sender, EventArgs e)
        {
            pictureBoxOk.Image = global::Resources.Graphics.Skin.nav_ok_over;
            pictureBoxOk.Refresh();
            //if (m_bHasCamera == true)
            //{
                if (microReasons1.m_bCameraMode)
                {
                    microReasons1.selectedReason.GoBack();
                }
                else
                {
                    if (microReasons1.HasDataChanged)
                    {
                        if (microReasons1.UploadPictures())
                            DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        DialogResult = DialogResult.OK;
                    }
                }
            //}
            //else
            //{
            //    if (microReasons1.HasDataChanged)
            //    { 
            //        //create photo capture task.
            //    }
            //}

        }

        private void pictureBoxCancel_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = global::Resources.Graphics.Skin.nav_cancel_over;

        }

        private void pictureBoxCancel_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = global::Resources.Graphics.Skin.nav_cancel;
        }

        private void pictureBoxCancel_Click(object sender, EventArgs e)
        {
            pictureBoxCancel.Image = global::Resources.Graphics.Skin.nav_cancel_over;
            pictureBoxCancel.Refresh();

            if (microReasons1.m_bCameraMode)
            {
                microReasons1.selectedReason.GoBack();
            }
            else
            {
                if (microReasons1.HasDataChanged)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(PhotoCaptureResource.Text_ExitCameraWarning,
                               PhotoCaptureResource.Text_ConfirmExit, CargoMatrix.UI.CMXMessageBoxIcon.Hand,
                               MessageBoxButtons.OKCancel, DialogResult.Cancel))
                    {
                        ImagesDBManager.RemoveImageFromDB(microReasons1.PhotoCaptureData.reference);
                       
                        DialogResult = DialogResult.Cancel;

                    }
                }
                else
                {
                    DialogResult = DialogResult.Cancel;
                }
            }
        }

        private void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxUp.Image = global::Resources.Graphics.Skin.nav_up_over;
            microReasons1.smoothListBoxReasons.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);

        }

        private void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxUp.Image = global::Resources.Graphics.Skin.nav_up;
            microReasons1.smoothListBoxReasons.UpButtonPressed = false;
        }

        //private void pictureBoxUp_Click(object sender, EventArgs e)
        //{
        //    pictureBoxUp.Image = PhotoCaptureResource.nav_up_over;
        //    pictureBoxUp.Refresh();
        //}

        private void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxDown.Image = global::Resources.Graphics.Skin.nav_down_over;
            microReasons1.smoothListBoxReasons.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);

        }

        private void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxDown.Image = global::Resources.Graphics.Skin.nav_down;
            microReasons1.smoothListBoxReasons.DownButtonPressed = false;
        }

        //private void pictureBoxDown_Click(object sender, EventArgs e)
        //{
        //    pictureBoxDown.Image = PhotoCaptureResource.nav_down_over;
        //    pictureBoxDown.Refresh();
        //}

        private void pictureBoxUp_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxUp.Enabled)
                pictureBoxUp.Image = global::Resources.Graphics.Skin.nav_up;
            else
            {
                pictureBoxUp.Image = global::Resources.Graphics.Skin.nav_up_dis;
                microReasons1.smoothListBoxReasons.UpButtonPressed = false;
            }

        }

        private void pictureBoxDown_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxDown.Enabled)
                pictureBoxDown.Image = global::Resources.Graphics.Skin.nav_down;
            else
            {
                pictureBoxDown.Image = global::Resources.Graphics.Skin.nav_down_dis;
                microReasons1.smoothListBoxReasons.DownButtonPressed = false;
            }

        }
       
    }
}