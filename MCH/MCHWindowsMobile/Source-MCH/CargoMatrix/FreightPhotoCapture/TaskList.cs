﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using System.Threading;

namespace CargoMatrix.FreightPhotoCapture
{
    public delegate void ExitEventHandler(Control sender);

    public partial class TaskList : SmoothListbox.SmoothListbox// UserControl
    {

        public string ReferenceData = string.Empty;
        Reasons reasonsControl;
        Thread m_thread;
        Viewer.HousebillViewer billViewer;

        //Thread m_updateThread;
        CargoMatrix.UI.CMXUserControl m_newPhotoCaptureTask;
        string m_filter = "Not Completed";
        //CustomListItems.OptionsListITem m_optionsRefresh;
        public TaskList()
        {
            InitializeComponent();

            TitleText = "CargoPhotoCapture " + "(" + m_filter + ")";
        }
        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            if (Enabled == false)
            {
                AbortAllThreads();
                smoothListBoxMainList.RemoveAll();
            }
            else
            {
                if (billViewer != null)
                {
                    billViewer.Dispose();
                    billViewer = null;

                }
                if (reasonsControl != null)
                {
                    reasonsControl.Dispose();
                    reasonsControl = null;
                }
                if (m_newPhotoCaptureTask != null)
                {
                    m_newPhotoCaptureTask.Dispose();
                    m_newPhotoCaptureTask = null;
                }

                ReloadList();
                if (BarcodeEnabled == false)
                    this.BarcodeEnabled = true;
            }
            //ReloadList();
        }
        //protected override void InitSmoothListBox(object sender, EventArgs e)
        //{
        //    base.InitSmoothListBox(sender, e);
        //    //TitleText = "Cargo Photo Capture " + "(" + m_filter + ")";

        //    Top = CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight;
        //    panelSoftkeys.Enabled = true;

        //    //pictureBoxMenu.Click += new EventHandler(pictureBoxMenu_Click);
        //    m_optionsRefresh = new CustomListItems.OptionsListITem("Refresh List", PhotoCaptureResource.Refresh);
        //    AddOptionsListItem(m_optionsRefresh);
        //    AddOptionsListItem(new CustomListItems.OptionsListITem("Return to Main Menu", PhotoCaptureResource.Window_View_Details));
        //    SmoothListbox.ListItems.OptionsListNode temp3 = new SmoothListbox.ListItems.OptionsListNode("Filters", PhotoCaptureResource.Symbol_Search);




        //    temp3.ListType = SmoothListbox.ListItems.OptionsListNode.ListNodeTye.SINGLE_WITH_APPLY_BUTTON;

        //    temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Not Completed"), true);
        //    temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Not Started"));
        //    temp3.AddItem(new SmoothListbox.ListItems.CheckBox("In Progress"));
        //    temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Completed"));
        //    temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Show All"));


        //    AddOptionsListItem(temp3);
        //    AddOptionsListItem(new CustomListItems.OptionsListITem("Logout", "Logout"));
        //    temp3.ApplyNodeClicked += new SmoothListbox.ListItems.OptionsListNode.ApplyNodeClickedHandler(temp3_ApplyNodeClicked);

        //}
        /// <summary>
        /// Needs re-implementation
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="values"></param>
        void temp3_ApplyNodeClicked(string nodeName, List<Control> values)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (nodeName == "Filters")
            {
                this.DisappearOptionsMenu();
                Refresh();
                if (values != null && values.Count > 0)
                {
                    switch ((values[0] as SmoothListbox.ListItems.CheckBox).ID)
                    {
                        case 0:
                            m_filter = "Not Completed";
                            break;
                        case 1:
                            m_filter = "Not Started";
                            break;
                        case 2:
                            m_filter = "In Progress";
                            break;
                        case 3:
                            m_filter = "Completed";
                            break;
                        case 4:
                            m_filter = "Show All";
                            break;
                        case 5:
                            m_filter = "Not Assigned";
                            break;

                    }
                    TitleText = "Cargo Photo Capture " + "(" + m_filter + ")";

                    this.Enabled = false;
                    this.Enabled = true;

                }
            }
            LoadControl();
        }

        public bool DisplayReasons(CargoMatrix.Communication.DTO.FreightPhotoCapture photoCaptureData)
        {
            return ReasonsLogic.CreateTask(photoCaptureData.carrier, photoCaptureData.actualBill, photoCaptureData.taskType, this, ref reasonsControl);
		}
        void TaskList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        {

            if (listItem is SmoothListbox.ListItems.StandardListItem)
            {


                if ((listItem as SmoothListbox.ListItems.StandardListItem).ID == 101) // add new housebill
                {
                    Cursor.Current = Cursors.WaitCursor;
                    System.Threading.Thread.Sleep(500);

                    if (m_newPhotoCaptureTask != null)
                    {
                        m_newPhotoCaptureTask.Dispose();
                        m_newPhotoCaptureTask = null;
                    }
                    m_newPhotoCaptureTask = new AddNewHousebill();

                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_newPhotoCaptureTask);

                }
                else if ((listItem as SmoothListbox.ListItems.StandardListItem).ID == 102) // add new masterbill
                {
                    Cursor.Current = Cursors.WaitCursor;
                    System.Threading.Thread.Sleep(500);

                    if (m_newPhotoCaptureTask != null)
                    {
                        m_newPhotoCaptureTask.Dispose();
                        m_newPhotoCaptureTask = null;
                    }
                    m_newPhotoCaptureTask = new AddNewMasterbill();

                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_newPhotoCaptureTask);

                }
                else if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                {
                    if (listItem is FreightPhotoCaptureTaskItem)
                    {

                        ReasonsLogic.CreateTask((listItem as FreightPhotoCaptureTaskItem).PhotoCaptureData.carrier, (listItem as FreightPhotoCaptureTaskItem).PhotoCaptureData.actualBill, (listItem as FreightPhotoCaptureTaskItem).PhotoCaptureData.taskType, this, ref reasonsControl);
                    }
                }
                else
                    smoothListBoxMainList.RefreshScroll();

            }

        }
        CargoMatrix.Communication.DTO.FreightPhotoCapture[] m_tasks = null;
        private void TaskList_EnabledChanged(object sender, EventArgs e)
        {



        }

        private void TaskList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.LOGOUT)
                {
                    //if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to logout?",
                    //   "Confirm Logout", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                    //   MessageBoxButtons.OKCancel, DialogResult.Cancel))
                    if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                    {

                        AbortAllThreads();
                        BarcodeEnabled = false;
                        CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                    }
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.MAIN_MENU)
                {
                    AbortAllThreads();
                    BarcodeEnabled = false;

                    if (CargoMatrix.UI.CMXAnimationmanager.CanGoBack())
                    {
                        this.Enabled = false;
                        CargoMatrix.UI.CMXAnimationmanager.GoBack();
                    }
                    else
                    {
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to exit Cargo Photo Capture?",
                           "Exit Cargo Photo Capture", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                           MessageBoxButtons.OKCancel, DialogResult.Cancel))
                        {
                            //AbortAllThreads();
                            //BarcodeEnabled = false;
                            CargoMatrix.UI.CMXAnimationmanager.ExitForm();//.GetParent().Close();
                        }
                    }

                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.REFRESH)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    ReloadList();
                }
                //else if ((listItem as CustomListItems.OptionsListITem).title.Text == "Refresh List*")
                //{
                //    ReloadList();

                //}


            }

        }
        void AbortAllThreads()
        {
            if (m_thread != null)
            {
                m_thread.Abort();
                m_thread = null;
                smoothListBoxMainList.progressBar1.Visible = false;
            }
            //if (m_updateThread != null)
            //{
            //    m_updateThread.Abort();
            //    m_updateThread = null;
            //    updateListTimer.Enabled = false;
            //}
        }
        void ReloadList()
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxMainList.RemoveAll();


            AddMainListItem(new SmoothListbox.ListItems.StandardListItem(PhotoCaptureResource.Text_AddNewHousebill, PhotoCaptureResource.Digital_Camera_Add, 101));
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem(PhotoCaptureResource.Text_AddNewMasterbill, PhotoCaptureResource.Digital_Camera_Add, 102));


            if (CargoMatrix.Communication.WebServiceManager.Instance().GetFreightPhotoCaptureTasks(out m_tasks, m_filter))
            {

                smoothListBoxMainList.progressBar1.Visible = true;
                smoothListBoxMainList.progressBar1.Minimum = -1;
                smoothListBoxMainList.progressBar1.Maximum = m_tasks.Length;
                smoothListBoxMainList.progressBar1.Value = 0;

                threadIndex = 0;

                ThreadStart starter = new ThreadStart(UpdateList);
                m_thread = new Thread(starter);
                m_thread.Priority = ThreadPriority.BelowNormal;
                m_thread.Start();

            }
            Refresh();

            //Cursor.Current = Cursors.Default;
        }
        private bool GetHouseBillTaskDetails(string housebill, out CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {
            photoCapture = null;// new CargoMatrix.Communication.Data.FreightPhotoCapture();
            int result = CargoMatrix.Communication.WebServiceManager.Instance().GetLastFPCHouseBillTask(housebill, out photoCapture);

            switch (result)
            {
                case 1:
                    return true;
                //break;
                case 2:
                    if (CargoMatrix.UI.CMXMessageBox.Show("This task is assigned to a different user. Would you like to continue?", "Alert!", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Cancel) == DialogResult.OK)
                    {
                        CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(photoCapture.taskID);
                        return true;

                    }
                    break;
                default:

                    break;

            }

            return false;
        }
        private void TaskList_BarcodeReadNotify(string barcodeData)
        {
            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
            Cursor.Current = Cursors.WaitCursor;
            System.Threading.Thread.Sleep(0);
            string tempBarcode;// = barcodeData;
            string carrier = null, origMB;
            CargoMatrix.Communication.DTO.TaskType taskType;
            if (TaskList.IsMasterBill(barcodeData, out carrier, out origMB))
            {
                tempBarcode = origMB;
                taskType = TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL;
            }
            else
            {
                tempBarcode = CMXBarcode.BarcodeParser.FixNumber(barcodeData);
                taskType = TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL;

            }


            if (tempBarcode == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode.", "Invalid Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                barcode.StartRead();
                return;
            }


            if (ReasonsLogic.CreateTask(carrier, tempBarcode, taskType, this, ref reasonsControl) == false)
            {
                if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn())
                    barcode.StartRead();
            }

        }
        int threadIndex = 0;

        void UpdateList()
        {
            threadIndex = 0;
            for (int i = 0; i < m_tasks.Length; i++)
                this.Invoke(new EventHandler(WorkerUpdate));

            this.Invoke(new EventHandler(WorkerEndUpdate));


        }
        public void WorkerUpdate(object sender, EventArgs e)
        {

            //for (int i = 0; i < m_tasks.Length; i++)
            {
                smoothListBoxMainList.AddItem2(new FreightPhotoCaptureTaskItem(m_tasks[threadIndex++], this));
                //this.listBox1.Items.Add(this.Message);
                //this.listBox1.Update();
                if (threadIndex % 2 == 0)
                {

                    smoothListBoxMainList.progressBar1.Value = threadIndex;
                    smoothListBoxMainList.LayoutItems();
                    smoothListBoxMainList.Update();
                    smoothListBoxMainList.RefreshScroll();
                    Cursor.Current = Cursors.Default;
                }
            }

        }

        public void WorkerEndUpdate(object sender, EventArgs e)
        {
            smoothListBoxMainList.LayoutItems();
            smoothListBoxMainList.Update();
            smoothListBoxMainList.RefreshScroll();
            smoothListBoxMainList.progressBar1.Visible = false;
            //updateListTimer.Enabled = true;

        }
		      

        protected override void SetBackButton()
        {
            pictureBoxBack.Enabled = true;

        }
        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            pictureBoxBack.Refresh();
            if (m_bOptionsMenu)
                base.pictureBoxBack_Click(sender, e);
            else
            {
                AbortAllThreads();
                BarcodeEnabled = false;

                if (CargoMatrix.UI.CMXAnimationmanager.CanGoBack())
                {
                    this.Enabled = false;
                    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                }
                else
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to exit Cargo Photo Capture?",
                               "Exit Cargo Photo Capture", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation,
                               MessageBoxButtons.OKCancel, DialogResult.Cancel))
                    {
                        //AbortAllThreads();
                        //BarcodeEnabled = false;
                        CargoMatrix.UI.CMXAnimationmanager.ExitForm();//.GetParent().Close();
                    }
                }
            }

        }

        private void TaskList_LoadOptionsMenu(object sender, EventArgs e)
        {
            Top = CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight;
            panelSoftkeys.Enabled = true;

           
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MAIN_MENU));
            SmoothListbox.ListItems.OptionsListNode temp3 = new SmoothListbox.ListItems.OptionsListNode("Filters", "Filter");




            temp3.ListType = SmoothListbox.ListItems.OptionsListNode.ListNodeTye.SINGLE;

            temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Not Completed") { ID = 0 }, true);
            temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Not Started") { ID = 1 });
            temp3.AddItem(new SmoothListbox.ListItems.CheckBox("In Progress") { ID = 2 });
            temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Completed") { ID = 3 });
            temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Show All") { ID = 4 });
            temp3.AddItem(new SmoothListbox.ListItems.CheckBox("Not Assigned") { ID = 5 });



            AddOptionsListItem(temp3);
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
            temp3.ApplyNodeClicked += new SmoothListbox.ListItems.OptionsListNode.ApplyNodeClickedHandler(temp3_ApplyNodeClicked);


        }

        //public void DisplayHousebillViewer(string housebill)
        //{
        //    if (billViewer != null)
        //        billViewer.Dispose();
        //    billViewer = new CargoMatrix.Viewer.HousebillViewer(housebill);
        //}
        public void DisplayBillViewer(string carrier, string masterbill)
        {
            if (billViewer != null)
                billViewer.Dispose();
            billViewer = new CargoMatrix.Viewer.HousebillViewer(carrier, masterbill);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);// CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }


        public static bool IsMasterBill(string barcode, out string carrier, out string originalMasterBill)
        {
            carrier = null;
            originalMasterBill = null;
            
            var scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcode);
            
            if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.MasterBill)
            {
                carrier = scanObj.CarrierNumber;
                originalMasterBill = scanObj.MasterBillNumber;
                return true;
            }

            try
            {
                if (string.IsNullOrEmpty(barcode))
                    return false;

                if (barcode.Length != 16 && barcode.Length != 17)
                    return false;

                int cursor = 0;

                if (barcode.StartsWith("M", StringComparison.OrdinalIgnoreCase))
                    cursor++;
                
                for (int i = cursor; i < barcode.Length; i++)
                {
                    if (!char.IsDigit(barcode[cursor]))
                        return false;
                }
                carrier = barcode.Substring(cursor, 3);
                originalMasterBill = barcode.Substring(cursor + 3, 8);

            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
