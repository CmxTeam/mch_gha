﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CargoMatrix.FreightPhotoCapture
{
    public delegate void LoadCamera();
    public delegate void UploadPhotoCapture( CargoMatrix.Communication.DTO.FreightPhotoCapture PhotoCaptureData);
    public partial class Reasons : SmoothListbox.SmoothListbox
    {
        //public Color thumbnailBackColor;
        int m_ColorCameraResolution = 4;
        private CustomListItems.OptionsListITem completeTask, completeTaskLater;
        private CustomListItems.OptionsListITem exitwithoutSavingMenuItem;
        private CustomListItems.OptionsListITem deleteTask;
        SmoothListbox.ListItems.OptionsListNode resolutionItem;
        public event LoadCamera LoadCameraNotify;
        
        bool m_bCameraMode = false;
        ReasonsListItem selectedReason;
        private CargoMatrix.Communication.DTO.FreightPhotoCapture m_photoCaptureData;

        bool m_isExistingTask;

        //RDF
        bool ShowReasons = true;
        string HawbNo = string.Empty;
        string Destination = string.Empty;
        string Origin = string.Empty;
        public void LoadCamera()
        {

            foreach (var listItem in smoothListBoxMainList.Items)
            {
                if (m_bCameraMode == false)
                {
                    if (listItem is ReasonsListItem)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        BarcodeEnabled = false;
                        CargoMatrix.UI.CMXAnimationmanager.RefreshHeader();
                        Scrollable = false;
                        prevItemsLocation = smoothListBoxMainList.itemsPanel.Top;

                        //smoothListBoxMainList.selectedItemsMap[listItem] = false;

                        (listItem as ReasonsListItem).ShowCamera();
                        //LayoutItems();

                        //smoothListBoxMainList.MoveControlToTop(listItem);
                        //listItem.ResumeLayout();

                        m_bCameraMode = true;
                        selectedReason = listItem as ReasonsListItem;
                        selectedReason.threadSyncControl = pictureBoxMenu;
                        selectedReason.Focus(false);
                        //selectedReason.miniPhotoCaptureControl.FilmBackColor = thumbnailBackColor;// Color.DarkRed;
                        this.pictureBoxBack.Enabled = true;

                        Cursor.Current = Cursors.Default;

                    }

                    break;

                }

                Cursor.Current = Cursors.Default;
            }

    
        }


        private static int GetHouseBillTask(string housebill, out CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {
            photoCapture = null;
            return CargoMatrix.Communication.WebServiceManager.Instance().GetLastFPCHouseBillTask(housebill, out photoCapture);

        }
        public void LoadAgain(string hawbNo, string origin, string destination)
        {
            LoadAgain(hawbNo, origin, destination, false);
        }
        public void LoadAgain(string hawbNo, string origin, string destination,bool showReasons)
        {
            this.ShowReasons = showReasons;
            this.HawbNo = hawbNo;
            this.Destination = destination;
            this.Origin = origin;
            if (ReasonsLogic.CreateNewTask("", hawbNo, CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL) == false)
            {
                return;
            }

            GetHouseBillTask(hawbNo, out m_photoCaptureData);
            bool isExistingTask = true;

            pictureBox1.Image = PhotoCaptureResource.Digital_Camera;

            /// panel titile disable (uncomment 3 lines and add panel1 to this.Controls instead of panelHeader2.Controls)
            panelTitle.Visible = false;
            panelTitle.Height = panel1.Height;
            panelTitle.Top = panel1.Top;

            //panelOptions.Height = Height - labelTitle.Height - panelSoftkeys.Height;


            //panelTitle.Height = panel1.Top;
            m_isExistingTask = isExistingTask;

            completeTask = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.COMPLETE_TASK);
            completeTaskLater = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.COMPLETE_TASK_LATER);
            exitwithoutSavingMenuItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.EXIT_WITHOUT_SAVING);

            deleteTask = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.DELETE_TASK);



            completeTask.Enabled = false;
            exitwithoutSavingMenuItem.Enabled = false;
            completeTaskLater.Enabled = false;

            if (HawbNo == string.Empty)
            {
                AddOptionsListItem(completeTask);
                AddOptionsListItem(completeTaskLater);
                AddOptionsListItem(exitwithoutSavingMenuItem);


				resolutionItem = new SmoothListbox.ListItems.OptionsListNode("Picture Quality", "Settings");
				resolutionItem.ListType = SmoothListbox.ListItems.OptionsListNode.ListNodeTye.SINGLE;

				resolutionItem.AddItem(new SmoothListbox.ListItems.CheckBox("Low"));
				resolutionItem.AddItem(new SmoothListbox.ListItems.CheckBox("Medium"), true);
				resolutionItem.AddItem(new SmoothListbox.ListItems.CheckBox("High"));
				AddOptionsListItem(resolutionItem);
				resolutionItem.ApplyNodeClicked += new SmoothListbox.ListItems.OptionsListNode.ApplyNodeClickedHandler(resolutionItem_ApplyNodeClicked);
                AddOptionsListItem(deleteTask);
            }
            else
            {

                AddOptionsListItem(completeTask);
                AddOptionsListItem(exitwithoutSavingMenuItem);

            }

        


          
        }

        public Reasons()
        {
            InitializeComponent();
        }

        public Reasons(bool isExistingTask/*string taskID*/)
        {
            InitializeComponent();
            //using (Bitmap bmp = new Bitmap(PhotoCaptureResource.thumbnailBackColor))
            //{
            //    thumbnailBackColor = bmp.GetPixel(0, 0);
            //}
             
            pictureBox1.Image = PhotoCaptureResource.Digital_Camera;
            
            /// panel titile disable (uncomment 3 lines and add panel1 to this.Controls instead of panelHeader2.Controls)
            panelTitle.Visible = false;
            panelTitle.Height = panel1.Height;
            panelTitle.Top = panel1.Top;

            //panelOptions.Height = Height - labelTitle.Height - panelSoftkeys.Height;


            //panelTitle.Height = panel1.Top;
            m_isExistingTask = isExistingTask;
           
            completeTask = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.COMPLETE_TASK);
            completeTaskLater = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.COMPLETE_TASK_LATER);
            exitwithoutSavingMenuItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.EXIT_WITHOUT_SAVING);

            deleteTask = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.DELETE_TASK);

           

            completeTask.Enabled = false;
            exitwithoutSavingMenuItem.Enabled = false;
            completeTaskLater.Enabled = false;

            

            AddOptionsListItem(completeTask);
               AddOptionsListItem(completeTaskLater);
            AddOptionsListItem(exitwithoutSavingMenuItem);


                resolutionItem = new SmoothListbox.ListItems.OptionsListNode("Picture Quality", "Settings");
                resolutionItem.ListType = SmoothListbox.ListItems.OptionsListNode.ListNodeTye.SINGLE;

                resolutionItem.AddItem(new SmoothListbox.ListItems.CheckBox("Low"));
                resolutionItem.AddItem(new SmoothListbox.ListItems.CheckBox("Medium"), true);
                resolutionItem.AddItem(new SmoothListbox.ListItems.CheckBox("High"));                
                AddOptionsListItem(resolutionItem);
                resolutionItem.ApplyNodeClicked += new SmoothListbox.ListItems.OptionsListNode.ApplyNodeClickedHandler(resolutionItem_ApplyNodeClicked);

            AddOptionsListItem(deleteTask);

             
        
        }
        /// <summary>
        /// Needs re-implementation
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="SelectedItems"></param>
        void resolutionItem_ApplyNodeClicked(string nodeName, List<Control> SelectedItems)
        {
            // to be fixed in future
            //Cursor.Current = Cursors.WaitCursor;
            //if (nodeName == "Picture Quality")
            //{
            //    this.DisappearOptionsMenu();
            //    Refresh();
            //    if (values != null)
            //    {
            //        for (int i = 0; i < values.Length; i++)
            //        {
            //            if (values[i] == true)
            //            {
            //                switch (i)
            //                {
            //                    case 0:
            //                        ColorCameraResolution = 3;
                                    
            //                        //m_filter = "Not Completed";
            //                        break;
            //                    case 1:
            //                        ColorCameraResolution = 4;
            //                        //m_filter = "Not Started";
            //                        break;
            //                    case 2:
            //                        ColorCameraResolution = 7;
            //                        //m_filter = "In Progress";
            //                        break;
            //                //    case 3:
            //                //        m_filter = "Completed";
            //                //        break;
            //                //    case 4:
            //                //        m_filter = "Show All";
            //                //        break;
            //                //    case 5:
            //                //        m_filter = "Not Assigned";
            //                //        break;

            //                }
            //                //TitleText = "Freight Photo Capture " + "(" + m_filter + ")";

            //                //this.Enabled = false;
            //                //this.Enabled = true;
            //                //break;

            //            }
            //        }
            //    }
            //}
            //if (m_bCameraMode)
            //{
            //    if (selectedReason != null)
            //    {
            //        if (selectedReason.miniPhotoCaptureControl != null)
            //        {
                        
            //            selectedReason.miniPhotoCaptureControl.Enabled = true;
            //        }
            //    }
            //}
            //Cursor.Current = Cursors.Default;
        }

        
        void LoadReasons()
        {
            
            this.smoothListBoxMainList.RemoveAll();
            smoothListBoxMainList.LayoutItems();


            Cursor.Current = Cursors.WaitCursor;
            try
            {
                CargoMatrix.Communication.WebServiceManager.Instance().GetExistingImage(ref m_photoCaptureData);
            }
            catch
            {
                return;
            }
            if (m_photoCaptureData != null)
            {
                
                //labelHouseBill.Text = "Housebill: " + m_photoCaptureData.reference;
                labelHouseBill.Text = m_photoCaptureData.taskPrefix + " " + m_photoCaptureData.reference;
                
                //RDF
                if (HawbNo != string.Empty)
                {
                    labelHouseBill.Text =  m_photoCaptureData.reference;
                }
                else
                {
                    labelHouseBill.Text = m_photoCaptureData.taskPrefix + " " + m_photoCaptureData.reference;
                }

                



                labelLine2.Text = m_photoCaptureData.line2;
                labelLine3.Text = m_photoCaptureData.line3;
                //if (m_photoCaptureData.description == null)
                //    labelPiece.Text = "Pieces: N/A, Weight: N/A";    
                //else
                //    labelPiece.Text = m_photoCaptureData.description;

                //if (m_photoCaptureData.location == null)
                //    labelLocation.Text = "Location: N/A";
                //else
                //    labelLocation.Text = m_photoCaptureData.location;// +", Rack: " + m_photoCaptureData.rack + ", Bin: " + m_photoCaptureData.bin;

                this.Update();
                if (m_photoCaptureData.itemsList != null)
                {
                    smoothListBoxMainList.progressBar1.Visible = true;

                    smoothListBoxMainList.progressBar1.Minimum = -1;
                    smoothListBoxMainList.progressBar1.Maximum = m_photoCaptureData.itemsList.Count;
                    smoothListBoxMainList.progressBar1.Value = 0;
                    UpdateList();
                    
                }

                
            }


            Cursor.Current = Cursors.Default;


            //panel1.BringToFront();
            //panelTitle.Visible = false;
            //panelTitle.BackColor = Color.Black;
            smoothListBoxMainList.LayoutItems();
            //LayoutMenuItems();
        
        }

        private void Reasons_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled == false)
            {
                this.smoothListBoxMainList.RemoveAll();
                smoothListBoxMainList.LayoutItems();

                
                labelHouseBill.Text = "";
                labelLine2.Text = "";
                labelLine3.Text = "";
                GC.Collect();
                return;
            }
            //this.panelSoftkeys.Top = this.Height - panelSoftkeys.Height;
            
            //this.panelSoftkeys.Width = Width;
            
            this.Refresh();
            LoadReasons();
            pictureBoxBack.Enabled = true;
            
        }
        private void MoveCapturedImagesToTop()
        {
            foreach (var item in smoothListBoxMainList.Items)
            {
                if ((item is ReasonsListItem) && (item as ReasonsListItem).HasImage)
                    smoothListBoxMainList.MoveItemToTop(item as Control);
            }
        }
        void ReasonsListItem_CameraExitNotify(Control sender)
        {
            
            if (HasDataChanged)
            {
                pictureBoxBack.Enabled = false;
                MoveCapturedImagesToTop();
            }

            smoothListBoxMainList.Scrollable = true;

            
            smoothListBoxMainList.itemsPanel.Top = 0;//prevItemsLocation;
            m_bCameraMode = false;
            selectedReason = null;
            LayoutItems();
        }

        private void buttonCameraMode_Click(object sender, EventArgs e)
        {
            if(LoadCameraNotify != null)
                LoadCameraNotify();
        }

        
        int prevItemsLocation;
        private void Reasons_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (m_bCameraMode == false)
            {
                if (listItem is ReasonsListItem)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    BarcodeEnabled = false;
                    CargoMatrix.UI.CMXAnimationmanager.RefreshHeader();
                    Scrollable = false;
                    prevItemsLocation = smoothListBoxMainList.itemsPanel.Top;
                    
                    smoothListBoxMainList.selectedItemsMap[listItem] = false;

                    (listItem as ReasonsListItem).ShowCamera();
                    //LayoutItems();
                    
                    //smoothListBoxMainList.MoveControlToTop(listItem);
                    //listItem.ResumeLayout();

                    m_bCameraMode = true;
                    selectedReason = listItem as ReasonsListItem;
                    selectedReason.threadSyncControl = pictureBoxMenu;
                    selectedReason.Focus(false);
                    //selectedReason.miniPhotoCaptureControl.FilmBackColor = thumbnailBackColor;// Color.DarkRed;
                    this.pictureBoxBack.Enabled = true;
                    
                    Cursor.Current = Cursors.Default;
                    
                }
            }
            
            Cursor.Current = Cursors.Default;
        }

        private void UploadPictures()
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxMainList.progressBar1.Minimum = -1;
            smoothListBoxMainList.progressBar1.Maximum = 100;
            smoothListBoxMainList.progressBar1.Value = 5;
            smoothListBoxMainList.progressBar1.Visible = true;
            smoothListBoxMainList.progressBar1.Refresh();
            CargoMatrix.Communication.DTO.FreightPhotoCapture data = new CargoMatrix.Communication.DTO.FreightPhotoCapture();
            data = m_photoCaptureData;

            data.itemsList = new List<CargoMatrix.Communication.DTO.FreightPhotoCaptureItem>();
            CargoMatrix.Communication.DTO.FreightPhotoCaptureItem item;
            foreach (Control control in smoothListBoxMainList.itemsPanel.Controls)
            {
                if (control is ReasonsListItem)
                {
                    item = new CargoMatrix.Communication.DTO.FreightPhotoCaptureItem();
                    item.reason = (control as ReasonsListItem).title.Text;
                    item.imagelist = (control as ReasonsListItem).GetUploadImageList();//.GetImageList();

                    data.itemsList.Add(item);
                }
            }
            smoothListBoxMainList.progressBar1.Value = 25;
            smoothListBoxMainList.progressBar1.Refresh();
            if (CargoMatrix.Communication.WebServiceManager.Instance().UploadPhotoCapture(m_photoCaptureData.taskID, data) == true)
            {
                //if (ExitNotify != null)
                //   ExitNotify();
                CargoMatrix.UI.CMXAnimationmanager.GoBack();

            }
            else
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("Server unable to process the request. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                smoothListBoxMainList.progressBar1.Visible = false;
                return;
                
            }
            smoothListBoxMainList.progressBar1.Value = 90;
            smoothListBoxMainList.progressBar1.Refresh();
            ImagesDBManager.RemoveImageFromDB(m_photoCaptureData.reference);
            Cursor.Current = Cursors.Default;
            smoothListBoxMainList.progressBar1.Value = 100;
            smoothListBoxMainList.progressBar1.Refresh();
            smoothListBoxMainList.progressBar1.Visible = false;
            smoothListBoxMainList.progressBar1.Refresh();
        
        }
        bool m_hasDataChanged;
        public bool HasDataChanged
        {
            get
            {
                return m_hasDataChanged;
            }
            set
            {
                m_hasDataChanged = value;
                
            }

            
        
        }
        bool HasPictures
        {
            get
            {
                bool dataFlag = false;
                foreach (Control control in smoothListBoxMainList.itemsPanel.Controls)
                {
                    if (control is ReasonsListItem)
                    {

                        if ((control as ReasonsListItem).pictureBoxCheck.Image != null)
                        {
                            dataFlag = true;
                            break;
                        }

                    }
                }
                return dataFlag;
            
            }
        }
        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (m_bCameraMode)
            {
                if (selectedReason != null)
                {
                    selectedReason.GoBack();
                    if (HasDataChanged)
                    {
                        pictureBoxBack.Enabled = false;
                        
                        smoothListBoxMainList.MoveItemToTop(selectedReason);
                        //smoothListBoxMainList.LayoutItems();
                    }

                    if (HawbNo == string.Empty)
                    {
                        return;
                    }
                    else
                    {

                        if (this.ShowReasons)
                        {
                            return;
                        }

                        HawbNo = string.Empty;


                         //string msg = string.Format("Are you sure you want to finalize and save?");
                        //if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Screening", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        //{
                            CargoMatrix.Communication.WebServiceManager.Instance().CloseTask(m_photoCaptureData.taskID);
                            try
                            {
                                UploadPictures();
                            }
                            catch
                            { }
                           
                            //CargoMatrix.UI.CMXAnimationmanager.GoBack();
                            return;
                        //}
                        //else
                        //{
                        //    return;
                        //}


                    }
                }


            }
            CargoMatrix.UI.CMXAnimationmanager.GoBack();

        }

        public CargoMatrix.Communication.DTO.FreightPhotoCapture PhotoCaptureData
        {
            set
            {
               m_photoCaptureData = value;
               if (value.logo != null)
               {
                   System.IO.MemoryStream stream = new System.IO.MemoryStream(value.logo);
                   pictureBoxLogo.Image = new Bitmap(stream);
                   stream.Close();
               }

            }
            get { return m_photoCaptureData; }
        }

        private void Reasons_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            Cursor.Current = Cursors.WaitCursor;
            Refresh();
            
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.DELETE_TASK)
                {
                    bool result = false;
                    if (CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to delete this task.", "Confirm Delete", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.OKCancel, DialogResult.Cancel) == DialogResult.OK)
                    {
                        if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                        {
                            if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(this, "Delete Task: " + m_photoCaptureData.actualBill) == DialogResult.OK)
                            {
                                result = CargoMatrix.Communication.WebServiceManager.Instance().DeleteTask(this.m_photoCaptureData.taskID);
                            }

                        }
                        else
                        {
                            result = CargoMatrix.Communication.WebServiceManager.Instance().DeleteTask(this.m_photoCaptureData.taskID);
                        
                        }
                    
                    }
                    if (result == true)
                    {
                        if (m_bCameraMode)
                        {
                            selectedReason.GoBack();
                        }
                        CargoMatrix.UI.CMXAnimationmanager.GoBack();

                    
                    }
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.COMPLETE_TASK)
                {
                    CargoMatrix.Communication.WebServiceManager.Instance().CloseTask(m_photoCaptureData.taskID);

                    if (m_bCameraMode)
                    {
                        selectedReason.GoBack();
                    }
                    UploadPictures();
                    //if (m_isExistingTask)
                    //if (ExitNotify != null)
                    //    ExitNotify();


                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.COMPLETE_TASK_LATER)
                {
                    if (m_bCameraMode)
                    {
                        selectedReason.GoBack();
                    }

                    UploadPictures();
                    //if (ExitNotify != null)
                    //  ExitNotify();


                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.EXIT_WITHOUT_SAVING)
                {

                    if (HasPictures)
                    {
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(PhotoCaptureResource.Text_ExitCameraWarning,
                           PhotoCaptureResource.Text_ConfirmExit, CargoMatrix.UI.CMXMessageBoxIcon.Hand,
                           MessageBoxButtons.OKCancel, DialogResult.Cancel))
                        {
                            ImagesDBManager.RemoveImageFromDB(m_photoCaptureData.reference);
                            if (m_bCameraMode)
                            {
                                selectedReason.GoBack();
                            }
                            CargoMatrix.UI.CMXAnimationmanager.GoBack();
                        }
                        else
                        {
                            DisappearOptionsMenu();
                            if (selectedReason != null)
                            {
                                selectedReason.miniPhotoCaptureControl.Refresh();
                                selectedReason.miniPhotoCaptureControl.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        if (m_bCameraMode)
                        {
                            selectedReason.GoBack();
                        }
                        CargoMatrix.UI.CMXAnimationmanager.GoBack();
                    }

                }

            }
            else
            {
                Cursor.Current = Cursors.Default;
            }
        }


        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
 


            pictureBoxBack.Refresh();
            if (selectedReason != null)
                selectedReason.StopCameraTimer();

            if (m_bOptionsMenu == false)
                buttonBack_Click(sender, e);
            else
            {
                base.pictureBoxBack_Click(sender, e);
                if (m_bCameraMode)
                {
                    if (m_bOptionsMenu == false)
                    {
                        if (selectedReason != null)
                        {
                            if (selectedReason.miniPhotoCaptureControl != null)
                            {
                                DisappearOptionsMenu();
                                selectedReason.miniPhotoCaptureControl.Refresh();
                                selectedReason.miniPhotoCaptureControl.Enabled = true;
                            }
                        }
                    }

                }
            }
        }

        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            
            
            if (m_bOptionsMenu == false) // options are opening
            {
                

                if (selectedReason != null)
                    selectedReason.StopCameraTimer();

                completeTask.Enabled = (HasPictures || HasDataChanged);
                completeTaskLater.Enabled = (HasPictures || HasDataChanged);
                exitwithoutSavingMenuItem.Enabled = (HasPictures || HasDataChanged);

                //if (HasDataChanged)
                //{
                //    completeTask.Picture = PhotoCaptureResource.Task;//.Symbol_Check_2;
                //    completeTaskLater.Picture = PhotoCaptureResource.Save;//.Symbol_Check_2;
                //    exitwithoutSavingMenuItem.Picture = PhotoCaptureResource.Symbol_Delete_2;

                //}

                //else
                //{
                //    completeTask.Picture = PhotoCaptureResource.Task_Dis;//.Symbol_Check_2_dis;
                //    completeTaskLater.Picture = PhotoCaptureResource.Save_Dis;//.Symbol_Check_2_dis;
                //    exitwithoutSavingMenuItem.Picture = PhotoCaptureResource.Symbol_Delete_2_dis;

                //}
                
            }
            if (m_bCameraMode)
            {
                if (selectedReason != null)
                {
                    if (selectedReason.miniPhotoCaptureControl != null)
                    {
                        if (m_bOptionsMenu)
                        {
                            DisappearOptionsMenu();
                            selectedReason.miniPhotoCaptureControl.Refresh();
                            selectedReason.miniPhotoCaptureControl.Enabled = true;
                            return;
                        }
                        else
                            selectedReason.miniPhotoCaptureControl.Enabled = false;

                        
                    }
                }
            }
            
            base.pictureBoxMenu_Click(sender, e);

            
        
        }
        protected override void SetBackButton()
        {
            base.SetBackButton();
            if (m_bOptionsMenu == false && m_bCameraMode == false && HasDataChanged == true)
            {
                pictureBoxBack.Enabled = false;
            }
        }

        int threadIndex = 0;
        void UpdateList()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            
            for (threadIndex = 0; threadIndex < m_photoCaptureData.itemsList.Count; threadIndex++)
            {
                this.Invoke(new EventHandler(WorkerUpdate));

                if (!this.ShowReasons)
                {
                    if (HawbNo != string.Empty)
                    {
                        break;
                    }
                }
         

            }
            
                

            this.Invoke(new EventHandler(WorkerEndUpdate));
            
            Cursor.Current = Cursors.Default;

        }
        public void WorkerUpdate(object sender, EventArgs e)
        {

            //for (int i = 0; i < m_tasks.Length; i++)
            {

                string reason = m_photoCaptureData.itemsList[threadIndex].reason;

                if (HawbNo != string.Empty)
                {
                    if (!this.ShowReasons)
                    {
                        reason = "Screening";
                    }
                   

                }

                ReasonsListItem temp = new ReasonsListItem(reason, this, m_photoCaptureData.itemsList[threadIndex].ReasonType);//
                temp.Enabled = false;
                //if(temp.ReasonType == 1)
                //    smoothListBoxMainList.AddItemToFront(temp);
                //else

                
                    smoothListBoxMainList.AddItem2(temp);
                
                if (m_photoCaptureData.itemsList[threadIndex].imagelist.Count > 0)
                {
                    using (System.IO.MemoryStream stream = new System.IO.MemoryStream(m_photoCaptureData.itemsList[threadIndex].imagelist[0].m_rawImage))
                    {
                        temp.InsertOuterImage(new Bitmap(stream));
                    }
                    
                }
                temp.CameraExitNotify += new ExitEventHandler(ReasonsListItem_CameraExitNotify);

                //if (threadIndex % 5 == 0)
                {

                    smoothListBoxMainList.progressBar1.Value = threadIndex;
                    smoothListBoxMainList.LayoutItems();
                    smoothListBoxMainList.Update();
                    
                }
            }

        }

        public void WorkerEndUpdate(object sender, EventArgs e)
        {
            smoothListBoxMainList.LayoutItems();
            smoothListBoxMainList.Update();
            smoothListBoxMainList.progressBar1.Visible = false;
            
            
            for (int i = 0; i < smoothListBoxMainList.itemsPanel.Controls.Count; i++)
                smoothListBoxMainList.itemsPanel.Controls[i].Enabled = true;



        }

        public int MainControlIndex(Control control)
        {
            if (smoothListBoxMainList.itemsPanel.Controls.Contains(control))
                return smoothListBoxMainList.itemsPanel.Controls.GetChildIndex(control);
            return -1;
        }
        public int ColorCameraResolution
        {
            set
            {
                m_ColorCameraResolution = value;
                if (selectedReason != null)
                {
                    selectedReason.miniPhotoCaptureControl.SetColorCameraResolution();
                    
                }
            }
            get
            {
                return m_ColorCameraResolution;
            
            }
        }

        private void Reasons_LoadOptionsMenu(object sender, EventArgs e)
        {
            

        }

    }
}
