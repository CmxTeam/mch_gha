﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Drawing.Imaging;

namespace CargoMatrix.FreightPhotoCapture
{
    //public delegate void ImageReadNotifyHandler(Image image);

    public partial class CMXCameraControl : CMXCameraBase
    {
        public override event ImageReadNotifyHandler ImageReadNotify;

        FullscreenForm fullscreenDialog;
        
        private Symbol.Imaging.Imager imager;
        private System.IO.MemoryStream imageStream;
        private Symbol.ResourceCoordination.Trigger trigger;
        Symbol.ResourceCoordination.Trigger.TriggerEventHandler triggerHandler;
        private bool imageView;
        private bool activated;
        ArrayList triggerList;
        bool isFullScreen = false;
        

        public CMXCameraControl()
        {
            InitializeComponent();
            fullscreenDialog = new FullscreenForm();
            try
            {
                Symbol.Imaging.Device[] device = Symbol.Imaging.Device.AvailableDevices;
                imager = new Symbol.Imaging.Imager(Symbol.Imaging.Device.AvailableDevices[0]);
                Symbol.Imaging.Capabilities cap = new Symbol.Imaging.Capabilities(imager);
                StartAcquisition();

            }
            catch (Exception ex)
            {
                //CargoMatrix.UI.CMXMessageBox.Show("Failed to create Imager: " + ex.Message, "Error!" + " (30011)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30011);
                return;
            }

            imager.ImageFormat.FileFormat = Symbol.Imaging.FileFormats.JPEG;

            triggerList = new ArrayList();
            foreach (Symbol.ResourceCoordination.TriggerDevice device
                             in Symbol.ResourceCoordination.TriggerDevice.AvailableTriggers)
            {
                try
                {
                    trigger = new Symbol.ResourceCoordination.Trigger(device);
                    triggerList.Add(trigger);
                    triggerHandler = new Symbol.ResourceCoordination.Trigger.TriggerEventHandler(trigger_Stage2Notify);
                    trigger.Stage2Notify += triggerHandler;//triggerHandlerList[triggerHandlerList.Count-1];
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to create Trigger: " + ex.Message, "Error");
                    return;
                }
            }

            activated = true;
        
        }
        
        protected override void OnEnabledChanged(EventArgs e)
        {
            if (Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;
                if (imager == null)
                {
                    try
                    {
                        Symbol.Imaging.Device[] device = Symbol.Imaging.Device.AvailableDevices;
                        imager = new Symbol.Imaging.Imager(Symbol.Imaging.Device.AvailableDevices[0]);
                        Symbol.Imaging.Capabilities cap = new Symbol.Imaging.Capabilities(imager);

                    }
                    catch (Exception ex)
                    {
                        //CargoMatrix.UI.CMXMessageBox.Show("Failed to create Imager: " + ex.Message, "Error!" + " (30011)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30011);
                        return;
                    }

                    imager.ImageFormat.FileFormat = Symbol.Imaging.FileFormats.JPEG;
                }
                if (triggerList == null)
                {
                    triggerList = new ArrayList();
                    foreach (Symbol.ResourceCoordination.TriggerDevice device
                                     in Symbol.ResourceCoordination.TriggerDevice.AvailableTriggers)
                    {
                        try
                        {
                            trigger = new Symbol.ResourceCoordination.Trigger(device);
                            triggerList.Add(trigger);
                            triggerHandler = new Symbol.ResourceCoordination.Trigger.TriggerEventHandler(trigger_Stage2Notify);
                            trigger.Stage2Notify += triggerHandler;//triggerHandlerList[triggerHandlerList.Count-1];
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Failed to create Trigger: " + ex.Message, "Error");
                            return;
                        }
                    }
                }

                // Trigger only works then ImagerForm is activated.
                activated = true;
                //StopAcquisition();
                StartAcquisition();
                //imageView = true;
                //imager.ImageFormat.JPEGQuality = 50;
                Cursor.Current = Cursors.Default;

            }
            else
            {
                StopAcquisition();
                ///////////////////
                if (triggerList != null)
                {
                    foreach (Symbol.ResourceCoordination.Trigger trigger
                                     in triggerList)
                    {
                        if (trigger != null)
                            trigger.Dispose();
                    }
                    triggerList.Clear();
                    triggerList = null;
                }
                if (imager != null)
                {
                    imager.Dispose();
                    imager = null;
                }

                if (this.cameraPictureBox.Image != null)
                {

                    this.cameraPictureBox.Image.Dispose();

                    this.cameraPictureBox.Image = null;

                }

                
            }
            base.OnEnabledChanged(e);
        }
        private void trigger_Stage2Notify(object sender, Symbol.ResourceCoordination.TriggerEventArgs e)
        {
            if (isFullScreen)
            {
                fullscreenDialog.Close();
                isFullScreen = false;
                return;
            }

            if (activated && e.NewState == Symbol.ResourceCoordination.TriggerState.STAGE2)
            {
                if (!imageView)
                    GetImage();
                else
                    StartAcquisition();
            }
        }
        //int indexer = 0;
        private void GetImage()
        {
            imageStream = imager.GetImage();

            if (imager.Viewfinder.IsStarted)
                imager.Viewfinder.Stop();
            imager.StopAcquisition();

            //if (this.cameraPictureBox.Image != null)
            //{

            //    this.cameraPictureBox.Image.Dispose();

            //    this.cameraPictureBox.Image = null;

            //}

            //try
            //{
            //    GC.Collect();
            //    this.cameraPictureBox.Image = new Bitmap(imageStream);
            //}
            //catch (Exception e)
            //{
            //    //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            //    this.Enabled = false;
            //    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30001);
            //    this.Enabled = true;
            //    return;
            //}
            

            if (ImageReadNotify != null)
            {
                CargoMatrix.UI.CMXSound.Play(PhotoCaptureResource.CameraClick);

                //ImageReadNotify(cameraPictureBox.Image);
                ImageReadNotify(imageStream);
            }
            
            imageView = true;
            
        }
        public override void StopAcquisition()
        {
            if (imager != null)
            {
                if (imager.Viewfinder.IsStarted)
                    imager.Viewfinder.Stop();
                imager.StopAcquisition();
                imageView = true;
            }
        
        }
        public override void StartAcquisition()
        {
            imageView = false;
            try
            {
                imager.StartAcquisition();
            }
            catch (Exception ex)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Failed to start Imager: " + ex.Message, "Error!" + " (30012)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                //this.Close();
                return;
            }
            //if (this.menuItemViewfinder.Checked)
            imager.Viewfinder.Start(this.cameraPictureBox);
            if(ImageReadNotify != null)
                ImageReadNotify(null);
            
        }

        public bool Aim
        {
            set
            {
                if (imager != null)
                {
                    imager.AcqParams.Aiming = value;
                }

            
            }
            get
            {
                if (imager != null)
                {
                    return imager.AcqParams.Aiming;
                }
                else
                    return false;
            }
        }
        public bool Lamp
        {
            set
            {
                if (imager != null)
                {
                    imager.AcqParams.LampOn = value;
                }


            }
            get
            {
                if (imager != null)
                {
                    return imager.AcqParams.LampOn;
                }
                else
                    return false;
            }

        }
        public bool IsFullScreen
        {
            get
            {
                return isFullScreen;
            }
        }

        public bool IsImageView
        {
            get
            {
                return imageView;
            }
        }

        public override Image image
        {
            get
            {
                return cameraPictureBox.Image;
            }
            set
            {
                try
                {
                    //GC.Collect();
                    if(value == null)
                    {
                        if (cameraPictureBox.Image != null)
                        {
                            cameraPictureBox.Image.Dispose();
                            cameraPictureBox.Image = null;
                        }
                    }
                        
                    else
                        cameraPictureBox.Image = new Bitmap(value);
                        
                }
                catch (Exception e)
                {
                    //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30002)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30002);
                }
                
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }
        
        public override void FullScreen(bool fullscreen)
        {
            if (!imageView)
                return;
            isFullScreen = true;
            try
            {
               fullscreenDialog.Image = cameraPictureBox.Image;
               fullscreenDialog.ShowDialog();
               
            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30003)", null);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30003);
            }
            isFullScreen = false;
                
        }
        
        
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            

        }
        
        private void CMXCameraControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (isFullScreen)
            {
                FullScreen(false);
                return;
            }
        }

        
    }
}

