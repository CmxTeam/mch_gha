﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class ReasonsListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            //if (miniPhotoCaptureControl != null)
            //{
            //    //miniPhotoCaptureControl.ImageList.Clear();
            //    miniPhotoCaptureControl.Dispose();
            //    miniPhotoCaptureControl = null;
            //}
            if (pictureBoxCheck.Image != null)
            {
                pictureBoxCheck.Image.Dispose();
                pictureBoxCheck.Image = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxCheck = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // pictureBoxCheck
            // 
            this.pictureBoxCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCheck.BackColor = System.Drawing.Color.White;
            this.pictureBoxCheck.Location = new System.Drawing.Point(181, 4);
            this.pictureBoxCheck.Name = "pictureBoxCheck";
            this.pictureBoxCheck.Size = new System.Drawing.Size(56, 42);
            this.pictureBoxCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCheck.Visible = false;
            // 
            // ReasonsListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pictureBoxCheck);
            this.Name = "ReasonsListItem";
            this.Size = new System.Drawing.Size(240, 50);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox pictureBoxCheck;

    }
}
