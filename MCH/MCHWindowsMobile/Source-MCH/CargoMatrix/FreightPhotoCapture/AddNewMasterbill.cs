﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightPhotoCapture
{
   
    public partial class AddNewMasterbill : SmoothListbox.SmoothListbox// UserControl
    {
        
        //CargoMatrix.FreightPhotoCapture.PhotoCaptureControl2 photoCaptureControl;
        //ListAllReasons reasonsList;
        Reasons reasonsControl;
        bool needFocus = true;

        public AddNewMasterbill()
        {
            InitializeComponent();
            pictureBox1.Image = PhotoCaptureResource.scan2_small;
            Location = new Point(CargoMatrix.UI.CMXAnimationmanager.GetParent().Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            Size = new Size(CargoMatrix.UI.CMXAnimationmanager.GetParent().Width,
                CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            smoothListBoxMainList.Height = 0;
            smoothListBoxMainList.Visible = false;
           


            //buttonLogin.BackgroundImage = PhotoCaptureResource.button_default;
            //buttonLogin.ActiveBackgroundImage = PhotoCaptureResource.button_pressed;
            //buttonLogin.Text = "Pictures ";
            //buttonLogin.Image = PhotoCaptureResource.Digital_Camera_24;
            //buttonLogin.ImageAlign = OpenNETCF.Drawing.ContentAlignment2.MiddleLeft;
            //buttonLogin.TextAlign = OpenNETCF.Drawing.ContentAlignment2.MiddleRight;
            //buttonLogin.TransparentImage = true;

            cmxTextBox1.Focus();
            //Disposed += new EventHandler(NewPhotoCaptureTask_Disposed);
            pictureBox1.Visible = false;
            buttonClear.Visible = false;
            buttonDamage.Visible = false;
            //buttonLogin.Visible = false;
            cmxTextBox1.Visible = false;
            cmxTextBox2.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;

            this.TitleText = PhotoCaptureResource.Text_AddNewMasterbill;
            //pictureBoxMenu.Enabled = false;
            
                
        }

        //void NewPhotoCaptureTask_Disposed(object sender, EventArgs e)
        //{
        //    //throw new NotImplementedException();
        //}

        private void buttonClear_Click(object sender, EventArgs e)
        {
            cmxTextBox1.Text = "";
            cmxTextBox2.Text = "";
            cmxTextBox1.Focus();

        }

        //private void buttonLogin_Click(object sender, EventArgs e)
        //{
        //    if (cmxTextBox1.Text == "")
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("You must scan or enter barcode manually to proceed.", "Barcode cannot be Empty", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //        return;

        //    }
        //    Cursor.Current = Cursors.WaitCursor;
            
        //    if (photoCaptureControl == null)
        //    {
        //        photoCaptureControl = new PhotoCaptureControl2(cmxTextBox1.Text);
        //    }
        //    photoCaptureControl.ControlType = PhotoCaptureControl2.PhotoCaptureControlType.NO_REASONS;
        //    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(photoCaptureControl);
        //}

        private void NewPhotoCaptureTask_BarcodeReadNotify(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
            if (barcodeData == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode.", "Invalid Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                barcode.StartRead();
                return;
            }
            string carrier, origMB;
            if (TaskList.IsMasterBill(barcodeData, out carrier, out origMB))
            {
                cmxTextBox1.Text = carrier;
                cmxTextBox2.Text = origMB;
                
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("This Barcode is not a valid Masterbill.", "Invalid Masterbill", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                barcode.StartRead();
                return;
                
            }
            //cmxTextBox1.Text = barcodeData;

            
            barcode.StartRead();
            this.buttonDamage_Click(this, EventArgs.Empty);
        }

        private void NewPhotoCaptureTask_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled == false)
            {
                cmxTextBox1.Text = "";
                cmxTextBox2.Text = "";
                pictureBox1.Visible = false;
                buttonClear.Visible = false;
                buttonDamage.Visible = false;
                //buttonLogin.Visible = false;
                cmxTextBox1.Visible = false;
                cmxTextBox2.Visible = false;
                label1.Visible = false;
                label2.Visible = false;
                label3.Visible = false;

                
            }

            else
            {
                //if (photoCaptureControl != null)
                //{
                //    photoCaptureControl.Dispose();
                //    photoCaptureControl = null;
                //}

                if (reasonsControl!= null)
                {
                    reasonsControl.Dispose();
                    reasonsControl = null;
                }

                pictureBox1.Visible = true;
                buttonClear.Visible = true;
                buttonDamage.Visible = true;
                //buttonLogin.Visible = true;
                cmxTextBox1.Visible = true;
                cmxTextBox2.Visible = true;
                label1.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                
            }
            needFocus = Enabled;
        }

        private void cmxTextBox1_GotFocus(object sender, EventArgs e)
        {
            
            cmxTextBox1.SelectAll();
        }

        private void buttonDamage_Click(object sender, EventArgs e)
        {
            if (cmxTextBox1.Text == "")
            {
                //CargoMatrix.UI.CMXMessageBox.Show("You must scan or enter barcode manually to proceed.", "Barcode cannot be Empty", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;

            }
            Cursor.Current = Cursors.WaitCursor;
            ReasonsLogic.CreateTask(cmxTextBox1.Text, cmxTextBox2.Text, CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL, this, ref reasonsControl);

        }

        private void cmxTextBox1_EnabledChanged(object sender, EventArgs e)
        {
            cmxTextBox1.Focus();
            cmxTextBox1.Invalidate();

            
        }

        private void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
           
            switch (e.KeyCode)
            {
                case Keys.Tab:
                case Keys.Enter:
                    cmxTextBox2.Focus();
                    e.Handled = true;
                    break;
            }
            if ((e.KeyValue >= 'a' && e.KeyValue <= 'z') || (e.KeyValue >= 'A' && e.KeyValue <= 'Z') || (e.KeyValue >= '0' && e.KeyValue <= '9'))
            {
                Application.DoEvents();
                
                if (cmxTextBox1.Text.Length >= 3)
                {
                    cmxTextBox1.Text = cmxTextBox1.Text.ToUpper();
                    cmxTextBox2.Focus();


                }
                
            }
        }

        private void cmxTextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (cmxTextBox2.Text.Length == 0 && e.KeyCode == Keys.Back)
                cmxTextBox1.Focus();
        }

        private void cmxTextBox2_GotFocus(object sender, EventArgs e)
        {
            cmxTextBox2.SelectAll();
        }

        private void cmxTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void AddNewMasterbill_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.LOGOUT)
            {
                if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                {

                    //AbortAllThreads();
                    BarcodeEnabled = false;
                    CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                }
            }
        }

        private void AddNewMasterbill_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));
        }

        private void AddNewMasterbill_GotFocus(object sender, EventArgs e)
        {
            
            cmxTextBox1.Focus();
            cmxTextBox1.Invalidate();
        }

        private void AddNewMasterbill_Paint(object sender, PaintEventArgs e)
        {
             if(this.Enabled)
             {
                if (needFocus)
                {
                    needFocus = false;
                    cmxTextBox1.Focus();

                }
             }
        
        }
        
    }
}
