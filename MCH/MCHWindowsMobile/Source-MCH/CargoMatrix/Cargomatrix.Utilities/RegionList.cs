﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WarehouseManager;
using CustomUtilities;
using CargoMatrix.UI;

namespace CargoMatrix.CargoUtilities
{
    public partial class RegionList : SmoothListbox.SmoothListBoxAsync<IRegion>
    {
        public IWarehouse wareHouse;

        public RegionList(IWarehouse wareHouse)
        {
            this.wareHouse = wareHouse;

            InitializeComponent();

            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(RegionList_ListItemClicked);
            this.LoadOptionsMenu += new EventHandler(RegionList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(RegionList_MenuItemClicked);
        }

        void RegionList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;
            }
        }

        void RegionList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

        void RegionList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            SmoothListbox.ListItems.TwoLineListItem<IRegion> item = (SmoothListbox.ListItems.TwoLineListItem<IRegion>)listItem;

            if (item == null) return;

            var region = item.ItemData;

            CMXAnimationmanager.DisplayForm(new LocationList(region));
        }

        protected override Control InitializeItem(IRegion item)
        {
            SmoothListbox.ListItems.TwoLineListItem<IRegion> listItem = new SmoothListbox.ListItems.TwoLineListItem<IRegion>(item.Code, CargoMatrix.Resources.Icons.Delivery_Points_24x24, item.Name, item);
            return listItem;
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;

            var list = WarehouseManagerProvider.Instance.GetRegionList(this.wareHouse);

            this.headerItem.Label1 = string.Format("WRH Code : {0}", this.wareHouse.Code);
            this.headerItem.Label2 = string.Format("WRH Name : {0}", this.wareHouse.Name);

            this.ReloadItemsAsync(list);

            SetWindowTitle(list.Count());

            Cursor.Current = Cursors.Default;
        }


        void AddRegion_ButtonClick(object sender, System.EventArgs e)
        {
            var user = WarehouseManagerProvider.Instance.CurrentUser;

            if (user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin &&
                DialogResult.OK != CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override")) return;

            IRegion region;
            bool result = CommonMethods.CreateRegion(this.wareHouse, out region);

            if (result == false) return;

            this.smoothListBoxMainList.AddItem(this.InitializeItem(region));
            //this.smoothListBoxMainList.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);

            this.SetWindowTitle();

        }

        private void SetWindowTitle()
        {
            SetWindowTitle(this.smoothListBoxMainList.Items.Count);
        }

        private void SetWindowTitle(int itemsCount)
        {
            this.TitleText = string.Format("LocationManager ({0})", itemsCount);
        }


    }
}
