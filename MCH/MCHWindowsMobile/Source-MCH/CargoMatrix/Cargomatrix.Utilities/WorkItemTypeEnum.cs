﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.CargoUtilities
{
    public enum WorkItemTypeEnum
    {
        MASTERBILL,
        HOUSEBILL,
        ULD,
        ONHAND,
        LOCATION,
        TRUCK,
        PRINTER_Bluetooth,
        PRINTER_Wireless,
        USER
    }
}
