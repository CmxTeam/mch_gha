﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.CargoUtilities
{
    public partial class CreateRegionForm : CreateInstanceForm
    {
        public CreateRegionForm()
        {
            InitializeComponent();

            this.Header = "Create Region";
        }
    }
}