﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WarehouseManager;

namespace CargoMatrix.CargoUtilities
{
    public partial class CreateWareHouseForm : CreateInstanceForm
    {
        public CreateWareHouseForm()
        {
            InitializeComponent();

            this.Header = "Create Warehouse";
        }
    }
}