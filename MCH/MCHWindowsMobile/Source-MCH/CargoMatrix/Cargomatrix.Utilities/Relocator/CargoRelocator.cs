﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.InventoryWS;
using CargoMatrix.Communication;
using CargoMatrix.UI;
using SmoothListbox.ListItems;
using CargoMatrix.CargoUtilities.Relocator;

namespace CargoMatrix.CargoUtilities
{
    public partial class CargoRelocator : CargoUtilities.SmoothListBoxOptions
    {
        private HawbDetailedItem hawbDetails;

        public CargoRelocator()
        {
            InitializeComponent();
            panelHeader2.Height = 231;
            topLabel.Top = 22;
            this.TitleText = RelocatorResx.Text_RelocatorTitle;

            this.LoadOptionsMenu += new EventHandler(HawbInventory_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(HawbInventory_MenuItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeScanned);
            topLabel.Text = RelocatorResx.Text_ScanLoc;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            smoothListBoxMainList.Scrollable = false;
            this.BackButtonHold = false;
            Cursor.Current = Cursors.Default;
            Forklift.Instance.ItemsCountChanged += (o, e) => { this.buttonScannedList.Value = Forklift.Instance.ItemsCount; };
            Forklift.Instance.ForkliftItemsDeleted += new EventHandler(OnForkliftItemsDeleted);
        }        

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            var count = Communication.Relocator.Instance.GetForkliftPiecesCount(Forklift.Instance.ForkliftID, Forklift.Instance.TaskID);
            Forklift.Instance.ItemsCount = count;

            if (BarcodeEnabled == false)
                BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            if (Forklift.Instance.ItemsCount != 0)
            {
                /// forklift is not empty
                CargoMatrix.UI.CMXMessageBox.Show(RelocatorResx.Text_EmptyForklift, RelocatorResx.Text_ConfirmLocation, CMXMessageBoxIcon.Hand);//, MessageBoxButtons.OK, DialogResult.OK);
            }
            else
                base.pictureBoxBack_Click(sender, e);
        }


        private void BarcodeScanned(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;
            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    ScanPiece(scanItem.HouseBillNumber, scanItem.PieceNumber, MOT.Air);
                    break;
                case CMXBarcode.BarcodeTypes.OnHand:
                    ScanPiece(scanItem.HouseBillNumber, scanItem.PieceNumber, MOT.Ocean);
                    break;

                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    if (Forklift.Instance.ItemsCount > 0)
                    {
                        DropAllPiecesToLocation(scanItem.Location);
                    }
                    break;
                default:
                    CargoMatrix.UI.CMXSound.Play(CMXSound.SoundType.FAIL);
                    ShowFailMessage(RelocatorResx.Text_InvalidBarcode);
                    break;
            }

            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        private void ScanPiece(string hawbNo, int pieceNo, MOT mot)
        {
            CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
            var hawbItem = Communication.Relocator.Instance.ScanPieceIntoForklift(hawbNo, pieceNo, Forklift.Instance.TaskID, Forklift.Instance.ForkliftID, mot);
            if (hawbItem.Transaction.TransactionStatus1)
            {
                DisplayHawb(hawbItem);
            }
            else
            {
                ShowFailMessage(hawbItem.Transaction.TransactionError);
            }
        }

        private void DropAllPiecesToLocation(string locationName)
        {
            CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
            var status = Communication.Relocator.Instance.DropAllPiecesIntoLocation(Forklift.Instance.TaskID, locationName, Forklift.Instance.ForkliftID);
            if (status.TransactionStatus1)
            {
                Forklift.Instance.ItemsCount = status.TransactionRecords;
                //if (hawbDetails != null)
                //    hawbDetails.Visible = false;
            }
            else
            {
                ShowFailMessage(status.TransactionError);
            }
        }

        private void OnForkliftItemsDeleted(object sender, EventArgs e)
        {
            if (hawbDetails != null)
                hawbDetails.Visible = false;
        }

        private static void ShowFailMessage(string errormessage)
        {
            CargoMatrix.UI.CMXMessageBox.Show(errormessage, RelocatorResx.Text_Error, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }

        private void DisplayHawb(HouseBillItem hawb)
        {
            if (hawbDetails == null)
            {
                hawbDetails = new HawbDetailedItem() { Location = new Point(0, 45) , RelocatorParent = this };
                this.panelHeader2.Controls.Add(hawbDetails);
            }
            hawbDetails.DisplayHousebill(hawb);
            Forklift.Instance.ItemsCount = hawb.Transaction.TransactionRecords;
        }

        void HawbInventory_LoadOptionsMenu(object sender, EventArgs e)
        {
            //this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO) { Title = "Housebill Lookup" });
        }

        void buttonScannedList_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            Forklift.ShowForkliftPieces();
            //if (hawbDetails != null)
            //    hawbDetails.Visible = false;
            Cursor.Current = Cursors.Default;
            LoadControl();
        }

        void HawbInventory_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO:
                        break;
                    default:
                        break;
                }
        }
    }
}

