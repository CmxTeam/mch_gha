﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using CargoMatrix.Communication.InventoryWS;
using CustomListItems;
using CargoMatrix.UI;
using CargoMatrix.CargoUtilities.Relocator;
using CMXBarcode;

namespace CargoMatrix.CargoUtilities
{
    public class Forklift
    {
        private Forklift()
        {
            var taskSettings = Communication.Relocator.Instance.GetTaskSettings();
            TaskID = taskSettings.TaskId;
            ForkliftID = taskSettings.ForkliftId;
        }

        public event EventHandler ItemsCountChanged;
        public event EventHandler ForkliftItemsDeleted;
        public int TaskID { get; private set; }
        public int ForkliftID { get; private set; }
        private CargoMatrix.Utilities.MessageListBox listBox;

        private static Forklift _instance;
        public static Forklift Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Forklift();
                return _instance;
            }
        }

        private int itemsCount;
        public int ItemsCount
        {
            get { return itemsCount; }
            set
            {
                itemsCount = value;
                if (ItemsCountChanged != null)
                {
                    ItemsCountChanged(this, EventArgs.Empty);
                }
            }
        }

        public static DialogResult ShowForkliftPieces()
        {
            if (Instance.listBox == null || Instance.listBox.IsDisposed)
            {
                Instance.listBox = new CargoMatrix.Utilities.MessageListBox();
                Instance.listBox.HeaderText = "Scanned List: ";
                Instance.listBox.HeaderText2 = String.Empty;
                Instance.listBox.OKClicked += new EventHandler(listBox_OKClicked);
            }
            
            Instance.listBox.RemoveAllItems();
            Instance.listBox.OkEnabled = false;
            
            var pieces = Communication.Relocator.Instance.GetForkliftPieces(Forklift.Instance.ForkliftID, Forklift.Instance.TaskID);
            if (pieces.Length == 0)
                return DialogResult.Cancel;

            Forklift.Instance.ItemsCount = pieces.Length;

            var groups = from p in pieces
                         group p by p.HouseBillId
                             into pieceGroups
                             select new { HousebillId = pieceGroups.Key, Mot = pieceGroups.First().Mot, HawbNumber = pieceGroups.First().HousebillNumber, Pieces = pieceGroups, TotalPieces = pieceGroups.First().TotalPieces };

            foreach (var g in groups)
            {
                var pieceItems = g.Pieces.Select(p => new CustomListItems.ComboBoxItemData(string.Format("Piece {0}", p.PieceNumber), string.Empty, p.PieceId) { isReadonly = false, IsSelectable = false });
                List<CustomListItems.ComboBoxItemData> hawbPieces = new List<CustomListItems.ComboBoxItemData>();
                hawbPieces.AddRange(pieceItems);

                Image icon = (g.Mot == MOT.Air ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.PieceModeOcean);
                string line2 = string.Format("Pieces: {0} of {1}", g.Pieces.Count(), g.TotalPieces);

                CustomListItems.ComboBox<MOT> combo = new CustomListItems.ComboBox<MOT>(g.Mot, g.HousebillId, g.HawbNumber, line2, icon, hawbPieces);

                combo.ContainerName = "ScanView";
                combo.IsSelectable = true;
                //combo.ReadOnly = true;
                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(combo_LayoutChanged);
                combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                Instance.listBox.smoothListBoxBase1.AddItem(combo);
            }

            return Instance.listBox.ShowDialog();
        }

        static void listBox_OKClicked(object sender, EventArgs e)
        {
            CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup( CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
            confirmLoc.TextLabel = "Scan Location";

            if (confirmLoc.ShowDialog() == DialogResult.OK)
            {
                DropPiecesIntoLocation(confirmLoc.Text);
            }


            //            PopulateForkliftContent();            
        }

        private static void DropPiecesIntoLocation(string locationName)
        {
            List<RelocationPieceItem> pieces = new List<RelocationPieceItem>();
            CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

            foreach (var item in Forklift.Instance.listBox.smoothListBoxBase1.Items)
            {
                if (item is CustomListItems.ComboBox<MOT>)
                {
                    var pieceWithMot = item as CustomListItems.ComboBox<MOT>;
                    foreach (var pieceId in (item as CustomListItems.ComboBox<MOT>).SelectedIds)
                        pieces.Add(new RelocationPieceItem() { HouseBillId = pieceWithMot.ID, PieceId = pieceId, Mot = pieceWithMot.Data });
                }
            }

            string message = string.Format(RelocatorResx.Text_DropConfirm, pieces.Count, locationName);
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(message, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OKCancel, DialogResult.OK))
                return;
            
            TransactionStatus status = Communication.Relocator.Instance.DropPiecesIntoLocation(pieces.ToArray(), Forklift.Instance.TaskID, locationName, Forklift.Instance.ForkliftID);
            if (!status.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

            Forklift.Instance.ItemsCount = status.TransactionRecords;
        }

        static void combo_LayoutChanged(CustomListItems.ComboBox sender, int subItemId, bool comboIsEmpty)
        {
            Cursor.Current = Cursors.WaitCursor;
            MOT mot = (sender as CustomListItems.ComboBox<MOT>).Data;
            if (comboIsEmpty)
                Instance.listBox.smoothListBoxBase1.RemoveItem(sender as Control);

            combo_SelectionChanged(null, null);
            Instance.listBox.smoothListBoxBase1.LayoutItems();
            TransactionStatus finalStatus = new TransactionStatus() { TransactionStatus1 = true };

            if (subItemId == -1)
            {
                finalStatus = CargoMatrix.Communication.Relocator.Instance.RemoveHousebillFromForklift(sender.ID, Instance.TaskID, Instance.ForkliftID, mot);
            }
            else
            {
                finalStatus = CargoMatrix.Communication.Relocator.Instance.RemovePieceFromForkLift(sender.ID, subItemId, Instance.TaskID, ScanModes.Piece, Instance.ForkliftID, mot);
            }

            if (finalStatus.TransactionStatus1 == false)
                CargoMatrix.UI.CMXMessageBox.Show(finalStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            else
            {
                Instance.ItemsCount = finalStatus.TransactionRecords;
                if (finalStatus.TransactionRecords == 0 && Instance.ForkliftItemsDeleted != null)
                {
                    Instance.ForkliftItemsDeleted(Instance, EventArgs.Empty);                        
                }
            }

            Cursor.Current = Cursors.Default;
        }

        static void combo_SelectionChanged(object sender, EventArgs e)
        {
            if (Instance.listBox.smoothListBoxBase1.SelectedItems.Count == 0)
            {
                bool enableflag = false;

                CustomListItems.ComboBox<MOT> item = sender as CustomListItems.ComboBox<MOT>;
                if (item is CustomListItems.ComboBox<MOT> && (item as CustomListItems.ComboBox<MOT>).SubItemSelected)
                {
                    enableflag = true;
                }
                Instance.listBox.OkEnabled = enableflag;
            }
            else
                Instance.listBox.OkEnabled = true;
        }
    }
}
