﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5466
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CargoMatrix.CargoUtilities.Relocator {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class RelocatorResx {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        internal RelocatorResx() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CargoMatrix.CargoUtilities.Relocator.RelocatorResx", typeof(RelocatorResx).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This will change HAWB {0} scan mode to COUNT. Do you want to continue?.
        /// </summary>
        internal static string Text_ChangeModeToCount {
            get {
                return ResourceManager.GetString("Text_ChangeModeToCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This will change HAWB {0} scan mode to PIECEID. Do you want to continue?.
        /// </summary>
        internal static string Text_ChangeModeToPiece {
            get {
                return ResourceManager.GetString("Text_ChangeModeToPiece", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hawb {0} : Mode changed to COUNT..
        /// </summary>
        internal static string Text_ChangetoCountConfirm {
            get {
                return ResourceManager.GetString("Text_ChangetoCountConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hawb {0} : Mode changed to PIECEID..
        /// </summary>
        internal static string Text_ChangeToPieceConfirm {
            get {
                return ResourceManager.GetString("Text_ChangeToPieceConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Location.
        /// </summary>
        internal static string Text_ConfirmLocation {
            get {
                return ResourceManager.GetString("Text_ConfirmLocation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter number of missing pieces..
        /// </summary>
        internal static string Text_CountBoxMissing {
            get {
                return ResourceManager.GetString("Text_CountBoxMissing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter number of over pieces..
        /// </summary>
        internal static string Text_CountBoxOver {
            get {
                return ResourceManager.GetString("Text_CountBoxOver", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm count.
        /// </summary>
        internal static string Text_CountHeader {
            get {
                return ResourceManager.GetString("Text_CountHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} Piece/s will be scanned at {1}.
        /// </summary>
        internal static string Text_DropConfirm {
            get {
                return ResourceManager.GetString("Text_DropConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm location for the scanned shipments..
        /// </summary>
        internal static string Text_EmptyForklift {
            get {
                return ResourceManager.GetString("Text_EmptyForklift", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        internal static string Text_Error {
            get {
                return ResourceManager.GetString("Text_Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid barcode has been scanned.
        /// </summary>
        internal static string Text_InvalidBarcode {
            get {
                return ResourceManager.GetString("Text_InvalidBarcode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cargo Relocator.
        /// </summary>
        internal static string Text_RelocatorTitle {
            get {
                return ResourceManager.GetString("Text_RelocatorTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SCAN HOUSEBILL/ONHAND.
        /// </summary>
        internal static string Text_ScanHawb {
            get {
                return ResourceManager.GetString("Text_ScanHawb", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SCAN LOCATION....
        /// </summary>
        internal static string Text_ScanLoc {
            get {
                return ResourceManager.GetString("Text_ScanLoc", resourceCulture);
            }
        }
    }
}
