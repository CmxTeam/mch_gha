﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.CargoUtilities
{
    public enum ScanActionTypeEnum
    {
        Browse,
        PrinLabels,
        CalibratePrinter_Bluetooth,
        CalibratePrinter_Wireless
    }
}
