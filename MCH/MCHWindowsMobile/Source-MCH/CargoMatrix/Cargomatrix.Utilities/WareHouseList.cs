﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WarehouseManager;
using CargoMatrix.UI;

namespace CargoMatrix.CargoUtilities
{
    public partial class WareHouseList : SmoothListbox.SmoothListBoxAsync<IWarehouse>
    {
        public WareHouseList()
        {
            InitializeComponent();

            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(WareHouseList_ListItemClicked);
            this.LoadOptionsMenu += new EventHandler(WareHouseList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(WareHouseList_MenuItemClicked);
            
        }

        void WareHouseList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;
            }
        }

        void WareHouseList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

        void WareHouseList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            SmoothListbox.ListItems.TwoLineListItem<IWarehouse> item = (SmoothListbox.ListItems.TwoLineListItem<IWarehouse>)listItem;

            if(item == null) return;

            var wareHouse = item.ItemData;

            CMXAnimationmanager.DisplayForm(new RegionList(wareHouse));
            

        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            var list = WarehouseManagerProvider.Instance.GetWareHoulseList();

            this.SetWindowTitle(list.Length);
            
            this.headerItem.Label1 = string.Format("Gateway : {0}", WarehouseManagerProvider.Instance.GateWayCode);

            this.ReloadItemsAsync(list);

            Cursor.Current = Cursors.Default;
        }

        protected override Control InitializeItem(IWarehouse item)
        {
            SmoothListbox.ListItems.TwoLineListItem<IWarehouse> listItem = new SmoothListbox.ListItems.TwoLineListItem<IWarehouse>(item.Code, CargoMatrix.Resources.Icons.Warehouse_24x24, item.Name, item);
            return listItem;
        }

        void AddWarehouse_ButtonClick(object sender, System.EventArgs e)
        {
            var user = WarehouseManagerProvider.Instance.CurrentUser;

            if (user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin &&
                DialogResult.OK != CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override")) return;


            var wareHouses = from item in this.smoothListBoxMainList.Items.OfType<SmoothListbox.ListItems.TwoLineListItem<IWarehouse>>()
                             select item.ItemData;
                             

            IWarehouse wareHouse;
            bool result = CommonMethods.CreateWareHouse(out wareHouse);

            if (result == false) return;

            this.smoothListBoxMainList.AddItem(this.InitializeItem(wareHouse));
            //this.smoothListBoxMainList.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);

            this.SetWindowTitle();
        }

        private void SetWindowTitle()
        {
            SetWindowTitle(this.smoothListBoxMainList.Items.Count);
        }

        private void SetWindowTitle(int itemsCount)
        {
            this.TitleText = string.Format("LocationManager ({0})", itemsCount);
        }
        
    }
}
