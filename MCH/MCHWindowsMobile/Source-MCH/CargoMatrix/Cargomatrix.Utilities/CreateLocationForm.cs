﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.CargoUtilities
{
    public partial class CreateLocationForm : CreateInstanceForm
    {
        public CreateLocationForm()
        {
            InitializeComponent();

            this.Header = "Create Location";
        }
    }
}