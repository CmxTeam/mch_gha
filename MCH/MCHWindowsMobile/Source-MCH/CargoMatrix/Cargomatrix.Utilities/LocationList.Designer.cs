﻿namespace CargoMatrix.CargoUtilities
{
    partial class LocationList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.labelBlink = new CustomUtilities.CMXBlinkingLabel();
            this.lblLine1 = new System.Windows.Forms.Label();
            this.lblLine2 = new System.Windows.Forms.Label();
            this.btnScanned = new CargoMatrix.UI.CMXCounterIcon();
            this.logo = new System.Windows.Forms.PictureBox();

            //
            // logo
            //
            this.logo.Image = CargoMatrix.Resources.Icons.Delivery_Points_24x24;
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.Size = new System.Drawing.Size(24, 24);
            this.logo.Location = new System.Drawing.Point(3, 2);
            //this.logo.TransparentColor = System.Drawing.Color.White;
            
            
            // 
            // lblLine1
            // 
            this.lblLine1.Location = new System.Drawing.Point(39, 3);
            this.lblLine1.Name = "labelReference";
            this.lblLine1.Size = new System.Drawing.Size(160, 13);
            this.lblLine1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            
            
            //
            //lblLine2
            //
            this.lblLine2 = new System.Windows.Forms.Label();
            this.lblLine2.Name = "lblLine2";
            this.lblLine2.Location = new System.Drawing.Point(39, 20);
            this.lblLine2.Size = new System.Drawing.Size(160, 13);
            this.lblLine2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(240, 1);
            this.splitter1.BackColor = System.Drawing.Color.DarkGray;


            // 
            // labelBlink
            // 
            this.labelBlink.Location = new System.Drawing.Point(75, 37);
            this.labelBlink.Name = "labelBlink";
            this.labelBlink.ForeColor = System.Drawing.Color.Red;
            this.labelBlink.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelBlink.Size = new System.Drawing.Size(160, 13);
            this.labelBlink.Text = "SCAN LOCATION";
            this.labelBlink.Blink = true;


            //
            // btnScanned
            //
            this.btnScanned.Location = new System.Drawing.Point(200, 3);
            this.btnScanned.Name = "buttonScannedList";
            this.btnScanned.Image = CargoMatrix.Resources.Skin.ScanerView_btn;
            this.btnScanned.PressedImage = CargoMatrix.Resources.Skin.ScanerView_btn_over;
            this.btnScanned.Size = new System.Drawing.Size(36, 36);
            this.btnScanned.TabIndex = 1;
            this.btnScanned.Click += new System.EventHandler(btnScanned_Click);
            this.btnScanned.BringToFront();

            this.panel1.Height = 0;
            panelHeader2.Height = 53;

            //
            // lastScanItem
            //

            this.lastScanItem = new CustomListItems.BottomListItem();
            this.lastScanItem.Height = 178;
            this.lastScanItem.CaptionLine1 = "Last scan :";
            this.lastScanItem.CaptionLine2 = "Location type :";


            this.panelHeader2.Controls.Add(this.logo);
            this.panelHeader2.Controls.Add(this.lblLine1);
            this.panelHeader2.Controls.Add(this.lblLine2);
            this.panelHeader2.Controls.Add(this.labelBlink);
            this.panelHeader2.Controls.Add(this.btnScanned);
            this.panelHeader2.Controls.Add(this.splitter1);

            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private CustomUtilities.CMXBlinkingLabel labelBlink;
        private System.Windows.Forms.Label lblLine2;
        private System.Windows.Forms.Label lblLine1;
        private CargoMatrix.UI.CMXCounterIcon btnScanned;
        private System.Windows.Forms.PictureBox logo;
        private CustomListItems.BottomListItem lastScanItem;
    }
}
