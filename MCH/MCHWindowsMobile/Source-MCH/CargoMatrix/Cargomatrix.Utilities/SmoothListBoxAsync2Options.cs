﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmoothListbox;
using CargoMatrix.UI;

namespace CargoMatrix.CargoUtilities
{
    public abstract class SmoothListBoxAsync2Options<T> : SmoothListBoxAsync2<T> where T : class
    {
        private CustomListItems.OptionsListITem utilitesOptionsListItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.UTILITIES);
        private CustomListItems.OptionsListITem messagesOptionsListItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MESSAGES);
        /// <summary>
        /// Gets utilities options list item
        /// </summary>
        public CustomListItems.OptionsListITem UtilitesOptionsListItem
        {
            get { return utilitesOptionsListItem; }
        }
        public CustomListItems.OptionsListITem MessagesOptionsListItem
        {
            get { return messagesOptionsListItem; }
        }

        public SmoothListBoxAsync2Options()
        {
            this.MenuItemClicked += new ListItemClickedHandler(SmoothListBoxAsync2Options_MenuItemClicked);
            this.LoadOptionsMenu += new EventHandler(SmoothListBoxAsync2Options_LoadOptionsMenu);
        }

        void SmoothListBoxAsync2Options_LoadOptionsMenu(object sender, EventArgs e)
        {
            //this.AddOptionsListItem(messagesOptionsListItem);
        }

        void SmoothListBoxAsync2Options_MenuItemClicked(SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.UTILITIES:

                    CMXAnimationmanager.DisplayForm(new UtilitiesList());

                    break;
                case CustomListItems.OptionsListITem.OptionItemID.MESSAGES:
                    //CargoMatrix.Messageing.MessageManager.Instance.DipslayMessages();
                    break;

            }
        }
    }
}
