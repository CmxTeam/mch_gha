﻿namespace CargoMatrix.CargoUtilities
{
    partial class CreateInstanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
     
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateInstanceForm));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pcxFooter = new System.Windows.Forms.PictureBox();
            this.pcxHeader = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtName = new CargoMatrix.UI.CMXTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCode = new CargoMatrix.UI.CMXTextBox();
            this.btnOk = new CargoMatrix.UI.CMXPictureButton();
            this.btnCancel = new CargoMatrix.UI.CMXPictureButton();
            this.pnlMain.SuspendLayout();
            
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pcxFooter);
            this.pnlMain.Controls.Add(this.pcxHeader);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.txtName);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.txtCode);
            this.pnlMain.Location = new System.Drawing.Point(14, 39);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(212, 161);
            // 
            // pcxFooter
            // 
            this.pcxFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pcxFooter.Location = new System.Drawing.Point(0, 114);
            this.pcxFooter.Name = "pcxFooter";
            this.pcxFooter.Size = new System.Drawing.Size(212, 47);
            this.pcxFooter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pcxHeader
            // 
            this.pcxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcxHeader.Location = new System.Drawing.Point(0, 0);
            this.pcxHeader.Name = "pcxHeader";
            this.pcxHeader.Size = new System.Drawing.Size(212, 20);
            this.pcxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pcxHeader_Paint);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(23, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 15);
            this.label2.Text = "Name";
            // 
            // txtwrhName
            // 
            this.txtName.Location = new System.Drawing.Point(23, 85);
            this.txtName.Name = "txtwrhName";
            this.txtName.Size = new System.Drawing.Size(166, 21);
            this.txtName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(23, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 15);
            this.label1.Text = "Code";
            // 
            // txtwrhCode
            // 
            this.txtCode.Location = new System.Drawing.Point(23, 40);
            this.txtCode.Name = "txtwrhCode";
            this.txtCode.Size = new System.Drawing.Size(166, 21);
            this.txtCode.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(42, 158);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(52, 37);
            
            // 
            // btnCancel
            //
            this.btnCancel.BackColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(146, 158);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(52, 37);

            this.InitializeFields();

            // 
            // CreateWarehouseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.pnlMain);
            this.Name = "CreateWarehouseForm";
            this.Text = "CreateWarehouseForm";
            this.pnlMain.ResumeLayout(false);
            
            this.ResumeLayout(false);

        }

        

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private CargoMatrix.UI.CMXTextBox txtCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private CargoMatrix.UI.CMXTextBox txtName;
        private System.Windows.Forms.PictureBox pcxFooter;
        private System.Windows.Forms.PictureBox pcxHeader;
        private CargoMatrix.UI.CMXPictureButton btnCancel;
        private CargoMatrix.UI.CMXPictureButton btnOk;
    }
}