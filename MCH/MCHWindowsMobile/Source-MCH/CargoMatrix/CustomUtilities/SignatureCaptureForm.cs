﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public partial class SignatureCaptureForm : Form
    {
        string header;
      
        public string SubHeader
        {
            get { return this.lblSignatureRequired.Text; }
            set { this.lblSignatureRequired.Text = value; }
        }
     
        /// <summary>
        /// Gets or Sets form's header text
        /// </summary>
        public string Header
        {
            get { return header; }
            set { header = value; }
        }

        /// <summary>
        /// Gets or sets signature as byte array (point(x,y))
        /// </summary>
        public byte[] SugnatureAsBytes
        {
            get
            {
                return this.signatureControl.GetSignatureEx();
            }
            set
            {
                this.signatureControl.LoadSignatureEx(value);
            }
        }

        /// <summary>
        /// Gets the signature as a bitmap
        /// </summary>
        public Bitmap SignatureAsBitmap
        {
            get
            {
                return this.signatureControl.ToBitmap();
            }
        }

        
        public SignatureCaptureForm()
        {
            InitializeComponent();
        }

        void pcxHeader_Paint(object sender, PaintEventArgs e)
        {
            using(Brush brush = new SolidBrush(Color.White))
            {
                e.Graphics.DrawString(this.Header, 
                                      new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold),
                                      brush, 5, 3);
            }
        }

        private void InitializeFields()
        {
            this.Header = "Confirm Signature";

            this.signatureControl.BackgroundBitmap = CargoMatrix.Resources.Skin.SignatureBackground;

            this.pcxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.pcxFooter.Image = Resources.Graphics.Skin.nav_bg;
            this.pcxFooter.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pcxHeader.Paint += new PaintEventHandler(pcxHeader_Paint);

            this.buttonCancel.BackgroundImage = CargoMatrix.Resources.Skin.button_default; //Resources.Graphics.Skin.nav_cancel;
            this.buttonCancel.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;//   Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.BackColor = Color.White;
            this.buttonCancel.Click += new EventHandler(buttonCancel_Click);

            this.buttonOk.BackgroundImage = CargoMatrix.Resources.Skin.button_default; //Resources.Graphics.Skin.nav_ok;
            this.buttonOk.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;
            this.buttonOk.Text = "OK";
            this.buttonCancel.BackColor = Color.White;
            this.buttonOk.Click += new EventHandler(buttonOk_Click);

            this.buttonClear.BackgroundImage = CargoMatrix.Resources.Skin.button_default; //Resources.Graphics.Skin.nav_cancel;
            this.buttonClear.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;// Resources.Graphics.Skin.nav_cancel_over;
            this.buttonClear.Text = "Clear";
            this.buttonCancel.BackColor = Color.White;
            this.buttonClear.Click += new EventHandler(buttonClear_Click);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult result = CargoMatrix.UI.CMXMessageBox.Show("Please confirm the signature is valid.", "Confirm Signature", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, DialogResult.Yes);

            if (result == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                this.signatureControl.Clear();
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            this.signatureControl.Clear();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
            
        }

    }
}