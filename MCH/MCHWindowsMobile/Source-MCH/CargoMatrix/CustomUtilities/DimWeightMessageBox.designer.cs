﻿namespace CustomUtilities
{
    partial class DimWeightMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonPlus = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmxTextBoxWeight = new CargoMatrix.UI.CMXTextBox();
            this.cmxTextBoxWidth = new CargoMatrix.UI.CMXTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.Label();
            this.cmxTextBoxLength = new CargoMatrix.UI.CMXTextBox();

            this.cmxTextBoxHeight = new CargoMatrix.UI.CMXTextBox();
            this.label4 = new System.Windows.Forms.Label();

            this.cmxTextBoxPieces = new CargoMatrix.UI.CMXTextBox();
            this.cmxTextBoxPiece = new CargoMatrix.UI.CMXTextBox();
            this.cmxTextBoxType = new CargoMatrix.UI.CMXTextBox();

            this.panel1.SuspendLayout();
            this.SuspendLayout();
       

            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonPlus);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cmxTextBoxWeight);
            this.panel1.Controls.Add(this.cmxTextBoxWidth);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.message);
            this.panel1.Controls.Add(this.cmxTextBoxLength);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);

            this.panel1.Controls.Add(this.cmxTextBoxPieces);
            this.panel1.Controls.Add(this.cmxTextBoxPiece);
            this.panel1.Controls.Add(this.cmxTextBoxType);

            this.panel1.Controls.Add(this.cmxTextBoxHeight);
            this.panel1.Location = new System.Drawing.Point(3, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 214);
   

            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(127, 174);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
   

            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(56, 174);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
        

            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 170);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            

            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 24);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
           

            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(65, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 21);
            this.label2.Text = "W:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;

            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(130, 114); 
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 21);
            this.label4.Text = "H:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
 
            this.cmxTextBoxWeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxWeight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxWeight.Location = new System.Drawing.Point(88, 141);
            this.cmxTextBoxWeight.Name = "cmxTextBoxWeight";
            this.cmxTextBoxWeight.Size = new System.Drawing.Size(56, 28);
            this.cmxTextBoxWeight.TabIndex = 1;
            this.cmxTextBoxWeight.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
          

            this.cmxTextBoxWidth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxWidth.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxWidth.Location = new System.Drawing.Point(88, 110);
            this.cmxTextBoxWidth.MaxLength = 3;
            this.cmxTextBoxWidth.Name = "cmxTextBoxWidth";
            this.cmxTextBoxWidth.Size = new System.Drawing.Size(39, 28);
            this.cmxTextBoxWidth.TabIndex = 3;
            this.cmxTextBoxWidth.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Alphabetic;



            this.cmxTextBoxHeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxHeight.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxHeight.Location = new System.Drawing.Point(152, 110); //129
            this.cmxTextBoxHeight.MaxLength = 3;
            this.cmxTextBoxHeight.Name = "cmxTextBoxHeight";
            this.cmxTextBoxHeight.Size = new System.Drawing.Size(39, 28);
            this.cmxTextBoxHeight.TabIndex = 3;
            this.cmxTextBoxHeight.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Alphabetic;

             
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(42, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 21);
            this.label3.Text = "Wgt:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;

            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 21);
            this.label1.Text = "L:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;



            this.message.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.message.BackColor = System.Drawing.Color.White;
            this.message.Location = new System.Drawing.Point(13, 27);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(209, 20);
            this.message.Text = "Dimesions detail:";
         
            this.cmxTextBoxLength.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxLength.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxLength.Location = new System.Drawing.Point(22, 110);
            this.cmxTextBoxLength.MaxLength = 3;
            this.cmxTextBoxLength.Name = "cmxTextBoxLenght";
            this.cmxTextBoxLength.Size = new System.Drawing.Size(39, 28);
            this.cmxTextBoxLength.TabIndex = 2;
            this.cmxTextBoxLength.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Alphabetic;




            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
  | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(194,114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 21);
            this.label5.Text = "Inch.";


            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
              | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(147,145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 21);
            this.label6.Text = "Kgs.";
            



            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
              | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(42, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 21);
            this.label3.Text = "Wgt:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;




            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(128, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 21);
            this.label7.Text = "of";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;

            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(24, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 21);
            this.label8.Text = "Piece#:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;

            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(3, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 21);
            this.label9.Text = "Type:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;



            this.cmxTextBoxPieces.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxPieces.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxPieces.Location = new System.Drawing.Point(152, 48);
            this.cmxTextBoxPieces.Name = "cmxTextBoxPieces";
            this.cmxTextBoxPieces.Size = new System.Drawing.Size(39, 28);
            this.cmxTextBoxPieces.TabIndex = 1;
            this.cmxTextBoxPieces.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;


            this.cmxTextBoxPiece.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxPiece.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxPiece.Location = new System.Drawing.Point(88, 48);
            this.cmxTextBoxPiece.Name = "cmxTextBoxPiece";
            this.cmxTextBoxPiece.Size = new System.Drawing.Size(39, 28);
            this.cmxTextBoxPiece.TabIndex = 1;
            this.cmxTextBoxPiece.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;


            this.cmxTextBoxType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxType.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxType.Location = new System.Drawing.Point(88, 79);
            this.cmxTextBoxType.Name = "cmxTextBoxType";
            this.cmxTextBoxType.Size = new System.Drawing.Size(103, 28);
            this.cmxTextBoxType.TabIndex = 1;
            this.cmxTextBoxType.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;


            this.buttonPlus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPlus.Location = new System.Drawing.Point(199, 79);
            this.buttonPlus.Name = "buttonPlus";
            this.buttonPlus.Size = new System.Drawing.Size(28, 28);
            this.buttonPlus.Click += new System.EventHandler(this.buttonAdd_Click);


            InitializeImages();
            // 
            // OriginDestinationMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "OriginDestinationMessageBox";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label label2;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxWidth;
        private System.Windows.Forms.Label label1;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxLength;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxWeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxHeight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;


        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;


        public CargoMatrix.UI.CMXTextBox cmxTextBoxPieces;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxPiece;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxType;
        private CargoMatrix.UI.CMXPictureButton buttonPlus;

    }
}