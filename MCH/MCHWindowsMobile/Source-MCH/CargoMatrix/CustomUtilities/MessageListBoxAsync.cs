﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

namespace CustomUtilities
{
    public abstract partial class MessageListBoxAsync<T> : CargoMatrix.UI.MessageBoxBase where T : class
    {
        protected Thread _asyncListThread;
        public event SmoothListbox.ListItemClickedHandler ListItemClicked;
        public event CargoMatrix.Utilities.LoadSmoothList LoadListEvent;

        public string HeaderText { get; set; }
        public MessageListBoxAsync()
        {
            InitializeComponent();
            InitializeImages();


            smoothListBoxBase1.AutoScroll += new SmoothListbox.AutoScrollHandler(smoothListBoxReasons_AutoScroll);
            //panel1.BackColor = Resources.Graphics.Skin.thumbnailBackColor.GetPixel(0, 0);
            //pictureBoxMenuBack.Image = Resources.Graphics.Skin.popup_Main_nav;
            //pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            //pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok_dis;
            //pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel;
            //pictureBoxUp.Image = Resources.Graphics.Skin.nav_up;
            //pictureBoxDown.Image = Resources.Graphics.Skin.nav_down;

        }
        private void InitializeImages()
        {
            panel1.SuspendLayout();
            this.pictureBoxHeader.Image = global::Resources.Graphics.Skin.popup_header;
            this.pictureBoxMenuBack.Image = global::Resources.Graphics.Skin.nav_bg;

            this.pictureBoxOk.Image = global::Resources.Graphics.Skin.nav_ok_dis;
            //this.pictureBoxOk.PressedImage = global::Resources.Graphics.Skin.nav_ok_over;
            //this.pictureBoxOk.DisabledImage = global::Resources.Graphics.Skin.nav_ok_dis;

            this.pictureBoxCancel.Image = global::Resources.Graphics.Skin.nav_cancel;
            //this.buttonCancel.PressedImage = global::Resources.Graphics.Skin.nav_cancel_over;
            //this.buttonCancel.DisabledImage = global::Resources.Graphics.Skin.nav_cancel_dis;

            this.pictureBoxUp.Image = global::Resources.Graphics.Skin.nav_up;
            //this.buttonUp.PressedImage = global::Resources.Graphics.Skin.nav_up_over;
            //this.buttonUp.DisabledImage = global::Resources.Graphics.Skin.nav_up_dis;

            this.pictureBoxDown.Image = global::Resources.Graphics.Skin.nav_down;
            //this.buttonDown.PressedImage = global::Resources.Graphics.Skin.nav_down_over;
            //this.buttonDown.DisabledImage = global::Resources.Graphics.Skin.nav_down_dis;
            panel1.ResumeLayout();
        }
        void filterTextBox_TextChanged(object sender, System.EventArgs e)
        {
            smoothListBoxBase1.Filter(filterTextBox.Text);
        }
        //void filtertextbox_TextChanged(object sender, EventArgs e)
        //{
        //    foreach (SmoothListbox.ListItems.StandardListItem item in smoothListBoxBase1.Items)
        //    {
        //        if(item.Text.Contains(filtertextbox.Text))
        //            item.Height =  
        //    }
        //}

        protected T[] _asyncItems;
        public void ReloadItemsAsync(T[] items)
        {
            if (_asyncListThread != null)
            {
                _asyncListThread.Abort();
            }

            _asyncItems = items;
            if (_asyncItems != null && _asyncItems.Length != 0)
            {
                smoothListBoxBase1.labelEmpty.Text = "Loading ...";

                smoothListBoxBase1.RemoveAll();
                ThreadStart starter = new ThreadStart(LoadListAsync);
                _asyncListThread = new Thread(starter);
                _asyncListThread.Priority = ThreadPriority.BelowNormal;
                _asyncListThread.Start();
            }
        }
        private void LoadListAsync()
        {
            if (_asyncItems.Length > 0)
            {
                this.Invoke(new EventHandler(WorkerBeginLoadLocationList));
                for (int i = 0; i < _asyncItems.Length; i++)
                {
                    object[] param = { _asyncItems[i], EventArgs.Empty };
                    this.Invoke(new EventHandler(WorkerLoadLocationList), param);


                }
                this.Invoke(new EventHandler(WorkerEndLoadLocationList));
            }
            else smoothListBoxBase1.labelEmpty.Visible = true;
        }
        protected abstract Control InitializeItem(T item);
        protected virtual void WorkerLoadLocationList(object sender, EventArgs e)
        {
            if (sender is T)
            {
                Control ctrl = InitializeItem(sender as T);
                smoothListBoxBase1.AddItem(ctrl);
                this.ProgressValue++;
            }



        }
        private void WorkerBeginLoadLocationList(object sender, EventArgs e)
        {
            this.Progress(0, _asyncItems.Length);
            this.ProgressBar = true;

        }
        private void WorkerEndLoadLocationList(object sender, EventArgs e)
        {
            this.ProgressBar = false;
            this.smoothListBoxBase1.labelEmpty.Text = "No Records Found.";


        }


        /********************************************/
        public void WideWidth()
        {
            this.panel1.Width = Screen.PrimaryScreen.Bounds.Width;
            this.panel1.Left = Screen.PrimaryScreen.Bounds.Left;
        }

        //SmoothListbox.ListItems.ComboBox m_PiecesComboBox;
        //SmoothListbox.ListItems.ComboBox m_directionComboBox;

        //public int SelectedPieceNo = -1;

        //public string HousebillName;
        //public int[] remainingPieces;
        bool _okEnabled = false;

        void smoothListBoxReasons_AutoScroll(SmoothListbox.SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            switch (direction)
            {
                case SmoothListbox.SmoothListBoxBase.DIRECTION.UP:
                    pictureBoxUp.Enabled = enable;
                    break;
                case SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN:
                    pictureBoxDown.Enabled = enable;
                    break;
            }

        }
        //protected override void OnPaintBackground(PaintEventArgs e)
        //{
        //    using (Pen pen = new Pen(Color.Gray, 1))
        //    {



        //        e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));
        //        //pen.Color = Color.Gray;
        //        //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
        //        int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
        //        for (int i = 0; i < this.Height; i += 2)
        //        {
        //            e.Graphics.DrawLine(pen, x1, y1, x2, y2);

        //            x1 -= 2;
        //            y2 += 2;
        //        }
        //        x1 = 0;
        //        x2 = this.Height;
        //        y1 = 0;
        //        y2 = this.Height;
        //        for (int i = 0; i < this.Height; i += 2)
        //        {
        //            e.Graphics.DrawLine(pen, x1, y1, x2, y2);

        //            y1 += 2;
        //            x2 -= 2;
        //        }


        //    }
        //}
        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    base.OnPaint(e);
        //    if (bFirstTimeLoad == false)
        //    {
        //        bFirstTimeLoad = true;
        //        //HeaderText = "Manual Entry";


        //    }
        //}


        private void MesageListBox_Load(object sender, EventArgs e)
        {

            if (LoadListEvent != null)
            {
                smoothListBoxBase1.RemoveAll();
                LoadListEvent(smoothListBoxBase1);
            }
            if (OkEnabled)
            {
                pictureBoxOk.Enabled = true;
            }
            else
            {
                if (smoothListBoxBase1.SelectedItems.Count > 0)
                    pictureBoxOk.Enabled = true;
                else
                    pictureBoxOk.Enabled = false;
            }
            //if (LoadFormEvent != null)
            //{
            //    Application.DoEvents();
            //    List<Control> controlList = LoadFormEvent(this);
            //    if (controlList != null)
            //    {
            //        smoothListBoxBase1.progressBar1.Visible = true;

            //        smoothListBoxBase1.progressBar1.Minimum = -1;
            //        smoothListBoxBase1.progressBar1.Maximum = controlList.Count;
            //        smoothListBoxBase1.progressBar1.Value = 0;

            //        ReloadList(controlList);

            //    }
            //}


        }

        private void ReloadList(List<Control> list)
        {


            //CargoMatrix.Communication.ScannerWebService.ULDTypeObject[] typeList = CargoMatrix.Communication.HostPlusIncomming.Instance.GetULDTypeList();
            smoothListBoxBase1.RemoveAll();

            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    //smoothListBoxBase1.AddItem(new SmoothListbox.ListItems.StandardListItem(typeList[i].ULDName, null, typeList[i].UldID));
                    smoothListBoxBase1.AddItem(list[i]);

                    smoothListBoxBase1.progressBar1.Value = i;
                    smoothListBoxBase1.LayoutItems();
                    smoothListBoxBase1.Update();
                }


            }
            smoothListBoxBase1.progressBar1.Visible = false;
            smoothListBoxBase1.RefreshScroll();
            //Cursor.Current = Cursors.Default;

        }

        Font HeaderFont = new Font(FontFamily.GenericSerif, 8, FontStyle.Bold);
        SolidBrush HeaderBrush = new SolidBrush(Color.White);
        private void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawString(HeaderText, HeaderFont, HeaderBrush, 3, 4);
        }

        private void pictureBoxOk_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok_over;
        }

        private void pictureBoxOk_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;
        }

        protected virtual void pictureBoxOk_Click(object sender, EventArgs e)
        {
            pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok_over;
            pictureBoxOk.Refresh();

            //smoothListBoxBase1

            DialogResult = DialogResult.OK;

        }

        private void pictureBoxCancel_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel_over;

        }

        private void pictureBoxCancel_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel;
        }

        private void pictureBoxCancel_Click(object sender, EventArgs e)
        {
            pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel_over;
            pictureBoxCancel.Refresh();
            DialogResult = DialogResult.Cancel;
        }

        private void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxUp.Image = Resources.Graphics.Skin.nav_up_over;
            smoothListBoxBase1.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);

        }

        private void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxUp.Image = Resources.Graphics.Skin.nav_up;
            smoothListBoxBase1.UpButtonPressed = false;
        }

        private void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxDown.Image = Resources.Graphics.Skin.nav_down_over;
            smoothListBoxBase1.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);

        }

        private void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxDown.Image = Resources.Graphics.Skin.nav_down;
            smoothListBoxBase1.DownButtonPressed = false;
        }

        private void pictureBoxUp_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxUp.Enabled)
                pictureBoxUp.Image = Resources.Graphics.Skin.nav_up;
            else
            {
                pictureBoxUp.Image = Resources.Graphics.Skin.nav_up_dis;
                smoothListBoxBase1.UpButtonPressed = false;
            }

        }

        private void pictureBoxDown_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxDown.Enabled)
                pictureBoxDown.Image = Resources.Graphics.Skin.nav_down;
            else
            {
                pictureBoxDown.Image = Resources.Graphics.Skin.nav_down_dis;
                smoothListBoxBase1.DownButtonPressed = false;
            }

        }

        private void textLocationCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok_over;
                pictureBoxOk.Refresh();
                DialogResult = DialogResult.OK;
                pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;

            }
        }

        private void textLocationCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;

            }
        }

        public List<Control> SelectedItems
        {
            get
            {
                return smoothListBoxBase1.SelectedItems;
            }
        }
        public ControlCollection Items
        {
            get
            {
                return smoothListBoxBase1.Items;
            }
        }
        public bool UnselectListEnabled
        {
            get
            {
                return smoothListBoxBase1.UnselectEnabled;
            }
            set
            {
                smoothListBoxBase1.UnselectEnabled = value;
            }
        }
        public bool MultiSelectListEnabled
        {
            get
            {
                return smoothListBoxBase1.MultiSelectEnabled;
            }
            set
            {
                smoothListBoxBase1.MultiSelectEnabled = value;

            }
        }
        public bool CancelEnabled
        {

            set
            {
                pictureBoxCancel.Enabled = value;
                if (value)
                {
                    pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel;
                }
                else
                {
                    pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel_dis;
                }
            }
        }

        public bool OkEnabled
        {

            set
            {
                _okEnabled = value;
                pictureBoxOk.Enabled = value;
                if (value)
                {
                    pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;
                }
                else
                {
                    pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok_dis;
                }
            }
            get
            {
                return _okEnabled;
            }
        }
        public void ResetSelection()
        {
            smoothListBoxBase1.Reset();
        }

        public void SelectControl(Control control)
        {
            smoothListBoxBase1.MoveControlToTop(control);
            smoothListBoxBase1.SelectControl(control);
            pictureBoxOk.Enabled = true;
            smoothListBoxBase1.RefreshScroll();
        }

        private void pictureBoxOk_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxOk.Enabled)
                pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;
            else
            {
                pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok_dis;

            }
        }

        private void smoothListBoxBase1_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (smoothListBoxBase1.SelectedItems.Count > 0)
                OkEnabled = true;
            else
                OkEnabled = false;

            if (ListItemClicked != null)
                ListItemClicked(sender, listItem, isSelected);

            if (MultiSelectListEnabled == false && isSelected == true)
            {
                if (OneTouchSelection)
                {
                    pictureBoxOk_Click(this, EventArgs.Empty);
                    pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;
                }
            }
        }

        public void AddItem(Control control)
        {
            smoothListBoxBase1.AddItem(control);
        }
        public void AddItemToFront(Control control)
        {
            smoothListBoxBase1.AddItemToFront(control);
        }
        public void AddItems(List<Control> controls)
        {
            foreach (Control ctrl in controls)
            {
                AddItem(ctrl);
            }
        }
        public void RemoveItem(Control control)
        {
            smoothListBoxBase1.RemoveItem(control);
        }
        public void RemoveAllItems()
        {
            smoothListBoxBase1.RemoveAll();
        }
        public bool IsSelectable
        {
            get
            {
                return smoothListBoxBase1.IsSelectable;
            }
            set
            {
                smoothListBoxBase1.IsSelectable = value;
            }
        }
        public void Progress(int current, int total)
        {

            smoothListBoxBase1.progressBar1.SuspendLayout();
            smoothListBoxBase1.progressBar1.Maximum = total;
            smoothListBoxBase1.progressBar1.Minimum = -1;
            smoothListBoxBase1.progressBar1.Value = current;
            smoothListBoxBase1.progressBar1.ResumeLayout();
        }
        public string LabelEmpty
        {
            set
            {
                smoothListBoxBase1.labelEmpty.Text = value;
                smoothListBoxBase1.labelEmpty.Visible = true;
            }
        }

        public bool ProgressBar
        {
            set
            {
                smoothListBoxBase1.progressBar1.Visible = value;

            }
            get
            {
                return smoothListBoxBase1.progressBar1.Visible;

            }
        }
        public int ProgressValue
        {
            set
            {
                smoothListBoxBase1.progressBar1.Value = value;
            }
            get
            {
                return smoothListBoxBase1.progressBar1.Value;

            }
        }
        public bool OneTouchSelection
        {
            get;
            set;
        }
        public new int Top
        {
            set
            {
                panel1.Top = value;
            }
            get
            {
                return panel1.Top;
            }
        }
        public new int Height
        {
            set
            {
                panel1.Height = value;
            }
            get
            {
                return panel1.Height;
            }
        }

        private void MessageListBox_Closing(object sender, CancelEventArgs e)
        {
            if (_asyncListThread != null)
            {
                _asyncListThread.Abort();
            }
        }

    }
}
