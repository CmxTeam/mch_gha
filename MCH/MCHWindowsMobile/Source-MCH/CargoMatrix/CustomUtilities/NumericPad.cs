﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CustomUtilities
{
    public partial class NumericPad : Form
    {
        public string Title { get; set; }
        private int[] _Ids;
        private int _totalCount;
        private int rangeStart = -1;
        private int startindex = 1;
        private bool[] selectedLabels;
        private CMXCounterIcon[] buttons;
        private int buttonSize = 40;
        private int vSpacing = 4;
        private int hSpacing = 16;
        private bool rangeMode = false;

        public int TotalCount
        {
            get { return _totalCount; }
            set
            {
                _totalCount = value;
                selectedLabels = new bool[value];
                buttonPrev.Enabled = false;
                buttonNext.Enabled = value > 9;
                DisplayText();
            }
        }

        //public int[] IDs
        //{
        //    set
        //    {
        //        _Ids = value;
        //        selectedLabels = new bool[value.Length];
        //        buttonPrev.Enabled = false;
        //        buttonNext.Enabled = value.Length > 9;
        //        DisplayText();
        //    }
        //}

        public List<int> SelectedIDs
        {
            get
            {
                List<int> indexes = new List<int>();
                for (int i = 0; i < selectedLabels.Length; i++)
                {
                    if (selectedLabels[i])
                        indexes.Add(_Ids[i]);
                }
                return indexes;
            }
        }


        private void InitializeButtons()
        {
            Point startLoc = new Point(2, 2);
            buttons = new CMXCounterIcon[9];
            panelButtons.SuspendLayout();
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    buttons[i * 3 + j] = new CMXCounterIcon();
                    buttons[i * 3 + j].Location = new System.Drawing.Point(startLoc.X + (hSpacing + buttonSize) * j, startLoc.Y + (vSpacing + buttonSize) * i);
                    buttons[i * 3 + j].Size = new System.Drawing.Size(buttonSize, buttonSize);
                    buttons[i * 3 + j].TabStop = false;
                    buttons[i * 3 + j].MouseDown += new MouseEventHandler(PadButton_MouseDown);
                    buttons[i * 3 + j].Size = new Size(40, 40);
                    buttons[i * 3 + j].BackColor = this.BackColor;
                    buttons[i * 3 + j].Value = i * 3 + j;
                    panelButtons.Controls.Add(buttons[i * 3 + j]);
                }
            panelButtons.ResumeLayout(false);
        }

        public NumericPad(int[] Ids)
            : this(Ids.Length)
        {
            _Ids = Ids;
        }
        public NumericPad(int count)
        {
            InitializeComponent();
            TotalCount = count;
            //InitializeButtons();
            buttonOk.Enabled = false;
            DispalyLabels();
            buttonNext.Image = CargoMatrix.Resources.Skin.btn_next;
            buttonPrev.Image = CargoMatrix.Resources.Skin.btn_prev;
            buttonNext.PressedImage = CargoMatrix.Resources.Skin.btn_next_over;
            buttonPrev.PressedImage = CargoMatrix.Resources.Skin.btn_prev_over;
            buttonNext.DisabledImage = CargoMatrix.Resources.Skin.btn_next;
            buttonPrev.DisabledImage = CargoMatrix.Resources.Skin.btn_prev;
            this.Title = "Select labels to print";

        }

        public NumericPad(int count, bool rangeMode)
            : this(count)
        {
            this.rangeMode = rangeMode;
        }
        public NumericPad(int[] Ids, bool rangeMode)
            : this(Ids.Length, rangeMode)
        { _Ids = Ids; }
        private void DispalyLabels()
        {
            int uplimit = TotalCount - startindex < 9 ? TotalCount - startindex + 1 : 9;
            for (int i = 0; i < uplimit; i++)
            {
                buttons[i].Visible = true;
                buttons[i].Value = startindex + i;
                if (selectedLabels[startindex - 1 + i])
                    buttons[i].Image = CargoMatrix.Resources.Skin.labelbtn_over;
                else
                    buttons[i].Image = CargoMatrix.Resources.Skin.labelbtn;
            }
            for (int i = uplimit; i < 9; i++)
            {
                buttons[i].Visible = false;
            }
        }
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                e.Graphics.DrawString(Title, font, new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

        }

        private void PadButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (rangeMode)
                MouseDownInRangeMode(sender);
            else
                MouseDownInRandomMode(sender);
        }

        private void MouseDownInRandomMode(object sender)
        {
            CargoMatrix.UI.CMXCounterIcon btn = sender as CargoMatrix.UI.CMXCounterIcon;
            int index = btn.Value;
            selectedLabels[index - 1] = !selectedLabels[index - 1];

            if (selectedLabels[index - 1])
                btn.Image = CargoMatrix.Resources.Skin.labelbtn_over;
            else
                btn.Image = CargoMatrix.Resources.Skin.labelbtn;

            bool flag = false;
            foreach (var item in selectedLabels)
                if (item)
                    flag = true;
            buttonOk.Enabled = flag;
        }

        private void MouseDownInRangeMode(object sender)
        {
            CargoMatrix.UI.CMXCounterIcon btn = sender as CargoMatrix.UI.CMXCounterIcon;
            if (rangeStart == -1)
            {
                rangeStart = btn.Value - 1;
                buttons[rangeStart % 9].Image = CargoMatrix.Resources.Skin.labelbtn_over;
                buttonOk.Enabled = false;
            }
            else
            {
                for (int i = Math.Min(rangeStart, btn.Value - 1); i <= Math.Max(rangeStart, btn.Value - 1); i++)
                {
                    selectedLabels[i] = !selectedLabels[i];
                    //buttons[i%9].Image = selectedLabels[i] ? CargoMatrix.Resources.Skin.labelbtn_over : CargoMatrix.Resources.Skin.labelbtn;
                }
                rangeStart = -1;
                bool flag = false;
                foreach (var item in selectedLabels)
                    if (item)
                    {
                        flag = true;
                        break;
                    }
                buttonOk.Enabled = flag;
                DispalyLabels();
            }

        }
        private void buttonNext_Click(object sender, EventArgs e)
        {
            startindex += 9;
            DispalyLabels();
            DisplayText();
            if (TotalCount - startindex < 9)
                buttonNext.Enabled = false;
            if (startindex > 9)
                buttonPrev.Enabled = true;

        }

        private void DisplayText()
        {
            int uplimit = TotalCount - startindex < 9 ? TotalCount : startindex + 8;
            label2.Text = string.Format("{0} to {1} of {2}", startindex, uplimit, TotalCount);
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            startindex -= 9;
            DispalyLabels();
            DisplayText();
            if (TotalCount - startindex > 9)
                buttonNext.Enabled = true;
            if (startindex < 9)
                buttonPrev.Enabled = false;

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black, 1))
            {
                e.Graphics.DrawLine(pen, panelButtons.Left - 30, panelButtons.Top - 2, panelButtons.Right + 30, panelButtons.Top - 2); // top horizontal
                e.Graphics.DrawLine(pen, panelButtons.Right + 30, panelButtons.Top - 2, buttonNext.Right + 30, buttonNext.Bottom + 2); // left vertical
                e.Graphics.DrawLine(pen, panelButtons.Left - 30, panelButtons.Bottom + 2, panelButtons.Right + 30, panelButtons.Bottom + 2); // middle horizontal
                e.Graphics.DrawLine(pen, panelButtons.Left - 30, panelButtons.Top - 2, panelButtons.Left - 30, buttonPrev.Bottom + 2); // right vertical
                e.Graphics.DrawLine(pen, panelButtons.Left - 30, buttonPrev.Bottom + 2, panelButtons.Right + 30, buttonNext.Bottom + 2); // middle horizontal

            }
        }
    }
}