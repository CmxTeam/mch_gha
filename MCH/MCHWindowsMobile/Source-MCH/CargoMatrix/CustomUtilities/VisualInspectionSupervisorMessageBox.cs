﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CustomUtilities
{
    public partial class VisualInspectionSupervisorMessageBox : SupervisorAuthorizationMessageBox
    {
        public static string SupervisorName { get; private set; }
        public new static DialogResult Show(UserControl control, string Title)
        {
            instance = new VisualInspectionSupervisorMessageBox();

            instance.SuspendLayout();

            instance.pictureBox.Image = CustomUtilitiesResource.tsa_logo;
            instance.HeaderText = Title;// UtilitiesResource.Text_SupervisorCaption;
            instance.message.Text = CustomUtilitiesResource.Text_VisualInspectionMessage;
            instance.label1.Text = CustomUtilitiesResource.Text_VisualInspectionMessage2;

            instance.ResumeLayout();

            return instance.CMXShowDialog(control);
        }
        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (cmxTextBox1.Text != "")
            {

                try
                {
                    string userName = CargoMatrix.Communication.HostPlusIncomming.Instance.VerifyVisualInspectionSupervisorPIN(cmxTextBox1.Text);
                    if (!string.IsNullOrEmpty(userName))
                    {
                        m_lifeTimer.Enabled = false;
                        SupervisorName = userName;
                        DialogResult = DialogResult.OK;
                    }
                    else
                        CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_Invalid_Visual_Inspection_PIN, CustomUtilitiesResource.Text_Error, CMXMessageBoxIcon.Exclamation);
                }
                catch (Exception ex)
                {
                    //CargoMatrix.UI.CMXMessageBox.Show(ex.Message, "Error!" + " (40001)", CMXMessageBoxIcon.Exclamation);
                    barcodeControl.BarcodeEnabled = false;

                    if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn() == true)
                    {

                        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 40011);
                        barcodeControl.BarcodeEnabled = true;
                    }

                    else// (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn() == false)
                    {

                        DialogResult = DialogResult.Abort;

                    }

                }


                //DialogResult = DialogResult.OK;
            }
            cmxTextBox1.SelectAll();
            cmxTextBox1.Focus();
            Cursor.Current = Cursors.Default;
        }
    }
}