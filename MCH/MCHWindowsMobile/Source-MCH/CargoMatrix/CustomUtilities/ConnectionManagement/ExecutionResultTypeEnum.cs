﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CustomUtilities.ConnectionManagement
{
    public enum ExecutionResultTypeEnum
    {
        Success,
        Error,
        UnableToConnect,
        StoppedByUser
    }
}
