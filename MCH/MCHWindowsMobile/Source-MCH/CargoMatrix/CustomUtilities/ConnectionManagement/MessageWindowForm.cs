﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities.ConnectionManagement
{
    public partial class MessageWindowForm : Form
    {
        string header;
        
        public string Message
        {
            get 
            { 
                return this.txtMessage.Text; 
            }
            set 
            {

                if (this.txtMessage.InvokeRequired == false)
                {
                    this.txtMessage.Text = value;
                }
                else
                {
                    this.txtMessage.BeginInvoke((Action)delegate
                    {
                        this.txtMessage.Text = value;
                    });
                }
            }
        }

        public string Header
        {
            get { return header; }
            set { header = value; }
        }
        
        public MessageWindowForm()
        {
            InitializeComponent();
        }

        private void InitializeFields()
        {
            
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(this.pnlMain.Left - 1, this.pnlMain.Top - 1, this.pnlMain.Width + 1, this.pnlMain.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }

        private void pcxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Brush brush = new SolidBrush(Color.White))
            {
                e.Graphics.DrawString(this.Header,
                                      new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold),
                                      brush, 5, 3);
            }
        }

        private void txtMessage_TextChanged(object sender, EventArgs e)
        {

        }
    }
}