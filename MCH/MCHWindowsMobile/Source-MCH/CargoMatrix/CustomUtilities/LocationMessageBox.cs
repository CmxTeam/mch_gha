﻿using CargoMatrix.Communication.ScannerUtilityWS;
using System.Drawing;
namespace CustomUtilities
{
    public class WareHouseLocation
    {
        public string Name;
        public int PrefixID;
    }
    class LocationMessageBox : MessageListBoxAsync<LocationItem>
    {
        public LocationMessageBox()
        {
            OneTouchSelection = false;
            MultiSelectListEnabled = false;
            OkEnabled = false;
            HeaderText = "Select a Location From List";
        }
        protected override System.Windows.Forms.Control InitializeItem(LocationItem item)
        {
            Image icon = null;
            switch (item.LocationType)
            {
                case LocationTypes.Door:
                    icon = CargoMatrix.Resources.Icons.Door;
                    break;
                case LocationTypes.Truck:
                    icon = CargoMatrix.Resources.Icons.Truck;
                    break;
                case LocationTypes.Uld:
                    icon = CargoMatrix.Resources.Icons.ULD;
                    break;
                default:
                    break;
            }
            return new SmoothListbox.ListItems.StandardListItem<LocationItem>(item.Location, icon, item.LocationId, item);
        }
    }
}
