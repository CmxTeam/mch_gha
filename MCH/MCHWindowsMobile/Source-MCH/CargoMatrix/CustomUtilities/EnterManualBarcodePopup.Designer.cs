﻿namespace CustomUtilities
{
    partial class EnterManualBarcodePopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.textBox1 = new CargoMatrix.UI.CMXTextBox();
            this.textBox2 = new CargoMatrix.UI.CMXTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.rbnHousebillNo = new System.Windows.Forms.RadioButton();
            this.labelHousebillNo = new System.Windows.Forms.Label();
            this.labelPieceNumber = new System.Windows.Forms.Label();
            this.txtPieceNumber = new CargoMatrix.UI.CMXTextBox();
            this.rbnLocation = new System.Windows.Forms.RadioButton();
            this.labelBarcodeType = new System.Windows.Forms.Label();
            this.labelLocationType = new System.Windows.Forms.Label();
            this.txtLocationType = new CargoMatrix.UI.CMXTextBox();
            this.btnLocationType = new CargoMatrix.UI.CMXPictureButton();

            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.White;
            this.buttonOk.Enabled = false;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok_dis;
            this.buttonOk.Location = new System.Drawing.Point(54, 160);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonOk.TransparentColor = System.Drawing.Color.White;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.White;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.buttonCancel.Location = new System.Drawing.Point(133, 160);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonCancel.TransparentColor = System.Drawing.Color.White;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = Resources.Graphics.Skin.popup_header;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(230, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 93);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Image = Resources.Graphics.Skin.nav_bg;
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.Size = new System.Drawing.Size(230, 43);
            //
            // rbnHousebillNo
            //
            this.rbnHousebillNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.rbnHousebillNo.Location = new System.Drawing.Point(10, 30);
            this.rbnHousebillNo.Name = "rbnHousebillNo";
            this.rbnHousebillNo.Text = "Housebill Scan";
            this.rbnHousebillNo.Size = new System.Drawing.Size(100, 22);
            this.rbnHousebillNo.CheckedChanged += new System.EventHandler(rbnHousebillNo_CheckedChanged);
            this.rbnHousebillNo.Checked = true;
            //
            // rbnLocation
            //
            this.rbnLocation.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.rbnLocation.Location = new System.Drawing.Point(114, 30);
            this.rbnLocation.Name = "rbnLocation";
            this.rbnLocation.Text = "Location Scan";
            this.rbnLocation.Size = new System.Drawing.Size(100, 25);
            this.rbnLocation.CheckedChanged += new System.EventHandler(rbnLocation_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(10, 70);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(130, 28);
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(10, 70);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(210, 28);
            this.textBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBox2.TabIndex = 0;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // labelPieceNumber
            // 
            this.labelPieceNumber.Location = new System.Drawing.Point(150, 55);
            this.labelPieceNumber.Name = "labelPieceNumber";
            this.labelPieceNumber.Size = new System.Drawing.Size(50, 20);
            this.labelPieceNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelPieceNumber.Text = "Piece#";
            // 
            // txtPieceNumber
            // 
            this.txtPieceNumber.Location = new System.Drawing.Point(150, 70);
            this.txtPieceNumber.Name = "txtPieceNumber";
            this.txtPieceNumber.Size = new System.Drawing.Size(70, 22);
            this.txtPieceNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtPieceNumber.TextChanged += new System.EventHandler(this.txtPieceNumber_TextChanged);
            // 
            // labelBarcodeType
            // 
            this.labelBarcodeType.Location = new System.Drawing.Point(10, 55);
            this.labelBarcodeType.Name = "labelBarcodeType";
            this.labelBarcodeType.Size = new System.Drawing.Size(60, 20);
            this.labelBarcodeType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // labelLocationType
            // 
            this.labelLocationType.Location = new System.Drawing.Point(10, 103);
            this.labelLocationType.Name = "labelLocationType";
            this.labelLocationType.Size = new System.Drawing.Size(110, 15);
            this.labelLocationType.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLocationType.Text = "Location Type";
            // 
            // txtLocationType
            // 
            this.txtLocationType.Location = new System.Drawing.Point(10, 120);
            this.txtLocationType.Name = "txtLocationType";
            this.txtLocationType.Size = new System.Drawing.Size(170, 28);
            this.txtLocationType.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtLocationType.Enabled = false;
            // 
            // btnLocationType
            // 
            this.btnLocationType.BackColor = System.Drawing.SystemColors.Control;
            this.btnLocationType.Location = new System.Drawing.Point(187, 115);
            this.btnLocationType.Name = "btnLocationType";
            this.btnLocationType.Size = new System.Drawing.Size(32, 32);
            this.btnLocationType.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnLocationType.TransparentColor = System.Drawing.Color.White;
            this.btnLocationType.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.btnLocationType.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.btnLocationType.Click += new System.EventHandler(this.btnLocationType_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.rbnHousebillNo);
            this.panel1.Controls.Add(this.txtPieceNumber);
            this.panel1.Controls.Add(this.rbnLocation);
            this.panel1.Controls.Add(this.btnLocationType);
            this.panel1.Controls.Add(this.labelHousebillNo);
            this.panel1.Controls.Add(this.labelPieceNumber);
            this.panel1.Controls.Add(this.labelBarcodeType);
            this.panel1.Controls.Add(this.labelLocationType);
            this.panel1.Controls.Add(this.txtLocationType);
            this.panel1.Location = new System.Drawing.Point(5, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 200);
            // 
            // EnterManualBarcodePopup
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "EnterManualBarcodePopup";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ScanEnterPopup_KeyDown);
            this.panel1.ResumeLayout(false);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);
        }

        #endregion

        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private CargoMatrix.UI.CMXTextBox textBox1; // hawb
        private CargoMatrix.UI.CMXTextBox textBox2; // location
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.RadioButton rbnHousebillNo;
        private CargoMatrix.UI.CMXTextBox txtPieceNumber;
        private System.Windows.Forms.RadioButton rbnLocation;
        private CargoMatrix.UI.CMXPictureButton btnLocationType;
        private System.Windows.Forms.Label labelHousebillNo;
        private System.Windows.Forms.Label labelPieceNumber;
        private System.Windows.Forms.Label labelBarcodeType;
        private System.Windows.Forms.Label labelLocationType;
        private CargoMatrix.UI.CMXTextBox txtLocationType;
    }
}