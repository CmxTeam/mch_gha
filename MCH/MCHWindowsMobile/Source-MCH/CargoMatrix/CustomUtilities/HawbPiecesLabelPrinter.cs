﻿using System.Windows.Forms;
using CargoMatrix.Utilities;
using CargoMatrix.Communication;
using SmoothListbox.ListItems;
using System;
using CMXExtensions;
using CargoMatrix.Communication.DTO;
using System.Drawing;
using CargoMatrix.Communication.ScannerUtilityWS;
using System.Collections.Generic;
using System.Linq;

namespace CustomUtilities
{
    public class HawbPiecesLabelPrinter : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        protected Label labelPrinter;
        protected Label labelPrtrTxt;
        protected Label labelHeader;
        protected PictureBox pictureBoxLogo;
        protected CargoMatrix.UI.CMXPictureButton buttonPrinterList;
        private static HawbPiecesLabelPrinter instance;
        protected static LabelPrinter selectedPrinter;
        protected string mawbNo, carrierNo;
        protected string reason;

        protected HawbPiecesLabelPrinter(string mbNo, string carrNo)
        {
            this.carrierNo = carrNo;
            this.mawbNo = mbNo;
            InitializeComponent();
        }
        protected void LoadControl()
        {
            this.Refresh();
            if (selectedPrinter == null)
                selectedPrinter = ScannerUtility.Instance.GetDefaultPrinter(CargoMatrix.Communication.ScannerUtilityWS.LabelTypes.MB);

            DisplayPrinter();
            this.Title = string.Format("{0}-{1}", this.carrierNo, this.mawbNo);
        }
        public static DialogResult Show(string mbNo, string carrNo)
        {
            if (instance == null)
                instance = new HawbPiecesLabelPrinter(mbNo, carrNo);
            else
            {
                instance.mawbNo = mbNo;
                instance.carrierNo = carrNo;
            }
            Cursor.Current = Cursors.Default;
            string rsn;
            if (PrinterUtilities.PopulatePrintReasons(false, out rsn))
            {
                instance.reason = rsn;
                instance.LoadControl();
                return instance.ShowDialog();
            }
            else return DialogResult.Cancel;
        }
        protected void DisplayPrinter()
        {
            labelPrinter.Text = selectedPrinter.PrinterName;
            switch (selectedPrinter.PrinterType)
            {
                case PrinterType.MobileLabel:
                    pictureBoxLogo.Image = CargoMatrix.Resources.Icons.printerBtooth;
                    break;
                case PrinterType.MobileWireless:
                    pictureBoxLogo.Image = CargoMatrix.Resources.Icons.printer_wifi;
                    break;
                default:
                    pictureBoxLogo.Image = CargoMatrix.Resources.Skin.Printer;
                    break;
            }
        }
        protected virtual void InitializeComponent()
        {
            this.labelPrinter = new System.Windows.Forms.Label();
            this.labelHeader = new System.Windows.Forms.Label();
            this.labelPrtrTxt = new System.Windows.Forms.Label();
            this.buttonPrinterList = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBoxLogo = new PictureBox();
            this.SuspendLayout();
            // 
            // labelHeader
            // 
            this.labelHeader.Location = new System.Drawing.Point(20, 26);
            this.labelHeader.Name = "labelPrtrTxt";
            this.labelHeader.Size = new System.Drawing.Size(206, 20);
            this.labelHeader.Font = new System.Drawing.Font("Tahoma", 9, System.Drawing.FontStyle.Bold);
            this.labelHeader.Text = "Print Housebill Labels";
            // 
            // labelPrinter
            // 
            this.labelPrinter.Location = new System.Drawing.Point(60, 70);
            this.labelPrinter.Name = "labelPrinter";
            this.labelPrinter.Size = new System.Drawing.Size(100, 20);
            this.labelPrinter.Font = new System.Drawing.Font("Tahoma", 9, System.Drawing.FontStyle.Bold);
            // 
            // labelPrtrTxt
            // 
            this.labelPrtrTxt.Location = new System.Drawing.Point(60, 50);
            this.labelPrtrTxt.Name = "labelPrtrTxt";
            this.labelPrtrTxt.Size = new System.Drawing.Size(100, 20);
            this.labelPrtrTxt.Font = new System.Drawing.Font("Tahoma", 9, System.Drawing.FontStyle.Bold);
            this.labelPrtrTxt.Text = "Printer:";
            //
            // pictureBoxLogo
            //
            this.pictureBoxLogo.Location = new System.Drawing.Point(20, 50);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxLogo.SizeMode = PictureBoxSizeMode.StretchImage;  
            this.pictureBoxLogo.Text = "pictureBoxLogo";
            // 
            // buttonPrinterList
            // 
            this.buttonPrinterList.Location = new System.Drawing.Point(184, 50);
            this.buttonPrinterList.Name = "buttonPrinterList";
            this.buttonPrinterList.Size = new System.Drawing.Size(32, 32);
            this.buttonPrinterList.TabIndex = 3;
            this.buttonPrinterList.Text = "button1";
            this.buttonPrinterList.Image = CargoMatrix.Resources.Skin.printerButton;
            this.buttonPrinterList.PressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            this.buttonPrinterList.Click += new System.EventHandler(buttonPrinterList_Click);
            // 
            // MawbLabelPrinter
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScaleDimensions = new SizeF(96F, 96F);
            this.panelContent.Controls.Add(this.buttonPrinterList);
            this.panelContent.Controls.Add(this.labelPrinter);
            this.panelContent.Controls.Add(this.labelPrtrTxt);
            this.panelContent.Controls.Add(this.labelHeader);
            this.panelContent.Controls.Add(this.pictureBoxLogo);
            this.Name = "MawbLabelPrinter";
            this.ResumeLayout(false);

        }

        protected virtual void buttonPrinterList_Click(object sender, System.EventArgs e)
        {
            if (PrinterUtilities.PopulateAvailablePrinters(LabelTypes.HB, ref selectedPrinter))
            {
                DisplayPrinter();
            }
        }

        void printers_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            StandardListItem<LabelPrinter> item = (listItem as SmoothListbox.ListItems.StandardListItem<LabelPrinter>);
            selectedPrinter = item.ItemData;
            DisplayPrinter();
        }

        protected override void buttonOk_Click(object sender, System.EventArgs e)
        {
            if (selectedPrinter.PrinterType == PrinterType.MobileLabel ||
                selectedPrinter.PrinterType == PrinterType.MobileWireless)
            {
                var hawbs = CargoMatrix.Communication.ScannerUtility.Instance.GetHouseBillByMasterBill(carrierNo, mawbNo);
                if (hawbs.Length == 0)
                    return;
                
                /// capture printing reason before printing from mobile printer
                CargoMatrix.Communication.ScannerUtility.Instance.CapturePrintReasonForConsole(mawbNo, carrierNo, reason,selectedPrinter.PrinterId);

                var mbilePrinter = new MobilePrinting.MobilePrinterUtility(selectedPrinter);
                mbilePrinter.NumberOfCopies = 1;

                var hbs = from hb in hawbs
                          select new KeyValuePair<string, int>(hb.HouseBillNo, hb.NoOfPieces);
                mbilePrinter.HouseBillPrint(hbs.ToList());

            }
            else
            {

                var response = ScannerUtility.Instance.PrintAllHousebillLabels(selectedPrinter.PrinterId, this.carrierNo, this.mawbNo, reason);
                if (response.TransactionStatus1)
                    CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_PrintQueued + labelPrinter.Text, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                else
                    CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_PrintFailed, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
            DialogResult = DialogResult.OK;
        }
    }
}
