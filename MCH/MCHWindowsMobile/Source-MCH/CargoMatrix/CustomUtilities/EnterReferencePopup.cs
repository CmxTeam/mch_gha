﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public partial class EnterReferencePopup : Form
    {
        public void Reset()
        {
            textBox1.Text = string.Empty;
            labelText.Text = string.Empty;
        }
        public EnterReferencePopup()
        {
            InitializeComponent();
            Title = "Enter number";
            this.textBox1.TextChanged += new EventHandler(textBox1_TextChanged);
        }
        public EnterReferencePopup(string title,string title2):this()
        {
            Title = title;
            Title2 = title2;
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }
        public string Title2
        {
            get { return this.labelText.Text; }
            set { this.labelText.Text = value; }
        }
        public string Title
        {
            get;
            set;
        }
        public string EnteredText
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value.ToUpper(); }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text.ToUpper();
            DialogResult = DialogResult.OK;

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = string.Empty;
            DialogResult = DialogResult.Cancel;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == string.Empty)
            {
                buttonOk.Enabled = false;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok_dis;
            }
            else
            {
                buttonOk.Enabled = true;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            }
        }
        private void ScanEnterPopup_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                buttonOk_Click(null, null);
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                e.Graphics.DrawString(Title, font, new SolidBrush(Color.White), 5, 3);
            }
        }
    }
}