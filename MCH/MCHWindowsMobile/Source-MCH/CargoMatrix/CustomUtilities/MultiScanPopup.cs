﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace CustomUtilities
{
    public partial class MultiScanPopup : Form
    {
        private CargoMatrix.UI.BarcodeReader barcode;
        private bool threeScan;

        private string scannedPrefix1;
        private string scannedPrefix2;
        private string scannedPrefix3;

        public bool ThreeScan
        {
            get { return threeScan; }
            set
            {
                threeScan = value;
                label3.Visible = textBox3.Visible = true;
                if (value)
                    panel1.Height = 266;
                else
                    panel1.Height = 210;
                panel1.Top = (Screen.PrimaryScreen.Bounds.Height - panel1.Height) / 2;
            }
        }
        public MultiScanPopup()
        {
            InitializeComponent();
            barcode = new CargoMatrix.UI.BarcodeReader();
            barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcode_BarcodeReadNotify);
            barcode.StartRead();
            pictureBox1.Paint += new PaintEventHandler(pictureBox1_Paint);
            textBox1.Click += new EventHandler(textBox1_Click);
            textBox1.MouseUp += new MouseEventHandler(textBox1_Clicked);
            textBox1.WatermarkText = textBox2.WatermarkText = textBox3.WatermarkText = "Scan/Enter number";
            textBox1.Text = textBox2.Text = textBox3.Text = string.Empty;

        }

        void textBox1_Clicked(object sender, EventArgs e)
        {
            textBox1.SelectAll();
        }

        void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.SelectAll();
        }

        void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {
                //pictureBox.SendToBack();

                pen.Width = 1;
                e.Graphics.DrawString(Header, new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
            //throw new NotImplementedException();
        }

        void barcode_BarcodeReadNotify(string barcodeData)
        {
            char[] separators = new char[] { '-', '*' };
            int startIndex = barcodeData.IndexOfAny(separators);
            string prefx, bcode;

            if (startIndex > -1)
            {
                prefx = barcodeData.Substring(0, startIndex).ToUpper();
                bcode = barcodeData.Substring(startIndex + 1).ToUpper();
            }
            else
            {
                prefx = string.Empty;
                bcode = barcodeData;

            }
            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);


            if (textBox1.Focused)
            {
                textBox1.Text = bcode;
                scannedPrefix1 = prefx;
                textBox2.Focus();

            }
            else
            {
                if (textBox2.Focused)
                {
                    textBox2.Text = bcode;
                    scannedPrefix2 = prefx;
                    if (threeScan)
                        textBox3.Focus();
                }
                else
                    if (textBox3.Focused)
                    {
                        textBox3.Text = bcode;
                        scannedPrefix3 = prefx;
                    }

            }

            barcode.StartRead();
        }

        public string Header
        {
            get;
            set;
        }
        public string Caption
        {
            get { return labelCaption.Text; }
            set { labelCaption.Text = value; }
        }
        public string Label1
        { set { label1.Text = value; } }
        public string Label2
        { set { label2.Text = value; } }
        public string Label3
        { set { label3.Text = value; } }

        public string Text1
        {
            get { return textBox1.Text.ToUpper();}//string.IsNullOrEmpty(scannedPrefix1) ?  : scannedPrefix1 + '-' + textBox1.Text.ToUpper(); }
            set { textBox1.Text = value; }
        }
        public string Text2
        {
            get { return textBox2.Text.ToUpper(); }//string.IsNullOrEmpty(scannedPrefix2) ?  : scannedPrefix2 + '-' + textBox2.Text.ToUpper(); }
            set { textBox2.Text = value; }
        }
        public string Text3
        {
            get { return textBox3.Text.ToUpper(); }//string.IsNullOrEmpty(scannedPrefix3) ?  : scannedPrefix3 + '-' + textBox3.Text.ToUpper(); }
            set { textBox3.Text = value; }
        }
        public BarcodeType Prefix1;
        public BarcodeType Prefix2;
        public BarcodeType Prefix3;


        public bool Text2ISOptional
        {
            get;
            set;
        }
        public bool Text3IsOptional
        { get; set; }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            barcode.StopRead();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            barcode.StopRead();
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }

        private void textBox_LostFocus(object sender, EventArgs e)
        {
            //barcode.StopRead();
        }

        private void textBox_GotFocus(object sender, EventArgs e)
        {
            (sender as CargoMatrix.UI.CMXTextBox).SelectAll();
        }
        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            if (textBox1.Text != string.Empty && (Text2ISOptional || textBox2.Text != string.Empty) && (threeScan == false || Text3IsOptional || textBox3.Text != string.Empty))
            {
                buttonOk.Enabled = true;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            }
            else
            {
                buttonOk.Enabled = false;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok_dis;

            }
            string name = (sender  as Control).Name;
            SetPrefix(name[name.Length - 1]);
        }
        private void SetPrefix(char number)
        {
            switch (number)
            {
                case '1':
                    setPrefixHelper(ref scannedPrefix1, Prefix1);
                    break;
                case '2':
                    setPrefixHelper(ref scannedPrefix2, Prefix2);
                    break;
                case '3':
                    setPrefixHelper(ref scannedPrefix3, Prefix3);
                    break;
                default:
                    break;
            }
        }
        private void setPrefixHelper(ref string prefix, BarcodeType prefixType)
        {
            switch (prefixType)
            {
                case BarcodeType.ULD:
                    prefix = "L";
                    break;
                case BarcodeType.Truck:
                    prefix = "T";
                    break;
                case BarcodeType.Door:
                    prefix = "D";
                    break;
            }
        }
    }
}