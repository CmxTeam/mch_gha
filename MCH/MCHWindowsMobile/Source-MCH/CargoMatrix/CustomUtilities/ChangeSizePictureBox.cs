﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public partial class ChangeSizePictureBox : System.Windows.Forms.PictureBox
    {
        string header;
        bool isEnlarged;
        bool enlargeToParent;
        Size sizeEnlarged;
        Size sizeRegular;
        Point locationRegular;
        Point locationEnlarged;
        bool persistLocation;
        SizeF autoscaleDimantions;

        public SizeF AutoscaleDimantions
        {
            get { return autoscaleDimantions; }
            set { autoscaleDimantions = value; }
        }

        public Point LocationEnlarged
        {
            get { return locationEnlarged; }
            set { locationEnlarged = value; }
        }
        
        public Size SizeRegular
        {
            get { return sizeRegular; }
            set { sizeRegular = value; }
        }

        public Size SizeEnlarged
        {
            get { return sizeEnlarged; }
            set { sizeEnlarged = value; }
        }

        public Point LocationRegular
        {
            get { return locationRegular; }
            set { locationRegular = value; }
        }

        public string Header
        {
            get { return header; }
            set { header = value; }
        }
     
        public bool IsEnlarged
        {
            get { return isEnlarged; }
            set { isEnlarged = value; }
        }

        public bool EnlargeToParent
        {
            get { return enlargeToParent; }
            set { this.enlargeToParent = value; }
        }

        public bool PersistLocation
        {
            get { return persistLocation; }
            set { persistLocation = value; }
        }


        public ChangeSizePictureBox()
        {
            InitializeComponent();

            this.isEnlarged = false;
            this.sizeEnlarged = this.Size;
            this.sizeRegular = this.Size;
            this.locationRegular = this.Location;
            this.autoscaleDimantions = new SizeF();

        }


        protected override void OnPaint(PaintEventArgs pe)
        {
            // Calling the base class OnPaint
            base.OnPaint(pe);

            if (string.IsNullOrEmpty(this.Header) == false && this.isEnlarged == true)
            {
                using (Brush brush = new SolidBrush(Color.Gray))
                {
                    pe.Graphics.DrawString(this.Header,
                                          new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold),
                                          brush, 5, this.Height - 30);
                }
            }
        }

        private void ChangeSizaPictureBox_Click(object sender, EventArgs e)
        {
            this.isEnlarged = !this.isEnlarged;

            if (this.isEnlarged)
            {
                if (this.persistLocation == false)
                {
                    this.Location = this.locationEnlarged != null ? this.locationEnlarged : new Point(0, 0); 
                }


                if (this.enlargeToParent)
                {
                    this.Size = this.Parent.Size;
                }
                else
                {
                    this.Size = this.sizeEnlarged;
                }
            }
            else
            {
                this.Size = this.sizeRegular;
                this.Location = this.locationRegular;
            }

            this.Scale( new SizeF(this.autoscaleDimantions.Width / 96F, this.autoscaleDimantions.Height / 96F));

            this.Invalidate();

        }
    }
}
