﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public partial class PassFailMessageBox : Form
    {
        private static PassFailMessageBox instance = null;
        private string header;
        public PassFailMessageBox()
        {
            InitializeComponent();
        }
        public static DialogResult Show( string header,string message)
        {

            instance = new PassFailMessageBox();
            instance.header = header;
            instance.labelMessage.Text = message;

            return instance.ShowDialog();
        }
        public static DialogResult Show(string header,string message,  string okButtonText, string cancelButtonText)
        {

            instance = new PassFailMessageBox();
            instance.buttonOk.Text = okButtonText;
            instance.buttonCancel.Text = cancelButtonText;
            instance.header = header;
            instance.labelMessage.Text = message;
            return instance.ShowDialog();
        }

        private void initializeImages()
        {
            this.buttonCancel.BackgroundImage = CargoMatrix.Resources.Skin.button_default;
            this.buttonCancel.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;
            this.buttonOk.BackgroundImage = CargoMatrix.Resources.Skin.button_default;
            this.buttonOk.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;
            this.pictureBoxHeader.Image = global::Resources.Graphics.Skin.popup_header;
        }

        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {
                pen.Width = 1;
                e.Graphics.DrawString(header, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}