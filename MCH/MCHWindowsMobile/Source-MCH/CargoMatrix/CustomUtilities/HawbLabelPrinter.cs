﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CargoMatrix.Utilities;
using System.Drawing;
using CargoMatrix.Communication.ScannerUtilityWS;
using System.Linq;
using CustomUtilities;

namespace CustomUtilities
{
    public class HawbLabelPrinter
    {
        //private static HawbLabelPrinter instance;
        protected MessageListBox printModePopup;
        protected int[] pieceIds;
        protected string hawbNo;
        protected long hawbID;
        protected LabelPrinter selectedPrinter;
        protected CargoMatrix.UI.BarcodeReader barcode;
        protected string reason;
        protected LabelTypes labelType;

        private void createModePopup()
        {
            Control[] actions = new Control[] {
                new StandardListItem("SELECT ALL",null,3), 
                new StandardListItem("SELECT RANGE",null,2){Enabled = pieceIds.Length > 1},
                new StandardListItem("SELECT PIECES",null,1){Enabled = pieceIds.Length > 1} };

            printModePopup = new CargoMatrix.Utilities.MessageListBox();
            printModePopup.OkEnabled = false;
            printModePopup.ButtonVisible = true;
            printModePopup.ButtonImage = CargoMatrix.Resources.Skin.printerButton;
            printModePopup.ButtonPressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            printModePopup.ButtonClick += new EventHandler(printModePopup_ButtonClick);
            printModePopup.MultiSelectListEnabled = false;
            printModePopup.HeaderText = hawbNo;
            printModePopup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(printModePopup_ListItemClicked);
            printModePopup.OneTouchSelection = false;
            selectedPrinter = CargoMatrix.Communication.ScannerUtility.Instance.GetDefaultPrinter(labelType);


            printModePopup.HeaderText2 = "Printer " + selectedPrinter.PrinterName;
            printModePopup.AddItems(actions);

            barcode = new CargoMatrix.UI.BarcodeReader();
        }


        protected virtual void printModePopup_ButtonClick(object sender, EventArgs e)
        {
            if (PrinterUtilities.PopulateAvailablePrinters(labelType, ref selectedPrinter))
            {
                printModePopup.HeaderText2 = "Printer " + selectedPrinter.PrinterName;
            }
        }
        protected HawbLabelPrinter(long hawbId, string hawbNo, int noOfPcs, LabelTypes lblType)
        {
            pieceIds = Enumerable.Range(1, noOfPcs).ToArray<int>();


            this.hawbID = hawbId;
            this.hawbNo = hawbNo;
            this.labelType = lblType;
            createModePopup();
        }
        private HawbLabelPrinter(long hawbId, string hawbNo, int noOfPcs)
            : this(hawbId, hawbNo, noOfPcs, LabelTypes.HB)
        {
        }
        private void printModePopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            CustomUtilities.NumericPad numPad;
            //int[] pcsIDs = selectedPrinter.PrinterType == PrinterType.MobileLabel  ? convertPieceIdsToNumbers() : pieceIds;

            switch ((listItem as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 3:// select all
                    PrintLabels(pieceIds.ToList<int>());
                    printModePopup.Close();
                    break;
                case 2:// select range
                    numPad = new CustomUtilities.NumericPad(pieceIds, true);
                    numPad.Title = "Select pieces to print label";

                    if (DialogResult.OK == numPad.ShowDialog())
                    {
                        PrintLabels(numPad.SelectedIDs.ToList());
                        printModePopup.Close();
                    }
                    else printModePopup.ResetSelection();

                    break;// select random
                case 1:


                    numPad = new CustomUtilities.NumericPad(pieceIds);
                    numPad.Title = "Select pieces to print label";


                    if (DialogResult.OK == numPad.ShowDialog())
                    {
                        PrintLabels(numPad.SelectedIDs.ToList());
                        printModePopup.Close();
                    }
                    else printModePopup.ResetSelection();


                    break;
            }
        }

        protected virtual void PrintLabels(List<int> piecesToPrint)
        {

            if (selectedPrinter.PrinterType == PrinterType.MobileLabel ||
                selectedPrinter.PrinterType == PrinterType.MobileWireless)
            {
                /// Capture printing reason if printed from mobile printer
                CargoMatrix.Communication.ScannerUtility.Instance.CapturePrintReason(hawbNo, string.Empty, reason, false, piecesToPrint.Count, selectedPrinter.PrinterId);
                var mbilePrinter = new MobilePrinting.MobilePrinterUtility(selectedPrinter);
                var pieces = (from p in piecesToPrint select p.ToString()).ToArray();
                mbilePrinter.HouseBillPrint(hawbNo, pieces);
            }
            else            /// if network printer
            {
                string pcsString = string.Join(",", piecesToPrint.ConvertAll<string>(p => p.ToString()).ToArray());
                var status = CargoMatrix.Communication.ScannerUtility.Instance.PrintHouseBillLabels((int)hawbID, selectedPrinter.PrinterId, pcsString, reason);
                if (status.TransactionStatus1)
                    CargoMatrix.UI.CMXMessageBox.Show("Labels printing has been queued on " + selectedPrinter.PrinterName, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                else
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Fail", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
        }


        public static bool Show(long hawbId, string hawbNo, int NumberOfPcs)
        {
            var instance = new HawbLabelPrinter(hawbId, hawbNo, NumberOfPcs);
            if (PrinterUtilities.PopulatePrintReasons(false, out instance.reason))
            {
                instance.printModePopup.ShowDialog();
            }
            //instance.barcode.StopRead();
            return false;
        }

    }
}