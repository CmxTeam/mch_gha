﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.InventoryWS;
using CargoMatrix.Communication.InventoryCom;

namespace CustomUtilities
{
    public partial class HawbDetailedItem : UserControl
    {
        private CargoMatrix.Communication.InventoryWS.HouseBillItem hawb;
        public CargoMatrix.Communication.InventoryWS.HouseBillItem Hawb { get { return hawb; } }
        public event EventHandler ButtonDetailsClick;
        public event EventHandler ButtonDamageClick;
        public event EventHandler ButtonBrowseClick;
        public void DisplayHousebill(CargoMatrix.Communication.InventoryWS.HouseBillItem hawb)
        {
            this.hawb = hawb;
            title.Text = hawb.Reference();
            labelWeight.Text = hawb.Weight + "KGS";
            labelPieces.Text = hawb.Pieces();
            labelLoc.Text = hawb.LastLocation;
            labelSender.Text = hawb.ShipperName;
            labelReceiver.Text = hawb.ConsigneeName;
            labelDescr.Text = hawb.Description;
            labelLastScan.Text = hawb.LastScan;

            switch (hawb.Status)
            {
                case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Open:
                case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;

            }
            panelIndicators.Flags = hawb.Flags;
            panelIndicators.PopupHeader = hawb.Reference();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

        }

        public HawbDetailedItem()
        {
            InitializeComponent();
        }

        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            if (ButtonDetailsClick != null)
                ButtonDetailsClick(this, e);
            //DisplayBillViewer(hawb.HousebillNumber);
        }
        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            if (ButtonBrowseClick != null)
                ButtonBrowseClick(this, e);
        }
        void buttonDamage_Click(object sender, System.EventArgs e)
        {
            if (ButtonDamageClick != null)
                ButtonDamageClick(this, e);

            //Cursor.Current = Cursors.WaitCursor;

            //InventoryHouseBilItem invHouseBill = new InventoryHouseBilItem(hawb);

            //CargoMatrix.DamageCapture.DamageCapture damageCapture = new CargoMatrix.DamageCapture.DamageCapture(invHouseBill);

            //damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            //{
            //    args.ShipmentConditions = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionTypes();
            //};

            //damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            //{
            //    args.ConditionsSummaries = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionSummary(args.HouseBillID);
            //};

            //damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            //{
            //    args.Result = CargoMatrix.Communication.LoadConsol.Instance.UpdateShippmentCondition(args.HouseBillId, args.ScanModes, args.ShipmentConditions, Forklift.Instance.TaskID);
            //};

            //damageCapture.GetHouseBillPiecesIDs = () =>
            //{
            //    return
            //        (
            //            from item in CargoMatrix.Communication.LoadConsol.Instance.GetHousebillInfo(invHouseBill.HousebillId, Forklift.Instance.TaskID).Pieces
            //            select item.PieceId
            //        ).ToArray();
            //};



            //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(damageCapture);
            //Cursor.Current = Cursors.Default;
        }
        /*
        CargoMatrix.Utilities.MessageListBox actPopup;
        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            parent.BarcodeEnabled = false;
            if (actPopup == null)
            {
                List<Control> actions = new List<Control> { 
                new SmoothListbox.ListItems.StandardListItem("SWITCH MODE",null,3),
                new SmoothListbox.ListItems.StandardListItem("PRINT LABELS",null,2),
                new SmoothListbox.ListItems.StandardListItem("REPORT MISSING",null,4),
                new SmoothListbox.ListItems.StandardListItem("REPORT OVER",null,5),
                new SmoothListbox.ListItems.StandardListItem("DOWNLOAD HAWB",null,1), };
                
                actPopup = new CargoMatrix.Utilities.MessageListBox();
                actPopup.AddItems(actions);
                actPopup.MultiSelectListEnabled = false;
                actPopup.OneTouchSelection = true;
            }
            actPopup.HeaderText = title.Text;
            actPopup.HeaderText2 = "Select action for " + hawb.HousebillNumber;

            if (DialogResult.OK == actPopup.ShowDialog())
            {

                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 4:
 
                                if (Hawb.ScanMode == ScanModes.Count)
                                    ReportMissingInCountMode();
                                else
                                    ReportMissingInPieceMode();

                        break;
                    case 5:

                                ReportOverInCountMode();

                        break;
                    case 3:
                        SwitchMode();
                        var refreshed = CargoMatrix.Communication.Inventory.Instance.GetHousebillInfo(hawb.HousebillId, Forklift.Instance.TaskID);
                        DisplayHousebill(refreshed);
                        break;
                    case 2:
                        PrintLabels();
                        break;
                    case 1:
                        DownloadHawb();
                        break;
                }
            }
            actPopup.ResetSelection();
            parent.BarcodeEnabled = true;
        }
        private void ReportMissingInCountMode()
        {
            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.PieceCount = Hawb.TotalPieces;
            CntMsg.HeaderText = InventoryResources.Text_CountHeader;
            CntMsg.LabelDescription = InventoryResources.Text_CountBoxMissing;
            CntMsg.LabelReference = Hawb.Reference();

            CustomUtilities.RemarksMessageBox rmrksMsg = new CustomUtilities.RemarksMessageBox("Missing Remark");


            if (CntMsg.ShowDialog() == DialogResult.OK && rmrksMsg.ShowDialog() == DialogResult.OK)
            {
                
                TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInCount(Hawb.HousebillId, CntMsg.PieceCount, Forklift.Instance.TaskID,rmrksMsg.TextEntered);
                if( status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
        }

        private void ReportMissingInPieceMode()
        {
            CargoMatrix.Utilities.MessageListBox notScanned = new CargoMatrix.Utilities.MessageListBox();
            notScanned.MultiSelectListEnabled = true;
            notScanned.UnselectListEnabled = true;
            notScanned.HeaderText = Hawb.Reference();
            notScanned.HeaderText2 = "Select Missing Pieces";
            
            CustomUtilities.RemarksMessageBox rmrksMsg = new CustomUtilities.RemarksMessageBox("Missing Remark");

            var pieces = from p in CargoMatrix.Communication.Inventory.Instance.GetNotScannedPieces(Hawb.HousebillId, Forklift.Instance.TaskID)
                         select new SmoothListbox.ListItems.StandardListItem("Piece " + p.PieceNumber, null, p.PieceId) as Control;
            notScanned.AddItems(pieces.ToList<Control>());


            if (DialogResult.OK == notScanned.ShowDialog() && rmrksMsg.ShowDialog() == DialogResult.OK)
            {
                foreach (var item in notScanned.SelectedItems)
                {
                    var tStatus = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInPiece(Hawb.HousebillId, (item as SmoothListbox.ListItems.StandardListItem).ID, Forklift.Instance.TaskID,rmrksMsg.TextEntered);
                    if (!tStatus.TransactionStatus1)
                        CargoMatrix.UI.CMXMessageBox.Show(tStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }

        }

        private void ReportOverInCountMode()
        {
            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.PieceCount = int.MaxValue;
            CntMsg.HeaderText = InventoryResources.Text_CountHeader;
            CntMsg.LabelDescription = InventoryResources.Text_CountBoxOver;
            CntMsg.LabelReference = Hawb.Reference();
            CustomUtilities.RemarksMessageBox rmrksMsg = new CustomUtilities.RemarksMessageBox("Missing Remark");

            if (CntMsg.ShowDialog() == DialogResult.OK && rmrksMsg.ShowDialog() == DialogResult.OK)
            {
                CargoMatrix.Communication.Inventory.Instance.ReportOverPiecesInCount(Hawb.HousebillId, CntMsg.PieceCount, Forklift.Instance.TaskID,rmrksMsg.TextEntered);
            }
        }
        private void DisplayBillViewer(string hawbNo)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (billViewer != null)
                billViewer.Dispose();

            billViewer = new CargoMatrix.Viewer.HousebillViewer(hawbNo);//"3TB7204" masterbill);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
            Cursor.Current = Cursors.Default;
        }
        private void DownloadHawb()
        {
            int downId = CargoMatrix.Communication.Inventory.Instance.DownloadHousebillDetails(hawb.HousebillNumber);
            CargoMatrix.UI.CMXMessageBox.Show("Download for Housbill " + hawb.HousebillNumber + " has been scheduled", "Donwload Scheduled", CargoMatrix.UI.CMXMessageBoxIcon.Success);

            if (this.Parent.Parent is HawbInventory)
            {
                (this.Parent.Parent as HawbInventory).QueueDownload(new HaousebillDownload(downId, hawb.HousebillNumber));
            }
        }

        internal void PrintLabels()
        {
            int[] pieceIds = (from p in hawb.Pieces
                              select p.PieceId).ToArray<int>();
            CustomUtilities.HawbLabelPrinter.Show(hawb.HousebillId, hawb.Reference(), pieceIds);
        }

        #region Switch Mode
        private bool SwitchMode()
        {
            bool retValue = false;
            /// piece Mode -> Count
            if (hawb.ScanMode == ScanModes.Piece)
            {
                string msg = string.Format(InventoryResources.Text_ChangeModeToCount, hawb.HousebillNumber);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin || DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {
                        TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.ChangeHawbMode(hawb.HousebillId, Forklift.Instance.TaskID, ScanModes.Count);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(InventoryResources.Text_ChangetoCountConfirm, hawb.HousebillNumber), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            // Count Mode -> Piece
            else
            {
                string msg = string.Format(InventoryResources.Text_ChangeModeToPiece, hawb.HousebillNumber);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {

                    TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.ChangeHawbMode(hawb.HousebillId, Forklift.Instance.TaskID, ScanModes.Piece);
                    if (status.TransactionStatus1)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(string.Format(InventoryResources.Text_ChangeToPieceConfirm, hawb.HousebillNumber), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                        retValue = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        retValue = false;
                    }

                }
            }
            return retValue;
        }
        #endregion



        */

    }
}
