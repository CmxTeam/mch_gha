﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmoothListbox.ListItems;
using SmoothListbox;
using CargoMatrix.Communication.TaskManager;
using System.Drawing;
using CustomListItems;

namespace CargoMatrix.TaskManager
{
    public class TaskItemRendered : RenderItemBase, ISmartListItem
    {
        private OpenNETCF.Windows.Forms.PictureBox2 pcxIcon;
        private CargoMatrix.UI.CMXPictureButton btnDetails;

        public event EventHandler ButtonDetailsClick;

        BusinessTask task;

        public BusinessTask Task
        {
            get { return task; }
        }

        public TaskItemRendered(BusinessTask task)
        {
            this.task = task;

            this.InitializeItems();

            this.RetentionLevel = 2;
        }

        private void InitializeItems()
        {
            this.SuspendLayout();

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.BackColor = Color.White;
            this.Size = new System.Drawing.Size(240, 67);
           
            this.ResumeLayout(false);
        }
        
        
        
        protected override void InitializeControls()
        {
            this.pcxIcon = new OpenNETCF.Windows.Forms.PictureBox2();
            this.btnDetails = new CargoMatrix.UI.CMXPictureButton();
          
            var rHeight = (int)(this.CurrentAutoScaleDimensions.Height / 96F);
            var rWidth = (int)(this.CurrentAutoScaleDimensions.Width / 96F);

            // 
            // pcxIcon
            // 
            this.pcxIcon.Location = new System.Drawing.Point(3 * rWidth, 3 * rHeight);
            this.pcxIcon.Name = "pcxLogo";
            this.pcxIcon.Size = new System.Drawing.Size(24 * rWidth, 24 * rHeight);
            this.pcxIcon.TransparentColor = System.Drawing.Color.White;
            this.pcxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;


            // 
            // btnDetails
            // 
            this.btnDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.btnDetails.Location = new System.Drawing.Point((int)(195 * rWidth), (int)(25 * rHeight));
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.btnDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            this.btnDetails.Size = new System.Drawing.Size((int)(32 * rWidth), (int)(32 * rHeight));
            this.btnDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetails.TransparentColor = System.Drawing.Color.White;
            this.btnDetails.Visible = true;
            this.btnDetails.Click += new EventHandler(btnDetails_Click);
    
            this.Controls.Add(this.pcxIcon);
            this.Controls.Add(this.btnDetails);

            this.btnDetails.BringToFront();
        }

        void btnDetails_Click(object sender, EventArgs e)
        {
            var detailsClick = this.ButtonDetailsClick;

            if(detailsClick != null)
            {
                detailsClick(this, EventArgs.Empty);
            }
        }

        protected override void Draw(System.Drawing.Graphics gOffScreen)
        {
            this.SuspendLayout();

            var rHeight = this.CurrentAutoScaleDimensions.Height / 96F;
            var rWidth = this.CurrentAutoScaleDimensions.Width / 96F;

            int linesInterval = 16;

            using (Brush brush = new SolidBrush(Color.Black))
            {
                gOffScreen.DrawString(string.Format("{0} ({1})", this.task.GeneralReference, this.task.Percent),
                                     new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold), brush, 30 * rWidth, 3 * rHeight);


                gOffScreen.DrawString(string.Format("Pcs: {0}", this.task.Pieces),
                                    new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular), brush, 40 * rWidth, (3 + linesInterval) * rHeight);


                gOffScreen.DrawString(string.Format("Wgt: {0} KGS", this.task.Weight),
                                    new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular), brush, 90 * rWidth, (3 + linesInterval) * rHeight);


                string description = "Ulds:";

                if (task.Ulds.Count > 0)
                {
                    description = task.Ulds.Count == 1 ?
                        string.Format("{0} {1}", description, task.Ulds[0]) :
                        string.Format("{0} {1} ...", description, task.Ulds[0]);
                }

                gOffScreen.DrawString(description,
                                    new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular), brush, 40 * rWidth, (3 + linesInterval * 2) * rHeight);
                
                
                description = "Users:";

                if (task.UserIDs.Count > 0)
                {
                    description = task.UserIDs.Count == 1 ?
                        string.Format("{0} {1}", description, task.UserIDs[0]) :
                        string.Format("{0} {1} ...", description, task.UserIDs[0]);
                }
                
                gOffScreen.DrawString(description,
                                    new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular), brush, 40 * rWidth, (3 + linesInterval * 3) * rHeight);

            }

            using (Pen pen = new Pen(Color.Gainsboro))
            {
                gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
            }


            switch (this.task.Status)
            {
                case TaskProgressTypeEnum.Completed:
                    this.pcxIcon.Image = CustomListItemsResource.Clipboard_Check;
                    break;

                case TaskProgressTypeEnum.InProgress:
                    this.pcxIcon.Image = CustomListItemsResource.History;
                    break;

                case TaskProgressTypeEnum.NotAssigned:
                case TaskProgressTypeEnum.NotStarted:
                    this.pcxIcon.Image = CustomListItemsResource.Clipboard;
                    break;

            }

            this.btnDetails.BringToFront();
            this.btnDetails.Refresh();

            this.ResumeLayout(false);
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string textToUpper = text.ToUpper();

            return this.task.Reference.ToUpper().Contains(textToUpper) == true ||
                   this.task.UserIDs.Exists(id => id.ToUpper().Contains(textToUpper)) == true;
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
