﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.TaskManager;


namespace CargoMatrix.TaskManager
{
    public partial class TaskDetailsControl : UserControl
    {
        public TaskDetailsControl()
        {
            InitializeComponent();
        }


        public void PopulateTask(CargoMatrix.Communication.WSTaskManager.Task task)
        {
            this.lblTask.Text = task.TaskName;
            this.lblConsol.Text = task.Reference;

            this.lblCloseOut.Text = task.CloseoutTime.HasValue ? task.CloseoutTime.Value.ToString("G") : string.Empty;

            this.lblStatus.Text = task.Status.AsString();
            this.lblStart.Text = task.StartDate.HasValue ? task.StartDate.Value.ToString("G") : string.Empty;
            this.lblEnd.Text = task.EndDate.HasValue ? task.EndDate.Value.ToString("G") : string.Empty;
            this.lblPieces.Text = string.Format("{0} of {1}", task.CompletedPieces, task.TotalPieces);
            this.lblLastHawb.Text = task.LastScanHouseBillNo;
            this.lblLastScan.Text = task.LastScan.HasValue ? task.LastScan.Value.ToString("G") : string.Empty;
            this.lblProgress.Text = double.IsNaN(task.Progress) == false ? string.Format("{0:N2} %", task.Progress) : string.Empty;

            this.Refresh();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            using (Pen pen = new Pen(Color.Gainsboro))
            {
                e.Graphics.DrawLine(pen, 0, Height - 1, Width, Height - 1);
            }

        }
    }
}
