﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmoothListbox.ListItems;
using SmoothListbox;
using CargoMatrix.Communication.TaskManager;
using System.Drawing;

namespace CargoMatrix.TaskManager
{
    public class UserItemRendered : RenderItemBase, ISmartListItem
    {
        private OpenNETCF.Windows.Forms.PictureBox2 pcxIcon;

        bool isChecked;
        IBusinessUser user;

        public bool IsChecked 
        {
            get { return this.isChecked; }
            set
            {
                this.isChecked = value;
                this.UpdateIcon();
            }
        }

        public IBusinessUser User
        {
            get { return user; }
        }

        public UserItemRendered(IBusinessUser user, bool isChecked)
        {
            this.user = user;
            this.isChecked = isChecked;
          
            this.InitializeItems();

            this.RetentionLevel = 2;
        }

        public UserItemRendered(IBusinessUser user)
            : this(user, false)
        {

        }


        private void InitializeItems()
        {
            this.SuspendLayout();

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.BackColor = Color.White;
            this.Size = new System.Drawing.Size(240, 37);

            this.ResumeLayout(false);
        }


        protected override void InitializeControls()
        {
            this.pcxIcon = new OpenNETCF.Windows.Forms.PictureBox2();

            var rHeight = (int)(this.CurrentAutoScaleDimensions.Height / 96F);
            var rWidth = (int)(this.CurrentAutoScaleDimensions.Width / 96F);

            // 
            // pcxIcon
            // 
            this.pcxIcon.Location = new System.Drawing.Point(3 * rWidth, 5 * rHeight);
            this.pcxIcon.Name = "pcxLogo";
            this.pcxIcon.Size = new System.Drawing.Size(24 * rWidth, 24 * rHeight);
            this.pcxIcon.TransparentColor = System.Drawing.Color.White;
            this.pcxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            this.Controls.Add(this.pcxIcon);
        }

        protected override void Draw(System.Drawing.Graphics gOffScreen)
        {
            this.SuspendLayout();

            var rHeight = this.CurrentAutoScaleDimensions.Height / 96F;
            var rWidth = this.CurrentAutoScaleDimensions.Width / 96F;

            using (Brush brush = new SolidBrush(Color.Black))
            {
                gOffScreen.DrawString(string.Format("{0} {1}", this.user.FirstName, this.user.LastName),
                                     new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), brush, 40 * rWidth, 3 * rHeight);

                gOffScreen.DrawString(this.user.UserId,
                                    new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular), brush, 40 * rWidth, 19 * rHeight);
            }

            using (Pen pen = new Pen(Color.Gainsboro))
            {
                gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
            }

            this.UpdateIcon();

            this.ResumeLayout(false);
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string textToUpper = text.ToUpper();

            return this.user.UserId.ToUpper().Contains(textToUpper) ||
                   this.user.FirstName.ToUpper().Contains(textToUpper) ||
                   this.user.LastName.ToUpper().Contains(textToUpper);
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion

        private void UpdateIcon()
        {
            this.pcxIcon.Image = this.IsChecked == true ? CargoMatrix.Resources.Skin.Worker_check : CargoMatrix.Resources.Skin.worker;
        }
    }
}
