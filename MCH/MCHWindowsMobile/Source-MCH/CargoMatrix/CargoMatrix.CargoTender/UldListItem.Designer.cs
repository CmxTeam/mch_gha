﻿namespace CargoMatrix.CargoTender
{
    partial class UldListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLine1 = new System.Windows.Forms.Label();
            this.lblPiecesHdr = new System.Windows.Forms.Label();
            this.ButtonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.pcxLogo = new OpenNETCF.Windows.Forms.PictureBox2();
            this.lblLine3 = new System.Windows.Forms.Label();
            this.lblPieces = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();

            this.SuspendLayout();
            // 
            // lblLine1
            // 
            this.lblLine1.BackColor = System.Drawing.Color.White;
            this.lblLine1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblLine1.Location = new System.Drawing.Point(42, 2);
            this.lblLine1.Name = "lblLine1";
            this.lblLine1.Size = new System.Drawing.Size(156, 26);
            // 
            // lblPiecesHdr
            // 
            this.lblPiecesHdr.BackColor = System.Drawing.Color.White;
            this.lblPiecesHdr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblPiecesHdr.Location = new System.Drawing.Point(42, 28);
            this.lblPiecesHdr.Name = "lblPiecesHdr";
            this.lblPiecesHdr.Size = new System.Drawing.Size(32, 12);
            this.lblPiecesHdr.Text = "PCS:";
            // 
            // ButtonBrowse
            // 
            this.ButtonBrowse.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonBrowse.Image = global::Resources.Graphics.Skin._3dots;
            this.ButtonBrowse.Location = new System.Drawing.Point(203, 5);
            this.ButtonBrowse.Name = "ButtonBrowse";
            this.ButtonBrowse.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.ButtonBrowse.Size = new System.Drawing.Size(32, 32);
            this.ButtonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ButtonBrowse.TransparentColor = System.Drawing.Color.White;
            this.ButtonBrowse.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // pcxLogo
            // 
            this.pcxLogo.BackColor = System.Drawing.SystemColors.Control;
            this.pcxLogo.Location = new System.Drawing.Point(4, 5);
            this.pcxLogo.Name = "pcxLogo";
            this.pcxLogo.Size = new System.Drawing.Size(32, 32);
            this.pcxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcxLogo.TransparentColor = System.Drawing.Color.White;
            // 
            // lblLine3
            // 
            this.lblLine3.BackColor = System.Drawing.Color.White;
            this.lblLine3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblLine3.Location = new System.Drawing.Point(42, 40);
            this.lblLine3.Name = "lblLine3";
            this.lblLine3.Size = new System.Drawing.Size(156, 12);
            // 
            // lblPieces
            // 
            this.lblPieces.BackColor = System.Drawing.Color.White;
            this.lblPieces.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblPieces.Location = new System.Drawing.Point(67, 28);
            this.lblPieces.Name = "lblPieces";
            this.lblPieces.Size = new System.Drawing.Size(56, 12);
            // 
            // lblWeight
            // 
            this.lblWeight.BackColor = System.Drawing.Color.White;
            this.lblWeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWeight.Location = new System.Drawing.Point(123, 28);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(75, 12);
            // 
            // ULDViewItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblPieces);
            this.Controls.Add(this.lblLine3);
            this.Controls.Add(this.lblLine1);
            this.Controls.Add(this.pcxLogo);
            this.Controls.Add(this.ButtonBrowse);
            this.Controls.Add(this.lblPiecesHdr);
            this.Name = "ULDViewItem";
            this.Size = new System.Drawing.Size(240, 63);

            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLine1;
        private System.Windows.Forms.Label lblPiecesHdr;
        private CargoMatrix.UI.CMXPictureButton ButtonBrowse;
        private OpenNETCF.Windows.Forms.PictureBox2 pcxLogo;
        private System.Windows.Forms.Label lblLine3;
        private System.Windows.Forms.Label lblPieces;
        private System.Windows.Forms.Label lblWeight;
    }
}
