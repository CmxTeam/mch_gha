﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace PictureButton
{

    public partial class CMXPictureButton : UserControl
    {
        //Private members      
        private Image image;
        //flag to indicate the pressed state
        private bool bPushed;
        private Bitmap m_bmpOffscreen;


        public CMXPictureButton()
        {
            InitializeComponent();
            bPushed = false;
            //default minimal size
            this.Size = new Size(48, 48);



        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics gxOff;      //Offscreen graphics
            Rectangle imgRect; //image rectangle
            Brush backBrush;   //brush for filling a backcolor

            if (m_bmpOffscreen == null) //Bitmap for doublebuffering
            {
                m_bmpOffscreen = new Bitmap(ClientSize.Width, ClientSize.Height);
            }

            gxOff = Graphics.FromImage(m_bmpOffscreen);

            gxOff.Clear(this.BackColor);

            if (!bPushed)
                //backBrush = new SolidBrush(Color.Green);
                backBrush = new SolidBrush(Parent.BackColor);
            else //change the background when it's pressed
                backBrush = new SolidBrush(Color.LightGray);

            gxOff.FillRectangle(backBrush, this.ClientRectangle);

            if (image != null)
            {
                //Center the image relativelly to the control
                int imageLeft = (this.Width - image.Width) / 2;
                int imageTop = (this.Height - image.Height) / 2;




                if (!bPushed)
                {
                    imgRect = new Rectangle(imageLeft, imageTop, image.Width,
            image.Height);
                }
                else //The button was pressed
                {
                    //Shift the image by one pixel
                    imgRect = new Rectangle(imageLeft + 1, imageTop + 1, image.Width,
                                                                     image.Height);
                }
                //Set transparent key
                ImageAttributes imageAttr = new ImageAttributes();

                imageAttr.SetColorKey(BackgroundImageColor(image),
                                                      BackgroundImageColor(image));
                //Draw image

                gxOff.DrawImage(image, imgRect, 0, 0, image.Width, image.Height,
                                                    GraphicsUnit.Pixel, imageAttr);

            }
            

            if (bPushed) //The button was pressed
            {
                //Prepare rectangle
                Rectangle rc = this.ClientRectangle;
                rc.Width--;
                rc.Height--;
                //Draw rectangle
                
                using (Pen pen = new Pen(Color.Red))
                {
                    gxOff.DrawRectangle(pen, rc);
                }
                //OpenNETCF.Drawing.GraphicsEx graph = OpenNETCF.Drawing.GraphicsEx.FromGraphics(gxOff);
                //using (OpenNETCF.Drawing.PenEx pen = new OpenNETCF.Drawing.PenEx(Color.Red))
                //{
                    
                //    graph.DrawRoundRectangle(pen, rc, new Size(5, 5));
                //}
                //graph.Dispose();
                //graph.DrawRoundRectangle(new open
            }

            //Draw from the memory bitmap
            e.Graphics.DrawImage(m_bmpOffscreen, 0, 0);

            base.OnPaint(e);

        }

        private Color BackgroundImageColor(Image image)
        {
            Bitmap bmp = new Bitmap(image);
            return bmp.GetPixel(0, 0);
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            bPushed = true;
            this.Invalidate();
        }

        protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            bPushed = false;
            this.Invalidate();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
        }
        public Image Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                //this.Size = new Size(Image.Width, image.Height);
                Invalidate();

            }
        }
        
    }

    /*  public partial class CMXPictureButton : PictureBox
  {
      public CMXPictureButton()
      {

          this.BackColor = Color.Transparent;

      }

      protected override void OnPaint(PaintEventArgs e)

      {

          if (this.Image != null)

          { 

              Bitmap bmp = new Bitmap(this.Image);

              ImageAttributes attr = new ImageAttributes();

              attr.SetColorKey(bmp.GetPixel(0, 0), bmp.GetPixel(0, 0));

              Rectangle dstRect = new Rectangle(0, 0, bmp.Width, bmp.Height);

              e.Graphics.DrawImage(bmp, dstRect, 0, 0, bmp.Width, bmp.Height,

              GraphicsUnit.Pixel, attr);

          }

          else

              base.OnPaint(e);

      }


    
  }*/
}
