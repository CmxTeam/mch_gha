﻿namespace CargoMatrix.Viewer
{
    partial class SummaryPageMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.message = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelSender = new System.Windows.Forms.Label();
            this.labelReceiver = new System.Windows.Forms.Label();
            this.labelPieces = new System.Windows.Forms.Label();
            this.labelWeight = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // message
            // 
            this.message.BackColor = System.Drawing.Color.White;
            this.message.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.message.Location = new System.Drawing.Point(3, 10);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(63, 14);
            this.message.Text = "Sender:";
            this.message.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.Text = "Receiver:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.Text = "Weight:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(3, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.Text = "Pieces:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelSender
            // 
            this.labelSender.BackColor = System.Drawing.Color.White;
            this.labelSender.Location = new System.Drawing.Point(68, 10);
            this.labelSender.Name = "labelSender";
            this.labelSender.Size = new System.Drawing.Size(169, 14);
            this.labelSender.Text = "<Sender>";
            // 
            // labelReceiver
            // 
            this.labelReceiver.BackColor = System.Drawing.Color.White;
            this.labelReceiver.Location = new System.Drawing.Point(68, 36);
            this.labelReceiver.Name = "labelReceiver";
            this.labelReceiver.Size = new System.Drawing.Size(169, 14);
            this.labelReceiver.Text = "<Receiver>";
            // 
            // labelPieces
            // 
            this.labelPieces.BackColor = System.Drawing.Color.White;
            this.labelPieces.Location = new System.Drawing.Point(68, 63);
            this.labelPieces.Name = "labelPieces";
            this.labelPieces.Size = new System.Drawing.Size(169, 14);
            this.labelPieces.Text = "<Pcs>";
            // 
            // labelWeight
            // 
            this.labelWeight.BackColor = System.Drawing.Color.White;
            this.labelWeight.Location = new System.Drawing.Point(68, 90);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(169, 14);
            this.labelWeight.Text = "<Wgt>";
            // 
            // SummaryPageMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.labelPieces);
            this.Controls.Add(this.labelReceiver);
            this.Controls.Add(this.labelSender);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.message);
            this.Name = "SummaryPageMain";
            this.Size = new System.Drawing.Size(240, 108);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label labelSender;
        public System.Windows.Forms.Label labelReceiver;
        public System.Windows.Forms.Label labelPieces;
        public System.Windows.Forms.Label labelWeight;
    }
}
