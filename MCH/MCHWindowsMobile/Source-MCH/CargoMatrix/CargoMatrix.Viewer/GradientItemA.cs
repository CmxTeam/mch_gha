﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Viewer
{
    public partial class GradientItemA : UserControl
    {
        public string m_description;
        OpenNETCF.Drawing.FontEx descriptionFont;
        Font descriptionFont2;
        Brush descBrush;
        //Rectangle descRect;
        StringFormat descFormat;
        public GradientItemA(string description)
        {
            InitializeComponent();
            m_description = description;
            descriptionFont = new OpenNETCF.Drawing.FontEx("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            
            descriptionFont2 = new Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            OpenNETCF.Drawing.GraphicsEx g = OpenNETCF.Drawing.GraphicsEx.FromControl(this);
            int diff = Height - pictureBox1.Height;
            int height = 0;
            if(m_description != null)
                height = (int)g.MeasureString(m_description, descriptionFont, pictureBox1.Width).Height;
            this.Height = height + diff;
            g.Dispose();
            descBrush = new SolidBrush(Color.White);
            descFormat = new StringFormat();
    
            //Graphics g = CreateGraphics();
            //OpenNETCF.Drawing.BitmapEx bitmap = new OpenNETCF.Drawing.BitmapEx(pictureBox1.Width, pictureBox1.Height);
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            Graphics g2 = Graphics.FromImage(bitmap);

            g2.Clear(Color.White);
            g2.DrawRectangle(new Pen(Color.Red), pictureBox1.ClientRectangle);
            g2.DrawString(m_description, descriptionFont2, new SolidBrush(Color.Blue), pictureBox1.ClientRectangle);

            g2.Dispose();
            
            pictureBox1.Image = bitmap;
            
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawString(m_description, descriptionFont2, descBrush, pictureBox1.ClientRectangle);
            //OpenNETCF.Drawing.GraphicsEx gx = OpenNETCF.Drawing.GraphicsEx.FromControl(pictureBox1);//.FromGraphics(e.Graphics);
            //gx.DrawString(m_description, descriptionFont, Color.White, pictureBox1.ClientRectangle);
            //gx.Dispose();
            
            
            
            
        }
    }
}
