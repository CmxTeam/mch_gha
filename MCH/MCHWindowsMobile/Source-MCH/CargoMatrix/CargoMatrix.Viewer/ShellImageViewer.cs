﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CargoMatrix.Viewer
{
    public partial class ShellImageViewer : UserControl
    {
        
        string m_reference = null;
         Thread m_thread;
         Color thumbnailBackColor;
         FullscreenForm fullscreen;
        
        //List <CargoMatrix.Communication.Data.ImageObject> imageList;
        public ShellImageViewer(string reference, int viewerSize )
        {
            if (viewerSize == 0)
            {
                InitializeComponent();
            }
            else
            {
                InitializeComponent(viewerSize);                
            }
            using (Bitmap bmp = new Bitmap(Resources.Graphics.Skin.thumbnailBackColor))
            {
                thumbnailBackColor = bmp.GetPixel(0, 0);
            }
            this.FilmBackColor = thumbnailBackColor;
            m_reference = reference;
            ImageList = CargoMatrix.Communication.WebServiceManager.Instance().GetShellViewerImages(m_reference);
            horizontalSmoothListbox1.Reset();
            if (horizontalSmoothListbox1.ItemsCount > 0)
            {
                (horizontalSmoothListbox1.Items[0] as SmoothListbox.IExtendedListItem).SelectedChanged(true);
                horizontalSmoothListbox1_ListItemClicked(null, horizontalSmoothListbox1.Items[0], true);
            }
            else
            {
                labelNoRecords.Visible = true;
            }
            
            //ImageList = imageList;
            
        }

        private void InitializeComponent(int viewerSize)
        {
            this.cameraTimer = new System.Windows.Forms.Timer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.horizontalSmoothListbox1 = new SmoothListbox.HorizontalSmoothListbox();
            this.labelNoRecords = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cameraTimer
            // 
            this.cameraTimer.Interval = 1000;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(49, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(142, 102);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // horizontalSmoothListbox1
            // 
            this.horizontalSmoothListbox1.BackColor = System.Drawing.Color.White;
            this.horizontalSmoothListbox1.Location = new System.Drawing.Point(0, 104);
            this.horizontalSmoothListbox1.Name = "horizontalSmoothListbox1";
            this.horizontalSmoothListbox1.Size = new System.Drawing.Size(240, 53);
            this.horizontalSmoothListbox1.TabIndex = 19;
            this.horizontalSmoothListbox1.UnselectEnabled = false;
            this.horizontalSmoothListbox1.ListItemClicked += new SmoothListbox.ListItemClickedHandler(this.horizontalSmoothListbox1_ListItemClicked);
            // 
            // labelNoRecords
            // 
            this.labelNoRecords.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelNoRecords.ForeColor = System.Drawing.Color.White;
            this.labelNoRecords.Location = new System.Drawing.Point(0, 55);
            this.labelNoRecords.Name = "labelNoRecords";
            this.labelNoRecords.Size = new System.Drawing.Size(240, 24);
            this.labelNoRecords.Text = "No Records Found";
            this.labelNoRecords.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelNoRecords.Visible = false;
            // 
            // ShellImageViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.labelNoRecords);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.horizontalSmoothListbox1);
            this.Name = "ShellImageViewer";
            this.Size = new System.Drawing.Size(240, 158);
            this.ResumeLayout(false);

        }
        
        //the Thumbnail that is asved and bring added is of type CustomListItems.Thumbnail 
        private CustomListItems.Thumbnail selectedThumbnail;
        
        private void UpdateImage()
        {
            this.Invoke(new EventHandler(WorkerUpdateImage));            
        }

        public void WorkerUpdateImage(object sender, EventArgs e)
        {
            try
            {
                CargoMatrix.Communication.DTO.ImageObject imgObj = CargoMatrix.Communication.WebServiceManager.Instance().GetFullImage(m_reference, selectedThumbnail.ID);
                if (imgObj.m_rawImage != null)
                {
                    using (System.IO.MemoryStream stream = new System.IO.MemoryStream(imgObj.m_rawImage))
                    {
                        if (pictureBox1.Image != null)
                        {
                            //cmxCameraControl1.image.Dispose();
                            pictureBox1.Image = null; // this will dispose the image too
                        }
                        pictureBox1.Image = new Bitmap(stream);
                    }
                }                
            }
            catch (Exception ex)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10016)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);                
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30113); 
            }
        }

        private void horizontalSmoothListbox1_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (isSelected)
            {                
                selectedThumbnail = listItem as CustomListItems.Thumbnail;
                if (selectedThumbnail != null)
                {
                    pictureBox1.Image = selectedThumbnail.image;
                    pictureBox1.Update();
                    if (selectedThumbnail.ID > 0)
                    {
                        ThreadStart starter = new ThreadStart(UpdateImage);
                        m_thread = new Thread(starter);
                        m_thread.Priority = ThreadPriority.Normal;//.BelowNormal;
                        //m_thread.IsBackground = true;

                        m_thread.Start();
                    }
                }
            }           
        }

        
        public bool LeftButtonPressed
        { 
            set
            {
                horizontalSmoothListbox1.leftButtonPressed = value;
            }
        }

        public bool RightButtonPressed
        {
            set
            {
                horizontalSmoothListbox1.rightButtonPressed = value;
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
        }
        
        public Color FilmBackColor
        {            
            set
            {
                horizontalSmoothListbox1.BackColor = value;
                //base.BackColor = value;
            }
        }

        public List<CargoMatrix.Communication.DTO.ImageObject> ImageList
        {         
            set
            {
                horizontalSmoothListbox1.RemoveAll();
                if (value != null)
                {
                    for (int i = 0; i < value.Count; i++)
                    {
                        horizontalSmoothListbox1.AddItem(new CustomListItems.Thumbnail(value[i]));
                    }
                    horizontalSmoothListbox1.MoveListToEnd();                    
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (fullscreen == null)
            {
                fullscreen = new FullscreenForm();
            }

            if (pictureBox1.Image != null)
            {// set up the Image only if it's not NULL
                fullscreen.Image = pictureBox1.Image;
                fullscreen.SetText(selectedThumbnail.Reason + "\r\nUser: " + selectedThumbnail.userID + "\r\nDate Taken: " +
                    selectedThumbnail.Time + "\r\nLocation: " + selectedThumbnail.Location);
                fullscreen.ShowDialog();
            }
        }              
    }
}
