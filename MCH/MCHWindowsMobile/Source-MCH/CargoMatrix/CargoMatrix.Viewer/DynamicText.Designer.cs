﻿namespace CargoMatrix.Viewer
{
    partial class DynamicText
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(string heading, string []items)
        {
            this.labelMessage = new System.Windows.Forms.Label();
            this.labelHeading = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelMessage
            // 
            this.labelMessage.BackColor = System.Drawing.Color.White;
            this.labelMessage.Location = new System.Drawing.Point(68, 10);
            this.labelMessage.Name = "labelMessage";
            int height = 15;
            if (items != null)
            {
                height = items.Length * 15;
                //this.Height = height + 15;
                //labelMessage.Height = ;
                this.labelMessage.Size = new System.Drawing.Size(169, height);
                labelMessage.Text = "";
                for (int i = 0; i < items.Length; i++)
                {
                    labelMessage.Text += (items[i] + "\r\n");
                }
                
                //this.labelMessage.Text = "Desc\r\nAnd\r\nWhat else\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6";
            }

            
            // 
            // labelHeading
            // 
            this.labelHeading.BackColor = System.Drawing.Color.White;
            this.labelHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelHeading.Location = new System.Drawing.Point(2, 10);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(64, 15);
            this.labelHeading.Text = heading;
            this.labelHeading.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // DynamicText
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.labelHeading);
            this.Name = "DynamicText";
            this.Size = new System.Drawing.Size(240, height+10);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Label labelHeading;
    }
}
