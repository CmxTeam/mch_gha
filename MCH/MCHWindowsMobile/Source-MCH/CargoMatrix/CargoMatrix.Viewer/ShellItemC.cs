﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Viewer
{
    public partial class ShellItemC : UserControl
    {
        public ShellItemC(string heading, string desc)
        {
            InitializeComponent();
            labelHeading.Text = heading;
            labelDescription.Text = desc;
            OpenNETCF.Drawing.FontEx descriptionFont = new OpenNETCF.Drawing.FontEx(labelDescription.Font.Name, labelDescription.Font.Size, labelDescription.Font.Style);
            OpenNETCF.Drawing.FontEx headingFont = new OpenNETCF.Drawing.FontEx(labelHeading.Font.Name, labelHeading.Font.Size, labelHeading.Font.Style);
            OpenNETCF.Drawing.GraphicsEx g = OpenNETCF.Drawing.GraphicsEx.FromControl(this);
            int headingHeight = 0;
            if (heading != null)
                headingHeight = (int)g.MeasureString(heading, headingFont, labelHeading.Width).Height;
            int descHeight = 0;
            if (desc != null)
                descHeight = (int)g.MeasureString(desc, descriptionFont, labelDescription.Width).Height;

            int height = headingHeight + descHeight;

            int diff = Height - (labelDescription.Height + labelHeading.Height);
            
            labelHeading.Height = headingHeight;
            labelDescription.Height = descHeight;
            //labelHeading.Top = 1;
            //labelHeading.Top = labelHeading.Top + headingHeight;

           
            this.Height = height + diff;
            g.Dispose();
        }
    }
}
