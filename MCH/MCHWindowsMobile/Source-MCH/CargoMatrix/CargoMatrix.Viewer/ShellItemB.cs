﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Viewer
{
    public partial class ShellItemB : UserControl
    {
        public ShellItemB(string heading, string desc)
        {
            InitializeComponent();
            //Graphics g = CreateGraphics();
            labelHeading.Text = heading;
            labelDescription.Text = desc;
            OpenNETCF.Drawing.FontEx font = new OpenNETCF.Drawing.FontEx(labelDescription.Font.Name, labelDescription.Font.Size, labelDescription.Font.Style);
            OpenNETCF.Drawing.GraphicsEx g = OpenNETCF.Drawing.GraphicsEx.FromControl(this);
            int height1 = 0;
            if(desc != null)
                height1 = (int)g.MeasureString(desc, font, labelDescription.Width).Height;
            font.Dispose();
            font = new OpenNETCF.Drawing.FontEx(labelHeading.Font.Name, labelHeading.Font.Size, labelHeading.Font.Style);

            int height2 =0;
            if (heading != null)
                height2 = (int)g.MeasureString(heading, font, labelHeading.Width).Height;
            int height = 0;
            if (height1 >= height2)
                height = height1;
            else
                height = height2;
                
            int diff = Height - labelDescription.Height;
            this.Height = height + diff;
            font.Dispose();
            g.Dispose();
        }
    }
}
