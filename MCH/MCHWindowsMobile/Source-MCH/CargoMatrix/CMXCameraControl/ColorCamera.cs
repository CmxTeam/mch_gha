﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace CMXCameraControl
{
    //public enum RESOLUTION
    //{
    //    _176x144 = 0,
    //    _352x288,
    //    _320x240,
    //    _640x480,
    //    _800x600,
    //    _1280x960,
    //    _1280x1024,
    //    _1600x1200
    //}
    public partial class ColorCamera : UserControl
    {
        
        int m_resolution = 4;  // 800x600
        string ImageStoreLocation = "\\cmximg.jpg";
        private bool  m_flash = true;
        bool initialized = false;
        public ColorCamera(int resolution)
        {
            try
            {
                m_resolution = resolution;
                InitializeComponent();
                InitializeGraph(this.Handle, m_resolution);
                EnableFlash(false);
                initialized = true;
            }
               
            catch(MissingMethodException e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 90001);
                
              
            }

        }

        public ColorCamera()
        {
            try
            {
                //m_resolution = resolution;
                InitializeComponent();
                InitializeGraph(this.Handle, m_resolution);
                EnableFlash(false);
                initialized = true;
            }

            catch (MissingMethodException e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 90002);
            }
           
        }

        public string TakePicture()
        {
            try
            {
                System.IO.File.Delete(ImageStoreLocation);
            }
            catch(FileNotFoundException ex)
            {
                // ignore it!
            }

            if(m_flash)
                EnableFlash(true);

            try
            {
                CaptureStill(ImageStoreLocation);
            }
            catch (MissingMethodException e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 90004);
            }
            
            if(m_flash)
                EnableFlash(false);

            ////System.IO.Stream stream = null;
            //byte[] imageFile = null;
            //try
            //{
            //    System.IO.Stream stream = new System.IO.FileStream(ImageStoreLocation, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            //    imageFile = new byte[stream.Length];
            //    stream.Read(imageFile, 0, imageFile.Length);
            //    stream.Close();

            //}
            //catch
            //{ 
            //}
            return ImageStoreLocation;

        }
        public int Resolution
        {
            set
            {
                m_resolution = value;
                //SetResolution(m_resolution);
            }
            get
            {
                return m_resolution;
            }
        }
        public bool Flash
        {
            get
            {
                return m_flash;
            }
            set
            {
                m_flash = value;
            }
        }
        public System.IO.MemoryStream GetImageMemoryStream()
        {
            FileStream fs = new FileStream(ImageStoreLocation,FileMode.Open,FileAccess.Read);
            int length = Convert.ToInt32(fs.Length);
            MemoryStream ms = new MemoryStream(length);
            BinaryReader br = new BinaryReader(fs);
            byte[] byteBuffer = br.ReadBytes(length);
            ms.Write(byteBuffer, 0, length);
            ms.Position = 0;

            br.Close();
            fs.Close();


            
            return ms;
        
        }
        public void CloseCamera()
        {
            try
            {
                ShutDown();
            }
            catch (MissingMethodException e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 90005);
            }
        }
        [DllImport("CMXCameraCaptureDLL.DLL")]
        private static extern bool CaptureStill(string Path);

        [DllImport("CMXCameraCaptureDLL.DLL")]
        private static extern int InitializeGraph(IntPtr hWnd, int resolution);

        [DllImport("CMXCameraCaptureDLL.DLL")]
        private static extern bool EnableFlash(bool enabled);

        [DllImport("CMXCameraCaptureDLL.DLL")]
        private static extern void ShutDown();

        [DllImport("CMXCameraCaptureDLL.DLL")]
        private static extern void PreviewResize();

        //[DllImport("CMXCameraCaptureDLL.DLL")]
        //private static extern bool SetResolution(int resolution);

        private void ColorCamera_Resize(object sender, EventArgs e)
        {
            if (initialized)
                PreviewResize();
        }

        private void ColorCamera_EnabledChanged(object sender, EventArgs e)
        {

        }

        private void ColorCamera_Paint(object sender, PaintEventArgs e)
        {

        }
      

    }
    


}