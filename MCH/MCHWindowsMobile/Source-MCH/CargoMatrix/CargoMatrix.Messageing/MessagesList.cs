﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Messageing;
using SmoothListbox.ListItems;
using System.Drawing;
using System.Windows.Forms;

namespace CargoMatrix.Messageing
{
    public class MessagesList : CargoMatrix.Utilities.MessageListBox
    {
        internal List<IMessage> messages;
        public MessagesList()
        {
            this.HeaderText = "Messages";
            this.HeaderText2 = string.Empty;
            this.ButtonVisible = true;
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(MessagesList_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(MessagesList_ListItemClicked);
            this.ButtonClick += new EventHandler(MessagesList_ButtonClick);
        }

        void MessagesList_ButtonClick(object sender, EventArgs e)
        {
            NewMessagePopup newMessage = new NewMessagePopup();
            if (DialogResult.OK == newMessage.ShowDialog())
                MessagesList_LoadListEvent(null);

        }

        void MessagesList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        {
            var msg = (listItem as TwoLineListItem<IMessage>).ItemData;
            //(listItem as TwoLineListItem<IMessage>).ItemPicture = global::Resources.Graphics.Icons.Email;
            int index = messages.IndexOf(msg);

            ViewMessagePopup tctrl = new ViewMessagePopup(messages, index);
            tctrl.ShowDialog();
            MessagesList_LoadListEvent(null);
        }

        void MessagesList_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            smoothListBoxBase1.RemoveAll();
            this.messages = Messageing.MessageManager.Instance.GetRecentMessages().ToList<IMessage>();
            foreach (var msg in messages)
            {
                string title = msg.Body.Length > 12 ? msg.Body.Substring(0, 15) + "..." : msg.Body;
                string line2 = msg.SenderName + "\t\t" + msg.DateCreated.ToString("MM/dd HH:mm");
                Image logo = msg.IsUnread ? Resources.Icons.Envelope : global::Resources.Graphics.Icons.Email;

                this.smoothListBoxBase1.AddItem2(new TwoLineListItem<IMessage>(title, logo, line2, msg));
            }
            this.smoothListBoxBase1.LayoutItems();
            this.smoothListBoxBase1.RefreshScroll();
        }
    }
}
