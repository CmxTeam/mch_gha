﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using Microsoft.WindowsMobile.Configuration;
using System.Xml;

namespace CargoMatrix
{
    public partial class CMXShell : Form
    {
        bool m_firstTime = false;
        
        
        public CMXShell()
        {
            Cursor.Current = Cursors.WaitCursor;
            InitializeComponent();
            

            
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            
            
            //Cursor.Current = Cursors.Default;
        }

        private static string GetCurrentDirectory()
		{
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
		}

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            if (m_firstTime == false)
            {
                //Cursor.Current = Cursors.WaitCursor;
                LoadTimer.Enabled = true;
                
                m_firstTime = true;
                
            }
        }

        private void InitApp()
        {
            bool result = false;
            //progressBar21.Visible = false;
            cmxProgressBar1.Visible = false;
            //Application.DoEvents();
           

            labelMessage.Text = "Checking for avaiable updates...";
            int installedVersion;
            System.IO.FileStream fs = null;

            string versionFilePath = GetCurrentDirectory() + @"\Version.txt";

           
            labelFooter.Text = "© " + DateTime.Now.Year + " CargoMatrix Inc. All Rights Reserved";
            labelFooter.Refresh();
            CargoMatrixScanner.CMXConfig config;
            XmlSerializer s = new XmlSerializer(typeof(CargoMatrixScanner.CMXConfig));

            TextReader r = new StreamReader(CargoMatrix.Program.ConfigFile);
            config = (CargoMatrixScanner.CMXConfig)s.Deserialize(r);
            r.Close();
            r.Dispose();

            try
            {
                fs = System.IO.File.Open(versionFilePath, System.IO.FileMode.OpenOrCreate);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error opening version file");

            }

            if (fs != null)
            {
                
                System.IO.StreamReader sr = new System.IO.StreamReader(fs);
                installedVersion = Convert.ToInt32(sr.ReadLine());
                
                fs.Close();
                fs.Dispose();
                sr.Close();
                sr.Dispose();

                if (installedVersion <= 0)
                {
                    labelMessage.Text = "Configuring Network...";
                    labelMessage.Refresh();

                    XmlDocument configDoc = new XmlDocument();
                    configDoc.LoadXml(
                        "<wap-provisioningdoc>" +
                        "<characteristic type=\"CM_Networks\">" +
                        "<nocharacteristic type=\"My ISP\"/>" +

                        "</characteristic>" +
                        "</wap-provisioningdoc>"
                        );
                    ConfigurationManager.ProcessConfiguration(configDoc, false);

                }

                labelMessage.Text = "Checking for Updates...";
                labelMessage.Refresh();
                
                UpdaterWS.CMXScannerUpdateWS updater = new CargoMatrix.UpdaterWS.CMXScannerUpdateWS();
                updater.Url = config.WSURL + "CMXScannerUpdateWS.asmx";
                DialogResult dlgRslt;
                CargoMatrix.UpdaterWS.UpdateInfo updateInfo = null;

                do
                {
                    dlgRslt = DialogResult.OK;
                    
                    try
                    {
                        updateInfo = updater.GetUpdateInfo(installedVersion, true, config.gateway);
                    }
                    catch (Exception ex)
                    {
                        dlgRslt = MessageBox.Show("Unable to communicate to the server for updates.", "Error!", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        Refresh();
                        if (dlgRslt == DialogResult.Abort)
                        {
                            this.Close();
                        }
                    }
                } while (dlgRslt == DialogResult.Retry);
                if (updateInfo != null)
                {
                    if (updateInfo.IsAvailable)
                    {
                        labelMessage.Text = "Installing Updates...";
                        labelMessage.Refresh();
                        if (updateInfo.filesList != null)
                        {
                            cmxProgressBar1.Minimum = -1;
                            //progressBar21.Maximum = 100;
                            cmxProgressBar1.Maximum = updateInfo.filesList.Length;

                            cmxProgressBar1.Value = 0;
                            cmxProgressBar1.Visible = true;
                            //progressBar21.Refresh();
                            cmxProgressBar1.Refresh();

                            for (int i = 0; i < updateInfo.filesList.Length; i++)
                            {
                                //labelMessage.Text = "Installing: " + updateInfo.filesList[i].filePath;
                                labelMessage.Refresh();
                                byte[] filebytes = null;
                                
                                do
                                {
                                    dlgRslt = DialogResult.OK;
                                    
                                    try
                                    {
                                        filebytes = updater.GetFile(updateInfo.filesList[i].fileID);
                                    }
                                    catch (Exception ex)
                                    {
                                       dlgRslt = MessageBox.Show("Unable to download " + updateInfo.filesList[i].filePath, "Error!", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                                       Refresh();
                                       if (dlgRslt == DialogResult.Abort)
                                           return;
                                    }
                                    
                                } while (dlgRslt == DialogResult.Retry);
                                try
                                {
                                    fs = System.IO.File.Open(GetCurrentDirectory() + updateInfo.filesList[i].filePath, System.IO.FileMode.OpenOrCreate);
                                }
                                catch (DirectoryNotFoundException dirEx)
                                {
                                    string dir = updateInfo.filesList[i].filePath.Substring(0, updateInfo.filesList[i].filePath.LastIndexOf('\\'));//.TrimEnd(@"\".ToCharArray());
                                    Directory.CreateDirectory(GetCurrentDirectory() + dir);

                                    fs = System.IO.File.Open(GetCurrentDirectory() + updateInfo.filesList[i].filePath, System.IO.FileMode.OpenOrCreate);



                                }
                                //catch (IOException)
                                //{
                                //    if (updateInfo.filesList[i].filePath.IndexOf("opennetcf") >= 0)  // this seems like an opennetcf dll, shell is sharing this file
                                //    {
                                //        continue;
                                //    }
                                //    //int x = updateInfo.filesList[i].filePath.CompareTo("opennetcf");
                                   
                                //}

                                if (filebytes != null)
                                    fs.Write(filebytes, 0, filebytes.Length);
                                fs.Close();
                                cmxProgressBar1.Value = i;
                                cmxProgressBar1.Refresh();
                                //progressBar21.Value = (i * 100) / updateInfo.filesList.Length; ;
                                //progressBar21.Refresh();
                            }
                        }

                        //progressBar21.Value = 100;
                        cmxProgressBar1.Value = cmxProgressBar1.Maximum;
                        cmxProgressBar1.Refresh();
                        //progressBar21.Refresh();

                        fs = System.IO.File.Open(versionFilePath, System.IO.FileMode.OpenOrCreate);
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
                        sw.WriteLine(updateInfo.buidID);
                        sw.Close();
                        fs.Close();
                        
                    }
                }

            }

        
        }

        private void LoadTimer_Tick(object sender, EventArgs e)
        {
            LoadTimer.Enabled = false;
            InitApp();
            labelMessage.Text = "Loading Mobile Cargo Handler...";
            labelMessage.Refresh();
            //string filename = GetCurrentDirectory() + "\\MobileCargoHandler.exe";
            //System.Diagnostics.Process process = System.Diagnostics.Process.Start(filename, null) ;

            //process.WaitForExit();
            
              Assembly SampleAssembly;
              SampleAssembly = Assembly.LoadFrom("MobileCargoHandler.dll");
                                // Obtain a reference to a method known to exist in assembly.
                                Type myType;

                                myType = SampleAssembly.GetType("CargoMatrix.RunDLL");
                                if (myType != null)
                                {
                                    object m_activeApp;
                                    m_activeApp = SampleAssembly.CreateInstance(myType.FullName);
                                    //Application.Run((Form) m_activeApp);
                                    //(m_activeApp as Form).ShowDialog();
                                    

                                    //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                                }

            this.Close();
        }
    }
}