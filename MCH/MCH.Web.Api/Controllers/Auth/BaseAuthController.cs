﻿using CMX.Framework.Utils.Security;
using CMX.Shell.Security;
using CMX.Shell.ServiceProxy;
using CMX.Shell.ServiceProxy.MobileAuthServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Web.Http;

namespace MCH.Web.Api.Controllers.Auth
{
    public class BaseAuthController : ApiController
    {

        protected Transactional<K> DoBusinessAction<K>(Func<K> argAction) where K : class
        {
            Transactional<K> tmpResult = new Transactional<K>();
            try
            {
                K tmpData = argAction();
                tmpResult.Data = tmpData;
                tmpResult.Transaction.Status = true;
            }
            catch (TimeoutException)
            {
                tmpResult.Transaction.Status = false;
                tmpResult.Transaction.Error = "Inner Timeout Exception.";
            }
            catch (CommunicationException)
            {
                tmpResult.Transaction.Status = false;
                tmpResult.Transaction.Error = "Communication Exception.";
            }
            catch (Exception)
            {
                tmpResult.Transaction.Status = false;
                tmpResult.Transaction.Error = "Internal Server Error.";
            }

            return tmpResult;
        }

    }
}
