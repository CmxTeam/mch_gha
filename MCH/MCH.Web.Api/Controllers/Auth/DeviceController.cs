﻿
using CMX.Framework.Utils.Security;
using CMX.Shell.Security;
using CMX.Shell.ServiceProxy;
using CMX.Shell.ServiceProxy.MobileAuthServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Api.Controllers.Auth
{
    public class DeviceController : BaseAuthController
    {

        [HttpGet]
        [Obsolete("moved to shell")]
        public Transactional<DeviceInfoDto> GetDeviceInfo(string deviceId)
        {
            Transactional<DeviceInfoDto> tmpResult = new Transactional<DeviceInfoDto>();
            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.GetDeviceInfo(deviceId);
                    if (tmpData != null)
                    {
                        if (tmpData.Data != null)
                        {
                            tmpResult.Data = new DeviceInfoDto { CompanyId = tmpData.Data.CompanyId, CompanyName = tmpData.Data.CompanyName, Id = tmpData.Data.Id, Name = tmpData.Data.Name };
                        }

                        if (tmpData.Transaction != null)
                        {
                            tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                            {
                                Error = tmpData.Transaction.Error,
                                Status = tmpData.Transaction.Status
                            };
                        }
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }

        [HttpPost]
        [Obsolete("moved to shell")]
        public Transactional<DeviceDto> SaveDevice(DeviceDto model)
        {
            Transactional<DeviceDto> tmpResult = new Transactional<DeviceDto>();

            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.SaveDevice(new CMX.Shell.ServiceProxy.MobileAuthServiceReference.DeviceDto
                    {
                        CompanyId = model.CompanyId,
                        Description = model.Description,
                        DeviceName = model.DeviceName,
                        Id = model.Id,
                        MAC = model.MAC
                    });
                    if (tmpData != null && tmpData.Data != null)
                    {

                        tmpResult.Data = new DeviceDto { CompanyId = tmpData.Data.CompanyId, Description = tmpData.Data.Description, Id = tmpData.Data.Id, DeviceName = tmpData.Data.DeviceName, MAC = tmpData.Data.MAC };
                    }

                    if (tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }
    }
}
