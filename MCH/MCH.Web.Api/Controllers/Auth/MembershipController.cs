﻿using CMX.Framework.Utils.Security;
using CMX.Shell.ServiceProxy;
using CMX.Shell.ServiceProxy.MobileAuthServiceReference;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Api.Controllers.Auth
{
    public class MembershipController : BaseAuthController
    {

        [HttpGet]
        [Obsolete("Moved to shell")]
        public Transactional<CompanySecurityConfigDTO> GetCompanySecuritySettings(string companyName)
        {
            Transactional<CompanySecurityConfigDTO> tmpResult = new Transactional<CompanySecurityConfigDTO>();
            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.GetCompanySecuritySettings(companyName);

                    tmpResult.Data = tmpData.Data;

                    if (tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }

        [HttpGet]
        [Obsolete("Moved to shell")]
        public Transactional<CompanySecurityConfigDTO> GetCompanySecurityById(int companyId)
        {
            Transactional<CompanySecurityConfigDTO> tmpResult = new Transactional<CompanySecurityConfigDTO>();
            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.GetCompanySecurityById(companyId);

                    tmpResult.Data = tmpData.Data;

                    if (tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }

        [HttpPost]
        public Transactional<UserMobileInfo> AuthenticateAppUser(AuthUserDto model)
        {
            Transactional<UserMobileInfo> tmpResult = new Transactional<UserMobileInfo>();
            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.AuthenticateAppUser(model);

                    tmpResult.Data = tmpData.Data;

                    if (tmpData.Data != null)
                    {
                        //update with app local ids
                        using (UserUnitOfWork unit = new UserUnitOfWork())
                        {
                            var tmpLocalUser = unit.GetUserByShellId(tmpData.Data.UserId);
                            if (tmpLocalUser != null)
                            {
                                tmpResult.Data.AppUserId = tmpLocalUser.Id;
                                if (tmpLocalUser.UserWarehouses.Any())
                                {
                                    var defaultWarehouseId = tmpLocalUser.UserWarehouses.FirstOrDefault(e => e.IsDefault);
                                    if (defaultWarehouseId != null && tmpResult.Data.ShellSetting != null)
                                    {
                                        tmpResult.Data.ShellSetting.DefaultWarehouseId = defaultWarehouseId.WarehouseId;
                                        tmpResult.Data.ShellSetting.WarehouseIds = String.Join(",", tmpLocalUser.UserWarehouses.Select(e => e.WarehouseId).ToList());
                                    }
                                }
                            }
                            else
                            {
                                //TODO : add transaction error that users local cache does not exist
                            }
                        }
                    }


                    if (tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }

        [HttpPost]
        public Transactional<UserMobileInfo> AuthenticateUserPin(AuthAppUserDto model)
        {
            Transactional<UserMobileInfo> tmpResult = new Transactional<UserMobileInfo>();
            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.AuthenticateUserPin(model);

                    tmpResult.Data = tmpData.Data;
                    if (tmpData.Data != null)
                    {
                        using (UserUnitOfWork unit = new UserUnitOfWork())
                        {
                            var tmpLocalUser = unit.GetUserByShellId(tmpData.Data.UserId);
                            if (tmpLocalUser != null)
                            {
                                tmpResult.Data.AppUserId = tmpLocalUser.Id;
                                if (tmpLocalUser.UserWarehouses.Any())
                                {
                                    var defaultWarehouseId = tmpLocalUser.UserWarehouses.FirstOrDefault(e => e.IsDefault);
                                    if (defaultWarehouseId != null && tmpResult.Data.ShellSetting != null)
                                    {
                                        tmpResult.Data.ShellSetting.DefaultWarehouseId = defaultWarehouseId.WarehouseId;
                                        tmpResult.Data.ShellSetting.WarehouseIds = String.Join(",", tmpLocalUser.UserWarehouses.Select(e => e.WarehouseId).ToList());
                                    }
                                }
                            }
                            else
                            {
                                //TODO : Handle this case
                            }
                        }
                    }

                    if (tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }

        [HttpPost]
        public Transactional<UserMobileInfo> GetUserAuthDataByPin(AuthAppUserDto model)
        {
            Transactional<UserMobileInfo> tmpResult = new Transactional<UserMobileInfo>();
            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.GetUserAuthDataByPin(model);

                    tmpResult.Data = tmpData.Data;

                    if (tmpData.Data != null)
                    {
                        using (UserUnitOfWork unit = new UserUnitOfWork())
                        {
                            var tmpLocalUser = unit.GetUserByShellId(tmpData.Data.UserId);
                            if (tmpLocalUser != null)
                            {
                                tmpResult.Data.AppUserId = tmpLocalUser.Id;
                                if (tmpLocalUser.UserWarehouses.Any())
                                {
                                    var defaultWarehouseId = tmpLocalUser.UserWarehouses.FirstOrDefault(e => e.IsDefault);
                                    if (defaultWarehouseId != null && tmpResult.Data.ShellSetting != null)
                                    {
                                        tmpResult.Data.ShellSetting.DefaultWarehouseId = defaultWarehouseId.WarehouseId;
                                        tmpResult.Data.ShellSetting.WarehouseIds = String.Join(",", tmpLocalUser.UserWarehouses.Select(e => e.WarehouseId).ToList());
                                    }

                                }
                            }
                            else
                            {
                                //TODO : Handle this case
                            }
                        }
                    }

                    if (tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }

        [HttpPost]
        public Transactional<SessionStateModel> CheckUserSession(SessionCheckDto model)
        {
            Transactional<SessionStateModel> tmpResult = new Transactional<SessionStateModel>();
            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.CheckUserSession(model);

                    tmpResult.Data = tmpData.Data;

                    if (tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }
    }
}
