﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using CMX.Shell.ServiceProxy.MobileAuthServiceReference;
using CMX.Shell.ServiceProxy;
using CMX.Shell.Security;
using CMX.Framework.Utils.Security;

namespace MCH.Web.Api.Controllers.Auth
{
    public class CommonController : BaseAuthController
    {
        [HttpGet]
        [Obsolete("Moved to shell")]
        public Transactional<List<CompanyModel>> GetCompanies()
        {
            Transactional<List<CompanyModel>> tmpResult = new Transactional<List<CompanyModel>>();
            try
            {
                using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
                {
                    var tmpData = authProxy.Client.GetActiveCompanies();

                    tmpResult.Data = tmpData.Data.Select(e => new CompanyModel { Id = e.Id, Code = e.Code, Name = e.Name }).ToList();
                    if (tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }

                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }


            return tmpResult;
        }

    }
}
