﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CMX.Shell.ServiceProxy.MobileAuthServiceReference;
using CMX.Shell.ServiceProxy;
using CMX.Framework.Utils.Security;
using MCH.Web.Api.Models.Common;
using MCH.BLL.Units;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.Tasks;
using MCH.Core.Models.Membership;

namespace MCH.Web.Api.Controllers.Auth
{
    public class MenuController : BaseAuthController
    {
        [HttpPost]
        public Transactional<IList<TaskCategory>> GetMenuCategories(GetRoleMenuModel model)
        {
            Transactional<IList<TaskCategory>> tmpResult = new Transactional<IList<TaskCategory>>();
            try
            {
                var categoryIDs = GetAppRoleMenus(model).Data.Select(p => p.CategoryId).ToList();

                using (TasksUnitOfWork unit = new TasksUnitOfWork())
                {
                    tmpResult.Data = unit.GetTaskTypeCategories(categoryIDs);
                }
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = true };
            }
            catch (Exception )
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Internal Server Error." };
                throw;
            }
           
            return tmpResult;
        }

        [HttpPost]
        public Transactional<IList<MenuItem>> GetAppRoleMenus(GetRoleMenuModel model)
        {
            Transactional<IList<MenuItem>> tmpResult = new Transactional<IList<MenuItem>>();

            try
            {
                using (TasksUnitOfWork unit = new TasksUnitOfWork())
                {
                  
                    TransactionalOfArrayOfAppNavigation2uZVmtVa tmpData = null;

                    tmpResult.Data = unit.GetTaskTypeMenueItems(model, ref tmpData);

                    if (tmpData != null && tmpData.Transaction != null)
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Error = tmpData.Transaction.Error,
                            Status = tmpData.Transaction.Status
                        };
                    }
                    else
                    {
                        tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus
                        {
                            Status = true
                        };
                    }
                }
            }
            catch (Exception)
            {
                tmpResult.Transaction = new CMX.Framework.Utils.Security.TransactionStatus { Status = false, Error = "Communication Exception." };
            }

            return tmpResult;
        }
    }
}
