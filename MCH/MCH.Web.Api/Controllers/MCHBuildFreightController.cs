﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using CMX.Framework.Utils.Security;
using MCH.BLL.Model.BuildFreight;
using MCH.BLL.Model.Enum;
using MCH.BLL.Units;

namespace MCH.Web.Api.Controllers
{
    public class MCHBuildFreightController : BaseApiController
    {
        [HttpGet]
        public Transactional<List<BuildFreightTaskItem>> GetBuildFreightTasks(long warehouseId, long userId,
                                                                            enumBuildFreightStatusTypes status)
        {
            return this.WrapInTransaction<List<BuildFreightTaskItem>>(() =>
            {
                using (BuildFreightUnit unit = new BuildFreightUnit())
                {
                    return unit.GetBuildFreightTasks(warehouseId, userId, status);
                }
            });
        }

        [HttpGet]
        public Transactional<BuildFreightTaskItem> GetBuildFreightTask(long taskId, long flightManifestId)
        {
            return this.WrapInTransaction<BuildFreightTaskItem>(() =>
            {
                using (BuildFreightUnit unit = new BuildFreightUnit())
                {
                    return unit.GetBuildFreightTask(taskId, flightManifestId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus StageLocationTask(long taskId, long userId, long locationId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    unit.StageLocationTask(taskId, userId, locationId);
                    return new TransactionStatus { Status = true };
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus AddUld(long userId, long flightManifestId, long uldUnitTypeId, string serialNumber,
            string carrierCode, long taskId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.AddUld(userId, flightManifestId, uldUnitTypeId, serialNumber, carrierCode, taskId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus DeleteUld(long userId, long flightManifestId, long uldId, long taskId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.DeleteUld(userId, flightManifestId, uldId, taskId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }


        public TransactionStatus EditUld(long uldId, long userId, long flightManifestId, long uldUnitTypeId,
            string serialNumber, string carrierCode, long taskId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.EditUld(uldId, userId, flightManifestId, uldUnitTypeId, serialNumber, carrierCode, taskId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus SwitchBUPMode(long taskId, long uldId, long userId, long manifestId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.SwitchBUPMode(taskId, uldId, userId, manifestId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public Transactional<List<BuildFreightUldListItem>> GetBuildFreightUldListItems(long userId, long flightManifestId, enumBuildFreightStatusTypes status, long taskId)
        {
            return this.WrapInTransaction<List<BuildFreightUldListItem>>(() =>
            {
                using (BuildFreightUnit unit = new BuildFreightUnit())
                {
                    return unit.GetBuildFreightUldListItems(userId, flightManifestId, status, taskId);
                }
            });
        }

        [HttpGet]
        public Transactional<BuildFreightUldListItem> GetBuildFreightUld(long userId, long flightManifestId, long uldId)
        {
            return this.WrapInTransaction<BuildFreightUldListItem>(() =>
            {
                using (BuildFreightUnit unit = new BuildFreightUnit())
                {
                    return unit.GetBuildFreightUld(userId, flightManifestId, uldId);
                }
            });
        }

        [HttpGet]
        public Transactional<List<BuildFreightShipmentItem>> GetShipments(long userId, long flightManifestId, long taskId, enumBuildFreightStatusTypes status)
        {
            return this.WrapInTransaction<List<BuildFreightShipmentItem>>(() =>
            {
                using (BuildFreightUnit unit = new BuildFreightUnit())
                {
                    return unit.GetShipments(userId, flightManifestId, taskId, status);
                }
            });
        }

        [HttpGet]
        public Transactional<List<BuildFreightShipmentItem>> GetShipmentsByUld(long userId, long flightManifestId, long taskId, long uldId)
        {
            return this.WrapInTransaction<List<BuildFreightShipmentItem>>(() =>
            {
                using (BuildFreightUnit unit = new BuildFreightUnit())
                {
                    return unit.GetShipmentsByUld(userId, flightManifestId, taskId, uldId);
                }
            });
        }

        [HttpGet]
        public Transactional<BuildFreightShipmentItem> ScanShipment(long userId, long taskId, long flightManifestId, string shipmentNumber)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                return unit.ScanShipment(userId, taskId, flightManifestId, shipmentNumber);
            }
        }

        [HttpGet]
        public TransactionStatus UpdateBuildULDWeight(long userId, long taskId, long flightManifestId, long uldId,
            double weight, double tareWeight, string weightUOM)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.UpdateBuildULDWeight(userId, taskId, flightManifestId, uldId, weight, tareWeight, weightUOM);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus StageBuildULD(long uldId, long userId, long flightManifestId, int locationId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.StageBuildULD(uldId, userId, flightManifestId, locationId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public Transactional<BuildUldViewItem> GetBuildULDView(long detailId)
        {
            return this.WrapInTransaction<BuildUldViewItem>(() =>
            {
                using (BuildFreightUnit unit = new BuildFreightUnit())
                {
                    return unit.GetBuildULDView(detailId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus RemovePiecesFromUld(long userId, long uldId, long detailId, int pieces)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.RemovePiecesFromUld(userId, uldId, detailId, pieces);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public Transactional<List<ForkLiftViewItem>> GetForkliftView(long taskId, long userId)
        {
            return this.WrapInTransaction<List<ForkLiftViewItem>>(() =>
            {
                using (BuildFreightUnit unit = new BuildFreightUnit())
                {
                    return unit.GetForkliftView(taskId, userId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus AddForkliftPieces(long awbId, long taskId, long flightManifestId, int pieces,
            long userId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.AddForkliftPieces(awbId, taskId, flightManifestId, pieces, userId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus RemoveItemsFromForklift(long forkliftDetailsId, int count, long locationId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.RemoveItemsFromForklift(forkliftDetailsId, count, locationId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }
        [HttpGet]
        public TransactionStatus DropForkliftPieces(long taskId, long userId, long uldId, long locationId, long detailId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.DropForkliftPieces(taskId, userId, uldId, locationId, detailId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus DropAllForkliftPieces(long taskId, long userId, long uldId, long locationId)
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                try
                {
                    return unit.DropAllForkliftPieces(taskId, userId, uldId, locationId);
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }
    }
}