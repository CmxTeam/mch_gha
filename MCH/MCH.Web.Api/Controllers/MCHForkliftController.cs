﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Model.Forklift;
using MCH.BLL.Units;
using MCH.Web.Api.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace MCH.Web.Api.Controllers
{
    public class MCHForkliftController : BaseApiController
    {

        [HttpGet]
        public Transactional<int> GetForkliftCountGeneric(long userId, long taskTypeId, long taskId)
        {
            return this.WrapInTransaction<int>(() =>
            {
                using (ForkliftUnitOfWork unit = new ForkliftUnitOfWork())
                {
                    return unit.GetForkliftCountGeneric(userId,taskTypeId,taskId);
                }
            });
        }
        [HttpGet]
        public Transactional<ForkliftViewGeneric[]> GetForkliftViewByTaskType(long userId, long taskTypeId, long taskId)
        {
            return this.WrapInTransaction<ForkliftViewGeneric[]>(() =>
            {
                using (ForkliftUnitOfWork unit = new ForkliftUnitOfWork())
                {
                    return unit.GetForkliftViewByTaskType(taskTypeId, taskId, userId);
                }
            });
        }
        [HttpPost]
        public TransactionStatus RemoveAllItemsFromForkliftGeneric(ForkliftModel model)
        {
            using (ForkliftUnitOfWork unit = new ForkliftUnitOfWork())
            {
                try
                {
                    unit.RemoveAllItemsFromForkliftGeneric(model.UserId,model.TaskTypeId,model.TaskId);
                    return new TransactionStatus { Status = true };
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                } 
            }           
        }

    }
}