﻿using CMX.Framework.Utils.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class BaseApiController:ApiController
    {

        protected Transactional<K> WrapInTransaction<K>(Func<K> argAction)
        {
            Transactional<K> tmpResult = new Transactional<K>();
            tmpResult.Transaction = new TransactionStatus();
            try
            {
                K tmpData = argAction();
                tmpResult.Data = tmpData;
                tmpResult.Transaction.Status = true;
            }
            catch (ArgumentNullException ex)
            {
                tmpResult.Transaction.Status = false;
                tmpResult.Transaction.Error = ex.Message;
            }
            catch (Exception ex1)
            {
                tmpResult.Transaction.Status = false;
                tmpResult.Transaction.Error = "Internal Server Error.";
            }

            return tmpResult;
        }
    }
}
