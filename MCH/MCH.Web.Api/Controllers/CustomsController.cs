﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Customs;
using MCH.BLL.Units;
using MCH.Web.Api.Models.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class CustomsController : BaseApiController
    {
        [HttpPost]
        public Transactional<CheckListModel> GetDGCheckList(GetChecklistParam model) 
        {
            return this.WrapInTransaction<CheckListModel>(() => {

                CheckListModel tmpData = null;
                using (CustomsUnit customsUnit = new CustomsUnit())
                {
                    tmpData = customsUnit.GetCheckListModel(model.TaskId, model.UserId, model.AwbId);
                }
                return tmpData;

            });
        }

        [HttpPost]
        public TransactionStatus FinalizeChecklist(ChecklistResultsParam param) {
            TransactionStatus tmpResult = new TransactionStatus{Status = true};

            return tmpResult;
        }

        public TransactionStatus SaveChecklist(ChecklistResultsParam param)
        {
            TransactionStatus tmpResult = new TransactionStatus { Status = true };

            return tmpResult;
        }

        
    }
}
