﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.CargoAcceptance;
using MCH.BLL.Model.Customs;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Api.Controllers
{
    public class MCHAcceptController : BaseApiController
    {
        public Transactional<AcceptFreightTaskItem> GetAcceptFreightTask(long taskId)
        {
            return this.WrapInTransaction<AcceptFreightTaskItem>(() =>
            {
                using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
                {
                    return unit.GetAcceptFreightTask(taskId);
                }
            });
        }
    }
}