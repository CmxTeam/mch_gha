﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CMX.Framework.Utils.Security;
using MCH.BLL.Units;
using MCH.BLL.Model.Common;


namespace MCH.Web.Api.Controllers
{
    public class PrinterController : ApiController
    {
        [HttpGet]
        public TransactionStatus PrintAttachment(long attachmentId, string printerName, int numberOfCopies)
        {
            using (ReportPrintingUnit unit = new ReportPrintingUnit())
            {
                try
                {
                    unit.PrintAttachment(attachmentId, printerName, numberOfCopies);
                    return new TransactionStatus() { Status = true };

                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus PrintReport(string reportPath, string reportName, Dictionary<string, string> pars, string printerName,
            int numberOfCopies)
        {
            using (ReportPrintingUnit unit = new ReportPrintingUnit())
            {
                try
                {
                    unit.PrintReport(reportPath, reportName, pars, printerName, numberOfCopies);
                    return new TransactionStatus() { Status = true };

                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus PrintAWBLabel(long awbId, long userId,long overPackItemId, int count)
        {
            using (LabelPrintingUnit unit = new LabelPrintingUnit())
            {
                try
                {
                    unit.PrintAWBLabel(awbId, userId,overPackItemId, count);
                    return new TransactionStatus() { Status = true };

                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public TransactionStatus PrintHWBLabel(long hwbId, long userId,long overPackItemId, int count)
        {
            using (LabelPrintingUnit unit = new LabelPrintingUnit())
            {
                try
                {
                    unit.PrintHWBLabel(hwbId, userId, overPackItemId,count);
                    return new TransactionStatus() { Status = true };

                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        } 

        [HttpGet]
        public TransactionStatus SetUserDefault(long userId, int printerId)
        {
            using (LabelPrintingUnit unit = new LabelPrintingUnit())
            {
                try
                {
                    unit.SetUserDefault(userId, printerId);
                    return new TransactionStatus { Status = true };
                }
                catch (Exception ex)
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };
                }
            }
        }

        [HttpGet]
        public Transactional<List<PrinterItem>> GetPrinters(long? userId, PrinterTypes? printerType)
        {
            Transactional<List<PrinterItem>> tmpResult = new Transactional<List<PrinterItem>>();
            using (LabelPrintingUnit unit = new LabelPrintingUnit())
            {
                try
                {
                    tmpResult.Data = unit.GetPrinters(userId, printerType);
                    tmpResult.Transaction = new TransactionStatus { Status = true };
                }
                catch (Exception ex)
                {
                    tmpResult.Transaction = new TransactionStatus { Status = false, Error = ex.Message };
                }

                return tmpResult;
            }
        }
    }
}
