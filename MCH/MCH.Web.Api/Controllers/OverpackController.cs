﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Units;
using MCH.Core.Models.Overpack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{


    public class OverpackController : ApiController
    {

        [HttpGet]
        public Transactional<OverPackTaskItem> GetOverpackTask(long? taskId, long userId, string barcode, long warehouseId)
        {
            Transactional<OverPackTaskItem> tmpResult = new Transactional<OverPackTaskItem>();
            using (OverpackUnit unit = new OverpackUnit())
            {
                try
                {
                    tmpResult.Data = unit.GetOverpackTask(taskId, userId, barcode, warehouseId);
                    TransactionStatus tmpStatus = new TransactionStatus { Status = true };
                    tmpResult.Transaction = tmpStatus;
                }
                catch (Exception ex)
                {
                    tmpResult.Transaction = new TransactionStatus { Status = false, Error = ex.Message };
                }

                return tmpResult;
            }
        }

        [HttpGet]
        public Transactional<List<OverPackItem>> GetOverPackItems(long taskId)
        {
            Transactional<List<OverPackItem>> tmpResult = new Transactional<List<OverPackItem>>();
            using (OverpackUnit unit = new OverpackUnit())
            {
                try
                {
                    tmpResult.Data = unit.GetOverpackItems(taskId);
                    TransactionStatus tmpStatus = new TransactionStatus { Status = true };
                    tmpResult.Transaction = tmpStatus;
                }
                catch (Exception ex)
                {
                    tmpResult.Transaction = new TransactionStatus { Status = false, Error = ex.Message };
                }

                return tmpResult;
            }
        }

        [HttpGet]
        public Transactional<List<OverpackTypeItem>> GetOverpackTypes(long? warehouseId)
        {
            Transactional<List<OverpackTypeItem>> tmpResult = new Transactional<List<OverpackTypeItem>>();
            using (OverpackUnit unit = new OverpackUnit())
            {
                try
                {
                    tmpResult.Data = unit.GetOverpackTypes(warehouseId);
                    TransactionStatus tmpStatus = new TransactionStatus { Status = true };
                    tmpResult.Transaction = tmpStatus;
                }
                catch (Exception ex)
                {
                    tmpResult.Transaction = new TransactionStatus { Status = false, Error = ex.Message };
                }

                return tmpResult;
            }
        }

        [HttpGet]
        public TransactionStatus DeleteOverpack(long overpackId, long taskId, long userId)
        {
            TransactionStatus tmpStatus = new TransactionStatus { Status = true };
            using (OverpackUnit unit = new OverpackUnit())
            {
                try
                {
                    unit.DeleteOverpack(overpackId, taskId, userId);

                }
                catch (Exception ex)
                {
                    tmpStatus = new TransactionStatus { Status = false, Error = ex.Message };
                }

            }
            return tmpStatus;
        }

        [HttpGet]
        public TransactionStatus AddOverpack(long ? taskId, long  userId, int ? overpackTypeId, SkidOwnerTypes skidOwnerType, int ? pieces)
        {
            TransactionStatus tmpStatus = new TransactionStatus();
            using (OverpackUnit unit = new OverpackUnit())
            {
                try
                {
                    var tmpResult = unit.InsertOverpack(taskId, userId, overpackTypeId, skidOwnerType, pieces);
                    if (tmpResult.Success)
                    {
                        tmpStatus = new TransactionStatus { Status = true };
                    }
                    else
                    {
                        tmpStatus = new TransactionStatus { Status = false, Error = tmpResult.Message };
                    }

                }
                catch (Exception ex)
                {
                    tmpStatus = new TransactionStatus { Status = false, Error = ex.Message };
                }

            }
            return tmpStatus;
        }

        //public Transactional<long> AddOverpack(long taskId, long userId, long OverpackTypeId, SkidOwnerTypes skidOwnerType, int pieces)
        //{
        //    Transactional<long> tmpResult = new Transactional<long>();
        //    using (OverpackUnit unit = new OverpackUnit())
        //    {
        //        try
        //        {
        //            tmpResult.Data = unit.AddOverpack(taskId, userId, OverpackTypeId, skidOwnerType, pieces);
        //            tmpResult.Transaction = new TransactionStatus { Status = true };
        //        }
        //        catch (Exception ex)
        //        {
        //            tmpResult.Transaction = new TransactionStatus { Status = false, Error = ex.Message };
        //        }

        //    }
        //    return tmpResult;
        //}

        [HttpGet]
        public TransactionStatus EditOverpack(long  taskId, long ? overpackId, long userId, int ? overpackTypeId, SkidOwnerTypes skidOwnerType, int  pieces)
        {
            TransactionStatus tmpStatus = new TransactionStatus();
            using (OverpackUnit unit = new OverpackUnit())
            {
                try
                {
                    var tmpResult = unit.UpdateOverPack(overpackId, overpackTypeId, pieces, userId, skidOwnerType, taskId);
                    if (tmpResult.Success)
                    {
                        tmpStatus = new TransactionStatus { Status = true };
                    }
                    else
                    {
                        tmpStatus = new TransactionStatus { Status = false, Error = tmpResult.Message };
                    }
                   
                }
                catch (Exception ex)
                {
                    tmpStatus = new TransactionStatus { Status = false, Error = ex.Message };
                }

            }
            return tmpStatus;
        }

    }
}
