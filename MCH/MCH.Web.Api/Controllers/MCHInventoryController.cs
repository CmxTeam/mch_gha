﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Inventory;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class MCHInventoryController : BaseApiController
    {

        [HttpGet]
        public TransactionStatus ResetInventory(long awbId, long hwbId, long userId, long taskId)
        {
            try
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    unit.ResetInventory(awbId, hwbId, userId, taskId);
                }
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        [HttpGet]
        public TransactionStatus SetLocationAsEmpty(long userId, int locationId, long taskId)
        {
            try
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    unit.SetLocationAsEmpty(userId, locationId, taskId);
                }
                return new TransactionStatus { Status = true };
            }
            catch(Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }                        
        }

        [HttpGet]
        public Transactional<bool> IsInventoryLocationEmpty(long userId, int locationId, long taskId)
        {
            return this.WrapInTransaction<bool>(() =>
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    return unit.IsInventoryLocationEmpty(userId, locationId, taskId);
                }
            });
        }

        [HttpGet]
        public Transactional<long> GetInventoryTaskId(long userId, long warehouseId)
        {
            return this.WrapInTransaction<long>(() =>
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    return unit.GetInventoryTaskId(userId, warehouseId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus ScanShipment(long userId, long awbId, long hwbId, int locationId, int pieces, long taskId)
        {
            try
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    unit.ScanShipment(userId, awbId, hwbId, locationId, pieces, taskId);
                }
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }


        [HttpGet]
        public TransactionStatus FinalizeInventory(long userId, long taskId)
        {
            try
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    unit.FinalizeInventory(userId, taskId);
                }
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        [HttpGet]
        public Transactional<InventoryShipmentItem>  ValidateShipment(long warehouseId, string barcode, long taskId, long userId)
        {
            return this.WrapInTransaction<InventoryShipmentItem>(() =>
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    return unit.ValidateShipment(warehouseId, barcode, taskId, userId);
                }
            });
        }


        [HttpGet]
        public Transactional<List<InventoryShipmentItem>>  GetLocationsHistory(long warehouseId, string barcode, long taskId, long userId)
        {
            return this.WrapInTransaction<List<InventoryShipmentItem>>(() =>
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    return unit.GetLocationsHistory(warehouseId, barcode, taskId, userId);
                }
            });
        }


        [HttpGet]
        public TransactionStatus EditAwbPiecesCount(long awbId, int oldWarehouseLocationId, long taskId, int pcs, long userId)
        {
            try
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    unit.EditAwbPiecesCount(awbId, oldWarehouseLocationId, taskId, pcs, userId);
                }
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        [HttpGet]
        public TransactionStatus EditHwbPiecesCount(long hwbId, int oldWarehouseLocationId, long taskId, int pcs, long userId)
        {
            try
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    unit.EditHwbPiecesCount(hwbId, oldWarehouseLocationId, taskId, pcs, userId);
                }
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }


        [HttpGet]
        public Transactional<int> GetAwbPiecesCountInLocation(long awbId, long locationId, long taskId)
        {
            return this.WrapInTransaction<int>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAwbPiecesCountInLocation(awbId, locationId, taskId);
                }
            });
        }

        [HttpGet]
        public Transactional<int> GetHwbPiecesCountInLocation(long hwbId, long locationId, long taskId)
        {
            return this.WrapInTransaction<int>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetHwbPiecesCountInLocation(hwbId, locationId, taskId);
                }
            });
        }


        [HttpGet]
        public Transactional<List<InventoryLocationViewItem>>  GetMyInventory(long userId, long taskId)
        {
            return this.WrapInTransaction<List<InventoryLocationViewItem>>(() =>
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    return unit.GetMyInventory(userId, taskId);
                }
            });
        } 

        [HttpGet]
        public Transactional<List<InventoryShipmentItem>>  GetMyInventoryShipments(long userId, long locationId, long taskId)
        {
            return this.WrapInTransaction<List<InventoryShipmentItem>>(() =>
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    return unit.GetMyInventoryShipments(userId, locationId, taskId);
                }
            });
        }

        [HttpGet]
        public Transactional<List<DiscrepancyItem>> GetInventoryDiscrepancies(long taskId)
        {
            return this.WrapInTransaction<List<DiscrepancyItem>>(() =>
            {
                using (InventoryUnit unit = new InventoryUnit())
                {
                    return unit.GetInventoryDiscrepancies(taskId);
                }
            });
        }     
    }
}