﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Api.Controllsers
{
    public class CargoLoaderController : ApiController
    {

        public CargoLoaderFlight[] GetCargoLoaderFlights(string station, int userID, TaskStatuses status, string carrierNo, string destination, string flightNo, ReceiverSortFields sortBy)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlights(station, userID, status, carrierNo, destination, flightNo, sortBy);
            }
        }

        public CargoLoaderFlight GetCargoLoaderFlight(long flightManifestId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlight(flightManifestId, taskId);
            }
        }

        public CargoLoaderFlightLeg[] GetCargoLoaderFlightLegs(long flightManifestId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlightLegs(flightManifestId);
            }
        }

        public AlertModel[] GetFlightAlerts(string warehousePort, long manifestId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetFlightAlerts(warehousePort, manifestId);
            }
        }

        public AlertModel[] GetLegAlerts(long legId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetLegAlerts(legId);
            }
        }

        public CargoLoaderFlightLeg GetCargoLoaderFlightLeg(long legId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlightLeg(legId);
            }
        }

        public TransactionStatus UpdateFlightLegLocation(long legId, long locationId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateFlightLegLocation(legId, locationId, userId);
            }
        }

        public TransactionStatus UpdateCargoLoaderFlightLegUld(long uldLegId, string uldSerialNo, string prefix, long userId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateCargoLoaderFlightLegUld(uldLegId, uldSerialNo, prefix, userId, taskId);
            }
        }

        public CargoLoaderFlightLegUld[] GetCargoLoaderFlightUlds(long flightId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlightUlds(flightId);
            }
        }

        public string[] GetUldPrefix()
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetUldPrefix();
            }
        }

        public CargoLoaderFlightLegUld[] GetFlightDestinationBUPs(long legId, FlightDestinationTypes type, string uldType)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetFlightDestinationBUPs(legId, type, uldType);
            }
        }

        public int GetForkLiftCount(long userId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetForkLiftCount(userId, taskId);
            }
        }

        public UldType[] GetUldTypes()
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetUldTypes();
            }
        }

        public TransactionStatus UpdateUldWeight(long uldId, decimal value)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateUldWeight(uldId, value);
            }
        }

        public TransactionStatus UpdateUldTareWeight(long uldId, decimal value)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateUldTareWeight(uldId, value);
            }
        }

        public TransactionStatus FinalizeCargoLoader(long taskId, long flightId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.FinalizeCargoLoader(taskId, flightId, userId);
            }
        }

        public TransactionStatus AddCargoLoaderFlightLegULD(long uldTypeId, string prefix, string serial, long taskId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.AddCargoLoaderFlightLegULD(uldTypeId, prefix, serial, taskId, userId);
            }
        }

        public TransactionStatus UpdateFlightLegUldLocation(long uldId, int locationId, long taskId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateFlightLegUldLocation(uldId, locationId, taskId, userId);
            }
        }

        public TransactionStatus DeleteCargoLoaderFlightLegUld(long uldId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DeleteCargoLoaderFlightLegUld(uldId, userId);
            }
        }

        public AwbModel[] GetLoadingPlan(long flightId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetLoadingPlan(flightId, taskId);
            }
        }

        public AwbModel GetLoadingPlanForAwb(long awbId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetLoadingPlanForAwb(awbId, taskId);
            }
        }

        public UldViewItem[] GetUldView(long flightId, long uldId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetUldView(flightId, uldId);
            }
        }

        public ValidatedShipment ValidateLoaderShipment(long flightId, long taskId, string shipment)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.ValidateLoaderShipment(flightId, taskId, shipment);
            }
        }

        public TransactionStatus DropPiecesIntoForklift(long userId, long taskId, long awbId, int count)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropPiecesIntoForklift(userId, taskId, awbId, count);
            }
        }

        public CargoLoaderFlightLegUld[] AvailableULDsForBuild(long flightId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.AvailableULDsForBuild(flightId);
            }
        }

        public LoadForkliftViewItem[] GetForkliftView(long taskId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetForkliftView(taskId, userId);
            }
        }

        public TransactionStatus RemovePiecesFromForklift(long detailsId, int count)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.RemovePiecesFromForklift(detailsId, count);
            }
        }


        public TransactionStatus DropSelectedItemsIntoLocation(long taskId, long userId, int locationId, long[] detailIds)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropSelectedItemsIntoLocation(taskId, userId, locationId, detailIds);
            }
        }

        public TransactionStatus DropAllForkliftPiecesIntoLocation(long taskId, long userId, int locationId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropAllForkliftPiecesIntoLocation(taskId, userId, locationId);
            }
        }

        public TransactionStatus DropSelectedItemsIntoULD(long taskId, long userId, long uldId, long[] detailIds)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropSelectedItemsIntoULD(taskId, userId, uldId, detailIds);
            }
        }

        public TransactionStatus DropAllForkliftPiecesIntoULD(long taskId, long userId, long uldId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropAllForkliftPiecesIntoULD(taskId, userId, uldId);
            }
        }

        public TransactionStatus SetFlightBuildLocation(long taskId, long userId, int locationId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.SetFlightBuildLocation(taskId, userId, locationId);
            }
        }

        public TransactionStatus RemoveAwbFromUld(long manifestAwbDetaildId, long uldId, long taskId, long userId, int locationId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.RemoveAwbFromUld(manifestAwbDetaildId, uldId, taskId, userId, locationId);
            }
        }

        public TransactionStatus UpdateFlightBuildLocation(long flightManifestId, long userId, long taskId, int locationId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateFlightBuildLocation(flightManifestId, userId, taskId, locationId);
            }
        }
    }
}
