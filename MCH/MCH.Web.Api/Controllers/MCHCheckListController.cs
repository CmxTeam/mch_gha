﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Customs;
using MCH.BLL.Units;
using MCH.Web.Api.Models.Customs;
using System.Collections.Generic;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class MCHChecklistController : BaseApiController
    {
        [HttpPost]
        public Transactional<CheckListModel> GetCheckList(GetChecklistParam model)
        {
            return this.WrapInTransaction<CheckListModel>(() =>
            {
                using (CustomsUnit customsUnit = new CustomsUnit())
                {
                    return customsUnit.GetCheckListModel(model.TaskId, model.UserId, model.AwbId);
                }
            });
        }

        public Transactional<List<CheckListShipmentItem>> GetCheckListTasks(long warehouseId, int menuId, long userId, CheckListStatusTypes status)
        {
            return this.WrapInTransaction<List<CheckListShipmentItem>>(() =>
            {
                using (CustomsUnit customsUnit = new CustomsUnit())
                {
                    return customsUnit.GetCheckListTasks(warehouseId, menuId, userId, status);
                }
            });
        }

        [HttpPost]
        public TransactionStatus FinalizeChecklist(ChecklistResultsParam param)
        {
            using (CustomsUnit unitOfWork = new CustomsUnit())
            {                
                unitOfWork.FinalizeChecklistResult(param);
            }

            TransactionStatus tmpResult = new TransactionStatus { Status = true };
            return tmpResult;
        }

        [HttpPost]
        public TransactionStatus SaveChecklist(ChecklistResultsParam param)
        {
            using (CustomsUnit unitOfWork = new CustomsUnit())
            {
                unitOfWork.SaveCheckListResult(param);
            }
            TransactionStatus tmpResult = new TransactionStatus { Status = true };
            return tmpResult;
        }       
    }
}