﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Tasks;
using MCH.BLL.Units;
using MCH.Web.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class MCHTasksController : BaseApiController
    {
        [HttpPost]
        public TransactionStatus AssignToUsers(UserTasksUsersParam param)
        {
            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                unit.AssignTasksToUsers(param.TaskIds, param.ToUserIds, param.UserId);
            }
            return new TransactionStatus { Status = true };            
        }

        [HttpPost]
        public Transactional<List<TaskStatus>> CancelTasks(UserTasksParam param)
        {
            return this.WrapInTransaction<List<TaskStatus>>(() =>
            {
                using (TasksUnitOfWork unit = new TasksUnitOfWork())
                {
                    return unit.CancelTasks(param.TaskIds, param.UserId);
                }
            });            
        }

        [HttpPost]
        public Transactional<List<TaskStatus>> ReopenTasks(UserTasksParam param)
        {
            return this.WrapInTransaction<List<TaskStatus>>(() =>
            {
                using (TasksUnitOfWork unit = new TasksUnitOfWork())
                {
                    return unit.ReopenTasks(param.TaskIds, param.UserId);
                }
            });  
        }

        [HttpPost]
        public TransactionStatus AddInstructions(UserTasksCommentParam param)
        {
            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                unit.AddInstructions(param.TaskIds, param.UserId, param.Comment);
            }
            return new TransactionStatus { Status = true };        
        }


        public Transactional<List<TaskAlert>> GetTaskAlerts(long taskId)
        {
            return this.WrapInTransaction<List<TaskAlert>>(() =>
            {
                using (TasksUnitOfWork unit = new TasksUnitOfWork())
                {
                    return unit.GetTaskAlerts(taskId);
                }
            });  
        }

    }
}