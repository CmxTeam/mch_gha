﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Common;
using MCH.BLL.Units;
using MCH.Core.Models.Manual;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class UtilsController : ApiController
    {
        [HttpGet]
        public Transactional<List<AccountCarrierItem>> GetCarrierAccounts()
        {
            Transactional<List<AccountCarrierItem>> tmpResult = new Transactional<List<AccountCarrierItem>>();

            using (CommonUnit unit = new CommonUnit())
            {
                tmpResult.Data = unit.GetAccountCarriers(null);
                tmpResult.Transaction = new TransactionStatus { Status = true };
            }

            return tmpResult;
        }

        [HttpGet]
        public Transactional<List<PortItem>> GetPorts()
        {
            Transactional<List<PortItem>> tmpResult = new Transactional<List<PortItem>>();

            using (CommonUnit unit = new CommonUnit())
            {
                tmpResult.Data = unit.GetPorts();
                tmpResult.Transaction = new TransactionStatus { Status = true };
            }

            return tmpResult;
        }

        [HttpPost]
        public Transactional<long> AddAwb(AddAwbModel model)
        {
            Transactional<long> tmpStatus = new Transactional<long>();
            using (ManualUnit unit = new ManualUnit())
            {
                var tmpResult = unit.DataContext.MCH_ManualAddAWB(model.AccountId, model.CarrierNumber, model.AwbSerialNumber, model.OriginId, model.DestinationId, model.TotalPieces, model.ShipperName, model.ConsigneeName, model.UserId, model.WarehouseId).FirstOrDefault();

                tmpStatus.Data = tmpResult.AwbId.Value;
                tmpStatus.Transaction = new TransactionStatus { Status = tmpResult.Success == 1 ? true : false, Error = tmpResult.Msg };
            }

            return tmpStatus;
        }

        [HttpPost]
        public Transactional<long> AddAwbHwb(AddHwbModel model)
        {
            Transactional<long> tmpStatus = new Transactional<long>();

            using (ManualUnit unit = new ManualUnit())
            {
                var tmpResult = unit.DataContext.MCH_ManualAddHWB(model.AwbId, model.HwbSerialNumber, model.OriginId, model.DestinationId, model.TotalPieces, model.ShipperName, model.ConsigneeName, model.UserId, model.WarehouseId).FirstOrDefault();

                tmpStatus.Data = tmpResult.HwbId.Value;
                tmpStatus.Transaction = new TransactionStatus { Status = tmpResult.Success == 1 ? true : false, Error = tmpResult.Msg };
            }

            return tmpStatus;
        }

        [HttpGet]
        public Transactional<List<AwbHwbModel>> GetAwbHwbs(long awbId)
        {
            Transactional<List<AwbHwbModel>> tmpResult = new Transactional<List<AwbHwbModel>>();

            using (ManualUnit unit = new ManualUnit())
            {
                tmpResult.Data = unit.GetAwbHwbs(awbId);
                tmpResult.Transaction = new TransactionStatus { Status = true };
            }

            return tmpResult;
        }

        [HttpGet]
        public TransactionStatus CompleteManualAccept(long awbId, long userId, long warehouseId)
        {
            TransactionStatus tmpStatus = new TransactionStatus { Status = true };
            using (ManualUnit unit = new ManualUnit())
            {
                var tmpResult = unit.DataContext.MCH_CompleteManualAccept(awbId, userId, warehouseId);
            }

            return tmpStatus;
        }


        [HttpGet]
        public TransactionStatus CompleteManualRelease()
        {
            TransactionStatus tmpStatus = new TransactionStatus { Status = true };
            using (ManualUnit unit = new ManualUnit())
            {
                
            }
            return tmpStatus;
        }
       
    }
}
