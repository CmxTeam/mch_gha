﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Units;
using MCH.Web.Api.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MCH.Web.Api.Controllsers
{
    public class MCHCargoReceiverController : BaseApiController
    {
        [HttpGet]
        public Transactional<CargoReceiverFlight[]> GetCargoReceiverFlights(string station, int userID, TaskStatuses status, string carrierNo, string origin, string flightNo, ReceiverSortFields sortBy)
        {
            return this.WrapInTransaction<CargoReceiverFlight[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetCargoReceiverFlights(station, userID, status, carrierNo, origin, flightNo, sortBy);
                }
            });
        }

        [HttpGet]
        public TransactionStatus RecoverFlight(string station, long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.RecoverFlight(station, taskId, userId);
            }
        }

        [HttpGet]
        public Transactional<FlightUldInfo> GetFlightULDs(long flightManifestId, RecoverStatuses status, string filter)
        {
            return this.WrapInTransaction<FlightUldInfo>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetFlightULDs(flightManifestId, status, filter);
                }
            });
        }

        [HttpGet]
        public Transactional<FlightUldInfo> GetFlightULD(long flightManifestId, long uldId)
        {
            return this.WrapInTransaction<FlightUldInfo>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetFlightULD(flightManifestId, uldId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus SwitchBUPMode(string station, long uldId, long userId, long manifestId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.SwitchBUPMode(station, uldId, userId, manifestId);
            }
        }

        [HttpGet]
        public TransactionStatus UpdateFlightETA(string station, long flightManifestId, long taskId, long userId, DateTime eta)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.UpdateFlightETA(station, flightManifestId, taskId, userId, eta);
            }
        }

        [HttpGet]
        public Transactional<int> GetForkliftCount(long userId, long taskId)
        {
            return this.WrapInTransaction<int>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetForkliftCount(userId, taskId);
                }
            });
        }

        [HttpGet]
        public Transactional<Uld> GetFlightULD(long uldId)
        {
            return this.WrapInTransaction<Uld>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetFlightULD(uldId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus RecoverUld(long uldId, long userId, long flightManifestId, long locationId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.RecoverUld(uldId, userId, flightManifestId, locationId);
            }

            return new TransactionStatus { Status = true };
        }

        [HttpGet]
        public TransactionStatus DropForkliftPieces(long taskId, long userId, int locationId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.DropForkliftPieces(taskId, userId, locationId);
            }

            return new TransactionStatus { Status = true };
        }

        [HttpGet]
        public TransactionStatus DropPiecesToLocation(long taskId, long userId, int locationId, long forkliftDetailsId, int pieces)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.DropPiecesToLocation(taskId, userId, locationId, forkliftDetailsId, pieces);
            }
            return new TransactionStatus { Status = true };
        }

        [HttpGet]
        public Transactional<ValidatedShipment> ValidateShipment(long userId, long taskId, long uldId, string shipment)
        {
            return this.WrapInTransaction<ValidatedShipment>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.ValidateShipment(userId, taskId, uldId, shipment);
                }
            });
        }

        [HttpGet]
        public Transactional<ForkliftView[]> GetForkliftView(long taskId, long userId)
        {
            return this.WrapInTransaction<ForkliftView[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetForkliftView(taskId, userId);
                }
            });
        }

        [HttpGet]
        public Transactional<UldView[]> GetUldView(long taskId, long userId, long uldId, int status)
        {
            return this.WrapInTransaction<UldView[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetUldView(taskId, userId, uldId, status);
                }
            });
        }        

        [HttpGet]
        public Transactional<UldView[]> GetFlightView(long taskId, long manifestId)
        {
            return this.WrapInTransaction<UldView[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetFlightView(taskId, manifestId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus AddForkliftPieces(long detailsId, long taskId, int quantity, long userid)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.AddForkliftPieces(detailsId, taskId, quantity, userid);
            }
        }

        [HttpGet]
        public Transactional<CargoReceiverFlight> GetFlightManifestById(long manifestId, long taskId, long userId)
        {
            return this.WrapInTransaction<CargoReceiverFlight>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetFlightManifestById(manifestId, taskId, userId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus RemoveItemsFromForklift(long forkliftDetailsId, int count)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.RemoveItemsFromForklift(forkliftDetailsId, count);
            }
            return new TransactionStatus { Status = true };
        }

        [HttpGet]
        public TransactionStatus RemoveAllItemsFromForklift(long userId, long taskId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                receiverUnit.RemoveAllItemsFromForklift(userId, taskId);
            }
            return new TransactionStatus { Status = true };
        }

        [HttpGet]
        public Transactional<AlertModel[]> GetFlightAlerts(string port, long manifestId)
        {
            return this.WrapInTransaction<AlertModel[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetFlightAlerts(port, manifestId);
                }
            });
        }

        [HttpGet]
        public Transactional<AlertModel[]> GetUldAlerts(string port, long uldId)
        {
            return this.WrapInTransaction<AlertModel[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetUldAlerts(port, uldId);
                }
            });
        }

        [HttpGet]
        public Transactional<AlertModel[]> GetTaskAlerts(string port, long taskId)
        {
            return this.WrapInTransaction<AlertModel[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetTaskAlerts(port, taskId);
                }
            });
        }

        [HttpGet]
        public Transactional<AlertModel[]> GetAwbAlerts(string port, long awbId)
        {
            return this.WrapInTransaction<AlertModel[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAwbAlerts(port, awbId);
                }
            });
        }

        [HttpGet]
        public Transactional<FlightProgress> GetFlightProgress(long manifestId, long taskId)
        {
            return this.WrapInTransaction<FlightProgress>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetFlightProgress(manifestId, taskId);
                }
            });
        }

        [HttpGet]
        public TransactionStatus FinalizeReceiver(long taskId, long manifestId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.FinalizeReceiver(taskId, manifestId, userId);
            }
        }

        [HttpGet]
        public Transactional<ScannedShipmentInfo[]> ScanShipment(long userId, long taskId, string shipmentNumber)
        {
            return this.WrapInTransaction<ScannedShipmentInfo[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.ScanShipment(userId, taskId, shipmentNumber);
                }
            });
        }

        [HttpGet]
        public TransactionStatus AddToForklift(long taskId, long userId, long uldId, long hwbId, long awbId, int count)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.AddToForklift(taskId, userId, uldId, hwbId, awbId, count);
            }
        }

        [HttpGet]
        public Transactional<AvailableItmes[]> GetAvailableCarriers(string station, long userId)
        {
            return this.WrapInTransaction<AvailableItmes[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAvailableCarriers(station, userId);
                }
            });
        }

        [HttpGet]
        public Transactional<DateTime[]> GetAvailableEtds(long carrierId, string station, long userId)
        {
            return this.WrapInTransaction<DateTime[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAvailableEtds(carrierId, station, userId);
                }
            });
        }

        [HttpGet]
        public Transactional<AvailableFlight[]> GetAvailableFlights(long carrierId, DateTime etd, string station, long userId)
        {
            return this.WrapInTransaction<AvailableFlight[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAvailableFlights(carrierId, etd, station, userId);
                }
            });
        }       

        [HttpGet]
        public Transactional<AvailableFlight[]> GetAvailableFlightsByEta(long carrierId, DateTime eta, string station, long userId)
        {
            return this.WrapInTransaction<AvailableFlight[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAvailableFlightsByEta(carrierId, eta, station, userId);
                }
            });            
        }

        [HttpGet]
        public Transactional<DateTime[]> GetAvailableEtas(long carrierId, string station, long userId)
        {
            return this.WrapInTransaction<DateTime[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAvailableEtas(carrierId, station, userId);
                }
            });  
        }

        [HttpGet]
        public TransactionStatus LinkTaskToUserId(long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.LinkTaskToUserId(taskId, userId);
            }
        }

        [HttpGet]
        public Transactional<int> GetAwbPiecesCountInLocation(long awbId, long locationId)
        {
            return this.WrapInTransaction<int>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAwbPiecesCountInLocation(awbId, locationId, 0);
                }
            });             
        }

        [HttpGet]
        public Transactional<int> GetHwbPiecesCountInLocation(long hwbId, long locationId)
        {
            return this.WrapInTransaction<int>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetHwbPiecesCountInLocation(hwbId, locationId, 0);
                }
            });
        }


        [HttpGet]
        public TransactionStatus RelocateAwbPieces(long awbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.RelocateAwbPieces(awbId, oldLocationId, newLocationId, pcs, taskId, userId);
            }
        }

        [HttpGet]
        public TransactionStatus RelocateHwbPieces(long hwbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.RelocateHwbPieces(hwbId, oldLocationId, newLocationId, pcs, taskId, userId);
            }
        }

        [HttpGet]
        public Transactional<long> GetLocationId(string locationName, string station)
        {
            return this.WrapInTransaction<long>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetLocationId(locationName, station);
                }
            });            
        }

        [HttpGet]
        public Transactional<List<CargoReceiverFlight>> GetFlightManifestByShipment(long userId, long warehouseId, string shipmentRef)
        {
            return this.WrapInTransaction<List<CargoReceiverFlight>>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetFlightManifestByShipment(userId, warehouseId, shipmentRef).ToList();
                }
            }); 
        }

        [HttpGet]
        public Transactional<UldView[]> GetShipmentUldView(long taskId, long manifestId, string shipmentRef)
        {
            return this.WrapInTransaction<UldView[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetShipmentUldView(taskId, manifestId, shipmentRef);
                }
            }); 
        }
        
         [HttpGet]
        public Transactional<AvailableFlight[]> GetAvailableFlightsByCarrier(long carrierId, string station, long userId)
        {
            return this.WrapInTransaction<AvailableFlight[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAvailableFlightsByCarrier(carrierId, station, userId);
                }
            });
        }

        [HttpGet]
        public Transactional<AvailableFlight[]> GetAvailableFlightsByFlightNo(long carrierId, string flightNo, string station, long userId)
        {
            return this.WrapInTransaction<AvailableFlight[]>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetAvailableFlightsByFlightNo(carrierId,flightNo, station, userId);
                }
            });
        }

        [HttpGet]
        public Transactional<List<FlightUldInfoExtended>> GetCargoReceiverUlds(long warehouseId, RecoverStatuses status, string filter)
        {
            return this.WrapInTransaction<List<FlightUldInfoExtended>>(() =>
            {
                using (ReceiverUnitOfWork unit = new ReceiverUnitOfWork())
                {
                    return unit.GetCargoReceiverUlds(warehouseId, status, filter);
                }
            });
        } 
    }
}
