﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Units;
using MCH.Web.Api.Controllers;
using MCH.Web.Api.Models.Task;
using MCH.Web.Api.Models.Warehouse;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace MCH.Web.Api.Controllsers
{
    public class ScannerMCHController : BaseApiController
    {

        public WarehouseLocationModel[] GetWarehouseLocations(string station, LocationType location)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetWarehouseLocations(station, location);
            }
        }

        [HttpPost]
        public Transactional<UserTaskCounter[]> GetUserTaskCounts(TaskCountSelector model)
        {
            return this.WrapInTransaction<UserTaskCounter[]>(() =>
            {
                using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
                {
                    return scannerUnit.GetUserTaskCounts(model.UserId, model.TaskTypeIds, model.WarehouseId);
                }
            });
        }

        [HttpPost]
        public Transactional<WarehouseSimpleModel[]> GetUserWarehouses(WarehouseSelector model)
        {
            return this.WrapInTransaction<WarehouseSimpleModel[]>(() =>
            {
                using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
                {
                    return scannerUnit.GetUserWarehouses(model.WarehouseShellIds, model.UserId);
                }
            });
        }

        [HttpGet]
        public Transactional<LocationItem> GetLocationIdByLocationBarcode(string station, string barcode)
        {
            return this.WrapInTransaction<LocationItem>(() =>
            {
                using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
                {
                    return scannerUnit.GetLocationIdByLocationBarcode(station, barcode);
                }
            });
        }

        public void AddTaskSnapshotReference(long taskId, string reference)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                scannerUnit.AddTaskSnapshotReference(taskId, reference);
            }
        }

        public bool ExecuteNonQuery(string station, string query)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.ExecuteNonQuery(station, query);
            }
        }

        public DataTable ExecuteQuery(string station, string query)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.ExecuteQuery(station, query);
            }
        }

        [HttpGet]
        public Transactional<AwbInfo> GetAwbInfo(string station, string shipment)
        {
            return this.WrapInTransaction<AwbInfo>(() =>
            {
                using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
                {
                    return scannerUnit.GetAwbInfo(station, shipment);
                }
            });            
        }

        [HttpGet]
        public Transactional<List<PrinterItem>> GetPrinters(long userId, PrinterTypes printerType)
        {
            return this.WrapInTransaction<List<PrinterItem>>(() =>
            {
                using (ScannerUnitOfWork unit = new ScannerUnitOfWork())
                {
                    return unit.GetPrinters(userId, printerType);
                }
            });  
        }

    }
}
