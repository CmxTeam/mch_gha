﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Screening;
using MCH.BLL.Units;
using System.Collections.Generic;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class MCHScreeningController : BaseApiController
    {
        [HttpGet]
        public Transactional<List<ScreeningDeviceItem>> GetScreeningDevices(int warehouseId, ScreeningDeviceTypes deviceType)
        {
            return this.WrapInTransaction<List<ScreeningDeviceItem>>(() =>
            {
                using (ScreeningUnit unit = new ScreeningUnit())
                {
                    return unit.GetScreeningDevices(warehouseId, deviceType);
                }
            });  
        }

        [HttpGet]
        public Transactional<ScreeningDeviceItem> GetDeviceInfo(long deviceId)
        {
            return this.WrapInTransaction<ScreeningDeviceItem>(() =>
            {
                using (ScreeningUnit unit = new ScreeningUnit())
                {
                    return unit.GetDeviceInfo(deviceId);
                }
            }); 
        }

        [HttpGet]
        public Transactional<List<MorphoScreeningItem>> GetMorphoScreeningInfo(long deviceId, long userId)
        {
            return this.WrapInTransaction<List<MorphoScreeningItem>>(() =>
            {
                using (ScreeningUnit unit = new ScreeningUnit())
                {
                    return unit.GetMorphoScreeningInfo(deviceId, userId);
                }
            }); 
        }

        [HttpGet]
        public TransactionStatus KeepDeviceAlive(long deviceId, long userId)
        {
            using (ScreeningUnit unit = new ScreeningUnit())
            {
                unit.KeepDeviceAlive(deviceId, userId);
            }
            return new TransactionStatus { Status = true };            
        }

        [HttpGet]
        public TransactionStatus LinkDeviceToUser(long deviceId, long userId)
        {
            using (ScreeningUnit unit = new ScreeningUnit())
            {
                unit.LinkDeviceToUser(deviceId, userId);
            }
            return new TransactionStatus { Status = true };
        }

        [HttpGet]
        public Transactional<ScreeningTaskItem> GetScreeningTask(long warehouseid, long taskId, long userId, string barcode)
        {
            return this.WrapInTransaction<ScreeningTaskItem>(() =>
            {
                using (ScreeningUnit unit = new ScreeningUnit())
                {
                    return unit.GetScreeningTask(warehouseid, taskId, userId, barcode);
                }
            }); 
        }

        [HttpPost]
        public TransactionStatus SaveScreeningTransaction(ScreeningTransaction model)
        {
            using (ScreeningUnit unit = new ScreeningUnit())
            {
                unit.SaveScreeningTransaction(model);
            }
            return new TransactionStatus { Status = true };
        }

        [HttpGet]
        public Transactional<CCSFItem> ValidateCertificationNumber(string certificationNumber)
        {
            return this.WrapInTransaction<CCSFItem>(() =>
            {
                using (ScreeningUnit unit = new ScreeningUnit())
                {
                    return unit.ValidateCertificationNumber(certificationNumber);
                }
            });
        }

        [HttpGet]
        public Transactional<List<ScreeningInspectionRemark>> GetScreeningInspectionRemarks(int warehouseId)
        {
            return this.WrapInTransaction<List<ScreeningInspectionRemark>>(() =>
            {
                using (ScreeningUnit unit = new ScreeningUnit())
                {
                    return unit.GetScreeningInspectionRemarks(warehouseId);
                }
            });
        }
    }
}