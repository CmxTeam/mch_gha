﻿using System;
using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Common;
using MCH.BLL.Units;
using MCH.Web.Api.Models.Common;
using System.Collections.Generic;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class MCHCommonController : BaseApiController
    {
        [HttpPost]
        public Transactional<List<LocationItem>> GetLocations(LocationTypeListParam locationTypes)
        {
            return this.WrapInTransaction<List<LocationItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetLocations(locationTypes.LocationTypes);
                }
            });
        }

        [HttpGet]
        public Transactional<List<UldTypeItem>> GetUldTypes(long warehouseId)
        {
            return this.WrapInTransaction<List<UldTypeItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetUldTypes(warehouseId);
                }
            });
        }

        [HttpGet]
        public Transactional<List<PortItem>> GetPorts()
        {
            return this.WrapInTransaction<List<PortItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetPorts();
                }
            });
        }

        [HttpGet]
        public Transactional<List<CarrierItem>> GetCarrierList(long motId)
        {
            return this.WrapInTransaction<List<CarrierItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetCarrierList(motId);
                }
            });
        }

        [HttpGet]
        public Transactional<List<AccountCarrierItem>> GetAccountCarriers(long? accountId)
        {
            return this.WrapInTransaction<List<AccountCarrierItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetAccountCarriers(accountId);
                }
            });
        }

        [HttpGet]
        public Transactional<List<AccountingInfoIdentifierItem>> GetAccountingCodes()
        {
            return this.WrapInTransaction<List<AccountingInfoIdentifierItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetAccountingCodes();
                }
            });
        }

        [HttpGet]
        public Transactional<List<SearchableItem>> GetCustomerList()
        {
            return this.WrapInTransaction<List<SearchableItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetCustomerList();
                }
            });
        }

        [HttpGet]
        public Transactional<List<SearchableItem>> GetAgentList()
        {
            return this.WrapInTransaction<List<SearchableItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetAgentList();
                }
            });
        }

        [HttpGet]
        public Transactional<List<IdCodeItem>> GetShipmentUnitTypes()
        {
            return this.WrapInTransaction<List<IdCodeItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetShipmentUnitTypes();
                }
            });
        }

        [HttpGet]
        public Transactional<List<IdCodeItem>> GetUldUnitTypes()
        {
            return this.WrapInTransaction<List<IdCodeItem>>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetUldUnitTypes();
                }
            });
        }

        [HttpGet]
        public Transactional<int?> GetForkliftCount(long taskId, long userId)
        {
            return this.WrapInTransaction<int?>(() =>
            {
                using (CommonUnit commonUnit = new CommonUnit())
                {
                    return commonUnit.GetForkliftCount(taskId, userId);
                }
            });
        }
    }
}