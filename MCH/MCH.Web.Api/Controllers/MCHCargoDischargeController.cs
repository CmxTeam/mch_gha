﻿using System.Collections.Generic;
using System.Web.Http;
using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Discharge;
using MCH.BLL.Model.Enum;
using MCH.BLL.Units;

namespace MCH.Web.Api.Controllers
{
    public class MCHCargoDischargeController : BaseApiController
    {
        [HttpGet]
        public Transactional<List<CargoDischargeTaskItem>> GetCargoDischargeTasks(long warehouseId, enumCargoDischargeStatusTypes status)
        {
            return this.WrapInTransaction<List<CargoDischargeTaskItem>>(() =>
            {
                using (DischargeUnitOfWork unit = new DischargeUnitOfWork())
                {
                    return unit.GetCargoDischargeTasks(warehouseId, status);
                }
            });
        }

        [HttpGet]
        public Transactional<CargoDischargeTaskItem> GetCargoDischargeTask(long taskId)
        {
            return this.WrapInTransaction<CargoDischargeTaskItem>(() =>
            {
                using (DischargeUnitOfWork unit = new DischargeUnitOfWork())
                {
                    return unit.GetCargoDischargeTask(taskId);
                }
            });
        }


        [HttpGet]
        public TransactionStatus DischargeAwbPieces(long awbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId, string truckNumber)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.DischargeAwbPieces(awbId, oldLocationId, newLocationId, pcs, taskId, userId, truckNumber);
            }
        }

        [HttpGet]
        public TransactionStatus DischargeHwbPieces(long hwbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId, string truckNumber)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.DischargeHwbPieces(hwbId, oldLocationId, newLocationId, pcs, taskId, userId, truckNumber);
            }
        }

    }
}