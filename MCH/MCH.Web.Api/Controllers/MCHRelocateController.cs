﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Relocate;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class MCHRelocateController : BaseApiController
    {
        public Transactional<RelocateTaskItem> GetRelocateTask(long warehouseId, long userId, string barcode)
        {
            return this.WrapInTransaction<RelocateTaskItem>(() =>
            {
                using (RelocateUnit relocateUnit = new RelocateUnit())
                {
                    return relocateUnit.GetRelocateTask(warehouseId, userId, barcode);
                }
            });
        }

        [HttpGet]
        public TransactionStatus RelocatePieces(long referenceId, ReferenceTypes referenceType , long oldLocationId, long newLocationId, int pcs, long taskId, long userId)
        {
            using(RelocateUnit rlUnit = new RelocateUnit())
            {
                try
                {
                    rlUnit.RelocatePieces(referenceId, referenceType, oldLocationId, newLocationId, pcs, taskId, userId);
                    return new TransactionStatus { Status = true };
                }
                catch(Exception ex) 
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };  
                }    
            }
        }


        [HttpGet]
        public TransactionStatus RelocateAwbPieces(long awbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.RelocateAwbPieces(awbId, oldLocationId, newLocationId, pcs, taskId, userId);
            }
        }

        [HttpGet]
        public TransactionStatus RelocateHwbPieces(long hwbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        {
            using (ReceiverUnitOfWork receiverUnit = new ReceiverUnitOfWork())
            {
                return receiverUnit.RelocateHwbPieces(hwbId, oldLocationId, newLocationId, pcs, taskId, userId);
            }
        }
    }
}