﻿using CMX.Framework.Utils.Security;
using MCH.BLL.Model.Snapshot;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MCH.Web.Api.Controllers
{
    public class MCHSnapShotController : BaseApiController
    {
        public Transactional<SnapShotTask> GetSnapShotTask(long taskId, long warehouseId, long userId, string barcode)
        {
            return this.WrapInTransaction<SnapShotTask>(() =>
            {
                using (SnapshotUnit snapshotUnit = new SnapshotUnit())
                {
                    return snapshotUnit.GetSnapShotTask(taskId, warehouseId, userId, barcode);
                }
            });            
        }

        public Transactional<List<ConditionType>> GetConditionTypes(int companyId)
        {
            return this.WrapInTransaction<List<ConditionType>>(() =>
            {
                using (SnapshotUnit snapshotUnit = new SnapshotUnit())
                {
                    return snapshotUnit.GetConditionTypes(companyId);
                }
            });
        }

        [HttpPost]
        public TransactionStatus UploadSnapshotImage(SnapShotImageItem item) 
        {
            using (SnapshotUnit unit = new SnapshotUnit())
            {
                try
                {
                    unit.UploadSnapshotImage(item);
                    return new TransactionStatus { Status = true };  
                }
                catch(Exception ex) 
                {
                    return new TransactionStatus { Status = false, Error = ex.Message };  
                }                
            }
            
        }
              


    }
}