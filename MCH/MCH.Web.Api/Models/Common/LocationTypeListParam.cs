﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Api.Models.Common
{
    public class LocationTypeListParam
    {
        public List<long> LocationTypes { get; set; }
    }
}