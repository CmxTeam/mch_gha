﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Api.Models.Warehouse
{
    public class WarehouseSelector
    {
        public long[] WarehouseShellIds { get; set; }

        public long UserId { get; set; }
    }
}