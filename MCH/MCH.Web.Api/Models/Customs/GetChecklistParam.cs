﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Web.Api.Models.Customs
{
    public class GetChecklistParam
    {
        public long TaskId { get; set; }
        public long AwbId { get; set; }
        public long UserId { get; set; }
    }
}
