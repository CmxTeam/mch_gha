﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Api.Models
{
    public class UserTasksParam
    {
        public long UserId { get; set; }
        public long[] TaskIds { get; set; }
    }

    public class UserTasksUsersParam : UserTasksParam
    {
        public long[] ToUserIds { get; set; }
    }

    public class UserTasksCommentParam : UserTasksParam
    {
        public string Comment { get; set; }
    }
}