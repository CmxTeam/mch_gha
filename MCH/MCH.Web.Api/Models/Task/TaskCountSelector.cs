﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Api.Models.Task
{
    public class TaskCountSelector
    {
        public long UserId { get; set; }

        public int[] TaskTypeIds { get; set; }

        public long? WarehouseId { get; set; }
    }
}