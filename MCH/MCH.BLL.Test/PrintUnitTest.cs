﻿using System;
using MCH.BLL.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MCH.BLL.Test
{
    [TestClass]
    public class PrintUnitTest
    {
        [TestMethod]
        public void PrintAttachmentTest()
        {
            const long attachid = 86;
            using (ReportPrintingUnit unit = new ReportPrintingUnit())
            {
                unit.PrintAttachment(attachid, @"\\AMCMXWSDC2\Samsung SCX-4300 Series", 1);
            }
        }
    }
}
