﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MCH.BLL.Units;
using System.Linq;

namespace MCH.BLL.Test
{
    [TestClass]
    public class UserAuthTest
    {
        [TestMethod]
        public void TestUserCreation()
        {
            //MembershipCreateStatus creationResult;
            //Membership.CreateUser("Oniel.Parker@cargomatrix.com", "RDiaz123#", "Oniel.Parker@cargomatrix.com", "Are you the admin ?", "YEP", true, out creationResult);
            //var tmpMembershipUser = Membership.GetUser("oniel.parker@cargomatrix.com");

            //Console.WriteLine("Changed : " + tmpMembershipUser.ChangePassword("OParker123#", "OParker123#"));

            //Membership.DeleteUser("Oniel.Parker@cargomatrix.com");
        }

        internal string EncodePassword(string pass, int passwordFormat, string salt)
        {
            //if (passwordFormat == 0) // MembershipPasswordFormat.Clear
            //    return pass;

            //byte[] bIn = Encoding.Unicode.GetBytes(pass);
            //byte[] bSalt = Convert.FromBase64String(salt);
            //byte[] bAll = new byte[bSalt.Length + bIn.Length];
            //byte[] bRet = null;

            //Buffer.BlockCopy(bSalt, 0, bAll, 0, bSalt.Length);
            //Buffer.BlockCopy(bIn, 0, bAll, bSalt.Length, bIn.Length);

            //HashAlgorithm s = HashAlgorithm.Create();
            //bRet = s.ComputeHash(bAll);

            //return Convert.ToBase64String(bRet);

            return String.Empty;
        }

        [TestMethod]
        public void GetAwbStep()
        {
            using (AwbWizardUnit unit = new AwbWizardUnit())
            {
                var awb = unit.GetAwbStep(2310);
                Assert.IsTrue(awb.SenderReferences.Count() > 0);

                var references = awb.SenderReferences;

                Assert.IsFalse(references.All(p => String.IsNullOrEmpty(p.FileReference) &&
                                    !p.ParticipantIdentifierId.HasValue &&
                                    String.IsNullOrEmpty(p.ParticipantCode) &&
                                    !p.AirportId.HasValue));
            }
        }

        [TestMethod]
        public void GetAwbByNumber()
        {
            using (AwbWizardUnit unit = new AwbWizardUnit())
            {
                var awb = unit.GetAwbByNumber(2391,"32423234",19295,13505);
                //Assert.IsTrue(awb.SenderReferences.Count() > 0);

                //var references = awb.SenderReferences;

                //Assert.IsFalse(references.All(p => String.IsNullOrEmpty(p.FileReference) &&
                //                    !p.ParticipantIdentifierId.HasValue &&
                //                    String.IsNullOrEmpty(p.ParticipantCode) &&
                //                    !p.AirportId.HasValue));
            }
        }
    }
}
