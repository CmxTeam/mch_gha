﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MCH.BLL.Units;

namespace MCH.BLL.Test
{
    [TestClass]
    public class ImportUnitTest
    {
        [TestMethod]
        public void GetAWBImportControlTest()
        {
            using (ImportExportUnit unit = new ImportExportUnit())
            {
                var tmpImport = unit.GetAWBImportControl(1109, "");

                Assert.AreEqual<string>(tmpImport, "IT 668391813");
            }
        }
    }
}
