﻿using System;
using System.Linq;
using CMX.Framework.Utils.Security;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MCH.BLL.Units;

namespace MCH.BLL.Test
{
    [TestClass]
    public class BuildFreightTest
    {
        //[TestMethod]
        //public void GetBuildFreightTasksTest()
        //{
        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        var tmpTasks = unit.GetBuildFreightTasks(1, 1, enumBuildFreightStatusTypes.All);

        //        Assert.AreEqual<int>(tmpTasks.Count, 5);
        //        Assert.AreEqual<long>(tmpTasks[0].TaskId, 105);
        //    }
        //}

        //[TestMethod]
        //public void GetBuildFreightTaskTest()
        //{
        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        var tmpTask = unit.GetBuildFreightTask(105, 105);
        //        Assert.AreEqual(tmpTask.TaskId, 105);
        //    }
        //}

        //[TestMethod]
        //public void AddUldTest()
        //{
        //    MCHDBEntities DataContext = new MCHDBEntities();

        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        string serialNo = "Test" + DateTime.Now.Ticks.ToString();
        //        TransactionStatus status = unit.AddUld(2, 105, 1, serialNo, "AA", 106);

        //        if (status.Status == true)
        //        {
        //            Assert.IsTrue(DataContext.ULDs.Any(s => s.SerialNumber == serialNo && s.FlightManifestId == 105));
        //            var uld = DataContext.ULDs.Single(s => s.SerialNumber == serialNo && s.FlightManifestId == 105);
        //            Assert.IsTrue(DataContext.Transactions.Any(s => s.EntityId == uld.Id && s.EntityTypeId == 4 && s.Description == "ULD Added" && s.TaskId == 106));
        //        }
        //    }
        //}

        //[TestMethod]
        //public void EditUldTest()
        //{
        //    string serialNo = "TestEdit" + DateTime.Now.Ticks.ToString();
        //    MCHDBEntities DataContext = new MCHDBEntities();

        //    var firstUldId = DataContext.ULDs.Select(u => u.Id).FirstOrDefault();

        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        TransactionStatus status = unit.EditUld(firstUldId, 2, 105, 1, serialNo, "AA", 105);
        //        if (status.Status == true)
        //        {
        //            var uld = DataContext.ULDs.First(u => u.Id == firstUldId);
        //            Assert.IsTrue(uld.SerialNumber == serialNo);
        //        }
        //    }
        //}

        //[TestMethod]
        //public void AddAndDeleteUldTest()
        //{
        //    MCHDBEntities DataContext = new MCHDBEntities();

        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        string serialNo = "Test" + DateTime.Now.Ticks.ToString();
        //        TransactionStatus status = unit.AddUld(2, 105, 1, serialNo, "AA", 106);

        //        if (status.Status == true)
        //        {
        //            Assert.IsTrue(DataContext.ULDs.Any(s => s.SerialNumber == serialNo && s.FlightManifestId == 105));
        //            var uld = DataContext.ULDs.Single(s => s.SerialNumber == serialNo && s.FlightManifestId == 105);
        //            Assert.IsTrue(DataContext.Transactions.Any(s => s.EntityId == uld.Id && s.EntityTypeId == 4 && s.Description == "ULD Added" && s.TaskId == 106));

        //            status = unit.DeleteUld(2, 105, uld.Id, 106);
        //            Assert.IsFalse(DataContext.ULDs.Any(s => s.Id == uld.Id));
        //        }
        //    }
        //}

        //[TestMethod]
        //public void GetShipmentsTest()
        //{
        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        var shipoments = unit.GetShipments(2, 105, 106, enumBuildFreightStatusTypes.All);
        //    }
        //}

        //[TestMethod]
        //public void SwitchBUPModeTest()
        //{
        //    MCHDBEntities DataContext = new MCHDBEntities();
        //    var firstUld = DataContext.ULDs.FirstOrDefault();

        //    if (firstUld != null)
        //    {
        //        bool isBUP = firstUld.IsBUP ?? false;
        //        long id = firstUld.Id;
        //        using (BuildFreightUnit unit = new BuildFreightUnit())
        //        {
        //            TransactionStatus status = unit.SwitchBUPMode(105,993, 2, 105);

        //            if (status.Status == true)
        //            {
        //                DataContext = new MCHDBEntities();
        //                var uld = DataContext.ULDs.First(u => u.Id == id);
        //                //Assert.AreNotEqual(isBUP, uld.IsBUP);
        //            }
        //        }
        //    }

        //}

        //[TestMethod]
        //public void FinalizeBuildTest()
        //{
        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        TransactionStatus status = unit.FinalizeBuild(105, 105, 2);
        //        Assert.AreEqual(true, status.Status);
        //    }
        //}

        //[TestMethod]
        //public void UpdateBuildULDWeightTest()
        //{
        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        MCHDBEntities DataContext = new MCHDBEntities();
        //        var firstUld = DataContext.ULDs.FirstOrDefault(u => u.FlightManifestId == 105);


        //        if (firstUld != null)
        //        {
        //            var id = firstUld.Id;
        //            TransactionStatus status = unit.UpdateBuildULDWeight(2, 105, 105, id, 100, 10, "LB");
        //            Assert.AreEqual(true, status.Status);

        //            DataContext = new MCHDBEntities();
        //            firstUld = DataContext.ULDs.First(u => u.Id == id);
        //            Assert.AreEqual(100, firstUld.TotalWeight);
        //            Assert.AreEqual(10, firstUld.TareWeight);
        //        }


        //    }
        //}
        //[TestMethod]
        //public void StageBuildULDTest()
        //{
        //    using (BuildFreightUnit unit = new BuildFreightUnit())
        //    {
        //        MCHDBEntities DataContext = new MCHDBEntities();
        //        var firstUld = DataContext.ULDs.FirstOrDefault(u => u.FlightManifestId == 105);

        //        if (firstUld != null)
        //        {
        //            var id = firstUld.Id;
        //            TransactionStatus status = unit.StageBuildULD(id, 2, 105, 2);
        //            Assert.AreEqual(true, status.Status);

        //            DataContext = new MCHDBEntities();
        //            firstUld = DataContext.ULDs.First(u => u.Id == id);
        //            Assert.AreEqual(2, firstUld.LocationId);
        //        }
        //    }
        //}

        [TestMethod]
        public void RemovePiecesFromUldTest()
        {
            using (BuildFreightUnit unit = new BuildFreightUnit())
            {
                MCHDBEntities DataContext = new MCHDBEntities();
                var det = DataContext.ManifestAWBDetails.Single(d => d.Id == 1121);
                int? count = det.LoadCount;
                TransactionStatus status = unit.RemovePiecesFromUld(2, 498, 1121, 1);
                Assert.AreEqual(true, status.Status);

                DataContext = new MCHDBEntities();
                det = DataContext.ManifestAWBDetails.Single(d => d.Id == 1121);
                Assert.AreEqual(count - 1, det.LoadCount);
            }
        }

        [TestMethod]
        public void GetForkliftCount()
        {
            using (CommonUnit unit = new CommonUnit())
            {
                int? status = unit.GetForkliftCount(105,2);
            }
        }
    }
}
