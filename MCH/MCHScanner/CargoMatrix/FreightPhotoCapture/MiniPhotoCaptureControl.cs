﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using System.Threading;
using System.Data.SqlServerCe;
using OpenNETCF.Drawing.Imaging;

namespace CargoMatrix.FreightPhotoCapture
{

    public partial class MiniPhotoCaptureControl : UserControl
    {
        Thread m_thread;
        public string HouseBill;
        public event EventHandler DataChangedEvent;
        public Reasons m_parent;
        public event ExitEventHandler ExitNotify;
        public string HeaderName;
        public ImageReadNotifyHandler ImageReadNotifyEvent;

        private CMXCameraBase cmxCameraControl1;


        string imgConnectionString;// = "Data Source =  \\Program Files\\cargomatrixscanner\\ImagesDB.sdf; Password =''";

        public MiniPhotoCaptureControl()
        {
            InitializeComponent();
            this.ImageReadNotifyEvent += new ImageReadNotifyHandler(ImageReadNotify);

            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            imgConnectionString = "Data Source =  " + path + "\\ImagesDB.sdf; Password =''";

            using (Bitmap bmp = new Bitmap(global::Resources.Graphics.Skin.thumbnailBackColor))
            {
                horizontalSmoothListbox1.BackColor = bmp.GetPixel(0, 0);
            }
        }

        CustomListItems.Thumbnail selectedThumbnail;

        private void ImageReadNotify(System.IO.MemoryStream stream)
        {
            try
            {
                if (stream == null)
                {
                    if (selectedThumbnail != null)
                    {
                        horizontalSmoothListbox1.Reset();
                    }
                    EnableButtons(false);

                }
                else
                {
                    selectedThumbnail = new CustomListItems.Thumbnail();

                    IBitmapImage imageBitmap = CreateThumbnail(stream, new Size(64, 48));
                    selectedThumbnail.image = ImageUtils.IBitmapImageToBitmap(imageBitmap);


                    //selectedThumbnail.Time = DateTime.Now;
                    //selectedThumbnail.image = new Bitmap(image);
                    horizontalSmoothListbox1.AddItem(selectedThumbnail);
                    selectedThumbnail.SelectedChanged(true);
                    horizontalSmoothListbox1.MoveListToEnd();

                    EnableButtons(true);
                    cameraTimer.Enabled = true;
                    horizontalSmoothListbox1.Refresh();
                    Cursor.Current = Cursors.Default;
                    if (m_parent != null)
                    {
                        m_parent.HasDataChanged = true;
                    }

                    if (DataChangedEvent != null)
                        DataChangedEvent(this, EventArgs.Empty);

                    using (System.Data.SqlServerCe.SqlCeConnection conn = new System.Data.SqlServerCe.SqlCeConnection(imgConnectionString))
                    {
                        conn.Open();
                        using (SqlCeCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = "INSERT INTO Images ([UserID], [HousebillNo], [GUID], [Image], [Reason], [TimeStamp] ) Values(@userID, @housebill, @guid, @pic, @reason, @time )";

                            SqlCeParameter userID = new SqlCeParameter("@userID", SqlDbType.NVarChar);
                            userID.Value = CargoMatrix.Communication.WebServiceManager.GetUserID();
                            cmd.Parameters.Add(userID);

                            SqlCeParameter housebill = new SqlCeParameter("@housebill", SqlDbType.NVarChar);
                            if (m_parent != null)
                            {
                                if (m_parent.PhotoCaptureData != null)
                                {
                                    housebill.Value = m_parent.PhotoCaptureData.reference;
                                }
                            }
                            cmd.Parameters.Add(housebill);

                            SqlCeParameter guid = new SqlCeParameter("@guid", SqlDbType.UniqueIdentifier);
                            selectedThumbnail.GUID = Guid.NewGuid();
                            guid.Value = selectedThumbnail.GUID;
                            cmd.Parameters.Add(guid);

                            SqlCeParameter pic = new SqlCeParameter("@pic", SqlDbType.Image);
                            pic.Value = stream.ToArray();
                            cmd.Parameters.Add(pic);

                            SqlCeParameter reason = new SqlCeParameter("@reason", SqlDbType.NVarChar);
                            reason.Value = "";
                            cmd.Parameters.Add(reason);

                            SqlCeParameter time = new SqlCeParameter("@time", SqlDbType.DateTime);
                            selectedThumbnail.Time = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");
                            time.Value = selectedThumbnail.Time;
                            cmd.Parameters.Add(time);

                            //cmd.CommandText = "CREATE TABLE employees ( id integer NOT NULL PRIMARY KEY AUTO_INCREMENT, fname VARCHAR(255))";



                            cmd.ExecuteNonQuery();
                        }
                        //conn.Close();
                        //conn.Dispose();
                        //cmd.Dispose();

                    }

                }
            }
            catch (System.OutOfMemoryException e)
            {
                selectedThumbnail.Dispose();
                cmxCameraControl1.Enabled = false;
                CargoMatrix.UI.CMXMessageBox.Show("Application is out of memory, unable to take new pictures, if you are in the middle of a task then select complete task or complete task now from options menu to save images.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                //CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 40102);
                cmxCameraControl1.Enabled = true;

            }
            catch (Exception e)
            {
                if (selectedThumbnail != null)
                {
                    selectedThumbnail.Dispose();
                    selectedThumbnail = null;
                }

                cmxCameraControl1.Enabled = false;
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 40002);
                cmxCameraControl1.Enabled = true;
            }

        }


        void UpdateImage()
        {
            this.Invoke(new EventHandler(WorkerUpdateImage));

        }

        public void WorkerUpdateImage(object sender, EventArgs e)
        {
            try
            {
                if (m_parent != null)
                {
                    if (m_parent.PhotoCaptureData != null)
                    {
                        CargoMatrix.Communication.DTO.ImageObject imgObj = CargoMatrix.Communication.WebServiceManager.Instance().GetFullImage(m_parent.PhotoCaptureData.reference, selectedThumbnail.ID);
                        if (imgObj.m_rawImage != null)
                        {
                            using (System.IO.MemoryStream stream = new System.IO.MemoryStream(imgObj.m_rawImage))
                            {
                                if (cmxCameraControl1.image != null)
                                {
                                    //cmxCameraControl1.image.Dispose();
                                    cmxCameraControl1.image = null; // this will dispose the image too
                                }
                                cmxCameraControl1.image = new Bitmap(stream);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (10016)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);                
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30013);

                if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn() == false)
                {
                    cmxCameraControl1.Enabled = false;

                }
            }



        }

        private void horizontalSmoothListbox1_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (isSelected)
            {
                selectedThumbnail = listItem as CustomListItems.Thumbnail;
                cmxCameraControl1.StopAcquisition();
                //cmxCameraControl1.Enabled = false;

                cmxCameraControl1.image = selectedThumbnail.image;
                cmxCameraControl1.Update();
                if (selectedThumbnail.ID > 0)
                {
                    ThreadStart starter = new ThreadStart(UpdateImage);
                    m_thread = new Thread(starter);
                    m_thread.Priority = ThreadPriority.Normal;//.BelowNormal;
                    //m_thread.IsBackground = true;

                    m_thread.Start();
                }
                else
                {

                    byte[] img = ImagesDBManager.GetImageFromDB(selectedThumbnail.GUID);
                    if (img != null)
                    {
                        using (System.IO.MemoryStream fs = new System.IO.MemoryStream(img))
                        {
                            if (cmxCameraControl1.image != null)
                            {
                                //    cmxCameraControl1.image.Dispose();
                                cmxCameraControl1.image = null;
                            }
                            IBitmapImage imageBitmap = CreateThumbnail(fs, new Size(640, 480));
                            cmxCameraControl1.image = ImageUtils.IBitmapImageToBitmap(imageBitmap);

                        }
                    }

                }


                cameraTimer.Enabled = false;
                EnableButtons(true);
            }
            else
            {
                StartCamera();
            }
        }

        public bool LeftButtonPressed
        {
            set
            {
                horizontalSmoothListbox1.leftButtonPressed = value;
            }

        }

        public bool RightButtonPressed
        {
            set
            {
                horizontalSmoothListbox1.rightButtonPressed = value;
            }

        }


        protected virtual void buttonExit_Click(object sender, EventArgs e)
        {
            buttonExit.Refresh();
            //CargoMatrix.UI.CMXAnimationmanager.ExitForm();
            cameraTimer.Enabled = false;
            cmxCameraControl1.StopAcquisition();
            //cmxCameraControl1.Dispose();
            if (m_thread != null)
            {
                m_thread.Abort();
                m_thread = null;

            }
            if (ExitNotify != null)
                ExitNotify(this);
        }

        private void EnableButtons(bool enable)
        {
            buttonDelete.Enabled = buttonZoom.Enabled = enable;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to delete this picture ?", "Confirm Delete", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Cancel))
                return;

            if (selectedThumbnail != null)
            {
                if (selectedThumbnail.ID == -1)
                {
                    ImagesDBManager.RemoveImageFromDB(selectedThumbnail.GUID);
                    horizontalSmoothListbox1.RemoveItem(selectedThumbnail);
                    selectedThumbnail.image.Dispose();
                    selectedThumbnail.image = null;
                    selectedThumbnail.Dispose();
                    selectedThumbnail = null;

                }
                else
                {

                    if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID != CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                    {
                        this.Enabled = false;
                        if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(this, "Delete Picture") == DialogResult.OK)
                            DeleteImage();

                    }
                    else
                        DeleteImage();

                }


                horizontalSmoothListbox1.MoveListToEnd();

                if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn())
                    StartCamera();

            }
        }

        private void DeleteImage()
        {
            if (selectedThumbnail.image != null)
            {
                selectedThumbnail.image.Dispose();
                selectedThumbnail.image = null;
            }

            selectedThumbnail.Visible = false;
            selectedThumbnail.Width = selectedThumbnail.Height = 0;

            if (m_parent != null)
            {
                m_parent.HasDataChanged = true;
            }
            if (DataChangedEvent != null)
                DataChangedEvent(this, EventArgs.Empty);
        }

        private void buttonNote_Click(object sender, EventArgs e)
        {
            CargoMatrix.UI.CMXAnimationmanager.GoBack();
        }

        private void buttonZoom_Click(object sender, EventArgs e)
        {
            buttonZoom.Refresh();
            GC.Collect();
            cmxCameraControl1.FullScreen(true);

        }
        public void StartCamera()
        {
            UI.BarcodeReader.TermReader();
            this.Enabled = true;
            if (cmxCameraControl1 != null)
            {
                cmxCameraControl1.StartAcquisition();
            }
        }

        public void StopCamera()
        {
            if (cmxCameraControl1 != null)
            {
                cmxCameraControl1.Enabled = false;
                cmxCameraControl1.StopAcquisition();
            }
        }

        public bool HasImage
        {
            get
            {
                return this.Image != null;
            }
        }

        public Image Image
        {
            get
            {
                if (horizontalSmoothListbox1.Items.Count > 0)
                    for (int i = 0; i < horizontalSmoothListbox1.Items.Count; i++)
                    {
                        if ((horizontalSmoothListbox1.Items[i] as CustomListItems.Thumbnail).image != null)
                            return (horizontalSmoothListbox1.Items[i] as CustomListItems.Thumbnail).image;
                    }

                return null;
            }

        }

        public List<ImageObject> UploadImageList
        {
            get
            {
                List<ImageObject> list;
                System.IO.MemoryStream imageStream;// = new System.IO.MemoryStream();
                if (horizontalSmoothListbox1.Items.Count > 0)
                {
                    list = new List<ImageObject>();
                    foreach (CustomListItems.Thumbnail temp in horizontalSmoothListbox1.Items)
                    {

                        try
                        {
                            //GC.Collect();
                            imageStream = new System.IO.MemoryStream();

                            //if(temp.image != null)
                            //    temp.image.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                            if (temp.ID == -1)
                            {
                                byte[] img = ImagesDBManager.GetImageFromDB(temp.GUID);

                                imageStream = new System.IO.MemoryStream(img);
                                list.Add(new ImageObject(imageStream.ToArray(), temp.Time, null, null, null, temp.ID));
                                imageStream.Close();
                            }
                            else
                            {
                                if (temp.image == null)
                                {
                                    imageStream = new System.IO.MemoryStream();
                                    list.Add(new ImageObject(imageStream.ToArray(), temp.Time, null, null, null, temp.ID));
                                    imageStream.Close();
                                }

                            }
                            //imageStream.Close();
                        }
                        catch (Exception e)
                        {
                            //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30004)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30004);
                        }

                    }
                    return list;

                }
                return null;
            }

        }
        public List<ImageObject> GetImageListAndDispose
        {
            get
            {
                List<ImageObject> list;
                System.IO.MemoryStream imageStream;// = new System.IO.MemoryStream();
                if (horizontalSmoothListbox1.Items.Count > 0)
                {
                    list = new List<ImageObject>();
                    foreach (CustomListItems.Thumbnail temp in horizontalSmoothListbox1.Items)
                    {

                        try
                        {
                            GC.Collect();

                            if (temp.image != null)
                            {
                                imageStream = new System.IO.MemoryStream();
                                temp.image.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                list.Add(new ImageObject(imageStream.ToArray(), temp.Time, null, null, null, temp.ID));
                                imageStream.Close();
                            }
                            else
                                list.Add(new ImageObject(null, temp.Time, null, null, null, temp.ID));
                        }
                        catch (Exception e)
                        {
                            //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30005)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30005);
                        }

                        horizontalSmoothListbox1.RemoveItem(temp);
                        temp.Dispose();
                        //temp = null;

                    }

                    return list;

                }
                return null;
            }

        }
        public List<ImageObject> ImageList
        {
            get
            {
                List<ImageObject> list;
                System.IO.MemoryStream imageStream;// = new System.IO.MemoryStream();
                if (horizontalSmoothListbox1.Items.Count > 0)
                {
                    list = new List<ImageObject>();
                    GC.Collect();
                    foreach (CustomListItems.Thumbnail temp in horizontalSmoothListbox1.Items)
                    {

                        try
                        {


                            if (temp.image != null)
                            {
                                imageStream = new System.IO.MemoryStream();
                                temp.image.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                list.Add(new ImageObject(imageStream.ToArray(), temp.Time, null, null, null, temp.ID));
                                imageStream.Close();
                            }
                            else
                                list.Add(new ImageObject(null, temp.Time, null, null, null, temp.ID));
                        }
                        catch (Exception e)
                        {
                            //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30006)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30006);
                        }

                    }
                    return list;

                }
                return null;
            }
            set
            {
                horizontalSmoothListbox1.RemoveAll();
                if (value != null)
                {
                    for (int i = 0; i < value.Count; i++)
                    {
                        horizontalSmoothListbox1.AddItem(new CustomListItems.Thumbnail(value[i]));
                    }

                    horizontalSmoothListbox1.MoveListToEnd();
                    horizontalSmoothListbox1.Reset();
                    //horizontalSmoothListbox1.LayoutItems();

                    //horizontalSmoothListbox1.Refresh();

                }

            }

        }

        private void cameraTimer_Tick(object sender, EventArgs e)
        {
            cameraTimer.Enabled = false;
            StartCamera();
        }

        public string Title
        {
            set { HeaderName = value; }
        }


        Font titleFont = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);
        private void labelTitle_Paint(object sender, PaintEventArgs e)
        {
            SizeF textSize = e.Graphics.MeasureString(HeaderName, titleFont);
            int x = (labelTitle.Width - (int)textSize.Width) / 2;
            int y = (labelTitle.Height - (int)textSize.Height) / 2;
            e.Graphics.DrawString(HeaderName, titleFont, new SolidBrush(Color.White), x, y);
        }

        public Color FilmBackColor
        {
            set
            {
                horizontalSmoothListbox1.BackColor = value;
            }
        }

        private void MiniPhotoCaptureControl_EnabledChanged(object sender, EventArgs e)
        {
            if (this.Enabled)
            {
                if (cmxCameraControl1 != null)
                    return;
                this.cmxCameraControl1 = new CMXColorCameraControl(m_parent.ColorCameraResolution);

                this.SuspendLayout();
                cmxCameraControl1.ImageReadNotify += ImageReadNotifyEvent;

                this.cmxCameraControl1.BackColor = System.Drawing.Color.White;
                this.cmxCameraControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.cmxCameraControl1.Name = "cmxCameraControl1";
                this.cmxCameraControl1.Bounds = panel2.Bounds;
                this.cmxCameraControl1.TabIndex = 9;

                this.Controls.Add(this.cmxCameraControl1);
                cmxCameraControl1.BringToFront();
                this.ResumeLayout();
            }
            else
            {
                if (m_thread != null)
                {
                    m_thread.Abort();
                    m_thread = null;

                }

                if (cmxCameraControl1 != null)
                {

                    cmxCameraControl1.Enabled = false;
                    cmxCameraControl1.ImageReadNotify -= ImageReadNotifyEvent;
                    if (Controls.Contains(cmxCameraControl1))
                        Controls.Remove(cmxCameraControl1);

                    cmxCameraControl1.Dispose();
                    cmxCameraControl1 = null;

                }

            }
        }

        private IBitmapImage CreateThumbnail(System.IO.Stream stream, Size size)
        {
            IBitmapImage imageBitmap;
            ImageInfo ii;
            IImage image;

            ImagingFactory factory = new ImagingFactoryClass();
            factory.CreateImageFromStream(new StreamOnFile(stream), out image);
            image.GetImageInfo(out ii);
            factory.CreateBitmapFromImage(image, (uint)size.Width, (uint)size.Height,
          ii.PixelFormat, InterpolationHint.InterpolationHintDefault, out imageBitmap);
            return imageBitmap;
        }

        public void SetColorCameraResolution()
        {
            if (cmxCameraControl1 != null)
            {
                if (cmxCameraControl1 is CMXColorCameraControl)
                {
                    if (m_parent != null)
                    {
                        // m_parent.ColorCameraResolution = resolution;
                        (cmxCameraControl1 as CMXColorCameraControl).CameraResolution = m_parent.ColorCameraResolution;
                    }
                }
            }
        }

    }
}
