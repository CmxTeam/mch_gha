﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CargoMatrix.FreightPhotoCapture
{
    public delegate void ExitEventHandler(Control sender);
   // public delegate void CameraExitEventHandler(MiniPhotoCaptureControl cameraControl);


    public partial class PhotoCaptureForm : SmoothForm// Form
    {
        
        string m_guid;
        string m_taskID;
        int m_timeout;
        string m_userID;
        string m_userType;
        
        PhotoCaptureControl photoCaptureControl;
        
        TaskList photoCaptureTaskList;
        public PhotoCaptureForm()
        {


            InitFormNotify += new InitForm(PhotoCaptureForm_InitFormNotify);
            
            SplashScreenLogo = PhotoCaptureResource.splash_screen_logo;


            


        }
        public PhotoCaptureForm(string guid, string userID, string userType, string taskID, int timeout)
        {
            InitFormNotify += new InitForm(PhotoCaptureForm_InitFormNotify);
            SplashScreenLogo = PhotoCaptureResource.splash_screen_logo;
            m_guid = guid;
            m_taskID = taskID;
            m_timeout = timeout;
            m_userID = userID;
            m_userType = userType;
        
        }



        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (loadSplash)
                base.OnPaintBackground(e);
        }

        private void buttonCaptureSave_Click(object sender, EventArgs e)
        {
            
        }

        
        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to exit?", "Confirm Exit", CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.Cancel))
                return;

            photoCaptureControl.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;
            CargoMatrix.UI.CMXAnimationmanager.ExitForm();
        }

        private void PhotoCaptureForm_InitFormNotify()
        {
            InitializeComponent();
            //hasFooter = false;
            CargoMatrix.Communication.GuidManager.SetGuid(m_guid, m_timeout);
            CargoMatrix.Communication.WebServiceManager.SetUserID(m_userID, m_userType);
            if (photoCaptureTaskList == null)
                photoCaptureTaskList = new TaskList();
            photoCaptureTaskList.Location = new Point(Left, HeaderHeight);
            photoCaptureTaskList.Size = new Size(Width, Height - HeaderHeight - FooterHeight);
            
            SetDefaultForm(photoCaptureTaskList);
            DisplayForm2(photoCaptureTaskList);

            photoCaptureTaskList.ListItemClicked += new SmoothListBox.UI.ListItemClickedHandler(photoCaptureTaskList_ListItemClicked);
            
            this.EndSplashScreen();
        }

        void photoCaptureTaskList_ListItemClicked(SmoothListBox.UI.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            //throw new NotImplementedException();
        }

        void reasonsControl_UploadPhotoCaptureNotify(CargoMatrix.Communication.Data.FreightPhotoCapture PhotoCaptureData)
        {
            //throw new NotImplementedException();
        }

        void  Control_ExitNotify(Control sender)
        {
 	        //throw new NotImplementedException();
            CargoMatrix.UI.CMXAnimationmanager.ExitForm();
        }

        void reasonsControl_LoadCameraNotify()
        {
            //throw new NotImplementedException();
            if (photoCaptureControl == null)
            {
                photoCaptureControl = new PhotoCaptureControl();
                photoCaptureControl.ExitNotify +=new ExitEventHandler(Control_ExitNotify);
            }
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(photoCaptureControl);
        }

        //private void PhotoCaptureForm_Paint(object sender, PaintEventArgs e)
        //{

        //}
        

      

       
    }
}