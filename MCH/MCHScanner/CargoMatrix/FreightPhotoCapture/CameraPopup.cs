﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class CameraPopup : Form
    {
        public string Title { get; set; }

              
        public CameraPopup(string title, string line2, string line3)
        {
            InitializeComponent();

            this.buttonCancel.BackgroundImage = CargoMatrix.Resources.Skin.button_default; //Resources.Graphics.Skin.nav_cancel;
            this.buttonCancel.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;//   Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.BackColor = Color.White;
            this.buttonCancel.Click += new EventHandler(buttonCancel_Click);

            this.buttonOk.BackgroundImage = CargoMatrix.Resources.Skin.button_default; //Resources.Graphics.Skin.nav_ok;
            this.buttonOk.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;
            this.buttonOk.Text = "OK";
            this.buttonCancel.BackColor = Color.White;
            this.buttonOk.Click += new EventHandler(buttonOk_Click);

            this.buttonClear.BackgroundImage = CargoMatrix.Resources.Skin.button_default; //Resources.Graphics.Skin.nav_cancel;
            this.buttonClear.ActiveBackgroundImage = CargoMatrix.Resources.Skin.button_pressed;// Resources.Graphics.Skin.nav_cancel_over;
            this.buttonClear.Text = "Clear";
            this.buttonCancel.BackColor = Color.White;
            this.buttonClear.Click += new EventHandler(buttonClear_Click);

            this.Title = title;
            Line2 = line2;
            Line3 = line3;
            //buttonOk.Enabled = false;
            //buttonClear.Enabled = false;
        }

      
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                e.Graphics.DrawString(Title, font, new SolidBrush(Color.White), 5, 3);
            }
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }

       

      



        private void cameraControl_ImageReadNotify(System.IO.MemoryStream stream)
        {
            if (stream != null)
            {
                CameraImage = stream.ToArray();
                //buttonOk.Enabled = true;
               
                cameraControl.StopAcquisition();
                //buttonClear.Enabled = true;
                //cameraControl.StopAcquisition();
                //DialogResult = DialogResult.OK;
            }
            else
            {
                CameraImage = null;
                //buttonOk.Enabled = false;
                
            }
            Cursor.Current = Cursors.Default;
        }
        public byte[] CameraImage
        {
            get;
            private set;

        }
        public string Line2
        {
            set
            {
                label1.Text = value;
            }
        }
        public string Line3
        {
            set
            {
                label2.Text = value;
            }
        }
       
        
        private void CameraPopup_Load(object sender, EventArgs e)
        {
            cameraControl.StartAcquisition();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            cameraControl.StopAcquisition();
            CameraImage = null;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            cameraControl.StopAcquisition();

            this.DialogResult = DialogResult.OK;
                
            this.Close();
            
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            //buttonOk.Enabled = false;
            //buttonClear.Enabled = false;
            cameraControl.StartAcquisition();
            
        }
        
      
    }
}