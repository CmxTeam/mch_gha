﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class CMXColorCameraControl 
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            StopAcquisition();
            ///////////////////
            if (triggerList != null)
            {
                foreach (Symbol.ResourceCoordination.Trigger trigger
                                 in triggerList)
                {
                    if (trigger != null)
                        trigger.Dispose();
                }
                triggerList.Clear();
                triggerList = null;
            }
            if ( colorCamera1 != null)
            {
                colorCamera1.Dispose();
                colorCamera1 = null;
            }

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorCamera1 = new global::CMXCameraControl.ColorCamera();
            this.cameraPictureBox = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // colorCamera1
            // 
            this.colorCamera1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorCamera1.Location = new System.Drawing.Point(0, 0);
            this.colorCamera1.Name = "colorCamera1";
            this.colorCamera1.Size = new System.Drawing.Size(240, 180);
            this.colorCamera1.TabIndex = 0;
            // 
            // cameraPictureBox
            // 
            this.cameraPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraPictureBox.Location = new System.Drawing.Point(0, 0);
            this.cameraPictureBox.Name = "cameraPictureBox";
            this.cameraPictureBox.Size = new System.Drawing.Size(240, 180);
            this.cameraPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // CMXColorCameraControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.colorCamera1);
            this.Controls.Add(this.cameraPictureBox);
            this.Name = "CMXColorCameraControl";
            this.Size = new System.Drawing.Size(240, 180);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CMXCameraControl_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private global::CMXCameraControl.ColorCamera colorCamera1;
        private System.Windows.Forms.PictureBox cameraPictureBox;

    }
}
