﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightPhotoCapture
{
    public partial class CameraScreen : CargoMatrix.UI.CMXUserControl 
    {
       
        
        int m_previousHeight;
        public event ExitEventHandler CameraExitNotify;
        CargoMatrix.Communication.DTO.FreightPhotoCaptureItem photoCaptureData;
        public MiniPhotoCaptureControl miniPhotoCaptureControl;
        private bool hasImage;
        internal bool HasImage
        {
            get
            {
                if (miniPhotoCaptureControl != null)
                    return this.miniPhotoCaptureControl.HasImage;
                else 
                    return hasImage;
            }
            private set { hasImage = value; }

        }
        public Control threadSyncControl;

        //public ReasonsListItem()
        //{
        //    InitReasonsListItem();

        //}

        public CameraScreen()
        {
        
            InitReasonsListItem();
 
            photoCaptureData.reason = "????";



        }

        private void InitReasonsListItem()
        {
            InitializeComponent();
            m_previousHeight = Height;

            //if (m_reasonType == 1)
                //itemPicture.Image = PhotoCaptureResource.Notepad_Information;//.Notepad_Edit;
            //else
                //itemPicture.Image = PhotoCaptureResource.Notepad;

            photoCaptureData = new CargoMatrix.Communication.DTO.FreightPhotoCaptureItem();
            //this.FocusedColor = Color.WhiteSmoke;//Color.Khaki;//.LightYellow;

            //title.Width = pictureBoxCheck.Left - title.Left;

        }
        public void InsertOuterImage(Image image)
        {
            //itemPicture.Image = PhotoCaptureResource.Notepad_Check;
            if (pictureBoxCheck.Image != null)
            {
                pictureBoxCheck.Image.Dispose();
                pictureBoxCheck.Image = null;
            }
            pictureBoxCheck.Image = image;
            pictureBoxCheck.Visible = true;
            HasImage = true;
        }

        //public override void SelectedChanged(bool isSelected)
        //{
     

        //}

        void miniPhotoCaptureControl_ExitNotify(Control sender)
        {
            try
            {
                if (sender is MiniPhotoCaptureControl)
                {
                    List<CargoMatrix.Communication.DTO.ImageObject> imgList = (sender as MiniPhotoCaptureControl).ImageList;
                    if (imgList == null)
                    {
                        CloseCamera();
                        if (miniPhotoCaptureControl != null)
                        {
                            Controls.Remove(miniPhotoCaptureControl);
                            miniPhotoCaptureControl.Dispose();
                            miniPhotoCaptureControl = null;
                        }

                        if (pictureBoxCheck.Image != null)
                        {
                            this.pictureBoxCheck.Image.Dispose();
                            this.pictureBoxCheck.Image = null;
                        }
                        return;
                    }
                    else
                    {
                        if (imgList.Count == 0)
                        {
                            CloseCamera();
                            if (miniPhotoCaptureControl != null)
                            {
                                Controls.Remove(miniPhotoCaptureControl);
                                miniPhotoCaptureControl.Dispose();
                                miniPhotoCaptureControl = null;

                            }

                            if (pictureBoxCheck.Image != null)
                            {
                                this.pictureBoxCheck.Image.Dispose();
                                this.pictureBoxCheck.Image = null;
                            }
                            return;
                        }
                    }

                    if (pictureBoxCheck.Image != null)
                    {
                        this.pictureBoxCheck.Image.Dispose();
                        this.pictureBoxCheck.Image = null;
                    }
                    //base.SelectedChanged(false);

                    //if (m_reasonType == 1)
                    //    itemPicture.Image = PhotoCaptureResource.Notepad_Information;
                    //else
                    //    itemPicture.Image = PhotoCaptureResource.Notepad;

                    if (imgList != null)
                    {
                        if (imgList.Count > 0)
                        {
                            //this reason has images
                            GC.Collect();

                            for (int i = 0; i < imgList.Count; i++)
                            {
                                if (imgList[i].m_rawImage != null)
                                {
                                    InsertOuterImage(new Bitmap(new System.IO.MemoryStream(imgList[i].m_rawImage)));
                                    break;
                                }
                            }


                        }

                    }

                    CloseCamera();

                }

            }
            catch (Exception e)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(e.Message, "Error!" + " (30007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 30007);

            }
        }
        public void CloseCamera()
        {
            Height = m_previousHeight;
            miniPhotoCaptureControl.Enabled = false;
            miniPhotoCaptureControl.Visible = false;
            //title.Visible = true;
            //itemPicture.Visible = true;
            pictureBoxCheck.Visible = true;
            
            if(!HasImage)
            {
                //if (m_reasonType == 1)
                //    itemPicture.Image = PhotoCaptureResource.Notepad_Information;
                //else
                //    itemPicture.Image = PhotoCaptureResource.Notepad;
            }

            if (CameraExitNotify != null)
            {

                CameraExitNotify(this);
            }

        }

        public void ShowCamera()
        {
            UI.BarcodeReader.TermReader();
            SuspendLayout();
            if (miniPhotoCaptureControl == null)
            {

                miniPhotoCaptureControl = new MiniPhotoCaptureControl();
                //miniPhotoCaptureControl.FilmBackColor = m_parent.thumbnailBackColor;
                //miniPhotoCaptureControl.m_parent = m_parent;

                miniPhotoCaptureControl.ExitNotify += new ExitEventHandler(miniPhotoCaptureControl_ExitNotify);
                miniPhotoCaptureControl.Location = new Point(0, 0);
                this.Controls.Add(miniPhotoCaptureControl);
                //int index = m_parent.MainControlIndex(this);

                //if (index >= 0)
                //    miniPhotoCaptureControl.ImageList = m_parent.PhotoCaptureData.itemsList[index].imagelist;



            }


            miniPhotoCaptureControl.Visible = true;

            //title.Visible = false;
            //itemPicture.Visible = false;
            pictureBoxCheck.Visible = false;

            this.Height = miniPhotoCaptureControl.Height + m_previousHeight;
            miniPhotoCaptureControl.Title = "??????????";
            //m_parent.LayoutItems();
            //m_parent.smoothListBoxMainList.MoveControlToTop(this);

            ResumeLayout(true);
            Refresh();

            miniPhotoCaptureControl.Enabled = false;
            miniPhotoCaptureControl.Enabled = true;


        }
        public List<CargoMatrix.Communication.DTO.ImageObject> GetImageList()
        {
            if (miniPhotoCaptureControl == null)
                return null;
            return miniPhotoCaptureControl.ImageList;

        }

        public List<CargoMatrix.Communication.DTO.ImageObject> GetUploadImageList()
        {
            if (miniPhotoCaptureControl == null)
                return null;
            return miniPhotoCaptureControl.UploadImageList;

        }
        public void GoBack()
        {
            if (miniPhotoCaptureControl != null)
                miniPhotoCaptureControl.cameraTimer.Enabled = false;

            miniPhotoCaptureControl_ExitNotify(miniPhotoCaptureControl);
        }
        public void StopCameraTimer()
        {
            if (miniPhotoCaptureControl != null)
                miniPhotoCaptureControl.cameraTimer.Enabled = false;
        }
    }
}
