﻿namespace CargoMatrix.FreightPhotoCapture
{
    partial class MicroReasons
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.smoothListBoxReasons = new SmoothListbox.SmoothListBoxBase();
            this.SuspendLayout();
            // 
            // smoothListBoxReasons
            // 
            this.smoothListBoxReasons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.smoothListBoxReasons.Location = new System.Drawing.Point(0, 0);
            this.smoothListBoxReasons.Name = "smoothListBoxReasons";
            this.smoothListBoxReasons.Size = new System.Drawing.Size(240, 200);
            this.smoothListBoxReasons.TabIndex = 0;
            this.smoothListBoxReasons.ListItemClicked += new SmoothListbox.ListItemClickedHandler(this.smoothListBoxReasons_ListItemClicked);
            // 
            // MicroReasons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.smoothListBoxReasons);
            this.Name = "MicroReasons";
            this.Size = new System.Drawing.Size(240, 200);
            this.ResumeLayout(false);

        }

        #endregion

        public SmoothListbox.SmoothListBoxBase smoothListBoxReasons;




    }
}
