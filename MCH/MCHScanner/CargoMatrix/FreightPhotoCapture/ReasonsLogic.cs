﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.FreightPhotoCapture
{
    public class ReasonsLogic
    {


        public static bool CreateNewTask(string carrier, string actualBill, CargoMatrix.Communication.DTO.TaskType taskType)
        {
            try
            {
                CargoMatrix.Communication.DTO.FreightPhotoCapture tempPhotoCapture = null;

                int taskStatus = 0;
                //bool isBillExists = false;
                switch (taskType)
                {
                    case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
                        taskStatus = GetHouseBillTask(actualBill, out tempPhotoCapture);
                        //isBillExists = CargoMatrix.Communication.WebServiceManager.Instance().HouseBillExists(actualBill);
                        break;
                    case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL:
                        taskStatus = GetMasterBillTask(carrier, actualBill, out tempPhotoCapture);
                        //isBillExists = CargoMatrix.Communication.WebServiceManager.Instance().MasterBillExists(carrier, actualBill);
                        break;
                }

                if (tempPhotoCapture == null)
                    return false;

                if (taskStatus != 0)
                //if(isBillExists == true)
                {
                    if (tempPhotoCapture.taskID == null)
                    {
                         
                        return false;
                    }
                    if (tempPhotoCapture.statusCode == 'C' && taskStatus == 1) // completed task
                    {

                    }
                    else if (tempPhotoCapture.statusCode == 'N' && taskStatus == 3) // task exists but not assigned to anyone
                    {
                        CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(tempPhotoCapture.taskID);

                    }
                    else if ((tempPhotoCapture.statusCode == 'I' || tempPhotoCapture.statusCode == 'N') && taskStatus == 2) // in progress or not started task assigned to another user.
                    {
      
                    }
                    else if (tempPhotoCapture.statusCode == 'C' && taskStatus == 2) // task already completed by another user
                    {
                        //CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewFPCTask(housebill.ToUpper(), ref tempPhotoCapture);
                        switch (taskType)
                        {
                            case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
                                CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill.ToUpper(), tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture);
                                break;
                            case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL:
                                CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewMasterBillTask(carrier, actualBill.ToUpper(), tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture);
                                break;
                        }



                    }
                    else if (taskStatus != 1)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("An Unknown Error has occured", "Error!" + " (30009)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return false;
                    }



                }
                else  // task does not exist, check if HB exists
                {

                    //string origin, destination;
                    CargoMatrix.Communication.DTO.FreightPhotoCapture photoCaptureData = new CargoMatrix.Communication.DTO.FreightPhotoCapture();

                    bool result = false;
                    if (taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
                    {
                        result = CargoMatrix.Communication.WebServiceManager.Instance().HouseBillExists(actualBill);
                    }
                    else if (taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
                    {
                        result = CargoMatrix.Communication.WebServiceManager.Instance().MasterBillExists(carrier, actualBill);

                    }
                    if (result == false) // housebill and msterbill exists so assuming that the origing and destination will get auto assigned during create task
                    {
                        string origin, dest;
                        if (CargoMatrix.Utilities.OriginDestinationMessageBox.Show(out origin, out dest) == DialogResult.OK)
                        {
                            tempPhotoCapture.origin = origin;
                            tempPhotoCapture.destination = dest;

                        }
                        else
                            return false;
                    }
                    //if (result == true)
                    //{


                    string reference = null;
                    //if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
                    //{
                    //    //if (CargoMatrix.Communication.WebServiceManager.Instance().GetHouseBill(actualBill, out reference, out desc, out loc) != 1)
                    //    if(CargoMatrix.Communication.WebServiceManager.Instance().GetLastFPCTask(actualBill,out tempPhotoCapture))
                    //        return false;
                    //}
                    //else if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
                    //{ 

                    //}

                    if (taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
                    {
                        CargoMatrix.Communication.ScannerUtilityWS.HouseBillItem h = CargoMatrix.Communication.ScannerUtility.Instance.GetHouseBillByNumber(actualBill);

                        // tempPhotoCapture.origin, tempPhotoCapture.destination
                        if (CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill, h.Origin, h.Destination, ref tempPhotoCapture) == true)
                        {
                            GetHouseBillTask(actualBill, out tempPhotoCapture);
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30010)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            return false;

                        }
                    }
                    else if (taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
                    {
                        if (CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewMasterBillTask(carrier, actualBill, tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture) == true)
                        {
                            GetMasterBillTask(carrier, actualBill, out tempPhotoCapture);
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30019)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            return false;

                        }

                    }


                    //CargoMatrix.Communication.WebServiceManager.Instance().TaskInProgress(reasonsControl.PhotoCaptureData.taskID);
                    //}





                }

                if (tempPhotoCapture.taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
                {
                    /// origin or destination are null, empty or "???"
                    if (string.IsNullOrEmpty(tempPhotoCapture.origin) || string.IsNullOrEmpty(tempPhotoCapture.destination) || tempPhotoCapture.origin == "???" || tempPhotoCapture.destination == "???")
                    {
                        string origin, dest;
                        if (CargoMatrix.Utilities.OriginDestinationMessageBox.Show(out origin, out dest) == DialogResult.OK)
                        {
                            tempPhotoCapture.origin = origin.ToUpper();
                            tempPhotoCapture.destination = dest.ToUpper();
                            if (CargoMatrix.Communication.WebServiceManager.Instance().RenameReference(tempPhotoCapture.actualBill, origin, dest))
                                tempPhotoCapture.reference = string.Format("{0}-{1}-{2}", tempPhotoCapture.origin, tempPhotoCapture.actualBill, tempPhotoCapture.destination);
                            else
                                CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30010)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30010)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            return false;

                        }
                    }
                }

             
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30020);
                return false;
            }
            return true;
        }


        public static bool CreateTask(string carrier, string actualBill, CargoMatrix.Communication.DTO.TaskType taskType, UserControl control, ref Reasons reasonsControl)
        {
            try
            {
                CargoMatrix.Communication.DTO.FreightPhotoCapture tempPhotoCapture = null;

                int taskStatus = 0;
                //bool isBillExists = false;
                switch (taskType)
                {
                    case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
                        taskStatus = GetHouseBillTask(actualBill, out tempPhotoCapture);
                        //isBillExists = CargoMatrix.Communication.WebServiceManager.Instance().HouseBillExists(actualBill);
                        break;
                    case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL:
                        taskStatus = GetMasterBillTask(carrier, actualBill, out tempPhotoCapture);
                        //isBillExists = CargoMatrix.Communication.WebServiceManager.Instance().MasterBillExists(carrier, actualBill);
                        break;
                }

                if (tempPhotoCapture == null)
                    return false;

                if (taskStatus != 0)
                //if(isBillExists == true)
                {
                    if (tempPhotoCapture.taskID == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Cannot continue. \'Record ID\' does not exist for this task.", "Error!" + " (30008)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return false;
                    }
                    if (tempPhotoCapture.statusCode == 'C' && taskStatus == 1) // completed task
                    {
                        if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(control, "Re-Open Task: " + tempPhotoCapture.actualBill) != DialogResult.OK)
                            return false;
                    }
                    else if (tempPhotoCapture.statusCode == 'N' && taskStatus == 3) // task exists but not assigned to anyone
                    {
                        CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(tempPhotoCapture.taskID);

                    }
                    else if ((tempPhotoCapture.statusCode == 'I' || tempPhotoCapture.statusCode == 'N') && taskStatus == 2) // in progress or not started task assigned to another user.
                    {
                        if (CargoMatrix.UI.CMXMessageBox.Show("This task is assigned to a different user. Would you like to Re-Assign this task to you?", "Alert!", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Cancel) == DialogResult.OK)
                        {

                            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                            {
                                CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(tempPhotoCapture.taskID);
                            }
                            else
                            {
                                if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(control, "Re-Assign Task: " + tempPhotoCapture.actualBill) == DialogResult.OK)
                                {
                                    CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(tempPhotoCapture.taskID);
                                }
                                else
                                {
                                    return false;
                                }
                            }

                        }
                        else
                            return false;
                    }
                    else if (tempPhotoCapture.statusCode == 'C' && taskStatus == 2) // task already completed by another user
                    {
                        //CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewFPCTask(housebill.ToUpper(), ref tempPhotoCapture);
                        switch (taskType)
                        {
                            case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:

                                CargoMatrix.Communication.ScannerUtilityWS.HouseBillItem h = CargoMatrix.Communication.ScannerUtility.Instance.GetHouseBillByNumber(actualBill);


                                CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill.ToUpper(), tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture);
                                break;
                            case CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL:
                                CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewMasterBillTask(carrier, actualBill.ToUpper(), tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture);
                                break;
                        }


                        
                       
                    }
                    else if (taskStatus != 1)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("An Unknown Error has occured", "Error!" + " (30009)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return false;
                    }



                }
                else  // task does not exist, check if HB exists
                {

                    //string origin, destination;
                    CargoMatrix.Communication.DTO.FreightPhotoCapture photoCaptureData = new CargoMatrix.Communication.DTO.FreightPhotoCapture();

                    bool result = false;
                    if (taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
                    {
                        result = CargoMatrix.Communication.WebServiceManager.Instance().HouseBillExists(actualBill);
                    }
                    else if (taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
                    {
                        result = CargoMatrix.Communication.WebServiceManager.Instance().MasterBillExists(carrier, actualBill);

                    }
                    if (result == false) // housebill and msterbill exists so assuming that the origing and destination will get auto assigned during create task
                    {
                        string origin, dest;
                        if (CargoMatrix.Utilities.OriginDestinationMessageBox.Show(out origin, out dest) == DialogResult.OK)
                        {
                            tempPhotoCapture.origin = origin;
                            tempPhotoCapture.destination = dest;

                        }
                        else
                            return false;
                    }
                    //if (result == true)
                    //{


                    string reference = null;
                    //if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
                    //{
                    //    //if (CargoMatrix.Communication.WebServiceManager.Instance().GetHouseBill(actualBill, out reference, out desc, out loc) != 1)
                    //    if(CargoMatrix.Communication.WebServiceManager.Instance().GetLastFPCTask(actualBill,out tempPhotoCapture))
                    //        return false;
                    //}
                    //else if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
                    //{ 

                    //}

                    if (taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
                    {
                        if (CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill, tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture) == true)
                        {
                            GetHouseBillTask(actualBill, out tempPhotoCapture);
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30010)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            return false;

                        }
                    }
                    else if (taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
                    {
                        if (CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewMasterBillTask(carrier, actualBill, tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture) == true)
                        {
                            GetMasterBillTask(carrier, actualBill, out tempPhotoCapture);
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30019)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            return false;

                        }

                    }


                    //CargoMatrix.Communication.WebServiceManager.Instance().TaskInProgress(reasonsControl.PhotoCaptureData.taskID);
                    //}





                }

                if (tempPhotoCapture.taskType == CargoMatrix.Communication.DTO.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
                {
                    /// origin or destination are null, empty or "???"
                    if (string.IsNullOrEmpty(tempPhotoCapture.origin) || string.IsNullOrEmpty(tempPhotoCapture.destination) || tempPhotoCapture.origin == "???" || tempPhotoCapture.destination == "???")
                    {
                        string origin, dest;
                        if (CargoMatrix.Utilities.OriginDestinationMessageBox.Show(out origin, out dest) == DialogResult.OK)
                        {
                            tempPhotoCapture.origin = origin.ToUpper();
                            tempPhotoCapture.destination = dest.ToUpper();
                            if( CargoMatrix.Communication.WebServiceManager.Instance().RenameReference(tempPhotoCapture.actualBill, origin, dest))
                                tempPhotoCapture.reference = string.Format("{0}-{1}-{2}",tempPhotoCapture.origin,tempPhotoCapture.actualBill,tempPhotoCapture.destination);
                            else 
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30010)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30010)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            return false;

                        }
                    }
                }

                // Show Reasons window with the photo capture only if the device has a cam available
                if (Communication.Utilities.CameraPresent)
                {
                    reasonsControl = DisplayReasons(reasonsControl, tempPhotoCapture);
                }
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30020);
                return false;
            }
            return true;
        }

        private static Reasons DisplayReasons(Reasons reasonsControl, CargoMatrix.Communication.DTO.FreightPhotoCapture tempPhotoCapture)
        {
            if (reasonsControl != null)
                reasonsControl.Dispose();

            reasonsControl = new Reasons(true);
            reasonsControl.PhotoCaptureData = tempPhotoCapture;// new CargoMatrix.Communication.Data.FreightPhotoCapture();


            reasonsControl.Location = new Point(CargoMatrix.UI.CMXAnimationmanager.GetParent().Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            reasonsControl.Size = new Size(CargoMatrix.UI.CMXAnimationmanager.GetParent().Width,
                CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.Communication.WebServiceManager.Instance().TaskInProgress(reasonsControl.PhotoCaptureData.taskID);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(reasonsControl);
            return reasonsControl;
        }


        public static bool DisplayReasons2(string actualBill, int peiceNo, UserControl control, out  CargoMatrix.Communication.DTO.FreightPhotoCapture PhotoCapture)
        {
            PhotoCapture = null;
            try
            {


                int taskStatus = 0;
                //bool isBillExists = false;
                taskStatus = GetHouseBillTask(actualBill, out PhotoCapture);


                if (PhotoCapture == null)
                    return false;

                if (taskStatus != 0)
                //if(isBillExists == true)
                {
                    if (PhotoCapture.taskID == null)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Cannot continue. \'Record ID\' does not exist for this task.", "Error!" + " (30008)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return false;
                    }
                    if (PhotoCapture.statusCode == 'C' && taskStatus == 1) // completed task
                    {
                        if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(control, "Re-Open Task: " + PhotoCapture.actualBill) != DialogResult.OK)
                            return false;
                    }
                    else if (PhotoCapture.statusCode == 'N' && taskStatus == 3) // task exists but not assigned to anyone
                    {
                        CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(PhotoCapture.taskID);

                    }
                    else if ((PhotoCapture.statusCode == 'I' || PhotoCapture.statusCode == 'N') && taskStatus == 2) // in progress or not started task assigned to another user.
                    {
                        if (CargoMatrix.UI.CMXMessageBox.Show("This task is assigned to a different user. Would you like to Re-Assign this task to you?", "Alert!", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Cancel) == DialogResult.OK)
                        {

                            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                            {
                                CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(PhotoCapture.taskID);
                            }
                            else
                            {
                                if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(control, "Re-Assign Task: " + PhotoCapture.actualBill) == DialogResult.OK)
                                {
                                    CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(PhotoCapture.taskID);
                                }
                                else
                                {
                                    return false;
                                }
                            }

                        }
                        else
                            return false;
                    }
                    else if (PhotoCapture.statusCode == 'C' && taskStatus == 2) // task already completed by another user
                    {
                        //CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewFPCTask(housebill.ToUpper(), ref PhotoCapture);

                        CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill.ToUpper(), PhotoCapture.origin, PhotoCapture.destination, ref PhotoCapture);



                    }
                    else if (taskStatus != 1)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("An Unknown Error has occured", "Error!" + " (30021)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return false;
                    }



                }
                else  // task does not exist, check if HB exists
                {

                    //string origin, destination;
                    CargoMatrix.Communication.DTO.FreightPhotoCapture photoCaptureData = new CargoMatrix.Communication.DTO.FreightPhotoCapture();

                    bool result = false;
                    result = CargoMatrix.Communication.WebServiceManager.Instance().HouseBillExists(actualBill);

                    if (result == false) // housebill and msterbill exists so assuming that the origing and destination will get auto assigned during create task
                    {
                        //CargoMatrix.Utilities.OriginDestinationMessageBox.Show(out PhotoCapture.origin, out PhotoCapture.destination);
                        string origin, dest;
                        if (CargoMatrix.Utilities.OriginDestinationMessageBox.Show(out origin, out dest) == DialogResult.OK)
                        {
                            PhotoCapture.origin = origin;
                            PhotoCapture.destination = dest;

                        }
                        else
                            return false;
                    }
                    //if (result == true)
                    //{


                    string reference = null;




                    if (CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill, PhotoCapture.origin, PhotoCapture.destination, ref PhotoCapture) == true)
                    {
                        GetHouseBillTask(actualBill, out PhotoCapture);
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30022)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return false;

                    }






                }
                //if (reasonsControl != null)
                //    reasonsControl.Dispose();

                //reasonsControl = new Reasons(true);
                //reasonsControl.PhotoCaptureData = PhotoCapture;// new CargoMatrix.Communication.Data.FreightPhotoCapture();


                //reasonsControl.Location = new Point(CargoMatrix.UI.CMXAnimationmanager.GetParent().Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
                //reasonsControl.Size = new Size(CargoMatrix.UI.CMXAnimationmanager.GetParent().Width,
                //    CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

                //CargoMatrix.Communication.WebServiceManager.Instance().TaskInProgress(reasonsControl.PhotoCaptureData.taskID);

                //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(reasonsControl);
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30023);
                return false;
            }
            return true;
        }

        //public static bool DisplayReasons(string carrier, string actualBill, CargoMatrix.Communication.Data.TaskType taskType, UserControl control, ref Reasons reasonsControl)
        //{
        //    CargoMatrix.Communication.Data.FreightPhotoCapture tempPhotoCapture = null;
        //    try
        //    {
        //        if (reasonsControl != null)
        //            reasonsControl.Dispose();

        //        reasonsControl = new Reasons(true);
        //        reasonsControl.Location = new Point(CargoMatrix.UI.CMXAnimationmanager.GetParent().Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
        //        reasonsControl.Size = new Size(CargoMatrix.UI.CMXAnimationmanager.GetParent().Width,
        //            CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);



        //        bool isBillExists = false;
        //        if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
        //        {
        //            isBillExists = CargoMatrix.Communication.WebServiceManager.Instance().HouseBillExists(actualBill);
        //        }
        //        else if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
        //        {
        //            isBillExists = CargoMatrix.Communication.WebServiceManager.Instance().MasterBillExists(carrier, actualBill);

        //        }
        //        else
        //            return false;  // type not known

        //        if (isBillExists == true)
        //        {
        //            int taskStatus = 0;
        //             switch (taskType)
        //             {
        //                 case CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
        //                     taskStatus = GetHouseBillTask(actualBill, out tempPhotoCapture);
        //                     break;
        //                 case CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL:
        //                     taskStatus = GetMasterBillTask(carrier, actualBill, out tempPhotoCapture);
        //                     break;
        //             }
        //             if (tempPhotoCapture == null)
        //                 return false;

        //             if (taskStatus != 0) // task exists
        //             {
        //                 if (tempPhotoCapture.taskID == null)
        //                 {
        //                     CargoMatrix.UI.CMXMessageBox.Show("Cannot continue. \'Record ID\' does not exist for this task.", "Error!" + " (30008)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //                     return false;
        //                 }
        //                 if (tempPhotoCapture.statusCode == 'C' && taskStatus == 1) // completed task
        //                 {
        //                     if (Utilities.SupervisorAuthorizationMessageBox.Show(control) != DialogResult.OK)
        //                         return false;
        //                 }
        //                 else if (tempPhotoCapture.statusCode == 'N' && taskStatus == 3) // task exists but not assigned to anyone
        //                 {
        //                     CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(tempPhotoCapture.taskID);

        //                 }
        //                 else if ((tempPhotoCapture.statusCode == 'I' || tempPhotoCapture.statusCode == 'N') && taskStatus == 2) // in progress or not started task assigned to another user.
        //                 {
        //                     if (CargoMatrix.UI.CMXMessageBox.Show("This task is assigned to a different user. Would you like to Re-Assign this task to you?", "Alert!", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Cancel) == DialogResult.OK)
        //                     {

        //                         if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.userType == "ADMIN")
        //                         {
        //                             CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(tempPhotoCapture.taskID);
        //                         }
        //                         else
        //                         {
        //                             if (Utilities.SupervisorAuthorizationMessageBox.Show(control) == DialogResult.OK)
        //                             {
        //                                 CargoMatrix.Communication.WebServiceManager.Instance().AssignUser(tempPhotoCapture.taskID);
        //                             }
        //                             else
        //                             {
        //                                 return false;
        //                             }
        //                         }

        //                     }
        //                     else
        //                         return false;
        //                 }
        //                 else if (tempPhotoCapture.statusCode == 'C' && taskStatus == 2) // task already completed by another user
        //                 {
        //                     //CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewFPCTask(housebill.ToUpper(), ref tempPhotoCapture);
        //                     switch (taskType)
        //                     {
        //                         case CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
        //                             CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill.ToUpper(), tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture);
        //                             break;
        //                         case CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL:
        //                             CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewMasterBillTask(carrier, actualBill.ToUpper(), tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture);
        //                             break;
        //                     }



        //                 }
        //                 else if (taskStatus != 1)
        //                 {
        //                     CargoMatrix.UI.CMXMessageBox.Show("An Unknown Error has occured", "Error!" + " (30009)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //                     return false;
        //                 }


        //             }
        //             else  // HB exists but task does not exist 
        //             {

        //                 //reasonsControl.PhotoCaptureData = new CargoMatrix.Communication.Data.FreightPhotoCapture();
        //                 //reasonsControl.PhotoCaptureData.itemsList = new List<CargoMatrix.Communication.Data.FreightPhotoCaptureItem>();



        //                 if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
        //                 {
        //                     if (CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill, tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture) == false)
        //                     {
        //                         CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30010)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //                         return false;

        //                     }
        //                     taskStatus = GetHouseBillDetails(actualBill, out tempPhotoCapture);
        //                 }
        //                 else if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
        //                 {
        //                     if (CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewMasterBillTask(carrier, actualBill, tempPhotoCapture.origin, tempPhotoCapture.destination, ref tempPhotoCapture) == false)
        //                     {
        //                         CargoMatrix.UI.CMXMessageBox.Show("Unable to create task for this item", "Error!" + " (30019)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //                         return false;

        //                     }
        //                     taskStatus = GetMasterBillDetails(carrier, actualBill, out tempPhotoCapture);

        //                 }
        //                 if (taskStatus != 1)
        //                     return false;





        //             }

        //        }
        //        else  // HB does not exist...there is  a possibility that a task exist for this reference so check if task is there.
        //        {

        //           //reasonsControl.PhotoCaptureData.itemsList = new List<CargoMatrix.Communication.Data.FreightPhotoCaptureItem>();
        //            string origin, destination;
        //            CargoMatrix.Utilities.OriginDestinationMessageBox.Show(out origin, out destination);

        //            //reasonsControl.PhotoCaptureData.actualBill = actualBill.ToUpper();
        //            //if (tempPhotoCapture == null)
        //              //  tempPhotoCapture = new CargoMatrix.Communication.Data.FreightPhotoCapture();

        //            switch (taskType)
        //            {
        //                case CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL:
        //                    if(GetHouseBillTask(actualBill, out tempPhotoCapture) != 1)
        //                        CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewHouseBillTask(actualBill.ToUpper(), origin, destination, ref tempPhotoCapture);
        //                    break;
        //                case CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL:
        //                    if(GetMasterBillTask(carrier,actualBill, out tempPhotoCapture) != 1)
        //                        CargoMatrix.Communication.WebServiceManager.Instance().CreateAndAssignNewMasterBillTask(carrier, actualBill.ToUpper(), origin, destination, ref tempPhotoCapture);
        //                    break;
        //            }
        //            ////if (tempPhotoCapture != null)
        //            ////{
        //            //    tempPhotoCapture.actualBill = actualBill.ToUpper();
        //            //    tempPhotoCapture.carrier = carrier;
        //            //    tempPhotoCapture.origin = origin;
        //            //    tempPhotoCapture.destination = destination;


        //            //    string[] reasons = null;
        //            //    if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_HOUSEBILL)
        //            //    {
        //            //        //tempPhotoCapture.reference = 
        //            //        if (CargoMatrix.Communication.WebServiceManager.Instance().GetAllHouseBillReasons(out reasons) == false)
        //            //            return false;
        //            //    }
        //            //    else if (taskType == CargoMatrix.Communication.Data.TaskType.FREIGHT_PHOTO_CAPTURE_MASTERBILL)
        //            //    {
        //            //        if (CargoMatrix.Communication.WebServiceManager.Instance().GetAllMasterBillReasons(out reasons) == false)
        //            //            return false;
        //            //    }

        //            //    if (reasons != null)
        //            //    {
        //            //        if (tempPhotoCapture.itemsList == null)
        //            //            tempPhotoCapture.itemsList = new List<CargoMatrix.Communication.Data.FreightPhotoCaptureItem>();


        //            //        reasonsControl.PhotoCaptureData.itemsList.Clear();

        //            //        for (int i = 0; i < reasons.Length; i++)
        //            //        {
        //            //            tempPhotoCapture.itemsList.Add(new CargoMatrix.Communication.Data.FreightPhotoCaptureItem(reasons[i], null, 0));
        //            //        }

        //            //    }
        //            //    //tempPhotoCapture.taskPrefix
        //            //    //reasonsControl.PhotoCaptureData.taskPrefix = tempPhotoCapture.taskPrefix;
        //            ////}
        //            ////reasonsControl.PhotoCaptureData.reference = actualBill.ToUpper();
        //        }


        //        reasonsControl.PhotoCaptureData = tempPhotoCapture;// new CargoMatrix.Communication.Data.FreightPhotoCapture();             
        //        CargoMatrix.Communication.WebServiceManager.Instance().TaskInProgress(reasonsControl.PhotoCaptureData.taskID);
        //        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(reasonsControl);

        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 30020);
        //        return false;

        //    }


        //    return true;


        //}
        private static int GetHouseBillTask(string housebill, out CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {
            photoCapture = null;// new CargoMatrix.Communication.Data.FreightPhotoCapture();
            return CargoMatrix.Communication.WebServiceManager.Instance().GetLastFPCHouseBillTask(housebill, out photoCapture);

        }
        private static int GetMasterBillTask(string carrier, string masterbill, out CargoMatrix.Communication.DTO.FreightPhotoCapture photoCapture)
        {
            photoCapture = null;
            return CargoMatrix.Communication.WebServiceManager.Instance().GetLastFPCMasterBillTask(carrier, masterbill, out photoCapture);

        }

    }

}
