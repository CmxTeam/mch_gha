﻿using System;
using System.Linq;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoDischarge;
using CargoMatrix.Communication.Common;
using CustomListItems;
using CargoMatrix.Communication.WSCargoDischarge;
using SmoothListbox;
using CMXExtensions;
using System.Collections.Generic;

namespace CargoMatrix.FreightDischarge
{
    public partial class DischargeList : CargoUtilities.SmoothListBoxAsync2Options<IDischargeHouseBillItem>
    {
        HousebillFilterCriteria filterCriteria;
        HouseBillSortCriteria sortCriteria;
        CustomListItems.OptionsListITem filterSortMenuItem;
        CustomListItems.OptionsListITem undoMenuItem = new OptionsListITem(OptionsListITem.OptionItemID.UNDO_DSCHARGE_SCAN);
     
        bool textSearch = true;
       
        int listItemHeight;
     
        public DischargeList()
        {
            InitializeComponent();
            this.TitleText = "CargoDischarge";
           
            this.filterCriteria = new HousebillFilterCriteria();
            this.sortCriteria = new HouseBillSortCriteria();
            this.listItemHeight = 0;

            this.LoadOptionsMenu += new EventHandler(DischargeList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(DischargeList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(DischargeList_ListItemClicked);

            this.buttonScannedList.Click += new EventHandler(buttonScannedList_Click);
            
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeScanned);

            this.ShowContinueButton(false);
           
            this.panel1.Visible = false;
            this.panel1.Height = 0;
            this.ButtonFinalizeText = "Release";

            this.smoothListBoxMainList.MultiSelectEnabled = false;
            this.smoothListBoxMainList.UnselectEnabled = true;

            var forklift = Forklift.Instance;

            if (forklift.IsLoaded == false) return;
            
            Forklift.MoveUnreleasedPieces();

            int forkliftItemsCount = forklift.ItemsCountDB;
            forklift.ItemsCountList = forkliftItemsCount;
            this.buttonScannedList.Value = forkliftItemsCount;

            forklift.HouseBillItems.Clear();
         
            if (forkliftItemsCount > 0)
            {
                var firkliftHouseBills = CargoDischarge.Instance.GetForkLiftHouseBills(new HouseBillSortCriteria(), Forklift.Instance.ID);

                forklift.HouseBillItems.AddRange(firkliftHouseBills);
            }

            this.undoMenuItem.Enabled = forkliftItemsCount != 0;
            this.FinalizeButtonEnabled = this.FinalizeEnabled();
        }
        
        void buttonScannedList_Click(object sender, EventArgs e)
        {
            if (buttonScannedList.Value <= 0) return;

            BarcodeEnabled = false;
            BarcodeEnabled = false;

            var instance = Forklift.Instance;
            instance.RemovedHouseBillsIds.Clear();
            instance.ReleasedHouseBillsIds.Clear();

            instance.ItemsCountList = buttonScannedList.Value;

            instance.ShowDialog();

            this.Refresh();

            buttonScannedList.Value = instance.ItemsCountList;

            var allListItems = this.smoothListBoxMainList.Items.OfType<DischargeListItemRendered>();

            if (buttonScannedList.Value <= 0)
            {
                this.EnableTextSearch();

                this.undoMenuItem.Enabled = false;
                this.FinalizeButtonEnabled = this.FinalizeEnabled();
                this.filterSortMenuItem.Enabled = true;

                instance.ItemsCountList = 0;
                this.buttonScannedList.Value = 0;

                this.LoadControl();

                BarcodeEnabled = false;
                BarcodeEnabled = true;

                return;
            }

            this.undoMenuItem.Enabled = true;
            this.FinalizeButtonEnabled = this.FinalizeEnabled();
            this.filterSortMenuItem.Enabled = false;

            filterSortMenuItem.DescriptionLine = this.GetFilterSortHeaderString(this.filterCriteria, this.sortCriteria);
         
            switch (instance.LastContentChangeType)
            {
                case ChangeCauseEnum.Closed:
                    this.TitleText = this.GetMainScreenHeaderString(this.smoothListBoxMainList.Items.Count);
                    break;

                case ChangeCauseEnum.Remove:

                    var removedItems = from item in allListItems
                                       where (from id in instance.RemovedHouseBillsIds
                                              select id).Contains(item.HouseBillItem.HousebillId)
                                       select item.HouseBillItem;

                    var refreshedItems = DischargeItemFunctionallity.RefreshHouseBills(removedItems.ToArray());


                    foreach (var item in refreshedItems)
                    {
                        var listItem = (
                                            from li in allListItems
                                            where li.HouseBillItem.HousebillId == item.HousebillId
                                            select li
                                       ).First();

                        listItem.HouseBillItem = item;
                    }

                    this.TitleText = this.GetMainScreenHeaderString(this.smoothListBoxMainList.Items.Count);

                    break;

                case ChangeCauseEnum.Release:

                    this.LoadControl();

                    break;

                default: throw new NotImplementedException();
            }

            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        void DischargeList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            DischargeListItemRendered dischargeListItem = (DischargeListItemRendered)listItem;

            ((IExtendedListItem)listItem).Focus(true);
            this.smoothListBoxMainList.MoveControlToTop(dischargeListItem);

            this.LayoutItems();

            IDischargeHouseBillItem houseBillItem = dischargeListItem.HouseBillItem;
            
            bool wasScanned;
            IDischargeHouseBillItem resultItem;

            this.BarcodeEnabled = false;

            DischargeItemFunctionallity.ShowLocations(houseBillItem, false, out wasScanned, out resultItem);

            this.BarcodeEnabled = true;

            this.Refresh();

            if (wasScanned == true)
            {
                this.buttonScannedList.Value = resultItem.TransactionStatus.TransactionRecords;
                Forklift.Instance.ItemsCountList = resultItem.TransactionStatus.TransactionRecords;

                this.UpdateInterfaceOnScan(true, resultItem, dischargeListItem);
            }

        }

        void searchBox_TextChanged(object sender, EventArgs e)
        {
            if (this.textSearch == false) return;

            string filterText = this.txtFilter.Text.TrimEnd(new char[] { ' ' });

            this.smoothListBoxMainList.Filter(filterText);

            this.TitleText = string.Format("CargoDischarge ({0})", this.smoothListBoxMainList.VisibleItemsCount);
        }

        void DischargeList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            
            this.AddOptionsListItem(filterSortMenuItem);

            this.AddOptionsListItem(this.undoMenuItem);

            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        protected override Control InitializeItem(IDischargeHouseBillItem item)
        {
            DischargeListItemRendered listItem = new DischargeListItemRendered();
            
            this.listItemHeight = listItem.Height;

            listItem.OnDamageCaptureClick += new EventHandler(listItem_OnDamageCaptureClick);
            listItem.OnViewerClick  += new EventHandler(listItem_OnDetailsClick);
            listItem.OnMoreDetailsClick += new EventHandler(listItem_OnMoreDetailsClick);
           
            
            listItem.HouseBillItem = item;

            return listItem;
        }

        void listItem_OnEnterClick(object sender, EventArgs e)
        {
            DischargeListItemRendered listItem = (DischargeListItemRendered)sender;

            this.smoothListBoxMainList.MoveControlToTop(listItem);

            IDischargeHouseBillItem houseBillItem = listItem.HouseBillItem;

            Cursor.Current = Cursors.WaitCursor;         
            
            bool wasScanned;
            IDischargeHouseBillItem resultItem;
            DischargeItemFunctionallity.ShowLocations(houseBillItem, false, out wasScanned, out resultItem);

            this.Refresh();
            
            this.smoothListBoxMainList.LayoutItems();
            
            if (wasScanned == true)
            {
                this.buttonScannedList.Value = resultItem.TransactionStatus.TransactionRecords;
                Forklift.Instance.ItemsCountList = resultItem.TransactionStatus.TransactionRecords;

                this.UpdateInterfaceOnScan(true, resultItem, listItem);
            }
        }

        void listItem_OnMoreDetailsClick(object sender, EventArgs e)
        {
            DischargeListItemRendered listItem = (DischargeListItemRendered)sender;

            IDischargeHouseBillItem houseBillItem = listItem.HouseBillItem;

            IDischargeHouseBillItem hbItem;
         
            var action = DischargeItemFunctionallity.DisplayBrowse(houseBillItem.Reference(), houseBillItem, out hbItem);

            switch (action)
            {
                case OptionsActionPerformedEnum.CancelTask:
                    this.smoothListBoxMainList.RemoveItem(listItem);
                    break;

                case OptionsActionPerformedEnum.SwitchMode:
                    listItem.HouseBillItem = hbItem;
                    break;

                case OptionsActionPerformedEnum.PiecesMovedToForklift:
                    
                    this.buttonScannedList.Value = hbItem.TransactionStatus.TransactionRecords;
                    Forklift.Instance.ItemsCountList = hbItem.TransactionStatus.TransactionRecords;

                    this.UpdateInterfaceOnScan(true, hbItem, listItem);

                    break;
            }
        }

        void listItem_OnDetailsClick(object sender, EventArgs e)
        {
            DischargeListItemRendered listItem = (DischargeListItemRendered)sender;

            IDischargeHouseBillItem houseBillItem = listItem.HouseBillItem;

            DischargeItemFunctionallity.DisplayBillViewer(houseBillItem.HousebillNumber, this.Left, this.Width);
        }

        void listItem_OnDamageCaptureClick(object sender, EventArgs e)
        {
            DischargeListItemRendered listItem = (DischargeListItemRendered)sender;

            IDischargeHouseBillItem houseBillItem = listItem.HouseBillItem;

            DischargeItemFunctionallity.DisplayDamage(houseBillItem);
        }  
    
        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            pictureBoxDown.Enabled = false;
            pictureBoxUp.Enabled = false;
            panelSoftkeys.Refresh();

            var forklift = Forklift.Instance;

            if (forklift.IsLoaded == false) return;
           

            if (this.filterSortMenuItem == null)
            {
                this.filterSortMenuItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER);
            }
            
          
            var items = CargoDischarge.Instance.GetCargoDischargeHouseBills(this.filterCriteria, this.sortCriteria);
            filterSortMenuItem.DescriptionLine = this.GetFilterSortHeaderString(this.filterCriteria, this.sortCriteria);

            this.smoothListBoxMainList.RemoveAll();

            if (forklift.HouseBillItems.Count > 0)
            {
                var first = forklift.HouseBillItems.First();

                this.DisableTextSearch(first);

                this.filterSortMenuItem.Enabled = false;

                var relatedItemCreteria = DischargeItemFunctionallity.RelatedItemCreteria();

                var relatedItems = from item in items
                                   where relatedItemCreteria(first, item) == true
                                   select item;

                foreach (var item in relatedItems)
                {
                    this.smoothListBoxMainList.AddItem2(InitializeItem(item));
                }
            }
            else
            {
             
                this.smoothListBoxMainList.RemoveAll();

                foreach(var item in items)
                {
                    this.smoothListBoxMainList.AddItem2(InitializeItem(item));
                }

                this.EnableTextSearch();
            }

            this.LayoutItems();
            this.smoothListBoxMainList.RefreshScroll();

            this.TitleText = this.GetMainScreenHeaderString(this.smoothListBoxMainList.Items.Count);

            this.undoMenuItem.Enabled = forklift.HouseBillItems.Count > 0;
            this.FinalizeButtonEnabled = this.FinalizeEnabled();

            
            this.Focus();
          
            BarcodeEnabled = false;
            BarcodeEnabled = true;
          
            Cursor.Current = Cursors.Default;
        }

        void DischargeList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;
            
            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.FILTER:

                    FilterPopup filterPopup = new FilterPopup(this.filterCriteria, this.sortCriteria);
                  
                    if (DialogResult.OK == filterPopup.ShowDialog())
                    {
                        this.filterCriteria = filterPopup.FilterCriteria;
                        this.sortCriteria = filterPopup.SortCriteria;
                        this.filterSortMenuItem.DescriptionLine = string.Format("{0} / {1}", this.filterCriteria, this.sortCriteria);

                        this.Refresh();
                        
                        this.LoadControl();
                    }

                    break;

                case CustomListItems.OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();
                        
                    break;

                case OptionsListITem.OptionItemID.UNDO_DSCHARGE_SCAN:

                    var result = CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to undo scanned pieces?", "Question", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.OK);

                    if (result != DialogResult.OK) return;

                    this.Refresh();

                    Cursor.Current = Cursors.WaitCursor;

                    TransactionStatus status = CargoDischarge.Instance.ClearForklift(Forklift.Instance.ID);

                    if (status.TransactionStatus1 == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        return;
                    }

                    this.EnableTextSearch();

                    this.undoMenuItem.Enabled = false;
                    this.filterSortMenuItem.Enabled = true;

                    Forklift.Instance.ItemsCountList = 0;
                    Forklift.Instance.HouseBillItems.Clear();
                    this.buttonScannedList.Value = 0;

                    Cursor.Current = Cursors.Default;

                    this.LoadControl();

                    break;

                default: return;
            }
        }


        #region Additional Methods
        void BarcodeScanned(string barcodeData)
        {
            Cursor.Current = Cursors.WaitCursor;

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    Scan(scanItem.HouseBillNumber, scanItem.PieceNumber);
                    break;

                case CMXBarcode.BarcodeTypes.Door:

                    this.ContinueToRelease(scanItem.Location);

                    break;
                
                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                    break;
            }

            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;

        }

        private void Scan(string housebillNumber, int pieceNumber)
        {
            var selectedItem = (
                                        from item in this.smoothListBoxMainList.Items.OfType<DischargeListItemRendered>()
                                        where  item.HouseBillItem != null &&
                                        string.Compare(item.HouseBillItem.HousebillNumber, housebillNumber) == 0 &&
                                              item.Filter == false
                                        select item
                                     ).FirstOrDefault();

            if (selectedItem == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Housebill is not in the list", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                if (this.buttonScannedList.Value <= 0)
                {
                    this.EnableTextSearchPersist();
                }

                return;
            }

            var selectedHouseBill = selectedItem.HouseBillItem; 

            ((IExtendedListItem)selectedItem).Focus(true);
            this.smoothListBoxMainList.MoveControlToTop(selectedItem);

            this.LayoutItems();

            DischargeHousebillItem houseBillItem;
            bool result = false;

            switch (selectedHouseBill.ScanMode)
            {
                case CargoMatrix.Communication.DTO.ScanModes.Piece:
                    result = this.ScanPiecesMode(selectedHouseBill, pieceNumber, out houseBillItem);
                    break;

                case CargoMatrix.Communication.DTO.ScanModes.Count:
                    result = this.ScanCountMode(selectedHouseBill, pieceNumber, out houseBillItem);
                    break;

                default: throw new NotImplementedException();
            }

            this.UpdateInterfaceOnScan(result, houseBillItem, selectedItem);
        }

        private void UpdateInterfaceOnScan(bool scanResult, IDischargeHouseBillItem refreshedItem, DischargeListItemRendered listItem)
        {
            if (scanResult == true)
            {
                this.UpdateHouseBillsListAndForklift(refreshedItem, listItem);
              
                this.RemoveUnrelatedListItems(refreshedItem);

                this.DisableTextSearch(refreshedItem);

                this.filterSortMenuItem.Enabled = false;
            }
            else
            {
                if (this.buttonScannedList.Value <= 0)
                {
                    this.EnableTextSearch();
                }
            }

            this.filterSortMenuItem.DescriptionLine = this.GetFilterSortHeaderString(this.filterCriteria, sortCriteria);

            this.undoMenuItem.Enabled = Forklift.Instance.HouseBillItems.Count > 0;
            this.FinalizeButtonEnabled = this.FinalizeEnabled();

            ((IExtendedListItem)listItem).Focus(true);
            this.smoothListBoxMainList.MoveControlToTop(listItem);

            this.LayoutItems();
            this.smoothListBoxMainList.RefreshScroll();

            listItem.Refresh();
        }

        private void UpdateHouseBillsListAndForklift(IDischargeHouseBillItem houseBillItem, DischargeListItemRendered listItem)
        {
            //var selectedListItem = (
            //                           from item in this.smoothListBoxMainList.Items.OfType<DischargeListItemRendered>()
            //                           where item.HouseBillItem.HousebillId == houseBillItem.HousebillId
            //                           select item
            //                        ).FirstOrDefault();

            listItem.HouseBillItem = houseBillItem;

            ((IExtendedListItem)listItem).Focus(true);
            
            var forkliftHb = (from item in Forklift.Instance.HouseBillItems
                             where item.HousebillId == houseBillItem.HousebillId
                             select item).FirstOrDefault();

            if (forkliftHb != null)
            {
                Forklift.Instance.HouseBillItems.Remove(forkliftHb);
            }

            Forklift.Instance.HouseBillItems.Add(houseBillItem);
        }

        private void RemoveUnrelatedListItems(IDischargeHouseBillItem houseBillItem)
        {
            var relatedItemCreteria = DischargeItemFunctionallity.RelatedItemCreteria();
            
            foreach (DischargeListItemRendered item in smoothListBoxMainList.Items)
            {
                item.Filter = !relatedItemCreteria(houseBillItem, item.HouseBillItem); 
            }

            this.smoothListBoxMainList.itemsPanel.Top = 0;
            LayoutItems();
            this.smoothListBoxMainList.RefreshScroll();

            this.TitleText = string.Format("CargoDischarge ({0})", this.smoothListBoxMainList.VisibleItemsCount);
        }

        private void EnableTextSearchPersist()
        {
            this.textSearch = true;
            this.txtFilter.Enabled = true;
        }
        
        private void EnableTextSearch()
        {
            this.EnableTextSearchPersist();
            this.txtFilter.Text = string.Empty;
        }

        private void DisableTextSearch(IDischargeHouseBillItem houseBillItem)
        {
            this.textSearch = false;
            this.txtFilter.Text = string.Format("{0}, {1}", houseBillItem.CompanyName, houseBillItem.DriverFullName);
            this.txtFilter.Enabled = false;
        }

        private bool ScanPiecesMode(IDischargeHouseBillItem selectedHouseBill, int pieceNumber, out DischargeHousebillItem houseBiullItem)
        {
            houseBiullItem = null;
            int piecesCount = 1;
            int slacNumber = piecesCount;

            if (selectedHouseBill.AvailableSlac <= 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Scanned HAWB has no available pieces to discharge", "Message", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                return false;
            }

            int remainigPieces = selectedHouseBill.AvailablePieces;

            if (remainigPieces == piecesCount)
            {
                slacNumber = selectedHouseBill.ReleseSlac - selectedHouseBill.ScannedSlac;
            }
            else
            {
                if (selectedHouseBill.TotalPieces != selectedHouseBill.Slac)
                {
                    bool result = this.ScanCountMode(selectedHouseBill, pieceNumber, out houseBiullItem);

                    return result;
                }
            }

            houseBiullItem = CargoDischarge.Instance.ScanPieceNumberIntoForkLift(selectedHouseBill.HousebillNumber,
                                                                                 pieceNumber,
                                                                                 slacNumber,
                                                                                 ScanTypeEnum.Automatic,
                                                                                 Forklift.Instance.ID);

            if (houseBiullItem.TransactionStatus.TransactionStatus1 == true)
            {
                foreach (var alert in houseBiullItem.ShipmentAlerts)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(alert.AlertMessage, string.Format("Alert {0:MM/dd/yyyy}", alert.AlertDate), CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                }
                
                
                buttonScannedList.Value = houseBiullItem.TransactionStatus.TransactionRecords;
                return true;
            }

            if (houseBiullItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Count)
            {
                return this.ScanCountMode(houseBiullItem, pieceNumber, out houseBiullItem);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(houseBiullItem.TransactionStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
            return false;
        }

        private bool ScanCountMode(IDischargeHouseBillItem selectedHouseBill, int pieceNumber, out DischargeHousebillItem houseBiullItem)
        {
            houseBiullItem = null;

            int remainigPieces = selectedHouseBill.AvailablePieces;

            if (remainigPieces == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Scanned HAWB has no available pieces to discharge", "Message", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                return false;
            }

            int piecesEntered = remainigPieces;
           
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = "Confirm count";
            cntMsg.LabelDescription = "Enter number of pieces";
            cntMsg.LabelReference = selectedHouseBill.Reference();
            cntMsg.PieceCount = remainigPieces;

            if (DialogResult.OK != cntMsg.ShowDialog()) return false;

            piecesEntered = cntMsg.PieceCount;

            int slacNumber = piecesEntered;
         
           
            if (selectedHouseBill.TotalPieces != selectedHouseBill.Slac)
            {
                bool isReadonly = false;// remainigPieces == piecesEntered;
                
                int maxReleseSlac = selectedHouseBill.CalculateMaxReleaseSlac(piecesEntered);
                int? enteredSlac = DischargeItemFunctionallity.GetSlacFromUser(piecesEntered, maxReleseSlac, isReadonly);

                if (enteredSlac.HasValue == false) return false;

                slacNumber = enteredSlac.Value;
            }
          
            houseBiullItem = CargoDischarge.Instance.ScanPieceCountIntoForkLift(selectedHouseBill,
                                                                               piecesEntered,
                                                                               slacNumber,
                                                                               ScanTypeEnum.Automatic,
                                                                               Forklift.Instance.ID);

            if (houseBiullItem.TransactionStatus.TransactionStatus1 == true)
            {
                foreach (var alert in houseBiullItem.ShipmentAlerts)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(alert.AlertMessage, string.Format("Alert {0:MM/dd/yyyy}", alert.AlertDate), CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                }
                
                buttonScannedList.Value = houseBiullItem.TransactionStatus.TransactionRecords;
                return true;
            }

            CargoMatrix.UI.CMXMessageBox.Show(houseBiullItem.TransactionStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            return false;
        }

        bool ItemTextMatch(string text, IDischargeHouseBillItem housebillItem)
        {
            string textToMatch = text.ToUpper();

            if (housebillItem.DriverFullName.ToUpper().Contains(textToMatch)) return true;

            if (housebillItem.CompanyName.ToUpper().Contains(textToMatch)) return true;

            if (housebillItem.Reference().ToUpper().Contains(textToMatch)) return true;

            return false;
        }

        string GetFilterSortHeaderString(HousebillFilterCriteria filter, HouseBillSortCriteria sort)
        {
            return string.Format("{0} / {1}", filter, sort);
        }

        string GetMainScreenHeaderString(int itemsCount)
        {
            return string.Format("CargoDischarge ({0})", itemsCount);

        }

        bool FinalizeEnabled()
        {
            return Forklift.Instance.HouseBillItems.Count > 0; 
                //Forklift.Instance.HouseBillItems.Exists(item => item.ReleaseStatus == ReleaseStatuses.Completed);
        }

        void ContinueToRelease(string doorName)
        {
            var forklift = Forklift.Instance;

            var firkliftHouseBills = CargoDischarge.Instance.GetForkLiftHouseBills(new HouseBillSortCriteria(), Forklift.Instance.ID);

            if (firkliftHouseBills.Count() <= 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Scan housebills to the forklift", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }
            
            if (forklift.ScanningType == CargoMatrix.Communication.ScannerUtilityWS.StepsTypes.TwoScan)
            {
                forklift.ReleaseDoorName = doorName;
                
                this.buttonScannedList_Click(this.buttonScannedList, EventArgs.Empty);

                forklift.ReleaseDoorName = string.Empty;
                return;
            }

            var partiallyScannedHouseBiills = from item in firkliftHouseBills
                                              where item.ReleaseStatus != ReleaseStatuses.Completed
                                              select item;

            if (partiallyScannedHouseBiills.Count() > 0)
            {
                var dialogResult = CargoMatrix.UI.CMXMessageBox.Show("Release partially scanned housebills?", "Question", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.OK);

                if (dialogResult != DialogResult.OK) return;
            }

            BarcodeEnabled = false;
            BarcodeEnabled = false;

            var pieces = this.GetReleasePieces(firkliftHouseBills);

            bool result = DischargeItemFunctionallity.ReleaseHousebills(doorName, firkliftHouseBills, pieces);

            this.Refresh();

            BarcodeEnabled = false;
            BarcodeEnabled = true;
            
            if (result == false) return;

            buttonScannedList.Value = forklift.ItemsCountList;

            this.EnableTextSearch();

            this.undoMenuItem.Enabled = false;
            this.filterSortMenuItem.Enabled = true;

            forklift.ItemsCountList = 0;
            forklift.HouseBillItems.Clear();
            this.buttonScannedList.Value = 0;

            this.LoadControl();
        }

        private IEnumerable<PieceItem> GetReleasePieces(IEnumerable<IDischargeHouseBillItem> houseBills)
        {
            return from houseBill in houseBills
                   from piece in houseBill.Pieces
                   select DischargeItemFunctionallity.CreateReleasePieceItem(piece, houseBill);
        }


        #endregion
    }
}