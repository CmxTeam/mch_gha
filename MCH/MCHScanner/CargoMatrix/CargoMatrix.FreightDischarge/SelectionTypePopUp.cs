﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.FreightDischarge
{
    public partial class SelectionTypePopUp : CargoMatrix.Utilities.MessageListBox
    {
        bool multiSelect;

        public bool MultiSelect
        {
            get { return multiSelect; }
            set { multiSelect = value; }
        }


        public SelectionTypePopUp()
        {
            InitializeComponent();

            this.HeaderText = "Select filters";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = true;
            this.TopPanel = false;

            this.AddItem(new CustomListItems.CkeckItem<string>("Single Select", 1, "Single Select"));
            this.AddItem(new CustomListItems.CkeckItem<string>("Multiple Select", 2, "Multiple Select"));

        }
    }
}