﻿using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using CargoMatrix.UI;
using System.Drawing;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Utilities;
using CargoMatrix.Communication.CargoDischarge;
using CargoMatrix.Communication.WSCargoDischarge;
using CargoMatrix.Communication;
using CustomUtilities;
using CMXExtensions;
using System.IO;


namespace CargoMatrix.FreightDischarge
{
    internal static class DischargeItemFunctionallity
    {
        public static void DisplayDamage(IDischargeHouseBillItem houseBillItem)
        {
            Cursor.Current = Cursors.WaitCursor;

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(houseBillItem);

            damageCapture.OnLoadShipmentConditions += (sender, args) =>
                {
                    args.ShipmentConditions = ScannerUtility.Instance.GetHouseBillShipmentConditionTypes();
                };

            damageCapture.OnLoadShipmentConditionsSummary += (sender, args) =>
                {     
                    args.ConditionsSummaries = ScannerUtility.Instance.GetHouseBillShipmentConditionSummary(args.HouseBillID);
                    
                };


            damageCapture.OnUpdateShipmentConditions += (sender, args) =>
                {
                    args.Result = ScannerUtility.Instance.UpdateHouseBillShipmentConditions(args.HouseBillId, args.ScanModes, args.ShipmentConditions, houseBillItem.TaskID, CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.CargoDischarge,string.Empty, args.Slac);
                };

            damageCapture.GetHouseBillPiecesIDs = () => 
            {
                var refreshedHouseBill = CargoDischarge.Instance.GetCargoDischargeHouseBillByNumber(houseBillItem.HousebillNumber);

                return (
                            from piece in refreshedHouseBill.Pieces
                            select piece.PieceId
                       ).ToArray();
            };


            CMXAnimationmanager.DisplayForm(damageCapture);
            Cursor.Current = Cursors.Default;
        }

        public static void DisplayBillViewer(string hawbNo, int left, int width)
        {
            Cursor.Current = Cursors.WaitCursor;

            CargoMatrix.Viewer.HousebillViewer billViewer = new CargoMatrix.Viewer.HousebillViewer(hawbNo);//"3TB7204" masterbill);

            billViewer.Location = new Point(left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
            Cursor.Current = Cursors.Default;
        }

        public static OptionsActionPerformedEnum DisplayBrowse(string headerTitle, IDischargeHouseBillItem houseBillItem, out IDischargeHouseBillItem result)
        {
            result = null;
          
            OptionsActionPerformedEnum actionPerformed = OptionsActionPerformedEnum.Close;
            
            MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();

            List<Control> actions = new List<Control> 
            { 
                new SmoothListbox.ListItems.StandardListItem("SWITCH MODE", null, 1),
                new SmoothListbox.ListItems.StandardListItem("PRINT LABELS", null, 2),
                new SmoothListbox.ListItems.StandardListItem("SHOW LOCATIONS", null, 3),
            };

            if (houseBillItem.status != CargoMatrix.Communication.DTO.HouseBillStatuses.InProgress)
            {
                actions.Add(new SmoothListbox.ListItems.StandardListItem("CANCEL TASK", null, 4));
            }

            actPopup.AddItems(actions.ToArray());
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = headerTitle;
            actPopup.HeaderText2 = "Select action for " + houseBillItem.HousebillNumber;

            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1:
                        bool switchResult = SwitchMode(houseBillItem);

                        if (switchResult == true)
                        {
                            result = CargoDischarge.Instance.GetCargoDischargeHouseBillByNumber(houseBillItem.HousebillNumber);
                            actionPerformed = OptionsActionPerformedEnum.SwitchMode;
                        }
                        break;

                    case 2:
                        var hb = CargoMatrix.Communication.ScannerUtility.Instance.GetHouseBillByNumber(houseBillItem.HousebillNumber);
                        PrintLabels(hb);
                        break;

                    case 3:
                        bool wasScanned;
                        ShowLocations(houseBillItem, true, out wasScanned, out result);

                        if (wasScanned == true)
                        {
                            actionPerformed = OptionsActionPerformedEnum.PiecesMovedToForklift;
                        }
                        
                        break;

                    case 4:

                        DialogResult superDialogResult = CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override");

                        if (superDialogResult != DialogResult.OK) break;

                        int forkliftID = Forklift.Instance.ID;
                        TransactionStatus status = CargoDischarge.Instance.CancelCargoDischarge(new int[] { houseBillItem .TaskID}, forkliftID);

                        if (status.TransactionStatus1 == false)
                        {
                            CMXMessageBox.Show(status.TransactionError, "Error", CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                        }
                        else
                        {
                            actionPerformed = OptionsActionPerformedEnum.CancelTask;
                        }

                        break;
                }
            }
            actPopup.ResetSelection();

            return actionPerformed;
        }

        public static IDischargeHouseBillItem[] RefreshHouseBills(IDischargeHouseBillItem[] houseBillItems)
        {
            var hbNumbers = from item in houseBillItems
                            select item.HousebillNumber;

            return CargoDischarge.Instance.GetCargoDischargeHouseBillsByNumber(hbNumbers.ToArray());
        }

        private static void PrintLabels(CargoMatrix.Communication.ScannerUtilityWS.HouseBillItem houseBillItem)
        {
            CustomUtilities.HawbLabelPrinter.Show(houseBillItem.HousebillId, houseBillItem.HousebillNumber, houseBillItem.TotalPieces);
        }

        private static bool SwitchMode(IDischargeHouseBillItem houseBillItem)
        {
            bool retValue = false;
            // piece Mode -> Count
            if (houseBillItem.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
            {
                string msg = string.Format("This will change Hawb {0} scan mode to COUNT. Do you want to continue?", houseBillItem.HousebillNumber);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    User user = CargoMatrix.Communication.WebServiceManager.Instance().m_user;
                    
                    if (user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin || 
                        DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {
                        var status = ScannerUtility.Instance.ChangeHawbMode(houseBillItem.HousebillId,
                                                                            houseBillItem.TaskID,
                                                                            CargoMatrix.Communication.DTO.ScanModes.Count);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format("Hawb {0} : Mode changed to COUNT", houseBillItem.HousebillNumber), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            // Count Mode -> Piece
            else
            {
                string msg = string.Format("This will change HAWB {0} scan mode to PIECEID. Do you want to continue?", houseBillItem.HousebillNumber);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    User user = CargoMatrix.Communication.WebServiceManager.Instance().m_user;

                    if (user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin ||
                        DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {

                        var status = ScannerUtility.Instance.ChangeHawbMode(houseBillItem.HousebillId,
                                                                                          houseBillItem.TaskID,
                                                                                          CargoMatrix.Communication.DTO.ScanModes.Piece);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format("Hawb {0} : Mode changed to PIECEID", houseBillItem.HousebillNumber), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }

                }
            }
            return retValue;
        }

        public static void ShowLocations(IDischargeHouseBillItem houseBillItem, bool previewOnly , out bool wasScanned, out IDischargeHouseBillItem refreshedHouseBill)
        {
            int forkliftID = Forklift.Instance.ID;

            LocationView locationsViewer = new LocationView(houseBillItem, forkliftID, previewOnly);

            locationsViewer.ShowDialog();
           
            wasScanned = locationsViewer.WasScanned;
            refreshedHouseBill = locationsViewer.HouseBillItem;

            if (locationsViewer.WasScanned)
            {
                
            }
            else
            {
             
            }


        }

        public static int? GetSlacFromUser(int piecesCount, int maxReleseSlac, bool isReadonly)
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = "Confirm slac";
            cntMsg.LabelDescription = "Enter the slac";
            cntMsg.LabelReference = string.Format("Number of pieces: {0}", piecesCount);
            cntMsg.PieceCount = maxReleseSlac;
            cntMsg.MinPiecesCount = 1;
            cntMsg.Readonly = isReadonly;
            cntMsg.MinPiecesCount = piecesCount;
          
            if (DialogResult.OK != cntMsg.ShowDialog()) return null;

            return cntMsg.PieceCount;
        }

        public static bool GetLocationFromUser(string title, string subTitle, CargoMatrix.Communication.ScannerUtilityWS.LocationTypes locationType, out string scannedText, out string locationName)
        {
            scannedText = null;
            locationName = null;

            ScanEnterPopup scanInfo = new ScanEnterPopup(locationType);
            scanInfo.Title = title;
            scanInfo.TextLabel = subTitle;
            DialogResult result = scanInfo.ShowDialog();

            if (result == DialogResult.OK)
            {
                scannedText = scanInfo.ScannedText;
                locationName = scanInfo.Text;
                return true;
            }

            return false;
        }

        public static int? GetPiecesCountFromUser(string header, string subHeder, int piecesCount, bool isReadonly)
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = header;
            cntMsg.LabelReference = subHeder;
            cntMsg.LabelDescription = "Enter pieces count";
            cntMsg.PieceCount = piecesCount;
            cntMsg.Readonly = isReadonly;

            if (DialogResult.OK != cntMsg.ShowDialog()) return null;

            return cntMsg.PieceCount;
        }

        public static bool ReleaseHousebills(string doorName, IEnumerable<IDischargeHouseBillItem> houseBills, IEnumerable<PieceItem> pieces)
        {
            if (houseBills.Count() <= 0) return false;
            
            var forklift = Forklift.Instance;
         
            forklift.ReleasedHouseBillsIds.Clear();

            Cursor.Current = Cursors.WaitCursor;

            SignatureCaptureForm signatureForm = new SignatureCaptureForm();

            var hbill = houseBills.First();
            byte[] driverImage = CargoDischarge.Instance.GetDriverImage(hbill.TaskID);

            if (driverImage != null)
            {
                signatureForm.DriverPhotoAsArray = driverImage;
            }

            signatureForm.Header = string.Format("{0} - {1}",
                                                 houseBills.Count() == 1 ? hbill.Reference() : "Multiple housebills",
                                                 doorName);
            signatureForm.DriversName = hbill.DriverFullName;
            signatureForm.CompanyName = hbill.CompanyName;

            signatureForm.Pieces = pieces.Count();

            Cursor.Current = Cursors.Default;

            DialogResult result = signatureForm.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                Cursor.Current = Cursors.WaitCursor;

                byte[] signature = signatureForm.SignatureAsBytes;
             
                TransactionStatus status = DropForkliftManually(signature, doorName, pieces, forklift.ID);

                if (status.TransactionStatus1 == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    return false;
                }

                RemoveFromFrokliftHouseBills(houseBills);

                var hbIds = from item in houseBills select item.HousebillId;

                forklift.ReleasedHouseBillsIds.AddRange(hbIds);
                forklift.ItemsCountList = status.TransactionRecords;

                Cursor.Current = Cursors.Default;

                return status.TransactionStatus1;
            }

            return false;
        }

        public static void MoveUnreleasedPieces(IEnumerable<IDischargeHouseBillItem> houseBiillItems)
        {
            CargoMatrix.UI.CMXMessageBox.Show("There are unreleased pieces on the forklift. Return them to location", "Message", CargoMatrix.UI.CMXMessageBoxIcon.Message, MessageBoxButtons.OK, DialogResult.OK);

            MovePiecesView movePiecesView = new MovePiecesView(houseBiillItems);

            movePiecesView.ShowDialog();
        }

        public static PieceItem CreateReleasePieceItem(PieceItem initialPiece, IDischargeHouseBillItem houseBill)
        {
            PieceItem pItem = new PieceItem();
            pItem.Slac = initialPiece.Slac;
            pItem.HouseBillId = houseBill.HousebillId;
            pItem.scanMode = CargoDischarge.ToWSCargoDoschargeScanModes(houseBill.ScanMode);
            pItem.PieceId = initialPiece.PieceId;
            pItem.TaskId = houseBill.TaskID;

            return pItem;
        }
        

        private static IEnumerable<IDischargeHouseBillItem> GetNotScannedHousebills(IEnumerable<IDischargeHouseBillItem> housebills)
        {
            return from item in housebills
                   where item.ReleaseStatus != ReleaseStatuses.Completed
                   select item;
        }

        private static TransactionStatus DropForkliftManually(byte[] signature, string doorName, IEnumerable<PieceItem> pieces, int forkliftID)
        {
            TransactionStatus status = CargoDischarge.Instance.FinalizeCargoDischarge(pieces.ToArray(), doorName, forkliftID, signature);

            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }

            return status;
        }

        private static void RemoveFromFrokliftHouseBills(IEnumerable<IDischargeHouseBillItem> houseBillItems)
        {
            var forkliftHbs = Forklift.Instance.HouseBillItems;

            var removeHbs = (from item in forkliftHbs
                            where (from inputHb in houseBillItems
                                   select inputHb.HousebillId).Contains(item.HousebillId)
                            select item).ToArray();

            foreach (var item in removeHbs)
            {
                forkliftHbs.Remove(item);
            }
        }
  
        public static System.Func<IDischargeHouseBillItem, IDischargeHouseBillItem, bool> RelatedItemCreteria( )
        {
            return (relatedItem, item) =>
                {
                    return relatedItem.DriverFullName.Equals(item.DriverFullName); //&&
                        //relatedItem.CompanyName.Equals(item.CompanyName);
                };
        }


    }
}
