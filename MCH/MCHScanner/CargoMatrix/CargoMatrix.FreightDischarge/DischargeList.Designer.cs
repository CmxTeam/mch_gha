﻿namespace CargoMatrix.FreightDischarge
{
    partial class DischargeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.panelHeader2.SuspendLayout();
            this.buttonScannedList = new CargoMatrix.UI.CMXCounterIcon();
            this.labelBlink = new CustomUtilities.CMXBlinkingLabel();

            // 
            // labelBlink
            // 
            this.labelBlink.Location = new System.Drawing.Point(45, 3);
            this.labelBlink.Name = "labelBlink";
            this.labelBlink.ForeColor = System.Drawing.Color.Red;
            this.labelBlink.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelBlink.Size = new System.Drawing.Size(155, 13);
            this.labelBlink.Text = "SCAN/SELECT HOUSEBILL";
            this.labelBlink.Blink = true;

            //
            // txtFilter
            //
            this.txtFilter = new CargoMatrix.UI.CMXTextBox();
            this.txtFilter.WatermarkText = "Enter Filter text";
            this.txtFilter.Location = new System.Drawing.Point(4, 19);
            this.txtFilter.Size = new System.Drawing.Size(190, 23);
            this.txtFilter.TextChanged += new System.EventHandler(searchBox_TextChanged);

            //
            // buttonScannedList
            //
            this.buttonScannedList.Location = new System.Drawing.Point(200, 10);
            this.buttonScannedList.Name = "buttonScannedList";
            this.buttonScannedList.Image = CargoMatrix.Resources.Skin.Forklift_btn;
            this.buttonScannedList.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over;
            this.buttonScannedList.Size = new System.Drawing.Size(36, 36);
            this.buttonScannedList.TabIndex = 1;


            this.panelHeader2.Controls.Add(this.labelBlink);
            this.panelHeader2.Controls.Add(this.txtFilter);
            panelHeader2.Controls.Add(this.buttonScannedList);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
            panelHeader2.Height = 51;
            panelHeader2.Visible = true;

         
            // 
            // DischargeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            
            this.panelHeader2.ResumeLayout(false);
            this.ResumeLayout(false);

            

        }
       
        #endregion
        private CustomUtilities.CMXBlinkingLabel labelBlink;
        private CargoMatrix.UI.CMXTextBox txtFilter;
        private CargoMatrix.UI.CMXCounterIcon buttonScannedList;
    }
}