﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoDischarge;
using CargoMatrix.Communication.DTO;
using CMXBarcode;
using CMXExtensions;
namespace CargoMatrix.FreightDischarge
{
    public partial class MovePiecesView : CargoMatrix.Utilities.MessageListBox
    {
        IEnumerable<IDischargeHouseBillItem> housebills;
        
        CargoMatrix.UI.BarcodeReader barcode;


        #region Constructors
        public MovePiecesView(IEnumerable<IDischargeHouseBillItem> housebills)
        {
            InitializeComponent();

            this.housebills = housebills;

            smoothListBoxBase1.MultiSelectEnabled = false;
            this.OneTouchSelection = false;
            this.HeaderText = "Return Unreleased Pieces View";
            this.HeaderText2 = "Select/Scan HAWBs";

            this.smoothListBoxBase1.IsSelectable = false;
            this.CancelEnabled = false;

            this.barcode = new CargoMatrix.UI.BarcodeReader();
            this.barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(Barcode_BarcodeReadNotify);

            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(MovePiecesView_LoadListEvent);
            this.OKClicked += new EventHandler(MovePiecesView_OKClicked);
            

            Cursor.Current = Cursors.Default;
        }
        #endregion

        #region Handlers
        void Barcode_BarcodeReadNotify(string barcodeData)
        {
            ScanObject scanObject = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            this.barcode.StartRead();

            switch (scanObject.BarcodeType)
            {
                case BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    this.Scan(scanObject.HouseBillNumber, scanObject.PieceNumber);

                    break;

                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);

                    CargoMatrix.UI.CMXMessageBox.Show("Ivalid barcode scanned", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    break;
            }
        } 

        void MovePiecesView_OKClicked(object sender, EventArgs e)
        {

        }

        void MovePiecesView_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.barcode.StartRead();

            this.PopulateContent();
        }

        void Combo_Click(object sender, EventArgs e)
        {
            var combo = (CustomListItems.ComboBox<DischargeHousebillItemState>)sender;

            this.ScanCountMode(combo);
        }

        void Combo_SelectionChanged(object sender, EventArgs e)
        {
            var combo = (CustomListItems.ComboBox<DischargeHousebillItemState>)sender;

            if (combo.SelectedIds != null && combo.SelectedIds.Count() > 0)
            {
                int selectedPicesId = combo.SelectedIds.First();

                this.ScanPiecesMode(combo, selectedPicesId);
            }
        }
        #endregion

        #region Overrides
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            this.barcode.StopRead();

            base.OnClosing(e);
        }

        #endregion

        #region Additional Methods

        private void PopulateContent()
        {
            smoothListBoxBase1.RemoveAll();

            foreach (var housebill in this.housebills)
            {
                List<CustomListItems.ComboBoxItemData> pieces = new List<CustomListItems.ComboBoxItemData>();

                if (housebill.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
                {
                    foreach (var hawbPiece in housebill.Pieces)
                    {
                        var pieceItem = new CustomListItems.ComboBoxItemData(string.Format("Piece {0} ({1})", hawbPiece.PieceNumber, hawbPiece.Slac), "Loc " + hawbPiece.Location, hawbPiece.PieceId);
                        pieceItem.isReadonly = true;

                        pieces.Add(pieceItem);
                    }
                }

                DischargeHousebillItemState status = new DischargeHousebillItemState(housebill, DischargeHousebillItemStateEnum.NonScanned);
                status.TotalPieces = housebill.Pieces.Length;

                Image icon = housebill.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece ? CargoMatrix.Resources.Skin.PieceMode :
                                                                                              CargoMatrix.Resources.Skin.countMode;

                string line2 = this.GetLine2Text(status.TotalPieces);

                CustomListItems.ComboBox<DischargeHousebillItemState> combo = new CustomListItems.ComboBox<DischargeHousebillItemState>(status, housebill.HousebillId, ((IHouseBillItem)housebill).Reference(), line2, icon, pieces);
                combo.ContainerName = "Forklift";
                combo.Expandable = housebill.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece;
                combo.ReadOnly = true;
                combo.IsSelectable = true;
                combo.SelectionChanged += new EventHandler(Combo_SelectionChanged);

                if (housebill.ScanMode == ScanModes.Count)
                {
                    combo.Click += new EventHandler(Combo_Click);
                }

                smoothListBoxBase1.AddItem(combo);
            }

            OkEnabled = false;

            if (smoothListBoxBase1.ItemsCount == 0)
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        private void Scan(string housebillNumber, int pieceNumber)
        {
            var dischargeHouseBillCombo = (
                                            from item in this.Items.OfType<CustomListItems.ComboBox<DischargeHousebillItemState>>()
                                            where string.Compare(item.Data.Item.HousebillNumber, housebillNumber) == 0
                                            select item
                                         ).FirstOrDefault();

            if (dischargeHouseBillCombo == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Housebill is not in the list", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            var itemStateData = dischargeHouseBillCombo.Data;

            if (itemStateData.Item.ScanMode == ScanModes.Count)
            {
                this.ScanCountMode(dischargeHouseBillCombo);
            }
            else
            {
                var selectedPicesId = (
                                        from item in dischargeHouseBillCombo.Data.Item.Pieces
                                        join listPiece in dischargeHouseBillCombo.Items on item.PieceId equals listPiece.id into listPiecesLeftJoin
                                        from lp in listPiecesLeftJoin
                                        where item.PieceNumber == pieceNumber
                                        select new { PieseID = lp.id }
                                      ).FirstOrDefault();



                if (selectedPicesId == null)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(string.Format("Piece # {0} {1} is not in the list", housebillNumber, pieceNumber), "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }

                this.ScanPiecesMode(dischargeHouseBillCombo, selectedPicesId.PieseID);
            }
        }

        private void ScanCountMode(CustomListItems.ComboBox<DischargeHousebillItemState> listItem)
        {
            var houseBill = listItem.Data.Item;

            if (houseBill.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece) return;

            int maxPieces = listItem.Data.TotalPieces;

            bool isReadonly = maxPieces == 1;

            int? piecesCount = DischargeItemFunctionallity.GetPiecesCountFromUser(houseBill.Reference(), "Number of pieces to drop", maxPieces, isReadonly);

            if (piecesCount.HasValue == false)
            {
                listItem.SelectedChanged(false);
                return;
            }


            this.barcode.StopRead();
            
            string scannedText;
            string locationName;

            bool result = DischargeItemFunctionallity.GetLocationFromUser("Location to drop", "Scan/select location",
                                                            CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location,
                                                            out scannedText, out locationName);

            if (result == false) return;

            var pieces = from piece in houseBill.Pieces.Take(piecesCount.Value)
                         select DischargeItemFunctionallity.CreateReleasePieceItem(piece, houseBill);

            var status  = CargoDischarge.Instance.DropPiecesIntoLocation(pieces.ToArray(),
                                                           houseBill.TaskID,
                                                           CargoMatrix.Communication.WSCargoDischarge.ScanTypes.Automatic,
                                                           locationName, Forklift.Instance.ID);

            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);

                this.barcode.StartRead();
                return;
            }
            Forklift.Instance.ItemsCountList = status.TransactionRecords;

            this.barcode.StartRead();

            if (piecesCount == maxPieces)
            {
                this.smoothListBoxBase1.RemoveItem(listItem);

                this.CloseForm(true);

                return;
            }

            var houseBiillItem = (from item in CargoDischarge.Instance.GetForkLiftHouseBills(new HouseBillSortCriteria(), Forklift.Instance.ID)
                                  where item.status == CargoMatrix.Communication.DTO.HouseBillStatuses.Completed &&
                                        item.HousebillId == houseBill.HousebillId
                                  select item).FirstOrDefault();

            if (houseBiillItem != null)
            {
                listItem.Data.Item = houseBiillItem;
            }

            listItem.Data.TotalPieces -= piecesCount.Value;
            listItem.Line2 = this.GetLine2Text(listItem.Data.TotalPieces);
            listItem.TextColor = Color.Red;
            listItem.Line2Color = Color.Red;
            listItem.UpdateSubItemsIcon(true, listItem.Data.TotalPieces);

            
        }

        private void ScanPiecesMode(CustomListItems.ComboBox<DischargeHousebillItemState> listItem, int pieceID)
        {
            this.barcode.StopRead();

            string scannedText;
            string locationName;

            bool result = DischargeItemFunctionallity.GetLocationFromUser("Location to undo scan", "Scan/select location",
                                                            CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location,
                                                            out scannedText, out locationName);

            this.barcode.StartRead();

            if (result == false)
            {
                listItem.SelectedChanged(false);
                return;
            }

            var houseBill = listItem.Data.Item;

            var piece = (from p in houseBill.Pieces
                        where p.PieceId == pieceID
                        select p).First();


            var pItem = DischargeItemFunctionallity.CreateReleasePieceItem(piece, houseBill);
         
            var piecesArray = new CargoMatrix.Communication.WSCargoDischarge.PieceItem[] { pItem };

            var status = CargoDischarge.Instance.DropPiecesIntoLocation(piecesArray,
                                                           houseBill.TaskID,
                                                           CargoMatrix.Communication.WSCargoDischarge.ScanTypes.Automatic,
                                                           locationName, Forklift.Instance.ID);


            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }
            Forklift.Instance.ItemsCountList = status.TransactionRecords;

            listItem.RemoveSubItem(pieceID);

            listItem.Data.TotalPieces--;
            listItem.Line2 = this.GetLine2Text(listItem.Data.TotalPieces);

            if (listItem.Items.Count() <= 0)
            {
                this.smoothListBoxBase1.Items.Remove(listItem);

                this.CloseForm(true);
            }
        }
        
        private void CloseForm(bool checkToClose)
        {
            if (checkToClose == false)
            {
                this.Close();
                return;
            }

            if (this.smoothListBoxBase1.Items.Count <= 0)
            {
                this.Close();
            }

        }

        private string GetLine2Text(int pieces)
        {
            return string.Format("Pieces: {0}", pieces);
        }

        #endregion
    }
}
