﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartDeviceTest.Scanner.ScannerExtended;

namespace SmartDeviceTest
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var result = Parser.Parse("70076349125");

            result = Parser.Parse("H1AOM275+Y0001+");

            result = Parser.Parse("PMC93068UA");

            result = Parser.Parse("L-SOMEULD");

            result = Parser.Parse("T-SOMETRUCK");

            result = Parser.Parse("S-SOMEScreenigArea");

            result = Parser.Parse("D-SOMEDOOR");

            result = Parser.Parse("B-SOMEAREA");

            result = Parser.Parse("U-SOMEUSER");
        }
    }
}