﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SmartDeviceTest
{
    public partial class CountMessageBox : CargoMatrix.UI.MessageBoxBase
    {
        //bool bFirstTimeLoad = false;
       
        public string HeaderText;
        int _pieceCount = -1;


        public CountMessageBox()
        {
            InitializeComponent();

            pictureBoxMenuBack.Image = Resources.Graphics.Skin.popup_Main_nav;
            pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;
            pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel;
            HeaderText = "Confirm Piece Move";
            
        }

       
      


        private void RemarksMessageBox_Load(object sender, EventArgs e)
        {


        }

     
        Font HeaderFont = new Font(FontFamily.GenericSerif, 8, FontStyle.Bold);
        SolidBrush HeaderBrush = new SolidBrush(Color.White);
        private void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawString(HeaderText, HeaderFont, HeaderBrush,3, 4);
        }

        private void pictureBoxOk_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok_over;
        }

        private void pictureBoxOk_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;
        }

        private void pictureBoxOk_Click(object sender, EventArgs e)
        {
            pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok_over;
            pictureBoxOk.Refresh();

            try
            {
                int temp = Convert.ToInt32(textBoxPieceCount.Text);
                if (temp > 0 && temp <= _pieceCount)
                {
                    _pieceCount = temp;

                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Piece count must be atleast 1 and atmost " + _pieceCount, "Invalid Count", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;
                    textBoxPieceCount.SelectAll();
                    textBoxPieceCount.Focus();
                    return;
                
                }

            }
            catch (FormatException)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid text entered, it should be numbers only.", "Invalid Text", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;
                textBoxPieceCount.SelectAll();
                textBoxPieceCount.Focus();
                return;
            }
            DialogResult = DialogResult.OK;
            

        }

        private void pictureBoxCancel_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel_over;

        }

        private void pictureBoxCancel_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel;
        }

        private void pictureBoxCancel_Click(object sender, EventArgs e)
        {
            pictureBoxCancel.Image = Resources.Graphics.Skin.nav_cancel_over;
            pictureBoxCancel.Refresh();

            DialogResult = DialogResult.Cancel;
           
        }

     
        private void textLocationCode_KeyDown(object sender, KeyEventArgs e)
        {
            
            //if (e.KeyCode == Keys.Enter)
            //{
            //    pictureBoxOk_Click(sender, e);
            //    pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;    

            //}
        }
        public string LabelReference { get; set; }
        public string LabelDescription { get; set; }
        public new DialogResult ShowDialog()
        {
            if (LabelReference == string.Empty)
                labelReference.Text = "Total pieces in location \'" + TextLocation + "\': " + PieceCount;
            else
                labelReference.Text = LabelReference;
            
            if (LabelDescription != string.Empty)
                labelDescription.Text = LabelDescription;
            
            if (PieceCount != int.MaxValue)
            {
                textBoxPieceCount.Text = PieceCount.ToString();
                textBoxPieceCount.SelectAll();
                textBoxPieceCount.Focus();
            }
            return base.ShowDialog();
        }
        //private void textLocationCode_KeyUp(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        pictureBoxOk.Image = Resources.Graphics.Skin.nav_ok;                

        //    }
        //}

        public string TextLocation
        {
            set;
            get;



        }
        public int PieceCount
        {
            set
            {
                _pieceCount = value;
            }
            get
            {
                return _pieceCount;
            }

        }

        //private void textBoxPieceCount_KeyPress(object sender, KeyPressEventArgs e)
        //{

        //}
        
       
    }
}