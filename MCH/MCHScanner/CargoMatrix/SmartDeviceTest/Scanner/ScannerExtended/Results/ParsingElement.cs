﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SmartDeviceTest.Scanner.ScannerExtended.Results
{
    public class ParsingElement
    {
        public string Name { get; set; }
        public string Regex { get; set; }
        public int Order { get; set; }

        public ParsingElement() { }
    }
}
