﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SmartDeviceTest.Scanner.ScannerExtended.Results
{
    public enum ResultTypeEnum
    {
        NA,
        HouseBill,
        MasterBill,
        Uld,
        Door,
        Area,
        Truck,
        User,
        ScreeningArea
    }
}
