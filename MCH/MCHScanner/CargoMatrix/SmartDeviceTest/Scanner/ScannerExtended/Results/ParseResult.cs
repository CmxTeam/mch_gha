﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SmartDeviceTest.Scanner.ScannerExtended.Results
{
    public class ParseResult
    {
        string barcode;
        ResultTypeEnum resultType;
        protected string iD;
        protected ParsingElement[] elements;
        private string delimeter;
        private string prefix;

        public string ID
        {
            get { return iD; }
        }

        public ResultTypeEnum ResultType
        {
            get { return resultType; }

        }

        public string Barcode
        {
            get { return barcode; }
        }

        public string Delimeter
        {
            get { return delimeter; }
        }

        public string Prefix
        {
            get { return prefix; }
        }

        public ParseResult(string barcode, ResultTypeEnum resultType, ParsingPattern.PatternValue patternValue)
        {
            this.barcode = barcode;
            this.resultType = resultType;
            this.elements = patternValue.Elements;
        }


        public virtual ParseResult Parse()
        {
            string tempBarcode = this.barcode;

            foreach (var element in this.elements.OrderBy(el => el.Order))
            {
                if (string.IsNullOrEmpty(tempBarcode) == true) return this;

                var match = Regex.Match(tempBarcode, string.Format("^{0}", element.Regex));

                if (match.Success == true)
                {
                    this.EmbedValue(match.Value, element.Name);
                }

                tempBarcode = tempBarcode.Remove(match.Index, match.Length);
            }

            return this;

        }

        protected virtual void EmbedValue(string value, string name)
        {
            switch (name.ToLower())
            {
                case "id":
                    this.iD = value;
                    break;

                case "prefix":
                    this.prefix = value;
                    break;

                case "delimiter":
                    this.delimeter = value;

                    break;
            }
        }
    }
}
