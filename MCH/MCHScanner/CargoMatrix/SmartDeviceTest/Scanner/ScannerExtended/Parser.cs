﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmartDeviceTest.Scanner.ScannerExtended.Results;
using System.Text.RegularExpressions;
using SmartDeviceTest.Scanner.ScannerExtended.Data;

namespace SmartDeviceTest.Scanner.ScannerExtended
{
    public static class Parser
    {
        static Dictionary<ResultTypeEnum, ParsingPattern> patterns;

        static Parser()
        {
            var barcodePatterns = DataSource.GetBarcodePatterns();

            patterns = new Dictionary<ResultTypeEnum, ParsingPattern>();

            barcodePatterns.ForEach(pattern => Parser.patterns.Add(pattern.ResultType, pattern));
        }


        public static ParseResult Parse(string barcode)
        {
            foreach (var parsing in patterns.Values)
            {
                foreach (var pattern in parsing.Values)
                {
                    var match = Regex.Match(barcode, pattern.Regex);

                    if (match.Success == true)
                    {
                        return ParseExact(parsing.ResultType, barcode, pattern);
                    }
                }
            }

            return null;
        }

        private static ParseResult ParseExact(ResultTypeEnum resultType, string barcode, ParsingPattern.PatternValue parsingValue)
        {
            switch (resultType)
            {
                case ResultTypeEnum.MasterBill:

                    return new MasterBillResult(barcode, parsingValue).Parse();

                case ResultTypeEnum.HouseBill:

                    return new HouseBillResult(barcode, parsingValue).Parse();

                case ResultTypeEnum.Area:
                case ResultTypeEnum.Door:
                case ResultTypeEnum.Truck:
                case ResultTypeEnum.ScreeningArea:
                case ResultTypeEnum.Uld:
                case ResultTypeEnum.User:
                    return new ParseResult(barcode, resultType, parsingValue).Parse();

                default: return new ParseResult(barcode, ResultTypeEnum.NA, null);
            }
        }
    }
}
