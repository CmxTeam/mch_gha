﻿using System;
using CMXExtensions;
namespace CustomListItems
{
    [Obsolete("Use MawbRenderListItem")]
    internal partial class MawbListItem : CustomListItems.ExpandableListItem
    {
        public CargoMatrix.Communication.DTO.IMasterBillItem Mawb { get; set; }
        public MawbListItem(CargoMatrix.Communication.DTO.IMasterBillItem mawb)//:base(mawb.MasterBillNumber)
        {

            InitializeComponent();
            Mawb = mawb;
            this.title.Text = Mawb.Reference();


            labelLine2.Text = Mawb.Line2();
            labelLine3.Text = Mawb.Line3();

            switch (Mawb.Status)
            {
                case CargoMatrix.Communication.DTO.MasterbillStatus.Open:
                case CargoMatrix.Communication.DTO.MasterbillStatus.Pending:
                    Logo = CustomListItemsResource.Clipboard;
                    break;
                case CargoMatrix.Communication.DTO.MasterbillStatus.InProgress:
                    Logo = CustomListItemsResource.History;
                    break;
                case CargoMatrix.Communication.DTO.MasterbillStatus.Completed:
                    Logo = CustomListItemsResource.Clipboard_Check;
                    break;

            }
            this.CutoffTime = mawb.CutOff;
            this.LabelConfirmation = "Please enter mawb number";
            panelIndicators.Flags = mawb.Flags;
            panelIndicators.PopupHeader = mawb.Reference();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

        }
        
        protected override bool ButtonEnterValidation()
        {
            return string.Equals(Mawb.MasterBillNumber, TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }
    }
}