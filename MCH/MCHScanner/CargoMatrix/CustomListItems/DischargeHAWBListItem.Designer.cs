﻿namespace CustomListItems
{
    partial class DischargeHAWBListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            this.SuspendLayout();

            this.labelLine4 = new System.Windows.Forms.Label();
            this.labelLine4.Location = new System.Drawing.Point(41, 47);
            this.labelLine4.Size = new System.Drawing.Size(196, 13);
            this.labelLine4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);

            this.Controls.Add(this.labelLine4);

            this.Size = new System.Drawing.Size(240,78);
            this.panelIndicators.Location = new System.Drawing.Point(41, 59);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }

        System.Windows.Forms.Label labelLine4;



        #endregion
    }
}
