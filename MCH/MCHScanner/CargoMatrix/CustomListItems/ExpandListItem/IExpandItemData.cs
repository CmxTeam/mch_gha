﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomListItems
{
    public interface IExpandItemData
    {
        int ID { get; }
        string TitleLine { get; set; }
        string DescriptionLine { get; set; }
        bool IsSelected { get; set; }
        bool IsSelectable { get; set; }
        bool IsReadonly { get; set; }
        bool IsChecked { get; set; }
        bool IsLast { get; set; }
        bool Expandable { get; set; }
        IExpandItemData ParentItemData { get; set; }
        void RefreshView();
    }
}
