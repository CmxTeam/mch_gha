﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmoothListbox.ListItems;
using System.Windows.Forms;
using SmoothListbox;

namespace CustomListItems
{
    public class ExpandListSubItem : ListItem, IExpandItemData
    {
        public Label title;
        public OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        public event EventHandler ButtonClick;
        public event EventHandler<ExpandItemEventArgs> ItemSelected;
        public Label labelLoc;
        private bool selectable;
        private CargoMatrix.UI.CMXPictureButton buttonDelte;
        private bool isLast;
        IExpandItemData data;
        SmoothListbox.SmoothListBoxBase parentListBox;


        public override string Text
        {
            get
            {
                return this.title.Text;
            }
            set
            {
                this.title.Text = value;
                this.data.TitleLine = value;
            }
        }

        public ExpandListSubItem(IExpandItemData data)
        {
            InitializeComponent();
            panelBaseLine.Visible = false;
            this.data = data;

            RefreshView();
        }

        public override void SelectedChanged(bool isSelected)
        {
            base.Focus(isSelected);
            m_selected = isSelected;
            selectionFixer(isSelected);
            SetIcon();
            if(ItemSelected != null)
                ItemSelected(this,new ExpandItemEventArgs(ID,isSelected));
        }

        private void selectionFixer(bool selected)
        {
            if(parentListBox == null)
                FindParentListBox();
            if (parentListBox == null)
                return;

            parentListBox.selectedItemsMap[this] = selected;

        }
        private void FindParentListBox()
        {
            Control control = this.Parent;
            while (control != null)
            {
                if (control is SmoothListBoxBase)
                {
                    if ((control as SmoothListBoxBase).selectedItemsMap.ContainsKey(this))
                    {
                        parentListBox = control as SmoothListBoxBase;
                        break;
                    }
                }
                control = control.Parent;
            }
        }
        private void SetIcon()
        {
            if (IsSelectable)
            {
                if (this.IsSelected)
                {
                    if (isLast)
                        itemPicture.Image = CustomListItemsResource.DottedLineHalfSel;
                    else
                        itemPicture.Image = CustomListItemsResource.DottedLineFullSel;
                }
                else
                {
                    if (isLast)
                        itemPicture.Image = CustomListItemsResource.DottedLineHalfUnsel;
                    else
                        itemPicture.Image = CustomListItemsResource.DottedLineFullUnsel;
                }
            }
            else
            {
                if (IsChecked)
                {
                    if (isLast)
                        itemPicture.Image = CustomListItemsResource.DottedLineHalfSelDisabled;
                    else
                        itemPicture.Image = CustomListItemsResource.DottedLineFullSelDisabled;

                }
                else
                {
                    if (isLast)
                        itemPicture.Image = CustomListItemsResource.DottedLineHalfDisabled;
                    else
                        itemPicture.Image = CustomListItemsResource.DottedLineFullDisabled;
                }
            }
        }

        public void RefreshIcon()
        {
            this.SetIcon();
        }

        public override void Focus(bool focused)
        {
            foreach (Control control in Controls)
            {
                if (control is Splitter)
                    continue;
                control.BackColor = BackColor;
            }
        }


        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDelte = new CargoMatrix.UI.CMXPictureButton();
            this.labelLoc = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.title.Location = new System.Drawing.Point(45, 4);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(160, 15);
            this.title.Text = "<title>";
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(1, 0);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(40, 40);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDelte
            // 
            this.buttonDelte.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDelte.Image = CargoMatrix.Resources.Skin.cc_trash;
            this.buttonDelte.Location = new System.Drawing.Point(192, 4);
            this.buttonDelte.Name = "buttonDelte";
            this.buttonDelte.PressedImage = CargoMatrix.Resources.Skin.cc_trash_over;
            this.buttonDelte.Size = new System.Drawing.Size(32, 32);
            this.buttonDelte.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDelte.TransparentColor = System.Drawing.Color.White;
            this.buttonDelte.Click += new System.EventHandler(this.buttonDelte_Click);
            // 
            // labelLoc
            // 
            this.labelLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLoc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLoc.Location = new System.Drawing.Point(45, 21);
            this.labelLoc.Name = "labelLoc";
            this.labelLoc.Size = new System.Drawing.Size(160, 15);
            // 
            // ComboBoxItem
            // 
            this.Controls.Add(this.buttonDelte);
            this.Controls.Add(this.labelLoc);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Name = "ComboBoxItem";
            this.Size = new System.Drawing.Size(226, 40);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }

        void buttonDelte_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null)
                ButtonClick(this, e);
        }


        #region IExpandItemData Members


        public string TitleLine
        {
            get
            {
                return title.Text;
            }
            set
            {
                title.Text = value;
                this.data.TitleLine = value;
            }
        }

        public string DescriptionLine
        {
            get
            {
                return labelLoc.Text;
            }
            set
            {
                labelLoc.Text = value;
                this.data.DescriptionLine = value;
            }
        }
        public new bool IsSelected
        {
            get { return base.IsSelected; }
            set
            {
                
                this.data.IsSelected = value;
            }
        }

        public bool IsSelectable
        {
            get
            {
                return this.data.IsSelectable;
            }
            set
            {
                this.data.IsSelectable = value;
            }
        }

        public bool IsReadonly
        {
            get
            {
                return !buttonDelte.Visible;
            }
            set
            {
                buttonDelte.Visible = !value;
                this.data.IsReadonly = value;
            }
        }

        public bool IsChecked 
        {
            get { return this.data.IsChecked; }
            set 
            {
                this.data.IsChecked = value;
            } 
        }

        public bool IsLast
        {
            get { return isLast; }
            set
            {
                isLast = value;
                this.data.IsLast = value;
            }
        }

        /// <summary>
        /// Always returns false. Setter does nothing.
        /// </summary>
        public bool Expandable 
        {
            get { return false; }

            set { }
        }

        public IExpandItemData ParentItemData
        {
            get { return this.data.ParentItemData; }
            set
            {
                this.data.ParentItemData = value;
            }
        }

        public void RefreshView()
        {
            this.selectable = this.data.IsSelectable;
            title.Text = this.data.TitleLine;
            labelLoc.Text = this.data.DescriptionLine;
            base.ID = this.data.ID;
            this.IsChecked = this.data.IsChecked;
            this.IsSelected = this.data.IsSelected;
            IsLast = this.data.IsLast;
            this.IsReadonly = this.data.IsReadonly;
            SelectedChanged(this.data.IsSelected);
        }

        #endregion
    }
}
