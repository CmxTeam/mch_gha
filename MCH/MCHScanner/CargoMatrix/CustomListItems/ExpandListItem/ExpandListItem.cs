﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using SmoothListbox;
using System.Linq;

namespace CustomListItems
{
    public partial class ExpandListItem : ListItem, IExpandItemData
    {
        public event EventHandler ButtonClick;
        public event EventHandler<ExpandItemEventArgs> SubItemSelected;
        public event EventHandler<SubItemButtonClickEventArgs> SubItemButtonClick;
        
        protected int m_previousHeight;
        protected List<IExpandItemData> m_items;
        protected SmoothListBoxBase m_smoothList;
        protected bool m_firstTimeSelection = false;

        private bool isExpanded = false;
        private SmoothListBoxBase parentListBox;
        private bool isLast = false;
        private IExpandItemData data;

        #region Properties
        public IEnumerable<IExpandItemData> Items
        {
            get
            {
                if (m_firstTimeSelection == false)
                    foreach (var item in m_items)
                    {
                        yield return item;
                    }
                else
                {
                    foreach (ExpandListSubItem item in m_smoothList.Items)
                    {
                        yield return item;
                    }
                    //return (from item in m_smoothList.Items.OfType<ListItem<IExpandItemData>>()
                    //        select item.ItemData).ToList<IExpandItemData>();
                }
            }
        }

        public bool EnableSelectBehavior { get; set; }

        public IEnumerable<IExpandItemData> SelectedItems
        {
            get
            {
                foreach (var item in this.Items)
                {
                    if (item.IsSelected)
                        yield return item;
                }
            }
        }

        public IEnumerable<IExpandItemData> CheckedItems
        {
            get
            {
                foreach (var item in this.Items)
                {
                    if (item.IsChecked)
                        yield return item;
                }
            }
        }

        public Color TitleForeColor
        {
            get
            {
                return this.labelHeading.ForeColor;
            }
            set
            {
                this.labelHeading.ForeColor = value;
            }
        }

        public Color DescriptionForeColor
        {
            get
            {
                return this.title.ForeColor;
            }
            set
            {
                this.title.ForeColor = value;
            }
        }
        
        #endregion

        #region Constructors
        public ExpandListItem(IExpandItemData data, Image image)
        {
            InitializeComponent();
            this.data = data;
            m_previousHeight = this.Height;
            itemPicture.Image = image;
            labelHeading.Text = data.TitleLine;
            this.title.Text = data.DescriptionLine;
            pictureBoxDottedLine.Image = CustomListItemsResource.DottedLineVertical;
            this.selectionColor = Color.Gainsboro;
            this.m_items = new List<IExpandItemData>();
            this.m_smoothList = new SmoothListBoxBase();
            EnableSelectBehavior = true;
            if (this.data.IsReadonly)
            {
                buttonDelte.Visible = false;
                buttonCollapse.Location = buttonDelte.Location;
            }
            else
            {
                buttonDelte.Visible = true;
                buttonCollapse.Left = 162;
            }
            if (this.data.IsSelected)
                this.SelectedChanged(this.data.IsSelected);
            this.m_smoothList.IsSelectable = this.data.IsSelectable;
            this.ID = data.ID;
        }

        public ExpandListItem(IExpandItemData data)
            : this(data, null)
        { }

        public ExpandListItem(int ID, Image image)
            : this(new ExpenListItemData(ID), image)
        {
        }

        public ExpandListItem(int ID)
            : this(new ExpenListItemData(ID), null)
        {
        } 
        #endregion

        #region Handlers
        void item_ItemSelected(object sender, ExpandItemEventArgs e)
        {
            if (this.IsSelectable == false) return;
            
            if (EnableSelectBehavior)
            {
                bool newState = SelectedItems.Count() == Items.Count();
                base.Focus(newState);
                m_selected = newState;
                this.data.IsSelected = newState;
            }

            if (SubItemSelected != null)
            {
                e.ParetntId = this.ID;
                SubItemSelected(this, e);
            }
        }

        public override void SelectedChanged(bool isSelected)
        {
            base.Focus(isSelected);
            m_selected = isSelected;
            this.data.IsSelected = isSelected;

            if (EnableSelectBehavior)
            {
                SelectAll(isSelected);
            }

        }

        void m_smoothList_ListItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

        }

        void item_ButtonClick(object sender, EventArgs e)
        {
            var subItemClick = this.SubItemButtonClick;

            if(subItemClick != null)
            {
                if(sender is ExpandListSubItem)
                {
                    var args = new SubItemButtonClickEventArgs((ExpandListSubItem)sender);

                    subItemClick(this, args);
                }
            }
        }

        private void ComboBox_Resize(object sender, EventArgs e)
        {
            if (m_smoothList != null)
            {
                m_smoothList.Width = ClientSize.Width - 8;
                m_smoothList.Left = 4;
            }
        }

        void buttonCollapse_Click(object sender, System.EventArgs e)
        {

            isExpanded = !isExpanded;
            if (isExpanded)
                Expand();
            else
                Collapse();
            RefershLayout();
        }

        private void buttonDelte_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null)
                ButtonClick(this, new EventArgs());
        }
        #endregion

        #region Functionality
        public void AddExpandItem(IExpandItemData subItem)
        {
            List<IExpandItemData> subItems = new List<IExpandItemData>(){ subItem };

            this.AddExpandItemsRange(subItems);
        }

        public void AddExpandItemsRange(IEnumerable<IExpandItemData> subItems)
        {
            if (subItems.Count() <= 0) return;

            var addList = subItems.ToList();

            addList.ForEach(item => item.ParentItemData = this);
           
            addList.Last().IsLast = true;

            var lastItem = this.m_items.LastOrDefault();

            if (lastItem != null)
            {
                lastItem.IsLast = false;
            }

            this.m_items.AddRange(addList);

            if (this.m_firstTimeSelection == false) return;

            m_smoothList.SuspendLayout();

            if(lastItem != null)
            {
                var lastlistItem = (from item in m_smoothList.Items.OfType<IExpandItemData>()
                                   where item.ID == lastItem.ID
                                   select item).First();
                
                lastlistItem.RefreshView();
            }

            foreach (var subItem in addList)
            {
                ExpandListSubItem item = new ExpandListSubItem(subItem);
                item.ButtonClick += new EventHandler(item_ButtonClick);
                item.ItemSelected += new EventHandler<ExpandItemEventArgs>(item_ItemSelected);
                m_smoothList.AddItem2(item);
            }

            m_smoothList.Width = ClientSize.Width - 12;
            m_smoothList.Left = 16;
            m_smoothList.Top = m_previousHeight;

            m_smoothList.LayoutItems();

            m_smoothList.Height = m_smoothList.itemsPanel.Height;

            m_smoothList.ResumeLayout();

            if (EnableSelectBehavior && this.IsSelected == true)
            {
                this.SelectItems(addList.Select(item => item.ID), true);
            }

            if (this.isExpanded == true)
            {

                AnimateCombo(Height, m_previousHeight + m_smoothList.Height + 5);
            }
            else
            {
                AnimateCombo(Height, m_previousHeight);
            }
        }

        public void AddExpandItemsRange(IEnumerable<ExpenListItemData> subItems)
        {
            this.AddExpandItemsRange(subItems.OfType<IExpandItemData>());
        }

        public void AddItem<K>(K subItem) where K : Control, IExpandItemData
        {
            List<K> list = new List<K>() { subItem };

            this.AddRange(list);
        }

        public void AddRange<K>(IEnumerable<K> subItems) where K : Control, IExpandItemData
        {
            if (subItems.Count() <= 0) return;

            var addList = subItems.ToList();

            addList.ForEach(item => item.ParentItemData = this);

            addList.Last().IsLast = true;

            var lastItem = this.m_items.LastOrDefault();

            if (lastItem != null)
            {
                lastItem.IsLast = false;
            }

            this.m_items.AddRange(addList.OfType<IExpandItemData>());

            //if (this.m_firstTimeSelection == false) return;

            m_smoothList.SuspendLayout();

            if (lastItem != null)
            {
                var lastlistItem = (from item in m_smoothList.Items.OfType<IExpandItemData>()
                                    where item.ID == lastItem.ID
                                    select item).First();

                lastlistItem.RefreshView();
            }

            foreach (var subItem in addList)
            {
                m_smoothList.AddItem2(subItem);
            }

            m_smoothList.Width = ClientSize.Width - 12;
            m_smoothList.Left = 16;
            m_smoothList.Top = m_previousHeight;

            m_smoothList.LayoutItems();

            m_smoothList.Height = m_smoothList.itemsPanel.Height;

            m_smoothList.ResumeLayout();

            if (EnableSelectBehavior && this.IsSelected == true)
            {
                this.SelectItems(addList.Select(item => item.ID), true);
            }

            //if (this.isExpanded == true)
            //{

            //    AnimateCombo(Height, m_previousHeight + m_smoothList.Height + 5);
            //}
            //else
            //{
            //    AnimateCombo(Height, m_previousHeight);
            //}
        }

        public void RemoveItem(IExpandItemData item)
        {
            RemoveItem(item.ID);
            item.ParentItemData = null;
        }

        public void RemoveItem(int id)
        {
            if (m_firstTimeSelection == false)
            {
                m_items.RemoveAll(i => i.ID == id);
                if (m_items.Count > 0)
                    m_items[m_items.Count - 1].IsLast = true;
            }
            else
            {
                var match = m_smoothList.Items.OfType<IExpandItemData>().FirstOrDefault(i => i.ID == id);
              
                if (match == null) return;

                match.ParentItemData = null;

                Control matchControl = match as Control;
                m_smoothList.Items.Remove(matchControl);

                if (m_smoothList.Items.Count > 0)
                    (m_smoothList.Items[m_smoothList.Items.Count - 1] as IExpandItemData).IsLast = true;

                m_smoothList.Height -= matchControl.Height;
                if (isExpanded)
                    this.Height -= matchControl.Height;
            }

            m_smoothList.LayoutItems();
        }

        public void SelectItems(IEnumerable<int> ids, bool seleced)
        {
            foreach (var item in Items)
            {
                if (ids.Contains<int>(item.ID))
                {
                    item.IsSelected = seleced;
                    item.RefreshView();
                }
            }
        }

        public void SelectAll(bool selected)
        {
            foreach (var item in Items)
            {
                item.IsSelected = selected;
                item.RefreshView();
            }
        }
        
        #endregion

        #region Additional Methods

        protected void FirstTimeLoad()
        {
            if (m_items != null)
            {
                if (m_firstTimeSelection == false)
                {
                    m_firstTimeSelection = true;

                    this.SuspendLayout();
                    Cursor.Current = Cursors.WaitCursor;
                    this.m_smoothList.ListItemClicked += new ListItemClickedHandler(m_smoothList_ListItemClicked);
                    m_smoothList.MultiSelectEnabled = true;
                    m_smoothList.UnselectEnabled = true;


                    if (m_items.Count > 0)
                    {
                        m_items.Last().IsLast = true;
                        foreach (var mitem in m_items)
                        {
                            if (m_smoothList.Items.OfType<Control>().FirstOrDefault<Control>(itm => object.ReferenceEquals(itm, mitem)) != null)
                                continue;
                            ExpandListSubItem item = new ExpandListSubItem(mitem);
                            item.ButtonClick += new EventHandler(item_ButtonClick);
                            item.ItemSelected += new EventHandler<ExpandItemEventArgs>(item_ItemSelected);
                            m_smoothList.AddItem2(item);

                            item.SelectedChanged(mitem.IsSelected);
                        }

                        //if (m_smoothList.Items.OfType<Control>().FirstOrDefault<Control>(itm => object.ReferenceEquals(itm, m_items[i])) == null)
                        //{

                        //    m_items[i].IsLast = true;
                        //    ExpandListSubItem lastitem = new ExpandListSubItem(m_items[i]);

                        //    lastitem.ButtonClick += new EventHandler(item_ButtonClick);
                        //    lastitem.ItemSelected += new EventHandler<ExpandItemEventArgs>(item_ItemSelected);
                        //    m_smoothList.AddItem2(lastitem);
                        //}
                    }



                    m_smoothList.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                    m_smoothList.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;



                    m_smoothList.Width = ClientSize.Width - 12;
                    m_smoothList.Left = 16;
                    m_smoothList.Top = m_previousHeight;

                    m_smoothList.LayoutItems();

                    m_smoothList.Height = m_smoothList.itemsPanel.Height;

                    Controls.Add(m_smoothList);

                    this.ResumeLayout();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void Collapse()
        {
            AnimateCombo(Height, m_previousHeight);

            pictureBoxDottedLine.Visible = false;
            this.buttonCollapse.Image = CustomListItemsResource.Collapse_Down;
            this.buttonCollapse.PressedImage = CustomListItemsResource.Collapse_Down;
        }

        private void AnimateCombo(int start, int end)
        {
            int step = (end - start) / 4;
            for (int i = 0; i < 4; i++)
            {
                this.Height += step;
                RefershLayout();
                Application.DoEvents();
            }
            this.Height = end;
        }

        private void Expand()
        {
            if (m_firstTimeSelection == false)
                FirstTimeLoad();
            AnimateCombo(Height, m_previousHeight + m_smoothList.Height + 5);
            this.buttonCollapse.Image = CustomListItemsResource.Collapse_Up;
            this.buttonCollapse.PressedImage = CustomListItemsResource.Collapse_Up;
            pictureBoxDottedLine.Visible = true;
            ScrollIntoView();
        }

        private void RefershLayout()
        {
            if (parentListBox == null)
                FindParentListBox();
            parentListBox.LayoutItems();
            parentListBox.RefreshScroll();
        }

        private void FindParentListBox()
        {
            Control control = this.Parent;
            while (control != null)
            {
                if (control is SmoothListBoxBase)
                {
                    if ((control as SmoothListBoxBase).selectedItemsMap.ContainsKey(this))
                    {
                        parentListBox = control as SmoothListBoxBase;
                        break;
                    }
                }
                control = control.Parent;
            }
        }

        private void ScrollIntoView()
        {
            if (parentListBox == null)
                FindParentListBox();
            parentListBox.MoveControlToTop(this);
        } 
        #endregion
        
        #region IExpandItemData Members


        public string TitleLine
        {
            get
            {
                return labelHeading.Text;
            }
            set
            {
                labelHeading.Text = value;
                this.data.TitleLine = value;
            }
        }

        public string DescriptionLine
        {
            get
            {
                return title.Text;
            }
            set
            {
                title.Text = value;
                this.data.DescriptionLine = value;
            }
        }

        public new bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                SelectedChanged(value);
            }
        }

        public bool IsSelectable 
        {
            get
            {
                return this.data.IsSelectable;
            }
            set
            {
                this.data.IsSelectable = value;

                this.m_smoothList.IsSelectable = value;
            } 
        }

        public bool IsReadonly
        {
            get
            {
                return this.data.IsReadonly;
            }
            set
            {
                if (value)
                {
                    buttonDelte.Visible = false;
                    buttonCollapse.Location = buttonDelte.Location;
                }
                else
                {
                    buttonDelte.Visible = true;
                    buttonCollapse.Left = 162;
                }

                this.data.IsReadonly = value;
            }
        }
        
        [Obsolete("Illegal to use")]
        public bool IsChecked
        {
            get
            {
                return false;
            }
            set
            {

            }
        }

        public bool IsLast
        {
            get
            {
                return isLast;
            }
            set
            {
                isLast = value;
                this.data.IsLast = value;
            }
        }

        public bool Expandable
        {
            get { return this.buttonCollapse.Visible; }

            set 
            { 
                this.buttonCollapse.Visible = value;
                this.data.Expandable = value;
            }
        }

        public IExpandItemData ParentItemData 
        {
            get { return this.data.ParentItemData; }

            set { this.data.ParentItemData = value; }
        }
        

        public void RefreshView()
        {
           // this.IsReadonly = data.IsReadonly;
        }

        #endregion
       
    }

    public partial class ExpandListItem<T> : ExpandListItem
    {
        public T CustomData
        {
            get;
            set;
        }
        
        
        public ExpandListItem(T customData, IExpandItemData data, Image image)
            : base(data, image)
        {
            this.CustomData = customData;
        }

        public ExpandListItem(T customData, int ID, Image image)
            : base(ID, image)
        {
            this.CustomData = customData;
        }

        public ExpandListItem(T customData, IExpandItemData data)
            : this(customData, data, null)
        {

        }

        public ExpandListItem(T customData, int ID)
            : this(customData, ID, null)
        {

        }
    }
}
