﻿using SmoothListbox.ListItems;

namespace CustomListItems
{
    public abstract class RenderItemBase<T> : SmoothListbox.ListItems.RenderItemBase, IDataHolder<T>
    {
        DataHolder<T> itemData;
        public RenderItemBase(T item)
        {
            itemData = new DataHolder<T>(item);
        }
        public T ItemData
        {
            get
            {
                return itemData.ItemData;
            }
            set
            {
                OnBeforeDataItemChanged(value);
                
                itemData.ItemData = value;

                OnDataItemChanged();
            }
        }

        public virtual bool OnBeforeDataItemChanged(T item)
        {
            return true;
        }
        ///<summary>
        ///
        ///</summary>
        public virtual void OnDataItemChanged()
        { }
    }

}
