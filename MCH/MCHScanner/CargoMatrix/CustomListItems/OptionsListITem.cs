﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;


namespace CustomListItems
{
    public partial class OptionsListITem : SmoothListbox.ListItems.StandardListItem, SmoothListbox.IOptionsListItem// UserControl
    {
        public enum OptionItemID
        {
            EXIT = 101,
            LOGOUT,
            REFRESH,
            MAIN_MENU,
            ABOUT,
            COMPLETE_TASK,
            COMPLETE_TASK_LATER,
            EXIT_WITHOUT_SAVING,
            MANUAL_LOCATION,
            FILTER,
            SELECT_ALL,
            SELECT_NONE,
            REMOVE_ALL,
            DELETE_TASK,
            MANUAL_PEICE_ENTRY,
            UNKNOWN,
            PRINT_HAWB_LABEL,
            PRINT_MAWB_LABEL,
            STAGE_ULD,
            PRINT_ULD_LABEL,
            VISUAL_INSPECTION,
            RESET_SCREENING,
            CANCEL_SCREENING,
            FIND,
            SCREENING_DIRECTION,
            SWITCH_MODE,
            TASK_SETTINGS,
            INVENTORY_REPORT,
            INVENTORY_REPORT_LOC,
            INVENTORY_REPORT_CURRENT,
            SHARE_TASK,
            FINALIZE_CONSOL,
            FINALIZE_INVENTORY,
            FINALIZE_LOAD_TRUCK,
            FINALIZE_MEASUREMENTS,
            VIEW_MAWB_LIST,
            SELECTION_TYPE,
            VIEW_HAWB_LIST,
            CANCEL_TASK,
            METRICS_SYSTEM,
            DEVICES,
            SHOW_LOCATIONS,
            PRINT_MEASUREMENTS_LABELS,
            PRINT_TRUCK_LABEL,
            SETUP_MEASUREMENTS_PRINTING,
            UNDO_DSCHARGE_SCAN,
            ENTER_HAWB_NO,
            VIEW_ALL_LOCATIONS,
            UTILITIES,
            ADD_MEASUREMENTS,
            EDIT_MEASURMENTS,
            MESSAGES,
            ENTER_BARCODE,
            FINALIZETASK,
        }
        public string DescriptionLine { get { return LabelUser.Text; } set { LabelUser.Text = value; } }
        public string Title { get { return title.Text; } set { title.Text = value; } }

        public OptionsListITem(OptionItemID optionID)
        {
            InitializeComponent();
            ID = optionID;
            switch (optionID)
            {
                case OptionItemID.ABOUT:
                    this.itemPicture.Image = CustomListItemsResource.Analyze;
                    this.title.Text = CustomListItemsResource.Text_About;
                    break;
                case OptionItemID.COMPLETE_TASK:
                    //this.itemPicture.Image = ListItemsResource.Symbol_Check_2;
                    this.title.Text = CustomListItemsResource.Text_CompleteTask;
                    break;
                case OptionItemID.COMPLETE_TASK_LATER:
                    //this.itemPicture.Image = ListItemsResource.Save;
                    this.title.Text = CustomListItemsResource.Text_CompleteTaskLater;
                    break;
                case OptionItemID.EXIT:
                    this.title.Text = CustomListItemsResource.Text_Exit;
                    this.itemPicture.Image = CustomListItemsResource.Logout;
                    break;
                case OptionItemID.EXIT_WITHOUT_SAVING:
                    //this.itemPicture.Image = ListItemsResource.Symbol_Delete_2;
                    this.title.Text = CustomListItemsResource.Text_ExitWithoutSaving;
                    break;
                case OptionItemID.LOGOUT:
                    this.title.Text = CustomListItemsResource.Text_Logout;
                    this.LabelUser.Text = CargoMatrix.Communication.WebServiceManager.Instance().m_user.firstName
                         + " " + CargoMatrix.Communication.WebServiceManager.Instance().m_user.lastName
                         + " (" + CargoMatrix.Communication.WebServiceManager.Instance().m_user.UserName + ")";
                    this.itemPicture.Image = CustomListItemsResource.Logout;
                    break;
                case OptionItemID.MAIN_MENU:
                    this.title.Text = CustomListItemsResource.Text_MainMenu;
                    this.itemPicture.Image = CustomListItemsResource.Menu;
                    break;
                case OptionItemID.MANUAL_LOCATION:
                    this.title.Text = CustomListItemsResource.Text_ManualLocation;
                    this.itemPicture.Image = CustomListItemsResource.Symbol_Edit;
                    break;
                case OptionItemID.REFRESH:
                    this.title.Text = CustomListItemsResource.Text_Refresh;
                    this.itemPicture.Image = CustomListItemsResource.Refresh;
                    break;
                case OptionItemID.FILTER:
                    this.title.Text = CustomListItemsResource.Text_Filter;
                    itemPicture.Image = CustomListItemsResource.Symbol_Search;
                    break;
                case OptionItemID.SELECT_ALL:
                    this.title.Text = CustomListItemsResource.Text_Select_ALL;
                    itemPicture.Image = CustomListItemsResource.Selection;
                    break;
                case OptionItemID.SELECT_NONE:
                    this.title.Text = CustomListItemsResource.Text_Select_None;
                    itemPicture.Image = CustomListItemsResource.Selection_Delete;
                    break;
                case OptionItemID.REMOVE_ALL:
                    this.title.Text = CustomListItemsResource.Text_Remove_ALL;
                    itemPicture.Image = CustomListItemsResource.Trash_Can;
                    break;

                case OptionItemID.DELETE_TASK:
                    this.title.Text = CustomListItemsResource.Text_Delete_Task;
                    itemPicture.Image = CustomListItemsResource.Trash_Can;
                    break;

                case OptionItemID.MANUAL_PEICE_ENTRY:
                    this.title.Text = CustomListItemsResource.Text_Manual_Piece_Entry;
                    itemPicture.Image = CustomListItemsResource.Assembly;
                    break;
                case OptionItemID.PRINT_HAWB_LABEL:
                    this.title.Text = CustomListItemsResource.Text_Print_HAWB_Label;
                    itemPicture.Image = CustomListItemsResource.Printer_Laser;
                    break;
                case OptionItemID.PRINT_MAWB_LABEL:
                    this.title.Text = CustomListItemsResource.Text_Print_MAWB_Label;
                    itemPicture.Image = CustomListItemsResource.Printer_Laser;
                    break;
                case OptionItemID.STAGE_ULD:
                    this.title.Text = CustomListItemsResource.Text_Stage_ULD;
                    itemPicture.Image = CustomListItemsResource.Objects;
                    break;
                case OptionItemID.PRINT_ULD_LABEL:
                    this.title.Text = CustomListItemsResource.Text_Print_ULD;
                    itemPicture.Image = CustomListItemsResource.Printer_Laser;
                    break;
                case OptionItemID.VISUAL_INSPECTION:
                    itemPicture.Image = CustomListItemsResource.Visual_Inspection_32x32;
                    title.Text = CustomListItemsResource.Text_Visual_Inspection;
                    break;
                case OptionItemID.RESET_SCREENING:
                    itemPicture.Image = CustomListItemsResource.Undo;
                    title.Text = CustomListItemsResource.Text_Reset_Screening;
                    break;
                case OptionItemID.CANCEL_SCREENING:
                    itemPicture.Image = CustomListItemsResource.Symbol_Delete_2;
                    title.Text = CustomListItemsResource.Text_Cancel_Screening;
                    break;
                case OptionItemID.FIND:
                    itemPicture.Image = CustomListItemsResource.Symbol_Find;
                    title.Text = CustomListItemsResource.Text_Find;
                    break;
                case OptionItemID.SCREENING_DIRECTION:
                    itemPicture.Image = CustomListItemsResource.Symbol_Find;
                    break;
                case OptionItemID.TASK_SETTINGS:
                    itemPicture.Image = CustomListItemsResource.Configuration;
                    title.Text = CustomListItemsResource.Text_Task_Settings;
                    break;
                case OptionItemID.SWITCH_MODE:
                    itemPicture.Image = CustomListItemsResource.Restart;
                    title.Text = CustomListItemsResource.Text_Switch_Mode;
                    break;
                case OptionItemID.INVENTORY_REPORT:
                    itemPicture.Image = CustomListItemsResource.Restart;
                    title.Text = CustomListItemsResource.Text_Report;
                    break;
                case OptionItemID.INVENTORY_REPORT_LOC:
                    itemPicture.Image = CustomListItemsResource.Restart;
                    title.Text = CustomListItemsResource.Text_ReportLoc;
                    break;
                case OptionItemID.INVENTORY_REPORT_CURRENT:
                    itemPicture.Image = CustomListItemsResource.Restart;
                    title.Text = CustomListItemsResource.Text_ReportCurrent;
                    break;
                case OptionItemID.SHARE_TASK:
                    itemPicture.Image = CargoMatrix.Resources.Skin.shareTask;
                    title.Text = CustomListItemsResource.Text_ShareTask;
                    break;
                case OptionItemID.FINALIZE_CONSOL:
                    itemPicture.Image = CustomListItemsResource.finalize;
                    title.Text = CustomListItemsResource.Text_Finalize;
                    break;
                case OptionItemID.FINALIZETASK:
                    itemPicture.Image = CustomListItemsResource.finalize;
                    title.Text = CustomListItemsResource.Text_FinalizeTask;
                    break;
                case OptionItemID.FINALIZE_INVENTORY:
                    itemPicture.Image = CustomListItemsResource.finalize;
                    title.Text = CustomListItemsResource.Text_Finalize_Inventory;
                    break;

                case OptionItemID.FINALIZE_LOAD_TRUCK:
                    itemPicture.Image = CustomListItemsResource.finalize;
                    title.Text = CustomListItemsResource.Text_Finalize_Load_Truck;
                    break;

                case OptionItemID.FINALIZE_MEASUREMENTS:
                    itemPicture.Image = CustomListItemsResource.finalize;
                    title.Text = CustomListItemsResource.Text_Finalize_Mesurements;
                    break;

                case OptionItemID.VIEW_MAWB_LIST:
                    itemPicture.Image = CustomListItemsResource.Clipboard;
                    title.Text = CustomListItemsResource.Text_View_MawbList;
                    break;
                case OptionItemID.SELECTION_TYPE:
                    this.itemPicture.Image = CustomListItemsResource.Selection;
                    this.title.Text = CustomListItemsResource.Text_Selection_Type;
                    break;
                case OptionItemID.VIEW_HAWB_LIST:
                    itemPicture.Image = CustomListItemsResource.Clipboard;
                    title.Text = CustomListItemsResource.Text_View_HawbList;
                    break;

                case OptionItemID.CANCEL_TASK:
                    itemPicture.Image = CustomListItemsResource.Symbol_Delete_2;
                    title.Text = CustomListItemsResource.Text_Cancel_Task;
                    break;

                case OptionItemID.METRICS_SYSTEM:
                    itemPicture.Image = CustomListItemsResource.Menu;
                    title.Text = CustomListItemsResource.Text_Metric_System;
                    break;

                case OptionItemID.DEVICES:
                    itemPicture.Image = CustomListItemsResource.Digital_Camera;
                    title.Text = CustomListItemsResource.Text_Devices;
                    break;
                case OptionItemID.SHOW_LOCATIONS:
                    itemPicture.Image = CustomListItemsResource.Change_Rooms;
                    this.title.Text = CustomListItemsResource.Text_ShowLocations;
                    break;

                case OptionItemID.PRINT_MEASUREMENTS_LABELS:

                    this.title.Text = CustomListItemsResource.Text_Print_Measurements_Labels;
                    itemPicture.Image = CustomListItemsResource.Printer_Laser;
                    break;

                case OptionItemID.PRINT_TRUCK_LABEL:
                    this.title.Text = CustomListItemsResource.Text_Print_Truck_Label;
                    itemPicture.Image = CustomListItemsResource.Printer_Laser;
                    break;

                case OptionItemID.SETUP_MEASUREMENTS_PRINTING:
                    this.title.Text = CustomListItemsResource.Text_Setup_Measurmenets_printing;
                    itemPicture.Image = CustomListItemsResource.Printer_Laser;
                    break;

                case OptionItemID.UNDO_DSCHARGE_SCAN:
                    this.title.Text = CustomListItemsResource.Text_Undo_Discharge_Scan;
                    itemPicture.Image = CustomListItemsResource.Undo;
                    break;
                case OptionItemID.ENTER_HAWB_NO:
                    itemPicture.Image = CustomListItemsResource.Symbol_Edit;
                    title.Text = CustomListItemsResource.Text_Enter_Hawb_No;
                    break;

                case OptionItemID.VIEW_ALL_LOCATIONS:
                    itemPicture.Image = CustomListItemsResource.Selection;
                    title.Text = CustomListItemsResource.Text_View_All_Locations;
                    break;

                case  OptionItemID.UTILITIES:
                    itemPicture.Image = CustomListItemsResource.Configuration;
                    title.Text = CustomListItemsResource.Text_Utilities;
                    break;
                case OptionItemID.ADD_MEASUREMENTS:
                    itemPicture.Image = CustomListItemsResource.Expand;
                    title.Text = CustomListItemsResource.Text_Add_Measurements;
                    break;

                case OptionItemID.EDIT_MEASURMENTS:
                    itemPicture.Image = CustomListItemsResource.Clipboard;
                    title.Text = CustomListItemsResource.Text_Edit_Mesurements;
                    break;
                case OptionItemID.MESSAGES:
                    itemPicture.Image = CargoMatrix.Resources.Icons.Envelope;
                    title.Text = CustomListItemsResource.Text_Messages;
                    break;

                case OptionItemID.ENTER_BARCODE:
                    itemPicture.Image = CustomListItemsResource.Symbol_Edit;
                    title.Text = CustomListItemsResource.Text_Enter_Barcode;
                    break;
            }

            Init();



        }
        //public OptionsListITem(string titlee, Image picture, OptionItemID optionID)
        //{
        //    InitializeComponent();
        //    this.titlee.Text = titlee;
        //    this.Name = titlee;
        //    this.itemPicture.Image = picture;

        //    Init();


        //}

        //public OptionsListITem(string titlee, Image picture, Color bkColor, OptionItemID optionID)
        //{
        //    InitializeComponent();
        //    this.titlee.Text = titlee;
        //    this.Name = titlee;
        //    this.itemPicture.Image = picture;
        //    m_backColor = bkColor;
        //    Init();


        //}
        //public OptionsListITem(string titlee, string image, OptionItemID optionID)
        //{ 
        //    InitializeComponent();
        //    this.titlee.Text = titlee;
        //    this.Name = titlee;
        //    //this.itemPicture.Image = picture;
        //    if (image != null)
        //        this.itemPicture.Image = (Image)ListItemsResource.ResourceManager.GetObject(image);

        //    Init();


        //}
        private void Init()
        {
            //this.FocusedColor = Color.WhiteSmoke; // Color.Silver;//.IndianRed;//.Khaki;//.LightYellow;
            this.backColor = m_backColor;// Color.White;
            //if (itemPicture.Image != null)
            {
                //Bitmap bmp = new Bitmap(itemPicture.Image);
                itemPicture.TransparentColor = m_backColor;// Color.White; //bmp.GetPixel(0, 0);
                //bmp.Dispose();
            }

        }
        public Image Picture
        {
            set
            {
                itemPicture.Image = value;
            }
        }
        public new OptionItemID ID
        {
            set
            {
                base.ID = (int)value;
            }
            get
            {
                return (OptionItemID)base.ID;
            }
        }

        private void OptionsListITem_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled)
            {
                switch (ID)
                {
                    case OptionItemID.ABOUT:
                        this.itemPicture.Image = CustomListItemsResource.Analyze;
                        break;
                    case OptionItemID.COMPLETE_TASK:
                        this.itemPicture.Image = CustomListItemsResource.Symbol_Check_2;
                        break;
                    case OptionItemID.COMPLETE_TASK_LATER:
                        this.itemPicture.Image = CustomListItemsResource.Save;
                        break;
                    case OptionItemID.EXIT:
                        this.itemPicture.Image = CustomListItemsResource.Logout;
                        break;
                    case OptionItemID.EXIT_WITHOUT_SAVING:
                        this.itemPicture.Image = CustomListItemsResource.Symbol_Delete_2;
                        break;
                    case OptionItemID.LOGOUT:
                        this.itemPicture.Image = CustomListItemsResource.Logout;
                        break;
                    case OptionItemID.MAIN_MENU:
                        this.itemPicture.Image = CustomListItemsResource.Menu;
                        break;
                    case OptionItemID.MANUAL_LOCATION:
                        this.itemPicture.Image = CustomListItemsResource.Symbol_Edit;
                        break;
                    case OptionItemID.REFRESH:
                        this.itemPicture.Image = CustomListItemsResource.Refresh;
                        break;
                    case OptionItemID.STAGE_ULD:
                        itemPicture.Image = CustomListItemsResource.Objects;
                        break;
                    case OptionItemID.MANUAL_PEICE_ENTRY:
                        itemPicture.Image = CustomListItemsResource.Assembly;
                        break;
                    case OptionItemID.PRINT_ULD_LABEL:
                        itemPicture.Image = CustomListItemsResource.Printer_Laser;
                        break;
                    case OptionItemID.VISUAL_INSPECTION:
                        itemPicture.Image = CustomListItemsResource.Visual_Inspection_32x32;
                        break;
                    case OptionItemID.RESET_SCREENING:
                        itemPicture.Image = CustomListItemsResource.Undo;
                        break;
                    case OptionItemID.CANCEL_SCREENING:
                        itemPicture.Image = CustomListItemsResource.Symbol_Delete_2;
                        break;
                    case OptionItemID.TASK_SETTINGS:
                        itemPicture.Image = CustomListItemsResource.Configuration;
                        break;
                    case OptionItemID.SHARE_TASK:
                        itemPicture.Image = CargoMatrix.Resources.Skin.shareTask;
                        break;
                    case OptionItemID.FINALIZE_CONSOL:
                        itemPicture.Image = CustomListItemsResource.Shipping_Box_Check;
                        break;
                    case OptionItemID.ENTER_HAWB_NO:
                        itemPicture.Image = CustomListItemsResource.Symbol_Edit;
                        break;

                }
            }
            else
            {
                switch (ID)
                {
                    case OptionItemID.ABOUT:
                        //this.itemPicture.Image = ListItemsResource.Analyze;
                        break;
                    case OptionItemID.COMPLETE_TASK:
                        this.itemPicture.Image = CustomListItemsResource.Symbol_Check_2_dis;
                        break;
                    case OptionItemID.COMPLETE_TASK_LATER:
                        this.itemPicture.Image = CustomListItemsResource.Save_Dis;
                        break;
                    case OptionItemID.EXIT:
                        //this.itemPicture.Image = ListItemsResource.Logout;
                        break;
                    case OptionItemID.EXIT_WITHOUT_SAVING:
                        this.itemPicture.Image = CustomListItemsResource.Symbol_Delete_2_dis;
                        break;
                    case OptionItemID.LOGOUT:
                        //this.itemPicture.Image = ListItemsResource.Logout;
                        break;
                    case OptionItemID.MAIN_MENU:
                        this.itemPicture.Image = CustomListItemsResource.Menu;
                        break;
                    case OptionItemID.MANUAL_LOCATION:
                        this.itemPicture.Image = CustomListItemsResource.Symbol_Edit_dis;
                        break;
                    case OptionItemID.REFRESH:
                        //this.itemPicture.Image = ListItemsResource.Refresh;
                        break;
                    case OptionItemID.STAGE_ULD:
                        itemPicture.Image = CustomListItemsResource.Objects_dis;
                        break;
                    case OptionItemID.MANUAL_PEICE_ENTRY:
                        itemPicture.Image = CustomListItemsResource.Assembly_dis;
                        break;
                    case OptionItemID.PRINT_ULD_LABEL:
                        itemPicture.Image = CustomListItemsResource.Printer_Laser_dis;
                        break;
                    case OptionItemID.VISUAL_INSPECTION:
                        itemPicture.Image = CustomListItemsResource.Visual_Inspection_32x32_dis;
                        break;
                    case OptionItemID.RESET_SCREENING:
                        itemPicture.Image = CustomListItemsResource.Reset_dis;
                        break;
                    case OptionItemID.CANCEL_SCREENING:
                        itemPicture.Image = CustomListItemsResource.Symbol_Delete_2_dis;
                        break;
                    case OptionItemID.TASK_SETTINGS:
                        itemPicture.Image = CustomListItemsResource.Configuration_Disabled;
                        break;
                    case OptionItemID.SHARE_TASK:
                        itemPicture.Image = CargoMatrix.Resources.Skin.shareTask_dis;
                        break;
                    case OptionItemID.FINALIZE_CONSOL:
                        itemPicture.Image = CustomListItemsResource.Shipping_Box_Check_Disabled;
                        break;
                    case OptionItemID.ENTER_HAWB_NO:
                        itemPicture.Image = CustomListItemsResource.Symbol_Edit_dis;
                        break;


                }


            }

        }
    }
}
