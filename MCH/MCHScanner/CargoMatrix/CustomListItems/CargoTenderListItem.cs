﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoTender;

namespace CustomListItems
{
    public partial class CargoTenderListItem : ExpandableListItem
    {
        ICargTenderMasterBillItem masterBillItem;

        public ICargTenderMasterBillItem MasterBillItem
        {
            get { return masterBillItem; }
            set 
            { 
                masterBillItem = value;

                this.DispalyItem(masterBillItem);
            }
        }
        
        public CargoTenderListItem()
        {
            InitializeComponent();

            this.LabelConfirmation = "Confirm masterbill number";

            this.panelCutoff.Visible = true;

            this.previousHeight = this.Height;
        }

        private void DispalyItem(ICargTenderMasterBillItem masterBillItem)
        {

        }

        protected override bool ButtonEnterValidation()
        {
            return false;
            
            //return string.Equals(this.masterBillItem.HousebillNumber, TextBox1.Text, StringComparison.OrdinalIgnoreCase)
        }
    }
}
