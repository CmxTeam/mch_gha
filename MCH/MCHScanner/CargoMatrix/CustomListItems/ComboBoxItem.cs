﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using SmoothListbox;

namespace CustomListItems
{
    class ComboBoxItem : ListItem
    {
        public System.Windows.Forms.Label title;
        public OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        public event EventHandler ButtonClick;
        public Label labelLoc;
        private bool selectable;
        private CargoMatrix.UI.CMXPictureButton buttonDelte;
        private bool isLast;
        private bool sl;
        private SmoothListbox.SmoothListBoxBase parentListBox;
        
        public new bool IsSelected
        {
            get { return sl; }
            set { sl = value; }
        }
        public bool Selectable
        {
            get { return selectable; }
            set { selectable = value; }
        }

        public bool IsChecked = false;

        public bool IsLast
        {
            get { return isLast; }
            set
            {
                isLast = value;
                SetIcon();
            }
        }
        public bool ReadOnly
        {
            set { buttonDelte.Visible = !value; }
        }

        public override string Text
        {
            get
            {
                return this.title.Text;
            }
            set
            {
                this.title.Text = value;
            }
        }

        public ComboBoxItem(string text, string loc, int id, bool isLast, bool selectable, bool isChecked)
        {
            InitializeComponent();
            this.selectable = selectable;
            panelBaseLine.Visible = false;
            title.Text = text;
            labelLoc.Text = loc;
            Name = text;
            this.ID = id;
            this.IsChecked = isChecked;
            IsLast = isLast;
        }

        public ComboBoxItem(string text, string loc, int id, bool isLast, bool selectable)
        {
            InitializeComponent();
            this.selectable = selectable;
            panelBaseLine.Visible = false;
            title.Text = text;
            labelLoc.Text = loc;
            IsLast = isLast;
            Name = text;
            this.ID = id;
        }
        public ComboBoxItem(string text, string loc, bool isLast) : this(text, loc, -1, isLast, true) { }


        public override void SelectedChanged(bool isSelected)
        {
            base.Focus(isSelected);
            this.IsSelected = isSelected;
            selectionFixer(isSelected);
            SetIcon();
        }
        private void selectionFixer(bool selected)
        {
            if (parentListBox == null)
                FindParentListBox();
            if (parentListBox == null)
                return;

            parentListBox.selectedItemsMap[this] = selected;

        }
        private void FindParentListBox()
        {
            Control control = this.Parent;
            while (control != null)
            {
                if (control is SmoothListBoxBase)
                {
                    if ((control as SmoothListBoxBase).selectedItemsMap.ContainsKey(this))
                    {
                        parentListBox = control as SmoothListBoxBase;
                        break;
                    }
                }
                control = control.Parent;
            }
        }
        private void SetIcon()
        {
            if (!selectable)
            {
                if (IsChecked)
                {
                    if (isLast)
                        itemPicture.Image = CustomListItemsResource.DottedLineHalfSelDisabled;
                    else
                        itemPicture.Image = CustomListItemsResource.DottedLineFullSelDisabled;

                }
                else
                {
                    if (isLast)
                        itemPicture.Image = CustomListItemsResource.DottedLineHalfDisabled;
                    else
                        itemPicture.Image = CustomListItemsResource.DottedLineFullDisabled;
                }
            }
            else
            {
                if (this.IsSelected)
                {
                    if (isLast)
                        itemPicture.Image = CustomListItemsResource.DottedLineHalfSel;
                    else
                        itemPicture.Image = CustomListItemsResource.DottedLineFullSel;
                }
                else
                {
                    if (isLast)
                        itemPicture.Image = CustomListItemsResource.DottedLineHalfUnsel;
                    else
                        itemPicture.Image = CustomListItemsResource.DottedLineFullUnsel;
                }
            }
        }

        public void RefreshIcon()
        {
            this.SetIcon();
        }

        public override void Focus(bool focused)
        {
            foreach (Control control in Controls)
            {
                if (control is Splitter)
                    continue;
                control.BackColor = BackColor;
            }
        }


        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDelte = new CargoMatrix.UI.CMXPictureButton();
            this.labelLoc = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.title.Location = new System.Drawing.Point(45, 4);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(160, 15);
            this.title.Text = "<title>";
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(1, 0);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(40, 40);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDelte
            // 
            this.buttonDelte.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDelte.Image = CargoMatrix.Resources.Skin.cc_trash;
            this.buttonDelte.Location = new System.Drawing.Point(192, 4);
            this.buttonDelte.Name = "buttonDelte";
            this.buttonDelte.PressedImage = CargoMatrix.Resources.Skin.cc_trash_over;
            this.buttonDelte.Size = new System.Drawing.Size(32, 32);
            this.buttonDelte.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDelte.TransparentColor = System.Drawing.Color.White;
            this.buttonDelte.Click += new System.EventHandler(this.buttonDelte_Click);
            // 
            // labelLoc
            // 
            this.labelLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLoc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLoc.Location = new System.Drawing.Point(45, 21);
            this.labelLoc.Name = "labelLoc";
            this.labelLoc.Size = new System.Drawing.Size(160, 15);
            // 
            // ComboBoxItem
            // 
            this.Controls.Add(this.buttonDelte);
            this.Controls.Add(this.labelLoc);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Name = "ComboBoxItem";
            this.Size = new System.Drawing.Size(226, 40);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }

        void buttonDelte_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null)
                ButtonClick(this, e);
        }
    }
}
