﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;

namespace CustomListItems
{
    public partial class ChoiceListItem : SmoothListbox.ListItems.TwoLineListItem
    {
        public ChoiceListItem(string title, Image picture, string line2):base(title, picture, line2)
        {
            InitializeComponent();
            this.title.Width = 110;
            this.LabelLine2.Width = 110;
        }
    }

    public class ChoiceListItem<T> : ChoiceListItem, IDataHolder<T>
    {
        private DataHolder<T> dataHolder;

        public ChoiceListItem(T item, string title, Image picture, string line2)
            : base(title, picture, line2)
        {
            dataHolder = new DataHolder<T>(item);
        }

        public T ItemData
        {
            get { return this.dataHolder.ItemData; }

            set
            {
                this.dataHolder.ItemData = value;
            }
        }

        public virtual bool OnBeforeDataItemChanged(T item)
        {
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual void OnDataItemChanged()
        { }
    }
}
