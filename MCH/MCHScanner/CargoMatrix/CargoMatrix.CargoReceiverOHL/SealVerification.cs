﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CargoMatrix.Utilities;

namespace CargoMatrix.CargoReceiverOHL
{
    public partial class SealVerification : CargoMatrix.Utilities.MessageListBox
    {
        public SealVerification()
        {
            InitializeComponent();
            MultiSelectListEnabled = false;
            OneTouchSelection = false;
            this.HeaderText = DataProvider.Title;
            this.HeaderText2 = "Select Seal Type";
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(SealVerification_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(SealVerification_ListItemClicked);
        }

        void SealVerification_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as ListItem).ID)
            {
                case 1:
                    string reference = GetReference();
                    if (!string.IsNullOrEmpty(reference))
                    {
                        SaveSealInfo((listItem as ListItem).ID, reference);

                    }
                    else this.ResetSelection();
                    break;
                default:
                    SaveSealInfo((listItem as ListItem).ID, string.Empty);
                    break;
            }
        }

        void SealVerification_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
                this.AddItem(new StandardListItem("TRUCK SEALED", null, 1));
                this.AddItem(new StandardListItem("NO SEAL", null, 2));
        }

        private void SaveSealInfo(int sealId, string sealRef)
        {
            Cursor.Current = Cursors.WaitCursor;
//            CargoMatrix.Communication.CargoReceiver.Instance.SaveSealInformation(Forklift.Instance.Mawb.CarrierNumber, Forklift.Instance.Mawb.MasterBillNumber, sealId, sealRef, string.Empty, string.Empty);
            DialogResult = DialogResult.OK;
            Cursor.Current = Cursors.Default;
        }

        private string GetReference()
        {
            CustomUtilities.EnterReferencePopup refPopup = new CustomUtilities.EnterReferencePopup(this.HeaderText, "Enter Seal Number");
            if (DialogResult.OK == refPopup.ShowDialog())
            {
                return refPopup.EnteredText;
            }
            else return string.Empty;
        }
    }
}
