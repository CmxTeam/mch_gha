﻿namespace CargoMatrix.CargoReceiverOHL
{
    partial class TaskDetailsItem
    {
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // labelPro
            // 
            this.labelPro = new System.Windows.Forms.Label();
            this.labelPro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPro.BackColor = System.Drawing.Color.White;
            this.labelPro.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelPro.Location = new System.Drawing.Point(4, 2);
            this.labelPro.Name = "labelPro";
            this.labelPro.Text = "TRUCKER PRO#";
            this.labelPro.Size = new System.Drawing.Size(160, 12);
            //
            // textBoxPro
            // 
            this.textBoxPro = new CargoMatrix.UI.CMXTextBox();
            this.textBoxPro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPro.BackColor = System.Drawing.Color.White;
            this.textBoxPro.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxPro.Location = new System.Drawing.Point(4, 15);
            this.textBoxPro.Name = "textBoxPro";
            this.textBoxPro.Size = new System.Drawing.Size(190, 13);
            this.textBoxPro.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxPro.TabStop = true;
            this.textBoxPro.TabIndex = 1;
            // 
            // labelIdVehicleID
            // 
            this.labelIdVehicleID = new System.Windows.Forms.Label();
            this.labelIdVehicleID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIdVehicleID.BackColor = System.Drawing.Color.White;
            this.labelIdVehicleID.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelIdVehicleID.Location = new System.Drawing.Point(4, 38);
            this.labelIdVehicleID.Name = "labelIdVehicleID";
            this.labelIdVehicleID.Text = "Vehicle ID";
            this.labelIdVehicleID.Size = new System.Drawing.Size(160, 12);
            //
            // textBoxIDNumber
            // 
            this.textBoxIDNumber = new CargoMatrix.UI.CMXTextBox();
            this.textBoxIDNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxIDNumber.BackColor = System.Drawing.Color.White;
            this.textBoxIDNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxIDNumber.Location = new System.Drawing.Point(4, 55);
            this.textBoxIDNumber.Name = "textBoxIDNumber";
            this.textBoxIDNumber.Size = new System.Drawing.Size(190, 13);
            this.textBoxIDNumber.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxIDNumber.TabStop = true;
            this.textBoxIDNumber.TabIndex = 2; 
            // 
            // labelTruckingComp
            // 
            this.labelTruckingComp = new System.Windows.Forms.Label();
            this.labelTruckingComp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTruckingComp.BackColor = System.Drawing.Color.White;
            this.labelTruckingComp.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelTruckingComp.Location = new System.Drawing.Point(4, 78);
            this.labelTruckingComp.Name = "labelTruckingComp";
            this.labelTruckingComp.Text = "Trucking Company";
            this.labelTruckingComp.Size = new System.Drawing.Size(160, 12);
            //
            // textBoxTruckingCompany
            // 
            this.textBoxTruckingCompany = new CargoMatrix.UI.CMXTextBox();
            this.textBoxTruckingCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTruckingCompany.BackColor = System.Drawing.Color.White;
            this.textBoxTruckingCompany.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxTruckingCompany.Location = new System.Drawing.Point(4, 95);
            this.textBoxTruckingCompany.Name = "textBoxTruckingCompany";
            this.textBoxTruckingCompany.Size = new System.Drawing.Size(190, 13);
            this.textBoxTruckingCompany.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxTruckingCompany.TabStop = true;
            this.textBoxTruckingCompany.TabIndex = 3;
            // 
            // labelTruckerName
            // 
            this.labelTruckerName = new System.Windows.Forms.Label();
            this.labelTruckerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTruckerName.BackColor = System.Drawing.Color.White;
            this.labelTruckerName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelTruckerName.Location = new System.Drawing.Point(4, 118);
            this.labelTruckerName.Name = "labelTruckerName";
            this.labelTruckerName.Text = "Trucker's Name";
            this.labelTruckerName.Size = new System.Drawing.Size(160, 12);
            //
            // textBoxTrucker
            // 
            this.textBoxTrucker = new CargoMatrix.UI.CMXTextBox();
            this.textBoxTrucker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTrucker.BackColor = System.Drawing.Color.White;
            this.textBoxTrucker.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxTrucker.Location = new System.Drawing.Point(4, 135);
            this.textBoxTrucker.Name = "textBoxTrucker";
            this.textBoxTrucker.Size = new System.Drawing.Size(190, 13);
            this.textBoxTrucker.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxTrucker.TabStop = true;
            this.textBoxTrucker.TabIndex = 4;
            // 
            // labelRefNo
            // 
            this.labelRefNo = new System.Windows.Forms.Label();
            this.labelRefNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRefNo.BackColor = System.Drawing.Color.White;
            this.labelRefNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelRefNo.Location = new System.Drawing.Point(4, 158);
            this.labelRefNo.Name = "labelRefNo";
            this.labelRefNo.Text = "Shipper Ref (optional)";
            this.labelRefNo.Size = new System.Drawing.Size(160, 12);            
            //
            // textBoxShipperRef
            // 
            this.textBoxShipperRef = new CargoMatrix.UI.CMXTextBox();
            this.textBoxShipperRef.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxShipperRef.BackColor = System.Drawing.Color.White;
            this.textBoxShipperRef.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxShipperRef.Location = new System.Drawing.Point(4, 175);
            this.textBoxShipperRef.Name = "textBoxIDNumber";
            this.textBoxShipperRef.Size = new System.Drawing.Size(190, 13);
            this.textBoxShipperRef.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxShipperRef.TabStop = true;
            this.textBoxShipperRef.TabIndex = 5;             
            // 
            // buttonIDNumber
            // 
            this.buttonIDNumber = new CargoMatrix.UI.CMXPictureButton();
            this.buttonIDNumber.BackColor = System.Drawing.Color.White;
            this.buttonIDNumber.Location = new System.Drawing.Point(202, 43);
            this.buttonIDNumber.Name = "buttonIDNumber";
            this.buttonIDNumber.Size = new System.Drawing.Size(32, 32);
            this.buttonIDNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonIDNumber.TransparentColor = System.Drawing.Color.White;
            this.buttonIDNumber.Click += new System.EventHandler(buttonIDNumber_Click);
            this.buttonIDNumber.Image = CargoMatrix.Resources.Skin._3dots_btn;
            this.buttonIDNumber.PressedImage = CargoMatrix.Resources.Skin._3dots_btn_over;
            this.buttonIDNumber.TabStop = false;
            // 
            // buttonTruckCompany
            // 
            this.buttonTruckCompany = new CargoMatrix.UI.CMXPictureButton();
            this.buttonTruckCompany.BackColor = System.Drawing.Color.White;
            this.buttonTruckCompany.Location = new System.Drawing.Point(202, 83);
            this.buttonTruckCompany.Name = "buttonTruckCompany";
            this.buttonTruckCompany.Size = new System.Drawing.Size(32, 32);
            this.buttonTruckCompany.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonTruckCompany.TransparentColor = System.Drawing.Color.White;
            this.buttonTruckCompany.Click += new System.EventHandler(buttonTruckCompany_Click);
            this.buttonTruckCompany.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonTruckCompany.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonTruckCompany.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            this.buttonTruckCompany.TabStop = false;
            // 
            // buttonTrucker
            // 
            this.buttonTrucker = new CargoMatrix.UI.CMXPictureButton();
            this.buttonTrucker.BackColor = System.Drawing.Color.White;
            this.buttonTrucker.Location = new System.Drawing.Point(202, 123);
            this.buttonTrucker.Name = "buttonTrucker";
            this.buttonTrucker.Size = new System.Drawing.Size(32, 32);
            this.buttonTrucker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonTrucker.TransparentColor = System.Drawing.Color.White;
            this.buttonTrucker.Click += new System.EventHandler(buttonTrucker_Click);
            this.buttonTrucker.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonTrucker.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonTrucker.TabStop = false;
            // 
            // buttonShipperRef
            // 
            this.buttonShipperRef = new CargoMatrix.UI.CMXPictureButton();
            this.buttonShipperRef.BackColor = System.Drawing.Color.White;
            this.buttonShipperRef.Location = new System.Drawing.Point(202, 163);
            this.buttonShipperRef.Name = "buttonShipperRef";
            this.buttonShipperRef.Size = new System.Drawing.Size(32, 32);
            this.buttonShipperRef.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonShipperRef.TransparentColor = System.Drawing.Color.White;
            this.buttonShipperRef.Click += new System.EventHandler(buttonShipperRef_Click);
            this.buttonShipperRef.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonShipperRef.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonShipperRef.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            this.buttonShipperRef.TabStop = false;
            ////
            ////buttonRecover
            ////
            //this.buttonRecover = new OpenNETCF.Windows.Forms.Button2();
            //this.buttonRecover.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            //| System.Windows.Forms.AnchorStyles.Right)));
            //this.buttonRecover.BackColor = System.Drawing.SystemColors.Control;
            //this.buttonRecover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            //this.buttonRecover.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            //this.buttonRecover.ForeColor = System.Drawing.Color.White;
            //this.buttonRecover.Location = new System.Drawing.Point(80, 197);
            //this.buttonRecover.Name = "ProceedButton";
            //this.buttonRecover.Size = new System.Drawing.Size(80, 32);
            //this.buttonRecover.TabIndex = 17;
            //this.buttonRecover.Text = "RECOVER";
            //this.buttonRecover.BackgroundImage = Resources.Skin.btn;
            //this.buttonRecover.ActiveBackgroundImage = Resources.Skin.btn_over;
            //this.buttonRecover.DisabledBackgroundImage = Resources.Skin.btn_dis;
            //this.buttonRecover.Enabled = false;
            //this.buttonRecover.TabStop = false;
            //this.buttonRecover.Click += new System.EventHandler(buttonRecover_Click);
            //
            //  buttonKnown
            //
            this.buttonKnown = new OpenNETCF.Windows.Forms.Button2();
            this.buttonKnown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonKnown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buttonKnown.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.buttonKnown.ForeColor = System.Drawing.Color.White;
            this.buttonKnown.Location = new System.Drawing.Point(20, 197);
            this.buttonKnown.Name = "buttonKnown";
            this.buttonKnown.Size = new System.Drawing.Size(80, 32);
            this.buttonKnown.Text = "Known";
            this.buttonKnown.BackgroundImage = Resources.Skin.btn;
            this.buttonKnown.ActiveBackgroundImage = Resources.Skin.btn_over;
            this.buttonKnown.DisabledBackgroundImage = Resources.Skin.btn_dis;
            this.buttonKnown.Enabled = false;
            this.buttonKnown.Click += new System.EventHandler(buttonKnown_Click);
            //
            //  buttonUnKnown
            //
            this.buttonUnKnown = new OpenNETCF.Windows.Forms.Button2();
            this.buttonUnKnown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUnKnown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buttonUnKnown.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.buttonUnKnown.ForeColor = System.Drawing.Color.White;
            this.buttonUnKnown.Location = new System.Drawing.Point(125, 197);
            this.buttonUnKnown.Name = "buttonUnKnown";
            this.buttonUnKnown.Size = new System.Drawing.Size(80, 32);
            this.buttonUnKnown.Text = "UnKnown";
            this.buttonUnKnown.BackgroundImage = Resources.Skin.btn;
            this.buttonUnKnown.ActiveBackgroundImage = Resources.Skin.btn_over;
            this.buttonUnKnown.DisabledBackgroundImage = Resources.Skin.btn_dis;
            this.buttonUnKnown.Enabled = false;
            this.buttonUnKnown.Click += new System.EventHandler(buttonUnKnown_Click);
            // 
            // pictureBoxBottomStrip
            // 
            this.pictureBoxBottomStrip = new System.Windows.Forms.PictureBox();
            this.pictureBoxBottomStrip.BackColor = System.Drawing.Color.Red;
            this.pictureBoxBottomStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBoxBottomStrip.Location = new System.Drawing.Point(0, 196);
            this.pictureBoxBottomStrip.Name = "pictureBox2";
            this.pictureBoxBottomStrip.Size = new System.Drawing.Size(240, 40);
            this.pictureBoxBottomStrip.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBottomStrip.Image = global::Resources.Graphics.Skin.nav_bg;
            // 
            // MawbDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelPro);
            this.Controls.Add(this.labelTruckingComp);
            this.Controls.Add(this.labelTruckerName);
            this.Controls.Add(this.labelIdVehicleID);
            this.Controls.Add(this.labelRefNo);

            this.Controls.Add(this.textBoxPro);
            this.Controls.Add(this.textBoxTruckingCompany);
            this.Controls.Add(this.textBoxTrucker);
            this.Controls.Add(this.textBoxIDNumber);
            this.Controls.Add(this.textBoxShipperRef);

            this.Controls.Add(this.buttonTruckCompany);
            this.Controls.Add(this.buttonTrucker);
            this.Controls.Add(this.buttonIDNumber);
            this.Controls.Add(this.buttonShipperRef);
            //this.Controls.Add(this.buttonRecover);
            this.Controls.Add(this.buttonKnown);
            this.Controls.Add(this.buttonUnKnown);
            this.Controls.Add(this.pictureBoxBottomStrip);
            //this.buttonRecover.BringToFront();
            this.Name = "DetailsItem";
            this.Location = new System.Drawing.Point(0, 0);
            this.Size = new System.Drawing.Size(240, 237);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelPro;
        private System.Windows.Forms.Label labelTruckingComp;
        private System.Windows.Forms.Label labelTruckerName;
        private System.Windows.Forms.Label labelIdVehicleID;
        private System.Windows.Forms.Label labelRefNo;

        private CargoMatrix.UI.CMXPictureButton buttonTruckCompany;
        private CargoMatrix.UI.CMXPictureButton buttonTrucker;
        private CargoMatrix.UI.CMXPictureButton buttonIDNumber;
        private CargoMatrix.UI.CMXPictureButton buttonShipperRef;

        private CargoMatrix.UI.CMXTextBox textBoxPro;
        private CargoMatrix.UI.CMXTextBox textBoxTruckingCompany;
        private CargoMatrix.UI.CMXTextBox textBoxTrucker;
        private CargoMatrix.UI.CMXTextBox textBoxIDNumber;
        private CargoMatrix.UI.CMXTextBox textBoxShipperRef;

        //private OpenNETCF.Windows.Forms.Button2 buttonRecover;
        private OpenNETCF.Windows.Forms.Button2 buttonKnown;
        private OpenNETCF.Windows.Forms.Button2 buttonUnKnown;
        private System.Windows.Forms.PictureBox pictureBoxBottomStrip;


    }
}
