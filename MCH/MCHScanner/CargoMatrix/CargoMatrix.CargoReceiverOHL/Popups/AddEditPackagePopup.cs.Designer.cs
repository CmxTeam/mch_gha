﻿using CargoMatrix.UI;
namespace CargoMatrix.CargoReceiverOHL
{
    partial class AddEditPackagePopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPackaging = new CMXTextBox();
            this.textBoxPieces = new CMXTextBox();
            this.buttonPackageType = new CMXPictureButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 16);
            this.label2.Text = "Packaging:";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(3, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.Text = "# of Pieces";
            // 
            // textBoxPackaging
            // 
            this.textBoxPackaging.Location = new System.Drawing.Point(99, 45);
            this.textBoxPackaging.Name = "textBoxPackaging";
            this.textBoxPackaging.Size = new System.Drawing.Size(83, 21);
            this.textBoxPackaging.TabIndex = 1;
            this.textBoxPackaging.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // textBoxPieces
            // 
            this.textBoxPieces.Location = new System.Drawing.Point(99, 85);
            this.textBoxPieces.Name = "textBoxPieces";
            this.textBoxPieces.InputMode = CMXTextBoxInputMode.Numeric;
            this.textBoxPieces.Size = new System.Drawing.Size(83, 21);
            this.textBoxPieces.TabIndex = 2;
            this.textBoxPieces.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // buttonPackageType
            // 
            this.buttonPackageType.Location = new System.Drawing.Point(200, 45);
            this.buttonPackageType.Name = "button1";
            this.buttonPackageType.Size = new System.Drawing.Size(32, 32);
            this.buttonPackageType.TabIndex = 3;
            this.buttonPackageType.Image = CargoMatrix.Resources.Skin.shipping_box_help;
            this.buttonPackageType.PressedImage = CargoMatrix.Resources.Skin.shipping_box_help_over;
            this.buttonPackageType.Click += new System.EventHandler(buttonPackageType_Click);
            // 
            // LoosePiecesPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.panelContent.Controls.Add(this.buttonPackageType);
            this.panelContent.Controls.Add(this.textBoxPieces);
            this.panelContent.Controls.Add(this.textBoxPackaging);
            this.panelContent.Controls.Add(this.label3);
            this.panelContent.Controls.Add(this.label2);
            this.panelContent.Controls.Add(this.label1);
            this.Name = "LoosePiecesPopup";
            this.Size = new System.Drawing.Size(236, 150);
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private CMXTextBox textBoxPackaging;
        private CMXTextBox textBoxPieces;
        private CMXPictureButton buttonPackageType;
    }
}
