﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CustomUtilities;
using CargoMatrix.Communication.PieceScanWCF;
using CargoMatrix.Communication.WSOnHand;
using CustomListItems;
using CargoMatrix.FreightPhotoCapture;

namespace CargoMatrix.CargoReceiverOHL
{
    public partial class OnHandDetails : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        private TaskDetailsItem detailsItem;
        private bool showDoorPopup;
        public OnHandDetails()
        {
            InitializeComponent();
            showDoorPopup = true;
            this.TitleText = "CargoReceiver - Receving Details";
            this.detailsItem = new TaskDetailsItem();
            this.Scrollable = false;
            this.panelHeader2.Height = this.smoothListBoxMainList.Bottom - this.panelHeader2.Top;
            this.panelHeader2.Controls.Add(detailsItem);
            this.detailsItem.ButtonRecoverClick += new EventHandler(detailsItem_ButtonRecoverClick);
            this.detailsItem.ButtonKnownClick += new EventHandler(detailsItem_ButtonKnownClick);
            this.detailsItem.ButtonUnKnownClick += new EventHandler(detailsItem_ButtonUnKnownClick);
            this.LoadOptionsMenu += new EventHandler(OnHandDetails_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(OnHandDetails_MenuItemClicked);
        }

        public override void LoadControl()
        {
            if (showDoorPopup)
            {
                ScanEnterPopup_old doorPopup = new ScanEnterPopup_old(BarcodeType.All);
                doorPopup.Title = "Scan or enter number";
                doorPopup.TextLabel = "Scan door barcode";
                showDoorPopup = DialogResult.OK != doorPopup.ShowDialog();
            }
        }

        void OnHandDetails_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            switch ((listItem as OptionsListITem).ID)
            {
                case OptionsListITem.OptionItemID.REFRESH:
                    LoadControl();
                    break;
            }
        }

        void OnHandDetails_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new OptionsListITem(OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }


        #region Event Handlers


        void detailsItem_ButtonUnKnownClick(object sender, EventArgs e)
        {
            ProceedNext();
        }

        void detailsItem_ButtonKnownClick(object sender, EventArgs e)
        {
            SealVerificationPopup();
            if (CaptureDriverInfo())
            {
                byte[] signature = CaptureSignature();
                if (signature == null)
                    return;

                ProceedNext();

            }
        }

        private void ProceedNext()
        {
            UI.CMXAnimationmanager.DisplayForm(new DEmoULDBuilder() { Width = this.Width, Height = this.Height });
        }


        void detailsItem_ButtonRecoverClick(object sender, EventArgs e)
        {

            if (!DisplayDoorTruckPopup())
                return;
            if (!SealVerificationPopup())
                return;

            byte[] signature = CaptureSignature();
            if (signature == null)
                return;

            UI.CMXAnimationmanager.DisplayForm(new DEmoULDBuilder() { Width = this.Width, Height = this.Height });

        }

        #endregion

        #region Popups
        public bool DisplayDoorTruckPopup()
        {
            CustomUtilities.MultiScanPopup mscanPop = new CustomUtilities.MultiScanPopup();
            mscanPop.Header = DataProvider.Title;
            mscanPop.Caption = "Scan or enter number";
            mscanPop.Label1 = "Scan door barcode";
            mscanPop.Label2 = "Scan truck barcode (optional)";
            mscanPop.Text2ISOptional = true;
            mscanPop.Prefix1 = CustomUtilities.BarcodeType.Door;
            mscanPop.Prefix2 = CustomUtilities.BarcodeType.Truck;
            return DialogResult.OK == mscanPop.ShowDialog();
        }
        private byte[] CaptureSignature()
        {
            SignatureCaptureForm signatureForm = new SignatureCaptureForm();


            signatureForm.Header = DataProvider.Title;

            //signatureForm.pi

            Cursor.Current = Cursors.Default;

            DialogResult result = signatureForm.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                return signatureForm.SugnatureAsBytes;
            }
            else
                return null;
        }
        public bool SealVerificationPopup()
        {
            return DialogResult.OK == new SealVerification().ShowDialog();
        }

        public bool CaptureDriverInfo()
        {
            try
            {
                UI.BarcodeReader.TermReader();
                using (FreightPhotoCapture.CameraPopup popup = new CargoMatrix.FreightPhotoCapture.CameraPopup(DataProvider.Title, "Trucker's ID Number",string.Empty))
                {
                    return DialogResult.OK == popup.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                UI.CMXMessageBox.Show(ex.Message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return false;
            }

        }

        #endregion
    }
}
