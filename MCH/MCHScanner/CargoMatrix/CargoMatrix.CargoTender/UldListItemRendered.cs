﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CustomListItems;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using System.Drawing;

namespace CargoMatrix.CargoTender
{
    public class UldListItemRendered : ExpandableRenderListitem<IULD>
    {
        private CargoMatrix.UI.CMXPictureButton btnDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 pcxLogo;

        public event EventHandler ButtonBrosweClick;

        public bool IsRejected { get; set; }

        public UldListItemRendered(IULD uld)
            : base(uld)
        {
            this.InitializeItems();
        }

        private void InitializeItems()
        {
            this.SuspendLayout();

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.Size = new System.Drawing.Size(240, 55);

            this.ResumeLayout(false);
        }

        protected override void InitializeControls()
        {
            this.btnDetails = new CargoMatrix.UI.CMXPictureButton();
            this.pcxLogo = new OpenNETCF.Windows.Forms.PictureBox2();

            var rHeight = this.CurrentAutoScaleDimensions.Height / 96F;
            var rWidth = this.CurrentAutoScaleDimensions.Width / 96F;

            // 
            // btnDetails
            // 
            this.btnDetails.Image = CargoMatrix.Resources.Skin._3dots_btn;
            this.btnDetails.Location = new System.Drawing.Point((int)(199 * rWidth), (int)(10 * rHeight));
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.PressedImage = CargoMatrix.Resources.Skin._3dots_btn_over;
            this.btnDetails.Size = new System.Drawing.Size((int)(32 * rWidth), (int)(32 * rHeight));
            this.btnDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetails.TransparentColor = System.Drawing.Color.White;
            this.btnDetails.Click += new EventHandler(BtnDetails_Click);

            // 
            // pcxLogo
            // 
            this.pcxLogo.BackColor = System.Drawing.SystemColors.Control;
            this.pcxLogo.Location = new System.Drawing.Point(4, 5);
            this.pcxLogo.Name = "pcxLogo";
            this.pcxLogo.Size = new System.Drawing.Size(32, 32);
            this.pcxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcxLogo.TransparentColor = System.Drawing.Color.White;


            this.Controls.Add(this.btnDetails);
            this.Controls.Add(this.pcxLogo);

        }

        protected override bool ButtonEnterValidation()
        {
            if (this.ItemData.IsLoose() == true)
            {
                return true;
            }

            return string.Compare(this.TextBox1.Text, this.ItemData.ULDNo, StringComparison.OrdinalIgnoreCase) == 0;
        }

        protected override void Draw(System.Drawing.Graphics gOffScreen)
        {
            var uld = this.ItemData;
            bool isLoose = uld.IsLoose();

            this.SuspendLayout();

            int fontSize = 8;
            var rHeight = this.CurrentAutoScaleDimensions.Height / 96F;
            var rWidth = this.CurrentAutoScaleDimensions.Width / 96F;

            var headerFont = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);
            var boldFont = new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Bold);
            var regularFont = new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Regular);

            using (Brush brush = new SolidBrush(Color.Black))
            {
                string uldHeader = isLoose == true ? "LOOSE" : string.Format("{0}-{1}", uld.ULDType, this.ItemData.ULDNo);

                gOffScreen.DrawString(uldHeader, headerFont, brush, 45 * rWidth, 1 * rHeight);

                this.pcxLogo.Image = this.ItemData.Pieces == this.ItemData.ScannedPieces ? CargoMatrix.Resources.Skin.PieceMode : CargoMatrix.Resources.Skin.Freight_Car;

                gOffScreen.DrawString("Pieces : ", boldFont, brush, 45 * rWidth, 20 * rHeight);

                if (this.ItemData.IsLoose())
                {

                    if (uld.ScannedPieces == 0)
                    {
                        gOffScreen.DrawString(string.Format("{0} of {1}", uld.Pieces, uld.Pieces + uld.ScannedPieces), regularFont,
                                      brush, 95 * rWidth, 20 * rHeight);
                    }
                    else
                    {
                        using (Brush piecesBrush = new SolidBrush(uld.Pieces == uld.ScannedPieces ? Color.Green : Color.Red))
                        {
                            gOffScreen.DrawString(string.Format("{0} of {1}", uld.Pieces, uld.Pieces + uld.ScannedPieces), regularFont,
                                      piecesBrush, 95 * rWidth, 20 * rHeight);
                        }
                    }
                }
                else
                {
                    gOffScreen.DrawString(uld.Pieces.ToString(), regularFont, brush, 95 * rWidth, 20 * rHeight);
                }

                gOffScreen.DrawString("Weight : ",
                                   boldFont, brush, 45 * rWidth, 36 * rHeight);
                gOffScreen.DrawString(string.Format("{0:N2}KGS", uld.Weight),
                                    regularFont, brush, 95 * rWidth, 36 * rHeight);
            }

            using (Pen pen = new Pen(Color.Gainsboro))
            {
                gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
            }

            headerFont.Dispose();
            boldFont.Dispose();
            regularFont.Dispose();

            this.ResumeLayout(false);
        }

        public override void OnDataItemChanged()
        {
            this.Dirty = true;
            this.Render();
        }

        private void BtnDetails_Click(object sender, EventArgs args)
        {
            var details = System.Threading.Interlocked.CompareExchange<EventHandler>(ref this.ButtonBrosweClick, null, null);

            if (details != null)
            {
                details(this, EventArgs.Empty);
            }
        }

        private void EnableValidatingTextBox(bool enable)
        {
            this.TextBox1.Enabled = enable;
        }

        private void EnableValidatingTextBox()
        {
            this.EnableValidatingTextBox(!this.ItemData.IsLoose());
        }
    }
}
