﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.CargoTender;
using CustomListItems;
using System.Drawing;

namespace CargoMatrix.CargoTender
{
    class CargoTenderMasterBillItemRendered : ExpandableRenderListitem<ICargTenderMasterBillItem>
    {
        private CargoMatrix.UI.CMXPictureButton btnDamageCapture;
        private CustomUtilities.IndicatorPanel pnlIndicators;
        private CargoMatrix.UI.CMXPictureButton btnDetails;
        private CargoMatrix.UI.CMXPictureButton btnMoreDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 pcxLogo;

        public event EventHandler DamageCaptureClick;
        public event EventHandler ViewerClick;
        public event EventHandler DetailsClick;

        public CargoTenderMasterBillItemRendered(ICargTenderMasterBillItem masterBill)
            : base(masterBill)
        {
            this.SuspendLayout();

            this.BackColor = Color.White;

            this.InitializeItems();

            this.ResumeLayout();

            this.RetentionLevel = 2;
        }

        private void InitializeItems()
        {
            this.SuspendLayout();

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.Size = new System.Drawing.Size(240, 110);

            this.ResumeLayout(false);
        }

        protected override void InitializeControls()
        {
            this.btnDetails = new CargoMatrix.UI.CMXPictureButton();
            this.pnlIndicators = new CustomUtilities.IndicatorPanel();
            this.pcxLogo = new OpenNETCF.Windows.Forms.PictureBox2();
            this.btnDamageCapture = new CargoMatrix.UI.CMXPictureButton();
            this.btnMoreDetails = new CargoMatrix.UI.CMXPictureButton();
           
            var rHeight = this.CurrentAutoScaleDimensions.Height / 96F;
            var rWidth = this.CurrentAutoScaleDimensions.Width / 96F;

            // 
            // pcxLogo
            // 

            this.pcxLogo.Location = new System.Drawing.Point((int)(3 * rWidth), (int)(3 * rHeight));
            this.pcxLogo.Name = "pcxLogo";
            this.pcxLogo.Size = new System.Drawing.Size((int)(24 * rWidth), (int)(24 * rHeight));
            this.pcxLogo.TransparentColor = System.Drawing.Color.White;
            this.pcxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            // 
            // btnDetails
            // 
            this.btnDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.btnDetails.Location = new System.Drawing.Point((int)(3 * rWidth), (int)(39 * rHeight));
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.btnDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            this.btnDetails.Size = new System.Drawing.Size((int)(32 * rWidth), (int)(32 * rHeight));
            this.btnDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDetails.TransparentColor = System.Drawing.Color.White;
            this.btnDetails.Click += new EventHandler(btnDetails_Click);

            // 
            // btnMoreDetails
            // 
            this.btnMoreDetails.Image = CargoMatrix.Resources.Skin._3dots_btn;
            this.btnMoreDetails.Location = new System.Drawing.Point((int)(204 * rWidth), (int)(55 * rHeight));
            this.btnMoreDetails.Name = "btnMoreDetails";
            this.btnMoreDetails.PressedImage = CargoMatrix.Resources.Skin._3dots_btn_over;
            this.btnMoreDetails.Size = new System.Drawing.Size((int)(32 * rWidth), (int)(32 * rHeight));
            this.btnMoreDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMoreDetails.TransparentColor = System.Drawing.Color.White;
            this.btnMoreDetails.Click += new EventHandler(btnMoreDetails_Click);

            // 
            // btnDamageCapture
            // 
            this.btnDamageCapture.Image = CargoMatrix.Resources.Skin.damage;
            this.btnDamageCapture.Location = new System.Drawing.Point((int)(204 * rWidth), (int)(17 * rHeight));
            this.btnDamageCapture.Name = "btnDamageCapture";
            this.btnDamageCapture.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.btnDamageCapture.Size = new System.Drawing.Size((int)(32 * rWidth), (int)(32 * rHeight));
            this.btnDamageCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDamageCapture.TransparentColor = System.Drawing.Color.White;
            this.btnDamageCapture.Click += new EventHandler(btnDamageCapture_Click);

            this.pnlIndicators.PopupHeader = "Test";
            this.pnlIndicators.BackColor = this.BackColor;
            this.pnlIndicators.Location = new System.Drawing.Point((int)(3 * rWidth), this.Height - (int)(18 * rHeight));
            this.pnlIndicators.Size = new System.Drawing.Size(this.Width - 3, (int)(16 * rHeight));

            this.Controls.Add(this.pcxLogo);
            this.Controls.Add(this.btnDamageCapture);
            this.Controls.Add(this.btnDetails);
            this.Controls.Add(this.btnMoreDetails);
            this.Controls.Add(this.pnlIndicators);

            btnMoreDetails.BringToFront();

        }

        void btnMoreDetails_Click(object sender, EventArgs e)
        {
            var detailsClick = this.DetailsClick;

            if (detailsClick != null)
            {
                detailsClick(this, EventArgs.Empty);
            }
        }

        void btnDamageCapture_Click(object sender, EventArgs e)
        {
            var damageCaptureClick = this.DamageCaptureClick;

            if (damageCaptureClick != null)
            {
                damageCaptureClick(this, EventArgs.Empty);
            }
        }

        void btnDetails_Click(object sender, EventArgs e)
        {
            var detailsClick = this.ViewerClick;

            if (detailsClick != null)
            {
                detailsClick(this, EventArgs.Empty);
            }
        }

        protected override bool ButtonEnterValidation()
        {
            return false;
        }

        protected override void Draw(System.Drawing.Graphics gOffScreen)
        {
            this.SuspendLayout();

            var rHeight = this.CurrentAutoScaleDimensions.Height / 96F;
            var rWidth = this.CurrentAutoScaleDimensions.Width / 96F; 
            
            this.pcxLogo.Image = CargoMatrix.Resources.Skin.Airplane24x24;

            var headerFont = new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold);
            var boldFont = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold);
            var regularFont = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular);

            using (Brush brush = new SolidBrush(Color.Black))
            {
                gOffScreen.DrawString("001-123456789-001(23:23)", 
                                      headerFont, brush, 40 * rWidth, 1 * rHeight);

                gOffScreen.DrawString("Hbs : ",
                                      boldFont, brush, 40 * rWidth, 20 * rHeight);

                gOffScreen.DrawString("24", regularFont,
                                      brush, 70 * rWidth, 20 * rHeight);

                gOffScreen.DrawString("ULDs : ", boldFont,
                                     brush, 103 * rWidth, 20 * rHeight);
                
                gOffScreen.DrawString("2",
                                    regularFont, brush, 143 * rWidth, 20 * rHeight);


                gOffScreen.DrawString("Pcs : ", boldFont, brush, 40 * rWidth, 36 * rHeight);
                gOffScreen.DrawString("24",regularFont, brush, 70 * rWidth, 36 * rHeight);

                gOffScreen.DrawString("Wht : ",
                                    boldFont, brush, 103 * rWidth, 36 * rHeight);
                gOffScreen.DrawString("157 KGS",
                                    regularFont, brush, 138 * rWidth, 36 * rHeight);

                gOffScreen.DrawString("Flight No : ",
                                    boldFont, brush, 40 * rWidth, 52 * rHeight);
                gOffScreen.DrawString("0631",
                                    regularFont, brush, 101 * rWidth, 52 * rHeight);

            }

            using (Pen pen = new Pen(Color.Gainsboro))
            {
                gOffScreen.DrawLine(pen, 0, Height - 1, Width, Height - 1);
            }

            this.pnlIndicators.Flags = 3;

            headerFont.Dispose();
            boldFont.Dispose();
            regularFont.Dispose();

            this.ResumeLayout(false);
        }
    }
}
