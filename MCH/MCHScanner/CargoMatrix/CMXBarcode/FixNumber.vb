﻿Partial Public Class BarcodeParser
    Public Shared Function FixNumber(ByVal Value As String) As String

        Try



            If Value = Nothing Then

                Return ""

            End If



            Value = Trim(Value)

            Value = Replace(Value, "'", "")

            Value = Replace(Value, "&", "")



            If Len(Value) = 12 Then

                If Mid(Value, 4, 1) = "-" Then

                    Return Mid(Value, 5, 8)

                End If

            End If



            Value = UCase(Trim(Value))



            If Mid(Value, 1, 2) = "B*" Then

                Return ""

            ElseIf Mid(Value, 1, 2) = "B-" Then

                Return ""

            End If



            If Len(Value) < 3 Then

                Return ""

            Else

                If Mid(Value, 1, 1) = "H" And Mid(Value, 3, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 1))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 4, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 2))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 5, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 3))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 6, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 4))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 7, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 5))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 8, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 6))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 9, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 7))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 10, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 8))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 11, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 9))

                ElseIf Mid(Value, 1, 1) = "H" And Mid(Value, 12, 1) = "+" Then

                    Return Trim(Mid(Value, 2, 10))

                ElseIf Mid(Value, 1, 1) = "M" And IsNumeric(Mid(Value, 2, Len(Value) - 1)) Then

                    Return Trim(Mid(Value, 2, Len(Value) - 1))

                ElseIf Mid(Value, 1, 1) = "H" And Len(Value) = 8 Then

                    Return Trim(Mid(Value, 2, 8))

                ElseIf Mid(Value, 1, 3) = "JFK" And Len(Value) = 15 Then

                    Return Trim(Mid(Value, 4, 7))

                ElseIf Not IsNumeric(Mid(Value, 1, 1)) And Not IsNumeric(Mid(Value, 2, 1)) And Not IsNumeric(Mid(Value, 3, 1)) And IsNumeric(Mid(Value, 4, 8)) And Len(Value) = 11 Then

                    Return Trim(Mid(Value, 4, 8))

                ElseIf Len(Value) = 16 Then

                    Return ""

                Else





                    Dim z() As String = Split(Value, "-")

                    If z.Length = 3 Then

                        If z(1) = "XA" Then

                            Return Trim(Mid(z(2), 1, 7))

                        End If

                    End If



                    'Value = Replace(Value, "+", "")
                    Dim index As Integer
                    index = Value.IndexOf("+")
                    Value = Value.Substring(0, Len(Value) - index) 'Trim(Value)
                    Return Trim(Value)
                End If

            End If





        Catch ex As Exception

            Return ""

        End Try

    End Function

End Class