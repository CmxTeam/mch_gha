﻿namespace CustomUtilities
{
    partial class EtaEditMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.message = new System.Windows.Forms.Label();


            this.lblHour = new System.Windows.Forms.Label();
            this.cmbTime = new System.Windows.Forms.ComboBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.cmbYear = new System.Windows.Forms.ComboBox();
            this.cmbDay = new System.Windows.Forms.ComboBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.lblMin = new System.Windows.Forms.Label();
            this.cmbMin = new System.Windows.Forms.ComboBox();

            this.panel1.SuspendLayout();
            this.SuspendLayout();
       

            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Controls.Add(this.message);
            this.panel1.Controls.Add(this.lblTitle);
            this.panel1.Controls.Add(this.lblHour);
            this.panel1.Controls.Add(this.cmbMonth);
            this.panel1.Controls.Add(this.cmbTime);
            this.panel1.Controls.Add(this.cmbDay);
            this.panel1.Controls.Add(this.lblYear);
            this.panel1.Controls.Add(this.cmbYear);
            this.panel1.Controls.Add(this.lblDay);
            this.panel1.Controls.Add(this.lblMonth);
            this.panel1.Controls.Add(this.lblMin);
            this.panel1.Controls.Add(this.cmbMin);


            this.panel1.Location = new System.Drawing.Point(3, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 171);
   

            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(127, 130);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
   

            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(56, 130);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
        

            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Location = new System.Drawing.Point(0, 126);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            

            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 24);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
           
 
       
            this.message.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.message.BackColor = System.Drawing.Color.White;
            this.message.Location = new System.Drawing.Point(13, 27);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(209, 20);
            this.message.Text = "";
            this.message.Visible = false;

            // 
            // lblHour
            // 
            this.lblHour.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblHour.Location = new System.Drawing.Point(147, 94);
            this.lblHour.Name = "lblHour";
            this.lblHour.Size = new System.Drawing.Size(37, 17);
            this.lblHour.Text = "HH";
            this.lblHour.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbTime
            // 
            this.cmbTime.Items.Add("00");
            this.cmbTime.Items.Add("01");
            this.cmbTime.Items.Add("02");
            this.cmbTime.Items.Add("03");
            this.cmbTime.Items.Add("04");
            this.cmbTime.Items.Add("05");
            this.cmbTime.Items.Add("06");
            this.cmbTime.Items.Add("07");
            this.cmbTime.Items.Add("08");
            this.cmbTime.Items.Add("09");
            this.cmbTime.Items.Add("10");
            this.cmbTime.Items.Add("11");
            this.cmbTime.Items.Add("12");
            this.cmbTime.Items.Add("13");
            this.cmbTime.Items.Add("14");
            this.cmbTime.Items.Add("15");
            this.cmbTime.Items.Add("16");
            this.cmbTime.Items.Add("17");
            this.cmbTime.Items.Add("18");
            this.cmbTime.Items.Add("19");
            this.cmbTime.Items.Add("20");
            this.cmbTime.Items.Add("21");
            this.cmbTime.Items.Add("22");
            this.cmbTime.Items.Add("23");
            this.cmbTime.Location = new System.Drawing.Point(144, 69);
            this.cmbTime.Name = "cmbTime";
            this.cmbTime.Size = new System.Drawing.Size(42, 22);
            this.cmbTime.TabIndex = 30;
            // 
            // lblYear
            // 
            this.lblYear.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblYear.Location = new System.Drawing.Point(98, 94);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(44, 17);
            this.lblYear.Text = "YY";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDay
            // 
            this.lblDay.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblDay.Location = new System.Drawing.Point(52, 94);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(44, 17);
            this.lblDay.Text = "DD";
            this.lblDay.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblMonth
            // 
            this.lblMonth.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblMonth.Location = new System.Drawing.Point(7, 94);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(44, 17);
            this.lblMonth.Text = "MM";
            this.lblMonth.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbYear
            // 
            this.cmbYear.Items.Add("14");
            this.cmbYear.Items.Add("15");
            this.cmbYear.Location = new System.Drawing.Point(98, 69);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(44, 22);
            this.cmbYear.TabIndex = 29;
            // 
            // cmbDay
            // 
            this.cmbDay.Items.Add("01");
            this.cmbDay.Items.Add("02");
            this.cmbDay.Items.Add("03");
            this.cmbDay.Items.Add("04");
            this.cmbDay.Items.Add("05");
            this.cmbDay.Items.Add("06");
            this.cmbDay.Items.Add("07");
            this.cmbDay.Items.Add("08");
            this.cmbDay.Items.Add("09");
            this.cmbDay.Items.Add("10");
            this.cmbDay.Items.Add("11");
            this.cmbDay.Items.Add("12");
            this.cmbDay.Items.Add("13");
            this.cmbDay.Items.Add("14");
            this.cmbDay.Items.Add("15");
            this.cmbDay.Items.Add("16");
            this.cmbDay.Items.Add("17");
            this.cmbDay.Items.Add("18");
            this.cmbDay.Items.Add("19");
            this.cmbDay.Items.Add("20");
            this.cmbDay.Items.Add("21");
            this.cmbDay.Items.Add("22");
            this.cmbDay.Items.Add("23");
            this.cmbDay.Items.Add("24");
            this.cmbDay.Items.Add("25");
            this.cmbDay.Items.Add("26");
            this.cmbDay.Items.Add("27");
            this.cmbDay.Items.Add("28");
            this.cmbDay.Items.Add("29");
            this.cmbDay.Items.Add("30");
            this.cmbDay.Items.Add("31");
            this.cmbDay.Location = new System.Drawing.Point(52, 69);
            this.cmbDay.Name = "cmbDay";
            this.cmbDay.Size = new System.Drawing.Size(44, 22);
            this.cmbDay.TabIndex = 28;
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblTitle.Location = new System.Drawing.Point(12, 42);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(192, 24);
            this.lblTitle.Text = "Update ETA:";
            // 
            // cmbMonth
            // 
            this.cmbMonth.Items.Add("01");
            this.cmbMonth.Items.Add("02");
            this.cmbMonth.Items.Add("03");
            this.cmbMonth.Items.Add("04");
            this.cmbMonth.Items.Add("05");
            this.cmbMonth.Items.Add("06");
            this.cmbMonth.Items.Add("07");
            this.cmbMonth.Items.Add("08");
            this.cmbMonth.Items.Add("09");
            this.cmbMonth.Items.Add("10");
            this.cmbMonth.Items.Add("11");
            this.cmbMonth.Items.Add("12");
            this.cmbMonth.Location = new System.Drawing.Point(7, 69);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(44, 22);
            this.cmbMonth.TabIndex = 26;
 
            // 
            // lblMin
            // 
            this.lblMin.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblMin.Location = new System.Drawing.Point(190, 94);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(37, 17);
            this.lblMin.Text = "MM";
            this.lblMin.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbMin
            // 
            this.cmbMin.Items.Add("01");
            this.cmbMin.Items.Add("02");
            this.cmbMin.Items.Add("03");
            this.cmbMin.Items.Add("04");
            this.cmbMin.Items.Add("05");
            this.cmbMin.Items.Add("06");
            this.cmbMin.Items.Add("07");
            this.cmbMin.Items.Add("08");
            this.cmbMin.Items.Add("09");
            this.cmbMin.Items.Add("10");
            this.cmbMin.Items.Add("11");
            this.cmbMin.Items.Add("12");
            this.cmbMin.Items.Add("13");
            this.cmbMin.Items.Add("14");
            this.cmbMin.Items.Add("15");
            this.cmbMin.Items.Add("16");
            this.cmbMin.Items.Add("17");
            this.cmbMin.Items.Add("18");
            this.cmbMin.Items.Add("19");
            this.cmbMin.Items.Add("20");
            this.cmbMin.Items.Add("21");
            this.cmbMin.Items.Add("22");
            this.cmbMin.Items.Add("23");
            this.cmbMin.Items.Add("24");
            this.cmbMin.Items.Add("25");
            this.cmbMin.Items.Add("26");
            this.cmbMin.Items.Add("27");
            this.cmbMin.Items.Add("28");
            this.cmbMin.Items.Add("29");
            this.cmbMin.Items.Add("30");
            this.cmbMin.Items.Add("31");
            this.cmbMin.Items.Add("32");
            this.cmbMin.Items.Add("33");
            this.cmbMin.Items.Add("34");
            this.cmbMin.Items.Add("35");
            this.cmbMin.Items.Add("36");
            this.cmbMin.Items.Add("37");
            this.cmbMin.Items.Add("38");
            this.cmbMin.Items.Add("39");
            this.cmbMin.Items.Add("40");
            this.cmbMin.Items.Add("41");
            this.cmbMin.Items.Add("42");
            this.cmbMin.Items.Add("43");
            this.cmbMin.Items.Add("44");
            this.cmbMin.Items.Add("45");
            this.cmbMin.Items.Add("46");
            this.cmbMin.Items.Add("47");
            this.cmbMin.Items.Add("48");
            this.cmbMin.Items.Add("49");
            this.cmbMin.Items.Add("50");
            this.cmbMin.Items.Add("51");
            this.cmbMin.Items.Add("52");
            this.cmbMin.Items.Add("53");
            this.cmbMin.Items.Add("54");
            this.cmbMin.Items.Add("55");
            this.cmbMin.Items.Add("56");
            this.cmbMin.Items.Add("57");
            this.cmbMin.Items.Add("58");
            this.cmbMin.Items.Add("59");
            this.cmbMin.Location = new System.Drawing.Point(187, 69);
            this.cmbMin.Name = "cmbMin";
            this.cmbMin.Size = new System.Drawing.Size(42, 22);
            this.cmbMin.TabIndex = 36;


            InitializeImages();
      
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "OriginDestinationMessageBox";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private System.Windows.Forms.PictureBox pictureBox1;


        private System.Windows.Forms.Label lblHour;
        private System.Windows.Forms.ComboBox cmbTime;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.ComboBox cmbYear;
        private System.Windows.Forms.ComboBox cmbDay;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ComboBox cmbMonth;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.ComboBox cmbMin;


      

    }
}