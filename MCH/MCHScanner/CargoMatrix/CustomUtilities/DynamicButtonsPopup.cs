﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    [Obsolete("Not used anymore")]
    internal partial class DynamicButtonsPopup : Form
    {
        public int Result { get; private set; }
        private List<CMXDynamicButton> buttons;
        public DynamicButtonsPopup(List<CMXDynamicButton> btns, bool twoline)
        {
            InitializeComponent();

            if (btns != null)
            {
                System.Drawing.Size sz = new System.Drawing.Size(160, 40);
                int startLoc;
                if (twoline)
                {
                    startLoc = 70;
                    label1.Visible = true;
                }
                else
                {
                    label1.Visible = false;
                    startLoc = 50;
                }
                int padding = 10;
                for (int i = 0; i < btns.Count; i++)
                {
                    CargoMatrix.UI.CMXTextButton btn = new CargoMatrix.UI.CMXTextButton();
                    btn.Location = new System.Drawing.Point(40, startLoc + i * (padding + sz.Height)); // Y gets calculated dynamically
                    btn.Size = sz;
                    btn.TabIndex = 2;
                    btn.Text = btns[i].Text;
                    btn.Image = btns[i].Image ?? CargoMatrix.Resources.Skin.btn;
                    btn.PressedImage = btns[i].PressedImage ?? CargoMatrix.Resources.Skin.btn_over;
                    btn.DisabledImage = btns[i].DisbaledImage ?? CargoMatrix.Resources.Skin.btn_dis;
                    btn.Click += new System.EventHandler(Button_Click);
                    btn.Tag = btns[i];
                    btn.Enabled = btns[i].Enabled;
                    //btn.Paint += new PaintEventHandler(btn_Paint);
                    this.panel1.Controls.Add(btn);

                }
                this.panel1.Height += btns.Count * (padding + sz.Height) + padding;
                this.panel1.Top = (this.Height - this.panel1.Height) / 2;
            }
        }
        public event EventHandler ButtonClicked;
        public DynamicButtonsPopup(List<CMXDynamicButton> btns) : this(btns, false) { }

        void btn_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(CargoMatrix.Resources.Skin.Close, new Rectangle(0, 0, 32, 32), new Rectangle(0, 0, 32, 32), GraphicsUnit.Pixel);
        }

        /// <summary>
        /// enables/disables the button search criteria (button.result)  
        /// </summary>
        /// <param name="result"></param>
        /// <param name="flag"></param>
        public void EnableButton(int result, bool flag)
        {
            foreach (Control c in this.panel1.Controls)
            {
                if (c is CargoMatrix.UI.CMXTextButton)
                {
                    CMXDynamicButton btn = (c as CargoMatrix.UI.CMXTextButton).Tag as CMXDynamicButton;
                    if (btn.Result == result)
                    {
                        c.Enabled = flag;
                        break;
                    }
                }

            }
        }

        public void Button_Click(object sender, EventArgs e)
        {
            this.Result = ((sender as CargoMatrix.UI.CMXTextButton).Tag as CMXDynamicButton).Result;
            if (ButtonClicked != null)
                ButtonClicked(this, e);
            else
                this.DialogResult = DialogResult.OK;
        }

        public string Header { get; set; }
        public string Label { get { return labelHeader.Text; } set { labelHeader.Text = value; } }
        public string Label2 { get { return label1.Text; } set { label1.Text = value; } }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }
        void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(Header, new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }

    }

    public class CMXDynamicButton
    {
        public string Text;
        public Image Image;
        public Image PressedImage;
        public Image DisbaledImage;
        public int Result;
        public bool Enabled = true;
    }
}