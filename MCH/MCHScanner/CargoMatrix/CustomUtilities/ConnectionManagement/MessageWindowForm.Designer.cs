﻿namespace CustomUtilities.ConnectionManagement
{
    partial class MessageWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
      
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.pcxFooter = new System.Windows.Forms.PictureBox();
            this.pcxHeader = new System.Windows.Forms.PictureBox();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.txtMessage);
            this.pnlMain.Controls.Add(this.pcxFooter);
            this.pnlMain.Controls.Add(this.pcxHeader);
            this.pnlMain.Location = new System.Drawing.Point(3, 65);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(234, 94);
            // 
            // txtMessage
            // 
            this.txtMessage.AcceptsReturn = true;
            this.txtMessage.AcceptsTab = true;
            this.txtMessage.Location = new System.Drawing.Point(3, 26);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ReadOnly = true;
            this.txtMessage.Size = new System.Drawing.Size(228, 44);
            this.txtMessage.TabIndex = 3;
            this.txtMessage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMessage.TextChanged += new System.EventHandler(this.txtMessage_TextChanged);
            // 
            // pcxFooter
            // 
            this.pcxFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pcxFooter.Location = new System.Drawing.Point(0, 74);
            this.pcxFooter.Name = "pcxFooter";
            this.pcxFooter.Size = new System.Drawing.Size(234, 20);
            this.pcxFooter.Image = global::Resources.Graphics.Skin.nav_bg;
            // 
            // pcxHeader
            // 
            this.pcxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcxHeader.Location = new System.Drawing.Point(0, 0);
            this.pcxHeader.Name = "pcxHeader";
            this.pcxHeader.Size = new System.Drawing.Size(234, 20);
            this.pcxHeader.Image = global::Resources.Graphics.Skin.popup_header;
            this.pcxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pcxHeader_Paint);
            // 
            // MessageWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.pnlMain);
            this.Name = "MessageWindowForm";
            this.Text = "MessageWindowForm";
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.PictureBox pcxHeader;
        private System.Windows.Forms.PictureBox pcxFooter;
        private System.Windows.Forms.TextBox txtMessage;
    }
}