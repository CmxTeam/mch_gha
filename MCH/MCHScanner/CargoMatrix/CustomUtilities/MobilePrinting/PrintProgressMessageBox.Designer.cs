﻿namespace CustomUtilities.MobilePrinting
{
    partial class PrintProgressMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
      
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCommunicationStatus = new System.Windows.Forms.Label();
            this.lblPrinterStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCommunicationStatus
            // 
            this.lblCommunicationStatus.Location = new System.Drawing.Point(20, 30);
            this.lblCommunicationStatus.Name = "lblText";
            this.lblCommunicationStatus.Size = new System.Drawing.Size(200, 70);
            this.lblCommunicationStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;


            // 
            // lblPrinterStatus
            // 
            this.lblPrinterStatus.Location = new System.Drawing.Point(20, 105);
            this.lblPrinterStatus.Name = "lblPrinterStatus";
            this.lblPrinterStatus.Size = new System.Drawing.Size(200, 70);
            this.lblPrinterStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;


            // 
            // PrintProgressMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.panelContent.Controls.Add(this.lblCommunicationStatus);
            this.panelContent.Controls.Add(lblPrinterStatus);
            this.Name = "PrintProgressMessageBox";
            this.Text = "UpdateMessageBox";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCommunicationStatus;
        private System.Windows.Forms.Label lblPrinterStatus;

    }
}