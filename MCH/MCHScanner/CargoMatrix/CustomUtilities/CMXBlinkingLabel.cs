﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace CustomUtilities
{
    public class CMXBlinkingLabel : Label
    {
        private int blinkCounter = 0;
        private Color blinkColor;
        private Timer blinktimer;
        private Timer splashTimer;
        public bool Blink
        {
            get { return blinktimer.Enabled; }
            set { blinktimer.Enabled = value; }
        }
        public new string Text
        {
            get { return base.Text; }
            set
            {
                if (IsDisposed)
                    return;
                if (splashTimer != null && splashTimer.Enabled)
                {
                    oldtext = value;
                }
                else base.Text = value;
            }
        }
        public override Color ForeColor
        {
            get
            {
                return blinkColor;
            }
            set
            {
                blinkColor = value;
            }
        }
        public CMXBlinkingLabel()
        {
            base.ForeColor = blinkColor;
            blinktimer = new Timer();
            blinktimer.Interval = 200;
            blinktimer.Tick += new EventHandler(blinktimer_Tick);
        }
        protected override void Dispose(bool disposing)
        {
            this.blinktimer.Enabled = false;
            base.Dispose(disposing);
        }
        void blinktimer_Tick(object sender, EventArgs e)
        {
            blinkCounter++;
            if (blinkCounter % 7 == 0)
            {
                blinkCounter = 0;
                base.ForeColor = this.BackColor;
            }
            else
                base.ForeColor = blinkColor;
        }

        string oldtext;
        Color oldColor;
        public void SplashText(string splashText)
        {
            if (splashTimer == null) // if timer is null - initialize
            {
                splashTimer = new Timer();
                splashTimer.Interval = 4000;
                splashTimer.Tick += new EventHandler(splashTimer_Tick);
            }
            if (!splashTimer.Enabled) // if its not splashing save current state (text and color) before splashing
            {
                oldColor = blinkColor;
                oldtext = Text;
            }
            else // if splashing stop splash timer
            {
                splashTimer.Enabled = false;
            }
            blinkColor = Color.Green;
            Text = splashText;
            splashTimer.Enabled = true;
        }

        void splashTimer_Tick(object sender, EventArgs e) // end of slash timer - restore saved state
        {
            splashTimer.Enabled = false;
            Text = oldtext;
            blinkColor = oldColor;
        }

    }
}
