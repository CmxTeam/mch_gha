﻿namespace CustomUtilities
{
    partial class NumericPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonNext = new CargoMatrix.UI.CMXTextButton();
            this.buttonPrev = new CargoMatrix.UI.CMXTextButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonOk = new CargoMatrix.UI.CMXPictureButton();
            this.buttonCancel = new CargoMatrix.UI.CMXPictureButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.panelButtons);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.buttonNext);
            this.panel1.Controls.Add(this.buttonPrev);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(3, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 271);
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panelButtons
            // 
            this.panelButtons.Location = new System.Drawing.Point(40, 27);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(155, 151);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(75, 193);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.Text = "1 to 9 of 18";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonNext
            // 
            this.buttonNext.BackColor = System.Drawing.SystemColors.Control;
            this.buttonNext.Image = null;
            this.buttonNext.Location = new System.Drawing.Point(163, 184);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.PressedImage = null;
            this.buttonNext.Size = new System.Drawing.Size(32, 32);
            this.buttonNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonNext.TransparentColor = System.Drawing.Color.White;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPrev.Image = null;
            this.buttonPrev.Location = new System.Drawing.Point(40, 184);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.PressedImage = null;
            this.buttonPrev.Size = new System.Drawing.Size(32, 32);
            this.buttonPrev.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonPrev.TransparentColor = System.Drawing.Color.White;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = Resources.Graphics.Skin.popup_header;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.SystemColors.Control;
            this.buttonOk.DisabledImage = Resources.Graphics.Skin.nav_ok_dis;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonOk.Location = new System.Drawing.Point(8, 231);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonOk.TransparentColor = System.Drawing.Color.White;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.SystemColors.Control;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.buttonCancel.Location = new System.Drawing.Point(64, 231);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonCancel.TransparentColor = System.Drawing.Color.White;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 229);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(234, 42);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.Image = Resources.Graphics.Skin.nav_bg;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(120, 231);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(52, 37);
            this.pictureBox3.Image = Resources.Graphics.Skin.nav_up_dis;
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(176, 231);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(52, 37);
            this.pictureBox4.Image = Resources.Graphics.Skin.nav_down_dis;
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // NumericPad
            // 
            InitializeButtons();

            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "NumericPad";
            this.Text = "NumericPad";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CargoMatrix.UI.CMXTextButton buttonPrev;
        private System.Windows.Forms.Label label2;
        private CargoMatrix.UI.CMXTextButton buttonNext;
        private CargoMatrix.UI.CMXPictureButton buttonOk;
        private CargoMatrix.UI.CMXPictureButton buttonCancel;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}