﻿namespace CustomUtilities
{
    partial class SignatureCaptureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
    
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pcxHeader = new System.Windows.Forms.PictureBox();
            this.buttonClear = new OpenNETCF.Windows.Forms.Button2();
            this.buttonCancel = new OpenNETCF.Windows.Forms.Button2();
            this.buttonOk = new OpenNETCF.Windows.Forms.Button2();
            this.lblSignatureRequired = new System.Windows.Forms.Label();
            this.signatureControl = new CustomUtilities.Signature();
            this.pcxFooter = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pcxHeader);
            this.panel1.Controls.Add(this.buttonClear);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.lblSignatureRequired);
            this.panel1.Controls.Add(this.signatureControl);
            this.panel1.Controls.Add(this.pcxFooter);
            this.panel1.Location = new System.Drawing.Point(7, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(226, 247);
            // 
            // pcxHeader
            // 
            this.pcxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcxHeader.Location = new System.Drawing.Point(0, 0);
            this.pcxHeader.Name = "pcxHeader";
            this.pcxHeader.Size = new System.Drawing.Size(226, 20);
            this.pcxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.Location = new System.Drawing.Point(85, 206);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(52, 37);
            this.buttonClear.TabIndex = 8;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(161, 206);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(52, 37);
            this.buttonCancel.TabIndex = 9;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(12, 206);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(52, 37);
            this.buttonOk.TabIndex = 10;
            // 
            // lblSignatureRequired
            // 
            this.lblSignatureRequired.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblSignatureRequired.Location = new System.Drawing.Point(15, 31);
            this.lblSignatureRequired.Name = "lblSignatureRequired";
            this.lblSignatureRequired.Size = new System.Drawing.Size(198, 17);
            this.lblSignatureRequired.Text = "Signature required";
            // 
            // signatureControl
            // 
            this.signatureControl.Location = new System.Drawing.Point(6, 58);
            this.signatureControl.Name = "signatureControl";
            this.signatureControl.Size = new System.Drawing.Size(214, 133);
            this.signatureControl.TabIndex = 7;
            // 
            // pcxFooter
            // 
            this.pcxFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pcxFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pcxFooter.Location = new System.Drawing.Point(0, 200);
            this.pcxFooter.Name = "pcxFooter";
            this.pcxFooter.Size = new System.Drawing.Size(226, 47);

            this.InitializeFields();

            // 
            // SignatureCaptureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "SignatureCaptureForm";
            this.Text = "SignatureCaptureForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pcxHeader;
        private OpenNETCF.Windows.Forms.Button2 buttonClear;
        private OpenNETCF.Windows.Forms.Button2 buttonCancel;
        private OpenNETCF.Windows.Forms.Button2 buttonOk;
        private System.Windows.Forms.Label lblSignatureRequired;
        private Signature signatureControl;
        private System.Windows.Forms.PictureBox pcxFooter;
        
    }
}