﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public partial class EnterManualBarcodePopup : Form
    {
        #region Fields
        CargoMatrix.UI.BarcodeReader barcode;
        string scannedPrefix;
        BarcodeType currentBarcodeType = BarcodeType.None;
        BarcodeType previousBarcodeType = BarcodeType.None;
        string scannedItemMainPart;
        CMXBarcode.ScanObject scanItem;
        bool displayHousebillPiece = true;
        CargoMatrix.Utilities.MessageListBox locationTypePopup;
        int pieces = 0;
        #endregion

        #region Constructors
        public EnterManualBarcodePopup(BarcodeType barcodeType, bool reset)
        {
            this.currentBarcodeType = barcodeType;
            Title = "Scan or enter number";
            this.barcode = new CargoMatrix.UI.BarcodeReader();

            InitializeComponent();
            if (reset)
            {
                Reset();
            }
        }

        public EnterManualBarcodePopup(BarcodeType barcodeType)
            : this(barcodeType, true)
        {
        }
        #endregion

        #region Methods
        public void Reset()
        {
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            barcode.StopRead();
            barcode.StartRead();
            SetPrefix();
        }

        private bool TryConvertToBarcodeType(CMXBarcode.BarcodeTypes cmxBarcodeType, out BarcodeType barcodeType)
        {
            barcodeType = BarcodeType.Housebill;

            switch (cmxBarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Truck:

                    barcodeType = BarcodeType.Truck;

                    break;

                case CMXBarcode.BarcodeTypes.Uld:

                    barcodeType = BarcodeType.ULD;

                    break;

                case CMXBarcode.BarcodeTypes.Door:

                    barcodeType = BarcodeType.Door;

                    break;

                case CMXBarcode.BarcodeTypes.HouseBill:

                    barcodeType = BarcodeType.Housebill;

                    break;

                case CMXBarcode.BarcodeTypes.MasterBill:

                    barcodeType = BarcodeType.Masterbill;

                    break;

                case CMXBarcode.BarcodeTypes.Area:

                    barcodeType = BarcodeType.Area;

                    break;

                case CMXBarcode.BarcodeTypes.ScreeningArea:

                    barcodeType = BarcodeType.ScreeningArea;

                    break;

                case CMXBarcode.BarcodeTypes.User:

                    barcodeType = BarcodeType.User;

                    break;

                case CMXBarcode.BarcodeTypes.OnHand:

                    barcodeType = BarcodeType.OnHand;

                    break;

                default:
                    return false;

            }

            return true;
        }

        private void SetPrefix()
        {
            SetPrefix(currentBarcodeType);
        }

        private void SetPrefix(BarcodeType argBarcodeType)
        {
            switch (argBarcodeType)
            {
                case BarcodeType.All:
                    break;
                case BarcodeType.ULD:
                    scannedPrefix = "L";
                    break;
                case BarcodeType.Truck:
                    scannedPrefix = "T";
                    break;
                case BarcodeType.Door:
                    scannedPrefix = "D";
                    break;
                case BarcodeType.Housebill:
                    scannedPrefix = "H";
                    break;
                case BarcodeType.Masterbill:
                    scannedPrefix = string.Empty;
                    break;
                case BarcodeType.Area:
                    scannedPrefix = "B";
                    break;
                case BarcodeType.ScreeningArea:
                    scannedPrefix = "S";
                    break;
                case BarcodeType.Location:
                    break;
                case BarcodeType.Printer:
                    break;
                case BarcodeType.User:
                    scannedPrefix = "U";
                    break;
                case BarcodeType.OnHand:
                    scannedPrefix = "O";
                    break;
                default:
                    break;
            }
        }

        private string GetPieceNumberPostfix(int pieces)
        {
            string result = "";
            if (pieces >= 1 && pieces <= 9)
            {
                result = "+Y000" + pieces + "+";
            }
            else if (pieces >= 10 && pieces <= 99)
            {
                result = "+Y00" + pieces + "+";
            }
            else if (pieces >= 100 && pieces <= 999)
            {
                result = "+Y0" + pieces + "+";
            }
            else if (pieces >= 1000 && pieces <= 9999)
            {
                result = "+Y" + pieces + "+";
            }
            return result;
        }

        private string GetBarcode(string text)
        {
            return this.GetBarcode(text, this.currentBarcodeType);
        }

        private string GetBarcode(string text, BarcodeType barcodeType)
        {
            switch (barcodeType)
            {
                case BarcodeType.Door: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixDoor, text);

                case BarcodeType.Housebill: return string.Format("{0}+Y0001+", text);

                case BarcodeType.Masterbill: return text;

                case BarcodeType.Truck: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixTruck, text);

                case BarcodeType.ULD: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixULD, text);

                case BarcodeType.Area: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixArea, text);

                case BarcodeType.ScreeningArea: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixScreeningArea, text);

                default: return null;
            }
        }

        private string GetScannedItemMainPart(CMXBarcode.ScanObject scanItem)
        {
            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill: return scanItem.HouseBillNumber;

                case CMXBarcode.BarcodeTypes.Truck: return scanItem.Location;

                case CMXBarcode.BarcodeTypes.Uld: return scanItem.UldNumber;

                case CMXBarcode.BarcodeTypes.Door: return scanItem.Location;

                case CMXBarcode.BarcodeTypes.Area: return scanItem.Location;

                case CMXBarcode.BarcodeTypes.ScreeningArea: return scanItem.Location;

                case CMXBarcode.BarcodeTypes.MasterBill: return scanItem.MasterBillNumber;


                default: return string.Empty;
            }
        }

        private KeyValuePair<BarcodeType, string> ChooseLocationType()
        {
            KeyValuePair<BarcodeType, string> currentLocationType = new KeyValuePair<BarcodeType, string>();
            if (locationTypePopup == null)
            {
                Control[] actions = new Control[]
                { 
                    new SmoothListbox.ListItems.StandardListItem("Rack / Bin Area",null,1),
                    new SmoothListbox.ListItems.StandardListItem("Screening Area",null,2),
                    new SmoothListbox.ListItems.StandardListItem("Door",null,3),
                    new SmoothListbox.ListItems.StandardListItem("Truck",null,4),
                    new SmoothListbox.ListItems.StandardListItem("ULD",null,5),
                };

                locationTypePopup = new CargoMatrix.Utilities.MessageListBox();
                locationTypePopup.AddItems(actions);
                locationTypePopup.MultiSelectListEnabled = false;
                locationTypePopup.OneTouchSelection = true;
            }
            locationTypePopup.HeaderText = "Location Types";
            locationTypePopup.HeaderText2 = "Select location type";

            if (DialogResult.OK == locationTypePopup.ShowDialog())
            {
                var item = (locationTypePopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem);
                switch (item.ID)
                {
                    case 1: // Rack / Bin Area
                        currentLocationType = new KeyValuePair<BarcodeType, string>(BarcodeType.Area, item.title.Text);
                        break;
                    case 2: // Screening Area
                        currentLocationType = new KeyValuePair<BarcodeType, string>(BarcodeType.ScreeningArea, item.title.Text);
                        break;
                    case 3: // Door
                        currentLocationType = new KeyValuePair<BarcodeType, string>(BarcodeType.Door, item.title.Text);
                        break;
                    case 4: // Truck
                        currentLocationType = new KeyValuePair<BarcodeType, string>(BarcodeType.Truck, item.title.Text);
                        break;
                    case 5: // ULD
                        currentLocationType = new KeyValuePair<BarcodeType, string>(BarcodeType.ULD, item.title.Text);
                        break;
                }
            }
            locationTypePopup.ResetSelection();
            return currentLocationType;
        }
        #endregion

        #region Event Handlers
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (currentBarcodeType == BarcodeType.Location)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Please select a location type.", "Location Type Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (currentBarcodeType == BarcodeType.Housebill)
            {
                try
                {
                    pieces = Int32.Parse(txtPieceNumber.Text);
                    if (pieces <= 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Piece number must be more than 0 and less than 9999.", "Piece Number Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }
                }
                catch
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Please insert a valid piece number.", "Piece Number Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }
            }

            textBox1.Text = textBox1.Text.ToUpper();
            textBox2.Text = textBox2.Text.ToUpper();

            SetPrefix();

            this.scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(this.ScannedText);

            if (this.currentBarcodeType == BarcodeType.Printer ||
                this.currentBarcodeType == BarcodeType.All)
            {
                this.scannedItemMainPart = this.currentBarcodeType == BarcodeType.Housebill ? this.textBox1.Text : this.textBox2.Text;
                barcode.StopRead();
                DialogResult = DialogResult.OK;
                return;
            }

            BarcodeType tmpBarcodeType;
            bool result = this.TryConvertToBarcodeType(scanItem.BarcodeType, out tmpBarcodeType);

            if (result == false && this.currentBarcodeType != BarcodeType.Location)
            {
                if (this.currentBarcodeType == BarcodeType.Masterbill)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }

                string reconstructedBarcode = this.GetBarcode(this.currentBarcodeType == BarcodeType.Housebill ? this.textBox1.Text : this.textBox2.Text);

                if (reconstructedBarcode == null)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }

                this.scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(reconstructedBarcode);

                result = this.TryConvertToBarcodeType(scanItem.BarcodeType, out tmpBarcodeType);

                if (result == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }
            }

            if (this.currentBarcodeType == BarcodeType.Location &&
                tmpBarcodeType != BarcodeType.Area &&
                tmpBarcodeType != BarcodeType.Door &&
                tmpBarcodeType != BarcodeType.ScreeningArea)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (tmpBarcodeType != this.currentBarcodeType &&
                this.currentBarcodeType != BarcodeType.Location)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }


            this.scannedItemMainPart = this.GetScannedItemMainPart(scanItem);

            barcode.StopRead();
            DialogResult = DialogResult.OK;

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = string.Empty;
            this.textBox2.Text = string.Empty;
            barcode.StopRead();

            DialogResult = DialogResult.Cancel;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == string.Empty)
            {
                buttonOk.Enabled = false;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok_dis;
            }
            else
            {
                buttonOk.Enabled = true;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            }
            SetPrefix();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text == string.Empty)
            {
                buttonOk.Enabled = false;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok_dis;
            }
            else
            {
                buttonOk.Enabled = true;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            }
            SetPrefix();
        }

        private void txtPieceNumber_TextChanged(object sender, EventArgs e)
        {
            this.rbnHousebillNo.Checked = true;
        }

        private void ScanEnterPopup_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                buttonOk_Click(this.buttonOk, EventArgs.Empty);
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                e.Graphics.DrawString(Title, font, new SolidBrush(Color.White), 5, 3);
            }
        }

        private void btnLocationType_Click(object sender, System.EventArgs e)
        {
            this.rbnLocation.Checked = true;
            var locationInfo = ChooseLocationType();
            this.currentBarcodeType = locationInfo.Key;
            this.txtLocationType.Text = locationInfo.Value;
        }

        private void rbnHousebillNo_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbnHousebillNo.Checked)
            {
                previousBarcodeType = currentBarcodeType;
                currentBarcodeType = BarcodeType.Housebill;
                this.labelBarcodeType.Text = "Hawb#";
                //this.textBox1.Size = new System.Drawing.Size(130, 28);
                this.textBox1.Visible = true;
                this.labelPieceNumber.Visible = true;
                this.txtPieceNumber.Visible = true;

                this.textBox2.Visible = false;
                this.labelLocationType.Visible = false;
                this.txtLocationType.Visible = false;
                this.btnLocationType.Visible = false;
            }
        }

        private void rbnLocation_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbnLocation.Checked)
            {
                if (previousBarcodeType != BarcodeType.None)
                {
                    currentBarcodeType = previousBarcodeType;
                }
                else
                {
                    previousBarcodeType = currentBarcodeType;
                    currentBarcodeType = BarcodeType.Location;
                }
                this.labelBarcodeType.Text = "Location";
                //this.textBox1.Size = new System.Drawing.Size(210, 28);
                this.textBox1.Visible = false;
                this.labelPieceNumber.Visible = false;
                this.txtPieceNumber.Visible = false;

                this.textBox2.Visible = true;
                this.labelLocationType.Visible = true;
                this.txtLocationType.Visible = true;
                this.btnLocationType.Visible = true;
            }
        }
        #endregion

        #region Properties
        public CMXBarcode.ScanObject ScanItem
        {
            get { return scanItem; }
        }

        public bool DisplayHousebillPiece
        {
            get { return displayHousebillPiece; }
            set { this.displayHousebillPiece = value; }
        }

        public string OKText
        {
            get { return buttonOk.Text; }
            set { buttonOk.Text = value; }
        }

        public string CancelText
        {
            get { return buttonCancel.Text; }
            set { buttonCancel.Text = value; }
        }

        public string Title
        {
            get;
            set;
        }

        public string ScannedText
        {
            get
            {
                if (string.IsNullOrEmpty(scannedPrefix))
                    return this.currentBarcodeType == BarcodeType.Housebill ? this.textBox1.Text : this.textBox2.Text;
                else
                {
                    string text = this.currentBarcodeType == BarcodeType.Housebill ? this.textBox1.Text : this.textBox2.Text;
                    if (currentBarcodeType == BarcodeType.OnHand || currentBarcodeType == BarcodeType.Housebill)
                        if (pieces > 0)
                        {
                            return scannedPrefix + text + GetPieceNumberPostfix(pieces);
                        }
                        else
                        {
                            return scannedPrefix + text;
                        }
                    else
                        return scannedPrefix + '-' + text;
                }
            }
            set { textBox1.Text = value.ToUpper(); }
        }

        public string ScannedItemMainPart
        {
            get
            {
                return this.scannedItemMainPart;
            }
        }
        #endregion
    }
}