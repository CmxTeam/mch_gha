﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CustomUtilities
{
    public enum BarcodeType { None, All, ULD, Truck, Door, Housebill, Masterbill, Area, ScreeningArea, Location, Printer, User, OnHand }
    public partial class ScanEnterPopup_old : Form
    {
        #region Fields
        CargoMatrix.UI.BarcodeReader barcode;
        string scannedPrefix;
        BarcodeType barcodeType;
        string scannedItemMainPart;
        CMXBarcode.ScanObject scanItem;
        bool displayHousebillPiece = true; 
        #endregion

        #region Constructors
        public ScanEnterPopup_old(BarcodeType barcodeType, bool reset)
        {
            InitializeComponent();
            this.barcodeType = barcodeType;
            Title = "Scan or enter number";
            this.barcode = new CargoMatrix.UI.BarcodeReader();
            barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcodeReader_BarcodeReadNotify);
            if (reset)
            {
                Reset();
            }
        }

        public ScanEnterPopup_old(BarcodeType barcodeType)
            : this(barcodeType, true)
        {
        } 
        #endregion

        #region Methods
        public void Reset()
        {
            textBox1.Text = string.Empty;
            labelText.Text = string.Empty;
            barcode.StopRead();
            barcode.StartRead();
            setPrefix();
        }

        private bool TryConvertToBarcodeType(CMXBarcode.BarcodeTypes cmxBarcodeType, out BarcodeType barcodeType)
        {
            barcodeType = BarcodeType.Housebill;

            switch (cmxBarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Truck:

                    barcodeType = BarcodeType.Truck;

                    break;

                case CMXBarcode.BarcodeTypes.Uld:

                    barcodeType = BarcodeType.ULD;

                    break;

                case CMXBarcode.BarcodeTypes.Door:

                    barcodeType = BarcodeType.Door;

                    break;

                case CMXBarcode.BarcodeTypes.HouseBill:

                    barcodeType = BarcodeType.Housebill;

                    break;

                case CMXBarcode.BarcodeTypes.MasterBill:

                    barcodeType = BarcodeType.Masterbill;

                    break;

                case CMXBarcode.BarcodeTypes.Area:

                    barcodeType = BarcodeType.Area;

                    break;

                case CMXBarcode.BarcodeTypes.ScreeningArea:

                    barcodeType = BarcodeType.ScreeningArea;

                    break;

                case CMXBarcode.BarcodeTypes.User:

                    barcodeType = BarcodeType.User;

                    break;

                case CMXBarcode.BarcodeTypes.OnHand:

                    barcodeType = BarcodeType.OnHand;

                    break;

                default:
                    return false;

            }

            return true;
        }

        private void setPrefix()
        {
            switch (barcodeType)
            {
                case BarcodeType.ULD:
                    scannedPrefix = "L";
                    break;
                case BarcodeType.Truck:
                    scannedPrefix = "T";
                    break;
                case BarcodeType.Door:
                    scannedPrefix = "D";
                    break;

                case BarcodeType.User:
                    scannedPrefix = "U";
                    break;
                case BarcodeType.OnHand:
                    scannedPrefix = "O";
                    break;

                case BarcodeType.Masterbill:
                    scannedPrefix = string.Empty;
                    break;
            }
        }

        private string GetBarcode(string text)
        {
            return this.GetBarcode(text, this.barcodeType);
        }

        private string GetBarcode(string text, BarcodeType barcodeType)
        {
            switch (barcodeType)
            {
                case BarcodeType.Door: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixDoor, text);

                case BarcodeType.Housebill: return string.Format("{0}+Y0001+", text);

                case BarcodeType.Masterbill: return text;

                case BarcodeType.Truck: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixTruck, text);

                case BarcodeType.ULD: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixULD, text);

                case BarcodeType.Area: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixArea, text);

                case BarcodeType.ScreeningArea: return string.Format("{0}-{1}", CMXBarcode.BarcodeParser.PrefixScreeningArea, text);

                default: return null;
            }
        }

        private string GetScannedItemMainPart(CMXBarcode.ScanObject scanItem)
        {
            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill: return scanItem.HouseBillNumber;

                case CMXBarcode.BarcodeTypes.Truck: return scanItem.Location;

                case CMXBarcode.BarcodeTypes.Uld: return scanItem.UldNumber;

                case CMXBarcode.BarcodeTypes.Door: return scanItem.Location;

                case CMXBarcode.BarcodeTypes.Area: return scanItem.Location;

                case CMXBarcode.BarcodeTypes.ScreeningArea: return scanItem.Location;

                case CMXBarcode.BarcodeTypes.MasterBill: return scanItem.MasterBillNumber;


                default: return string.Empty;
            }
        }
        #endregion

        #region Event Handlers
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {
                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));

                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }
            }
        }

        void barcodeReader_BarcodeReadNotify(string barcodeData)
        {
            if (this.barcodeType == BarcodeType.All)
            {
                this.textBox1.Text = barcodeData;
                // Disable barcode scanning for manual case
                //barcode.StartRead();
                return;
            }

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            BarcodeType barcodeType;
            bool result = this.TryConvertToBarcodeType(scanItem.BarcodeType, out barcodeType);

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Truck:
                case CMXBarcode.BarcodeTypes.Uld:
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.User:

                    int startIndex = -1;
                    if (barcodeData.Contains("-"))
                        startIndex = barcodeData.IndexOf('-');
                    else
                        startIndex = barcodeData.IndexOf('*');

                    if (startIndex > -1)
                    {
                        scannedPrefix = barcodeData.Substring(0, startIndex).ToUpper();

                        if (result == true &&
                            this.barcodeType != barcodeType)
                        {
                            textBox1.Text = barcodeData;
                        }
                        else
                        {
                            textBox1.Text = this.barcodeType == BarcodeType.Location ?
                            barcodeData :
                            barcodeData.Substring(startIndex + 1);
                        }

                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    }
                    else
                    {
                        scannedPrefix = string.Empty;
                        textBox1.Text = string.Empty;

                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                    break;

                case CMXBarcode.BarcodeTypes.HouseBill:
                    textBox1.Text = this.displayHousebillPiece == true ? barcodeData : scanItem.HouseBillNumber;

                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    break;
                case CMXBarcode.BarcodeTypes.OnHand:

                    textBox1.Text = this.displayHousebillPiece == true ? barcodeData : scanItem.OnHandNumber;

                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    break;

                case CMXBarcode.BarcodeTypes.MasterBill:
                    textBox1.Text = barcodeData;
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    break;


                default:

                    if (this.barcodeType == BarcodeType.Printer)
                    {
                        textBox1.Text = barcodeData;
                        scannedPrefix = string.Empty;
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        return;
                    }

                    scannedPrefix = string.Empty;
                    textBox1.Text = barcodeData;
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

                    break;
            }

            this.textBox1.Text = this.textBox1.Text.ToUpper();
            barcode.StartRead();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text.ToUpper();

            this.scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(this.ScannedText);

            if (this.barcodeType == BarcodeType.Printer ||
                this.barcodeType == BarcodeType.All)
            {
                this.scannedItemMainPart = this.textBox1.Text;
                barcode.StopRead();
                DialogResult = DialogResult.OK;
                return;
            }


            BarcodeType barcodeType;
            bool result = this.TryConvertToBarcodeType(scanItem.BarcodeType, out barcodeType);

            if (result == false && this.barcodeType != BarcodeType.Location)
            {
                if (this.barcodeType == BarcodeType.Masterbill)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }

                string reconstructedBarcode = this.GetBarcode(textBox1.Text);

                if (reconstructedBarcode == null)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }

                this.scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(reconstructedBarcode);

                result = this.TryConvertToBarcodeType(scanItem.BarcodeType, out barcodeType);

                if (result == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }
            }

            if (this.barcodeType == BarcodeType.Location &&
                barcodeType != BarcodeType.Area &&
                barcodeType != BarcodeType.Door &&
                barcodeType != BarcodeType.ScreeningArea)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }

            if (barcodeType != this.barcodeType &&
                this.barcodeType != BarcodeType.Location)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }


            this.scannedItemMainPart = this.GetScannedItemMainPart(scanItem);

            barcode.StopRead();
            DialogResult = DialogResult.OK;

        }

        public void ClosePopUp()
        {
            try
            {
                barcode.StopRead();
                DialogResult = DialogResult.OK;
            }
            catch { }
 
      
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = string.Empty;
            barcode.StopRead();

            DialogResult = DialogResult.Cancel;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == string.Empty)
            {
                buttonOk.Enabled = false;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok_dis;
            }
            else
            {
                buttonOk.Enabled = true;
                buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            }
            setPrefix();
        }

        private void ScanEnterPopup_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                buttonOk_Click(this.buttonOk, EventArgs.Empty);
            }

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                e.Graphics.DrawString(Title, font, new SolidBrush(Color.White), 5, 3);
            }
        }
        #endregion

        #region Properties
        public CMXBarcode.ScanObject ScanItem
        {
            get { return scanItem; }
        }

        public bool DisplayHousebillPiece
        {
            get { return displayHousebillPiece; }
            set { this.displayHousebillPiece = value; }
        }

        public string OKText
        {
            get { return buttonOk.Text; }
            set { buttonOk.Text = value; }
        }

        public string CancelText
        {
            get { return buttonCancel.Text; }
            set { buttonCancel.Text = value; }
        }

        public string TextLabel
        {
            get { return this.labelText.Text; }
            set { this.labelText.Text = value; }
        }

        public string Title
        {
            get;
            set;
        }

        public string ScannedText
        {
            get
            {
                if (string.IsNullOrEmpty(scannedPrefix))
                    return textBox1.Text;
                else
                    if (barcodeType == BarcodeType.OnHand)
                        return scannedPrefix + textBox1.Text;
                    else
                        return scannedPrefix + '-' + textBox1.Text;
            }
            set { textBox1.Text = value.ToUpper(); }
        }

        public override string Text
        {
            get
            {
                return this.textBox1.Text;
            }
            set
            {
                this.textBox1.Text = value;
                this.setPrefix();
            }
        }

        public string ScannedItemMainPart
        {
            get
            {
                return this.scannedItemMainPart;
            }
        }
        #endregion
    }
}