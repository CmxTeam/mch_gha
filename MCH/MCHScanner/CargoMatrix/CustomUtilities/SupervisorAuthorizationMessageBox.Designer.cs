﻿namespace CustomUtilities
{
    partial class SupervisorAuthorizationMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.Label();
            this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
            this.buttonOk = new OpenNETCF.Windows.Forms.Button2();
            this.buttonCancel = new OpenNETCF.Windows.Forms.Button2();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.message);
            this.panel1.Controls.Add(this.cmxTextBox1);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Location = new System.Drawing.Point(3, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 209);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(3, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 19);
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // message
            // 
            this.message.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.message.BackColor = System.Drawing.Color.White;
            this.message.Location = new System.Drawing.Point(55, 30);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(175, 48);
            // 
            // cmxTextBox1
            // 
            this.cmxTextBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBox1.Location = new System.Drawing.Point(3, 131);
            this.cmxTextBox1.Name = "cmxTextBox1";
            this.cmxTextBox1.Size = new System.Drawing.Size(228, 28);
            this.cmxTextBox1.TabIndex = 3;
            this.cmxTextBox1.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.cmxTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmxTextBox1_KeyDown);
            // 
            // buttonOk
            // 
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(21, 170);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(77, 25);
            this.buttonOk.TabIndex = 6;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(132, 170);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(77, 25);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(6, 30);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(48, 48);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 24);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;

            // 
            // SupervisorAuthorizationMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Size = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "SupervisorAuthorizationMessageBox";
            this.Text = "SupervisorAuthorizationMessageBox";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private OpenNETCF.Windows.Forms.Button2 buttonOk;
        private OpenNETCF.Windows.Forms.Button2 buttonCancel;
        public System.Windows.Forms.PictureBox pictureBox;
        public System.Windows.Forms.PictureBox pictureBoxHeader;
        public CargoMatrix.UI.CMXTextBox cmxTextBox1;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label message;
        //private Barcode.Barcode barcode1;
    }
}