﻿namespace CustomUtilities
{
    partial class PassFailSlacSampleMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.buttonCancel = new OpenNETCF.Windows.Forms.Button2();
            this.buttonOk = new OpenNETCF.Windows.Forms.Button2();
            this.labelMessage = new System.Windows.Forms.Label();
            this.labelSample = new System.Windows.Forms.Label();
            this.labelPieces = new System.Windows.Forms.Label();
            this.cmxTextBoxSample = new CargoMatrix.UI.CMXTextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelMessage);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Controls.Add(this.labelSample);
            this.panel1.Controls.Add(this.labelPieces);
            this.panel1.Controls.Add(this.cmxTextBoxSample);
            this.panel1.Location = new System.Drawing.Point(4, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(232, 183);
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Size = new System.Drawing.Size(232, 23);
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(pictureBoxHeader_Paint);
            // 
            // buttonCancel
            // 
            this.buttonCancel.ActiveBackColor = System.Drawing.Color.White;
            this.buttonCancel.ActiveBorderColor = System.Drawing.Color.White;
            this.buttonCancel.ActiveForeColor = System.Drawing.Color.Black;
            this.buttonCancel.BorderColor = System.Drawing.Color.White;
            this.buttonCancel.DisabledBackColor = System.Drawing.Color.White;
            this.buttonCancel.DisabledBorderColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(129, 116);
            this.buttonCancel.BackColor = System.Drawing.Color.White;
            this.buttonCancel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(96, 59);
            this.buttonCancel.TabStop = false;
            this.buttonCancel.Text = "Fail";
            this.buttonCancel.TransparentImage = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);

            // 
            // buttonOk
            // 
            this.buttonOk.ActiveBackColor = System.Drawing.Color.White;
            this.buttonOk.ActiveBorderColor = System.Drawing.Color.White;
            this.buttonOk.ActiveForeColor = System.Drawing.Color.Black;
            this.buttonOk.BorderColor = System.Drawing.Color.White;
            this.buttonOk.DisabledBackColor = System.Drawing.Color.White;
            this.buttonOk.DisabledBorderColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(12, 116);
            this.buttonOk.BackColor = System.Drawing.Color.White;
            this.buttonOk.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(96, 59);
            this.buttonOk.TabStop = false;
            this.buttonOk.Text = "Pass";
            this.buttonOk.TransparentImage = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // labelMessage
            // 
            this.labelMessage.BackColor = System.Drawing.Color.White;
            this.labelMessage.Location = new System.Drawing.Point(6, 31);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(220, 15);
            // 
            // PassFailMessageBox
            // 
            this.labelSample.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.labelSample.BackColor = System.Drawing.Color.White;
            this.labelSample.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelSample.ForeColor = System.Drawing.Color.Black;
            this.labelSample.Location = new System.Drawing.Point(11, 84);
            this.labelSample.Name = "labelSample";
            this.labelSample.Size = new System.Drawing.Size(65, 21);
            this.labelSample.Text = "Sample#:";
            this.labelSample.TextAlign = System.Drawing.ContentAlignment.TopLeft;


            this.labelPieces.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.labelPieces.BackColor = System.Drawing.Color.White;
            this.labelPieces.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelPieces.ForeColor = System.Drawing.Color.Black;
            this.labelPieces.Location = new System.Drawing.Point(11, 56);
            this.labelPieces.Name = "labelPieces";
            this.labelPieces.Size = new System.Drawing.Size(214, 21);
            this.labelPieces.Text = "Pieces:";
            this.labelPieces.TextAlign = System.Drawing.ContentAlignment.TopLeft;



            this.cmxTextBoxSample.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmxTextBoxSample.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cmxTextBoxSample.Location = new System.Drawing.Point(82, 80);
            this.cmxTextBoxSample.MaxLength = 20;
            this.cmxTextBoxSample.Name = "cmxTextBoxSample";
            this.cmxTextBoxSample.Size = new System.Drawing.Size(143, 28);
            this.cmxTextBoxSample.TabIndex = 3;
            this.cmxTextBoxSample.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;


            initializeImages();

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private OpenNETCF.Windows.Forms.Button2 buttonCancel;
        private OpenNETCF.Windows.Forms.Button2 buttonOk;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Label labelSample;
        private System.Windows.Forms.Label labelPieces;
        public CargoMatrix.UI.CMXTextBox cmxTextBoxSample;

    }
}