﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CustomUtilities
{
    public partial class SupervisorAuthorizationMessageBox : CargoMatrix.UI.MessageBoxBase// Form
    {
        protected Timer m_lifeTimer;
        public string HeaderText;
        protected CMXUserControl barcodeControl;
        protected static SupervisorAuthorizationMessageBox instance = null;
        public SupervisorAuthorizationMessageBox()
        {
            InitializeComponent();
            cmxTextBox1.PasswordChar = '*';
            barcodeControl = new CMXUserControl();
            barcodeControl.BarcodeReadNotify += new BarcodeReadNotifyHandler(barcodeControl_BarcodeReadNotify);
            barcodeControl.Enabled = true;

            pictureBoxHeader.Paint += new PaintEventHandler(pictureBoxHeader_Paint);
            buttonOk.BackgroundImage = Resources.Graphics.Skin.mgBox_default;
            buttonOk.ActiveBackgroundImage = Resources.Graphics.Skin.mgBox_Over;

            buttonCancel.BackgroundImage = Resources.Graphics.Skin.mgBox_default;
            buttonCancel.ActiveBackgroundImage = Resources.Graphics.Skin.mgBox_Over;
            buttonOk.Text = CustomUtilitiesResource.Text_Ok;
            buttonCancel.Text = CustomUtilitiesResource.Text_Cancel;
            m_lifeTimer = new Timer();
            m_lifeTimer.Tick += new System.EventHandler(LifeTimer_Tick);
            m_lifeTimer.Enabled = false;
            this.m_lifeTimer.Interval = 120000;
        }

        void barcodeControl_BarcodeReadNotify(string barcodeData)
        {
            cmxTextBox1.Text = barcodeData;
            barcodeControl.BarcodeEnabled = true;
            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
            this.buttonOk_Click(this, EventArgs.Empty);
            //throw new NotImplementedException();
        }


        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {

            e.Graphics.DrawString(HeaderText, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
        }

        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }


        protected virtual void buttonOk_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (cmxTextBox1.Text != "")
            {

                try
                {
                    if (CargoMatrix.Communication.HostPlusIncomming.Instance.VerifySupervisorPIN(cmxTextBox1.Text))
                    {
                        m_lifeTimer.Enabled = false;
                        DialogResult = DialogResult.OK;
                    }
                    else
                        CargoMatrix.UI.CMXMessageBox.Show(CustomUtilitiesResource.Text_InvalidPin, CustomUtilitiesResource.Text_Error, CMXMessageBoxIcon.Exclamation);
                }
                catch (Exception ex)
                {
                    //CargoMatrix.UI.CMXMessageBox.Show(ex.Message, "Error!" + " (40001)", CMXMessageBoxIcon.Exclamation);
                    barcodeControl.BarcodeEnabled = false;

                    if (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn() == true)
                    {

                        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 40001);
                        barcodeControl.BarcodeEnabled = true;
                    }

                    else// (CargoMatrix.UI.CMXAnimationmanager.IsLoggedIn() == false)
                    {

                        DialogResult = DialogResult.Abort;

                    }

                }


                //DialogResult = DialogResult.OK;
            }
            cmxTextBox1.SelectAll();
            cmxTextBox1.Focus();
            Cursor.Current = Cursors.Default;
        }
        public static DialogResult Show(UserControl control, string Title)
        {

            //if (instance == null)
            instance = new SupervisorAuthorizationMessageBox();
            instance.SuspendLayout();

            //instance.buttonOk.Left = (instance.Width - instance.buttonOk.Width) / 2;
            //instance.buttonCancel.Visible = false;
            //instance.pictureBox.Image = UtilitiesResource.Symbol_Security;
            instance.HeaderText = Title;// UtilitiesResource.Text_SupervisorCaption;
            instance.pictureBox.Image = CustomUtilitiesResource.Symbol_Security;
            instance.message.Text = CustomUtilitiesResource.Text_SupervisorMessage;
            instance.label1.Text = CustomUtilitiesResource.Text_SupervisorMessage2;

            instance.ResumeLayout();
            //instance.DeScale();
            //instance.Animate();

            return instance.CMXShowDialog(control);
        }
        public DialogResult CMXShowDialog(UserControl control)
        {

            bool prevStatus = false;
            if (control is CMXUserControl)
            {
                prevStatus = (control as CMXUserControl).BarcodeEnabled;
                (control as CMXUserControl).BarcodeEnabled = false;
            }
            //control.Enabled = false;
            //this.barcode1.EnableScanner = true;
            barcodeControl.BarcodeEnabled = true;
            m_lifeTimer.Enabled = true;
            DialogResult result = ShowDialog();
            //this.barcode1.EnableScanner = false;
            barcodeControl.BarcodeEnabled = false;
            if (control is CMXUserControl)
                (control as CMXUserControl).BarcodeEnabled = prevStatus;

            //control.Enabled = true;
            //isShowing = false;
            return result;// instance.cmxTextBox1.Text;

        }
        public static void StopBarcode()
        {
            if (instance != null)
            {
                if (instance.barcodeControl.BarcodeEnabled == true)
                    instance.barcodeControl.BarcodeEnabled = false;
            }
        }
        private void buttonOk_GotFocus(object sender, EventArgs e)
        {

        }

        private void buttonOk_LostFocus(object sender, EventArgs e)
        {

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }


        private void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {

                case Keys.Enter:
                    buttonOk_Click(sender, e);
                    e.Handled = true;
                    break;
            }



        }
        private void LifeTimer_Tick(object sender, EventArgs e)
        {
            m_lifeTimer.Enabled = false;
            this.DialogResult = DialogResult.Cancel;
        }

        public static string SupervisorPIN
        {
            get
            {
                if (instance != null)
                {
                    return instance.cmxTextBox1.Text;

                }
                return null;

            }

        }
    }
}