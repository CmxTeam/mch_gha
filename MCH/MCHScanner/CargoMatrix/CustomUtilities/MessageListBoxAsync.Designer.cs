﻿namespace CustomUtilities
{
    partial class MessageListBoxAsync<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            if (_asyncListThread != null)
                _asyncListThread.Abort();
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.filterTextBox = new CargoMatrix.UI.CMXTextBox();
            this.smoothListBoxBase1 = new SmoothListbox.SmoothListBoxBase();
            this.pictureBoxDown = new System.Windows.Forms.PictureBox();
            this.pictureBoxUp = new System.Windows.Forms.PictureBox();
            this.pictureBoxCancel = new System.Windows.Forms.PictureBox();
            this.pictureBoxOk = new System.Windows.Forms.PictureBox();
            this.pictureBoxMenuBack = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.panelHeader);
            this.panel1.Controls.Add(this.smoothListBoxBase1);
            this.panel1.Controls.Add(this.pictureBoxDown);
            this.panel1.Controls.Add(this.pictureBoxUp);
            this.panel1.Controls.Add(this.pictureBoxCancel);
            this.panel1.Controls.Add(this.pictureBoxOk);
            this.panel1.Controls.Add(this.pictureBoxMenuBack);
            this.panel1.Controls.Add(this.pictureBoxHeader);
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 266);
            // 
            // panelHeader
            // 
            this.panelHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHeader.BackColor = System.Drawing.Color.White;
            this.panelHeader.Controls.Add(this.splitter1);
            this.panelHeader.Controls.Add(this.filterTextBox);
            this.panelHeader.Location = new System.Drawing.Point(0, 19);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(234, 25);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 24);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(234, 1);
            // 
            // filterTextBox
            // 
            this.filterTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.filterTextBox.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.filterTextBox.Location = new System.Drawing.Point(5, 1);
            this.filterTextBox.Name = "filterTextBox";
            this.filterTextBox.Size = new System.Drawing.Size(224, 22);
            this.filterTextBox.TabIndex = 1;
            this.filterTextBox.WatermarkText = "Enter Text to Filter";
            this.filterTextBox.TextChanged += new System.EventHandler(this.filterTextBox_TextChanged);
            // 
            // smoothListBoxBase1
            // 
            this.smoothListBoxBase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.smoothListBoxBase1.BackColor = System.Drawing.Color.White;
            this.smoothListBoxBase1.Location = new System.Drawing.Point(1, 44);
            this.smoothListBoxBase1.MultiSelectEnabled = true;
            this.smoothListBoxBase1.Name = "smoothListBoxBase1";
            this.smoothListBoxBase1.Size = new System.Drawing.Size(232, 180);
            this.smoothListBoxBase1.TabIndex = 6;
            this.smoothListBoxBase1.UnselectEnabled = true;
            this.smoothListBoxBase1.ListItemClicked += new SmoothListbox.ListItemClickedHandler(this.smoothListBoxBase1_ListItemClicked);
            // 
            // buttonDown
            // 
            this.pictureBoxDown.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBoxDown.Location = new System.Drawing.Point(176, 226);
            this.pictureBoxDown.Name = "buttonDown";
            this.pictureBoxDown.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxDown_MouseDown);
            this.pictureBoxDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxDown_MouseUp);
            this.pictureBoxDown.EnabledChanged += new System.EventHandler(pictureBoxDown_EnabledChanged);
            // 
            // buttonUp
            // 
            this.pictureBoxUp.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBoxUp.Location = new System.Drawing.Point(120, 226);
            this.pictureBoxUp.Name = "buttonUp";
            this.pictureBoxUp.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxUp_MouseDown);
            this.pictureBoxUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxUp_MouseUp);
            this.pictureBoxUp.EnabledChanged += new System.EventHandler(pictureBoxUp_EnabledChanged);
            // 
            // buttonCancel
            // 
            this.pictureBoxCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBoxCancel.Location = new System.Drawing.Point(64, 226);
            this.pictureBoxCancel.Name = "buttonCancel";
            this.pictureBoxCancel.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxCancel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCancel.Click += new System.EventHandler(this.pictureBoxCancel_Click);
            // 
            // buttonOk
            // 
            this.pictureBoxOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBoxOk.Enabled = false;
            this.pictureBoxOk.Location = new System.Drawing.Point(8, 226);
            this.pictureBoxOk.Name = "buttonOk";
            this.pictureBoxOk.Size = new System.Drawing.Size(52, 37);
            this.pictureBoxOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxOk.Click += new System.EventHandler(this.pictureBoxOk_Click);
            // 
            // pictureBoxMenuBack
            // 
            this.pictureBoxMenuBack.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxMenuBack.Location = new System.Drawing.Point(0, 224);
            this.pictureBoxMenuBack.Name = "pictureBoxMenuBack";
            this.pictureBoxMenuBack.Size = new System.Drawing.Size(234, 42);
            this.pictureBoxMenuBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxHeader.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(234, 18);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
            // 
            // MessageListBoxAsync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "MessageListBoxAsync";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MesageListBox_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MessageListBox_Closing);
            this.panel1.ResumeLayout(false);
            this.panelHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.PictureBox pictureBoxMenuBack;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.PictureBox pictureBoxCancel;
        private System.Windows.Forms.PictureBox pictureBoxOk;
        protected CargoMatrix.UI.CMXTextBox filterTextBox;
        protected System.Windows.Forms.Panel panelHeader;
        public SmoothListbox.SmoothListBoxBase smoothListBoxBase1;
        private System.Windows.Forms.PictureBox pictureBoxDown;
        private System.Windows.Forms.PictureBox pictureBoxUp;
        private System.Windows.Forms.Splitter splitter1;
        protected System.Windows.Forms.Panel panel1;
    }
}
