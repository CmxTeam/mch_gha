﻿namespace SmoothListBox.UI
{
    partial class BillViewerShell
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxBookMarkNext = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeader = new System.Windows.Forms.PictureBox();
            this.panelShellHeader = new System.Windows.Forms.Panel();
            this.panelBookMarks = new System.Windows.Forms.Panel();
            this.panelBookMarksList = new System.Windows.Forms.Panel();
            this.smoothListBoxBookMarks = new SmoothListBox.UI.SmoothListBoxBase();
            this.panelOptionsBorder2 = new System.Windows.Forms.Panel();
            this.panelOptionsBorder3 = new System.Windows.Forms.Panel();
            this.panelOptionsBorder1 = new System.Windows.Forms.Panel();
            this.bookMarksTimer = new System.Windows.Forms.Timer();
            this.pictureBoxBookMarkPrevious = new System.Windows.Forms.PictureBox();
            this.panelShellHeader.SuspendLayout();
            this.panelBookMarks.SuspendLayout();
            this.panelBookMarksList.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxBookMarkNext
            // 
            this.pictureBoxBookMarkNext.BackColor = System.Drawing.Color.White;
            this.pictureBoxBookMarkNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBoxBookMarkNext.Location = new System.Drawing.Point(208, 0);
            this.pictureBoxBookMarkNext.Name = "pictureBoxBookMarkNext";
            this.pictureBoxBookMarkNext.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxBookMarkNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBookMarkNext.Click += new System.EventHandler(this.pictureBoxBookMarkNext_Click);
            this.pictureBoxBookMarkNext.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxBookMarkNext_MouseDown);
            this.pictureBoxBookMarkNext.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxBookMarkNext_MouseUp);
            // 
            // pictureBoxHeader
            // 
            this.pictureBoxHeader.BackColor = System.Drawing.Color.White;
            this.pictureBoxHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxHeader.Location = new System.Drawing.Point(32, 0);
            this.pictureBoxHeader.Name = "pictureBoxHeader";
            this.pictureBoxHeader.Size = new System.Drawing.Size(176, 32);
            this.pictureBoxHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHeader.Click += new System.EventHandler(this.pictureBoxHeader_Click);
            this.pictureBoxHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxHeader_Paint);
            // 
            // panelShellHeader
            // 
            this.panelShellHeader.Controls.Add(this.pictureBoxHeader);
            this.panelShellHeader.Controls.Add(this.pictureBoxBookMarkNext);
            this.panelShellHeader.Controls.Add(this.pictureBoxBookMarkPrevious);
            this.panelShellHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelShellHeader.Location = new System.Drawing.Point(0, 0);
            this.panelShellHeader.Name = "panelShellHeader";
            this.panelShellHeader.Size = new System.Drawing.Size(240, 32);
            // 
            // panelBookMarks
            // 
            this.panelBookMarks.BackColor = System.Drawing.Color.DarkRed;
            this.panelBookMarks.Controls.Add(this.panelBookMarksList);
            this.panelBookMarks.Controls.Add(this.panelOptionsBorder2);
            this.panelBookMarks.Controls.Add(this.panelOptionsBorder3);
            this.panelBookMarks.Controls.Add(this.panelOptionsBorder1);
            this.panelBookMarks.Location = new System.Drawing.Point(1, 53);
            this.panelBookMarks.Name = "panelBookMarks";
            this.panelBookMarks.Size = new System.Drawing.Size(238, 76);
            this.panelBookMarks.Visible = false;
            // 
            // panelBookMarksList
            // 
            this.panelBookMarksList.BackColor = System.Drawing.Color.Cyan;
            this.panelBookMarksList.Controls.Add(this.smoothListBoxBookMarks);
            this.panelBookMarksList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBookMarksList.Location = new System.Drawing.Point(1, 0);
            this.panelBookMarksList.Name = "panelBookMarksList";
            this.panelBookMarksList.Size = new System.Drawing.Size(236, 74);
            // 
            // smoothListBoxBookMarks
            // 
            this.smoothListBoxBookMarks.BackColor = System.Drawing.Color.Gray;
            this.smoothListBoxBookMarks.Location = new System.Drawing.Point(63, 25);
            this.smoothListBoxBookMarks.Name = "smoothListBoxBookMarks";
            this.smoothListBoxBookMarks.Size = new System.Drawing.Size(67, 20);
            this.smoothListBoxBookMarks.TabIndex = 4;
            this.smoothListBoxBookMarks.UnselectEnabled = false;
            this.smoothListBoxBookMarks.ListItemClicked += new SmoothListBox.UI.ListItemClickedHandler(this.smoothListBoxBookMarks_ListItemClicked);
            // 
            // panelOptionsBorder2
            // 
            this.panelOptionsBorder2.BackColor = System.Drawing.Color.Black;
            this.panelOptionsBorder2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelOptionsBorder2.Location = new System.Drawing.Point(0, 0);
            this.panelOptionsBorder2.Name = "panelOptionsBorder2";
            this.panelOptionsBorder2.Size = new System.Drawing.Size(1, 74);
            // 
            // panelOptionsBorder3
            // 
            this.panelOptionsBorder3.BackColor = System.Drawing.Color.Black;
            this.panelOptionsBorder3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelOptionsBorder3.Location = new System.Drawing.Point(237, 0);
            this.panelOptionsBorder3.Name = "panelOptionsBorder3";
            this.panelOptionsBorder3.Size = new System.Drawing.Size(1, 74);
            // 
            // panelOptionsBorder1
            // 
            this.panelOptionsBorder1.BackColor = System.Drawing.Color.Black;
            this.panelOptionsBorder1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelOptionsBorder1.Location = new System.Drawing.Point(0, 74);
            this.panelOptionsBorder1.Name = "panelOptionsBorder1";
            this.panelOptionsBorder1.Size = new System.Drawing.Size(238, 2);
            // 
            // bookMarksTimer
            // 
            this.bookMarksTimer.Interval = 75;
            this.bookMarksTimer.Tick += new System.EventHandler(this.bookMarksTimer_Tick);
            // 
            // pictureBoxBookMarkPrevious
            // 
            this.pictureBoxBookMarkPrevious.BackColor = System.Drawing.Color.White;
            this.pictureBoxBookMarkPrevious.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxBookMarkPrevious.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxBookMarkPrevious.Name = "pictureBoxBookMarkPrevious";
            this.pictureBoxBookMarkPrevious.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxBookMarkPrevious.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBookMarkPrevious.Click += new System.EventHandler(this.pictureBoxBookMarkPrevious_Click);
            this.pictureBoxBookMarkPrevious.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxBookMarkPrevious_MouseDown);
            this.pictureBoxBookMarkPrevious.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxBookMarkPrevious_MouseUp);
            // 
            // BillViewerShell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelBookMarks);
            this.Controls.Add(this.panelShellHeader);
            this.Name = "BillViewerShell";
            this.Size = new System.Drawing.Size(240, 150);
            this.panelShellHeader.ResumeLayout(false);
            this.panelBookMarks.ResumeLayout(false);
            this.panelBookMarksList.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxBookMarkNext;
        private System.Windows.Forms.PictureBox pictureBoxHeader;
        private System.Windows.Forms.Panel panelShellHeader;
        public System.Windows.Forms.Panel panelBookMarks;
        private System.Windows.Forms.Panel panelBookMarksList;
        protected SmoothListBoxBase smoothListBoxBookMarks;
        private System.Windows.Forms.Panel panelOptionsBorder2;
        private System.Windows.Forms.Panel panelOptionsBorder3;
        private System.Windows.Forms.Panel panelOptionsBorder1;
        private System.Windows.Forms.Timer bookMarksTimer;
        protected System.Windows.Forms.PictureBox pictureBoxBookMarkPrevious;
    }
}
