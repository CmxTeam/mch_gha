﻿namespace CargoMatrix.OnHand
{
    partial class TaskDetailsItem
    {
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelVendor = new System.Windows.Forms.Label();
            this.labelPickupNo = new System.Windows.Forms.Label();
            this.labelRefNo = new System.Windows.Forms.Label();
            this.labelDest = new System.Windows.Forms.Label();
            this.labelSplitter = new System.Windows.Forms.Label();
            this.buttonRecover = new OpenNETCF.Windows.Forms.Button2();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.buttonTruckCompany = new CargoMatrix.UI.CMXPictureButton();
            this.buttonTrucker = new CargoMatrix.UI.CMXPictureButton();
            this.labelTruckingComp = new System.Windows.Forms.Label();
            this.labelTruckerName = new System.Windows.Forms.Label();
            this.labelPieces = new System.Windows.Forms.Label();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.pictureBoxBottomStrip = new System.Windows.Forms.PictureBox();
            this.textBoxTruckingCompany = new CargoMatrix.UI.CMXTextBox();
            this.textBoxTrucker = new CargoMatrix.UI.CMXTextBox();
            this.textBoxPieces = new CargoMatrix.UI.CMXTextBox();
            this.labelPro = new System.Windows.Forms.Label();
            this.textBoxPro = new CargoMatrix.UI.CMXTextBox();
            this.SuspendLayout();
            // 
            // labelRefNo
            // 
            this.labelRefNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRefNo.BackColor = System.Drawing.Color.White;
            this.labelRefNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelRefNo.Location = new System.Drawing.Point(42, 4);
            this.labelRefNo.Name = "labelRefNo";
            this.labelRefNo.Size = new System.Drawing.Size(160, 13);
            // 
            // labelVendor
            // 
            this.labelVendor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVendor.BackColor = System.Drawing.Color.White;
            this.labelVendor.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelVendor.Location = new System.Drawing.Point(42, 18);
            this.labelVendor.Name = "labelWeight";
            this.labelVendor.Size = new System.Drawing.Size(160, 13);
            // 
            // labelPickupNo
            // 
            this.labelPickupNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPickupNo.BackColor = System.Drawing.Color.White;
            this.labelPickupNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPickupNo.Location = new System.Drawing.Point(42, 32);
            this.labelPickupNo.Name = "labelPieces";
            this.labelPickupNo.Size = new System.Drawing.Size(160, 13);
            // 
            // labelDest
            // 
            this.labelDest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDest.BackColor = System.Drawing.Color.White;
            this.labelDest.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDest.Location = new System.Drawing.Point(42, 46);
            this.labelDest.Name = "labelUlds";
            this.labelDest.Size = new System.Drawing.Size(160, 13);
            // 
            // labelSplitter
            // 
            this.labelSplitter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSplitter.BackColor = System.Drawing.Color.Black;
            this.labelSplitter.Location = new System.Drawing.Point(0, 61);
            this.labelSplitter.Name = "labelUlds";
            this.labelSplitter.Size = new System.Drawing.Size(240, 1);
            // 
            // itemPicture
            // 
            this.itemPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.itemPicture.BackColor = System.Drawing.Color.White;
            this.itemPicture.Location = new System.Drawing.Point(4, 4);//new System.Drawing.Point(202, 41);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(32, 32);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.Color.White;
            this.buttonDetails.Location = new System.Drawing.Point(202, 6);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(buttonDetails_Click);
            this.buttonDetails.Image = global::Resources.Graphics.Skin._3dots;
            this.buttonDetails.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.buttonDetails.TabStop = false;
            // 
            // buttonTruckCompany
            // 
            this.buttonTruckCompany.BackColor = System.Drawing.Color.White;
            this.buttonTruckCompany.Location = new System.Drawing.Point(202, 78);
            this.buttonTruckCompany.Name = "buttonDamage";
            this.buttonTruckCompany.Size = new System.Drawing.Size(32, 32);
            this.buttonTruckCompany.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonTruckCompany.TransparentColor = System.Drawing.Color.White;
            this.buttonTruckCompany.Click += new System.EventHandler(buttonTruckCompany_Click);
            this.buttonTruckCompany.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonTruckCompany.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonTruckCompany.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            this.buttonTruckCompany.Enabled = false;
            this.buttonTruckCompany.TabStop = false;
            // 
            // buttonTrucker
            // 
            this.buttonTrucker.BackColor = System.Drawing.Color.White;
            this.buttonTrucker.Location = new System.Drawing.Point(202, 120);
            this.buttonTrucker.Name = "buttonPrint";
            this.buttonTrucker.Size = new System.Drawing.Size(32, 32);
            this.buttonTrucker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonTrucker.TransparentColor = System.Drawing.Color.White;
            this.buttonTrucker.Click += new System.EventHandler(buttonTrucker_Click);
            this.buttonTrucker.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonTrucker.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonTrucker.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            this.buttonTrucker.TabStop = false;
            // 
            // labelTruckingComp
            // 
            this.labelTruckingComp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTruckingComp.BackColor = System.Drawing.Color.White;
            this.labelTruckingComp.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelTruckingComp.Location = new System.Drawing.Point(4, 64);
            this.labelTruckingComp.Name = "labelHawbs";
            this.labelTruckingComp.Text = "Trucking Company";
            this.labelTruckingComp.Size = new System.Drawing.Size(121, 13);
            //
            // textBoxTruckingCompany
            // 
            this.textBoxTruckingCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTruckingCompany.BackColor = System.Drawing.Color.White;
            this.textBoxTruckingCompany.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxTruckingCompany.Location = new System.Drawing.Point(4, 78);
            this.textBoxTruckingCompany.Name = "textBoxTruckingCompany";
            this.textBoxTruckingCompany.Size = new System.Drawing.Size(190, 13);
            this.textBoxTruckingCompany.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxTruckingCompany.TextChanged += new System.EventHandler(textBoxTrackingList_TextChanged);
            this.textBoxTruckingCompany.TabStop = true;
            this.textBoxTruckingCompany.TabIndex = 0;
            // 
            // labelTruckerName
            // 
            this.labelTruckerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTruckerName.BackColor = System.Drawing.Color.White;
            this.labelTruckerName.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelTruckerName.Location = new System.Drawing.Point(4, 105);
            this.labelTruckerName.Name = "labelFlightNo";
            this.labelTruckerName.Text = "Trucker's Name";
            this.labelTruckerName.Size = new System.Drawing.Size(121, 13);
            //
            // textBoxTrucker
            // 
            this.textBoxTrucker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTrucker.BackColor = System.Drawing.Color.White;
            this.textBoxTrucker.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxTrucker.Location = new System.Drawing.Point(4, 120);
            this.textBoxTrucker.Name = "textBoxTruckingCompany";
            this.textBoxTrucker.Size = new System.Drawing.Size(190, 13);
            this.textBoxTrucker.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxTrucker.TabStop = true;
            this.textBoxTrucker.TabIndex = 1;
            // 
            // labelPieces
            // 
            this.labelPieces.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPieces.BackColor = System.Drawing.Color.White;
            this.labelPieces.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelPieces.Location = new System.Drawing.Point(134, 145);
            this.labelPieces.Name = "labelLastScan";
            this.labelPieces.Text = "Pieces";
            this.labelPieces.Size = new System.Drawing.Size(60, 13);
            //
            // textBoxPieces
            // 
            this.textBoxPieces.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPieces.BackColor = System.Drawing.Color.White;
            this.textBoxPieces.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxPieces.Location = new System.Drawing.Point(134, 161);
            this.textBoxPieces.Name = "textBoxTruckingCompany";
            this.textBoxPieces.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.textBoxPieces.Size = new System.Drawing.Size(60, 13);
            this.textBoxPieces.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxPieces.TabStop = true;
            this.textBoxPieces.TabIndex = 2;
            // 
            // labelPro
            // 
            this.labelPro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPro.BackColor = System.Drawing.Color.White;
            this.labelPro.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelPro.Location = new System.Drawing.Point(4, 145);
            this.labelPro.Name = "labelPro";
            this.labelPro.Text = "PRO (Optional)";
            this.labelPro.Size = new System.Drawing.Size(121, 13);
            //
            // textBoxPro
            // 
            this.textBoxPro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPro.BackColor = System.Drawing.Color.White;
            this.textBoxPro.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.textBoxPro.Location = new System.Drawing.Point(4, 161);
            this.textBoxPro.Name = "textBoxPro";
            this.textBoxPro.Size = new System.Drawing.Size(124, 15);
            this.textBoxPro.TabStop = true;
            this.textBoxPro.TabIndex = 3;
            // 
            // panelIndicators
            // 
            this.panelIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIndicators.Location = new System.Drawing.Point(3, 44);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            this.panelIndicators.TabIndex = 19;
            //
            //buttonRecover
            //
            this.buttonRecover.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRecover.BackColor = System.Drawing.SystemColors.Control;
            this.buttonRecover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buttonRecover.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.buttonRecover.ForeColor = System.Drawing.Color.White;
            this.buttonRecover.Location = new System.Drawing.Point(80, 196);
            this.buttonRecover.Name = "ProceedButton";
            this.buttonRecover.Size = new System.Drawing.Size(80, 32);
            this.buttonRecover.TabIndex = 17;
            this.buttonRecover.Text = "RECOVER";
            this.buttonRecover.BackgroundImage = Resources.Skin.btn;
            this.buttonRecover.ActiveBackgroundImage = Resources.Skin.btn_over;
            this.buttonRecover.DisabledBackgroundImage = Resources.Skin.btn_dis;
            this.buttonRecover.Enabled = false;
            this.buttonRecover.TabStop = false;
            this.buttonRecover.Click += new System.EventHandler(buttonRecover_Click);
            // 
            // pictureBoxBottomStrip
            // 
            this.pictureBoxBottomStrip.BackColor = System.Drawing.Color.White;
            this.pictureBoxBottomStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBoxBottomStrip.Location = new System.Drawing.Point(0, 175);
            this.pictureBoxBottomStrip.Name = "pictureBox2";
            this.pictureBoxBottomStrip.Size = new System.Drawing.Size(240, 40);
            this.pictureBoxBottomStrip.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBottomStrip.Image = global::Resources.Graphics.Skin.nav_bg;
            // 
            // MawbDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelVendor);
            this.Controls.Add(this.labelPickupNo);
            this.Controls.Add(this.labelPieces);
            this.Controls.Add(this.labelTruckerName);
            this.Controls.Add(this.labelTruckingComp);
            //this.Controls.Add(this.labelDest);
            this.Controls.Add(this.labelSplitter);
            this.Controls.Add(this.labelRefNo);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonTruckCompany);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.panelIndicators);
            this.Controls.Add(this.buttonRecover);
            this.Controls.Add(this.buttonTrucker);
            this.Controls.Add(this.pictureBoxBottomStrip);
            this.Controls.Add(this.textBoxTruckingCompany);
            this.Controls.Add(this.textBoxTrucker);
            this.Controls.Add(this.textBoxPieces);
            this.Controls.Add(this.labelPro);
            this.Controls.Add(this.textBoxPro);
            this.buttonRecover.BringToFront();
            this.Name = "DetailsItem";
            this.Location = new System.Drawing.Point(0, 0);
            this.Size = new System.Drawing.Size(240, 234);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelRefNo;
        private System.Windows.Forms.Label labelVendor;
        private System.Windows.Forms.Label labelPickupNo;
        private System.Windows.Forms.Label labelDest;
        private System.Windows.Forms.Label labelTruckingComp;
        private System.Windows.Forms.Label labelTruckerName;
        private System.Windows.Forms.Label labelPieces;
        private System.Windows.Forms.Label labelSplitter;
        private System.Windows.Forms.Label labelPro;
        private CustomUtilities.IndicatorPanel panelIndicators;
        private CargoMatrix.UI.CMXPictureButton buttonDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private CargoMatrix.UI.CMXPictureButton buttonTruckCompany;
        private CargoMatrix.UI.CMXPictureButton buttonTrucker;
        private OpenNETCF.Windows.Forms.Button2 buttonRecover;
        private System.Windows.Forms.PictureBox pictureBoxBottomStrip;
        private CargoMatrix.UI.CMXTextBox textBoxTruckingCompany;
        private CargoMatrix.UI.CMXTextBox textBoxTrucker;
        private CargoMatrix.UI.CMXTextBox textBoxPro;
        private CargoMatrix.UI.CMXTextBox textBoxPieces;


    }
}
