﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SmoothListbox.ListItems;

namespace CargoMatrix.OnHand
{
    public class CheckBoxItem<T> : CheckBox, IDataHolder<T>
    {
        private DataHolder<T> dataHolder;

        public CheckBoxItem(T item,string text)
        {
            dataHolder = new DataHolder<T>(item);
            this.title.Text = text;
        }        
        
        #region IDataHolder<T> Members

        public T ItemData
        {
            get { return this.dataHolder.ItemData; }


            set
            {
                if (OnBeforeDataItemChanged(value))
                {
                    this.dataHolder.ItemData = value;

                    OnDataItemChanged();
                }
            }
        }


        public virtual bool OnBeforeDataItemChanged(T item)
        {
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual void OnDataItemChanged()
        { }
        #endregion
    }
}
