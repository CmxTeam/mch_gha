﻿namespace CargoMatrix.OnHand
{
    partial class OnhandDetails
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.title = new System.Windows.Forms.Label();
            this.labelWeight = new System.Windows.Forms.Label();
            this.labelPiece = new System.Windows.Forms.Label();
            this.labelDims = new System.Windows.Forms.Label();
            this.labelPkg = new System.Windows.Forms.Label();
            this.labelVendor = new System.Windows.Forms.Label();
            this.labelDropAt = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.buttonList = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDamage = new CargoMatrix.UI.CMXPictureButton();
            this.buttonPrint = new CargoMatrix.UI.CMXPictureButton();
            this.SuspendLayout();
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(5, 3);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(28, 28);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.BackColor = System.Drawing.Color.Transparent;
            this.title.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(52, 10);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(136, 16);
            this.title.Text = "Dyncorp";
            // 
            // labelWeight
            // 
            this.labelWeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelWeight.Location = new System.Drawing.Point(74, 69);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(90, 13);
            this.labelWeight.Text = "25 LB";
            // 
            // labelPiece
            // 
            this.labelPiece.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPiece.Location = new System.Drawing.Point(74, 54);
            this.labelPiece.Name = "labelPiece";
            this.labelPiece.Size = new System.Drawing.Size(90, 13);
            this.labelPiece.Text = "1 of 3";
            // 
            // labelDims
            // 
            this.labelDims.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDims.Location = new System.Drawing.Point(74, 84);
            this.labelDims.Name = "labelDims";
            this.labelDims.Size = new System.Drawing.Size(90, 13);
            this.labelDims.Text = "12 x 12 x 12";
            // 
            // labelPkg
            // 
            this.labelPkg.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPkg.Location = new System.Drawing.Point(74, 99);
            this.labelPkg.Name = "labelPkg";
            this.labelPkg.Size = new System.Drawing.Size(90, 13);
            this.labelPkg.Text = "Box";
            // 
            // labelVendor
            // 
            this.labelVendor.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelVendor.Location = new System.Drawing.Point(74, 39);
            this.labelVendor.Name = "labelVendor";
            this.labelVendor.Size = new System.Drawing.Size(90, 13);
            this.labelVendor.Text = "US 21 Inc";
            // 
            // labelDropAt
            // 
            this.labelDropAt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDropAt.Location = new System.Drawing.Point(74, 114);
            this.labelDropAt.Name = "labelDropAt";
            this.labelDropAt.Size = new System.Drawing.Size(90, 13);
            this.labelDropAt.Text = "Box";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(3, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.Text = "Vendor :";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(3, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.Text = "Piece :";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(3, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.Text = "Weight :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label4.Location = new System.Drawing.Point(3, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.Text = "Dim :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(3, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.Text = "Pkg :";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label6.Location = new System.Drawing.Point(3, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.Text = "Drop At :";
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDetails.Location = new System.Drawing.Point(202, 40);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(buttonDetails_Click);
            // 
            // buttonList
            // 
            this.buttonList.BackColor = System.Drawing.SystemColors.Control;
            this.buttonList.Location = new System.Drawing.Point(202, 3);
            this.buttonList.Name = "buttonList";
            this.buttonList.Size = new System.Drawing.Size(32, 32);
            this.buttonList.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonList.TransparentColor = System.Drawing.Color.White;
            this.buttonList.Click += new System.EventHandler(buttonList_Click);
            // 
            // buttonDamage
            // 
            this.buttonDamage.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDamage.Location = new System.Drawing.Point(202, 77);
            this.buttonDamage.Name = "buttonDamage";
            this.buttonDamage.Size = new System.Drawing.Size(32, 32);
            this.buttonDamage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDamage.TransparentColor = System.Drawing.Color.White;
            this.buttonDamage.Click += new System.EventHandler(buttonDamage_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPrint.Location = new System.Drawing.Point(202, 114);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(32, 32);
            this.buttonPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonPrint.TransparentColor = System.Drawing.Color.White;
            this.buttonPrint.Click += new System.EventHandler(buttonPrint_Click);
            // 
            // OnhandDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.buttonDamage);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.buttonList);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.labelDims);
            this.Controls.Add(this.labelPiece);
            this.Controls.Add(this.labelDropAt);
            this.Controls.Add(this.labelPkg);
            this.Controls.Add(this.labelVendor);
            this.Controls.Add(this.title);
            this.Name = "OnhandDetails";
            InitializeImages();
            this.Size = new System.Drawing.Size(240, 160);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label labelVendor;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelPiece;
        private System.Windows.Forms.Label labelDims;
        private System.Windows.Forms.Label labelPkg;
        private System.Windows.Forms.Label labelDropAt;
        //private CustomUtilities.IndicatorPanel panelIndicators;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        internal CargoMatrix.UI.CMXPictureButton buttonDetails;
        internal CargoMatrix.UI.CMXPictureButton buttonList;
        internal CargoMatrix.UI.CMXPictureButton buttonDamage;
        internal CargoMatrix.UI.CMXPictureButton buttonPrint;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}
