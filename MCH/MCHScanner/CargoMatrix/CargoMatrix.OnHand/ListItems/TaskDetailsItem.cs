﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.OnHand
{
    public partial class TaskDetailsItem : UserControl
    {
        public event EventHandler ButtonDetailsClick;
        public event EventHandler ButtonVendorClick;
        public event EventHandler ButtonTruckerClick;
        public event EventHandler ButtonRecoverClick;

        public string TruckingCompany
        {
            get { return textBoxTruckingCompany.Text; }
            set { textBoxTruckingCompany.Text = value; }
        }
        public string TruckerName
        {
            get { return textBoxTrucker.Text; }
            set { textBoxTrucker.Text = value; }
        }

        public int Pieces
        {
            get { return int.Parse(textBoxPieces.Text); }
            set { textBoxPieces.Text = value.ToString(); }
        }
        public string PRO
        {
            get { return textBoxPro.Text; }
            set { textBoxPro.Text = value; }
        }
        public TaskDetailsItem()
        {
            InitializeComponent();
            InitialzeDetails();
        }

        public new void Focus()
        {
            textBoxTruckingCompany.Focus();
        }

        private void InitialzeDetails()
        {
            //this.labelDest.Text = SharedMethods.CurrentPickup.Destination;
            this.labelPickupNo.Text = SharedMethods.CurrentPickup.PONumber;
            this.labelVendor.Text = SharedMethods.CurrentPickup.Vendor;
            this.labelRefNo.Text = SharedMethods.CurrentPickup.ShipmentNO;
            this.panelIndicators.Flags = SharedMethods.CurrentPickup.Flags;
            this.panelIndicators.PopupHeader = SharedMethods.CurrentPickup.ShipmentNO;
            this.textBoxTruckingCompany.Text = SharedMethods.CurrentPickup.TruckingCompany;
            this.textBoxTrackingList_TextChanged(null, EventArgs.Empty);
            this.itemPicture.Image = SharedMethods.GetStatusIcon(SharedMethods.CurrentPickup.Status);
        }

        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonDetailsClick != null)
                ButtonDetailsClick(sender, e);
        }

        void buttonTruckCompany_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonVendorClick != null)
                ButtonVendorClick(sender, e);
        }

        void buttonTrucker_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonTruckerClick != null)
                this.ButtonTruckerClick(sender, e);
        }

        void buttonRecover_Click(object sender, System.EventArgs e)
        {
            if (this.ButtonRecoverClick != null)
                ButtonRecoverClick(sender, e);
        }

        void textBox_TextChanged(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxPieces.Text) &&
                !string.IsNullOrEmpty(textBoxTrucker.Text) &&
                !string.IsNullOrEmpty(textBoxTruckingCompany.Text))
                buttonRecover.Enabled = true;
        }

        void textBoxTrackingList_TextChanged(object sender, System.EventArgs e)
        {
            buttonTruckCompany.Enabled = buttonTrucker.Enabled = textBoxTruckingCompany.Text.Length > 2;
        }
    }
}
