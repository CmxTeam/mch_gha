﻿using System;
namespace CargoMatrix.OnHand
{
    partial class PODetailsItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRemQty = new System.Windows.Forms.Label();
            this.labelItemNo = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonList = new CargoMatrix.UI.CMXPictureButton();
            this.buttonPrint = new CargoMatrix.UI.CMXPictureButton();
            this.labelPartNo = new System.Windows.Forms.Label();
            this.labelPartDescr = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelOrderQty = new System.Windows.Forms.Label();
            this.textBoxShipQty = new CargoMatrix.UI.CMXTextBox();
            this.buttonApply = new OpenNETCF.Windows.Forms.Button2();
            this.pictureBoxBottomPane = new System.Windows.Forms.PictureBox();
            this.buttonDone = new OpenNETCF.Windows.Forms.Button2();
            this.SuspendLayout();
            // 
            // labelRemQty
            // 
            this.labelRemQty.BackColor = System.Drawing.Color.White;
            this.labelRemQty.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelRemQty.Location = new System.Drawing.Point(85, 80);
            this.labelRemQty.Name = "labelRemQty";
            this.labelRemQty.Size = new System.Drawing.Size(111, 13);
            // 
            // labelItemNo
            // 
            this.labelItemNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelItemNo.BackColor = System.Drawing.Color.White;
            this.labelItemNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelItemNo.Location = new System.Drawing.Point(85, 8);
            this.labelItemNo.Name = "labelItemNo";
            this.labelItemNo.Size = new System.Drawing.Size(111, 14);
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(7, 5);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(24, 24);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonList
            // 
            this.buttonList.BackColor = System.Drawing.SystemColors.Control;
            this.buttonList.Location = new System.Drawing.Point(202, 8);
            this.buttonList.Name = "buttonList";
            this.buttonList.Size = new System.Drawing.Size(32, 32);
            this.buttonList.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonList.TransparentColor = System.Drawing.Color.White;
            this.buttonList.Click += new System.EventHandler(buttonList_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPrint.Location = new System.Drawing.Point(202, 48);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(32, 32);
            this.buttonPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonPrint.TransparentColor = System.Drawing.Color.White;
            this.buttonPrint.Click += new System.EventHandler(buttonPrint_Click);
            // 
            // labelPartNo
            // 
            this.labelPartNo.BackColor = System.Drawing.Color.White;
            this.labelPartNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPartNo.Location = new System.Drawing.Point(85, 32);
            this.labelPartNo.Name = "labelPartNo";
            this.labelPartNo.Size = new System.Drawing.Size(111, 13);
            // 
            // labelPartDescr
            // 
            this.labelPartDescr.BackColor = System.Drawing.Color.White;
            this.labelPartDescr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPartDescr.Location = new System.Drawing.Point(85, 48);
            this.labelPartDescr.Name = "labelPartDescr";
            this.labelPartDescr.Size = new System.Drawing.Size(111, 13);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(7, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.Text = "Rem Qty:";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(7, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.Text = "SHIP QTY :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(7, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.Text = "Part# :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(7, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.Text = "Part Desc :";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(7, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.Text = "PO Qty :";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(7, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.Text = "Reference :";
            // 
            // labelOrderQty
            // 
            this.labelOrderQty.BackColor = System.Drawing.Color.White;
            this.labelOrderQty.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelOrderQty.Location = new System.Drawing.Point(85, 64);
            this.labelOrderQty.Name = "labelOrderQty";
            this.labelOrderQty.Size = new System.Drawing.Size(111, 13);
            this.textBoxShipQty.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.textBoxShipQty.TextChanged += new EventHandler(textBoxShipQty_TextChanged);
            // 
            // textBoxShipQty
            // 
            this.textBoxShipQty.Location = new System.Drawing.Point(85, 105);
            this.textBoxShipQty.Name = "textBoxShipQty";
            this.textBoxShipQty.Size = new System.Drawing.Size(65, 21);
            this.textBoxShipQty.TabIndex = 1;
            this.textBoxShipQty.TabStop = true;
            this.textBoxShipQty.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Decimal;
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(164, 103);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(58, 32);
            this.buttonApply.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buttonApply.Click += new System.EventHandler(buttonApply_Click);
            this.buttonApply.Text = "APPLY";
            this.buttonApply.BackgroundImage = Resources.Skin.btn;
            this.buttonApply.ActiveBackgroundImage = Resources.Skin.btn_over;
            this.buttonApply.DisabledBackgroundImage = Resources.Skin.btn_dis;
            this.buttonApply.ForeColor = System.Drawing.Color.Black;
            this.buttonApply.Enabled = false;
            // 
            // pictureBoxBottomPane
            // 
            this.pictureBoxBottomPane.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBoxBottomPane.Location = new System.Drawing.Point(0, 142);
            this.pictureBoxBottomPane.Name = "pictureBox1";
            this.pictureBoxBottomPane.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBottomPane.Size = new System.Drawing.Size(240, 38);
            // 
            // buttonDone
            // 
            this.buttonDone.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDone.Location = new System.Drawing.Point(81, 150);
            this.buttonDone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buttonDone.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.buttonDone.ForeColor = System.Drawing.Color.Black;
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(79, 32);
            this.buttonDone.Text = "DONE";
            this.buttonDone.BackgroundImage = Resources.Skin.btn;
            this.buttonDone.ActiveBackgroundImage = Resources.Skin.btn_over;
            this.buttonDone.Click += new System.EventHandler(buttonDone_Click);
            // 
            // OnHandDetailedItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.textBoxShipQty);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelRemQty);
            this.Controls.Add(this.labelOrderQty);
            this.Controls.Add(this.labelPartDescr);
            this.Controls.Add(this.labelPartNo);
            this.Controls.Add(this.labelItemNo);
            //this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonDone);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.buttonList);
            this.Controls.Add(this.pictureBoxBottomPane);
            this.Name = "OnHandDetailedItem";
            this.InitializeComponentHelper();
            this.Size = new System.Drawing.Size(240, 184);
            this.ResumeLayout(false);
        }


        #endregion

        private System.Windows.Forms.Label labelItemNo;
        private System.Windows.Forms.Label labelRemQty;
        private System.Windows.Forms.Label labelPartNo;
        private System.Windows.Forms.Label labelPartDescr;
        private CargoMatrix.UI.CMXPictureButton buttonList;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private CargoMatrix.UI.CMXPictureButton buttonPrint;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelOrderQty;
        private CargoMatrix.UI.CMXTextBox textBoxShipQty;
        private OpenNETCF.Windows.Forms.Button2 buttonApply;
        private System.Windows.Forms.PictureBox pictureBoxBottomPane;
        private OpenNETCF.Windows.Forms.Button2 buttonDone;
    }
}