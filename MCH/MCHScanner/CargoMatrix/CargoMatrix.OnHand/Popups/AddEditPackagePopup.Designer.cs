﻿namespace CargoMatrix.OnHand
{
    partial class AddEditPackagePopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCondition = new CargoMatrix.UI.CMXPictureButton();
            this.buttonPackageType = new CargoMatrix.UI.CMXPictureButton();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPackageType = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelCondition = new System.Windows.Forms.Label();
            this.textBoxLength = new CargoMatrix.UI.CMXTextBox();
            this.textBoxWidth = new CargoMatrix.UI.CMXTextBox();
            this.textBoxHeight = new CargoMatrix.UI.CMXTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxWeight = new CargoMatrix.UI.CMXTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxSlac = new CargoMatrix.UI.CMXTextBox();
            this.buttonAcquire = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDevice = new CargoMatrix.UI.CMXPictureButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxQuantity = new CargoMatrix.UI.CMXTextBox();
            this.SuspendLayout();
            // 
            // buttonCondition
            // 
            this.buttonCondition.Location = new System.Drawing.Point(198, 24);
            this.buttonCondition.Name = "buttonCondition";
            this.buttonCondition.Size = new System.Drawing.Size(32, 32);
            this.buttonCondition.Image = CargoMatrix.Resources.Skin.damage;
            this.buttonCondition.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.buttonCondition.Click += new System.EventHandler(buttonCondition_Click);
            // 
            // buttonPackageType
            // 
            this.buttonPackageType.Location = new System.Drawing.Point(160, 24);
            this.buttonPackageType.Name = "buttonPackageType";
            this.buttonPackageType.Size = new System.Drawing.Size(32, 32);
            this.buttonPackageType.Image = CargoMatrix.Resources.Skin.shipping_box_help;
            this.buttonPackageType.PressedImage = CargoMatrix.Resources.Skin.shipping_box_help_over;
            this.buttonPackageType.Click += new System.EventHandler(buttonPackageType_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 15);
            this.label1.Text = "Packaging: ";
            // 
            // labelPackageType
            // 
            this.labelPackageType.BackColor = System.Drawing.Color.White;
            this.labelPackageType.Location = new System.Drawing.Point(74, 24);
            this.labelPackageType.Name = "labelPackageType";
            this.labelPackageType.Size = new System.Drawing.Size(81, 15);
            this.labelPackageType.TextChanged += new System.EventHandler(InputChanged_Validate);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 15);
            this.label2.Text = "Condition: ";
            // 
            // labelCondition
            // 
            this.labelCondition.BackColor = System.Drawing.Color.White;
            this.labelCondition.Location = new System.Drawing.Point(74, 41);
            this.labelCondition.Name = "labelCondition";
            this.labelCondition.Size = new System.Drawing.Size(81, 15);
            this.labelCondition.TextChanged += InputChanged_Validate;
            // 
            // textBoxLength
            // 
            this.textBoxLength.Location = new System.Drawing.Point(4, 84);
            this.textBoxLength.Name = "textBoxLength";
            this.textBoxLength.Size = new System.Drawing.Size(53, 21);
            this.textBoxLength.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Decimal;
            this.textBoxLength.TabIndex = 1;
            this.textBoxLength.TextChanged += InputChanged_Validate;
            // 
            // textBoxWidth
            // 
            this.textBoxWidth.Location = new System.Drawing.Point(83, 84);
            this.textBoxWidth.Name = "textBoxWidth";
            this.textBoxWidth.Size = new System.Drawing.Size(53, 21);
            this.textBoxWidth.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Decimal;
            this.textBoxWidth.TabIndex = 2;
            this.textBoxWidth.TextChanged += InputChanged_Validate;
            // 
            // textBoxHight
            // 
            this.textBoxHeight.Location = new System.Drawing.Point(162, 84);
            this.textBoxHeight.Name = "textBoxHight";
            this.textBoxHeight.Size = new System.Drawing.Size(53, 21);
            this.textBoxHeight.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Decimal;
            this.textBoxHeight.TabIndex = 3;
            this.textBoxHeight.TextChanged += InputChanged_Validate;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(22, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 15);
            this.label3.Text = "L";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(102, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 15);
            this.label4.Text = "W";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(181, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 15);
            this.label5.Text = "H";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(57, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 15);
            this.label6.Text = "in";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(136, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 15);
            this.label7.Text = "in";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(215, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 15);
            this.label8.Text = "in";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(5, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 15);
            this.label9.Text = "Weight";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(57, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 15);
            this.label10.Text = "lbs";
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Location = new System.Drawing.Point(4, 130);
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.Size = new System.Drawing.Size(53, 21);
            this.textBoxWeight.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Decimal;
            this.textBoxWeight.TabIndex = 4;
            this.textBoxWeight.TextChanged += InputChanged_Validate;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.White;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(83, 115);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 15);
            this.label11.Text = "Slac";
            // 
            // textBoxSlac
            // 
            this.textBoxSlac.Location = new System.Drawing.Point(83, 130);
            this.textBoxSlac.Name = "textBoxSlac";
            this.textBoxSlac.Size = new System.Drawing.Size(63, 21);
            this.textBoxSlac.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.textBoxSlac.TabIndex = 5;
            this.textBoxSlac.TextChanged += InputChanged_Validate;
            // 
            // buttonAcquire
            // 
            this.buttonAcquire.Enabled = false;
            this.buttonAcquire.Location = new System.Drawing.Point(198, 165);
            this.buttonAcquire.Name = "buttonAcquire";
            this.buttonAcquire.Size = new System.Drawing.Size(32, 32);
            this.buttonAcquire.Image = CargoMatrix.Resources.Skin.acquire_dis;
            // 
            // buttonDevice
            // 
            this.buttonDevice.Enabled = false;
            this.buttonDevice.Location = new System.Drawing.Point(160, 165);
            this.buttonDevice.Name = "buttonDevice";
            this.buttonDevice.Size = new System.Drawing.Size(32, 32);
            this.buttonDevice.Image = CargoMatrix.Resources.Skin.button_scale_dis;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.label12.Location = new System.Drawing.Point(3, 165);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 15);
            this.label12.Text = "Device: ";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.White;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.label13.Location = new System.Drawing.Point(3, 182);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 15);
            this.label13.Text = "Status";
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(81, 165);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 15);
            this.label14.Text = "N/A";
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(81, 182);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 15);
            this.label15.Text = "N/A";
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.White;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(181, 112);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 15);
            this.label16.Text = "Qty.";
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.Location = new System.Drawing.Point(162, 130);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(63, 21);
            this.textBoxQuantity.InputMode = CargoMatrix.UI.CMXTextBoxInputMode.Numeric;
            this.textBoxQuantity.TabIndex = 6;
            this.textBoxQuantity.Text = "1";
            this.textBoxQuantity.TextChanged += InputChanged_Validate;
            // 
            // AddEditPackageDesign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Gray;
            this.panelContent.Size = new System.Drawing.Size(236, 244);
            this.panelContent.Location = new System.Drawing.Point(3, 20);
            this.panelContent.Controls.Add(this.textBoxHeight);
            this.panelContent.Controls.Add(this.textBoxWidth);
            this.panelContent.Controls.Add(this.textBoxSlac);
            this.panelContent.Controls.Add(this.textBoxWeight);
            this.panelContent.Controls.Add(this.textBoxLength);
            this.panelContent.Controls.Add(this.labelCondition);
            this.panelContent.Controls.Add(this.label14);
            this.panelContent.Controls.Add(this.labelPackageType);
            this.panelContent.Controls.Add(this.label1);
            this.panelContent.Controls.Add(this.label5);
            this.panelContent.Controls.Add(this.label4);
            this.panelContent.Controls.Add(this.label8);
            this.panelContent.Controls.Add(this.label10);
            this.panelContent.Controls.Add(this.label11);
            this.panelContent.Controls.Add(this.label7);
            this.panelContent.Controls.Add(this.label9);
            this.panelContent.Controls.Add(this.label6);
            this.panelContent.Controls.Add(this.label3);
            this.panelContent.Controls.Add(this.label13);
            this.panelContent.Controls.Add(this.label12);
            this.panelContent.Controls.Add(this.label2);
            this.panelContent.Controls.Add(this.label15);
            this.panelContent.Controls.Add(this.label16);
            this.panelContent.Controls.Add(this.textBoxQuantity);
            this.panelContent.Controls.Add(this.buttonDevice);
            this.panelContent.Controls.Add(this.buttonAcquire);
            this.panelContent.Controls.Add(this.buttonPackageType);
            this.panelContent.Controls.Add(this.buttonCondition);
            this.Name = "AddEditPackageDesign";
            this.Size = new System.Drawing.Size(240, 244);
            this.ResumeLayout(false);

        }


        #endregion

        private CargoMatrix.UI.CMXPictureButton buttonCondition;
        private CargoMatrix.UI.CMXPictureButton buttonPackageType;
        private CargoMatrix.UI.CMXPictureButton buttonAcquire;
        private CargoMatrix.UI.CMXPictureButton buttonDevice;
        private CargoMatrix.UI.CMXTextBox textBoxLength;
        private CargoMatrix.UI.CMXTextBox textBoxWidth;
        private CargoMatrix.UI.CMXTextBox textBoxHeight;
        private System.Windows.Forms.Label labelPackageType;
        private System.Windows.Forms.Label labelCondition;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private CargoMatrix.UI.CMXTextBox textBoxWeight;
        private CargoMatrix.UI.CMXTextBox textBoxSlac;
        private CargoMatrix.UI.CMXTextBox textBoxQuantity;
    }
}
