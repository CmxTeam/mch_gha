﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CustomListItems;
using CargoMatrix.Communication.PieceScanWCF;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.OnHand
{
    partial class ReferenceEditor : CargoMatrix.Utilities.MessageListBox
    {
        public ReferenceEditor()
        {
            InitializeComponent();
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(ReferenceEditor_LoadListEvent);
            this.HeaderText = "References - " + SharedMethods.CurrentPickup.ShipmentNO;
            this.ButtonVisible = true;
            this.HeaderText2 = "Add/Edit Reference";
            this.WideWidth();
            this.ButtonClick += new EventHandler(ButtonAdd_Click);
            this.OkEnabled = true;
        }

        void ReferenceEditor_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            LoadControl();
        }

        public void LoadControl()
        {
            this.smoothListBoxBase1.RemoveAll();
            foreach (var rfrnc in Communication.OnHand.OnHand.Instance.GetShipmentReferences(SharedMethods.CurrentPickup.ShipmentID))
            {
                var item = new ReferenceListItem<CMXReference>(rfrnc, global::Resources.Graphics.Skin.radio_button, rfrnc.ReferenceValue) { Line2 = rfrnc.ReferenceType };
                item.ButtonEditClick += new EventHandler(item_ButtonEditClick);
                item.ButtonDeleteClick += new EventHandler(item_ButtonDeleteClick);
                this.smoothListBoxBase1.AddItem2(item);
            }
            smoothListBoxBase1.LayoutItems();
            smoothListBoxBase1.RefreshScroll();
        }

        void item_ButtonDeleteClick(object sender, EventArgs e)
        {
            var reference = (sender as ReferenceListItem<CMXReference>).ItemData;
            var status = Communication.OnHand.OnHand.Instance.RemoveShipmentReference(reference);
            if (status.TransactionStatus1 == false && string.IsNullOrEmpty(status.TransactionError))
                SharedMethods.ShowFailMessage(status.TransactionError);
            LoadControl();
        }

        void item_ButtonEditClick(object sender, EventArgs e)
        {
            var reference = (sender as ReferenceListItem<CMXReference>).ItemData;
            var editRefPopup = new AddEditReference(reference);
            if (DialogResult.OK == editRefPopup.ShowDialog())
            {
                var status = Communication.OnHand.OnHand.Instance.EditShipmentReference(editRefPopup.Reference as CMXReference);
                if (status.TransactionStatus1 == false && string.IsNullOrEmpty(status.TransactionError))
                    SharedMethods.ShowFailMessage(status.TransactionError);
                LoadControl();
            }
        }

        void ButtonAdd_Click(object sender, System.EventArgs e)
        {
            var editRefPopup = new AddEditReference(null);
            if (DialogResult.OK == editRefPopup.ShowDialog())
            {
                var status = Communication.OnHand.OnHand.Instance.AddShipmentReference(SharedMethods.CurrentPickup.ShipmentID, editRefPopup.Reference as CMXReference);
                if (status.TransactionStatus1 == false && string.IsNullOrEmpty(status.TransactionError))
                    SharedMethods.ShowFailMessage(status.TransactionError);
                else
                    LoadControl();
            }
        }
    }
}