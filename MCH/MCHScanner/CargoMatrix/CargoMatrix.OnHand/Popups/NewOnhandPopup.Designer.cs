﻿using CargoMatrix.UI;
namespace CargoMatrix.OnHand
{
    partial class NewOnhandPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.panelContent.SuspendLayout();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            // 
            // labelPickupRef
            // 
            this.labelPickupRef = new System.Windows.Forms.Label();
            this.labelPickupRef.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelPickupRef.Location = new System.Drawing.Point(4, 26);
            this.labelPickupRef.Name = "labelPickupRef";
            this.labelPickupRef.Size = new System.Drawing.Size(226, 13);
            this.labelPickupRef.Text = "Pickup Ref#";
            // 
            // textBoxPickupRef
            // 
            this.textBoxPickupRef = new CMXTextBox();
            this.textBoxPickupRef.Location = new System.Drawing.Point(4, 44);
            this.textBoxPickupRef.Name = "textBoxPickupRef";
            this.textBoxPickupRef.Size = new System.Drawing.Size(226, 21);
            this.textBoxPickupRef.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxPickupRef.TabIndex = 1;
            // 
            // labelVendor
            // 
            this.labelVendor = new System.Windows.Forms.Label();
            this.labelVendor.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelVendor.Location = new System.Drawing.Point(4, 72);
            this.labelVendor.Name = "labelVendor";
            this.labelVendor.Size = new System.Drawing.Size(186, 13);
            this.labelVendor.Text = "Vendor";
            // 
            // textBoxVendor
            // 
            this.textBoxVendor = new CMXTextBox();
            this.textBoxVendor.Location = new System.Drawing.Point(4, 90);
            this.textBoxVendor.Name = "textBoxVendor";
            this.textBoxVendor.Size = new System.Drawing.Size(186, 21);
            this.textBoxVendor.TabIndex = 2;
            this.textBoxVendor.TextChanged += new System.EventHandler(textBox_TextChanged);
            this.textBoxVendor.TextChanged += new System.EventHandler(textBoxVendor_TextChanged);
            // 
            // buttonVendor
            // 
            this.buttonVendor = new CMXPictureButton();
            this.buttonVendor.BackColor = System.Drawing.Color.White;
            this.buttonVendor.Location = new System.Drawing.Point(196, 80);
            this.buttonVendor.Name = "buttonPrint";
            this.buttonVendor.Size = new System.Drawing.Size(32, 32);
            this.buttonVendor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonVendor.TransparentColor = System.Drawing.Color.White;
            this.buttonVendor.Click += new System.EventHandler(buttonVendor_Click);
            this.buttonVendor.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonVendor.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonVendor.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            this.buttonVendor.Enabled = false;
            // 
            // labelOrdRef
            // 
            this.labelOrdRef = new System.Windows.Forms.Label();
            this.labelOrdRef.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelOrdRef.Location = new System.Drawing.Point(4, 120);
            this.labelOrdRef.Name = "labelOrdRef";
            this.labelOrdRef.Size = new System.Drawing.Size(226, 13);
            this.labelOrdRef.Text = "PO Reference";
            // 
            // textBoxOrdRef
            // 
            this.textBoxOrdRef = new CMXTextBox();
            this.textBoxOrdRef.Location = new System.Drawing.Point(4, 138);
            this.textBoxOrdRef.Name = "textBoxOrdRef";
            this.textBoxOrdRef.Size = new System.Drawing.Size(226, 21);
            this.textBoxOrdRef.TabIndex = 3;
            this.textBoxOrdRef.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // labelDest
            // 
            this.labelDest = new System.Windows.Forms.Label();
            this.labelDest.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelDest.Location = new System.Drawing.Point(4, 170);
            this.labelDest.Name = "labelDest";
            this.labelDest.Size = new System.Drawing.Size(80, 13);
            this.labelDest.Text = "Destination";
            // 
            // textBoxDest
            // 
            this.textBoxDest = new CMXTextBox();
            this.textBoxDest.Location = new System.Drawing.Point(4, 188);
            this.textBoxDest.Name = "textBoxDest";
            this.textBoxDest.Size = new System.Drawing.Size(80, 21);
            this.textBoxDest.TabIndex = 4;
            this.textBoxDest.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // labelMot
            // 
            this.labelMot = new System.Windows.Forms.Label();
            this.labelMot.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelMot.Location = new System.Drawing.Point(100, 170);
            this.labelMot.Name = "labelMot";
            this.labelMot.Size = new System.Drawing.Size(70, 13);
            this.labelMot.Text = "MOT";
            // 
            // textBoxMOT
            // 
            this.textBoxMOT = new CMXTextBox();
            this.textBoxMOT.Location = new System.Drawing.Point(100, 188);
            this.textBoxMOT.Name = "textBoxMOT";
            this.textBoxMOT.Size = new System.Drawing.Size(85, 21);
            this.textBoxMOT.TabIndex = 4;
            this.textBoxMOT.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // buttonMOT
            // 
            this.buttonMOT = new CMXPictureButton();
            this.buttonMOT.BackColor = System.Drawing.Color.White;
            this.buttonMOT.Location = new System.Drawing.Point(194, 180);
            this.buttonMOT.Name = "buttonDamage";
            this.buttonMOT.Size = new System.Drawing.Size(32, 32);
            this.buttonMOT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonMOT.TransparentColor = System.Drawing.Color.White;
            this.buttonMOT.Click += new System.EventHandler(buttonMOT_Click);
            this.buttonMOT.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonMOT.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            // 
            // Form1
            // 
            this.panelContent.Size = new System.Drawing.Size(234, 268);
            this.panelContent.Location = new System.Drawing.Point(3, 20);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.panelContent.Controls.Add(this.labelOrdRef);
            this.panelContent.Controls.Add(this.textBoxOrdRef);
            this.panelContent.Controls.Add(this.labelVendor);
            this.panelContent.Controls.Add(this.textBoxVendor);
            this.panelContent.Controls.Add(this.buttonVendor);
            this.panelContent.Controls.Add(this.labelDest);
            this.panelContent.Controls.Add(this.textBoxPickupRef);
            this.panelContent.Controls.Add(this.labelPickupRef);
            this.panelContent.Controls.Add(this.textBoxDest);
            this.panelContent.Controls.Add(this.labelMot);
            this.panelContent.Controls.Add(this.textBoxMOT);
            this.panelContent.Controls.Add(this.buttonMOT);
            this.panelContent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelPickupRef;
        private System.Windows.Forms.Label labelVendor;
        private System.Windows.Forms.Label labelOrdRef;
        private System.Windows.Forms.Label labelDest;
        private System.Windows.Forms.Label labelMot;
        private CMXTextBox textBoxPickupRef;
        private CMXTextBox textBoxVendor;
        private CMXTextBox textBoxOrdRef;
        private CMXTextBox textBoxDest;
        private CMXTextBox textBoxMOT;
        private CMXPictureButton buttonMOT;
        private CargoMatrix.UI.CMXPictureButton buttonVendor;

    }
}
