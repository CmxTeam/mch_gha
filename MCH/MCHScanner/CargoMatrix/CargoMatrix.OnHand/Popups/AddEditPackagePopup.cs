﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.OnHand;
using CargoMatrix.Communication.WSOnHand;

namespace CargoMatrix.OnHand
{
    public partial class AddEditPackagePopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        private IPackage package;
        public int Quantitiy { get; set; }
        private bool isBatchEdit;

        public AddEditPackagePopup(bool isBatchEdit)
        {
            InitializeComponent();
            this.isBatchEdit = isBatchEdit;
            this.buttonOk.Enabled = false;
            this.Title = isBatchEdit ? "Edit Pieces" : "Add New Piece/s";
            this.textBoxQuantity.Text = Quantitiy > 1 ? Quantitiy.ToString() : "1";
            this.textBoxQuantity.Enabled = true;
        }

        public IPackage Package
        {
            get
            {
                if (this.package == null)
                    this.package = new CMXDomesticPackage();
                return this.package;
            }
            set
            {
                this.package = value;
                this.textBoxQuantity.Enabled = false;
                this.Title = "Edit Piece " + value.PieceNumber;
                this.labelCondition.Text = value.Condition;
                this.labelPackageType.Text = value.PackageType;
                this.textBoxHeight.Text = value.Height.ToString();
                this.textBoxLength.Text = value.Length.ToString();
                this.textBoxSlac.Text = value.Slac.ToString();
                this.textBoxWeight.Text = value.Weight.ToString();
                this.textBoxWidth.Text = value.Width.ToString();
                InputChanged_Validate(null, EventArgs.Empty);


            }
        }

        protected override void buttonOk_Click(object sender, EventArgs e)
        {
            if (ValidateInput())
            {
                Package.Condition = labelCondition.Text;
                Package.PackageType = labelPackageType.Text;
                Package.Height = double.Parse(textBoxHeight.Text);
                Package.Length = double.Parse(textBoxLength.Text);
                Package.Width = double.Parse(textBoxWidth.Text);
                Package.Weight = double.Parse(textBoxWeight.Text);
                Package.Slac = int.Parse(textBoxSlac.Text);
                Package.DimUOM = "IN";
                Package.WeightUOM = "LB";
                Quantitiy = int.Parse(textBoxQuantity.Text);
                DialogResult = DialogResult.OK;
            }
        }

        private bool ValidateInput()
        {
            /// validate length
            try
            {
                int length = int.Parse(textBoxLength.Text);
                if (length <= 0)
                    throw new Exception();
            }
            catch (Exception)
            {
                SharedMethods.ShowFailMessage("Length cannot be empty or zero");
                textBoxLength.Focus();
                return false;
            }

            /// validate width
            try
            {
                int width = int.Parse(textBoxWidth.Text);
                if (width <= 0)
                    throw new Exception();
            }
            catch (Exception)
            {
                SharedMethods.ShowFailMessage("Width cannot be empty or zero");
                textBoxWidth.Focus();
                return false;
            }

            /// validate height
            try
            {
                int height = int.Parse(textBoxHeight.Text);
                if (height <= 0)
                    throw new Exception();
            }
            catch (Exception)
            {
                SharedMethods.ShowFailMessage("Height cannot be empty or zero");
                textBoxHeight.Focus();
                return false;
            }

            /// validate weight
            try
            {
                int weight = int.Parse(textBoxWeight.Text);
                if (weight <= 0)
                    throw new Exception();

            }
            catch (Exception)
            {
                SharedMethods.ShowFailMessage("Weight cnnot be empty or zero");
                textBoxWeight.Focus();
                return false;
            }

            /// validate slac
            try
            {
                int slac = int.Parse(textBoxSlac.Text);
                if (slac <= 0)
                    throw new Exception();
            }
            catch (Exception)
            {
                SharedMethods.ShowFailMessage("Slac cnnot be empty or zero");
                textBoxSlac.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(labelPackageType.Text))
            {
                SharedMethods.ShowFailMessage("Select Package Type");
                return false;
            }

            if (string.IsNullOrEmpty(labelCondition.Text))
            {
                SharedMethods.ShowFailMessage("Select Condition");
                return false;
            }
            return true;
        }
        private void buttonPackageType_Click(object sender, System.EventArgs e)
        {
            var package = SharedMethods.DisplayPackageTypesList();
            if (!string.IsNullOrEmpty(package))
            {
                labelPackageType.Text = package;
            }
        }
        private void buttonCondition_Click(object sender, System.EventArgs e)
        {
            var condition = SharedMethods.DisplayConditionsList();
            if (!string.IsNullOrEmpty(condition))
            {
                labelCondition.Text = condition;
            }
        }
        private void InputChanged_Validate(object sender, System.EventArgs e)
        {
            buttonOk.Enabled = !string.IsNullOrEmpty(labelCondition.Text) &&
                !string.IsNullOrEmpty(labelPackageType.Text) &&
                !string.IsNullOrEmpty(textBoxHeight.Text) &&
                !string.IsNullOrEmpty(textBoxLength.Text) &&
                !string.IsNullOrEmpty(textBoxSlac.Text) &&
                !string.IsNullOrEmpty(textBoxWeight.Text) &&
                !string.IsNullOrEmpty(textBoxWidth.Text) &&
                !string.IsNullOrEmpty(textBoxQuantity.Text);


        }

    }
}
