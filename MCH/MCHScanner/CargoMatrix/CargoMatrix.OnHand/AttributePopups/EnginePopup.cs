﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.OnHand
{
    public partial class EnginePopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        public EnginePopup()
        {
            InitializeComponent();
        }

        private void textBoxSerialNo_TextChanged(object sender, EventArgs e)
        {
            buttonOk.Enabled = !string.IsNullOrEmpty(textBoxSerialNo.Text);
        }
    }
}
