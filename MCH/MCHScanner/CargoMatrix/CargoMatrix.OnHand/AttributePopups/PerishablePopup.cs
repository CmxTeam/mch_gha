﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.OnHand
{
    public partial class PerishablePopup : CargoMatrix.Utilities.CMXMessageBoxPopup
    {
        public PerishablePopup()
        {
            InitializeComponent();
        }

        private void textBoxQuantity_TextChanged(object sender, EventArgs e)
        {
            buttonOk.Enabled = !string.IsNullOrEmpty(textBoxTempFrom.Text);
        }
    }
}
