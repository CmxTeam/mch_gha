﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.WSOnHand;
using System.Windows.Forms;
using CustomListItems.RenderItems;
using CustomUtilities;

namespace CargoMatrix.OnHand
{
    public class DropToLocation : CargoMatrix.CargoUtilities.SmoothListBoxOptions
    {
        private CustomListItems.HeaderItem headerItem;
        private MessageListBox PiecesList;

        private OnhandDetails onhandDetails;
        private string progress;
        private PictureBox pictureBoxBottomPane;

        public string Progress
        {
            get { return progress; }
            set
            {
                progress = value;
                this.pictureBoxBottomPane.Refresh();
            }
        }

        public DropToLocation()
        {
            InitializeComponent();


            this.panelHeader2.Controls.Add(onhandDetails);
            this.Scrollable = false;
            headerItem = new CustomListItems.HeaderItem();
            this.TitleText = "CargoOnhand - Drop to Location";
            headerItem.Label1 = SharedMethods.CurrentPickup.ShipmentNO;
            headerItem.Label2 = "Total Pieces: " + SharedMethods.CurrentPickup.TotalPieces;
            headerItem.Label3 = "SCAN ONHAND";

            headerItem.ButtonImage = CargoMatrix.Resources.Skin.Forklift_btn;
            headerItem.ButtonPressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over;
            this.headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            this.panelHeader2.Controls.Add(headerItem);
            headerItem.Logo = SharedMethods.GetStatusIcon(SharedMethods.CurrentPickup.Status);

            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeReadNotifyHandler);

            onhandDetails = new OnhandDetails() { Location = new Point(0, headerItem.Height) };
            onhandDetails.ButtonListClick += new EventHandler(ButtonListClick);
            onhandDetails.ButtonPrintClick += new EventHandler(ButtonPrintClick);
            this.panelHeader2.Controls.Add(onhandDetails);
            onhandDetails.DisplayOnhand(null);
            Forklift.Instance.ItemCountChanged += new EventHandler(ForkliftCountChanged);
            var taskDetails = Communication.OnHand.OnHand.Instance.GetOnhandTask(SharedMethods.CurrentPickup.ShipmentNO);
            if (taskDetails.id != 0)
                Forklift.Instance.TaskID = (int)taskDetails.id;
        }


        #region eventhandlers
        void ButtonPrintClick(object sender, EventArgs e)
        {
            BarcodeEnabled = false;
            CustomUtilities.OnhandLabelPrinter.Show(SharedMethods.CurrentPickup.ShipmentID, SharedMethods.CurrentPickup.ShipmentNO, SharedMethods.CurrentPickup.TotalPieces);
            BarcodeEnabled = true;
        }

        void ButtonListClick(object sender, EventArgs e)
        {
            var pieceItem = SharedMethods.DisplayPiecesList();

            if (null != pieceItem)
            {
                var status = Communication.OnHand.OnHand.Instance.ScanPiecesToForklift(pieceItem.Cast<int>().ToArray<int>(), SharedMethods.CurrentPickup.ShipmentID, Forklift.Instance.TaskID, Forklift.Instance.ID);
                if (status.TransactionStatus1 == true)
                    Forklift.Instance.ItemsCount = status.TransactionRecords;

                else
                    SharedMethods.ShowFailMessage(status.TransactionError);
            }
        }
        void ForkliftCountChanged(object sender, EventArgs e)
        {
            this.headerItem.Counter = Forklift.Instance.ItemsCount;
        }

        void BarcodeReadNotifyHandler(string barcodeData)
        {
            var barcode = Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (barcode.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    var status = Communication.OnHand.OnHand.Instance.DropAllPiecesToLocation(Forklift.Instance.TaskID, barcode.Location, Forklift.Instance.ID);
                    if (status.TransactionStatus1 == true)
                        onhandDetails.Clear();
                    EvaluateStatus(status);
                    if (TryFinalize(status))
                        return;
                    break;

                case CMXBarcode.BarcodeTypes.OnHand:
                    var packageInfo = Communication.OnHand.OnHand.Instance.ScanPieceToForklift(barcode.OnHandNumber, barcode.PieceNumber, Forklift.Instance.TaskID, Forklift.Instance.ID);
                    if (packageInfo.TransactionStatus.TransactionStatus1 == true)
                        onhandDetails.DisplayOnhand(packageInfo);
                    EvaluateStatus(packageInfo.TransactionStatus);


                    break;
                default:
                    SharedMethods.ShowFailMessage("Invalid Barcode Scan");
                    break;
            }
            BarcodeEnabled = true;
        }

        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            PiecesList = new MessageListBox();
            PiecesList.HeaderText = "Forklift";
            PiecesList.HeaderText2 = "Select Pieces to Drop to Location";
            PiecesList.MultiSelectListEnabled = true;
            PiecesList.OneTouchSelection = false;
            PopulateForkliftPieces();
            if (DialogResult.OK == PiecesList.ShowDialog())
            {
                var pcs = PiecesList.SelectedItems.OfType<ListItemWithButton<PieceItem>>().Select(selIte => selIte.ItemData).ToArray<PieceItem>();
                DropPiecesToLocation(pcs);
            }
        }

        void RemoveFromFL(object sender, EventArgs e)
        {
            var itemData = (sender as ListItemWithButton<PieceItem>).ItemData;

            string msg = string.Format("Are you sure you want to remove piece {0}?", itemData.PieceNumber);
            if (DialogResult.OK == UI.CMXMessageBox.Show(msg, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
            {
                var status = Communication.OnHand.OnHand.Instance.RemovePieceFromForklift((int)SharedMethods.CurrentPickup.ShipmentID, itemData.PieceId, Forklift.Instance.TaskID, Forklift.Instance.ID);
                EvaluateStatus(status);
                PopulateForkliftPieces();
            }
        }
        #endregion

        public override void LoadControl()
        {
            if (!BarcodeEnabled)
                BarcodeEnabled = true;
        }

        private void PopulateForkliftPieces()
        {
            var pieces = Communication.OnHand.OnHand.Instance.GetForkliftPieces(Forklift.Instance.ID);
            PiecesList.RemoveAllItems();
            Forklift.Instance.ItemsCount = pieces.Length;
            foreach (var pieceItem in pieces)
            {
                string line1 = string.Format("Piece: {0} of {1}", pieceItem.PieceNumber, pieces.Length);
                var pieceListItem = new ListItemWithButton<PieceItem>(line1, string.Empty, Resources.Skin.PieceMode, Resources.Skin.cc_trash, Resources.Skin.cc_trash_over, pieceItem);
                pieceListItem.OnButtonClick += new EventHandler(RemoveFromFL);
                PiecesList.AddItem(pieceListItem);
            }
        }



        private void EvaluateStatus(TransactionStatus status)
        {
            if (status.TransactionStatus1 == true)
            {
                Forklift.Instance.ItemsCount = status.TransactionRecords;
                this.Progress = string.Format("Pcs: {0} of {1} | {2}%", status.TotalProcessed, status.TotalCount, status.TaskProgress.ToString("0."));
            }
            else
                SharedMethods.ShowFailMessage(status.TransactionError);
        }

        private bool TryFinalize(TransactionStatus status)
        {
            if (status.TotalProcessed == status.TotalCount
                && DialogResult.OK == UI.CMXMessageBox.Show("All Pieces Are Dropped to Location. Click Ok To Go To Tasks List", "Drop Complete", CargoMatrix.UI.CMXMessageBoxIcon.Success))
            {
                Communication.OnHand.OnHand.Instance.DropToLocationCompleted(SharedMethods.CurrentPickup.ShipmentID);
                UI.CMXAnimationmanager.GoToContol(OnHandTasksList.FORM_NAME);
                return true;
            }
            return false;
        }

        private void DropPiecesToLocation(PieceItem[] pcs)
        {
            BarcodeEnabled = false;
            ScanEnterPopup dropPopup = new ScanEnterPopup(CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Location);
            dropPopup.Title = "Scan/Enter Drop Location";
            if (DialogResult.OK == dropPopup.ShowDialog())
            {
                var status = CargoMatrix.Communication.OnHand.OnHand.Instance.DropPiecesToLocation(pcs, SharedMethods.CurrentPickup.ShipmentID, Forklift.Instance.TaskID, dropPopup.Text, Forklift.Instance.ID);
                EvaluateStatus(status);
                if (TryFinalize(status))
                    return;
            }
            BarcodeEnabled = true;
        }


        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxBottomPane = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // pictureBoxBottomPane
            // 
            this.pictureBoxBottomPane.Location = new System.Drawing.Point(0, 211);
            this.pictureBoxBottomPane.Name = "pictureBoxBottomPane";
            this.pictureBoxBottomPane.Size = new System.Drawing.Size(240, 20);
            this.pictureBoxBottomPane.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBottomPane.Image = global::Resources.Graphics.Skin.nav_bg;
            this.pictureBoxBottomPane.Paint += new System.Windows.Forms.PaintEventHandler(pictureBoxBottomPane_Paint);
            //
            // page
            //
            this.panelHeader2.Height = 231;
            this.panelHeader2.Controls.Add(this.pictureBoxBottomPane);
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.ResumeLayout(false);
        }
        void pictureBoxBottomPane_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            using (Font ft = new Font("Tahoma", 9F, FontStyle.Bold))
            {
                SizeF textProgressSize = e.Graphics.MeasureString(Progress, ft);
                int y1 = (int)(pictureBoxBottomPane.Height - textProgressSize.Height) / 2;
                int x2 = (int)(pictureBoxBottomPane.Width - textProgressSize.Width) / 2;
                e.Graphics.DrawString(Progress, ft, new SolidBrush(Color.White), x2, y1);
            }
        }
    }
}
