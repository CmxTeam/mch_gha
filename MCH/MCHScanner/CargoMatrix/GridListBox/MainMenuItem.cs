﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;


namespace CargoMatrix.GridListBox
{


    public partial class MainMenuItem :  SmoothListbox.ListItems.ListItem, IGridListItem// GridListItem//SmoothListbox.ListItems.ListItem// UserControl
    {
        //public delegate bool ValidateQuantity(MainMenuItem selectedItem, int prevQuantity, int newQuantity);
        
        //public EventHandler UndoClicked;
        //public ValidateQuantity Validate_Quantity;
        public EventHandler QuantityChanged;
        Stack<int> _undoHistory = new Stack<int>();
        
        //public EventHandler Quantity_Changed;

        private bool editQuantityButtonDown = false;
        private bool editPiecesPressed = false;
        private CargoMatrix.Communication.WSPieceScan.Modes m_mode;
        Nullable<int> rowRecID = null;
        //public CargoMatrix.UI.CMXTextBox countTextBox;
        CargoMatrix.Communication.WSPieceScan.StorageTypes _locationType;
        int _actionID;
        public MainMenuItem(CargoMatrix.Communication.WSPieceScan.Modes mode, int pieceNo, bool EditMode, int actionID)
        {
            InitializeComponent();
            _actionID = actionID;
            m_mode = mode;
            //labelPieceNo.Text = Convert.ToString(pieceNo);
            Init();
            editPiecesPressed = EditMode;
            //if (EditMode)
            //{
            //    if (countTextBox == null)
            //    {
            //        countTextBox = new CargoMatrix.UI.CMXTextBox();

            //        countTextBox.KeyDown += new KeyEventHandler(countTextBox_KeyDown);
            //        countTextBox.Location = new Point(1, 1);
            //        countTextBox.Width = panelPiece.Width - 2;
            //        countTextBox.Height = panelPiece.Height - 2;
            //        panelPiece.Controls.Add(countTextBox);
            //        countTextBox.BringToFront();
            //    }
            //    countTextBox.Text = Convert.ToString(pieceNo);
            //    pictureBox1.Visible = false;
            //    countTextBox.Visible = true;
            //    countTextBox.SelectAll();
            //    countTextBox.Focus();
            
            //}
        
        }
        public MainMenuItem(CargoMatrix.Communication.WSPieceScan.Modes mode, int pieceNo, int actionID)
        {
            InitializeComponent();
            _actionID = actionID;
            m_mode = mode;
            labelPieceNo.Text = Convert.ToString(pieceNo);
            Init();
           
        }
        private void Init()
        {
            //buttonUndo.Image = GridListBoxResources.undo_btn_dis;
            //buttonUndo.PressedImage = GridListBoxResources.undo_btn_over;

            if (m_mode == CargoMatrix.Communication.WSPieceScan.Modes.Count) // only allow edit piece in count mode
            {
                (labelPieceNo as Control).MouseDown += new MouseEventHandler(EditQuantity_MouseDown);
                panelPiece.MouseDown += new MouseEventHandler(EditQuantity_MouseDown);

                (labelPieceNo as Control).MouseMove += new MouseEventHandler(EditQuantity_MouseMove);
                panelPiece.MouseMove += new MouseEventHandler(EditQuantity_MouseMove);

                (labelPieceNo as Control).MouseUp += new MouseEventHandler(EditQuantity_MouseUp);
                panelPiece.MouseUp += new MouseEventHandler(EditQuantity_MouseUp);

                (labelPieceNo as Control).Click += new EventHandler(EditQuantity_MouseClick);
                panelPiece.Click += new EventHandler(EditQuantity_MouseClick);
            }

            panelBaseLine.SendToBack();
            pictureBox1.Visible = false;
        
        }
        void EditQuantity_MouseClick(object sender, EventArgs e)
        {
            editPiecesPressed = true;

        }
        

        void EditQuantity_MouseMove(object sender, MouseEventArgs e)
        {
            if (editQuantityButtonDown)
            {
                if ((e.X < panelPiece.Left || e.X > panelPiece.Right) || (e.Y < panelPiece.Top || e.Y > panelPiece.Bottom))
                {
                    editQuantityButtonDown = false;
                
                }
            }
        }
              
        void EditQuantity_MouseDown(object sender, MouseEventArgs e)
        {
            editQuantityButtonDown = true;
        }
        void EditQuantity_MouseUp(object sender, MouseEventArgs e)
        {
            

            //if (editQuantityButtonDown == true)
            //{
            //    editQuantityButtonDown = false;

                
              
                

            //    if (EditQuantity_Click != null)
            //        EditQuantity_Click(this, e);

            
            //}
            
            
        }

        void countTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Tab:
                case Keys.Enter:
                    //UpdateQuantity();
                    //if (Validate_Quantity != null)
                    //{
                    //    if (Validate_Quantity(this, Convert.ToInt32(labelPieceNo.Text), Convert.ToInt32(countTextBox.Text)))
                    //    {
                    //        labelPieceNo.Text = countTextBox.Text;
                    //        countTextBox.Visible = false;
                    //        if (QuantityChanged != null)
                    //            QuantityChanged(this, EventArgs.Empty);
                        
                    //    }
                    //}
                    
                    e.Handled = true;
                    
                    break;
            }

            if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                e.Handled = true;
        }

       


        public override void SelectedChanged(bool isSelected)
        {
            m_selected = isSelected;

            if (isSelected)
                BackColor = focusedColor;
            else
                BackColor = backColor;

            foreach (Control control in Controls)
            {
                if (control is Splitter)
                    continue;
                control.BackColor = BackColor;
            }

            if (isSelected)
                pictureBox1.Visible = true;
            else
                pictureBox1.Visible = false;
           
            //Focus(isSelected);
            if (editPiecesPressed == true)
            {
                editPiecesPressed = false;
                int forkliftID = CargoMatrix.Communication.HostPlusIncomming.Instance.GetForkLiftID();
                //if (LocationID != forkliftID)
                //{
                //    if (countTextBox == null)
                //    {
                //        countTextBox = new CargoMatrix.UI.CMXTextBox();

                //        countTextBox.KeyDown += new KeyEventHandler(countTextBox_KeyDown);
                //        countTextBox.Location = new Point(1, 1);
                //        countTextBox.Width = panelPiece.Width - 2;
                //        countTextBox.Height = panelPiece.Height - 2;
                //        panelPiece.Controls.Add(countTextBox);
                //        countTextBox.BringToFront();
                //    }
                //    countTextBox.Text = labelPieceNo.Text;
                //    pictureBox1.Visible = false;
                //    countTextBox.Visible = true;
                //    countTextBox.SelectAll();
                //    countTextBox.Focus();
                //}

            }
            else
            {
                //if (countTextBox != null)
                {
                    //if (countTextBox.Visible)
                    {
                        //UpdateQuantity();
                        //if (Validate_Quantity != null)
                        //{
                        //    if (Validate_Quantity(this, Convert.ToInt32(labelPieceNo.Text), Convert.ToInt32(countTextBox.Text)))
                        //    {
                        //        labelPieceNo.Text = countTextBox.Text;
                        //        countTextBox.Visible = false;
                        //        if (QuantityChanged != null)
                        //            QuantityChanged(this, EventArgs.Empty);

                        //    }
                        //}
                        
                        
                        
                    }
                }

            }
        }
        public override void Focus(bool focused)
        {
            if (focused || m_selected)
                BackColor = focusedColor;
            else 
                BackColor = backColor;

            foreach (Control control in Controls)
            {
                if (control is Splitter)
                    continue;
                control.BackColor = BackColor;
            }
            //this.panelBaseLine.BackColor = System.Drawing.Color.LightGray;

        }

        

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (_undoHistory.Count > 0)
            {
                int pieces = _undoHistory.Pop();
                int pieces2 = Convert.ToInt32(labelPieceNo.Text);

                //if (Validate_Quantity(this, pieces2, pieces + pieces2))
                {
                    
                    labelPieceNo.Text = Convert.ToString(pieces + pieces2);
                    //int moving = Convert.ToInt32(labelMoving.Text);
                    //moving -= pieces;
                    //labelMoving.Text = Convert.ToString(moving);

                    if (QuantityChanged != null)
                        QuantityChanged(this, EventArgs.Empty);

                    CargoMatrix.Communication.WSPieceScan.ScanCountInfo[] items = new CargoMatrix.Communication.WSPieceScan.ScanCountInfo[1];
                    items[0] = new CargoMatrix.Communication.WSPieceScan.ScanCountInfo();
                    items[0].NoOfPieces = pieces;

                    int loc = CargoMatrix.Communication.HostPlusIncomming.Instance.GetForkLiftID();
                    items[0].LocationID = loc;

                    CargoMatrix.Communication.HostPlusIncomming.Instance.SetLocationInCount(items, _actionID, HousebillRecID, LocationID);
                    
                }
                //else
                //{
                //    _undoHistory.Push(pieces);
                //}
            }

            //if (_undoHistory.Count <= 0)
                //buttonUndo.Enabled = false;
           
        }

        public int PieceCount
        {
            get
            {

                //if (countTextBox != null && countTextBox.Visible)
                //    return Convert.ToInt32(countTextBox.Text);
                //else
                    return Convert.ToInt32(labelPieceNo.Text);
            }
            set
            {
                labelPieceNo.Text = Convert.ToString(value);
                //if (countTextBox != null)
                //{
                //    countTextBox.Visible = false;
                //}
            }
        }
        public string TextLocation
        {
            set
            {
                labelLocation.Text = value;

                //if (countTextBox != null)
                //{
                //    if (countTextBox.Visible)
                //    {
                //        UpdateQuantity();
                        
                //    }
                //}

            }
            get
            {
                return labelLocation.Text;
            }
        }

        //public int UpdateQuantity()
        //{ 
            
        //    int newValue = 0;
        //    try
        //    {
        //        newValue = Convert.ToInt32(countTextBox.Text);
        //    }
        //    catch (FormatException) { return -1; }

        //    if (newValue < 0)
        //        return -1;

        //    int existing = Convert.ToInt32(labelPieceNo.Text);
        //    int updatedQuantity = existing - newValue;
            

        //    if (updatedQuantity < 0)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("Invalid Quantity, You can enter \'" + labelPieceNo.Text + "\' at the max", "Invalid Quantity", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //        return -1;
        //    }
        //    else
        //    {
                
               


        //        if (newValue > 0 /*&& LocationID > 0*/)
        //        {
        //            int moving = Convert.ToInt32(labelMoving.Text);
        //            moving += newValue;
        //            _undoHistory.Push(newValue);
        //            labelMoving.Text = Convert.ToString(moving);
        //            buttonUndo.Enabled = true;
        //        }

                
        //        if (Validate_Quantity != null)
        //        {
        //            if (Validate_Quantity(this, existing, updatedQuantity))
        //            {
        //                labelPieceNo.Text = Convert.ToString(updatedQuantity);

        //                //countTextBox.Visible = false;
        //                if (QuantityChanged != null)
        //                {
        //                    QuantityChanged(this, EventArgs.Empty);
        //                    if (HousebillRecID > 0 /*&& LocationID > 0*/)
        //                    {
        //                        CargoMatrix.Communication.WSPieceScan.ScanCountInfo[] items = new CargoMatrix.Communication.WSPieceScan.ScanCountInfo[1];
        //                        items[0] = new CargoMatrix.Communication.WSPieceScan.ScanCountInfo();
        //                        items[0].NoOfPieces = newValue;
        //                        items[0].LocationID = LocationID;
        //                        int loc = CargoMatrix.Communication.HostPlusIncomming.Instance.GetForkLiftID();
        //                        CargoMatrix.Communication.HostPlusIncomming.Instance.SetLocationInCount(items, _actionID, HousebillRecID, loc);
        //                    }
        //                }

        //            }
        //        }
               
        //        return updatedQuantity;
        //    }

        
        //}

        public int UpdateQuantity(int quantity)
        {

            int newValue = 0;
            try
            {
                newValue = quantity;
            }
            catch (FormatException) { return -1; }

            if (newValue < 0)
                return -1;

            int existing = Convert.ToInt32(labelPieceNo.Text);
            int updatedQuantity = existing - newValue;


            if (updatedQuantity < 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid Quantity, You can enter \'" + labelPieceNo.Text + "\' at the max", "Invalid Quantity", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return -1;
            }
            else
            {




                //if (newValue > 0 /*&& LocationID > 0*/)
                //{
                //    int moving = Convert.ToInt32(labelMoving.Text);
                //    moving += newValue;
                //    _undoHistory.Push(newValue);
                //    labelMoving.Text = Convert.ToString(moving);
                //    buttonUndo.Enabled = true;
                //}

                labelPieceNo.Text = Convert.ToString(updatedQuantity);

                //if (Validate_Quantity != null)
                //{
                //    if (Validate_Quantity(this, existing, updatedQuantity))
                //    {
                //        labelPieceNo.Text = Convert.ToString(updatedQuantity);

                //        //countTextBox.Visible = false;
                //        if (QuantityChanged != null)
                //        {
                //            QuantityChanged(this, EventArgs.Empty);
                //            if (HousebillRecID > 0 /*&& LocationID > 0*/)
                //            {
                //                CargoMatrix.Communication.WSPieceScan.ScanCountInfo[] items = new CargoMatrix.Communication.WSPieceScan.ScanCountInfo[1];
                //                items[0] = new CargoMatrix.Communication.WSPieceScan.ScanCountInfo();
                //                items[0].NoOfPieces = newValue;
                //                items[0].LocationID = LocationID;
                //                int loc = CargoMatrix.Communication.HostPlusIncomming.Instance.GetForkLiftID();
                //                CargoMatrix.Communication.HostPlusIncomming.Instance.SetLocationInCount(items, _actionID, HousebillRecID, loc);
                //            }
                //        }

                //    }
                //}

                return updatedQuantity;
            }


        }
        //int UndoQuantity(int pieces)
        //{
        //    //int existing = Convert.ToInt32(labelPieceNo.Text);
        //    //int newValue = Convert.ToInt32(countTextBox.Text);
        //    //int updatedQuantity = existing - newValue;
           
        //    //{
        //        //labelPieceNo.Text = Convert.ToString(updatedQuantity);
        //        //int moving = Convert.ToInt32(labelMoving.Text);
        //        //moving += newValue;
        //        //_undoHistory.Push(newValue);
        //        //labelMoving.Text = Convert.ToString(moving);
        //        if (Validate_Quantity != null)
        //        {
        //            if (Validate_Quantity(this, existing, updatedQuantity))
        //            {
        //                countTextBox.Visible = false;
        //                if (QuantityChanged != null)
        //                    QuantityChanged(this, EventArgs.Empty);

        //            }
        //        }

        //        return updatedQuantity;
        //    //}


        //}
        public int LocationID
        {
            get;
            set;
        }
        public CargoMatrix.Communication.WSPieceScan.StorageTypes LocationType
        {
            get
            {
                return _locationType;
            }
            set
            {
                _locationType = value;
                switch (value)
                { 
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.Door:
                        pictureBoxLocationIcon.Image = GridListBoxResources.Door1;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.ScreeningArea:
                        pictureBoxLocationIcon.Image = GridListBoxResources._500DT_16x16;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.Bin:
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.Rack:
                        pictureBoxLocationIcon.Image = GridListBoxResources.Rack;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.Hazardous:
                        pictureBoxLocationIcon.Image = GridListBoxResources.DG;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.HighValue:
                        pictureBoxLocationIcon.Image = GridListBoxResources.Cage;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.TemperatureControl:
                        pictureBoxLocationIcon.Image = GridListBoxResources.termometer;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.InTransit:
                        pictureBoxLocationIcon.Image = GridListBoxResources.Truck16;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.Missing:
                        pictureBoxLocationIcon.Image = GridListBoxResources.Symbol_Information;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.Recovered:
                        pictureBoxLocationIcon.Image = GridListBoxResources.Recovered;
                        break;
                    case CargoMatrix.Communication.WSPieceScan.StorageTypes.Forklift:
                        pictureBoxLocationIcon.Image = GridListBoxResources.Fork_Lift;
                        break;
                        
                        
                }
            
            }
        }
        public Nullable<int> RowRecID
        {
            get
            {
                return rowRecID;
            }
            set
            {
                rowRecID = value;
            }
        }

        //private void buttonUndo_EnabledChanged(object sender, EventArgs e)
        //{
        //    if (buttonUndo.Enabled)
        //    {
        //        buttonUndo.Image = GridListBoxResources.undo_btn;

        //    }
        //    else
        //    {
        //        buttonUndo.Image = GridListBoxResources.undo_btn_dis;
        //    }
        //    //buttonUndo.Refresh();
        //}
        public int HousebillRecID
        {
            set;
            get;

        }
        public int TaskID
        {
            set;
            get;
        }
        public int ActionID
        {
            set;
            get;
        }

        public int Moving
        {
            set
            {
                //labelMoving.Text = Convert.ToString(value);
            }
        }
        public CargoMatrix.Communication.WSPieceScan.ScanTypes ScanType
        {
            set;
            get;
        }
        //public void MovingComplete()
        //{
        //    buttonUndo.Enabled = false;
        //    _undoHistory.Clear();
        //    labelMoving.Text = "0";

        //}
    }
}
