﻿namespace CargoMatrix.GridListBox
{
    partial class ForkLiftItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxLocationIcon = new OpenNETCF.Windows.Forms.PictureBox2();
            this.labelLocation = new System.Windows.Forms.Label();
            this.panelPiece = new System.Windows.Forms.Panel();
            this.labelPieceNo = new System.Windows.Forms.Label();
            this.panelMoving = new System.Windows.Forms.Panel();
            this.panelBaseLine = new System.Windows.Forms.Splitter();
            this.panel1.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxLocationIcon)).BeginInit();
            this.panelPiece.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitter4
            // 
            this.splitter4.BackColor = System.Drawing.Color.Silver;
            this.splitter4.Location = new System.Drawing.Point(188, 0);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(1, 31);
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.Color.Silver;
            this.splitter3.Location = new System.Drawing.Point(157, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1, 31);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.Silver;
            this.splitter2.Location = new System.Drawing.Point(40, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1, 31);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.pictureBoxLocationIcon);
            this.panel1.Controls.Add(this.labelLocation);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(41, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(116, 31);
            // 
            // pictureBoxLocationIcon
            // 
            this.pictureBoxLocationIcon.BackColor = System.Drawing.Color.Gainsboro;
            this.pictureBoxLocationIcon.Location = new System.Drawing.Point(2, 8);
            this.pictureBoxLocationIcon.Name = "pictureBoxLocationIcon";
            this.pictureBoxLocationIcon.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxLocationIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLocationIcon.TransparentColor = System.Drawing.Color.White;
            // 
            // labelLocation
            // 
            this.labelLocation.BackColor = System.Drawing.Color.Gainsboro;
            this.labelLocation.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelLocation.ForeColor = System.Drawing.Color.Red;
            this.labelLocation.Location = new System.Drawing.Point(20, 10);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(95, 12);
            // 
            // panelPiece
            // 
            this.panelPiece.BackColor = System.Drawing.Color.Gainsboro;
            this.panelPiece.Controls.Add(this.labelPieceNo);
            this.panelPiece.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPiece.Location = new System.Drawing.Point(0, 0);
            this.panelPiece.Name = "panelPiece";
            this.panelPiece.Size = new System.Drawing.Size(40, 31);
            // 
            // labelPieceNo
            // 
            this.labelPieceNo.BackColor = System.Drawing.Color.Gainsboro;
            this.labelPieceNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelPieceNo.ForeColor = System.Drawing.Color.Red;
            this.labelPieceNo.Location = new System.Drawing.Point(10, 10);
            this.labelPieceNo.Name = "labelPieceNo";
            this.labelPieceNo.Size = new System.Drawing.Size(28, 12);
            this.labelPieceNo.Text = "0";
            // 
            // panelMoving
            // 
            this.panelMoving.BackColor = System.Drawing.Color.Gainsboro;
            this.panelMoving.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMoving.Location = new System.Drawing.Point(158, 0);
            this.panelMoving.Name = "panelMoving";
            this.panelMoving.Size = new System.Drawing.Size(30, 31);
            // 
            // panelBaseLine
            // 
            this.panelBaseLine.BackColor = System.Drawing.Color.LightGray;
            this.panelBaseLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBaseLine.Location = new System.Drawing.Point(0, 31);
            this.panelBaseLine.Name = "panelBaseLine";
            this.panelBaseLine.Size = new System.Drawing.Size(238, 1);
            // 
            // ForkLiftItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.Controls.Add(this.splitter4);
            this.Controls.Add(this.panelMoving);
            this.Controls.Add(this.splitter3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelPiece);
            this.Controls.Add(this.panelBaseLine);
            this.Name = "ForkLiftItem";
            this.Size = new System.Drawing.Size(238, 32);
            this.panel1.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.pictureBoxLocationIcon)).EndInit();
            this.panelPiece.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelPiece;
        public System.Windows.Forms.Label labelPieceNo;
        private System.Windows.Forms.Label labelLocation;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBoxLocationIcon;
        private System.Windows.Forms.Panel panelMoving;
        protected System.Windows.Forms.Splitter panelBaseLine;
    }
}
