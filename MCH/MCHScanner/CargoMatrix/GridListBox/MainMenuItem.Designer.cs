﻿namespace CargoMatrix.GridListBox
{
    partial class MainMenuItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenuItem));
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxLocationIcon = new OpenNETCF.Windows.Forms.PictureBox2();
            this.labelLocation = new System.Windows.Forms.Label();
            this.panelPiece = new System.Windows.Forms.Panel();
            this.pictureBox1 = new OpenNETCF.Windows.Forms.PictureBox2();
            this.labelPieceNo = new System.Windows.Forms.Label();
            this.pictureBox2 = new OpenNETCF.Windows.Forms.PictureBox2();
            this.panel1.SuspendLayout();
            this.panelPiece.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.Silver;
            this.splitter2.Location = new System.Drawing.Point(40, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1, 32);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBoxLocationIcon);
            this.panel1.Controls.Add(this.labelLocation);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(41, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(197, 32);
            // 
            // pictureBoxLocationIcon
            // 
            this.pictureBoxLocationIcon.Location = new System.Drawing.Point(2, 8);
            this.pictureBoxLocationIcon.Name = "pictureBoxLocationIcon";
            this.pictureBoxLocationIcon.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxLocationIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLocationIcon.TransparentColor = System.Drawing.Color.White;
            // 
            // labelLocation
            // 
            this.labelLocation.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.labelLocation.Location = new System.Drawing.Point(20, 10);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(76, 12);
            // 
            // panelPiece
            // 
            this.panelPiece.Controls.Add(this.pictureBox1);
            this.panelPiece.Controls.Add(this.labelPieceNo);
            this.panelPiece.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPiece.Location = new System.Drawing.Point(0, 0);
            this.panelPiece.Name = "panelPiece";
            this.panelPiece.Size = new System.Drawing.Size(40, 32);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(8, 8);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TransparentColor = System.Drawing.Color.White;
            // 
            // labelPieceNo
            // 
            this.labelPieceNo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPieceNo.Location = new System.Drawing.Point(10, 10);
            this.labelPieceNo.Name = "labelPieceNo";
            this.labelPieceNo.Size = new System.Drawing.Size(28, 12);
            this.labelPieceNo.Text = "0";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = GridListBoxResources.Collapse_Right;
            this.pictureBox2.Location = new System.Drawing.Point(170, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.TransparentColor = System.Drawing.Color.White;
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            // 
            // MainMenuItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelPiece);
            this.Name = "MainMenuItem";
            this.Size = new System.Drawing.Size(238, 32);
            this.panel1.ResumeLayout(false);
            this.panelPiece.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelPiece;
        protected System.Windows.Forms.Label labelPieceNo;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBox1;
        private System.Windows.Forms.Label labelLocation;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBoxLocationIcon;
        private OpenNETCF.Windows.Forms.PictureBox2 pictureBox2;
    }
}
