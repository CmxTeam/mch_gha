﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Viewer;
using SmoothListbox;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSPieceScan;

namespace CargoMatrix.GridListBox
{
    public partial class GridListBox: SmoothListbox.SmoothListbox// CargoMatrix.UI.CMXUserControl// UserControl
    {

        //HAWBList m_housebillList;
        UserControl _previousPage;
        bool m_bViewer = false;
        int m_remainingPieces = 0;
        private int pageCount = 0;
        private int currentPageIndex = -1;
        private List<ShellPage> m_Pages;
        CargoMatrix.Communication.DTO.ShellData m_houseBillData = null;
        protected CargoMatrix.Communication.DTO.TaskItem m_houseBill;
        CargoMatrix.Communication.DTO.TaskItem m_masterBill;
        protected CargoMatrix.Communication.WSPieceScan.PieceListItem[] m_pieceList = null;

        //ManualPieceEntry manualPieceEntryForm;
        CargoMatrix.Utilities.MessageListBox manualPieceEntryForm;
        CustomListItems.OptionsListITem switchMode;
        //CargoMatrix.FreightPhotoCapture.MicroReasons reasonsControl;

        ManualLocationForm m_manualLocationForm;
        protected bool isLoaded = false;
        protected int _actionID;
        int m_scannedPiece;

        CustomListItems.OptionsListITem manualPieceEntryOptionItem;
        CustomListItems.OptionsListITem manualLocationOptionItem;

        protected int m_checkedInPieces = 0;
        //int m_addedPiecesInPieceMode = 0;
        public GridListBox()
        { 
        
        }
        public GridListBox(CargoMatrix.Communication.DTO.TaskItem housebill, CargoMatrix.Communication.DTO.TaskItem masterBill, int actionID, int scannedPiece, Image icon)
        {
            InitializeComponent();
            m_scannedPiece = scannedPiece;
            _actionID = actionID;
            smoothListBoxMainList.labelEmpty.Text = "";
            //panelTitle.Height = labelTitle.Height + panel1.Height;
            using (Bitmap bmp = new Bitmap(GridListBoxResources.thumbnailBackColor))
            {
                horizontalSmoothListbox1.BackColor = bmp.GetPixel(0, 0);
            }
            
            panel1.Parent = panelHeader2;// panelTitle;
            panelHeader2.Height = panel1.Height;
            //panel1.BringToFront();
            m_houseBill = housebill;
            m_masterBill = masterBill;
            //labelHouseBill.Text = housebill.reference;
            //labelLine2.Text = housebill.line2;
            //labelLine3.Text = housebill.line3;
            pictureBoxIcon.Image = icon;// FreightHandlerResources.CheckInManifest_icon;

            panelViewer.Visible = false;
            horizontalSmoothListbox1.Visible = false;
            horizontalSmoothListbox1.ListItemClicked += new ListItemClickedHandler(horizontalSmoothListbox1_ListItemClicked);
            
            //TitleText = m_masterBill.reference;
            m_remainingPieces = (int)m_houseBill.totalPieces;
            summaryRow1.TotalCount = (int)m_houseBill.totalPieces;
            summaryRow1.Mode = m_houseBill.mode;
            //m_pieceList = new List<CargoMatrix.Communication.Data.PeiceItem>();
            switchMode = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.UNKNOWN);
            manualLocationOptionItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MANUAL_LOCATION);
            manualPieceEntryOptionItem = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MANUAL_PEICE_ENTRY);

            //m_housebill = "7M68190";
            
           

        }



        void horizontalSmoothListbox1_ListItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is SmoothListbox.ListItems.ViewerThumbnail)
            { 
                //if(!isLoaded)
                  //  LoadHousebillViewer();
                if (isSelected)
                    OpenPage((listItem as SmoothListbox.ListItems.ViewerThumbnail).ID);
                else
                    ClosePage();
            }
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (horizontalSmoothListbox1 != null && smoothListBoxMainList != null)
            {
                int viewerHeight = 0;
                if (horizontalSmoothListbox1.Visible)
                    viewerHeight = horizontalSmoothListbox1.Height;
                horizontalSmoothListbox1.Width = Width;
                horizontalSmoothListbox1.Top = panelSoftkeys.Top - horizontalSmoothListbox1.Height;
                if(horizontalSmoothListbox1.Visible)
                    summaryRow1.Top = horizontalSmoothListbox1.Top - summaryRow1.Height;
                else
                    summaryRow1.Top = panelSoftkeys.Top - summaryRow1.Height;
                smoothListBoxMainList.Top = panelHeader2.Bottom + headerRow1.Height;
                smoothListBoxMainList.Height = summaryRow1.Top - headerRow1.Bottom;// (viewerHeight + summaryRow1.Height + headerRow1.Height + 1);// horizontalSmoothListbox1.Top - labelTitle.Bottom;
                
            }

        }
        void ClearViewer()
        {
            if (m_Pages != null)
            {
                for (int i = 0; i < m_Pages.Count; i++)
                {
                    m_Pages[i].UnLoadControl();
                    m_Pages[i].Parent = null;
                    m_Pages[i].Dispose();

                }
                m_Pages.Clear();
            }
            pageCount = 0;
            isLoaded = false;
        
        }
        private void LoadHousebillViewer()
        {
            if (!isLoaded)
            {
                if (m_houseBill.actualBill != null)
                {

                    m_houseBillData = CargoMatrix.Communication.WebServiceManager.Instance().GetHousebillDetails(null, m_houseBill.actualBill);

                    //this.smoothListBoxMainList.progressBar1.Value = 45;
                    //this.smoothListBoxMainList.progressBar1.Refresh();
                }
                if (m_houseBillData != null)
                {
                    //this.TitleText = m_houseBillData.Name;

                    ClearViewer();

                    if (m_Pages == null)
                        m_Pages = new List<ShellPage>();


                    panelViewer.Top = panelTitle.Height + panelHeader2.Height;// panel1.Bottom;  // use this for animation  horizontalSmoothListbox1.Top;
                    panelViewer.Left = 0;
                    panelViewer.Width = Width;
                    panelViewer.Height = horizontalSmoothListbox1.Top - panelViewer.Top;

                    for (int i = 0; i < m_houseBillData.m_pages.Count; i++)
                    {
                        SmoothListbox.ListItems.ViewerThumbnail tempThumb = new SmoothListbox.ListItems.ViewerThumbnail();
                        tempThumb.ID = i;
                        tempThumb.Height = horizontalSmoothListbox1.Height - 1;
                        //tempThumb.pictureBox1.Image = FreightHandlerResources.Couch;
                        tempThumb.ImageRegular = (Image) GridListBoxResources.ResourceManager.GetObject(m_houseBillData.m_pages[i].Icon);
                        tempThumb.ImageOver = (Image)GridListBoxResources.ResourceManager.GetObject(m_houseBillData.m_pages[i].Icon + "_over");
                        InsertPage(tempThumb, m_houseBillData.m_pages[i], m_houseBillData.Reference);
                        //SmoothListbox.ListItems.ViewerThumbnail tempThumb = new SmoothListbox.ListItems.ViewerThumbnail();
                        //tempThumb.ID = i;
                        //tempThumb.Height = horizontalSmoothListbox1.Height - 1;
                        //tempThumb.pictureBox1.Image = FreightHandlerResources.Couch;
                        //horizontalSmoothListbox1.AddItem(tempThumb);

                    }

                    isLoaded = true;


                }
            }
        
        }

        public void InsertPage(Control header, CargoMatrix.Communication.DTO.ShellPage page, string pageReference)
        {
            ShellPage newPage = new ShellPage(page, pageReference, 1);
            //newPage.BorderStyle = BorderStyle.FixedSingle;
            newPage.Name = header.Name;
            newPage.Top = 0;
            newPage.Height = panelViewer.Height;// horizontalSmoothListbox1.Top - panelTitle.Bottom;// Height - panelTitle.Height - panelSoftkeys.Height - 4;
            newPage.Left = 0;
            newPage.Width = panelViewer.Width;
            //newPage.m_itemsList = itemsList;
            //newPage.AddItems(itemsList);

            //newPage.LayoutItems();
            //foreach (Control control in newPage.Controls)
            //control.BackColor = Color.Maroon;
            //newPage.LayoutItems();
            m_Pages.Add(newPage);
            AddHeader(header);
            pageCount++;

            //Controls.Add(newPage);
            panelViewer.Controls.Add(newPage);
            
            
            //OpenPage(0);
            //newPage.BringToFront();
            //panelBookMarks.BringToFront();

        }
        private void OpenPage(int pageID)
        {
            
            SuspendLayout();

            m_Pages[pageID].Left = 0;
            m_Pages[pageID].BringToFront();
            panelViewer.Visible = true;
            BarcodeEnabled = false;
            panelViewer.BringToFront();
            
            //panelTitle.BringToFront();
            panelOptions.BringToFront();
            horizontalSmoothListbox1.BringToFront();

            panelSoftkeys.BringToFront();
            currentPageIndex = pageID;

            SetViewerAutoScroll(m_Pages[pageID]);

            
            //HeaderText = m_Pages[currentPageIndex].Name;
            ResumeLayout();

            //smoothListBoxBookMarks.Reset();
            //(smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).SelectedChanged(true);
            
            //smoothListBoxBookMarks.selectedItemsMap[smoothListBoxBookMarks.Items[currentPageIndex]] = true;
            Cursor.Current = Cursors.WaitCursor;
            m_Pages[currentPageIndex].LoadControl();
            Cursor.Current = Cursors.Default;


        }
        private void ClosePage()
        {
            currentPageIndex = -1;
            panelViewer.Visible = false;
            BarcodeEnabled = true;
            horizontalSmoothListbox1.Reset();
            RemoveViewerAutoScroll();
        
        }
        private void AddHeader(Control header)
        {
            if (header is IExtendedListItem)
            {
                (header as IExtendedListItem).SetID(pageCount);
                (header as IExtendedListItem).SetBackColor(Color.AliceBlue);
            }

            horizontalSmoothListbox1.AddItem(header);

        }

        private void SetViewerAutoScroll(SmoothListBoxBase menu)
        {
            this.smoothListBoxMainList.AutoScroll -= smoothListBoxMainList_AutoScroll;

            for (int i = 0; i < m_Pages.Count; i++)
            {
                m_Pages[i].AutoScroll -= PageAutoScroll;

            }
            menu.AutoScroll += PageAutoScroll;
            menu.Scrollable = true;

        }

        private void RemoveViewerAutoScroll()
        {
            if (m_Pages != null)
            {
                for (int i = 0; i < m_Pages.Count; i++)
                {
                    m_Pages[i].AutoScroll -= PageAutoScroll;

                }
            }
            this.smoothListBoxMainList.AutoScroll += smoothListBoxMainList_AutoScroll;
        }
        private void PageAutoScroll(SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            if (m_bOptionsMenu == false)
            {
                switch (direction)
                {
                    case SmoothListBoxBase.DIRECTION.UP:
                        pictureBoxUp.Enabled = enable;
                        if (pictureBoxUp.Enabled == false)
                            m_Pages[currentPageIndex].UpButtonPressed = false;
                        break;
                    case SmoothListBoxBase.DIRECTION.DOWN:
                        //pictureBoxBookMarkNext.Enabled = enable;
                        pictureBoxDown.Enabled = enable;
                        if (pictureBoxDown.Enabled == false)
                            m_Pages[currentPageIndex].DownButtonPressed = false;
                        break;
                }

            }

        }
        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            pictureBoxBack.Refresh();
            if (m_bOptionsMenu)
            {
                base.pictureBoxBack_Click(sender, e);
                return;
            }

            if (panelViewer.Visible)
                ClosePage();
            else
            {
                //base.pictureBoxBack_Click(sender, e);

                if (ContinueWithoutSaving() == false)
                {   
                    return;
                    
                }
                
                pictureBoxBack.Refresh();
                smoothListBoxMainList.RemoveAll();
                CargoMatrix.UI.CMXUserControl control = CargoMatrix.UI.CMXAnimationmanager.GetPreviousControl();

                //if (control is HAWBList)
                {
                    CargoMatrix.UI.CMXAnimationmanager.GoBack();
                }
                //else
                {
                    //CargoMatrix.UI.CMXMessageBox.Show("An unexpected error has occured in loading Consol: " + barcodeObj.MasterBillNo, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    //m_masterBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetMAWBSummary(barcodeObj.MasterBillID);
                    //LoadHAWBList(m_masterBill);

                }
                
            }
        }
        private bool ContinueWithoutSaving()
        {
            return true;
            //bool has = false;
            //for (int i = 0; i < smoothListBoxMainList.ItemsCount; i++)
            //{
            //    if (smoothListBoxMainList.Items[i] is MainMenuItem)
            //    {
            //        if ((smoothListBoxMainList.Items[i] as MainMenuItem).LocationID <= 0)
            //        {
            //            has = true;
            //            break;
            //        }
                
            //    }            
            //}

            //if (has)
            //{
            //    if (CargoMatrix.UI.CMXMessageBox.Show("Un-finished check-in detected. Pieces without location will be discarded. Do you want to continue?", "Incomplete peices will be deleted", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
            //    {
            //        return true;

            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //return true;
        }
        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            //SuspendLayout();
            //if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Count) //count mode
            //{
            //    switchMode.title.Text = "Switch to Piece Mode";

            //}
            //else
            //{
            //    switchMode.title.Text = "Switch to Count Mode";
            //}

            //if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Piece)
            //{
            //    int addedPiecesInPieceMode = 0;

            //    for (int i = 0; i < smoothListBoxMainList.ItemsCount; i++)
            //    {
            //        if (smoothListBoxMainList.Items[i] is MainMenuItem)
            //        {
            //            if ((smoothListBoxMainList.Items[i] as MainMenuItem).PieceCount > 0)
            //            {
            //                if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Piece)
            //                {
            //                    addedPiecesInPieceMode++;
            //                }
            //            }
            //        }
            //    }
            //    if (addedPiecesInPieceMode < summaryRow1.TotalCount)
            //        manualPieceEntryOptionItem.Enabled = true;
            //    else
            //        manualPieceEntryOptionItem.Enabled = false;

            //}
            //else
            //{
            //    manualPieceEntryOptionItem.Enabled = false;
            
            //}

            //if (smoothListBoxMainList.SelectedItems.Count > 0)
            //{
            //    manualLocationOptionItem.Enabled = true;
            //}
            //else
            //{
            //    manualLocationOptionItem.Enabled = false;
            //}


            //panelOptions.BringToFront();
            //panelSoftkeys.BringToFront();
            //ResumeLayout();
            base.pictureBoxMenu_Click(sender, e);
            //smoothListBoxOptions.RefreshScroll();
        }

        private void TempLoad()
        {
            


            

            //SmoothListbox.ListItems.ViewerThumbnail tempThumb = new SmoothListbox.ListItems.ViewerThumbnail();
            //tempThumb.ID = 0;
            //tempThumb.Height = horizontalSmoothListbox1.Height - 1;
            //tempThumb.pictureBox1.Image = FreightHandlerResources.Couch;
            //horizontalSmoothListbox1.AddItem(tempThumb);

            //tempThumb = new SmoothListbox.ListItems.ViewerThumbnail();
            //tempThumb.ID = 1;
            //tempThumb.Height = horizontalSmoothListbox1.Height - 1;
            //tempThumb.pictureBox1.Image = FreightHandlerResources.Couch;
            //horizontalSmoothListbox1.AddItem(tempThumb);

            //tempThumb = new SmoothListbox.ListItems.ViewerThumbnail();
            //tempThumb.ID = 2;
            //tempThumb.Height = horizontalSmoothListbox1.Height - 1;
            //tempThumb.pictureBox1.Image = FreightHandlerResources.Couch;
            //horizontalSmoothListbox1.AddItem(tempThumb);

            //tempThumb = new SmoothListbox.ListItems.ViewerThumbnail();
            //tempThumb.ID = 3;
            //tempThumb.Height = horizontalSmoothListbox1.Height - 1;
            //tempThumb.pictureBox1.Image = FreightHandlerResources.Couch;
            //horizontalSmoothListbox1.AddItem(tempThumb);

            //tempThumb = new SmoothListbox.ListItems.ViewerThumbnail();
            //tempThumb.ID = 4;
            //tempThumb.Height = horizontalSmoothListbox1.Height - 1;
            //tempThumb.pictureBox1.Image = FreightHandlerResources.Couch;
            //horizontalSmoothListbox1.AddItem(tempThumb);

            //tempThumb = new SmoothListbox.ListItems.ViewerThumbnail();
            //tempThumb.ID = 5;
            //tempThumb.Height = horizontalSmoothListbox1.Height - 1;
            //tempThumb.pictureBox1.Image = FreightHandlerResources.Couch;
            //horizontalSmoothListbox1.AddItem(tempThumb);

            //MainMenuItem menuItem = new MainMenuItem(false, 3);
            //menuItem.Delete_Click += new EventHandler(DeleteRow_Click);
            //menuItem.PhotoCapture_Click += new EventHandler(PhotoCaptureRow_Click);
            ////menuItem.Quantity_Changed += new EventHandler(Quantity_Changed);
            //menuItem.Validate_Quantity += new MainMenuItem.ValidateQuantity(Validate_Quantity);
            //menuItem.QuantityChanged += new EventHandler(Quantity_Changed);

            //smoothListBoxMainList.AddItem(menuItem);
            //smoothListBoxMainList.AddItem(new MainMenuItem(m_houseBill.pieceLevel, 1));
            //smoothListBoxMainList.AddItem(new MainMenuItem(m_houseBill.pieceLevel, 2));
            //smoothListBoxMainList.AddItem(new MainMenuItem(m_houseBill.pieceLevel, 3 ));
            //smoothListBoxMainList.AddItem(new MainMenuItem(m_houseBill.pieceLevel, 4));
            //smoothListBoxMainList.AddItem(new MainMenuItem(m_houseBill.pieceLevel, 5));
            UpdateSummary();
        }
        protected virtual void UpdateSummary()
        {
            m_checkedInPieces = 0;
            

            for (int i = 0; i < smoothListBoxMainList.ItemsCount; i++)
            {
                if (smoothListBoxMainList.Items[i] is IGridListItem)
                {
                    if ((smoothListBoxMainList.Items[i] as IGridListItem).PieceCount > 0 && (smoothListBoxMainList.Items[i] as IGridListItem).TextLocation != "")
                    {
                        if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Piece)
                        {
                            m_checkedInPieces++;
                        }
                        else
                        {
                            m_checkedInPieces += (smoothListBoxMainList.Items[i] as MainMenuItem).PieceCount;
                        }
                    }
                }
            }

            summaryRow1.Progress = m_checkedInPieces;
            
            if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Count)
            {
                if (m_checkedInPieces < summaryRow1.TotalCount)
                {

                    AddNewCountModeRow(summaryRow1.TotalCount - m_checkedInPieces);
                }
                else if (m_checkedInPieces > summaryRow1.TotalCount)
                {
                    if (smoothListBoxMainList.Items[0] is MainMenuItem)
                    {
                        if ((smoothListBoxMainList.Items[0] as MainMenuItem).TextLocation == string.Empty)
                        {
                            int floatingPcs = (smoothListBoxMainList.Items[0] as MainMenuItem).PieceCount;

                            if (floatingPcs >= m_checkedInPieces - summaryRow1.TotalCount)
                            {
                                (smoothListBoxMainList.Items[0] as MainMenuItem).PieceCount = floatingPcs - (m_checkedInPieces - summaryRow1.TotalCount);

                            }
                            else
                            {
                                CargoMatrix.UI.CMXMessageBox.Show("Unexpected error ", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            }
                        }

                    }
                }
                else
                {
                    if (smoothListBoxMainList.Items[0] is MainMenuItem)
                    {
                        if ((smoothListBoxMainList.Items[0] as MainMenuItem).TextLocation == string.Empty)
                        {
                            smoothListBoxMainList.RemoveItem(smoothListBoxMainList.Items[0]);
                        }
                    }
                
                }
            }
        
        }
        protected void AddNewCountModeRow(int number)
        {
            if (smoothListBoxMainList.ItemsCount > 0)
            {
                if (smoothListBoxMainList.Items[0] is ForkLiftItem)
                     
                {
                    //smoothListBoxMainList.Reset();
                    //(smoothListBoxMainList.Items[0] as IGridListItem).Visible = true;
                    smoothListBoxMainList.selectedItemsMap[smoothListBoxMainList.Items[0]] = true;
                    //(smoothListBoxMainList.Items[0] as IGridListItem).SelectedChanged(true);
                    
                    //(smoothListBoxMainList.Items[0] as MainMenuItem).countTextBox.Text = Convert.ToString(number);
                    int previousQuantity = (smoothListBoxMainList.Items[0] as IGridListItem).PieceCount;
                    (smoothListBoxMainList.Items[0] as IGridListItem).PieceCount = previousQuantity + number;
                    
                    
                }
                else
                {
                   
                    //if ((smoothListBoxMainList.Items[0] as MainMenuItem).TextLocation.ToUpper() != GridListBoxResources.Text_Forklift.ToUpper())
                    {
                        ForkLiftItem menuItem = new ForkLiftItem(m_houseBill.mode, number, false, _actionID);
                        
                        //menuItem.Validate_Quantity += new ForkLiftItem.ValidateQuantity(Validate_Quantity);
                        //menuItem.QuantityChanged += new EventHandler(Quantity_Changed);
                        smoothListBoxMainList.AddItemToFront(menuItem);
                        smoothListBoxMainList.Reset();
                        

                        smoothListBoxMainList.selectedItemsMap[menuItem] = true;
                        //menuItem.SelectedChanged(true);
                        //menuItem.countTextBox.Visible = true;
                        //menuItem.countTextBox.Text = Convert.ToString(number);
                        menuItem.labelPieceNo.Text = Convert.ToString(number);
                        //menuItem.countTextBox.SelectAll();
                        //menuItem.countTextBox.Focus();
                        int forkliftID = CargoMatrix.Communication.HostPlusIncomming.Instance.GetForkLiftID();
                        menuItem.LocationID = forkliftID;
                        menuItem.TextLocation = GridListBoxResources.Text_Forklift;
                        menuItem.LocationType = CargoMatrix.Communication.WSPieceScan.StorageTypes.Forklift;
                        smoothListBoxMainList.RefreshScroll();
                    }
                }
               

                
            }
            //else
            //{
            //    MainMenuItem menuItem = new MainMenuItem(m_houseBill.mode, number, false, _actionID);
                
            //    //menuItem.Validate_Quantity += new MainMenuItem.ValidateQuantity(Validate_Quantity);
            //    menuItem.QuantityChanged += new EventHandler(Quantity_Changed);
            //    //menuItem.UndoClicked += new EventHandler(Undo_Clicked);
            //    smoothListBoxMainList.Reset();

            //    smoothListBoxMainList.AddItem(menuItem);
                
            //    smoothListBoxMainList.selectedItemsMap[menuItem] = true;
            //    menuItem.SelectedChanged(true);
                
            //    menuItem.PieceCount = number; 
                
            //    smoothListBoxMainList.LayoutItems();
            
            //}

            
            
        }

        //private bool Validate_Quantity(MainMenuItem selectecItem, int prevQuantity, int newQuantity)
        //{
        //    int totalPcs = 0;
        //    int inProgessPieces = 0;
        //    for (int i = 0; i < smoothListBoxMainList.Items.Count; i++)
        //    { 
        //        if(smoothListBoxMainList.Items[i] is GridListItemBase)
        //        {
        //            if (selectecItem != (smoothListBoxMainList.Items[i] as GridListItemBase))
        //                totalPcs += (smoothListBoxMainList.Items[i] as GridListItemBase).PieceCount;
        //        }
                
        //    }
        //    if (smoothListBoxMainList.Items[0] is GridListItemBase)
        //    {
        //        if (/*(smoothListBoxMainList.Items[0] as MainMenuItem).TextLocation == string.Empty ||*/
        //            (smoothListBoxMainList.Items[0] as GridListItemBase).TextLocation.ToUpper() == GridListBoxResources.Text_Forklift.ToUpper())
        //        {
        //            inProgessPieces = (smoothListBoxMainList.Items[0] as MainMenuItem).PieceCount;
        //        }
            
        //    }
        //    int difference = totalPcs + newQuantity - inProgessPieces;// -prevQuantity;

        //    if (difference <= summaryRow1.TotalCount)
        //    {
        //        //summaryRow1.Progress += difference;
        //        return true;
        //    }
        //    CargoMatrix.UI.CMXMessageBox.Show("Quantity entered is more than actual number of pieces. You can enter \'" + (summaryRow1.TotalCount - totalPcs) + "\' pieces at the max.", "Invalid Quantity", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //    return false;
            
            
        //}
        public override void LoadControl()
        {
            //Application.DoEvents();
            Cursor.Current = Cursors.WaitCursor;
            //base.LoadControl();
            SuspendLayout();
            
            panelSoftkeys.Width = Width;
            panelSoftkeys.Top = Height - panelSoftkeys.Height - 1;

            

            //this.smoothListBoxMainList.progressBar1.Minimum = 0;
            //this.smoothListBoxMainList.progressBar1.Maximum = 100;
            //this.smoothListBoxMainList.progressBar1.Visible = true;
            //this.smoothListBoxMainList.progressBar1.Value = 10;
            //this.smoothListBoxMainList.progressBar1.Refresh();


            pictureBoxDetails.Image = GridListBoxResources.cc_zoom;
            horizontalSmoothListbox1.Visible = false;

            //smoothListBoxMainList.MultiSelectEnabled = true;
            //smoothListBoxMainList.UnselectEnabled = true;

            headerRow1.Width = Width;
            headerRow1.Left = 0;
            headerRow1.Top = panelHeader2.Height +  panelTitle.Height;

            //panelTitle.Visible = false;
            
            //panelTitle.Top = panel1.Top;

            horizontalSmoothListbox1.Width = Width;
            horizontalSmoothListbox1.Left = 0;
            horizontalSmoothListbox1.Top = panelSoftkeys.Top - horizontalSmoothListbox1.Height;

            summaryRow1.Width = Width;
            summaryRow1.Left = 0;
            summaryRow1.Top = horizontalSmoothListbox1.Top - summaryRow1.Height;
            

            //this.smoothListBoxMainList.progressBar1.Value = 20;
            //this.smoothListBoxMainList.progressBar1.Refresh();
            //this.Refresh();
            ResumeLayout();
            headerRow1.Refresh();
            summaryRow1.Refresh();
            panelSoftkeys.Refresh();
            //Application.DoEvents();
            Refresh();
            //TempLoad();

           
            smoothListBoxMainList.RefreshScroll();

            
            //m_houseBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHAWBSummary(Convert.ToInt32(m_houseBill.taskID), m_actionID);
            //m_pieceList = CargoMatrix.Communication.HostPlusIncomming.Instance.GetPieceList(Convert.ToInt32(m_houseBill.taskID), CargoMatrix.Communication.WSPieceScan.ScanStatuses.Scanned, null, m_actionID);


           // m_pieceList = new List<CargoMatrix.Communication.Data.PeiceItem>();



            //CargoMatrix.Communication.Data.PeiceItem item = CargoMatrix.Communication.HostPlusIncomming.Instance.GetPieceList(Convert.ToInt32(m_houseBill.actualBill), CargoMatrix.Communication.WSPieceScan.ScanStatuses.Scanned, null, 105);// new CargoMatrix.Communication.Data.PeiceItem();

            //item.locationName = "DOOR21";
            //item.locationRecID = 267;
            //item.pieceNo = 3;
            //item.RecID = 101;
            //m_pieceList.Add(item);

            //item = new CargoMatrix.Communication.Data.PeiceItem();
            //item.locationName = "DOOR22";
            //item.locationRecID = 268;
            //item.pieceNo = 3;
            //item.RecID = 102;
            //m_pieceList.Add(item);

            //item = new CargoMatrix.Communication.Data.PeiceItem();
            //item.locationName = "DOOR23";
            //item.locationRecID = 269;
            //item.pieceNo = 4;
            //item.RecID = 103;
            //m_pieceList.Add(item);

            //counterIcon.Value = 0;

            //summaryRow1.Progress = 2;
            
                
            //TitleText = m_masterBill.reference;
             //labelHouseBill.Text = m_houseBill.reference;
             //labelLine2.Text = m_houseBill.line2;

             //if (m_pieceList != null)
             //{
             //    foreach (CargoMatrix.Communication.Data.PeiceItem piece in m_pieceList)
             //    {
             //        AddRow((int)piece.pieceNo, piece.locationName, (int)piece.locationRecID, (int)piece.RecID);
             //    }
             //}
             //if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Piece && m_scannedPiece > 0)
             //{
             //    bool bAlreadyExist = false;
             //    foreach (Control control in smoothListBoxMainList.itemsPanel.Controls)
             //    {
             //        if (control is MainMenuItem)
             //        {
             //            if ((control as MainMenuItem).PieceCount == m_scannedPiece)
             //            {
             //                bAlreadyExist = true;
             //                CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
             //                CargoMatrix.UI.CMXMessageBox.Show("Scanned label is already in the list.", "Label already scanned", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
             //                break;
             //            }
             //        }
             //    }
             //    if (bAlreadyExist == false)
             //        AddNewPieceLevelRow(m_scannedPiece, CargoMatrix.Communication.WSPieceScan.ScanTypes.Automatic);
                 
             //}
             //UpdateSummary();


            Reload();

            //this.smoothListBoxMainList.progressBar1.Value = 100;
            //this.smoothListBoxMainList.progressBar1.Invalidate();
            //this.smoothListBoxMainList.progressBar1.Visible = false;
            Cursor.Current = Cursors.Default;
        }



        void AddNewPieceLevelRow(int peiceno, CargoMatrix.Communication.WSPieceScan.ScanTypes scanType)
        {
            MainMenuItem menuItem = new MainMenuItem(m_houseBill.mode, peiceno, false, _actionID);
            menuItem.RowRecID = -1;
            menuItem.ScanType = scanType;

            //menuItem.UndoClicked += new EventHandler(Undo_Clicked);
           // menuItem.Validate_Quantity += new MainMenuItem.ValidateQuantity(Validate_Quantity);
            menuItem.QuantityChanged += new EventHandler(Quantity_Changed);

           
            smoothListBoxMainList.AddItemToFront(menuItem);

            smoothListBoxMainList.selectedItemsMap[menuItem] = true;
            menuItem.SelectedChanged(true);
            
            menuItem.PieceCount = peiceno;
            smoothListBoxMainList.RefreshScroll();

            
        }

        protected void AddRow(int peiceno, string location, int locationID, Nullable<int> recID, CargoMatrix.Communication.WSPieceScan.StorageTypes locationType)
        {

            IGridListItem menuItem;
            if (locationType == StorageTypes.Forklift)
            {
                menuItem = new ForkLiftItem(m_houseBill.mode, peiceno, false, _actionID);
            }
            //else if (locationType == StorageTypes.ScreeningArea)
            //{
            //    menuItem = new ScreeningAreaItem(m_houseBill.mode, peiceno, false, _actionID);
            //}
            else
            {
                menuItem = new MainMenuItem(m_houseBill.mode, peiceno, false, _actionID);
                //(menuItem as MainMenuItem).UndoClicked += new EventHandler(Undo_Clicked);
                //menuItem.Validate_Quantity += new MainMenuItem.ValidateQuantity(Validate_Quantity);
                (menuItem as MainMenuItem).QuantityChanged += new EventHandler(Quantity_Changed);

            }
            menuItem.RowRecID = recID;
            menuItem.LocationID = locationID;
            menuItem.LocationType = locationType;
            menuItem.ActionID = _actionID;
            menuItem.HousebillRecID = Convert.ToInt32(m_houseBill.taskID);
           
            
            if (menuItem.LocationType == CargoMatrix.Communication.WSPieceScan.StorageTypes.Forklift)
            {
                smoothListBoxMainList.AddItemToFront(menuItem as UserControl);
            }
            else
            {
                smoothListBoxMainList.AddItem(menuItem as UserControl);
            }



            menuItem.PieceCount = peiceno;
            menuItem.TextLocation = location;
            //smoothListBoxMainList.MoveToEnd();
            //smoothListBoxMainList.LayoutItems();
            smoothListBoxMainList.RefreshScroll();

            //if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Piece) // piece mode
            //{
            //    summaryRow1.Progress++;
            //}
            //else
            //{
            //    summaryRow1.Progress += peiceno;
            //}
            
           
            
        }
        //void Undo_Clicked(object sender, EventArgs e)
        //{ 
        
        //}
        private void Quantity_Changed(object sender, EventArgs e)
        {
            UpdateSummary();
        }
        
        //private void DeleteRow_Click(object sender, EventArgs e)
        //{
            
        //    if ((sender as MainMenuItem).LocationID > 0)
        //    {
        //        switch (m_houseBill.mode)
        //        {
        //            case CargoMatrix.Communication.WSPieceScan.Modes.Piece:
        //                if (CargoMatrix.Communication.HostPlusIncomming.Instance.DeletePieceRow((sender as MainMenuItem).PieceCount, Convert.ToInt32(m_houseBill.taskID), _actionID) > 0)
        //                {
        //                    smoothListBoxMainList.RemoveItem(sender as Control);
        //                    UpdateSummary();
        //                }
        //                break;

        //            case CargoMatrix.Communication.WSPieceScan.Modes.Count:
        //                if (CargoMatrix.Communication.HostPlusIncomming.Instance.DeleteCountRow((sender as MainMenuItem).PieceCount, Convert.ToInt32(m_houseBill.taskID), _actionID, "", (sender as MainMenuItem).LocationID) > 0)
        //                {
        //                    smoothListBoxMainList.RemoveItem(sender as Control);
        //                    UpdateSummary();
        //                }
        //                break;
        //        }
        //    }
        //    else
        //    {
        //        smoothListBoxMainList.RemoveItem(sender as Control);
        //        UpdateSummary();
        //    }
            
        
        //}
        

        private void pictureBoxDetails_Click(object sender, EventArgs e)
        {
            
            if (m_bViewer == false)
                ShowViewer();
            else
                HideViewer();
        }

        private void ShowViewer()
        {
            if (m_bViewer)
                return;
            Cursor.Current = Cursors.WaitCursor;
            pictureBoxDetails.Image = GridListBoxResources.cc_zoom_over;
            pictureBoxDetails.Refresh();
            
            LoadHousebillViewer();
            if (isLoaded)
            {
                m_bViewer = true;
                horizontalSmoothListbox1.Visible = true;
                smoothListBoxMainList.Height -= horizontalSmoothListbox1.Height;
                summaryRow1.Top -= horizontalSmoothListbox1.Height;
                smoothListBoxMainList.RefreshScroll();
            }
            else
            {
                pictureBoxDetails.Image = GridListBoxResources.cc_zoom;
            
            }
            Cursor.Current = Cursors.Default;
        }
        protected void HideViewer()
        {
            if (!m_bViewer)
                return;
            currentPageIndex = -1;
            pictureBoxDetails.Image = GridListBoxResources.cc_zoom;
            m_bViewer = false;
            horizontalSmoothListbox1.Visible = false;
            horizontalSmoothListbox1.Reset();
            panelViewer.Visible = false;
            if (BarcodeEnabled == false)
                BarcodeEnabled = true;
            smoothListBoxMainList.Height += horizontalSmoothListbox1.Height;
            summaryRow1.Top += horizontalSmoothListbox1.Height;
            RemoveViewerAutoScroll();
            smoothListBoxMainList.RefreshScroll();
            
        
        }
        private void ShowViewerPage(Control control)
        {
            if (!m_bViewer)
            {
                ShowViewer();
                horizontalSmoothListbox1.Refresh();
            }

        
        }
        
        private void HideViewerPage()
        { 
        
        }
        private void LoadControl(string reference)
        { 
        
        }

        protected override void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true || currentPageIndex < 0)
                base.pictureBoxDown_MouseDown(sender, e);

            else
            {
                //if (currentPageIndex >= 0)
                {
                    m_Pages[currentPageIndex].Scroll(SmoothListBoxBase.DIRECTION.DOWN);

                    if (pictureBoxDown.Enabled)
                    {
                        DownButtonPressed(true);
                    }
                }

            }



        }
        protected override void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true || currentPageIndex < 0)
                base.pictureBoxDown_MouseUp(sender, e);

            else
            {
                //if (currentPageIndex >= 0)
                {
                    m_Pages[currentPageIndex].DownButtonPressed = false;

                    if (pictureBoxDown.Enabled)
                    {
                        DownButtonPressed(false);
                    }
                }

            }
        }

        protected override void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true || currentPageIndex < 0)
                base.pictureBoxUp_MouseDown(sender, e);

            else
            {
                //if (currentPageIndex >= 0)
                {
                    m_Pages[currentPageIndex].Scroll(SmoothListBoxBase.DIRECTION.UP);

                    if (pictureBoxDown.Enabled)
                    {
                        UpButtonPressed(true);
                    }
                }

            }
        }

        protected override void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true || currentPageIndex < 0)
                base.pictureBoxUp_MouseUp(sender, e);

            else
            {
                //if (currentPageIndex >= 0)
                {
                    m_Pages[currentPageIndex].UpButtonPressed = false;

                    if (pictureBoxUp.Enabled)
                    {
                        UpButtonPressed(false);
                    }
                }

            }

        }

        protected virtual void ApplyLocation(string location, int locID, CargoMatrix.Communication.WSPieceScan.StorageTypes locationType)
        {
            
            if (locID > 0)
            {
                
                
                
               
                if (smoothListBoxMainList.Items[0] is IGridListItem)
                {
                    CargoMatrix.Communication.WSPieceScan.ScanCountInfo[] items = new  ScanCountInfo[1];
                    items[0] = new ScanCountInfo();
                    items[0].LocationID = (smoothListBoxMainList.Items[0] as IGridListItem).LocationID;
                    items[0].NoOfPieces = (smoothListBoxMainList.Items[0] as IGridListItem).PieceCount;
                    int result = CargoMatrix.Communication.HostPlusIncomming.Instance.SetLocationInCount(items, _actionID,  Convert.ToInt32(m_houseBill.taskID), locID);
                        
                    if (result < 0)
                    {
                        Reload();
                        return;
                    }
                    
                    //foreach (Control temp in smoothListBoxMainList.Items)
                    //{
                    //    if (temp is MainMenuItem)
                    //        (temp as MainMenuItem).MovingComplete();
                    //}

                    AddRow(items[0].NoOfPieces, location, locID, null, locationType);
                    smoothListBoxMainList.RemoveItem(smoothListBoxMainList.Items[0]);



                }
              
                UpdateSummary();
                /////////////////////////////////////
                //////////////////////////////////////
            }
        
        }

        void MergeCountModeRows()
        {
            Dictionary<int,MainMenuItem> pieceLocationCollection = new Dictionary<int, MainMenuItem>();
            List<MainMenuItem> deletedItems = new List<MainMenuItem>();
            foreach (MainMenuItem control in smoothListBoxMainList.Items)
            {

                if (pieceLocationCollection.ContainsKey(control.LocationID))
                {
                    pieceLocationCollection[control.LocationID].PieceCount += control.PieceCount;
                    control.PieceCount = 0;
                    deletedItems.Add(control);

                }
                else
                {
                    pieceLocationCollection.Add(control.LocationID, control);
                }


                
            }

            foreach (MainMenuItem listItem in deletedItems)
            {
                smoothListBoxMainList.RemoveItem(listItem);
            }
            smoothListBoxMainList.LayoutItems();

            
        }
        //private void ApplyLocation(string location, string direction, int locID)
        //{

        //    if (locID <= 0)
        //    {
        //        if (CargoMatrix.UI.CMXMessageBox.Show("Location Does not exist, would you like to add this location?", "Location not found", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.YesNo, DialogResult.Yes) == DialogResult.OK)
        //        {
        //            if (m_manualLocationForm.ShowDialog() == DialogResult.OK)
        //                locID = CargoMatrix.Communication.HostPlusIncomming.Instance.AddNewLocation(location, direction);
        //        }
        //        else
        //            return;
        //    }
        //    else
        //    {
        //        locID = CargoMatrix.Communication.HostPlusIncomming.Instance.GetGatwayLocationID(location);
            
        //    }

        //    if (locID <= 0)
        //        return;
        //    //if (locID < 0)
        //    //{
        //    //    for (int i = 0; i < smoothListBoxMainList.SelectedItems.Count; i++)
        //    //    {
        //    //        if (smoothListBoxMainList.SelectedItems[i] is MainMenuItem)
        //    //        {
        //    //            (smoothListBoxMainList.SelectedItems[i] as MainMenuItem).TextLocation = location;


        //    //            UpdateSummary();
        //    //        }

        //    //    }
        //    //}

            
            
        //}
        //private void MainMenu_BarcodeReadNotify(string barcodeData)
        //{
        //    //CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
        //    //CargoMatrix.Communication.WSPieceScan.BarCode barcodeObj = CargoMatrix.Communication.HostPlusIncomming.Instance.ParseBarcode(barcodeData);
        //    //if (barcodeObj != null)
        //    //{
        //    //    switch (barcodeObj.Type)
        //    //    {
        //    //        case CargoMatrix.Communication.WSPieceScan.Types.Area:

        //    //        case CargoMatrix.Communication.WSPieceScan.Types.Door:
        //    //            if (smoothListBoxMainList.SelectedItems.Count > 0)
        //    //            {
        //    //                if (barcodeObj.LocationID > 0)
        //    //                    ApplyLocation(barcodeObj.Location, barcodeObj.LocationID);  // uses one less check
        //    //                else
        //    //                {
        //    //                    if (CargoMatrix.UI.CMXMessageBox.Show("Location: " + barcodeObj.Location + " does not exist in the system. Would you like to add this location?", "Add Location", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
        //    //                    {
        //    //                        NewLocationForm newLocation = new NewLocationForm();
        //    //                        newLocation.TextLocation = barcodeObj.Location;
        //    //                        if (newLocation.ShowDialog() == DialogResult.OK)
        //    //                        {
        //    //                            if (newLocation.LocationID > 0)
        //    //                            {
        //    //                                ApplyLocation(barcodeObj.Location, newLocation.LocationID);
        //    //                            }
        //    //                        }
        //    //                        newLocation.Dispose();
        //    //                    }
                            
        //    //                }

        //    //            }
        //    //            else
        //    //            {
        //    //                CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
        //    //                CargoMatrix.UI.CMXMessageBox.Show("No item has been selected. Select item before scanning location", "No item selected", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //    //            }
                       
        //    //            break;
        //    //        case CargoMatrix.Communication.WSPieceScan.Types.HouseBill:
        //    //            if (Convert.ToInt32(m_masterBill.taskID) == barcodeObj.MasterBillID)
        //    //            {
        //    //                if (Convert.ToInt32(m_houseBill.taskID) == barcodeObj.HouseBillID)
        //    //                {
        //    //                    if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Piece)
        //    //                    {
        //    //                        bool bAlreadyExist = false;
        //    //                        foreach (Control control in smoothListBoxMainList.itemsPanel.Controls)
        //    //                        {
        //    //                            if (control is MainMenuItem)
        //    //                            {
        //    //                                if ((control as MainMenuItem).PieceCount == barcodeObj.PieceNo)
        //    //                                {
        //    //                                    bAlreadyExist = true;
        //    //                                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
        //    //                                    CargoMatrix.UI.CMXMessageBox.Show("Scanned label is already in the list.", "Label already scanned", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //    //                                    break;
        //    //                                }
        //    //                            }
        //    //                        }
        //    //                        if (bAlreadyExist == false)
        //    //                            AddNewPieceLevelRow(barcodeObj.PieceNo, CargoMatrix.Communication.WSPieceScan.ScanTypes.Automatic);

        //    //                    }
        //    //                }
        //    //                else
        //    //                { 
        //    //                    // same masterbill but different housebill
        //    //                    if (CargoMatrix.UI.CMXMessageBox.Show("You have scanned different housebill, would you like to switch from " + m_houseBill.reference + ", to HAWB: " + barcodeObj.HouseBillNo, "Different Housebill", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
        //    //                    {
        //    //                        m_houseBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHAWBSummary(barcodeObj.HouseBillID, m_actionID);
        //    //                        Reload();
                                
        //    //                    }
        //    //                }
        //    //            }
        //    //            else
        //    //            { 
        //    //                // housebill belongs to different masterbill
        //    //                if (CargoMatrix.UI.CMXMessageBox.Show("Housebill belongs different Masterbill, would you like to switch from " + m_masterBill.reference + ", to MB:" + barcodeObj.MasterBillNo, "Different Consol", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
        //    //                {
        //    //                    m_masterBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetMAWBSummary(barcodeObj.MasterBillID);
        //    //                    //CargoMatrix.UI.CMXAnimationmanager.GoBack();

        //    //                    m_houseBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHAWBSummary(barcodeObj.HouseBillID, m_actionID);

        //    //                    CargoMatrix.UI.CMXUserControl control = CargoMatrix.UI.CMXAnimationmanager.GetPreviousControl();

        //    //                    if (control is HAWBList)
        //    //                    {
        //    //                        (control as HAWBList).MAWB = m_masterBill;
                                    
        //    //                    }
        //    //                    //else
        //    //                    //{
        //    //                    //    //CargoMatrix.UI.CMXMessageBox.Show("An unexpected error has occured in loading Consol: " + barcodeObj.MasterBillNo, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //    //                    //    m_masterBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetMAWBSummary(barcodeObj.MasterBillID);
                                    

        //    //                    //}
        //    //                    Reload();
                               
        //    //                }
                        
        //    //            }


        //    //            break;
        //    //        case CargoMatrix.Communication.WSPieceScan.Types.MasterBill:
        //    //            if (Convert.ToInt32(m_masterBill.taskID) != barcodeObj.MasterBillID)
        //    //            {
        //    //                if (CargoMatrix.UI.CMXMessageBox.Show("You have scanned to different Masterbill, would you like to switch from " + m_masterBill.reference + ", to Consol: " + barcodeObj.MasterBillNo, "Different Consol", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
        //    //                {
        //    //                    m_masterBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetMAWBSummary(barcodeObj.MasterBillID);
        //    //                    CargoMatrix.UI.CMXUserControl control = CargoMatrix.UI.CMXAnimationmanager.GetPreviousControl();

        //    //                    if (control is HAWBList)
        //    //                    {
        //    //                        (control as HAWBList).MAWB = m_masterBill;
        //    //                        CargoMatrix.UI.CMXAnimationmanager.GoBack();
        //    //                    }
        //    //                    else
        //    //                    {
        //    //                        //CargoMatrix.UI.CMXMessageBox.Show("An unexpected error has occured in loading Consol: " + barcodeObj.MasterBillNo, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //    //                        m_masterBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetMAWBSummary(barcodeObj.MasterBillID);
        //    //                        //LoadHAWBList(m_masterBill);
                                
        //    //                    }
                                
        //    //                }
        //    //            }

        //    //            else
        //    //            {
        //    //                //CargoMatrix.UI.CMXAnimationmanager.GoBack();
        //    //                CargoMatrix.UI.CMXUserControl control = CargoMatrix.UI.CMXAnimationmanager.GetPreviousControl();

        //    //                if (control is HAWBList)
        //    //                {
        //    //                    CargoMatrix.UI.CMXAnimationmanager.GoBack();
        //    //                }
        //    //                else
        //    //                {
        //    //                    //CargoMatrix.UI.CMXMessageBox.Show("An unexpected error has occured in loading Consol: " + barcodeObj.MasterBillNo, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
        //    //                    m_masterBill = CargoMatrix.Communication.HostPlusIncomming.Instance.GetMAWBSummary(barcodeObj.MasterBillID);
        //    //                    //LoadHAWBList(m_masterBill);

        //    //                }
        //    //            }

        //    //            break;
        //    //        case CargoMatrix.Communication.WSPieceScan.Types.Truck:
        //    //            break;
        //    //        case CargoMatrix.Communication.WSPieceScan.Types.Tracking:
        //    //            break;
        //    //        case CargoMatrix.Communication.WSPieceScan.Types.User:
        //    //            break;
        //    //    }

        //    //}

        //    //ReArrangeRows();

        //    //barcode.StartRead();
        //    //UpdateSummary();
            
        //}
        //private void LoadHAWBList(CargoMatrix.Communication.Data.TaskItem masterBillItem)
        //{
        //    if (m_housebillList != null)
        //    {
        //        m_housebillList.Dispose();
        //        m_housebillList = null;
        //    }
        //    int actionID = CargoMatrix.Utilities.ActonIDManager.GetActionID(m_actionID, (CargoMatrix.Communication.WSPieceScan.EnumDirections)masterBillItem.direction);
        //    m_housebillList = new HAWBList(masterBillItem, actionID, false, 5);
        //    m_housebillList.Location = this.Location;
        //    m_housebillList.Size = this.Size;
        //    CargoMatrix.UI.CMXAnimationmanager.GoBack(m_housebillList);



        //}
        protected void ReArrangeRows()
        {
            bool needLayout = false;
            if (smoothListBoxMainList.itemsPanel.Controls.Count > 0)
            {
                for (int i = smoothListBoxMainList.itemsPanel.Controls.Count - 1; i >= 0; i--)
                {
                    if (smoothListBoxMainList.itemsPanel.Controls[i] is MainMenuItem)
                    {
                        if ((smoothListBoxMainList.itemsPanel.Controls[i] as MainMenuItem).TextLocation == null || (smoothListBoxMainList.itemsPanel.Controls[i] as MainMenuItem).TextLocation == "")
                        {
                            smoothListBoxMainList.MoveItemToTop(smoothListBoxMainList.itemsPanel.Controls[i]);
                            needLayout = true;
                        }
                    }
                
                }
            }
            //foreach (Control itemcontrol in smoothListBoxMainList.itemsPanel.Controls)
            //{
            //    if (itemcontrol is MainMenuItem)
            //    {
            //        if ((itemcontrol as MainMenuItem).TextLocation == null || (itemcontrol as MainMenuItem).TextLocation == "")
            //        {
            //            smoothListBoxMainList.MoveItemToTop(itemcontrol);
            //            needLayout = true;
            //        }
            //    }
            //}
            if (needLayout)
                smoothListBoxMainList.LayoutItems();
            
        }
        //private void MainMenu_LoadOptionsMenu(object sender, EventArgs e)
        //{
        //    switchMode.Picture = FreightHandlerResources.Shipping_Routes;
        //    AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        //    AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.PRINT_HAWB_LABEL));
        //    //AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.SELECT_ALL));
        //    //AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.SELECT_NONE));
        //    //AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REMOVE_ALL));
        //    //AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.MAIN_MENU));
            
        //    AddOptionsListItem(manualLocationOptionItem);
            
        //    //manualPieceEntryOptionItem.Enabled = false;
        //    AddOptionsListItem(manualPieceEntryOptionItem);
        //    //AddOptionsListItem(switchMode);
        //    AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.LOGOUT));

        //}

       
        //private void MainMenu_MenuItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        //{
        //    if (listItem is CustomListItems.OptionsListITem)
        //    {
        //        switch ((listItem as CustomListItems.OptionsListITem).ID)
        //        {
        //            case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
        //                smoothListBoxMainList.RemoveAll();
        //                m_pieceList = CargoMatrix.Communication.HostPlusIncomming.Instance.GetPieceList(Convert.ToInt32(m_houseBill.taskID), CargoMatrix.Communication.WSPieceScan.ScanStatuses.Scanned, null, m_actionID);

        //                if (m_pieceList != null)
        //                {
        //                    foreach (CargoMatrix.Communication.Data.PeiceItem piece in m_pieceList)
        //                    {
        //                        AddRow((int)piece.pieceNo, piece.locationName, (int)piece.locationRecID, (int)piece.RecID);
        //                    }
        //                }
        //                //LoadHousebillViewer();

        //                UpdateSummary();
        //                break;
        //            case CustomListItems.OptionsListITem.OptionItemID.SELECT_ALL:
        //                smoothListBoxMainList.SelectAll();
        //                break;
        //            case CustomListItems.OptionsListITem.OptionItemID.SELECT_NONE:
        //                smoothListBoxMainList.SelectNone();
        //                break;
        //            case CustomListItems.OptionsListITem.OptionItemID.REMOVE_ALL:
        //                if (CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to clear the list?" , "Confirm Delete All", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
        //                    smoothListBoxMainList.RemoveAll();
        //                UpdateSummary();
        //                break;
        //            //case CustomListItems.OptionsListITem.OptionItemID.MAIN_MENU:
        //            //    break;
        //            case CustomListItems.OptionsListITem.OptionItemID.MANUAL_LOCATION:
        //                if (m_manualLocationForm == null)
        //                {
        //                    Cursor.Current = Cursors.WaitCursor;
        //                    m_manualLocationForm = new ManualLocationForm();
        //                    Cursor.Current = Cursors.Default;
        //                }
        //                BarcodeEnabled = false;
        //                if (m_manualLocationForm.ShowDialog() == DialogResult.OK)
        //                {
        //                    //string locDir = null;
        //                    //switch (m_manualLocationForm.Direction)
        //                    //{
        //                    //    case 0:
        //                    //        locDir = "Import";
        //                    //        break;
        //                    //    case 1:
        //                    //        locDir = "Export";
        //                    //        break;
        //                    //    case 2:
        //                    //        locDir = "";
        //                    //        break;
        //                    //}
        //                    ApplyLocation(m_manualLocationForm.TextLocation, m_manualLocationForm.LocationID);
        //                    //int locID = CargoMatrix.Communication.HostPlusIncomming.Instance.GetGatwayLocationID(m_manualLocationForm.TextLocation);
        //                    //if (locID < 0)
        //                    //{
        //                    //    locID = CargoMatrix.Communication.HostPlusIncomming.Instance.AddNewLocation(m_manualLocationForm.TextLocation, locDir);

        //                    //}

        //                    //for (int i = 0; i < smoothListBoxMainList.SelectedItems.Count; i++)
        //                    //{
        //                    //    if (smoothListBoxMainList.SelectedItems[i] is MainMenuItem)
        //                    //    {
        //                    //        (smoothListBoxMainList.SelectedItems[i] as MainMenuItem).TextLocation = m_manualLocationForm.TextLocation;
                                   
                                    
        //                    //        UpdateSummary();
        //                    //    }
                            
        //                    //}



        //                }
        //                BarcodeEnabled = true;
        //                break;
        //            case CustomListItems.OptionsListITem.OptionItemID.MANUAL_PEICE_ENTRY:

                        
        //                    BarcodeEnabled = false;
                            
        //                    if (manualPieceEntryForm == null)
        //                    {
        //                        manualPieceEntryForm = new CargoMatrix.Utilities.MesageListBox();// ManualPieceEntry();
        //                        manualPieceEntryForm.LoadFormEvent += new CargoMatrix.Utilities.LoadFormHandler(manualPieceEntryForm_LoadFormEvent);
        //                    }

        //                    manualPieceEntryForm.HeaderText = "Manual Piece Entry";
        //                    manualPieceEntryForm.HeaderText2 = "Select Pieces below:";
        //                    if (manualPieceEntryForm.ShowDialog() == DialogResult.OK)
        //                    {
        //                        List<Control> tempList = manualPieceEntryForm.SelectedItems;

        //                        int[] pieces = new int[tempList.Count];// = manualPieceEntryForm.SelectedPieces;
        //                        for (int i = 0; i < tempList.Count; i++)
        //                        {
        //                            pieces[i] = (tempList[i] as SmoothListbox.ListItems.StandardListItem).ID;
        //                        }
        //                        if (pieces != null)
        //                        {
        //                            for (int i = 0; i < pieces.Length; i++)
        //                            {
        //                                AddNewPieceLevelRow(pieces[i], CargoMatrix.Communication.WSPieceScan.ScanTypes.Manual);
        //                            }
        //                        }
        //                    }
        //                    BarcodeEnabled = true;

        //                break;
        //            case CustomListItems.OptionsListITem.OptionItemID.UNKNOWN:

        //                if (CargoMatrix.Utilities.SupervisorAuthorizationMessageBox.Show(this, switchMode.title.Text) == DialogResult.OK)
        //                {
        //                    if (m_houseBill.mode == CargoMatrix.Communication.WSPieceScan.Modes.Piece)
        //                    {
        //                        if (CargoMatrix.Communication.HostPlusIncomming.Instance.SwitchToCountMode(Convert.ToInt32(m_houseBill.taskID), m_actionID,
        //                            CargoMatrix.Utilities.SupervisorAuthorizationMessageBox.SupervisorPIN))
        //                        {
        //                            switchMode.title.Text = "Switch to Piece Mode";
        //                            m_houseBill.mode = CargoMatrix.Communication.WSPieceScan.Modes.Count;
        //                            Reload();
        //                        }

        //                    }
        //                    else
        //                    {
        //                        if (CargoMatrix.Communication.HostPlusIncomming.Instance.SwitchToPieceMode(Convert.ToInt32(m_houseBill.taskID), m_actionID))
        //                        {
        //                            switchMode.title.Text = "Switch to Count Mode";
        //                            m_houseBill.mode = CargoMatrix.Communication.WSPieceScan.Modes.Piece;
        //                            Reload();
        //                        }
                            
        //                    }
        //                }

        //                break;
        //            case CustomListItems.OptionsListITem.OptionItemID.PRINT_HAWB_LABEL:
        //                break;
        //            case CustomListItems.OptionsListITem.OptionItemID.LOGOUT:
        //                if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
        //                    CargoMatrix.UI.CMXAnimationmanager.DoLogout();
        //                break;
        //        }
        //    }

        //}

        //List<Control> manualPieceEntryForm_LoadFormEvent(Form MessageListBox)
        //{
        //    if (manualPieceEntryForm == null)
        //    {
        //        manualPieceEntryForm = new CargoMatrix.Utilities.MessageListBox();// ManualPieceEntry();
        //        manualPieceEntryForm.LoadFormEvent += new CargoMatrix.Utilities.LoadFormHandler(manualPieceEntryForm_LoadFormEvent);
        //    }

        //    List<int> remainingPieces = new List<int>();// = smoothListBoxMainList.SelectedItems;
        //    //List<int> tempPieces = new List<int>();
        //    int[] tempPieces = new int[(int)m_houseBill.totalPieces];

        //    foreach (Control key in smoothListBoxMainList.itemsPanel.Controls)
        //    {
        //        //if (smoothListBoxMainList..selectedItemsMap[key])
        //        {
        //            if (key is MainMenuItem)
        //            {
        //                //tempPieces.Add((key as MainMenuItem).PieceCount);
        //                tempPieces[(key as MainMenuItem).PieceCount - 1] = 1;

        //            }

        //        }
        //    }
        //    for (int i = 0; i < m_houseBill.totalPieces; i++)
        //    {
        //        if (tempPieces[i] != 1)
        //            remainingPieces.Add(i + 1);
        //    }
        //    List<Control> list = new List<Control>();

        //    for (int i = 0; i < remainingPieces.Count; i++)
        //    {
        //        list.Add(new SmoothListbox.ListItems.StandardListItem("Piece: " + remainingPieces[i], null, remainingPieces[i]));
        //    }
        //    return list;
            
        //    //manualPieceEntryForm.HousebillName = m_houseBill.actualBill;
        //    //manualPieceEntryForm.remainingPieces = remainingPieces.ToArray();
            
        //}

        protected void Reload()
        {
            smoothListBoxMainList.RemoveAll();
            TitleText = m_houseBill.reference;// m_masterBill.reference;
            labelHouseBill.Text = m_houseBill.line2; ;// m_houseBill.reference;
            labelLine2.Text = m_houseBill.line3;// m_houseBill.line2;
            summaryRow1.Mode = m_houseBill.mode;
            summaryRow1.TotalCount = (int) m_houseBill.totalPieces;
            m_pieceList = CargoMatrix.Communication.HostPlusIncomming.Instance.GetPieceList(Convert.ToInt32(m_houseBill.taskID), CargoMatrix.Communication.WSPieceScan.EnumStatus.All, 0, _actionID);

            if (m_pieceList != null)
            {
                for (int i = 0; i < m_pieceList.Length; i++)
                {
                    AddRow((int)m_pieceList[i].Piece, m_pieceList[i].Lane, (int)m_pieceList[i].LocationID,null, m_pieceList[i].LocationType);
                }
                //foreach (CargoMatrix.Communication.Data.PeiceItem piece in m_pieceList)
                //{
                   
                    
                //    AddRow((int)piece.pieceNo, piece.locationName, (int)piece.locationRecID, (int)piece.RecID, piece.locationType);
                //}
            }
            //LoadHousebillViewer();

            UpdateSummary();
        }

        private void panel1_EnabledChanged(object sender, EventArgs e)
        {
            summaryRow1.Visible =
                panel1.Visible =
                headerRow1.Visible =
                horizontalSmoothListbox1.Visible = Enabled;
               
            //Visible = Enabled;
        }








        #region Selcetion
        
        int mousemovedelta = 0;
        protected int scrollStage = 0;
        protected Point mouseDownPoint = Point.Empty;
        protected Point previousPoint = Point.Empty;
        protected bool renderLockFlag = false;
        protected float scrollBoostDuration = 2.0f;
        protected float scrollBoostFactor = 2.0f;
        protected bool mouseIsDown = false;
        protected int draggedDistance = 0;
        public string m_titleText;
        protected int ClickArea = 15;

        
        
        /// <summary>
        /// Handles mouse down events by storing a set of <c>Point</c>s that
        /// will be used to determine animation velocity.
        /// </summary>
        protected void MouseDownHandler(object sender, MouseEventArgs e)
        {
            if (IsSelectable)
            {
                if (e.Button == MouseButtons.Left)
                {
                    mouseIsDown = true;
                    // Since list items move when scrolled all locations are 
                    // in absolute values (meaning local to "this" rather than to "sender".
                    //mouseDownPoint = Utils.GetAbsolute(new Point(e.X, e.Y), sender as Control, this);
                    mouseDownPoint.X = e.X;
                    mouseDownPoint.Y = e.Y;
                    mouseDownPoint = (sender as Control).PointToScreen(mouseDownPoint);
                    //previousPoint = new Point(mouseDownPoint.X, mouseDownPoint.Y);
                    FocusListItem(sender, true);
                }
            }
        }

        /// <summary>
        /// Handles mouse move events by scrolling the moved distance.
        /// </summary>
        /// 
        protected virtual void MouseMoveHandler(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                // The lock flag prevents too frequent rendering of the 
                // controls, something which becomes an issue of Devices
                // because of their limited performance.
                Point absolutePoint = new Point();
                //Point absolutePoint = Utils.GetAbsolute(new Point(e.X, e.Y), sender as Control, this);


                absolutePoint.X = e.X;
                absolutePoint.Y = e.Y;
                absolutePoint = (sender as Control).PointToScreen(absolutePoint);

                  //  if (((absolutePoint.X - ClickArea < mouseDownPoint.X) && (mouseDownPoint.X < absolutePoint.X + ClickArea)) &&
                  //((absolutePoint.Y - ClickArea < mouseDownPoint.Y) && (mouseDownPoint.Y < absolutePoint.Y + ClickArea)) 
                /*&&velocity == 0*/
                if (Math.Abs(absolutePoint.X - mouseDownPoint.X) < ClickArea && Math.Abs(absolutePoint.Y - mouseDownPoint.Y) < ClickArea)
                {
                    return;
                }
                else
                {
                    FocusListItem(sender, false);
                    mouseIsDown = false;
                }
                
           }
        }

        protected void FocusListItem(object sender, bool focus)
        {

            // Get the list item (regardless if it was a child Control that was clicked). 
            //Control item = GetListItemFromEvent(sender as Control);
            //if (item != null)
            //{
            //    // After "normal" selection rules have been applied,
            //    // check if the list items affected are IExtendedListItems
            //    // and call the appropriate methods if it is so.
            //    //foreach (Control listItem in itemsPanel.Controls)
            //    //{
            //    //    if (listItem is IExtendedListItem)
            //    //        (listItem as IExtendedListItem).Focus(false);
            //    //}
            //    if (m_prevFocused != null)
            //        m_prevFocused.Focus(false);

            //    if (item is IExtendedListItem)
            //    {
            //        item.SuspendLayout();
            //        (item as IExtendedListItem).Focus(focus);
            //        item.ResumeLayout();
            //        m_prevFocused = item as IExtendedListItem;
            //    }

            //    // Force a re-layout of all items

            //    LayoutItems();
            //    //Application.DoEvents();
            //    Refresh();
            //}
        }

        /// <summary>
        /// Handles the mouse up event and determines if the list needs to animate 
        /// after it has been "released".
        /// </summary>
        protected void MouseUpHandler(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && mouseIsDown)
            {
                // If the mouse was lifted from the same location it was pressed down on 
                // then this is not a drag but a click, do item selection logic instead
                // of dragging logic.
                //Point tempPoint = new Point();
                //tempPoint.X = e.X;
                //tempPoint.Y = e.Y;
                //tempPoint = (sender as Control).PointToScreen(tempPoint);
                //tempPoint = GetAbsolute(new Point(e.X, e.Y), sender as Control, this);
                //if (((tempPoint.X - 10 < mouseDownPoint.X) && (mouseDownPoint.X < tempPoint.X + 10)) &&
                //   ((tempPoint.Y - 10 < mouseDownPoint.Y) && (mouseDownPoint.Y < tempPoint.Y + 10)) /*&&
                //    velocity == 0*/
                //                  )
                LayoutItems();
                //Application.DoEvents();
                Refresh();
                pictureBoxDetails_Click(null,null);
            }
            mouseIsDown = false;
        }
        public static Point GetAbsolute(Point point, Control sourceControl, Control rootControl)
        {
            Point tempPoint = new Point();
            for (Control iterator = sourceControl; iterator != rootControl; iterator = iterator.Parent)
            {
                tempPoint.Offset(iterator.Left, iterator.Top);
            }

            tempPoint.Offset(point.X, point.Y);
            return tempPoint;
        }
        #endregion
    }
}
