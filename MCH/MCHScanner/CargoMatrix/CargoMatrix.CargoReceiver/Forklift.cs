﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WSCargoReceiver;

namespace CargoMatrix.CargoReceiver
{
    //internal enum Mode { Import, Domestic, OnHand, NA }
    internal partial class Forklift : CargoMatrix.Utilities.MessageListBox
    {
        private static Forklift instance;
        public event EventHandler StatusChanged;
        private TransactionStatus _status;
        private bool isLoaded;

        public TransactionStatus Status
        {
            get { return _status; }
            internal set
            {
                _status = value;
                if (StatusChanged != null)
                    StatusChanged(null, null);
            }
        }
        public string Summary
        {
            get
            {
                return string.Format("Pcs: {0} of {1} | {2}%",
                                  Status.TotalProcessed,
                                  Status.TotalCount,
                                  (int)Status.TaskProgress);
            }
        }
        public int ItemCount
        {
            get { return Status.TransactionRecords; }
        }
        public static Forklift Instance
        {
            get
            {
                if (instance == null)
                    instance = new Forklift();

                if (instance.isLoaded == false)
                    instance.loadAppSettings();

                return instance;
            }
        }

        public static bool IsLoaded
        {
            get { return instance.isLoaded; }
        }

        public string FirstScannedHousebill { get; set; }
        public CurrentHawb CurrentHousebill { get; set; }
        public MasterBillDirection ReceiveMode { get; internal set; }
        public int ForkliftID
        { get; private set; }
        private MasterBillItem _mawb;
        public MasterBillItem Mawb
        {
            get { return _mawb; }
            set
            {
                _mawb = value;
                if (_mawb != null)
                    ReceiveMode = _mawb.Direction;
                else
                    ReceiveMode = MasterBillDirection.NA;
            }
        }
        //public PartItem SplitPart { get; set; }
        internal bool ChangeHawbModeAllowed { get; private set; }
        public bool AllowMultipleHawbOnForkLift { get; private set; }


        private Forklift()
        {
            InitializeComponent();
            smoothListBoxBase1.MultiSelectEnabled = true;
            this.HeaderText = "Forklift View";
            this.HeaderText2 = CargoReceiverResources.Text_SelectPieces;
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(Forklift_LoadListEvent);

        }

        private void loadAppSettings()
        {
            var settings = CargoMatrix.Communication.CargoReceiver.Instance.GetTaskSettings();

            if (settings.Transaction.TransactionStatus1 == true)
            {
                instance.isLoaded = true;
                this.ChangeHawbModeAllowed = settings.ChangeHawbScanModeAllowed;
                this.AllowMultipleHawbOnForkLift = settings.AllowMultipleHawbOnForkLift;
                this.ForkliftID = settings.ForkliftId;
                this.Status = new TransactionStatus()
                {
                    TransactionRecords = settings.Transaction.TransactionRecords,

                    TotalProcessed = settings.Transaction.TotalProcessed,
                    TotalCount = settings.Transaction.TotalCount,
                    TaskProgress = settings.Transaction.TaskProgress
                };
                CurrentHousebill = new CurrentHawb();
                CurrentHousebill.SetHawb(string.Empty, string.Empty);
            }
            else
            {
                instance.isLoaded = false;
                CargoMatrix.UI.CMXMessageBox.Show("Error has occured while downloading task settings. Refresh or restart the application", "Unable to connect", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }

        void Forklift_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            PopulateForkliftContent();
        }


        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            if (DialogResult.OK == confirmLoc.ShowDialog())
            {
                var barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                if (barcode.BarcodeType == CMXBarcode.BarcodeTypes.Area || barcode.BarcodeType == CMXBarcode.BarcodeTypes.Door || barcode.BarcodeType == CMXBarcode.BarcodeTypes.ScreeningArea)
                    DropForkliftManually(barcode.Location);
                else
                    CargoMatrix.UI.CMXMessageBox.Show(CargoReceiverResources.Text_InvalidBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
            //PopulateForkliftContent();
        }

        private void DropForkliftManually(string locName)
        {
            if (Forklift.instance.AllowMultipleHawbOnForkLift == false && Instance.CurrentHousebill != null)
                if (!string.IsNullOrEmpty(Forklift.Instance.CurrentHousebill.suggestedDropLoc) && !string.Equals(Forklift.Instance.CurrentHousebill.suggestedDropLoc, locName, StringComparison.OrdinalIgnoreCase))
                {
                    string msg = string.Format(CargoReceiverResources.Text_confirmNotSuggestedLoc, CurrentHousebill.hawbNo, CurrentHousebill.suggestedDropLoc, locName);
                    if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Location", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                        return;
                }

            List<PieceItem> pieces = new List<PieceItem>();
            foreach (var item in smoothListBoxBase1.Items)
            {
                if (item is CustomListItems.ComboBox)
                {
                    foreach (var pieceId in (item as CustomListItems.ComboBox).SelectedIds)
                        pieces.Add(new PieceItem() { HouseBillId = (item as CustomListItems.ComboBox).ID, PieceId = pieceId });
                }
            }
            var status = CargoMatrix.Communication.CargoReceiver.Instance.DropPiecesIntoLocation(pieces.ToArray<PieceItem>(), Mawb.TaskId, locName, ForkliftID);

            if (!status.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            else
                Status = status;
            PopulateForkliftContent();
            DialogResult = DialogResult.OK;
        }
        internal bool DropForklift(string location)
        {
            if (Forklift.instance.AllowMultipleHawbOnForkLift == false && Instance.CurrentHousebill != null)
                if (!string.IsNullOrEmpty(Forklift.Instance.CurrentHousebill.suggestedDropLoc) && !string.Equals(Forklift.Instance.CurrentHousebill.suggestedDropLoc, location, StringComparison.OrdinalIgnoreCase))
                {
                    string msg = string.Format(CargoReceiverResources.Text_DropConfirmation, CurrentHousebill.hawbNo, CurrentHousebill.suggestedDropLoc, location);
                    if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Location", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                        return false;
                }

            string mesage = string.Format(CargoReceiverResources.Text_ScanConfirmation, Forklift.Instance.ItemCount, location);
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(mesage, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OKCancel, DialogResult.OK))
            {
                return false;
            }
            TransactionStatus status = CargoMatrix.Communication.CargoReceiver.Instance.DropForkliftIntoLocation(Forklift.Instance.Mawb.TaskId, location, Forklift.Instance.ForkliftID);
            if (!status.TransactionStatus1)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                return false;
            }
            else
            {
                Status = status;
                return true;
            }
        }
        private void PopulateForkliftContent()
        {

            smoothListBoxBase1.RemoveAll();
            foreach (var hawb in CargoMatrix.Communication.CargoReceiver.Instance.GetForkLiftHouseBills(ForkliftID, Mawb.TaskId))
            {
                List<CustomListItems.ComboBoxItemData> pieces = new List<CustomListItems.ComboBoxItemData>();

                foreach (var hawbPiece in hawb.Pieces)
                {
                    pieces.Add(new CustomListItems.ComboBoxItemData(string.Format("Piece {0}", hawbPiece.PieceNumber), "Loc " + hawbPiece.Location, hawbPiece.PieceId));
                }
                Image icon;
                string line2;
                if (hawb.ScanMode == CargoMatrix.Communication.WSCargoReceiver.ScanModes.Piece)
                {
                    icon = CargoMatrix.Resources.Skin.PieceMode;
                    line2 = string.Format("Pieces: {0} of {1}", hawb.Pieces.Length, hawb.TotalPieces);
                }
                else
                {
                    icon = CargoMatrix.Resources.Skin.countMode;
                    line2 = string.Format("Pieces: {0} of {1}", hawb.Pieces.Length, hawb.TotalPieces);
                }
                string title = hawb.HousebillNumber;
                if (!string.IsNullOrEmpty(hawb.DropLocation))
                    title += " (" + hawb.DropLocation + ")";

                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawb.HousebillId, title, line2, icon, pieces);
                combo.ContainerName = "Forklift";
                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(combo_LayoutChanged);
                combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                smoothListBoxBase1.AddItem(combo);
            }

            if (smoothListBoxBase1.ItemsCount == 0)
                this.DialogResult = DialogResult.Cancel;
        }


        void combo_LayoutChanged(CustomListItems.ComboBox sender, int subItemId, bool comboIsEmpty)
        {
            if (comboIsEmpty)
                smoothListBoxBase1.RemoveItem(sender as Control);
            combo_SelectionChanged(null, null);
            smoothListBoxBase1.LayoutItems();
            if (subItemId == -1)
            {

                TransactionStatus status = CargoMatrix.Communication.CargoReceiver.Instance.RemoveHawbFromForkLift(sender.ID, Mawb.TaskId, ForkliftID);
                if (status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                else
                {
                    Status = status;
                }
            }
            else
            {
                TransactionStatus status = CargoMatrix.Communication.CargoReceiver.Instance.RemovePieceFromForkLift(sender.ID, subItemId, Mawb.TaskId, ForkliftID);
                if (status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                else
                {
                    Status = status;
                }

            }
            if (smoothListBoxBase1.ItemsCount == 0)
                DialogResult = DialogResult.Cancel;
        }

        void combo_SelectionChanged(object sender, EventArgs e)
        {
            if (smoothListBoxBase1.SelectedItems.Count == 0)
            {
                bool enableflag = false;

                CustomListItems.ComboBox item = sender as CustomListItems.ComboBox;
                if (item is CustomListItems.ComboBox && (item as CustomListItems.ComboBox).SubItemSelected)
                {
                    enableflag = true;
                }
                OkEnabled = enableflag;
            }
            else
                OkEnabled = true;
        }
    }
    internal class CurrentHawb
    {
        public string hawbNo { get; private set; }
        public string suggestedDropLoc { get; private set; }
        public void SetHawb(string No, string loc)
        {
            hawbNo = No;
            suggestedDropLoc = loc;
        }
    }
}
