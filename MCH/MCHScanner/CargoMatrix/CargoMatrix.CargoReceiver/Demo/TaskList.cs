﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CustomListItems;
using CMXExtensions;
using System.Collections.Generic;
using CargoMatrix.Communication.WSCargoReceiver;
using CMXBarcode;
using SmoothListbox;

namespace CargoMatrix.CargoReceiver.Demo
{
    public class TaskList : MawbList
    {
        public TaskList()
            : base()
        {
            this.Name = "TaskList";
            Forklift.Instance.ReceiveMode = MasterBillDirection.NA;
            Forklift.Instance.Mawb = null;
            //this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
        }


        public override void LoadControl()
        {
            smoothListBoxMainList.RemoveAll();
            if (!Forklift.IsLoaded)
            {
                this.TitleText = string.Format("CargoReceiver");
                this.smoothListBoxMainList.labelEmpty.Text = "Unable to Load\r\nTry to Refresh ...";
                return;
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;
                this.label1.Text = string.Format("{0}/{1}", filter, sort);
                this.searchBox.Text = string.Empty;
                this.Refresh();
                Cursor.Current = Cursors.WaitCursor;


                smoothListBoxMainList.AddItemToFront(new StaticListItem("Receive New", Resources.Icons.Notepad_Add));

                var items = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillList(Forklift.Instance.ReceiveMode, sort, filter, true);
                this.TitleText = string.Format("CargoReceiver - ({0})", items.Length);

                Cursor.Current = Cursors.Default;

                foreach (var item in items)
                {
                    smoothListBoxMainList.AddItem2(InitializeItem(item));
                }

                smoothListBoxMainList.LayoutItems();
                smoothListBoxMainList.RefreshScroll();
            }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private void ProceedNext()
        {
            Forklift.Instance.Mawb = null;
            //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new CargoReceiver() { Location = this.Location, Size = this.Size });


            //if (Forklift.Instance.ReceiveMode == MasterBillDirection.NA)
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = CargoReceiverResources.Text_ActionPopupText;
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoReceiver";

            Control[] actions = new Control[]
                {   
                    new SmoothListbox.ListItems.StandardListItem("RECEIVE IMPORT FREIGHT",null,1 ),
                    new SmoothListbox.ListItems.StandardListItem("RECEIVE DOMESTIC FREIGHT", null, 2),
                };
            actPopup.RemoveAllItems();
            actPopup.AddItems(actions);
            BarcodeEnabled = false;

            if (DialogResult.OK == actPopup.ShowDialog())
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1:
                        Forklift.Instance.ReceiveMode = MasterBillDirection.Import;
                        break;
                    case 2:
                        Forklift.Instance.ReceiveMode = MasterBillDirection.Domestic;
                        break;
                }

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new MawbList());
        }



        protected override void MawbList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is StaticListItem)
                ProceedNext();
            else
                base.MawbList_ListItemClicked(sender, listItem, isSelected);
        }

        protected override void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            /// show count - 1  -1 is the Receive New button
            this.TitleText = string.Format("CargoReceiver - ({0})", smoothListBoxMainList.VisibleItemsCount - 1);
        }

    }
}
