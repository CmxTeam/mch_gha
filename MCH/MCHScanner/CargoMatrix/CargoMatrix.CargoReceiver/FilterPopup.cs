﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSCargoReceiver;

namespace CargoMatrix.CargoReceiver
{
    public enum FilteringMode
    {
        Hawb, Mawb
    }
    public partial class FilterPopup : MessageListBox
    {
        private CustomListItems.ChoiceListItem filterItem;
        private CustomListItems.ChoiceListItem sortItem;
        private MessageListBox optionalChoiceList;
        private string filter = CargoReceiverResources.TextAll;
        private string sort = MawbSort.MASTERBILL;
        private FilteringMode mode;
        public string Filter
        {
            get { return filter; }
            set { filter = value; filterItem.LabelLine2.Text = value; }
        }
        public string Sort
        {
            get { return sort; }
            set { sort = value; sortItem.LabelLine2.Text = value; }
        }
        public int SortValue
        {
            get;
            set;
        }
        public FilterPopup(FilteringMode mode)
        {
            InitializeComponent();
            this.mode = mode;
            this.HeaderText = "Select filters";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = true;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FilterPopup_ListItemClicked);
            TopPanel = false;
            filterItem = new CustomListItems.ChoiceListItem("Filter", CargoMatrix.Resources.Skin.Filter, !string.IsNullOrEmpty(filter) ? filter : CargoReceiverResources.TextAll);
            AddItem(filterItem);
            sortItem = new CustomListItems.ChoiceListItem("Sort", CargoMatrix.Resources.Skin.Sort, !string.IsNullOrEmpty(sort) ? sort : CargoReceiverResources.TextAll);
            AddItem(sortItem);
        }


        void FilterPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            // first time initialization
            if (null == this.optionalChoiceList)
            {
                optionalChoiceList = new MessageListBox();
                optionalChoiceList.MultiSelectListEnabled = false;
                optionalChoiceList.OneTouchSelection = true;
                optionalChoiceList.TopPanel = false;
            }
            // if filter
            if (listItem == filterItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = CargoReceiverResources.Text_FilterBy;



                optionalChoiceList.AddItems(GetFilters());

                // select current selected status
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == filter)
                    {
                        optionalChoiceList.SelectControl(item);
                        break;
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    filter = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    filterItem.LabelLine2.Text = filter;
                }
            }
            //if sort 
            if (listItem == sortItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = CargoReceiverResources.Text_Sortby;
                optionalChoiceList.AddItems(GetSorts());

                // select current selected direction
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == sort)
                    {
                        optionalChoiceList.SelectControl(item);
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    sort = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    sortItem.LabelLine2.Text = sort;
                    SortValue = (optionalChoiceList.SelectedItems[0] as StandardListItem).ID;
                }
            }
        }



        private Control[] GetFilters()
        {
            if (mode == FilteringMode.Hawb)
                return new Control[] 
                {
                    new StandardListItem(ScanStatuses.All.ToString(), null),
                    new StandardListItem(ScanStatuses.NotScanned.ToString(), null),
                    new StandardListItem(ScanStatuses.Scanned.ToString(), null)
                };
            else
                return new Control[] {
                    new StandardListItem(CargoReceiverResources.TextAll, null),
                    new StandardListItem(MawbFilter.NOT_STARTED, null),
                    new StandardListItem(MawbFilter.IN_PROGRESS, null),
                    new StandardListItem(MawbFilter.NOT_COMPLETED, null),
                    new StandardListItem(MawbFilter.COMPLETED, null) };
        }

        private Control[] GetSorts()
        {
            if (mode == FilteringMode.Hawb)
                return new Control[] {
                    new StandardListItem(HouseBillFields.HousebillNumber.ToString(), null),
                    new StandardListItem(HouseBillFields.Weight.ToString(), null),
                    new StandardListItem(HouseBillFields.TotalPieces.ToString(), null)
                };
            else
                return new Control[]
                { 
                new StandardListItem(MawbSort.CARRIER, null),
                new StandardListItem(MawbSort.MASTERBILL, null),
                //new StandardListItem(MawbSort.WEIGHT, null),
                new StandardListItem(MawbSort.ORIGIN, null),
                new StandardListItem(MawbSort.HAWBS, null),
                };
        }
    }
}
