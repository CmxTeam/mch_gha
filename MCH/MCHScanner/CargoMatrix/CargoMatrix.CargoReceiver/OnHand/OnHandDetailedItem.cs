﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.InventoryWS;
using SmoothListbox.ListItems;

namespace CargoMatrix.CargoReceiver
{
    [Obsolete("use HawbDetaildItem")]
    public partial class OnHandDetailedItem : UserControl
    {
        private OnHand parentForm;
        OnhandDetails onHand;
        public void DisplayHousebill(OnhandDetails onhnd)
        {
            this.onHand = onhnd;
            refreshDisplay();
            //switch (hawb.status)
            //{
            //    case CargoMatrix.Communication.DTO.HouseBillStatuses.Open:
            //    case CargoMatrix.Communication.DTO.HouseBillStatuses.Pending:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //    case CargoMatrix.Communication.DTO.HouseBillStatuses.InProgress:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
            //        break;
            //    case CargoMatrix.Communication.DTO.HouseBillStatuses.Completed:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
            //        break;
            //}

            panelIndicators.Flags = 12345;
            //panelIndicators.PopupHeader = hawb.Reference();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

        }

        internal void refreshDisplay()
        {
            title.Text = onHand.onHandNo;
            labelWeight.Text = onHand.measurements.Sum <CargoMatrix.Communication.CargoMeasurements.MettlerMeasurements>(m=>m.Weight).ToString() + "KGS";
            labelCarrier.Text = onHand.carrier;
            labelDropAt.Text = string.Empty;
            labelPcs.Text = onHand.noOfPcs.ToString();
            labelAccount.Text = onHand.customerAccnt;
        }

        public OnHandDetailedItem(OnHand parent)
        {
            InitializeComponent();
            this.parentForm = parent;
        }

        private void InitializeComponentHelper()
        {
            this.buttonDetails.Image = Resources.Skin.printerButton;
            this.buttonDetails.PressedImage = Resources.Skin.printerButton_over;
            this.buttonBrowse.Image = global::Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.buttonDamage.Image = Resources.Skin.damage;
            this.buttonDamage.PressedImage = Resources.Skin.damage_over;
        }

        void buttonDetails_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Cursor.Current = Cursors.Default;

        }

        void buttonDamage_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Cursor.Current = Cursors.Default;
        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            List<Control> actions = new List<Control> {
                new StandardListItem("EDIT ATTRIBUTES",null,6),
                new StandardListItem("EDIT DIMENSIONS",null,5),
                new StandardListItem("EDIT CARRIER",null,4),
                new StandardListItem("EDIT ACCOUNT",null,3),
                new StandardListItem("EDIT REFERENCES",null,2),};
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;

            actPopup.HeaderText = title.Text;
            actPopup.HeaderText2 = "Select action for ";

            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as StandardListItem).ID)
                {
                    case 2:
                        parentForm.SelectReferences();
                        break;
                    case 3:
                        parentForm.SelectCustomerAccount();
                        break;
                    case 4:
                        parentForm.SelectCarrier();
                        break;
                    case 5:
                        parentForm.DisplayDimsCapture(true);
                        break;
                    case 6:
                        parentForm.SelectAttributes();
                        break;
                    default:
                        break;
                }
            }
            actPopup.ResetSelection();

        }
    }
}
