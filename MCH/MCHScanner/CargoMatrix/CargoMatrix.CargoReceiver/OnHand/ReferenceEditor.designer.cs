﻿namespace CargoMatrix.CargoReceiver
{
    partial class ReferenceEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            //this.ButtonAdd = new CargoMatrix.UI.CMXPictureButton();
            //this.labelBlink = new CustomUtilities.CMXBlinkingLabel();

            this.SuspendLayout();
            // 
            // ButtonAdd
            // 
            //this.ButtonAdd.Location = new System.Drawing.Point(202, 2);
            //this.ButtonAdd.Name = "ButtonAdd";
            //this.ButtonAdd.Size = new System.Drawing.Size(32, 32);
            //this.ButtonAdd.TabIndex = 1;
            //this.ButtonAdd.Image = CargoMatrix.Resources.Skin.add_btn;
            //this.ButtonAdd.PressedImage = CargoMatrix.Resources.Skin.add_btn_over;
            //this.ButtonAdd.Click += new System.EventHandler(ButtonAdd_Click);
             
            // labelBlink
             
            //this.labelBlink.BackColor = System.Drawing.Color.White;
            //this.labelBlink.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            //this.labelBlink.ForeColor = System.Drawing.Color.Red;
            //this.labelBlink.Location = new System.Drawing.Point(26,12);
            //this.labelBlink.Name = "labelBlink";
            //this.labelBlink.Text = "Add/Edit Reference";
            //this.labelBlink.Blink = true;
            //this.labelBlink.Size = new System.Drawing.Size(177, 15);
            //this.labelBlink.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            
            //this.panel1.Controls.Remove(labelHeader);
            //this.panel1.Controls.Remove(labelHeader2);
            //this.panel1.Controls.Add(this.ButtonAdd);
            //this.panel1.Controls.Add(this.labelBlink);
            //this.panel1.BackColor = System.Drawing.Color.White;
            ///
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Size = new System.Drawing.Size(240, 292);
            this.ResumeLayout(false);

        }



        //private CargoMatrix.UI.CMXPictureButton ButtonAdd;
        //private CustomUtilities.CMXBlinkingLabel labelBlink;

        #endregion
    }
}
