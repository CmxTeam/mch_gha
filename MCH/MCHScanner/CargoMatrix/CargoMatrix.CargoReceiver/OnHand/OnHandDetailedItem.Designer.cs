﻿namespace CargoMatrix.CargoReceiver
{
    partial class OnHandDetailedItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelWeight = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.labelDropAt = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonDetails = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDamage = new CargoMatrix.UI.CMXPictureButton();
            this.labelCarrier = new System.Windows.Forms.Label();
            this.labelAccount = new System.Windows.Forms.Label();
            this.labelPcs = new System.Windows.Forms.Label();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelWeight
            // 
            this.labelWeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelWeight.Location = new System.Drawing.Point(85, 80);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(111, 13);
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(44, 8);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(190, 14);
            // 
            // labelDropAt
            // 
            this.labelDropAt.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDropAt.Location = new System.Drawing.Point(85, 96);
            this.labelDropAt.Name = "labelDropAt";
            this.labelDropAt.Size = new System.Drawing.Size(111, 13);
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(7, 5);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(24, 24);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDetails.Location = new System.Drawing.Point(202, 33);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(32, 32);
            this.buttonDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDetails.TransparentColor = System.Drawing.Color.White;
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.BackColor = System.Drawing.SystemColors.Control;
            this.buttonBrowse.Location = new System.Drawing.Point(202, 109);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // buttonDamage
            // 
            this.buttonDamage.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDamage.Location = new System.Drawing.Point(202, 71);
            this.buttonDamage.Name = "buttonDamage";
            this.buttonDamage.Size = new System.Drawing.Size(32, 32);
            this.buttonDamage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDamage.TransparentColor = System.Drawing.Color.White;
            this.buttonDamage.Click += new System.EventHandler(this.buttonDamage_Click);
            // 
            // labelCarrier
            // 
            this.labelCarrier.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelCarrier.Location = new System.Drawing.Point(85, 32);
            this.labelCarrier.Name = "labelCarrier";
            this.labelCarrier.Size = new System.Drawing.Size(111, 13);
            // 
            // labelAccount
            // 
            this.labelAccount.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelAccount.Location = new System.Drawing.Point(85, 48);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(111, 13);
            // 
            // labelPcs
            // 
            this.labelPcs.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelPcs.Location = new System.Drawing.Point(85, 64);
            this.labelPcs.Name = "labelPcs";
            this.labelPcs.Size = new System.Drawing.Size(111, 13);
            // 
            // panelIndicators
            // 
            this.panelIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIndicators.Location = new System.Drawing.Point(3, 161);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            this.panelIndicators.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(7, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.Text = "Ttl Weight:";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(7, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.Text = "Drop At :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(7, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.Text = "Carrier :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(7, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.Text = "Account :";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(7, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.Text = "Pieces :";
            // 
            // OnHandDetailedItem
            // 
            InitializeComponentHelper();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.labelPcs);
            this.Controls.Add(this.labelAccount);
            this.Controls.Add(this.labelCarrier);
            this.Controls.Add(this.labelDropAt);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonDamage);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.panelIndicators);
            this.Name = "OnHandDetailedItem";
            this.Size = new System.Drawing.Size(240, 177);
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelDropAt;
        private System.Windows.Forms.Label labelCarrier;
        private System.Windows.Forms.Label labelAccount;
        private System.Windows.Forms.Label labelPcs;
        private CustomUtilities.IndicatorPanel panelIndicators;
        private CargoMatrix.UI.CMXPictureButton buttonDetails;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private CargoMatrix.UI.CMXPictureButton buttonBrowse;
        private CargoMatrix.UI.CMXPictureButton buttonDamage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}