﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CargoMatrix.Communication.CargoMeasurements;
using SmoothListbox.ListItems;

namespace CargoMatrix.CargoMeasurements
{
    public partial class DevicesPopup : MessageListBox
    {
        DimensionerDevice currentDevice;

        public DimensionerDevice CurrentDevice
        {
            get { return currentDevice; }
        }

        public DevicesPopup()
        {
            InitializeComponent();

            this.TopPanel = true;
            this.ButtonVisible = true;
            this.ButtonImage = CargoMatrix.Resources.Skin.button_scale;
            this.ButtonPressedImage = CargoMatrix.Resources.Skin.button_scale_over;
            this.HeaderText = "Select device";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = false;
     
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(DevicesPopup_ListItemClicked);
            this.LoadListEvent += new LoadSmoothList(DevicesPopup_LoadListEvent);
            this.ButtonClick += new EventHandler(DevicesPopup_ButtonClick);
        }

        void DevicesPopup_ButtonClick(object sender, EventArgs e)
        {
            if (this.SelectedItems.Count <= 0) return;

            using (var cursorChanger = CustomUtilities.CursorChanger.WaitChanger)
            {
                var selectedItem = (TwoLineListItem<DimensionerDevice>)this.SelectedItems[0];

                if (selectedItem.ItemData.IsDefault == true) return;
                
                CargoMeasurementsService.Instance.SetDefaultDimensiongDevice(selectedItem.ItemData);

                this.PopulateContent();
            }
        }

        void DevicesPopup_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.PopulateContent();
        }

        void PopulateContent()
        {
            this.HeaderText2 = string.Empty;

            var devices = CargoMeasurementsService.Instance.GetDimensioningDevices();

            this.RemoveAllItems();
            
            foreach (var device in devices)
            {
                if(device.IsDefault == true)
                {
                    this.currentDevice = device;
                    this.HeaderText2 = this.currentDevice.Name;
                    var item = new TwoLineListItem<DimensionerDevice>(device.Name, CargoMatrix.Resources.Icons.Shipping_Scale_Check, device.IpAddress.ToString(), device);
                    this.AddItem(item);
                }
                else
                {
                    var item = new TwoLineListItem<DimensionerDevice>(device.Name, CargoMatrix.Resources.Icons.Shipping_Scale, device.IpAddress.ToString(), device);
                    this.AddItem(item);
                }
            }

            Cursor.Current = Cursors.Default;

        }

        void DevicesPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var clickedItem = (SmoothListbox.ListItems.TwoLineListItem<DimensionerDevice>)listItem;

            this.currentDevice = clickedItem.ItemData;

            this.HeaderText2 = this.currentDevice.Name;
            
        }
    }
}