﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoMeasurements;
using CargoMatrix.Communication.CargoMeasurements.Communication;
using CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver;
using CargoMatrix.ExceptionManager;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol;
using CustomListItems;
using CargoMatrix.Utilities;
using CargoMatrix.Communication.ScannerUtilityWS;
using System.Threading;
using CargoMatrix.Communication;
using CustomUtilities;
using CargoMatrix.Communication.WSCargoDimensioner;



namespace CargoMatrix.CargoMeasurements.MeasurementsCapture
{
    public partial class RedeemDimensions : SmoothListbox.SmoothListbox
    {
        private CargoMatrix.UI.CMXPictureButton btnDamgeCapture;
        private CargoMatrix.UI.CMXPictureButton btnSelectBoxType;
        private CargoMatrix.UI.CMXPictureButton btnSelectDevice;
        private CargoMatrix.UI.CMXPictureButton btnAquire;
        private CargoMatrix.UI.CMXPictureButton btnEditPackageReference;


        MetricSystemTypeEnum metricSystemType;
        PrintingOrigin printingOrigin;

        OptionsListITem metricSystemMenuItem = new OptionsListITem(OptionsListITem.OptionItemID.METRICS_SYSTEM);
        OptionsListITem finalizeMenuItem = new OptionsListITem(OptionsListITem.OptionItemID.FINALIZE_MEASUREMENTS);
        OptionsListITem printMenuItem = new OptionsListITem(OptionsListITem.OptionItemID.PRINT_MEASUREMENTS_LABELS);

        ICommunicationProtocolDriver protocolDriver;
        CargoMatrix.Communication.ScannerUtilityWS.PackageType[] packageTypes;
        LabelPrinter defaultPrinter;
        bool printingDisables;
        DimensioningMeasurementDetails savedMeasurement;
       
        #region Constructors
	    public RedeemDimensions()
        {
            InitializeComponent();

            this.metricSystemType = MetricSystemTypeEnum.Imperial;
            this.savedMeasurement  = null;

            this.TitleText = "CargoDimensioner";
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(barcode_BarcodeReadNotify);

            this.LoadOptionsMenu += new EventHandler(RedeemDimensions_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(RedeemDimensions_MenuItemClicked);
        }
	    #endregion

        #region Handlers
        void RedeemDimensions_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            DialogResult dialogResult;

            switch (lItem.ID)
            {
                case OptionsListITem.OptionItemID.METRICS_SYSTEM:

                    Cursor.Current = Cursors.WaitCursor;

                    MetricSystemPopup metricPopup = new MetricSystemPopup(this.metricSystemType);

                    dialogResult = metricPopup.ShowDialog();

                    if (dialogResult != DialogResult.OK) return;

                    this.metricSystemType = metricPopup.MetricSystemType;

                    metricSystemMenuItem.DescriptionLine = CommonMethods.GetMetricSystemMenuItemText(this.metricSystemType);

                    this.redeemControl.ChangeMetricSystem(this.metricSystemType);

                    break;

                case OptionsListITem.OptionItemID.PRINT_MEASUREMENTS_LABELS:

                    if (this.savedMeasurement != null)
                    {
                        this.defaultPrinter = CommonMethods.PrintMeasurements(new DimensioningMeasurementDetails[]{ savedMeasurement }, this.printingOrigin, this.defaultPrinter);
                    }
               
                    break;

                case OptionsListITem.OptionItemID.FINALIZE_MEASUREMENTS:

                    this.SaveMeasurements();

                    break;
            }
        }

        void RedeemDimensions_LoadOptionsMenu(object sender, EventArgs e)
        {
            metricSystemMenuItem.DescriptionLine = CommonMethods.GetMetricSystemMenuItemText(this.metricSystemType);

            this.AddOptionsListItem(metricSystemMenuItem);

            this.AddOptionsListItem(this.printMenuItem);

            this.AddOptionsListItem(this.finalizeMenuItem);
        } 
        
        void barcode_BarcodeReadNotify(string barcodeData)
        {
            this.BarcodeEnabled = false;
            if (this.redeemControl.ParsePackegeReference(barcodeData) == true)
            {
                this.savedMeasurement = null;
            }

            this.BarcodeEnabled = true;
        }

        void btnDamgeCapture_Click(object sender, EventArgs e)
        {
            if (CargoMatrix.Communication.Utilities.CameraPresent == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Camera is not present on the device", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                return;
            }

            string packageReference = this.redeemControl.PackageRefernce.Trim();

            if (string.IsNullOrEmpty(packageReference) == true)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Enter package ID", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OK, DialogResult.OK);
                return;
            }

            this.BarcodeEnabled = false;

            this.ShutDownDriver();

            Cursor.Current = Cursors.WaitCursor;

            CargoMatrix.FreightPhotoCapture.Reasons reasons = new CargoMatrix.FreightPhotoCapture.Reasons(false);

            CargoMatrix.FreightPhotoCapture.ReasonsLogic.DisplayReasons(string.Empty, packageReference, CargoMatrix.Communication.DTO.TaskType.UNKNOWN, this, ref reasons);


            //CargoMatrix.FreightPhotoCapture.PhotoCapturePopup photoCapturePopup = new CargoMatrix.FreightPhotoCapture.PhotoCapturePopup();
            //photoCapturePopup.ShowDialog();

            this.BarcodeEnabled = true;

            this.redeemControl.SetInfoStatusMessage("Checking connection");

            this.btnAquire.Enabled = false;

            this.AttachDriver(CommonMethods.CurrentDevice);

            this.DriverPerformStep();
        }

        void btnSelectDevice_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            DevicesPopup devicePopup = new DevicesPopup();

            if (devicePopup.ShowDialog() != DialogResult.OK) return;

            CommonMethods.CurrentDevice = devicePopup.CurrentDevice;

            this.redeemControl.UpdateDeviceInformation("Changing device");

            this.Refresh();

            this.ShutDownDriver();

            //CommonMethods.CurrentDevice.IpAddress = System.Net.IPAddress.Parse("10.0.0.212");
            //CommonMethods.CurrentDevice.ServerPort = 51242;
            //CommonMethods.CurrentDevice.ServerIP = System.Net.IPAddress.Parse("10.0.0.25");

            this.AttachDriver(CommonMethods.CurrentDevice);

            this.btnAquire.Enabled = false;

            this.DriverPerformStep();
        }

        void btnAquire_Click(object sender, EventArgs e)
        {
            this.btnAquire.Enabled = false;
            this.btnDamgeCapture.Enabled = false;

            this.DriverPerformStep();
        }

        void btnSelectBoxType_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            MessageListBox listBox = new MessageListBox();
            listBox.MultiSelectListEnabled = false;
            listBox.OneTouchSelection = true;
            listBox.OkEnabled = false;
            listBox.HeaderText = "Select package type";
            listBox.HeaderText2 = string.Empty;

            foreach (var packageType in packageTypes)
            {
                var listItem = new SmoothListbox.ListItems.StandardListItem<CargoMatrix.Communication.ScannerUtilityWS.PackageType>(packageType.name, CargoMatrix.Resources.Skin.Shipping_Crate1, packageType);

                listBox.AddItem(listItem);
            }

            Cursor.Current = Cursors.Default;

            if (listBox.ShowDialog() != DialogResult.OK) return;

            this.redeemControl.PackageType = ((SmoothListbox.ListItems.StandardListItem<CargoMatrix.Communication.ScannerUtilityWS.PackageType>)listBox.SelectedItems[0]).ItemData;

            this.savedMeasurement = null;
        }

        void btnEditPackageReference_Click(object sender, EventArgs e)
        {
            this.BarcodeEnabled = false;

            ScanEnterPopup_old popUp = new ScanEnterPopup_old(CustomUtilities.BarcodeType.All);
            popUp.Title = "Scan/Enter barcode";
            popUp.TextLabel = "Barcode";
            var result = popUp.ShowDialog();

            if (result == DialogResult.OK)
            {
                bool parseResult = this.redeemControl.ParsePackegeReference(popUp.ScannedText);

                if (parseResult == true)
                {
                    this.savedMeasurement = null;
                }
            }

            this.BarcodeEnabled = true;
        }

        void redeemControl_DimensionsChanged(object sender, EventArgs e)
        {
            this.savedMeasurement = null;
        }

        void redeemControl_PackageScanned(object sender, PackageEventAgrs e)
        {
            if (e.Success)
            {
                this.btnDamgeCapture.Enabled = e.DimensioningDetails != null && e.DimensioningDetails.BarcodeType == CMXBarcode.BarcodeTypes.HouseBill;
            }
        }
        
        void CommunicationClient_OnError(object sender, CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ExceptionEventArgs e)
        {
            Action action = () =>
            {
                Logger.Log(e.Exception, 70000002);

                this.redeemControl.SetErrorStatusMessage("Checking connection");

                this.btnAquire.Enabled = false;
                this.btnDamgeCapture.Enabled = true;
            };

            this.SafeInvoke(action);
        }

        void CommunicationClient_OnResponseReceived(object sender, CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ResponseReceivedEventArgs e)
        {
            Action action = () =>
            {
                string message = CommonMethods.CommunicatorCommandAsString(e.Command);

                switch (e.Command.CommnadType)
                {
                    case CommandTypesEnum.ALIVE:

                        this.btnAquire.Enabled = true;
                        this.redeemControl.SetInfoStatusMessage(message);

                        break;

                    case CommandTypesEnum.BUSY:
                    case CommandTypesEnum.UNAVAILABLE:

                        this.redeemControl.SetErrorStatusMessage(message);

                        break;

                    case CommandTypesEnum.DATA:

                        var measurements = e.Command.DataAsMeasurements;

                        if (this.metricSystemType != measurements.MetricSystemType)
                        {
                            measurements = measurements.ConvertToMetricSystem(this.metricSystemType);
                        }

                        this.redeemControl.DisplayMesurement(measurements, false);

                        this.redeemControl.SetInfoStatusMessage(message);

                        break;
                }

                this.btnAquire.Enabled = true;
                this.btnDamgeCapture.Enabled = true;
            };

            this.SafeInvoke(action);
        }

        void CommunicationClient_OnMettlerIPSent(object sender, EventArgs e)
        {
            Action action = () =>
            {
                this.redeemControl.SetInfoStatusMessage("Waiting");
            };

            this.SafeInvoke(action);
        }

        void CommunicationClient_OnConnected(object sender, EventArgs e)
        {
            Action action = () =>
            {
                this.redeemControl.SetInfoStatusMessage("Checking connection");

                this.DriverPerformStep();
            };

            this.SafeInvoke(action);
        } 
        
        #endregion

        #region Overrides
        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;

            if (this.smoothListBoxMainList.Items.Count <= 0)
            {
                this.AddMainListItem(this.redeemControl);
                this.redeemControl.DimensionsChanged += new EventHandler(redeemControl_DimensionsChanged);
                this.redeemControl.PackageScanned += new EventHandler<PackageEventAgrs>(redeemControl_PackageScanned);

                this.packageTypes = Communication.ScannerUtility.Instance.GetPackageTypeList();

                if (packageTypes.Length > 0)
                {
                    this.redeemControl.PackageType = packageTypes[0];
                }

                this.printingDisables = false;

                this.printingOrigin = Communication.CargoMeasurements.CargoMeasurementsService.Instance.GetPrinterOrigin();

                if (printingOrigin != PrintingOrigin.DISABLED)
                {
                    this.defaultPrinter = Communication.ScannerUtility.Instance.GetDefaultPrinter(LabelTypes.MML);
                }
                else
                {
                    printingDisables = true;
                }
            }
          
            base.LoadControl();

            if (CommonMethods.CurrentDevice == null)
            {
                CommonMethods.CurrentDevice = Communication.CargoMeasurements.CargoMeasurementsService.Instance.GetDefaultDimensioningDevice();
            }

            this.redeemControl.UpdateDeviceInformation();

            this.AttachDriver(CommonMethods.CurrentDevice);

            this.btnAquire.Enabled = false;

            if (CommonMethods.CurrentDevice != null)
            {
                this.redeemControl.SetInfoStatusMessage("Checking connection");
            }

            this.DriverPerformStep();

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            Cursor.Current = Cursors.Default;
        }

        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            if (this.m_bOptionsMenu == true)
            {
                base.pictureBoxBack_Click(sender, e);
                return;
            }
            
            this.ShutDownDriver();
            
            base.pictureBoxBack_Click(sender, e);
        }

        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            this.UpdateMenuItems();
            
            base.pictureBoxMenu_Click(sender, e);
        }
        #endregion

        #region Additional methods
        private void SaveMeasurements()
        {
            this.BarcodeEnabled = false;

            var measurement = this.redeemControl.GetCompleteMeasurements();

            this.BarcodeEnabled = true;

            if (measurement == null) return;

            var status = CargoMeasurementsService.Instance.SaveMeasurement(measurement);

            if (status.transactionStatus == true)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Measurements saved successfully", "Message", CargoMatrix.UI.CMXMessageBoxIcon.Message);

                this.savedMeasurement = measurement;

                this.defaultPrinter = CommonMethods.PrintMeasurements(new DimensioningMeasurementDetails[]{ measurement}, this.printingOrigin, this.defaultPrinter);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                this.savedMeasurement = null;
            }
        }
        
        private void InitializeFields2()
        {
            this.btnDamgeCapture = new CargoMatrix.UI.CMXPictureButton();
            this.btnSelectBoxType = new CargoMatrix.UI.CMXPictureButton();
            this.btnSelectDevice = new CargoMatrix.UI.CMXPictureButton();
            this.btnAquire = new CargoMatrix.UI.CMXPictureButton();
            this.btnEditPackageReference = new CargoMatrix.UI.CMXPictureButton();

            // 
            // btnAquire
            // 
            this.btnAquire.Location = new System.Drawing.Point(104, 148);
            this.btnAquire.Name = "btnAquire";
            this.btnAquire.Size = new System.Drawing.Size(125, 32);
            this.btnAquire.BackColor = Color.White;
            this.btnAquire.Image = CargoMatrix.Resources.Skin.button_acquire;
            this.btnAquire.PressedImage = CargoMatrix.Resources.Skin.button_acquire_over;
            this.btnAquire.DisabledImage = CargoMatrix.Resources.Skin.button_acquire_dis;
            this.btnAquire.Click += new EventHandler(btnAquire_Click);

            // 
            // btnSelectDevice
            // 
            this.btnSelectDevice.Location = new System.Drawing.Point(193, 190);
            this.btnSelectDevice.Name = "btnSelectDevice";
            this.btnSelectDevice.Size = new System.Drawing.Size(36, 36);
            this.btnSelectDevice.BackColor = Color.White;
            this.btnSelectDevice.Image = CargoMatrix.Resources.Skin.button_scale;
            this.btnSelectDevice.PressedImage = CargoMatrix.Resources.Skin.button_scale_over;
            this.btnSelectDevice.Click += new EventHandler(btnSelectDevice_Click);

            // 
            // btnDamgeCapture
            // 
            this.btnDamgeCapture.Location = new System.Drawing.Point(193, 57);
            this.btnDamgeCapture.Name = "btnDamgeCapture";
            this.btnDamgeCapture.Size = new System.Drawing.Size(36, 36);
            this.btnDamgeCapture.BackColor = Color.White;
            this.btnDamgeCapture.Image = CargoMatrix.Resources.Skin.damage;
            this.btnDamgeCapture.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.btnDamgeCapture.DisabledImage = CargoMatrix.Resources.Skin.damage_dis;
            this.btnDamgeCapture.Click += new EventHandler(btnDamgeCapture_Click);

            // 
            // btnPrint
            // 
            this.btnSelectBoxType.Location = new System.Drawing.Point(148, 57);
            this.btnSelectBoxType.Name = "btnPrint";
            this.btnSelectBoxType.Size = new System.Drawing.Size(36, 36);
            this.btnSelectBoxType.BackColor = Color.White;
            this.btnSelectBoxType.Image = CargoMatrix.Resources.Skin.shipping_box_help;
            this.btnSelectBoxType.PressedImage = CargoMatrix.Resources.Skin.shipping_box_help_over;
            this.btnSelectBoxType.Click += new EventHandler(btnSelectBoxType_Click);

            //
            // btnEditPackageReference
            // 
            this.btnEditPackageReference.Location = new System.Drawing.Point(193, 17);
            this.btnEditPackageReference.Size = new System.Drawing.Size(36, 36);
            this.btnEditPackageReference.BackColor = System.Drawing.Color.White;
            this.btnEditPackageReference.Image = CargoMatrix.Resources.Skin.edit_btn;
            this.btnEditPackageReference.PressedImage = CargoMatrix.Resources.Skin.edit_btn_over;
            this.btnEditPackageReference.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEditPackageReference.Click += new EventHandler(btnEditPackageReference_Click);

            this.redeemControl.Controls.Add(this.btnAquire);
            this.redeemControl.Controls.Add(this.btnSelectDevice);
            this.redeemControl.Controls.Add(this.btnDamgeCapture);
            this.redeemControl.Controls.Add(this.btnSelectBoxType);
            this.redeemControl.Controls.Add(this.btnEditPackageReference);

            this.btnDamgeCapture.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.btnSelectBoxType.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.btnSelectDevice.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.btnAquire.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.btnEditPackageReference.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
        }

        private void AttachDriver(DimensionerDevice device)
        {
            if (device == null) return;

            this.protocolDriver = new AutoConnectCommunicationProtocolDriver(device);
            this.protocolDriver.CommunicationClient.OnConnected += new EventHandler(CommunicationClient_OnConnected);
            this.protocolDriver.CommunicationClient.OnMettlerIPSent += new EventHandler(CommunicationClient_OnMettlerIPSent);
            this.protocolDriver.CommunicationClient.OnResponseReceived += new EventHandler<ResponseReceivedEventArgs>(CommunicationClient_OnResponseReceived);
            this.protocolDriver.CommunicationClient.OnError += new EventHandler<ExceptionEventArgs>(CommunicationClient_OnError);
        }

        private void ShutDownDriver()
        {
            if (this.protocolDriver == null) return;

            if (this.protocolDriver.CommunicationClient != null)
            {
                this.protocolDriver.CommunicationClient.OnConnected -= new EventHandler(CommunicationClient_OnConnected);
                this.protocolDriver.CommunicationClient.OnMettlerIPSent -= new EventHandler(CommunicationClient_OnMettlerIPSent);
                this.protocolDriver.CommunicationClient.OnResponseReceived -= new EventHandler<ResponseReceivedEventArgs>(CommunicationClient_OnResponseReceived);
                this.protocolDriver.CommunicationClient.OnError -= new EventHandler<ExceptionEventArgs>(CommunicationClient_OnError);
            }

            this.protocolDriver.ShutDown();
        }

        private void DriverPerformStep()
        {
            if (this.protocolDriver == null) return;

            this.protocolDriver.PerformAction();
        }

        private void SafeInvoke(Action action)
        {
            try
            {
                if (this.InvokeRequired == true)
                {
                    this.Invoke(action);
                }
                else
                {
                    action();
                }
            }
            catch 
            {
                // It is ugly, I konow            	
            }
        }

        private void UpdateMenuItems()
        {
            bool hasComleteMeasurements = this.redeemControl.HasCompleteMeasurements;

            if (printingDisables == false)
            {
                if (hasComleteMeasurements == false)
                {
                    this.printMenuItem.Enabled = false;
                }
                else
                {
                    this.printMenuItem.Enabled = this.savedMeasurement != null;
                }
            }
             
            this.finalizeMenuItem.Enabled = hasComleteMeasurements;
        }
        
        #endregion
    }
}
