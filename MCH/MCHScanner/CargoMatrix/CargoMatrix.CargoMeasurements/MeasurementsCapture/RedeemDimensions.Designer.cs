﻿namespace CargoMatrix.CargoMeasurements.MeasurementsCapture
{
    partial class RedeemDimensions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            redeemControl = new RedeemDimensionControl();

            this.SuspendLayout();

            this.InitializeFields2();

            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            
            this.ResumeLayout(false);
        }

        private RedeemDimensionControl redeemControl;

        #endregion
    }
}
