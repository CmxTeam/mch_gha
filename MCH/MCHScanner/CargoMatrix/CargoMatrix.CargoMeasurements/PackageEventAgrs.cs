﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.CargoMeasurements;

namespace CargoMatrix.CargoMeasurements
{
    public class PackageEventAgrs : EventArgs
    {
        public DimensioningMeasurementDetails DimensioningDetails { get; set; }

        public bool Success { get; set; }
    }
}
