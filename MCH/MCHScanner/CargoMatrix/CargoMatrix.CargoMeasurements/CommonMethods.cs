﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoMeasurements;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSCargoDimensioner;
using CargoMatrix.Communication.ScannerUtilityWS;
using CustomUtilities;

namespace CargoMatrix.CargoMeasurements
{
    public static class CommonMethods
    {
        public static DimensionerDevice CurrentDevice;

        public static string CommunicatorCommandAsString(CommandValues command)
        {
            switch (command.CommnadType)
            {
                case CommandTypesEnum.ALIVE:
                    return "READY";

                case CommandTypesEnum.BUSY:
                    return "BUSY";

                case CommandTypesEnum.DATA:
                    return "Data received";

                case CommandTypesEnum.UNAVAILABLE:
                    return "Unavailable";

                default: return string.Empty;
            }
        }

        public static string GetMetricSystemMenuItemText(MetricSystemTypeEnum metricSystemType)
        {
            switch (metricSystemType)
            {
                case MetricSystemTypeEnum.Imperial: return "Imperial / USA";

                case MetricSystemTypeEnum.Metric: return "Metric";

                default: return string.Empty;

            }
        }

        public static bool GetMasterBillFromUser(out CargoMatrix.Communication.ScannerUtilityWS.MasterBillItem masterbill)
        {
            masterbill = null;

            do
            {
                CustomUtilities.ScanEnterPopup_old scanPopup = new CustomUtilities.ScanEnterPopup_old(CustomUtilities.BarcodeType.Masterbill);
                scanPopup.TextLabel = "Masterbill number";

                if (scanPopup.ShowDialog() != DialogResult.OK) return false;

                CMXBarcode.ScanObject scanMasterbillItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(scanPopup.Text);
                masterbill = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillByNumber(scanMasterbillItem.CarrierNumber, scanMasterbillItem.MasterBillNumber);

                if (masterbill == null)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Masterbill doesn't exist", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                }

                scanPopup.Dispose();
            }
            while (masterbill == null);

            return true;
        }

        public static bool ParsePackegeReference(string data, out DimensioningMeasurementDetails details)
        {
            details = null;

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(data);

            bool result = false;

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    return CommonMethods.ParseHousebill(scanItem, out details);

                case CMXBarcode.BarcodeTypes.Uld:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    return CommonMethods.ParseUld(scanItem, out details);


                case CMXBarcode.BarcodeTypes.MasterBill:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                    return CommonMethods.ParseLooseULD(scanItem, out details);

                case CMXBarcode.BarcodeTypes.OnHand:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

                    return CommonMethods.ParseOnHand(scanItem, out details);

                case CMXBarcode.BarcodeTypes.NA:
                    return CommonMethods.ParseNABarcode(scanItem, out details);

                default: CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                    CargoMatrix.UI.CMXMessageBox.Show("Wrong barcode type", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return false;
            }
        }

        public static bool ParseHousebill(CMXBarcode.ScanObject scanItem, out DimensioningMeasurementDetails measurementDetails)
        {
            measurementDetails = null;

            int pieceNumber = 0;
            var housebill = CargoMatrix.Communication.ScannerUtility.Instance.GetHouseBillByNumber(scanItem.HouseBillNumber);

            if (housebill == null ||
                (housebill != null &&
                housebill.HousebillId == 0))
            {
                if (CargoMatrix.UI.CMXMessageBox.Show("Third party package measurement?", "Question", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.OKCancel, DialogResult.OK) == DialogResult.OK)
                {
                    return CommonMethods.ParseNABarcode(scanItem, out measurementDetails);
                }

                int pieces;

                CargoMatrix.Communication.InventoryWS.TransactionStatus status = null;
                do
                {
                    string origin;
                    string destination;

                    if (DialogResult.OK != CustomUtilities.OriginDestinationMessageBox.Show(scanItem.HouseBillNumber, out pieces, out origin, out destination)) return false;

                    status = CargoMatrix.Communication.Inventory.Instance.CreateHousebill(scanItem.HouseBillNumber, origin, destination, pieces, 0);

                    if (status.TransactionStatus1 == false)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                }
                while (status.TransactionStatus1 == false);

                if (pieces > 0)
                {
                    CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
                    cntMsg.HeaderText = "Confirm piece number";
                    cntMsg.LabelDescription = "Enter piece number to measure";
                    cntMsg.LabelReference = "Piece number";
                    cntMsg.PieceCount = pieces;
                    cntMsg.MinPiecesCount = 1;
                    cntMsg.Readonly = false;

                    if (cntMsg.ShowDialog() != DialogResult.OK) return false;

                    pieceNumber = cntMsg.PieceCount;
                }
            }
            else
            {
                pieceNumber = scanItem.PieceNumber;
            }

            measurementDetails = new DimensioningMeasurementDetails();
            measurementDetails.PackageNumber = scanItem.HouseBillNumber;
            measurementDetails.FullReferenceNumber = scanItem.BarcodeData;
            measurementDetails.MasterbillNumber = null;
            measurementDetails.BarcodeType = CMXBarcode.BarcodeTypes.HouseBill;
            measurementDetails.PieceNumber = pieceNumber;
            measurementDetails.ULDType = null;

            return true;
        }

        public static bool ParseUld(CMXBarcode.ScanObject scanItem, out DimensioningMeasurementDetails measurementDetails)
        {
            measurementDetails = null;

            var uld = CargoMatrix.Communication.ScannerUtility.Instance.GetUldByNumber(scanItem.UldNumber);

            if (uld == null)
            {
                var uldTypes = CargoMatrix.Communication.ScannerUtility.Instance.GetULDTypes(false, MOT.Air);

                MessageListBox list = new MessageListBox();
                list.MultiSelectListEnabled = false;
                list.OneTouchSelection = false;
                list.OkEnabled = false;
                list.HeaderText = "Select ULD type";
                list.HeaderText2 = "ULD type";

                foreach (var uldType in uldTypes)
                {
                    list.AddItem(new SmoothListbox.ListItems.StandardListItem<IULDType>(uldType.ULDName, null, uldType));
                }

                if (list.ShowDialog() != DialogResult.OK) return false;

                CargoMatrix.Communication.ScannerUtilityWS.MasterBillItem currentMasterbill;

                if (CommonMethods.GetMasterBillFromUser(out currentMasterbill) == false) return false;

                var currentUldType = ((SmoothListbox.ListItems.StandardListItem<IULDType>)list.SelectedItems[0]).ItemData;

                measurementDetails = new DimensioningMeasurementDetails();
                measurementDetails.PackageNumber = scanItem.UldNumber;
                measurementDetails.FullReferenceNumber = scanItem.BarcodeData;
                measurementDetails.MasterbillNumber = currentMasterbill.MasterBillNumber;
                measurementDetails.BarcodeType = CMXBarcode.BarcodeTypes.Uld;
                measurementDetails.ULDType = new ULDType(currentUldType);
            }
            else
            {
                measurementDetails = new DimensioningMeasurementDetails();
                measurementDetails.PackageNumber = scanItem.UldNumber;
                measurementDetails.FullReferenceNumber = scanItem.BarcodeData;
                measurementDetails.MasterbillNumber = null;
                measurementDetails.BarcodeType = CMXBarcode.BarcodeTypes.Uld;
                measurementDetails.ULDType = new ULDType() { TypeID = uld.ULDTypeId, ULDName = uld.ULDType };
            }
            return true;
        }

        public static bool ParseLooseULD(CMXBarcode.ScanObject scanItem, out DimensioningMeasurementDetails measurementDetails)
        {
            measurementDetails = null;

            var currentMasterbill = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillByNumber(scanItem.CarrierNumber, scanItem.MasterBillNumber);

            if (currentMasterbill == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Masterbill doesn't exist", "Scan result error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return false;
            }

            var uldTypes = CargoMatrix.Communication.ScannerUtility.Instance.GetULDTypes(true, MOT.Air);
            var looseUldType = uldTypes.FirstOrDefault(t => t.IsLoose == true);

            if (looseUldType == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("LOOSE ULD type not found", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return false;
            }

            measurementDetails = new DimensioningMeasurementDetails();
            measurementDetails.PackageNumber = scanItem.MasterBillNumber;
            measurementDetails.FullReferenceNumber = scanItem.BarcodeData;
            measurementDetails.MasterbillNumber = currentMasterbill.MasterBillNumber;
            measurementDetails.BarcodeType = CMXBarcode.BarcodeTypes.Uld;
            measurementDetails.ULDType = new ULDType(looseUldType);

            return true;
        }

        public static bool ParseOnHand(CMXBarcode.ScanObject scanItem, out DimensioningMeasurementDetails measurementDetails)
        {
            measurementDetails = new DimensioningMeasurementDetails();
            measurementDetails.PackageNumber = scanItem.OnHandNumber;
            measurementDetails.PieceNumber = scanItem.PieceNumber;
            measurementDetails.FullReferenceNumber = scanItem.BarcodeData;
            measurementDetails.MasterbillNumber = null;
            measurementDetails.BarcodeType = CMXBarcode.BarcodeTypes.OnHand;
            measurementDetails.ULDType = null;

            return true;
        }

        public static bool ParseNABarcode(CMXBarcode.ScanObject scanItem, out DimensioningMeasurementDetails measurementDetails)
        {
            measurementDetails = null;

            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = "Confirm piece number";
            cntMsg.LabelDescription = "Enter piece number to measure";
            cntMsg.LabelReference = "Piece number";
            cntMsg.PieceCount = int.MaxValue;
            cntMsg.MinPiecesCount = 1;
            cntMsg.Readonly = false;

            if (cntMsg.ShowDialog() != DialogResult.OK) return false;

            measurementDetails = new DimensioningMeasurementDetails();
            measurementDetails.PackageNumber = scanItem.BarcodeData;
            measurementDetails.FullReferenceNumber = string.Format("{0}_{1}", scanItem.BarcodeData, cntMsg.PieceCount);
            measurementDetails.PieceNumber = cntMsg.PieceCount;
            measurementDetails.MasterbillNumber = null;
            measurementDetails.BarcodeType = CMXBarcode.BarcodeTypes.NA;
            measurementDetails.ULDType = null;

            return true;

        }

        public static string GetHeaderText(DimensioningMeasurementDetails details)
        {
            switch (details.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill: return "Package Ref # (HB)";

                case CMXBarcode.BarcodeTypes.MasterBill: return "Package Ref # (ULD LS)";

                case CMXBarcode.BarcodeTypes.Uld: return details.ULDType != null && details.ULDType.IsLoose ? "Package Ref # (ULD LS)" : "Package Ref # (ULD)";

                case CMXBarcode.BarcodeTypes.OnHand: return "Package Ref # (OH)";

                default: return "Package Ref # (N/A)";
            }
        }

        public static LabelPrinter PrintMeasurements(DimensioningMeasurementDetails[] details, PrintingOrigin printingOrigin, LabelPrinter labelPrinter)
        {
            bool success = false;
            CustomUtilities.MobilePrinting.MobilePrinterUtility printing;

            switch (printingOrigin)
            {
                case PrintingOrigin.DISABLED: return labelPrinter;

                case PrintingOrigin.MANUAL:

                    LabelPrinter printer = new LabelPrinter();
                    success = PrinterUtilities.PopulateAvailablePrinters(LabelTypes.MML, ref printer, true);

                    if (success == true)
                    {
                        printing = new CustomUtilities.MobilePrinting.MobilePrinterUtility(printer);
                        printing.MeasurementsPrint(details);
                    }

                    return labelPrinter;

                case PrintingOrigin.AUTO:

                    if (labelPrinter == null)
                    {
                        labelPrinter = new LabelPrinter();

                        success = PrinterUtilities.PopulateAvailablePrinters(LabelTypes.MML, ref labelPrinter, true);

                        labelPrinter = Communication.ScannerUtility.Instance.GetDefaultPrinter(LabelTypes.MML);

                        if (success == false) return labelPrinter;
                    }

                    printing = new CustomUtilities.MobilePrinting.MobilePrinterUtility(labelPrinter);
                    printing.MeasurementsPrint(details);

                    return labelPrinter;

                default: return labelPrinter;
            }
        }

        public static bool MeasurementExists(string data, IEnumerable<DimensioningMeasurementDetails> measurements)
        {
            var measurement = measurements.FirstOrDefault(item =>
                item != null &&
                item.BarcodeType != CMXBarcode.BarcodeTypes.NA &&
                string.Compare(data, item.FullReferenceNumber, StringComparison.OrdinalIgnoreCase) == 0);

            return measurement != null;
        }

        public static bool UndentifiedMeasurementExists(DimensioningMeasurementDetails details, IEnumerable<DimensioningMeasurementDetails> measurements)
        {
            var measurement = measurements.FirstOrDefault(item =>
                item != null &&
                item.BarcodeType == CMXBarcode.BarcodeTypes.NA &&
                string.Compare(details.PackageNumber, item.PackageNumber, StringComparison.OrdinalIgnoreCase) == 0 &&
                details.PieceNumber == item.PieceNumber);

            return measurement != null;
        }
    }
}
