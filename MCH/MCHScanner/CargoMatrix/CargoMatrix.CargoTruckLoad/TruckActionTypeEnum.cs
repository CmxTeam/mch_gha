﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.CargoTruckLoad
{
    public enum TruckActionTypeEnum
    {
        Close,
        Remove,
        Finalize
    }
}
