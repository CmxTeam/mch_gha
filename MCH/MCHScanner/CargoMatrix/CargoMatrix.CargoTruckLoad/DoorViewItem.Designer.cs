﻿namespace CargoMatrix.CargoTruckLoad
{
    partial class DoorViewItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.btnBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.btnEdit = new CargoMatrix.UI.CMXPictureButton();

            this.SuspendLayout();
          
            // 
            // ButtonBrowse
            // 
            this.btnBrowse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBrowse.Image = global::Resources.Graphics.Skin._3dots;
            this.btnBrowse.Location = new System.Drawing.Point(203, 5);
            this.btnBrowse.Name = "ButtonBrowse";
            this.btnBrowse.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.btnBrowse.Size = new System.Drawing.Size(32, 32);
            this.btnBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnBrowse.TransparentColor = System.Drawing.Color.White;
            this.btnBrowse.Click += new System.EventHandler(btnBrowse_Click);
            this.btnBrowse.BringToFront();

            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.SystemColors.Control;
            this.btnEdit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnEdit.Image = CargoMatrix.Resources.Skin.clipboardButton;
            this.btnEdit.Location = new System.Drawing.Point(167, 5);
            this.btnEdit.Name = "ButtonEdit";
            this.btnEdit.PressedImage = CargoMatrix.Resources.Skin.clipboardButton_over;
            this.btnEdit.Size = new System.Drawing.Size(32, 32);
            this.btnEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEdit.TabIndex = 3;
            this.btnEdit.TabStop = false;
            this.btnEdit.TransparentColor = System.Drawing.Color.White;
            this.btnEdit.Click += new System.EventHandler(btnEdit_Click);
            this.btnEdit.BringToFront();
            
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(4, 5);
            this.pictureBox1.Name = "pcxLogo";
            this.pictureBox1.Image = CargoMatrix.Resources.Icons.Door;
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TransparentColor = System.Drawing.Color.White;

            this.labelLine3.Visible = false;
            this.buttonDetails.Visible = false;
            this.panelIndicators.Visible = false;

            this.pictureBox1.Image = CargoMatrix.Resources.Icons.Door;

            this.panelCutoff.Visible = false;

            this.LabelConfirmation = "Enter door name";

            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.btnEdit);

            this.previousHeight = 45;

            this.Size = new System.Drawing.Size(240, this.previousHeight);
            this.title.Size = new System.Drawing.Size(125, 14);
            this.labelLine2.Size = new System.Drawing.Size(125, 14);

            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.previousHeight *= (int)(this.CurrentAutoScaleDimensions.Height / 96F);

            this.ResumeLayout();
        }

        private CargoMatrix.UI.CMXPictureButton btnBrowse;
        private CargoMatrix.UI.CMXPictureButton btnEdit;
      
        #endregion
    }
}
