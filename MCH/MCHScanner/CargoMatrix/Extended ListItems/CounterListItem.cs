﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class CounterListItem : ListItem// UserControl
    {
        public CounterListItem(int count)
        {
            InitializeComponent();
            cmxCounterIcon1.Value = count;
            cmxCounterIcon1.Image = ListItemsResource.Delivery_Truck_Furniture;

        }
        public int Value
        {
            set
            {
                cmxCounterIcon1.Value = value;
            }
        }
    }
}
