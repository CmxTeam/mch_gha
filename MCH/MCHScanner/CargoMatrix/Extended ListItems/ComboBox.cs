﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public struct ComboBoxItemData
    {
        public ComboBoxItemData(string Name, Image Icon)
        {
            name = Name;
            icon = Icon;
        }
        
        public string name;
        public Image icon;

    }
    public partial class ComboBox : ListItem// StandardListItem // UserControl
    {
        List<ComboBoxItemData> m_itemsList;
        //string m_titleText;
        bool m_firstTimeSelection = false;
        //int m_selectedIndex = -1;
        SmoothListBoxBase m_smoothList;
        int m_previousHeight;
        //public ComboBox()
        //{
        //    InitializeComponent();
            
        //    itemPicture.Image = null;// ListItemsResource.Notepad_Information;
            
        //}
        public ComboBox(string text, Image image, List<ComboBoxItemData> items)
        {
            
            InitializeComponent();
            //title.Width -= (Width - pictureBoxCombo.Left);
            m_previousHeight = this.Height;
            m_itemsList = items;
            itemPicture.Image = image;
            labelHeading.Text = text;
            this.title.Text = items[0].name;

            
            //this.backColor = Color.White;
            //itemPicture.TransparentColor = Color.White; ;// bmp.GetPixel(0, 0);
            pictureBoxCombo.Image = ListItemsResource.Expand;
            pictureBoxDottedLine.Image = ListItemsResource.DottedLineVertical;

            FirstTimeLoad();
            
        }
        public override void SelectedChanged(bool isSelected)
        {
            if (isSelected)
            {
                if (m_firstTimeSelection == false)
                    FirstTimeLoad();
                pictureBoxCombo.Image = ListItemsResource.Collapse;
                Height = m_previousHeight + m_smoothList.Height + 5;
                pictureBoxDottedLine.Visible = true;
                
                
            }
            else
            {
                
                pictureBoxCombo.Image = ListItemsResource.Expand;
                Height = m_previousHeight;
                pictureBoxDottedLine.Visible = false;

            }

            base.SelectedChanged(isSelected);

            Control control = this.Parent;
            while (control != null)
            {
                if (control is SmoothListBoxBase)
                {
                    if ((control as SmoothListBoxBase).selectedItemsMap.ContainsKey(this))
                    {
                        (control as SmoothListBoxBase).RefreshScroll();
                        

                    }
                }
                control = control.Parent;
            }
        }
        public override void Focus(bool focused)
        {
            //base.Focus(focused);
        }
        private void FirstTimeLoad()
        {
            if (m_itemsList != null)
            {
                if (m_firstTimeSelection == false)
                {
                    m_firstTimeSelection = true;

                    this.SuspendLayout();

                    m_smoothList = new SmoothListBoxBase();
                    m_smoothList.ListItemClicked += new ListItemClickedHandler(m_smoothList_ListItemClicked);
                    m_smoothList.UnselectEnabled = false;
                    


                    if (m_itemsList.Count > 0)
                    {
                        int i = 0;
                        for (; i < m_itemsList.Count - 1; i++)
                        {
                            m_smoothList.AddItem2(new ComboBoxItem(m_itemsList[i].name, m_itemsList[i].icon, false ));
                        }
                        m_smoothList.AddItem2(new ComboBoxItem(m_itemsList[i].name, m_itemsList[i].icon, true));
                    }
                    


                    m_smoothList.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                    m_smoothList.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

                    

                    m_smoothList.Width = ClientSize.Width - 12;
                    m_smoothList.Left = 17;
                    m_smoothList.Top = m_previousHeight;

                    m_smoothList.LayoutItems();

                    m_smoothList.Height = m_smoothList.itemsPanel.Height;

                    Controls.Add(m_smoothList);

                    this.ResumeLayout();
                }
            }
        }

        void m_smoothList_ListItemClicked(SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            SelectedChanged(false);
            //m_smoothList.selectedItemsMap[listItem] = false;
            this.title.Text = listItem.Name;
            if (listItem is ComboBoxItem)
            {
                itemPicture.Image = (listItem as ComboBoxItem).pictureboxIcon.Image;
            }
            Control control = this.Parent;
            while (control != null)
            {
                if (control is SmoothListBoxBase)
                {
                    if ((control as SmoothListBoxBase).selectedItemsMap.ContainsKey(this))
                    {
                        (control as SmoothListBoxBase).selectedItemsMap[this] = false;
                        (control as SmoothListBoxBase).LayoutItems();

                    }
                }
                control = control.Parent;
            }
            //throw new NotImplementedException();
        }
        public string SelectedItemName
        {
            get
            {
                int index = SelectedID;
                if(index >=0)
                    return m_smoothList.Items[index].Name;
                return null;
            }
        }

        public int SelectedID 
        {
            get
            { 
                foreach (Control key in m_smoothList.selectedItemsMap.Keys)
                {
                    if (m_smoothList.selectedItemsMap[key])
                    {
                        return m_smoothList.itemsPanel.Controls.GetChildIndex(key);
                        
                    }
                }

                return -1;
            }
            set
            {
                Control key = m_smoothList.itemsPanel.Controls[value];
                m_smoothList.Reset();
                m_smoothList.selectedItemsMap[key] = true;
                if (key is IExtendedListItem)
                {
                    (key as IExtendedListItem).SelectedChanged(true);
                    //(key as IExtendedListItem).Focus(true);
                    this.title.Text = key.Name;

                }
            }
        }

        private void ComboBox_Resize(object sender, EventArgs e)
        {
            if (m_smoothList != null)
            {
                m_smoothList.Width = ClientSize.Width - 10;
                m_smoothList.Left = 5;
            }
        }
    }
}
