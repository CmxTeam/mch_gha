﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class ViewMasterbillItem : UserControl
    {
        public event EventHandler Enter;
        public string Text, Carrier;
        public ViewMasterbillItem()
        {
            InitializeComponent();
        }

       
        private void buttonEnter_Click_1(object sender, EventArgs e)
        {
            if (Enter != null)
            {
                Text = textBoxHousebill.Text;
                Carrier = this.cmxTextBox1.Text;
                Enter(sender, e);
            }
        }
    }
}
