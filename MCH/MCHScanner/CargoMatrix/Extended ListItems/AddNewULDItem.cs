﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListBox.UI;
using SmoothListBox.UI.ListItems;

namespace SmoothListBox.UI.ListItems
{
    public partial class AddNewULDItem : StandardListItem
    {
        public EventHandler OnViewerClick;
        public EventHandler OnEnterClick;
        public string taskID;
        //CargoMatrix.Communication.Data.TaskItem m_taskItem;
        

        private CargoMatrix.UI.CMXTextBox cmxTextBox1;
        private OpenNETCF.Windows.Forms.PictureBox2 buttonEnter;
        private OpenNETCF.Windows.Forms.PictureBox2 buttonCancel;
        
        private System.Windows.Forms.Label labelConfirm;

        bool m_firstTimeSelection = false;

      

        public AddNewULDItem()
        {
            
            InitializeComponent();
            title.Text = "Add new ULD";
            itemPicture.Image = ListItemsResource.Shipping_Crate;
            previousHeight = this.Height;
       
            
        }

       
       
        int previousHeight;
        public override void SelectedChanged(bool isSelected)
        {
            m_selected = isSelected;
            
            base.SelectedChanged(isSelected);

            //if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID != CargoMatrix.Communication.Data.UserTypes.Admin)
            {


                if (isSelected)
                {
                    ////////////////////////////////
                    if (m_firstTimeSelection == false)
                    {
                        m_firstTimeSelection = true;

                        this.SuspendLayout();

                        this.cmxTextBox1 = new CargoMatrix.UI.CMXTextBox();
                        this.buttonEnter = new OpenNETCF.Windows.Forms.PictureBox2();
                        this.buttonCancel  = new OpenNETCF.Windows.Forms.PictureBox2();
                        
                        this.labelConfirm = new System.Windows.Forms.Label();
                        // cmxTextBox1
                        // 
                        this.cmxTextBox1.Location = new System.Drawing.Point(3, 60);
                        //this.cmxTextBox1.Name = "cmxTextBox1";
                        this.cmxTextBox1.Size = new System.Drawing.Size(80, 28);
                        this.cmxTextBox1.TabIndex = 2;
                        this.cmxTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmxTextBox1_KeyDown);
                        // 
                        // buttonEnter
                        // 
                        this.buttonEnter.Location = new System.Drawing.Point(85, 60);
                        //this.buttonEnter.Name = "buttonEnter";
                        this.buttonEnter.Size = new System.Drawing.Size(40, 28);
                        this.buttonEnter.Click += new System.EventHandler(this.buttonEnter_Click);
                        this.buttonEnter.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonEnter_MouseDown);
                        this.buttonEnter.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonEnter_MouseUp);
                        this.buttonEnter.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.buttonEnter.Image = ListItemsResource.manual_entry_ok;
                        this.buttonEnter.TransparentColor = Color.White;
                        // 
                        // buttonCancel
                        // 
                        this.buttonCancel.Location = new System.Drawing.Point(128, 60);
                        //this.buttonEnter.Name = "buttonEnter";
                        this.buttonCancel.Size = new System.Drawing.Size(40, 28);
                        this.buttonCancel.Click += new EventHandler(buttonCancel_Click);
                        this.buttonCancel.MouseDown += new MouseEventHandler(buttonCancel_MouseDown);
                        this.buttonCancel.MouseUp += new MouseEventHandler(buttonCancel_MouseUp);
                        this.buttonCancel.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.buttonCancel.Image = ListItemsResource.manual_entry_cancel;
                        this.buttonCancel.TransparentColor = Color.White;
                        
                       
                        // 
                        // labelConfirm
                        // 
                        this.labelConfirm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
                        this.labelConfirm.ForeColor = System.Drawing.Color.Black;
                        this.labelConfirm.Location = new System.Drawing.Point(3, 45);
                        //this.labelConfirm.Name = "labelConfirm";
                        this.labelConfirm.Size = new System.Drawing.Size(234, 12);
                        this.labelConfirm.Text = "Enter ULD number.";
                        

                        this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
                        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

                        this.Controls.Add(this.labelConfirm);
                        this.Controls.Add(this.cmxTextBox1);
                        this.Controls.Add(this.buttonEnter);
                        this.Controls.Add(this.buttonCancel);
                        
                        

                        this.ResumeLayout();
                    }
                    ////////////////////////////////
                    labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = true;

                    Cursor.Current = Cursors.Default;
                    
                    this.Height = buttonEnter.Bottom + 4;
                    cmxTextBox1.SelectAll();
                    cmxTextBox1.Focus();
                    //buttonEnterClicked = false;
                }
                else
                {
                    //if (this.ClientRectangle.Width == 480)
                    //    this.Height = 112;// previousHeight;
                    //else
                    //    this.Height = 56;
                    Height = previousHeight;

                    if (m_firstTimeSelection)
                        labelConfirm.Visible = cmxTextBox1.Visible = buttonEnter.Visible = buttonCancel.Visible = false;
                }
            }
            //else
            //{
            //    Cursor.Current = Cursors.WaitCursor;
            //    if (OnEnterClick != null)
            //        OnEnterClick(this, EventArgs.Empty);
                
                
            //}
            
        }

        void buttonCancel_MouseUp(object sender, MouseEventArgs e)
        {
            buttonCancel.Image = ListItemsResource.manual_entry_cancel; 
        }

        void buttonCancel_MouseDown(object sender, MouseEventArgs e)
        {
            buttonCancel.Image = ListItemsResource.manual_entry_cancel_over;
        }

        void buttonCancel_Click(object sender, EventArgs e)
        {
            cmxTextBox1.Text = "";
            //SelectedChanged(false);
        }
        //bool buttonEnterClicked = false;
        private void buttonEnter_Click(object sender, EventArgs e)
        {
            
            Cursor.Current = Cursors.WaitCursor;
            buttonEnter.Image = ListItemsResource.manual_entry_ok_over;
            buttonEnter.Refresh();

            //if (m_taskItem.actualBill.ToLower() == cmxTextBox1.Text.ToLower())
            {
               if (OnEnterClick != null)
                OnEnterClick(this, e);

                Cursor.Current = Cursors.Default;
            }
            //else
            //{

            //    Cursor.Current = Cursors.Default;
            //    CargoMatrix.UI.CMXMessageBox.Show("Text entered does not match. Try again.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            //    buttonEnter.Image = ListItemsResource.manual_entry_ok;
            //    buttonEnter.Refresh();
            //    cmxTextBox1.SelectAll();
            //    cmxTextBox1.Focus();
            //}
            

        }
        private void buttonEnter_MouseDown(object sender, MouseEventArgs e)
        {
            buttonEnter.Image = ListItemsResource.manual_entry_ok_over;
        }

        private void buttonEnter_MouseUp(object sender, MouseEventArgs e)
        {
            buttonEnter.Image = ListItemsResource.manual_entry_ok;
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            if (OnViewerClick != null)
            {
                Cursor.Current = Cursors.WaitCursor;
                OnViewerClick(this, e);
            }


        }
        

        private void cmxTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (this.m_selected)
                        buttonEnter_Click(sender, EventArgs.Empty);
                    break;
            }


        }

      
        

    }
}



