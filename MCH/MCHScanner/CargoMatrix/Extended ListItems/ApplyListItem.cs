﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class ApplyListItem : UserControl
    {
        SmoothListBoxBase m_parentSmoothList;
        
        public ApplyListItem()
        {
            InitializeComponent();
            buttonApply.Image = ListItemsResource.filter_apply;
        
        }
        Font titleFont = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);
        string m_buttonText = "Apply";
        private void buttonApply_Paint(object sender, PaintEventArgs e)
        {
            SizeF textSize = e.Graphics.MeasureString(m_buttonText, titleFont);
            int x = (buttonApply.Width - (int)textSize.Width) / 2;
            int y = (buttonApply.Height - (int)textSize.Height) / 2;
            e.Graphics.DrawString(m_buttonText, titleFont, new SolidBrush(Color.White), x, y);
        }

        private void buttonApply_MouseDown(object sender, MouseEventArgs e)
        {
            buttonApply.Image = ListItemsResource.filter_apply_over;
        }

        private void buttonApply_MouseUp(object sender, MouseEventArgs e)
        {
            buttonApply.Image = ListItemsResource.filter_apply;

        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            buttonApply.Refresh();

        }

    }
}
