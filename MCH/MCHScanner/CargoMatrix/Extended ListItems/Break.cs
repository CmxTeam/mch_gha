﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class Break : UserControl
    {
        public Break()
        {
            InitializeComponent();
        }
        public Break(int height, Color color)
        {
            InitializeComponent();
            this.Height = height;
            this.BackColor = color;
        }
    }
}
