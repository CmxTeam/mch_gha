﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class CheckInManifestItem : ListItem, IExtendedListItem
    {
        private bool onOddRow = false;
        private bool isSelected = false;

        public CheckInManifestItem()
        {
            InitializeComponent();
        }

        public CheckInManifestItem(string terminal, string location, string weight, Image alert)
        {
            InitializeComponent();
            this.Terminal.Text = terminal;
            this.labelLocation.Text = location;
            this.Weight.Text = weight;
            this.pictureBoxAlert.Image = alert;
            SelectedChanged(false);
        }

        #region IExtendedListItem Members

        public override void SelectedChanged(bool isSelected)
        {
            this.isSelected = isSelected;
            
            SetBackColor();
            
        }


        public override void PositionChanged(int index)
        {
            onOddRow = (index & 1) == 0;
            SetBackColor();

            
        }
        private void SetBackColor()
        {
            //if (isSelected)
            //    BackColor = Color.AliceBlue;// onOddRow ? Color.AliceBlue : Color.LightBlue;
            //else
            //    BackColor = onOddRow ? Color.White : Color.Silver;
        }
        public virtual void Focused(bool focused)
        {
        }
        #endregion

    }
}
