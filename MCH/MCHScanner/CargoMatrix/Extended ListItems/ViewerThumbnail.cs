﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmoothListBox.UI.ListItems
{
    public partial class ViewerThumbnail : ListItem
    {
        Image m_ImageRegular;
        Image m_ImageOver;
        public ViewerThumbnail(int id)
        {
            InitializeComponent();
            ID = id;
        }
        public override void SelectedChanged(bool isSelected)
        {

            //base.SelectedChanged(isSelected);
            if (isSelected)
                pictureBox1.Image = m_ImageOver;
            else
                pictureBox1.Image = m_ImageRegular;
        }
        public ViewerThumbnail()
        {
            InitializeComponent();
            
        }
        public Image ImageRegular
        {
            set
            {
                m_ImageRegular = value;
                pictureBox1.Image = value;
            }
        }
        public Image ImageOver
        {
            set
            {
                m_ImageOver = value;
            }
        }
    
    
    }
}
