﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmoothListbox.ListItems;
using CargoMatrix.UI;

namespace CargoMatrix.CargoUtilities
{
    public partial class UtilitiesList : SmoothListbox.SmoothListbox2
    {
        public UtilitiesList()
        {
            InitializeComponent();

            this.panel1.Height = 0;
            this.panelHeader2.Height = 0;
            this.smoothListBoxMainList.MultiSelectEnabled = false;
            this.TitleText = "CargoUtilities";

            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(UtilitiesList_ListItemClicked);
        }

        void UtilitiesList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            StandardListItem item = (StandardListItem)listItem;

            ScanActionTypeEnum actionType = ScanActionTypeEnum.Browse;

            switch ((ListItemType)item.ID)
            {
                case ListItemType.SHIPMENTLOOKUP:

                    CMXAnimationmanager.DisplayForm(new EnterNumberControl(actionType, WorkItemTypeEnum.HOUSEBILL));

                    break;

                case ListItemType.PRINTLABELS:

                    CMXAnimationmanager.DisplayForm(new PrintLabelsList());

                    actionType = ScanActionTypeEnum.PrinLabels;

                    break;

                case ListItemType.LOCATIONMANAGER:

                    CMXAnimationmanager.DisplayForm(new WareHouseList());

                    break;

                case ListItemType.RELEASEPIECE:
                    if (CargoMatrix.Communication.Utilities.IsAdmin ||
                        DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {
                        this.Refresh();
                        ReleasePieces.ReleaseHawbPiece();
                    }
                    smoothListBoxMainList.Reset();
                    break;
                case ListItemType.EDITHAWBSKEL:
                    if (CargoMatrix.Communication.Utilities.IsAdmin ||
                        DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {
                        this.Refresh();
                        EditHawbSkel.Edit();
                    }
                    break;
                case ListItemType.CALIBRATEPRINTER_Bluetooth:

                    CMXAnimationmanager.DisplayForm(new EnterNumberControl(ScanActionTypeEnum.CalibratePrinter_Bluetooth, WorkItemTypeEnum.PRINTER_Bluetooth));
                    break;

                case ListItemType.CALIBRATEPRINTER_Wireless:

                    CMXAnimationmanager.DisplayForm(new EnterNumberControl(ScanActionTypeEnum.CalibratePrinter_Wireless, WorkItemTypeEnum.PRINTER_Wireless));
                    break;
                case ListItemType.BARCODEPARSER:
                    CMXAnimationmanager.DisplayForm(new BarcodeParser());
                    break;
                case ListItemType.MESSAGES:
                    //CargoMatrix.Messageing.MessageManager.Instance.DipslayMessages();
                    break;
                case ListItemType.SCANNERCONFIG:
                    DisplayConfigFile();
                    break;
                case ListItemType.GOTOMAINMENU:
                    CMXAnimationmanager.DisplayDefaultForm();
                    break;
                case ListItemType.LOGOUT:
                    if (CargoMatrix.UI.CMXAnimationmanager.ConfirmLogout())
                        CargoMatrix.UI.CMXAnimationmanager.DoLogout();
                    break;
                case ListItemType.CARGORELOCATOR:
                    CMXAnimationmanager.DisplayForm(new CargoRelocator());
                    break;
            }
            sender.Reset();


        }

        private static void DisplayConfigFile()
        {
            string msg = string.Concat(Communication.Settings.Instance.ConnectionName, Environment.NewLine,
                Communication.Settings.Instance.Gateway, Environment.NewLine,
                Communication.Settings.Instance.URLPath);
            UI.CMXMessageBox.Show(msg, "Scanner Config", CMXMessageBoxIcon.Message);
        }

        public override void LoadControl()
        {
            this.smoothListBoxMainList.RemoveAll();
            this.AddMainListItem(new StandardListItem("CARGO RELOCATOR", null, (int)ListItemType.CARGORELOCATOR));

            this.AddMainListItem(new StandardListItem("SHIPMENT LOOKUP", null, (int)ListItemType.SHIPMENTLOOKUP));

            this.AddMainListItem(new StandardListItem("PRINT LABELS", CargoMatrix.Resources.Skin.Printer, (int)ListItemType.PRINTLABELS));

            this.AddMainListItem(new StandardListItem("LOCATION MANAGER", CargoMatrix.Resources.Icons.Hotel_24x24, (int)ListItemType.LOCATIONMANAGER));

            this.AddMainListItem(new StandardListItem("RELEASE HAWB PIECES", null, (int)ListItemType.RELEASEPIECE));

            this.AddMainListItem(new StandardListItem("EDIT HOUSEBILL", null, (int)ListItemType.EDITHAWBSKEL));

            this.AddMainListItem(new StandardListItem("CALIBRATE BLUETOOTH PRINTER", null, (int)ListItemType.CALIBRATEPRINTER_Bluetooth));

            this.AddMainListItem(new StandardListItem("CALIBRATE WIRELESS PRINTER", null, (int)ListItemType.CALIBRATEPRINTER_Wireless));

            this.AddMainListItem(new StandardListItem("BARCODE PARSER", null, (int)ListItemType.BARCODEPARSER));

            //this.AddMainListItem(new StandardListItem("VIEW MESSAGES", null, (int)ListItemType.MESSAGES));
            this.AddMainListItem(new StandardListItem("SCANNER CONFIG", null, (int)ListItemType.SCANNERCONFIG));

            //this.AddMainListItem(new StandardListItem("GO TO Main Menu", Resources.Icons.Menu, (int)ListItemType.GOTOMAINMENU));

            //this.AddMainListItem(new StandardListItem("LOG OUT", Resources.Icons.Exit, (int)ListItemType.LOGOUT));
        }


        enum ListItemType
        {
            SHIPMENTLOOKUP,
            PRINTLABELS,
            AQM,
            LOCATIONMANAGER,
            RELEASEPIECE,
            EDITHAWBSKEL,
            CALIBRATEPRINTER_Bluetooth,
            CALIBRATEPRINTER_Wireless,
            BARCODEPARSER,
            MESSAGES,
            SCANNERCONFIG,
            LOGOUT,
            GOTOMAINMENU,
            CARGORELOCATOR
        }
    }
}
