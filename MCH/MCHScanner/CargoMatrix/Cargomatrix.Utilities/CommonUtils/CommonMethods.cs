﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CargoMatrix.Communication.ScannerUtilityWS;
using CMXExtensions;
using CargoMatrix.Communication.WarehouseManager;
using CustomUtilities;

namespace CargoMatrix.CargoUtilities
{
    public static class CommonMethods
    {
        private static Action<SoapExceptionManager.SoapExceptionDetails> ErrorAction = (ex) => CargoMatrix.UI.CMXMessageBox.Show(ex.Details, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);

        public static void DisplayBillViewer(string hawbNo, int left, int width)
        {
            Cursor.Current = Cursors.WaitCursor;

            CargoMatrix.Viewer.HousebillViewer billViewer = new CargoMatrix.Viewer.HousebillViewer(hawbNo);

            billViewer.Location = new Point(left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
            Cursor.Current = Cursors.Default;
        }

        public static void PrintHouseBillLabels(CargoMatrix.Communication.ScannerUtilityWS.HouseBillItem houseBillItem)
        {
            CustomUtilities.HawbLabelPrinter.Show(houseBillItem.HousebillId, houseBillItem.HousebillNumber, houseBillItem.TotalPieces);
        }

        public static void PrintMasterBillLabels(CargoMatrix.Communication.ScannerUtilityWS.MasterBillItem masterBill)
        {
            MawbLabelPrinter.Show(masterBill);
        }

        private static LabelPrinter GetPrinter(string header, LabelTypes labelType)
        {
            Cursor.Current = Cursors.WaitCursor;

            MessageListBox printersList = new MessageListBox();

            printersList.HeaderText = header;

            printersList.HeaderText2 = "Select From Available Printers";
            printersList.MultiSelectListEnabled = false;
            printersList.OneTouchSelection = false;

            StandardListItem<LabelPrinter> defaultPrinterItem = null;
            var printers = CargoMatrix.Communication.ScannerUtility.Instance.GetPrinters(labelType);

            foreach (var printer in printers)
            {
                Image image = null;

                switch (printer.PrinterType)
                {
                    case PrinterType.MobileLabel: // bluetooth
                        image = printer.IsDefault ? CargoMatrix.Resources.Icons.printerBtooth_check : CargoMatrix.Resources.Icons.printerBtooth;
                        break;

                    case PrinterType.MobileWireless:

                        image = printer.IsDefault ? CargoMatrix.Resources.Icons.printer_wifi_check : CargoMatrix.Resources.Icons.printer_wifi;
                        break;

                    default:
                        image = printer.IsDefault ? CargoMatrix.Resources.Skin.Printer_Check : CargoMatrix.Resources.Skin.Printer;
                        break;
                }

                var printerListItem = new StandardListItem<LabelPrinter>(printer.PrinterName, image, printer.PrinterId, printer);
                printersList.AddItem(printerListItem);

                if (printer.IsDefault == true)
                {
                    defaultPrinterItem = printerListItem;
                }
            }

            if (defaultPrinterItem != null)
            {
                defaultPrinterItem.SelectedChanged(true);
                defaultPrinterItem.Focus(true);
                printersList.OkEnabled = true;
            }

            Cursor.Current = Cursors.Default;

            DialogResult dialogResult = printersList.ShowDialog();

            if (dialogResult != DialogResult.OK) return null;

            if (defaultPrinterItem == null &&
                printersList.SelectedItems.Count <= 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Printer was not selected", "Print error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return null;
            }

            var selectedPrinterItem = printersList.SelectedItems.Count > 0 ? (StandardListItem<LabelPrinter>)printersList.SelectedItems[0] : defaultPrinterItem;

            return selectedPrinterItem.ItemData;
        }


        public static void PrintUldLables(string uldNumber)
        {
            var printer = CommonMethods.GetPrinter(string.Format("Print ulds labels for {0}", uldNumber), LabelTypes.ULD);

            if (printer == null) return;

            if (printer.PrinterType == PrinterType.MobileLabel ||
                printer.PrinterType == PrinterType.MobileWireless)
            {
                var mobilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(printer);
                mobilePrinter.UldPrint(uldNumber);

                Cursor.Current = Cursors.Default;

                Application.DoEvents();

                return;
            }


            Cursor.Current = Cursors.WaitCursor;

            var uldId = CargoMatrix.Communication.ScannerUtility.Instance.GetUldIdByNumber(uldNumber);

            Cursor.Current = Cursors.Default;

            if (uldId.HasValue == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Uld was not found", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }


            Cursor.Current = Cursors.WaitCursor;

            bool result = CargoMatrix.Communication.ScannerUtility.Instance.PrintULDLabel(uldId.Value, printer.PrinterId, 1);

            Cursor.Current = Cursors.Default;
            if (result == true)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Labels printing has been queued on " + printer.PrinterName, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Error occurred while printing uld labels", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            }

        }

        public static void PrintLocationLabels(LocationItem location)
        {
            var printer = CommonMethods.GetPrinter("Print locations labels", LabelTypes.LOCATION);

            if (printer == null) return;

            if (printer.PrinterType == PrinterType.MobileLabel ||
                printer.PrinterType == PrinterType.MobileWireless)
            {

                var mobilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(printer);
                mobilePrinter.LocationPrint(location);

                Cursor.Current = Cursors.Default;

                Application.DoEvents();

                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var result = CargoMatrix.Communication.ScannerUtility.Instance.PrintLocationLabel(location.Location, printer.PrinterId, 1);

            Cursor.Current = Cursors.Default;

            if (result == true)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Labels printing has been queued on " + printer.PrinterName, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Error occurred while printing location labels", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            }
        }

        public static void PrintTruckLabel(string truckName)
        {
            var printer = CommonMethods.GetPrinter("Print locations labels", LabelTypes.LOCATION);

            if (printer == null) return;

            if (printer.PrinterType == PrinterType.MobileLabel ||
                printer.PrinterType == PrinterType.MobileWireless)
            {
                var mobilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(printer);
                mobilePrinter.TruckPrint(truckName);

                Application.DoEvents();

                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var locationID = CargoMatrix.Communication.ScannerUtility.Instance.GetLocationId(truckName);

            Cursor.Current = Cursors.Default;

            if (locationID.HasValue == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Truck was not found", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;

            }

            Cursor.Current = Cursors.WaitCursor;

            var result = CargoMatrix.Communication.ScannerUtility.Instance.PrintTruckLabels(new int[] { locationID.Value }, printer.PrinterId, 1);

            Cursor.Current = Cursors.Default;

            if (result == true)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Labels printing has been queued on " + printer.PrinterName, "Success", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show("Error occurred while printing truck labels", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            }
        }

        public static void PrintUserLabel(string userName)
        {
            var printer = CommonMethods.GetPrinter("Print locations labels", LabelTypes.LOCATION);

            if (printer == null) return;

            if (printer.PrinterType == PrinterType.MobileLabel ||
                printer.PrinterType == PrinterType.MobileWireless)
            {
                var mobilePrinter = new CustomUtilities.MobilePrinting.MobilePrinterUtility(printer);
                mobilePrinter.UserPrint(userName);

                Application.DoEvents();

                return;
            }
        }


        public static bool CreateWareHouse(out IWarehouse wareHouse)
        {
            wareHouse = null;

            CreateInstanceForm cwForm = new CreateWareHouseForm();
            var result = cwForm.ShowDialog();

            if (result != DialogResult.OK) return false;

            var item = new CargoMatrix.Communication.WSWarehouseManager.WarehouseListItem();

            item.Code = cwForm.InstanceCode;
            item.Name = cwForm.InstanceName;



            wareHouse = WarehouseManagerProvider.Instance.AddWareHouse(item, ErrorAction);

            return wareHouse != null;
        }

        public static bool CreateRegion(IWarehouse wareHouse, out IRegion region)
        {
            region = null;

            CreateInstanceForm crForm = new CreateRegionForm();
            var result = crForm.ShowDialog();

            if (result != DialogResult.OK) return false;

            var item = new CargoMatrix.Communication.WSWarehouseManager.WarehouseRegionListItem();

            item.Code = crForm.InstanceCode;
            item.Name = crForm.InstanceName;
            item.WarehouseID = wareHouse.RecID;

            region = WarehouseManagerProvider.Instance.AddRegion(item, ErrorAction);

            return region != null;
        }

        public static bool CreateLocation(IRegion region, CMXBarcode.ScanObject scanItem, out ILocation location)
        {
            location = null;

            var item = new CargoMatrix.Communication.WSWarehouseManager.WarehouseLocationListItem();

            item.Code = scanItem.Location;
            item.Name = item.Code;
            item.RegionTypeID = region.RecID;
            item.WarehouseID = region.WarehouseID;
            item.SetLocationType(scanItem.BarcodeType);
            item.TypeID = (int)item.LocationType;
            location = WarehouseManagerProvider.Instance.AddLocation(item, ErrorAction);

            if (location == null) return false;

            return true;
        }


        public static Image GetLocationIcon(LocationTypeEnum locationType)
        {
            switch (locationType)
            {
                case LocationTypeEnum.Area: return CargoMatrix.Resources.Icons.Selection;

                case LocationTypeEnum.Door: return CargoMatrix.Resources.Icons.Door;

                case LocationTypeEnum.ScreeningArea: return CargoMatrix.Resources.Icons.Selection;

                default: return null;
            }
        }

        public static string GetLocationDescription(LocationTypeEnum locationType)
        {
            switch (locationType)
            {
                case LocationTypeEnum.Area: return "AREA";

                case LocationTypeEnum.Door: return "DOOR";

                case LocationTypeEnum.ScreeningArea: return "SCREENING AREA";

                default: return string.Empty;
            }
        }

        public static BarcodeType GetBarcodeType(WorkItemTypeEnum workItemType)
        {
            switch (workItemType)
            {
                case WorkItemTypeEnum.HOUSEBILL: return BarcodeType.Housebill;

                case WorkItemTypeEnum.MASTERBILL: return BarcodeType.Masterbill;

                case WorkItemTypeEnum.ULD: return BarcodeType.ULD;

                case WorkItemTypeEnum.LOCATION: return BarcodeType.Location;

                case WorkItemTypeEnum.TRUCK: return BarcodeType.Truck;

                case WorkItemTypeEnum.ONHAND: return BarcodeType.OnHand;

                case WorkItemTypeEnum.PRINTER_Bluetooth:
                case WorkItemTypeEnum.PRINTER_Wireless:
                    return BarcodeType.Printer;

                case WorkItemTypeEnum.USER: return BarcodeType.User;

                default: return BarcodeType.Housebill;
            }
        }

        public static string GetScanWindowHeader(WorkItemTypeEnum workItemType)
        {
            switch (workItemType)
            {
                case WorkItemTypeEnum.HOUSEBILL: return "Scan/Enter housebill number";

                case WorkItemTypeEnum.MASTERBILL: return "Scan/Enter masterbill number";

                case WorkItemTypeEnum.ULD: return "Scan/Enter uld number";

                case WorkItemTypeEnum.LOCATION: return "Scan/Enter location name";

                case WorkItemTypeEnum.TRUCK: return "Scan/Enter truck number";

                case WorkItemTypeEnum.PRINTER_Bluetooth: return "Scan/Enter printer MAC address";

                case WorkItemTypeEnum.PRINTER_Wireless: return "Scan/Enter printer IP address";

                case WorkItemTypeEnum.USER: return "Scan/Enter user name";
                
                case WorkItemTypeEnum.ONHAND: return "Scan/Enter onhand number";

                default: return string.Empty;
            }
        }

        public static CargoMatrix.Communication.ScannerUtilityWS.LocationTypes GetLocationTypeByBarcodeType(CMXBarcode.BarcodeTypes barcodeType)
        {
            switch (barcodeType)
            {
                case CMXBarcode.BarcodeTypes.Area: return CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Area;

                case CMXBarcode.BarcodeTypes.Door: return CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.Door;

                case CMXBarcode.BarcodeTypes.ScreeningArea: return CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.ScreeningArea;

                default: return CargoMatrix.Communication.ScannerUtilityWS.LocationTypes.NA;
            }
        }

        public static bool GetItemFromUser(WorkItemTypeEnum workItemType, out string scannedText, out string scannedMainPart, out CMXBarcode.ScanObject scanItem)
        {
            var barcodeType = CommonMethods.GetBarcodeType(workItemType);
            string title = CommonMethods.GetScanWindowHeader(workItemType);

            scannedText = null;
            scannedMainPart = null;
            scanItem = null;

            ScanEnterPopup_old scanPopUp = new ScanEnterPopup_old(barcodeType);
            scanPopUp.Title = title;
            scanPopUp.DisplayHousebillPiece = true;
            Application.DoEvents();

            var dialogResult = scanPopUp.ShowDialog();

            if (dialogResult != DialogResult.OK) return false;

            scannedText = scanPopUp.ScannedText;
            scannedMainPart = scanPopUp.ScannedItemMainPart;
            scanItem = scanPopUp.ScanItem;

            return true;
        }

        public static int? GetPortNumber()
        {
            Cursor.Current = Cursors.Default;
            CargoMatrix.Utilities.CountMessageBox cntMsg = new CargoMatrix.Utilities.CountMessageBox();
            cntMsg.HeaderText = "Printer communication port";
            cntMsg.LabelDescription = "Enter communication port";
            cntMsg.LabelReference = "Port";
            cntMsg.PieceCount = 65535;
            cntMsg.MinPiecesCount = 1;
            cntMsg.Readonly = false;

            if (DialogResult.OK != cntMsg.ShowDialog()) return null;

            return cntMsg.PieceCount;
        }

        internal static void PrintOnhandLabels(CargoMatrix.Communication.WSOnHand.CMXShipment onhand)
        {
            CustomUtilities.OnhandLabelPrinter.Show(onhand.ShipmentID, onhand.ShipmentNO, onhand.TotalPieces);
        }
    }
}
