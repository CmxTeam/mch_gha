﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.WarehouseManager;

namespace CargoMatrix.CargoUtilities
{
    public partial class LocationTypeSelector : CargoMatrix.Utilities.MessageListBox
    {
        LocationTypeEnum locationType;

        public LocationTypeEnum LocationType
        {
            get { return locationType; }
        }

        public LocationTypeSelector()
        {
            InitializeComponent();

            this.OkEnabled = false;
            this.OneTouchSelection = true;
            this.MultiSelectListEnabled = false;
            this.HeaderText = "Select Location Type";
            this.HeaderText2 = string.Empty;

            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(LocationTypeSelector_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(LocationTypeSelector_ListItemClicked);
        }

        void LocationTypeSelector_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            this.locationType = (LocationTypeEnum)((SmoothListbox.ListItems.StandardListItem)listItem).ID;
        }

        void LocationTypeSelector_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            this.PopulateContent();
        }

        void PopulateContent()
        {
            var listItem = new SmoothListbox.ListItems.StandardListItem("AREA", null, (int)LocationTypeEnum.Area);
            this.smoothListBoxBase1.AddItem(listItem);

            listItem = new SmoothListbox.ListItems.StandardListItem("DOOR", null, (int)LocationTypeEnum.Door);
            this.smoothListBoxBase1.AddItem(listItem);

            listItem = new SmoothListbox.ListItems.StandardListItem("SCREENING AREA", null, (int)LocationTypeEnum.ScreeningArea);
            this.smoothListBoxBase1.AddItem(listItem);
        }
    }
}