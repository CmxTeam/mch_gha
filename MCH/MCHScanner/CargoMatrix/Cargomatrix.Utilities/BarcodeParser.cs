﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.CargoUtilities
{
    class BarcodeParser : SmoothListbox.SmoothListbox
    {
        private System.Windows.Forms.Label labelType;
        private CustomUtilities.CMXBlinkingLabel labelBlink;
        private bool showAllFields = false;
        public BarcodeParser()
        {
            InitializeComponent();
            this.TitleText = "Barcode Parser";
            BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeParser_BarcodeReadNotify);
            this.LoadOptionsMenu += new EventHandler(BarcodeParser_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(BarcodeParser_MenuItemClicked);
        }

        void BarcodeParser_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        {
            switch ((listItem as CustomListItems.OptionsListITem).ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                    LoadControl();
                    break;
            }
        }

        void BarcodeParser_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }
        public override void LoadControl()
        {
            labelType.Text = string.Empty;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        void BarcodeParser_BarcodeReadNotify(string barcodeData)
        {
            labelBlink.SplashText(barcodeData);
            var scan = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            labelType.Text = scan.BarcodeType.ToString();

            StringBuilder sb = new StringBuilder(Environment.NewLine);
            sb.AppendLine(barcodeData);
            foreach (var fld in scan.GetType().GetFields())
            {
                try
                {
                    object val = fld.GetValue(scan);
                    if (showAllFields)
                        sb.AppendLine(string.Format("{0}:\t {1}", fld.Name, val ?? "null"));
                    else
                        if (val != null && (!(val is int) || (int)val != 0))
                            sb.AppendLine(string.Format("{0}:\t {1}", fld.Name, val));
                }
                catch (Exception ex)
                {

                    CargoMatrix.UI.CMXMessageBox.Show("Error", ex.Message, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                }
            }
            labelType.Text = sb.ToString();
            BarcodeEnabled = true;
        }

        private void InitializeComponent()
        {
            this.labelType = new System.Windows.Forms.Label();
            this.labelBlink = new CustomUtilities.CMXBlinkingLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.labelType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelType.Location = new System.Drawing.Point(10, 34);
            this.labelType.Name = "labelType";
            this.labelType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.labelType.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelType.BackColor = System.Drawing.Color.White;
            this.labelType.Size = new System.Drawing.Size(238, 231);
            // 
            // labelBlink
            // 
            this.labelBlink.Location = new System.Drawing.Point(1, 20);
            this.labelBlink.Name = "labelType";
            this.labelBlink.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelBlink.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelBlink.BackColor = System.Drawing.Color.White;
            this.labelBlink.Size = new System.Drawing.Size(238, 13);
            this.labelBlink.ForeColor = System.Drawing.Color.Red;
            this.labelBlink.Blink = true;
            this.labelBlink.Text = "SCAN BARCODE";
            // 
            // BarcodeParser
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelBlink);
            smoothListBoxMainList.Visible = false;
            this.Name = "BarcodeParser";
            this.Size = new System.Drawing.Size(240, 292);
            this.BackColor = System.Drawing.Color.White;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ResumeLayout(false);

        }
    }
}
