﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication;
using CargoMatrix.Communication.InventoryWS;
using CargoMatrix.Communication.InventoryCom;
using CargoMatrix.CargoUtilities.Relocator;

namespace CargoMatrix.CargoUtilities
{
    public partial class HawbDetailedItem : UserControl
    {
        #region Private/Internal Members
        private CargoMatrix.Communication.InventoryWS.HouseBillItem hawb;
        private CargoMatrix.Viewer.HousebillViewer billViewer;
        internal CargoMatrix.Communication.InventoryWS.HouseBillItem Hawb { get { return hawb; } }
        #endregion

        #region Properties
        public SmoothListBoxOptions RelocatorParent { set; private get; }
        #endregion

        public void DisplayHousebill(CargoMatrix.Communication.InventoryWS.HouseBillItem hawb)
        {
            this.hawb = hawb;
            title.Text = hawb.Reference();
            labelAlias.Text = hawb.AliasNumber;
            labelWeight.Text = string.Format("{0} KGS",hawb.Weight);
            labelPieces.Text = hawb.Pieces();
            labelLoc.Text = hawb.LastLocation;
            labelSender.Text = hawb.ShipperName;
            labelReceiver.Text = hawb.ConsigneeName;
            labelDescr.Text = hawb.Description;
            labelLastScan.Text = hawb.LastScan;


            switch (hawb.Status)
            {
                case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Open:
                case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case CargoMatrix.Communication.InventoryWS.HouseBillStatuses.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;

            }
            panelIndicators.Flags = hawb.Flags;
            panelIndicators.PopupHeader = hawb.Reference();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Visible = true;
        }

        public HawbDetailedItem()
        {
            InitializeComponent();
        }

        private void InitializeImages()
        {
            this.buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonBrowse.Image = global::Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = global::Resources.Graphics.Skin._3dots_over;
            this.buttonDamage.Image = CargoMatrix.Resources.Skin.damage;
            this.buttonDamage.PressedImage = CargoMatrix.Resources.Skin.damage_over;
        }

        #region Private Handlers

        #region Details Button
        /// <summary>
        /// Handles the magnifier button click
        /// </summary>
        private void buttonDetails_Click(object sender, System.EventArgs e)
        {
            DisplayBillViewer(hawb.HousebillNumber);
        }

        private void DisplayBillViewer(string hawbNo)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (billViewer != null)
                billViewer.Dispose();

            billViewer = new CargoMatrix.Viewer.HousebillViewer(hawbNo);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
            Cursor.Current = Cursors.Default;
        }
        #endregion

        #region Damage Button
        private void buttonDamage_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            //the use here for InventoryHouseBilItem is just to build up the DamageCapture
            //no other use is being done with this object here or in the DamageCapture (there is jest get data in the DamageCapture for it)
            InventoryHouseBilItem invHouseBill = new InventoryHouseBilItem(hawb);
            CargoMatrix.DamageCapture.DamageCapture damageCapture = new CargoMatrix.DamageCapture.DamageCapture(invHouseBill);
            
            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionSummary(args.HouseBillID, CargoMatrix.Communication.WSLoadConsol.MOT.Air);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.LoadConsol.Instance.UpdateShippmentCondition(args.HouseBillId, args.ScanModes, args.ShipmentConditions, Forklift.Instance.TaskID, string.Empty, CargoMatrix.Communication.WSLoadConsol.MOT.Air);
            };

            damageCapture.GetHouseBillPiecesIDs = () =>
            {
                return
                    (
                        from item in CargoMatrix.Communication.LoadConsol.Instance.GetHousebillInfo(hawb.HousebillId, Forklift.Instance.TaskID).Pieces
                        select item.PieceId
                    ).ToArray();
            };
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(damageCapture);
            Cursor.Current = Cursors.Default;
        }
        #endregion

        #region Browse Button
        private CargoMatrix.Utilities.MessageListBox actPopup;
        private void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            RelocatorParent.BarcodeEnabled = false;
            if (actPopup == null)
            {
                Control[] actions = new Control[] { 
                new SmoothListbox.ListItems.StandardListItem("SWITCH MODE",null,3),
                new SmoothListbox.ListItems.StandardListItem("PRINT LABELS",null,2),
                new SmoothListbox.ListItems.StandardListItem("REPORT MISSING",null,4),
                new SmoothListbox.ListItems.StandardListItem("REPORT OVER",null,5),
                new SmoothListbox.ListItems.StandardListItem("DOWNLOAD HAWB",null,1), };

                actPopup = new CargoMatrix.Utilities.MessageListBox();
                actPopup.AddItems(actions);
                actPopup.MultiSelectListEnabled = false;
                actPopup.OneTouchSelection = true;
            }
            actPopup.HeaderText = title.Text;
            actPopup.HeaderText2 = "Select action for " + hawb.HousebillNumber;

            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 4:
                        if (Hawb.ScanMode == ScanModes.Count)
                            ReportMissingInCountMode();
                        else
                            ReportMissingInPieceMode();
                        break;
                    case 5:
                        ReportOverInCountMode();
                        break;
                    case 3:
                        SwitchMode();
                        var refreshed = CargoMatrix.Communication.Inventory.Instance.GetHousebillInfo(hawb.HousebillId, Forklift.Instance.TaskID);
                        DisplayHousebill(refreshed);
                        break;
                    case 2:
                        PrintLabels();
                        break;
                    case 1:
                        DownloadHawb();
                        break;
                }
            }
            actPopup.ResetSelection();
            RelocatorParent.BarcodeEnabled = true;
        }

        private void ReportMissingInCountMode()
        {
            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.PieceCount = Hawb.TotalPieces;
            CntMsg.HeaderText = RelocatorResx.Text_CountHeader;
            CntMsg.LabelDescription = RelocatorResx.Text_CountBoxMissing;
            CntMsg.LabelReference = Hawb.Reference();
            CustomUtilities.RemarksMessageBox rmrksMsg = new CustomUtilities.RemarksMessageBox("Missing Remark");

            if (CntMsg.ShowDialog() == DialogResult.OK && rmrksMsg.ShowDialog() == DialogResult.OK)
            {
                TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInCount(Hawb.HousebillId, CntMsg.PieceCount, Forklift.Instance.TaskID, rmrksMsg.TextEntered);
                if (status.TransactionStatus1 == false)
                    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
        }

        private void ReportMissingInPieceMode()
        {
            CargoMatrix.Utilities.MessageListBox notScanned = new CargoMatrix.Utilities.MessageListBox();
            notScanned.MultiSelectListEnabled = true;
            notScanned.UnselectListEnabled = true;
            notScanned.HeaderText = Hawb.Reference();
            notScanned.HeaderText2 = "Select Missing Pieces";
            CustomUtilities.RemarksMessageBox rmrksMsg = new CustomUtilities.RemarksMessageBox("Missing Remark");
            var pieces = from p in CargoMatrix.Communication.Inventory.Instance.GetNotScannedPieces(Hawb.HousebillId, Forklift.Instance.TaskID)
                         select new SmoothListbox.ListItems.StandardListItem("Piece " + p.PieceNumber, null, p.PieceId) as Control;
            notScanned.AddItems(pieces.ToArray<Control>());

            if (DialogResult.OK == notScanned.ShowDialog() && rmrksMsg.ShowDialog() == DialogResult.OK)
            {
                foreach (var item in notScanned.SelectedItems)
                {
                    var tStatus = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInPiece(Hawb.HousebillId, (item as SmoothListbox.ListItems.StandardListItem).ID, Forklift.Instance.TaskID, rmrksMsg.TextEntered);
                    if (!tStatus.TransactionStatus1)
                        CargoMatrix.UI.CMXMessageBox.Show(tStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }
        }

        private void ReportOverInCountMode()
        {
            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.PieceCount = int.MaxValue;
            CntMsg.HeaderText = RelocatorResx.Text_CountHeader;
            CntMsg.LabelDescription = RelocatorResx.Text_CountBoxOver;
            CntMsg.LabelReference = Hawb.Reference();
            CustomUtilities.RemarksMessageBox rmrksMsg = new CustomUtilities.RemarksMessageBox("Missing Remark");

            if (CntMsg.ShowDialog() == DialogResult.OK && rmrksMsg.ShowDialog() == DialogResult.OK)
            {
                CargoMatrix.Communication.Inventory.Instance.ReportOverPiecesInCount(Hawb.HousebillId, CntMsg.PieceCount, Forklift.Instance.TaskID, rmrksMsg.TextEntered);
            }
        }
        
        internal void PrintLabels()
        {
            CustomUtilities.HawbLabelPrinter.Show(hawb.HousebillId, hawb.HousebillNumber, hawb.TotalPieces);
        }

        private void DownloadHawb()
        {
            int downId = CargoMatrix.Communication.Inventory.Instance.DownloadHousebillDetails(hawb.HousebillNumber);
            CargoMatrix.UI.CMXMessageBox.Show("Download for Housbill " + hawb.HousebillNumber + " has been scheduled", "Donwload Scheduled", CargoMatrix.UI.CMXMessageBoxIcon.Success);
            if (RelocatorParent != null)
            {
                //TODO - Einav to complete
                //RelocatorParent.QueueDownload(new HousebillDownload(downId, hawb.HousebillNumber));
            }
        }

        private bool SwitchMode()
        {
            bool retValue = false;
            /// piece Mode -> Count
            if (hawb.ScanMode == ScanModes.Piece)
            {
                string msg = string.Format(RelocatorResx.Text_ChangeModeToCount, hawb.HousebillNumber);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin || DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, Forklift.Instance.TaskID, CargoMatrix.Communication.DTO.ScanModes.Count);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(RelocatorResx.Text_ChangetoCountConfirm, hawb.HousebillNumber), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            // Count Mode -> Piece
            else
            {
                string msg = string.Format(RelocatorResx.Text_ChangeModeToPiece, hawb.HousebillNumber);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, Forklift.Instance.TaskID, CargoMatrix.Communication.DTO.ScanModes.Piece);
                    if (status.TransactionStatus1)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(string.Format(RelocatorResx.Text_ChangeToPieceConfirm, hawb.HousebillNumber), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                        retValue = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        retValue = false;
                    }
                }
            }
            return retValue;
        }
        #endregion

        #endregion
    }
}
