﻿namespace CargoMatrix.CargoUtilities
{
    partial class RegionList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            headerItem = new CustomListItems.HeaderItem();
            headerItem.Label3 = "SELECT REGION";
            headerItem.ButtonClick += new System.EventHandler(AddRegion_ButtonClick);
            headerItem.Location = new System.Drawing.Point(0, 0);
            this.headerItem.Logo = CargoMatrix.Resources.Icons.Warehouse_24x24;

            this.panelHeader2.Controls.Add(headerItem);
            headerItem.Height = 53;
            this.panelHeader2.Height = headerItem.Height;
            headerItem.BringToFront();


            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.ResumeLayout(false);
            
        }

        #endregion

        CustomListItems.HeaderItem headerItem;
    }
}
