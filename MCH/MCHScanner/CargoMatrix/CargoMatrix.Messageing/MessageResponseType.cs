﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
namespace CargoMatrix.Messageing
{
    public enum MessageResponseType
    {
        Acknowledgement,
        YesNo,
        YesNoCancel,
        Text,
    }
}