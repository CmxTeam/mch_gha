﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Messageing
{
    public struct MessageCheckResponse
    {
        public int UnreadMsgsCount;
        public bool UrgentMsgExists;
    }
}
