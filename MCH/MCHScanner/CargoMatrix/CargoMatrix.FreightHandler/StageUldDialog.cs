﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;

namespace CargoMatrix.FreightHandler
{
    public partial class StageUldDialog : Form
    {
        bool bFirstTimeLoad = false;
       
        public string HeaderText;
        string m_masterbillID = "";
        int locationID = 0;
        string locationName = "";
        //SmoothListBox.UI.ListItems.ComboBox m_PiecesComboBox;
        //SmoothListBox.UI.ListItems.ComboBox m_directionComboBox;
        //public int SelectedPieceNo = -1;
        private static StageUldDialog m_instance = null;
        public string HousebillName;
        public int[] remainingPieces;
        CMXUserControl barcodeControl;
        public StageUldDialog()
        {
            InitializeComponent();

            smoothListBoxBase1.AutoScroll += new SmoothListBox.UI.AutoScrollHandler(smoothListBoxReasons_AutoScroll);
            panel1.BackColor = FreightHandlerResources.thumbnailBackColor.GetPixel(0, 0);
            pictureBoxMenuBack.Image = FreightHandlerResources.popup_Main_nav;
            pictureBoxHeader.Image = FreightHandlerResources.popup_header;
            pictureBoxOk.Image = FreightHandlerResources.nav_ok;
            pictureBoxCancel.Image = FreightHandlerResources.nav_cancel;
            pictureBoxUp.Image = FreightHandlerResources.nav_up;
            pictureBoxDown.Image = FreightHandlerResources.nav_down;

            barcodeControl = new CMXUserControl();
            barcodeControl.BarcodeReadNotify += new BarcodeReadNotifyHandler(barcodeControl_BarcodeReadNotify);
            barcodeControl.Enabled = true;
            pictureBoxOk.Enabled = false;
            pictureBoxEditLocation.Image = FreightHandlerResources.pic_thumb_down;
            pictureBoxManualULD.Image = FreightHandlerResources.pic_thumb_down;

          
        }

        void smoothListBoxReasons_AutoScroll(SmoothListBox.UI.SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            switch (direction)
            {
                case SmoothListBox.UI.SmoothListBoxBase.DIRECTION.UP:
                    pictureBoxUp.Enabled = enable;
                    break;
                case SmoothListBox.UI.SmoothListBoxBase.DIRECTION.DOWN:
                    pictureBoxDown.Enabled = enable;
                    break;
            }
            //throw new NotImplementedException();
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Gray, 1))
            {



                e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 1, panel1.Top - 1, panel1.Width + 1, panel1.Height + 1));
                //pen.Color = Color.Gray;
                //e.Graphics.DrawRectangle(pen, new Rectangle(panel1.Left - 2, panel1.Top - 2, panel1.Width + 2, panel1.Height + 2));
                int x1 = this.Height, x2 = this.Height, y1 = 0, y2 = 0;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    x1 -= 2;
                    y2 += 2;
                }
                x1 = 0;
                x2 = this.Height;
                y1 = 0;
                y2 = this.Height;
                for (int i = 0; i < this.Height; i += 2)
                {
                    e.Graphics.DrawLine(pen, x1, y1, x2, y2);

                    y1 += 2;
                    x2 -= 2;
                }


            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (bFirstTimeLoad == false)
            {
                bFirstTimeLoad = true;
                HeaderText = "Stage ULD(s)";
                
                
            }
        }
       

        private void MicroPhotoCaptureForm_Load(object sender, EventArgs e)
        {
            if (m_instance == null)
                m_instance = new StageUldDialog();
            m_instance.smoothListBoxBase1.RemoveAll();

            m_instance.smoothListBoxBase1.AddItem(new SmoothListBox.UI.ListItems.AddNewULDItem());
            barcodeControl.BarcodeEnabled = true;
            
            
        }

     
        Font HeaderFont = new Font(FontFamily.GenericSerif, 8, FontStyle.Bold);
        SolidBrush HeaderBrush = new SolidBrush(Color.White);
        private void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawString(HeaderText, HeaderFont, HeaderBrush,3, 4);
        }

        private void pictureBoxOk_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = FreightHandlerResources.nav_ok_over;
        }

        private void pictureBoxOk_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxOk.Image = FreightHandlerResources.nav_ok;
        }

        private void pictureBoxOk_Click(object sender, EventArgs e)
        {
            pictureBoxOk.Image = FreightHandlerResources.nav_ok_over;
            pictureBoxOk.Refresh();

            //smoothListBoxBase1
            barcodeControl.BarcodeEnabled= false;
            DialogResult = DialogResult.OK;

            

        }

        private void pictureBoxCancel_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = FreightHandlerResources.nav_cancel_over;

        }

        private void pictureBoxCancel_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxCancel.Image = FreightHandlerResources.nav_cancel;
        }

        private void pictureBoxCancel_Click(object sender, EventArgs e)
        {
            pictureBoxCancel.Image = FreightHandlerResources.nav_cancel_over;
            pictureBoxCancel.Refresh();

            barcodeControl.BarcodeEnabled = false;
            DialogResult = DialogResult.Cancel;
            
           
        }

        private void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxUp.Image = FreightHandlerResources.nav_up_over;
            smoothListBoxBase1.Scroll(SmoothListBox.UI.SmoothListBoxBase.DIRECTION.UP);

        }

        private void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxUp.Image = FreightHandlerResources.nav_up;
            smoothListBoxBase1.UpButtonPressed = false;
        }

        private void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxDown.Image = FreightHandlerResources.nav_down_over;
            smoothListBoxBase1.Scroll(SmoothListBox.UI.SmoothListBoxBase.DIRECTION.DOWN);

        }

        private void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxDown.Image = FreightHandlerResources.nav_down;
            smoothListBoxBase1.DownButtonPressed = false;
        }
               
        private void pictureBoxUp_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxUp.Enabled)
                pictureBoxUp.Image = FreightHandlerResources.nav_up;
            else
            {
                pictureBoxUp.Image = FreightHandlerResources.nav_up_dis;
                smoothListBoxBase1.UpButtonPressed = false;
            }

        }

        private void pictureBoxDown_EnabledChanged(object sender, EventArgs e)
        {
            if (pictureBoxDown.Enabled)
                pictureBoxDown.Image = FreightHandlerResources.nav_down;
            else
            {
                pictureBoxDown.Image = FreightHandlerResources.nav_down_dis;
                smoothListBoxBase1.DownButtonPressed = false;
            }

        }

        
       
       
       

        private void textLocationCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBoxOk.Image = FreightHandlerResources.nav_ok_over;
                pictureBoxOk.Refresh();
                DialogResult = DialogResult.OK;
                pictureBoxOk.Image = FreightHandlerResources.nav_ok;

            }
        }

        private void textLocationCode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBoxOk.Image = FreightHandlerResources.nav_ok;                

            }
        }
        public int[] SelectedPieces
        {
            get
            {
                
                List<int> selected = new List<int>();
                
                foreach (Control control in smoothListBoxBase1.SelectedItems)
                {
                    if(control is SmoothListBox.UI.ListItems.StandardListItem)
                    {
                        selected.Add((control as SmoothListBox.UI.ListItems.StandardListItem).ID);
                    }
                }
                return selected.ToArray();// remainingPieces[m_PiecesComboBox.SelectedID];
                
                
                
                

            }

        }

        public static DialogResult Show(string masterbillID)
        {
            if (m_instance == null)
                m_instance = new StageUldDialog();
            m_instance.m_masterbillID = masterbillID;
            m_instance.locationID = 0;
            m_instance.locationName = "";
            
            return m_instance.ShowDialog();

        
        }
        public static DialogResult Show(string masterbillID, int locID, string locName)
        {
            if (m_instance == null)
                m_instance = new StageUldDialog();
            m_instance.m_masterbillID = masterbillID;
            m_instance.locationID = locID;
            m_instance.locationName = locName;
            m_instance.labelLocation.Text = "Location: " + locName;
            return m_instance.ShowDialog();


        }

        void barcodeControl_BarcodeReadNotify(string barcodeData)
        {
            
            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);

            CargoMatrix.Communication.WSScanner.DTOBarCode barcodeObj = CargoMatrix.Communication.HostPlusIncomming.Instance.ParseBarcode(barcodeData);

            if (barcodeObj != null)
            {
                switch (barcodeObj.Type)
                {
                    case CargoMatrix.Communication.WSScanner.Types.Area:
                    case CargoMatrix.Communication.WSScanner.Types.Door: 
                        locationID = barcodeObj.LocationID;
                        locationName = barcodeObj.Location;
                        labelLocation.Text = "Location: " + locationName;
                        UpdateOkButton();
                        

                        break;
                    default:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        break;
                }
            }

            barcodeControl.BarcodeEnabled = true;
            
        }
        private void UpdateOkButton()
        {
            if (locationID > 0 && smoothListBoxBase1.SelectedItems.Count > 0)
            {
                pictureBoxOk.Enabled = true;

            }
            else
                pictureBoxOk.Enabled = false;
        
        }

        private void smoothListBoxBase1_ListItemClicked(SmoothListBox.UI.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            UpdateOkButton();
        }

        private void pictureBoxEditLocation_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            pictureBoxEditLocation.Image = FreightHandlerResources.pic_thumb_down_over;
            pictureBoxEditLocation.Refresh();
            NewLocationForm locDlg = new NewLocationForm();
            if (locDlg.ShowDialog() == DialogResult.OK)
            {
                locationName = locDlg.TextLocation;
                locationID = locDlg.LocationID;
                labelLocation.Text = "Location: " + locationName;
            }

            locDlg.Dispose();
            Cursor.Current = Cursors.Default;

        }

        private void pictureBoxEditLocation_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxEditLocation.Image = FreightHandlerResources.pic_thumb_down_over;
        }

        private void pictureBoxEditLocation_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxEditLocation.Image = FreightHandlerResources.pic_thumb_down;
        }

        private void pictureBoxManualULD_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            pictureBoxEditLocation.Image = FreightHandlerResources.pic_thumb_down_over;
            pictureBoxEditLocation.Refresh();
            ULDTypeListForm uldDlg = new ULDTypeListForm();
            if (uldDlg.ShowDialog() == DialogResult.OK)
            {
                
            }

            uldDlg.Dispose();
            Cursor.Current = Cursors.Default;
        }
       
    }
}