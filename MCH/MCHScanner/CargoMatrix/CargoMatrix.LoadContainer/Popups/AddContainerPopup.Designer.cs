﻿using CargoMatrix.UI;
using System.Windows.Forms;
namespace CargoMatrix.LoadContainer
{
    partial class AddContainerPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxContainerNo = new CMXTextBox();
            this.textBoxContainerType = new CMXTextBox();
            this.textBoxHawbNo = new CMXTextBox();
            this.buttonContainerType = new CMXPictureButton();
            this.buttonHawb = new CMXPictureButton();
            this.SuspendLayout();
            // 
            // textBoxContainerNo
            // 
            this.textBoxContainerNo.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.textBoxContainerNo.Location = new System.Drawing.Point(8, 46);
            this.textBoxContainerNo.Name = "textBoxContainerNo";
            this.textBoxContainerNo.Size = new System.Drawing.Size(181, 23);
            this.textBoxContainerNo.TabIndex = 0;
            this.textBoxContainerNo.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // textBoxContainerType
            // 
            this.textBoxContainerType.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.textBoxContainerType.Location = new System.Drawing.Point(8, 85);
            this.textBoxContainerType.Name = "textBoxContainerType";
            this.textBoxContainerType.Size = new System.Drawing.Size(181, 23);
            this.textBoxContainerType.TabIndex = 1;
            this.textBoxContainerType.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // textBoxHawbNo
            // 
            this.textBoxHawbNo.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.textBoxHawbNo.Location = new System.Drawing.Point(8, 128);
            this.textBoxHawbNo.Name = "textBoxHawbNo";
            this.textBoxHawbNo.Size = new System.Drawing.Size(181, 23);
            this.textBoxHawbNo.TabIndex = 3;
            this.textBoxHawbNo.TextChanged += new System.EventHandler(textBox_TextChanged);
            // 
            // buttonContainerType
            // 
            this.buttonContainerType.Location = new System.Drawing.Point(196, 85);
            this.buttonContainerType.Name = "buttonContainerType";
            this.buttonContainerType.Size = new System.Drawing.Size(32, 32);
            this.buttonContainerType.TabIndex = 2;
            this.buttonContainerType.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonContainerType.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonContainerType.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            // 
            // buttonHawb
            // 
            this.buttonHawb.Location = new System.Drawing.Point(196, 128);
            this.buttonHawb.Name = "buttonHawb";
            this.buttonHawb.Size = new System.Drawing.Size(32, 32);
            this.buttonHawb.TabIndex = 4;
            this.buttonHawb.Image = CargoMatrix.Resources.Skin.btn_listView;
            this.buttonHawb.PressedImage = CargoMatrix.Resources.Skin.btn_listView_over;
            this.buttonHawb.DisabledImage = CargoMatrix.Resources.Skin.btn_listView_dis;
            // 
            // AddContainerPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.panelContent.Size = new System.Drawing.Size(236, 200);
            this.panelContent.Controls.Add(this.buttonHawb);
            this.panelContent.Controls.Add(this.buttonContainerType);
            this.panelContent.Controls.Add(this.textBoxHawbNo);
            this.panelContent.Controls.Add(this.textBoxContainerType);
            this.panelContent.Controls.Add(this.textBoxContainerNo);
            this.Name = "AddContainerPopup";
            this.buttonOk.Enabled = false;
            this.ResumeLayout(false);

        }


        #endregion

        private CMXTextBox textBoxContainerNo;
        private CMXTextBox textBoxContainerType;
        private CMXTextBox textBoxHawbNo;
        private CMXPictureButton buttonContainerType;
        private CMXPictureButton buttonHawb;
    }
}
