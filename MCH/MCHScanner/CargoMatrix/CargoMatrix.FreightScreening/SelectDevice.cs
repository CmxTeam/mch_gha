﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication;
using System.Windows.Forms;
using System.Drawing;
using CargoMatrix.Communication.WSPieceScan;

namespace CargoMatrix.FreightScreening
{
    public class SelectDevice : CargoMatrix.Utilities.MessageListBox
    {
        public SelectDevice(InspectionMode mode)
        {
            Cursor.Current = Cursors.WaitCursor;
            HeaderText = string.Format(FreightScreening.Text_InspectionGeneric, mode.ToString());
            this.MultiSelectListEnabled = false;
            this.UnselectListEnabled = true;
            this.OneTouchSelection = true;
            this.smoothListBoxBase1.labelEmpty.Text = FreightScreening.Text_ReportEmptyLabel;
            Image icon = null;
            HeaderText2 = "Select device to continue";

            switch (mode)
            {
                case InspectionMode.Physical:
                    icon = FreightScreening.Visual_Inspection;
                    break;
                case InspectionMode.Canine:
                    icon = FreightScreening.Canine;
                    break;
                case InspectionMode.Customer:
                    icon = FreightScreening.Customer_screened;
                    break;
                case InspectionMode.XRay:
                    icon = FreightScreening.x_ray;
                    break;
                default:
                    break;
            }

            DisplayDevices(mode, icon);
            Cursor.Current = Cursors.Default;
        }

        private void DisplayDevices(InspectionMode mode, Image icon)
        {
            var devices = CargoMatrix.Communication.HostPlusIncomming.Instance.GetScrreningDevices(mode);
            foreach (var device in devices)
            {
                smoothListBoxBase1.AddItem2( new SmoothListbox.ListItems.StandardListItem<FspDevice>(device.SerialNo,icon,device));
            }
            smoothListBoxBase1.LayoutItems();
            smoothListBoxBase1.RefreshScroll();
        }
    }
}
