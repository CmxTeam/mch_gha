﻿using System;
using System.Linq;
using System.Text;
using CargoMatrix.Communication;
using System.Windows.Forms;
using CargoMatrix.Communication.WSPieceScan;
using CustomListItems;
using System.Drawing;

namespace CargoMatrix.FreightScreening
{
    class InspectionReport : CargoMatrix.Utilities.MessageListBox
    {
        public InspectionReport(InspectionMode mode)
        {
            Cursor.Current = Cursors.WaitCursor;
            this.HeaderText = FreightScreening.Text_ReportTitle + " " + Communication.WebServiceManager.Instance().m_user.UserName;
            this.smoothListBoxBase1.labelEmpty.Text = FreightScreening.Text_ReportEmptyLabel;
            Image icon = null;

            switch (mode)
            {
                case InspectionMode.Physical:
                    HeaderText2 = string.Format(FreightScreening.Text_InspectionGeneric,mode.ToString());
                    icon = FreightScreening.Visual_Inspection;
                    break;
                case InspectionMode.Canine:
                    HeaderText2 = string.Format(FreightScreening.Text_InspectionGeneric, mode.ToString());
                    icon = FreightScreening.Canine;
                    break;
                case InspectionMode.Customer:
                    HeaderText2 = FreightScreening.Text_CustomerTitle;
                    icon = FreightScreening.Customer_screened;
                    break;
                case InspectionMode.XRay:
                    HeaderText2 = string.Format(FreightScreening.Text_InspectionGeneric, mode.ToString());
                    icon = FreightScreening.x_ray;
                    break;
                default:
                    break;
            }
            this.smoothListBoxBase1.IsSelectable = false;
            DisplayReport(mode,icon);
            Cursor.Current = Cursors.Default;
        }

        private void DisplayReport(InspectionMode mode, Image icon)
        {
            var insps = CargoMatrix.Communication.HostPlusIncomming.Instance.GetInspectionReport(mode);
            var groups = from t in insps
                         group t by t.Origin + "-" + t.HouseBillNo + "-" + t.Destination  into g
                         select new { hawbNo = g.Key, details = g.ToArray() };

            foreach (var reps in groups)
            {
                CustomListItems.ExpandListItem hawbItem = new CustomListItems.ExpandListItem(1,icon);
                hawbItem.TitleLine = reps.hawbNo;
                string descLine = string.Format("Total pcs : {0} of {1}", reps.details.Sum(s => s.Slac),reps.details.First().TotalSlac);
                hawbItem.DescriptionLine = descLine;
                hawbItem.IsReadonly = true;
                hawbItem.IsSelectable = false;

                var range = from detali in reps.details
                            select new ReportItem(formatString(detali));

                hawbItem.AddRange<ReportItem>(range);
                this.AddItem(hawbItem);
            }

            if (smoothListBoxBase1.ItemsCount == 0)
                smoothListBoxBase1.labelEmpty.Visible = true;
        }

        string formatString(Inspection insp)
        {
            StringBuilder blder = new StringBuilder();
            blder.AppendFormat("{0} ({1}) {2}", insp.Result, insp.Slac, insp.Timestamp.ToString("MM/dd/yy HH:mm"));
            blder.Append(Environment.NewLine);
            if (!string.IsNullOrEmpty(insp.CertificationNo))
            {
                blder.Append("CERT#: ");
                blder.Append(insp.CertificationNo);
                blder.Append(Environment.NewLine);
            }
            if (!string.IsNullOrEmpty(insp.DeviceSerialNo))
            {
                blder.Append("DEVICE#: ");
                blder.Append(insp.DeviceSerialNo);
                blder.Append(Environment.NewLine);
            }

            if (!string.IsNullOrEmpty(insp.Remark))
            {
                blder.Append("RMRK : ");
                blder.Append(insp.Remark);
            }
            blder.Append(Environment.NewLine);

            return blder.ToString();
        }
    }
}
