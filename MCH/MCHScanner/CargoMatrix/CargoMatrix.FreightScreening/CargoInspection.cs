﻿using System;
using System.Windows.Forms;
using CustomListItems;
using CargoMatrix.Communication;
using CargoMatrix.Communication.WSPieceScan;
using System.Reflection;
using System.Drawing;

namespace CargoMatrix.FreightScreening
{
    public partial class CargoInspection : SmoothListbox.SmoothListbox
    {

        public object m_activeApp;

        CargoMatrix.Utilities.RemarksMessageBox remarkbox;
        private InspectionMode inspectionMode;
        private OptionsListITem optionManualHawb;
        private string DeviceNumber = string.Empty;

        public CargoInspection(InspectionMode mode)
        {
            inspectionMode = mode;
            InitializeComponent();
            this.smoothListBoxMainList.SendToBack();

            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(VisualScreening_MenuItemClicked);
            optionManualHawb = new OptionsListITem(OptionsListITem.OptionItemID.ENTER_HAWB_NO);
            AddOptionsListItem(optionManualHawb);
            this.topLabel.Text = FreightScreening.Text_VisInspection;
            remarkbox = new CargoMatrix.Utilities.RemarksMessageBox();
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(VisualScreening_BarcodeReadNotify);
            this.LoadOptionsMenu += new EventHandler(CargoInspection_LoadOptionsMenu);
            switch (mode)
            {
                case InspectionMode.Physical:
                    TitleText = string.Format(FreightScreening.Text_InspectionGeneric, mode.ToString());
                    break;
                case InspectionMode.Canine:
                    TitleText = string.Format(FreightScreening.Text_InspectionGeneric, mode.ToString());
                    this.topLabel.Text = FreightScreening.Text_ScanDog;
                    optionManualHawb.Enabled = false;
                    break;
                case InspectionMode.Customer:
                    TitleText = FreightScreening.Text_CustomerTitle;
                    break;
                case InspectionMode.XRay:
                    TitleText = string.Format(FreightScreening.Text_InspectionGeneric, mode.ToString());
                    this.topLabel.Text = FreightScreening.Text_ScanDevice;
                    optionManualHawb.Enabled = false;
                    break;
                case InspectionMode.ETD:
                    TitleText = string.Format(FreightScreening.Text_InspectionGeneric, mode.ToString());
                    this.topLabel.Text = FreightScreening.Text_ScanETDDevice;
                    optionManualHawb.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        void CargoInspection_LoadOptionsMenu(object sender, EventArgs e)
        {
            var option1 = new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.INVENTORY_REPORT_CURRENT);
            option1.title.Text = "Inspection Report";
            AddOptionsListItem(option1);
            if (inspectionMode == InspectionMode.Canine || inspectionMode == InspectionMode.XRay)
            {
                var option2 = new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.DEVICES);
                option2.title.Text = "Select Device Manually";
                if (inspectionMode == InspectionMode.XRay)
                {
                    option2.itemPicture.Image = FreightScreening.x_ray;
                }
                if (inspectionMode == InspectionMode.Canine)
                {
                    option2.itemPicture.Image = FreightScreening.Canine;
                }
                AddOptionsListItem(option2);
            }
        }
        /// <summary>
        /// called inside Initialize component re-align lables 
        /// </summary>
        private void xrayPlacement()
        {

            if (inspectionMode == InspectionMode.XRay)
            {
                topLabel.Top = 25;
            }
        }

        void VisualScreening_BarcodeReadNotify(string barcodeData)
        {
            topLabel.SplashText("Scanned: " + barcodeData);
            var scan = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            switch (scan.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    if ((inspectionMode == InspectionMode.XRay || inspectionMode == InspectionMode.Canine) && string.IsNullOrEmpty(DeviceNumber))
                    {
                        string msg = inspectionMode == InspectionMode.XRay ? FreightScreening.Text_ScanDevice : FreightScreening.Text_ScanDog;
                        CargoMatrix.UI.CMXMessageBox.Show(msg, "Unexpected Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    }
                    else
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                        InspectHousebill(scan.HouseBillNumber);
                    }
                    break;
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    if (inspectionMode == InspectionMode.XRay || inspectionMode == InspectionMode.Canine || inspectionMode == InspectionMode.ETD)
                    {

                        string deviceNo = CargoMatrix.Communication.HostPlusIncomming.Instance.GetFSPDeviceNumber(scan.Location, inspectionMode);
                        if (string.IsNullOrEmpty(deviceNo))
                        {
                            string msg = string.Format(FreightScreening.Text_DeviceNotFound, scan.Location);
                            CargoMatrix.UI.CMXMessageBox.Show(msg, "Unknown Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                            break;
                        }
                        //xrayDeviceLabel.Text = deviceNo;
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
                        selectDevice(deviceNo);
                        break;
                    }
                    else goto default;
                default:
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                    CargoMatrix.UI.CMXMessageBox.Show("Unexpected Barcode", "Invalid Barcode", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    break;
            }
            BarcodeEnabled = true;
        }

        private void selectDevice(string deviceNo)
        {
            DeviceNumber = deviceNo;
            TitleText = string.Format(FreightScreening.Text_InspectionGeneric, inspectionMode.ToString()) + " - " + deviceNo; ;
            optionManualHawb.Enabled = true;
            this.topLabel.Text = FreightScreening.Text_VisInspection;
        }

        void VisualScreening_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            switch ((listItem as OptionsListITem).ID)
            {
                case OptionsListITem.OptionItemID.ENTER_HAWB_NO:
                    remarkbox.RemarkInputMode = CargoMatrix.UI.CMXTextBoxInputMode.AlphaNumeric;
                    remarkbox.HeaderText = "Housebill Lookup";
                    remarkbox.Reference = "Enter Housebill Number";
                    remarkbox.Remarks = string.Empty;
                    remarkbox.Description = string.Empty;
                    if (DialogResult.OK == remarkbox.ShowDialog())
                    {
                        InspectHousebill(remarkbox.Remarks.ToUpper());

                    }
                    break;
                case OptionsListITem.OptionItemID.INVENTORY_REPORT_CURRENT:
                    InspectionReport report = new InspectionReport(inspectionMode);
                    report.ShowDialog();
                    break;
                case OptionsListITem.OptionItemID.DEVICES:
                    SelectDevice devices = new SelectDevice(inspectionMode);
                    if (DialogResult.OK == devices.ShowDialog())
                    {
                        var item = (devices.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem<FspDevice>);
                        if (item != null)
                        {
                            selectDevice(item.ItemData.SerialNo);
                        }

                    }
                    break;
            }
            BarcodeEnabled = true;
        }

        public override void LoadControl()
        {
            BarcodeEnabled = false;
            BarcodeEnabled = true;
            smoothListBoxMainList.labelEmpty.Visible = false;
            Cursor.Current = Cursors.Default;
        }

        private void InspectHousebill(string hawbNo)
        {
            Cursor.Current = Cursors.WaitCursor;
            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            Cursor.Current = Cursors.Default;
            if (null == hawb)
            {
                string msg = string.Format(FreightScreening.Text_InvalidHousebill, hawbNo);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Unknown number", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                    CustomUtilities.HousebillSkel skel;
                    if (DialogResult.OK == CustomUtilities.HousebillSkelPopup.Show(hawbNo, out skel))
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        if (skel.origin == string.Empty)
                            skel.origin = "???";
                        if (skel.destination == string.Empty)
                            skel.destination = "???";
                        HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, skel.pieces, skel.slac, skel.origin,skel.destination,skel.weight,skel.carrier,skel.mawbNo);
                        Cursor.Current = Cursors.Default;
                        InspectHousebill(hawbNo);
                    }
                    else return;
                }
                else return;
            }
            // terminate the recurisive call;
            if (hawb == null)
                return;


            string reference = string.Format("{0}-{1}-{2}", hawb.Origin, hawb.HouseBillNo, hawb.Destination);
            string origin = hawb.Origin;
            string destination = hawb.Destination;

            if (hawb.Status == CargoMatrix.Communication.WSPieceScan.EnumStatus.Completed)
            {
                string resetMsg = string.Format(FreightScreening.Text_AlreadyInspected, reference);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(resetMsg, "Housebill Inspected", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    CargoMatrix.Communication.HostPlusIncomming.Instance.ResetInspection(hawbNo);
                }
                else return;

            }

            if (hawb.ScreeningFailed)
            {
                string clearAlarmMsg = string.Format(FreightScreening.Text_ClearAlert, reference);
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(clearAlarmMsg, "Housebill Alert", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    CargoMatrix.Communication.HostPlusIncomming.Instance.ClearAlarm(hawbNo);
                }
                else return;
            }

            int count = 1;
            int total = 1;
            //# - processed slac
            if (hawb.ProcessedSlac.HasValue && hawb.TotalSlac - hawb.ProcessedSlac.Value > 1)
            {
                CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
                CntMsg.HeaderText = "Confirm Screen Count";
                CntMsg.LabelDescription = "Enter Number of Slac";
                CntMsg.LabelReference = reference;
                CntMsg.PieceCount = hawb.TotalSlac - hawb.ProcessedSlac.Value; // # - processed slac
                if (DialogResult.OK != CntMsg.ShowDialog())
                    return;
                else
                    count = CntMsg.PieceCount;
                    total = hawb.TotalSlac;
            }

            switch (inspectionMode)
            {
                case InspectionMode.Physical:
                    VisualInspection(hawbNo, reference, count, total, origin, destination);
                    break;
                case InspectionMode.Canine:
                    CanineInspection(hawbNo, reference, count);
                    break;
                case InspectionMode.Customer:
                    VerifyCustomerInspection(hawbNo, reference, count);
                    break;
                case InspectionMode.XRay:
                    XRayInspection(hawbNo, reference, count, total, origin,destination);
                    break;
                case InspectionMode.ETD:
                    ETDInspection(hawbNo, reference, count, total, origin, destination);
                    break;
                default:
                    break;
            }
        }
        private void VisualInspectionOld(string hawbNo, string reference, int count)
        {
            remarkbox.RemarkInputMode = CargoMatrix.UI.CMXTextBoxInputMode.Normal;
            remarkbox.HeaderText = "Confirm Physical Inspection";
            remarkbox.Remarks = string.Empty;
            remarkbox.Reference = reference;
            remarkbox.Description = "Enter Remarks (optional):";

            if (remarkbox.ShowDialog() == DialogResult.OK)
            {
                if (CargoMatrix.Communication.HostPlusIncomming.Instance.VisualInspection(hawbNo, count, remarkbox.Remarks))
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    topLabel.SplashText(string.Format(FreightScreening.Text_SucesfulScreening, reference, count));
                }
            }

        }
        private void VisualInspection(string hawbNo, string reference, int count,int total,string origin,string destination)
        {
        


            //RDF

            string remark = string.Empty;
            bool requiresPhotos = false;

            if (DialogResult.OK == CustomUtilities.VisualInspectionMessageBox.Show(reference,   ref remark,   count, total, ref requiresPhotos))
            {
                if (CargoMatrix.Communication.HostPlusIncomming.Instance.VisualInspection(hawbNo, count, remark))
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    topLabel.SplashText(string.Format(FreightScreening.Text_SucesfulScreening, reference, count));
                    if (requiresPhotos)
                    {
                        ShowFreightPhotoCapture(hawbNo, origin, destination,"Visual Inspection");
                    }
                }

              

            }
         

        }

        private void VerifyCustomerInspection(string hawbNo, string reference, int count)
        {
            remarkbox.RemarkInputMode = CargoMatrix.UI.CMXTextBoxInputMode.Normal;
            remarkbox.HeaderText = "Confirm Customer Inspection";
            remarkbox.Remarks = string.Empty;
            remarkbox.Reference = reference;
            remarkbox.Description = "ENTER CUSTOMER CCSF#";
            if (remarkbox.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            string ccsf = remarkbox.Remarks;

            remarkbox.RemarkInputMode = CargoMatrix.UI.CMXTextBoxInputMode.Normal;
            remarkbox.HeaderText = "Verify Customer Inspection";
            remarkbox.Remarks = string.Empty;
            remarkbox.Reference = reference;
            remarkbox.Description = "Enter Remarks (optional):";
            if (remarkbox.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            if (CargoMatrix.Communication.HostPlusIncomming.Instance.VerifyCustomerScreening(hawbNo, count, remarkbox.Remarks, ccsf))
            {
                CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                topLabel.SplashText(string.Format(FreightScreening.Text_SucesfulScreening, reference, count));
            }
        }

        private void CanineInspection(string hawbNo, string reference, int count)
        {
            //remarkbox.RemarkInputMode = CargoMatrix.UI.CMXTextBoxInputMode.Normal;
            //remarkbox.HeaderText = "Confirm Canine Inspection";
            //remarkbox.Remarks = string.Empty;
            //remarkbox.Reference = reference;
            //remarkbox.Description = "ENTER CERTIFICATION NO";

            //if (remarkbox.ShowDialog() != DialogResult.OK)
            //{
            //    return;
            //}
            //string certNo = remarkbox.Remarks;

            remarkbox.RemarkInputMode = CargoMatrix.UI.CMXTextBoxInputMode.Normal;
            remarkbox.HeaderText = "Verify Canine Inspection";
            remarkbox.Remarks = string.Empty;
            remarkbox.Reference = reference;
            remarkbox.Description = "Enter Remarks (optional):";
            if (remarkbox.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            if (CargoMatrix.Communication.HostPlusIncomming.Instance.CanineInspection(hawbNo, count, remarkbox.Remarks, DeviceNumber))
            {
                CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                topLabel.SplashText(string.Format(FreightScreening.Text_SucesfulScreening, reference, count));
            }
        }


        private void ShowFreightPhotoCapture(string hawbNo,string origin ,string destination,string title)
        {
            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    
                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;
                    
                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();
                         
                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo,origin,destination);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************
                   
        }
        //private bool CompleteProcess(string reference, string sampleNumber, ref string remark, string result, int count,int total, ref bool requiresPhotos)
        //{
        //    if (DialogResult.OK == CustomUtilities.PassFailCompleteMessageBox.Show(reference, sampleNumber, ref remark, result, count, total, ref requiresPhotos))
        //    {
        //        if (requiresPhotos)
        //        {
        //            ShowFreightPhotoCapture(reference);

        //            return CompleteProcess(reference, sampleNumber, ref remark, result, count, total, ref requiresPhotos);
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        private int ConvertToIn(string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch
            {
                return 0;
            }
        }
        private void ETDInspection(string hawbNo, string reference, int count, int total,string origin ,string destination)
        {
             
            //bool pass = CustomUtilities.PassFailMessageBox.Show(reference, "Select Screening Result", "PASS", "FAIL") == DialogResult.OK;
            //if (CargoMatrix.Communication.HostPlusIncomming.Instance.XRayInspection(hawbNo, count, pass, DeviceNumber))
            //{
            //    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
            //    topLabel.SplashText(string.Format(FreightScreening.Text_SucesfulScreening, reference, count));
            //}

            //RDF
            string sampleNumber = string.Empty;
            string remark = string.Empty;
            string result = string.Empty;
            bool requiresPhotos = false;
            //bool pass = CustomUtilities.PassFailSampleMessageBox.Show(reference, "Select Screening Result", "PASS", "FAIL", ref sampleNumber) == DialogResult.OK;
            bool pass = CustomUtilities.PassFailSlacSampleMessageBox.Show(reference, "ETD Screening Result", "PASS", "FAIL", ref sampleNumber,count,total) == DialogResult.OK;
            if (pass)
            {
                result = "Passed";
            }
            else
            {
                result = "Failed";
            }



            if (DialogResult.OK == CustomUtilities.PassFailCompleteMessageBox.Show(reference, sampleNumber, ref remark, result, count, total, ref requiresPhotos))
            {
                //if (CargoMatrix.Communication.HostPlusIncomming.Instance.XRayInspection(hawbNo, count, pass, DeviceNumber))

        

                if (remark.Length >= 200)
                {
                    remark = remark.Substring(0, 200);
                }


                if (CargoMatrix.Communication.HostPlusIncomming.Instance.ScreeningResult(hawbNo, count, pass, DeviceNumber, remark, ConvertToIn(sampleNumber), "ETDInspection"))
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    topLabel.SplashText(string.Format(FreightScreening.Text_SucesfulScreening, reference, count));

                    if (requiresPhotos)
                    {
                        ShowFreightPhotoCapture(hawbNo, origin, destination, "ETD Inspection");
                    }
                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show("System was unabled to process this shipment.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                }

            }
            else
            {
                //Action was cancelled
            }

                //if (CompleteProcess(reference, sampleNumber, ref remark, result, count, ref requiresPhotos))
                //{

                //    // user similar to CargoMatrix.Communication.HostPlusIncomming.Instance.VerifyCustomerScreening(
                //    if (CargoMatrix.Communication.HostPlusIncomming.Instance.XRayInspection(hawbNo, count, pass, DeviceNumber))
                //    {
                //        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                //        topLabel.SplashText(string.Format(FreightScreening.Text_SucesfulScreening, reference, count));
                //        MessageBox.Show("Action was completed");
                //    }
                //    else
                //    {
                //        //Action was unsuccessful
                //        CargoMatrix.UI.CMXMessageBox.Show("System was unabled to process this shipment.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                //    }
 

                //}
                //else
                //{
                //    MessageBox.Show("Action was cancelled");
                //    //Action was cancelled
                //}
     




        }

        private void XRayInspection(string hawbNo, string reference, int count, int total, string origin, string destination)
        {

          
            //RDF
            string sampleNumber = string.Empty;
            string remark = string.Empty;
            string result = string.Empty;
            bool requiresPhotos = false;
            bool pass = CustomUtilities.PassFailSlacSampleMessageBox.Show(reference, "XRay Screening Result", "PASS", "FAIL", ref sampleNumber, count, total) == DialogResult.OK;
            if (pass)
            {
                result = "Passed";
            }
            else
            {
                result = "Failed";
            }



            if (DialogResult.OK == CustomUtilities.PassFailCompleteMessageBox.Show(reference, sampleNumber, ref remark, result, count, total, ref requiresPhotos))
            {
    
                if (remark.Length >= 200)
                {
                    remark = remark.Substring(0, 200);
                }


                if (CargoMatrix.Communication.HostPlusIncomming.Instance.ScreeningResult(hawbNo, count, pass, DeviceNumber, remark, ConvertToIn(sampleNumber), "XRayInspection"))
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    topLabel.SplashText(string.Format(FreightScreening.Text_SucesfulScreening, reference, count));

                    if (requiresPhotos)
                    {
                        ShowFreightPhotoCapture(hawbNo, origin, destination, "XRay Inspection");
                    }
                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show("System was unabled to process this shipment.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                }

            }
            else
            {
                //Action was cancelled
            }

         



        }
 
    }
}
