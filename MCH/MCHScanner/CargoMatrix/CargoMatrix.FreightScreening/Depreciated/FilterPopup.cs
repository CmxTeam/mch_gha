﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;

namespace CargoMatrix.FreightScreening
{
    public partial class FilterPopup : MessageListBox
    {
        private CustomListItems.ChoiceListItem filterItem;
        private CustomListItems.ChoiceListItem sortItem;
        private MessageListBox optionalChoiceList;
        private string sort = "N/A";
        private string _destinationCode = string.Empty;
        private string _filter = "Not Completed";
        CargoMatrix.Utilities.NameTypeMesageListBox _filterForm;


        public string DestinationCode
        { get; set; }
        public string Filter
        {
            get { return _filter; }
            set { _filter = value; filterItem.LabelLine2.Text = value; }
        }
        public string Sort
        {
            get { return sort; }
            set { sort = value; sortItem.LabelLine2.Text = value; }
        }

        public int SortValue
        {
            get;
            set;
        }
        public int FilterValue
        {
            get;
            set;
        }

        public FilterPopup()
        {
            InitializeComponent();
            this.HeaderText = "Select filters";
            this.MultiSelectListEnabled = false;
            this.OneTouchSelection = false;
            this.OkEnabled = true;
            DestinationCode = string.Empty;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FilterPopup_ListItemClicked);
            TopPanel = false;
            filterItem = new CustomListItems.ChoiceListItem("Filter", CargoMatrix.Resources.Skin.Filter, !string.IsNullOrEmpty(_filter) ? _filter : FreightScreening.Text_All);
            AddItem(filterItem);
            sortItem = new CustomListItems.ChoiceListItem("Sort", CargoMatrix.Resources.Skin.Sort, !string.IsNullOrEmpty(sort) ? sort : FreightScreening.Text_All);
            AddItem(sortItem);
        }


        void FilterPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            // first time initialization
            if (null == this.optionalChoiceList)
            {
                optionalChoiceList = new MessageListBox();
                optionalChoiceList.MultiSelectListEnabled = false;
                optionalChoiceList.OneTouchSelection = true;
                optionalChoiceList.TopPanel = false;
            }

            // if filter
            if (listItem == filterItem)
            {
                //optionalChoiceList.RemoveAllItems();
                //optionalChoiceList.HeaderText = FreightScreening.Text_FilterBy;
                //optionalChoiceList.AddItems(GetFilters());

                //// select current selected status
                //foreach (Control item in optionalChoiceList.Items)
                //    if (item is StandardListItem && (item as StandardListItem).title.Text == filter)
                //    {
                //        optionalChoiceList.SelectControl(item);
                //        break;
                //    }

                //if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                //{
                //    filter = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                //    filterItem.LabelLine2.Text = filter;
                //    FilterValue = (optionalChoiceList.SelectedItems[0] as StandardListItem).ID;
                //}
                if (_filterForm == null)
                {
                    _filterForm = new CargoMatrix.Utilities.NameTypeMesageListBox();
                    _filterForm.HeaderText = "Filters";
                    _filterForm.TextBoxTitle = "Destination Code (Optional)";
                    _filterForm.MultiSelectListEnabled = false;
                    _filterForm.UnselectListEnabled = false;
                    PopulateFilters();
                }

                if (_filterForm.ShowDialog() == DialogResult.OK)
                {
                    if (_filterForm.SelectedItems.Count > 0)
                    {
                        if (_filterForm.SelectedItems[0] is SmoothListbox.ListItems.StandardListItem)
                        {
                            Filter = (_filterForm.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).title.Text;
                            FilterValue = (_filterForm.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID;

                        }
                    }


                    // doing string processing
                    _destinationCode = _filterForm.TextBoxString;

                    char[] whiteSpaces = " ".ToCharArray();
                    _destinationCode = _destinationCode.TrimEnd(whiteSpaces);
                    _destinationCode = _destinationCode.TrimStart(whiteSpaces);
                    _destinationCode = _destinationCode.ToUpper();
                    _filterForm.TextBoxString = _destinationCode;
                    DestinationCode = _destinationCode;

                    if (!string.IsNullOrEmpty(DestinationCode))
                        Filter += ":" + DestinationCode;
                }
            }


            //if sort 
            if (listItem == sortItem)
            {
                optionalChoiceList.RemoveAllItems();
                optionalChoiceList.HeaderText = FreightScreening.Text_Sortby;
                optionalChoiceList.AddItems(GetSorts());

                // select current selected direction
                foreach (Control item in optionalChoiceList.Items)
                    if (item is StandardListItem && (item as StandardListItem).title.Text == sort)
                    {
                        optionalChoiceList.SelectControl(item);
                    }


                if (optionalChoiceList.ShowDialog() == DialogResult.OK)
                {
                    sort = (optionalChoiceList.SelectedItems[0] as StandardListItem).title.Text;
                    sortItem.LabelLine2.Text = sort;
                    SortValue = (optionalChoiceList.SelectedItems[0] as StandardListItem).ID;
                }
            }
            smoothListBoxBase1.Reset();
        }

        void PopulateFilters()
        {
            SmoothListbox.ListItems.StandardListItem defaultSelection = new SmoothListbox.ListItems.StandardListItem("Not Completed", null, (int)CargoMatrix.Communication.WSPieceScan.EnumStatus.Open);
            defaultSelection.SelectedChanged(true);

            _filterForm.AddItem(new SmoothListbox.ListItems.StandardListItem("Show All", null, (int)CargoMatrix.Communication.WSPieceScan.EnumStatus.All));
            _filterForm.AddItem(defaultSelection);
            _filterForm.AddItem(new SmoothListbox.ListItems.StandardListItem("Not Started", null, (int)CargoMatrix.Communication.WSPieceScan.EnumStatus.Pending));
            _filterForm.AddItem(new SmoothListbox.ListItems.StandardListItem("In Progress", null, (int)CargoMatrix.Communication.WSPieceScan.EnumStatus.InProgress));
            _filterForm.AddItem(new SmoothListbox.ListItems.StandardListItem("Completed", null, (int)CargoMatrix.Communication.WSPieceScan.EnumStatus.Completed));
            _filterForm.AddItem(new SmoothListbox.ListItems.StandardListItem("Cancelled", null, (int)CargoMatrix.Communication.WSPieceScan.EnumStatus.Cancelled));
        }

        private List<Control> GetSorts()
        {
            return new List<Control>()
            {
                    new StandardListItem("N/A", null,(int)CargoMatrix.Communication.WSPieceScan.HBSortFields.NA),
                    new StandardListItem("Airbill No", null,(int)CargoMatrix.Communication.WSPieceScan.HBSortFields.AirbillNo),
                    new StandardListItem("Weight", null,(int)CargoMatrix.Communication.WSPieceScan.HBSortFields.GrossWeight),
                    new StandardListItem("Piece Count", null,(int)CargoMatrix.Communication.WSPieceScan.HBSortFields.NoOfPackages),
                    new StandardListItem("Origin", null,(int)CargoMatrix.Communication.WSPieceScan.HBSortFields.Origin)};
        }
    }
}
