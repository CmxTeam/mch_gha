﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSPieceScan;
using CMXExtensions;

namespace CargoMatrix.FreightScreening
{
    [Obsolete()]
    public partial class ScreeningList : SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IHouseBillItem>
    {
        private Viewer.HousebillViewer billViewer;
        private string _destinationCode = string.Empty;
        private string _filter = "Not Completed";
        FilterPopup _filterPopup;
        CargoMatrix.Utilities.RemarksMessageBox _find;
        CargoMatrix.Utilities.RemarksMessageBox _visualInspectionForm;
        private int sortValue;
        private int filterValue;
        CargoMatrix.Utilities.CountMessageBox cntMsgBx;

        public ScreeningList(int actionID)
        {
            InitializeComponent();
            Forklift.Instance.ActionID = actionID;
            this.topLabel.Text = FreightScreening.Text_TopMessage;
            Forklift.Instance.ForkliftID = CargoMatrix.Communication.HostPlusIncomming.Instance.GetForkliftID();
            TitleText = string.Format("Move {0} ETD ({1})", actionID == 106 ? "To" : "From", _filter);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(ScreeningList_ListItemClicked);
            this.LoadOptionsMenu += new EventHandler(ScreeningList_LoadOptionsMenu);
            this.smoothListBoxMainList.UnselectEnabled = true;
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ScreeningList_MenuItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(BarcodeParse);
            this.HeaderClick += new EventHandler(ScreeningList_HeaderClick);
            this.cntMsgBx = new CargoMatrix.Utilities.CountMessageBox();
            cntMsgBx.HeaderText = "Please confirm count";
            cntMsgBx.LabelDescription = "Enter number of pieces to load";

            CargoMatrix.Communication.BarcodeParser.Instance.Parse(string.Empty);
            #region
            //panelHeader2.Height = 40;
            //headerItem.Location = panelHeader2.Location;
            //panelHeader2.Visible = false;
            //headerItem.Label1 = mawb.Reference();
            //headerItem.Label2 = mawb.Line2();
            //headerItem.Label3 = string.Format(LoadConsolResources.Text_HawbListBlink, ForkLiftViewer.Instance.Mode.ToString().ToUpper());
            //headerItem.Blink = true;
            ////headerItem.Logo = CargoMatrix.Resources.Skin.load_consol;
            //headerItem.ButtonImage = CargoMatrix.Resources.Skin.Forklift_btn; //CargoMatrix.Resources.Skin.buffer;
            //headerItem.ButtonPressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over; //CargoMatrix.Resources.Skin.buffer_over;
            //this.headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            //this.headerItem.ItemClick += new EventHandler(headerItem_Click);
            //this.Controls.Add(headerItem);
            //this.TitleText = ForkLiftViewer.Instance.Mode.ToString() + " Consol (Not Completed)";
            //this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(LoadHawbs_ListItemClicked);
            //this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(LoadHawbs_MenuItemClicked);
            //this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(LoadHawbs_BarcodeReadNotify);
            #endregion
        }

        void ScreeningList_HeaderClick(object sender, EventArgs e)
        {
            FilterOption();
        }

        void ScreeningList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                hawbItem_OnEnterClick(listItem, null);
            else
            {
                smoothListBoxMainList.MoveControlToTop(listItem);
                smoothListBoxMainList.RefreshScroll();
            }
        }
        private IHouseBillItem IsKnownHawb(string barcodeData)
        {
            var scan = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scan.BarcodeType == CMXBarcode.BarcodeTypes.HouseBill)
            {
                foreach (var item in smoothListBoxMainList.Items)
                {
                    if (item is CustomListItems.ConsolHAWBListItem && (item as CustomListItems.ConsolHAWBListItem).HawbItem.HousebillNumber == scan.HouseBillNumber)
                    {
                        return (item as CustomListItems.ConsolHAWBListItem).HawbItem;
                    }

                }
            }
            return null;
        }

        private void BarcodeParse(string barcodeData)
        {
            IHouseBillItem hawb = IsKnownHawb(barcodeData);
            bool refresh;
            if (hawb != null)
                refresh = KnownHawbScan(hawb);
            else
                refresh = UnknownScan(barcodeData);

            if (refresh)
                LoadControl();
            else BarcodeEnabled = true;
        }
        private bool KnownHawbScan(IHouseBillItem hawb)
        {
            bool refreshflag = true;
            MoveHBPiecesResponse response;

            if (hawb.TotalPieces == 1)
            {
                response = CargoMatrix.Communication.HostPlusIncomming.Instance.MoveHouseBilltoForklift(hawb.HousebillNumber, Forklift.Instance.ActionID, Forklift.Instance.ForkliftID, ScanTypes.Automatic);
                if (!response.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(response.Message))
                        CargoMatrix.UI.CMXMessageBox.Show(response.Message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
                else
                    buttonScannedList.Value = response.NoOfPiecesInForklift;

            }
            else
            {
                cntMsgBx.PieceCount = hawb.TotalPieces;
                cntMsgBx.LabelReference = hawb.HousebillNumber;
                if (cntMsgBx.ShowDialog() == DialogResult.OK)
                {
                    response = CargoMatrix.Communication.HostPlusIncomming.Instance.MoveHouseBillPiecestoForklift(hawb.HousebillNumber, Forklift.Instance.ActionID, Forklift.Instance.ForkliftID, cntMsgBx.PieceCount, ScanTypes.Automatic);
                    if (!response.IsSuccess)
                    {
                        if (!string.IsNullOrEmpty(response.Message))
                            CargoMatrix.UI.CMXMessageBox.Show(response.Message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                    else
                        buttonScannedList.Value = response.NoOfPiecesInForklift;

                }
                else refreshflag = false;
            }
            return refreshflag;
        }
        bool UnknownScan(string barcodeData)
        {
            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            bool refreshflag = true;

            switch (scanItem.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    MoveHBPiecesResponse response;
                    // assume scanned hawb has only one piece
                    response = CargoMatrix.Communication.HostPlusIncomming.Instance.MoveHouseBilltoForklift(scanItem.HouseBillNumber, Forklift.Instance.ActionID, Forklift.Instance.ForkliftID, ScanTypes.Automatic);

                    // if canned hawb has more than one piece show count popup
                    if (!response.IsSuccess && response.TotalPieces > 1)
                    {
                        cntMsgBx.PieceCount = response.TotalPieces;
                        cntMsgBx.HeaderText = scanItem.HouseBillNumber;
                        cntMsgBx.TextLocation = "Please confirm count";
                        // if count is ok
                        if (cntMsgBx.ShowDialog() == DialogResult.OK)
                        {
                            response = CargoMatrix.Communication.HostPlusIncomming.Instance.MoveHouseBillPiecestoForklift(scanItem.HouseBillNumber, Forklift.Instance.ActionID, Forklift.Instance.ForkliftID, cntMsgBx.PieceCount, ScanTypes.Automatic);
                        }
                        //in case cntMsgBx has been cancelled set response to success.
                        else response.IsSuccess = true;
                    }
                    if (!response.IsSuccess)
                    {
                        if (!string.IsNullOrEmpty(response.Message))
                            CargoMatrix.UI.CMXMessageBox.Show(response.Message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                    buttonScannedList.Value = response.NoOfPiecesInForklift;


                    break;
                case CMXBarcode.BarcodeTypes.ScreeningArea:
                    if (Forklift.Instance.ActionID == 106)
                        CargoMatrix.Communication.HostPlusIncomming.Instance.DropForkliftIntoLocation(Forklift.Instance.ActionID, Forklift.Instance.ForkliftID, scanItem.Location, ScanTypes.Automatic);
                    else goto default;
                    break;
                case CMXBarcode.BarcodeTypes.Area:
                case CMXBarcode.BarcodeTypes.Door:
                case CMXBarcode.BarcodeTypes.Truck:
                    if (Forklift.Instance.ActionID != 106)
                        CargoMatrix.Communication.HostPlusIncomming.Instance.DropForkliftIntoLocation(Forklift.Instance.ActionID, Forklift.Instance.ForkliftID, scanItem.Location, ScanTypes.Automatic);
                    else goto default;
                    break;
                default:
                    CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    refreshflag = false;
                    break;
            }
            return refreshflag;
        }

        void ScreeningList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FIND:
                        if (_find == null)
                        {
                            _find = new CargoMatrix.Utilities.RemarksMessageBox();
                            _find.HeaderText = "Find";
                            _find.Description = "Enter housebill number:";
                        }
                        bool retry = false;
                        do
                        {
                            if (_find.ShowDialog() == DialogResult.OK)
                            {
                                //do magic
                                int i = 0;
                                foreach (CustomListItems.ConsolHAWBListItem item in smoothListBoxMainList.Items)
                                {
                                    if (_find.Remarks.Equals(item.HawbItem.HousebillNumber, StringComparison.OrdinalIgnoreCase))
                                    {
                                        smoothListBoxMainList.SelectNone();
                                        smoothListBoxMainList.SelectControl(item);
                                        //item.Focus(true);
                                        smoothListBoxMainList.LayoutItems();
                                        smoothListBoxMainList.MoveControlToTop(item);
                                        break;
                                    }
                                    i++;
                                }

                                if (i >= smoothListBoxMainList.ItemsCount)
                                {
                                    if (CargoMatrix.UI.CMXMessageBox.Show("Housebill: " + _find.Remarks + ", is not present in current list. Would you like to try again?", "Housebill Not Found", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No) == DialogResult.OK)
                                    {
                                        retry = true;
                                    }
                                    else
                                    {
                                        retry = false;
                                    }
                                }
                                else
                                    retry = false;
                            }
                            else
                            {
                                retry = false;
                            }
                        } while (retry);

                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        #region oldCode
                        //if (_filterForm == null)
                        //{
                        //    _filterForm = new CargoMatrix.Utilities.NameTypeMesageListBox();
                        //    _filterForm.HeaderText = "Filters";
                        //    _filterForm.TextBoxTitle = "Destination Code (Optional)";
                        //    _filterForm.MultiSelectListEnabled = false;
                        //    _filterForm.UnselectListEnabled = false;
                        //    PopulateFilters();
                        //}

                        //if (_filterForm.ShowDialog() == DialogResult.OK)
                        //{
                        //    if (_filterForm.SelectedItems.Count > 0)
                        //    {
                        //        if (_filterForm.SelectedItems[0] is SmoothListbox.ListItems.StandardListItem)
                        //        {
                        //            _filter = (_filterForm.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).title.Text;
                        //            _statusCode = (CargoMatrix.Communication.DTO.HouseBillStatuses)(_filterForm.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID;

                        //        }
                        //    }


                        //    // doing string processing
                        //    _destinationCode = _filterForm.TextBoxString;

                        //    char[] whiteSpaces = " ".ToCharArray();
                        //    _destinationCode = _destinationCode.TrimEnd(whiteSpaces);
                        //    _destinationCode = _destinationCode.TrimStart(whiteSpaces);
                        //    _destinationCode = _destinationCode.ToUpper();
                        //    _filterForm.TextBoxString = _destinationCode;


                        //    string dest = string.Empty;

                        //    if (_destinationCode != string.Empty)
                        //        dest = "/" + _destinationCode;
                        //}
                        #endregion

                        FilterOption();

                        break;
                }
            }
        }

        private void FilterOption()
        {
            if (_filterPopup == null)
                _filterPopup = new FilterPopup();

            if (DialogResult.OK == _filterPopup.ShowDialog())
            {
                sortValue = _filterPopup.SortValue;
                filterValue = _filterPopup.FilterValue;
                _destinationCode = _filterPopup.DestinationCode;
                TitleText = string.Format("Move {0} ETD ({1})", Forklift.Instance.ActionID == 106 ? "To" : "From", _filterPopup.Filter);
                LoadControl();
            }
        }
        void ScreeningList_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER));
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FIND));
        }

        void hawbItem_ButtonConditionClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            IHouseBillItem houseBillItem = (sender as CustomListItems.ConsolHAWBListItem).HawbItem;

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(houseBillItem);

            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionSummary(args.HouseBillID);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.LoadConsol.Instance.UpdateShippmentCondition(args.HouseBillId, args.ScanModes, args.ShipmentConditions, Forklift.Instance.ActionID,string.Empty); 
            };

            damageCapture.GetHouseBillPiecesIDs = () => 
            {
                return
                    (
                        from item in CargoMatrix.Communication.LoadConsol.Instance.GetHousebillInfo(houseBillItem.HousebillId, Forklift.Instance.ActionID).Pieces
                        select item.PieceId
                    ).ToArray();
            };


            damageCapture.Size = this.Size;
            BarcodeEnabled = false;
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(damageCapture);
            Cursor.Current = Cursors.Default;
        }
        void hawbItem_ButtonBrowseClick(object sender, EventArgs e)
        {
            List<Control> actions = new List<Control>
            {
                new SmoothListbox.ListItems.StandardListItem("RESET SCREENING",null,3),
                new SmoothListbox.ListItems.StandardListItem("CANCEL SCREENING",null,2),
                new SmoothListbox.ListItems.StandardListItem("VISUAL INSPECTION",null,1)
            };
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.OneTouchSelection = true;
            actPopup.MultiSelectListEnabled = false;
            actPopup.HeaderText = (sender as CustomListItems.ConsolHAWBListItem).HawbItem.Reference();
            actPopup.HeaderText2 = string.Format("Select action for {0}", (sender as CustomListItems.ConsolHAWBListItem).HawbItem.HousebillNumber);
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 3:
                        ResetScreening((sender as CustomListItems.ConsolHAWBListItem).HawbItem);
                        break;
                    case 2:
                        CancelScreening((sender as CustomListItems.ConsolHAWBListItem).HawbItem);
                        break;
                    case 1:
                        VisualInspection((sender as CustomListItems.ConsolHAWBListItem).HawbItem);
                        break;
                    default:
                        break;

                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();

        }

        private void VisualInspection(IHouseBillItem hawb)
        {
            if (_visualInspectionForm == null)
            {
                _visualInspectionForm = new CargoMatrix.Utilities.RemarksMessageBox();
                _visualInspectionForm.HeaderText = "Confirm Visual Inspection";

            }
            _visualInspectionForm.Remarks = string.Empty;
            _visualInspectionForm.Reference = hawb.Reference();
            if (CustomUtilities.VisualInspectionSupervisorMessageBox.Show(this, "Visual Inspect: " + _visualInspectionForm.Reference) == DialogResult.OK)
            {
                if (_visualInspectionForm.ShowDialog() == DialogResult.OK)
                {
                    string superName = CustomUtilities.VisualInspectionSupervisorMessageBox.SupervisorName;
                    CargoMatrix.Communication.HostPlusIncomming.Instance.VisualInspection(hawb.HousebillNumber,hawb.TotalPieces,_visualInspectionForm.Remarks);
                    LoadControl();
                }
            }
        }
        private void ResetScreening(IHouseBillItem hawb)
        {
            string message = string.Format(FreightScreening.Text_ConfirmMessage, "Reset", hawb.HousebillNumber);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(message, "Confirm Reset", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(this, "Reset Screening: " + hawb.Reference()) == DialogResult.OK)
                {
                    int pin = int.Parse(CustomUtilities.SupervisorAuthorizationMessageBox.SupervisorPIN);
                    CargoMatrix.Communication.HostPlusIncomming.Instance.ResetInspection(hawb.HousebillNumber, pin);
                    LoadControl();
                }
            }
        }
        private void CancelScreening(IHouseBillItem hawb)
        {
            string message = string.Format(FreightScreening.Text_ConfirmMessage, "Cancel", hawb.HousebillNumber);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(message, "Cancel Screening", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                if (CustomUtilities.SupervisorAuthorizationMessageBox.Show(this, "Cancel Screening: " + hawb.Reference()) == DialogResult.OK)
                {
                    string pin = CustomUtilities.SupervisorAuthorizationMessageBox.SupervisorPIN;
                    CargoMatrix.Communication.HostPlusIncomming.Instance.CancelScreening(hawb.HousebillNumber, pin);
                    LoadControl();
                }
            }
        }

        void hawbItem_OnViewerClick(object sender, EventArgs e)
        {
            DisplayBillViewer((sender as CustomListItems.ConsolHAWBListItem).HawbItem.HousebillNumber);
        }
        void hawbItem_OnEnterClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            HawbViewer hawbViewer = new HawbViewer((sender as CustomListItems.ConsolHAWBListItem).HawbItem, Forklift.Instance.ActionID);//
            hawbViewer.Tag = this;
            Cursor.Current = Cursors.Default;
            hawbViewer.ShowDialog();
            LoadControl();
        }

        private void DisplayBillViewer(string hawbNo)
        {
            if (billViewer != null)
                billViewer.Dispose();

            billViewer = new CargoMatrix.Viewer.HousebillViewer(hawbNo);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            BarcodeEnabled = false;
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }

        public override void LoadControl()
        {
            ReloadItemsAsync(CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillList(Forklift.Instance.ActionID, (CargoMatrix.Communication.WSPieceScan.EnumStatus)filterValue, (CargoMatrix.Communication.WSPieceScan.HBSortFields)sortValue, string.Empty, string.Empty, string.Empty, _destinationCode));
            BarcodeEnabled = false;
            BarcodeEnabled = true;
            buttonScannedList.Value = CargoMatrix.Communication.FreightScreening.Instance.GetForkLiftHouseBillsCount(Forklift.Instance.ForkliftID);
        }

        void buttonScannedList_Click(object sender, System.EventArgs e)
        {
            BarcodeEnabled = false;
            Forklift.Instance.ShowDialog();
            LoadControl();
        }

        protected override Control InitializeItem(IHouseBillItem item)
        {
            CustomListItems.ConsolHAWBListItem hawbItem = new CustomListItems.ConsolHAWBListItem(item);
            hawbItem.OnViewerClick += new EventHandler(hawbItem_OnViewerClick);
            hawbItem.ButtonBrowseClick += new EventHandler(hawbItem_ButtonBrowseClick);
            hawbItem.ButtonConditionClick += new EventHandler(hawbItem_ButtonConditionClick);
            hawbItem.OnEnterClick += new EventHandler(hawbItem_OnEnterClick);
            return hawbItem;
        }
    }
}
