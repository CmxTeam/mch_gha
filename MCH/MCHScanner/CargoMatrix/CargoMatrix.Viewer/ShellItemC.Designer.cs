﻿namespace CargoMatrix.Viewer
{
    partial class ShellItemC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelHeading = new System.Windows.Forms.Label();
            this.Panel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelBaseLine = new System.Windows.Forms.Panel();
            this.Panel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelDescription
            // 
            this.labelDescription.BackColor = System.Drawing.Color.White;
            this.labelDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDescription.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDescription.Location = new System.Drawing.Point(0, 14);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(232, 12);
            this.labelDescription.Text = "<desc>";
            // 
            // labelHeading
            // 
            this.labelHeading.BackColor = System.Drawing.Color.White;
            this.labelHeading.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHeading.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelHeading.Location = new System.Drawing.Point(0, 0);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(232, 14);
            this.labelHeading.Text = "Heading";
            // 
            // Panel
            // 
            this.Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel.BackColor = System.Drawing.Color.Red;
            this.Panel.Controls.Add(this.labelDescription);
            this.Panel.Controls.Add(this.labelHeading);
            this.Panel.Location = new System.Drawing.Point(1, 1);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(232, 26);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.Panel);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 28);
            // 
            // panelBaseLine
            // 
            this.panelBaseLine.BackColor = System.Drawing.Color.LightGray;
            this.panelBaseLine.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBaseLine.Location = new System.Drawing.Point(0, 33);
            this.panelBaseLine.Name = "panelBaseLine";
            this.panelBaseLine.Size = new System.Drawing.Size(240, 1);
            // 
            // ShellItemC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelBaseLine);
            this.Controls.Add(this.panel1);
            this.Name = "ShellItemC";
            this.Size = new System.Drawing.Size(240, 34);
            this.Panel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelHeading;
        public System.Windows.Forms.Panel Panel;
        public System.Windows.Forms.Panel panel1;
        protected System.Windows.Forms.Panel panelBaseLine;
    }
}
