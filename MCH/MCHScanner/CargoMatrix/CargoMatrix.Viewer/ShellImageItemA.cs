﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CargoMatrix.Viewer
{
    public partial class ShellImageItemA : UserControl
    {
        Thread m_thread;
        string m_imageLocation;
        public ShellImageItemA(string description, string imageLocation)
        {
            InitializeComponent();
            labelDescription.Text = description;
            m_imageLocation = imageLocation;
            if (imageLocation != null)
            {
                //pictureBoxIcon.Image = ViewerResource.Download;
                ThreadStart starter = new ThreadStart(LoadImage);
                m_thread = new Thread(starter);
                m_thread.Priority = ThreadPriority.Normal;
                m_thread.Start();
            }
            //else
            //{
            //    pictureBoxIcon.Image = ViewerResource.imageNotFound;
            //}
            
        }

        void LoadImage()
        {
            this.Invoke(new EventHandler(WorkerLoadImage));


        }
        public void WorkerLoadImage(object sender, EventArgs e)
        {
            Image img = CargoMatrix.Communication.WebServiceManager.Instance().GetImageFromServer(m_imageLocation);
            if (img != null)
            {
                pictureBoxIcon.Image = img;
            }
            
        }
    }
}
