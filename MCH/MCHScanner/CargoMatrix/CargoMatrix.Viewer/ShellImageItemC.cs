﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CargoMatrix.Viewer
{
    public partial class ShellImageItemC : UserControl
    {
        Thread m_thread;
        string m_imageLocation;
        public ShellImageItemC(string heading, string description, string imageLocation)
        {
            InitializeComponent();
            labelDescription.Text = description;
            labelHeading.Text = heading;
            m_imageLocation = imageLocation;
            if (imageLocation != null)
            {
                //pictureBoxIcon.Image = ViewerResource.Download;
                ThreadStart starter = new ThreadStart(LoadImage);
                m_thread = new Thread(starter);
                m_thread.Priority = ThreadPriority.Normal;
                m_thread.Start();
            }
            //else
            //{
            //    pictureBoxIcon.Image = ViewerResource.imageNotFound;
            //}

            OpenNETCF.Drawing.FontEx descriptionFont = new OpenNETCF.Drawing.FontEx(labelDescription.Font.Name, labelDescription.Font.Size, labelDescription.Font.Style);
            OpenNETCF.Drawing.FontEx headingFont = new OpenNETCF.Drawing.FontEx(labelHeading.Font.Name, labelHeading.Font.Size, labelHeading.Font.Style);
            OpenNETCF.Drawing.GraphicsEx g = OpenNETCF.Drawing.GraphicsEx.FromControl(this);

            int headingHeight = 0;
            if(heading != null)
                headingHeight = (int)g.MeasureString(heading, headingFont, labelHeading.Width).Height;
            int descHeight = 0;
            if(description != null)
                descHeight = (int)g.MeasureString(description, descriptionFont, labelDescription.Width).Height;
            int height = headingHeight + descHeight;

            int diff = Height - (labelDescription.Height + labelHeading.Height);

            labelHeading.Height = headingHeight;
            labelDescription.Height = descHeight;
            //labelHeading.Top = 1;
            //labelHeading.Top = labelHeading.Top + headingHeight;


            this.Height = height + diff;
            g.Dispose();


        }
        void LoadImage()
        {
            this.Invoke(new EventHandler(WorkerLoadImage));


        }
        public void WorkerLoadImage(object sender, EventArgs e)
        {
            Image img = CargoMatrix.Communication.WebServiceManager.Instance().GetImageFromServer(m_imageLocation);
            if (img != null)
            {
                pictureBoxIcon.Image = img;
            }

        }
        
    }
}
