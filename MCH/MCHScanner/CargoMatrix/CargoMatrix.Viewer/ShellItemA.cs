﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix.Viewer
{
    public partial class ShellItemA : UserControl
    {
        public ShellItemA(string description, string image)
        {
            InitializeComponent();
            if (description == null)
                description = "";
            labelDescription.Text = description;

            OpenNETCF.Drawing.FontEx descriptionFont = new OpenNETCF.Drawing.FontEx(labelDescription.Font.Name, labelDescription.Font.Size, labelDescription.Font.Style);
            //Size size = CargoMatrix.UI.CMXMeasureString.MeasureString(CreateGraphics(),description,labelDescription.ClientRectangle, false );
            OpenNETCF.Drawing.GraphicsEx g = OpenNETCF.Drawing.GraphicsEx.FromControl(this);
            int height = 0;
            if(description != null)
                height = (int)g.MeasureString(description, descriptionFont, labelDescription.Width).Height;
            int diff = Height - labelDescription.Height;
            this.Height = height + diff;
            if (image == null)
                labelDescription.Left = 1;
            //else
            //    pictureBox21.Image = image;

        }

        //public ShellItemA(string description, int height)
        //{
        //    InitializeComponent(description, height);
        //    labelDescription.Text = description;
        //   // Size size = CargoMatrix.UI.CMXMeasureString.MeasureString(description);
           
        //}
    }
}
