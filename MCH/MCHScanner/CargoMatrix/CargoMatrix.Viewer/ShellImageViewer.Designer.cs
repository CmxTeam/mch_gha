﻿namespace CargoMatrix.Viewer
{
    partial class ShellImageViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(m_thread != null)
                m_thread.Abort();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cameraTimer = new System.Windows.Forms.Timer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.horizontalSmoothListbox1 = new SmoothListbox.HorizontalSmoothListbox();
            this.labelNoRecords = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cameraTimer
            // 
            this.cameraTimer.Interval = 1000;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(23, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(195, 143);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // horizontalSmoothListbox1
            // 
            this.horizontalSmoothListbox1.BackColor = System.Drawing.Color.White;
            this.horizontalSmoothListbox1.Location = new System.Drawing.Point(0, 144);
            this.horizontalSmoothListbox1.Name = "horizontalSmoothListbox1";
            this.horizontalSmoothListbox1.Size = new System.Drawing.Size(240, 53);
            this.horizontalSmoothListbox1.TabIndex = 19;
            this.horizontalSmoothListbox1.UnselectEnabled = false;
            this.horizontalSmoothListbox1.ListItemClicked += new SmoothListbox.ListItemClickedHandler(this.horizontalSmoothListbox1_ListItemClicked);
            // 
            // labelNoRecords
            // 
            this.labelNoRecords.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelNoRecords.ForeColor = System.Drawing.Color.White;
            this.labelNoRecords.Location = new System.Drawing.Point(0, 55);
            this.labelNoRecords.Name = "labelNoRecords";
            this.labelNoRecords.Size = new System.Drawing.Size(240, 24);
            this.labelNoRecords.Text = "No Records Found";
            this.labelNoRecords.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelNoRecords.Visible = false;
            // 
            // ShellImageViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.labelNoRecords);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.horizontalSmoothListbox1);
            this.Name = "ShellImageViewer";
            this.Size = new System.Drawing.Size(240, 198);
            this.ResumeLayout(false);

        }

        #endregion

        private SmoothListbox.HorizontalSmoothListbox horizontalSmoothListbox1;
        public System.Windows.Forms.Timer cameraTimer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelNoRecords;

    }
}
