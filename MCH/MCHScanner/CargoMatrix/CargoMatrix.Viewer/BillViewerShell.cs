﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
//using SmoothListBox;

namespace CargoMatrix.Viewer
{
    public interface IShellPage
    { 
        void LoadControl();
        void UnLoadControl();
    }
    public class ShellPage : SmoothListbox.SmoothListBoxBase
    {
        protected CargoMatrix.Communication.DTO.ShellPage m_shellPage = null;
        string m_reference;
        int m_viewerSize;
        bool isLoaded = false;


        public ShellPage(CargoMatrix.Communication.DTO.ShellPage shellPage, string reference, int viewerSize)
        {
            m_shellPage = shellPage;
            m_reference = reference;
            labelEmpty.Visible = false;
            m_viewerSize = viewerSize;
            //isLoaded = false;
        }
        public void LoadControl()
        {
            
            if (isLoaded)
                return;

            if (m_shellPage != null)
            {
                List<Control> itemsList = new List<Control>(m_shellPage.m_items.Count);
                for (int i = 0; i < m_shellPage.m_items.Count; i++)
                {
                    
                    switch (m_shellPage.m_items[i].m_type)
                    {
                        case CargoMatrix.Communication.DTO.ShellItemType.CONTACT:
                            itemsList.Add(new Contact(m_shellPage.m_items[i].m_heading,
                                m_shellPage.m_items[i].m_description));
                            break;
                        case CargoMatrix.Communication.DTO.ShellItemType.SHELL_ITEM_A:
                            itemsList.Add(new ShellItemA(m_shellPage.m_items[i].m_description,
                                m_shellPage.m_items[i].m_image));
                            break;
                        case CargoMatrix.Communication.DTO.ShellItemType.SHELL_ITEM_B:
                            itemsList.Add(new ShellItemB(m_shellPage.m_items[i].m_heading,
                                m_shellPage.m_items[i].m_description));
                            break;
                        case CargoMatrix.Communication.DTO.ShellItemType.SHELL_ITEM_C:
                            itemsList.Add(new ShellItemC(m_shellPage.m_items[i].m_heading,
                                m_shellPage.m_items[i].m_description));
                            break;
                        case CargoMatrix.Communication.DTO.ShellItemType.SHELL_IMAGE_ITEM_A:
                            itemsList.Add(new ShellImageItemA(m_shellPage.m_items[i].m_description,
                                m_shellPage.m_items[i].m_image));
                            break;

                        case CargoMatrix.Communication.DTO.ShellItemType.SHELL_IMAGE_ITEM_B:
                            itemsList.Add(new ShellImageItemB(m_shellPage.m_items[i].m_heading,
                                m_shellPage.m_items[i].m_description,
                                m_shellPage.m_items[i].m_image));
                            break;

                        case CargoMatrix.Communication.DTO.ShellItemType.SHELL_IMAGE_ITEM_C:
                            itemsList.Add(new ShellImageItemC(m_shellPage.m_items[i].m_heading,
                                m_shellPage.m_items[i].m_description,
                                m_shellPage.m_items[i].m_image));
                            break;

                        case CargoMatrix.Communication.DTO.ShellItemType.SHELL_ITEM_PHOTO:
                            itemsList.Add(new ShellImageViewer(m_reference, m_viewerSize));
                            break;
                    }

                    

                }
                AddItems(itemsList.ToArray());
                if (itemsList.Count == 0)
                    this.labelEmpty.Visible = true;
                isLoaded = true;
            }
            

        }
        public void UnLoadControl()
        {
            this.RemoveAll();
            isLoaded = false;


        }
    }
    public partial class BillViewerShell : SmoothListbox.SmoothListbox
    {
        private int pageCount = 0;
        private int currentPageIndex = 0;
        private List<ShellPage> m_Pages;
        private bool m_bHeaderMenu = false;
        private string m_headerText;
        
        public BillViewerShell()
        {
            InitializeComponent();
            pictureBoxHeader.Image = Resources.Graphics.Skin.gen_dropdown;
            pictureBoxBookMarkPrevious.Image = Resources.Graphics.Skin.gen_btn_left;
            pictureBoxBookMarkNext.Image = Resources.Graphics.Skin.gen_btn_right;
            
            panelShellHeader.BringToFront();
            panelTitle.BringToFront();
            
            this.smoothListBoxMainList.AutoScroll -= smoothListBoxMainList_AutoScroll;
            //smoothListBoxMainList.Dispose();
            //smoothListBoxMainList = null;
            //labelTitle.Height = 0;
            //labelTitle.Visible = false;
            m_Pages = new List<ShellPage>();
            //panelBookMarks.Top =  panelTitle.Top - panelBookMarks.Height;
            panelBookMarks.Top = panelShellHeader.Bottom;
            panelBookMarks.Left = 0;
            panelBookMarks.Width = Width - 2;
            panelBookMarks.Height = Height - panelTitle.Height - panelShellHeader.Height - panelSoftkeys.Height - 4;

            foreach (Control control in smoothListBoxBookMarks.Controls)
                control.BackColor = Color.AliceBlue;
           
            
        }
        public void InsertPage(Control header, CargoMatrix.Communication.DTO.ShellPage page, string pageReference)
        {
            ShellPage newPage = new ShellPage(page, pageReference, 0);
            //newPage.BorderStyle = BorderStyle.FixedSingle;
            newPage.Name = header.Name;
            newPage.Top = panelShellHeader.Height + panelTitle.Height;
            newPage.Height = Height - panelTitle.Height - panelShellHeader.Height - panelSoftkeys.Height - 4;
            newPage.Left = 0;
            newPage.Width = Width;
            //newPage.m_itemsList = itemsList;
            //newPage.AddItems(itemsList);

            //newPage.LayoutItems();
            //foreach (Control control in newPage.Controls)
                //control.BackColor = Color.Maroon;
            //newPage.LayoutItems();
            m_Pages.Add(newPage);
            AddHeader(header);
            pageCount++;
            
            Controls.Add(newPage);
            
            //newPage.BringToFront();
            //panelBookMarks.BringToFront();
            
        }
        public void Reset()
        {
            smoothListBoxBookMarks.RemoveAll();
            this.smoothListBoxMainList.RemoveAll();
            smoothListBoxMainList.labelEmpty.Visible = false;
            for(int i=0;i<m_Pages.Count;i++)
            {
                m_Pages[i].UnLoadControl();
                m_Pages[i].Parent = null;
                m_Pages[i].Dispose();
                
            }
            m_Pages.Clear();
            pageCount = 0;
        
        }
        private void OpenBookMarksMenu()
        {
            panelBookMarks.Top = panelShellHeader.Height /*+ panelTitle.Height*/ - panelBookMarks.Height;// panelShellHeader.Bottom;// CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight;
            panelBookMarks.Visible = true;
            panelTitle.Visible = false;
            m_bHeaderMenu = true;
            smoothListBoxBookMarks.AutoScroll += BookMarksAutoScroll;
            smoothListBoxBookMarks.Scrollable = true;

            int bookmarksMenuAnimationStep = panelBookMarks.Height / 3;
            for (int i = 0; i < 3; i++)
            {
                panelBookMarks.Top += bookmarksMenuAnimationStep;
                panelBookMarks.Refresh();
            }
        }
        private void CloseBookMarksMenu()
        {
            m_bHeaderMenu = false;
            panelBookMarks.Visible = false;
            panelTitle.Visible = true;
            smoothListBoxBookMarks.AutoScroll -= BookMarksAutoScroll;

            pictureBoxHeader.Image = Resources.Graphics.Skin.gen_dropdown;
            //if (m_bOptionsMenu == true)
            //    DisappearOptionsMenu();
        }
        private void OpenNextPage()
        {
            if (m_bHeaderMenu == true)
                CloseBookMarksMenu();
            if (pageCount <= 0)
                return;
            SuspendLayout();
            int prevPageIndex = currentPageIndex;
            currentPageIndex++;
            currentPageIndex = currentPageIndex % pageCount;

            m_Pages[currentPageIndex].Left = Width;


            m_Pages[currentPageIndex].BringToFront();
            panelBookMarks.BringToFront();
            panelShellHeader.BringToFront();
            panelTitle.BringToFront();
            panelOptions.BringToFront();
            panelSoftkeys.BringToFront();
            SetPageAutoScroll(m_Pages[currentPageIndex]);

            ResumeLayout();

            int step = Width / 3;
            
            for (int i = 0; i < 3; i++)
            {
                m_Pages[currentPageIndex].Left -= step;
                m_Pages[prevPageIndex].Left -= step;
                m_Pages[currentPageIndex].Refresh();
                m_Pages[prevPageIndex].Refresh();
                
            }
            m_Pages[currentPageIndex].Left = 0;

            //labelHeader.Text = m_Pages[currentPageIndex].Name;
            HeaderText = m_Pages[currentPageIndex].Name;
            smoothListBoxBookMarks.Reset();
            (smoothListBoxBookMarks.Items[currentPageIndex] as SmoothListbox.IExtendedListItem).SelectedChanged(true);
            //(smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).Focus(true);
            smoothListBoxBookMarks.selectedItemsMap[smoothListBoxBookMarks.Items[currentPageIndex]] = true;
            Cursor.Current = Cursors.WaitCursor;
            m_Pages[currentPageIndex].LoadControl();
            Cursor.Current = Cursors.Default;


        }
        private void OpenPreviousPage()
        {
            if (m_bHeaderMenu == true)
                CloseBookMarksMenu();
            
            SuspendLayout();
            int prevPageIndex = currentPageIndex;
            currentPageIndex--;
            if (currentPageIndex < 0)
                currentPageIndex = pageCount - 1;

            //m_Pages[prevPageIndex].Left = (-Width);
            m_Pages[currentPageIndex].Left = (-Width);

            m_Pages[currentPageIndex].BringToFront();
            panelBookMarks.BringToFront();
            panelShellHeader.BringToFront();
            panelTitle.BringToFront();
            panelOptions.BringToFront();
            
            panelSoftkeys.BringToFront();
            SetPageAutoScroll(m_Pages[currentPageIndex]);

            ResumeLayout();

            int step = Width / 3;

            for (int i = 0; i < 3; i++)
            {
                m_Pages[currentPageIndex].Left += step;
                m_Pages[prevPageIndex].Left += step;
                m_Pages[currentPageIndex].Refresh();
                m_Pages[prevPageIndex].Refresh();

            }
            m_Pages[currentPageIndex].Left = 0;
            //labelHeader.Text = m_Pages[currentPageIndex].Name;
            HeaderText = m_Pages[currentPageIndex].Name;

            smoothListBoxBookMarks.Reset();
            (smoothListBoxBookMarks.Items[currentPageIndex] as SmoothListbox.IExtendedListItem).SelectedChanged(true);
            //(smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).Focus(true);
            smoothListBoxBookMarks.selectedItemsMap[smoothListBoxBookMarks.Items[currentPageIndex]] = true;
            Cursor.Current = Cursors.WaitCursor;
            m_Pages[currentPageIndex].LoadControl();
            Cursor.Current = Cursors.Default;
        
        }
        protected void OpenPage(int pageID)
        {
            if (m_bHeaderMenu == true)
                CloseBookMarksMenu();
            
            SuspendLayout();

            m_Pages[pageID].Left = 0;
            m_Pages[pageID].BringToFront();
            panelBookMarks.BringToFront();
            panelShellHeader.BringToFront();
            panelTitle.BringToFront();
            panelOptions.BringToFront();
            
            panelSoftkeys.BringToFront();
            currentPageIndex = pageID;

            SetPageAutoScroll(m_Pages[pageID]);

            //labelHeader.Text = m_Pages[currentPageIndex].Name;
            HeaderText = m_Pages[currentPageIndex].Name;
            ResumeLayout();

             smoothListBoxBookMarks.Reset();
             (smoothListBoxBookMarks.Items[currentPageIndex] as SmoothListbox.IExtendedListItem).SelectedChanged(true);
            //(smoothListBoxBookMarks.Items[currentPageIndex] as IExtendedListItem).Focus(true);
            smoothListBoxBookMarks.selectedItemsMap[smoothListBoxBookMarks.Items[currentPageIndex]] = true;
            Cursor.Current = Cursors.WaitCursor;
            m_Pages[currentPageIndex].LoadControl();
            Cursor.Current = Cursors.Default;

        
        }
        private void AddHeader(Control header)
        {
            if (header is SmoothListbox.IExtendedListItem)
            {
                (header as SmoothListbox.IExtendedListItem).SetID(pageCount);
                (header as SmoothListbox.IExtendedListItem).SetBackColor(Color.AliceBlue);
            }
                
            smoothListBoxBookMarks.AddItem(header);
            
        }

        void ResizeBookMarksMenu(SmoothListbox.SmoothListBoxBase menu)
        {
            menu.Left = 0;// panelOptionsList.Right;
            menu.Top = 0;// pictureBoxOptionsHeader.Height + 1;
            menu.Height = panelBookMarks.Height;// panelOptionsList.Height;// panelOptions.Height - pictureBoxOptionsHeader.Height;
            menu.Width = panelBookMarks.Width;// panelOptionsList.Width;// panelOptions.Width - panelOptionsBorder2.Width - panelOptionsBorder3.Width;
            //menuOptionsOuterPanel.Left = 2;
        }
        protected override void OnResize(EventArgs e)
        {

            //if (panelTitle != null && panelShellHeader != null)
              //  panelTitle.Height = panelShellHeader.Height;

            if (panelBookMarks != null)
                panelBookMarks.Height = Height /*- panelTitle.Height*/ - panelShellHeader.Height - panelSoftkeys.Height - 4;
            //if (panelBookMarks != null)
            //    panelBookMarks.Height = Height - labelTitle.Height - panelSoftkeys.Height;// -10;
            if (smoothListBoxBookMarks != null)
                ResizeBookMarksMenu(smoothListBoxBookMarks);

            base.OnResize(e);
        }

        protected override void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseDown(sender, e);

            else
            {
                if (m_bHeaderMenu)
                    smoothListBoxBookMarks.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);
                else
                {
                    m_Pages[currentPageIndex].Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);
                }
                if (pictureBoxDown.Enabled)
                {
                    DownButtonPressed(true);
                    //pictureBoxDown.Image = ViewerResource.btnDown_over;
                }
            
            }

            

        }
        protected override void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseUp(sender, e);

            else
            {
                if (m_bHeaderMenu)
                    smoothListBoxBookMarks.DownButtonPressed = false;
                else
                {
                    m_Pages[currentPageIndex].DownButtonPressed = false;
                }
                if (pictureBoxDown.Enabled)
                {
                    //pictureBoxDown.Image = ViewerResource.btnDown;
                    DownButtonPressed(false);
                }

            }
        }

        protected override void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxUp_MouseDown(sender, e);

            else
            {
                if (m_bHeaderMenu)
                    smoothListBoxBookMarks.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);
                else
                {
                    m_Pages[currentPageIndex].Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);
                }
                if (pictureBoxDown.Enabled)
                {
                    //pictureBoxUp.Image = ViewerResource.btnUp_over;
                    UpButtonPressed(true);
                }

            }
        }

        protected override void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxUp_MouseUp(sender, e);

            else
            {
                if (m_bHeaderMenu)
                    smoothListBoxBookMarks.UpButtonPressed = false;
                else
                {
                    m_Pages[currentPageIndex].UpButtonPressed = false;
                }
                if (pictureBoxUp.Enabled)
                {
                    //pictureBoxUp.Image = ViewerResource.btnUp;
                    UpButtonPressed(false);
                }

            }

        }

        private void optionsTimer_Tick(object sender, EventArgs e)
        {

        }

        private void bookMarksTimer_Tick(object sender, EventArgs e)
        {
            //Todo: Animation
        }

        private void pictureBoxHeader_Click(object sender, EventArgs e)
        {
            //panelBookMarks.BringToFront();
            
            if (m_bHeaderMenu == false)
            {
                pictureBoxHeader.Image = Resources.Graphics.Skin.gen_dropdown_over;
                pictureBoxHeader.Refresh();
                OpenBookMarksMenu();
                
                
            }
            else
            {
                CloseBookMarksMenu();
                
                //panelBookMarks.Visible = false;
                //m_bHeaderMenu = false;
            }
        }

        
        private void RemovePageAutoScroll()
        {
            for (int i = 0; i < m_Pages.Count; i++)
            {
                m_Pages[i].AutoScroll -= PageAutoScroll;

            }
        }
        private void SetPageAutoScroll(SmoothListbox.SmoothListBoxBase menu)
        {
            for (int i = 0; i < m_Pages.Count; i++)
            {
                m_Pages[i].AutoScroll -= PageAutoScroll;

            }
            menu.AutoScroll += PageAutoScroll;
            menu.Scrollable = true;

        }
        private void PageAutoScroll(SmoothListbox.SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            if (m_bHeaderMenu == false && m_bOptionsMenu == false)
            {
                switch (direction)
                {
                    case SmoothListbox.SmoothListBoxBase.DIRECTION.UP:
                        pictureBoxUp.Enabled = enable;
                        if (pictureBoxUp.Enabled == false)
                            m_Pages[currentPageIndex].UpButtonPressed = false;
                        break;
                    case SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN:
                        //pictureBoxBookMarkNext.Enabled = enable;
                        pictureBoxDown.Enabled = enable;
                        if (pictureBoxDown.Enabled == false)
                            m_Pages[currentPageIndex].DownButtonPressed = false;
                        break;
                }

            }

        }

        private void BookMarksAutoScroll(SmoothListbox.SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            if (m_bHeaderMenu == true && m_bOptionsMenu == false)
            {
                switch (direction)
                {
                    case SmoothListbox.SmoothListBoxBase.DIRECTION.UP:
                        pictureBoxUp.Enabled = enable;
                        if (pictureBoxUp.Enabled == false)
                             smoothListBoxBookMarks.UpButtonPressed = false;
                        break;
                    case SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN:
                        //pictureBoxBookMarkNext.Enabled = enable;
                        pictureBoxDown.Enabled = enable;
                        if (pictureBoxDown.Enabled == false)
                            smoothListBoxBookMarks.DownButtonPressed = false;
                        break;
                }

            }

        }

        private void pictureBoxBookMarkNext_Click(object sender, EventArgs e)
        {
            OpenNextPage();
        }

        private void pictureBoxBookMarkPrevious_Click(object sender, EventArgs e)
        {
            OpenPreviousPage();
        }

        private void smoothListBoxBookMarks_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is SmoothListbox.IExtendedListItem)
            {
                CloseBookMarksMenu();
                //listItem.Left = 0;
                OpenPage((listItem as SmoothListbox.IExtendedListItem).GetID());

            }
        }
        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            CloseBookMarksMenu();
            base.pictureBoxMenu_Click(sender, e);
        
        }

        private void pictureBoxBookMarkPrevious_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxBookMarkPrevious.Image = Resources.Graphics.Skin.gen_btn_left_over;//.back_over;
            pictureBoxBookMarkPrevious.Refresh();
            
        }

        private void pictureBoxBookMarkPrevious_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxBookMarkPrevious.Image = Resources.Graphics.Skin.gen_btn_left;//.back_reg;
        }

        private void pictureBoxBookMarkNext_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBoxBookMarkNext.Image = Resources.Graphics.Skin.gen_btn_right_over;//.forward_over;
            pictureBoxBookMarkNext.Refresh();
            
        }

        private void pictureBoxBookMarkNext_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBoxBookMarkNext.Image = Resources.Graphics.Skin.gen_btn_right;//.forward_reg;
        }

        public string HeaderText
        {
            set 
            {
                m_headerText = value;
                pictureBoxHeader.Refresh();
            }
        }


        Font headerFont = new Font(FontFamily.GenericSansSerif,12,FontStyle.Bold);    
        
        private void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            //SizeF textSize = e.Graphics.MeasureString(m_headerText, headerFont);
            //int x = ((pictureBoxHeader.Width - 32) - (int)textSize.Width) / 2;
            //int y = (pictureBoxHeader.Height - (int)textSize.Height) / 2;
            
            Rectangle strRect = new Rectangle(0,0, pictureBoxHeader.Width - 32, pictureBoxHeader.Height) ;
            
            StringFormat format = new StringFormat( StringFormatFlags.NoWrap);
            //format.FormatFlags = StringFormatFlags.NoWrap;
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            e.Graphics.DrawString(m_headerText, headerFont, new SolidBrush(Color.White),strRect, format);//, x, y);
            format.Dispose();

        }

        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            if (m_bHeaderMenu == true)
            {             
                CloseBookMarksMenu();
            }
            else
            {
                base.pictureBoxBack_Click(sender, e);
            }
                
            
        }

       
        
    }
}
