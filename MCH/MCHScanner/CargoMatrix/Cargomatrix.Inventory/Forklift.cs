﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.InventoryWS;
using CargoMatrix.Communication;
using System.Drawing;
using System.Windows.Forms;
using CMXBarcode;

namespace CargoMatrix.Inventory
{
    class Forklift : CargoMatrix.Utilities.MessageListBox
    {
        private static Forklift instance;
        private bool isLoaded;
        /// <summary>
        /// task Id for pre assigned tasks
        /// </summary>
        private int preAssignedTaskId;
        private CargoMatrix.Communication.ScannerUtilityWS.TaskSettings settings;
        private bool enableChangeLocation = true;
        private int itemsCount;
        
        public event EventHandler ItemCountChanged;

        public static bool Isloaded
        {
            get { return Instance.isLoaded; }
        }
        public int ForkliftID { get { return settings.ForkliftId; } }
        
        public bool InventryScanLocationFirst
        {
            get { return settings.InventryScanLocationFirst; }
        }
        public bool LocationDifferentfromCurrentCheck
        {
            get { return settings.SystemLocationDifferentfromCurrent; }
        }

        public int ItemsCount
        {
            internal set 
            {
                itemsCount = value;
                var handlers = this.ItemCountChanged;
                if (handlers != null)
                    handlers(this, EventArgs.Empty);
            }
            get { return itemsCount; }

        }

        internal bool EnableChangeLocation { get { return enableChangeLocation; } }
        internal string ScannedLocation { get; set; }
        internal bool IsLocationScanned { get { return !string.IsNullOrEmpty(ScannedLocation); } }
        
        
        private Forklift()
        {
            this.smoothListBoxBase1.MultiSelectEnabled = true;
            this.isLoaded = false;
            this.HeaderText2 = "Select HAWBs to Confirm Location";
            this.LoadListEvent += new CargoMatrix.Utilities.LoadSmoothList(Forklift_LoadListEvent);
            this.IsSelectable = false; // differen logic for hawbs in count mode
            Cursor.Current = Cursors.Default;
        }
        public static Forklift Instance
        {
            get
            {
                if (instance == null)
                    instance = new Forklift();


                if (instance.isLoaded == false)
                    instance.LoadAppSettings();

                return instance;
            }
        }


        private void LoadAppSettings()
        {
            var tempSettings = CargoMatrix.Communication.Inventory.Instance.GetTaskSettings();
            if (tempSettings.Transaction.TransactionStatus1 == true)
            {
                this.settings = tempSettings;
                this.isLoaded = true;
                ItemsCount = CargoMatrix.Communication.Inventory.Instance.GetForkliftItemsCount(TaskID, ForkliftID);
            }
            else
            {
                this.isLoaded = false;
                CargoMatrix.UI.CMXMessageBox.Show("Error has occured while downloading task settings. Refresh or restart the application", "Unable to connect", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }

        /// <summary>
        /// returns taskId from settings if user doing pre assigned inventory
        /// </summary>
        public int TaskID
        {
            get
            {
                if (enableChangeLocation == false)
                    return preAssignedTaskId;
                else
                    return settings.TaskId;
            }
            set { this.preAssignedTaskId = value; }
        }



        void Forklift_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            PopulateForkliftContent();
        }

        //public void DropAllIntoLocation(int locationID)
        //{
        //    TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.DropAllPiecesIntoLocation(TaskID, locationID);
        //    if (!status.TransactionStatus1)
        //        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        //}

        //public bool AddToForkliftCount(int hawbId, int count, ScanTypes scanType)
        //{
        //    TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.ScanHousebillIntoForklift(hawbId, count, TaskID, ScanModes.Count, scanType);
        //    if (!status.TransactionStatus1)
        //        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        //    else
        //        ItemsCount = status.TransactionRecords;
        //    return status.TransactionStatus1;
        //}
        //public bool AddToForkliftPiece(int hawbId, int piecId, ScanTypes scanType)
        //{
        //    DateTime dt = DateTime.Now; 
        //    TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.ScanHousebillIntoForklift(hawbId, piecId, TaskID, ScanModes.Piece, scanType);
        //    TimeSpan ts = DateTime.Now - dt;
        //    if (!status.TransactionStatus1)
        //        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        //    else
        //        ItemsCount = status.TransactionRecords;
        //    return status.TransactionStatus1;

        //}

        //public bool AddToForkliftPiece(string hawbNo, int piecId, ScanTypes scanType)
        //{
        //    DateTime dt = DateTime.Now;
        //    TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.ScanHousebillIntoForklift(hawbNo, piecId, TaskID,scanType);
        //    TimeSpan ts = DateTime.Now - dt;
        //    if (!status.TransactionStatus1)
        //        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        //    else
        //        ItemsCount = status.TransactionRecords;
        //    return status.TransactionStatus1;

        //}

        //void Forklift_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, System.Windows.Forms.Control listItem, bool isSelected)
        //{
        //    //foreach (var item in smoothListBoxMainList.Items)
        //    //{
        //    //    if ((item as CustomListItems.ComboBox).SubItemSelected == true)
        //    //    {
        //    //        ShowContinueButton(true);
        //    //        return;
        //    //    }
        //    //}
        //    //ShowContinueButton(false);
        //}

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {
            CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        DropForkliftManually(scanItem.Location);
                        exitflag = true;
                    }
                    else
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
                exitflag = true;

            } while (exitflag != true);

            PopulateForkliftContent();

            //if (DialogResult.OK == confirmLoc.ShowDialog())
            //{
            //    ScanItem scanItem = CargoMatrix.Communication.Inventory.Instance.ParseBarcode(confirmLoc.ScannedText, Forklift.Instance.TaskID);
            //    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door)
            //    {
            //        DropForkliftManually(scanItem.LocationId);
            //    }
            //    else
            //        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            //    PopulateForkliftContent();
            //}
        }

        private void DropForkliftManually(string LocName)
        {
            List<PieceItem> pieceItems = new List<PieceItem>();
            foreach (var item in smoothListBoxBase1.Items)
            {
                if (item is CustomListItems.ComboBox)
                {
                    foreach (var pieceId in (item as CustomListItems.ComboBox).SelectedIds)
                        pieceItems.Add(new PieceItem() { HouseBillId = (item as CustomListItems.ComboBox).ID, PieceId = pieceId });
                }
            }

            string mesage = string.Format(InventoryResources.Text_DropConfirm, pieceItems.Count, LocName);
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(mesage, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation, MessageBoxButtons.OKCancel, DialogResult.OK))
                return;
            TransactionStatus status = CargoMatrix.Communication.Inventory.Instance.DropPiecesIntoLocation(pieceItems.ToArray(), TaskID, LocName, ScanTypes.Manual, ForkliftID);
            if (!status.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        }

        private void PopulateForkliftContent()
        {
            this.HeaderText = "Scanned List: " + ScannedLocation;
            smoothListBoxBase1.RemoveAll();
            int piecesCount = 0;
            foreach (var hawb in CargoMatrix.Communication.Inventory.Instance.GetForkLiftHouseBills(TaskID, ForkliftID))
            {
                piecesCount += hawb.Pieces.Count();

                Image icon;
                string line2;
                List<CustomListItems.ComboBoxItemData> pieces = new List<CustomListItems.ComboBoxItemData>();
                if (hawb.ScanMode == ScanModes.Piece)
                {
                    icon = CargoMatrix.Resources.Skin.PieceMode;
                    line2 = string.Format("Pieces: {0} of {1}", hawb.Pieces.Length, hawb.TotalPieces);
                    foreach (var hawbPiece in hawb.Pieces)
                    {
                        pieces.Add(new CustomListItems.ComboBoxItemData(string.Format("Piece {0}", hawbPiece.PieceNumber), string.Empty, hawbPiece.PieceId) { isReadonly = false, IsSelectable = false });
                    }
                }
                else
                {
                    icon = CargoMatrix.Resources.Skin.countMode;
                    line2 = string.Format("Pieces: {0} of {1}", hawb.Pieces.Length, hawb.TotalPieces);
                }

                CustomListItems.ComboBox combo = new CustomListItems.ComboBox(hawb.HousebillId, hawb.Reference(), line2, icon, pieces);
                if (hawb.ScanMode == ScanModes.Count)
                    combo.Expandable = false;
                combo.ContainerName = "ScanView";
                combo.IsSelectable = false;
                //combo.ReadOnly = true;
                combo.LayoutChanged += new CustomListItems.ComboBox.ComboBoxLayoutChangedHandler(combo_LayoutChanged);
                combo.SelectionChanged += new EventHandler(combo_SelectionChanged);
                smoothListBoxBase1.AddItem(combo);
            }
            ItemsCount = piecesCount;
            if (smoothListBoxBase1.ItemsCount == 0)
                DialogResult = DialogResult.Cancel;
            Cursor.Current = Cursors.Default;
        }

        void combo_LayoutChanged(CustomListItems.ComboBox sender, int subItemId, bool comboIsEmpty)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (comboIsEmpty)
                smoothListBoxBase1.RemoveItem(sender as Control);
            combo_SelectionChanged(null, null);
            smoothListBoxBase1.LayoutItems();
            TransactionStatus finalStatus = new TransactionStatus() { TransactionStatus1 = true };

            if (subItemId == -1)
            {
                finalStatus = CargoMatrix.Communication.Inventory.Instance.RemoveHousebillFromForklift(sender.ID, TaskID, ForkliftID);
            }
            else
            {
                finalStatus = CargoMatrix.Communication.Inventory.Instance.RemovePieceFromForklift(sender.ID, subItemId, TaskID, ForkliftID, ScanModes.Piece);
            }

            if (finalStatus.TransactionStatus1 == false)
                CargoMatrix.UI.CMXMessageBox.Show(finalStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            else ItemsCount = finalStatus.TransactionRecords;

            if (smoothListBoxBase1.ItemsCount == 0)
                DialogResult = DialogResult.Cancel;
            Cursor.Current = Cursors.Default;
        }

        void combo_SelectionChanged(object sender, EventArgs e)
        {
            if (smoothListBoxBase1.SelectedItems.Count == 0)
            {
                bool enableflag = false;

                CustomListItems.ComboBox item = sender as CustomListItems.ComboBox;
                if (item is CustomListItems.ComboBox && (item as CustomListItems.ComboBox).SubItemSelected)
                {
                    enableflag = true;
                }
                OkEnabled = enableflag;
            }
            else
                OkEnabled = true;
        }

        internal void SetPreAssignedLocation(string location, int taskId)
        {
            Forklift.Instance.ScannedLocation = location;
            this.enableChangeLocation = false;
            this.preAssignedTaskId = taskId;
        }

        internal void ResetPreAssignedLocation()
        {
            Forklift.Instance.ScannedLocation = string.Empty;
            this.enableChangeLocation = true;
        }
    }
}
