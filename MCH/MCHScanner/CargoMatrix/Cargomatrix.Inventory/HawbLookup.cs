﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CustomListItems;
using System.Drawing;
using CargoMatrix.Communication.ScannerUtilityWS;

namespace CargoMatrix.Inventory
{
    class HawbLookup
    {
        private static CargoMatrix.Utilities.MessageListBox instance;
        private HawbLookup()
        {
        }
        public static void Show()
        {
            CustomUtilities.ScanEnterPopup_old hawbNoPopup = new CustomUtilities.ScanEnterPopup_old( CustomUtilities.BarcodeType.Housebill);
            hawbNoPopup.DisplayHousebillPiece = false;
            hawbNoPopup.Title = "Enter Housebill Number";
            if (DialogResult.OK == hawbNoPopup.ShowDialog())
            {
                if (instance == null)
                    instance = new CargoMatrix.Utilities.MessageListBox();

                LoadControl(hawbNoPopup.ScannedText);
                instance.ShowDialog();
            }
        }
        private static void LoadControl(string hwabNo)
        {
            Cursor.Current = Cursors.WaitCursor;
            instance.HeaderText = hwabNo;
            instance.HeaderText2 = "Housebill Pieces Locations";
            instance.MultiSelectListEnabled = false;
            instance.IsSelectable = false;
            instance.IsSelectable = false;
            var locations = CargoMatrix.Communication.ScannerUtility.Instance.GetPiecesByLocation(hwabNo);
            int totalpieces = locations.Sum(l => l.Piece.Length);
            foreach (var Loc in locations)
            {
                //var subItems = from pc in Loc.Piece
                //               select new ExpenListItemData(pc.PieceId,"Piece " + pc.PieceNumber, string.Empty) { IsSelectable = false, IsReadonly = true };

                //var headerData = new ExpenListItemData(Loc.LocationId, Loc.Location, string.Format("Pieces: {0} of {1}", Loc.Piece.Length, totalpieces)) { IsReadonly = true, IsSelectable = false };
                
                Image icon = null;
                switch (Loc.LocationType)
                {
                    case LocationTypes.Door:
                        icon = Resources.Icons.Door;
                        break;
                    case LocationTypes.Truck:
                        icon = Resources.Icons.Truck;
                        break;
                    case LocationTypes.Uld:
                        icon = Resources.Icons.ULD;
                        break;
                    case LocationTypes.Area:
                    case LocationTypes.ScreeningArea:
                    case LocationTypes.Location:
                    case LocationTypes.Forklift:
                    default:
                        icon = CargoMatrix.Resources.Icons.Selection;
                        break;

                }
                //var LocationItem = new ExpandListItem(headerData, icon);
                //LocationItem.AddExpandItemsRange(subItems);
               var listItem = new SmoothListbox.ListItems.TwoLineListItem(Loc.Location,icon, string.Format("Pieces: {0} of {1}", Loc.Piece.Length, totalpieces));
               instance.smoothListBoxBase1.AddItem2(listItem);
            }
            instance.smoothListBoxBase1.LayoutItems();
            instance.smoothListBoxBase1.RefreshScroll();

            Cursor.Current = Cursors.Default;
        }
    }
}
