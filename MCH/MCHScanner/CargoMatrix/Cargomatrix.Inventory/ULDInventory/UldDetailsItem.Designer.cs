﻿namespace CargoMatrix.Inventory
{
    partial class UldDetailsItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelWeight = new System.Windows.Forms.Label();
            this.labelLength = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.labelWidth = new System.Windows.Forms.Label();
            this.itemPicture = new OpenNETCF.Windows.Forms.PictureBox2();
            this.buttonPrint = new CargoMatrix.UI.CMXPictureButton();
            this.buttonDamage = new CargoMatrix.UI.CMXPictureButton();
            this.labelHeight = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            //this.labelLastScan = new System.Windows.Forms.Label();
            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            //this.labelDescr = new System.Windows.Forms.Label();
            this.labelCarrier = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            //this.label6 = new System.Windows.Forms.Label();
            //this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(44, 8);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(190, 14);
            this.title.Text = "ULD Number";
            // 
            // labelCarrier
            // 
            this.labelCarrier.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelCarrier.Location = new System.Drawing.Point(80, 32);
            this.labelCarrier.Name = "labelCarrier";
            this.labelCarrier.Size = new System.Drawing.Size(121, 13);
            this.labelCarrier.Text = "labelcarrier";
            // 
            // labelWeight
            // 
            this.labelWeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelWeight.Location = new System.Drawing.Point(80, 47);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(121, 13);
            this.labelWeight.Text = "Weight";
            // 
            // labelLength
            // 
            this.labelLength.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelLength.Location = new System.Drawing.Point(80, 63);
            this.labelLength.Name = "labelPieces";
            this.labelLength.Size = new System.Drawing.Size(121, 13);
            this.labelLength.Text = "Pieces";
            // 
            // labelWidth
            // 
            this.labelWidth.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelWidth.Location = new System.Drawing.Point(80, 79);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(121, 13);
            this.labelWidth.Text = "width";
            // 
            // labelHeight
            // 
            this.labelHeight.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelHeight.Location = new System.Drawing.Point(80, 95);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(121, 13);
            this.labelHeight.Text = "Sender";
            // 
            // labelDays
            // 
            this.labelDays.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.labelDays.Location = new System.Drawing.Point(80, 111);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(121, 13);
            this.labelDays.Text = "Receiver";
            //// 
            //// labelLastScan
            //// 
            //this.labelLastScan.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            //this.labelLastScan.Location = new System.Drawing.Point(80, 127);
            //this.labelLastScan.Name = "labelLastScan";
            //this.labelLastScan.Size = new System.Drawing.Size(121, 13);
            //this.labelLastScan.Text = "12/06 12:06";
            //// 
            //// labelDescr
            //// 
            //this.labelDescr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            //this.labelDescr.Location = new System.Drawing.Point(80, 143);
            //this.labelDescr.Name = "labelDescr";
            //this.labelDescr.Size = new System.Drawing.Size(121, 13);
            //this.labelDescr.Text = "Text jkashdfajksdfasdfasd asdf asdf asdf";
            // 
            // panelIndicators
            // 
            this.panelIndicators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIndicators.Location = new System.Drawing.Point(3, 161);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(234, 16);
            this.panelIndicators.TabIndex = 19;
            // 
            // itemPicture
            // 
            this.itemPicture.BackColor = System.Drawing.SystemColors.Control;
            this.itemPicture.Location = new System.Drawing.Point(7, 5);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(24, 24);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemPicture.TransparentColor = System.Drawing.Color.White;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(7, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.Text = "Carrier :";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(7, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.Text = "Length :";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(7, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.Text = "Width :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(7, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.Text = "Height :";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(7, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.Text = "No of Days :";
            //// 
            //// label6
            //// 
            //this.label6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            //this.label6.Location = new System.Drawing.Point(7, 127);
            //this.label6.Name = "label6";
            //this.label6.Size = new System.Drawing.Size(67, 13);
            //this.label6.Text = "LastScan :";
            //// 
            //// label7
            //// 
            //this.label7.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            //this.label7.Location = new System.Drawing.Point(7, 143);
            //this.label7.Name = "label7";
            //this.label7.Size = new System.Drawing.Size(67, 13);
            //this.label7.Text = "Descr. :";
            // 
            // label9 : Max Weight
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(7, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.Text = "Max Weight :";
            // 
            // buttonDetails
            // 
            this.buttonPrint.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPrint.Location = new System.Drawing.Point(202, 33);
            this.buttonPrint.Name = "buttonDetails";
            this.buttonPrint.Size = new System.Drawing.Size(32, 32);
            this.buttonPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonPrint.TransparentColor = System.Drawing.Color.White;
            this.buttonPrint.Image = CargoMatrix.Resources.Skin.printerButton;
            this.buttonPrint.PressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonDamage
            // 
            this.buttonDamage.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDamage.Location = new System.Drawing.Point(202, 71);
            this.buttonDamage.Name = "buttonDamage";
            this.buttonDamage.Size = new System.Drawing.Size(32, 32);
            this.buttonDamage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonDamage.TransparentColor = System.Drawing.Color.White;
            this.buttonDamage.Image = CargoMatrix.Resources.Skin.damage;
            this.buttonDamage.PressedImage = CargoMatrix.Resources.Skin.damage_over;
            this.buttonDamage.Click += new System.EventHandler(this.buttonDamage_Click);
            // 
            // HawbDetailedItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            //this.Controls.Add(this.label7);
            //this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labelCarrier);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.labelLength);
            //this.Controls.Add(this.labelDescr);
            //this.Controls.Add(this.labelLastScan);
            this.Controls.Add(this.labelDays);
            this.Controls.Add(this.labelHeight);
            this.Controls.Add(this.labelWidth);
            this.Controls.Add(this.title);
            this.Controls.Add(this.itemPicture);
            this.Controls.Add(this.buttonDamage);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.panelIndicators);
            this.Name = "HawbDetailedItem";
            this.Size = new System.Drawing.Size(240, 177);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label labelCarrier;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelLength;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.Label labelLastScan;
        private System.Windows.Forms.Label labelDescr;
        private CustomUtilities.IndicatorPanel panelIndicators;
        private CargoMatrix.UI.CMXPictureButton buttonPrint;
        private OpenNETCF.Windows.Forms.PictureBox2 itemPicture;
        private CargoMatrix.UI.CMXPictureButton buttonDamage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;

    }
}
