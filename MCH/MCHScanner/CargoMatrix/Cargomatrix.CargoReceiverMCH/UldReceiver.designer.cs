﻿namespace CargoMatrix.CargoReceiver
{
    partial class UldReceiver
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            //this.buttonFilter = new CargoMatrix.UI.CMXPictureButton();
            this.buttonScannedList = new CargoMatrix.UI.CMXCounterIcon();
            this.buttonSearch = new CargoMatrix.UI.CMXPictureButton();
            this.ProceedButton = new OpenNETCF.Windows.Forms.Button2();
            this.UldButton = new OpenNETCF.Windows.Forms.Button2();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panelHeader2.SuspendLayout();
  

            // 
            // itemPicture
            // 
            this.itemPicture = new System.Windows.Forms.PictureBox();
            this.itemPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.itemPicture.BackColor = System.Drawing.Color.White;
            this.itemPicture.Location = new System.Drawing.Point(6, 10);//new System.Drawing.Point(202, 41);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(32, 32);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            //
            // searchbox
            //
            this.searchBox = new CargoMatrix.UI.CMXTextBox();
            this.searchBox.Location = new System.Drawing.Point(6,108);
            this.searchBox.Size = new System.Drawing.Size(195,31);
            this.searchBox.TextChanged += new System.EventHandler(searchBox_TextChanged);
            this.searchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchBox_KeyDown);
            this.searchBox.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular);
            this.searchBox.Visible = false;
            //
            //label1
            //
            this.label1 = new System.Windows.Forms.Label();
            this.label1.Location = new System.Drawing.Point(40, 13);
            this.label1.Size = new System.Drawing.Size(162, 23);
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            //
            //label2
            //
            this.label2 = new System.Windows.Forms.Label();
            this.label2.Location = new System.Drawing.Point(40, 36);
            this.label2.Size = new System.Drawing.Size(140, 15);
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            //
            //label3
            //
            this.label3 = new System.Windows.Forms.Label();
            this.label3.Location = new System.Drawing.Point(40, 57);
            this.label3.Size = new System.Drawing.Size(162, 15);
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            //
            //label3
            //
            this.label4 = new System.Windows.Forms.Label();
            this.label4.Location = new System.Drawing.Point(40, 78);
            this.label4.Size = new System.Drawing.Size(162, 15);
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            //
            //label3
            //
            this.label6 = new System.Windows.Forms.Label();
            this.label6.Location = new System.Drawing.Point(6, 145);
            this.label6.Size = new System.Drawing.Size(228, 45);
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label6.Text = "Last Scanned: N/A";
            this.label6.Visible = false;
      
            //
            //
            //
            //this.buttonFilter.Location = new System.Drawing.Point(202, 10);
            //this.buttonFilter.Size = new System.Drawing.Size(32, 32);
            //this.buttonFilter.Image = CargoMatrix.Resources.Skin.Forklift_btn;
            //this.buttonFilter.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over;
            //this.buttonFilter.Click += new System.EventHandler(buttonFilter_Click);


            this.buttonScannedList.Location = new System.Drawing.Point(202, 10);
            this.buttonScannedList.Name = "buttonScannedList";
            this.buttonScannedList.Image = CargoMatrix.Resources.Skin.Forklift_btn;
            this.buttonScannedList.PressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over;
            this.buttonScannedList.Size = new System.Drawing.Size(32, 32);
            this.buttonScannedList.TabIndex = 1;
            this.buttonScannedList.Click += new System.EventHandler(buttonScannedList_Click);
            this.buttonScannedList.Visible = false;

            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(0, 194);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(240, 40);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;


            this.buttonSearch.Location = new System.Drawing.Point(202, 108);
            this.buttonSearch.Size = new System.Drawing.Size(32, 32);
            this.buttonSearch.Image = CargoMatrix.Resources.Skin.magnify_btn;
            this.buttonSearch.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            this.buttonSearch.Click += new System.EventHandler(buttonSearch_Click);

            //
            //ProceedButton
            //
 
            this.ProceedButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProceedButton.BackColor = System.Drawing.SystemColors.Control;
            this.ProceedButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProceedButton.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ProceedButton.ForeColor = System.Drawing.Color.White;
            this.ProceedButton.Location = new System.Drawing.Point(133, 196);
            this.ProceedButton.Name = "ProceedButton";
            this.ProceedButton.Size = new System.Drawing.Size(80, 32);
            this.ProceedButton.TabIndex = 17;
            this.ProceedButton.Text = "DROP";
            this.ProceedButton.Click += new System.EventHandler(ProceedButton_Click);
            this.ProceedButton.Visible = false;


            this.UldButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
            this.UldButton.BackColor = System.Drawing.SystemColors.Control;
            this.UldButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UldButton.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.UldButton.ForeColor = System.Drawing.Color.White;
            this.UldButton.Location = new System.Drawing.Point(25, 196);
            this.UldButton.Name = "UldButton";
            this.UldButton.Size = new System.Drawing.Size(80, 32);
            this.UldButton.TabIndex = 17;
            this.UldButton.Text = "ULD";
            this.UldButton.Click += new System.EventHandler(UldViewButton_Click);
            this.UldButton.Visible = false;
            //
            //
            //
            this.panelHeader2.Controls.Add(searchBox);
            //this.panelHeader2.Controls.Add(buttonFilter);
            this.panelHeader2.Controls.Add(buttonScannedList);
            this.panelHeader2.Controls.Add(buttonSearch);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
            this.panelHeader2.Controls.Add(label1);
            this.panelHeader2.Controls.Add(label2);
            this.panelHeader2.Controls.Add(label3);
            this.panelHeader2.Controls.Add(label4);
            this.panelHeader2.Controls.Add(label6);
            this.panelHeader2.Controls.Add(itemPicture);
            this.panelHeader2.Controls.Add(this.ProceedButton);
            this.panelHeader2.Controls.Add(this.UldButton);
            this.panelHeader2.Controls.Add(this.pictureBox2);  
            this.ProceedButton.BringToFront();

           // panelHeader2.Height = 49;
            panelHeader2.Height = 292;
            panelHeader2.Visible = true;
            this.Size = new System.Drawing.Size(240,292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.ResumeLayout(false);

        }


        private System.Windows.Forms.PictureBox pictureBox2;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.Label label4;
        protected System.Windows.Forms.Label label6;
        //protected CargoMatrix.UI.CMXPictureButton buttonFilter;
        private CargoMatrix.UI.CMXCounterIcon buttonScannedList;
        protected CargoMatrix.UI.CMXPictureButton buttonSearch;
        private System.Windows.Forms.PictureBox itemPicture;
        internal OpenNETCF.Windows.Forms.Button2 ProceedButton;
        internal OpenNETCF.Windows.Forms.Button2 UldButton;


        #endregion
    }
}
