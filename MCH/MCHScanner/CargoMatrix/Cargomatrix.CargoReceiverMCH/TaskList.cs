﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CustomListItems;
using CMXExtensions;
using System.Collections.Generic;
using CargoMatrix.Communication.WSCargoReceiver;
using CMXBarcode;
using SmoothListbox;
using System.Linq;

namespace CargoMatrix.CargoReceiver
{
    public class TaskList : FlightList
    {
        public TaskList()
            : base()
        {
            this.Name = "TaskList";
 
            //this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
        }


        public override void LoadControl()
        {
            smoothListBoxMainList.RemoveAll();
      
                Cursor.Current = Cursors.WaitCursor;
                this.label1.Text = string.Format("{0}/{1}", filter, sort);
                this.searchBox.Text = string.Empty;
                this.Refresh();
                Cursor.Current = Cursors.WaitCursor;


                smoothListBoxMainList.AddItemToFront(new StaticListItem("Receive New", Resources.Icons.Notepad_Add));

                //var items = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillList(Forklift.Instance.ReceiveMode, sort, filter, true);
                //this.TitleText = string.Format("CargoReceiver - ({0})", items.Length);

               

                         CargoMatrix.Communication.DTO.FlightItem[] flightItems = null;


                         if (CargoMatrix.Communication.WebServiceManager.Instance().GetFlightsMCH(out flightItems,FlightStatus.All,"","","", FlightSort.Carrier))
                         {
                             var rItems = from i in flightItems
                                          select InitializeItem(i);
                             smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                         }

                         this.TitleText = string.Format("CargoReceiver - ({0})", flightItems.Length);

                //var rItems = from i in items
                //             select InitializeItem(i);

                //smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());


                         Cursor.Current = Cursors.Default;

            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

     

        protected override void MawbList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is StaticListItem)
                MessageBox.Show("Feature Not Available");
            else
                base.MawbList_ListItemClicked(sender, listItem, isSelected);
        }

        protected override void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            /// show count - 1  -1 is the Receive New button
            this.TitleText = string.Format("CargoReceiver - ({0})", smoothListBoxMainList.VisibleItemsCount - 1);
        }

    }
}
