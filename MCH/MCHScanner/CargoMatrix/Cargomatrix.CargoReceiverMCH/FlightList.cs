﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;


namespace CargoMatrix.CargoReceiver
{
    public partial class FlightList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
        private int listItemHeight;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;

        public FlightList()
        {

            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(MawbList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(MawbList_ListItemClicked);
        }

        protected virtual void MawbList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            //if (CargoMatrix.Communication.Utilities.IsAdmin)
            //{
            MAWB_Enter_Click(listItem, null);
            //}
            //else
            //{
            //    smoothListBoxMainList.MoveControlToTop(listItem);
            //    smoothListBoxMainList.LayoutItems();
            //}

        }
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            BarcodeEnabled = false;

            if (ShowFilterPopup() == false)
                BarcodeEnabled = true;
        }
        void MawbList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO:
                        string carrier, mawb, hawb;
                        if (DialogResult.OK == ManualHawbMawb.Show("Enter Manually", "Enter Mawb or Hawb Details", out carrier, out mawb, out hawb))
                        {
                            var tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(carrier, mawb, hawb);
                            //ProceedWithFlight(tempMawb);
                        }
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Flight);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter + " / " + sort;
                LoadControl();
                return true;
            }
            return false;
        }

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
            //this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.ENTER_HAWB_NO) { Title = "Manual Lookup" });
            //this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;

            this.label1.Text = string.Format("{0}/{1}", filter, sort);
            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;
            // var items = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillList(Forklift.Instance.ReceiveMode, sort, filter, false);
            //this.TitleText = string.Format("CargoReceiver - {0} - ({1})", Forklift.Instance.ReceiveMode.ToString(), items.Length);




            CargoMatrix.Communication.DTO.FlightItem[] flightItems = null;

            FlightStatus status = FlightStatus.All;
            switch (filter)
            {
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                    status = FlightStatus.Pending;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
                    status = FlightStatus.InProgress;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
                    status = FlightStatus.Completed;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                    status = FlightStatus.Open;
                    break;
                default:
                    status = FlightStatus.All;
                    break;
            }

            FlightSort sortfield = FlightSort.Carrier;
            switch (sort)
            {
                case CargoMatrix.Communication.DTO.FlightSorts.ORIGIN:
                    sortfield = FlightSort.Origin;
                    break;
                case CargoMatrix.Communication.DTO.FlightSorts.CARRIER:
                    sortfield = FlightSort.Carrier;
                    break;
                case CargoMatrix.Communication.DTO.FlightSorts.FLIGHTNO:
                    sortfield = FlightSort.FlightNo;
                    break;
                default:
                    sortfield = FlightSort.Carrier;
                    break;
            }


            if (CargoMatrix.Communication.WebServiceManager.Instance().GetFlightsMCH(out flightItems, status, "", "", "", sortfield))
            {
                var rItems = from i in flightItems
                             select InitializeItem(i);
                smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
            }

            this.TitleText = string.Format("CargoReceiver - ({0})", flightItems.Length);

            Cursor.Current = Cursors.Default;
        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            //this.TitleText = string.Format("CargoReceiver - {0} - ({1})", Forklift.Instance.ReceiveMode.ToString(), smoothListBoxMainList.VisibleItemsCount);
            this.TitleText = string.Format("CargoReceiver - {0}", smoothListBoxMainList.VisibleItemsCount);
        }

        protected ICustomRenderItem InitializeItem(FlightItem item)
        {
            FlightCargoItem tempMAWB = new FlightCargoItem(item);
            tempMAWB.OnEnterClick += new EventHandler(MAWB_Enter_Click);
            //tempMAWB.ButtonClick += new EventHandler(MAWB_OnMoreClick);

            listItemHeight = tempMAWB.Height;
            return tempMAWB;
        }

        void MAWB_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            var mawb = (sender as FlightCargoItem).ItemData;
            Cursor.Current = Cursors.Default;
            //MAWB_PrintLabels(mawb);

        }
        private void MAWB_PrintLabels(IMasterBillItem item)
        {
            BarcodeEnabled = false;
            CustomUtilities.HawbPiecesLabelPrinter.Show(item.MasterBillNumber, item.CarrierNumber);
            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        //private void MAWB_DamageCapture(IMasterBillItem item)
        //{
        //    Cursor.Current = Cursors.WaitCursor;

        //    MssterBillAsHouseBill masterAsHouseBill = new MssterBillAsHouseBill(item);

        //    DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(masterAsHouseBill, DamageCapture.DamageCaptureApplyTypeEnum.MasterBill);

        //    damageCapture.OnLoadShipmentConditions += (sndr, args) =>
        //    {
        //        args.ShipmentConditions = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionTypes();
        //    };

        //    damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
        //    {
        //        args.ConditionsSummaries = CargoMatrix.Communication.ScannerUtility.Instance.GetMasterBillShipmentConditionsSummarys(item.TaskId);
        //    };

        //    damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
        //    {
        //        args.Result = CargoMatrix.Communication.ScannerUtility.Instance.UpdateMasterBillConditions(item.TaskId, args.ShipmentConditions);
        //    };

        //    CMXAnimationmanager.DisplayForm(damageCapture);
        //    Cursor.Current = Cursors.Default;
        //}

        //private void MAWB_DetailsViewer(IMasterBillItem item)
        //{
        //    CargoMatrix.Viewer.HousebillViewer billViewer = new CargoMatrix.Viewer.HousebillViewer(item.CarrierNumber, item.MasterBillNumber);
        //    billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
        //    billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
        //    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        //}

        protected virtual void MAWB_Enter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            FlightItem flight = (sender as FlightCargoItem).ItemData;

            string reference = string.Format("{0}  {1}  {2}", flight.Origin, flight.CarrierCode, flight.FlightNumber);
            CargoMatrix.Communication.WSCargoReceiverMCHService.Alert[] alerts = CargoMatrix.Communication.WebServiceManager.Instance().GetFlightAlertsMCH(flight.TaskId, flight.FlightManifestId);
            foreach (CargoMatrix.Communication.WSCargoReceiverMCHService.Alert item in alerts)
            {
                CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + reference, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }



            //var tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(mawb.CarrierNumber, mawb.MasterBillNumber, string.Empty);
            ProceedWithFlight(flight);



            Cursor.Current = Cursors.Default;
        }

        private bool ProceedWithFlight(FlightItem tempFlight)
        {
            if (tempFlight == null)
                return false;


            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightReceiver(tempFlight));
            Cursor.Current = Cursors.Default;
            return true;
        }

        void MawbList_BarcodeReadNotify(string barcodeData)
        {
            //Cursor.Current = Cursors.WaitCursor;
            //var barcode = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            //CargoMatrix.Communication.WSCargoReceiver.MasterBillItem tempMawb = null;
            //switch (barcode.BarcodeType)
            //{
            //    case BarcodeTypes.HouseBill:
            //        CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
            //        //Forklift.Instance.FirstScannedHousebill = barcodeData;
            //        tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(string.Empty, string.Empty, barcode.HouseBillNumber);
            //        break;
            //    case BarcodeTypes.MasterBill:
            //        CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);
            //        tempMawb = CargoMatrix.Communication.CargoReceiver.Instance.GetMasterbillInfo(barcode.CarrierNumber, barcode.MasterBillNumber, string.Empty);
            //        break;
            //    default:
            //        CargoMatrix.UI.CMXMessageBox.Show(CargoMatrix.CargoReceiverMCH.CargoReceiverResources.Text_ScanHwbMawb, "Invalid Barcode", CMXMessageBoxIcon.Hand);
            //        break;
            //}

            ////if (ProceedWithFlight(tempMawb))
            ////    return;
            ////else
            ////{

            ////}


            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }

    }
}