﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;

namespace CargoMatrix.CargoReceiver
{
    public partial class ShipmentCargoItem : CustomListItems.ExpandableRenderListitem<ShipmentItem>, ISmartListItem
    {

        public event EventHandler ButtonClick;

        public ShipmentCargoItem(ShipmentItem shipment)
            : base(shipment)
        {
            InitializeComponent();

          
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this, EventArgs.Empty);
            }
        }


        protected override void InitializeControls()
        {
            this.SuspendLayout();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBrowse.Location = new System.Drawing.Point(202, 6);
            this.buttonBrowse.Name = "btnMore";
            this.buttonBrowse.Image = Resources.Skin._3dots_btn;
            this.buttonBrowse.PressedImage = Resources.Skin._3dots_btn_over;
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            this.buttonBrowse.Click += new System.EventHandler(buttonBrowse_Click);


            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.panelIndicators.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelIndicators.Location = new System.Drawing.Point(4, 55);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(230, 16);
            this.panelIndicators.TabIndex = 19;


            if (ItemData.IsForkLift)
            {
                this.Controls.Add(this.buttonBrowse);
            }
            this.Controls.Add(panelIndicators);

            this.buttonBrowse.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.ResumeLayout(false);
        }

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Format("HWB: {0}",ItemData.Hwb);
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line2 = string.Format("AWB: {0}", ItemData.AWB);
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 17 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular))
                {
                    string line3= string.Empty;

                    try
                    {
                        if (ItemData.UldType.ToUpper() == "LOOSE")
                        {
                            line3 = string.Format("ULD: {0}", ItemData.UldType);
                        }
                        else
                        {
                            line3 = string.Format("ULD: {0}{1}", ItemData.UldType, ItemData.UldSerialNo);
                        }
                    }
                    catch
                    {
                        line3 = string.Format("ULD: {0}", "N/A");

                    }
                    if (ItemData.Flags == 0)
                    {
                        this.panelIndicators.Visible = false;
                    }
                    else
                    { 
                      this.panelIndicators.Flags = ItemData.Flags;
                      this.panelIndicators.Visible=true;
                    }
                  
 
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 31 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular))
                {
                    string line4 =string.Empty;
                    if(ItemData.IsForkLift)
                    {
                        line4 = string.Format("PCS: {0} of {1}", ItemData.ForkliftPieces, ItemData.TotalPieces);
                    }
                    else
                    {
                        int scannedPieces = ItemData.ReceivedPieces + ItemData.ForkliftPieces;
                        line4 = string.Format("PCS: {0} of {1}", scannedPieces, ItemData.TotalPieces);
                    }
                    gOffScreen.DrawString(line4, fnt, brush, new RectangleF(40 * hScale, 45 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular))
                {
                    string line5 = string.Format("LOC: {0}", ItemData.Locations);
                    //gOffScreen.DrawString(line5, fnt, brush, new RectangleF(40 * hScale, 58 * vScale, 170 * hScale, 13 * vScale));
                    gOffScreen.DrawString(line5, fnt, brush, new RectangleF(134 * hScale, 45 * vScale, 170 * hScale, 13 * vScale));
                }


                this.buttonBrowse.Image = CargoMatrix.Resources.Skin.cc_trash;
                this.buttonBrowse.PressedImage = CargoMatrix.Resources.Skin.cc_trash_over;
          

                Image img = CargoMatrix.Resources.Skin.countMode;
                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string content = string.Empty;
            content = ItemData.Hwb + " " + ItemData.AWB + ItemData.References;
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
