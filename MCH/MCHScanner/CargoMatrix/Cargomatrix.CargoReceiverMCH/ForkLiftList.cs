﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;

namespace CargoMatrix.CargoReceiver
{
    public partial class ForkLiftList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;

       
        private FlightItem flightDetailsItem;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
        private int listItemHeight;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        //CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;
        //private long manifestId;
        public ForkLiftList(FlightItem flightDetailsItem)
        {
         
            this.flightDetailsItem = flightDetailsItem;
      
             
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_ListItemClicked);
        }

        protected virtual void UldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
        
            ShipmentCargoItem tempShipmentCargoItem = (ShipmentCargoItem)listItem;
            ShipmentItem tempShipmetItem = (ShipmentItem)tempShipmentCargoItem.ItemData;

            string enteredLocation = string.Empty;
             string reference = string.Empty;
            if (tempShipmetItem.Hwb != null)
            {
                reference = tempShipmetItem.Hwb;
            }

            if (reference == string.Empty)
            {
                reference = tempShipmetItem.AWB;
           
            }
            
            bool isFPC = false;
            if (DoLocation(out enteredLocation, out isFPC, reference, true))
            {
    
                long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                if (locationId == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }
                
                
                
                CargoMatrix.Communication.WebServiceManager.Instance().DropPiecesToLocationMCH(flightDetailsItem.TaskId, (int)locationId, tempShipmetItem.DetailId, tempShipmetItem.ForkliftPieces);
                if (isFPC)
                {
                    ShowFreightPhotoCapture(reference, flightDetailsItem.Origin, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), tempShipmetItem.ForkliftPieces, "CargoReceiver");
                }
                LoadControl();

                return;
            }


    
      


      }
        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        { 


            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + hawbNo + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }
    
 
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("buttonFilter_Click");
        }

        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

 

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
 
            //this.label1.Text = string.Format("{0}", filter);
 

            this.label1.Text =string.Format("{0}  {1}  {2}", this.flightDetailsItem.Origin, this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
            this.label2.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.RecoveredULDs, this.flightDetailsItem.ULDCount);
            this.label3.Text = string.Format("PCS: {0} of {1}", this.flightDetailsItem.ReceivedPieces, this.flightDetailsItem.TotalPieces);

            switch (this.flightDetailsItem.Status)
            {
                case FlightStatus.Open:
                case FlightStatus.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case FlightStatus.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case FlightStatus.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;
                default:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
            }


            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;
  

                    //RecoverStatuses status = RecoverStatuses.All;
                    // switch (filter)
                    // {
                    //     case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                    //         status = RecoverStatuses.Pending;
                    //         break;
                    //     case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
                    //         status = RecoverStatuses.InProgress;
                    //         break;
                    //     case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
                    //         status = RecoverStatuses.Complete;
                    //         break;
                    //     case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                    //         status = RecoverStatuses.NotCompleted;
                    //         break;
                    //     default:
                    //         status = RecoverStatuses.All;
                    //         break;
                    // }



                     CargoMatrix.Communication.DTO.ShipmentItem[] shipmentItemsArray = CargoMatrix.Communication.WebServiceManager.Instance().GetForkliftViewMCH(flightDetailsItem.TaskId);


                     if (shipmentItemsArray!=null)
                     {
                         var rItems = from i in shipmentItemsArray
                                      select InitializeItem(i);
                         smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                     }
 
                     this.TitleText = string.Format("Forklift {0}",  shipmentItemsArray.Length);
         
            Cursor.Current = Cursors.Default;


            if (shipmentItemsArray.Length==0)
            {
                CargoMatrix.UI.CMXAnimationmanager.GoBack();
            }

        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("Forklift {0}", smoothListBoxMainList.VisibleItemsCount);
        }

        protected ICustomRenderItem InitializeItem(ShipmentItem item)
        {
            ShipmentCargoItem tempShipment = new ShipmentCargoItem(item);
            tempShipment.OnEnterClick += new EventHandler(ULD_Enter_Click);
            tempShipment.ButtonClick += new EventHandler(ULD_OnMoreClick);
            listItemHeight = tempShipment.Height;
            return tempShipment;
        }

        void ULD_OnMoreClick(object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
             
            //Cursor.Current = Cursors.Default;
           // MessageBox.Show("OnMoreClick" + (sender as ShipmentCargoItem).ItemData.AWB);
            //MessageBox.Show("Remove shipment from forklift");

            ShipmentCargoItem obj = (sender as ShipmentCargoItem);


            string reference = string.Empty;
            if (obj.ItemData.Hwb != null)
            {
                reference = obj.ItemData.Hwb;
            }

            if (reference == string.Empty)
            {
                reference = obj.ItemData.AWB;
            }
          
            int remaningPieces = obj.ItemData.ForkliftPieces;
            int enteredPieces = 0;
            if (DoPieces(reference, remaningPieces, out enteredPieces))
            {
                if (CargoMatrix.Communication.WebServiceManager.Instance().RemoveItemsFromForkliftMCH(obj.ItemData.DetailId, enteredPieces))
                {
                    LoadControl();
                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Unable to remove pieces from ForkLift.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                }
            }



        }



        bool DoPieces(string reference, int remaningPieces, out int enteredPieces)
        {

            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.PieceCount = remaningPieces;
            CntMsg.LabelReference = reference;

            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                enteredPieces = CntMsg.PieceCount;
                return true;
            }
            else
            {
                enteredPieces = 0;
                return false;
            }

        }


  
        protected virtual void ULD_Enter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            ShipmentItem shipment = (sender as ShipmentCargoItem).ItemData;
            ProceedWithUld(shipment);
            Cursor.Current = Cursors.Default;
        }

        private bool ProceedWithUld(ShipmentItem tempShipment)
        {
            if (tempShipment == null)
                return false;

            MessageBox.Show("ProceedWithUld" + tempShipment.AWB);
            //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightReceiver(tempFlight));
            Cursor.Current = Cursors.Default;
            return true;
        }
        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        void DropForkLift(string location)
        {
            long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(location);
            if (locationId == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                return;
            }
            CargoMatrix.Communication.WebServiceManager.Instance().DropForkliftPiecesMCH(flightDetailsItem.TaskId, (int)locationId);
            CargoMatrix.UI.CMXAnimationmanager.GoBack();
        }

        void MawbList_BarcodeReadNotify(string barcodeData)
        {
            ////if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop all pieces into a location?", "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))



            //MessageBox.Show(barcodeData);

            //Cursor.Current = Cursors.Default;
            //BarcodeEnabled = true;



            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
            {
                int userid = CargoMatrix.Communication.WebServiceManager.UserID();
                int ForkLiftCount = CargoMatrix.Communication.WebServiceManager.Instance().GetForkliftCountMCH((long)userid, flightDetailsItem.TaskId);
         
                    if (ForkLiftCount == 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Your Forklift is empty.", "CargoReceiver", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                    else
                    {

                        if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop all pieces into this location?", "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            LoadControl();
                            return;
                        }

                        DropForkLift(scanItem.Location);

                        //string enteredLocation = string.Empty;
                        //string reference = string.Format("{0} {1} {2}", flightDetailsItem.Origin, flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber);
                        //bool isFPC = false;
                        //if (DoLocation(out enteredLocation, out isFPC, reference, false))
                        //{
                        //    DropForkLift(enteredLocation);
                        //    return;
                        //}

                    }
                
            }

            LoadControl();


        }
    }

}
