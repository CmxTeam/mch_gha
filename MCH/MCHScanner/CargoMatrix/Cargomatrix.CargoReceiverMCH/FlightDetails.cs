﻿using System.Drawing;
using System.Windows.Forms;
using System;
using CargoMatrix.Communication.WSCargoReceiver;
using CargoMatrix.Communication;
using CargoMatrix.UI;
using CMXExtensions;
using CargoMatrix.Communication.DTO;
namespace CargoMatrix.CargoReceiver
{
    public partial class FlightDetails : UserControl
    {
        public FlightItem flightDetailsItem;
       
        //private CargoMatrix.Viewer.HousebillViewer billViewer;
        //public string ButtonText {get {return ProceedButton.te



        public void UpdateFlightInfo()
        {
            flightDetailsItem = CargoMatrix.Communication.WebServiceManager.Instance().GetFlightMCH(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);
        }

        public void DisplayMasterbill()
        {
            this.SuspendLayout();
  
            title.Text = string.Format("{0}  {1}  {2}", this.flightDetailsItem.Origin, this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);

         
            labelWeight.Text = string.Format("{0:dd-MMM-yy HH:mm}", DateTime.Parse(this.flightDetailsItem.ETA.ToString()));
   


            
            labelPieces.Text = this.flightDetailsItem.AWBCount.ToString();
            labelUlds.Text = string.Format("{0} of {1}", this.flightDetailsItem.RecoveredULDs, this.flightDetailsItem.ULDCount);
            labelHawbs.Text = string.Format("{0} of {1}", this.flightDetailsItem.ReceivedPieces, this.flightDetailsItem.TotalPieces);

    
            if (this.flightDetailsItem.RecoveredBy == string.Empty)
            {
                labelFlightNo.Text = "";
                labelLocation.Text = "";
            }
            else
            {
                labelFlightNo.Text = string.Format("{0:dd-MMM-yy HH:mm}", DateTime.Parse(this.flightDetailsItem.RecoveredDate.ToString()));
                labelLocation.Text = this.flightDetailsItem.RecoveredBy;
            }


            labelSlac.Text = this.flightDetailsItem.CarrierName;
           



             
            switch (this.flightDetailsItem.Status)
            {
                case FlightStatus.Open:
                case FlightStatus.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case FlightStatus.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case FlightStatus.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;
                default:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
            }


            object O = Resources.Airlines.GetImage(this.flightDetailsItem.CarrierCode);
            if (O != null)
            {
                Image logo = (Image)O;
                pictureBoxCarrierLogo.Image = logo;
   
            }
            else
            {
                pictureBoxCarrierLogo.Image = Resources.Skin.Airplane24x24;
            }
             
         
            buttonDetails.Enabled = true;

            panelIndicators.Flags = this.flightDetailsItem.Flags;
            panelIndicators.PopupHeader = title.Text;
            ProceedButton.BackgroundImage = Resources.Skin.button;
            ProceedButton.ActiveBackgroundImage = Resources.Skin.button_over;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;

            this.ResumeLayout(false);
            this.Visible = true;



        }



        public FlightDetails(FlightItem flightDetailsItem)
        {
            this.flightDetailsItem = flightDetailsItem;
            InitializeComponent();
            this.SuspendLayout();
            buttonDetails.Image = CargoMatrix.Resources.Skin.magnify_btn;
            buttonDetails.PressedImage = CargoMatrix.Resources.Skin.magnify_btn_over;
            buttonDetails.DisabledImage = CargoMatrix.Resources.Skin.magnify_btn_dis;
            buttonDamage.Image = CargoMatrix.Resources.Skin.edit_btn;
            buttonDamage.PressedImage = CargoMatrix.Resources.Skin.edit_btn_over;
            buttonPrint.Image = CargoMatrix.Resources.Skin.printerButton;
            buttonPrint.PressedImage = CargoMatrix.Resources.Skin.printerButton_over;
            this.pictureBox2.Image = global::Resources.Graphics.Skin.nav_bg;
            this.ResumeLayout(false);

        }

        //void buttonDetails_Click(object sender, System.EventArgs e)
        //{
        //   CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightShipmentsList(flightDetailsItem, ref selectedUld));
        //}

        void buttonDamage_Click(object sender, System.EventArgs e)
        {

            string reference = string.Format("{0}  {1}  {2}", this.flightDetailsItem.Origin, this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
            
            DateTime eta = this.flightDetailsItem.ETA;
            if (DialogResult.OK == CustomUtilities.EtaEditMessageBox.Show(reference, ref eta))
            {
                CargoMatrix.Communication.WebServiceManager.Instance().UpdateFlightETAMCH(flightDetailsItem.TaskId, flightDetailsItem.FlightManifestId, eta);
                UpdateFlightInfo();
                DisplayMasterbill();
            }
      
        }


        void buttonPrint_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Feature Not Available");
        }
    }

 

}
