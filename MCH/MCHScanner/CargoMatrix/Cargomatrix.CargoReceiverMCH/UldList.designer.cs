﻿namespace CargoMatrix.CargoReceiver
{
    partial class UldList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.buttonFilter = new CargoMatrix.UI.CMXPictureButton();
            this.panelHeader2.SuspendLayout();

            // 
            // itemPicture
            // 
            this.itemPicture = new System.Windows.Forms.PictureBox();
            this.itemPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.itemPicture.BackColor = System.Drawing.Color.White;
            this.itemPicture.Location = new System.Drawing.Point(6, 10);//new System.Drawing.Point(202, 41);
            this.itemPicture.Name = "itemPicture";
            this.itemPicture.Size = new System.Drawing.Size(32, 32);
            this.itemPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            //
            // searchbox
            //
            this.searchBox = new CargoMatrix.UI.CMXTextBox();
            this.searchBox.Location = new System.Drawing.Point(6, 48);
            this.searchBox.Size = new System.Drawing.Size(228, 23);
            this.searchBox.TextChanged += new System.EventHandler(searchBox_TextChanged);
            //
            //label1
            //
            this.label1 = new System.Windows.Forms.Label();
            this.label1.Location = new System.Drawing.Point(40, 3);
            this.label1.Size = new System.Drawing.Size(162, 15);
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            //
            //label2
            //
            this.label2 = new System.Windows.Forms.Label();
            this.label2.Location = new System.Drawing.Point(40, 18);
            this.label2.Size = new System.Drawing.Size(162, 15);
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            //
            //label3
            //
            this.label3 = new System.Windows.Forms.Label();
            this.label3.Location = new System.Drawing.Point(40, 34);
            this.label3.Size = new System.Drawing.Size(162, 15);
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            //
            //
            //
            this.buttonFilter.Location = new System.Drawing.Point(202, 10);
            this.buttonFilter.Size = new System.Drawing.Size(32, 32);
            this.buttonFilter.Image = CargoMatrix.Resources.Skin.filter_btn;
            this.buttonFilter.PressedImage = CargoMatrix.Resources.Skin.filter_btn_over;
            this.buttonFilter.Click += new System.EventHandler(buttonFilter_Click);
            //
            //
            //
            this.panelHeader2.Controls.Add(searchBox);
            this.panelHeader2.Controls.Add(buttonFilter);
            this.panelHeader2.Controls.Add(new System.Windows.Forms.Splitter() { Dock = System.Windows.Forms.DockStyle.Bottom, Height = 1, BackColor = System.Drawing.Color.Black });
            this.panelHeader2.Controls.Add(label1);
            this.panelHeader2.Controls.Add(label2);
            this.panelHeader2.Controls.Add(label3);
            this.panelHeader2.Controls.Add(itemPicture);

            panelHeader2.Height = 73;
            panelHeader2.Visible = true;
            this.Size = new System.Drawing.Size(240, 292);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.panelHeader2.ResumeLayout(false);

        }


        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label label3;
        protected CargoMatrix.UI.CMXPictureButton buttonFilter;
        private System.Windows.Forms.PictureBox itemPicture;
        


        #endregion
    }
}
