﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;

namespace CargoMatrix.CargoReceiver
{
    public partial class FlightCargoItem : CustomListItems.ExpandableRenderListitem<FlightItem>, ISmartListItem
    {

        //public event EventHandler ButtonClick;

        public FlightCargoItem(FlightItem flight)
            : base(flight)
        {
            InitializeComponent();
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;
        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        //void buttonBrowse_Click(object sender, System.EventArgs e)
        //{
        //    if (ButtonClick != null)
        //    {
        //        ButtonClick(this, EventArgs.Empty);
        //    }
        //}
        protected override void InitializeControls()
        {
            this.SuspendLayout();
            //this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            //this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            //this.buttonBrowse.Location = new System.Drawing.Point(202, 6);
            //this.buttonBrowse.Name = "btnMore";
            //this.buttonBrowse.Image = Resources.Skin.magnify_btn;
            //this.buttonBrowse.PressedImage = Resources.Skin.magnify_btn_over;
            //this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            //this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            //this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            //this.buttonBrowse.Click += new System.EventHandler(buttonBrowse_Click);
            //this.Controls.Add(this.buttonBrowse);
            //this.buttonBrowse.Scale(new SizeF(CurrentAutoScaleDimensions.Width/96F,CurrentAutoScaleDimensions.Height/96F));
            this.ResumeLayout(false);
        }

        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Format("{0}  {1}  {2}", ItemData.Origin, ItemData.CarrierCode, ItemData.FlightNumber);
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {

                    string line2 = string.Format("ETA: {0:dd-MMM-yy HH:mm}", ItemData.ETA);
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 16 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular))
                {

                    string line3 = string.Format("ULD: {0}   AWBS: {1}     PCS: {2}", ItemData.ULDCount, ItemData.AWBCount, ItemData.TotalPieces);
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 30 * vScale, 170 * hScale, 13 * vScale));
                }

                Image img = CargoMatrix.Resources.Skin.Clipboard;
                switch (ItemData.Status)
                {
                    case FlightStatus.Open:
                    case FlightStatus.Pending:
                        img = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case FlightStatus.InProgress:
                        img = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case FlightStatus.Completed:
                        img = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;

                }

                
                object O = Resources.Airlines.GetImage(ItemData.CarrierCode);
                if (O != null)
                {
                    Image logo = (Image)O;
                    //gOffScreen.DrawImage(logo, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
                    gOffScreen.DrawImage(logo, new Rectangle((int)(202 * hScale), (int)(6 * vScale), (int)(34 * hScale), (int)(32 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
                
                }


              


                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string content = ItemData.FlightNumber + ItemData.CarrierCode + ItemData.Origin;
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
