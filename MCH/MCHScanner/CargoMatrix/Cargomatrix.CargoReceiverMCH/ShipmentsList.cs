﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;

namespace CargoMatrix.CargoReceiver
{
    public partial class ShipmentsList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;

        private UldItem uldDetailsItem;
        private FlightItem flightDetailsItem;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
        private int listItemHeight;
        protected CargoMatrix.UI.CMXTextBox searchBox;
        //CustomListItems.OptionsListITem filterOption;
        protected MessageListBox MawbOptionsList;
        //private long manifestId;
        public ShipmentsList(FlightItem flightDetailsItem, UldItem uldDetailsItem)
        {
            this.uldDetailsItem = uldDetailsItem;
            this.flightDetailsItem = flightDetailsItem;
      
             
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(MawbList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(MawbList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(UldList_ListItemClicked);
        }

        protected virtual void UldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
        
            ShipmentCargoItem tempShipmentCargoItem = (ShipmentCargoItem)listItem;
            ShipmentItem tempShipmetItem = (ShipmentItem)tempShipmentCargoItem.ItemData;
            //AddShipmentIntoForkLift(tempShipmetItem);


            string reference = string.Empty;
            if (tempShipmetItem.Hwb != null)
            {
                reference = tempShipmetItem.Hwb;
            }
            if (reference == string.Empty)
            {
                reference = tempShipmetItem.AWB;
            }


            ValidateShipment(reference);
         


      }

 
        void buttonFilter_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("buttonFilter_Click");
        }

        void UldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

 

        void MawbList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;
 
            //this.label1.Text = string.Format("{0}", filter);
 

            this.label1.Text =string.Format("{0}  {1}  {2}", this.flightDetailsItem.Origin, this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
            this.label2.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.RecoveredULDs, this.flightDetailsItem.ULDCount);
            this.label3.Text = string.Format("PCS: {0} of {1}", this.flightDetailsItem.ReceivedPieces, this.flightDetailsItem.TotalPieces);

            switch (this.flightDetailsItem.Status)
            {
                case FlightStatus.Open:
                case FlightStatus.Pending:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
                case FlightStatus.InProgress:
                    itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
                    break;
                case FlightStatus.Completed:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
                    break;
                default:
                    itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
                    break;
            }


            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;
  

                     CargoMatrix.Communication.DTO.ShipmentItem[] shipmentItemsArray = CargoMatrix.Communication.WebServiceManager.Instance().GetUldViewMCH(flightDetailsItem.TaskId,uldDetailsItem.UldId);


                     if (shipmentItemsArray!=null)
                     {
                         var rItems = from i in shipmentItemsArray
                                      select InitializeItem(i);
                         smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                     }

            string uld = string.Empty;
            if(uldDetailsItem.UldType.ToUpper()=="LOOSE")
            {
            uld =uldDetailsItem.UldType;
            }
            else
            {
            uld = uldDetailsItem.UldType + uldDetailsItem.UldSerialNo;
            }

            this.TitleText = string.Format("CargoReceiver - {0} ({1})", uld, shipmentItemsArray.Length);
         
            Cursor.Current = Cursors.Default;
        }

        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);


            string uld = string.Empty;
            if (uldDetailsItem.UldType.ToUpper() == "LOOSE")
            {
                uld = uldDetailsItem.UldType;
            }
            else
            {
                uld = uldDetailsItem.UldType + uldDetailsItem.UldSerialNo;
            }

            this.TitleText = string.Format("CargoReceiver - {0} ({1})", uld, smoothListBoxMainList.VisibleItemsCount);
      
        }

        protected ICustomRenderItem InitializeItem(ShipmentItem item)
        {
            ShipmentCargoItem tempShipment = new ShipmentCargoItem(item);
            tempShipment.OnEnterClick += new EventHandler(ULD_Enter_Click);
            tempShipment.ButtonClick += new EventHandler(ULD_OnMoreClick);
            listItemHeight = tempShipment.Height;
            return tempShipment;
        }

        void ULD_OnMoreClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
             
            Cursor.Current = Cursors.Default;
            MessageBox.Show("OnMoreClick" + (sender as ShipmentCargoItem).ItemData.AWB);
        }
 
  
        protected virtual void ULD_Enter_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            ShipmentItem shipment = (sender as ShipmentCargoItem).ItemData;
            ProceedWithUld(shipment);
            Cursor.Current = Cursors.Default;
        }


        //void ValidateShipmentOld(string shipment)
        //{

        //    CargoMatrix.Communication.MchScannerService.ValidatedShipment obj = CargoMatrix.Communication.WebServiceManager.Instance().ValidateShipmentMCH(flightDetailsItem.TaskId, uldDetailsItem.UldId, shipment);
        //    if (obj == null)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("Unable to validare shipment.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //        LoadControl();
        //        return;
        //    }

        //    if (obj.TransactionStatus.Status == false)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show(obj.TransactionStatus.Error, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //        LoadControl();
        //        return;
        //    }

        //    bool isHawb = true;
        //    string reference = string.Empty;
        //    if (obj.Hwb != null)
        //    {
        //        reference = obj.Hwb;
        //    }

        //    if (reference == string.Empty)
        //    {
        //        reference = obj.Awb;
        //        isHawb = false;
        //    }

        //    if (obj.AvailablePieces == 0)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show("There are no available pieces.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //        LoadControl();
        //        return;
        //    }


        //    foreach (CargoMatrix.Communication.MchScannerService.Alert item in obj.Alerts)
        //    {
        //        CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + reference, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        //    }

        //    int remaningPieces = obj.AvailablePieces;
        //    int enteredPieces = 0;
        //    if (DoPieces(reference, remaningPieces, out enteredPieces))
        //    {




        //        if (enteredPieces == 0 || enteredPieces > obj.AvailablePieces)
        //        {
        //            CargoMatrix.UI.CMXMessageBox.Show("Invalid number of pieces.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //            LoadControl();
        //            return;
        //        }

        //        CargoMatrix.Communication.MchScannerService.TransactionStatus t = null;

        //        t = CargoMatrix.Communication.WebServiceManager.Instance().AddForkliftPiecesMCH(obj.DetailsId, flightDetailsItem.TaskId, enteredPieces, isHawb);
 

        //        //if (isHawb)
        //        //{
        //        //      t = CargoMatrix.Communication.WebServiceManager.Instance().AddForkliftPiecesMCH(obj.DetailsId, 0, uldDetailsItem.UldId, flightDetailsItem.TaskId, enteredPieces);
        //        //}
        //        //else
        //        //{
        //        //      t = CargoMatrix.Communication.WebServiceManager.Instance().AddForkliftPiecesMCH(0, obj.DetailsId, uldDetailsItem.UldId, flightDetailsItem.TaskId, enteredPieces);
        //        //}


        //        if (t == null)
        //        {
        //           CargoMatrix.UI.CMXMessageBox.Show("Unable to add shipment to forklift.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //            LoadControl();
        //            return;
        //        }

        //        if (t.Status == false)
        //        {
        //            CargoMatrix.UI.CMXMessageBox.Show( t.Error, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        //            LoadControl();
        //            return;
        //        }


        //        CargoMatrix.UI.CMXAnimationmanager.GoBack();
        //        return;

        //    }
        //    else
        //    {
        //        LoadControl();
        //        return;
        //    }
 

        //}
        void ValidateShipment(string shipment)
        {

            CargoMatrix.Communication.WSCargoReceiverMCHService.ValidatedShipment obj = CargoMatrix.Communication.WebServiceManager.Instance().ValidateShipmentMCH(flightDetailsItem.TaskId, uldDetailsItem.UldId, shipment);
            if (obj == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show("Unable to validare shipment.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                LoadControl();
                return;
            }

            if (obj.TransactionStatus.Status == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(obj.TransactionStatus.Error, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                LoadControl();
                return;
            }

            bool isHawb = true;
            string reference = string.Empty;
            if (obj.Hwb != null)
            {
                reference = obj.Hwb;
            }

            if (reference == string.Empty)
            {
                reference = obj.Awb;
                isHawb = false;
            }

            foreach (CargoMatrix.Communication.WSCargoReceiverMCHService.Alert item in obj.Alerts)
            {
                CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + reference, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }

            int remaningPieces = obj.TotalPieces - (obj.ForkliftPieces + obj.ReceivedPieces);
            if (remaningPieces < 0)
            {
                remaningPieces = 0;
            }

            int enteredPieces = 0;
            if (DoPieces(reference, remaningPieces, out enteredPieces,true))
            {

                if (enteredPieces == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Invalid number of pieces.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    LoadControl();
                    return;
                }

                if (enteredPieces > remaningPieces)
                {
                    if (DialogResult.Cancel == CargoMatrix.UI.CMXMessageBox.Show("The number of pieces you have entered is more than the available number pieces for this shipment. Are you sure you want to report an overage?", "Confirm Recover", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        LoadControl();
                        return;
                    }
                }

                CargoMatrix.Communication.WSCargoReceiverMCHService.TransactionStatus t = null;
                t = CargoMatrix.Communication.WebServiceManager.Instance().AddForkliftPiecesMCH(obj.DetailsId, flightDetailsItem.TaskId, enteredPieces, isHawb);

                if (t == null)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Unable to add shipment to forklift.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    LoadControl();
                    return;
                }

                if (t.Status == false)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(t.Error, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    LoadControl();
                    return;
                }

               
                LoadControl();
                return;

            }
            else
            {
                LoadControl();
                return;
            }


        }


        void AddShipmentIntoForkLift(ShipmentItem tempShipmetItem)
        {

  

            bool isHawb = true;
            string reference = tempShipmetItem.Hwb;
            if (tempShipmetItem.Hwb == string.Empty)
            {
                reference = tempShipmetItem.AWB;
                isHawb = false;
            }

            if (tempShipmetItem.AvailablePieces == 0)
            {
                CargoMatrix.UI.CMXMessageBox.Show("There are no available pieces.", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                LoadControl();
                return;
            }


            int remaningPieces = tempShipmetItem.AvailablePieces;
            int enteredPieces = 0;
            if (DoPieces(reference, remaningPieces, out enteredPieces, true))
            {
                if (enteredPieces == 0)
                {
                    LoadControl();
                    return;
                }


                MessageBox.Show("Shipment scanned into Forklift");
                //LoadControl()
                CargoMatrix.UI.CMXAnimationmanager.GoBack();  
                return;

            }
    
 

        }


        bool DoPieces(string reference, int remaningPieces, out int enteredPieces, bool allowOverage)
        {

            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces";
            CntMsg.PieceCount = remaningPieces;
            CntMsg.LabelReference = reference;
            CntMsg.AllowOverage = allowOverage;
            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                enteredPieces = CntMsg.PieceCount;
                return true;
            }
            else
            {
                enteredPieces = 0;
                return false;
            }

        }


        private bool ProceedWithUld(ShipmentItem tempShipment)
        {
            //if (tempShipment == null)
            //    return false;

            //MessageBox.Show("ProceedWithUld" + tempShipment.AWB);
            ////CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightReceiver(tempFlight));
            //Cursor.Current = Cursors.Default;
            return true;
        }



        void MawbList_BarcodeReadNotify(string barcodeData)
        {

        
            ValidateShipment(barcodeData);
            Cursor.Current = Cursors.Default;
         
            if (!BarcodeEnabled)
            {
                BarcodeEnabled = true;
            }


        }
    }

}
