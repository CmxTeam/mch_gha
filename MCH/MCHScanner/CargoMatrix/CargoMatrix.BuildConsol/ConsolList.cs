﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.WSLoadConsol;
using CMXExtensions;
using CustomListItems;
using SmoothListbox;
using System.Linq;

namespace CargoMatrix.LoadConsol
{
    public partial class ConsolList : CargoUtilities.SmoothListBoxOptions
    {
        private string filter = MawbFilter.NOT_COMPLETED;
        private string sort = MawbSort.CUTOFF;
        private CargoMatrix.Viewer.HousebillViewer billViewer;
        private MawbRenderListItem selectedMawb;
        private CargoMatrix.Utilities.MessageListBox actPopup;
        private CustomListItems.OptionsListITem filterOption;

        public ConsolList()
        {
            InitializeComponent();
            this.Name = "ConsolList";
            smoothListBoxMainList.UnselectEnabled = true;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(ConsolList_ListItemClicked);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(ConsolList_BarcodeReadNotify);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ConsolList_MenuItemClicked);
            this.LoadOptionsMenu += new EventHandler(ConsolList_LoadOptionsMenu);
        }

        void ConsolList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }

        void ConsolList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.FILTER)
                {
                    FilterPopup fp = new FilterPopup(FilteringMode.Mawb);
                    fp.Filter = filter;
                    fp.Sort = sort;
                    if (DialogResult.OK == fp.ShowDialog())
                    {
                        filter = fp.Filter;
                        sort = fp.Sort;
                        if (filterOption != null)
                            filterOption.DescriptionLine = filter + " /  " + sort;
                        LoadControl();
                    }
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.REFRESH)
                {
                    LoadControl();
                }
            }
        }

        void ConsolList_BarcodeReadNotify(string barcodeData)
        {
            bool mawbFound = false;
            var scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            /// get scan Ids from backend 
            ScanItem scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(ForkLiftViewer.getScanItem(scanObj), -1, MOT.Air);

            if (scanItem != null && scanItem.Transaction.TransactionStatus1 && (scanItem.BarcodeType == BarcodeTypes.MasterBill || scanItem.BarcodeType == BarcodeTypes.HouseBill))
            {
                foreach (var item in smoothListBoxMainList.Items)
                {
                    if (item is MawbRenderListItem)
                    {
                        if ((item as MawbRenderListItem).ItemData.MasterBillId == scanItem.MasterBillId)
                        {
                            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                            //smoothListBoxMainList.MoveControlToTop(item as Control);
                            //(item as MawbRenderListItem).Focus(true);
                            MAWB_Enter_Click(item, null);
                            mawbFound = true;
                            smoothListBoxMainList.Reset();
                            break;
                        }
                    }
                }
                if (mawbFound == false)
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                    CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_MawbNotFound, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    BarcodeEnabled = true;

                }
            }
            else
            {
                CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                string message = scanItem.Transaction.TransactionStatus1 ? LoadConsolResources.Text_MawbNotFound : scanItem.Transaction.TransactionError;
                CargoMatrix.UI.CMXMessageBox.Show(message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                BarcodeEnabled = true;
            }
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            var items = CargoMatrix.Communication.LoadConsol.Instance.GetMAWBList(filter, sort, MOT.Air);
            this.TitleText = string.Format("CargoLoader ({0})", items.Length);
            smoothListBoxMainList.RemoveAll();
            
            var listItems = from i in items
                            select InitializeItem(i);

            smoothListBoxMainList.AddItemsR(listItems.ToArray<ICustomRenderItem>());

            BarcodeEnabled = false;
            BarcodeEnabled = true;
            Cursor.Current = Cursors.Default;
        }

        private void CheckMawbAlerts(int mawbId, string mawnNo, int taskId)
        {
            foreach (var item in CargoMatrix.Communication.LoadConsol.Instance.GetMasterbillAlerts(mawbId, taskId))

                CargoMatrix.UI.CMXMessageBox.Show(item.AlertMessage + Environment.NewLine + item.AlertSetBy + Environment.NewLine + item.AlertDate.ToString("MM/dd HH:mm"), "Alert: " + mawnNo, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }

        private void MAWB_Enter_Click(object sender, EventArgs e)
        {
            //check if task settings are loaded
            if (!ForkLiftViewer.Isloaded)
                return;

            Cursor.Current = Cursors.WaitCursor;
            selectedMawb = sender as MawbRenderListItem;
            if (selectedMawb.ItemData.Status == MasterbillStatus.Completed)
            {
                Cursor.Current = Cursors.Default;

                if (DialogResult.OK != CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor override"))
                {
                    BarcodeEnabled = true;
                    smoothListBoxMainList.Reset();
                    return;
                }
            }
            ForkLiftViewer.Instance.Mawb = selectedMawb.ItemData;
            smoothListBoxMainList.Reset();
            if (ForkLiftViewer.Instance.Mawb.Mot == MOT.Air && ForkLiftViewer.Instance.FinalizeReady)
            {
                Cursor.Current = Cursors.Default;
                BarcodeEnabled = false;
                bool isFinalized = SharedMethods.FinalizeConsol();
                if (isFinalized)
                {
                    LoadControl();
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    CheckMawbAlerts(selectedMawb.ItemData.MasterBillId, selectedMawb.ItemData.MasterBillNumber, selectedMawb.ItemData.TaskId);
                    ShowActionPopup();
                    actPopup.HeaderText = (sender as MawbRenderListItem).ItemData.Reference();
                    BarcodeEnabled = false;
                    actPopup.Show();
                }
               
            }
            else
            {
                Cursor.Current = Cursors.Default;
                CheckMawbAlerts(selectedMawb.ItemData.MasterBillId, selectedMawb.ItemData.MasterBillNumber, selectedMawb.ItemData.TaskId);
                ShowActionPopup();
                actPopup.HeaderText = (sender as MawbRenderListItem).ItemData.Reference();
                BarcodeEnabled = false;
                actPopup.Show();
            }
            //smoothListBoxMainList.Reset();
        }

        private void ShowActionPopup()
        {
            int sharedUsersCount = CargoMatrix.Communication.LoadConsol.Instance.GetTaskUsers(selectedMawb.ItemData.TaskId).Length - 1;
            if (actPopup == null)
            {
                actPopup = new CargoMatrix.Utilities.MessageListBox();
                actPopup.OneTouchSelection = false;
                actPopup.OkEnabled = false;
                actPopup.MultiSelectListEnabled = false;
                actPopup.HeaderText2 = LoadConsolResources.Text_ActionPopupText;
                actPopup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(actPopup_ListItemClicked);
                actPopup.CancelClick += delegate(object s, EventArgs e) { actPopup.Hide(); BarcodeEnabled = false; BarcodeEnabled = true; LoadControl(); };


                if (CargoMatrix.Communication.LoadConsol.Instance.IsPullEnabled())
                {
                    Control[] actions = new Control[]
                    {   
                        new SmoothListbox.ListItems.StandardListItem("PULL",null,1 ),
                        new SmoothListbox.ListItems.StandardListItem("LOAD", null, 2),
                        new SmoothListbox.ListItems.StandardListItem(string.Format("SHARE TASK ({0})",sharedUsersCount),CargoMatrix.Resources.Skin.shareTask, 3) {Enabled = selectedMawb.ItemData.AllowShare },
                    };
                    actPopup.AddItems(actions);
                    (actPopup.Items[2] as SmoothListbox.ListItems.StandardListItem).title.Text = string.Format("SHARE TASK ({0})", sharedUsersCount);
                    (actPopup.Items[2] as SmoothListbox.ListItems.StandardListItem).Enabled = selectedMawb.ItemData.AllowShare;

                }
                else
                {
                    Control[] actions = new Control[]
                    {   
                        new SmoothListbox.ListItems.StandardListItem("LOAD", null, 2),
                        new SmoothListbox.ListItems.StandardListItem(string.Format("SHARE TASK ({0})",sharedUsersCount),CargoMatrix.Resources.Skin.shareTask, 3) {Enabled = selectedMawb.ItemData.AllowShare },
                    };
                    actPopup.AddItems(actions);
                    (actPopup.Items[1] as SmoothListbox.ListItems.StandardListItem).title.Text = string.Format("SHARE TASK ({0})", sharedUsersCount);
                    (actPopup.Items[1] as SmoothListbox.ListItems.StandardListItem).Enabled = selectedMawb.ItemData.AllowShare;

                }

            }

        }

        void actPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            

            SmoothListbox.ListItems.StandardListItem lItem = listItem as SmoothListbox.ListItems.StandardListItem;
            
            
            switch (lItem.ID)
            {
                case 3: // share task
                    ShareTask.ShowDialog(selectedMawb.ItemData);
                    int sharedUsersCount = CargoMatrix.Communication.LoadConsol.Instance.GetTaskUsers(selectedMawb.ItemData.TaskId).Length - 1;
                    (actPopup.Items[2] as SmoothListbox.ListItems.StandardListItem).title.Text = string.Format("SHARE TASK ({0})", sharedUsersCount);
                    break;
                case 2:// Load
                    ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                    actPopup.Hide();
                    ProceedtoULDBuilder(selectedMawb);
                    break;
                case 1: // pull
                    Cursor.Current = Cursors.WaitCursor;
                    ForkLiftViewer.Instance.Mawb = selectedMawb.ItemData;
                    ForkLiftViewer.Instance.Mode = ConsolMode.Pull;
                    var doortruckForm = new DoorTruckSetUp();
                    (doortruckForm as UserControl).Location = Location;
                    (doortruckForm as UserControl).Size = Size;
                    smoothListBoxMainList.Reset();
                    actPopup.Hide();
                    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(doortruckForm as CargoMatrix.UI.CMXUserControl);
                    break;
            }
            actPopup.ResetSelection();
            actPopup.OkEnabled = false;
            smoothListBoxMainList.Reset();
        }

        private void ProceedtoULDBuilder(object sender)
        {
            Cursor.Current = Cursors.WaitCursor;
            CargoMatrix.Communication.DTO.IMasterBillItem Mawb = (sender as MawbRenderListItem).ItemData;
            ForkLiftViewer.Instance.Mawb = Mawb;

            CargoMatrix.UI.CMXUserControl uldEditor;
            
            if (Mawb.Mot == MOT.Air)
            {
                uldEditor = new ULDBuilder();
            }
            else
            {
                uldEditor = new CargoMatrix.LoadContainer.ContainerEditor();
            }

            (uldEditor as UserControl).Location = new Point(Left, Top);
            (uldEditor as UserControl).Size = new Size(Width, Height);
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(uldEditor);
            this.smoothListBoxMainList.Reset();
        }

        private void ConsolList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            //if (CargoMatrix.Communication.Utilities.IsAdmin)
            MAWB_Enter_Click(listItem, null);
            //else
            //{
            //    smoothListBoxMainList.MoveControlToTop(listItem);
            //    smoothListBoxMainList.RefreshScroll();
            //}
        }

        private void MAWB_Viewer_Click(object listItem, EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            if (billViewer != null)
                billViewer.Dispose();

            billViewer = new CargoMatrix.Viewer.HousebillViewer((listItem as MawbRenderListItem).ItemData.CarrierNumber, (listItem as MawbRenderListItem).ItemData.MasterBillNumber);//"3TB7204" masterbill);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }

        protected ICustomRenderItem InitializeItem(IMasterBillItem item)
        {
            CustomListItems.MawbRenderListItem tempMAWB = new CustomListItems.MawbRenderListItem(item);
            tempMAWB.OnViewerClick += new EventHandler(MAWB_Viewer_Click);
            tempMAWB.OnEnterClick += new EventHandler(MAWB_Enter_Click);
            return tempMAWB;
        }
    }
}
