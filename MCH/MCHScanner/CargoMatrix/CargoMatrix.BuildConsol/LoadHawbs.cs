﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CustomListItems;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.LoadConsol
{
    public partial class LoadHawbs : SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IHouseBillItem>
    {
        private CustomListItems.HeaderItem headerItem;
        private CargoMatrix.Communication.DTO.IMasterBillItem mawb;
        private Viewer.HousebillViewer billViewer;
        private string filter = HawbFilter.NOT_SCANNED;
        private string sort = HawbSort.WEIGHT;
        private CargoMatrix.Utilities.MessageListBox reasonsBox;
        private bool lastScannedIsLocation = true;
        int selectedhwbId;

        private List<int> checkedHawbs = new List<int>(); // list of hawbs that alreadey check for Alerts

        public LoadHawbs(CargoMatrix.Communication.DTO.IMasterBillItem mawb)
        {
            InitializeComponent();
            headerItem = new CustomListItems.HeaderItem();
            this.mawb = mawb;
            panelHeader2.Height = headerItem.Height;
            headerItem.Location = panelHeader2.Location;
            panelHeader2.Visible = false;
            headerItem.Label1 = mawb.Reference();
            headerItem.Label2 = mawb.Line2();
            headerItem.Label3 = string.Format(LoadConsolResources.Text_HawbListBlink, ForkLiftViewer.Instance.Mode.ToString().ToUpper());
            headerItem.Blink = true;
            //headerItem.Logo = CargoMatrix.Resources.Skin.load_consol;
            headerItem.ButtonImage = CargoMatrix.Resources.Skin.Forklift_btn; //CargoMatrix.Resources.Skin.buffer;
            headerItem.ButtonPressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over; //CargoMatrix.Resources.Skin.buffer_over;
            this.headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            this.headerItem.ItemClick += new EventHandler(headerItem_Click);
            this.Controls.Add(headerItem);
            this.smoothListBoxMainList.UnselectEnabled = true;
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(LoadHawbs_ListItemClicked);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(LoadHawbs_MenuItemClicked);

            if (CargoMatrix.Communication.GTWSettings.Instance.ScanHawbToULD)
                this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(LoadHawbs_BarcodeReadNotify2);
            else
                this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(LoadHawbs_BarcodeReadNotify);
            //PopulateStubHawbs();

            /// download parse rules.
            CargoMatrix.Communication.BarcodeParser.Instance.Parse(string.Empty);
            headerItem.Logo = ForkLiftViewer.Instance.CarrierLogo;
            ForkLiftViewer.Instance.ItemCountChanged += new EventHandler(Instance_ItemCountChanged);
        }

        void Instance_ItemCountChanged(object sender, EventArgs e)
        {
            headerItem.Counter = ForkLiftViewer.Instance.ItemsCount;
        }

        void LoadHawbs_BarcodeReadNotify(string barcodeData)
        {
            bool refreshFlag = true;
            int forkliftPieces = ForkLiftViewer.Instance.ItemsCount;

            DateTime dt = DateTime.Now;
            ScanItem scanItem = CargoMatrix.Communication.LoadConsol.Instance.ParseBarcode(barcodeData, mawb.TaskId);
            TimeSpan sc = DateTime.Now - dt;

            #region PULL MODE
            if (ForkLiftViewer.Instance.Mode == ConsolMode.Pull)
            {
                switch (scanItem.BarcodeType)
                {
                    /// Drop to Location
                    case BarcodeTypes.Door:
                    case BarcodeTypes.Area:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DropAllPiecesIntoLocation(mawb.TaskId, scanItem.LocationId);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullDrop, forkliftPieces, scanItem.Location), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            ForkLiftViewer.Instance.ItemsCount = status.TransactionRecords;
                        }
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        break;

                    /// Change Mode to Load
                    case BarcodeTypes.Truck: //seperate doesn't have loose
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        // 1. have loose good load to truck
                        // 2. no loose "loose is not set up. Please set up loose" uld stup
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchMode, scanItem.BarcodeType.ToString()), LoadConsolResources.Text_SwitchModeTitle, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                            /// Loose is there
                            var looseUld = CargoMatrix.Communication.LoadConsol.Instance.MawbHasLooseULD(mawb.MasterBillId);
                            if (looseUld != null)
                            {
                                TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoLocation(mawb.TaskId, looseUld.UldId, scanItem.LocationId);
                                if (dropStatus.TransactionStatus1)
                                {
                                    CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.UldNumber), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                    ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                                }
                                else
                                    CargoMatrix.UI.CMXMessageBox.Show(dropStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            }
                            else // loose is not there set up loose
                            {
                                CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_LooseSetUp, "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                                Cursor.Current = Cursors.WaitCursor;
                                ULDBuilder uldbuilder = new ULDBuilder(mawb);
                                uldbuilder.Location = new Point(Left, Top);
                                uldbuilder.Size = new Size(Width, Height);
                                this.smoothListBoxMainList.Reset();
                                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(uldbuilder as CargoMatrix.UI.CMXUserControl);
                                return;
                            }
                        }
                        break;
                    case BarcodeTypes.Uld:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchMode, scanItem.BarcodeType.ToString()), LoadConsolResources.Text_SwitchModeTitle, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                            /// show ULD BUilder
                            if (scanItem.UldId <= 0)
                            {
                                CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_ULDNotAttached, "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                                Cursor.Current = Cursors.WaitCursor;
                                ULDBuilder uldbuilder = new ULDBuilder(mawb);
                                uldbuilder.Location = new Point(Left, Top);
                                uldbuilder.Size = new Size(Width, Height);
                                this.smoothListBoxMainList.Reset();
                                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(uldbuilder as CargoMatrix.UI.CMXUserControl);
                                return;

                            }
                            else
                            {
                                TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoULD(mawb.TaskId, scanItem.UldId);
                                if (dropStatus.TransactionStatus1)
                                {
                                    CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.UldNumber), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                    ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                                }
                                else
                                    CargoMatrix.UI.CMXMessageBox.Show(dropStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            }
                        }
                        break;

                    case BarcodeTypes.User:
                    case BarcodeTypes.NA:
                    case BarcodeTypes.MasterBill:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        refreshFlag = false;
                        break;
                    case BarcodeTypes.HouseBill:
                    default:
                        break;

                }

            }
            #endregion

            #region LOAD MODE
            else
            {
                DateTime load = DateTime.Now;
                switch (scanItem.BarcodeType)
                {
                    case BarcodeTypes.Uld:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        TransactionStatus loadStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoULD(mawb.TaskId, scanItem.UldId);
                        if (loadStatus.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.UldNumber), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            ForkLiftViewer.Instance.ItemsCount = loadStatus.TransactionRecords;
                            if (ForkLiftViewer.Instance.FinalizeReady)
                                ForkLiftViewer.Instance.FinalizeConsol();
                        }
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(loadStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

                        break;

                    case BarcodeTypes.Truck: // loose needs to be there
                    case BarcodeTypes.Door:
                    case BarcodeTypes.Area:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        /// Loose is there
                        var looseUld = CargoMatrix.Communication.LoadConsol.Instance.MawbHasLooseULD(mawb.MasterBillId);
                        if (null != looseUld)
                        {
                            TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoLocation(mawb.TaskId, looseUld.UldId, scanItem.LocationId);
                            if (dropStatus.TransactionStatus1)
                            {
                                CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.Location), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                            }
                            else
                                CargoMatrix.UI.CMXMessageBox.Show(dropStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        }
                        else // loose is not there, set up loose
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_LooseSetUp, "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                            Cursor.Current = Cursors.WaitCursor;
                            ULDBuilder uldbuilder = new ULDBuilder(mawb);
                            uldbuilder.Location = new Point(Left, Top);
                            uldbuilder.Size = new Size(Width, Height);
                            this.smoothListBoxMainList.Reset();
                            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(uldbuilder as CargoMatrix.UI.CMXUserControl);
                            return;
                        }

                        break;

                    case BarcodeTypes.User:
                    case BarcodeTypes.NA:
                    case BarcodeTypes.MasterBill:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        refreshFlag = false;
                        break;
                    case BarcodeTypes.HouseBill:
                    default:
                        break;
                }
                TimeSpan loadTs = DateTime.Now - load;
            }
            #endregion

            /// HAWB Scanned
            if (scanItem.BarcodeType == BarcodeTypes.HouseBill)
            {
                /// HAWB from consol
                if (scanItem.MasterBillId == mawb.MasterBillId)
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    //BringHawbIntoView(scanItem.HouseBillId);
                    addHawbToForkLift(scanItem);
                }


                /// HAWB from other Consol
                else
                {
                    /// HAWB not attached to any Consol
                    if (scanItem.MasterBillId == 0 && scanItem.MasterBillNumber == string.Empty)
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        string confMsg = string.Format(LoadConsolResources.Text_ConfirmAttach, scanItem.HouseBillNumber);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(confMsg, "Please Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            CargoMatrix.Communication.LoadConsol.Instance.AttachHousebillToMasterbill(scanItem.HouseBillId, mawb.TaskId);
                            addHawbToForkLift(scanItem);
                        }

                    }
                    else
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_ShipmentNotInConsol, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.OK, DialogResult.OK);
                        refreshFlag = false;
                    }
                }
            }
            TimeSpan ts = DateTime.Now - dt;
            if (refreshFlag)
                LoadControl();
            else
                BarcodeEnabled = true;
        }

        void LoadHawbs_BarcodeReadNotify2(string barcodeData)
        {
            bool refreshFlag = true;
            int forkliftPieces = ForkLiftViewer.Instance.ItemsCount;
            if (forkliftPieces == 0)
                lastScannedIsLocation = true;

            ScanItem scanItem = CargoMatrix.Communication.LoadConsol.Instance.ParseBarcode(barcodeData, mawb.TaskId);

            if (lastScannedIsLocation && scanItem.BarcodeType != BarcodeTypes.HouseBill) // expecting hawb to be scanned
            {
                CargoMatrix.UI.CMXMessageBox.Show("Please scan a hawb first", "Hawb Expected", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                BarcodeEnabled = true;
                return;
            }
            if (!lastScannedIsLocation && (scanItem.BarcodeType != BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.Truck || scanItem.BarcodeType == BarcodeTypes.Uld)) // expecting hawb to be scanned
            {
                CargoMatrix.UI.CMXMessageBox.Show("Please scan drop location", "Location Expected", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                BarcodeEnabled = true;
                return;
            }

            #region PULL MODE
            if (ForkLiftViewer.Instance.Mode == ConsolMode.Pull)
            {
                switch (scanItem.BarcodeType)
                {
                    /// Drop to Location
                    case BarcodeTypes.Door:
                    case BarcodeTypes.Area:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DropAllPiecesIntoLocation(mawb.TaskId, scanItem.LocationId);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullDrop, forkliftPieces, scanItem.Location), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            ForkLiftViewer.Instance.ItemsCount = status.TransactionRecords;
                        }
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        lastScannedIsLocation = true;
                        break;

                    /// Change Mode to Load
                    case BarcodeTypes.Truck: //seperate doesn't have loose
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        // 1. have loose good load to truck
                        // 2. no loose "loose is not set up. Please set up loose" uld stup
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchMode, scanItem.BarcodeType.ToString()), LoadConsolResources.Text_SwitchModeTitle, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                            /// Loose is there
                            var looseUld = CargoMatrix.Communication.LoadConsol.Instance.MawbHasLooseULD(mawb.MasterBillId);
                            if (looseUld != null)
                            {
                                TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoLocation(mawb.TaskId, looseUld.UldId, scanItem.LocationId);
                                if (dropStatus.TransactionStatus1)
                                {
                                    CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.UldNumber), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                    ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                                }
                                else
                                    CargoMatrix.UI.CMXMessageBox.Show(dropStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            }
                            else // loose is not there set up loose
                            {
                                CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_LooseSetUp, "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                                Cursor.Current = Cursors.WaitCursor;
                                ULDBuilder uldbuilder = new ULDBuilder(mawb);
                                uldbuilder.Location = new Point(Left, Top);
                                uldbuilder.Size = new Size(Width, Height);
                                this.smoothListBoxMainList.Reset();
                                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(uldbuilder as CargoMatrix.UI.CMXUserControl);
                                return;
                            }
                        }
                        lastScannedIsLocation = true;
                        break;
                    case BarcodeTypes.Uld:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchMode, scanItem.BarcodeType.ToString()), LoadConsolResources.Text_SwitchModeTitle, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                            /// show ULD BUilder
                            if (scanItem.UldId <= 0)
                            {
                                CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_ULDNotAttached, "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                                Cursor.Current = Cursors.WaitCursor;
                                ULDBuilder uldbuilder = new ULDBuilder(mawb);
                                uldbuilder.Location = new Point(Left, Top);
                                uldbuilder.Size = new Size(Width, Height);
                                this.smoothListBoxMainList.Reset();
                                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(uldbuilder as CargoMatrix.UI.CMXUserControl);
                                return;

                            }
                            else
                            {
                                TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoULD(mawb.TaskId, scanItem.UldId);
                                if (dropStatus.TransactionStatus1)
                                {
                                    CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.UldNumber), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                    ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                                }
                                else
                                    CargoMatrix.UI.CMXMessageBox.Show(dropStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            }
                        }
                        lastScannedIsLocation = true;
                        break;
                    case BarcodeTypes.User:
                    case BarcodeTypes.NA:
                    case BarcodeTypes.MasterBill:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        refreshFlag = false;
                        break;
                    case BarcodeTypes.HouseBill:
                    default:
                        break;

                }

            }
            #endregion

            #region LOAD MODE
            else
            {
                switch (scanItem.BarcodeType)
                {
                    case BarcodeTypes.Uld:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        TransactionStatus loadStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoULD(mawb.TaskId, scanItem.UldId);
                        if (loadStatus.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.UldNumber), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            ForkLiftViewer.Instance.ItemsCount = loadStatus.TransactionRecords;
                            if (ForkLiftViewer.Instance.FinalizeReady)
                                ForkLiftViewer.Instance.FinalizeConsol();
                        }
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(loadStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        lastScannedIsLocation = true;
                        break;

                    case BarcodeTypes.Truck: // loose needs to be there
                    case BarcodeTypes.Door:
                    case BarcodeTypes.Area:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        /// Loose is there
                        var looseUld = CargoMatrix.Communication.LoadConsol.Instance.MawbHasLooseULD(mawb.MasterBillId);
                        if (null != looseUld)
                        {
                            TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoLocation(mawb.TaskId, looseUld.UldId, scanItem.LocationId);
                            if (dropStatus.TransactionStatus1)
                            {
                                CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.UldNumber), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                            }
                            else
                                CargoMatrix.UI.CMXMessageBox.Show(dropStatus.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        }
                        else // loose is not there, set up loose
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_LooseSetUp, "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                            Cursor.Current = Cursors.WaitCursor;
                            ULDBuilder uldbuilder = new ULDBuilder(mawb);
                            uldbuilder.Location = new Point(Left, Top);
                            uldbuilder.Size = new Size(Width, Height);
                            this.smoothListBoxMainList.Reset();
                            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(uldbuilder as CargoMatrix.UI.CMXUserControl);
                            return;
                        }
                        lastScannedIsLocation = true;
                        break;

                    case BarcodeTypes.User:
                    case BarcodeTypes.NA:
                    case BarcodeTypes.MasterBill:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        refreshFlag = false;
                        break;
                    case BarcodeTypes.HouseBill:
                    default:
                        break;
                }
            }
            #endregion

            /// HAWB Scanned
            if (scanItem.BarcodeType == BarcodeTypes.HouseBill)
            {
                /// HAWB from consol
                if (scanItem.MasterBillId == mawb.MasterBillId)
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    //BringHawbIntoView(scanItem.HouseBillId);
                    addHawbToForkLift(scanItem);
                }
                /// HAWB from other Consol
                else
                {
                    /// HAWB not attached to any Consol
                    if (scanItem.MasterBillId == 0 && scanItem.MasterBillNumber == string.Empty)
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        string confMsg = string.Format(LoadConsolResources.Text_ConfirmAttach, scanItem.HouseBillNumber);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(confMsg, "Please Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            CargoMatrix.Communication.LoadConsol.Instance.AttachHousebillToMasterbill(scanItem.HouseBillId, mawb.TaskId);
                            addHawbToForkLift(scanItem);
                        }
                        else refreshFlag = false;

                    }
                    else
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_ShipmentNotInConsol, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.OK, DialogResult.OK);
                        refreshFlag = false;
                    }
                }
                lastScannedIsLocation = false;
            }

            if (refreshFlag)
                LoadControl();
            else

                BarcodeEnabled = true;
        }

        private void addHawbToForkLift(ScanItem scanItem)
        {

            CheckHawbAlerts(scanItem.HouseBillId, scanItem.HouseBillNumber);

            switch (scanItem.ScanMode)
            {
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Count:
                    AddToForkliftInCountMode(scanItem);
                    break;
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Slac:
                    AddToForkliftInSlacMode(scanItem);
                    break;
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece:
                default:
                    AddToForkliftInPieceMode(scanItem);
                    break;
            }
            //}
            //else
            //{
            //    ShowFailMessage(status.TransactionError);
            //}
        }

        /*
        private void BringHawbIntoView(int hawbid)
        {
            foreach (var item in smoothListBoxMainList.Items)
            {

                if (item is CustomListItems.ConsolHAWBListItem && (item as CustomListItems.ConsolHAWBListItem).HawbItem.HousebillId == hawbid)
                {
                    smoothListBoxMainList.MoveControlToTop(item as Control);
                    smoothListBoxMainList.Reset();
                    (item as CustomListItems.ConsolHAWBListItem).Focus(true);
                    smoothListBoxMainList.RefreshScroll();
                    break;
                }
            }
        }
        private bool isHawbInTheList(int hawbId)
        {
            /// search within the list the housebill with the given ID
            return null != smoothListBoxMainList.Items.Cast<CustomListItems.ConsolHAWBListItem>().First<CustomListItems.ConsolHAWBListItem>(h => h.HawbItem.HousebillId == hawbId);
        }

        private void LastPieceChecked()
        {
            if (smoothListBoxMainList.ItemsCount > 0)
            {
                smoothListBoxMainList.RemoveItem(smoothListBoxMainList.Items[0]);
                this.headerItem.Counter++;
                if (smoothListBoxMainList.ItemsCount == 0)
                    ForkLiftViewer.Instance.Finalize = true;

            }
        }
        */

        private void AddToForkliftInSlacMode(ScanItem hawb)
        {
            List<Control> actions = new List<Control>
                {   
                    new SmoothListbox.ListItems.StandardListItem("PIECE ID",null,1),
                    new SmoothListbox.ListItems.StandardListItem("COUNT",null,2),
                };
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.AddItems(actions);
            actPopup.HeaderText2 = string.Format("Total Pieces: {0} SLAC: {1}", hawb.TotalPieces, "N/A");
            actPopup.HeaderText = hawb.MasterBillNumber;
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1:
                        AddToForkliftInPieceMode(hawb);
                        break;
                    case 2:
                        AddToForkliftInCountMode(hawb);
                        break;
                    default:
                        break;
                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();
        }

        private void AddToForkliftInPieceMode(ScanItem hawb)
        {
            TransactionStatus status = new TransactionStatus() { TransactionStatus1 = true };
            if (hawb.RemoveMode == CargoMatrix.Communication.WSLoadConsol.RemoveModes.NA)
            {
                status = CargoMatrix.Communication.LoadConsol.Instance.ScanHousebillIntoForkliftPiece(hawb.HouseBillId, hawb.PieceId, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, ScanTypes.Automatic);
            }
            else HandleRemovedHousebill(hawb, ScanTypes.Automatic);

            if (!status.TransactionStatus1)
                ShowFailMessage(status.TransactionError);
            else
                ForkLiftViewer.Instance.ItemsCount = status.TransactionRecords;

        }



        private void AddToForkliftInCountMode(ScanItem hawb)
        {
            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.PieceCount = hawb.TotalPieces;
            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces to load";
            CntMsg.LabelReference = hawb.HouseBillNumber;

            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                TransactionStatus status = new TransactionStatus() { TransactionStatus1 = false };

                if (hawb.RemoveMode == CargoMatrix.Communication.WSLoadConsol.RemoveModes.NA)
                {
                    status = CargoMatrix.Communication.LoadConsol.Instance.ScanHousebillIntoForkliftCount(hawb.HouseBillId, CntMsg.PieceCount, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, ScanTypes.Automatic);
                    if (!status.TransactionStatus1)
                        ShowFailMessage(status.TransactionError);
                    else
                        ForkLiftViewer.Instance.ItemsCount = status.TransactionRecords;
                }
                else
                {
                    HandleRemovedHousebill(hawb, ScanTypes.Automatic);

                }
            }
        }

        private void ShowFailMessage(string message)
        {
            CargoMatrix.UI.CMXMessageBox.Show(message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }
        private bool firstLoad = true;
        //public override void LoadControl()
        //{
        //    Cursor.Current = Cursors.WaitCursor;
        //    this.headerItem.Counter = ForkLiftViewer.Instance.ItemsCount;
        //    BarcodeEnabled = false;
        //    BarcodeEnabled = true;
        //    headerItem.Label3 = string.Format(LoadConsolResources.Text_HawbListBlink, ForkLiftViewer.Instance.Mode.ToString().ToUpper());


        //    HousebillsSummary sumary = CargoMatrix.Communication.LoadConsol.Instance.GetHawbList(mawb.MasterBillId, mawb.TaskId, filter, sort, ForkLiftViewer.Instance.Mode == ConsolMode.Load);
        //    //headerItem.LabelProgress = sumary.progress + "%";

        //    if (firstLoad)
        //    {
        //        this.ReloadItemsAsync(sumary.Housebills);
        //        firstLoad = false;
        //    }
        //    else
        //        DisplayChanges(sumary.Housebills);
        //    this.TitleText = string.Format("CargoLoader - {1} ({0}) - {2}%", sumary.Housebills.Length, ForkLiftViewer.Instance.Mode.ToString(), sumary.progress);

        //    Cursor.Current = Cursors.Default;

        //}

        private void DisplayChanges(IHouseBillItem[] hawbs)
        {
            DateTime start = DateTime.Now;
            List<IHouseBillItem> serverItems = hawbs.ToList<IHouseBillItem>();
            List<ConsolHAWBListItem> uiItems = smoothListBoxMainList.Items.OfType<ConsolHAWBListItem>().ToList<ConsolHAWBListItem>();
            bool layoutChanged = false;
            for (int i = 0; i < hawbs.Length; i++)
            {
                bool exisits = false;
                foreach (var uiItem in uiItems)
                {
                    if (uiItem.HawbItem.HousebillId == hawbs[i].HousebillId)
                    {
                        uiItem.HawbItem = hawbs[i];// update UI Item
                        serverItems.Remove(hawbs[i]); // remove from serverlist
                        uiItems.Remove(uiItem); // remove from uilist
                        exisits = true;
                        break;
                    }
                }
                if (false == exisits)
                {
                    smoothListBoxMainList.AddItemAt(InitializeItem(hawbs[i]), i);
                    layoutChanged = true;
                }
            }

            foreach (var remItem in uiItems)
            {
                smoothListBoxMainList.RemoveItem(remItem);
                layoutChanged = true;
            }
            if (layoutChanged)
            {
                LayoutItems();
                smoothListBoxMainList.RefreshScroll();
            }



            TimeSpan ts = DateTime.Now - start;
            ts.ToString();
        }

        void LoadHawbs_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                HawbItem_OnEnterClick(listItem, null);
            else
            {
                smoothListBoxMainList.MoveControlToTop(listItem);
                smoothListBoxMainList.RefreshScroll();
            }
        }

        void headerItem_Click(object sender, EventArgs e)
        {
            // CargoMatrix.UI.CMXAnimationmanager.GoBack();
        }

        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            BarcodeEnabled = false;
            ForkLiftViewer.Instance.ShowDialog();
            LoadControl();
        }

        void hawbItem_ButtonConditionClick(object sender, EventArgs e)
        {
            IHouseBillItem houseBillItem = (sender as CustomListItems.ConsolHAWBListItem).HawbItem;

            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(houseBillItem);

            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionSummary(args.HouseBillID);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.LoadConsol.Instance.UpdateShippmentCondition(args.HouseBillId, args.ScanModes, args.ShipmentConditions, mawb.TaskId);
            };

            damageCapture.GetHouseBillPiecesIDs = () =>
            {
                return
                    (
                        from item in CargoMatrix.Communication.LoadConsol.Instance.GetHousebillInfo(houseBillItem.HousebillId, mawb.TaskId).Pieces
                        select item.PieceId
                    ).ToArray();
            };

            damageCapture.Size = this.Size;
            BarcodeEnabled = false;
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(damageCapture);

        }
        void hawbItem_ButtonBrowseClick(object sender, EventArgs e)
        {
            List<Control> actions = new List<Control>
            {
                new SmoothListbox.ListItems.StandardListItem("PRINT LABELS",null,4),
                new SmoothListbox.ListItems.StandardListItem("SWITCH MODE",null,3),
                new SmoothListbox.ListItems.StandardListItem("TAKEOFF",null,1),
                new SmoothListbox.ListItems.StandardListItem("SHORT",null,2) };

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.OneTouchSelection = true;
            actPopup.MultiSelectListEnabled = false;
            actPopup.HeaderText = (sender as CustomListItems.ConsolHAWBListItem).HawbItem.Reference();
            actPopup.HeaderText2 = string.Format("Select action for {0}", (sender as CustomListItems.ConsolHAWBListItem).HawbItem.HousebillNumber);
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1:
                        RemoveHawb((sender as CustomListItems.ConsolHAWBListItem).HawbItem, CargoMatrix.Communication.DTO.RemoveModes.TakeOff);
                        break;
                    case 2:
                        RemoveHawb((sender as CustomListItems.ConsolHAWBListItem).HawbItem, CargoMatrix.Communication.DTO.RemoveModes.Shortage);
                        break;
                    case 3:
                        SwitchMode((sender as CustomListItems.ConsolHAWBListItem).HawbItem);
                        LoadControl();
                        break;
                    case 4:
                        PrintLabels((sender as CustomListItems.ConsolHAWBListItem).HawbItem);
                        break;
                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();
        }
        void hawbItem_OnViewerClick(object sender, EventArgs e)
        {
            DisplayBillViewer((sender as CustomListItems.ConsolHAWBListItem).HawbItem.HousebillNumber);
        }
        void HawbItem_OnEnterClick(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxMainList.Reset();
            CheckHawbAlerts((sender as CustomListItems.ConsolHAWBListItem).HawbItem.HousebillId, (sender as CustomListItems.ConsolHAWBListItem).HawbItem.HousebillNumber);
            BarcodeEnabled = false;
            HawbViewer hawbViewer = new HawbViewer((sender as CustomListItems.ConsolHAWBListItem).HawbItem, mawb.TaskId);//
            //hawbViewer.ShowDialog();
            LoadControl();
        }
        CustomListItems.OptionsListITem filterOption;
        void LoadHawbs_LoadOptionsMenu(object sender, System.EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.SHARE_TASK) { Enabled = mawb.AllowShare });
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL) { Enabled = ForkLiftViewer.Instance.Mode == ConsolMode.Load });
        }


        private void DisplayBillViewer(string hawbNo)
        {
            if (billViewer != null)
                billViewer.Dispose();

            billViewer = new CargoMatrix.Viewer.HousebillViewer(hawbNo);//"3TB7204" masterbill);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            BarcodeEnabled = false;
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }
        private void CheckHawbAlerts(int hawbId, string hawbNo)
        {
            if (!checkedHawbs.Contains(hawbId))
            {
                foreach (var item in CargoMatrix.Communication.LoadConsol.Instance.GetHousebillAlerts(hawbId, mawb.TaskId))
                {
                    CargoMatrix.UI.CMXMessageBox.Show(item.AlertMessage + Environment.NewLine + item.AlertSetBy + Environment.NewLine + item.AlertDate.ToString("MM/dd HH:mm"), "Alert: " + hawbNo, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                }
                checkedHawbs.Add(hawbId);
            }
        }

        void LoadHawbs_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.FILTER)
                {
                    FilterPopup fp = new FilterPopup(FilteringMode.Hawb);
                    fp.Filter = filter;
                    fp.Sort = sort;
                    if (DialogResult.OK == fp.ShowDialog())
                    {
                        sort = fp.Sort;
                        filter = fp.Filter;
                        if (filterOption != null)
                            filterOption.DescriptionLine = filter + " / " + sort;
                        firstLoad = true;
                        LoadControl();
                    }
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.REFRESH)
                {
                    LoadControl();
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.SHARE_TASK)
                {
                    ShareTask.ShowDialog(mawb);
                }
                else if ((listItem as CustomListItems.OptionsListITem).ID == CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL)
                {
                    BarcodeEnabled = false;
                    if (ForkLiftViewer.Instance.FinalizeConsol())
                    {
                        CargoMatrix.UI.CMXAnimationmanager.DisplayDefaultForm();
                    }
                }
            }
        }

        internal void PrintLabels(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            var hawbInfo = CargoMatrix.Communication.LoadConsol.Instance.GetHousebillInfo(hawb.HousebillId, mawb.TaskId);

            int[] ids = (from p in hawbInfo.Pieces
                         select p.PieceId).ToArray<int>();

            CustomUtilities.HawbLabelPrinter.Show(hawb.HousebillId, hawb.Reference(), ids);
        }



        #region RemovedPieces
        internal static void HandleRemovedHousebill(ScanItem hawb, ScanTypes scanType)
        {
            //BarcodeEnabled = false;
            var mawb = ForkLiftViewer.Instance.Mawb;
            TransactionStatus status = new TransactionStatus() { TransactionStatus1 = true };
            string msg;
            CargoMatrix.Utilities.MessageListBox actPopup;
            List<Control> actions = new List<Control>
                {   
                    new SmoothListbox.ListItems.StandardListItem("RETURN",null, 2),
                    new SmoothListbox.ListItems.StandardListItem("REMOVE",null, 1) };

            actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = "Select Action To Continue";
            actPopup.HeaderText2 = "Hawb was removed";
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 2: // RETURN
                        msg = string.Format("This will return the HB {0} into consol {1}. Do you want to continue?", hawb.HouseBillNumber, mawb.MasterBillNumber);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Return Hawb", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            if (hawb.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece)
                                status = CargoMatrix.Communication.LoadConsol.Instance.ScanRemovedPieceIntoForklift(hawb.HouseBillId, hawb.PieceId, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece, scanType);
                            else
                                status = CargoMatrix.Communication.LoadConsol.Instance.ScanRemovedPieceIntoForklift(hawb.HouseBillId, hawb.TotalPieces, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, CargoMatrix.Communication.WSLoadConsol.ScanModes.Count, scanType);

                            if (!status.TransactionStatus1)
                                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        }

                        break; // REMOVE
                    case 1:
                        msg = string.Format("Are you sure you want to remove  HB {0} from consol {1}?", hawb.HouseBillNumber, mawb.MasterBillNumber);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Remove Hawb", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            CustomUtilities.ScanEnterPopup dropLoc = new CustomUtilities.ScanEnterPopup();
                            dropLoc.Title = "Scan Drop Location";
                            dropLoc.TextLabel = "Return Location for HB " + hawb.HouseBillNumber;
                            if (DialogResult.OK == dropLoc.ShowDialog())
                            {
                                ScanItem scitem = CargoMatrix.Communication.LoadConsol.Instance.ParseBarcode(dropLoc.ScannedText, mawb.TaskId);
                                if (BarcodeTypes.Area == scitem.BarcodeType || BarcodeTypes.Door == scitem.BarcodeType)
                                {
                                    //status = CargoMatrix.Communication.LoadConsol.Instance.ScanRemovedPieceIntoForklift(hawb.HouseBillId, hawb.TotalPieces, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, CargoMatrix.Communication.WSLoadConsol.ScanModes.Count, scanType);
                                    //if (!status.TransactionStatus1)
                                    //    CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                                    //else
                                    {
                                        status = CargoMatrix.Communication.LoadConsol.Instance.ReturnPieceIntoLocation(hawb.HouseBillId, mawb.TaskId, hawb.TotalPieces, scitem.LocationId, CargoMatrix.Communication.WSLoadConsol.ScanModes.Count, scanType);
                                        if (!status.TransactionStatus1)
                                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                                    }
                                }
                                else
                                    CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_InvalidLocationBarcode, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                            }
                        }
                        break;
                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();
            if (!status.TransactionStatus1)
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
        }
        #endregion

        #region SHORT & TAKEOFF

        private void RemoveHawb(CargoMatrix.Communication.DTO.IHouseBillItem hawb, CargoMatrix.Communication.DTO.RemoveModes removeMode)
        {
            CargoMatrix.Communication.DTO.RemoveModes hawbMode = CargoMatrix.Communication.LoadConsol.Instance.GetHouseBillAllowedRemoveModes(hawb.HousebillId, mawb.MasterBillId);
            /// remove allowed
            if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin || hawbMode == CargoMatrix.Communication.DTO.RemoveModes.All || hawbMode == removeMode)
            {
                removeHelper(hawb, removeMode, 0);
            }
            else             /// supervisor Override if user is not admin of remove is not allowed
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_RemoveOverride, hawb.HousebillNumber), "Supervisor Override", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                    if (DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                    {
                        int superId = CargoMatrix.Communication.LoadConsol.Instance.GetSupervisorIDByPin(int.Parse(CustomUtilities.SupervisorAuthorizationMessageBox.SupervisorPIN));
                        int pin = int.Parse(CustomUtilities.SupervisorAuthorizationMessageBox.SupervisorPIN);
                        removeHelper(hawb, removeMode, pin);
                    }
                }
            }
        }

        private void removeHelper(CargoMatrix.Communication.DTO.IHouseBillItem hawb, CargoMatrix.Communication.DTO.RemoveModes removeMode, int pin)
        {
            if (PopulateReasonsList())
            {
                int reasonId = (reasonsBox.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID;
                reasonsBox.ResetSelection();
                bool shortage = removeMode == CargoMatrix.Communication.DTO.RemoveModes.Shortage;
                if (removeMode == CargoMatrix.Communication.DTO.RemoveModes.Shortage)
                {
                    if (hawb.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
                    {
                        ShortPieces(hawb);
                    }
                    else
                    {
                        CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
                        CntMsg.PieceCount = hawb.TotalPieces;
                        CntMsg.HeaderText = "Confirm count";
                        CntMsg.LabelDescription = "Enter number of short pieces";
                        CntMsg.LabelReference = hawb.Reference();

                        if (DialogResult.OK == CntMsg.ShowDialog())
                        {

                            var tStat = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInCount(hawb.HousebillId, CntMsg.PieceCount, mawb.TaskId, string.Empty);
                            if (tStat.TransactionStatus1 == false)
                                CargoMatrix.UI.CMXMessageBox.Show(tStat.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);

                        }
                    }
                }

                TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.RemoveHouseBillFromConsol(hawb.HousebillId, mawb.MasterBillId, mawb.TaskId, pin, reasonId, shortage, ForkLiftViewer.Instance.ForkliftID);
                if (status.TransactionStatus1)
                {
                    CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_HousebillRemoved, hawb.Reference()), removeMode.ToString(), CargoMatrix.UI.CMXMessageBoxIcon.Success);
                    LoadControl();
                }
                else
                {
                    if (status.TransactionRecords > 0)
                        CargoMatrix.UI.CMXMessageBox.Show(string.Format("There are {0} scanned piece/s from hawb {1} remove them from ULD", status.TransactionRecords, hawb.HousebillNumber), "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                    else
                        CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                }
            }
        }
        private bool PopulateReasonsList()
        {
            if (reasonsBox == null)
            {
                reasonsBox = new CargoMatrix.Utilities.MessageListBox();
                reasonsBox.HeaderText = "Select Reason";
                reasonsBox.HeaderText2 = "Select a reason from list";
                reasonsBox.OneTouchSelection = true;
                reasonsBox.MultiSelectListEnabled = false;
                foreach (var reason in CargoMatrix.Communication.LoadConsol.Instance.GetRemoveReasons())
                    reasonsBox.AddItem(new SmoothListbox.ListItems.StandardListItem(reason.Reason, null, reason.ReasonId));
            }
            return DialogResult.OK == reasonsBox.ShowDialog();
        }

        #region ShortPieces
        CargoMatrix.Utilities.MessageListBox shortPiecesPopup;
        private void ShortPieces(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            if (shortPiecesPopup == null)
            {
                shortPiecesPopup = new CargoMatrix.Utilities.MessageListBox();
                List<Control> actions = new List<Control> {
                new SmoothListbox.ListItems.StandardListItem("SELECT ALL",null,3), 
                new SmoothListbox.ListItems.StandardListItem("SELECT RANGE",null,2),
                new SmoothListbox.ListItems.StandardListItem("SELECT PIECES",null,1) };

                shortPiecesPopup.AddItems(actions);
                shortPiecesPopup.MultiSelectListEnabled = false;
                shortPiecesPopup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(shortPiecesPopup_ListItemClicked);
                shortPiecesPopup.CancelClick += delegate(object s, EventArgs e) { shortPiecesPopup.Hide(); };
            }
            shortPiecesPopup.HeaderText = hawb.Reference();
            shortPiecesPopup.HeaderText2 = "Select Action to Continue";
            selectedhwbId = hawb.HousebillId;
            shortPiecesPopup.Show();

        }

        void shortPiecesPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var hawb = CargoMatrix.Communication.Inventory.Instance.GetHousebillInfo(selectedhwbId, mawb.TaskId);
            CustomUtilities.NumericPad numPad;

            var ids = from p in hawb.Pieces
                      select p.PieceId;
            switch ((listItem as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 3:// select all
                    foreach (int pid in ids)
                    {
                        var tStat = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInPiece(hawb.HousebillId, pid, mawb.TaskId, string.Empty);
                        if (tStat.TransactionStatus1 == false)
                            CargoMatrix.UI.CMXMessageBox.Show(tStat.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                    shortPiecesPopup.Hide();
                    break;
                case 2:// select range
                    numPad = new CustomUtilities.NumericPad(ids.ToArray<int>(), true);
                    numPad.Title = "Select missing pieces";

                    if (DialogResult.OK == numPad.ShowDialog())
                    {
                        foreach (int pid in numPad.SelectedIDs)
                        {
                            var tStat = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInPiece(hawb.HousebillId, pid, mawb.TaskId, string.Empty);
                            if (tStat.TransactionStatus1 == false)
                                CargoMatrix.UI.CMXMessageBox.Show(tStat.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        }
                        shortPiecesPopup.Hide();
                    }
                    break;// select random
                case 1:
                    numPad = new CustomUtilities.NumericPad(ids.ToArray<int>(),false);
                    numPad.Title = "Select missing pieces";

                    if (DialogResult.OK == numPad.ShowDialog())
                    {
                        foreach (int pid in numPad.SelectedIDs)
                        {
                            var tStat = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInPiece(hawb.HousebillId, pid, mawb.TaskId, string.Empty);
                            if (tStat.TransactionStatus1 == false)
                                CargoMatrix.UI.CMXMessageBox.Show(tStat.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                        }
                        shortPiecesPopup.Hide();
                    }
                    break;
            }
            shortPiecesPopup.OkEnabled = false;
            sender.Reset();
        }
        #endregion
        #endregion

        #region Switch Mode

        private bool SwitchMode(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            bool retValue = false;
            /// piece Mode -> Count
            if (hawb.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
            {
                string msg = string.Format(LoadConsolResources.Text_ChangeModeToCount, hawb.Reference());
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, mawb.TaskId, CargoMatrix.Communication.DTO.ScanModes.Count);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_ChangeCountConfirm, hawb.Reference()), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            // Count Mode -> Piece
            else
            {
                string msg = string.Format(LoadConsolResources.Text_ChangeModeToPiece, hawb.Reference());
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (!ChangeModeAllowed(hawb))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchNotAllowed, hawb.Reference()), "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        retValue = false;
                    }
                    else
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, mawb.TaskId, CargoMatrix.Communication.DTO.ScanModes.Piece);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_ChangePieceConfirm, hawb.Reference()), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            return retValue;
        }

        private bool ChangeModeAllowed(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            if (CargoMatrix.Communication.GTWSettings.Instance.ChangeHawbScanModeAllowed ||
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin ||
                DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                return true;
            else
                return false;
        }

        #endregion

        //#region PrintLabels
        //private CargoMatrix.Communication.DTO.HouseBillItem selectedHawb;

        //private void PrintHawbLabels(CargoMatrix.Communication.DTO.HouseBillItem hawb)
        //{
        //    selectedHawb = hawb;

        //    List<CustomUtilities.CMXDynamicButton> butons = new List<CustomUtilities.CMXDynamicButton> {
        //        new CustomUtilities.CMXDynamicButton() { Text = "SELECT ALL", Result = 3 }, 
        //        new CustomUtilities.CMXDynamicButton() { Text = "SELECT RANGE", Result = 1 },
        //        new CustomUtilities.CMXDynamicButton() { Text = "SELECT PIECES", Result = 2 },
        //        new CustomUtilities.CMXDynamicButton() { Text = "CANCEL", Result = 0 } };

        //    CustomUtilities.DynamicButtonsPopup printModePopup = new CustomUtilities.DynamicButtonsPopup(butons);
        //    printModePopup.Header = hawb.Reference;
        //    printModePopup.Label = "Select Action To Continue";
        //    printModePopup.ButtonClicked += new EventHandler(printModePopup_ButtonClicked);
        //    printModePopup.Show();

        //}

        //void printModePopup_ButtonClicked(object sender, EventArgs e)
        //{
        //    var actPopup = sender as CustomUtilities.DynamicButtonsPopup;
        //    CustomUtilities.NumericPad numPad;
        //    int[] hawbPieceIds = (from piece in CargoMatrix.Communication.LoadConsol.Instance.GetHousebillInfo(selectedHawb.HousebillId, mawb.TaskId).Pieces
        //                          select piece.PieceId).ToArray<int>();
        //    var ids = from i in smoothListBoxMainList.SelectedItems
        //              select (i as SmoothListbox.ListItems.StandardListItem).ID;
        //    switch (actPopup.Result)
        //    {
        //        case 3:// select all
        //            CargoMatrix.Communication.LoadConsol.Instance.PrintHouseBillLabels(selectedHawb.HousebillId, ids.ToArray<int>());
        //            actPopup.Close();
        //            break;
        //        case 2:// select random
        //            numPad = new CustomUtilities.NumericPad(hawbPieceIds);
        //            numPad.Title = "Select pieces to apply damage";

        //            if (DialogResult.OK == numPad.ShowDialog())
        //            {

        //                CargoMatrix.Communication.LoadConsol.Instance.PrintHouseBillLabels(selectedHawb.HousebillId, numPad.SelectedIDs.ToArray<int>());
        //                actPopup.Close();
        //            }
        //            break;// select range
        //        case 1:


        //            numPad = new CustomUtilities.NumericPad(hawbPieceIds, true);
        //            numPad.Title = "Select range of piece numbers to apply damage";


        //            if (DialogResult.OK == numPad.ShowDialog())
        //            {
        //                CargoMatrix.Communication.LoadConsol.Instance.PrintHouseBillLabels(selectedHawb.HousebillId, numPad.SelectedIDs.ToArray<int>());
        //                actPopup.Close();
        //            }
        //            break;
        //        case 0: // cancel
        //            actPopup.Close();
        //            break;
        //    }
        //}
        //#endregion

        protected override Control InitializeItem(CargoMatrix.Communication.DTO.IHouseBillItem item)
        {
            CustomListItems.ConsolHAWBListItem hawbItem = new CustomListItems.ConsolHAWBListItem(item);
            //hawbItem.
            hawbItem.OnViewerClick += new EventHandler(hawbItem_OnViewerClick);
            hawbItem.ButtonBrowseClick += new EventHandler(hawbItem_ButtonBrowseClick);
            hawbItem.ButtonConditionClick += new EventHandler(hawbItem_ButtonConditionClick);
            hawbItem.OnEnterClick += new EventHandler(HawbItem_OnEnterClick);
            return hawbItem;
        }
    }
}
