﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Communication.CargoTruckLoad;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Utilities;
using CustomListItems;
using CargoMatrix.UI;
using CMXExtensions;
using CargoMatrix.CargoTruckLoad;
using CargoMatrix.LoadContainer;


namespace CargoMatrix.LoadConsol
{
    public partial class DoorTruckSetUp : CargoUtilities.SmoothListBox2Options
    {
        public DoorTruckSetUp()
        {
            InitializeComponent();

            InitializeFields();

            this.ButtonFinalizeText = "Next";
            this.SetWindowTitle();

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;


            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(DoorTruckSetUp_BarcodeReadNotify);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(DoorTruckSetUp_ListItemClicked);
            this.LoadOptionsMenu += new EventHandler(DoorTruckSetUp_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(DoorTruckSetUp_MenuItemClicked);

            this.FinalizeButtonEnabled = false;
        }

        void DoorTruckSetUp_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if ((listItem is CustomListItems.OptionsListITem) == false) return;

            CustomListItems.OptionsListITem lItem = listItem as CustomListItems.OptionsListITem;

            switch (lItem.ID)
            {
                case OptionsListITem.OptionItemID.REFRESH:

                    this.LoadControl();

                    break;

                case OptionsListITem.OptionItemID.PRINT_TRUCK_LABEL:

                    var trucks = from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                                 where item.ItemData.Truck != null
                                 group item by item.ItemData.Truck.LocationId into truckGroups
                                 select truckGroups.First().ItemData.Truck;

                    TruckLabels truckLabels = new TruckLabels(trucks.ToArray());
                    truckLabels.ShowDialog();

                    break;
                default: return;
            }
        }

        void DoorTruckSetUp_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.PRINT_TRUCK_LABEL));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(this.UtilitesOptionsListItem);
        }

        void DoorTruckSetUp_BarcodeReadNotify(string barcodeData)
        {
            this.BarcodeEnabled = true;

            var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);

            if (scanItem.BarcodeType != CMXBarcode.BarcodeTypes.Door)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtWrongBarcodeType, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            CargoMatrix.UI.CMXSound.Play(CargoMatrix.Resources.Sounds.GoodScan);

            var listItem = (
                                from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                                where string.Compare(item.ItemData.Name, scanItem.Location) == 0
                                select item
                           ).FirstOrDefault();

            if (listItem == null)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_DoorNotInList, LoadTruckResources.TxtScanError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            this.UpdateDoorTruck(listItem);

            this.Refresh();

        }

        private void InitializeFields()
        {
            this.panel1.SuspendLayout();

            var headerItem = new CustomListItems.HeaderItem();
            headerItem.Label1 = ForkLiftViewer.Instance.Mawb.Reference();
            headerItem.Label2 = ForkLiftViewer.Instance.Mawb.Line2();
            headerItem.Label3 = "SCAN/SELECT DOOR";
            headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);

            headerItem.Location = panel1.Location;
            panel1.Visible = true;
            this.panel1.Controls.Add(headerItem);
            headerItem.BringToFront();
            panel1.Height = headerItem.Height;
            panelHeader2.Height = headerItem.Height;

            headerItem.Logo = ForkLiftViewer.Instance.CarrierLogo;

            this.panel1.ResumeLayout();
        }

        void headerItem_ButtonClick(object sender, EventArgs e)
        {
            this.BarcodeEnabled = false;

            CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem door;
            bool result = CommonMethods.AddDoor(CargoMatrix.Communication.Utilities.IsAdmin, out door);

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            if (result == false) return;

            var existingDoor = (
                                   from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                                   where item.ItemData.Name.Equals(door.Door)
                                   select item
                               ).FirstOrDefault();

            if (existingDoor != null)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtDoorAlreadyAssigned, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }


            CargoTruckLoadDoor newDoor = new CargoTruckLoadDoor(door);

            var status = CargoMatrix.Communication.LoadConsol.Instance.AssigneNewDoor(ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.TaskId, newDoor);

            if (status.TransactionStatus1 == true)
            {
                door.DoorId = status.TransactionId;
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            var listItem = this.InitializeItem(newDoor);

            this.smoothListBoxMainList.AddItem(listItem);

            this.SetWindowTitle();
        }

        protected Control InitializeItem(ICargoTruckLoadDoor item)
        {
            DoorViewItem listItem = new DoorViewItem(item);
            listItem.OnBrowseClick += new EventHandler(listItem_OnBrowseClick);
            listItem.OnEditClick += new EventHandler(listItem_OnEditClick);
            listItem.OnEnterClick += new EventHandler(listItem_OnEnterClick);


            return listItem;
        }

        void DoorTruckSetUp_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

            if (!CargoMatrix.Communication.Utilities.IsAdmin)
                return;

            var doorView = (DoorViewItem)listItem;

            this.UpdateDoorTruck(doorView);

            this.Refresh();

            this.smoothListBoxMainList.Reset();
        }

        void listItem_OnEnterClick(object sender, EventArgs e)
        {
            var listItem = (DoorViewItem)sender;

            this.UpdateDoorTruck(listItem);

            listItem.ClearEnter();

            this.Refresh();

            this.LayoutItems();
        }

        void listItem_OnEditClick(object sender, EventArgs e)
        {
            this.BarcodeEnabled = false;
            this.BarcodeEnabled = false;

            var listItem = (DoorViewItem)sender;

            var cargoloadItem = listItem.ItemData;

            CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem newDoorItem;
            bool result = CommonMethods.EditDoor(Communication.Utilities.IsAdmin, cargoloadItem, out newDoorItem);

            this.BarcodeEnabled = false;
            this.BarcodeEnabled = true;

            if (result == false) return;

            var existingDoor = (
                               from item in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                               where item.ItemData.Name.Equals(newDoorItem.Door)
                               select item
                           ).FirstOrDefault();

            if (existingDoor != null)
            {
                CargoMatrix.UI.CMXMessageBox.Show(LoadTruckResources.TxtDoorAlreadyAssigned, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            CargoTruckLoadDoor newDoor = new CargoTruckLoadDoor(newDoorItem);

            var status = CargoMatrix.Communication.LoadConsol.Instance.ReAssignDoor(ForkLiftViewer.Instance.Mawb.MasterBillId, cargoloadItem, ForkLiftViewer.Instance.Mawb.TaskId, newDoor);

            if (status.TransactionStatus1 == true)
            {
                newDoorItem.DoorId = status.TransactionId;
                newDoor.Truck = cargoloadItem.Truck;
                listItem.ItemData = newDoor;
            }
            else
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
            }
        }

        void listItem_OnBrowseClick(object sender, EventArgs e)
        {
            var listItem = (DoorViewItem)sender;

            MessageListBox actPopup = new MessageListBox();
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = listItem.ItemData.Name;
            actPopup.HeaderText2 = string.Format("Select action for {0}", listItem.ItemData.Name);

            var viewTuckContentitem = new SmoothListbox.ListItems.StandardListItem("VIEW CONTENT", null, 1);
            viewTuckContentitem.Enabled = listItem.ItemData.Truck != null;
            actPopup.AddItem(viewTuckContentitem);

            var printTruckLabelsItem = new SmoothListbox.ListItems.StandardListItem("PRINT TRUCK LABEL", null, 2);
            printTruckLabelsItem.Enabled = listItem.ItemData.Truck != null;
            actPopup.AddItem(printTruckLabelsItem);

            var dialogResult = actPopup.ShowDialog();

            if (dialogResult != DialogResult.OK) return;

            switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 1:

                    Truck truckView = new Truck(ForkLiftViewer.Instance.Mawb, listItem.ItemData.Truck);
                    truckView.ShowDialog();

                    break;

                case 2:

                    CommonMethods.PrintTruckLabels(listItem.ItemData.Truck);

                    break;
            }

        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;

            this.BarcodeEnabled = false;

            var doors = CargoMatrix.Communication.LoadConsol.Instance.GetMasterBillDoors(ForkLiftViewer.Instance.Mawb.MasterBillId);


            this.FinalizeButtonEnabled = true;
            smoothListBoxMainList.RemoveAll();
            foreach (var door in doors)
            {
                this.smoothListBoxMainList.AddItem(InitializeItem(door));
            }

            this.ShowContinueButton(true);

            this.BarcodeEnabled = true;

            this.SetWindowTitle();
            Cursor.Current = Cursors.Default;
        }


        protected override void buttonContinue_Click(object sender, EventArgs e)
        {
            base.buttonContinue_Click(sender, e);

            var assignedDoors = from doorItem in this.smoothListBoxMainList.Items.OfType<DoorViewItem>()
                                where doorItem.ItemData.Truck != null
                                select doorItem.ItemData;


            Cursor.Current = Cursors.WaitCursor;

            ProceedToLoad();

            Cursor.Current = Cursors.Default;
        }


        private void UpdateDoorTruck(DoorViewItem listItem)
        {
            this.barcode.StopRead();

            string scannedText;
            string locationName;


            var oldTruckName = listItem.ItemData.Truck == null ? null : listItem.ItemData.Truck.Location;

            bool result = CommonMethods.GetTruckFromUser(oldTruckName, string.Format("Assign truck for door: {0}", listItem.ItemData.Name),
                                                         "Scan/enter truck",
                                                         out scannedText, out locationName);

            this.barcode.StartRead();

            if (result == false) return;

            CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem locationItem = new CargoMatrix.Communication.WSLoadConsol.LoadTruckLocationItem();
            locationItem.Door = listItem.ItemData.Name;
            locationItem.DoorId = listItem.ItemData.ID;

            CargoTruckLoadDoor door = new CargoTruckLoadDoor(locationItem);

            CargoMatrix.Communication.WSLoadConsol.LocationItem truck = new CargoMatrix.Communication.WSLoadConsol.LocationItem();
            truck.LocationType = CargoMatrix.Communication.WSLoadConsol.LocationTypes.Truck;
            truck.LocationBarcode = scannedText;
            truck.Location = locationName;

            door.Truck = truck;

            var status = CargoMatrix.Communication.LoadConsol.Instance.AssigneDoorTruck(ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.TaskId, door);

            if (status.TransactionStatus1 == false)
            {
                CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, LoadTruckResources.TxtError, CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                return;
            }

            truck.LocationId = status.TransactionId;

            listItem.ItemData = door;

            this.FinalizeButtonEnabled = true;
        }

        private void SetWindowTitle()
        {
            this.TitleText = string.Format("CargoLoader - {0}-({1})", ForkLiftViewer.Instance.Mode, smoothListBoxMainList.ItemsCount);
        }

        private void ProceedToLoad()
        {
            Cursor.Current = Cursors.WaitCursor;

            CargoMatrix.UI.CMXUserControl nextForm ;
            if (ForkLiftViewer.Instance.Mawb.Mot == CargoMatrix.Communication.WSLoadConsol.MOT.Air)
                nextForm = new HousebillLoader();
            else
                nextForm = new OnhandLoader();

            (nextForm as UserControl).Location = new Point(Left, Top);
            (nextForm as UserControl).Size = new Size(Width, Height);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(nextForm as CargoMatrix.UI.CMXUserControl);
            smoothListBoxMainList.Reset();
        }

        protected override void pictureBoxBack_Click(object sender, EventArgs e)
        {
            if (ForkLiftViewer.Instance.Mode == ConsolMode.Load && !(CMXAnimationmanager.GetPreviousControl() is ULDBuilder))
                DisplayUldBuilder();
            else
                base.pictureBoxBack_Click(sender, e);

        }

        private void DisplayUldBuilder()
        {
            if (CMXAnimationmanager.GetPreviousControl() is ULDBuilder)
                CMXAnimationmanager.GoBack();
            else
            {
                Cursor.Current = Cursors.WaitCursor;
                ULDBuilder uldbuilder = new ULDBuilder();
                uldbuilder.Location = new Point(Left, Top);
                uldbuilder.Size = new Size(Width, Height);
                this.smoothListBoxMainList.Reset();
                CargoMatrix.UI.CMXAnimationmanager.GoBack(uldbuilder as CargoMatrix.UI.CMXUserControl);
            }
        }
    }

}
