﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.LoadConsol
{
    class Forklift
    {
        private static Forklift isntance;
        private int itemsCount = 0;
        private bool justUnloaded = false;
        private Forklift()
        { }
        public static Forklift Instance
        {
            get
            {
                if (Forklift.isntance == null)
                    Forklift.isntance = new Forklift();
                return Forklift.isntance;
            }
                    
        }
        /// <summary>
        /// temprorary property for working with stub data
        /// </summary>
        public bool JustUnloaded 
        {
            get
            {
                bool ret = justUnloaded;
                justUnloaded = false;
                return ret;
            }
        }
        public void Unload()
        {
            itemsCount = 0;
            justUnloaded = true;
        }
        public void AddItem(string id)
        {
            itemsCount ++;
        }
        public void Removeitem(string id)
        {
            itemsCount--;
        }
        public int ItemsCount
        {
            get { return itemsCount; }
        }
    }
}
