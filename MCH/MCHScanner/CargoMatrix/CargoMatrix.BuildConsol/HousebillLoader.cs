﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CustomListItems;
using CargoMatrix.Communication.WSLoadConsol;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using CargoMatrix.UI;
using CMXBarcode;
using CustomUtilities;

namespace CargoMatrix.LoadConsol
{
    public partial class HousebillLoader : CargoUtilities.SmoothListBoxOptions
    {
        private static CustomUtilities.ScanEnterPopup dropLoc;
        private CustomListItems.HeaderItem headerItem;
        private CargoMatrix.Communication.DTO.IMasterBillItem mawb { get { return ForkLiftViewer.Instance.Mawb; } }
        private Viewer.HousebillViewer billViewer;
        private string filter = HawbFilter.NOT_SCANNED;
        private string sort = HawbSort.WEIGHT;
        private static CargoMatrix.Utilities.MessageListBox reasonsBox;
        private int selectedhwbId;
        private LocationItem currentLoc = new LocationItem();
        private LocationItem[] locations;
        private HawbDetails hawbDetails;
        private bool m_bLocationsList = false;
        private bool locationListLoaded = false;
        private SmoothListbox.SmoothListBoxBase smoothListLocations = new SmoothListbox.SmoothListBoxBase();
        private string lastScan;
        private string progress;
        private CustomListItems.OptionsListITem filterOption;

        internal string LastScan
        {
            get
            {
                return "Last Scan: " + lastScan;
            }
            set { lastScan = value; pictureBoxBottomPane.Refresh(); }//headerItem.Label3 = LastScan; }
        }
        internal string Progress
        {
            get { return progress; }
            set { progress = value; pictureBoxBottomPane.Refresh(); }
        }

        private LocationItem CurrentLocation
        {
            get { return currentLoc; }
            set
            {
                currentLoc = value;
                if (hawbDetails != null) hawbDetails.buttonHeader.Refresh();
            }
        }
        private void SetCurrentLocation(string loc)
        {
            var cLoc = locations.FirstOrDefault(l => string.Equals(loc, l.Location, StringComparison.OrdinalIgnoreCase));
            if (cLoc != null)
                CurrentLocation = cLoc;
            else
                CurrentLocation = new LocationItem() { Location = loc };

        }

        private void SetCurrentLocation(string[] locs)
        {
            /// if list is empty or contains current location don't do anything.
            if (locs == null || locs.Contains(CurrentLocation.Location))
                return;
            else
            {
                string lastLoc = locs.Length > 0 ? locs[0] : string.Empty;
                var cLoc = locations.FirstOrDefault(l => string.Equals(lastLoc, l.Location, StringComparison.OrdinalIgnoreCase));
                if (cLoc != null)
                    CurrentLocation = cLoc;
                else
                    CurrentLocation = new LocationItem() { Location = lastLoc };
            }
        }
        public HousebillLoader()
        {
            InitializeComponent();



            this.panelHeader2.Controls.Add(hawbDetails);
            headerItem = new CustomListItems.HeaderItem();
            headerItem.Label1 = mawb.Reference();
            headerItem.Label2 = mawb.Line2();


            headerItem.Label3 = string.Format(LoadConsolResources.Text_HawbListBlink, ForkLiftViewer.Instance.Mode.ToString().ToUpper());
            headerItem.ButtonImage = CargoMatrix.Resources.Skin.Forklift_btn; //CargoMatrix.Resources.Skin.buffer;
            headerItem.ButtonPressedImage = CargoMatrix.Resources.Skin.Forklift_btn_over; //CargoMatrix.Resources.Skin.buffer_over;
            this.headerItem.ButtonClick += new EventHandler(headerItem_ButtonClick);
            this.panelHeader2.Controls.Add(headerItem);

            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(LoadHawbs_MenuItemClicked);

            //if (ForkLiftViewer.Instance.ScanHawbToULD)
            //    this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(LoadHawbs_BarcodeReadNotify2);
            //else
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(LoadHawbs_BarcodeReadNotify);
            this.Scrollable = false;
            headerItem.Logo = ForkLiftViewer.Instance.CarrierLogo;
            ForkLiftViewer.Instance.ItemCountChanged += new EventHandler(Forklift_ItemCountChanged);

            hawbDetails = new HawbDetails() { Location = new Point(0, headerItem.Height) };
            hawbDetails.buttonBrowse.Click += HawbItem_BrowseClick;
            hawbDetails.buttonDamage.Click += HawbItem_DamageClick;
            hawbDetails.buttonDetails.Click += HawbItem_ViewerClick;
            hawbDetails.buttonLeft.Click += buttonLeft_Click;
            hawbDetails.buttonRight.Click += buttonRight_Click;
            hawbDetails.buttonHeader.Paint += buttonHeader_Paint;
            hawbDetails.buttonHeader.Click += buttonHeader_Click;
            this.panelHeader2.Controls.Add(hawbDetails);

            if (ForkLiftViewer.Instance.DoubleScanEnabled)
            {
                buttonDrop.Visible = true;
            }
        }
        private void Forklift_ItemCountChanged(object sender, EventArgs e)
        {
            headerItem.Counter = ForkLiftViewer.Instance.ItemsCount;
        }


        private void LoadHawbs_BarcodeReadNotify(string barcodeData)
        {
            int forkliftPieces = ForkLiftViewer.Instance.ItemsCount;

            var scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            /// get scan Ids from backend 
            var scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(ForkLiftViewer.getScanItem(scanObj), mawb.TaskId, mawb.Mot);
            if (scanItem.Transaction.TransactionStatus1 == false)
            {
                scanObj.BarcodeType = CMXBarcode.BarcodeTypes.NA;
            }

            #region PULL MODE
            if (ForkLiftViewer.Instance.Mode == ConsolMode.Pull)
            {
                switch (scanObj.BarcodeType)
                {
                    /// Drop to Location
                    case CMXBarcode.BarcodeTypes.Door:
                    case CMXBarcode.BarcodeTypes.Area:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);

                        TransactionStatus status = CargoMatrix.Communication.LoadConsol.Instance.DropAllPiecesIntoLocation(mawb.TaskId, scanItem.LocationId, ForkLiftViewer.Instance.Mawb.Mot);
                        if (status.TransactionStatus1)
                        {
                            //CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullDrop, forkliftPieces, scanItem.Location), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            ForkLiftViewer.Instance.ItemsCount = status.TransactionRecords;
                            LoadControl();
                        }
                        else
                            ShowFailMessage(status.TransactionError);
                        break;

                    /// Change Mode to Load
                    case CMXBarcode.BarcodeTypes.Truck: //seperate doesn't have loose
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        // 1. have loose good load to truck
                        // 2. no loose "loose is not set up. Please set up loose" uld stup
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchMode, scanItem.BarcodeType.ToString()), LoadConsolResources.Text_SwitchModeTitle, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                            /// Loose is there
                            var looseUld = CargoMatrix.Communication.LoadConsol.Instance.MawbHasLooseULD(mawb.MasterBillId, MOT.Air);
                            if (looseUld != null)
                            {
                                TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoLocation(mawb.TaskId, looseUld.UldId, scanItem.LocationId, ForkLiftViewer.Instance.Mawb.Mot);
                                if (dropStatus.TransactionStatus1)
                                {
                                    //CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.UldNumber), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                    ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                                }
                                else
                                    ShowFailMessage(dropStatus.TransactionError);
                            }
                            else // loose is not there set up loose
                            {
                                DisplayUldBuilder(LoadConsolResources.Text_LooseSetUp);
                                return;
                            }
                        }
                        break;
                    case CMXBarcode.BarcodeTypes.Uld:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchMode, scanItem.BarcodeType.ToString()), LoadConsolResources.Text_SwitchModeTitle, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                            /// show ULD BUilder
                            if (scanItem.UldId <= 0)
                            {
                                DisplayUldBuilder(string.Format(LoadConsolResources.Text_ULDNotAttached, scanItem.UldNumber));
                                return;

                            }
                            else
                            {
                                LoadPiecesIntoULD(scanItem.UldId, scanItem.UldNumber);
                            }
                        }
                        break;

                    case CMXBarcode.BarcodeTypes.User:
                    case CMXBarcode.BarcodeTypes.NA:
                    case CMXBarcode.BarcodeTypes.MasterBill:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        ShowFailMessage(LoadConsolResources.Text_InvalidBarcode);
                        break;
                    case CMXBarcode.BarcodeTypes.HouseBill:
                    default:
                        break;

                }

            }
            #endregion

            #region LOAD MODE
            else
            {
                switch (scanObj.BarcodeType)
                {
                    case CMXBarcode.BarcodeTypes.Uld:
                        if (scanItem.UldId == 0)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_ULDNotAttached, scanItem.UldNumber), "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
                        }
                        else
                        {
                            CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                            LoadPiecesIntoULD(scanItem.UldId, scanItem.UldNumber);
                        }
                        break;

                    case CMXBarcode.BarcodeTypes.Truck: // loose needs to be there
                    case CMXBarcode.BarcodeTypes.Door:
                    case CMXBarcode.BarcodeTypes.Area:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);

                        if (ForkLiftViewer.Instance.IsRemoveHawbMode == true)
                        {
                            /// Drop only
                            dropLoc.ScannedPrefix = "B";
                            dropLoc.Text = scanObj.Location;
                            
                            dropLoc.ClosePopUp();
                            return;
                            //TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.DropHousebillIntoLocation("", mawb.TaskId, scanItem.LocationId);
                            //if (dropStatus.TransactionStatus1)
                            //{
                            //    ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                            //    LoadControl();
                            //    return;
                            //}
                            //else
                            //    ShowFailMessage(dropStatus.TransactionError);
           
                        }
                        else
                        {
                            /// Loose is there
                            var looseUld = CargoMatrix.Communication.LoadConsol.Instance.MawbHasLooseULD(mawb.MasterBillId, MOT.Air);
                            if (null != looseUld)
                            {
                                TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoLocation(mawb.TaskId, looseUld.UldId, scanItem.LocationId, ForkLiftViewer.Instance.Mawb.Mot);
                                if (dropStatus.TransactionStatus1)
                                {
                                    //CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, forkliftPieces, scanItem.Location), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                                    ForkLiftViewer.Instance.ItemsCount = dropStatus.TransactionRecords;
                                    //if (ForkLiftViewer.Instance.FinalizeReady)
                                    //{
                                    //    bool success = ForkLiftViewer.Instance.FinalizeConsol();
                                    //    if (success)
                                    //        return;
                                    //    else
                                    LoadControl();
                                    //}

                                }
                                else
                                    ShowFailMessage(dropStatus.TransactionError);
                            }
                            else // loose is not there, set up loose
                            {
                                DisplayUldBuilder(LoadConsolResources.Text_LooseSetUp);
                                return;
                            }
                        }

                      

                        break;

                    case CMXBarcode.BarcodeTypes.User:
                    case CMXBarcode.BarcodeTypes.NA:
                    case CMXBarcode.BarcodeTypes.MasterBill:
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        ShowFailMessage(LoadConsolResources.Text_InvalidBarcode);
                        break;
                    case CMXBarcode.BarcodeTypes.HouseBill:
                    default:
                        break;
                }
            }
            #endregion

            /// HAWB Scanned
            if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.HouseBill)
            {
                /// HAWB from consol
                if (scanItem.MasterBillId == mawb.MasterBillId)
                {
                    CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                    //BringHawbIntoView(scanItem.HouseBillId);
                    addHawbToForkLift(scanItem);
                }


                /// HAWB from other Consol
                else
                {
                    /// HAWB not attached to any Consol
                    if (scanItem.MasterBillId == 0 && scanItem.MasterBillNumber == string.Empty)
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.PASS);
                        string confMsg = string.Format(LoadConsolResources.Text_ConfirmAttach, scanItem.HouseBillNumber);
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(confMsg, "Confirm", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            CargoMatrix.Communication.LoadConsol.Instance.AttachHousebillToMasterbill(scanItem.HouseBillId, mawb.TaskId);
                            addHawbToForkLift(scanItem);
                        }

                    }
                    else
                    {
                        CargoMatrix.UI.CMXSound.Play(CargoMatrix.UI.CMXSound.SoundType.FAIL);
                        CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_ShipmentNotInConsol, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete, MessageBoxButtons.OK, DialogResult.OK);
                    }
                }
            }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private void LoadPiecesIntoULD(int uldId, string uldNo)
        {
            TransactionStatus loadStatus = CargoMatrix.Communication.LoadConsol.Instance.LoadAllPiecesIntoULD(mawb.TaskId, uldId, ForkLiftViewer.Instance.Mawb.Mot);
            if (loadStatus.TransactionStatus1)
            {
                //CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SuccessfullLoad, ForkLiftViewer.Instance.ItemsCount, uldNo), "Dropped", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                ForkLiftViewer.Instance.ItemsCount = loadStatus.TransactionRecords;
                //bool success = true;
                //if (ForkLiftViewer.Instance.FinalizeReady)
                //    success = ForkLiftViewer.Instance.FinalizeConsol();
                //if (!success)
                LoadControl();
            }
            else
                ShowFailMessage(loadStatus.TransactionError);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg">shows a messagebos if message is provided</param>
        private void DisplayUldBuilder(string message)
        {
            if (!string.IsNullOrEmpty(message))
                CargoMatrix.UI.CMXMessageBox.Show(message, "Set Up ULD", CargoMatrix.UI.CMXMessageBoxIcon.Message);
            if (CMXAnimationmanager.GetPreviousControl() is ULDBuilder)
                CMXAnimationmanager.GoBack();
            else
            {
                Cursor.Current = Cursors.WaitCursor;
                ULDBuilder uldbuilder = new ULDBuilder();
                uldbuilder.Location = new Point(Left, Top);
                uldbuilder.Size = new Size(Width, Height);
                this.smoothListBoxMainList.Reset();
                CargoMatrix.UI.CMXAnimationmanager.GoBack(uldbuilder as CargoMatrix.UI.CMXUserControl);
            }
        }

        #region add Hawb into F/L
        private void addHawbToForkLift(ScanItem scanItem)
        {
            switch (scanItem.ScanMode)
            {
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Count:
                    AddToForkliftInCountMode(scanItem);
                    break;
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Slac:
                    AddToForkliftInSlacMode(scanItem);
                    break;
                case CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece:
                default:
                    AddToForkliftInPieceMode(scanItem);
                    break;
            }
        }
        private void AddToForkliftInSlacMode(ScanItem hawb)
        {
            Control[] actions = new Control[]
                {   
                    new SmoothListbox.ListItems.StandardListItem("PIECE ID",null,1),
                    new SmoothListbox.ListItems.StandardListItem("COUNT",null,2),
                };
            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.AddItems(actions);
            actPopup.HeaderText2 = string.Format("Total Pieces: {0} SLAC: {1}", hawb.TotalPieces, "N/A");
            actPopup.HeaderText = hawb.MasterBillNumber;
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1:
                        AddToForkliftInPieceMode(hawb);
                        break;
                    case 2:
                        AddToForkliftInCountMode(hawb);
                        break;
                    default:
                        break;
                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();
        }
        private void AddToForkliftInPieceMode(ScanItem hawb)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (hawb.RemoveMode == CargoMatrix.Communication.WSLoadConsol.RemoveModes.NA)
            {
                var response = CargoMatrix.Communication.LoadConsol.Instance.ScanHousebillIntoForkliftPiece(hawb.HouseBillId, mawb.MasterBillId, hawb.PieceId, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, ScanTypes.Automatic, filter, sort, CurrentLocation.Location, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
                if (!response.Status.TransactionStatus1)
                    ShowFailMessage(response.Status.TransactionError);
                else
                {
                    lastScan = hawb.HouseBillNumber;
                    //PopulateHousebillDetails(response); #warning needs to be refactored
                    LoadControl();
                }
            }
            else HandleRemovedHousebill(hawb, ScanTypes.Automatic);

            Cursor.Current = Cursors.Default;
        }
        private void AddToForkliftInCountMode(ScanItem hawb)
        {
            CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
            CntMsg.PieceCount = hawb.TotalPieces - hawb.ScannedPieces;
            if (CntMsg.PieceCount == 0)
            {
                string msg = string.Format(LoadConsolResources.Text_HawbAlreadyScanned, hawb.HouseBillNumber);
                ShowFailMessage(msg);
                return;
            }

            CntMsg.HeaderText = "Confirm count";
            CntMsg.LabelDescription = "Enter number of pieces to load";
            CntMsg.LabelReference = hawb.HouseBillNumber;

            if (DialogResult.OK == CntMsg.ShowDialog())
            {
                if (hawb.RemoveMode == CargoMatrix.Communication.WSLoadConsol.RemoveModes.NA)
                {
                    var response = CargoMatrix.Communication.LoadConsol.Instance.ScanHousebillIntoForkliftCount(hawb.HouseBillId, mawb.MasterBillId, CntMsg.PieceCount, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, ScanTypes.Automatic, filter, sort, CurrentLocation.Location, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
                    if (!response.Status.TransactionStatus1)
                        ShowFailMessage(response.Status.TransactionError);
                    else
                    {
                        lastScan = hawb.HouseBillNumber;
                        //PopulateHousebillDetails(response); #warning needs to be refactored
                        LoadControl();
                    }
                }
                else
                {
                    HandleRemovedHousebill(hawb, ScanTypes.Automatic);
                }
            }
        }
        #endregion

        private static void ShowFailMessage(string message)
        {
            Cursor.Current = Cursors.Default;
            if (!string.IsNullOrEmpty(message))
                CargoMatrix.UI.CMXMessageBox.Show(message, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
        }
        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            ShowNavButtons(true);
            this.buttonUp.BringToFront();
            this.buttonDown.BringToFront();
            this.TitleText = string.Format("CargoLoader - {0} - ({1}/{2})", ForkLiftViewer.Instance.Mode.ToString(), filter, sort);


            locations = CargoMatrix.Communication.LoadConsol.Instance.GetMasterbillLocations(mawb.MasterBillId, filter, ForkLiftViewer.Instance.Mode == ConsolMode.Load, MOT.Air);
            if (locations.Length > 0)
            {
                // if the previous location is in the list find it and keep as a new currentLocation
                CurrentLocation = locations.FirstOrDefault<LocationItem>(l => l.Location == CurrentLocation.Location);

                // if the previous location not in the list take the first one as currentLocation
                if (null == CurrentLocation)
                    CurrentLocation = locations[0];
            }
            else
                CurrentLocation = new LocationItem();

            if (!BarcodeEnabled)
                BarcodeEnabled = true;
            DisplayHawb(ListDirection.Current);
            Cursor.Current = Cursors.Default;
        }

        private void ShowNavButtons(bool flag)
        {
            this.buttonDown.Visible = flag;
            this.buttonUp.Visible = flag;
            this.pictureBoxDown.Visible = !flag;
            this.pictureBoxUp.Visible = !flag;
        }

        private void headerItem_ButtonClick(object sender, EventArgs e)
        {
            //RDF
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            ForkLiftViewer.Instance.ShowDialog();

            try
            {
                BarcodeEnabled = true;
                LoadControl();
            }
            catch (Exception ex)
            {
                //Object is already destroyed
            }
    

        }

        private void HawbItem_DamageClick(object sender, EventArgs e)
        {
            DamageCapture.DamageCapture damageCapture = new DamageCapture.DamageCapture(hawbDetails.Hawb);

            damageCapture.OnLoadShipmentConditions += (sndr, args) =>
            {
                args.ShipmentConditions = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionTypes();
            };

            damageCapture.OnLoadShipmentConditionsSummary += (sndr, args) =>
            {
                args.ConditionsSummaries = CargoMatrix.Communication.LoadConsol.Instance.GetShipmentConditionSummary(args.HouseBillID, ForkLiftViewer.Instance.Mawb.Mot);
            };

            damageCapture.OnUpdateShipmentConditions += (sndr, args) =>
            {
                args.Result = CargoMatrix.Communication.LoadConsol.Instance.UpdateShippmentCondition(args.HouseBillId, args.ScanModes, args.ShipmentConditions, mawb.TaskId, string.Empty, ForkLiftViewer.Instance.Mawb.Mot);
            };

            damageCapture.GetHouseBillPiecesIDs = () =>
            {
                return
                    (
                        from item in CargoMatrix.Communication.LoadConsol.Instance.GetHousebillInfo(hawbDetails.Hawb.HousebillId, mawb.TaskId).Pieces
                        select item.PieceId
                    ).ToArray();
            };

            damageCapture.Size = this.Size;
            BarcodeEnabled = false;
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(damageCapture);

        }
        private void HawbItem_BrowseClick(object sender, EventArgs e)
        {
            List<Control> actions = new List<Control>
            {
                new SmoothListbox.ListItems.StandardListItem("SHOW LOCATIONS",null,5),
                new SmoothListbox.ListItems.StandardListItem("PRINT LABELS",null,4),
                new SmoothListbox.ListItems.StandardListItem("SWITCH MODE",null,3),
            };
            if (hawbDetails.Hawb.RemoveMode == CargoMatrix.Communication.DTO.RemoveModes.TakeOff || hawbDetails.Hawb.RemoveMode == CargoMatrix.Communication.DTO.RemoveModes.Shortage)
            {
                actions.Add(new SmoothListbox.ListItems.StandardListItem("RETURN TO CONSOL", null, 6));
                actions.Add(new SmoothListbox.ListItems.StandardListItem("REMOVE FROM CONSOL", null, 7));
            }
            else
            {
                actions.Add(new SmoothListbox.ListItems.StandardListItem("TAKEOFF AND REMOVE", null, 8));
                actions.Add(new SmoothListbox.ListItems.StandardListItem("SHORT AND REMOVE", null, 9));
                actions.Add(new SmoothListbox.ListItems.StandardListItem("TAKEOFF", null, 1));
                actions.Add(new SmoothListbox.ListItems.StandardListItem("SHORT", null, 2));
            }


            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions.ToArray());
            actPopup.OneTouchSelection = true;
            actPopup.MultiSelectListEnabled = false;
            actPopup.HeaderText = hawbDetails.Hawb.Reference();
            actPopup.HeaderText2 = "Select action to continue";
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 1:
                        TakeOffHousebill(hawbDetails.Hawb, CargoMatrix.Communication.DTO.RemoveModes.TakeOff);
                        break;
                    case 2:
                        TakeOffHousebill(hawbDetails.Hawb, CargoMatrix.Communication.DTO.RemoveModes.Shortage);
                        break;
                    case 3:
                        BarcodeEnabled = false;
                        SwitchMode(hawbDetails.Hawb);
                        LoadControl();
                        break;
                    case 4:
                        PrintLabels(hawbDetails.Hawb);
                        break;
                    case 5:
                        ShowLocations(hawbDetails.Hawb);
                        break;
                    case 6:
                        if (ReturnHawbIntoConsol(hawbDetails.Hawb.HousebillId, hawbDetails.Hawb.HousebillNumber))
                            LoadControl();
                        break;
                    case 7:
                        if (RemoveHawbFromConsol(hawbDetails.Hawb.HousebillId, hawbDetails.Hawb.HousebillNumber, ScanTypes.Manual))
                            LoadControl();
                        break;
                    case 8:
                        if (TakeOffAndRemoveHousebill(hawbDetails.Hawb, CargoMatrix.Communication.DTO.RemoveModes.TakeOff))
                            LoadControl();
                        break;
                    case 9:
                        if (TakeOffAndRemoveHousebill(hawbDetails.Hawb, CargoMatrix.Communication.DTO.RemoveModes.Shortage))
                            LoadControl();
                        break;
                }
            }
            actPopup.OkEnabled = false;
            actPopup.ResetSelection();
        }


        private void HawbItem_ViewerClick(object sender, EventArgs e)
        {
            DisplayBillViewer(hawbDetails.Hawb.HousebillNumber);
        }
        private void ShowLocations(IHouseBillItem hawb)
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxMainList.Reset();
            BarcodeEnabled = false;
            HawbViewer hawbViewer = new HawbViewer(hawb, filter, sort, CurrentLocation.Location);//
            LoadControl();
        }

        private void LoadHawbs_LoadOptionsMenu(object sender, System.EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter + " / " + sort };
            this.AddOptionsListItem(filterOption);
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.SHOW_LOCATIONS));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.VIEW_HAWB_LIST) { Title = "Locked Housebills" });

            this.AddOptionsListItem(new CustomListItems.OptionsListITem(OptionsListITem.OptionItemID.PRINT_MAWB_LABEL));

            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.SHARE_TASK) { Enabled = mawb.AllowShare });
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL) { Enabled = ForkLiftViewer.Instance.Mode == ConsolMode.Load });
            this.AddOptionsListItem(UtilitesOptionsListItem);
        }


        private void DisplayBillViewer(string hawbNo)
        {
            if (billViewer != null)
                billViewer.Dispose();

            billViewer = new CargoMatrix.Viewer.HousebillViewer(hawbNo);//"3TB7204" masterbill);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);
            BarcodeEnabled = false;
            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }

        private void LoadHawbs_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is CustomListItems.OptionsListITem)
            {
                FilterPopup fp;
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {

                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        fp = new FilterPopup(FilteringMode.Hawb);
                        fp.Filter = filter;
                        fp.Sort = sort;
                        if (DialogResult.OK == fp.ShowDialog())
                        {
                            sort = fp.Sort;
                            filter = fp.Filter;
                            if (filterOption != null)
                                filterOption.DescriptionLine = filter + " / " + sort;
                            Cursor.Current = Cursors.WaitCursor;
                            LoadControl();
                        }
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:
                        LoadControl();
                        break;
                    case OptionsListITem.OptionItemID.PRINT_MAWB_LABEL:
                        CustomUtilities.MawbLabelPrinter.Show(mawb);
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.SHARE_TASK:
                        ShareTask.ShowDialog(mawb);
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZE_CONSOL:
                        if (ForkLiftViewer.Instance.FinalizeReady)
                            SharedMethods.FinalizeConsol();
                        else
                            CargoMatrix.UI.CMXMessageBox.Show(ForkLiftViewer.Instance.FinalizeMessage, "Not Ready", CMXMessageBoxIcon.Hand);
                        break;
                    case OptionsListITem.OptionItemID.SHOW_LOCATIONS:
                        HawbLocationList locViewer = new HawbLocationList(filter, sort);
                        locViewer.ShowDialog();
                        if (locViewer.SelectedHousebill != null)
                        {
                            SetCurrentLocation(locViewer.SelectedHousebill.LastLocation);
                            hawbDetails.DisplayHousebill(new CargoMatrix.Communication.DTO.HouseBillItem(locViewer.SelectedHousebill));
                        }
                        break;
                    case OptionsListITem.OptionItemID.VIEW_HAWB_LIST:
                        new LockedHawbList().ShowDialog();
                        break;
                    default:
                        break;
                }
                ShowNavButtons(true);
            }
        }

        internal void PrintLabels(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            BarcodeEnabled = false;
            CustomUtilities.HawbLabelPrinter.Show(hawb.HousebillId, hawb.HousebillNumber, hawb.TotalPieces);
            BarcodeEnabled = true;
        }

        protected override void pictureBoxMenu_Click(object sender, EventArgs e)
        {
            if (m_bLocationsList)
                ShowLocationList(false);
            base.pictureBoxMenu_Click(sender, e);
            if (m_bOptionsMenu)
            {
                ShowNavButtons(false);
            }
            else
            {
                ShowNavButtons(true);
            }
        }
        //protected override void pictureBoxBack_Click(object sender, EventArgs e)
        //{
        //    if (ForkLiftViewer.Instance.Mode == ConsolMode.Load && !(CMXAnimationmanager.GetPreviousControl() is ULDBuilder))
        //        DisplayUldBuilder(string.Empty);
        //    else
        //        base.pictureBoxBack_Click(sender, e);

        //    if (m_bOptionsMenu)
        //    {
        //        ShowNavButtons(false);
        //    }
        //    else
        //    {
        //        ShowNavButtons(true);
        //    }
        //}



        #region Navigation
        protected override void pictureBoxUp_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxUp_MouseUp(sender, e);

            else
            {
                if (m_bLocationsList)
                    smoothListLocations.UpButtonPressed = false;

                if (pictureBoxUp.Enabled)
                    UpButtonPressed(false);

            }
        }
        protected override void pictureBoxUp_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxUp_MouseDown(sender, e);

            else
            {
                if (m_bLocationsList)
                    smoothListLocations.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.UP);

                if (pictureBoxDown.Enabled)
                    UpButtonPressed(true);
            }
        }
        protected override void pictureBoxDown_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseDown(sender, e);

            else
            {
                if (m_bLocationsList)
                    smoothListLocations.Scroll(SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN);

                if (pictureBoxDown.Enabled)
                    DownButtonPressed(true);
            }
        }
        protected override void pictureBoxDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_bOptionsMenu == true)
                base.pictureBoxDown_MouseUp(sender, e);

            else
            {
                if (m_bLocationsList)
                    smoothListLocations.DownButtonPressed = false;
                if (pictureBoxDown.Enabled)
                    DownButtonPressed(false);

            }
        }
        private void smoothListLocations_AutoScroll(SmoothListbox.SmoothListBoxBase.DIRECTION direction, bool enable)
        {
            if (m_bLocationsList == true && m_bOptionsMenu == false)
            {
                switch (direction)
                {
                    case SmoothListbox.SmoothListBoxBase.DIRECTION.UP:
                        pictureBoxUp.Enabled = enable;
                        if (pictureBoxUp.Enabled == false)
                            smoothListLocations.UpButtonPressed = false;
                        break;
                    case SmoothListbox.SmoothListBoxBase.DIRECTION.DOWN:
                        //pictureBoxBookMarkNext.Enabled = enable;
                        pictureBoxDown.Enabled = enable;
                        if (pictureBoxDown.Enabled == false)
                            smoothListLocations.DownButtonPressed = false;
                        break;
                }

            }

        }

        void pictureBoxBottomPane_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            using (Font ft = new Font("Tahoma", 9F, FontStyle.Bold))
            {
                SizeF textScanSize = e.Graphics.MeasureString(LastScan, ft);
                SizeF textProgressSize = e.Graphics.MeasureString(Progress, ft);
                int y1 = (int)(pictureBoxBottomPane.Height - 2 * textScanSize.Height) / 2;
                int y2 = (int)(textScanSize.Height + (2 * y1));
                int x2 = (int)(pictureBoxBottomPane.Width - textProgressSize.Width) / 2;
                e.Graphics.DrawString(LastScan, ft, new SolidBrush(Color.White), 10, y1);
                e.Graphics.DrawString(Progress, ft, new SolidBrush(Color.White), x2, y2);
            }
        }


        private void DisplayHawb(ListDirection dir)
        {
            int currentHawbid = 0;
            if (hawbDetails.Hawb != null)
                currentHawbid = hawbDetails.Hawb.HousebillId;
            var hb = CargoMatrix.Communication.LoadConsol.Instance.GetHousebill(currentHawbid, dir, ForkLiftViewer.Instance.Mawb.MasterBillId, ForkLiftViewer.Instance.Mawb.TaskId, filter, sort, CurrentLocation.Location, ForkLiftViewer.Instance.ForkliftID, ForkLiftViewer.Instance.Mode == ConsolMode.Load, ForkLiftViewer.Instance.Mawb.Mot);
            if (hb.Status.TransactionStatus1 == false && hb.Status.TransactionError != "No records found.")
            {
                ShowFailMessage(hb.Status.TransactionError);
                if (hb.HouseBill == null && hawbDetails.Hawb != null)
                {
                    string[] locs = new string[0];
                    if ((hb.HouseBill is CargoMatrix.Communication.WSLoadConsol.HouseBillItem))
                    {
                        var pieceLocs = (hb.HouseBill as CargoMatrix.Communication.WSLoadConsol.HouseBillItem).PieceLocations;
                        locs = (from ploc in pieceLocs
                                select ploc.LocationName).ToArray<string>();
                    }

                    SetCurrentLocation(locs);
                }
                else
                    PopulateHousebillDetails(hb);
            }
            else
                PopulateHousebillDetails(hb);
        }

        private void PopulateHousebillDetails(HousebillSummary hb)
        {
            BarcodeEnabled = false;
            ForkLiftViewer.Instance.ItemsCount = hb.Status.TransactionRecords;

            Progress = string.Format("Progress : {0}%", (int)hb.Status.TaskProgress);
            if (hb.Status.TaskProgress == 100)
            {
                if (ForkLiftViewer.Instance.FinalizeReady
                && SharedMethods.FinalizeConsol())
                    return;

            }

            string[] locs = new string[0];
            if ((hb.HouseBill is CargoMatrix.Communication.WSLoadConsol.HouseBillItem))
            {
                var pieceLocs = (hb.HouseBill as CargoMatrix.Communication.WSLoadConsol.HouseBillItem).PieceLocations;
                locs = (from ploc in pieceLocs
                        select ploc.LocationName).ToArray<string>();
            }

            SetCurrentLocation(locs);
            if (hb.HouseBill != null)
            {
                foreach (var item in (hb.HouseBill as CargoMatrix.Communication.WSLoadConsol.HouseBillItem).ShipmentAlerts)
                    CargoMatrix.UI.CMXMessageBox.Show(item.AlertMessage + Environment.NewLine + item.AlertSetBy + Environment.NewLine + item.AlertDate.ToString("MM/dd HH:mm"), "Alert: " + hb.HouseBill.HousebillNumber, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
            hawbDetails.DisplayHousebill(hb.HouseBill);

            /// Pull proccess is finished
            if (ForkLiftViewer.Instance.Mode == ConsolMode.Pull && hb.Status.TaskProgress == 100)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(LoadConsolResources.Text_SwitchToLoad, "Switch to Load", CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.OK))
                {
                    ForkLiftViewer.Instance.Mode = ConsolMode.Load;
                    pictureBoxBack_Click(null, EventArgs.Empty);
                }
            }
            if (BarcodeEnabled == false)
                BarcodeEnabled = true;
        }

        void buttonRight_Click(object sender, EventArgs e)
        {
            if (locations.Length > 1)
            {
                int newindex = (currenLocationIndex + 1) % locations.Length;
                CurrentLocation = locations[newindex];
                if (m_bLocationsList)
                    ShowLocationList(false);
                DisplayHawb(ListDirection.Current);
            }
        }

        void buttonLeft_Click(object sender, EventArgs e)
        {
            if (locations.Length > 1)
            {
                int newindex = (currenLocationIndex + locations.Length - 1) % locations.Length;
                CurrentLocation = locations[newindex];
                if (m_bLocationsList)
                    ShowLocationList(false);
                DisplayHawb(ListDirection.Current);
            }
        }
        private int currenLocationIndex
        {
            get
            {
                for (int i = 0; i < locations.Length; i++)
                    if (locations[i] == CurrentLocation)
                        return i;

                return 0;
            }
        }


        void LocationItem_ButtonClick(object sender, EventArgs e)
        {
            BarcodeEnabled = false;
            string loc = (sender as ListItemCheckWithButton<LocationItem>).ItemData.Location;
            //string filter = FilterPopup.GetFilter();
            var locViewer = new HawbLocationList(filter, sort, loc);
            ShowLocationList(false);
            if (locViewer.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                this.Refresh();
                if (locViewer.SelectedHousebill != null)
                {
                    SetCurrentLocation(locViewer.SelectedHousebill.LastLocation);
                    hawbDetails.DisplayHousebill(new CargoMatrix.Communication.DTO.HouseBillItem(locViewer.SelectedHousebill));
                }
                Cursor.Current = Cursors.Default;
            }
            BarcodeEnabled = true;
        }

        void smoothListLocations_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            CurrentLocation = (listItem as ListItemCheckWithButton<LocationItem>).ItemData;
            ShowLocationList(false);
            DisplayHawb(ListDirection.Current);
        }

        void buttonHeader_Click(object sender, EventArgs e)
        {
            ShowLocationList(!m_bLocationsList);
        }
        private void CreateLocationList()
        {
            locationListLoaded = true;
            smoothListLocations.Width = this.Width;
            smoothListLocations.Height = hawbDetails.Height - hawbDetails.buttonHeader.Height + pictureBoxBottomPane.Height;
            smoothListLocations.Location = new Point(0, hawbDetails.Top + hawbDetails.buttonHeader.Height + panelTitle.Height);
            smoothListLocations.Visible = false;
            smoothListLocations.UnselectEnabled = false;
            smoothListLocations.MultiSelectEnabled = false;
            smoothListLocations.ListItemClicked += new SmoothListbox.ListItemClickedHandler(smoothListLocations_ListItemClicked);
            this.Controls.Add(smoothListLocations);
            smoothListLocations.BringToFront();

        }

        private void ShowLocationList(bool show)
        {
            if (show)
            {
                m_bLocationsList = true;
                if (locationListLoaded == false)
                    CreateLocationList();
                smoothListLocations.RemoveAll();

                locations = CargoMatrix.Communication.LoadConsol.Instance.GetMasterbillLocations(mawb.MasterBillId, filter, ForkLiftViewer.Instance.Mode == ConsolMode.Load, MOT.Air);
                foreach (var item in locations.Where<LocationItem>(l => !string.IsNullOrEmpty(l.Location)))
                {
                    var cntrl = new ListItemCheckWithButton<LocationItem>(item, item.Location, string.Format("Hawb: {0} WGT: {1} KGS", item.TotalHousebills, item.TotalWeight.ToString("0.##")));
                    cntrl.OnButtonClick += new EventHandler(LocationItem_ButtonClick);
                    smoothListLocations.AddItem(cntrl);
                }
                selectCurrentLocation();
                smoothListLocations.Visible = true;
                this.hawbDetails.buttonHeader.Image = global::Resources.Graphics.Skin.gen_dropdown_oposite;
                smoothListBoxMainList.AutoScroll -= smoothListBoxMainList_AutoScroll;
                smoothListLocations.AutoScroll += new SmoothListbox.AutoScrollHandler(smoothListLocations_AutoScroll);
                ShowNavButtons(false);
                smoothListLocations.RefreshScroll();
            }
            else
            {
                m_bLocationsList = false;
                smoothListLocations.Visible = false;
                smoothListBoxMainList.AutoScroll += smoothListBoxMainList_AutoScroll;
                smoothListLocations.AutoScroll -= new SmoothListbox.AutoScrollHandler(smoothListLocations_AutoScroll);
                this.hawbDetails.buttonHeader.Image = global::Resources.Graphics.Skin.gen_dropdown_oposite_over;
                ShowNavButtons(true);
            }
        }

        private void selectCurrentLocation()
        {
            smoothListLocations.SelectNone();
            foreach (ListItemCheckWithButton<LocationItem> item in smoothListLocations.Items)
            {
                if (CurrentLocation == item.ItemData)
                {
                    item.SelectedChanged(true);
                    break;
                }
            }
        }

        void buttonHeader_Paint(object sender, PaintEventArgs e)
        {
            if (CurrentLocation == null || string.IsNullOrEmpty(CurrentLocation.Location))
                return;
            using (Font ft = new Font("Tahoma", 9F, FontStyle.Bold))
            {
                using (Brush b = new SolidBrush(Color.White))
                {
                    string text = string.Format("{0} ({1})", CurrentLocation.Location, CurrentLocation.TotalHousebills);
                    SizeF textSize = e.Graphics.MeasureString(text, ft);
                    int x = (hawbDetails.buttonHeader.Width - (int)textSize.Width) / 2;
                    int y = (hawbDetails.buttonHeader.Height - (int)textSize.Height) / 2;
                    e.Graphics.DrawString(text, ft, b, x, y);
                }
            }
        }

        void buttonDown_Click(object sender, System.EventArgs e)
        {
            DisplayHawb(ListDirection.NextItem);
        }

        void buttonUp_Click(object sender, System.EventArgs e)
        {
            DisplayHawb(ListDirection.PreviousItem);
        }
        void buttonDrop_Click(object sender, System.EventArgs e)
        {
            ForkLiftViewer.Instance.ShowDialog();
            LoadControl();
        }
        #endregion


        #region RemovedPieces
        internal void HandleRemovedHousebill(ScanItem hawb, ScanTypes scanType)
        {
            CargoMatrix.Utilities.MessageListBox actPopup;
            Control[] actions = new Control[]
                {   
                    new SmoothListbox.ListItems.StandardListItem("RETURN TO CONSOL",null, 2),
                    new SmoothListbox.ListItems.StandardListItem("REMOVE FROM CONSOL",null, 1) };

            actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.AddItems(actions);
            actPopup.MultiSelectListEnabled = false;
            actPopup.OneTouchSelection = true;
            actPopup.HeaderText = "Select Action To Continue";
            actPopup.HeaderText2 = "Hawb was removed";
            Cursor.Current = Cursors.Default;
            if (DialogResult.OK == actPopup.ShowDialog())
            {
                switch ((actPopup.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem).ID)
                {
                    case 2: // RETURN
                        #region oldCode
                        //msg = string.Format(LoadConsolResources.Text_ReturnToConsol, hawb.HouseBillNumber, mawb.MasterBillNumber);
                        //if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Return Hawb", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        //{
                        //    HousebillSummary HbSummary;
                        //    if (hawb.ScanMode == CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece)
                        //        HbSummary = CargoMatrix.Communication.LoadConsol.Instance.ScanRemovedPieceIntoForklift(hawb.HouseBillId, mawb.MasterBillId, hawb.PieceId, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, CargoMatrix.Communication.WSLoadConsol.ScanModes.Piece, scanType, filter, sort, CurrentLocation.Location, ForkLiftViewer.Instance.Mode == ConsolMode.Load);
                        //    else
                        //    {
                        //        CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
                        //        CntMsg.PieceCount = hawb.TotalPieces;
                        //        CntMsg.HeaderText = "Confirm count";
                        //        CntMsg.LabelDescription = "Enter number of pieces to load";
                        //        CntMsg.LabelReference = hawb.HouseBillNumber;

                        //        if (DialogResult.OK == CntMsg.ShowDialog())
                        //            HbSummary = CargoMatrix.Communication.LoadConsol.Instance.ScanRemovedPieceIntoForklift(hawb.HouseBillId, mawb.MasterBillId, hawb.TotalPieces, mawb.TaskId, ForkLiftViewer.Instance.ForkliftID, CargoMatrix.Communication.WSLoadConsol.ScanModes.Count, scanType, filter, sort, CurrentLocation.Location, ForkLiftViewer.Instance.Mode == ConsolMode.Load);
                        //        else break;
                        //    }
                        //    if (!HbSummary.Status.TransactionStatus1)
                        //        CargoMatrix.UI.CMXMessageBox.Show(HbSummary.Status.TransactionError, "Error", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        //    else
                        //    {
                        //        lastScan = HbSummary.HouseBill.HousebillNumber;
                        //        PopulateHousebillDetails(HbSummary);
                        //    }
                        //}
                        #endregion
                        bool result = ReturnHawbIntoConsol(hawb.HouseBillId, hawb.HouseBillNumber);
                        if (result)
                            addHawbToForkLift(hawb);
                        break; // REMOVE
                    case 1:
                        if (RemoveHawbFromConsol(hawb.HouseBillId, hawb.HouseBillNumber, scanType))
                            LoadControl();
                        break;
                }
            }
            //actPopup.OkEnabled = false;
            //actPopup.ResetSelection();
        }

        internal static bool ReturnHawbIntoConsol(int hawbId, string hawbNo)
        {
            string msg = string.Format(LoadConsolResources.Text_ReturnToConsol, hawbNo, ForkLiftViewer.Instance.Mawb.MasterBillNumber);
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Return Hawb", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {

                var status = CargoMatrix.Communication.LoadConsol.Instance.ReturnHawbIntoConsol(hawbId);

                if (!status.TransactionStatus1)
                    ShowFailMessage(status.TransactionError);
                return status.TransactionStatus1;
            }
            return false;
        }

        internal static bool RemoveHawbFromConsol(int hawbId, string hawbNo, ScanTypes scanType)
        {
            string msg = string.Format(LoadConsolResources.Text_RemoveFromConsol, hawbNo, ForkLiftViewer.Instance.Mawb.MasterBillNumber);
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(msg, "Remove Hawb", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                return false;
            int superId = 0;

            if (!CargoMatrix.Communication.Utilities.IsAdmin)
            {
                if (DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                    superId = CargoMatrix.Communication.LoadConsol.Instance.GetSupervisorIDByPin(int.Parse(CustomUtilities.SupervisorAuthorizationMessageBox.SupervisorPIN));
                else
                    return false;
            }


            if (!PopulateReasonsList()) // show remove reasons list
                return false;

            RemoveReasonItem reason = (reasonsBox.SelectedItems[0] as SmoothListbox.ListItems.StandardListItem<RemoveReasonItem>).ItemData;
            reasonsBox.ResetSelection();

            string location = string.Empty;
            if (reason.LocationConfirmationRequired) // if remove location required
            {
                ForkLiftViewer.Instance.IsRemoveHawbMode = true;
                
           
                dropLoc = new CustomUtilities.ScanEnterPopup();
                 
                dropLoc.Title = "Scan Drop Location";
                dropLoc.TextLabel = "Return Location for HB " + hawbNo;
                if (DialogResult.OK == dropLoc.ShowDialog())
                {
                    var scanObj = CargoMatrix.Communication.BarcodeParser.Instance.Parse(dropLoc.ScannedText);
                    if (CMXBarcode.BarcodeTypes.Area == scanObj.BarcodeType || CMXBarcode.BarcodeTypes.Door == scanObj.BarcodeType || CMXBarcode.BarcodeTypes.ScreeningArea == scanObj.BarcodeType)
                        location = scanObj.Location;


 
                    if (scanObj.BarcodeType == CMXBarcode.BarcodeTypes.Area)
                    {
                        var scanItem = CargoMatrix.Communication.LoadConsol.Instance.GetScanDetail(ForkLiftViewer.getScanItem(scanObj), ForkLiftViewer.Instance.Mawb.TaskId, ForkLiftViewer.Instance.Mawb.Mot);
                        TransactionStatus dropStatus = CargoMatrix.Communication.LoadConsol.Instance.DropHousebillIntoLocation(hawbNo, ForkLiftViewer.Instance.Mawb.TaskId, scanItem.LocationId);
                        if (dropStatus.TransactionStatus1 == false)
                        {
                            ForkLiftViewer.Instance.IsRemoveHawbMode = false;
                            ShowFailMessage(dropStatus.TransactionError);
                            return false;
                        }
                        
                    }
                    else
                    {
                        ForkLiftViewer.Instance.IsRemoveHawbMode = false;
                        ShowFailMessage(LoadConsolResources.Text_InvalidLocationBarcode);
                        return false;
                    }
                }
                else
                {
                    ForkLiftViewer.Instance.IsRemoveHawbMode = false;
                    return false;
                }

                ForkLiftViewer.Instance.IsRemoveHawbMode = false;
            }

            var status = CargoMatrix.Communication.LoadConsol.Instance.RemoveHawbFromConsol(hawbId, superId, location, reason.ReasonId, scanType);
            if (!status.TransactionStatus1)
                ShowFailMessage(status.TransactionError);
            return status.TransactionStatus1;
        }

        private bool TakeOffAndRemoveHousebill(IHouseBillItem hawb, CargoMatrix.Communication.DTO.RemoveModes removeMode)
        {
            string msg = string.Format("Are you sure you want to Short/Takeoff and Remove {0}?", hawb.HousebillNumber);
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Remove", CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                return false;

            bool isShortage = removeMode == CargoMatrix.Communication.DTO.RemoveModes.Shortage;

            TakeoffHelper(isShortage, hawb);

            return RemoveHawbFromConsol(hawb.HousebillId, hawb.HousebillNumber, ScanTypes.Manual);

        }
        #endregion

        #region SHORT & TAKEOFF

        private void TakeOffHousebill(CargoMatrix.Communication.DTO.IHouseBillItem hawb, CargoMatrix.Communication.DTO.RemoveModes removeMode)
        {
            string msg = string.Format("Are you sure you want to Short/Takeoff {0}?", hawb.HousebillNumber);
            if (DialogResult.OK != CargoMatrix.UI.CMXMessageBox.Show(msg, "Confirm Short/Takeoff", CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                return;

            bool isShortage = removeMode == CargoMatrix.Communication.DTO.RemoveModes.Shortage;
            var status = TakeoffHelper(isShortage, hawb);
            if (status.TransactionStatus1)
            {
                CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_HousebillRemoved, hawb.Reference(), removeMode.ToString()), removeMode.ToString(), CargoMatrix.UI.CMXMessageBoxIcon.Success);
                LoadControl();
            }
            else
            {
                ShowFailMessage(status.TransactionError);
            }
        }

        private TransactionStatus TakeoffHelper(bool isShortage, IHouseBillItem hawb)
        {
            TransactionStatus status = new TransactionStatus() { TransactionStatus1 = false };
            if (isShortage) // if Shortage Report missing pieces
            {
                if (hawb.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
                {
                    if (DialogResult.OK != ReportShortPieces(hawb))
                        return status;
                }
                else
                {
                    CargoMatrix.Utilities.CountMessageBox CntMsg = new CargoMatrix.Utilities.CountMessageBox();
                    CntMsg.PieceCount = hawb.TotalPieces;
                    CntMsg.HeaderText = "Confirm count";
                    CntMsg.LabelDescription = "Enter number of short pieces";
                    CntMsg.LabelReference = hawb.Reference();

                    if (DialogResult.OK == CntMsg.ShowDialog())
                    {

                        var tStat = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInCount(hawb.HousebillId, CntMsg.PieceCount, mawb.TaskId, string.Empty);
                        if (tStat.TransactionStatus1 == false)
                            ShowFailMessage(tStat.TransactionError);
                    }
                }
            }

            return CargoMatrix.Communication.LoadConsol.Instance.ShortOrTakeOffHawb(hawb.HousebillId, mawb.MasterBillId, isShortage, ForkLiftViewer.Instance.Mawb.TaskId);

        }
        internal static bool PopulateReasonsList()
        {
            if (reasonsBox == null)
            {
                reasonsBox = new CargoMatrix.Utilities.MessageListBox();
                reasonsBox.HeaderText = "Select Reason";
                reasonsBox.HeaderText2 = "Select a reason from list";
                reasonsBox.OneTouchSelection = true;
                reasonsBox.MultiSelectListEnabled = false;
                foreach (var reason in CargoMatrix.Communication.LoadConsol.Instance.GetRemoveReasons())
                    reasonsBox.AddItem(new SmoothListbox.ListItems.StandardListItem<RemoveReasonItem>(reason.Reason, null, reason));
            }
            return DialogResult.OK == reasonsBox.ShowDialog();
        }

        #region ShortPieces
        CargoMatrix.Utilities.MessageListBox shortPiecesPopup;
        private DialogResult ReportShortPieces(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            if (shortPiecesPopup == null)
            {
                shortPiecesPopup = new CargoMatrix.Utilities.MessageListBox();
                Control[] actions = new Control[] {
                new SmoothListbox.ListItems.StandardListItem("SELECT ALL",null,3), 
                new SmoothListbox.ListItems.StandardListItem("SELECT RANGE",null,2),
                new SmoothListbox.ListItems.StandardListItem("SELECT PIECES",null,1) };

                shortPiecesPopup.AddItems(actions);
                shortPiecesPopup.MultiSelectListEnabled = false;
                shortPiecesPopup.ListItemClicked += new SmoothListbox.ListItemClickedHandler(shortPiecesPopup_ListItemClicked);
                shortPiecesPopup.CancelClick += delegate(object s, EventArgs e) { shortPiecesPopup.DialogResult = DialogResult.Cancel; };
            }
            shortPiecesPopup.HeaderText = hawb.Reference();
            shortPiecesPopup.HeaderText2 = "Select Action to Continue";
            selectedhwbId = hawb.HousebillId;
            return shortPiecesPopup.ShowDialog();

        }

        void shortPiecesPopup_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            var hawb = CargoMatrix.Communication.Inventory.Instance.GetHousebillInfo(selectedhwbId, mawb.TaskId);
            CustomUtilities.NumericPad numPad;

            var ids = from p in hawb.Pieces
                      select p.PieceId;
            switch ((listItem as SmoothListbox.ListItems.StandardListItem).ID)
            {
                case 3:// select all
                    foreach (int pid in ids)
                    {
                        var tStat = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInPiece(hawb.HousebillId, pid, mawb.TaskId, string.Empty);
                        if (tStat.TransactionStatus1 == false)
                            ShowFailMessage(tStat.TransactionError);
                    }
                    //shortPiecesPopup.Hide();
                    break;
                case 2:// select range
                    numPad = new CustomUtilities.NumericPad(ids.ToArray<int>(), true);
                    numPad.Title = "Select missing pieces";

                    if (DialogResult.OK == numPad.ShowDialog())
                    {
                        foreach (int pid in numPad.SelectedIDs)
                        {
                            var tStat = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInPiece(hawb.HousebillId, pid, mawb.TaskId, string.Empty);
                            if (tStat.TransactionStatus1 == false)
                                ShowFailMessage(tStat.TransactionError);
                        }
                        //shortPiecesPopup.Hide();
                    }
                    break;// select random
                case 1:
                    numPad = new CustomUtilities.NumericPad(ids.ToArray<int>(), false);
                    numPad.Title = "Select missing pieces";

                    if (DialogResult.OK == numPad.ShowDialog())
                    {
                        foreach (int pid in numPad.SelectedIDs)
                        {
                            var tStat = CargoMatrix.Communication.Inventory.Instance.ReportMissingPiecesInPiece(hawb.HousebillId, pid, mawb.TaskId, string.Empty);
                            if (tStat.TransactionStatus1 == false)
                                ShowFailMessage(tStat.TransactionError);
                        }
                        //shortPiecesPopup.Hide();
                    }
                    break;
            }
            shortPiecesPopup.OkEnabled = false;
            sender.Reset();
        }
        #endregion
        #endregion

        #region Switch Mode

        private bool SwitchMode(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            bool retValue = false;
            /// piece Mode -> Count
            if (hawb.ScanMode == CargoMatrix.Communication.DTO.ScanModes.Piece)
            {
                string msg = string.Format(LoadConsolResources.Text_ChangeModeToCount, hawb.Reference());
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Supervisor Override"))
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, mawb.TaskId, CargoMatrix.Communication.DTO.ScanModes.Count);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_ChangeCountConfirm, hawb.Reference()), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            // Count Mode -> Piece
            else
            {
                string msg = string.Format(LoadConsolResources.Text_ChangeModeToPiece, hawb.Reference());
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(msg, "Change Mode", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.No))
                {
                    if (!ChangeModeAllowed(hawb))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_SwitchNotAllowed, hawb.Reference()), "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Hand);
                        retValue = false;
                    }
                    else
                    {
                        var status = CargoMatrix.Communication.ScannerUtility.Instance.ChangeHawbMode(hawb.HousebillId, mawb.TaskId, CargoMatrix.Communication.DTO.ScanModes.Piece);
                        if (status.TransactionStatus1)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(string.Format(LoadConsolResources.Text_ChangePieceConfirm, hawb.Reference()), "Mode Changed", CargoMatrix.UI.CMXMessageBoxIcon.Success);
                            retValue = true;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show(status.TransactionError, "Mode Change Failed", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                            retValue = false;
                        }
                    }
                }
            }
            return retValue;
        }

        private bool ChangeModeAllowed(CargoMatrix.Communication.DTO.IHouseBillItem hawb)
        {
            if (ForkLiftViewer.Instance.ChangeHawbScanModeAllowed ||
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin ||
                DialogResult.OK == CustomUtilities.SupervisorAuthorizationMessageBox.Show(null, "Contact Supervisor"))
                return true;
            else
                return false;
        }

        #endregion
    }
}
