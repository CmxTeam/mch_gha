﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CargoMatrix.Communication.WSPieceScan;
using System.Reflection;
using CustomListItems;
using CustomUtilities;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.FreightTransfer
{
    public partial class BillsList : SmoothListbox.SmoothListbox
    {
        public object m_activeApp;
        private CargoMatrix.Viewer.HousebillViewer billViewer;
        private SearchPopup searchPopup;
        public BillsList()
        {
            InitializeComponent();
            //panel1.Location = panelHeader2.Location;
            //panelHeader2.Height = panel1.Height;
            //panelHeader2.Visible = false;
        }

        public override void LoadControl()
        {
            base.LoadControl();
            this.smoothListBoxMainList.labelEmpty.Visible = false;
            //this.smoothListBoxMainList.labelEmpty.Text = CargoMatrix_FreightTransfer.TextEmptyBillList;
            this.TitleText = CargoMatrix_FreightTransfer.TextSearchTitle;
            Cursor.Current = Cursors.Default;
            Application.DoEvents();
            PopulateSearch(false);

        }

        private void PopulateSearch(bool expanded)
        {
            searchPopup = new SearchPopup();
            if (searchPopup.ShowDialog(expanded) == DialogResult.OK)
            {
                SearchForRecords(searchPopup.Carrier, searchPopup.BillNumber, searchPopup.Direction, searchPopup.Status);
                //this.labelLine1.Text = searchPopup.Carrier + " " + searchPopup.BillNumber;
                string stat = searchPopup.Status != string.Empty ? searchPopup.Status : "All";
                string dir = searchPopup.Direction != string.Empty ? searchPopup.Direction : "All";
                //this.labelLine2.Text = string.Format("{0} - {1}, {2} - {3}", CargoMatrix_FreightTransfer.TextStatus, stat, CargoMatrix_FreightTransfer.TextDirection, dir);
                this.TitleText = CargoMatrix_FreightTransfer.TextSearchTitle + string.Format("({0}/{1})", stat, dir);
            }

            BarcodeEnabled = false;
        }
        
        private void SearchForRecords(string carier, string billNumber, string direction, string status)
        {
            this.smoothListBoxMainList.RemoveAll();
            AddMainListItem(new SmoothListbox.ListItems.StandardListItem("ADD NEW SHIPMENT", CargoMatrix_FreightTransfer.NewShipment, 1));
            CargoMatrix.Communication.DTO.TaskItem item = new CargoMatrix.Communication.DTO.TaskItem();
            item.carrier = "001";
            item.actualBill = billNumber;
            item.status = status;
            item.direction = 1;
            
            item.line2 = "PCS: 45 of 45, WT: 489KG";
            item.line3 = "LOC: Not Recovered ";
            MawbListItem billItem = new MawbListItem(item);

            billItem.title.Text = string.Format("ORD-{0}-{1}-JFK",carier, billNumber);
            billItem.itemPicture.Image = CargoMatrix_FreightTransfer.History;
            AddMainListItem(billItem);
            billItem.OnEnterClick += new EventHandler(billItem_OnEnterClick);
            billItem.OnViewerClick += new EventHandler(billItem_OnViewerClick);


            //MawbListItem billItem2 = new MawbListItem(item);

            //billItem.title.Text = string.Format("ORD-{0}-{1}-JFK", carier, billNumber);
            //billItem.itemPicture.Image = CargoMatrix_FreightTransfer.History;
            //AddMainListItem(billItem2);
            //billItem.OnEnterClick += new EventHandler(billItem_OnEnterClick);



            this.smoothListBoxMainList.labelEmpty.Visible = false;

        }

        void billItem_OnViewerClick(object sender, EventArgs e)
        {
            MawbListItem mawbItem = sender as MawbListItem;
            DisplayBillViewer("Carrier", mawbItem.Mawb.Reference());
        }

        void billItem_OnEnterClick(object sender, EventArgs e)
        {
            this.smoothListBoxMainList.Reset();
            NavigateToMawb(sender as Control);
        }
        public void DisplayBillViewer(string carrier, string masterbill)
        {

            if (billViewer != null)
                billViewer.Dispose();
            billViewer = new CargoMatrix.Viewer.HousebillViewer(carrier, masterbill);

            billViewer.Location = new Point(Left, CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);// CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight);
            billViewer.Size = new Size(Width, CargoMatrix.UI.CMXAnimationmanager.GetParent().Height - CargoMatrix.UI.CMXAnimationmanager.GetParent().HeaderHeight - CargoMatrix.UI.CMXAnimationmanager.GetParent().FooterHeight);

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(billViewer);
        }
        void BillsList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            // Search Mawb/Hawb 

            // ADD NEW SHIPMENT
            if (listItem is SmoothListbox.ListItems.StandardListItem)
            {
                if ((listItem as SmoothListbox.ListItems.StandardListItem).ID == 1)
                {
                    // ADD NEW SHIPMENT
                    smoothListBoxMainList.Reset();
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show(CargoMatrix_FreightTransfer.TextRecordNotFoundDescr, CargoMatrix_FreightTransfer.TextRecordNotFound, CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        ScanEnterPopup hawbPopup = new ScanEnterPopup();
                        hawbPopup.TextLabel = "Enter Shipment Number";
                        if (hawbPopup.ShowDialog() == DialogResult.OK)
                        {
                            string org,dest;
                            if (DialogResult.OK == CargoMatrix.Utilities.OriginDestinationMessageBox.Show(out org, out dest))
                            {
 
                            }
                        }
                    }
                    return;
                }
            }
            else
            {
                if (CargoMatrix.Communication.WebServiceManager.Instance().m_user.GroupID == CargoMatrix.Communication.WSPieceScan.UserTypes.Admin)
                    billItem_OnEnterClick(listItem, new EventArgs());
            }

        }

        private void NavigateToMawb(Control listItem)
        {
            try
            {
                string truck;
                string door;
                string origin;

                MawbListItem mawbItem = listItem as MawbListItem;


                MultiScanPopup recoverPopup = new MultiScanPopup();
                recoverPopup.Header = mawbItem.Mawb.Reference();
                recoverPopup.Label1 =  "TRUCK ID:";
                recoverPopup.Label2 = "DOOR NUMBER:";
                recoverPopup.Label3 = "CONFIRM ORIGIN:";
                recoverPopup.Text3 = "ORD";

                if (DialogResult.OK == recoverPopup.ShowDialog())
                {
                    truck = recoverPopup.Text1;
                    door = recoverPopup.Text2;
                    origin = recoverPopup.Text3;

                    Cursor.Current = Cursors.WaitCursor;
                    if ((m_activeApp != null) && (m_activeApp is CargoReceipt))
                    {
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                    }
                    else
                    {

                        m_activeApp = new CargoReceipt(new CargoMatrix.Communication.WSPieceScan.MasterBillListItem()
                                                            {
                                                                MasterBillNo = (listItem as MawbListItem).title.Text,
                                                                Line1 = string.Format("MB# CRJ-{0}-ORD", (listItem as MawbListItem).ActualBill),
                                                                Line2 = "PCS:45 of 45; WT:485KG",
                                                                Line3 = string.Format("LOC: {0} ({1})", door, truck)
                                                            });

                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);
                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                    }
                }

                //ScanEnterPopup truckPopup = new ScanEnterPopup();
                //truckPopup.TextLabel = "TRUCK ID";
                //if (DialogResult.OK == truckPopup.ShowDialog())
                //{
                //    truck = truckPopup.ScannedText;
                //    truckPopup.Reset();

                //    truckPopup.TextLabel = "DOOR NUMBER - " + truck;
                //    if (DialogResult.OK == truckPopup.ShowDialog())
                //    {
                //        door = truckPopup.ScannedText;
                //        truckPopup.Reset();
                //        truckPopup.TextLabel = string.Format("ORG ({0} - {1})", truck, door);
                //        truckPopup.Title = " Confirm Origin";
                //        truckPopup.ScannedText = "ORD";
                //        if (DialogResult.OK == truckPopup.ShowDialog())
                //        {

                            #region
                //            Cursor.Current = Cursors.WaitCursor;
                //            if ((m_activeApp != null) && (m_activeApp is CargoReceipt))
                //            {
                //                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                //            }
                //            else
                //            {

                //                m_activeApp = new CargoReceipt(new CargoMatrix.Communication.WSPieceScan.MasterBillListItem()
                //                                                    {
                //                                                        MasterBillNo = (listItem as MawbListItem).title.Text,
                //                                                        Line1 = string.Format("MB# CRJ-{0}-ORD", (listItem as MawbListItem).ActualBill),
                //                                                    Line2 = "PCS:45 of 45; WT:485KG", Line3=string.Format("LOC: {0} ({1})",door,truck)});

                //                (m_activeApp as UserControl).Location = new Point(Left, Top);
                //                (m_activeApp as UserControl).Size = new Size(Width, Height);


                //                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                //                //Assembly SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightTransfer.dll");
                //                //// Obtain a reference to a method known to exist in assembly.
                //                //Type myType;

                //                //myType = SampleAssembly.GetType("CargoMatrix.FreightTransfer.CargoReceipt");
                //                //if (myType != null)
                //                //{
                //                //    ConstructorInfo ctor = myType.GetConstructor(new Type[] { typeof(CargoMatrix.Communication.WSPieceScan.MasterBillListItem) });
                //                //    m_activeApp = ctor.Invoke(new object[] { (new CargoMatrix.Communication.WSPieceScan.MasterBillListItem()
                //                //                                    {
                //                //                                        MasterBillNo = (listItem as CustomListItems.BillItem).title.Text,Line1 = "MB# CRJ-001-19228736-ORD",
                //                //                                    Line2 = "PCS:45 of 45; WT:485KG", Line3=string.Format("LOC: {0} ({1})",door,truck)}  )});

                //                //    (m_activeApp as UserControl).Location = new Point(Left, Top);
                //                //    (m_activeApp as UserControl).Size = new Size(Width, Height);


                //                //    CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);
                //                //}
                //                //else
                //                //{
                //                //    CargoMatrix.UI.CMXMessageBox.Show("Unable to load Cargo Receipt Module", "Error!" + " (60007)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                //                //}
                //            }
                            #endregion
                //        }

                //    }
                //}


            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 60009);

            }
        }

        private void BillsList_LoadOptionsMenu(object sender, EventArgs e)
        {
            AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER));
        }

        private void BillsList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            CustomListItems.OptionsListITem item = listItem as CustomListItems.OptionsListITem;

            switch (item.ID)
            {
                case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                    PopulateSearch(true);
                    break;
            }
        }
    }
}
