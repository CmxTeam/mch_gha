﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.DamageCapture
{
    public class UpdateShipmentConditionsEventArgs : EventArgs
    {   
        public bool Result
        {
            get;
            set;
        }

        public int HouseBillId
        {
            get;
            set;
        }

        public ScanModes ScanModes
        {
            get;
            set;
        }

        public ShipmentCondition[] ShipmentConditions
        {
            get;
            set;
        }

        public int Slac
        {
            get;
            set;
        }
    }
}
