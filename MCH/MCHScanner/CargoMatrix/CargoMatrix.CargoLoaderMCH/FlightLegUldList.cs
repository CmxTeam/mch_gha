﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CustomListItems;
using System.Text;

namespace CargoMatrix.CargoLoader
{
    public partial class FlightLegUldList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
   

        //private CargoLoaderFlightLeg flightLegDetailsItem;
        private CargoLoaderFlight flightDetailsItem;
        protected MessageListBox MawbOptionsList;
        public object m_activeApp;

        public FlightLegUldList(CargoLoaderFlight flightDetailsItem)
        {
            
            //this.flightLegDetailsItem = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlightLeg(flightLegDetailsItem.LegId);

            this.flightDetailsItem = flightDetailsItem;
      
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(FlightLegList_BarcodeReadNotify);
            BarcodeEnabled = false;
 
            this.LoadOptionsMenu += new EventHandler(FlightLegUldList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(FlightLegUldList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FlightLegUldList_ListItemClicked);
            this.panelCutoff.Paint += new System.Windows.Forms.PaintEventHandler(panelCutoff_Paint);
        }


        void panelCutoff_Paint(object sender, PaintEventArgs e)
        {
        
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                Rectangle topRect = new Rectangle(0, 0, panelCutoff.Width, panelCutoff.Height / 2);
                Rectangle botRect = new Rectangle(0, panelCutoff.Height / 2, panelCutoff.Width, panelCutoff.Height / 2);
                StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };


                if (flightDetailsItem.CutOfTime != null)
                {
                    e.Graphics.DrawString(string.Format("{0:ddd}", flightDetailsItem.CutOfTime), font, new SolidBrush(Color.White), topRect, sf);
                    e.Graphics.DrawString(string.Format("{0:HH:mm}", flightDetailsItem.CutOfTime), font, new SolidBrush(Color.Black), botRect, sf);

                }
                else
                {
                    e.Graphics.DrawString(string.Format("{0:ddd}", flightDetailsItem.ETD), font, new SolidBrush(Color.White), topRect, sf);
                    e.Graphics.DrawString(string.Format("{0:HH:mm}", flightDetailsItem.ETD), font, new SolidBrush(Color.Black), botRect, sf);

                }
      
            }
        }




        //void RefreshFlight()
        //{
        //    CargoLoaderFlight tempFlight = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlight(this.flightDetailsItem.FlightManifestId, this.flightDetailsItem.TaskId);
        //    if (tempFlight != null)
        //    {
        //        this.flightDetailsItem = tempFlight;
        //    }

        //}
        //void RefreshFlightLegUlds()
        //{
        //    CargoLoaderFlightLeg tempFlightLeg = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlightLeg(this.flightDetailsItem.FlightManifestId);
        //    if (tempFlightLeg != null)
        //    {
        //        this.flightLegDetailsItem = tempFlightLeg;
        //    }
 
        //}

        void FlightLegUldList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:

                        //RefreshFlight();
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FINALIZETASK:

                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure want to finalize this task?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                            CargoMatrix.Communication.CargoLoaderMCHService.Instance.FinalizeCargoLoader(flightDetailsItem.TaskId, flightDetailsItem.FlightManifestId);
                            this.flightDetailsItem = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlight(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);
                            LoadControl();
                        }
                     
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

 

        void FlightLegUldList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FINALIZETASK));
        }
        void buttonNext_Click(object sender, System.EventArgs e)
        {
 
            Cursor.Current = Cursors.WaitCursor;


            CargoLoaderFlightLegUld[] ulds = CargoMatrix.Communication.CargoLoaderMCHService.Instance.AvailableULDsForBuild(flightDetailsItem.FlightManifestId);
            if (ulds.Length == 0)
            {
                Cursor.Current = Cursors.Default;
                CargoMatrix.UI.CMXMessageBox.Show("No ULDs available.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
            }
            else
            {
                CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new ShipmentList(flightDetailsItem));
                Cursor.Current = Cursors.Default; 
            }

          

        }
        void AddNewUld()
        {
            UldType selectedUldType = GetUldType();
            string uldNumber = string.Empty;
            string uldPrefix = string.Empty;
            //int userid = CargoMatrix.Communication.WebServiceManager.UserID();

            if (selectedUldType != null)
            {
                string reference = string.Format("{0}{1}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber  );

                if (selectedUldType.Code.ToUpper() == "LOOSE")
                {
 
                    if (!CargoMatrix.Communication.CargoLoaderMCHService.Instance.AddCargoLoaderFlightLegUld(selectedUldType.Id, "", "", this.flightDetailsItem.TaskId))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to add Uld.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                    LoadControl();
                }
                else
                {
                    if (DialogResult.OK == UldEditMessageBox.Show(this.flightDetailsItem.FlightManifestId, reference,selectedUldType.Code ,ref uldPrefix, ref uldNumber))
                    {
                        if (!CargoMatrix.Communication.CargoLoaderMCHService.Instance.AddCargoLoaderFlightLegUld(selectedUldType.Id, uldPrefix, uldNumber, this.flightDetailsItem.TaskId))
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to add Uld.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        }
                        LoadControl();
                    }
                }
   

            }
        }


        UldType GetUldType()
        {
            UldType[] uldTypes = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetUldTypeList();
            if (uldTypes == null)
                return null;

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Uld Types";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoLoader";


            actPopup.RemoveAllItems();

            for (int i = 0; i < uldTypes.Length; i++)
            {
                actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(uldTypes[i].Code, null, i+1));
 
            }

        



            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {
                    for (int i = 0; i < uldTypes.Length; i++)
                    {
                        if (actPopup.SelectedItems[0].Name == uldTypes[i].Code)
                        {
                            return uldTypes[i];
                        }
                    }
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }



        }


        private string GetLocationLine(string[] locations)
        {
            StringBuilder sb = new StringBuilder("");
            foreach (string l in locations)
            {
                if (sb.ToString() == string.Empty)
                {
                    sb.Append(l);
                }
                else
                {
                    sb.Append(", " + l);
                }
            }
            return sb.ToString();
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;

            this.flightDetailsItem = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlight(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);
            if (flightDetailsItem.Progress >= 100 && flightDetailsItem.Status != TaskStatuses.Completed)
            {
                if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("All pieces were loaded. Do you want to finalize this task?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                {
                    CargoMatrix.Communication.CargoLoaderMCHService.Instance.FinalizeCargoLoader(flightDetailsItem.TaskId, flightDetailsItem.FlightManifestId);
                    this.flightDetailsItem = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlight(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId);
                }
            }
     
            this.label1.Text =string.Format("{0}{1} {2:ddMMM}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber,  this.flightDetailsItem.ETD).ToUpper();
            this.label2.Text = string.Format("Awbs: {0} STC: {1} Pcs: {2}", this.flightDetailsItem.TotalAwbs, this.flightDetailsItem.TotalSTC, this.flightDetailsItem.TotalPcs);
            this.label3.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.LoadedUlds, this.flightDetailsItem.TotalUlds);
            this.label4.Text = string.Format("{0:0}%", this.flightDetailsItem.Progress);
            this.label5.Text = string.Format("Loc: {0} Dest: {1}", this.flightDetailsItem.Location, GetLocationLine(this.flightDetailsItem.Destinations)); 

            //switch (this.flightDetailsItem.Status)
            //{
            //    case TaskStatuses.Open:
            //    case TaskStatuses.Pending:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //    case TaskStatuses.InProgress:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
            //        break;
            //    case TaskStatuses.Completed:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
            //        break;
            //    default:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //}


            object O = Resources.Airlines.GetImage(flightDetailsItem.CarrierCode);
            if (O != null)
            {
                Image logo = (Image)O;
                itemPicture.Image = logo;
            }
            else
            {
                itemPicture.Image = Resources.Skin.Airplane24x24;
            }


            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;

           

            CargoLoaderFlightLegUld[] ulds = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlightUlds( flightDetailsItem.FlightManifestId);
    

            if (this.flightDetailsItem.Status == TaskStatuses.Completed)
            {
                this.buttonNext.Visible = false;
                if (ulds != null)
                {
                    var rItems = from i in ulds
                                 select InitializeFinalItem(i);
                    smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                }
            }
            else
            {
                this.buttonNext.Visible = true;
                smoothListBoxMainList.AddItemToFront(new StaticListItem("Add new ULD", Resources.Icons.Notepad_Add));
                if (ulds != null)
                {
                    var rItems = from i in ulds
                                 select InitializeItem(i);
                    smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
                }
            }


            if (this.flightDetailsItem.Status != TaskStatuses.Completed)
            {
                if (this.flightDetailsItem.Location == string.Empty || this.flightDetailsItem.Location == "N/A")
                {
                    DoFlightLocation();
                }
            }



            this.TitleText = string.Format("CargoLoader - Ulds ({0})", smoothListBoxMainList.VisibleItemsCount);
            Cursor.Current = Cursors.Default;







        }



        protected ICustomRenderItem InitializeItem(CargoLoaderFlightLegUld item)
        {
            FlightLegUldCargoItem tempItem = new FlightLegUldCargoItem(item);
            tempItem.ButtonDeleteClick += new EventHandler(FlightLegUldList_OnDeleteClick);
            tempItem.ButtonEditClick += new EventHandler(FlightLegUldList_OnEditClick);
            return tempItem;
        }

        protected ICustomRenderItem InitializeFinalItem(CargoLoaderFlightLegUld item)
        {

            FlightLegUldFinalCargoItem tempItem = new FlightLegUldFinalCargoItem(item);
            tempItem.ButtonWeightClick += new EventHandler(FlightLegUldList_OnWeightClick);
            tempItem.ButtonLocationClick += new EventHandler(FlightLegUldList_OnLocationClick);
            return tempItem;
        }

        protected virtual void FlightLegUldList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            if (listItem is StaticListItem)
            {
                AddNewUld();
            }
            else if (listItem is FlightLegUldFinalCargoItem)
            {
                FlightLegUldFinalCargoItem itemControl = (FlightLegUldFinalCargoItem)listItem;
                CargoLoaderFlightLegUld item = (CargoLoaderFlightLegUld)itemControl.ItemData;
                if (!item.IsBup)
                {
                    BarcodeEnabled = false;
                    DialogResult dr = UldViewer.Instance.ShowDialog(flightDetailsItem, item);
                    LoadControl();
                }

            }
            else
            {
                FlightLegUldCargoItem itemControl = (FlightLegUldCargoItem)listItem;
                CargoLoaderFlightLegUld item = (CargoLoaderFlightLegUld)itemControl.ItemData;
                if (item.UldSerialNo != string.Empty || item.UldType.ToUpper() == "LOOSE")
                {
                    if (!item.IsBup)
                    {
                        BarcodeEnabled = false;
                        DialogResult dr = UldViewer.Instance.ShowDialog(flightDetailsItem, item);
                        LoadControl();
                    }
                }
                else
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Uld number was not setup.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                }

            }
        }

        void FlightLegUldList_OnDeleteClick(object sender, EventArgs e)
        {
 
 
            CargoLoaderFlightLegUld uld = (sender as FlightLegUldCargoItem).ItemData;

            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to delete selected ULD?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                //int userid = CargoMatrix.Communication.WebServiceManager.UserID();
                if (!CargoMatrix.Communication.CargoLoaderMCHService.Instance.DeleteCargoLoaderFlightLegUld(uld.UldId))
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Unable to delete Uld.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                }
                LoadControl();
            }


        }

        void FlightLegUldList_OnWeightClick(object sender, EventArgs e)
        {
            CargoLoaderFlightLegUld uld = (sender as FlightLegUldFinalCargoItem).ItemData;
            barcode.StopRead();
            string reference = string.Format("{0}{1}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
            decimal weight  =uld.Weight;
            decimal tareweight = uld.TareWeight;
            if (DialogResult.OK == UldWeightMessageBox.Show(this.flightDetailsItem.FlightManifestId, reference, uld.UldType,uld.UldPrefix, uld.UldSerialNo, ref weight, ref tareweight))
            {
                CargoMatrix.Communication.CargoLoaderMCHService.Instance.UpdateUldWeight(uld.UldId, weight);
                CargoMatrix.Communication.CargoLoaderMCHService.Instance.UpdateUldTareWeight(uld.UldId, tareweight);
                LoadControl();
            }
            else
            {
                barcode.StartRead();
            }
            

        }


        void DoFlightLocation()
        {

            string reference = string.Format("{0}{1}",this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
            string enteredLocation = string.Empty;
            bool isFPC = false;
            barcode.StopRead();
            if (DoLocation(out enteredLocation, out isFPC, reference, true))
            {

                long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                if (locationId == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }
                if (CargoMatrix.Communication.CargoLoaderMCHService.Instance.UpdateFlightBuildLocation(flightDetailsItem.FlightManifestId,flightDetailsItem.TaskId, (int)locationId))
                {

                    LoadControl();
                    if (isFPC)
                    {
                        ShowFreightPhotoCapture(reference, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), flightDetailsItem.Destinations[0], 1, "CargoLoader");
                    }

                }
 
            }
            else
            {
                barcode.StartRead();
            }
        
        }

        void FlightLegUldList_OnLocationClick(object sender, EventArgs e)
        {

            CargoLoaderFlightLegUld uld = (sender as FlightLegUldFinalCargoItem).ItemData;

            string reference = string.Format("{0}{1}", uld.UldPrefix, uld.UldSerialNo);
            string enteredLocation = string.Empty;
            bool isFPC = false;
            barcode.StopRead();
            if (DoLocation(out enteredLocation, out isFPC, reference, true))
            {

                long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                if (locationId == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }
                if(CargoMatrix.Communication.CargoLoaderMCHService.Instance.UpdateFlightLegUldLocation(uld.UldId, (int)locationId, flightDetailsItem.TaskId))
                {
                
                      LoadControl();
                         if (isFPC)
                        {
                            ShowFreightPhotoCapture(reference, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), flightDetailsItem.Destinations[0], 1, "CargoLoader");
                        }

                }
              
 


            }
            else
            {
                barcode.StartRead();
            }


        }


        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }


        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        { 


            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + hawbNo + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }
    

        void FlightLegUldList_OnEditClick(object sender, EventArgs e)
        {
            CargoLoaderFlightLegUld uld = (sender as FlightLegUldCargoItem).ItemData;
            string uldNumber= uld.UldSerialNo;
            string uldPrefix = uld.UldPrefix;
                string reference = string.Format("{0}{1}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
                if (DialogResult.OK == UldEditMessageBox.Show(this.flightDetailsItem.FlightManifestId, reference, uld.UldType,ref uldPrefix, ref uldNumber))
                {

                    if (!CargoMatrix.Communication.CargoLoaderMCHService.Instance.UpdateCargoLoaderFlightLegUld(uld.UldId, uldNumber, uldPrefix, this.flightDetailsItem.TaskId))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to edit Uld.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }
                    LoadControl();
                }
         
        }

  

        //private bool ProceedWithFlightLegUld(CargoLoaderFlightLeg tempFlightLeg)
        //{
         
        //    if (tempFlightLeg == null)
        //        return false;

        //    //CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightReceiver(tempFlight));
        //    MessageBox.Show("go to uld");

        //    Cursor.Current = Cursors.Default;
        //    return true;
        //}

        void FlightLegList_BarcodeReadNotify(string barcodeData)
        {

 
            Cursor.Current = Cursors.Default;

            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
            {
 
              
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to set this location as build location?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(scanItem.Location);
                        if (locationId == 0)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            BarcodeEnabled = true;
                            return;
                        }

                        if (CargoMatrix.Communication.CargoLoaderMCHService.Instance.UpdateFlightBuildLocation(flightDetailsItem.FlightManifestId, flightDetailsItem.TaskId, (int)locationId))
                        {

                            LoadControl();
                            return;
                        }
                        else
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Unable to update location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        }
      
                    }
            

            }



       

            BarcodeEnabled = true;
        }
    }

}
