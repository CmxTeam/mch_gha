﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CustomListItems;

namespace CargoMatrix.CargoLoader
{
    public partial class ForkLiftList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {

        public object m_activeApp;

        private CargoLoaderFlightLeg flightLegDetailsItem;
        private CargoLoaderFlight flightDetailsItem;
        protected MessageListBox MawbOptionsList;

        public ForkLiftList(CargoLoaderFlight flightDetailsItem, CargoLoaderFlightLeg flightLegDetailsItem)
        {

            this.flightLegDetailsItem = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlightLeg(flightLegDetailsItem.LegId);

            this.flightDetailsItem = flightDetailsItem;
      
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(ShipmentList_BarcodeReadNotify);
            BarcodeEnabled = false;

            this.LoadOptionsMenu += new EventHandler(ShipmentList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(ShipmentList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(ShipmentList_ListItemClicked);
            this.panelCutoff.Paint += new System.Windows.Forms.PaintEventHandler(panelCutoff_Paint);
        }


        void panelCutoff_Paint(object sender, PaintEventArgs e)
        {
         
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                Rectangle topRect = new Rectangle(0, 0, panelCutoff.Width, panelCutoff.Height / 2);
                Rectangle botRect = new Rectangle(0, panelCutoff.Height / 2, panelCutoff.Width, panelCutoff.Height / 2);
                StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
                if (flightDetailsItem.CutOfTime != null)
                {
                    e.Graphics.DrawString(string.Format("{0:ddd}", flightDetailsItem.CutOfTime), font, new SolidBrush(Color.White), topRect, sf);
                    e.Graphics.DrawString(string.Format("{0:HH:mm}", flightDetailsItem.CutOfTime), font, new SolidBrush(Color.Black), botRect, sf);

                }
                else
                {
                    e.Graphics.DrawString(string.Format("{0:ddd}", flightDetailsItem.ETD), font, new SolidBrush(Color.White), topRect, sf);
                    e.Graphics.DrawString(string.Format("{0:HH:mm}", flightDetailsItem.ETD), font, new SolidBrush(Color.Black), botRect, sf);

                }
    
            }
        }

  
        void RefreshFlightLegUlds()
        {
            CargoLoaderFlightLeg tempFlightLeg = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlightLeg(this.flightDetailsItem.FlightManifestId);
            if (tempFlightLeg != null)
            {
                this.flightLegDetailsItem = tempFlightLeg;
            }
 
        }

        void ShipmentList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:

                        RefreshFlightLegUlds();
                        LoadControl();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }



        void ShipmentList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
        }


        void buttonForkLift_Click(object sender, System.EventArgs e)
        {
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop all pieces into a location?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                BarcodeEnabled = false;
         
                string enteredLocation = string.Empty;
                bool isFPC = false;
                if (DoLocation(out enteredLocation, out isFPC, "Forklift", true))
                {
                    long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                    if (locationId == 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }
                    LoadControl();
                }
            }
        }
 

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;

            this.buttonForkLiftCounter.Value = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkLiftCount(flightDetailsItem.TaskId);
     
            this.label1.Text =string.Format("{0}{1}-{2} {3:ddMMM}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber,this.flightLegDetailsItem.Destination, this.flightDetailsItem.ETD).ToUpper();
            this.label2.Text = string.Format("Awbs: {0} STC: {1} Pcs: {2}", this.flightDetailsItem.TotalAwbs, this.flightDetailsItem.TotalSTC, this.flightDetailsItem.TotalPcs);
            this.label3.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.LoadedUlds, this.flightDetailsItem.TotalUlds);
            this.label4.Text = string.Format("{0:0}%", this.flightDetailsItem.Progress);
            this.label5.Text = string.Format("Loc: {0}", this.flightLegDetailsItem.Location); 

            //switch (this.flightDetailsItem.Status)
            //{
            //    case TaskStatuses.Open:
            //    case TaskStatuses.Pending:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //    case TaskStatuses.InProgress:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
            //        break;
            //    case TaskStatuses.Completed:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
            //        break;
            //    default:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //}


            object O = Resources.Airlines.GetImage(flightDetailsItem.CarrierCode);
            if (O != null)
            {
                Image logo = (Image)O;
                itemPicture.Image = logo;
            }
            else
            {
                itemPicture.Image = Resources.Skin.Airplane24x24;
            }


            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;


           // CargoLoaderShipment[] items = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderShipments(this.flightLegDetailsItem.LegId, this.flightDetailsItem.FlightManifestId);

             LoadForkliftViewItem[] items=  CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkliftView(this.flightDetailsItem.TaskId);

            if (items != null)
            {
                var rItems = from i in items
                             select InitializeItem(i);
                smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());
            }

            this.TitleText = string.Format("Forklift ({0})", smoothListBoxMainList.VisibleItemsCount);
            Cursor.Current = Cursors.Default;
        }



        protected ICustomRenderItem InitializeItem(LoadForkliftViewItem item)
        {
            ForkLiftCargoItem tempItem = new ForkLiftCargoItem(item);
            tempItem.ButtonClick += new EventHandler(ForkliftDelete_OnListClick);
            return tempItem;
        }

        protected virtual void ShipmentList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            ForkLiftCargoItem itemControl = (ForkLiftCargoItem)listItem;
            LoadForkliftViewItem item = (LoadForkliftViewItem)itemControl.ItemData;
            string enteredLocation = string.Empty;
            bool isFPC = false;
            if (DoLocation(out enteredLocation, out isFPC, item.AwbNumber, true))
            {
                long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                if (locationId == 0)
                {
                    CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    return;
                }
                LoadControl();
            }
            
        }



        void ForkliftDelete_OnListClick(object sender, EventArgs e)
        {

            ForkLiftCargoItem itemControl = (sender as ForkLiftCargoItem);
            LoadForkliftViewItem item = (LoadForkliftViewItem)itemControl.ItemData;

            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to remove this item from your forklift?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {

                LoadControl();
            }
        }

        void ShipmentList_BarcodeReadNotify(string barcodeData)
        {

            MessageBox.Show(barcodeData);

            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }


        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {
             

            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + hawbNo + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }

 

    }

}
