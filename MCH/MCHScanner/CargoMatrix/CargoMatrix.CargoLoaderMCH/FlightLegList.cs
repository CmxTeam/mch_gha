﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CargoMatrix.UI;
using CMXExtensions;
using CMXBarcode;
using CargoMatrix.Utilities;
using SmoothListbox.ListItems;
using CustomUtilities;
using System.Linq;
using SmoothListbox;
using System.Reflection;
using CargoMatrix.Communication.WSCargoLoaderMCHService;

namespace CargoMatrix.CargoLoader
{
    public partial class FlightLegList : CargoMatrix.CargoUtilities.SmoothListBoxOptions//SmoothListbox.SmoothListBoxAsync<CargoMatrix.Communication.DTO.IMasterBillItem>
    {
        public object m_activeApp;

  
        private CargoLoaderFlight flightDetailsItem;
        protected string filter = FlightFilters.NOT_COMPLETED;
        protected string sort = FlightSorts.CARRIER;
         
        protected CargoMatrix.UI.CMXTextBox searchBox;
        CustomListItems.OptionsListITem filterOption;
        //protected MessageListBox MawbOptionsList;
         
        public FlightLegList(CargoLoaderFlight flightDetailsItem)
        {
         
            this.flightDetailsItem = flightDetailsItem;
      
            InitializeComponent();
            this.BarcodeReadNotify += new BarcodeReadNotifyHandler(FlightLegList_BarcodeReadNotify);
            BarcodeEnabled = false;
            searchBox.WatermarkText = "Enter Filter Text";
            this.LoadOptionsMenu += new EventHandler(FlightLegList_LoadOptionsMenu);
            this.MenuItemClicked += new SmoothListbox.ListItemClickedHandler(FlightLegList_MenuItemClicked);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(FlightLegList_ListItemClicked);
            this.panelCutoff.Paint += new System.Windows.Forms.PaintEventHandler(panelCutoff_Paint);
        }


        void panelCutoff_Paint(object sender, PaintEventArgs e)
        {
            
            using (Font font = new Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
            {
                Rectangle topRect = new Rectangle(0, 0, panelCutoff.Width, panelCutoff.Height / 2);
                Rectangle botRect = new Rectangle(0, panelCutoff.Height / 2, panelCutoff.Width, panelCutoff.Height / 2);
                StringFormat sf = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
                if (flightDetailsItem.CutOfTime != null)
                {
                    e.Graphics.DrawString(string.Format("{0:ddd}", flightDetailsItem.CutOfTime), font, new SolidBrush(Color.White), topRect, sf);
                    e.Graphics.DrawString(string.Format("{0:HH:mm}", flightDetailsItem.CutOfTime), font, new SolidBrush(Color.Black), botRect, sf);

                }
                else
                {
                    e.Graphics.DrawString(string.Format("{0:ddd}", flightDetailsItem.ETD), font, new SolidBrush(Color.White), topRect, sf);
                    e.Graphics.DrawString(string.Format("{0:HH:mm}", flightDetailsItem.ETD), font, new SolidBrush(Color.Black), botRect, sf);

                }
    
            }
        }

        private void ShowAlerts(string reference, long legId)
        {
            Alert[] alerts = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetLegAlerts(legId);
            foreach (Alert item in alerts)
            {
                CargoMatrix.UI.CMXMessageBox.Show(item.Message + Environment.NewLine + item.SetBy + Environment.NewLine + item.Date.ToString("MM/dd HH:mm"), "Alert: " + reference, CargoMatrix.UI.CMXMessageBoxIcon.Hand);
            }
        }


        protected virtual void FlightLegList_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

            BarcodeEnabled = false;
            FlightLegCargoItem tempFlightLegCargoItem = (FlightLegCargoItem)listItem;
            CargoLoaderFlightLeg tempFlightLegItem = (CargoLoaderFlightLeg)tempFlightLegCargoItem.ItemData;
            string reference =string.Format("{0}{1} {2}", flightDetailsItem.CarrierCode, flightDetailsItem.FlightNumber,  tempFlightLegItem.Destination);
            ShowAlerts(reference, tempFlightLegItem.LegId);

            if (tempFlightLegItem.Location == "N/A" || tempFlightLegItem.Location == string.Empty)
            {
                string enteredLocation = string.Empty;
                bool isFPC = false;
                if (DoLocation(out enteredLocation, out isFPC, reference, true))
                {
                    long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                    if (locationId == 0)
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }

                    if (CargoMatrix.Communication.CargoLoaderMCHService.Instance.UpdateFlightLegLocation(tempFlightLegItem.LegId, locationId))
                    {
                        if (isFPC)
                        {
                            ShowFreightPhotoCapture(reference, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), flightDetailsItem.Destinations[0], 1, "CargoLoader");
                        }
               
                        ProceedWithFlightLeg(tempFlightLegItem);
                        return;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to update location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }

      
                }
                else
                {
                    LoadControl();
                }
            }
            else
            {
                ProceedWithFlightLeg(tempFlightLegItem);
                return;
            }
 
        }


        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {
 
            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

              CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + hawbNo + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }

 

        void RefreshFlight()
        {
            CargoLoaderFlight tempFlight = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlight(this.flightDetailsItem.FlightManifestId, this.flightDetailsItem.TaskId);
            if(tempFlight!=null)
            {
            this.flightDetailsItem  = tempFlight;
            }
        }
        void FlightLegList_MenuItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {
            BarcodeEnabled = false;
            this.Focus();
            if (listItem is CustomListItems.OptionsListITem)
                switch ((listItem as CustomListItems.OptionsListITem).ID)
                {
                    case CustomListItems.OptionsListITem.OptionItemID.REFRESH:

                        RefreshFlight();
                        LoadControl();
                        break;
                    case CustomListItems.OptionsListITem.OptionItemID.FILTER:
                        ShowFilterPopup();
                        break;
                }
            BarcodeEnabled = false;
            BarcodeEnabled = true;
        }

        private bool ShowFilterPopup()
        {
            FilterPopup fp = new FilterPopup(FilteringMode.Uld);
            fp.Filter = filter;
            fp.Sort = sort;
            if (DialogResult.OK == fp.ShowDialog())
            {
                filter = fp.Filter;
                sort = fp.Sort;
                if (filterOption != null)
                    filterOption.DescriptionLine = filter;
                LoadControl();
                return true;
            }
            return false;
        }

        void FlightLegList_LoadOptionsMenu(object sender, EventArgs e)
        {
            this.AddOptionsListItem(new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.REFRESH));
            filterOption = new CustomListItems.OptionsListITem(CustomListItems.OptionsListITem.OptionItemID.FILTER) { DescriptionLine = filter  };
            this.AddOptionsListItem(filterOption);
        }

        public override void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            BarcodeEnabled = false;
            BarcodeEnabled = true;

 

            this.label1.Text =string.Format("{0}{1} {2:ddMMM}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber, this.flightDetailsItem.ETD).ToUpper();
            this.label2.Text = string.Format("Awbs: {0} STC: {1} Pcs: {2}", this.flightDetailsItem.TotalAwbs, this.flightDetailsItem.TotalSTC, this.flightDetailsItem.TotalPcs);
            this.label3.Text = string.Format("ULDs: {0} of {1}", this.flightDetailsItem.LoadedUlds, this.flightDetailsItem.TotalUlds);
            this.label4.Text = string.Format("{0:0}%", this.flightDetailsItem.Progress);

            //switch (this.flightDetailsItem.Status)
            //{
            //    case TaskStatuses.Open:
            //    case TaskStatuses.Pending:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //    case TaskStatuses.InProgress:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.status_history;
            //        break;
            //    case TaskStatuses.Completed:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard_Check;
            //        break;
            //    default:
            //        itemPicture.Image = CargoMatrix.Resources.Skin.Clipboard;
            //        break;
            //}


            object O = Resources.Airlines.GetImage(flightDetailsItem.CarrierCode);
            if (O != null)
            {
                Image logo = (Image)O;
                itemPicture.Image = logo;
            }
            else
            {
                itemPicture.Image = Resources.Skin.Airplane24x24;
            }




            this.searchBox.Text = string.Empty;
            smoothListBoxMainList.RemoveAll();
            this.Refresh();
            Cursor.Current = Cursors.WaitCursor;


            RecoverStatuses status = RecoverStatuses.All;
            switch (filter)
            {
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_STARTED:
                    status = RecoverStatuses.Pending;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.IN_PROGRESS:
                    status = RecoverStatuses.InProgress;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.COMPLETED:
                    status = RecoverStatuses.Complete;
                    break;
                case CargoMatrix.Communication.DTO.FlightFilters.NOT_COMPLETED:
                    status = RecoverStatuses.NotCompleted;
                    break;
                default:
                    status = RecoverStatuses.All;
                    break;
            }
 
 
                     CargoLoaderFlightLeg[] tempFlightLegs = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlightLegs(this.flightDetailsItem.FlightManifestId);
                     if (tempFlightLegs != null)
                     {
                         var rItems = from i in tempFlightLegs
                                      select InitializeItem(i);
                         smoothListBoxMainList.AddItemsR(rItems.ToArray<ICustomRenderItem>());


                     }


                     this.TitleText = string.Format("Flight Legs - {0} ({1})", filter, smoothListBoxMainList.VisibleItemsCount);
            Cursor.Current = Cursors.Default;
        }
 
        protected virtual void searchBox_TextChanged(object sender, EventArgs e)
        {
            smoothListBoxMainList.Filter(searchBox.Text);
            this.TitleText = string.Format("Flight Legs - {0} ({1})", filter, smoothListBoxMainList.VisibleItemsCount);
        }

        protected ICustomRenderItem InitializeItem(CargoLoaderFlightLeg item)
        {

            FlightLegCargoItem tempFlightLeg = new FlightLegCargoItem(item);
            //tempFlightLeg.OnEnterClick += new EventHandler(FlightLegList_Enter_Click);
            tempFlightLeg.ButtonClick += new EventHandler(FlightLegList_OnMoreClick);
      
            return tempFlightLeg;
        }

        void FlightLegList_OnMoreClick(object sender, EventArgs e)
        {
            MessageBox.Show("FlightLegList_OnMoreClick");
            //Cursor.Current = Cursors.WaitCursor;
            //var uld = (sender as UldCargoItem).ItemData;
            //Cursor.Current = Cursors.Default;
            //MessageBox.Show((sender as UldCargoItem).ItemData.UldSerialNo);
        }




        //protected virtual void FlightLegList_Enter_Click(object sender, EventArgs e)
        //{
        //    MessageBox.Show("FlightLegList_Enter_Click");
        //    //Cursor.Current = Cursors.WaitCursor;
        //    //FlightItem flight = (sender as FlightCargoItem).ItemData;
        //    //ProceedWithUld(flight);
        //    //Cursor.Current = Cursors.Default;
        //}

        private bool ProceedWithFlightLeg(CargoLoaderFlightLeg tempFlightLeg)
        {
       
            if (tempFlightLeg == null)
                return false;

            CargoMatrix.UI.CMXAnimationmanager.DisplayForm(new FlightLegUldList(flightDetailsItem));
 

            Cursor.Current = Cursors.Default;
            return true;
        }

        void FlightLegList_BarcodeReadNotify(string barcodeData)
        {

            MessageBox.Show(barcodeData);

            Cursor.Current = Cursors.Default;
            BarcodeEnabled = true;
        }
    }

}
