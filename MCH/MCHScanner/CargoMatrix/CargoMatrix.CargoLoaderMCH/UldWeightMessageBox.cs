﻿using System;

 
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.UI;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
namespace CustomUtilities
{
    public partial class UldWeightMessageBox : CargoMatrix.UI.MessageBoxBase 
    {
        
        private string m_caption;
        private long flightId;
        private string uldType;
        private static UldWeightMessageBox instance = null;
        public UldWeightMessageBox()
        {
            InitializeComponent();
 
        }
        private void InitializeImages()
        {
            this.buttonCancel.PressedImage = Resources.Graphics.Skin.nav_cancel_over;
            this.buttonCancel.Image = Resources.Graphics.Skin.nav_cancel;
            this.pictureBoxHeader.Image = Resources.Graphics.Skin.popup_header;
            this.buttonOk.PressedImage = Resources.Graphics.Skin.nav_ok_over;
            this.buttonOk.Image = Resources.Graphics.Skin.nav_ok;
            this.buttonBrowse.Image = Resources.Graphics.Skin._3dots;
            this.buttonBrowse.PressedImage = Resources.Graphics.Skin._3dots_over ;
            this.pictureBox1.Image = Resources.Graphics.Skin.nav_bg;
             
             
        }

        
        void pictureBoxHeader_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {

                pen.Width = 1;
                e.Graphics.DrawString(m_caption, new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold), new SolidBrush(Color.White), 5, 3);
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            Cursor.Current = Cursors.Default;
            base.OnGotFocus(e);
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
  
        }


        private void buttonWeight_TextChanged(object sender, EventArgs e)
        {
            CalcTotalWeight();
        }

        private decimal ConvertToDecimal(string value)
        {
            try
            {
                return decimal.Parse(value);
            }
            catch
            {
                return 0;
            }

        }

        private  void CalcTotalWeight()
        {
            decimal t = ConvertToDecimal(instance.txtWeight.Text) - ConvertToDecimal(instance.txtTareWeight.Text);
            instance.txtTotalWeight.Text = t.ToString();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }


        public static DialogResult Show(long flightId, string reference, string uldType, string uldPrefix, string uldNumber, ref decimal weight, ref decimal tareweight)
        {


            if (instance == null)
                instance = new UldWeightMessageBox();


            instance.flightId = flightId;
            instance.uldType = uldType;

            
            instance.lblReference.Text = reference;
            instance.lblType.Text = "ULD: " + uldType + "-" + uldPrefix + uldNumber;
            instance.txtWeight.Text = string.Format("{0:0.0}",weight);
            instance.txtWeight.SelectAll();
            instance.txtTareWeight.Text = string.Format("{0:0.0}", tareweight);
            instance.CalcTotalWeight();
             
            instance.m_caption = "Enter Weight";

            return instance.CMXShowDialog(ref weight, ref tareweight);
             
        }
        private DialogResult CMXShowDialog(ref   decimal weight, ref decimal tareweight)
        {
            DialogResult result = ShowDialog();

            weight = ConvertToDecimal(instance.txtWeight.Text);
            tareweight = ConvertToDecimal(instance.txtTareWeight.Text);

            return result;
        }
       
      
    
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
           
        }


     
    }
}