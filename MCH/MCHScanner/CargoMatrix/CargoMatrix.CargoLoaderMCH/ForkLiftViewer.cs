﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CargoMatrix.Utilities;
using CustomListItems;
using CustomUtilities;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using CargoMatrix.Communication.WSScannerMCHService;
using CMXExtensions;
using CMXBarcode;
using System.Reflection;

namespace CargoMatrix.CargoLoader
{
    public partial class ForkLiftViewer : CargoMatrix.Utilities.MessageListBox
    {
        public object m_activeApp;
        private CargoLoaderFlight flightDetailsItem;
        private static ForkLiftViewer instance;
        CargoMatrix.UI.BarcodeReader barcode;
 
        public static ForkLiftViewer Instance
        {
            get
            {
                if (instance == null)
                    instance = new ForkLiftViewer();
 
                return instance;
            }
        }


        private ForkLiftViewer()
        {
            InitializeComponent();
            smoothListBoxBase1.MultiSelectEnabled = true;
   
            this.HeaderText = "Forklift Content";
            this.HeaderText2 = "Select shipments to drop into consol";
            this.LoadListEvent += new LoadSmoothList(ForkLiftViewer_LoadListEvent);
            this.ListItemClicked += new SmoothListbox.ListItemClickedHandler(ForkLiftViewer_ListItemClicked);
            barcode = new CargoMatrix.UI.BarcodeReader();
            barcode.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(ForkliftScan);
        }


        void ForkLiftViewer_ListItemClicked(SmoothListbox.SmoothListBoxBase sender, Control listItem, bool isSelected)
        {

     
            ForkLiftCargoItem itemControl = (ForkLiftCargoItem)listItem;
            LoadForkliftViewItem item = (LoadForkliftViewItem)itemControl.ItemData;
             

            if (item.Counter == 0)
            {
                item.Counter = item.ForkliftPieces;
            }
            else
            {
                item.Counter = 0;
            }
           

           

            sender.Reset();
        }

        void ForkLiftViewer_LoadListEvent(SmoothListbox.SmoothListBoxBase smoothList)
        {
            LoadControl();
        }

        public void LoadControl()
        {
            Cursor.Current = Cursors.WaitCursor;
            smoothListBoxBase1.RemoveAll();
            
            OkEnabled = false;



            //CargoLoaderShipment[] items = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderForkLiftShipments(0, flightDetailsItem.FlightManifestId);
            LoadForkliftViewItem[] items = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetForkliftView(flightDetailsItem.TaskId);
            if (items==null)
            {
                this.DialogResult = DialogResult.OK;
                return;
            }
            
            if (items.Length == 0)
            {
                 this.DialogResult = DialogResult.OK;
                 return;
            }


            foreach (LoadForkliftViewItem item in items)
            {
                ForkLiftCargoItem tempItem = new ForkLiftCargoItem(item);
                tempItem.ButtonClick += new EventHandler(ForkliftDelete_OnListClick);
                smoothListBoxBase1.AddItem(tempItem);
            }



        }



        void ForkliftDelete_OnListClick(object sender, EventArgs e)
        {

            ForkLiftCargoItem itemControl = (sender as ForkLiftCargoItem);
            LoadForkliftViewItem item = (LoadForkliftViewItem)itemControl.ItemData;
            if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to remove this item from your forklift?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
            {
                CargoMatrix.Communication.CargoLoaderMCHService.Instance.RemovePiecesFromForklift(item.DetailsId, item.ForkliftPieces);
                LoadControl();
            }
    
        }
 
        //void combo_SelectionChanged(object sender, EventArgs e)
        //{
        //    OkEnabled = !OkEnabled;

        //    if (sender is StaticListItem)
        //    {
        //        MessageBox.Show("StaticListItem");
        //        return;
        //    }

        //    ForkLiftCargoItem item = (ForkLiftCargoItem)sender;
        //    //CustomListItems.ComboBox item = sender as CustomListItems.ComboBox;
        //    //UpdateDescriptionLine(item);
        //    if (smoothListBoxBase1.SelectedItems.Count == 0)
        //    {
        //        //bool enableflag = false;
        //        //if (item.SubItemSelected)
        //        //{
        //         //   enableflag = true;
        //        //}
        //        OkEnabled = false;
        //    }
        //    else
        //    {
        //        OkEnabled = true;
        //    }
              


        //}



        CargoLoaderFlightLegUld SelectAvailableUld()
        {
            //CargoLoaderFlightLegUld[] ulds = CargoMatrix.Communication.CargoLoaderMCHService.Instance.GetCargoLoaderFlightLegUlds(  flightDetailsItem.FlightManifestId);
            CargoLoaderFlightLegUld[] ulds = CargoMatrix.Communication.CargoLoaderMCHService.Instance.AvailableULDsForBuild(flightDetailsItem.FlightManifestId);
            if (ulds == null)
                return null;

            CargoMatrix.Utilities.MessageListBox actPopup = new CargoMatrix.Utilities.MessageListBox();
            actPopup.HeaderText2 = "Ulds";
            actPopup.OneTouchSelection = true;
            actPopup.OkEnabled = false;
            actPopup.MultiSelectListEnabled = false;

            actPopup.HeaderText = "CargoLoader";


            actPopup.RemoveAllItems();

            for (int i = 0; i < ulds.Length; i++)
            {

                if (ulds[i].IsBup == false)
                {
                    if (ulds[i].UldType.ToUpper() == "LOOSE")
                    {
                        actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(ulds[i].UldType, CargoMatrix.Resources.Skin.Shipping_Crate1, i + 1));
                    }
                    else
                    {
                        if (ulds[i].UldSerialNo != null && ulds[i].UldSerialNo != string.Empty)
                        {
                            actPopup.AddItem(new SmoothListbox.ListItems.StandardListItem(ulds[i].UldType + "-" + ulds[i].UldPrefix + ulds[i].UldSerialNo, CargoMatrix.Resources.Skin.Shipping_Crate1, i + 1));
                        }
                       
                    }
                    
                }
 
            }


            try
            {
                if (DialogResult.OK == actPopup.ShowDialog())
                {

              


                    if (actPopup.SelectedItems[0].Name.ToUpper() == "LOOSE")
                    {
                        for (int i = 0; i < ulds.Length; i++)
                        {
                            if (actPopup.SelectedItems[0].Name == ulds[i].UldType)
                            {
                                return ulds[i];
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < ulds.Length; i++)
                        {
                            if (ulds[i].UldType + "-" + ulds[i].UldPrefix + ulds[i].UldSerialNo == actPopup.SelectedItems[0].Name)
                            {
                                return ulds[i];
                            }
                        }
                    }
        

                    return null;

                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }



        }

        protected override void pictureBoxOk_Click(object sender, EventArgs e)
        {

            //for (int i = 0; i < smoothListBoxBase1.SelectedItems.Count; i++)
            //{
            //    ForkLiftCargoItem ctr = (ForkLiftCargoItem)smoothListBoxBase1.SelectedItems[i];
            //    CargoLoaderShipment itm = (CargoLoaderShipment)ctr.ItemData;
            //    MessageBox.Show(itm.ShimpmentNumber);
            //}

            List<long> detailIds = new List<long>();
            for (int i = 0; i < smoothListBoxBase1.Items.Count; i++)
            {

                ForkLiftCargoItem ctr = (ForkLiftCargoItem)smoothListBoxBase1.Items[i];
                LoadForkliftViewItem itm = (LoadForkliftViewItem)ctr.ItemData;
                if (itm.Counter > 0)
                {
                   detailIds.Add(itm.DetailsId);
                }
            }

            if (detailIds.Count>0)
            {
                barcode.StopRead();
                CargoLoaderFlightLegUld uld = SelectAvailableUld();

                if (uld != null)
                {

                    if (uld.UldType.ToUpper() == "LOOSE")
                    {
                        //attach to loose


                        string reference = string.Format("{0}{1}", this.flightDetailsItem.CarrierCode, this.flightDetailsItem.FlightNumber);
                        string enteredLocation = string.Empty;
                        bool isFPC = false;

                        if (DoLocation(out enteredLocation, out isFPC, reference, true))
                        {

                            long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(enteredLocation);
                            if (locationId == 0)
                            {
                                CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);  
                                return;
                            }

                            CargoMatrix.Communication.CargoLoaderMCHService.Instance.DropSelectedItemsIntoLocation(this.flightDetailsItem.TaskId, (int)locationId, detailIds.ToArray());

                            if (isFPC)
                            {
                                ShowFreightPhotoCapture(reference, CargoMatrix.Communication.WebServiceManager.Instance().GetLocationMCH(), flightDetailsItem.Destinations[0], 1, "CargoLoader");
                            }
                            


                        }

                    }
                    else
                    {
               
                        CargoMatrix.Communication.CargoLoaderMCHService.Instance.DropSelectedItemsIntoULD(this.flightDetailsItem.TaskId, uld.UldId, detailIds.ToArray());


                    }

                    LoadControl();

                }

                barcode.StartRead();
            }
            else
            {
                OkEnabled = false;
            }
           
            
        }

        bool DoLocation(out string enterLocation, out bool isFPC, string title, bool isFPCActive)
        {
            enterLocation = string.Empty;

            ScanEnterLocationPopup confirmLoc = new ScanEnterLocationPopup();
            //CustomUtilities.ScanEnterPopup confirmLoc = new CustomUtilities.ScanEnterPopup();
            confirmLoc.TextLabel = "Scan Location";
            confirmLoc.Title = title;
            confirmLoc.scannedPrefix = "B";
            confirmLoc.isFPC = false;
            confirmLoc.isFPCActive = isFPCActive;
            isFPC = false;
            DialogResult dr;
            bool exitflag = false;
            do
            {
                dr = confirmLoc.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(confirmLoc.ScannedText);
                    if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
                    {
                        enterLocation = scanItem.Location;
                        isFPC = confirmLoc.isFPC;
                        exitflag = true;
                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Invalid barcode has been scanned", "Error", CargoMatrix.UI.CMXMessageBoxIcon.Delete);
                    }
                }
                else
                {
                    exitflag = true;
                }


            } while (exitflag != true);



            if (enterLocation.Trim() == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }


        private void ShowFreightPhotoCapture(string hawbNo, string origin, string destination, int pieces, string title)
        {

          
            var hawb = CargoMatrix.Communication.HostPlusIncomming.Instance.GetHousebillInfo(hawbNo);
            if (null == hawb)
            {
                CargoMatrix.Communication.HostPlusIncomming.Instance.CreateHouseBillSkeleton(hawbNo, pieces, pieces, origin, destination, 0, "", "");
            }

 
            CargoMatrix.Communication.ScannerMCHServiceManager.Instance.AddTaskSnapshotReference(flightDetailsItem.TaskId, origin + hawbNo + destination);

            //*********************************************
            if (CargoMatrix.Communication.Utilities.CameraPresent)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {

                    Assembly SampleAssembly;
                    SampleAssembly = Assembly.LoadFrom("CargoMatrix.FreightPhotoCapture.dll");
                    // Obtain a reference to a method known to exist in assembly.
                    Type myType;

                    //myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.TaskList");
                    myType = SampleAssembly.GetType("CargoMatrix.FreightPhotoCapture.Reasons");
                    if (myType != null)
                    {

                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).ReferenceData = reference;
                        //((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain();

                        m_activeApp = SampleAssembly.CreateInstance(myType.FullName);

                        ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadAgain(hawbNo, origin, destination, true);
                        (m_activeApp as UserControl).Location = new Point(Left, Top);
                        (m_activeApp as UserControl).Size = new Size(Width, Height);

                        ((SmoothListbox.SmoothListbox)(m_activeApp)).TitleText = title;

                        CargoMatrix.UI.CMXAnimationmanager.DisplayForm(m_activeApp as CargoMatrix.UI.CMXUserControl);

                        //  ((CargoMatrix.FreightPhotoCapture.Reasons)(m_activeApp)).LoadCamera();

                    }
                    else
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Unable to load Freight Photo Capture Module", "Error!" + " (60001)", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                    }

                }
                catch
                {

                }
            }

            //*********************************************

        }
    
 

        public  DialogResult ShowDialog(CargoLoaderFlight flightDetailsItem)
        {
            this.flightDetailsItem = flightDetailsItem;
                barcode.StartRead();
                smoothListBoxBase1.IsSelectable = true;
 
            DialogResult dr = (instance as MessageListBox).ShowDialog();
            barcode.StopRead();
            return dr;

        }


        CargoLoaderFlightLegUld ValidateUld(string uldnumber)
        {
            CargoLoaderFlightLegUld[] ulds = CargoMatrix.Communication.CargoLoaderMCHService.Instance.AvailableULDsForBuild(flightDetailsItem.FlightManifestId);
            if (ulds == null)
                return null;

            foreach (CargoLoaderFlightLegUld uld in ulds)
            {
                if ((uld.UldPrefix + uld.UldSerialNo).ToUpper() == uldnumber.ToUpper())
                {
                    return uld;
                }
            }

            return null;
        }

        void ForkliftScan(string barcodeData)
        {

            List<long> detailIds = new List<long>();
                for (int i = 0; i < smoothListBoxBase1.Items.Count; i++)
                {

                    ForkLiftCargoItem ctr = (ForkLiftCargoItem)smoothListBoxBase1.Items[i];
                    LoadForkliftViewItem itm = (LoadForkliftViewItem)ctr.ItemData;
                    if (itm.Counter > 0)
                    {
                        detailIds.Add(itm.DetailsId);
                    }
                }
            if (detailIds.Count==0)
            {
            OkEnabled = false;
            }




            CMXBarcode.ScanObject scanItem = CargoMatrix.Communication.BarcodeParser.Instance.Parse(barcodeData);
            if (scanItem.BarcodeType == BarcodeTypes.Area || scanItem.BarcodeType == BarcodeTypes.Door || scanItem.BarcodeType == BarcodeTypes.ScreeningArea)
            {

                if (detailIds.Count>0)
                {
                    if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop selected pieces into this location?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                    {
                        long locationId = CargoMatrix.Communication.WebServiceManager.Instance().GetLocationIdByLocationBarcodeMCH(scanItem.Location);
                        if (locationId == 0)
                        {
                            CargoMatrix.UI.CMXMessageBox.Show("Invalid location.", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                            barcode.StartRead();
                            return;
                        }

                        CargoMatrix.Communication.CargoLoaderMCHService.Instance.DropSelectedItemsIntoLocation(this.flightDetailsItem.TaskId, (int)locationId, detailIds.ToArray());

                        LoadControl();
                    }
                }
  

            }
            else if (scanItem.BarcodeType == BarcodeTypes.Uld)
            {
                if (detailIds.Count>0)
                {
                    CargoLoaderFlightLegUld uld = ValidateUld(scanItem.UldNumber);
                    if (uld != null)
                    {
                        if (DialogResult.OK == CargoMatrix.UI.CMXMessageBox.Show("Are you sure you want to drop selected pieces into this ULD?", "CargoLoader", CargoMatrix.UI.CMXMessageBoxIcon.Question, MessageBoxButtons.YesNo, DialogResult.Yes))
                        {
                      
                            CargoMatrix.Communication.CargoLoaderMCHService.Instance.DropSelectedItemsIntoULD(this.flightDetailsItem.TaskId, uld.UldId, detailIds.ToArray());
 
                            LoadControl();
                        }
                    }
   
                }
            }


            barcode.StartRead();
        }
 
    }
   
}
