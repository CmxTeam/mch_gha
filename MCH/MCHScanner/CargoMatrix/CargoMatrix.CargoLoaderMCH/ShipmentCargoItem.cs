﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CargoMatrix.Communication.DTO;
using CMXExtensions;
using SmoothListbox;
using CargoMatrix.Communication.WSCargoLoaderMCHService;
using System.Collections.Generic;
using System.Text;
namespace CargoMatrix.CargoLoader
{
    //public class ShipmentTest
    //{
    //    public TaskStatuses Status= TaskStatuses.Open;
    //    public long ShipmentId;
    //    public string ShimpmentNumber;
    //    public string ShimpmentType;
    //    public int TotalPcs;
    //    public int TotalSTC;
    //    public int TotalAwbs;
    //    public int Load;
    //    public string LoadIndicator;
    //    public List<string> Locations = new List<string>() ;
    //    public double Weight;
    //    public string WeigthUOM = "Kg";
    //    public int Flag =12;
    //}

    public partial class ShipmentCargoItem : CustomListItems.ExpandableRenderListitem<AwbModel>, ISmartListItem
    {

        public event EventHandler ButtonClick;

        public ShipmentCargoItem(AwbModel shipment)
            : base(shipment)
        {
            InitializeComponent();

          
            this.LabelConfirmation = "Enter Masterbill Number";
            this.previousHeight = this.Height;


       


        }

        protected override bool ButtonEnterValidation()
        {
            return string.Equals(ItemData.ToString(), TextBox1.Text, StringComparison.OrdinalIgnoreCase);
        }

        void buttonBrowse_Click(object sender, System.EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this, EventArgs.Empty);
            }
        }


        protected override void InitializeControls()
        {
            this.SuspendLayout();
            this.buttonBrowse = new CargoMatrix.UI.CMXPictureButton();
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBrowse.Location = new System.Drawing.Point(202, 6);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Image = Resources.Skin.magnify_btn;
            this.buttonBrowse.PressedImage = Resources.Skin.magnify_btn_over;
            this.buttonBrowse.Size = new System.Drawing.Size(32, 32);
            this.buttonBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonBrowse.TransparentColor = System.Drawing.Color.White;
            this.buttonBrowse.Click += new System.EventHandler(buttonBrowse_Click);


            this.panelIndicators = new CustomUtilities.IndicatorPanel();
            this.panelIndicators.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelIndicators.Location = new System.Drawing.Point(4, 55);
            this.panelIndicators.Name = "panelIndicators";
            this.panelIndicators.Size = new System.Drawing.Size(230, 16);
            this.panelIndicators.TabIndex = 19;


            //this.Controls.Add(this.buttonBrowse);
          
            this.Controls.Add(panelIndicators);

            this.buttonBrowse.Scale(new SizeF(CurrentAutoScaleDimensions.Width / 96F, CurrentAutoScaleDimensions.Height / 96F));
            this.ResumeLayout(false);


            this.panelIndicators.Flags = (int)ItemData.Flags;

        }

        //private string GetLocationLine(List<string> locations)
        //{
        //    StringBuilder sb = new StringBuilder("");
        //    foreach (string l in locations)
        //    {
        //        if (sb.ToString() == string.Empty)
        //        {
        //            sb.Append(l);
        //        }
        //        else
        //        {
        //            sb.Append(", "+ l);
        //        }
        //    }
        //    return sb.ToString();
        //}

        private string GetLocationLine(string[] locations)
        {
            StringBuilder sb = new StringBuilder("");
            foreach (string l in locations)
            {
                if (sb.ToString() == string.Empty)
                {
                    sb.Append(l);
                }
                else
                {
                    sb.Append(", " + l);
                }
            }
            return sb.ToString();
        }
        protected override void Draw(Graphics gOffScreen)
        {
            using (Brush brush = new SolidBrush(Color.Black))
            {
                float hScale = CurrentAutoScaleDimensions.Height / 96F; // horizontal scale ratio
                float vScale = CurrentAutoScaleDimensions.Height / 96F; // vertical scale ratio

                Brush redBrush = new SolidBrush(Color.Red);
                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold))
                {
                    string line1 = string.Format("AWB: {0}", ItemData.AWBNumber);
                    gOffScreen.DrawString(line1, fnt, brush, new RectangleF(40 * hScale, 4 * vScale, 170 * hScale, 14 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular))
                {
                    string line2 = string.Format("Pcs: {0} STC: {1}", ItemData.TotalPieces, ItemData.TotalSTC);
              
                    gOffScreen.DrawString(line2, fnt, brush, new RectangleF(40 * hScale, 17 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular))
                {
                    string line3 = string.Format("Loaded: {0} of {1}({2}) Wt: {3:0.0}{4}", ItemData.LoadedPcs, ItemData.LoadPcs, ItemData.LoadingIndicator, ItemData.Weight, ItemData.WeightUOM);
               
                    gOffScreen.DrawString(line3, fnt, brush, new RectangleF(40 * hScale, 31 * vScale, 170 * hScale, 13 * vScale));
                }

                using (Font fnt = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular))
                {
                    string line4 = string.Format("Loc: {0}", ItemData.Locations);
                    gOffScreen.DrawString(line4, fnt, brush, new RectangleF(40 * hScale, 45 * vScale, 170 * hScale, 13 * vScale));
                }
 

                //this.panelIndicators.Flags = (int)ItemData.Flags;


                Image img = CargoMatrix.Resources.Skin.Clipboard;
                

                switch (ItemData.Status)
                {
                    case TaskStatuses.Open:
                    case TaskStatuses.Pending:
                        img = CargoMatrix.Resources.Skin.Clipboard;
                        break;
                    case TaskStatuses.InProgress:
                        img = CargoMatrix.Resources.Skin.status_history;
                        break;
                    case TaskStatuses.Completed:
                        img = CargoMatrix.Resources.Skin.Clipboard_Check;
                        break;

                }


                gOffScreen.DrawImage(img, new Rectangle((int)(4 * hScale), (int)(10 * vScale), (int)(24 * hScale), (int)(24 * vScale)), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);

                using (Pen pen = new Pen(Color.Gainsboro, hScale))
                {
                    int hgt = isExpanded ? Height : previousHeight;
                    gOffScreen.DrawLine(pen, 0, hgt - 1, Width, hgt - 1);
                }
            }
        }

        #region ISmartListItem Members

        public bool Contains(string text)
        {
            string content = string.Empty;
            content = ItemData.AWBNumber;
            return content.ToUpper().Contains(text.ToUpper());
        }

        public bool Filter
        {
            get;
            set;
        }

        #endregion
    }
}
