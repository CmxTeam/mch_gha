// CMXNativeUtilities.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

extern "C" bool __declspec(dllexport) ShowTaskBar(bool show)
{
	HWND hTaskBar = ::FindWindow( _T( "HHTaskBar" ), NULL );

	if (hTaskBar != NULL)
	{
		if(show)
			::ShowWindow( hTaskBar, SW_SHOW);
		else
			::ShowWindow( hTaskBar, SW_HIDE );
	}

	return true;
}