﻿namespace CargoMatrixScanner
{
    partial class Login
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxPassword = new CargoMatrix.UI.CMXTextBox();
            this.textBoxUserID = new CargoMatrix.UI.CMXTextBox();
            this.labelTitleDescription = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelUserID = new System.Windows.Forms.Label();
            this.buttonLogin = new OpenNETCF.Windows.Forms.Button2();
            this.buttonClear = new OpenNETCF.Windows.Forms.Button2();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 54);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 57);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxPassword.Location = new System.Drawing.Point(72, 116);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(160, 28);
            this.textBoxPassword.TabIndex = 2;
            this.textBoxPassword.Click += new System.EventHandler(this.textBoxPassword_Click);
            this.textBoxPassword.GotFocus += new System.EventHandler(this.textBoxPassword_GotFocus);
            this.textBoxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxPassword_KeyDown);
            // 
            // textBoxUserID
            // 
            this.textBoxUserID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxUserID.Location = new System.Drawing.Point(72, 82);
            this.textBoxUserID.Name = "textBoxUserID";
            this.textBoxUserID.Size = new System.Drawing.Size(160, 28);
            this.textBoxUserID.TabIndex = 1;
            this.textBoxUserID.GotFocus += new System.EventHandler(this.textBoxUserID_GotFocus);
            this.textBoxUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxUserID_KeyDown);
            this.textBoxUserID.EnabledChanged += new System.EventHandler(this.textBoxUserID_EnabledChanged);
            // 
            // labelTitleDescription
            // 
            this.labelTitleDescription.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelTitleDescription.Location = new System.Drawing.Point(7, 59);
            this.labelTitleDescription.Name = "labelTitleDescription";
            this.labelTitleDescription.Size = new System.Drawing.Size(225, 20);
            this.labelTitleDescription.Text = "labelTitleDescription";
            this.labelTitleDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelPassword
            // 
            this.labelPassword.Location = new System.Drawing.Point(7, 122);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(65, 20);
            this.labelPassword.Text = "<Password>";
            // 
            // labelUserID
            // 
            this.labelUserID.Location = new System.Drawing.Point(7, 88);
            this.labelUserID.Name = "labelUserID";
            this.labelUserID.Size = new System.Drawing.Size(65, 20);
            this.labelUserID.Text = "<UserID>";
            // 
            // buttonLogin
            // 
            this.buttonLogin.ActiveBackColor = System.Drawing.Color.White;
            this.buttonLogin.ActiveBorderColor = System.Drawing.Color.White;
            this.buttonLogin.ActiveForeColor = System.Drawing.Color.Black;
            this.buttonLogin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonLogin.BackgroundImage")));
            this.buttonLogin.BorderColor = System.Drawing.Color.White;
            this.buttonLogin.DisabledBackColor = System.Drawing.Color.White;
            this.buttonLogin.DisabledBorderColor = System.Drawing.Color.White;
            this.buttonLogin.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.buttonLogin.Location = new System.Drawing.Point(140, 150);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(92, 53);
            this.buttonLogin.TabIndex = 17;
            this.buttonLogin.Text = "<login>";
            this.buttonLogin.TransparentImage = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.ActiveBackColor = System.Drawing.Color.White;
            this.buttonClear.ActiveBorderColor = System.Drawing.Color.White;
            this.buttonClear.ActiveForeColor = System.Drawing.Color.Black;
            this.buttonClear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonClear.BackgroundImage")));
            this.buttonClear.BorderColor = System.Drawing.Color.White;
            this.buttonClear.DisabledBackColor = System.Drawing.Color.White;
            this.buttonClear.DisabledBorderColor = System.Drawing.Color.White;
            this.buttonClear.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.Location = new System.Drawing.Point(7, 150);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(92, 53);
            this.buttonClear.TabIndex = 22;
            this.buttonClear.Text = "<login>";
            this.buttonClear.TransparentImage = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(7, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.Text = "Password:";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BarcodeEnabled = true;
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxUserID);
            this.Controls.Add(this.labelTitleDescription);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelUserID);
            this.Controls.Add(this.panel2);
            this.Name = "Login";
            this.Size = new System.Drawing.Size(240, 292);
            this.GotFocus += new System.EventHandler(this.Login_GotFocus);
            this.BarcodeReadNotify += new CargoMatrix.UI.BarcodeReadNotifyHandler(this.Login_BarcodeReadNotify);
            this.EnabledChanged += new System.EventHandler(this.Login_EnabledChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Login_Paint);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        //private System.Windows.Forms.TextBox textBoxPassword;
        //private System.Windows.Forms.TextBox textBoxUserID;
        private CargoMatrix.UI.CMXTextBox textBoxPassword;
        private CargoMatrix.UI.CMXTextBox textBoxUserID;
        private System.Windows.Forms.Label labelTitleDescription;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelUserID;
        private OpenNETCF.Windows.Forms.Button2 buttonLogin;
        private OpenNETCF.Windows.Forms.Button2 buttonClear;
        private System.Windows.Forms.Label label3;
    }
}
