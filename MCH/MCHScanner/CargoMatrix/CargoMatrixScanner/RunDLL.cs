﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CargoMatrix
{
    public class RunDLL
    {
        public RunDLL()
        {
            CargoMatrixScanner.Process[] processes = CargoMatrixScanner.Process.GetProcesses();

            if (processes != null)
            {
                for (int i = 0; i < processes.Length; i++)
                {
                    if (processes[i].ProcessName.Equals("CargoMatrix.exe", StringComparison.OrdinalIgnoreCase))
                    {
                        CargoMatrix.UI.CMXMessageBox.Show("Cannot start \'Mobile Cargo Handler\'. \'CargoMatrix.exe\' is already running. Close CargoMatrix application and then run \'Mobile Cargo Handler\'.", "Error!", CargoMatrix.UI.CMXMessageBoxIcon.Exclamation);
                        return;
                    }
                }
            }

            if (System.Environment.OSVersion.Version.Major > 4)
            {
                ShowTaskBar(false);
            }
            
            try
            {

                OpenNETCF.Windows.Forms.Application2.Run(new CargoMatrixScanner.MainForm());
            }
            
            finally
            {
                if (System.Environment.OSVersion.Version.Major > 4)
                {
                    ShowTaskBar(true);
                }
            }

            if (System.Environment.OSVersion.Version.Major > 4)
            {
                ShowTaskBar(true);
            }
        }

        [DllImport("CMXNativeUtilities.DLL")]
        private static extern void ShowTaskBar(bool bShow);
    }
}
