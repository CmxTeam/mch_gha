﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.OnHand;

namespace CargoMatrix.Communication.WSOnHand
{
    public partial class OnHandTaskItem : IShipment
    {
        #region IShipment Members

        public string ShipmentNO
        {
            get { return this.pickupRefField; }
        }

        public string Destination
        {
            get { return this.destinationField; }
        }

        public string Origin
        {
            get { return this.originField; }
        }

        public string MOT
        {
            get { return this.modeOfTransportField; }
        }

        public string Status
        {
            get { return this.statusField.ToString(); }
        }

        public string CustomerCode
        {
            get { return this.customerNameField; }
        }

        public string Vendor
        {
            get { return this.vendorNameField; }
        }

        public string TruckingCompany
        {
            get { return this.truckingCompanyField; }
        }

        public long ShipmentID
        {
            get { return this.pickupIDField; }
        }

        public int TotalPieces
        {
            get { return this.totalPiecesField; }
        }

        public string PONumber
        {
            get { return this.poNumberField; }
        }

        ShipmentStatus IShipment.Status
        {
            get { return this.statusField; }
        }

        #endregion
    }
}
