﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.OnHand;

namespace CargoMatrix.Communication.WSOnHand
{
    public partial class CMXDomesticPackage : IPackage
    {

        #region IPackage Members

        public string PackageType
        {
            get { return this.typeField; }
            set { this.typeField = value; }
        }

        double IPackage.Length
        {
            get { return (double)(this.lengthField ?? 0); }
            set { this.lengthField = (decimal)value; }
        }

        double IPackage.Width
        {
            get { return (double)(this.widthField ?? 0); }
            set { this.widthField = (decimal)value; }
        }

        double IPackage.Height
        {
            get { return (double)(this.heightField ?? 0); }
            set { this.heightField = (decimal)value; }
        }

        double IPackage.Weight
        {
            get { return (double)(this.weightField ?? 0); }
            set { this.weightField = (decimal)value; }
        }

        int IPackage.Slac
        {
            get
            {
                return this.slacField ?? 0;
            }
            set
            {
                this.slacField = value; ;
            }
        }

        public long PackageID
        {
            get
            {
                return this.recIDField;
            }
        }

        #endregion
    }
}
