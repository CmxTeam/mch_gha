﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.OnHand
{
    public interface IPackage
    {
        long PackageID { get;}
        string PackageType { get; set; }
        string Condition { get; set; }
        double Length { get; set; }
        double Width { get; set; }
        double Height { get; set; }
        double Weight { get; set; }
        int Slac { get; set; }
        int PieceNumber { get; }
        string WeightUOM { get; set; }
        string DimUOM { get; set; }
    }
}
