﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSCargoDimensioner;

namespace CargoMatrix.Communication.CargoMeasurements
{
    public class CargoMeasurementsService : CMXServiceWrapper
    {
        WSCargoDimensioner.CargoDimensioner service;
        static CargoMeasurementsService instance;
        string asmxName = "CargoDimensioner.asmx";

        public static CargoMeasurementsService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CargoMeasurementsService();
                }
                return instance;
            }
        }

        public static CargoMatrix.Communication.DTO.User CurrentUser
        {
            get
            {
                return WebServiceManager.Instance().m_user;
            }

        }

        private CargoMeasurementsService()
        {
            service = new WSCargoDimensioner.CargoDimensioner();
            service.Url = Settings.Instance.URLPath + asmxName;
        }


        public DimensionerDevice GetDefaultDimensioningDevice()
        {
            Func<DimensionerDevice> func = () =>
                {
                    var result = this.service.GetDefaultDimensioningDevice(this.gateway, this.userId);

                    return result != null ? new DimensionerDevice(result) : null;
                };

            return Invoke<DimensionerDevice>(func, 51002, null);
        }

        public DimensionerDevice[] GetDimensioningDevices()
        {
            Func<DimensionerDevice[]> func = () =>
            {
                var result = this.service.GetDimensioningDevices(this.gateway);

                return (from item in result
                        select new DimensionerDevice(item)).ToArray();
            };

            return Invoke<DimensionerDevice[]>(func, 51003, new DimensionerDevice[0]);
       }


        public WSCargoDimensioner.TransactionStatus SaveMeasurement(DimensioningMeasurementDetails details)
        {
            Func<WSCargoDimensioner.TransactionStatus> f = () =>
            {
                return this.service.SaveMeasurement(details.ToDimensioningMeasurement(userName, gateway));
            };

            WSCargoDimensioner.TransactionStatus status= new CargoMatrix.Communication.WSCargoDimensioner.TransactionStatus();
            status.transactionStatus = false;
            status.TransactionError = "Error occurred while executing save command";

            return this.Invoke(f, 51004, status);
        }

        public WSCargoDimensioner.TransactionStatus[] SaveMeasurements(DimensioningMeasurementDetails[] measurements)
        {
            Func<WSCargoDimensioner.TransactionStatus[]> f = () =>
                {
                    var dimArray = (from item in measurements
                                    select item.ToDimensioningMeasurement(this.userName, this.gateway)).ToArray();

                    return this.service.SaveMeasurements(dimArray);
                };

            return this.Invoke<WSCargoDimensioner.TransactionStatus[]>(f, 51005, new WSCargoDimensioner.TransactionStatus[0]);
        }

        public void SetDefaultDimensiongDevice(DimensionerDevice device)
        {
            Action action = () =>
            {
                var sercerDevice = device.ToWSDimensioningDevice();

                this.service.SetDeafaultDimensioningDevice(this.gateway, this.userId, sercerDevice);
            };

            this.Invoke(action, 51006);
        }

        public WSCargoDimensioner.PrintingOrigin GetPrinterOrigin()
        {
            Func<WSCargoDimensioner.PrintingOrigin> f = () =>
            {
                return this.service.GetPrinterOrigin(this.gateway);
            };

            return this.Invoke<WSCargoDimensioner.PrintingOrigin>(f, 51007, PrintingOrigin.DISABLED);
        }

    }

    internal static class Helper
    {
        public static WSCargoDimensioner.DimensioningMeasurement ToDimensioningMeasurement(this DimensioningMeasurementDetails details, string userName, string gateWay)
        {
            var result = new WSCargoDimensioner.DimensioningMeasurement();
            result.BarcodeType = details.BarcodeType.ToWSCargoDimensionerBarcodeType();
            result.MeasurenetUnit = details.Measurement.MetricSystemType.ToMeasurementsUnit();
            result.Origin = details.Measurement.Origin.ToMeasurementsOrigin();
            result.Gateway = gateWay;
            result.UserName = userName;
            result.Height = details.Measurement.Height;
            result.Length = details.Measurement.Length;
            result.Width = details.Measurement.Width;
            result.Weight = details.Measurement.Weight;
            result.PackageType = new CargoMatrix.Communication.WSCargoDimensioner.PackageType() { Id = details.PackageType.Id };
          
            switch (details.BarcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill:
                    result.Reference = details.PackageNumber;
                    result.PieceNumber = details.PieceNumber.Value;
                    break;

                case CMXBarcode.BarcodeTypes.Uld:
                    
                    result.Reference = details.PackageNumber;
                    result.MasterbillNo = details.MasterbillNumber;
                    result.ULDTypeID = details.ULDType.TypeID;
                    break;

                case CMXBarcode.BarcodeTypes.MasterBill:
                    result.Reference = null;
                    result.MasterbillNo = details.MasterbillNumber;
                    result.ULDTypeID = details.ULDType.TypeID;
                    break;
                    
                case CMXBarcode.BarcodeTypes.OnHand:
                    result.Reference = details.PackageNumber;
                    result.PieceNumber = details.PieceNumber.Value;
                    break;

                case CMXBarcode.BarcodeTypes.NA:
                    result.Reference = details.PackageNumber;
                    result.PieceNumber = details.PieceNumber.Value;
                    break;
            }
            
            return result;
        }

        public static WSCargoDimensioner.MeasurementsUnit ToMeasurementsUnit(this MetricSystemTypeEnum metricSystem)
        {
            switch (metricSystem)
            {
                case MetricSystemTypeEnum.Imperial: return WSCargoDimensioner.MeasurementsUnit.Imperial;

                case MetricSystemTypeEnum.Metric: return WSCargoDimensioner.MeasurementsUnit.Metric;

                default:
                    throw new ArgumentException("Unknown measurement unit type");
            }
        }

        public static WSCargoDimensioner.MeasurementsOrigin ToMeasurementsOrigin(this MeasurementsOriginEnum origin)
        {
            switch (origin)
            {
                case MeasurementsOriginEnum.Device: return WSCargoDimensioner.MeasurementsOrigin.Device;

                case MeasurementsOriginEnum.Manual: return WSCargoDimensioner.MeasurementsOrigin.Manual;

                default:
                    throw new ArgumentException("Unknown measurement origin type");
            }
        }

        public static WSCargoDimensioner.BarcodeType ToWSCargoDimensionerBarcodeType(this CMXBarcode.BarcodeTypes barcodeType)
        {
            switch (barcodeType)
            {
                case CMXBarcode.BarcodeTypes.HouseBill: return WSCargoDimensioner.BarcodeType.Housebill;

                case CMXBarcode.BarcodeTypes.Uld:
                case CMXBarcode.BarcodeTypes.MasterBill: 
                    return WSCargoDimensioner.BarcodeType.ULD;

                case CMXBarcode.BarcodeTypes.OnHand: return WSCargoDimensioner.BarcodeType.OnHand;

                case CMXBarcode.BarcodeTypes.NA: return WSCargoDimensioner.BarcodeType.Unknown;

                default:
                    throw new ArgumentException("Unknown barcode type");
            }
        }
    
    }
}
