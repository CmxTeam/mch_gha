﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol;



namespace CargoMatrix.Communication.CargoMeasurements.Communication
{
    public class MettlerCommunicatorClient : CargoMatrix.Communication.CargoMeasurements.Communication.IMettlerCommunicatorClient
    {
        #region Fields
        string serverIP;
        int serverPort;
        string mettlerDeviceIP;
        int bytesToRead;
        int stop;


        TcpClient client;

        public event EventHandler<ExceptionEventArgs> OnError;
        public event EventHandler OnConnected;
        public event EventHandler OnMettlerIPSent;
        public event EventHandler OnAliveRequestSent;
        public event EventHandler<ResponseReceivedEventArgs> OnResponseReceived;
       

        #endregion

        #region Properties
        public bool Connected
        {
            get
            {
                if (this.client == null) return false;

                return this.client.Client.Connected;

            }
        }
        
        public string ServerIP
        {
            get { return serverIP; }
        }

        public int ServerPort
        {
            get { return this.serverPort; }
        }

        public string MettlerDeviceIP
        {
            get { return mettlerDeviceIP; }
        }

        public int BytesToRead
        {
            get { return bytesToRead; }
            set { bytesToRead = value; }
        } 
        #endregion


        public MettlerCommunicatorClient(string serverIP, int serverPort, string mettlerDeviceIP)
        {
            IPAddress.Parse(serverIP);
            IPAddress.Parse(mettlerDeviceIP);
            
            this.serverIP = serverIP;
            this.serverPort = serverPort;
            this.mettlerDeviceIP = mettlerDeviceIP;
            this.bytesToRead = 256;

            this.stop = 0;
        }

        public void Connect()
        {
            try
            {
                if (this.Connected == true) return;
                
                this.client = new TcpClient(this.serverIP, this.serverPort);

                while (true)
                {
                    if (this.client.Client.Connected == true) break;

                    if (this.IsStopped == true) return;
                }

                this.IsStopped = false;

                this.ConnectedFired(EventArgs.Empty);
            }
            catch (Exception ex)
            {
                CommunicationException exception = new CommunicationException(ExceptionCodeEnum.CONNECT, "Connection exception", ex);

                this.ErrorFired(new ExceptionEventArgs(exception));
            }
        }

        public void ConnectAsync()
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                this.Connect();
            });
        }

        public void SendMettlerIPAsync()
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                this.SendMettlerIP();
            });
        }

        public void SendMettlerIP()
        {
            try
            {
                if (this.IsStopped == true) throw new ApplicationException("Client is stopped");

                CommandValues commandValues = new CommandValues();
                commandValues.CommnadType = CommandTypesEnum.IP;
                commandValues.CommunicatorType = CommunicatorTypesEnum.SCANNER;
                commandValues.Data = this.mettlerDeviceIP;

                this.SendCommand(commandValues);

                this.MettlerIPFired(EventArgs.Empty);

                this.GetResponse();
            }
            catch (Exception ex)
            {
                CommunicationException exception = new CommunicationException(ExceptionCodeEnum.SEND_IP, "Send IP exception", ex);

                this.ErrorFired(new ExceptionEventArgs(exception));
            }
        }

        private void SendCloseCommand()
        {
            try
            {
                CommandValues commandValues = new CommandValues();
                commandValues.CommnadType = CommandTypesEnum.STOP;
                commandValues.CommunicatorType = CommunicatorTypesEnum.SCANNER;
                commandValues.Data = string.Empty;

                this.SendCommand(commandValues);
            }
            catch (Exception ex)
            {
                CommunicationException exception = new CommunicationException(ExceptionCodeEnum.SEND_CLOSE, "Send CLOSE exception", ex);

                this.ErrorFired(new ExceptionEventArgs(exception));
            }
        }
        
        public void SendAliveCommand()
        {
            try
            {
                if (this.IsStopped == true) throw new ApplicationException("Client is stopped");

                CommandValues commandValues = new CommandValues();
                commandValues.CommnadType = CommandTypesEnum.ALIVE;
                commandValues.CommunicatorType = CommunicatorTypesEnum.SCANNER;

                this.SendCommand(commandValues);

                this.AliveRequestFired(EventArgs.Empty);

                this.GetResponse();
            }
            catch (System.Exception ex)
            {
                CommunicationException exception = new CommunicationException(ExceptionCodeEnum.SEND_ALIVE, "Send Alive exception", ex);

                this.ErrorFired(new ExceptionEventArgs(exception));
            }
        }
        
        public void SendAliveCommandAsync()
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                this.SendAliveCommand();
            });
        }
        
        private void SendCommand(CommandValues command)
        {
            NetworkStream stream = client.GetStream();

            if (stream.CanWrite == false && command.CommnadType == CommandTypesEnum.STOP) return;
            
            string commandStr = CommandsParser.FormatClientCommand(command);
            byte[] data = CommandsParser.ToByteArray(commandStr);

            stream.Write(data, 0, data.Length);

            stream.Flush();
            
        }

        private CommandValues GetResponse()
        {
            try
            {
                byte[] bytes = new byte[this.bytesToRead];
                int bytesRead;

                NetworkStream stream = client.GetStream();
                
                if (stream.CanRead == false) throw new ApplicationException("Cannot read network stream");
                
                bytesRead = stream.Read(bytes, 0, bytes.Length);
              
                byte[] bytesReadArray = new byte[bytesRead];

                Array.Copy(bytes, bytesReadArray, bytesRead);

                string strCommand = CommandsParser.ToString(bytesReadArray);

                CommandValues command = CommandsParser.ParseServerCommand(strCommand);

                ResponseReceivedEventArgs args = new ResponseReceivedEventArgs(command);

                this.OnResponseReceived(this, args);

                return command;

            }
            catch (Exception ex)
            {
                CommunicationException exception = new CommunicationException(ExceptionCodeEnum.GET_RESPONSE, "Get response exception", ex);

                this.ErrorFired(new ExceptionEventArgs(exception));
            }

            return null;
        }
        
        private bool IsStopped
        {
            get
            {
                return this.stop == 1;
            }
            set
            {
                int booInt = value == true ? 1 : 0;

                Interlocked.Exchange(ref this.stop, booInt);
            }
        }

        private void Stop()
        {
            try
            {
                this.IsStopped = true;

                this.SendCloseCommand();

                this.client.Client.Shutdown(SocketShutdown.Both);

                this.client.Close();
            }
            catch (Exception ex)
            {
                CommunicationException exception = new CommunicationException(ExceptionCodeEnum.STOP, "Stop exception", ex);

                this.ErrorFired(new ExceptionEventArgs(exception));
            }
        }

        public void Disconnect()
        {
            if (this.IsStopped == true) return;
            
            this.Stop();
        }

        #region EventHelpers
        private void ConnectedFired(EventArgs args)
        {
            var connected = this.OnConnected;

            if (connected != null)
            {
                connected(this, args);
            }
        }

        private void MettlerIPFired(EventArgs args)
        {
            var mettlerIPFired = this.OnMettlerIPSent;

            if (mettlerIPFired != null)
            {
                mettlerIPFired(this, args);
            }
        }

        private void AliveRequestFired(EventArgs args)
        {
            var aliveRequest = this.OnAliveRequestSent;

            if (aliveRequest != null)
            {
                aliveRequest(this, args);
            }
        }
        
        
        private void ResponseReceived(ResponseReceivedEventArgs args)
        {
            var responseReceived = this.OnResponseReceived;

            if (responseReceived != null)
            {
                responseReceived(this, args);
            }
        }

        private void ErrorFired(ExceptionEventArgs args)
        {
            var errorFired = this.OnError;

            if (errorFired != null)
            {
                errorFired(this, args);
            }
        } 
        #endregion
    
    
    }
}
