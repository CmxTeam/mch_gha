﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver
{
    public class AutoConnectCommunicationProtocolDriver : ICommunicationProtocolDriver
    {
        private readonly object actionLock = new object();

        DimensionerDevice dimensionerDevice;
        CommunicatorAction resetAction;
        CommunicatorAction currentAction;
        CommunicatorAction initialAction;

        public bool AutoConnectOnError { get; set; }

        public bool AutoConnectOnUnavailable { get; set; }

        public AutoConnectCommunicationProtocolDriver(DimensionerDevice dimensionerDevice)
        {
            this.AutoConnectOnError = true;
            this.AutoConnectOnUnavailable = true;

            this.CreateActionGraph(dimensionerDevice);
        }


        private void CreateActionGraph(DimensionerDevice device)
        {
            this.dimensionerDevice = device;
            this.CommunicationClient = new MettlerCommunicatorClient(device.ServerIP.ToString(), device.ServerPort, device.IpAddress.ToString());
           
            this.CommunicationClient.OnError -= new EventHandler<CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ExceptionEventArgs>(CommunicationClient_OnError);
            this.CommunicationClient.OnError += new EventHandler<CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ExceptionEventArgs>(CommunicationClient_OnError);
           
            var disconnectAction = new DisconnectAction(this.CommunicationClient);
            var connectAction = new ConnectActionAsync(this.CommunicationClient);
            var ipSendAction = new IPActionAsync(this.CommunicationClient);
            var aliveAction = new AliveActionAsync(this.CommunicationClient);

            this.resetAction = disconnectAction;
            this.initialAction = connectAction;
            this.currentAction = initialAction;

            this.initialAction.Next = aliveAction;
            aliveAction.Next = ipSendAction;
            ipSendAction.Next = ipSendAction;

            this.State = CommunicationStateEnum.Disconnected;
        }

        void CommunicationClient_OnError(object sender, CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ExceptionEventArgs e)
        {
            if (e.Exception != null &&
               (e.Exception.Code == Exceptions.ExceptionCodeEnum.SEND_CLOSE ||
                e.Exception.Code == Exceptions.ExceptionCodeEnum.STOP)) return;

            if (this.State == CommunicationStateEnum.Disconnecting ||
               this.State == CommunicationStateEnum.Disconnected) return;


            if (this.AutoConnectOnError == false) return;

            ThreadPool.QueueUserWorkItem(o =>
                {
                    this.Reset();

                    this.PerformAction();
                });
        }
        
        #region ICommunicationProtocolDriver Members

        public IMettlerCommunicatorClient CommunicationClient { get; private set; }

        public CommunicationStateEnum State { get; private set; }

        public void Reset()
        {
            if (this.State == CommunicationStateEnum.Disconnected) return;

            this.State = this.resetAction.Perform();
            this.currentAction = this.initialAction;
        }

        public void Reset(DimensionerDevice dimensionerDevice)
        {
            this.Reset();

            this.CommunicationClient = null;

            this.CreateActionGraph(dimensionerDevice);

        }

        public void ShutDown()
        {
            this.State = CommunicationStateEnum.Disconnecting;

            if (this.CommunicationClient != null)
            {
                this.CommunicationClient.OnError -= new EventHandler<CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions.ExceptionEventArgs>(CommunicationClient_OnError);
            }

            this.Reset();
            
            this.CommunicationClient = null;
        }

        public void PerformAction()
        {
            this.State = this.currentAction.Perform();
            this.currentAction = currentAction.Next;
        }

        #endregion
    }
}
