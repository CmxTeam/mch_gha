﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver;
using CargoMatrix.Communication.CargoMeasurements.Communication.Protocol;
using CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions;


namespace CargoMatrix.Communication.CargoMeasurements.Communication.Protocol.Driver
{
    public class CommunicationProtocolDriverAsync : ICommunicationProtocolDriver
    {
        DimensionerDevice dimensionerDevice;
        CommunicatorAction resetAction;
        CommunicatorAction currentAction;
        CommunicatorAction initialAction;

        public IMettlerCommunicatorClient CommunicationClient {get; private set;}
        
        public CommunicationStateEnum State { get; private set; }


        public CommunicationProtocolDriverAsync(DimensionerDevice dimensionerDevice)
        {
            this.CreateActionGraph(dimensionerDevice);
        }

        private void CreateActionGraph(DimensionerDevice device)
        {
            this.dimensionerDevice = device;
            this.CommunicationClient = new MettlerCommunicatorClient(device.ServerIP.ToString(), device.ServerPort, device.IpAddress.ToString());
           
            var disconnectAction = new DisconnectAction(this.CommunicationClient);
            var connectAction = new ConnectActionAsync(this.CommunicationClient);
            var ipSendAction = new IPActionAsync(this.CommunicationClient);
            var aliveAction = new AliveActionAsync(this.CommunicationClient);

            this.resetAction = disconnectAction;
            this.initialAction = connectAction;
            this.currentAction = initialAction;

            this.initialAction.Next = aliveAction;
            aliveAction.Next = ipSendAction;
            ipSendAction.Next = ipSendAction;

            this.State = CommunicationStateEnum.Disconnected;
        }
       
        #region ICommunicationProtocolDriver Members

        public void Reset()
        {
            if (this.State == CommunicationStateEnum.Disconnected) return;

            this.State = this.resetAction.Perform();
            this.currentAction = this.initialAction;
        }

        public void Reset(DimensionerDevice dimensionerDevice)
        {
            this.Reset();

            this.CommunicationClient = null;

            this.CreateActionGraph(dimensionerDevice);
        }

        public void ShutDown()
        {
            this.Reset();

            this.CommunicationClient = null;
        }
        
        public void PerformAction()
        {
            this.State = this.currentAction.Perform();
            this.currentAction = currentAction.Next;
        }

        #endregion
    }
}
