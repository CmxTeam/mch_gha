﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoMeasurements.Communication.Exceptions
{
    public enum ExceptionCodeEnum
    {
        CONNECT = 1,
        SEND_IP = 2,
        SEND_CLOSE = 3,
        GET_RESPONSE = 4,
        STOP = 5,
        SEND_ALIVE = 6
    }
}
