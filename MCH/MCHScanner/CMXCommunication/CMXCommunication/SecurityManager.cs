﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using CargoMatrix.Communication.WSPieceScan;

namespace CargoMatrix.Communication
{
    public class CMXWSSecurity : CargoMatrix.Communication.WSPieceScan.WSPieceScan
    {
        string asmxName = "WSPieceScan.asmx";
        public CMXWSSecurity()
        {
            Url = Settings.Instance.URLPath + asmxName;
        }
        public string URL
        {
            get { return Url; }
        }

    }

    public class SecurityManager
    {
        private static SecurityManager instance;
        CMXWSSecurity wsSecurity;
        private SecurityManager()
        {
            wsSecurity = new CMXWSSecurity();
            
        }
        public static SecurityManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SecurityManager();
                }

                return instance;
            }

        }
        public int LoginByPin(string pin)
        {
            try
            {
                //CargoMatrix.Communication.WSPieceScan.Context wsUserProfile = new CargoMatrix.Communication.WSPieceScan.Context();
                //wsUserProfile.SessionGuid = Guid.NewGuid();
                //wsUserProfile.Gateway = Settings.Instance.Gateway;

                UserSession session = wsSecurity.LoginByPin(Settings.Instance.ConnectionName, pin, Platforms.MobileDevice);
                
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user = wsUserProfile;

                OpenNETCF.WindowsCE.DateTimeHelper.LocalTime = session.LocalTime;

                WebServiceManager.SetUserID(session.UserID, session.UserName, session.GroupID);
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.firstName = session.FirstName;
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.lastName = session.LastName;
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.pin = pin;
                if (session.Timeout == null)
                    session.Timeout = 30;

                GuidManager.SetGuid(session.SessionGuid.ToString(), (int)session.Timeout);
                //wsUserProfile.

                //GuidManager.TimeOut = CargoMatrix.Communication.WebServiceManager.Instance().m_user.Timeout;
            }
            catch (SoapException ex)
            {
                if (ex.Code.Name.ToUpper() == "Server".ToUpper())
                {
                    return -1;
                }
                else
                {
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11002);

                    return -2;
                }
            }
            catch(Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 11001);

                return -2;

            }
            
           
            return 0;


        }
        public int LoginByUserID(string username, string password)
        {

            try
            {
                Context wsUserProfile = new Context();
                
                //wsUserProfile.SessionGuid = Guid.NewGuid();
                //wsUserProfile.UserID = username;
                //wsUserProfile.Gateway = Settings.Instance.Gateway;
                //wsUserProfile.

                //CargoMatrix.Communication.WebServiceManager.Instance().m_user = new CargoMatrix.Communication.WSSecurity.DTOUserProfile();
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user.SessionGuid = Guid.NewGuid();
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user.UserID = username;
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user.Gateway = Settings.Instance.Gateway;

                UserSession session = wsSecurity.LoginByUserName(Settings.Instance.Gateway, username, password, Platforms.MobileDevice);
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user.SessionGuid = session.Context.SessionGuid;
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user.UserName = session.UserName;
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user.userID =  session.Context.UserID;
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user = Settings.Instance.Gateway;

              
                //CargoMatrix.Communication.WebServiceManager.Instance().m_user = wsUserProfile;
                                    
                OpenNETCF.WindowsCE.DateTimeHelper.LocalTime = session.LocalTime;// CargoMatrix.Communication.WebServiceManager.Instance().m_user.LocalTime;


                WebServiceManager.SetUserID(session.UserID, session.UserName, session.GroupID);
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.firstName = session.FirstName;
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.lastName = session.LastName;
                if (session.Timeout == null)
                    session.Timeout = 30;
                GuidManager.SetGuid(session.SessionGuid.ToString(), (int)session.Timeout);


                

                //GuidManager.TimeOut = CargoMatrix.Communication.WebServiceManager.Instance().m_user.Timeout;
            }
            catch (SoapException ex)
            {
                if (ex.Code.Name.ToUpper() == "Server".ToUpper())
                {
                    return -1;
                }
                else
                {
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11002);

                    return -2;
                }
            }
            catch(Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 11003);

                return -2;

            }
            
            return 0; // user found
        }

        public LoginTypes LoginType()
        {
            try
            {
                return wsSecurity.LoginPreference(Settings.Instance.Gateway);
            }
            catch (Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 110030);
                throw ex;
            
            }
        }
    }
}
