﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.CargoDischarge
{
    public enum SortingFieldsEnum
    {
        NA = 1,
        Driver = 2,
        Company = 3,
        ReleseTime = 4
    }
}
