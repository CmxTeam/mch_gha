﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.Common;

namespace CargoMatrix.Communication.CargoDischarge
{
    public class DischargeShipmentCondition : IShipmentCondition
    {
        ScannerUtilityWS.ShipmentCondition wsShipmentCondition;

        public DischargeShipmentCondition(ScannerUtilityWS.ShipmentCondition wsShipmentCondition)
        {
            this.wsShipmentCondition = wsShipmentCondition;
        }


        #region IShipmentCondition Members

        public IShipmentConditionType[] Conditions
        {
            get 
            {
                return
                    (
                        from item in this.wsShipmentCondition.Conditions
                        select new DischargeShipmentConditionType(item)
                    ).ToArray();
            }
        }

        public int Pieces
        {
            get 
            {
                return this.wsShipmentCondition.Pieces;
            }
        }

        public int PieceId
        {
            get 
            {
                return this.wsShipmentCondition.PieceId;
            }
        }

        #endregion
    }
}
