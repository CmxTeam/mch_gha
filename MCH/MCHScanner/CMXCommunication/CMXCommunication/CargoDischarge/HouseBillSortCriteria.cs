﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSCargoDischarge;

namespace CargoMatrix.Communication.CargoDischarge
{
    public class HouseBillSortCriteria
    {
        SortingFieldsEnum sortingField;

        public SortingFieldsEnum Value
        {
            get { return sortingField; }
            set { sortingField = value; }
        }

        public static SortingFieldsEnum DefaultValue
        {
            get
            {
                return SortingFieldsEnum.ReleseTime;
            }
        }

        public static SortingFieldsEnum NoSortingValue
        {
            get
            {
                return SortingFieldsEnum.NA;
            }
        }

        public HouseBillSortCriteria(SortingFieldsEnum sortingField)
        {
            this.sortingField = sortingField;
        }

        public HouseBillSortCriteria()
            : this(HouseBillSortCriteria.DefaultValue)
        { }

        /// <summary>
        /// overriden. Returns string representation of the sorting field
        /// </summary>
        /// <returns>Returns string representation of the sorting field</returns>
        public override string ToString()
        {
            return HouseBillSortCriteria.ToString(this.sortingField);
        }


        public static string ToString(SortingFieldsEnum sortingField)
        {
            switch (sortingField)
            {
                case SortingFieldsEnum.NA: return "All";

                case SortingFieldsEnum.Company: return "Company";

                case SortingFieldsEnum.Driver: return "Driver";

                case SortingFieldsEnum.ReleseTime: return "Release time";

                default: throw new NotImplementedException();
            }
        }

        public WSCargoDischarge.HouseBillFields ToWsCargoDischargeFileds()
        {
            switch (sortingField)
            {
                case SortingFieldsEnum.NA: return WSCargoDischarge.HouseBillFields.NA;

                case SortingFieldsEnum.Company: return WSCargoDischarge.HouseBillFields.ReleaseCompany;

                case SortingFieldsEnum.Driver: return WSCargoDischarge.HouseBillFields.ReleaseDriver;

                case SortingFieldsEnum.ReleseTime: return WSCargoDischarge.HouseBillFields.ReleaseDate;

                default: throw new NotImplementedException();
            }
        }
    }
}
