﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.WSCargoReceiver
{
    public partial class HouseBillItem : CargoMatrix.Communication.DTO.IHouseBillItem
    {
        #region IHouseBillItem Members


        //public string PackingMethod
        //{
        //    get { return this.; }
        //}

        public int Slac{
            get { return this.totalSlacField; }
        }

        int CargoMatrix.Communication.DTO.IHouseBillItem.Weight
        {
            get { return (int)this.weightField; }
        }

        public string Location
        {
            get { return this.lastLocationField; }
        }

        CargoMatrix.Communication.DTO.ScanModes CargoMatrix.Communication.DTO.IHouseBillItem.ScanMode
        {
            get
            {
                switch (this.scanModeField)
                {
                    case ScanModes.Piece:
                        return CargoMatrix.Communication.DTO.ScanModes.Piece;
                    case ScanModes.Count:
                        return CargoMatrix.Communication.DTO.ScanModes.Count;
                    case ScanModes.Slac:
                        return CargoMatrix.Communication.DTO.ScanModes.Slac;
                    case ScanModes.NA:
                    default:
                        return CargoMatrix.Communication.DTO.ScanModes.NA;
                };
            }
        }

        public CargoMatrix.Communication.DTO.HouseBillStatuses status
        {
            get
            {
                switch (this.statusField)
                {
                    case HouseBillStatuses.Completed:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Completed;
                    case HouseBillStatuses.InProgress:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.InProgress;
                    case HouseBillStatuses.Open:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Open;
                    case HouseBillStatuses.Pending:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.Pending;
                    case HouseBillStatuses.All:
                    default:
                        return CargoMatrix.Communication.DTO.HouseBillStatuses.All;
                }
            }
        }

        CargoMatrix.Communication.DTO.RemoveModes CargoMatrix.Communication.DTO.IHouseBillItem.RemoveMode
        {
            get
            {
                switch (this.removeModeField)
                {
                    case RemoveModes.All:
                        return CargoMatrix.Communication.DTO.RemoveModes.All;
                    case RemoveModes.Shortage:
                        return CargoMatrix.Communication.DTO.RemoveModes.Shortage;
                    case RemoveModes.TakeOff:
                        return CargoMatrix.Communication.DTO.RemoveModes.TakeOff;
                    case RemoveModes.NA:
                    default:
                        return CargoMatrix.Communication.DTO.RemoveModes.NA;
                };
            }
        }


        #endregion
        public string Reference
        {
            get { return string.Format("{0}-{1}-{2}", this.Origin, this.HousebillNumber, this.Destination); }
        }

        public string Line2
        {
            get { return this.aliasNumberField; }
        }

        public string Line3
        {
            get { return string.Format("Pieces: {0} of {1}  Slac: {2}", this.ScannedPieces, this.TotalPieces, this.Slac); }
        }

        public string Line4
        {
            get { return string.Format("Drop At: {0}", this.DropLocation); }
        }
    }
}
