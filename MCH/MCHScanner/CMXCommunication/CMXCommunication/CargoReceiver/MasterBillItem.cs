﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.WSCargoReceiver
{
    public partial class MasterBillItem : CargoMatrix.Communication.DTO.IMasterBillItem
    {
        #region IMasterBillItem Members


        int CargoMatrix.Communication.DTO.IMasterBillItem.Weight
        {
            get { return (int)this.weightField; }
        }

        DateTime CargoMatrix.Communication.DTO.IMasterBillItem.CutOff
        {
            get { return this.cutOffField.Value; }
        }

        CargoMatrix.Communication.DTO.MasterbillStatus CargoMatrix.Communication.DTO.IMasterBillItem.Status
        {
            get
            {
                switch (this.statusField)
                {
                    case MasterBillStatuses.Completed:
                        return CargoMatrix.Communication.DTO.MasterbillStatus.Completed;
                    case MasterBillStatuses.InProgress:
                        return CargoMatrix.Communication.DTO.MasterbillStatus.InProgress;
                    case MasterBillStatuses.Open:
                        return CargoMatrix.Communication.DTO.MasterbillStatus.Open;
                    case MasterBillStatuses.Pending:
                        return CargoMatrix.Communication.DTO.MasterbillStatus.Pending;
                    case MasterBillStatuses.All:
                    default:
                        return CargoMatrix.Communication.DTO.MasterbillStatus.All;
                };
            }
        }

        public CargoMatrix.Communication.WSLoadConsol.MOT Mot
        {
            get { return CargoMatrix.Communication.WSLoadConsol.MOT.Air; }
        }

        #endregion
    }
}
