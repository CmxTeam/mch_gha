﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using CargoMatrix.Communication.InventoryWS;
using CargoMatrix.Communication.Parsing.Results;
using CargoMatrix.Communication.Parsing.Data;

namespace CargoMatrix.Communication
{
    public class BarcodeParser
    {
        static Dictionary<ResultTypeEnum, ParsingPattern> patterns;


        private static CMXBarcode.BarcodeParser parser;

        /// <summary>
        /// Event will be rised when barcode has been prsed, 
        /// designe to suspend idle lock in case there is scan activity
        /// </summary>
        internal static event EventHandler BarcodParsed;

        public static CMXBarcode.BarcodeParser Instance
        {
            get
            {
                if (null == parser)
                {
                    parser = new CMXBarcode.BarcodeParser();
                }
                var barcodeParsedHandler = BarcodParsed;
                if (barcodeParsedHandler != null)
                    barcodeParsedHandler(null, EventArgs.Empty);
                return parser;
            }
        }

        private static void DownloadParsingRules()
        {
            var barcodePatterns = DataSource.GetBarcodePatterns();

            patterns = new Dictionary<ResultTypeEnum, ParsingPattern>();

            barcodePatterns.ForEach(pattern => BarcodeParser.patterns.Add(pattern.ResultType, pattern));
        }

        public static ParseResult Parse(string barcode)
        {

            foreach (var parsing in patterns.Values)
            {
                foreach (var pattern in parsing.Values)
                {
                    var match = Regex.Match(barcode, pattern.Regex);

                    if (match.Success == true)
                    {
                        return ParseExact(parsing.ResultType, barcode, pattern);
                    }
                }
            }

            return null;
        }

        public static string Reconstruct(ParsingElement[] elements, ResultTypeEnum resultType)
        {
            var pattern = patterns[resultType];

            if (pattern == null) return null;

            var patterntValue = pattern.Values.FirstOrDefault();

            if (patterntValue == null) return null;

            var elemnts = patterntValue.Elements.OrderBy(element => element.Order);

            return null;
        }

        private static ParseResult ParseExact(ResultTypeEnum resultType, string barcode, ParsingPattern.PatternValue parsingValue)
        {
            switch (resultType)
            {
                case ResultTypeEnum.MasterBill:

                    return new MasterBillResult(barcode, parsingValue).Parse();

                case ResultTypeEnum.HouseBill:

                    return new HouseBillResult(barcode, parsingValue).Parse();

                case ResultTypeEnum.Area:
                case ResultTypeEnum.Door:
                case ResultTypeEnum.Truck:
                case ResultTypeEnum.ScreeningArea:
                case ResultTypeEnum.Uld:
                case ResultTypeEnum.User:
                case ResultTypeEnum.Printer:
                    return new ParseResult(barcode, resultType, parsingValue).Parse();

                default: return new ParseResult(barcode, ResultTypeEnum.NA, null);
            }
        }
    }
}
