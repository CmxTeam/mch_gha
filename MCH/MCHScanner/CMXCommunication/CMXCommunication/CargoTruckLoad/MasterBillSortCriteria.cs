﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSLoadConsol;

namespace CargoMatrix.Communication.CargoTruckLoad
{
    public class MasterBillSortCriteria
    {
        SortingFieldsEnum sortingFields;

        public SortingFieldsEnum Value
        {
            get { return sortingFields; }
            set { sortingFields = value; }
        }

        public static SortingFieldsEnum DefaultValue
        {
            get
            {
                return SortingFieldsEnum.CutOff;
            }
        }

        public static SortingFieldsEnum NoSortingValue
        {
            get
            {
                return SortingFieldsEnum.NA;
            }
        }

        public MasterBillSortCriteria(SortingFieldsEnum sortingFields)
        {
            this.sortingFields = sortingFields;
        }

        public MasterBillSortCriteria()
            : this(MasterBillSortCriteria.DefaultValue)
        {
        }

        public override string ToString()
        {
            return MasterBillSortCriteria.ToString(this.sortingFields);
        }

        public static string ToString(SortingFieldsEnum sortingFields)
        {
            switch(sortingFields)
            {
                case SortingFieldsEnum.NA: return "All";

                case SortingFieldsEnum.CutOff: return "Cutoff Time";

                case SortingFieldsEnum.CarrierNumber: return "Carrier Number";

                case SortingFieldsEnum.MasterBillNumber: return "MasterBill";

                case SortingFieldsEnum.Origin: return "Origin";

                case SortingFieldsEnum.Weight: return "Weight";

                default: throw new NotImplementedException();

            }
        }

        public MasterBillFields ToWsMasterBillFields()
        {
            switch (this.sortingFields)
            {
                case SortingFieldsEnum.NA: return MasterBillFields.NA;

                case SortingFieldsEnum.CutOff: return MasterBillFields.CutOff;

                case SortingFieldsEnum.CarrierNumber: return MasterBillFields.CarrierNumber;

                case SortingFieldsEnum.MasterBillNumber: return MasterBillFields.MasterBillNumber;

                case SortingFieldsEnum.Origin: return MasterBillFields.Origin;

                case SortingFieldsEnum.Weight: return MasterBillFields.Weight;
    

                default: throw new NotImplementedException();
            }
        }
            
    }
}
