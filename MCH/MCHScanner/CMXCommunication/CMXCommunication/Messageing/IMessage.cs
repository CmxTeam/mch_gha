﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.Messageing
{
    public interface IMessage
    {
        string Body
        {
            get;
            set;
        }

        System.DateTime DateCreated
        {
            get;
            set;
        }

        System.DateTime DateReceived
        {
            get;
            set;
        }

        int ID
        {
            get;
            set;
        }

        bool IsUnread
        {
            get;
            set;
        }

        MessagePriority Priority
        {
            get;
            set;
        }

        MessageType MessageDirection
        {
            get; set;
        }

        int[] ReceiverIds
        {
            get;
            set;
        }

        bool ResponseRequired
        {
            get;
            set;
        }

        /// <summary>
        /// the previous msg id if the message is response
        /// </summary>
        int ResponseTo
        {
            get;
            set;
        }

        int SenderId
        {
            get;
            set;
        }
        string SenderName
        { get; set; }
    }
}
