﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.DTO;

namespace CargoMatrix.Communication.Common
{
    public class HousebillFilterCriteria
    {
        FilteringFieldsEnum filteringField;

        public FilteringFieldsEnum Value
        {
            get { return filteringField; }
            set { filteringField = value; }
        }

        public static FilteringFieldsEnum DefaultValue
        {
            get
            {
                return FilteringFieldsEnum.NOT_COMPLETED;
            }
        }

        public static FilteringFieldsEnum NoSortingValue
        {
            get
            {
                return FilteringFieldsEnum.ALL;
            }
        }

        public HousebillFilterCriteria(FilteringFieldsEnum filteringFieldsEnum)
        {
            this.filteringField = filteringFieldsEnum;
        }

        public HousebillFilterCriteria()
            : this(HousebillFilterCriteria.DefaultValue)
        { }

        public static string ToString(FilteringFieldsEnum filteringField)
        {
            switch (filteringField)
            {
                case FilteringFieldsEnum.ALL: return "All";

                case FilteringFieldsEnum.COMPLETED: return "Complete";

                case FilteringFieldsEnum.IN_PROGRESS: return "In Progress";

                case FilteringFieldsEnum.NOT_COMPLETED: return "Not Complete";

                case FilteringFieldsEnum.NOT_STARTED: return "Not Started";

                default: throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return HousebillFilterCriteria.ToString(this.filteringField);
        }



        public WSCargoDischarge.HouseBillStatuses ToWSCargoDischargeStatus()
        {
            switch (this.filteringField)
            {
                case FilteringFieldsEnum.ALL: return WSCargoDischarge.HouseBillStatuses.All;

                case FilteringFieldsEnum.COMPLETED: return WSCargoDischarge.HouseBillStatuses.Completed;

                case FilteringFieldsEnum.IN_PROGRESS: return WSCargoDischarge.HouseBillStatuses.InProgress;

                case FilteringFieldsEnum.NOT_COMPLETED: return WSCargoDischarge.HouseBillStatuses.Open;

                case FilteringFieldsEnum.NOT_STARTED: return WSCargoDischarge.HouseBillStatuses.Pending;

                default: throw new NotImplementedException();
            }
        }

        public WSLoadConsol.MasterBillStatuses ToWsMasterBillStatuses()
        {
            switch (this.filteringField)
            {
                case FilteringFieldsEnum.ALL: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.All;

                case FilteringFieldsEnum.COMPLETED: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.Completed;

                case FilteringFieldsEnum.IN_PROGRESS: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.InProgress;

                case FilteringFieldsEnum.NOT_COMPLETED: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.Open;

                case FilteringFieldsEnum.NOT_STARTED: return CargoMatrix.Communication.WSLoadConsol.MasterBillStatuses.Pending;

                default: throw new NotImplementedException();

            }
        }
    }
}
