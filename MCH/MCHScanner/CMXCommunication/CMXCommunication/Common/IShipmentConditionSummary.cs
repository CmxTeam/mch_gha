﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CargoMatrix.Communication.Common
{
    public interface IShipmentConditionSummary
    {
        string ConditionTypeName
        {
            get;
        }

        int ConditionTypeId
        {
            get;
        }


        string ConditionTypeCode
        {
            get;
        }

        
        int Pieces
        {
            get;
        }

        bool ContainsAqm
        {
            get;
        }
    }
}
