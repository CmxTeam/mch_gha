﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using CargoMatrix.Communication.WSScannerMCHService;

namespace CargoMatrix.Communication
{
    public class WSScannerMCHServiceObj : CargoMatrix.Communication.WSScannerMCHService.ScannerMCHService
    {
        //string path = "http://10.0.0.235/ScannerServices/";
        string asmxName = "ScannerMCHService.asmx";
        public WSScannerMCHServiceObj()
        {
            //Do not Update from here
            //if (path == string.Empty)
            //{
                Url = Settings.Instance.AppURLPath + asmxName;
            //}
            //else
            //{
            //    Url = path + asmxName;
            //}

        }
        public string URL
        {
            get { return Url; }
        }

    }

    public class ScannerMCHServiceManager
    {

        //private static string gateway = "BOS";
        //private static string connectionName = "BOS";

        private static ScannerMCHServiceManager instance;
        WSScannerMCHServiceObj ws;
        private ScannerMCHServiceManager()
        {
            ws = new WSScannerMCHServiceObj();
        }
        public static ScannerMCHServiceManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ScannerMCHServiceManager();
                }

                return instance;
            }

        }

        public string GetLocationMCH()
        {
            //string currentConnectionName = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentConnectionName = Settings.Instance.ConnectionName;
            //}
            //else
            //{
            //    currentConnectionName = ScannerMCHServiceManager.connectionName;
            //}
            //return currentConnectionName;
            return GetConnectionName();
        }


        public string GetConnectionName()
        {

            //if (CargoLoaderMCHService.connectionName == string.Empty)
            //{
            return Settings.Instance.ConnectionName;
            //}
            //else
            //{
            //    return CargoLoaderMCHService.connectionName;
            //}

        }


        public int LoginByPin(string pin)
        {
            try
            {
 
                //string currentConnectionName = string.Empty;
                //if (ScannerMCHServiceManager.connectionName == string.Empty)
                //{
                //    currentConnectionName = Settings.Instance.ConnectionName;
                //}
                //else
                //{
                //    currentConnectionName = ScannerMCHServiceManager.connectionName;
                //}


                UserSession session = ws.LoginByPin(GetConnectionName(), pin);

                if (session == null) { return 1; }
                

                OpenNETCF.WindowsCE.DateTimeHelper.LocalTime = session.LocalTime;



                WSPieceScan.UserTypes userTypes;
                if (session.GroupID == UserTypes.Admin)
                {
                    userTypes = CargoMatrix.Communication.WSPieceScan.UserTypes.Admin;
                }
                else
                {
                    userTypes = CargoMatrix.Communication.WSPieceScan.UserTypes.User;
                }


                WebServiceManager.SetUserID((int)session.UserID, session.UserName, userTypes);
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.firstName = session.FirstName;
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.lastName = session.LastName;
                CargoMatrix.Communication.WebServiceManager.Instance().m_user.pin = pin;
                
                if (session.Timeout == null)
                    session.Timeout = 30;

                GuidManager.SetGuid(session.SessionGuid.ToString(), (int)session.Timeout);
            
            }
            catch (SoapException ex)
            {
                if (ex.Code.Name.ToUpper() == "Server".ToUpper())
                {
                    return -1;
                }
                else
                {
                    CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 11002);

                    return -2;
                }
            }
            catch (Exception e)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(e, 11001);

                return -2;

            }


            return 0;


        }




        public long GetLocationIdByLocationBarcodeMCH(int userId, string gateway, string location)
        {
            try
            {

   
                //string currentGateway = string.Empty;
                //if (ScannerMCHServiceManager.connectionName == string.Empty)
                //{
                //    currentGateway = gateway;
                //}
                //else
                //{
                //    currentGateway = ScannerMCHServiceManager.gateway;
                //}

                return ws.GetLocationIdByLocationBarcode(GetConnectionName(), location);

            }
            catch
            {
                return 0;
            }




        }


        public WSScannerMCHService.WarehouseLocation[] GetWarehouseLocationsMCH(int userId, string gateway, string connection, LocationType locationType)
        {

            //string currentConnectionName = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentConnectionName = connection;
            //}
            //else
            //{
            //    currentConnectionName = ScannerMCHServiceManager.connectionName;
            //}


            //string currentGateway = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentGateway = gateway;
            //}
            //else
            //{
            //    currentGateway = ScannerMCHServiceManager.gateway;
            //}


            WSScannerMCHService.WarehouseLocation[] locationitems = ws.GetWarehouseLocations(GetConnectionName(), locationType);


            return locationitems;
        }


        public WSScannerMCHService.MainMenuItem[] GetMainMenuMCH(int userId, string gateway, string connection)
        {

            //string currentConnectionName = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentConnectionName = connection;
            //}
            //else
            //{
            //    currentConnectionName = ScannerMCHServiceManager.connectionName;
            //}


            //string currentGateway = string.Empty;
            //if (ScannerMCHServiceManager.connectionName == string.Empty)
            //{
            //    currentGateway = gateway;
            //}
            //else
            //{
            //    currentGateway = ScannerMCHServiceManager.gateway;
            //}


             int v = 0;
             WSScannerMCHService.MainMenuItem[] menuitems = ws.GetMainMenu("", userId, GetConnectionName(), GetConnectionName(), out v);
            

            return menuitems;
        }


        public bool AddTaskSnapshotReference(long taskId, string reference)
        {

            try
            { 
               ws.AddTaskSnapshotReference(taskId, reference);

            return true;
            }
            catch
            {
                return false;
            }
         
        }


    }
}
