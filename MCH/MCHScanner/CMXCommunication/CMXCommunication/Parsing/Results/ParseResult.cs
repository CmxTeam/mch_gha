﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CargoMatrix.Communication.Parsing.Results
{
    public class ParseResult
    {
        string barcode;
        ResultTypeEnum resultType;
        protected string iD;
        protected ParsingElement[] elements;
        private string delimeter;
        private string prefix;

        public string ID
        {
            get { return iD; }
        }

        public ResultTypeEnum ResultType
        {
            get { return resultType; }

        }

        public string Barcode
        {
            get { return barcode; }
        }

        public string Delimeter
        {
            get { return delimeter; }
        }

        public string Prefix
        {
            get { return prefix; }
        }

        public ParseResult(string barcode, ResultTypeEnum resultType, ParsingPattern.PatternValue patternValue)
        {
            this.barcode = barcode;
            this.resultType = resultType;
            this.elements = patternValue.Elements;
        }


        public virtual ParseResult Parse()
        {
            string tempBarcode = this.barcode;

            foreach (var element in this.elements.OrderBy(el => el.Order))
            {
                if (string.IsNullOrEmpty(tempBarcode) == true) return this;

                var match = Regex.Match(tempBarcode, string.Format("^{0}", element.Regex));

                if (match.Success == true)
                {
                    this.EmbedValue(match.Value, element.Name);
                    element.Value = match.Value;
                }

                tempBarcode = tempBarcode.Remove(match.Index, match.Length);
            }

            return this;

        }

        protected virtual void EmbedValue(string value, string name)
        {
            switch (name.ToLower())
            {
                case "id":
                    this.iD = value;
                    break;

                case "prefix":
                    this.prefix = value;
                    break;

                case "delimiter":
                    this.delimeter = value;

                    break;
            }
        }

        public string Reconstruct(string ID)
        {
            StringBuilder strBuilder = new StringBuilder();

            foreach (var element in this.elements.OrderBy(el => el.Order))
            {
                switch(element.Name.ToLower())
                {
                    case "id":
                        strBuilder.Append(ID);
                        break;

                    default:
                        strBuilder.Append(element.Value);
                        break;
                }
            }

            return strBuilder.ToString();
        }

        public virtual ScanObjectLegacy AsScanObject()
        {
            ScanObjectLegacy result = new ScanObjectLegacy();

            result.BarcodeData = barcode;
            result.BarcodeType = this.resultType.AsCMXBarcodeType();

            switch (this.resultType)
            {
                case ResultTypeEnum.Area:
                case ResultTypeEnum.Door:
                case ResultTypeEnum.ScreeningArea:
                case ResultTypeEnum.Truck:
                    result.Location = this.ID;
                    break;

                case ResultTypeEnum.Uld:
                    result.UldNumber = this.ID;
                    break;

                case ResultTypeEnum.User:
                    try
                    {
                        result.UserNumber = int.Parse(this.ID);
                    }
                    catch { }

                    break;

                case ResultTypeEnum.Printer:
                    result.PrinterName = this.ID;
                    break;
            }

            return result;
        }
    }


    public static class Extentions
    {
        public static ScanObjectLegacy.BarcodeTypes AsCMXBarcodeType(this ResultTypeEnum barcodeType)
        {
            switch (barcodeType)
            {
                case ResultTypeEnum.Area: return ScanObjectLegacy.BarcodeTypes.Area;

                case ResultTypeEnum.Door: return ScanObjectLegacy.BarcodeTypes.Door;

                case ResultTypeEnum.HouseBill: return ScanObjectLegacy.BarcodeTypes.HouseBill;

                case ResultTypeEnum.MasterBill: return ScanObjectLegacy.BarcodeTypes.MasterBill;

                case ResultTypeEnum.NA: return ScanObjectLegacy.BarcodeTypes.NA;

                case ResultTypeEnum.Printer: return ScanObjectLegacy.BarcodeTypes.Printer;

                case ResultTypeEnum.ScreeningArea: return ScanObjectLegacy.BarcodeTypes.ScreeningArea;

                case ResultTypeEnum.Truck: return ScanObjectLegacy.BarcodeTypes.Truck;

                case ResultTypeEnum.Uld: return ScanObjectLegacy.BarcodeTypes.Uld;

                case ResultTypeEnum.User: return ScanObjectLegacy.BarcodeTypes.User;

                default: return ScanObjectLegacy.BarcodeTypes.NA;
            }

        }
    }
    
}
