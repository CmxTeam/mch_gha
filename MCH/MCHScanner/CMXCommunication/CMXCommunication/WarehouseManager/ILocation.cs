﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoMatrix.Communication.WarehouseManager
{
    public interface ILocation
    {
        int RecID
        {
            get;
            set;
            
        }

        /// <remarks/>
        int TypeID
        {
            get;
            set;
        }

        /// <remarks/>
        string TypeName
        {
            get;
            set;
        }

        /// <remarks/>
        int GatewayID
        {
            get;
            set;
        }

        /// <remarks/>
        string GatewayCode
        {
            get;
            set;
        }

        /// <remarks/>
        int WarehouseID
        {
            get;
            set;
        }

        /// <remarks/>
        string WarehouseName
        {
            get;
            set;
        }

        /// <remarks/>
        int RegionTypeID
        {
            get;
            set;
        }

        /// <remarks/>
        string RegionTypeName
        {
            get;
            set;
        }

        /// <remarks/>
        string Code
        {
            get;
            set;
        }

        /// <remarks/>
        string Name
        {
            get;
            set;
        }

        /// <remarks/>
        string Description
        {
            get;
            set;
        }

        /// <remarks/>
        int Flags
        {
            get;
            set;
        }

        /// <remarks/>
        int SortOrder
        {
            get;
            set;
        }

        /// <remarks/>
        string Proximity
        {
            get;
            set;
        }

        /// <remarks/>
        string ProximityName
        {
            get;
            set;
        }

        /// <remarks/>
        bool RFIDEnabled
        {
            get;
            set;
        }

        /// <remarks/>
        string RFIDSetupID
        {
            get;
            set;
        }

        /// <remarks/>
        int NoOfHouseBills
        {
            get;
            set;
        }

        /// <remarks/>
        int NoOfPieces
        {
            get;
            set;
        }

        /// <remarks/>
        double TotalWeight
        {
            get;
            set;
        }

        LocationTypeEnum LocationType
        {
            get;
            set;
        }

        void SetLocationType(CMXBarcode.BarcodeTypes barcodeType);
    }
}
