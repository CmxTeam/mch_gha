﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WarehouseManager;

namespace CargoMatrix.Communication.WSWarehouseManager
{
    public partial class WarehouseLocationListItem : ILocation
    {
        LocationTypeEnum locationType;

        public LocationTypeEnum LocationType
        {
            get 
            { 
                return locationType; 
            }
            set { locationType = value; }
        }

        public void SetLocationType(CMXBarcode.BarcodeTypes barcodeType)
        {
            switch (barcodeType)
            {
                case CMXBarcode.BarcodeTypes.Area: this.locationType = LocationTypeEnum.Area;
                    break;

                case CMXBarcode.BarcodeTypes.Door: this.locationType = LocationTypeEnum.Door;
                    break;

                case CMXBarcode.BarcodeTypes.ScreeningArea: this.locationType = LocationTypeEnum.ScreeningArea;
                    break;
            }
        }
    }
}
