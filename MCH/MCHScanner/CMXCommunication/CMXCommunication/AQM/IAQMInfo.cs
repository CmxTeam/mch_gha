﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CargoMatrix.Communication.WSAQM;

namespace CargoMatrix.Communication.DTO
{
    public enum AQMType { MAWB,HAWB}
    public interface IAQMInfo
    {
        int ID { get; }
        string CarrierNo { get; }
        string AwbNo { get; }
        string Origin { get; }
        string Destination { get; }
        int TotalPieces { get; }
        double Weight { get; }
        AqmQueue[] Queues { get; }
        AQMType Type { get; }
        string Reference { get; }
    }
}
