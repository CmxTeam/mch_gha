﻿using System;
using System.Linq;
using System.Collections.Generic;
using CargoMatrix.Communication.InventoryWS;

namespace CargoMatrix.Communication
{
    public class Inventory : CMXServiceWrapper
    {
        private WsInventory service;
        private static Inventory _instance;
        private string asmxName = "WsInventory.asmx";

        public static Inventory Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Inventory();
                return _instance;
            }
        }
        private Inventory()
        {
            service = new CargoMatrix.Communication.InventoryWS.WsInventory();
            service.Url = Settings.Instance.URLPath + asmxName;

        }

        public CargoMatrix.Communication.ScannerUtilityWS.TaskSettings GetTaskSettings()
        {
            return ScannerUtility.Instance.GetTaskSettings(CargoMatrix.Communication.ScannerUtilityWS.TaskTypes.Inventoy);
        }

        public int DownloadHousebillDetails(string hawbNo)
        {
            Func<int> f = () =>
            {
                return service.DownloadHousebillDetails(connectionName, hawbNo, userId);
            };

            return Invoke<int>(f, 11091, 0);
        }

        public TransactionStatus DropAllPiecesIntoLocation(int taskId, string locName, int fLiftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropAllPiecesIntoLocation(connectionName, taskId, userId, locName, fLiftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11092, defRet);
        }

        public TransactionStatus DropPiecesIntoLocation(PieceItem[] pieces, int taskId, string locName, ScanTypes scanType, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropPiecesIntoLocation(connectionName, pieces, taskId, userId, scanType, locName, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11093, defRet);
        }

        public DownloadStatus GetDownloadStatus(int dloadId)
        {
            Func<DownloadStatus> f = () =>
            {
                return service.GetDownloadStatus(connectionName, dloadId);
            };
            return Invoke<DownloadStatus>(f, 11094, DownloadStatus.Failed);
        }

        public HouseBillItem[] GetForkLiftHouseBills(int taskId, int forkliftId)
        {
            return GetForkLiftHouseBills(taskId, HouseBillFields.HousebillNumber, forkliftId);
        }

        public HouseBillItem[] GetForkLiftHouseBills(int taskId, HouseBillFields sort, int forkliftId)
        {
            Func<HouseBillItem[]> f = () =>
            {
                return service.GetForkLiftHouseBills(connectionName, taskId, userId, sort, forkliftId);
            };

            return Invoke<HouseBillItem[]>(f, 11095, new HouseBillItem[0]);
        }

        public int GetForkliftItemsCount(int taskId, int forkliftId)
        {
            Func<int> f = () =>
            {
                return service.GetForkLiftHouseBillsCount(connectionName, taskId, userId, forkliftId);
            };

            return Invoke<int>(f, 11096, 0);
        }

        public HouseBillItem GetHousebillInfo(int hawbId, int taskId)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.GetHouseBillInfo(connectionName, hawbId, taskId);
            };

            return Invoke<HouseBillItem>(f, 11097, new HouseBillItem() { Pieces = new PieceItem[0] });
        }

        public List<ShipmentAlert> GetHousebillAlerts(int hawbId, int taskId)
        {
            Func<List<ShipmentAlert>> f = () =>
            {
                return service.GetHouseBillsAlerts(connectionName, hawbId, taskId, WebServiceManager.Instance().m_user.UserName).ToList<ShipmentAlert>();
            };
            return Invoke<List<ShipmentAlert>>(f, 11098, new List<ShipmentAlert>());
        }

        public TransactionStatus RemovePieceFromForklift(int hawbId, int pieceId, int taskId, int fLiftId, ScanModes mode)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemovePieceFromForkLift(connectionName, hawbId, pieceId, taskId, userId, mode, fLiftId);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11104, defRet);

        }

        //public void ParseBarcode(string barcode, int taskId)
        //{
        //    try
        //    {
        //        service.ScanBarcode(connectionName, barcode, taskId);
        //    }
        //    catch (Exception ex)
        //    {
        //        CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 110105);
        //    }
        //    return new ScanItem() { BarcodeType = BarcodeTypes.NA, BarcodeData = barcode };
        //}


        public TransactionStatus CreateHousebill(string hawbNo, string origin, string dest, int pieces, int taskId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.CreateHousebillSkel(connectionName, origin, hawbNo, dest, pieces, taskId, userId);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11010, defRet);
        }

        public TransactionStatus FinalizeInventory(int taskId, int forkliftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.FinalizeInventory(connectionName, taskId, userId, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11011, defRet);
        }

        public TransactionStatus ReportMissingPiecesInCount(int hawbId, int count, int taskId, string remark)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.ReportMissingPieces(connectionName, hawbId, count, taskId, userId, ScanModes.Count, remark);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11012, defRet);
        }

        public TransactionStatus ReportMissingPiecesInPiece(int hawbId, int pieceId, int taskId, string remark)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.ReportMissingPieces(connectionName, hawbId, pieceId, taskId, userId, ScanModes.Piece, remark);
            };

            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11013, defRet);
        }

        public TransactionStatus ReportOverPiecesInCount(int hawbId, int count, int taskId, string remark)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.ReportOverPieces(connectionName, hawbId, count, taskId, userId, ScanModes.Count, remark);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11014, defRet);
        }

        public TransactionStatus ReportOverPiecesInPiece(int hawbId, int pieceId, int taskId, string remark)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.ReportOverPieces(connectionName, hawbId, pieceId, taskId, userId, ScanModes.Piece, remark);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11015, defRet);
        }

        public PieceItem[] GetNotScannedPieces(int hawbId, int taskId)
        {
            Func<PieceItem[]> f = () =>
            {
                return service.GetNotInventoryPiecesInfo(connectionName, hawbId, taskId);
            };
            return Invoke<PieceItem[]>(f, 110116, new PieceItem[0]);
        }

        public HouseBillItem[] GetDiscrepancyReport(int taskId)
        {
            Func<HouseBillItem[]> f = () =>
            {
                return service.GetInventoryReport(connectionName, taskId, userId);
            };
            return Invoke<HouseBillItem[]>(f, 11017, new HouseBillItem[0]);
        }

        public HouseBillItem ScanHousebillIntoForkliftLocation(string hawbNo, int pieceIdOrCount, int taskId, int fliftId, ScanTypes scanType, string loc, bool force)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.ScanPieceNumberIntoForkLiftLocation(connectionName, hawbNo, pieceIdOrCount, taskId, userId, scanType, fliftId, loc, force);
            };
            var defRet = new HouseBillItem() { Transaction = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage } };
            return Invoke<HouseBillItem>(f, 11018, defRet);

        }

        public HouseBillItem ScanPiecesIntoForkliftLocation(int hawbId, int noOfPieces, int taskId, int fLiftId, ScanTypes scanType, string loc, bool force)
        {
            Func<HouseBillItem> f = () =>
            {
                return service.ScanPieceCountIntoForkLiftLocation(connectionName, hawbId, noOfPieces, taskId, userId, scanType, fLiftId, loc, force);
            };
            var defRet = new HouseBillItem() { Transaction = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage } };
            return Invoke<HouseBillItem>(f, 11019, defRet);
        }

        public TaskItem[] GetInventoryTasks()
        {
            Func<TaskItem[]> f = () =>
            {
                return service.GetTasks(connectionName, userId);
            };
            return Invoke<TaskItem[]>(f, 11020, new TaskItem[0]);

        }
        public TransactionStatus RemoveHousebillFromForklift(int hawbId, int taskId, int fLiftId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveHousebillFromForklift(connectionName, hawbId, taskId, userId, fLiftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11021, defRet);
        }

        public ReturnReasonItem[] GetReturnReasons()
        {
            Func<ReturnReasonItem[]> f = () =>
            {
                return service.GetReturnReasons(connectionName);
            };
            var defRet = new ReturnReasonItem[0];
            return Invoke<ReturnReasonItem[]>(f, 11022, defRet);
        }

        public TransactionStatus ReturnDischargedShipment(int hawbId, int reasonId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.ReturnDischargeShipment(connectionName, hawbId, reasonId, userId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11023, defRet);
        }

        public UldObject ScanUldIntoForklift(string uldNo, int taskId, int forkliftId, ScanTypes scanType)
        {
            Func<UldObject> f = () =>
            {
                return service.ScanUldIntoForklift(connectionName, uldNo, taskId, userId, scanType, forkliftId);
            };
            var defRet = new UldObject() { TransactionStaus = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage } };
            return Invoke<UldObject>(f, 11024, defRet);
        }

        public UldObject GetUldDetails(string uldNo)
        {
            Func<UldObject> f = () =>
            {
                return service.GetUldDetails(connectionName, uldNo);
            };
            var defRet = new UldObject() { TransactionStaus = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage } };
            return Invoke<UldObject>(f, 11025, defRet);
        }

        public UldObject CreateNewUld(string uldNo, int UldTypeID, string carrier)
        {
            Func<UldObject> f = () =>
            {
                return service.CrateNewULD(connectionName, uldNo, UldTypeID, carrier, userId);
            };
            var defRet = new UldObject() { TransactionStaus = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage } };
            return Invoke<UldObject>(f, 11026, defRet);
        }

        public UldObject[] GetForkliftULds(int forkliftId, int taskId)
        {
            Func<UldObject[]> f = () =>
            {
                return service.GetForkliftULds(connectionName, forkliftId, taskId, userId);
            };
            var defRet = new UldObject[0];
            return Invoke<UldObject[]>(f, 11027, defRet);
        }

        public TransactionStatus RemoveUldFromForklift(int uldId, int forkliftId, int taskId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.RemoveUldFromForklift(connectionName, uldId, taskId, userId, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11028, defRet);
        }

        public TransactionStatus DropForkliftUldsIntoLocation(int forkliftId, string locName, int taskId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropForkliftUldsIntoLocation(connectionName, taskId, userId, locName, ScanTypes.Automatic, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11029, defRet);
        }

        public TransactionStatus DropUldsIntoLocation(int[] uldIds, int forkliftId, string locName, int taskId)
        {
            Func<TransactionStatus> f = () =>
            {
                return service.DropUldsIntoLocation(connectionName, uldIds, taskId, userId, locName, ScanTypes.Manual, forkliftId);
            };
            var defRet = new TransactionStatus() { TransactionStatus1 = false, TransactionError = failMessage };
            return Invoke<TransactionStatus>(f, 11030, defRet);
        }

    }


    public static class Extensions
    {
        public static string Reference(this HouseBillItem hawb)
        {
            return hawb.Origin + "-" + hawb.HousebillNumber + "-" + hawb.Destination;
        }
        public static string Pieces(this HouseBillItem hawb)
        {
            return hawb.ScannedPieces + " of " + hawb.TotalPieces + ": Slac " + hawb.TotalSlac;
        }
    }
}
