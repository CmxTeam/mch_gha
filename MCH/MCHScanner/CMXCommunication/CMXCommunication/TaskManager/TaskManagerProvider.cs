﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using CargoMatrix.Communication.WSTaskManager;
using CargoMatrix.Communication.DTO;
using System.Xml.Linq;

namespace CargoMatrix.Communication.TaskManager
{
    public class TaskManagerProvider : CMXServiceWrapper
    {
        private string asmxName = "tasks.asmx";
        private Tasks service;
        private static TaskManagerProvider instance;



        public static TaskManagerProvider Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new TaskManagerProvider();
                }

                return instance;
            }
        }

        private string connectionName
        {
            get
            {
                return WebServiceManager.Instance().m_connectionString;
            }
        }

        public string GateWayCode
        {
            get
            {
                return WebServiceManager.Instance().m_gateway;
            }
        }

        public TaskManagerProvider()
        {
            this.service = new Tasks();
            service.Url = Settings.Instance.URLPath + asmxName;
        }

        private User CurrentUser
        {
            get
            {
                return CargoMatrix.Communication.WebServiceManager.Instance().m_user;
            }
        }


        public BusinessTask[] GetTasks(string taskID, TaskProgressTypeEnum progreesType)
        {
            Func<BusinessTask[]> f = () =>
                {
                    string dateStr = DateTime.Now.ToString("MM/dd/yyyy");

                    var xmlNode = this.service.GetTasks(progreesType.AsString(), this.GateWayCode, string.Empty, taskID, dateStr, string.Empty, string.Empty, this.connectionName);

                    var xDoc = XDocument.Load(new System.Xml.XmlNodeReader(xmlNode));  // Parse(xmlNode.OuterXml);

                    var result = from taskRow in xDoc.Elements("Tasks").Elements("row")
                                 select new BusinessTask(taskRow.Attribute("RecordID").Value, 
                                                         taskRow.Attribute("Users") != null ? taskRow.Attribute("Users").Value.Split(new char[]{',', ' '}) : new string[0],
                                                         taskRow.Attribute("MULDS") != null ? taskRow.Attribute("MULDS").Value.Split(new char[] { ',', ' ' }) : new string[0])
                                 {
                                     MasterBillNumber = taskRow.Attribute("MasterbillNo") != null ? taskRow.Attribute("MasterbillNo").Value : null,
                                     Destination = taskRow.Attribute("Destination") != null ? taskRow.Attribute("Destination").Value : null,
                                     CarrierNumber = taskRow.Attribute("CarrierNumber") != null ? taskRow.Attribute("CarrierNumber").Value : null,
                                     Reference = taskRow.Attribute("Reference").Value != null ? taskRow.Attribute("Reference").Value : null,
                                     HouseBillNumber = taskRow.Attribute("AirbillNo")!= null ? taskRow.Attribute("AirbillNo").Value : null,
                                     ActionID = taskRow.Attribute("ActionID") != null ? taskRow.Attribute("ActionID").Value : null,
                                     Weight = taskRow.Attribute("Gweight") != null ? taskRow.Attribute("Gweight").Value : null,
                                     Pieces = taskRow.Attribute("MPCS") != null ? taskRow.Attribute("MPCS").Value : null,
                                     Percent = taskRow.Attribute("PCT") != null ? taskRow.Attribute("PCT").Value : null,
                                     Description = taskRow.Attribute("Description") != null ? taskRow.Attribute("Description").Value : null,
                                     Status = taskRow.Attribute("Status") != null ? BusinessTask.ToBusinessTaskStatus(taskRow.Attribute("Status").Value) : null,
                                     //ProcessID = taskRow.Attribute("ProcessID") != null ? taskRow.Attribute("ProcessID").Value : null,
                                 };

                    return result.ToArray();

                };

            return this.Invoke<BusinessTask[]>(f, 1100001, new BusinessTask[0]);
        }

        public void GetUsersByCriterion(string[] userIDs, out List<IBusinessUser> confirmCriterion, out List<IBusinessUser> inConfirmCriterion)
        {
            var cCriterion = new List<IBusinessUser>();
            var inCCriterion = new List<IBusinessUser>();

            Action action = () =>
            {
                var users = CargoMatrix.Communication.ScannerUtility.Instance.GetSameDepartmentUsers();

                List<string> userNamesList = new List<string>(userIDs);
                
                foreach (var user in users)
                {
                    if (userNamesList.Exists(userID => string.Compare(userID, user.UserId, StringComparison.OrdinalIgnoreCase) == 0) == true)
                    {
                        cCriterion.Add(user);
                    }
                    else
                    {
                        inCCriterion.Add(user);
                    }
                }
            };

            this.Invoke(action, 1100002);

            confirmCriterion = cCriterion;
            inConfirmCriterion = inCCriterion;
        }

        public void AssignUsers(BusinessTask task, IBusinessUser taskOwner, IBusinessUser[] users)
        {
            Action action = () =>
                {
                    string[] userIds = (from user in users
                                          select user.UserId).ToArray();

                    string newUsers = string.Join(",", userIds);

                    string originalUsers = string.Join(",", task.UserIDs.ToArray());

                    this.service.AssignUsers(originalUsers, newUsers, taskOwner.UserId, task.ID, this.connectionName);
                };

            this.Invoke(action, 1100003);
        }


        public void AssignUser(BusinessTask task, IBusinessUser user)
        {
            Action action = () =>
                {
                    this.service.AssignUser(user.UserId, task.ID, this.connectionName);
                };

            this.Invoke(action, 1100004);
        }

        public Task GetTask(int taskID)
        {
            Func<Task> f = () =>
            {
                return this.service.GetTask(this.connectionName, taskID);
            };

            return this.Invoke<Task>(f, 1100005, null);
        }

        public Task GetTask(string taskID)
        {
            int taskIDNumber;
            
            try
            {
                taskIDNumber = int.Parse(taskID);
            }
            catch (System.Exception ex)
            {
                CargoMatrix.ExceptionManager.CMXExceptionManager.DisplayException(ex, 1100006);
                
                return null;
            }

            return GetTask(taskIDNumber);
        }

    }


    public static class Hepler
    {
        public static string AsString(this TaskProgressTypeEnum progreesType)
        {
            switch(progreesType)
            {
                case TaskProgressTypeEnum.Completed: return "COMPLETED";

                case TaskProgressTypeEnum.InProgress: return "IN PROGRESS";

                case TaskProgressTypeEnum.NotAssigned: return "NOT ASSIGNED";

                case TaskProgressTypeEnum.NotStarted: return "NOT STARTED";

                default: return string.Empty;
            }
        }
        
        public static string AsString(this CargoMatrix.Communication.WSTaskManager.TaskStatus status)
        {
            switch(status)
            {
                case TaskStatus.Completed: return "COMPLETED";

                case TaskStatus.InProgress: return "IN PROGRESS";

                case TaskStatus.Pending: return "PENDING";

                default: return string.Empty;
            }
        }
    
    }
}
