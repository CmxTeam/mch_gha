﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CargoMatrix
{
    public partial class CMXProgressBar : UserControl
    {
        int _value;
        Brush _brush = new SolidBrush(Color.White);
        Rectangle _rectangle = new Rectangle();
        string _percentage;
        public CMXProgressBar()
        {
            InitializeComponent();
            
        }
        
        public override Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }
            set
            {
                _brush = new SolidBrush(value);
                base.ForeColor = value;
            }
        }
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }
        public int Maximum
        {
            get;
            set;
        }
        public int Minimum
        {
            get;
            set;
        }
        public int Value
        {
            get
            {
                return _value;
            
            }
            set
            {
                if (value <= Maximum && Maximum > 0)
                {
                    _value = value;
                    _rectangle.X = 0;
                    _rectangle.Y = 0;
                    _rectangle.Height = Height;
                    _rectangle.Width = (int)(((float)_value / (float)Maximum) * (float)Width);


                    _percentage = ""+ (int) (((float)_value / (float)Maximum) * 100.0f) + "%";
                }

            
            }
        }
        public Color TextColor
        {
            get;
            set;
        }
        private void CMXProgressBar_Paint(object sender, PaintEventArgs e)
        {
            
            if (this.Enabled)
            {
                SizeF textSize = e.Graphics.MeasureString(_percentage, Font);
                int x = (Width - (int)textSize.Width) / 2;
                int y = (Height - (int)textSize.Height) / 2;
               
                
                e.Graphics.FillRectangle(_brush, _rectangle);
                
                    
                e.Graphics.DrawString(_percentage, Font, new SolidBrush(TextColor), x, y - 1);
            }
            
        }
    }
}
