﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;

namespace CargoMatrix
{
   

    public partial class RunDLL : Form
    {
        public static string ConfigFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\CMXScannerConfig.xml";
        public RunDLL()
        {
            InitializeComponent();
            this.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                WebReference.ScannerWebService ws = new CargoMatrix.WebReference.ScannerWebService();
                Application.DoEvents();
                CMXConfig config;
                XmlSerializer s = new XmlSerializer(typeof(CMXConfig));

                TextReader r = new StreamReader(ConfigFile);
                config = (CMXConfig)s.Deserialize(r);
                r.Close();

                ws.Url = config.WSURL + "CMXScannerWebservice.asmx";


                if (ws.Ping())
                {
                    label2.Text = "Network Status: Success";
                }
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Error!");
                label2.Text = "Error: CMXScannerConfig.xml not found";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
                label2.Text = "Network Status: Error";
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    [XmlRoot("CMXConfig", IsNullable = false)]
    public class CMXConfig
    {
        public string connectionName;
        public string gateway;
        public string WSURL;
        public int camera;
    }
}