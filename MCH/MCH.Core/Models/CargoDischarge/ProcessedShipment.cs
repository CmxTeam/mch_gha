﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.CargoDischarge
{
    public class ProcessedShipment
    {
        public long Id { get; set; }
        public string Reference { get; set; }
        public bool IsHWB { get; set; }
    }
}
