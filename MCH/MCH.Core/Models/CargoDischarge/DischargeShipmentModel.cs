﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Core.Models.CargoDischarge
{
    public class DischargeShipmentModel
    {
        public long Id { get; set; }
        public string Reference { get; set; }

        public bool IsHWB { get; set; }
    }
}