﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Core.Models.CargoDischarge
{
    public class DischargePaymentsModel
    {
        public string Currancy { get; set; }
        public double TotalCharges { get; set; }
        public double TotalPayments { get; set; }

        public List<DischargePaymentRow> Payments { get; set; }
    }

    public class DischargePaymentRow
    {
        public long? Id { get; set; }
        public long? IdDB { get; set; }

        [Required(ErrorMessage="Required.")]
        public string TypeId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }


        [Required(ErrorMessage = "Required.")]
        [Range(0.01, 999999999999, ErrorMessage = "Required.")]
        public decimal Amount { get; set; }


        [Required(ErrorMessage = "Required.")]
        [Range(0.01, 999999999999, ErrorMessage = "Required.")]
        public decimal AmountPaid { get; set; }
        
        [Required(ErrorMessage = "Required.")]
        public string PaymentRef { get; set; }
        public DropDownModel PaymentOption { get; set; }

    }

    public class DropDownModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}