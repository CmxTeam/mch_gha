﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.CargoDischarge
{
    public class SaveShipment
    {
        public long DischargId { get; set; }
        public long ShipmentId { get; set; }
        public bool IsHWB { get; set; }
        public List<ShipmentCustomsRow> Parts { get; set; }

    }

    public class SaveShipmentResult
    {
        public long DischargeId { get; set; }

        public string Reference { get; set; }

        public long DischargeDetailId { get; set; }

        public string StepName { get; set; }
    }

    public class SaveCharges
    {
        public long? DischargId { get; set; }
        public long ShipmentId { get; set; }
        public bool IsHWB { get; set; }
        public List<DischargePaymentRow> Payments { get; set; }
    }

    public class ShipmentPart
    {
        public long Id { get; set; }
        public string Part { get; set; }
    }
}
