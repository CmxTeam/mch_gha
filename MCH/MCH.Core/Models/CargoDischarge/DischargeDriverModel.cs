﻿using MCH.Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Core.Models.CargoDischarge
{
    public class DischargeDriverModel
    {
        public long DischargeId { get; set; }

        public List<AttachmentModel> Documents { get; set; }

        public DischargePickupDetails PickupDetails { get; set; }

        [Required]
        public long? DriverId { get; set; }
        [Required]
        public long? TruckingCompId { get; set; }
        public string STANumber { get; set; }
        public string DriverLicense { get; set; }
        public int? StateId { get; set; }
        public string CapturePhoto { get; set; }
        public string CaptureID { get; set; }
        [Required]
        public int? FirstIDTypeId { get; set; }
        public bool? FirstIDMatched { get; set; }
        public int? SecondIDTypeId { get; set; }
        public bool? SecondIDMatched { get; set; }

        public DischargeDriverModel()
        {
            PickupDetails = new DischargePickupDetails();
            PickupDetails.Consignee = new Partner { Type = PartnerType.Consignee };
        }
    }


    public class DischargePickupDetails
    {
        public Partner Consignee { get; set; }
        public long? CarrierId { get; set; }
        public string TransferManifest { get; set; }

        public bool? IsConsigneeOrCarrier { get; set; }

        public bool IsConsignee
        {
            get
            {
                return this.IsConsigneeOrCarrier.HasValue && this.IsConsigneeOrCarrier.Value;
            }
        }
        public bool IsCarrier
        {
            get
            {
                return this.IsConsigneeOrCarrier.HasValue && !this.IsConsigneeOrCarrier.Value;
            }
        }
    }

}