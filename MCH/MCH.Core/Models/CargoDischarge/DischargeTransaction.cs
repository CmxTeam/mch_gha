﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.CargoDischarge
{
    public class DischargeTransaction
    {
        
        public long Id { get; set; }
        public string Part { get; set; }

        public string Origin { get; set; }
        public string Reference { get; set; }
        public string Dest { get; set; }
        public DateTime Date { get; set; }
        public string Driver { get; set; }
        public string Company { get; set; }
        public int SLAC { get; set; }
        public string User { get; set; }
        public string ReleasedBy { get; set; }
        public string CustomsStatus { get; set; }
        public string Status { get; set; }
        public bool IsDocumentPickedUp { get; set; }


        public long TaskId { get; set; }
        public string TaskAssigned { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string TaskStatus { get; set; }
        public int TaskStatusId { get; set; }
        
    }
}
