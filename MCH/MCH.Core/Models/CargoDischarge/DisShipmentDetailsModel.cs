﻿using MCH.Core.Model.Enum;
using MCH.Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.CargoDischarge
{
    public class DisShipmentDetailsModel
    {
        public ShipmentDetailsSection ShipmentDetails { get; set; }

        public List<ShipmentCustomsRow> CustomsClearance { get; set; }
        public List<ShipmentCustomsRow> CustomsFullHistory { get; set; }
        public enumDischargeShipmentStatuses Status { get; set; }
    }

    public class ShipmentDetailsSection
    {
        public long? AwbId { get; set; }
        public string AwbNumber { get; set; }

        public long? HwbId { get; set; }
        public string HwbNumber { get; set; }

        public string Destination { get; set; }
        public string Consignee { get; set; }
        public string ResultStatus { get; set; }
        public ShipmentCustomsStatuses ReleaseStatus { get; set; }

        public string StatusName
        {
            get
            {
                switch (this.ReleaseStatus)
                {
                    case ShipmentCustomsStatuses.OnHold:
                        {
                            return "On Hold";
                        }
                    case ShipmentCustomsStatuses.PartialAuthorized:
                        {
                            return "Partially Authorized";
                        }
                    case ShipmentCustomsStatuses.ReleaseAuthorized:
                        {
                            return "Release Authorized";
                        }
                    default:
                        {
                            return String.Empty;
                        }
                }
            }
        }
        public string StatusColor
        {
            get
            {
                switch (this.ReleaseStatus)
                {
                    case ShipmentCustomsStatuses.OnHold:
                        {
                            return "red";
                        }
                    case ShipmentCustomsStatuses.PartialAuthorized:
                        {
                            return "blue";
                        }
                    case ShipmentCustomsStatuses.ReleaseAuthorized:
                        {
                            return "green";
                        }
                    default:
                        {
                            return String.Empty;
                        }
                }
            }
        }

        public int? Pieces { get; set; }
        public double? Weight { get; set; }
        public string WeightUOM { get; set; }
        public int? ULDs { get; set; }
        public bool IsSplit { get; set; }
        public int? AvaileblePieces { get; set; }
    }

    public class ShipmentCustomsRow
    {
        public int Id { get; set; }
        public string Part { get; set; }
        public string CustomStaits { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? QuantityAffected { get; set; }
        public string EntryNo { get; set; }
        public DateTime? CustomsDate { get; set; }
        public DateTime? TransactionDate { get; set; }
        public bool ReadyForRelease { get; set; }
    }

    public enum ShipmentCustomsStatuses
    {
        [Description("On Hold")]
        OnHold = 0,
        [Description("Release Authorized")]
        ReleaseAuthorized = 1,
        [Description("Partially Authorized")]
        PartialAuthorized = 2
    }
}
