﻿using MCH.Core.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.CargoDischarge
{
    public class DischargeState
    {
        public long? DischargeId { get; set; }
        public string StepName
        {
            get
            {
                string tmpStepName = "1 Shipment Details";
                if (this.Status.HasValue)
                {
                    switch (this.Status)
                    {
                        case enumDischargeStatuses.DriverInfoVerified:
                            {
                                tmpStepName = "2 Driver Verification";
                                break;
                            }

                        case enumDischargeStatuses.Finalized:
                        case enumDischargeStatuses.Canceled:
                        default:
                            break;
                    }
                }

                return tmpStepName;
            }
        }

        public enumDischargeStatuses? Status { get; set; }

        public long? DriverId { get; set; }
        public string DriverName { get; set; }
        public string DriverPhotoData { get; set; }

        public int? ConsigneeId { get; set; }
        public int? CarrierId { get; set; }
        public string Consignee { get; set; }
        public string Carrier { get; set; }
        public string TransferManifest { get; set; }

        public bool IsConsigneeOrCarrier { get; set; }

        public int? TruckingCompanyId { get; set; }
        public string TruckingCompanyName { get; set; }
    }
}
