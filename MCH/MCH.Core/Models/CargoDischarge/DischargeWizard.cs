﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.CargoDischarge
{
    public class DischargeWizard
    {
        public DischargeState Header { get; set; }
        public ProcessedShipment LatestShipment { get; set; }
        public List<ProcessedShipment> ProcessedShipments { get; set; }
    }
}
