﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.Enums
{
    public enum enumPrinterLabelTypes : int
    {
        ULDLabel = 1,
        AWBLabel = 2,
        HWBLabel = 3
    }
}
