﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Model.Enum
{
    public enum enumDischargeStatuses:int
    {
        Draft = 55,
        DriverInfoVerified = 52,
        Finalized = 53,
        Canceled = 54
    }

    public enum enumDischargeShipmentStatuses : int
    {
        ShipmentVerified = 51,
        PaymentsVeriried = 52
    }
}
