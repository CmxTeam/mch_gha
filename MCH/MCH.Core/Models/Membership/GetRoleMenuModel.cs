﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.Membership
{
    public class GetRoleMenuModel
    {
        public int ApplicationTypeId { get; set; }
        public string AppName { get; set; }
        public long RoleId { get; set; }
        public long UserId { get; set; }
        public int WarehouseId { get; set; }
    }
}
