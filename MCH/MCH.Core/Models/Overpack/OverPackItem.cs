﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.Overpack
{
    public class OverPackItem
    {
        public long OverPackItemId { get; set; }
        public int OverPackItemNumber { get; set; }
        public int Pieces { get; set; }
        public SkidOwnerTypes SkidOwnerType { get; set; }
        public string OverpackType { get; set; }
        public long OverpackTypeId { get; set; }

        public string OverpackRef { get; set; }

        public int? TotalPieces { get; set; }
    }

    public enum SkidOwnerTypes
    {
        QAS = 0,
        CUSTOMER = 1,
    }

}
