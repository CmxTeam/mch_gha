﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.Overpack
{
    public class OverPackTaskItem
    {
        public long OverPackTaskId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Awb { get; set; }
        public long AwbId { get; set; }
        public string Hwb { get; set; }
        public long HwbId { get; set; }
        public int Skids { get; set; }
        public int TotalSkidPieces { get; set; }
        public int TotalPieces { get; set; }

    }

}
