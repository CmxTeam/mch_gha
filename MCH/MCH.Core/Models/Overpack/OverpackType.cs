﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.Overpack
{
    public class OverpackTypeItem
    {
        public string OverpackType { get; set; }
        public long OverpackTypeId { get; set; }

        public bool? IsDefault { get; set; }
    }
}
