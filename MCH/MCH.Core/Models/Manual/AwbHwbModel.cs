﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.Manual
{
    public class AwbHwbModel
    {
        public long HwbId { get; set; }

        public string Origin { get; set; }

        public string HWB { get; set; }

        public int? HPieces { get; set; }

        public string Destination { get; set; }

        public double? HWeight { get; set; }

        public string ShipperName { get; set; }

        public string ConsigneeName { get; set; }
    }
}
