﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.Manual
{
    public class AddAwbModel
    {
        public int? AccountId { get; set; }

        public string CarrierNumber { get; set; }

        public string AwbSerialNumber { get; set; }

        public int? OriginId { get; set; }

        public int? DestinationId { get; set; }

        public int? TotalPieces { get; set; }

        public string ShipperName { get; set; }

        public string ConsigneeName { get; set; }

        public long? UserId { get; set; }

        public long? WarehouseId { get; set; }
    }


    public class AddHwbModel
    {

        public long? AwbId { get; set; }

        public string HwbSerialNumber { get; set; }

        public int? OriginId { get; set; }

        public int? DestinationId { get; set; }

        public int? TotalPieces { get; set; }

        public string ShipperName { get; set; }

        public string ConsigneeName { get; set; }

        public long? UserId { get; set; }

        public long? WarehouseId { get; set; }
    }
}
