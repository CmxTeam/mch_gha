﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCH.Core;

namespace MCH.Core.Models.Common
{
    public class PartnerModel
    {
        public int? Id { get; set; }

        public enumPartnerTypes Type { get; set; }
        public string Name { get; set; }

        public string AccountNumber { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }

        public string Postal { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        // For Issuing Carrier's Agent
        public string IATACode { get; set; }
        public string CASSAddress { get; set; }
        public int? ParticipantIdentfierId { get; set; }
    }

    public enum enumPartnerTypes{
        Shipper = 1,
        Consignee,
        IssuingCarriersAgent
    }


    //frontend
    public class Partner
    {
        public int? Id { get; set; }

        public PartnerType Type { get; set; }
        public string Name { get; set; }

        public string AccountNumber { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }

        public string Postal { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public string StateText { get; set; }

        public string CountryText { get; set; }

        public string Summary
        {
            get
            {

                return String.Format(" Account: {0} \n Address: {1}  {2}  {3}  {4} {5} \n Contacts: {6} {7} ", AccountNumber, Address1, City, StateText, CountryText, Postal, Phone, Email);

            }
            set { }
        }

        public PartnerModel ToBackendModel()
        {
            PartnerModel tmpModle = new PartnerModel();
            tmpModle.AccountNumber = this.AccountNumber;
            tmpModle.Address1 = this.Address1;
            tmpModle.City = this.City;
            tmpModle.CountryId = this.CountryId;
            tmpModle.Fax = this.Fax;
            tmpModle.Id = this.Id;
            tmpModle.Name = this.Name;
            tmpModle.Phone = this.Phone;
            tmpModle.Postal = this.Postal;
            tmpModle.StateId = this.StateId;
            tmpModle.Email = this.Email;
            tmpModle.Type = this.ToPartnerType(this.Type);
            return tmpModle;
        }


        public enumPartnerTypes ToPartnerType(PartnerType type)
        {
            switch (type)
            {
                case PartnerType.Shipper:
                    return enumPartnerTypes.Shipper;
                case PartnerType.Consignee:
                    return enumPartnerTypes.Consignee;

                default:
                    return enumPartnerTypes.Shipper;
            }
        }

    }

    public class PartnerSimple
    {
        public int? Id { get; set; }

        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string City { get; set; }
        public String Country { get; set; }

    }


    public enum PartnerType
    {
        Shipper = 1,
        Consignee
    }
}
