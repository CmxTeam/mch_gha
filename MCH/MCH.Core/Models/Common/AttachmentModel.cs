﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.Core.Models.Common
{
   public class AttachmentModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long? ShipperDocTypeId { get; set; }
        public string DocumentType { get; set; }
        public string IconKey { get; set; }
        public bool IsSnapshot { get; set; }
        public string SnapshotData { get; set; }
        public string Path { get; set; } 

    }
}
