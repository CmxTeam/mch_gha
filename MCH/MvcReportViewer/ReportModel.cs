﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MvcReportViewer
{
    public class ReportModel
    {
        public string Name { get; set; }
        public string Path { get; set; }

        public Dictionary<string, object> ReportParams { get; set; }


        public void AdjustDictionary()
        {
            if (this.ReportParams != null)
            {
                var adjustedDict = new Dictionary<string, object>();
                foreach (var item in ReportParams)
                {
                    adjustedDict.Add(item.Key, ((string[])(item.Value))[0]);
                }
                this.ReportParams = adjustedDict;
            }
        }
    }
}
