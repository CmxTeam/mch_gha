﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Utilities.Exceptions
{
    public class ServiceException : Exception
    {
        #region Properties
        private int errorCode;
        //private System.ServiceModel.FaultCode faultCode;
       // private string p;
       // private CMX.Services.Security.ServiceFactory.MembershipServiceRef.ErrorCodes errorCodes;

        public int ErrorCode
        {
            get { return errorCode; }
            set { errorCode = value; }
        } 
        #endregion

        #region Constructors
        public ServiceException()
        {

        }

        public ServiceException(string errorMessage)
            : base(errorMessage)
        {

        }

        public ServiceException(int errorCode, string errorMessage)
            : base(errorMessage)
        {
            this.errorCode = errorCode;
        }
        
        #endregion
    }
}