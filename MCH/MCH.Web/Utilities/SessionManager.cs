﻿using CMX.Shell.Security;
using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Utilities
{
    public class SessionManager
    {
        public static UserInfo UserInfo
        {
            get
            {
                return HttpContext.Current.Session[Constants.SessionKey.USER_SESSION_OBJECT] as UserInfo;
            }
            set
            {
                HttpContext.Current.Session[Constants.SessionKey.USER_SESSION_OBJECT] = value;

                HttpCookie cookie = new HttpCookie(Constants.SessionKey.USER_SESSION_ID, value.UserId.ToString());
                HttpContext.Current.Response.Cookies.Remove(Constants.SessionKey.USER_SESSION_ID);
                HttpContext.Current.Response.SetCookie(cookie);
            }
        }

        public static bool Loaded = false;

        public static long AccountId
        {
            get
            {
                return Convert.ToInt64(HttpContext.Current.Session[Constants.SessionKey.ACCOUNT_ID]);
            }
            set
            {
                // For cashing, cause Session is not available in GetVaryByCustomString
                HttpCookie cookie = new HttpCookie(Constants.SessionKey.ACCOUNT_ID, value.ToString());
                HttpContext.Current.Response.Cookies.Remove(Constants.SessionKey.ACCOUNT_ID);
                HttpContext.Current.Response.SetCookie(cookie);

                HttpContext.Current.Session[Constants.SessionKey.ACCOUNT_ID] = value;
            }
        }

        public static List<SelectComboItem> Accounts
        {
            get
            {
                var tmpAccounts = HttpContext.Current.Session[Constants.SessionKey.ACCOUNT_LIST] as List<SelectComboItem>;
                if (tmpAccounts != null && SessionManager.AccountId != 0)
                {
                    var index = tmpAccounts.FindIndex(x => x.Value == SessionManager.AccountId);
                    if (index > 0)
                    {
                        var item = tmpAccounts[index];
                        tmpAccounts[index] = tmpAccounts[0];
                        tmpAccounts[0] = item;
                    }
                }
                return tmpAccounts;
            }
            set
            {
                HttpContext.Current.Session[Constants.SessionKey.ACCOUNT_LIST] = value;
            }
        }

       
        public static int SelectedAccountIndex {
            get {
                int tmpAccountIndex = 0;

                var tmpIndexValue = HttpContext.Current.Session[Constants.SessionKey.SELECTED_ACCOUNT_INDEX];

                if (tmpIndexValue != null) 
                {
                    try
                    {
                        tmpAccountIndex = Convert.ToInt32(tmpIndexValue);
                    }
                    catch (Exception) { }
                }

                return tmpAccountIndex;
            }
            set {
                HttpContext.Current.Session[Constants.SessionKey.SELECTED_ACCOUNT_INDEX] = value;
            }
        }
    }
}