﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Utilities
{
    public static class Constants
    {
        public const string DATE_FORMAT = "MM/dd/yyyy";
        public const string DATETIME_FORMAT = "MM/dd/yyyy h:mm tt";
        public static class Security
        {
            public const string STRONG_PASSWORD_EXPRESSION = @"(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W)\S{8,15}";
        }

        public static class Keys
        {
            public const string ThemeName = "Theme_Name";
        }

        public static class Messages 
        {
            public static readonly string RFID_MISSING_ON_MANIFEST_ULD = "Cannot complete the Manifest. RFID Number is required for the following ULD Numbers ";
            public static readonly string RANGE_POOL_VALIDATION_ERROR= "Cannot complete the Manifest. Range Pool result is invalid";
        }

        public static class ApplicationSettings 
        {
            public const string APPLICATION_NAME = "COM Fluor";
            public static readonly Guid APPLICATION_ID = new Guid("186C5A3B-02C1-4C07-9F50-033D7659609C");
            public static readonly int APPLICATION_TYPE = 1;
        }

        public static class Config
        {
            public static string PASSWORD_EXPIRATION_PERIOD = "PasswordExpirationPeriod";
            public static string MAX_REQ_PASS_LENGHT_KEY = "MaxRequiredPassword";
            public static string MIN_REQ_PASS_LENGHT_KEY = "MinRequiredPassword";

            public static string ATTACHMENTS_REPOSITORY = "AttachmentsRepository";
        }

        public static class SessionKey
        {
            public static string USER_SESSION_OBJECT = "User_Session_Object";

            public static string USER_SESSION_ID = "User_Session_Id";

            public static string Line_Item_Prompt { get; set; }

            public static readonly string ACCOUNT_ID = "Session_Account_ID";

            public static readonly string ACCOUNT_LIST = "Session_Account_List";

            public static readonly string MENU_ITEMS = "Session_Menu_Items";

            public static readonly string SELECTED_ACCOUNT_INDEX = "Selected_Account_Index";
        }
    }
}