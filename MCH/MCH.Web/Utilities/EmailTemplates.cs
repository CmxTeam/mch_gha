﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Utilities
{
    public class EmailTemplates
    {
        /// <summary>
        /// Keys:[Name],[LinkUrl],[LinkName],[FullName],[Year]
        /// </summary>
        public static readonly string RESET_PASSWORD_TEMPLATE = @"<div><div class='adM'>

</div><table border='0' width='550' cellpadding='0' cellspacing='0' style='max-width:550px;border-top:4px solid #39c;font:12px arial,sans-serif;margin:0 auto'><tbody><tr><td>  
  <h1 style='font:bold 23px arial;margin:5px 0'>Cargomatrix</h1>
<p style='margin-bottom:6pt'>Hi [Name],</p>

<p style='margin-bottom:6pt'>Your password has been reset to <b>[GeneratedPassword]</b> you can change it after logging in to the system.</p>

<p style='margin-bottom:6pt'>Thank you,</p>

<p style='margin-bottom:6pt'>The CargoMatrix Support Team.</p>

<table border='0' cellspacing='0' cellpadding='0' style='font-family:Arial' width='100%'>
  <tbody><tr><td><table width='1' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table></td></tr>
  <tr>
    <td align='left' style='font-size:11px;font-family:Arial,sans-serif;color:#999999'>
                This email was intended for [FullName]. ©[Year], Cargomatrix Inc. 20 East Sunrise Highway Valley Stream, NY 11581, USA
    </td>
  </tr>
  <tr><td><table width='1' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table></td></tr>
</tbody></table>

</td></tr></tbody></table></div>";

    }
}