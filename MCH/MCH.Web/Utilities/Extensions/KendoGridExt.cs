﻿using Kendo.Mvc.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kendo.Mvc.Extensions;

namespace MCH.Web.Utilities.Extensions
{
    public static class KendoGridExt
    {
        public static DataSourceResult ToCustomDataSource(this IEnumerable source, DataSourceRequest request, int total, bool forPagedData = true)
        {
            if (forPagedData)
            {
                request.Page = 1;
            }
            var dataResult = source.ToDataSourceResult(request);
            dataResult.Total = total;

            return dataResult;
        }
    }
}