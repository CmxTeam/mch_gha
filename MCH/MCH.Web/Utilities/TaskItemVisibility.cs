﻿using MCH.BLL.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Utilities
{
    public static class TaskItemVisibility
    {
        public static bool IsHidden(string property, enumTaskTypes taskType)
        {
            bool isHidden = false;
            if (!String.IsNullOrWhiteSpace(property))
            {
                switch (taskType)
                {
                    case enumTaskTypes.CargoReceiver:
                        if (property == "ETD" || property == "Driver" || property == "ScreeningResult") 
                        {
                            isHidden = true;
                        }
                        break;
                    case enumTaskTypes.CargoInventory:

                        break;
                    case enumTaskTypes.CargoDischarge:

                        break;
                    case enumTaskTypes.CargoSnapshot:

                        break;
                    case enumTaskTypes.CargoLoader:

                        break;
                    case enumTaskTypes.CargoTender:

                        break;
                    case enumTaskTypes.CargoScreener:

                        break;
                    case enumTaskTypes.DGCheckList:

                        break;
                    case enumTaskTypes.CargoAccpetWarehouse:

                        break;
                    case enumTaskTypes.LiveAnimal:

                        break;
                    case enumTaskTypes.HighValue:

                        break;
                    case enumTaskTypes.HumanRemains:

                        break;
                    default:
                        break;
                }
            }

            return isHidden;
        }
    }
}