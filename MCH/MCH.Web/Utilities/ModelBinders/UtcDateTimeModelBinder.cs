﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Utilities.ModelBinders
{
    public class UtcDateTimeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            try
            {
                ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

                DateTime retVal;
                if (valueResult != null && !string.IsNullOrEmpty(valueResult.AttemptedValue) 
                    && DateTime.TryParse(valueResult.AttemptedValue, out retVal))
                {
                    var userInfo = SessionManager.UserInfo;
                    if (userInfo != null)
                    {
                        TimeSpan offset = new TimeSpan(userInfo.ClientTimeZoneOffset, 0, 0);
                        TimeZoneInfo userTimeZone = TimeZoneInfo.CreateCustomTimeZone("ID", offset, userInfo.ClientTimeZone, "name");
                        DateTime userTime = TimeZoneInfo.ConvertTimeToUtc(retVal, userTimeZone);

                        return userTime.ToUniversalTime();
                    }
                    return retVal.ToUniversalTime();
                }

                return base.BindModel(controllerContext, bindingContext);
            }
            catch (HttpRequestValidationException)
            {
                return DateTime.MinValue;
            }
        }
    }
}