﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Utilities.ModelBinders
{
    
    public class InputTrimModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext,
        ModelBindingContext bindingContext)
        {
            try
            {
                var valueProvider = bindingContext.ValueProvider;
                ValueProviderResult valueResult;
                var containerType = bindingContext.ModelMetadata.ContainerType;
                var isHtmlAllowed = false;
                if (bindingContext.ModelMetadata.PropertyName != null)
                {
                    isHtmlAllowed = ((AllowHtmlAttribute[])containerType.GetProperty(bindingContext.ModelMetadata.PropertyName)
                                                              .GetCustomAttributes(typeof(AllowHtmlAttribute), false)).Any();
                }

                if (isHtmlAllowed)
                {
                    valueResult = ((IUnvalidatedValueProvider)valueProvider)
                .GetValue(bindingContext.ModelName, skipValidation: true);
                }
                else
                {
                    valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                }

                if (valueResult == null || string.IsNullOrEmpty(valueResult.AttemptedValue))
                    return null;
                return valueResult.AttemptedValue.Trim();
            }
            catch (HttpRequestValidationException) 
            {
                return new Guid().ToString();
            }
        }
    }
}