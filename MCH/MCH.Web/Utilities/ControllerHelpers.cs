﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace MCH.Web.Utilities
{
    public class ControllerHelpers
    {

        public static T ToObjectFromJSON<T>(string jsonString)
        {
            var serializer = new JavaScriptSerializer();
            var newObject = serializer.Deserialize<T>(jsonString);
            return newObject;
        }
    }
}