﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace MCH.Web.Utilities.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class EmailAddressAttribute : DataTypeAttribute, IClientValidatable
    {
        private Regex _regex = new Regex(@"^[\s]*\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+[\s]*$");
        public EmailAddressAttribute()
            : base(DataType.EmailAddress)
        {
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRegexRule("Invalid email format.", _regex.ToString())
            {
                ValidationType = "email",
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName())
            };
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }
            string valueAsString = value as string;
            return valueAsString != null && _regex.Match(valueAsString.Trim()).Length > 0;
        }
    }
}