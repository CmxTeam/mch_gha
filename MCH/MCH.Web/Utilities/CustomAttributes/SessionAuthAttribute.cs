﻿using MCH.Web.Models.Account;
using MCH.Web.ShellApi;
using MCH.Web.ShellApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Utilities.CustomAttributes
{
    public class SessionAuthAttribute:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAuthenticated)
            {
                try
                {
                    LoginViewModel model = new LoginViewModel();
                    model.UserId = "admin@cargomatrix.com";
                    model.Password = "Admin123#";
                    string returnUrl = String.Empty;

                    switch (ServiceFactory.CreateAuthenticationService().CheckUserValidity(model.UserId, model.Password))
                    {
                        case UserValidaitonResult.UserIsValid:
                            {
                                 this.AuthenticateUserHelper(model, returnUrl);
                                 break;
                            }
                        case UserValidaitonResult.InvalidPassword:
                            {
                                break;
                            }
                        case UserValidaitonResult.PasswordExpired:
                            {
                                //UserInfo tmpUserInfo = AuthenticationService.SignIn(model.UserId, model.Password, model.RememberMe);
                                //SessionManager.UserInfo = tmpUserInfo;
                                //return RedirectToAction("ChangePassword", new
                                //{
                                //    reason = "Your password has been expired, it is strongly recommended to change it!"
                                //});
                                 this.AuthenticateUserHelper(model, returnUrl);
                                 break;
                            }
                        case UserValidaitonResult.FirstTimeLogin:
                            {
                                //UserInfo tmpUserInfo = AuthenticationService.SignIn(model.UserId, model.Password, model.RememberMe);
                                //SessionManager.UserInfo = tmpUserInfo;
                                //return RedirectToAction("ChangePassword", new
                                //{
                                //    reason = "You are logging in first time, is it recommended to change your password!"
                                //});
                                 this.AuthenticateUserHelper(model, returnUrl);
                                 break;
                            }
                        case UserValidaitonResult.PasswordReset:
                            {
                                break;
                            }
                        case UserValidaitonResult.InvalidUserId:
                            {
                                break;
                            }
                        case UserValidaitonResult.UserIsInactive:
                            {
                                //TODO : Harut -> add contact administrators contact info here.
                                break;
                            }
                        case UserValidaitonResult.UserIsLocked:
                            {
                                //TODO : Harut -> add contact administrators contact info here.
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    
                }
            }
            base.OnActionExecuting(filterContext);
        }

        private void AuthenticateUserHelper(LoginViewModel model, string returnUrl)
        {
            UserAuthModel tmpUserInfo = ServiceFactory.CreateAuthenticationService().SignIn(model.UserId, model.Password, model.RememberMe);
            tmpUserInfo.ClientTimeZoneOffset = model.ClientTimeZone;
            if (tmpUserInfo != null)
            {
                if (tmpUserInfo.DefaultAccountId.HasValue)
                {
                    SessionManager.AccountId = tmpUserInfo.DefaultAccountId.Value;

                    tmpUserInfo.UserMenuItems = ServiceFactory.CreateAuthenticationService().GetUserMenuItems(tmpUserInfo.RoleNames, tmpUserInfo.DefaultAccountId.Value);
                }
            }
            SessionManager.UserInfo = tmpUserInfo;


            ServiceFactory.CreateAuthenticationService().SetAuthTicket(model.UserId, model.RememberMe, tmpUserInfo);
           
        }
    }
}