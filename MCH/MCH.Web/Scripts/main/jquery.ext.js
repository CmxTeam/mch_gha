﻿$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$.fn.serializeAllArray = function () {
    var obj = {};

    $('input', this).each(function () {
        obj[this.name] = $(this).val();
    });
    return $.param(obj);
};

$.fn.getDivHeight = function (isOuter)
{
    var prevStyle = $(this).attr("style");
    $(this).css({
        position:   'absolute', 
        visibility: 'hidden',
        display:    'block'
    });

    var result = isOuter ? $(this).outerHeight(true) : $(this).height();

    $(this).attr("style", prevStyle ? prevStyle : "");


    return result;
}

