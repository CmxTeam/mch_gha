﻿function CommonDialog(baseUrl, dialogContainerId) {
    this.baseUrl = baseUrl;
    this.dialogId = dialogContainerId;
}

// opens logout confirmation window: calls on confirm method after user confirmation
CommonDialog.prototype.openLogoutConfirmation = function (onConfirm) {
    bootbox.confirm({
        title: "Logout Confirmation",
        message: "Are you sure you want to logout ?",
        buttons:
            {
                'cancel': {
                    label: 'No',
                    className: 'btn-danger pull left'
                },
                'confirm': {
                    label: 'Yes',
                    className: 'btn-primary pull right'
                }
            },
        callback: function (result) {
            if (result) {
                onConfirm();
            }
        }
    });
}


CommonDialog.prototype.openConfirmation = function (onConfirm, onCancel, title, message) {
    var tmpGlobalConfDialog = $("#wndGlobalConfirmation").data("kendoWindow");

    tmpGlobalConfDialog.title(title);

    var dialogContent = $("#modal-confirm-task-action").tmpl({ message: message, confirmButtonId: "btnGlobalDialogConfirm", cancelGlobalButtonId: "btnGlobalDialogCancel" });

    tmpGlobalConfDialog.content(dialogContent);
    $("#btnGlobalDialogCancel").one("click", (onCancel != null && onCancel != undefined) ? onCancel : function () {
        tmpGlobalConfDialog.close();
    });

    $("#btnGlobalDialogConfirm").one("click", function () { onConfirm(), tmpGlobalConfDialog.close(); });

    tmpGlobalConfDialog.center().open();
}

CommonDialog.prototype.openAlert = function (title, message) {
    
    var tmpGlobalAlertDialog = $("#wndGlobalAlert").data("kendoWindow");

    tmpGlobalAlertDialog.title(title);

    var dialogContent = $("#modal-global-alert").tmpl({ message: message });
    tmpGlobalAlertDialog.content(dialogContent);

    tmpGlobalAlertDialog.center().open();
}

CommonDialog.prototype.openRemoteDialog = function (onConfirm, url, data, title) {
    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (content) {
            bootbox.dialog({
                message: content,
                title: title,
                buttons: {
                    danger:
                        {
                            label: "Cancel",
                            className: "btn-danger"
                        },
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: onConfirm
                    }
                }
            });
        }
    });
}

CommonDialog.prototype.openPartialWindow = function (argWindow, url, data, title) {
    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (content) {
            argWindow.content(content);
            argWindow.title(title);
            argWindow.center().open();
        }
    });
}

CommonDialog.prototype.openRemoteView = function (window, url, data, title) {
    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (content) {
            bootbox.dialog({
                message: content,
                title: title,
                className: 'cm-modal-remote'
            });
        }
    });
}

CommonDialog.prototype.openRemoteDialog = function (onConfirm, onCancel, url, data, title) {
    jQuery.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (content) {
            bootbox.dialog({
                message: content,
                title: title,
                className: 'cm-modal-remote-form',
                buttons: {
                    danger:
                        {
                            label: "Cancel",
                            className: "btn-danger",
                            callback: onCancel
                        },
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: onConfirm
                    }
                }
            });
        }
    });
}


CommonDialog.prototype.openStaticDialog = function (onConfirm, html, title) {
    bootbox.dialog({
        message: html,
        title: title,
        buttons: {
            danger:
                {
                    label: "Cancel",
                    className: "btn-danger"
                },
            success: {
                label: "Save",
                className: "btn-success",
                callback: onConfirm
            }
        }
    });
}

CommonDialog.prototype.openPhotoZoomWindow = function (html, title) {
    bootbox.dialog({
        message: html,
        title: title,
        buttons: {
            success: {
                label: "Close",
                className: "btn-primary",
            }
        }
    });
}

CommonDialog.prototype.showPopup = function (onConfirm, onCancel, html, title) {
    bootbox.dialog({
        message: html,
        title: title,
        buttons: {
            danger:
                {
                    label: "Cancel",
                    className: "btn-danger",
                    callback: onCancel
                },
            success:
                {
                    label: "Save",
                    className: "btn-success",
                    callback: onConfirm
                }
        }
    });
}

CommonDialog.prototype.showDocumentPopup = function (onConfirm, onCancel, html, title) {
    bootbox.dialog({
        message: html,
        title: title,
        className: 'cm-modal-document',
        buttons: {
            danger:
                {
                    label: "Cancel",
                    className: "btn-danger",
                    callback: onCancel
                },
            success:
                {
                    label: "Save",
                    className: "btn-success",
                    callback: onConfirm
                }
        }
    });
}


