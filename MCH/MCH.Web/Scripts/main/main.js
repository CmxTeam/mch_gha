﻿function GetAjaxProxy() {
    if (typeof XMLHttpRequest !== 'undefined') {
        return new XMLHttpRequest();  
    }
    var versions = [
        "MSXML2.XmlHttp.5.0",   
        "MSXML2.XmlHttp.4.0",  
        "MSXML2.XmlHttp.3.0",   
        "MSXML2.XmlHttp.2.0",  
        "Microsoft.XmlHttp"
    ];

    var xhr;
    for(var i = 0; i < versions.length; i++) {  
        try {  
            xhr = new ActiveXObject(versions[i]);  
            break;  
        } catch (e) {
        }  
    }
    return xhr;
};

if (!Function.prototype.bind) { // check if native implementation available
    Function.prototype.bind = function () {
        var fn = this, args = Array.prototype.slice.call(arguments),
            object = args.shift();
        return function () {
            return fn.apply(object,
              args.concat(Array.prototype.slice.call(arguments)));
        };
    };
}

function keepSessionAlive()
{
    var ajaxProxy = GetAjaxProxy();

    ajaxProxy.open("GET", 'base/KeepAlive', true);
    ajaxProxy.send();
}

//every 5 minutes
window.setInterval(keepSessionAlive, 300000);

window.resizeKendoWindow = function (e) {
    var wnd = $(e.sender)[0];
    wnd && wnd.wrapper && wnd.wrapper.height('auto');
};
