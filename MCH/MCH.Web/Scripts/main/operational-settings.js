﻿/// jquery
/// operational-classes.css

//defines all the common operations based on the element css-class

$(document).ready(function () {
    //window close buttons 
    $('body').on('click', '.op-window-close', function (e) {
        var windowDiv = $(e.target).closest("div.k-window-content");
        if (windowDiv) {
            $(windowDiv).data("kendoWindow").close();
        } else
        {
            console.log("Close window failed : Window not found.");
        }
    });
});