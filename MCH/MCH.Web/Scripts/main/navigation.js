﻿function NavigationHandler(baseUrl, loadContainerId) {
    this.baseUrl = baseUrl;
    this.currentUrl = "";
    this.loadContainer = loadContainerId;
}

NavigationHandler.prototype.getCurrentUrl = function ()
{
    return this.currentUrl;
}

NavigationHandler.prototype.setCurrentUrl = function (url)
{
    this.currentUrl = url;
}
//NavigationHandler.prototype.contentLoaded = $.Event('grid-content-loaded');
NavigationHandler.prototype.itemNavigate = function (url,callback) {
    //show the loading indicator bar
    $("#" + this.loadContainer).html(' <h1 class="content-heading bg-white border-bottom" >Loading <span>please wait...</span> <span class="loading-bar"></span></h1>');

    //replace the content
    $("#" + this.loadContainer).load(url, callback);
}

NavigationHandler.prototype.navigateTo = function (url, data, callback)
{
    var self = this;

    //show the loading indicator bar
    $("#" + this.loadContainer).html(' <h1 class="content-heading bg-white border-bottom" >Loading <span>please wait...</span> <span class="loading-bar"></span></h1>');

    //replace the content
    $("#" + this.loadContainer).load(url, data, callback);
}





