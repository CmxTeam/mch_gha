﻿function MediaHandler() {
    this.VideoStreamOptions = null;
}


MediaHandler.prototype.stop = function () {
    try {
        window.DocumentVideoElement.src = '';
        window.stream.stop();
    } catch (e) {
        console.log(e);
    }
}

MediaHandler.prototype.init = function (videoElemId, canvasElemId, sourceSelectorId)
{
    this.VideoStreamOptions = null;
}

MediaHandler.prototype.start = function (videoElemId, canvasElemId, source) {
    var self = this;
    window.ImageCaptureSource = 1;

    if (source == 'doc')
    {
        window.ImageCaptureSource = 0;
    }
    else if (source == 'img')
    {
        window.ImageCaptureSource = 1;
    }

    window.DocumentVideoElement = document.getElementById(videoElemId);

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

    if (typeof MediaStreamTrack === 'undefined') {
        alert('This browser does not support MediaStreamTrack.\n\nTry Chrome Canary.');
    } else {
        MediaStreamTrack.getSources(self.getMediaSources);
    }
}

MediaHandler.prototype.play = function ()
{

}

MediaHandler.prototype.getMediaSources = function (sourceInfos) {
    var videoSources = [];
    var audioSources = [];
    debugger;
    for (var i = 0; i != sourceInfos.length; ++i) {
        var sourceInfo = sourceInfos[i];
        //sourceInfo.id, sourceInfo.label
        if (sourceInfo.kind === 'audio') {
            audioSources.push(sourceInfo.id);
        } else if (sourceInfo.kind === 'video') {
            videoSources.push(sourceInfo.id);
        } else {
            console.log('Some other kind of source: ', sourceInfo);
        }
    }

    if (!!window.stream) {
        try {
            window.DocumentVideoElement.src = '';
            window.stream.stop();
        } catch (e) {
            console.log(e);
        }
    }
    var constraints = {
        audio: false,
        video: {
            optional: [{ sourceId: videoSources[window.ImageCaptureSource] }]
        }
    };


    navigator.getUserMedia(constraints,
        function (stream) {
            try {
                window.stream = stream; // make stream available to console
                window.DocumentVideoElement.src = window.URL.createObjectURL(stream);
                window.DocumentVideoElement.play();
            } catch (e) {
                console.log(e);
            }
        },
        function () {
            console.log("navigator.getUserMedia error: ", error);
        });
}








