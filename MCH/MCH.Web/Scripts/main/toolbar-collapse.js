﻿//menu-collapse.js
; (function (me) {
    me.events = {
        onClick: function (e) {
            e.preventDefault();
            if (me.toolbar && me.content) {
                if (me.arrows) {
                    me.arrows.toggleClass("glyphicon-chevron-up");
                    me.arrows.toggleClass("glyphicon-chevron-down");
                    if (me.toolbar.hasClass('collapsed')) {
                        me.content.show();
                        me.arrows.parent().attr('title', 'Collapse');
                    } else {
                        me.content.hide();
                        me.arrows.parent().attr('title', 'Expand');
                    }
                    me.toolbar.toggleClass('collapsed');
                    resizeGrid(true);
                    var tbWarehouseManagementGrids; 
                    ((tbWarehouseManagementGrids = me.toolbar.siblings('div[class^=cm-heightcalc100-]'))
                    && tbWarehouseManagementGrids.toggleClass('cm-heightcalc100-70 cm-heightcalc100-20'));
                    resizeGrid(false);
                }
            }
        }
    };
    me.cssClass = undefined;
    me.content = undefined;
    me.arrows = undefined;
    me.apply = function (cssClass) {        
        me.cssClass = cssClass || '[data-role="grid"]>.k-grid-toolbar';        
        $(document).on('grid-content-loaded', function () {
            me.toolbar = $(me.cssClass);
            if (!me.toolbar || !me.toolbar.length) return false;
            me.content = me.toolbar.find('.toolbarContent');
            if (me.toolbar && me.toolbar.find('.toolbar-collapse-arrow').length === 0) {
                me.toolbar.append([
                    '<div class="k-window-actions toolbar-collapse-arrow">',
                    '<a role="button" class="k-toolbar-window-action k-link" title="Collapse">',
                    '<span role="presentation" class="glyphicon glyphicon-chevron-up"></span>',
                    '</a>',
                    '</div>'
                ].join(''));

                var arrows = me.toolbar.find('.toolbar-collapse-arrow a');
                if (arrows && arrows.length) {
                    me.arrows = arrows.find('span');
                    arrows.on('click', me.events.onClick); 
                }
            }
        });
    }
})(window._toolbarCollapse = window._toolbarCollapse || {});
