﻿//menu-collapse.js
; (function (me) {
    me.events = {
        onClick: function (e) {
            e.preventDefault();
            if (me.menus && me.rigthContent) {
                if (me.arrows) {
                    me.arrows.toggleClass("glyphicon-chevron-left");
                    me.arrows.toggleClass("glyphicon-chevron-right");
                    me.menus.toggleClass('col-sm-2 hidden');
                    me.rigthContent.toggleClass('col-sm-10 col-sm-12');                    
                    if (me.menus.hasClass('collapsed')) {
                        me.menus.show();
                        me.arrows.parent().attr('title', 'Collapse');
                    } else {
                        me.menus.hide();                        
                        me.arrows.parent().attr('title', 'Expand');
                    }                    
                    me.menus.toggleClass('collapsed');
                    resizeGrid(true);
                }
            }
        },
        onMouseEnter: function (e) {
            e.preventDefault();
            me.arrows && me.arrows.show(10);
        },
        onMouseLeave: function (e) {
            e.preventDefault();
            me.arrows && me.arrows.hide(20);
        }

    };
    me.cssClass = undefined;
    me.rigthContent = undefined;
    me.menus = undefined;
    me.arrows = undefined;
    me.apply = function (cssClass) {
        me.cssClass = cssClass || '.menu-collapse';
        
        $(document).on('grid-content-loaded', function () {            
            me.menus = $(me.cssClass);
            var content = me.rigthContent = $('div#content');
            if (content && content.find('.collapse-arrow').length === 0) {
                content.append([
                    '<div class="k-window-actions visible-lg collapse-arrow">',
                    '<a role="button" class="k-horizontal-window-action k-link" title="Collapse">',
                    '<span role="presentation" class="glyphicon glyphicon-chevron-left"></span>',
                    '</a>',
                    '</div>'
                ].join(''));

                var arrows = content.find('.collapse-arrow a');
                if (arrows && arrows.length) {
                    me.arrows = arrows.find('span');
                    arrows.on('click', me.events.onClick);
                    //menus.on('mouseenter ', me.events.onMouseEnter);
                    //menus.on('mouseleave', me.events.onMouseLeave);

                }
            }
        });
    }
})(window._menuCollapse = window._menuCollapse || {});
