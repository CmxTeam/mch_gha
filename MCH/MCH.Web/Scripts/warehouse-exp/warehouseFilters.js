﻿var warehouseFilters = function (accordion, onFilterClicked, onAccordionReady, cell) {
    this.onFilterClicked = onFilterClicked;    
    this.onAccordionReady = onAccordionReady;    
    
    this._setCalendarDate = function (name, form, date) {
        var calendar = form.getCalendar(name);
        var calendarToKey = Object.keys(calendar.i)[0];
        calendar.i[calendarToKey].input.value = window.dhx4.date2str(date, '%m-%d-%Y');
        calendar.setDate(date);
    }

    this.getFilters = function () {
        var values = this.filterPanelForm.getValues();
        return {
            shipmentRef: values.shipmentRef,
            carriersId: values.carriersCombo,
            originId: values.originCombo,
            destId: values.destCombo,
            warehouseId: values.warehouseCombo,
            zoneId: values.zoneCombo,
            locationId: values.locationsCombo,
            fromDate: this.filterPanelForm.getCalendar('fromDateCalendar').getFormatedDate(),
            toDate: this.filterPanelForm.getCalendar('toDateCalendar').getFormatedDate()
        };
    }

    init = function () {        
        var warehouseFiltersContainer = accordion.addItem('warehouseFiltersContainer', 'EXPLORER');
        var filterFormStructure = [
                { type: "input", name: "shipmentRef", label: "Shipment#:", position: "label-left", labelWidth: 70, inputWidth: 200 },
                { type: "combo", name: "carriersCombo", label: "Carrier:", position: "label-left", labelWidth: 70, connector: '../api/Common/GetCarriers', inputWidth: 200 },
                { type: "combo", name: "originCombo", label: "Origin:", position: "label-left", labelWidth: 70, connector: "../api/Common/GetPorts", inputWidth: 200 },
                { type: "combo", name: "destCombo", label: "Dest:", position: "label-left", labelWidth: 70, connector: "../api/Common/GetPorts", inputWidth: 200 },
                { type: "combo", name: "warehouseCombo", label: "Warehouse:", position: "label-left", labelWidth: 70, inputWidth: 200 },
                { type: "combo", name: "zoneCombo", label: "Zone:", position: "label-left", labelWidth: 70, connector: "../api/Common/GetWarehouseZones", inputWidth: 200 },
                { type: "combo", name: "locationsCombo", label: "Location:", position: "label-left", labelWidth: 70, connector: "../api/Common/GetWarehouseLocations", inputWidth: 200 },

                { type: "block", offsetTop: "20" },

                { type: "combo", name: "period", label: "Period:", position: "label-left", labelWidth: 70, inputWidth: 200 },
                { type: "calendar", name: "fromDateCalendar", label: "From Date:", position: "label-left", labelWidth: 70, dateFormat: "%m-%d-%Y", inputWidth: 200 },
                { type: "calendar", name: "toDateCalendar", label: "To Date:", position: "label-left", labelWidth: 70, dateFormat: "%m-%d-%Y", inputWidth: 200 },

                { type: "block", list: [{ type: "button", name: "resetButton", value: "RESET", offsetLeft: 80 }, { type: "newcolumn" }, { type: "button", name: "filterButton", value: "SEARCH" }] }                
        ];

        var filterPanelForm = warehouseFiltersContainer.attachForm(filterFormStructure);
        
        filterPanelForm.attachEvent("onButtonClick", function (name) {            
            if (name == 'filterButton') {
                if (this.onFilterClicked) {
                    var values = filterPanelForm.getValues();                    
                    this.onFilterClicked.call(this, values.shipmentRef, values.carriersCombo, values.originCombo, values.destCombo,
                        values.warehouseCombo, values.zoneCombo, values.locationsCombo, filterPanelForm.getCalendar('fromDateCalendar').getFormatedDate(), filterPanelForm.getCalendar('toDateCalendar').getFormatedDate());
                }
            } else {
                filterPanelForm.getCombo('carriersCombo').unSelectOption();
                filterPanelForm.getCombo('originCombo').unSelectOption();
                filterPanelForm.getCombo('destCombo').unSelectOption();
                warehouseCombo.selectOption(0);
                filterPanelForm.getCombo('zoneCombo').unSelectOption();
                filterPanelForm.getCombo('locationsCombo').unSelectOption();
                filterPanelForm.setItemValue('shipmentRef', '');
                period.selectOption(0);
                this._setCalendarDate('fromDateCalendar', filterPanelForm, new Date());
                this._setCalendarDate('toDateCalendar', filterPanelForm, new Date());                
            }
        }.bind(this));        

        var customFilter = function (mask, option) {
            var r = false;
            if (mask.length == 0) {
                r = true;
            } else if (option.text.toLowerCase().indexOf(mask.toLowerCase()) !== -1) {
                r = true;
            }
            return r;
        };

        var carriersCombo = filterPanelForm.getCombo('carriersCombo');
        carriersCombo.setFilterHandler(customFilter);

        this._setCalendarDate('fromDateCalendar', filterPanelForm, new Date());
        this._setCalendarDate('toDateCalendar', filterPanelForm, new Date());        

        var period = filterPanelForm.getCombo('period');

        period.load("../api/Common/GetPeriods", function () {
            period.selectOption(0);
        });

        period.attachEvent("onChange", function (value) {
            var dateNow = new Date();
            var newDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - (value - 1));
            this._setCalendarDate('fromDateCalendar', filterPanelForm, newDate);
            this._setCalendarDate('toDateCalendar', filterPanelForm, dateNow);
        }.bind(this));

        var warehouseCombo = filterPanelForm.getCombo('warehouseCombo')

        warehouseCombo.load("../api/Common/GetWarehouses", function () {
            warehouseCombo.selectOption(0);
            if (this.onAccordionReady) {                
                onAccordionReady(warehouseCombo.getSelectedValue());
            }
        }.bind(this));

        warehouseCombo.attachEvent('onChange', function (value) {
            this._refreshCombo('zoneCombo', '../api/Common/GetWarehouseZones?warehouseId=' + value);
            this._refreshCombo('locationsCombo', '../api/Common/GetWarehouseLocations?warehouseId=' + value + '&zoneId=' + filterPanelForm.getValues().zoneCombo);
        }.bind(this));

        filterPanelForm.getCombo('zoneCombo').attachEvent('onChange', function (value) {            
            this._refreshCombo('locationsCombo', '../api/Common/GetWarehouseLocations?warehouseId=' + filterPanelForm.getValues().warehouseCombo + '&zoneId=' + value);
        }.bind(this));

        this.filterPanelForm = filterPanelForm;

        this._refreshCombo = function (comboName, loadUrl) {            
            var combo = filterPanelForm.getCombo(comboName);
            combo.clearAll();
            combo.setComboValue(null);
            combo.setComboText("");
            combo.load(loadUrl, 'json');
        }

    }.call(this);

    return this;
};