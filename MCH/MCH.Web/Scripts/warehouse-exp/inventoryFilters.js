﻿var inventoryFilters = function (accordion, onFilterClicked, onAccordionReady, cell) {
    this.onFilterClicked = onFilterClicked;
    this.onAccordionReady = onAccordionReady;

    this._setCalendarDate = function (name, form, date) {
        var calendar = form.getCalendar(name);
        var calendarToKey = Object.keys(calendar.i)[0];
        calendar.i[calendarToKey].input.value = window.dhx4.date2str(date, '%m-%d-%Y');
        calendar.setDate(date);
    }

    init = function () {        
        var inventoryFiltersContainer = accordion.addItem('inventoryFiltersContainer', 'INVENTORY LOGS');
        var filterFormStructure = [
            { type: "combo", name: "warehouseCombo", label: "Warehouse:", position: "label-left", labelWidth: 90, inputWidth: 180 },
            { type: "calendar", name: "inventoryDate", label: "Inventory Date:", position: "label-left", labelWidth: 90, dateFormat: "%m-%d-%Y", inputWidth: 180 },
            { type: "combo", name: "runsCombo", label: "Runs:", labelWidth: 90, position: "label-left", inputWidth: 180 },
            { type: "block", list: [{ type: "button", name: "resetButton", value: "RESET", offsetLeft: 80 }, { type: "newcolumn" }, { type: "button", name: "filterButton", value: "SEARCH" }] }
        ];

        var filterPanelForm = inventoryFiltersContainer.attachForm(filterFormStructure);

        filterPanelForm.attachEvent("onButtonClick", function (name) {
            if (name == 'filterButton') {
                if (this.onFilterClicked) {
                    var values = filterPanelForm.getValues();
                    this.onFilterClicked.call(this, values.warehouseCombo, filterPanelForm.getCalendar('inventoryDate').getFormatedDate(), values.runsCombo);
                }
            } else {                
                warehouseCombo.selectOption(0);                                
                this._setCalendarDate('inventoryDate', filterPanelForm, new Date());
                this._refreshCombo('runsCombo', '../api/Common/GetInventoryRuns?warehouseId=' + warehouseCombo.getSelectedValue() + '&date=' + filterPanelForm.getCalendar('inventoryDate').getFormatedDate());
            }
        }.bind(this));

        this._setCalendarDate('inventoryDate', filterPanelForm, new Date());

        var warehouseCombo = filterPanelForm.getCombo('warehouseCombo')

        warehouseCombo.load("../api/Common/GetWarehouses", function () {
            warehouseCombo.selectOption(0);
            if (this.onAccordionReady) {
                onAccordionReady(warehouseCombo.getSelectedValue());
            }
        }.bind(this));

        warehouseCombo.attachEvent('onChange', function (value) {            
            this._refreshCombo('runsCombo', '../api/Common/GetInventoryRuns?warehouseId=' + value + '&date=' + filterPanelForm.getCalendar('inventoryDate').getFormatedDate());
        }.bind(this));

        var inventoryDate = filterPanelForm.getCalendar('inventoryDate');
        inventoryDate.attachEvent("onClick", function (value) {
            this._refreshCombo('runsCombo', '../api/Common/GetInventoryRuns?warehouseId=' + warehouseCombo.getSelectedValue() + '&date=' + filterPanelForm.getCalendar('inventoryDate').getFormatedDate());
        }.bind(this));
        
        this._refreshCombo = function (comboName, loadUrl) {
            var combo = filterPanelForm.getCombo(comboName);
            combo.clearAll();
            combo.setComboValue(null);
            combo.setComboText("");
            combo.load(loadUrl, 'json');
        }
    }.call(this);

    return this;
};