﻿function AWBDetialsController(settings) {
    this.inited = false;

    this.Urls = {
        BackToDriver: settings.Urls.BackToDriver,
        NextToHandling: settings.Urls.NextToHandling,
        AWBLookup: settings.Urls.AWBLookup,
        SaveAwbDetials: settings.Urls.SaveAwbDetials,
        Cancel: settings.Urls.Cancel
    }

    this.Controls = {
        Id: function () { return $("#hdnAcaAwbId") },
        FlightTypes: function () { return $("#ddlAWBFlightTypes").data("kendoDropDownList"); },
        CarrierCode: function () { return $("#acaAwbCarrierCode"); },
        SerialNumber: function () { return $("#acaAwbSerialNumber"); },
        FlightNumber: function () { return $("#acaAwbFlightNumber"); },
        FlightDate: function () { return $("#acaAwbFlightDate").data("kendoDatePicker"); },
        Origins: function () { return $("#cbxAWBOrigin").data("kendoComboBox"); },
        Destinations: function () { return $("#cbxAWBDestination").data("kendoComboBox"); },
        Pieces: function () { return $("#txtAcaAwbPieces").data("kendoNumericTextBox"); },
        Weight: function () { return $("#txtAcaAwbWeight").data("kendoNumericTextBox"); },
        WeightUoms: function () { return $("#ddlAcaAwbWeightUom").data("kendoDropDownList"); },

        AWBList: function () { return $("#lsvAcaAwbs").data("kendoListView"); },

        Form: function () { return $("#frmAcaAwbDetails"); },

        Button_Next: function () { return $("#btnAWBDetialsNext"); },
        Button_Back: function () { return $("#btnAWBDetialsPrevious"); },
    }

    this.WizardContainer = function () { return $("#dvAcaAwbDetailsContainer"); }

    this.Window = function () { return $("#wndTMAddTaskView").data("kendoWindow"); }
}



AWBDetialsController.prototype.init = function () {
    if (this.inited == false) {

        this.Controls.Button_Next().click(this.saveAndNext.bind(this));

        this.Controls.Button_Back().click(this.goBackToDriver.bind(this));

        this.Controls.SerialNumber().focusout(this.awbLookup.bind(this));

        //add new awb
        var tmpAwBId = this.Controls.Id().val();
        if (tmpAwBId == 0 || tmpAwBId == -1) {

            if (!this.hasUnfinishedItem()) {
                var tmpCarrierCode = this.Controls.CarrierCode().val();

                this.Controls.AWBList().dataSource.add({ Id: -1, CarrierCode: tmpCarrierCode, AwbSerialNumber: "" });
            }
        }
        this.selectAwbById(tmpAwBId);

        this.inited = true;
    }
}

AWBDetialsController.prototype.hasUnfinishedItem = function () {
    var listView = this.Controls.AWBList();
    var children = listView.element.children();
    var hasUnfinished = false;
    for (var x = 0; x < children.length; x++) {
        if ($(listView.element.children()[x]).attr("data-item-id") == -1) {
            hasUnfinished = true;
        };
    };

    return hasUnfinished;
}

AWBDetialsController.prototype.selectAwbById = function (id) {
    var listView = this.Controls.AWBList();
    var children = listView.element.children();
    var index = -1;
    for (var x = 0; x < children.length; x++) {
        if ($(listView.element.children()[x]).attr("data-item-id") == id) {
            index = x;
        };
    };

    // selects first list view item
    if (index != -1) {
        listView.select(children[index]);
    }
}

AWBDetialsController.prototype.setSelectdAwbItem = function (dataItem) {

}

AWBDetialsController.prototype.awbLookup = function () {

    var tmpCarrierCode = this.Controls.CarrierCode().val();
    var tmpAwbNumber = this.Controls.SerialNumber().val();

    var tmpListView = this.Controls.AWBList();
    var tmpIndex = tmpListView.select().index();

    //update awb in the list
    tmpListView.dataSource.view()[tmpIndex].set("AwbSerialNumber", tmpAwbNumber);
    tmpListView.select(tmpListView.element.children()[tmpIndex]);

    var tmpUrl = this.Urls.AWBLookup + "/?number=" + tmpAwbNumber + "&code=" + tmpCarrierCode;

    $.getJSON(tmpUrl, function (data) {
        if (data) {
            this.setAwbDetials(data);
        }
    }.bind(this));
}

AWBDetialsController.prototype.goBackToDriver = function () {
    var tmpUrl = this.Urls.BackToDriver;
    var tmpWizardWindow = this.Window();
    tmpWizardWindow.setOptions({ url: tmpUrl });
    tmpWizardWindow.refresh(tmpUrl);
}

AWBDetialsController.prototype.setAwbDetials = function (awbData) {

    if (awbData.ConnectedToShipper == true) {
        this.Controls.SerialNumber().val("");
        this.Controls.SerialNumber().trigger("focusout");
    }
    else {
        this.Controls.Id().val(awbData.Id);
        this.Controls.FlightNumber().val(awbData.FlightNumber);
        this.Controls.FlightDate().value(new Date(parseInt(awbData.FlightDate.substr(6))));
        this.Controls.Origins().value(awbData.OriginId);
        this.Controls.Destinations().value(awbData.DestinationId);
        this.Controls.Pieces().value(awbData.Pieces);
        this.Controls.Weight().value(awbData.Weight);
        this.Controls.WeightUoms().value(awbData.WeightUomId);
    }
}

AWBDetialsController.prototype.saveAndNext = function () {

    var tmpId = this.Controls.Id().val();

    var awbId = (tmpId == 0 ? "" : tmpId);

    var validator = this.Controls.Form().kendoValidator().data("kendoValidator");
    if (validator.validate()) {
        var tmpData = this.Controls.Form().serializeObject();
        var tmpSaveUrl = this.Urls.SaveAwbDetials;
        var tmpUrl = this.Urls.NextToHandling;
        var tmpWizard = this.WizardContainer();
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            url: tmpSaveUrl,
            data: JSON.stringify(tmpData),
            success: function (data) {
                tmpUrl = tmpUrl + "/?id=&awbId=" + data.Id;
                tmpWizard.load(tmpUrl);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    }
}

AWBDetialsController.prototype.cancel = function () {
    var tmpCancelUrl = this.Urls.Cancel;
    var tmpWindow = this.Window();
    window.DIALOG_HANDLER.openConfirmation(function () {
        $.ajax({
            url: tmpCancelUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                tmpWindow.close();
            }
        })
    }, null, "Cancel Wizard", "Are you sure you want to cancel wizard ? You'll be have to start from very beginning.");
}