﻿function AWBShipperController(settings) {
    this.inited = false;

    this.Urls = {
        BackToHandling: settings.Urls.BackToHandling,
        NextToScreening: settings.Urls.NextToScreening,
        SaveShipperVerify: settings.Urls.SaveShipperVerify,
        Cancel : settings.Urls.Cancel
    }

    this.Controls = {

        Form : function(){return $("#frmAcaAwbShipperVerify");},
        AwbId: function () { return $("#hdnAcaAwbId") },
        Button_Next: function () { return $("#btnAWBShipperNext"); },
        Button_Back: function () { return $("#btnAWBShipperPrevious"); },
    }

    this.WizardContainer = function () { return $("#dvAcaAwbDetailsContainer"); }

    this.Window = function () { return $("#wndTMAddTaskView").data("kendoWindow"); }
}


AWBShipperController.prototype.init = function () {
    if (this.inited == false) {

        this.Controls.Button_Next().click(this.saveAndNext.bind(this));

        this.Controls.Button_Back().click(this.goBackHandling.bind(this));

        this.inited = true;
    }
}

AWBShipperController.prototype.goBackHandling = function () {
    var tmpUrl = this.Urls.BackToHandling;
    tmpUrl = tmpUrl + "/?awbId=" + this.Controls.AwbId().val();
    this.WizardContainer().load(tmpUrl);
}

AWBShipperController.prototype.saveAndNext = function () {

    var tmpFormData = this.getFormData();
    var tmpSaveUrl = this.Urls.SaveShipperVerify;

    var awbId = this.Controls.Form().find("#hdnAcaAwbId").val();

    var tmpData = {
        AwbId: awbId,
        ShipperVeriryFields : tmpFormData
    };

    var self = this;

    $.ajax({
        url: tmpSaveUrl,
        type: "POST",
        data: JSON.stringify(tmpData),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {

            var tmpUrl = self.Urls.NextToScreening + "/?id=&awbId=" + data.Id;

            self.WizardContainer().load(tmpUrl);
        }
    })
}

AWBShipperController.prototype.getFormData = function () {
    var tmpFormData = this.Controls.Form().serializeObject();

    var tmpFormInputs = this.Controls.Form().find(".dynamic-field");
    var tmpFormChecks = this.Controls.Form().find(".dynamic-check");
    var tmpFormNumerics = this.Controls.Form().find("input.dynamic-numeric.k-input[name]");
    var tmpFormDates = this.Controls.Form().find("input.dynamic-date.k-input[name]");

    var tmpFormFields = new Array();

    for (var i = 0; i < tmpFormInputs.length; i++) {
        var tmpInput = tmpFormInputs[i];
        tmpFormFields.push({ FieldId: tmpInput.id, Value: $(tmpInput).val() })
    }

    for (var i = 0; i < tmpFormNumerics.length; i++) {
        var tmpInput = tmpFormNumerics[i];
        var tmpKendoInput = $(tmpInput).data("kendoNumericTextBox");
        tmpFormFields.push({ FieldId: tmpInput.id, Value: tmpKendoInput.value() })
    }
    for (var i = 0; i < tmpFormDates.length; i++) {
        var tmpInput = tmpFormDates[i];
        var tmpKendoInput = $(tmpInput).data("kendoDatePicker");
        tmpFormFields.push({ FieldId: tmpInput.id, Value: tmpKendoInput.value() })
    }

    for (var i = 0; i < tmpFormChecks.length; i++) {
        var tmpInput = tmpFormChecks[i];
        tmpFormFields.push({ FieldId: tmpInput.id, Value: $(tmpInput).is(":checked") ? "True" : "False" })
    }

    return tmpFormFields;
}

AWBShipperController.prototype.cancel = function () {
    var tmpCancelUrl = this.Urls.Cancel;
    var tmpWindow = this.Window();
    window.DIALOG_HANDLER.openConfirmation(function () {
        $.ajax({
            url: tmpCancelUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                tmpWindow.close();
            }
        })
    }, null, "Cancel Wizard", "Are you sure you want to cancel wizard ? You'll be have to start from very beginning.");
}