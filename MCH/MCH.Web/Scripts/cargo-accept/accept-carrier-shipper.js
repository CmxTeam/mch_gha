﻿
/// <reference path="../controls/document-uploader.js" />
/// <reference path="../controls/signature-pad.js" />


function AcceptCarrierShipper(argUrls) {
    this.initialized = false;

    this.Uploader = null;
    this.SignPad = null;

    this.Signature = "";

    this.Urls =
        {
            SaveCarrierShipper: argUrls.SaveCarrierShipper,
            GoToDrivers: argUrls.GoToDrivers,
            Cancel: argUrls.Cancel
        },

    this.Window = function () { return $("#wndTMAddTaskView").data("kendoWindow"); }
    this.SignatureWindow = function () { return $("#wndCASigniture").data("kendoWindow"); }

    this.FormControls =
        {
            NeedsRestore: function () {
                var tmpNeeds = false;
                tmpNeeds = $("#hdnNeedsRestore").val() == 1;
                return tmpNeeds;
            },
            Carriers: function () { return $("#ddlAccountCarriers").data("kendoDropDownList"); },
            FlightTypes: function () { return $("#ddlFlightTypes").data("kendoDropDownList") },
            ShipperTypes: function () { return $("#ddlShipperTypes").data("kendoDropDownList"); },
            ApprovedTSACarriers: function () { return $("#ddlApprovedTSACarriers").data("kendoComboBox"); },
            DocumentTypes: function () { return $("#ddlDocumentTypes").data("kendoDropDownList"); },
            ShipperDocsVerified: function () { return $("#chbxDocumentVerified").data("kendoComboBox"); },
            ApprovedIACs: function () { return $("#ddlApprovedIACs").data("kendoComboBox"); },
            ApprovedKnownShippers: function () { return $("#ddlApprovedKnownShippers").data("kendoComboBox"); },
            ShipperName: function () { return $("#txtShipperName"); },
            ShipperStatusImage: function () { return $("#imgShipperStatus"); },
            GovAuthIds: function () { return $("#ddlGovAuthIds").data("kendoComboBox"); },
            BtnSaveNext: function () { return $("#btnNextToDrivers"); },
            BtnACASigPadClear: function () { return $("#btnACASigPadClear"); },
            BtnACASigPadConfirm: function () { return $("#btnACASigPadConfirm"); },
            AttachmentsGrid: function () { return $("#gridShipperCarrierDocuments").data("kendoGrid"); },
            ShipperVerified: function () { return $("#hdnShipperVerified"); },
            SigPad: function () { return document.getElementById("acaSigPad"); },
            Form: function () { return $("#frmCargoAcceptance"); },

            BtnACAOpenSignPad: function () { return $("#btnACAOpenSignWindow"); },
            SignImage: function () { return $("#imgACAFormSignature"); },
            HdnNeedsRestore: function () { return $("#hdnNeedsRestore"); }
        }


    this.FormSections =
        {
            ApprovedIACs: function () { return $("#dvApprovedIACs"); },
            ApprovedTSACarriers: function () { return $("#dvApprovedTSACarriers"); },
            ApprovedKnwonShippers: function () { return $("#dvApprovedKnownShippers"); },
            DocumentTypes: function () { return $("#dvDocumentTypes"); },
            DocumentUploader: function () { return $("#dvDocumentUploader"); },
            ShipperDocsVerified: function () { return $("#dvShipperDocsVerified"); },
            SignaturePad: function () { return $("#dvShigniturePad"); },
            Attachments: function () { return $("#dvDocumentUploader"); },
            GovAuthIds: function () { return $("#dvGovAuthIds"); }
        }

    this.Constants =
        {
            ShipperTypes: { Gov: 1, Shipper: 2, Carrier: 3, IAC: 4 },
        }
}

AcceptCarrierShipper.prototype.onUploaderClose = function () {
    this.Uploader.clear();
}

AcceptCarrierShipper.prototype.onSignatureClose = function () {
    this.SignPad.close();
}

AcceptCarrierShipper.prototype.openSignaturePad = function () {
    this.SignPad.init();
    this.SignPad.open();
    this.SignatureWindow().setOptions({ close: this.onSignatureClose.bind(this) });
}

AcceptCarrierShipper.prototype.onSignatureAccepted = function () {
    this.FormControls.SignImage().attr('src', this.SignPad.SignatureData);
}

AcceptCarrierShipper.prototype.restoreState = function () {
    var selectedShipperType = this.FormControls.ShipperTypes().dataItem();
    this.resetVisibility();
    this.adaptToSellectedShipper(selectedShipperType);
}

AcceptCarrierShipper.prototype.init = function () {

    if (this.initialized === false) {

        //set the drop down list change event handlers
        this.FormControls.Carriers().setOptions({ change: this.CarrierChanged.bind(this) });

        this.FormControls.FlightTypes().setOptions({
            change: this.FlightTypeChanged.bind(this)
        });

        this.FormControls.ShipperTypes().setOptions({ change: this.ShipperTypeChnaged.bind(this) });

        this.FormControls.DocumentTypes().setOptions({ change: this.DocumentTypeChanged.bind(this) });

        this.FormControls.ApprovedTSACarriers().setOptions({ change: this.ApprovedTSACarriersChanged.bind(this) });

        this.FormControls.ApprovedIACs().setOptions({ change: this.ApprovedIACsChanged.bind(this) });

        var tmpSaveNextButton = this.FormControls.BtnSaveNext();
        var tmpGrid = this.FormControls.AttachmentsGrid();
        var tmpDocVerified = this.FormControls.ShipperDocsVerified();


        tmpSaveNextButton.click(this.saveNextToDriver.bind(this))

        if (this.FormControls.NeedsRestore()) {
            this.restoreState();
        }

        this.FormControls.BtnACAOpenSignPad().click(this.openSignaturePad.bind(this));

        this.SignPad = new SignaturePad({
            WindowContainerId: "wndCASigniture",
            CanvasId: "canvasACASignPad",
            ClearId: "btnACASignClear",
            AcceptId: "btnACASignAccept",
            onSignatureAccept: this.onSignatureAccepted.bind(this)
        });

        this.initialized = true;
    }
}

AcceptCarrierShipper.prototype.saveNextToDriver = function () {
    var tmpNextDriversStep = this.Window();
    var tmpNextStepUrl = this.Urls.GoToDrivers;
    var tmpSignRequired = this.FormSections.SignaturePad().is(":visible");
    var tmpCanvasValue = this.FormControls.SignImage().attr("src");
    var tmpGrid = this.FormControls.AttachmentsGrid();
    var tmpDocVerified = this.FormControls.ShipperDocsVerified();
    // validation form
    var validator = this.FormControls.Form().kendoValidator({
        rules: {
            checkDocuments: function (input) {
                if (input.is("[name=checkdoc]")) {
                    return tmpGrid.dataSource.data().length > 0;
                } else {
                    return true;
                }
            },
            checkDocumentVerified: function (input) {
                if (input.is("[name=verifyDocs]")) {
                    return tmpDocVerified.value() == "True";
                } else {
                    return true;
                }
            },
            checkSigniture: function (input) {
                if (input.is("[name=check-signiture]")) {
                    if (tmpSignRequired) {
                        return tmpCanvasValue != "";
                    }
                    else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        },
        messages:
            {
                checkSigniture: "Please sign and confirm.",
                checkDocuments: "Documents cannot be empty.",
                checkDocumentVerified: "Please verify the documents."
            },
    }).data("kendoValidator");

    if (validator.validate()) {
        var tmpAttachments = this.FormControls.AttachmentsGrid().dataSource.data();
        var tmpFormData = new FormData();

        var formElementsJson = this.FormControls.Form().serializeObject();
        formElementsJson.SignImageData = tmpCanvasValue;
        formElementsJson.Documents = [];
        if (tmpAttachments && tmpAttachments.length > 0) {
            for (var i = 0; i < tmpAttachments.length; i++) {
                var documentItem = tmpAttachments[i];
                formElementsJson.Documents.push({
                    Id: documentItem.Id,
                    Name: documentItem.Name,
                    ShipperDocTypeId: documentItem.ShipperDocTypeId,
                    DocumentType: documentItem.DocumentType,
                    IsSnapshot: documentItem.IsSnapshot,
                    SnapshotData: documentItem.SnapshotData,
                    Path: documentItem.Path
                });

                if (documentItem.IsSnapshot == false) {
                    tmpFormData.append("file", documentItem.File);
                }
            }
        }

        tmpFormData.append("formData", JSON.stringify(formElementsJson));
        var tmpUrl = this.Urls.SaveCarrierShipper;
        var tmpNextPageUrl = this.Urls.GoToDrivers;
        $.ajax({
            async: true,
            type: 'POST',
            url: tmpUrl,
            data: tmpFormData,
            success: function (data) {
                var tmpUrl = tmpNextPageUrl;
                tmpNextDriversStep.setOptions({ url: tmpUrl });
                tmpNextDriversStep.refresh(tmpUrl);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
}

AcceptCarrierShipper.prototype.setUploader = function (uploader) {
    this.Uploader = uploader;
    //setup event handlers
    this.Uploader.setEventHandlers({
        onAttachemntAdd: function () {
        }.bind(this)
    })
}

AcceptCarrierShipper.prototype.FlightTypeChanged = function () {

    this.FormControls.ShipperTypes().dataSource.read();
}

AcceptCarrierShipper.prototype.DocumentTypeChanged = function () {
    var tmpGetSelected = this.FormControls.DocumentTypes().value();
}

AcceptCarrierShipper.prototype.shipperStatusVerified = function (isVerified) {
    var image = this.FormControls.ShipperStatusImage();
    var tmpSrc = image.attr("src");
    var srcSplitted = tmpSrc.split("_");
    var tmpNewSrc = srcSplitted[0] + "_" + (isVerified ? "yes.png" : "no.png");
    image.attr("src", tmpNewSrc);
    this.FormControls.ShipperVerified().val(isVerified ? "True" : "False");
}

AcceptCarrierShipper.prototype.ApprovedIACsChanged = function () {
    var tmpCarrier = this.FormControls.ApprovedIACs().dataItem();
    if (tmpCarrier != null) {
        if (tmpCarrier.Id != -1) {
            this.FormControls.ShipperName().val(tmpCarrier.Name);
            this.shipperStatusVerified(true);

        } else {
            this.FormControls.ShipperName().val("");
            this.shipperStatusVerified(false);
        }
    }
}

AcceptCarrierShipper.prototype.ApprovedTSACarriersChanged = function () {
    var tmpCarrier = this.FormControls.ApprovedTSACarriers().dataItem();
    if (tmpCarrier != null) {
        if (tmpCarrier.Id != -1) {
            this.FormControls.ShipperName().val(tmpCarrier.Name);
            this.shipperStatusVerified(true);
        } else {

            this.FormControls.ShipperName().val("");
            this.shipperStatusVerified(false);
        }
    }
}

AcceptCarrierShipper.prototype.resetVisibility = function ()
{
    this.FormSections.ApprovedIACs().hide();
    this.FormSections.ApprovedKnwonShippers().hide();
    this.FormSections.ApprovedTSACarriers().hide();
    this.FormSections.SignaturePad().hide();
    this.FormSections.GovAuthIds().hide();
    this.FormSections.Attachments().hide();
    this.FormSections.ShipperDocsVerified().hide();
}

AcceptCarrierShipper.prototype.resetData = function ()
{
    this.FormControls.ApprovedIACs().text('');
    this.FormControls.ApprovedKnownShippers().text('');
    this.FormControls.ApprovedTSACarriers().text('');
    this.FormControls.DocumentTypes().select(0);
    this.FormControls.GovAuthIds().text('');
    this.FormControls.ShipperDocsVerified().text('');
    //this.FormControls.ShipperDocsVerified().prop("disabled", true);


    this.FormControls.ShipperName().val("");
    //this.FormControls.ShipperName().prop('disabled', false);
    //this.shipperStatusVerified(false);
}

AcceptCarrierShipper.prototype.reset = function () {
   
    this.resetVisibility();

    this.resetData();
}

AcceptCarrierShipper.prototype.adaptToGovShipperType = function (shipperType) {

    this.FormControls.ShipperName().val(shipperType.Name);
    this.shipperStatusVerified(true);
    this.FormSections.GovAuthIds().show();
    this.FormControls.GovAuthIds().dataSource.read();
}

AcceptCarrierShipper.prototype.adaptToCarrierShipperType = function (shipperType) {

    this.FormSections.ApprovedTSACarriers().show();
    this.FormControls.ApprovedTSACarriers().refresh();
    this.FormControls.ApprovedTSACarriers().dataSource.read();
}

AcceptCarrierShipper.prototype.adaptToIACShipperType = function (shipperType) {
    this.FormSections.ApprovedIACs().show();
    this.FormControls.ApprovedIACs().refresh();
    this.FormControls.ApprovedIACs().dataSource.read();
}

AcceptCarrierShipper.prototype.adaptToShipperShipperType = function (shipperType) {
    this.FormSections.SignaturePad().show();
}

AcceptCarrierShipper.prototype.getSignImageData = function (close) {
    var tmpBase64String = "";
    if (NumberOfTabletPoints() != 0) {
        SetTabletState(0, window.TMR);

        SetImageXSize(500);
        SetImageYSize(100);
        SetImagePenWidth(5);
        GetSigImageB64(function (str) {
            (str);
            this.Signature = str;
        }.bind(this))

        if (close == true) {

        }
    } else {
        alert("Please sign before continuing. ");
    }

}

AcceptCarrierShipper.prototype.adaptToSellectedShipper = function (selectedShipperType) {
    
    this.FormControls.DocumentTypes().dataSource.read();
    if (selectedShipperType != null) {
        if (selectedShipperType.Id == -1) {
            this.FormSections.ShipperDocsVerified().hide();
            this.FormSections.DocumentUploader().hide();
        } else {
            this.FormSections.ShipperDocsVerified().show();
            this.FormSections.DocumentUploader().show();
        }
        switch (selectedShipperType.GroupId) {
            case this.Constants.ShipperTypes.Gov:
                {
                    this.adaptToGovShipperType(selectedShipperType);
                    break;
                }
            case this.Constants.ShipperTypes.Carrier:
                {
                    this.adaptToCarrierShipperType(selectedShipperType);
                    break;
                }
            case this.Constants.ShipperTypes.IAC:
                {
                    this.adaptToIACShipperType(selectedShipperType);
                    break;
                }
            case this.Constants.ShipperTypes.Shipper: {
                this.adaptToShipperShipperType(selectedShipperType);
                break;
            }
            default:
        }
    }
}

AcceptCarrierShipper.prototype.ShipperTypeChnaged = function (e) {
    
    var selectedShipperType = this.FormControls.ShipperTypes().dataItem();
    this.reset();
    this.adaptToSellectedShipper(selectedShipperType);
}

AcceptCarrierShipper.prototype.CarrierChanged = function (e) {
    this.FormControls.ApprovedKnownShippers().dataSource.read();
}

AcceptCarrierShipper.prototype.FilterByCarrier = function () {
    return { accountCarrierId: this.FormControls.Carriers().value() };
}

AcceptCarrierShipper.prototype.FilterByShipperType = function () {
    return { shipperTypeId: this.FormControls.ShipperTypes().value() };
}

AcceptCarrierShipper.prototype.FilterByFlightType = function () {
    return { flightTypeId: this.FormControls.FlightTypes().value() };
}

AcceptCarrierShipper.prototype.cancel = function () {
    var tmpCancelUrl = this.Urls.Cancel;
    var tmpWindow = this.Window();
    window.DIALOG_HANDLER.openConfirmation(function () {
        $.ajax({
            url: tmpCancelUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                tmpWindow.close();
            }
        })
    }, null, "Cancel Wizard", "Are you sure you want to cancel wizard ? You'll be have to start from very beginning.");
}





