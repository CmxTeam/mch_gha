﻿function AWBScreeningController(settings) {
    this.inited = false;

    this.Urls = {
        BackToShipper: settings.Urls.BackToShipper,
        Finalize: settings.Urls.Finalize,
        GetAWBDetialView: settings.Urls.GetAWBDetialView,
        SaveCurrentAWB: settings.Urls.SaveCurrentAWB,
        Cancel: settings.Urls.Cancel
    }

    this.Controls = {
        Id: function () { return $("#frmAcaAwbScreening").find("#awbId"); },
        Form: function () { return $("#frmAcaAwbScreening") },
        SealNumberGrid: function () { return $("#gridAcaAwbScreening").data("kendoGrid"); },
        Tenderd: function () { return $("input[name='IsTenderedAsScreened']"); },        
        IsTenderd: function () { return $("input[name='IsTenderedAsScreened']:checked").val() == "Yes"; },
        ApprovedCCSFs: function () { return $("#ddlAcaAwbScreeningApprovedCCSF").data("kendoComboBox"); },

        Button_AddSealNumber: function () { return $("#btnAcaAwbScreeningAdd"); },
        Button_AddNewAWB: function () { return $("#btnAWBScreeningAddNew"); },
        Button_Back: function () { return $("#btnAWBScreeningPrevious"); },
        Button_Finalize: function () { return $("#btnAWBScreeningFinalize") },

        //add seal number popup
        Button_SaveSealNumber: function () { return $("#btnAddSealNumberScreening"); },
        SealTypes: function () { return $("#ddlAcaAwbSealTypes").data("kendoDropDownList"); },
        SealNumber: function () { return $("#txtAcaAwbScreeningSealNumber"); }
    }

    this.Sections = {
        CheckScreening: function () { return $("#dvAcaAwbScreeningTendered"); },
        AlternativeScreening: function () { return $("#dvAcaAwbScreeningNotTendered") },
    }

    this.Window_AddSealNumber = function () { return $("#wndAcaAwbScreeningAddSeal").data("kendoWindow"); },

    this.Window_AddSealNumberContainer = function () { return $("#wndAcaAwbScreeningAddSeal"); }

    this.WizardContainer = function () { return $("#dvAcaAwbDetailsContainer"); }

    this.Window = function () { return $("#wndTMAddTaskView").data("kendoWindow"); }
}


AWBScreeningController.prototype.init = function () {
    if (this.inited == false) {

        var self = this;
        this.Controls.Tenderd().change(function () {
            if ($(this).val() == "Yes") {
                self.Sections.AlternativeScreening().hide();
                self.Sections.CheckScreening().show();
            } else {
                self.Sections.AlternativeScreening().show();
                self.Sections.CheckScreening().hide();
            }
        })

        this.Window_AddSealNumber().setOptions({
            close: this.closeAddSealNumber.bind(this)
        });

        this.Controls.Button_SaveSealNumber().click(this.saveSealNumber.bind(this));

        this.Controls.Button_AddSealNumber().click(this.showAddSealNumberPopup.bind(this));

        this.Controls.Button_AddNewAWB().click(this.addNewAWB.bind(this));

        this.Controls.Button_Back().click(this.goBackToShipper.bind(this));

        this.Controls.Button_Finalize().click(this.finalize.bind(this))

        this.inited = true;
    }
}

AWBScreeningController.prototype.getFormData = function () {
    var tmpAwbId = this.Controls.Id().val();
    var tmpCCSF = this.Controls.ApprovedCCSFs().dataItem();
    var tmpIsTendered = this.Controls.IsTenderd();
    var tmpSealNumbers = this.getSealNumbers();
    var tmpOptions = this.getAltScreeningValues();

    var tmpFormData = {
        AwbId: tmpAwbId,
        IsTendered: tmpIsTendered,
        SelaNumbers: tmpSealNumbers,
        AlernativeScreeningOptions: tmpOptions
    };
    if (tmpCCSF) {
        tmpFormData.ApprovedCCSFId = tmpCCSF.Id;
    }

    return tmpFormData;
}

AWBScreeningController.prototype.getSealNumbers = function () {
    var tmpGridItems = this.Controls.SealNumberGrid().dataSource.data();
    var tmpSealNumbers = new Array();
    for (var i = 0; i < tmpGridItems.length; i++) {
        var tmpItem = tmpGridItems[i];
        tmpSealNumbers.push({ Id: tmpItem.Id, SealTypeId: tmpItem.SealTypeId, SealNumberValue: tmpItem.SealNumberValue });
    }

    return tmpSealNumbers;
}


AWBScreeningController.prototype.getAltScreeningValues = function () {
    var tmpAltScreeningOptions = this.Sections.AlternativeScreening().find(".dynamic-alt-screening:checked");
    var tmpOptions = new Array();
    for (var i = 0; i < tmpAltScreeningOptions.length; i++) {
        var tmpItem = tmpAltScreeningOptions[i];
        var tmpItemValue = $(tmpItem).val();
        var tmpItemId = tmpItem.name;

        tmpOptions.push({ Id: tmpItemId, Checked: tmpItemValue == "Yes" ? true : false });
    }

    return tmpOptions;
}

AWBScreeningController.prototype.saveSealNumber = function () {
    var tmpSelectedSealType = this.Controls.SealTypes().dataItem();
    var tmpSealNumber = this.Controls.SealNumber().val();

    var tmpNewItem = { Id: "", SealTypeId: tmpSelectedSealType.Id, SealTypeName: tmpSelectedSealType.Name, SealNumberValue: tmpSealNumber };


    var tmpValidator = this.Window_AddSealNumberContainer().find("form").kendoValidator().data("kendoValidator");
    if (tmpValidator.validate()) {

        this.Controls.SealNumberGrid().dataSource.add(tmpNewItem);
        this.Window_AddSealNumber().close();
    }
}

AWBScreeningController.prototype.closeAddSealNumber = function () {
    this.Controls.SealNumber().val("");
    this.Controls.SealTypes().select(0);
}

AWBScreeningController.prototype.showAddSealNumberPopup = function () {
    var tmpWindow = this.Window_AddSealNumber();
    tmpWindow.setOptions({ minWidth: 500 });
    tmpWindow.center().open();
}

AWBScreeningController.prototype.addSealNumber = function (data) {
    var tmpData = this.Controls.SealNumberGrid().dataSource.data();
    var idLength = tmpData.length;

    this.Controls.SealNumberGrid().dataSource.add({ Id: idLength + 1, SealTypeId: data.SealTypeId, SealNumberValue: data.SealNumber });
}

AWBScreeningController.prototype.addNewAWB = function () {

    //save awb and add new
    var self = this;
    var tmpSaveAwbUrl = this.Urls.SaveCurrentAWB;
    var tmpData = this.getFormData();
    $.ajax({
        url: tmpSaveAwbUrl,
        type: "POST",
        data: JSON.stringify(tmpData),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            var tmpUrl = self.Urls.GetAWBDetialView;
            self.Window().refresh(tmpUrl);
        }
    })
}

AWBScreeningController.prototype.finalize = function () {
    //save awb and finalize
    var self = this;
    var tmpSaveAndFinalize = this.Urls.Finalize;
    var tmpData = this.getFormData();
    $.ajax({
        url: tmpSaveAndFinalize,
        type: "POST",
        data: JSON.stringify(tmpData),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            var tmpWindow = self.Window();
            tmpWindow.setOptions({ close: function () { } });
            tmpWindow.close();
        }
    })
}

AWBScreeningController.prototype.goBackToShipper = function () {
    var tmpUrl = this.Urls.BackToShipper;

    tmpUrl = tmpUrl + "/?awbId=" + this.Controls.Id().val();

    this.WizardContainer().load(tmpUrl);
}


AWBScreeningController.prototype.cancel = function () {
    var tmpCancelUrl = this.Urls.Cancel;
    var window = this.Window();
    window.DIALOG_HANDLER.openConfirmation(function () {
        $.ajax({
            url: tmpCancelUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                window.close();
            }
        })
    }, null, "Cancel Wizard", "Are you sure you want to cancel wizard ? You'll be have to start from very beginning.");
}