﻿function AWBWizardStepController(settings) {
    this.inited = false;
    this.StepId = settings.Step;
    this.WizardId = settings.WizardId;
    this.AddedAwb = null;


    this.Controls =
        {
            WizardContainer: function () { return $("#dvAcaAwbDetailsContainer"); },
            AWBList: function () { return $("#lsvAcaAwbs").data("kendoListView"); },
            AWBListRoot: function () { return $("#lsvAcaAwbs"); },
            CurrentAwbId: function () { return $("#hdnAcaAwbCurrentId"); },
            HasAwb: function () { return this.CurrentAwbId().val() != "" }
        }

    this.Urls =
        {
            LoadAWBDetials: settings.Urls.LoadAWBDetials,
            Cancel : settings.Urls.Cancel
        }
}

AWBWizardStepController.prototype.init = function () {
    if (this.inited == false) {
        if (this.Controls.HasAwb()) {
            this.loadAWBDetials(this.Controls.CurrentAwbId().val());
        } else {
            this.loadAWBDetials("");
        }

        this.Controls.AWBList().setOptions({
            change: this.AWBSelect.bind(this)
        })

        this.init = true;
    }
}

AWBWizardStepController.prototype.AWBSelect = function (e) {
    var index = e.sender.select().index(),
    dataItem = e.sender.dataSource.view()[index];

    if (!this.isAWBLoaded(dataItem.Id)) {
        this.loadAWBDetials(dataItem.Id);
    }
}

AWBWizardStepController.prototype.isAWBLoaded = function (id) {
    var loadedAWBId = this.Controls.WizardContainer().find("#hdnAcaAwbId");
    var loaded = false;
    if (loadedAWBId) {
        loaded = loadedAWBId.val() == id;
    }

    return loaded;
}

AWBWizardStepController.prototype.loadAWBDetials = function (awbId) {
    var tmpUrl = this.Urls.LoadAWBDetials + "/?id=" + this.WizardId + "&awbId=" + awbId + "&stepId=" + this.StepId;

    this.Controls.WizardContainer().load(tmpUrl);
}