﻿
/// <reference path="../controls/photo-capturer.js" />

function ACADriverController(argUrls) {
    this.initialized = false;
    this.IDCapturer = null;
    this.PhotoCapturere = null;

    this.Urls =
        {
            SaveACADriver: argUrls.SaveACADriver,
            SaveTruckingCompany: argUrls.SaveTruckingCompany,
            GoToAWBs: argUrls.GoToAWBs,
            GetAWBView: argUrls.GetAWBView,
            GetDriverDetails: argUrls.GetDriverDetials,
            BackToShipper: argUrls.BackToShipper,
            Cancel: argUrls.Cancel
        },

    this.Window = function () { return $("#wndTMAddTaskView").data("kendoWindow"); }
    this.Window_AddDriver = function () { return $("#wndACADriverAdd").data("kendoWindow"); };
    this.Window_AddTruckingCmp = function () { return $("#wndACATruckingCompAdd").data("kendoWindow"); }

    this.FormControls =
        {
            Drivers: function () { return $("#ddlDrivers").data("kendoComboBox"); },
            TruckingCompanies: function () { return $("#ddlTruckingCompany").data("kendoComboBox") },
            STANumber: function () { return $("#txtACADriverSTA"); },
            DriverLicense: function () { return $("#txtACADriverLicense"); },
            States: function () { return $("#ddlDriverState").data("kendoComboBox"); },
            FirstIDTypes: function () { return $("#ddlFirstIDTypes").data("kendoComboBox"); },
            FirstIDMatchedCheck: function () { return $("#chbxFirstIDMatched").data("kendoComboBox"); },
            IsFirstIDMatched: function () {
                var isMatched = this.FirstIDMatchedCheck().value();
                if (isMatched == "False") {
                    return false;
                }
                else {
                    return true;
                }
            },
            SecondIDTypes: function () { return $("#ddlSecondIDTypeId").data("kendoComboBox"); },
            SecondIDMatchedCheck: function () { return $("#chbxSecondIDMatched").data("kendoComboBox") },

            Image_CaptureId: function () { return $("#imgAcaCaptureId"); },
            ImageValue_CaptureId: function () { return $("#hdnAcaCaptureId"); },

            Image_CapturePhoto: function () { return $("#imgAcaCapturePhoto"); },
            ImageValue_CapturePhoto: function () { return $("#hdnAcaCapturePhoto"); },

            Button_CaptureId: function () { return $("#btnAcaCaptureId"); },
            Button_CapturePhoto: function () { return $("#btnAcaCapturePhoto"); },

            Button_AddDriver: function () {
                return $("#btnACAAddDriver");
            },
            Button_AddTruckingComp: function () {
                return $("#btnACAAddTruckComp");
            },
            Button_SumbitDriver: function () { return $("#btnFromDriver"); },
            Button_SubmitTruckComp: function () { return $("#btnFromTruckingComp") },

            Button_NextToAWB: function () { return $("#btnNextToAWBs") },

            Button_BackToShipper: function () { return $("#btnGoBackToShipper") },

            Form: function () { return $("#frmACADriver"); },
            FormPhoto : function(){return $("#frmACADriverPhotos");},
            FormWndDriver: function () { return $("#frmACADriverAdd"); },
            FormWndTruckCom: function () { return $("#frmACATruckCompAdd") }
        }

    this.FormSections =
        {
            SecondIDTypes: function () { return $("#dvSecondIDType"); }
        }
}

ACADriverController.prototype.onUploaderClose = function () {
    this.Uploader.clear();
}

ACADriverController.prototype.init = function () {

    if (this.initialized == false) {
        //set the drop down list change event handlers
        this.FormControls.TruckingCompanies().setOptions({ change: this.onTruckingCompChange.bind(this) });

        this.FormControls.Drivers().setOptions({ change: this.onDriversChange.bind(this) });

        this.FormControls.FirstIDMatchedCheck().setOptions({ change: this.onFirstIDCheckChange.bind(this) });

        this.FormControls.Button_AddDriver().click(function () {
            var tmpWindow = this.Window_AddDriver();
            tmpWindow.setOptions({ minWidth: 500 })
            tmpWindow.center().open();
        }.bind(this));

        this.FormControls.Button_SumbitDriver().click(this.submitDriver.bind(this));

        this.FormControls.Button_AddTruckingComp().click(function () {
            var tmpWindow = this.Window_AddTruckingCmp();
            tmpWindow.setOptions({ minWidth: 500 })
            tmpWindow.center().open();
        }.bind(this));

        this.FormControls.Button_SubmitTruckComp().click(this.submitTruckingComp.bind(this));

        this.FormControls.Button_CaptureId().click(this.showDriverIdCapture.bind(this));

        this.FormControls.Button_CapturePhoto().click(this.showDriverPhotoCapture.bind(this));

        this.FormControls.Button_BackToShipper().click(this.backToShipper.bind(this));

        this.FormControls.Button_NextToAWB().click(this.saveAndShowAWBStep.bind(this));

        var tmpCaptureIdPhoto = this.FormControls.ImageValue_CaptureId().val()
        if (tmpCaptureIdPhoto != "") {
            this.FormControls.Image_CaptureId().attr("src", tmpCaptureIdPhoto);
        }

        var tmpCaptureDriverPhoto = this.FormControls.ImageValue_CapturePhoto().val();
        if (tmpCaptureDriverPhoto != "") {
            this.FormControls.Image_CapturePhoto().attr("src", tmpCaptureDriverPhoto);
        }

        this.initialized = true;
    }
}

ACADriverController.prototype.backToShipper = function () {
    var tmpWizardWindow = this.Window();
    var tmpUrl = this.Urls.BackToShipper;
    tmpWizardWindow.setOptions({ url: tmpUrl });
    tmpWizardWindow.refresh(tmpUrl);
}

ACADriverController.prototype.saveAndShowAWBStep = function () {
    var tmpSecondIdRequired = this.FormSections.SecondIDTypes().is(":visible");
    var tmpSecondMatch = this.FormControls.SecondIDMatchedCheck().value();
    var tmpPhotoValidator = this.FormControls.FormPhoto().kendoValidator({}).data("kendoValidator");
    var tmpValidator = this.FormControls.Form().kendoValidator({
        rules: {
            checkSecondID: function (input) {
                if (input.is("[name=checkSecondID]")) {
                    if (tmpSecondIdRequired) {
                        return tmpSecondMatch == "True";
                    }
                    else { return true; }
                } else {
                    return true;
                }
            }
        },
        messages:
            {
                checkSecondID: "2nd Photo ID should match."
            }
    }).data("kendoValidator");

    if (tmpValidator.validate() && tmpPhotoValidator.validate()) {
        var tmpFormData = this.FormControls.Form().serializeObject();
        var tmpUrl = this.Urls.GoToAWBs;
        var tmpNextStepUrl = this.Urls.GetAWBView;
        var tmpNextDriversStep = this.Window();
        var tmpDriverData = {
            DriverId: tmpFormData.DriverId,
            TruckingCompId: tmpFormData.TruckingCompId,
            STANumber: tmpFormData.STANumber,
            DriverLicense: tmpFormData.DriverLicense,
            StateId: tmpFormData.StateId,
            FirstIDTypeId: tmpFormData.FirstIDTypeId,
            FirstIDMatched: tmpFormData.FirstIDMatched,
            SecondIDTypeId: tmpFormData.SecondIDTypeId,
            SecondIDMatched: tmpFormData.SecondIDMatched,
            CaptureID: $("#hdnAcaCaptureId").val(),
            CapturePhoto: $("#hdnAcaCapturePhoto").val(),
            ID: tmpFormData.ID,
        };
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            url: tmpUrl,
            data: JSON.stringify(tmpDriverData),
            success: function (data) {
                var tmpUrl = tmpNextStepUrl;
                tmpNextDriversStep.setOptions({ url: tmpUrl });
                tmpNextDriversStep.refresh(tmpUrl);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    }

}

ACADriverController.prototype.showDriverIdCapture = function () {
    this.IDCapturer = new PhotoCapturer("Take Driver ID", 'doc');
    this.IDCapturer.init();
    this.IDCapturer.open(null, this.FormControls.Image_CaptureId(), this.FormControls.ImageValue_CaptureId());
};

ACADriverController.prototype.showDriverPhotoCapture = function () {
    this.PhotoCapturere = new PhotoCapturer("Take Driver Photo", 'img');
    this.PhotoCapturere.init();

    this.PhotoCapturere.open({ width: 503, height: 254 }, this.FormControls.Image_CapturePhoto(), this.FormControls.ImageValue_CapturePhoto());
};

ACADriverController.prototype.submitDriver = function () {
    var tmpForm = this.FormControls.FormWndDriver();
    var validator = tmpForm.kendoValidator().data("kendoValidator");
    var self = this;
    var tmpNextAWBStep = this.Window();
    if (validator.validate()) {
        var formElementsJson = this.FormControls.FormWndDriver().serializeObject();
        var tmpUrl = this.Urls.SaveACADriver;
        var tmpNextStepUrl = this.Urls.GetAWBView;
        var tmpDriversList = this.FormControls.Drivers();
        var tmpDriverWindow = this.Window_AddDriver();
        var driverData = {
            Id: 0,
            FullName: formElementsJson.FullName,
            STANumber: formElementsJson.STANumber,
            DriverLicense: formElementsJson.DriverLicense,
            StateId: formElementsJson.StateId
        };
        $.ajax({
            type: 'POST',
            url: tmpUrl,
            data: JSON.stringify(driverData),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Id) {
                    tmpDriversList.dataSource.read().then(function () {
                        tmpDriversList.value(data.Id);
                    });
                    tmpDriverWindow.close();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    }
}

ACADriverController.prototype.submitTruckingComp = function () {
    var tmpForm = this.FormControls.FormWndTruckCom();
    var validator = tmpForm.kendoValidator().data("kendoValidator");

    if (validator.validate()) {
        var formElementsJson = this.FormControls.FormWndTruckCom().serializeObject();
        var tmpUrl = this.Urls.SaveTruckingCompany;
        var tmpTruckingCompWindow = this.Window_AddTruckingCmp();
        var tmpTruckCompList = this.FormControls.TruckingCompanies();
        var tmpTruckingData = {
            TruckingCompanyName: formElementsJson.TruckingCompanyName,
        };
        $.ajax({
            type: 'POST',
            url: tmpUrl,
            data: JSON.stringify(tmpTruckingData),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Id) {
                    tmpTruckCompList.dataSource.read().then(function () {
                        tmpTruckCompList.value(data.Id);
                    });
                    tmpTruckingCompWindow.close();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    }
}

ACADriverController.prototype.onDriversChange = function () {


    var tmpSelectedDriverId = this.FormControls.Drivers().value();
    if (tmpSelectedDriverId && tmpSelectedDriverId != "" && tmpSelectedDriverId != null) {
        var getDriverDetailsUrl = this.Urls.GetDriverDetails + "/?id=" + tmpSelectedDriverId;

        $.getJSON(getDriverDetailsUrl, function (data) {
            if (data) {
                this.FormControls.DriverLicense().val(data.DriverLicense);
                this.FormControls.STANumber().val(data.STANumber);
                this.FormControls.States().value(data.StateId);
            }
        }.bind(this));
    }
}

ACADriverController.prototype.onTruckingCompChange = function () {

}

ACADriverController.prototype.onFirstIDCheckChange = function () {
    if (!this.FormControls.IsFirstIDMatched()) {
        this.FormSections.SecondIDTypes().show();
    } else {
        this.FormSections.SecondIDTypes().hide();
    }
}

ACADriverController.prototype.cancel = function () {

    var tmpCancelUrl = this.Urls.Cancel;
    var tmpWindow = this.Window();
    window.DIALOG_HANDLER.openConfirmation(function () {
        $.ajax({
            url: tmpCancelUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                console.log(tmpWindow);
                tmpWindow.close();
            }
        })
    }, null, "Cancel Wizard", "Are you sure you want to cancel wizard ? You'll be have to start from very beginning.");

}








