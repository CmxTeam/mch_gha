﻿function AWBHandlingsController(settings) {
    this.inited = false;
    this.Uploader = null;

    this.Urls = {
        BackToDetails: settings.Urls.BackToDetails,
        NextToContacts: settings.Urls.NextToContacts,
        SaveAwbHandling: settings.Urls.SaveAwbHandling,
        Cancel : settings.Urls.Cancel
    }

    this.Controls = {
        AwbId: function () { return $("#hdnAcaAwbHandlingId"); },

        Documents: function () { return $("#gridAcaAwbHandlingDocuments").data("kendoGrid") },
        Button_AddDoc: function () { return $("#btnAcaAwbHandlingAddDoc"); },

        DocumentTypes: function () { return $("#ddlAcaAwbHandlingDocTypes").data("kendoDropDownList"); },

        HandlingList: function () { return $("#lsvAcaSHList"); },

        Button_Next: function () { return $("#btnAWBHandlingNext"); },
        Button_Back: function () { return $("#btnAWBHandlingPrevious"); },
    }

    this.WizardContainer = function () { return $("#dvAcaAwbDetailsContainer"); }

    this.Window = function () { return $("#wndTMAddTaskView").data("kendoWindow"); }
}

AWBHandlingsController.prototype.onUploaderClose = function () {
    this.Uploader.clear();
}

AWBHandlingsController.prototype.setUploader = function (uploader) {
    this.Uploader = uploader;

    //setup event handlers
    this.Uploader.setEventHandlers({
        onAttachmentAdd: function () {

        }.bind(this)
    })
}

AWBHandlingsController.prototype.getSelectedGroups = function () {
    var tmpCheckedItems = this.Controls.HandlingList().find(">li>div>label>input.k-checkbox");
    var selectedGroupIds = new Array();
    for (var i = 0; i < tmpCheckedItems.length; i++) {
        var tmpInput = tmpCheckedItems[i];
        if ($(tmpInput).is(":checked")) {
            var groupId = $(tmpInput).attr("data-group-id");
            var subGroupId = "0";

            if ($(tmpInput).siblings('ul').length > 0) {
                var subCheckedItem = $(tmpInput).siblings('ul').find('li >div>label> input:checked');
                if (subCheckedItem.length > 0) {
                    subGroupId = $(subCheckedItem[0]).attr("data-subgroup-id");
                }
            }
            selectedGroupIds.push(groupId + "|" + subGroupId);
        }
    }
    return selectedGroupIds;
}

AWBHandlingsController.prototype.getSelectedCodes = function() {
    var tmpCheckedItems = this.Controls.HandlingList().find("input.spcode:checked");
    var selectedCodeIds = new Array();
    
    for (var i = 0; i < tmpCheckedItems.length; i++) {
        
        var tmpInput = tmpCheckedItems[i];
        if ($(tmpInput).attr("data-item-code") != "") {
            selectedCodeIds.push($(tmpInput).attr("data-item-code"));
        }
    }
    return selectedCodeIds;
}

AWBHandlingsController.prototype.updateDocumentTypes = function () {
    return this.Controls.DocumentTypes().dataSource.read();
}

AWBHandlingsController.prototype.init = function () {
    if (this.inited == false) {
        var self = this;
        this.Controls.HandlingList().find(">li>div>label>input.k-checkbox").change(function () {
            var subGroup = $(this).parent().parent().parent().find(">ul");
            if (this.checked) {
                subGroup.show();
            } else {
                subGroup.hide();
            }
        });

        this.Controls.Button_AddDoc().click(this.showUploader.bind(this));

        this.Controls.Button_Next().click(this.saveAndNext.bind(this));

        this.Controls.Button_Back().click(this.goBackToDetails.bind(this));

        this.inited = true;
    }
}

AWBHandlingsController.prototype.showUploader = function () {
   
    this.Uploader.init();
    var self = this;
    this.updateDocumentTypes().then(function () {
        self.Uploader.setDocumentTypes(self.Controls.DocumentTypes().dataSource.data());
        self.Uploader.AddDocument();
    });
}

AWBHandlingsController.prototype.goBackToDetails = function () {
    var tmpUrl = this.Urls.BackToDetails;
    tmpUrl = tmpUrl + "/?awbId=" + this.Controls.AwbId().val();
    this.WizardContainer().load(tmpUrl);
}

AWBHandlingsController.prototype.saveAndNext = function () {

    var tmpAttachments = this.Controls.Documents().dataSource.data();
    var tmpFormData = new FormData();

    var formElementsJson = {};
    formElementsJson.AwbId = this.Controls.AwbId().val();
    formElementsJson.SphCodeIds = this.getSelectedCodes().join(',');
    formElementsJson.SphGroupIds = this.getSelectedGroups().join(',');
    formElementsJson.Documents = [];
    if (tmpAttachments && tmpAttachments.length > 0) {
        for (var i = 0; i < tmpAttachments.length; i++) {
            var documentItem = tmpAttachments[i];
            formElementsJson.Documents.push({
                Id: documentItem.Id,
                Name: documentItem.Name,
                ShipperDocTypeId: documentItem.ShipperDocTypeId,
                DocumentType: documentItem.DocumentType,
                IsSnapshot: documentItem.IsSnapshot,
                SnapshotData: documentItem.SnapshotData,
                Path: documentItem.Path
            });

            if (documentItem.IsSnapshot == false) {
                tmpFormData.append("file", documentItem.File);
            }
        }
    }
    tmpFormData.append("formData", JSON.stringify(formElementsJson));
    var tmpUrl = this.Urls.SaveAwbHandling;
    var tmpNextUrl = this.Urls.NextToContacts;
    var tmpWizardContainer = this.WizardContainer();
    $.ajax({
        async: true,
        type: 'POST',
        url: tmpUrl,
        data: tmpFormData,
        success: function (data) {
            if (data.Id) {
                 tmpNextUrl =  tmpNextUrl+ "/?id=&awbId=" + data.Id
                 tmpWizardContainer.load(tmpNextUrl);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

AWBHandlingsController.prototype.cancel = function () {
    var tmpCancelUrl = this.Urls.Cancel;
    var tmpWidow = this.Window();
    window.DIALOG_HANDLER.openConfirmation(function () {
        $.ajax({
            url: tmpCancelUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                tmpWidow.close();
            }
        })
    }, null, "Cancel Wizard", "Are you sure you want to cancel wizard ? You'll be have to start from very beginning.");
}