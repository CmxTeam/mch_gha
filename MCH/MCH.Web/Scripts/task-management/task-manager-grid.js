﻿// references to : jQuery, jQuery templates, kendoUI

function TaskManagerGrid(name, argUrls) {

    this._tableName = name;
    this.intialized = false;
    this.CurrentViewerUrl = "";

    this.toolbarFilters = {
        HandlingStation: null,
        TaskFrom: null,
        TaskTo: null,
        Department: null,
        TaskType: null,
        TaskAction: null,
        TaskTypeToAdd: null,
        Reference: null
    };

    this.filterButton = null;
    this.resetButton = null;
    this.actionButton = null;
    this.addButton = null;
    this.kendoGrid = null;

    this.windows = {
        Alert: null,
        Confirm: null,
        Viewer: null,
        AddEdit: null,
        AddPopupWizard: null,
        AddHWBPopup: null,
        Action: null,
        EntityDetials: null
    }
    this.urls = {
        GetTaskAlerts: argUrls.GetTaskAlerts,
        GetEntityViewerByType: argUrls.GetEntityViewerByType,
        GetTaskUserAssignView: argUrls.GetTaskUserAssignView,
        GetTaskInstrAssignView: argUrls.GetTaskInstrAssignView,
        GetCargoAcceptanceView: argUrls.GetCargoAcceptanceView,
        GetCargoDischargeView : argUrls.GetCargoDischargeView,
        ReopenTasks: argUrls.ReopenTasks,
        CancelTasks: argUrls.CancelTasks,
        GetAWBWizarView: argUrls.GetAWBWizarView,
        GetHWBWizarView: argUrls.GetHWBWizarView,
        CancelAwbWizard: argUrls.CancelAwbWizard,
        CancelDisWizard : argUrls.CancelDisWizard,
        GetHWBExtraPopupView: argUrls.GetHWBExtraPopupView
    },

    //constants
    this.actionIds = {
        AssignToUser: "1",
        Reopen: "2",
        Cancel: "3",
        AddInstructions: "4"
    };
    this.EntityTypes = new Array();
    this.EntityTypes[1] = "Flight";
    this.EntityTypes[2] = "AWB";
    this.EntityTypes[3] = "HWB";
    this.EntityTypes[4] = "ULD";
    this.EntityTypes[6] = "Task";

    this.addTaskTypeIds = {
        CargoAccept: "13",
        CargoDischarge : "3",
        AWBDataEntry: "16",
        HWBDataEntry: "17"
    }
}

TaskManagerGrid.prototype.init = function (e) {

    if (this.intialized === false) {
        var self = this;
        this.toolbarFilters = {
            HandlingStation: function () { return $("#ddlHandlingStation").data("kendoDropDownList"); },
            TaskFrom: function () { return $("#dtpFrom").data("kendoDatePicker"); },
            TaskTo: function () { return $("#dtpTo").data("kendoDatePicker"); },
            Department: function () { return $("#ddlDepartments").data("kendoDropDownList"); },
            TaskType: function () { return $("#ddlFilterByTaskTypes").data("kendoDropDownList"); },
            TaskAction: function () { return $("#ddlTaskActions").data("kendoDropDownList"); },
            TaskTypeToAdd: function () { return $("#ddlAddForTaskTypes").data("kendoDropDownList"); },
            Reference: function () { return $("#txtReference"); }
        };

        this.windows = {
            Alert: function () { return $("#wndTMAlert").data("kendoWindow"); },
            Confirm: function () { return $("#wndTMActionConfirm").data("kendoWindow"); },
            Viewer: function () { return $("#wndTMTaskViewer").data("kendoWindow"); },
            AddEdit: function () { return $("#wndTMAddTaskView").data("kendoWindow"); },
            AddPopupWizard: function () { return $("#wndTMAddTaskPopupWizard").data("kendoWindow"); },
            AddHWBPopup: function () { return $("#wndTMAddHWBPopup").data("kendoWindow"); },
            EntityDetials: function () { return $("#wndTMShowTaskDetails").data("kendoWindow"); },
            Action: function () { return $("#wndTMActionPerform").data("kendoWindow"); }
        }

        this.kendoGrid = function () { return $("#" + this._tableName).data("kendoGrid"); };

        this.filterButton = $("#" + this._tableName).find("input.cm-grid-filter-btn").first();
        if (this.filterButton) {
            this.filterButton.on('click', this.filterTaskManagerGrid.bind(this));
        }

        this.resetButton = $("#" + this._tableName).find("input.cm-grid-reset-btn").first();
        if (this.resetButton) {
            this.resetButton.on('click', this.resetTaskManagerGrid.bind(this));
        }

        this.actionButton = $("#" + this._tableName).find("input.cm-grid-action-btn").first();
        if (this.actionButton) {
            this.actionButton.on('click', this.actionTaskManagerGrid.bind(this));
        }

        this.addButton = $("#" + this._tableName).find("input.cm-grid-add-btn").first();
        if (this.addButton) {
            this.addButton.on('click', this.addTaskManagerGrid.bind(this));
        }

        $("#" + this._tableName).on("dblclick", "tr.k-state-selected", this.rowDBClick.bind(this));

        $(document).on("AWBWizardFinalized", function () {
            self.filterTaskManagerGrid();
        })

        this.intialized = true;
    }


}

TaskManagerGrid.prototype.rowDBClick = function (e) {
    var tmpGrid = this.kendoGrid();
    var tmpDataItem = tmpGrid.dataItem(tmpGrid.select())
    this.showViewer(tmpDataItem.TaskId, tmpDataItem.EntityTypeId, tmpDataItem.EntityId);
}

TaskManagerGrid.prototype.alert = function (message) {

    var tmpAlertWindow = this.windows.Alert();
    tmpAlertWindow.title("Warning");
    tmpAlertWindow.setOptions({ width: 300 });
    tmpAlertWindow.content("<b>" + message + "</b>");
    tmpAlertWindow.center().open();
}

TaskManagerGrid.prototype.showViewer = function (taskId, entityTypeId, entityId) {
    var tmpViewerWindow = this.windows.Viewer();

    var tmpUrl = this.urls.GetEntityViewerByType + "/?taskId=" + taskId + "&entityTypeId=" + entityTypeId + "&entityId=" + entityId;

    this.CurrentViewerUrl = tmpUrl;

    if (this.EntityTypes[entityTypeId] != undefined) {
        tmpViewerWindow.title(this.EntityTypes[entityTypeId] + " Viewer");
        tmpViewerWindow.refresh(tmpUrl)

        tmpViewerWindow.setOptions({ width: "90%", maxWidth:"1170px", height: "42%", minHeight : window.innerHeight - 400 });

        tmpViewerWindow.center().open();
    } else {
        alert("Entity Type is undefined.");
    }
}



TaskManagerGrid.prototype.refreshCurrentViewer = function ()
{
    if (this.CurrentViewerUrl != "") {
        var tmpViewerWindow = this.windows.Viewer();
        tmpViewerWindow.refresh(this.CurrentViewerUrl)
    }
}

TaskManagerGrid.prototype.showTaskAlerts = function (taskId) {

    var tmpDetailsWindow = this.windows.EntityDetials();
    tmpDetailsWindow.title("Task Alerts");

    var tmpUrl = this.urls.GetTaskAlerts + "/?taskId=" + taskId;
    tmpDetailsWindow.setOptions({minWidth:400, minHeight:200})
    tmpDetailsWindow.refresh(tmpUrl);
    tmpDetailsWindow.center().open();
}

TaskManagerGrid.prototype.filterTaskManagerGrid = function (e) {
    var tmpGrid = this.kendoGrid();
    var tmpFilters = [];

    var tmpHandlingStation = this.toolbarFilters.HandlingStation().value();
    if (tmpHandlingStation && tmpHandlingStation != -1) {
        tmpFilters.push({ field: "WarehouseId", operator: "eq", value: tmpHandlingStation });
    }

    var tmpFromDate = this.toolbarFilters.TaskFrom().value();
    if (tmpFromDate && tmpFromDate != "") {
        tmpFilters.push({ field: "TaskDate", operator: "gte", value: tmpFromDate });
    }

    var tmpToDate = this.toolbarFilters.TaskTo().value();
    if (tmpToDate && tmpToDate != "") {
        tmpFilters.push({ field: "TaskDate", operator: "lte", value: tmpToDate });
    }
    var tmpDepartment = this.toolbarFilters.Department().value();
    if (tmpDepartment && tmpDepartment != -1) {
        tmpFilters.push({ field: "DepartmentId", operator: "eq", value: tmpDepartment });
    }

    var tmpTaskType = this.toolbarFilters.TaskType().value();
    if (tmpTaskType && tmpTaskType != -1) {
        tmpFilters.push({ field: "TaskTypeId", operator: "eq", value: tmpTaskType });
    }

    var tmpReference = this.toolbarFilters.Reference().val();
    if (tmpReference) {
        tmpFilters.push({ field: "Reference", operator: "contains", value: tmpReference });
    }

    tmpGrid.dataSource.filter(tmpFilters);

}

TaskManagerGrid.prototype.resetTaskManagerGrid = function (e) {
   
    this.toolbarFilters.HandlingStation().select(1);
    this.toolbarFilters.Department().select(0);
    this.toolbarFilters.TaskFrom().value('');
    this.toolbarFilters.TaskTo().value('');
    this.toolbarFilters.TaskType().dataSource.read();
    this.toolbarFilters.TaskType().select(0);

    var currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);

    this.toolbarFilters.TaskFrom().value(currentDate);
    this.toolbarFilters.TaskTo().value(currentDate);

    this.filterTaskManagerGrid(e);
}

TaskManagerGrid.prototype.ShowTaskActionView = function (argTaskIds, rootUrl) {
    var tmpActionWindow = this.windows.Action();

    tmpActionWindow.title("Assign Tasks to Users");
   tmpActionWindow.setOptions({ minWidth: 400});

    var tmpUrl = rootUrl + "/?";
    for (var i = 0; i < argTaskIds.length; i++) {
        tmpUrl = tmpUrl + "taskIds=" + argTaskIds[i];
        if (i != argTaskIds.length - 1) {
            tmpUrl = tmpUrl + "&";
        }
    }
    tmpActionWindow.refresh(tmpUrl);
    tmpActionWindow.center().open();
}

TaskManagerGrid.prototype.ShowAssignTasksView = function (argTaskIds) {
    this.ShowTaskActionView(argTaskIds, this.urls.GetTaskUserAssignView);
}

TaskManagerGrid.prototype.ShowAssignInstrView = function (argTaskIds) {
    this.ShowTaskActionView(argTaskIds, this.urls.GetTaskInstrAssignView);
}

TaskManagerGrid.prototype.onTaskActionPerformed = function (e) {
    this.kendoGrid().dataSource.read();
    this.windows.Action().close();
}

TaskManagerGrid.prototype.onUserAssigned = function (e) {
    this.onTaskActionPerformed(e);
}

TaskManagerGrid.prototype.onInstrAssigned = function (e) {
    this.onTaskActionPerformed(e);
}

TaskManagerGrid.prototype.ShowActionResultsView = function (summary,actionResult, grid, window) {
    var tmpResultMessages = [];
    var tmpWindow = window;
    tmpWindow.title(summary);
    if (actionResult) {
        for (var i = 0; i < actionResult.length; i++) {
            if (actionResult[i].Status == 0) {
                tmpResultMessages.push(actionResult[i].Message);
            }
        }

        if (tmpResultMessages.length > 0) {
            var messages = tmpResultMessages.length > 1 ? ["One or more tasks could not be updated."]: tmpResultMessages;
            var windowContent = $("#modal-confirm-task-action-result").tmpl({ Messages: messages });
            tmpWindow.content(windowContent).center().open();
        } else {
            grid.dataSource.read();
        }
    }
}

TaskManagerGrid.prototype.ShowTaskConfirmView = function (argTaskIds, viewSettings) {
    var tmpWindow = this.windows.Confirm();
    var tmpGrid = this.kendoGrid();
    var tmpViewUrl = viewSettings.Url;
    tmpWindow.title(viewSettings.Title);
    var tmpActionResultsAnalizer = this.ShowActionResultsView;

    var windowContent = $("#modal-confirm-task-action").tmpl({ message: viewSettings.Message, confirmButtonId: viewSettings.ConfirmButtonId });

    tmpWindow.content(windowContent);

    $("#" + viewSettings.ConfirmButtonId).click(function () {

        $.ajax({
            url: tmpViewUrl,
            type: "POST",
            data: { taskIds: argTaskIds.join() },
            dataType: "json",
            traditional: true,
            success: function (result) {
                tmpWindow.close();

                if (result.ActionResults) {
                    tmpActionResultsAnalizer(viewSettings.Title + " Results", result.ActionResults, tmpGrid, tmpWindow);
                } else {
                    alert(result.ErrorMessage);
                }
            }
        }).fail(function () {
            alert("Something went wrong, please try again later.");
        });
    });

    tmpWindow.setOptions({ width: 250, height: 200 });
    tmpWindow.center().open();
}

TaskManagerGrid.prototype.ShowReopenTasksView = function (argTaskIds) {
    
    this.ShowTaskConfirmView(argTaskIds,
        {
            Title: "Reopen Task(s)",
            Message: "Are you sure you want to reopen selected task(s) ?",
            ConfirmButtonId: "btnReopenTasksConfirm",
            Url: this.urls.ReopenTasks
        });
}

TaskManagerGrid.prototype.ShowCancelTasksView = function (argTaskIds) {
    this.ShowTaskConfirmView(argTaskIds,
       {
           Title: "Cancel Task(s)",
           Message: "Are you sure you want to cancel selected task(s) ?",
           ConfirmButtonId: "btnCancelTasksConfirm",
           Url: this.urls.CancelTasks
       });
}

TaskManagerGrid.prototype.actionTaskManagerGrid = function (e) {
    var tmpGrid = this.kendoGrid();
    var tmpSelectedRows = tmpGrid.select();
    if (tmpSelectedRows.length > 0) {
        var tmpTaskIds = [];

        for (var i = 0; i < tmpSelectedRows.length; i++) {
            var gridDataItem = tmpGrid.dataItem(tmpSelectedRows[i]);
            tmpTaskIds.push(gridDataItem.TaskId);
        }


        var actionValue = this.toolbarFilters.TaskAction().value();
        switch (actionValue) {
            case this.actionIds.AssignToUser: {
                this.ShowAssignTasksView(tmpTaskIds);
                break;
            }
            case this.actionIds.AddInstructions: {
                this.ShowAssignInstrView(tmpTaskIds);
                break;
            }
            case this.actionIds.Cancel: {
                this.ShowCancelTasksView(tmpTaskIds);
                break;
            }
            case this.actionIds.Reopen: {
                this.ShowReopenTasksView(tmpTaskIds);
                break;
            }
            default: {
                alert("Unknown action !");
                break;
            }
        }
    } else {
        alert("No task(s) selected.");
    }

}

TaskManagerGrid.prototype.ShowAddDischargeView = function () {
    var tmpWindow = this.windows.AddEdit();
    var tmpCancelUrl = this.urls.CancelDisWizard;
    tmpWindow.title("Cargo Discharge");
    tmpWindow.setOptions({ width: 1200, height: window.innerHeight - 150, minHeight: 580, maxHeight: (window.innerHeight), url: this.urls.GetCargoDischargeView });
    tmpWindow.refresh(this.urls.GetCargoDischargeView);
    tmpWindow.center().open();
    tmpWindow.setOptions({
        close: function () {
            window.DIALOG_HANDLER.openConfirmation(function () {
                $.ajax({
                    url: tmpCancelUrl,
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data) {
                    }
                })
            }, null, "Cancel Wizard", "Do you want to cancel this wizard ? You'll be have to start from very beginning.");
        }
    });
}

TaskManagerGrid.prototype.ShowAddAcceptView = function () {
    var tmpWindow = this.windows.AddEdit();
    var tmpCancelUrl = this.urls.CancelAwbWizard;
    tmpWindow.title("Cargo Acceptance");
    tmpWindow.setOptions({ width: 1200, height: 580, minHeight: 580, maxHeight: (window.innerHeight), url: this.urls.GetCargoAcceptanceView });
    tmpWindow.refresh(this.urls.GetCargoAcceptanceView);
    tmpWindow.center().open();
    tmpWindow.setOptions({
        close: function () {

            try {
                ClearTablet();
                SetTabletState(0, window.TMR);
            } catch (ex) {
                console.log(ex);
            }

            window.DIALOG_HANDLER.openConfirmation(function () {
                $.ajax({
                    url: tmpCancelUrl,
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data) {
                    }
                })
            }, null, "Cancel Wizard", "Do you want to cancel this wizard ? You'll be have to start from very beginning.");
        }
    });
}


TaskManagerGrid.prototype.ShowAddAWBDataEntryView = function (id, step) {
    var tmpUrl = this.urls.GetAWBWizarView;
    if (id !== undefined)
        tmpUrl = tmpUrl + "/?id=" + id;
    if (step !== undefined)
        tmpUrl = tmpUrl +"&step=" + step;

    var tmpWindow = this.windows.AddPopupWizard();
    tmpWindow.content("");
    tmpWindow.title("AWB Data Entry");
    tmpWindow.setOptions({ width: '90%', height: '50%', maxWidth: "1100px", maxHeight: "730px", url: tmpUrl });
    tmpWindow.refresh(tmpUrl);
    tmpWindow.center().open();
    return tmpWindow;
}

TaskManagerGrid.prototype.ShowAddHWBDataEntryView = function (id, awbid) {
    var tmpUrl = this.urls.GetHWBWizarView;
    if (id !== undefined) {
        tmpUrl = tmpUrl + "/?id=" + id+"&awbid=" + awbid;
    }
    else {
        tmpUrl = tmpUrl + "/?awbid=" + awbid;
    }

    var tmpWindow = this.windows.AddHWBPopup();
    tmpWindow.content("");
    tmpWindow.title("HWB Data Entry");
    tmpWindow.setOptions({ width: '90%', height: '50%', maxWidth: "1000px", maxHeight: "680px", url: tmpUrl });
    tmpWindow.refresh(tmpUrl);
    tmpWindow.center().open();
}

TaskManagerGrid.prototype.ShowHWBExtraPopup = function (id) {
    var tmpUrl = this.urls.GetHWBExtraPopupView + "/?id=" + id;

    var tmpWindow = this.windows.AddHWBPopup();
    tmpWindow.content("");
    tmpWindow.title("HWB Data Entry");
    tmpWindow.setOptions({ width: '90%', height: '50%', maxWidth: "1000px", maxHeight: "680px", url: tmpUrl });
    tmpWindow.refresh(tmpUrl);
    tmpWindow.center().open();
}


TaskManagerGrid.prototype.addTaskManagerGrid = function (e) {
    var taskTypeToAddValue = this.toolbarFilters.TaskTypeToAdd().value();
    switch (taskTypeToAddValue) {
        case this.addTaskTypeIds.CargoAccept: {
            this.ShowAddAcceptView();
            break;
        }
        case this.addTaskTypeIds.CargoDischarge: {
            this.ShowAddDischargeView();
            break;
        }
        case this.addTaskTypeIds.AWBDataEntry: {
            
            this.ShowAddAWBDataEntryView(null,1);
            break;
           // window.NAVIGATION_HANDLER.itemNavigate(this.urls.GetAWBWizarView, function () { });
            //TODO : Lara open AWB wizard
            break;
        }
        case this.addTaskTypeIds.HWBDataEntry: {
           // this.ShowHWBExtraPopup(66); 
            break;
            //TODO : Lara open HWB wizard
            break;
        }
        default: {
            alert("Feature is coming soon !");
            break;
        }
    }
}

TaskManagerGrid.prototype.loadRemoteWindow = function(url, wnd, data, method){
    $.ajax({
        url: url,
        type: method,
        data: data,
        dataType: "html",
        traditional: true,
        success: function (html) {
            var cont = $("<div class='container-fluid'></div>")
            wnd.content(cont);
            cont.ready(function () {
                cont.html(html);
                wnd.center().open();
            });
        }
    }).fail(function () {
        me.common.popup("Error", "Something went wrong, please try again later.");
    });
}


