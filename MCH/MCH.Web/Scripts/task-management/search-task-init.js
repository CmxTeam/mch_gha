﻿if (typeof $.fn.bdatepicker == 'undefined')
    $.fn.bdatepicker = $.fn.datepicker.noConflict();
$(function () {

    /* DatePicker */
    // default
    $("#searchTaskFrom").bdatepicker({
        format: "mm/dd/yyyy hh:mm",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });

    $("#searchTaskTo").bdatepicker({
        format: "mm/dd/yyyy hh:mm",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });

    // if select2 plugin is included
    if (typeof $.fn.select2 != 'undefined')
    {
        
    }


    //init tasks data table

    $('#tblSearchGeneralTask').DataTable({
        responsive: true
    });

});