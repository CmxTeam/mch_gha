﻿window.ATTRIBUTE_TYPES =
    {
        HAZMAT: 'HAZMAT',
        PERISHABLES: 'PERISHABLES',
        DRYICE: 'DRY ICE',
        ENGINE: 'ENGINE',
        OVERSIZED: 'OVERSIZED',
        EXPEDITED: 'EXPEDITED',
        VEHICLE: 'VEHICLE',
        HIGHVALUE: 'HIGH VALUE',
        LIVEANIMALS: 'LIVE ANIMALS',
        HUMANREMAINS: 'HUMAN REMAINS',
        LICENSE: 'LICENSE',
        OTHERHANDLING: 'OTHER HANDLING'
    }

window.isDataTable = function( nTable )
{
    var settings = $.fn.dataTableSettings;
    for ( var i=0, iLen=settings.length ; i<iLen ; i++ )
    {
        if ( settings[i].nTable == nTable )
        {
            return true;
        }
    }
    return false;
}

window.gridCellDateTimeFormat = function (dateTime) {
    return "<br/>" + kendo.toString(dateTime, 'MM-dd-yyyy : hh:mm');
}