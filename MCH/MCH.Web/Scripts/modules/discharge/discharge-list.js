/// <reference path="cargo-pickup.js" />
/// <reference path="document-pickup.js" />
/// <reference path="../../dhtmlx.js" />


var appDischarge = {
    rootURL: '',
    wndDischarge: null,
    wndDocument: null,
    tbDischargeTrans: null,
    FilterForm: null,
    gridTransactions: null,
    gridTasks: null,


    setCalendarDate: function (name, form, date) {
        var calendar = form.getCalendar(name);
        var calendarToKey = Object.keys(calendar.i)[0];
        calendar.i[calendarToKey].input.value = window.dhx4.date2str(date, '%m-%d-%Y');
        calendar.setDate(date);
    }
}

function init() {

    var self = this;

    var tmpIconsPath = '././Content/imgs/';
    dhtmlx.image_path = tmpIconsPath;

    var windows = new dhtmlXWindows();

    appDischarge.wndDischarge = initDishchargeWindow(windows);
    appDischarge.wndDischarge.hide();

    appDischarge.wndDocument = initDocumentWindow(windows);
    appDischarge.wndDocument.hide();

    var main_layout = new dhtmlXLayoutObject(document.body, '2U', 'dhx_web');

    var a = main_layout.cells('a');
    a.setWidth('250');
    var accordion_1 = a.attachAccordion();
    var panel_discharge_tasks = accordion_1.addItem('panel_discharge_tasks', 'DISCHARGE FILTERS:');
    var str = [
		{ type: "input", name: "fltAwb", label: "AWB#:", maxLength:10 },
		{ type: "input", name: "fltHwb", label: "HWB#:" },
        { type: "combo", name: "period", label: "Period:", inputWidth: 120 },
		{ type: "calendar", name: "fltFromDate", label: "From:", dateFormat: "%m-%d-%Y" },
		{ type: "calendar", name: "fltToDate", label: "To:", dateFormat: "%m-%d-%Y" },
		{ type: "input", name: "fltCompany", label: "Company:" },
		{ type: "settings", labelWidth: 70, labelAlign: "right", offsetLeft: "6" },
		{ type: "input", name: "fltDriver", label: "Driver:" },
		{ type: "combo", name: "fltStatus", label: "Status:", inputWidth: 120 },
		{
		    type: "block", name: "form_block_1", list: [
            { type: "button", name: "btnResetDischargeFilter", value: "RESET" },
            { type: "newcolumn" },
            { type: "button", name: "btnSearchDischarge", value: "SEARCH" }
		    ]
		}
    ];
    appDischarge.FilterForm = panel_discharge_tasks.attachForm(str);

    appDischarge.FilterForm.attachEvent("onButtonClick", function (name) {
        switch (name) {
            case 'btnResetDischargeFilter': {
                self.resetFilter();
                break;
            }
            case 'btnSearchDischarge': {
                self.searchDischarges();
                break;
            }

        }
    });

    appDischarge.setCalendarDate('fltFromDate', appDischarge.FilterForm, new Date());
    appDischarge.setCalendarDate('fltToDate', appDischarge.FilterForm, new Date());

    var period = appDischarge.FilterForm.getCombo("period");
    period.load(appDischarge.rootURL + "CargoDischarge/GetPeriods", function () {
        period.selectOption(0);
    })

    period.attachEvent("onChange", function (value) {
        var dateNow = new Date();
        var newDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - (value - 1));

        appDischarge.setCalendarDate('fltFromDate', appDischarge.FilterForm, newDate);
    }.bind(this));

    var panel_dashboard = accordion_1.addItem('panel_dashboard', 'DASHBOARD');

    var b = main_layout.cells('b');


    //right side

    var layout_1 = b.attachLayout('2E');

    var cell_1 = layout_1.cells('a');
    cell_1.setText('DISCHARGE TRANSACTIONS:');

    appDischarge.gridTransactions = cell_1.attachGrid();
    appDischarge.gridTransactions.setIconsPath(tmpIconsPath);

    appDischarge.gridTransactions.setHeader(["Part", "Type", "Origin", "Shipping Reference#", "Dest", "Date", "Driver", "Company", "SLAC", "User", "Released by", "Customs Status", "Status"]);
    appDischarge.gridTransactions.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");

    appDischarge.gridTransactions.setColSorting('str,str,str,str,str,str,str,str,str,str,str,str,str');
    appDischarge.gridTransactions.setInitWidths('*,*,70,200,70,*,*,*,*,*,*,*,*');
    appDischarge.gridTransactions.init();

    //appDischarge.gridTransactions.load('./data/discharge_transactions.xml', 'xml');

    appDischarge.tbDischargeTrans = cell_1.attachToolbar();

    appDischarge.tbDischargeTrans.loadStruct('<toolbar>' +
        '<item type="text" id="button_text_quick_finder" text="QUICK FINDER:" />' +
        '<item type="buttonInput" id="input_quick_finder" width="220" value="" />' +
        '<item type="separator" id="button_separator_20" />' +
        '<item type="buttonSelect" id="button_select_view" text="Views" >' +
            '<item type="button" id="button_select_option_list" text="Discharge List" image="" />' +
            '<item type="button" id="button_select_option_tasks" text="Discharge Tasks" image="" />' +
        '</item>' +
        '<item type="separator" id="button_separator_21" />' +
        '<item type="buttonSelect" id="button_select_action" text="Actions" >' +
        '<item type="button" id="opBtnDocumentPickup" text="Document Pickup" image="" />' +
            '<item type="button" id="opBtnCargoPickup" text="Cargo Pickup" image="" />' +
            '</item>  <item type="separator" id="button_separator_22" />' +
        '<item type="buttonSelect" id="button_select_export" text="Export" title="" >' +
        '<item type="button" id="button_select_option_26" text="Excel" />' +
        '<item type="button" id="button_select_option_27" text="PDF" image="" />' +
        '</item>  <item type="separator" id="button_separator_23" /></toolbar>');

    appDischarge.tbDischargeTrans.attachEvent("onClick", function (btnId) {
        if (btnId == 'opBtnDocumentPickup') {
            appDischarge.wndDocument.show();
            appDischarge.wndDocument.maximize();
        } else if (btnId == 'opBtnCargoPickup') {
            appDischarge.wndDischarge.show();
            appDischarge.wndDischarge.maximize();
        }
    });

    var cell_2 = layout_1.cells('b');
    cell_2.setText('DISCHARGE DETAILS:');
    var str = [
		{ type: "input", name: "form_input_8", label: "Origin:" },
		{ type: "input", name: "form_input_5", label: "Shipping Reference#:", inputWidth: 200 },
		{ type: "input", name: "form_input_9", label: "Dest:" },
		{ type: "settings", labelWidth: 120, labelAlign: "right", offsetLeft: "3" },
		{ type: "calendar", name: "form_calendar_3", label: "Discharge Date:", dateFormat: "%m-%d-%Y" },
		{ type: "input", name: "form_input_12", label: "SLAC:" },
		{ type: "input", name: "form_input_13", label: "Released by:" },
		{ type: "input", name: "form_input_15", label: "Customs Status:" },
		{ type: "input", name: "form_input_14", label: "Operation Status:", inputWidth: 250 },
		{ type: "newcolumn" },
		{
		    type: "fieldset", name: "form_fieldset_1", label: "Driver Photo", offsetLeft: "80", list: [
            { type: "label", name: "form_label_1", label: "here goes photo", labelWidth: 120, labelHeight: 140 },
            {
                type: "block", name: "form_block_2", list: [
                { type: "label", name: "form_label_3", label: "Driver Name:" },
                { type: "label", name: "form_label_4", label: "State:" },
                { type: "newcolumn" },
                { type: "label", name: "form_label_7", label: "John Baitz" },
                { type: "label", name: "form_label_8", label: "New York" }
                ]
            }
		    ]
		},
		{ type: "newcolumn" },
		{
		    type: "fieldset", name: "form_fieldset_2", label: "Photo ID:", offsetLeft: "20", list: [
            { type: "label", name: "form_label_2", label: "here goes photo id", labelWidth: 250, labelHeight: 140, position: "label-left" }
		    ]
		}
    ];

    appDischarge.DischargeDetails = cell_2.attachForm(str);

    var statusBar = main_layout.attachStatusBar();
    statusBar.setText('ALL RIGHTS RESERVED TO CARGOMATRIX INC. 1999-2015 - MCH V1.0');

    this.resetFilter = function () {
        var awb = appDischarge.FilterForm.getItemValue('fltAwb');
        var hwb = appDischarge.FilterForm.getItemValue('fltHwb');
        var fromDate = appDischarge.FilterForm.getCalendar('fltFromDate').getFormatedDate();
        var toDate = appDischarge.FilterForm.getCalendar('fltToDate').getFormatedDate();
        var company = appDischarge.FilterForm.getItemValue('fltCompany');
        var driver = appDischarge.FilterForm.getItemValue('fltDriver');
        var status = appDischarge.FilterForm.getCombo('fltStatus').getSelectedValue();
    }

    this.searchDischarges = function () {

        var awb = appDischarge.FilterForm.getItemValue('fltAwb');
        var hwb = appDischarge.FilterForm.getItemValue('fltHwb');
        var fromDate = appDischarge.FilterForm.getCalendar('fltFromDate').getFormatedDate();
        var toDate = appDischarge.FilterForm.getCalendar('fltToDate').getFormatedDate();
        var company = appDischarge.FilterForm.getItemValue('fltCompany');
        var driver = appDischarge.FilterForm.getItemValue('fltDriver');
        var status = appDischarge.FilterForm.getCombo('fltStatus').getSelectedValue();

        appDischarge.gridTransactions.clearAll();
        appDischarge.gridTransactions.load(appDischarge.rootURL + "CargoDischarge/GetDischargeTransactions?awb="+awb+"&hwb="+hwb + "&from="+fromDate+"&to="+toDate+ "&company="+company + "&driver="+driver + "&statusId"+status, 'json');
    }


    /*#END_GENERATED_CODE#*/

}
