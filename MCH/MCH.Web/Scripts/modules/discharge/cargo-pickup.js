﻿/// <reference path="../../dhtmlx.js" />



function initDishchargeWindow(windows) {

    var tmpWindow = windows.createWindow('wndDischarge', 0, 0, 950, 680);
    
    tmpWindow.setDimension(window.innerWidth, window.innerHeight);
    tmpWindow.setPosition(0, 0);

    // init the layout here

    tmpWindow.setText('CARGO PICKUP');

    tmpWindow.attachEvent("onClose", function () {
        tmpWindow.hide();
    });

    tmpWindow.centerOnScreen();
    tmpWindow.button('minmax').show();
    tmpWindow.button('minmax').enable();
    
    return tmpWindow;
}