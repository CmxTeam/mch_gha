﻿function init() {
    /*#GENERATED_CODE#*/
    dhtmlx.image_path = './codebase/imgs/';

    var main_layout = new dhtmlXLayoutObject(document.body, '2U');

    var a = main_layout.cells('a');
    a.setWidth('225');
    var layout_1 = a.attachLayout('2E');

    var cell_1 = layout_1.cells('a');
    cell_1.setText('FILTERS');
    cell_1.setHeight('400');
    cell_1.setWidth('350');
    var str = [
		{ type: "settings", labelWidth: 120, offsetLeft: "10", position: "label-left" },
		{ type: "calendar", name: "form_calendar_1", label: "From Date:", dateFormat: "%m-%d-%Y" },
		{ type: "calendar", name: "form_calendar_2", label: "To Date:", dateFormat: "%m-%d-%Y" },
		{ type: "input", name: "form_input_1", label: "Shipping Reference#:", inputWidth: 200 },
		{ type: "combo", name: "form_combo_1", label: "Origin:", connector: "./data/mch_data/locations.json", inputWidth: 200 },
		{ type: "combo", name: "form_combo_2", label: "Destination:", connector: "./data/mch_data/locations.json", inputWidth: 200 },
		{ type: "combo", name: "form_combo_3", label: "Carrier:", connector: "./data/mch_data/carriers.json", inputWidth: 200 },
		{ type: "combo", name: "form_combo_4", label: "Photos Location:", connector: "./data/mch_data/locations.json", inputWidth: 200 },
		{ type: "multiselect", name: "form_multiselect_1", label: "Anotations:", inputWidth: 200, inputHeight: 100 },
		{
		    type: "block", name: "form_block_1", list: [
            { type: "button", name: "form_button_1", value: "RESET" },
            { type: "newcolumn" },
            { type: "button", name: "form_button_2", value: "REFRESH" }
		    ]
		},
		{ type: "newcolumn" }
    ];
    var form_1 = cell_1.attachForm(str);

    var cell_8 = layout_1.cells('b');
    cell_8.setText('SEARCH RESULTS');
    var data_view_4 = cell_8.attachDataView({
        type: {
            template: '#Package# : #Version#<br/>#Maintainer#',
            width: 310,
            template_edit: '<input class=\'dhx_item_editor\' bind=\'obj.Package\'>'
        }
    });

    data_view_4.load('./data/data_view.xml', 'xml');

    var b = main_layout.cells('b');
    b.setText('THUMBNAILS');
    var data_view_1 = b.attachDataView({
        type: {
            template: '#Package# : #Version#<br/>#Maintainer#',
            template_edit: '<input class=\'dhx_item_editor\' bind=\'obj.Package\'>'
        }
    });

    data_view_1.load('./data/data_view.xml', 'xml');



    var status_1 = main_layout.attachStatusBar();
    status_1.setText('All Rights Reserved to Cargomatrix Inc. 1999-2015');

    var windows = new dhtmlXWindows();

    var window_1 = windows.createWindow('window_1', 0, 0, 1200, 800);

    window_1.hide();
    window_1.centerOnScreen();
    window_1.button('minmax').show();
    window_1.button('minmax').enable();




    /*#END_GENERATED_CODE#*/

}

