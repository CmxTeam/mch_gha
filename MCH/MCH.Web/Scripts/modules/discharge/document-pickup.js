﻿

function initDocumentWindow(windows) {
    var tmpWindow = windows.createWindow('wndDocument', 0, 0, 950, 680);
    tmpWindow.setDimension(window.innerWidth, window.innerHeight);
    tmpWindow.setPosition(0, 0);
    

    //init layout

    tmpWindow.setText('DOCUMENT PICKUP');

    tmpWindow.attachEvent("onClose", function () {
        tmpWindow.hide();
    });

    tmpWindow.centerOnScreen();
    tmpWindow.button('minmax').show();
    tmpWindow.button('minmax').enable();

    return tmpWindow;

}

