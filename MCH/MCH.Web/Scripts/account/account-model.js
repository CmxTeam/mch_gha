﻿function AccountModel(baseUrl) {
    this.baseUrl = baseUrl;
}

// TODO: Harut fix this to pass just a base url and not entire logoff action
AccountModel.prototype.logoff = function () {
    window.location.href = this.baseUrl;
}

AccountModel.prototype.itemNavigate = function (url, callback) {
    $("#" + this.loadContainer).load(url, callback);
}