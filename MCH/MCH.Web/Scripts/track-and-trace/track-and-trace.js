﻿//track and trace.js

; (function (me) {

    me.el = {
        //elements,
        init: function (elements) {
            //
            $.each(elements, function (type, els) {
                if (els.length) {
                    $.each(els, function (i, el) {
                        me.el[el.id] = me.common.toElement[type]($('#' + el.id));
                    });
                }
            });
            $.each(me.el, function (key, val) {
                if (!val) return false;
                //me.common.popup('Error','');
            });
            return true;
        }
    };
    me.controls =
        {
            searchByFilter: function () {
                return $("#ddlTrackFilter").data("kendoDropDownList");
            },
            getCarrier: function () {
                return $("#txtFilterCarrier").val();
            },
            getAWB: function () {
                return $("#txtFilterAWB").val();
            },
            getHWB: function () {
                return $("#txtFilterHWB").val();
            },
            getAirlineCode: function () {
                return $("#txtFilterAirlineCode").val();
            },
            getFlightNumber: function () {
                return $("#txtFlightNumber").val();
            },
            getFlightDate: function ()
            {
                return $("#dprFilterFlightDate").data("kendoDatePicker").value();
            },
            getOriginCode: function ()
            {
                return $("#txtFilterOrigin").val();
            },
            getDestCode: function ()
            {
                return $("#txtFilterDestination").val();
            },
            getULD: function ()
            {
                return $("#txtFilterULD").val();
            }
        }

    me.filterDivs = function () {
        return [
            me.el.dvFilterAWB,
            me.el.dvFilterHWB,
            me.el.dvFilterFlight,
            me.el.dvFilterULD
        ];
    };

    me.filter = {
        operators: {
            '==': 'eq',
            '!=': 'neq',
            '>=': 'gte',
            '<=': 'lte',
            '>': 'gt',
            '<': 'lt',
            'contains': 'contains',
            'doesnotcontain': 'doesnotcontain',
            'startswith': 'startswith',
            'endswith': 'endswith',
        },
        init: function (fields, by) {
            me.filter.filters.length = 0;
            if (!fields[by]) return false;

            $.each(fields[by], function (k, filter) {
                var filterValue = $.trim(me.common.getVal[filter.el](me.el[k])),
                filterOperator = me.filter.operators[filter.op];
                if (filterValue) {
                    me.filter.filters.push({ field: filter.field, operator: filterOperator, value: filterValue });
                }
            });
        },
        reset: function (fields, by) {
            me.filter.filters.length = 0;
            if (!fields[by]) return false;

            $.each(fields[by], function (k, filter) {
                me.common.setVal[filter.el](me.el[k], null);
            });
        },
        filters: [],
        logic: 'or',
        fields: null,
        by: {
            'AWB': function () {
                var divs = me.filterDivs();
                if (!me.common.couldHideElements(divs)
                    || !me.el.dvFilterAWB) return false;
                me.el.dvFilterAWB.show();
            },
            'HWB': function () {
                var divs = me.filterDivs();
                if (!me.common.couldHideElements(divs)
                    || !me.el.dvFilterHWB) return false;
                me.el.dvFilterHWB.show();
            },
            'FLIGHT': function () {
                var divs = me.filterDivs();
                if (!me.common.couldHideElements(divs)
                    || !me.el.dvFilterFlight) return false;
                me.el.dvFilterFlight.show();
            },
            'ULD': function () {
                var divs = me.filterDivs();
                if (!me.common.couldHideElements(divs)
                    || !me.el.dvFilterULD) return false;
                me.el.dvFilterULD.show();
            }
        }

    };
    me.entityTypes = ['', 'Flight', 'AWB', 'HWB', 'ULD', ];
    me.methods = {
        resetColumnsBySearch: function (searchType) {
            me.el.gridTrackList.hideColumn("AWB");
            me.el.gridTrackList.hideColumn("Description");
            me.el.gridTrackList.hideColumn("HWB");
            me.el.gridTrackList.hideColumn("TotalULDs");
            me.el.gridTrackList.hideColumn("TotalAWBs");
            me.el.gridTrackList.hideColumn("ULD");
            me.el.gridTrackList.hideColumn("IsBUP");
            me.el.gridTrackList.hideColumn("IsTrasfer");

            switch (searchType) {
                case "1": {
                    me.el.gridTrackList.showColumn("AWB");
                    me.el.gridTrackList.showColumn("Description");
                    break;
                }
                case "2": {
                    me.el.gridTrackList.showColumn("AWB");
                    me.el.gridTrackList.showColumn("HWB");
                    me.el.gridTrackList.showColumn("Description");
                    break;
                }
                case "3": {
                    me.el.gridTrackList.showColumn("TotalULDs");
                    me.el.gridTrackList.showColumn("TotalAWBs");
                    break;
                }
                case "4": {
                    me.el.gridTrackList.showColumn("ULD");
                    me.el.gridTrackList.showColumn("IsBUP");
                    me.el.gridTrackList.showColumn("IsTrasfer");
                    break;
                }
                default:

            }
        }
    }
    me.events = {
        onFilterChange: function (e) {
            var sender = $(e.sender)[0],
                text = sender ? $.trim(sender.text()) : '';
            // 
            if (!me.filter.by[text.toUpperCase()]) return false;
            me.filter.by[text.toUpperCase()]();
            me.el.gridTrackList.dataSource.read();
            me.methods.resetColumnsBySearch(me.controls.searchByFilter().value());
            resizeGrid(true);
        },
        onTrackFilter: function (e) {
            var filterBy = $.trim(me.el.ddlTrackFilter.text());
            me.filter.init(me.filter.fields, filterBy);
            me.el.gridTrackList.dataSource.read();
            //me.el.gridTrackList.dataSource.filter({ logic: me.filter.logic, filters: me.filter.filters });
        },
        onTrackReset: function (e) {
            var filterBy = $.trim(me.el.ddlTrackFilter.text());
            me.filter.reset(me.filter.fields, filterBy);
            me.el.gridTrackList.dataSource.filter({});
        },
        onWndTMTrackViewerShow: function (e) {
            var item = me.el.gridTrackList.dataItem(me.el.gridTrackList.select());
            var wnd = me.el.wndTMTrackViewer;
            var tmpUrl = me.url.GetEntityViewerByType + "/?entityTypeId=" + item.EntityTypeId + "&entityId=" + item.EntityId;
            if (me.entityTypes[item.EntityTypeId]) {
                wnd.title(me.entityTypes[item.EntityTypeId] + " Viewer");
                wnd.refresh(tmpUrl);
                wnd.setOptions({ width: window.innerWidth - 200, minHeight: window.innerHeight - 400 });
                wnd.center().open();
            }
        },
        beforeGridRead: function () {
            return {
                SearchType: me.controls.searchByFilter().value(),
                Carrier: me.controls.getCarrier(),
                AWB: me.controls.getAWB(),
                HWB: me.controls.getHWB(),
                AirlineCode: me.controls.getAirlineCode(),
                FlightNumber: me.controls.getFlightNumber(),
                FlightDate : me.controls.getFlightDate(),
                OriginCode: me.controls.getOriginCode(),
                DestinationCode: me.controls.getDestCode(),
                ULD : me.controls.getULD()
            };
        }
    };

    me.init = function (options) {

        if (!options || !options.element || !options.filter
            || !me.el.init(options.element)
            ) {
            //me.common.popup('Error','Something went wrong, please try again later.');
            return false;
        }
        me.filter.fields = options.filter.fields;
        me.filter.logic = options.filter.logic;
        me.url = options.url;
        me.el.gridTrackList.element.on("dblclick", "tr.k-state-selected", me.events.onWndTMTrackViewerShow);
        me.el.ddlTrackFilter.bind("change", me.events.onFilterChange);
        me.el.btnTrackFilter.bind("click", me.events.onTrackFilter);
        me.el.btnTrackReset.bind("click", me.events.onTrackReset)
    };

    me.common = {
        couldHideElements: function (args) {
            if (!args || !args.length) return false;
            $.each(args, function (i, el) {
                $(el).hide();
            });
            return true;
        },
        eachKey: function (obj, handler) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    handler(key, obj[key]);
                }
            }
        },
        toElement: {
            'grid': function (el) {
                return el.data('kendoGrid');
            },
            'ddl': function (el) {
                return el.data('kendoDropDownList');
            },
            'cbx': function (el) {
                return el.data('kendoComboBox');
            },
            'msl': function (el) {
                return el.data('kendoMultiSelect');
            },
            'dpr': function (el) {
                return el.data('kendoDatePicker');
            },
            'wnd': function (el) {
                return el.data('kendoWindow');
            },
            'dv': function (el) {
                return el;
            },
            'txt': function (el) {
                return el;
            },
            'btn': function (el) {
                return el;
            },
        },
        getVal: {
            'grid': function (el) {
                return el.data();
            },
            'ddl': function (el) {
                return el.value();
            },
            'cbx': function (el) {
                return el.value();
            },
            'mls': function (el) {
                return el.value();
            },
            'dpr': function (el) {
                return el.element[0].value;//new Date(1980, 1, 1)
            },
            'txt': function (el) {
                return el.val();
            },
        },
        setVal: {
            'grid': function (el, data) {
                return el.data(data);
            },
            'ddl': function (el, val) {
                return el.value(val);
            },
            'cbx': function (el, val) {
                return el.value(val);
            },
            'mls': function (el, val) {
                return el.value(val);
            },
            'dpr': function (el, val) {
                return el.value(val);
            },
            'txt': function (el, val) {
                return el.val(val);
            },
        }


    };

})(window._track = window._track || {});