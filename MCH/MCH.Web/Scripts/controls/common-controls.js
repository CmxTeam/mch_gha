﻿/// <reference path="../kendo/2014.3.1119/jquery.min.js" />
$(document).ready(function () {

    //setup photo viewer
    // window used is in _CommonWindows.cshtml
    $(document.body).on('click', '.op-img-viewable', function (e) {
        var tmpKendoWindow = $("#wndImageViewer").data("kendoWindow");
        if (tmpKendoWindow) {
            var tmpImageData = $(e.target).attr('src');
            var windowContent = "<img src='" + tmpImageData + "' style='width:100%'/>";

            tmpKendoWindow.content(windowContent);
            
            tmpKendoWindow.center().open();
        } else {
            console.log("common-controls.js : Image viewer window is missing.")
        }
    })
});