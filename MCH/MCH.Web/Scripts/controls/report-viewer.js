﻿function ReportViewer(settings) {
    this.RootPath = settings.RootPath;
    this.WindowId = settings.WindowId;
    this.ContainerId = settings.ContainerId;

    this.Name = "";
    this.Params = {};
}

ReportViewer.prototype.setParams = function (params) {
    this.Params = params;
}

ReportViewer.prototype.setName = function (reportName) {
    this.Name = reportName;
    this.FullPath = this.RootPath + "/" + reportName;
}

ReportViewer.prototype.initThenOpen = function (settings) {
    
    this.setParams(settings.Params);
    this.setName(settings.Name);
    this.open();
}


//opens report viewer after it has been initialized
ReportViewer.prototype.open = function ()
{
    var self = this;
    var tmpReportViewerWindow = $("#" + self.WindowId).data("kendoWindow");
    $.ajax({
        type: "GET",
        url: self.RootPath,
        data: { Name: self.Name, Path: self.Name, ReportParams: self.Params },
        success: function (viewHTML) {
            $("#" + self.ContainerId).html(viewHTML);
            if (tmpReportViewerWindow) {
                tmpReportViewerWindow.setOptions({ minWidth: 800, minHeight: 400 });
                tmpReportViewerWindow.center().open();
            }
            $("#" + self.ContainerId + " iframe").load(function () {
                var tmpReportFrame = $("#" + self.ContainerId).find("iframe");
                tmpReportFrame.contents().find("#reportForm").css("margin-bottom", "0");
                tmpReportFrame.attr("height", "100%");
                tmpReportFrame.css("min-height", "400px");
                tmpReportFrame.trigger("resize");
            })
        },
        error: function (errorData) { console.log(errorData) }
    });
}

