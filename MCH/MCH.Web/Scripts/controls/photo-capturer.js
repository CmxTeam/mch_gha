﻿/// <reference path="../main/image-capture.js" />
// uses the view of _PhotoCapturer.cshtml

function PhotoCapturer(titile, type) {
    this.inited = false;
    this.title = titile;

    //'doc' or 'img'
    this.type = type;
    this.resultImage = null;
    this.resultSource = null;

    this.capturedImageData = null;

    this.Controls = {
        Window: function () { return $("#wndCapturePhoto").data("kendoWindow"); },
        Canvas: function () { return document.getElementById("cnvPhotoCapture");},
    };

    this.Buttons = {
        Accept: $("#btnAcceptPhotoCapturer"),
        Close : $("#btnClosePhotoCapturer")
    }

    this.AcceptCapture = function () {
        this.resetImageCapture();
        this.Controls.Window().close();

        var tmpData = this.getCapturedImageData();
        this.resultImage.attr("src", tmpData);
        this.resultSource.val(tmpData);
        this.resultSource.trigger("blur");

        var tmpCanvas = this.Controls.Canvas();
        var tmpContext = tmpCanvas.getContext("2d");

        tmpContext.clearRect(0, 0, tmpCanvas.width, tmpCanvas.height);

    }.bind(this);

    this.CloseCapture = function () {
        this.resetImageCapture();
        this.Controls.Window().close();
        var tmpCanvas = this.Controls.Canvas();
        var tmpContext = tmpCanvas.getContext("2d");
        
        tmpContext.clearRect(0, 0, tmpCanvas.width, tmpCanvas.height);
    }.bind(this);
}

PhotoCapturer.prototype.getCapturedImageData = function () {
    return this.Controls.Canvas().toDataURL();
}

PhotoCapturer.prototype.open = function (windowOptions, argResultImage, argResultSource) {
    var wnd = this.Controls.Window();
    if (!wnd) return false;
    if (windowOptions) {
        wnd.setOptions(windowOptions);
    }

    this.resultImage = argResultImage;
    this.resultSource = argResultSource;

    wnd.title(this.title);
    wnd.center().open();
    this.initImageCapture();
}

PhotoCapturer.prototype.initImageCapture = function () {
    window.MEDIA_HANDLER.start('vidPhotoCapture', 'cnvPhotoCapture', this.type);
    $("#btnCapturePhoto").click(function () {
        var canvas = document.getElementById("cnvPhotoCapture");
        var context = canvas.getContext("2d");
        var video = document.getElementById("vidPhotoCapture");
        var cropHeight = video.clientHeight;
        var cropWidth = video.clientWidth;
        canvas.width = cropWidth;
        canvas.height = cropHeight
        context.drawImage(video, 0, 0, cropWidth, cropHeight);
    });
}

PhotoCapturer.prototype.resetImageCapture = function () {
    
    window.MEDIA_HANDLER.stop();
}



PhotoCapturer.prototype.init = function () {
    if (this.inited == false) {

        this.Buttons.Accept.off("click");
        this.Buttons.Accept.on("click",this.AcceptCapture);

        this.Buttons.Close.off("click");
        this.Buttons.Close.click(this.CloseCapture);

        this.Controls.Window().setOptions({ close: this.resetImageCapture })

        this.inited = true;
    }
}