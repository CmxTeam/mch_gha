﻿function DocumentUploader(settings) {
    this._gridName = settings.GridName,
    this._windowName = settings.WindowName,
    this._form = settings.FormName,

    this.urls = settings.Urls;
    this._documentTypesData = {};

    this._eventHandlers = {
        onAttachemntAdd: function () { }
    }

    this._inited = false;

    this.getGrid = function () { return $("#" + this._gridName).data("kendoGrid"); }
    this.getWindow = function () { return $("#" + this._windowName).data("kendoWindow"); }
    this.getForm = function () { return $("#" + this._form); }

    this.getDocumentTypes = function () {
        var tmpDocTypes = null;

        tmpDocTypes = $("#" + this._form + "DocTypes").data("kendoDropDownList");

        return tmpDocTypes;
    }

    this.getDocumentName = function () {
        return $("#" + this._form + "DocName");
    }

    this.getUploadOptionsTab = function () {
        var tmpTab = null;
        tmpTab = $("#" + this._form + "UploaderTabs").data("kendoTabStrip");
        return tmpTab;
    }

    this.getFileUploader = function () {
        var tmpFileUploader = null;

        tmpFileUploader = $("#" + this._form + "UploadControl").data("kendoUpload");

        return tmpFileUploader;
    }

    this.getSnapshotVideo = function () {
        return document.getElementById(this._form + "-video");
    }.bind(this);

    this.getSnapshotCanvas = function () {
        return document.getElementById(this._form + "-canvas");
    }.bind(this);

    this.getSaveButton = function () {
        var tmpSaveButton = null;

        tmpSaveButton = $("#" + this._form + "-btn-save");

        return tmpSaveButton;
    }
}

DocumentUploader.prototype.init = function () {
    if (this._inited === false) {

        var tmpTab = this.getUploadOptionsTab();
        if (tmpTab) {
            tmpTab.setOptions({
                change: this.uploadOptionsChanged.bind(this)
            });
        }

        var tmpDocumentTypes = this.getDocumentTypes();
        if (tmpDocumentTypes) {
            tmpDocumentTypes.setOptions({
                change: this.documentTypeChanged.bind(this)
            })
        }

        var tmpWindow = this.getWindow();

        tmpWindow.setOptions({
            close: this.clear.bind(this)
        });

        var tmpGrid = this.getGrid();

        var tmpSaveButton = this.getSaveButton();
        tmpSaveButton.click(function () {
            this.saveAttachment();
        }.bind(this));

        this._inited = true;
    }
}

DocumentUploader.prototype.setEventHandlers = function (handers) {
    this._eventHandlers = handers;
}

DocumentUploader.prototype.documentTypeChanged = function () {
    var selectedDocument = this.getDocumentTypes().dataItem();
    if (selectedDocument != null) {
        this.getDocumentName().val(selectedDocument.Name);
    }
}

DocumentUploader.prototype.saveAttachment = function () {

    //validation form
    var tmpTab = this.getUploadOptionsTab();
    var tabIndex = tmpTab.select().index();
    var tmpFileUploader = this.getFileUploader();
    var tmpUploader = this.getFileUploader();
    var tmpTakenCanvas = this.getSnapshotCanvas();

    var validator = this.getForm().kendoValidator({
        rules: {
            selectFile: function (input) {
                if (input.is("[name=selectFile]") && tabIndex == 0) {
                    
                    var input = tmpUploader.element;
                    var tmpFile = input.context.files[0];
                    return tmpFile != null;
                } else {
                    return true;
                }
            },
            takeSnapshot: function (input) {
                if (input.is("[name=takeSnapshot]") && tabIndex == 1) {
                    var takenImage = tmpTakenCanvas.toDataURL();
                    return takenImage != "";
                } else {
                    return true;
                }
            }
        },
        messages:
            {
                selectFile: "Please select the file.",
                takeSnapshot: "Please take a snapshot."
            },

    }).data("kendoValidator");


    if (validator.validate()) {
        var tmpSelectedDocumentType = this.getDocumentTypes().dataItem();
        var documentTypeSelected = tmpSelectedDocumentType != null && tmpSelectedDocumentType.Id != -1;

        var tmpAttachmentData = {
            Id: 0,
            Name: this.getDocumentName().val(),
            ShipperDocTypeId: documentTypeSelected ? tmpSelectedDocumentType.Id : -1,
            DocumentType: documentTypeSelected ? tmpSelectedDocumentType.Name : "Other",
            IsSnapshot: false,
            SnapshotData: "",
            Path: "",
            File: null
        }

        if (tmpTab.select().index() == 0) {
            //get file
            var tmpUploader = this.getFileUploader();

            var input = tmpUploader.element;
            var tmpFile = input.context.files[0];

            tmpAttachmentData.File = tmpFile;
            tmpAttachmentData.IsSnapshot = false;

        } else {
            //get snapshot
            var tmpBase64Image = this.getSnapshotCanvas().toDataURL();
            tmpAttachmentData.IsSnapshot = true;
            tmpAttachmentData.SnapshotData = tmpBase64Image;
        }

        this.getGrid().dataSource.add(tmpAttachmentData);

        // if the handler defined
        if (this._eventHandlers.onAttachemntAdd) {
            this._eventHandlers.onAttachemntAdd();
        }

        this.getWindow().close();
        this.clear();
    }
}

DocumentUploader.prototype.clear = function () {
    this.getDocumentName().val("");

    this.clearFileUploader();
    this.clearDocumentSnapshot();
    this.clearCapture();
}

DocumentUploader.prototype.clearCapture = function () {
    var tmpCanvasId = this.getSnapshotCanvas().id;
    var canvas = document.getElementById(tmpCanvasId);
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
}

DocumentUploader.prototype.clearFileUploader = function () {
    var tmpUploader = this.getFileUploader();

    var input = tmpUploader.element;

    input.context.files = null;

    //input.replaceWith(input.val('').clone(true));

    $(tmpUploader.wrapper).find("ul").remove();
}

DocumentUploader.prototype.clearDocumentSnapshot = function () {
    window.MEDIA_HANDLER.stop();
}

DocumentUploader.prototype.initImageCapture = function () {
    var tmpCanvasId = this.getSnapshotCanvas().id;
    var tmpVideoId = this.getSnapshotVideo().id;
    window.MEDIA_HANDLER.start(tmpVideoId, tmpCanvasId, 'doc');
    $("#" + this._form + "-btn-capture").click(function () {
        var canvas = document.getElementById(tmpCanvasId);
        var context = canvas.getContext("2d");
        var video = document.getElementById(tmpVideoId);
        var cropHeight = video.clientHeight;
        var cropWidth = video.clientWidth;
        canvas.width = cropWidth;
        canvas.height = cropHeight;
        context.drawImage(video, 0, 0, cropWidth, cropHeight);
    })
}

DocumentUploader.prototype.uploadOptionsChanged = function () {

    var tmpTab = this.getUploadOptionsTab();

    if (tmpTab.select().index() == 1) {
        this.clearFileUploader();
        this.initImageCapture();
    } else {
        this.clearDocumentSnapshot();
    }
}

DocumentUploader.prototype.setDocumentTypes = function (data) {
    this._documentTypesData = data;
}

DocumentUploader.prototype.AddDocument = function () {
    var tmpWindow = this.getWindow();

    this.getDocumentTypes().setDataSource(this._documentTypesData);

    tmpWindow.setOptions({ minWidth: 500, minHeight: 275 });   
    this.getUploadOptionsTab().select(0);
    tmpWindow.center().open();
    
}