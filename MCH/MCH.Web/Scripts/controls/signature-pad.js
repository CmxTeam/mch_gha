﻿
/// <reference path="../SigWebTablet.js" />

function SignaturePad(settings) {
    this.Inited = false;
    this.SignatureData = "";

    this.AcceptListener = null;

    this.TemplateId = "#sign-usb-pad-template";

    this.TMR = null;

    this.Context = {
        WindowId: settings.WindowContainerId,
        CanvasId: settings.CanvasId,

        Buttons: {
            ClearId: settings.ClearId,
            AcceptId: settings.AcceptId
        },

        Controls: {
            Window: function () { return $("#" + settings.WindowContainerId).data("kendoWindow"); },
            Canvas: function () { return document.getElementById(settings.CanvasId); }
        }
    }

    this.AcceptListener = settings.onSignatureAccept;

}

SignaturePad.prototype.init = function () {
    if (this.Inited == false) {

        this.Inited = true;
    }
}

SignaturePad.prototype.clearSignature = function () {
    var tmpCanvas = this.Context.Controls.Canvas();
    ClearTablet();
    var tmpCanvasContext = tmpCanvas.getContext("2d")
    tmpCanvasContext.clearRect(0, 0, tmpCanvas.width, tmpCanvas.height);
    
    this.SignatureData = "";
}

SignaturePad.prototype.acceptSignature = function () {
  
    var tmpCanvas = this.Context.Controls.Canvas();
    this.SignatureData = tmpCanvas.toDataURL();
    if (this.AcceptListener) {
        this.AcceptListener();
    }
    else
    {
        console.log("Signature-pad.js : Accept signature event is not being listened.");
    }

    this.Context.Controls.Window().close();
}

SignaturePad.prototype.open = function () {
    var htmlContent = $(this.TemplateId).tmpl(
                {
                    canvasId: this.Context.CanvasId,
                    btnClearId: this.Context.Buttons.ClearId,
                    btnAcceptId : this.Context.Buttons.AcceptId
                });

    var tmpWindow = this.Context.Controls.Window();
    tmpWindow.content(htmlContent);
    tmpWindow.center().open();

    $("#" + this.Context.Buttons.ClearId).click(this.clearSignature.bind(this));
    $("#" + this.Context.Buttons.AcceptId).click(this.acceptSignature.bind(this));

    this.initTopazSignPad();
}

SignaturePad.prototype.close = function () {
    this.destroytopazSignPad();
}


SignaturePad.prototype.initTopazSignPad = function () {
    var sigPadcontext = this.Context.Controls.Canvas().getContext('2d');

    try {
        SetDisplayXSize(500);
        SetDisplayYSize(100);
        SetJustifyMode(0);
        ClearTablet();
        this.TMR = SetTabletState(1, sigPadcontext, 50);
    } catch (ex) {
        console.log("Signature-pad.js :" + ex);
    }
}

SignaturePad.prototype.destroytopazSignPad = function () {
    try {
        ClearTablet();
        SetTabletState(0, this.TMR);
    } catch (ex) {
        console.log("Signature-pad.js :" + ex);
    }
}





