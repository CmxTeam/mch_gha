﻿
/// <reference path="../jquery.linq-vsdoc.js" />
/// <reference path="../kendo/2014.3.1119/kendo.all-vsdoc.js" />
/// <reference path="../kendo/2014.3.1119/kendo.all.min.intellisense.js" />


window.DynamicGrid = {
    init: function (argName) {
        var name = argName;
        var toolbar = {};

        toolbar.SearchInput = $("#" + name).find(".cm-grid-toolbar-search-text").first();

        this.setFilterColumns(argName);

        this.initActionBar(argName);

        this.initSearchButtons(argName);

        this.initCheckboxSelect(argName);
    },

    initCheckboxSelect:function(gridName){

    },

    initActionBar: function (gridName) {
        var tmpActionList = $("#" + gridName).find(".cm-grid-toolbar-action-ext").first();
        if (tmpActionList.length > 0) {
           $(tmpActionList).kendoDropDownList();
        }
    },

    initSearchButtons: function (gridName) {

        var searchButton = $("#" + gridName).find(".cm-grid-toolbar-search-button").first();
        var resetButton = $("#" + gridName).find(".cm-grid-toolbar-reset-button").first();

        var tmpSearchInput = $("#" + gridName).find(".cm-grid-toolbar-search-text").first();

        if (searchButton.length > 0) {
            searchButton.on('click', function () {
                if (tmpSearchInput.val().trim() != "") {
                    var tmpGrid = $("#" + gridName).data("kendoGrid");
                    var tmpByColumn = $("#" + gridName).find(".cm-grid-toolbar-search-column input").first();
                    var columnList = tmpByColumn.data("kendoDropDownList");

                    var selectedValue = columnList.text();

                    if (selectedValue == "All") {

                        var allColumns = columnList.dataItems().filter(function (item) { return item.columnText != "All" });

                        for (var i = 0; i < allColumns.length; i++) {
                            tmpGrid.dataSource.filter({ field: allColumns[i].columnName, operator: "contains", value: tmpSearchInput.val() });
                        }

                    } else {
                        tmpGrid.dataSource.filter({ field: columnList.value(), operator: "contains", value: tmpSearchInput.val() });
                    }
                }
            });
        }

        if (resetButton.length > 0) {
            resetButton.on('click', function () {
                var tmpGrid = $("#" + gridName).data("kendoGrid");

                tmpSearchInput.val("");
                tmpGrid.dataSource.filter({});
            });
        }
    },

    setFilterColumns: function (gridName) {

        var tmpGrid = $("#" + gridName).data("kendoGrid");

        toolbar.FilterColumns = $("#" + gridName).find(".cm-grid-toolbar-search-column").first();

        var filterableColumns = tmpGrid.columns.filter(function (item) { return (item.filterable != false) });

        if (filterableColumns.length > 0) {
            var tmpDataSource = [];

            tmpDataSource.push({ columnText: "All", columnName: "" });

            for (var i = 0; i < filterableColumns.length; i++) {
                var item = filterableColumns[i];

                tmpDataSource.push({ columnText: item.title, columnName: item.field });
            }

            $(toolbar.FilterColumns).kendoDropDownList({
                dataTextField: "columnText",
                dataValueField: "columnName",
                dataSource: tmpDataSource
            });;
        }
    }
};