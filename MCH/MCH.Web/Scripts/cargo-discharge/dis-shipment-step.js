﻿/// <reference path="../controls/document-uploader.js" />

function DisShipmentStepController(settings) {
    this.Inited = false;

    this.Uploader = null;

    this.Urls = {
        SaveShipmentView: settings.Urls.SaveShipmentView
      
    }
    this.DisableNext = settings.DisableNext;
    this.Controls = {
        AWBNumber: function () { return $("#txtDisAwbNumber"); },
        AWBId: function () { return $("#hdnDisAwbId") },
        HWBNumber: function () { return $("#txtDisHwbNumber"); },
        HWBId: function () { return $("#hdnDisHwbId") },
        ReleaseStatus: function () { return $("#hdnReleaseStatus") },
        DivCustomsGrid: function () { return $("#divCustomsGrid") },
        
        SelectedParts: function () {
            var entityGrid = this.Grids.CustomsKendo();
            var rows = entityGrid.select();
            var tmpSelectedItems = [];
            rows.each(function (index, row) {
                var selectedItem = entityGrid.dataItem(row);
                tmpSelectedItems.push(selectedItem);
            });
            return tmpSelectedItems;
        },



        Grids: {
            Customs: function () { return $("#gridDisChargeCustoms"); },
            CustomsKendo: function () { return $("#gridDisChargeCustoms").data("kendoGrid") },
        },

        Windows:
            {
                CustomsFullHistory: function () { return $("#wndDISCustomsFullHistory").data("kendoWindow"); }
            },


        Buttons: {
            ViewAWB: function () { return $("#btnDisViewAwb") },
            ViewHWB: function () { return $("#btnDisViewHwb"); },
            ViewFullHistory: function () { return $("#btnDisViewCustomsHistory"); },
        },
    }
}

DisShipmentStepController.prototype.init = function () {
    if (this.Inited == false) {
        var self = this;

        this.Controls.Buttons.ViewFullHistory().click(this.openCustomsFullHistory.bind(this));

        this.Controls.Grids.CustomsKendo().table.on("click", ".check_customs", function (e) { this.selectShipmentPart(e) }.bind(self));
        this.Controls.Grids.CustomsKendo().thead.on("click", ".check_All", function (e) { this.selectShipmentPartAll(e) }.bind(self));

        this.Controls.Grids.CustomsKendo().bind("change", function (e) { this.selectShipmentRowPart(e) }.bind(self));

        this.Inited = true;
    }
}


DisShipmentStepController.prototype.openCustomsFullHistory = function () {
    this.Controls.Windows.CustomsFullHistory().center().open();
}

DisShipmentStepController.prototype.Save = function () {
    var self = this; 
    var validator = self.Controls.DivCustomsGrid().kendoValidator({
        rules: {
            checkCustomsClearance: function (input) {
                if (input.is("[name=checkclear]")) {
                    return self.Controls.SelectedParts().length > 0;
                } else {
                    return true;
                }
            }
        },
        messages:
            {
                checkCustomsClearance: "Customs Clearance cannot be empty."
            },
    }).data("kendoValidator");

    if (validator.validate()) {
        var selected = window.CargoDischargeWizard.Controls.List_Shipments().select();
        var shipmentId = selected.attr("data-item-id");
        var ishwb = selected.attr("data-item-ishwb");
        var dischargeId = window.CargoDischargeWizard.Controls.Hdn_DischargeId().val();

        var dfd = new $.Deferred();

        var list = this.Controls.SelectedParts();
        var formElementsJson = { Parts: list, IsHWB: ishwb, ShipmentId: shipmentId, DischargId: dischargeId };

        $.ajax({
            //  async: true,
            type: 'POST',
            dataType: "json",
            traditional: true,
            url: this.Urls.SaveShipmentView,
            data: JSON.stringify(formElementsJson),
            success: function (data) {
                window.CargoDischargeWizard.Controls.Lbl_StepName().text(data.StepName);
                window.CargoDischargeWizard.Controls.Hdn_DischargeId().val(data.DischargeId);
                var listView = window.CargoDischargeWizard.Controls.List_Shipments();
                listView.dataSource.page(1);

                dfd.resolve();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            cache: false,
            contentType: 'application/json, charset=utf-8',
            processData: false
        });


        //if (isValid) {
        //    dfd.resolve({ a: 21 });
        //}
    }
    return dfd.promise();
}

//select by row selection
DisShipmentStepController.prototype.selectShipmentRowPart = function (e) {
    var rowstodeselect = this.Controls.Grids.CustomsKendo().select().closest("tr:not(:has(input.check_customs))");
    rowstodeselect.removeClass("k-state-selected  k-state-selecting");

    var checkbox = this.Controls.Grids.CustomsKendo().select().closest("tr").find(".check_customs");

    if (checkbox != undefined) {
        var checkboxAll = this.Controls.Grids.CustomsKendo().table.find(".check_customs");
        $(checkboxAll).prop("checked", false);

        $(checkbox).prop("checked", true);      
    }
   
    //console.log(e);
}

DisShipmentStepController.prototype.selectShipmentPartAll = function (e) {
    var checked = e.currentTarget.checked;
    var table = this.Controls.Grids.CustomsKendo().table;
    var checkboxAll = table.find(".check_customs");
    var rows = table.find("tr:has(input.check_customs)");
    $(checkboxAll).prop("checked", checked);
    if (checked) {
        rows.addClass("k-state-selected");
    } else {
        rows.removeClass("k-state-selected");
    }
}

//select by checkbox
DisShipmentStepController.prototype.selectShipmentPart = function (e) {
    var checked = e.currentTarget.checked;
    var row = $(e.currentTarget).closest("tr");

    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}


