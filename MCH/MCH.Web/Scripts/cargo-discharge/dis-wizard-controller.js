﻿function DischargeWizardController(settings) {
    this.Inited = false;

    this.InnerController = {Save : function(){}};

    this.Constants = {
        Steps: {
            Shipment: 1,
            Driver: 2,
            Charges : 3
        }
    }

    this.CurrentStep = this.Constants.Steps.Shipment;

    this.Controls = {
        WizardContainer: function () { return $("#wizardDischangeContainer"); },

        Btn_Search: function () { return $("#btnDisSearchShipment"); },
        Btn_Reset: function () { return $("#btnDisResetShipment"); },

        Btn_Next: function () { return $("#btnDischargeWizNext"); },
        Btn_Prev: function () { return $("#btnDischargeWizPrev"); },
        Btn_Save: function () { return $("#btnDischargeSave"); },
        Btn_Finalize : function() {return $("#btnDischargeFinalize");},

        Txt_AWB: function () { return $("#txtDisSearchAwb"); },
        Txt_HWB: function () { return $("#txtDisSearchHwb"); },
        Hdn_DriverId: function () { return $("#hdnDriverId"); },

        Lbl_StepName: function () { return $("#lblHeaderStepName"); },

        List_Shipments: function () { return $("#lsvDisShipments").data("kendoListView"); },
        Hdn_DischargeId: function () { return $("#hdnDischargeId"); },
    }

    this.Urls = {
        GetShipmentView: settings.Urls.GetShipmentView,
        GetDriverView: settings.Urls.GetDriverView,
        GetChargesView: settings.Urls.GetChargesView,
        Finalize: settings.Urls.Finalize
    }
}

DischargeWizardController.prototype.init = function () {

    if (this.Inited == false) {

        this.Controls.Btn_Search().click(this.searchShipment.bind(this));

        this.Controls.Btn_Next().click(this.goNext.bind(this));
        this.Controls.Btn_Reset().click(this.resetSearch.bind(this));
        this.Controls.Btn_Prev().click(this.goBack.bind(this));
        this.Controls.Btn_Save().click(this.saveShipment.bind(this));
        this.Controls.Btn_Finalize().click(this.finalize.bind(this));

        this.Inited = true;
    }
}

DischargeWizardController.prototype.saveShipment = function () {

    var self = this;
    var currentStepSavePromise = self.InnerController.Save();

    currentStepSavePromise.done(function (data) {
     

        self.Controls.WizardContainer().empty();
    })
}

DischargeWizardController.prototype.saveDriver = function () {

}

DischargeWizardController.prototype.getDischargeId = function () {
    return { dischargeId: this.Controls.Hdn_DischargeId().val() };
}

DischargeWizardController.prototype.finalize = function () {

    var self = this;
    var currentStepSavePromise = self.InnerController.Save();

    currentStepSavePromise.done(function (data) {
        var dischargeId = window.CargoDischargeWizard.Controls.Hdn_DischargeId().val(); 
 
        var formElementsJson = { dischargeId: dischargeId };

        $.ajax({
            //  async: true,
            type: 'POST',
            dataType: "html",
            traditional: true,
            url: self.Urls.Finalize,
            data: JSON.stringify(formElementsJson),
            success: function (html) {
                alert("tadaaam!");
                var tmpWindow = window.TaskManager.windows.AddEdit();
                tmpWindow.close();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            cache: false,
            contentType: 'application/json, charset=utf-8',
            processData: false
        });
    })
}

DischargeWizardController.prototype.resetSearch = function () {
    this.Controls.Txt_AWB().val('');
    this.Controls.Txt_HWB().val('');
    this.Controls.WizardContainer().empty();
}

DischargeWizardController.prototype.searchShipment = function () {
    var tmpAWBNumber = this.Controls.Txt_AWB().val();
    var tmpHWBNumber = this.Controls.Txt_HWB().val();
    this.loadShipment('', tmpAWBNumber, tmpHWBNumber, false);
}

DischargeWizardController.prototype.databoundShipments = function () {
    //if (this.Controls.Hdn_DriverId().val() != "")
    //    {
    var listView = this.model.Controls.List_Shipments();
    for (index = 0; index < listView.element.children().length; index++) {
        var child = $(listView.element.children()[index]);
        if ($(child).attr("data-item-id") == this.id) {
            listView.select(child);
        }
    }
    listView.unbind("dataBound");
}

DischargeWizardController.prototype.selectShipment = function () {
    var selected = this.Controls.List_Shipments().select();
    var shipmentId = selected.attr("data-item-id");
    var ishwb = selected.attr("data-item-ishwb");

    this.loadShipment(shipmentId, null, null, ishwb);
}

DischargeWizardController.prototype.loadShipment = function (shipmentId, awbNumber, hwbNumber, isByHWB) {
    var tmpUrl = this.Urls.GetShipmentView + "/?shipmentId=" + shipmentId + "&awbNumber="+awbNumber + "&hwbNumber="+hwbNumber + "&isByHWB=" + isByHWB;
    var self = this;
    this.Controls.WizardContainer().load(tmpUrl, function () {
        self.InnerController = window.DisShipmentStep;

        self.Controls.Btn_Prev().hide();
        self.Controls.Btn_Finalize().hide();
        self.Controls.Btn_Save().hide();

        if (self.InnerController.DisableNext) {
            self.Controls.Btn_Next().hide();
        }
        else
        {
            self.Controls.Btn_Next().show();
        }
    });

   
}

DischargeWizardController.prototype.goToPage = function (page) {
    var self = this;
    switch (page) {
        case self.Constants.Steps.Shipment: {
            self.Controls.Btn_Prev().show();
            var tmpUrl = self.Urls.GetShipmentView;
            self.Controls.WizardContainer().load(tmpUrl, function () {
                self.CurrentStep = self.Constants.Steps.Shipment;
                    self.Controls.Btn_Next().show();
            
                self.Controls.Btn_Prev().hide();
                self.Controls.Btn_Finalize().hide();
                self.Controls.Btn_Save().hide();
            });
            break;
        }
        case self.Constants.Steps.Driver: {
            var tmpUrl = self.Urls.GetDriverView;
            self.Controls.WizardContainer().load(tmpUrl, function () {
                self.CurrentStep = self.Constants.Steps.Driver;
                self.Controls.Btn_Prev().show();
                self.Controls.Btn_Next().show();
                self.Controls.Btn_Save().hide();
                self.Controls.Btn_Finalize().hide();
                self.InnerController = window.WizardDriverStep;
            });
            break;
        }
        case self.Constants.Steps.Charges: {
            self.Controls.Btn_Prev().show();
            var tmpUrl = self.Urls.GetChargesView;
            self.Controls.WizardContainer().load(tmpUrl, function () {
                self.CurrentStep = self.Constants.Steps.Charges;
                self.Controls.Btn_Next().hide();
                self.Controls.Btn_Prev().show();
                self.Controls.Btn_Finalize().show();
                self.Controls.Btn_Save().show();
                self.InnerController = window.DisPaymentStep;
            });
            break;
        }
        default: {
            break;
        }
    }
}

DischargeWizardController.prototype.goNext = function () {

    //TODO : validate current form based on the step

    var self = this;
    var currentStepSavePromise = self.InnerController.Save();

    currentStepSavePromise.done(function (data) {
        switch (self.CurrentStep) {
            case self.Constants.Steps.Shipment: {

                var driverId = self.Controls.Hdn_DriverId().val();
                if (driverId != "") {
                    self.goToPage(self.Constants.Steps.Charges);
                }
                else {
                    self.goToPage(self.Constants.Steps.Driver);
                }
                break;
            }
            case self.Constants.Steps.Driver: {
                self.goToPage(self.Constants.Steps.Charges);
                break;
            }
            default: {
                break;
            }
        }
    })
}

DischargeWizardController.prototype.goBack = function () {
    var self = this;
    switch (self.CurrentStep) {
        case self.Constants.Steps.Charges: {
            var driverId = self.Controls.Hdn_DriverId().val();
            if (driverId != "") {
                self.goToPage(self.Constants.Steps.Shipment);
            }
            else {
                self.goToPage(self.Constants.Steps.Driver);
            }
            break;
        }
        case this.Constants.Steps.Driver: {
            self.goToPage(self.Constants.Steps.Shipment);
            break;
        }
        default: {
            break;
        }
    }
}

