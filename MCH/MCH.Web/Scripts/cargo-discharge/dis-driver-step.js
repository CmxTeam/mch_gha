﻿
/// <reference path="../controls/photo-capturer.js" />

function DisDriverStepController(argUrls) {
    this.initialized = false;
    this.IDCapturer = null;
    this.PhotoCapturere = null;

    this.Urls =
        {
            SaveDriver: argUrls.SaveDISDriver,
            SaveTruckingCompany: argUrls.SaveTruckingCompany,
            GetDriverDetails: argUrls.GetDriverDetials,
            SaveDriverView: argUrls.SaveDriverView,
        },

    this.Window_AddDriver = function () { return $("#wndDISDriverAdd").data("kendoWindow"); };
    this.Window_AddTruckingCmp = function () { return $("#wndDISTruckingCompAdd").data("kendoWindow"); }

    this.Controls =
        {
            PickupSections: {
                Consignee: function () { return $("#dvDischargPickupOptions").children().eq(0); },
                Carrier: function () { return $("#dvDischargPickupOptions").children().eq(1); }
            },

            PickupOption: function () { return $("input[name='disShipmentPickup']"); },

            DocumentTypes: function () { return $("#ddlDisDocumentTypes").data("kendoDropDownList"); },

            DocumentUploaderWindow: function () { return $("#wndDISShipmentDocUploader") },

            Documents: function () { return $("#gridDisChargeShipmentDocs"); },

            AddDocument: function () { return $("#btnDisAddDocument"); },


            Drivers: function () { return $("#ddlDrivers").data("kendoComboBox"); },
            TruckingCompanies: function () { return $("#ddlTruckingCompany").data("kendoComboBox") },
            STANumber: function () { return $("#txtDISDriverSTA"); },
            DriverLicense: function () { return $("#txtDISDriverLicense"); },
            States: function () { return $("#ddlDriverState").data("kendoComboBox"); },
            FirstIDTypes: function () { return $("#ddlFirstIDTypes").data("kendoComboBox"); },
            FirstIDMatchedCheck: function () { return $("#chbxFirstIDMatched").data("kendoComboBox"); },
            IsFirstIDMatched: function () {
                var isMatched = this.FirstIDMatchedCheck().value();
                if (isMatched == "False") {
                    return false;
                }
                else {
                    return true;
                }
            },
            SecondIDTypes: function () { return $("#ddlSecondIDTypeId").data("kendoComboBox"); },
            SecondIDMatchedCheck: function () { return $("#chbxSecondIDMatched").data("kendoComboBox") },

            Image_CaptureId: function () { return $("#imgAcaCaptureId"); },
            ImageValue_CaptureId: function () { return $("#hdnAcaCaptureId"); },

            Image_CapturePhoto: function () { return $("#imgAcaCapturePhoto"); },
            ImageValue_CapturePhoto: function () { return $("#hdnAcaCapturePhoto"); },

            Button_CaptureId: function () { return $("#btnAcaCaptureId"); },
            Button_CapturePhoto: function () { return $("#btnAcaCapturePhoto"); },

            Button_AddDriver: function () {
                return $("#btnDISAddDriver");
            },
            Button_AddTruckingComp: function () {
                return $("#btnDISAddTruckComp");
            },
            Button_SumbitDriver: function () { return $("#btnFromDriver"); },
            Button_SubmitTruckComp: function () { return $("#btnFromTruckingComp") },

            Form: function () { return $("#frmDriverView"); },
            FormWndDriver: function () { return $("#frmDISDriverAdd"); },
            FormWndTruckCom: function () { return $("#frmDISTruckCompAdd") }
        }

    this.FormSections =
        {
            SecondIDTypes: function () { return $("#dvSecondIDType"); }
        }

 
}

DisDriverStepController.prototype.onUploaderClose = function () {
    this.Uploader.clear();
}

DisDriverStepController.prototype.init = function () {
    var self = this;

    if (this.initialized == false) {
        //set the drop down list change event handlers

        this.Controls.AddDocument().click(this.openDocumentUploader.bind(this));

        this.Controls.TruckingCompanies().setOptions({ change: this.onTruckingCompChange.bind(this) });

        this.Controls.Drivers().setOptions({ change: this.onDriversChange.bind(this) });

        this.Controls.FirstIDMatchedCheck().setOptions({ change: this.onFirstIDCheckChange.bind(this) });

        this.Controls.Button_AddDriver().click(function () {
            var tmpWindow = this.Window_AddDriver();
            tmpWindow.setOptions({ minWidth: 500 })
            tmpWindow.center().open();
        }.bind(this));

        this.Controls.Button_SumbitDriver().click(this.submitDriver.bind(this));

        this.Controls.Button_AddTruckingComp().click(function () {
            var tmpWindow = this.Window_AddTruckingCmp();
            tmpWindow.setOptions({ minWidth: 500 })
            tmpWindow.center().open();
        }.bind(this));

        this.Controls.Button_SubmitTruckComp().click(this.submitTruckingComp.bind(this));

        this.Controls.Button_CaptureId().click(this.showDriverIdCapture.bind(this));

        this.Controls.Button_CapturePhoto().click(this.showDriverPhotoCapture.bind(this));

        var tmpCaptureIdPhoto = this.Controls.ImageValue_CaptureId().val()
        if (tmpCaptureIdPhoto != "") {
            this.Controls.Image_CaptureId().attr("src", tmpCaptureIdPhoto);
        }

        var tmpCaptureDriverPhoto = this.Controls.ImageValue_CapturePhoto().val();
        if (tmpCaptureDriverPhoto != "") {
            this.Controls.Image_CapturePhoto().attr("src", tmpCaptureDriverPhoto);
        }

        this.Controls.PickupOption().change(function (e) {

            if ($(this).val() == 'consignee') {
                self.showConsignee();
            } else {
                self.showCarrier();
            }
        });


        this.initialized = true;
    }
}



DisDriverStepController.prototype.showDriverIdCapture = function () {
    this.IDCapturer = new PhotoCapturer("Take Driver ID", 'doc');
    this.IDCapturer.init();
    this.IDCapturer.open(null, this.Controls.Image_CaptureId(), this.Controls.ImageValue_CaptureId());
};

DisDriverStepController.prototype.showDriverPhotoCapture = function () {
    this.PhotoCapturere = new PhotoCapturer("Take Driver Photo", 'img');
    this.PhotoCapturere.init();

    this.PhotoCapturere.open({ width: 503, height: 254 }, this.Controls.Image_CapturePhoto(), this.Controls.ImageValue_CapturePhoto());
};

DisDriverStepController.prototype.submitDriver = function () {
    var tmpForm = this.Controls.FormWndDriver();
    var validator = tmpForm.kendoValidator().data("kendoValidator");
    var self = this;
    if (validator.validate()) {
        var formElementsJson = this.Controls.FormWndDriver().serializeObject();
        var tmpUrl = this.Urls.SaveDriver;
        var tmpDriversList = this.Controls.Drivers();
        var tmpDriverWindow = this.Window_AddDriver();
        var driverData = {
            Id: 0,
            FullName: formElementsJson.FullName,
            STANumber: formElementsJson.STANumber,
            DriverLicense: formElementsJson.DriverLicense,
            StateId: formElementsJson.StateId
        };
        $.ajax({
            type: 'POST',
            url: tmpUrl,
            data: JSON.stringify(driverData),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Id) {
                    tmpDriversList.dataSource.read().then(function () {
                        tmpDriversList.value(data.Id);
                    });
                    tmpDriverWindow.close();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    }
}

DisDriverStepController.prototype.submitTruckingComp = function () {
    var tmpForm = this.Controls.FormWndTruckCom();
    var validator = tmpForm.kendoValidator().data("kendoValidator");

    if (validator.validate()) {
        var formElementsJson = this.Controls.FormWndTruckCom().serializeObject();
        var tmpUrl = this.Urls.SaveTruckingCompany;
        var tmpTruckingCompWindow = this.Window_AddTruckingCmp();
        var tmpTruckCompList = this.Controls.TruckingCompanies();
        var tmpTruckingData = {
            TruckingCompanyName: formElementsJson.TruckingCompanyName,
        };
        $.ajax({
            type: 'POST',
            url: tmpUrl,
            data: JSON.stringify(tmpTruckingData),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.Id) {
                    tmpTruckCompList.dataSource.read().then(function () {
                        tmpTruckCompList.value(data.Id);
                    });
                    tmpTruckingCompWindow.close();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    }
}

DisDriverStepController.prototype.onDriversChange = function () {
    var self = this;
    var tmpSelectedDriverId = this.Controls.Drivers().value();
    if (tmpSelectedDriverId && tmpSelectedDriverId != "" && tmpSelectedDriverId != null) {
        var getDriverDetailsUrl = this.Urls.GetDriverDetails + "/?id=" + tmpSelectedDriverId;

        $.getJSON(getDriverDetailsUrl, function (data) {
            if (data) {
                self.Controls.DriverLicense().val(data.DriverLicense);
                self.Controls.STANumber().val(data.STANumber);
                self.Controls.States().value(data.StateId);
            }
        });
    }
}

DisDriverStepController.prototype.onTruckingCompChange = function () {

}

DisDriverStepController.prototype.onFirstIDCheckChange = function () {
    if (!this.Controls.IsFirstIDMatched()) {
        this.FormSections.SecondIDTypes().show();
    } else {
        this.FormSections.SecondIDTypes().hide();
    }
}



DisDriverStepController.prototype.deleteRow = function (e) {
    var tr = $(e.target).closest("tr"); //get the row for deletion
    var data = this.dataItem(tr);
    var grid = $("#gridDisChargeShipmentDocs").data("kendoGrid");
    grid.dataSource.remove(data);
}

DisDriverStepController.prototype.setUploader = function (uploader) {
    this.Uploader = uploader;
}

DisDriverStepController.prototype.onUploaderClose = function () {
    this.Uploader.clear();
}

DisDriverStepController.prototype.showConsignee = function () {
    this.Controls.PickupSections.Consignee().show();
    this.Controls.PickupSections.Carrier().hide();
}

DisDriverStepController.prototype.showCarrier = function () {
    this.Controls.PickupSections.Consignee().hide();
    this.Controls.PickupSections.Carrier().show();
}


DisDriverStepController.prototype.openDocumentUploader = function () {
    var self = this;
    self.setUploader(new DocumentUploader(
     {
         GridName: self.Controls.Documents().attr("id"),
         WindowName: self.Controls.DocumentUploaderWindow().attr("id"),
         FormName: "frmDisShipmentDocUploader",
         Urls: {}
     }));
    self.Uploader.init();
    self.Uploader.setDocumentTypes(self.Controls.DocumentTypes().dataSource.data());
    self.Uploader.AddDocument();
}


/////


DisDriverStepController.prototype.Save = function () {
    var dfd = new $.Deferred();
    //var tmpSignRequired = this.FormSections.SignaturePad().is(":visible");
    //var tmpCanvasValue = this.Controls.SignImage().attr("src");
    var tmpGrid = this.Controls.Documents().data("kendoGrid");
    // validation form
    var tmpSecondIdRequired = this.FormSections.SecondIDTypes().is(":visible");
    var validator = this.Controls.Form().kendoValidator({
        rules: {
            checkDocuments: function (input) {
                if (input.is("[name=checkdoc]")) {
                    return tmpGrid.dataSource.data().length > 0;
                } else {
                    return true;
                }
            },
            checkSecondID: function (input) {
                if (input.is("[name=checkSecondID]")) {
                    if (tmpSecondIdRequired) {
                        return tmpSecondMatch == "True";
                    }
                    else { return true; }
                } else {
                    return true;
                }
            }
        },
        messages:
            {
                checkDocuments: "Documents cannot be empty.",
                checkDocumentVerified: "Please verify the documents.",
                checkSecondID: "2nd Photo ID should match."
            },
    }).data("kendoValidator");

    if (validator.validate()) {
        var tmpAttachments = tmpGrid.dataSource.data();
        var tmpFormData = new FormData();

        var formElementsJson = this.Controls.Form().serializeObject();
        formElementsJson.Documents = [];
        if (tmpAttachments && tmpAttachments.length > 0) {
            for (var i = 0; i < tmpAttachments.length; i++) {
                var documentItem = tmpAttachments[i];
                formElementsJson.Documents.push({
                    Id: documentItem.Id,
                    Name: documentItem.Name,
                    ShipperDocTypeId: documentItem.ShipperDocTypeId,
                    DocumentType: documentItem.DocumentType,
                    IsSnapshot: documentItem.IsSnapshot,
                    SnapshotData: documentItem.SnapshotData,
                    Path: documentItem.Path
                });

                if (documentItem.IsSnapshot == false) {
                    tmpFormData.append("file", documentItem.File);
                }
            }
        }

        //
        var isconsignee = formElementsJson.disShipmentPickup == "consignee";

        var tmpPickupData = {
            Consignee: { Id: formElementsJson["Consignee[Id]"] },
            CarrierId: formElementsJson["PickupDetails.CarrierId"],
            TransferManifest: formElementsJson["PickupDetails.TransferManifest"],
            IsConsigneeOrCarrier: isconsignee
        };

        formElementsJson.PickupDetails = tmpPickupData;

        var dischargeId = window.CargoDischargeWizard.Controls.Hdn_DischargeId().val();
        formElementsJson.DischargeId = dischargeId

        //
        tmpFormData.append("formData", JSON.stringify(formElementsJson));
        var tmpUrl = this.Urls.SaveDriverView;
        $.ajax({
            type: 'POST',
            dataType: "html",
            traditional: true,
            url: tmpUrl,
            data: tmpFormData,
            success: function (html) {
                $("#divCargoDischargeHeader").html(html);

                dfd.resolve();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    return dfd.promise();
}










