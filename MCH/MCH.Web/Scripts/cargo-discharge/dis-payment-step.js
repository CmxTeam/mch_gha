﻿function DisPaymentStepController(settings) {
    this.Inited = false;
    this.Urls = {
        SaveChargesView: settings.Urls.SaveChargesView
    };

    this.Controls = {
        PaymentsGrid: function () { return $("#gridDischargePayments").data("kendoGrid"); },
        Btn_AddCharge: function () { return $("#btnDisPaymentAddCharge"); },
        DivPayments: function () { return $("#divPayments"); },
    };
}

DisPaymentStepController.prototype.init = function () {
    if (this.Inited == false) {

        this.Inited = true;
    }
}

DisPaymentStepController.prototype.onPaymentCancel = function (e) {

}

DisPaymentStepController.prototype.onPaymentRemove = function (e) { }

DisPaymentStepController.prototype.onPaymentSave = function (e) {

}

DisPaymentStepController.prototype.Save = function () {
    
    var self = this;
    var tmpGrid = self.Controls.PaymentsGrid();

    var validator = self.Controls.DivPayments().kendoValidator({
        rules: {
            checkCustomsClearance: function (input) {
                if (input.is("[name=checkcharges]")) {
                    return tmpGrid.dataSource.data().length > 0;
                } else {
                    return true;
                }
            }
        },
        messages:
            {
                checkCustomsClearance: "Payment Charges cannot be empty."
            },
    }).data("kendoValidator");

    if (validator.validate()) {
        var selected = window.CargoDischargeWizard.Controls.List_Shipments().select();
        var shipmentId = selected.attr("data-item-id");
        var ishwb = selected.attr("data-item-ishwb");
        var dischargeId = window.CargoDischargeWizard.Controls.Hdn_DischargeId().val();

        var dfd = new $.Deferred();

        var formElementsJson = { IsHWB: ishwb, ShipmentId: shipmentId, DischargId: dischargeId };

        $.ajax({
            //  async: true,
            type: 'POST',
            dataType: "html",
            traditional: true,
            url: this.Urls.SaveChargesView,
            data: JSON.stringify(formElementsJson),
            success: function (html) {
                dfd.resolve();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
            cache: false,
            contentType: 'application/json, charset=utf-8',
            processData: false
        });


        //if (isValid) {
        //    dfd.resolve({ a: 21 });
        //}
    }
    return dfd.promise();
}

DisPaymentStepController.prototype.deleteCharge = function (e) {
    var tr = $(e.target).closest("tr"); //get the row for deletion
    var data = this.dataItem(tr);
    var grid = $("#gridDischargePayments").data("kendoGrid");
    grid.dataSource.remove(data);
}

