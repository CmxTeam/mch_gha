﻿


function DashboardModel(baseUrl, loadContainerId) {
    this.baseUrl = baseUrl;
    this.loadContainer = loadContainerId;
    this.dashPanelUrl = "";
}

DashboardModel.prototype.loadDashboardPanel = function (callback) {
    var dashboardFilter = {
        'FromDate': $("#dpDashFrom").val(),
        'ToDate': $("#dpDashTo").val(),
        'StationId': $("#dashHandlingStation").select2('data').id
    };

    var callbackFunction = callback != undefined && callback!=null ? callback : function () { };

    $("#" + this.loadContainer).load(this.dashPanelUrl,dashboardFilter, callbackFunction);
}

DashboardModel.prototype.initComponents = function (dashPanelUrl) {

    var self = this;


    this.dashPanelUrl = dashPanelUrl;

    if (typeof $.fn.bdatepicker == 'undefined') {
        $.fn.bdatepicker = $.fn.datepicker.noConflict();
    }

    //$("#dashboardFrom").val('' + new Date().customFormat("#MM#/#DD#/#YYYY#"));

    $("#dashboardFrom").bdatepicker({
        format: "mm/dd/yyyy",
        autoclose: true
    });


    //$("#dashboardTo").val('' + new Date().customFormat("#MM#/#DD#/#YYYY#"));
    $("#dashboardTo").bdatepicker({
        format: "mm/dd/yyyy",
        autoclose: true
    });
   

    $("#btnFilterDashPanel").click(function () {
        self.loadDashboardPanel(null);
    })

    this.loadDashboardPanel(function () { });
}



