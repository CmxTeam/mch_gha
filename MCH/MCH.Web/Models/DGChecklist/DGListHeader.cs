﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.DGChecklist
{
    public class DGListHeader
    {

        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<DGCheckItem> Items{get;set;}

    }

    public class DGCheckItem 
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int ? ParentId { get; set; }
        public string Options { get; set; }
        public IEnumerable<DGCheckItem> SubItems { get;set; }
        public string SelectedOption { get; set; }
        public DateTime? CreatedDate {get;set;}
        public DateTime ? LastUpdated {get;set;}
        public string Comment {get;set;}


        public int? Number { get; set; }
    }

    public class DGListModel 
    {
        public int? AccountId { get; set; }
        public int EntityTypeId {get;set;}
        public long EntityId {get;set;}
        public string Username {get;set;}
        public string Place {get;set;}
        public DateTime ? CommentedOn {get;set;}
        public string  Comments {get;set;}
        public long ? TaskId { get; set; }
        public string ShipmentNumber { get; set; }
        public string SignatureData { get; set; }

        public List<DGListHeader> CheckList { get; set; }

       
    }
}