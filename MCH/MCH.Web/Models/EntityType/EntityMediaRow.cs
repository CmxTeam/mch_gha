﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.EntityType
{
    public class EntityMediaRow
    {
        public long Id { get; set; }
        public string ImageData { get; set; }
        public string Title { get; set; }
        public DateTime Timestamp { get; set; }
        public string User { get; set; }
    }
}