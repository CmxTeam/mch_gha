﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.EntityType
{
    public class EntityHistoryRow
    {
        public long Id { get; set; }
        public string Reference { get; set; }
        public DateTime ? Timestamp { get; set; }
        public string Description { get; set; }
        public string User { get; set; }
    }
}