﻿using MCH.BLL.DAL;
using MCH.BLL.Model.AwbWizard;
using MCH.BLL.Model.Customs;
using MCH.BLL.Model.SpecialHandling;
using MCH.BLL.Model.Warehouse;
using MCH.Web.Models.Common;
using MCH.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Models.WarehouseManagement
{

    public class WarehouseManagement
    {
        public long? StationId { get; set; }

        public long? WarehouseId { get; set; }

        public long? RegionId { get; set; }

        public long? LocationId { get; set; }
    }


    public class ShipmentTab
    {
        public long StationId { get; set; }

        public long? WareHouseId { get; set; }
    }
    public class WarehouseShipmentInventory
    {

        public long ? ShipmentId { get; set; }

        public bool ? IsOSD { get; set; }

        public string SpecialHandlingsIndicators { get; set; }

        public  SpecialHandlingIndicator SpecialHandlingsSource { get; set; }

        public string ShipmentReference { get; set; }
        public string Warehouse { get; set; }

        public string Origin { get; set; }

        public string Destination { get; set; }

        public string Location { get; set; }

        public int? Pieces { get; set; }

        public int? TotalPieces { get; set; }


        public DateTime? FirstScan { get; set; }
        public DateTime? LastScan { get; set; }

        public string User { get; set; }
        public double? Duration
        {
            get
            {
                double? tmpHoures = null;
                if (this.LastScan.HasValue && this.FirstScan.HasValue)
                {
                    tmpHoures = (this.LastScan.Value - this.FirstScan.Value).TotalHours;
                }
                return tmpHoures;
            }
        }
        public string Shipper { get; set; }
        public string Consignee { get; set; }

        private double? totalWeight;
        public double? TotalWeight { get { return this.totalWeight; } set { this.totalWeight = value; } }

        public string WeightUOM { get; set; }
    }

    public class WarehouseLocationInventory
    {
        public long WarehouseLocationId { get; set; }
        public string Warehouse { get; set; }
        public string Location { get; set; }
        public int? Pieces { get; set; }
        public int? TotalPieces { get; set; }
        public DateTime? LastScan { get; set; }
        public string User { get; set; }
        public double? TotalWeight { get; set; }
        public string WeightUOM { get; set; }

    }


    public class LocationAddEdit
    {
        public long? LocationId { get; set; }

        public long? StationId { get; set; }

        public long? WarehouseId { get; set; }

        public int? RegionId { get; set; }

        public long? LocationTypeId { get; set; }

        public string LocationName { get; set; }

        public string BarCode { get; set; }

        public WarehouseLocationBo ToBusinessModel()
        {

            return new WarehouseLocationBo
                {
                    Barcode = this.BarCode,
                    LocationId = this.LocationId,
                    LocationName = this.LocationName,
                    LocationTypeId = this.LocationTypeId,
                    ZoneId = this.RegionId,
                    PortId = this.StationId,
                    WarehouseId = this.WarehouseId
                };
        }
    }

    public class WarehouseAddEdit
    {
        public long? WarehouseId { get; set; }

        public long? StationId { get; set; }

        public int? ShellWarehouseId { get; set; }
        public string WarehouseName { get; set; }

        public string AliasName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }

        public string Postal { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ManagerName { get; set; }
        public string GeoLocation { get; set; }

        public double ? Latitude { get; set; }
        public double ? Longitude { get; set; }

        public WarehouseBo ToBusinesModel() 
        {
            return new WarehouseBo
            {
                Address1 = this.Address1,
                Address2 = this.Address2,
                City = this.City,
                Code = this.AliasName,
                CountryId = this.CountryId,
                FirmCode = string.Empty,
                Id = this.WarehouseId,
                Name = this.WarehouseName,
                Phone = this.Phone,
                PortId = this.StationId,
                PostalCode = this.Postal,
                SateId = this.StateId,
                ShellWarehouseId = this.ShellWarehouseId,
                Supervisor = this.ManagerName,
                GeoLocation = this.GeoLocation,
                ContactEmail = this.Email,
            };
        }

    }

    public class RegionAddEdit
    {
        public long? RegionId { get; set; }
        public long? StationId { get; set; }
        public long? WarehouseId { get; set; }

        public string RegionName { get; set; }


        public WarehouseZoneBo ToBusinessModel()
        {
            return new WarehouseZoneBo
                {
                    ZoneId = this.RegionId,
                    PortId = this.StationId,
                    Name = this.RegionName,
                    WarehouseId = this.WarehouseId
                };
        }
    }


}