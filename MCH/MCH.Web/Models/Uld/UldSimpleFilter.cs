﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Uld
{
    public class UldSimpleFilter
    {
        public string Term { get; set; }
        public long? AwbId { get; set; }

        public long? Id { get; set; }
    }
}