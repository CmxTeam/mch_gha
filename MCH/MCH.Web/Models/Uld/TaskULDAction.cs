﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Uld
{
    public class TaskULDAction
    {
        public long TaskId { get; set; }
        public string ULDStringIds { get; set; }

        public bool IsBUP { get; set; }
    }
}