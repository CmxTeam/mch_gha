﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Uld
{
    public class FlightUldsRow
    {
        public long UldId { get; set; }
        public string UldType { get; set; }
        public string UldNumber { get; set; }
        public int AWBs { get; set;}
        public string Pieces { get; set; }
        public string NetWeight { get; set; }
        public string GrossWeight { get; set; }
        public string TareWeight { get; set; }
        public string Destination { get; set; }
        public bool IsTransferCargo { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public string StatusTimestamp { get; set; }

    }

    public class AwbUldsRow
    {

        public bool BUP { get; set; }

        public string Location { get; set; }

        public int? STC { get; set; }

        public long? Id { get; set; }

        public string Number { get; set; }

        public string Owner { get; set; }

        public string Type { get; set; }
    }
}