﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Uld
{
    public class AddTaskULDModel
    {

        public long UldTypeId { get; set; }
        public long  PrefixId { get; set; }
        public string Serial { get; set; }
        public long TaskId { get; set; }

    }
}