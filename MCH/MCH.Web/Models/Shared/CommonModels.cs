﻿using MCH.BLL.Model.Common;
using MCH.Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Shared
{
    public class ChargeDeclarations
    {
        public long? Id { get; set; }
        public long? CurrencyId { get; set; }

        //  public long? WTVALCharges { get; set; }

        public int? WTVALChargesType { get; set; }

        //  public long? OtherCharges { get; set; }

        public int? OtherChargesType { get; set; }

        public decimal? DeclaredValueForCarrige { get; set; }

        public bool NVD { get; set; }

        public decimal? DeclaredValueForCustoms { get; set; }

        public bool NCV { get; set; }

        public decimal? InsuranceAmount { get; set; }

        public bool XXX { get; set; }

        public ChargeDeclarationModel ToBackendModel()
        {
            ChargeDeclarationModel tmpModle = new ChargeDeclarationModel();
            tmpModle.CurrencyId = this.CurrencyId;
            tmpModle.DeclaredValueForCarrige = this.DeclaredValueForCarrige;
            tmpModle.DeclaredValueForCustoms = this.DeclaredValueForCustoms;
            tmpModle.Id = this.Id;
            tmpModle.InsuranceAmount = this.InsuranceAmount;
            tmpModle.NCV = this.NCV;
            tmpModle.NVD = this.NVD;
            tmpModle.OtherChargesType = this.OtherChargesType;
            tmpModle.WTVALChargesType = this.WTVALChargesType;
            tmpModle.XXX = this.XXX;
            return tmpModle;
        }
    }
  
    public class Agent
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string AccountNumber { get; set; }
        public string City { get; set; }
        public string IATACode { get; set; }
        public string CASSAddress { get; set; }
        public int? ParticipantIdentfierId { get; set; }

        public PartnerModel ToBackendModel()
        {
            PartnerModel tmpModle = new PartnerModel();
            tmpModle.AccountNumber = this.AccountNumber;
            tmpModle.CASSAddress = this.CASSAddress;
            tmpModle.City = this.City;
            tmpModle.IATACode = this.IATACode;
            tmpModle.Id = this.Id;
            tmpModle.Name = this.Name;
            tmpModle.ParticipantIdentfierId = this.ParticipantIdentfierId;
            return tmpModle;
        }

    }
    public class Code
    {
        public long Id { get; set; }
        public long? CodeId { get; set; }
        public string CodeNote { get; set; }
    }


    public class SimpleType
    {

        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class SimpleDoubleString
    {

        public string Action { get; set; }
        public string Controller { get; set; }
    }
}