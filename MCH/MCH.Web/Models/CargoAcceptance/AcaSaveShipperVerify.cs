﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AcaSaveShipperVerify
    {
        public long AwbId { get; set; }
        public List<AcaShipperVerifyField> ShipperVeriryFields { get; set; }
    }

    public class AcaShipperVerifyField{
        public long FieldId {get;set;}
        public string Value{get;set;}
    }
}