﻿using MCH.BLL.Model.SpecialHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AcaAwbContactModel:WizardPage
    {
        public long AwbId { get; set; }
        public List<SHListItem> SpecialHandlingIds { get; set; }

        public List<DtoVerifyField> Fields { get; set; }
    }
}