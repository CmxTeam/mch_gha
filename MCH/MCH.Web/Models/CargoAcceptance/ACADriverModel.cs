﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class ACADriverModel:WizardPage
    {
        [Required]
        public long ? DriverId { get; set; }
        [Required]
        public long ? TruckingCompId { get; set; }
        public string STANumber { get; set; }
        public string DriverLicense { get; set; }
        public int? StateId { get; set; }
        [Required(ErrorMessage="Driver's Picture is required.")]
        public string CapturePhoto { get; set; }
        public string CaptureID { get; set; }
        [Required]
        public int? FirstIDTypeId { get; set; }
        public bool ? FirstIDMatched { get; set; }
        public int? SecondIDTypeId { get; set; }
        public bool ? SecondIDMatched { get; set; }
    }
}