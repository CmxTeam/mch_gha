﻿using MCH.BLL.Model.Enum;
using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AcceptCarrierShipper:WizardPage
    {
        [Required(ErrorMessage="Carrier is required.")]
        public int AccountCarrierId { get; set; }

         [Required(ErrorMessage = "Flight Type is required.")]
        public int FlightTypeId { get; set; }

        [Required(ErrorMessage = "Shipper Type is required.")]
        public int ShipperTypeId { get; set; }

        public int? ApprovedTSACarrierId { get; set; }

        public int? ApprovedIACId { get; set; }

        public int? ApprovedKnownShipperId { get; set; }

        [Required(ErrorMessage = "Shipper Name is required.")]
        public string ShipperName { get; set; }

        public bool ShipperVerified { get; set; }

        public int? DocumentTypeId { get; set; }

        public bool IsDocumentVerified { get; set; }

        public int? GovAuthIdVerified { get; set; }

        [Required(ErrorMessage="Signature is required.")]
        public string SignImageData { get; set; }

        [Required(ErrorMessage="At least one document shoule be attached.")]
        public List<AttachmentModel> Documents { get; set; }

        public int NeedsRestor { get; set; }
    }
}
