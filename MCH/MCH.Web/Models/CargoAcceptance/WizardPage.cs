﻿using MCH.BLL.Model.CargoAcceptance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class WizardPage
    {
        public long? ID { get; set; }
        public string StepName { get{
            switch (this.HeaderStatus)
            {
                case ShipperManifestStatus.ShipperInfoVerified:
                    return "2 : Driver";
                case ShipperManifestStatus.DriverInfoVerified:
                    return "3 : Airwaybill Details";
                case ShipperManifestStatus.AWBDetailsVerified:
                    return "3 : Airwaybill Details";
                case ShipperManifestStatus.AWBSpecialHandlingVerified:
                    return "3 : Airwaybill Details";
                case ShipperManifestStatus.AWBContactInfoVeriried:
                    return "3 : Airwaybill Details";
                case ShipperManifestStatus.AWBScreeningVerified:
                    return "3 : Airwaybill Details";
                case ShipperManifestStatus.AWBEntryCompleted:
                    return "3 : Airwaybill Details";
                case ShipperManifestStatus.DocsFinalized:
                    return "1 : Shipper";
                case ShipperManifestStatus.Canceled:
                    return "1 : Shipper";
                default:
                    return "1 : Shipper";
            }
        }}

        public ShipperManifestStatus HeaderStatus { get; set; }

        public int ShipperManifestStatusId { get; set; }

        public long ? CarrierId { get; set; }
        public string CarrierName { get; set; }
        public long ? ShipperId { get; set; }
        public string CarrierShipperName { get; set; }

        public int? HeaderFlightTypeId { get; set; }
        public string HeaderFlightTypeName { get; set; }

        public string ShipperTypeName { get; set; }

        public long? HeaderDriverId { get; set; }
        public string DriverName { get; set; }
        public string DriverPhotoData { get; set; }

        public int? TrackingCompanyId { get; set; }
        public string TrackingCompanyName { get; set; }
        public long? CurrentAWBId { get; set; }
    }
}