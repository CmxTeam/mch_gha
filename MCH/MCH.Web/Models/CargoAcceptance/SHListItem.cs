﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class SHListItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long? SphCodeId { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public bool IsChecked { get; set; }
        public List<SHListItem> SubGroups { get; set; }
    }
}