﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AcaAwbSaveHandlings
    {
        public long AwbId { get; set; }

        public string SphCodeIds { get; set; }
        public string SphGroupIds { get; set; }

        public List<AttachmentModel> Documents { get; set; }
    }
}