﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AcaAwbHandlingDocument
    {
        public int Id { get; set; }

        public int SPHId { get; set; }
        public string SPHCode { get; set; }

        public int DocumentTypeId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentName { get; set; }

    }
}