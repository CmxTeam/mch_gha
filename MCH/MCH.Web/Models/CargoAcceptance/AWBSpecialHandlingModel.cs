﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AWBSpecialHandlingModel:WizardPage
    {
        public long AwbId { get; set; }
        public List<SHListItem> SpecialHandlings { get; set; }
        public List<AttachmentModel> Documents { get; set; }
    }
}