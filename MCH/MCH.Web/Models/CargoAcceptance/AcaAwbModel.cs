﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AcaAwbModel:WizardPage
    {
        public List<AcaAwbDetialsModel> Awbs { get; set; }
        public long? CurrentAwbId { get; set; }
    }
}