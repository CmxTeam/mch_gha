﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AcaAwbDetialsModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage="Carrier Code is required.")]
        public string CarrierCode { get; set; }
        [Required(ErrorMessage = "Serial Number is required.")]
        public string AwbSerialNumber { get; set; }

        [Required(ErrorMessage = "Flight Number is required.")]
        public string FlightNumber { get; set; }

        [Required(ErrorMessage = "Flight Date is required.")]
        public DateTime ? FlightDate { get; set; }

        [Required(ErrorMessage = "Origin Airport is required.")]
        public long? OriginId { get; set; }

        [Required(ErrorMessage = "Destination Airport is required.")]
        public long? DestinationId { get; set; }

        [Required(ErrorMessage = "Piece count is required.")]
        public int? Pieces { get; set; }

        [Required(ErrorMessage = "Weight is required.")]
        public double? Weight { get; set; }
        public string WeightUom { get; set; }
        public long ? WeightUomId { get; set; }
        public int? FlightTypeId { get; set; }
        public bool ConnectedToShipper { get; set; }
    }
}