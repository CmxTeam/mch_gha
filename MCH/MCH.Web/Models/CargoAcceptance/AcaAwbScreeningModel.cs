﻿using MCH.BLL.Model.Screening;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.CargoAcceptance
{
    public class AcaAwbScreeningModel:WizardPage
    {
        public long AwbId { get; set; }

        public bool IsTenderedAsScreened { get; set; }

        public long? ApprovedCCSFId { get; set; }
        public List<SealNumber> SealNumbers { get; set; }

        public List<AlternativeSceeningOption> AltScreeningOptions { get; set; }

    }

    public class SealNumber {

        public int Id { get; set; }
        public int SealTypeId { get; set; }
        public string SealTypeName { get; set; }
        [Required(ErrorMessage="Seal Number is required.")]
        public string SealNumberValue { get; set; }
    }
}