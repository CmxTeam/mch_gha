﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MCH.Web.Models
{
    public class TasksGridRow
    {
        public int ? WarehouseId { get; set; }
        public long TaskId { get; set; }
        public long? EntityId { get; set; }
        public int? EntityTypeId { get; set; }
        public string TaskType { get; set; }
        public int ? TaskTypeId { get; set; }
        public int ? Alerts { get; set; }
        public string Indicators { get; set; }
        public bool ? Priority { get; set; }
        public string AirlineCode { get; set;}
        public string Reference { get; set; }
        public DateTime TaskDate { get; set; }
        public DateTime ? DueOn { get; set; }

        public String Status { get; set; }
        public double ? Progress { get; set; }
        public bool Reopened { get; set; }
        public string AssignedTo { get; set; }
        public DateTime ? Started { get; set; }
        public DateTime ? Ended { get; set; }
        public int ? ULDs { get; set; }
        public int ? Pieces { get; set; }
        public double ? Weight { get; set; }
        public string WeightUOM { get; set; }
        public string Department { get; set; }
        public string TaskLocation { get; set; }

        public int DepartmentId { get; set; }
    }
}