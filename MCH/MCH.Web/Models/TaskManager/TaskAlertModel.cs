﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.TaskManager
{
    public class TaskAlertModel
    {
        public long TaskId { get; set; }
        public string Message { get; set; }
        public string UserName { get; set; }
        public DateTime? AlertDate { get; set; }
    }
}