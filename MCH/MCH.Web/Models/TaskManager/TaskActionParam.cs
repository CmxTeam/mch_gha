﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.TaskManager
{
    public class TaskActionParam
    {
        public string TaskIds { get; set; }
    }
}