﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Flight
{
    public class FlightViewerModel
    {
        public long FlightId { get; set; }
        public string FlightNumber { get; set; }


        public string CarrierName { get; set; }
        public long ? CarrierId { get; set; }

        public long ? LoadingPortId { get; set; }
        public string LoadingPortName { get; set; }

        public long ? UnloadingPort { get; set; }
        public string UloadingPortName { get; set; }

        public DateTime ? ETD { get; set; }
        public DateTime ? ETA { get; set; }

        public string AircraftRegNumber { get; set; }

        public int ? TotalULDs { get; set; }
        public int ? TotalAWBs { get; set; }
        public int ? TotalPieces { get; set; }
        public double ? TotalWeight { get; set; }
        public string WeightUOM { get; set; }

        public TaskDetialsModel TaskDetails { get; set; }



        public long? TaskId { get; set; }
    }
}