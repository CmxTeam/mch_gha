﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Flight
{
    public class FlightLegRow
    {
        public int? Seq { get; set; }
        public string Carrier { get; set; }
        public string FlightNumber { get; set;}
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateTime ? ETD { get; set; }
        public DateTime ? ETA { get; set; }
    }


}