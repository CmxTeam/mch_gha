﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Flight
{
    public class FlightDetails
    {
        public long FlightId { get; set; }

        public long? CarrierId { get; set; }
        public long? OriginId { get; set; }
        public long? DestinationId { get; set; }

        public string FligthNumber { get; set; }

        public long? HandlingStationId { get; set; }
        public DateTime ? ETA { get; set; }

        public TimeSpan? ETATime { get; set; }

        public DateTime ?  ETD { get; set; }

        public TimeSpan? ETDTime { get; set; }
        public int? TotalULDs { get; set; }
        public int? TotalAWBs { get; set; }

        public int? TotalHAWBs { get; set; }
        public string TotalWeight { get; set; }
        public int? TotalPieces { get; set; }
        public string TotalVolume { get; set; }

        public DateTime? Cutoff { get; set; }

        public string Carrier { get; set; }

        public string Origin { get; set; }

        public string Destination { get; set; }

        public string HandlingStation { get; set; }
    }
}