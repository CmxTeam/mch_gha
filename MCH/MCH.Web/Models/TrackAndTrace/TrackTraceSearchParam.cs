﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.TrackAndTrace
{
    public class TrackTraceSearchParam
    {
        public enumTrackTraceSearchType SearchType { get; set; }
        public string Carrier { get; set; }
        public string AWB { get; set; }
        public string HWB { get; set; }
        public string AirlineCode { get; set; }
        public string FlightNumber { get; set; }
        public DateTime FlightDate { get; set; }
        public string OriginCode { get; set; }
        public string DestinationCode { get; set; }
        public string ULD { get; set; }
    }
}