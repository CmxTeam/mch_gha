﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models
{
    
    public class TrackGridRow
    {
        public long? EntityId { get; set; }
        public int? EntityTypeId { get; set; }
        public string AirlineCode { get; set; }
        
        public string Carrier { get; set; }
        public string AWB { get; set; }
        public string HWB { get; set; }
        public string Origin { get; set; }
        public string FlightNumber { get; set; }
        public DateTime ? FlightDate { get; set; }
        public string Destination { get; set; }
        public string ULD { get; set; }
        public DateTime ? ETD { get; set; }
        public DateTime ? ETA { get; set; }
        public string Description { get; set; }
        public int ? Pieces { get; set; }
        public double ? Weight { get; set; }
        public string WeightUOM { get; set; }

        public string Location { get; set; }
        public string Status { get; set; }
        public bool? IsBUP { get; set; }
        public bool? IsTrasfer { get; set; }
        public int? TotalAWBs { get; set; }
        public int ? TotalULDs { get; set; }


        public DateTime? StatusTime { get; set; }
    }

}