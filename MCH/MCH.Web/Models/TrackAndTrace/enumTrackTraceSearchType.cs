﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.TrackAndTrace
{
    public enum enumTrackTraceSearchType:int
    {
        AWB = 1, 
        HWB = 2,
        Flight = 3, 
        ULD = 4
    }
}