﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Common
{
    public class TaskGridRow
    {
        public long Id { get; set; }

        public string Status { get; set; }

        public string Progress { get; set; }

        public String Reference { get; set; }

        public string AssignedTo { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }

        public int Total { get; set; }
    }
}