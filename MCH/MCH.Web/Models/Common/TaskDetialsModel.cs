﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Common
{
    public class TaskDetialsModel
    {
        public string TypeName { get; set; }
        public long ? TypeId { get; set; }

        public DateTime ? StartedAt { get; set; }
        public DateTime ? EndedAt { get; set; }

        public string Location { get; set; }
        public string Status { get; set; }
        public DateTime?  LastUpdated { get; set; }
        public double ? Progress { get; set; }

        public List<TaskAssigneModel> AssignedTo { get; set; }
    }

    public class TaskAssigneModel {
        public long UserId {get;set;}
        public string Username {get;set;}
    }
}