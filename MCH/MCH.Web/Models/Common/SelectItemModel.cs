﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Common
{
    public class SelectItem
    {
        public long? Id { get; set; }
        public string Name { get; set; }
    }

    public class DropDownModel
    {
        public long Id { get; set;}
        public string Name { get; set; }
    }

    public class SelectComboItem
    {
        public long Value { get; set; }
        public string Text { get; set; }
        public long DefaultIndex { get; set; }
    }
}