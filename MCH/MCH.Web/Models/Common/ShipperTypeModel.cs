﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Common
{
    public class ShipperTypeModel
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public long GroupId { get; set; }
    }
}