﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Common
{
    public class MenuItemModel
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string NavigationLink { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
    }
}