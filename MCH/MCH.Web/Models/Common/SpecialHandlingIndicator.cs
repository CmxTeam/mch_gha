﻿using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Common
{
    public class SpecialHandlingIndicator
    {
        public SpecialHandlingIndicator()
        {

        }

        public SpecialHandlingIndicator(SpecialHandlingCode  model)
        {

        }

        public string Code { get; set; }

        public bool? IsDryIce { get; set; }

        public bool? IsExpedited { get; set; }

        public bool? IsGovernment { get; set; }

        public bool? IsHazmatCode { get; set; }

        public bool? IsPerishable { get; set; }

        public bool? IsHuman { get; set; }

        public bool? IsTempControlled { get; set; }

        public bool? IsHighValue { get; set; }

        public bool? IsLiveAnimal { get; set; }

        public SpecialHandlingCode Data { get; set; }
    }
}