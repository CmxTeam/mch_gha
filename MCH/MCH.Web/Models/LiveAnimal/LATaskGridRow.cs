﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.LiveAnimal
{
    public class LATaskGridRow:TaskGridRow
    {

        public int HasAlert { get; set; }

        public int HasCommnets { get; set; }

        public byte ? IsReopened { get; set; }

        public DateTime ? Cutoff { get; set; }

        public string FlightNumber { get; set; }

        public String Customer { get; set; }

        public DateTime ?  ETD { get; set; }
    }
}