﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class CustomsHistoryFilter:DataTableFilter
    {
        public long? TaskId { get; set; }
        public long ShipmentId { get; set; }
        public long WayBillId { get; set; }
    }
}