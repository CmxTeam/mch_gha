﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class NotocDetailsModel
    {
        public long TaskId { get; set; }

        public long FlightId { get; set; }

        public string Carrier { get; set; }

        public string FlightNumber { get; set; }

        public DateTime? FlightDate { get; set; }

        public string StationOfLoading { get; set; }

        public string Origin { get; set; }

        public string Destination { get; set; }

        public string AircraftRegNumber { get; set; }

        public string LoadingSupervisor { get; set; }

        public string OtherInformation { get; set; }

        public string PreparedBy { get; set; }

        public string CheckedBy { get; set; }

        public long NotocId { get; set; }
    }
}