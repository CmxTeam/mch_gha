﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class CustomsDetailsViewModel
    {
        public long TaskId { get; set; }
        public long ShipmentId { get; set; }
        public string Part { get; set; }

        public int WayBillId { get; set; }

        public string CarrierFlight { get; set; }

        public string AirrivalAirport { get; set; }

        public string ArrivalDate { get; set; }

        public string AirportTerminal { get; set; }

        public string Status { get; set; }

        public long QTY { get; set; }

        public string EntityType { get; set; }

        public string EntityNo { get; set; }
    }
}