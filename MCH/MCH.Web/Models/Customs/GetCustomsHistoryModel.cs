﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class GetCustomsHistoryModel
    {
        public long TaskId { get; set; }
        public long ShipmentId { get; set; }
    }
}