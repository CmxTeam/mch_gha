﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Customs
{
    public class GetNotocViewModel
    {
        public long TaskId { get; set; }

        public long FlightId { get; set; }
    }
}