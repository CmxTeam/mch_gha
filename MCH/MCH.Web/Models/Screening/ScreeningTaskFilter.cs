﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Screening
{
    public class ScreeningTaskFilter:DataTableFilter
    {
        public long? TaskId { get; set; }
    }
}