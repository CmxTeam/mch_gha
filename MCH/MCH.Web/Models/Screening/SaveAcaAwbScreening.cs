﻿using MCH.Web.Models.CargoAcceptance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Screening
{
    public class SaveAcaAwbScreening
    {
        public long AwbId { get; set; }
        public long ? ApprovedCCSFId { get; set; }
        public bool IsTendered { get; set; }
        public List<SealNumber> SelaNumbers { get; set; }
        public List<SaveAltScreeningOptions> AlernativeScreeningOptions { get; set; }
    }

    public class SaveAltScreeningOptions{
        public long Id {get;set;}
        public bool Checked {get;set;}
    }
}