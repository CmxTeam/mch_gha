﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Screening
{
    public class SelectedScreenings
    {

         public long ? TaskId {get;set;}
         public string ScreeningIdsString { get; set; }

         public string Comment { get; set; }
    }
}