﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Awbs
{
    public class AcceptAwbDetailsModel
    {
        public long? AwbId { get; set; }
        public long? TaskId { get; set; }

        public long? ParentTaskId { get; set; }

        public int OriginId { get; set; }
        public int DestinationId { get; set; }
        public string FlightNumber { get; set; }
        public string SerialNumber { get; set; }

        public string DestinationPort { get; set; }

        public string CarrierName { get; set; }

        public string CarrierCode { get; set; }

        public DateTime? ETD { get; set; }

        public string OriginPort { get; set; }

        public int? TotalPieces { get; set; }

        public double? TotalVolume { get; set; }

        public double? TotalWeight { get; set; }

        public string VolumeUOM { get; set; }

        public string WeightUOM { get; set; }
    }
}