﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Awbs
{
    public class FlightAwbRow
    {
        public long AwbId { get; set; }
        public string AwbNumber { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Description { get; set; }
        public int Pieces { get; set; }
        public int SLAC { get; set; }
        public string WeightWithUOM { get; set; }
        public string SpecialHandling { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public DateTime StatusTimestamp { get; set; }
        public string ControlNumber { get; set; }
    }
}