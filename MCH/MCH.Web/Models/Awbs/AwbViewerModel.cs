﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Awbs
{
    public class AwbViewerModel
    {
        public long AwbId { get; set; }
        public long FlightId { get; set; }
        public string AwbNumber { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Description { get; set; }
        public string SpecialHandling { get; set; }
        public string ExportControlNumber { get; set; }
        public string ImportControlNumber {get;set;}
        public string CurrentLocation { get; set; }
        public string Shipper { get; set; }
        public string Consignee { get; set; }
        public int? TotalUlds { get; set; }
        public int? TotalHwbs { get; set; }
        public int? TotalSlac { get; set; }
        public int? TotalPieces { get; set; }
        public string TotalWeight { get; set; }

        public TaskDetialsModel TaskDetails { get; set; }
        public long? TaskId { get; set; }
    }
}