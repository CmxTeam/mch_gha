﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Awbs
{
    public class AwbsListSimpleFilter
    {
        public long ? CarrierId { get; set; }
        public long ? DestinationId { get; set; }
        public long ? OriginId { get; set; }

        public long? FlightId { get; set; }

        public string Term { get; set; }

        public long? Id { get; set; }

    }
}