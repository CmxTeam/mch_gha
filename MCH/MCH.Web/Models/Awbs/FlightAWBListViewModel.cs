﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Awbs
{
    public class FlightAWBListViewModel
    {
        public long FlightId { get; set; }
        public long? TaskId { get; set; }
    }
}