﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class TaskListRow
    {
        public long Id { get; set; }
        public long SecNumber { get; set; }
        public bool HasHazmat { get; set; }
        public string Alert { get; set; }
        public double Progress { get; set; }
        public string Refernece { get; set; }
        public string AssignedTo { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }

    }

    
}