﻿using MCH.Web.Models.Flight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class TaskDetailsCR
    {
        public long TaskId { get; set; }

        public long ? StationId { get; set; }

        public int ? PriorityId { get; set; }

        public int ? StatusId { get; set; }

        public string Reference { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? Completed { get; set; }
        public double ? Progress { get; set; }

        public FlightDetails Flight { get; set; }

        public IList<TaskAssignment> TaskAssignments { get; set; }


        public string Station { get; set; }

        public string Status { get; set; }

        public string Priority { get; set; }
    }


}