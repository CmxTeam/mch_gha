﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class TaskDetailsShipments : TabContentViewModel
    {
        public long TaskId { get; set; }

        public bool ShowCustomsHistory { get; set; }
    }
}