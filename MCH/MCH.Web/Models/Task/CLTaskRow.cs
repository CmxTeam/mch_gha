﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class CLTaskRow:TaskListRow
    {
        public DateTime Cutoff { get; set; }
        public string FlightNumber{get;set;}
        public DateTime ETD { get; set; }
    }
}