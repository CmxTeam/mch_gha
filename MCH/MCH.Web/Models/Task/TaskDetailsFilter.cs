﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class TaskDetailsFilter:DataTableFilter
    {
        public long TaskId { get; set; }
    }
}