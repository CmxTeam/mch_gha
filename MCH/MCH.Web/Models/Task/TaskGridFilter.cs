﻿using MCH.BLL.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class TaskGridFilter
    {
        public int TaskTypeId { get; set; }
        public int ? StatusId { get; set; }

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }

        public int Airline { get; set; }

        public string FlightNumber { get; set; }

        public long? WarehouseId { get; set; }


    }
}