﻿using MCH.BLL.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class SearchTaskViewModel
    {
        public enumTaskTypes TaskTypeId { get; set; }

        public enumTaskStatuses ? StatusId { get; set; }

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }

        public long WarehousePortId { get; set; }
    }
}