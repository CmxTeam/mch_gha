﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class DynamicTaskRow
    {


        public long Id { get; set; }

        public DateTime TaskDate { get; set; }

        public DateTime? DueDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Assignees { get; set; }

        public string Status { get; set; }

        public double? Progress{ get; set; }

        public bool IsReopened { get; set; }

        public string OriginAirport { get; set; }

        public string Station { get; set; }

        public string Warehouse { get; set; }

        public string WarehouseLocation { get; set; }

        public string  DestinationAirport { get; set; }

        public string Airline { get; set; }

        public string FlightNumber { get; set; }

        public DateTime? ETA { get; set; }

        public DateTime? ETD { get; set; }

        public bool? PAXorCAO { get; set; }

        public string Driver { get; set; }

        public string ScreeningResult { get; set; }

        public string Alerts { get; set; }

        public int? SPHCodes { get; set; }

        public string Reference { get; set; }
    }
}