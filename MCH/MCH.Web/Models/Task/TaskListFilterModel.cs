﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Task
{
    public class TaskListFilter : DataTableFilter
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }

        public int? TaskTypeId { get; set; }
        public int? TaskStatusId { get; set; }

        public long? WarehouseId { get; set; }

    }

    public class CargoLoaderTaskListFilter : TaskListFilter
    {
        public long? OriginId { get; set; }
        public long? DestinationId { get; set; }
        public int? CarrierId { get; set; }
        public long? AssignedUserId { get; set; }

        public string FlightNumber { get; set; }
    }

    public class ChecklistTaskListfilter : TaskListFilter 
    {
        public long? OriginId { get; set; }
        public long? DestinationId { get; set; }
        public int? CarrierId { get; set; }
        public long? AssignedUserId { get; set; }

        public string FlightNumber { get; set; }
    }

    public class CargoScreeningTaskListfilter : TaskListFilter
    {
        public long? OriginId { get; set; }
        public long? DestinationId { get; set; }
        public int? CarrierId { get; set; }
        public long? AssignedUserId { get; set; }

        public string FlightNumber { get; set; }
    }

    public class CargoAcceptTaskListFilter : TaskListFilter 
    {
        public long? OriginId { get; set; }
        public long? DestinationId { get; set; }
        public int? CarrierId { get; set; }
        public long? AssignedUserId { get; set; }

        public string FlightNumber { get; set; }
    }

}