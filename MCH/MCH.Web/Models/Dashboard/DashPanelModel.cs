﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Dashboard
{
    public class DashPanelModel
    {
        public DateTime FromDate { get; set;}
        public DateTime ToDate { get; set; }
        public long StationId { get; set; }
    }
}