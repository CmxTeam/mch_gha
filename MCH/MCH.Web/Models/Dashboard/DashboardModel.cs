﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Dashboard
{
    public class DashboardModel
    {
        public IEnumerable<Common.SelectComboItem> TaskTypes { get; set; }

        public IEnumerable<Common.SelectComboItem> Warehouses { get; set; }
    }
}