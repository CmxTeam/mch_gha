﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Hwbs
{
    public class FlightHwbListViewModel
    {
        public long EntityId { get; set; }
        public long? TaskId { get; set; }
    }
}