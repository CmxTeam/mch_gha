﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Hwbs
{
    public class HwbViewerModel
    {
        public long? HwbId { get; set; }
        public long? FlightId { get; set; }
        public string HwbNumber { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Description { get; set; }
        public string SpecialHandling { get; set; }
        public string ExportControlNumber {get;set;}
        public string ImportControlNumber { get; set; }
        public string CurrentLocation { get; set; }
        public string GoodsDesc { get; set; }
        
        public long AwbId { get; set; }
        public string AWBNumber { get; set; }

        public int? TotalSlac { get; set; }
        public int? TotalPieces { get; set; }
        public double ? TotalWeight { get; set; }
        public string WeightUOM { get; set; }
        public TaskDetialsModel TaskDetails { get; set; }
    }
}