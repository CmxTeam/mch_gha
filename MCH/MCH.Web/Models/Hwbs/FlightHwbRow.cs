﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Hwbs
{
    public class FlightHwbRow
    {
        public long HwbId { get; set; }
        public string HwbNumber { get; set; }
        public string AwbNumber { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Description { get; set;}
        public int Pieces {get;set;}
        public int SLAC {get;set;}
        public double Weight {get;set;}
        public string WeightUOM {get;set;}
        public string SpecialHandling {get;set;}
        public string ControlNumber {get;set;}
        public string Location {get;set;}
        public string Status {get;set;}
        public DateTime ? StatusTimestamp {get;set;}
    }

    public class AwbHwbsRow
    {

        public string Consignee { get; set; }

        public string DescofGoods { get; set; }

        public string Destination { get; set; }

        public string ExportControl { get; set; }

        public string HwbNumber { get; set; }

        public string ImportControl { get; set; }

        public string Origin { get; set; }

        public int? Pieces { get; set; }

        public string Shipper { get; set; }

        public int? Slac { get; set; }

        public string TotalWeight { get; set; }
    }
}