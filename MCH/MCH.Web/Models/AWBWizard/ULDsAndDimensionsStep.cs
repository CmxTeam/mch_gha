﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Models.AWBWizard
{
    public class ULDsAndDimensionsStep
    {
        public long? AirwaybillId { get; set; }

        public long? VolumeUOMId { get; set; }
        public double? Volume { get; set; }

        public int? SLAC { get; set; }
        public List<Dimentions> GoodsDescriptionList { get; set; }
        public List<UnitLoadDevice> UnitLoadDevicesList { get; set; }
    }

    public class Dimentions
    {
        public long? Id { get; set; }

        public long? IdToDB { get; set; }
        [Required(ErrorMessage = "Required.")]
        public int? Pieces { get; set; }
        [Required(ErrorMessage = "Required.")]

        //public double? Weight { get; set; }
        //[Required(ErrorMessage = "Required.")]
        //public string WeightUOMId { get; set; }
        //public string WeightUOM { get; set; }
        //[Required(ErrorMessage = "Required.")]

        public double? Length { get; set; }
        [Required(ErrorMessage = "Required.")]
        public double? Width { get; set; }
        [Required(ErrorMessage = "Required.")]
        public double? Height { get; set; }
        [Required(ErrorMessage = "Required.")]
        public string DimUOMId { get; set; }
        public string DimUOM { get; set; }
    }

    public class UnitLoadDevice
    {
        public long? Id { get; set; }
        public long? IdToDB { get; set; }

        [StringLength(5, ErrorMessage = "Max 5 chars")]
        public string ULDNo { get; set; }
        [Required(ErrorMessage = "Required.")]
        public string ULDTypeId { get; set; }
        public string ULDType { get; set; }
        [Required(ErrorMessage = "Required.")]
        public string OwnerId { get; set; } // CarrierCode
        public string Owner { get; set; }
        [Required(ErrorMessage = "Required.")]
        public long? Said { get; set; }
    }

}