﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Models.AWBWizard
{
    public class SummaryStep
    { 
        public long? AirwaybillId { get; set; }

        public long? AirwaybillTypeId { get; set; }

        public int? ParticipantIdentfierId { get; set; }

        public string ParticipantCode { get; set; }

        public long? AirportId { get; set; }

        public string CertificationName { get; set; }
        public string ExecutedAtPlace { get; set; }
        public DateTime ExecutedOnDate { get; set; }
        public string ExecutedBy { get; set; }
    }
}