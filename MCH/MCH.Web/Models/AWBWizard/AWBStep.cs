﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.Models.Shared;
using System.ComponentModel.DataAnnotations;
using MCH.Core.Models.Common;

namespace MCH.Web.Models.AWBWizard
{
    public class AWBMainPopup
    {
        public long? AirwaybillId { get; set; }

        public AWBWizardSteps Step { get; set; }
    }
    public class AWBStep
    {
        public long? AirwaybillId { get; set; }

        public int? AirwaybillTypeId { get; set; }

        public string AirwaybillSerialNumber { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

         
        public int? AircraftType { get; set; }

        [Range(0, long.MaxValue, ErrorMessage = "Airline is invalid")]
        public long? CarrierId { get; set; }

      //  public long? NumberOfHWBs { get; set; }

        public List<Flight> Flights { get; set; }

     
        public Partner Shipper { get; set; }
        public Partner Consignee { get; set; }
        public Agent IssuingCarriersAgent { get; set; }

        public string AccountInformationText { get; set; }

        public string FileReference { get; set; }

        public int? ParticipantIdentfierId { get; set; }

        public string ParticipantCode { get; set; }

        public long? AirportId { get; set; }

        // To remove
        //public List<Code> AccountInformation { get; set; }

        //public string GoodsDescription { get; set; }

        //public string SpecialServiceRequests { get; set; }

        //public string OtherServiceRequests { get; set; }
    }

   
    public class Flight
    {
        public long Id { get; set; }


        [Required(ErrorMessage = "Required")]
        public string FlightFromId { get; set; }


        [Required(ErrorMessage = "Required")]
        public string FlightToId { get; set; }


        [Required(ErrorMessage = "Required")]
        public string FlightCarrierId { get; set; }


        [Required(ErrorMessage = "Required")]
        [StringLength(5, ErrorMessage = "Max 5 chars")]
        public string FlightNo { get; set; }

        [Required(ErrorMessage = "Required")]
        public DateTime? FlightDate { get; set; }
    }


    public enum AWBWizardSteps
    {
        AWBStep = 1,
        ChargesStep,
        //ULDStep,
        //SummaryStep,
        HWBStep
    }

    public enum AWBTypes
    {
        Airwaybill = 0,
        SimpleAirwaybill
    }

}
