﻿using MCH.BLL.Model.AwbWizard;
using MCH.BLL.Model.Customs;
using MCH.BLL.Model.SpecialHandling;
using MCH.Core.Models.Common;
using MCH.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Models.AWBWizard
{

    public class HWBStep
    {
        public long? AirwaybillId { get; set; }
    }
    public class HWBAddEdit
    { 
        public long? Id { get; set; }

        public long? AirwaybillId { get; set; }

        public string AWBNumber { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

        public string HWBSerialNumber { get; set; }


        public long? TotalPieces { get; set; }
        public decimal? TotalWeight { get; set; }
        public long? UOMWeightId { get; set; }
        public long? SLAC { get; set; }

        public string DescriptionOfGoods { get; set; }


        public List<CustomInfo> OtherCustomsInfo { get; set; }
        public string ExportControlNo { get; set; }
        public Partner Shipper { get; set; }
        public Partner Consignee { get; set; }
        public List<Code> HarmonizedCommodityCodes { get; set; }

      

        public ChargeDeclarations ChargeDeclarations { get; set; }

        public AddEditHwbModel ToBackendModel() 
        {
            AddEditHwbModel tmpModel = new AddEditHwbModel();

            tmpModel.AirwaybillId = this.AirwaybillId;
            tmpModel.AWBNumber = this.AWBNumber;
            tmpModel.ChargeDeclarations = this.ChargeDeclarations != null ? this.ChargeDeclarations.ToBackendModel() : null;
            tmpModel.Consignee = this.Consignee != null ? this.Consignee.ToBackendModel() : null;
            tmpModel.DescriptionOfGoods = this.DescriptionOfGoods;
            tmpModel.DestinationId = this.DestinationId;
            tmpModel.ExportControlNo = this.ExportControlNo;
            tmpModel.HarmonizedCommodityCodes = this.HarmonizedCommodityCodes!=null && this.HarmonizedCommodityCodes.Any() ?
                this.HarmonizedCommodityCodes.Select(item=> new SphCode
                {
                    CodeId = item.CodeId, CodeNote = item.CodeNote, Id = item.Id
                }).ToList() : null ;

            tmpModel.HWBSerialNumber = this.HWBSerialNumber;
            tmpModel.Id = this.Id;
            tmpModel.OriginId = this.OriginId;
            tmpModel.OtherCustomsInfo = this.OtherCustomsInfo != null && this.OtherCustomsInfo.Any() ? this.OtherCustomsInfo.Select(item=> new CustomsInfo{
                CountryCode = item.CountryCode, CountryCodeId = item.CountryCodeId, CustomsInfoIdentifier = item.CustomsInfoIdentifier, CustomsInfoIdentifierId = item.CustomsInfoIdentifierId, Id = item.IdToDB,
                IdToDB = item.IdToDB, InfoIdentifier = item.InfoIdentifier, InfoIdentifierId = item.InfoIdentifierId, SupplementaryCustomsInfo = item.SupplementaryCustomsInfo
            }).ToList() : null;
            tmpModel.Shipper = this.Shipper.ToBackendModel();
            tmpModel.SLAC = this.SLAC;
            // TODO: Aram to fix
            //tmpModel.SpecialHandlingRequests = this.SpecialHandlingRequests != null && this.SpecialHandlingRequests.Any() ? this.SpecialHandlingRequests.Select(item => new SphCode {
            //    CodeId = item.CodeId, Id = item.Id, CodeNote = item.CodeNote
            //}).ToList() : null;
            tmpModel.TotalPieces = this.TotalPieces;
            tmpModel.TotalWeight = this.TotalWeight;
            tmpModel.UOMWeightId = this.UOMWeightId;

            return tmpModel;
        }
    }

    public class HWBSimple
    {
        public long Id { get; set; }

        public string Origin { get; set; }

        public string Destination { get; set; }

        public string Number { get; set; }


        public string Pieces { get; set; }
        public string Weight { get; set; }
        public string SLAC { get; set; }
        public string Shipper { get; set; }
        public string Consignee { get; set; }

       // public string SpecialHandlingCount { get; set; }

        public string ExportControl { get; set; }
    }


    public class CustomInfo
    {
        public long? Id { get; set; }
        public long? IdToDB { get; set; }
         public string CountryCodeId { get; set; }

        public string InfoIdentifierId { get; set; }

        public string CustomsInfoIdentifierId { get; set; }


        public string CountryCode { get; set; }

        public string InfoIdentifier { get; set; }

        public string CustomsInfoIdentifier { get; set; }

         [Required(ErrorMessage = "Required")]
        public string SupplementaryCustomsInfo { get; set; }
    }


    //public class AgeAttribute : ValidationAttribute, IClientValidatable
    //{
    //    private readonly int _MinAge = 0;
    //    private readonly int _MaxAge = 0;
    //    private const string errorMsg = "{0} must at least {1} or not more than {2}";

    //    public AgeAttribute(int MinAge, int MaxAge)
    //        : base(() => errorMsg)
    //    {
    //        _MinAge = MinAge;
    //        _MaxAge = MaxAge;
    //    }

    //    //Server-Side Validation
    //    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    //    {
    //        return new ValidationResult("katlet");
    //        if (value != null)
    //        {
    //            int data = (int)value;
    //            if (!(data > (int)_MinAge && data < (int)_MaxAge))
    //            {
    //                return new ValidationResult("sxtor");
    //            }
    //        }
    //        return ValidationResult.Success;
    //    }

    //    public IEnumerable<ModelClientValidationRule> GetClientValidationRules(
    //   ModelMetadata metadata, ControllerContext context)
    //    {
    //        var rule = new ModelClientValidationRule();
    //        rule.ErrorMessage = FormatErrorMessage("sox");
    //      //  rule.ValidationParameters.Add("wordcount", WordCount);
    //        rule.ValidationType = "age";
    //        yield return rule;
    //    }
    //}

}