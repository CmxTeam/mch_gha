﻿using MCH.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Models.AWBWizard
{
    public class ChargesStep
    {
        public long? AirwaybillId { get; set; }
        public ChargeDeclarations ChargeDeclarations { get; set; }
        public SimpleChargeTypeValue TotalWeightCharges { get; set; }
        public SimpleChargeTypeValue TotalValuationCharges { get; set; }
        public SimpleChargeTypeValue TotalTaxes { get; set; }
        public SimpleChargeTypeValue TotalOtherChargesDueCarrier { get; set; }
        public SimpleChargeTypeValue TotalOtherChargesDueAgent { get; set; }
        public SimpleChargeTypeValue Total { get; set; }

        public List<SimpleCharge> OtherChargesList { get; set; }

        public List<RateDescription> RateDescriptionsList { get; set; }


        // New
        public string HandlingInformation { get; set; }
        public List<Code> SpecialHandlingRequests { get; set; }

        public long? AirwaybillTypeId { get; set; }
        public string CertificationName { get; set; }
        public string ExecutedAtPlace { get; set; }
        public DateTime ExecutedOnDate { get; set; }
        public string ExecutedBy { get; set; }


    }

    public class SimpleCharge
    {

        public long? IdToDB { get; set; }
        public long? Id { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string CodeId { get; set; }

        public string Code { get; set; }

        public bool IsCodeDueCarrier { get; set; }

        [Required(ErrorMessage = "Required.")]
        [Range(0.01, 999999999999, ErrorMessage = "Required.")]
        public decimal? Amount { get; set; }

    }

    public class SimpleChargeTypeValue
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public decimal? Prepaid { get; set; }
        public decimal? Collect { get; set; }
    }

    public class RateDescription
    {
        public long? Id { get; set; }

        public long? IdToDB { get; set; }

        public long? NoOfPieces { get; set; }

        public double? GrossWeight { get; set; }


        public long UOMWeightId { get; set; }

        public string UOMWeight { get; set; }

        public string RateClass { get; set; }

        public string CommodityItemNo { get; set; }
        public double? ChargeableWeight { get; set; }

        public Decimal? RateCharge { get; set; }

        public decimal? Total { get; set; }


        //new
        public string GoodsDescription { get; set; }
        //public long? VolumeUOMId { get; set; }
        //public double? Volume { get; set; }

        //public int? STC { get; set; }
        //public List<Dimentions> Dimensions { get; set; }
        //public string ULD { get; set; }

        //public RateDescription()
        //{
        //    Dimensions = new List<Dimentions>();
        //}
    }

}