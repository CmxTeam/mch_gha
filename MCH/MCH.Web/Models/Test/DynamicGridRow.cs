﻿using CMX.Framework.Web.MVC.Attributes.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Test
{
    public class DynamicGridRow
    {
        [Hidden]
        public long Id { get; set; }
        [Filterable]
        public string Name { get; set; }
        [Filterable]
        public string FirstName { get; set; }
        [Filterable]
        public string LastName { get; set; }

        [Filterable]
        [Format(Format = "{0:MM/dd/yyyy HH:mm}")]
        public DateTime CreateDate { get; set; }
    }
}