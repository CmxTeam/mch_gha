﻿using MCH.BLL.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Manifest
{
    public class EntityManifestsViewModel
    {
        public long EntityId { get; set; }
        public enumEntityTypes EntityTypeId { get; set; }
        public long? TaskId { get; set; }
    }
}