﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Manifest
{
    public class AwbManifestsViewModel
    {
        public long AwbId { get; set; }
        public long? TaskId { get; set; }
    }
}