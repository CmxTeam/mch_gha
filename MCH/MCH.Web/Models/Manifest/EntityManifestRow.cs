﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Manifest
{
    public class EntityManifestRow
    {
        public long ? Id  { get; set; }
        public string ULDNumber { get; set; }
        public string AwbNumber { get; set; }
        public int ? TotalPieces { get; set; }
        public string Part { get; set; }
        public int ? Pieces { get; set; }
        public string Origin { get; set; }
        public string Transfer { get; set; }
        public string Destination { get; set; }
        public double ? Weight { get; set; }
        public string WeightUOM { get; set; }
        public string SpecialHandling { get; set; }
        public string ControlNumber { get; set; }
    }
}