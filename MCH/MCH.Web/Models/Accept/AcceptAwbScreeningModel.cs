﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Accept
{
    public class AcceptAwbScreeningModel
    {
        public string FormId { get; set; }

        public long ? TaskId { get;set;}
        public long? ParentTaskId { get; set; }
        public long? AwbId { get; set; }

        public bool Screened { get; set; }
        public long CSSFId { get; set; }
        public int SealType { get; set; }
        public string SealNumber { get; set; }
        public int ScreeingMethodId { get; set; }
        public string Remarks { get; set; }
    }
}