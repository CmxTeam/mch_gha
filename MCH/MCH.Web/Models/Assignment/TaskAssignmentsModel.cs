﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Assignment
{
    public class TaskAssignmentsModel
    {
        public long TaskId { get; set; }
        public string AssignmentsIdsString { get; set; }
    }
}