﻿using MCH.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.Models.Alert
{
    public class TaskAlertsListModel
    {
        public long TaskId { get; set; }
        public long? FlightId { get; set; }
    }

    public class TaskAlertListFliter : DataTableFilter 
    {
        public long TaskId { get; set; }
        public long? FlightId { get; set; }
    }
}