﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.Models;
using System.Web.Routing;
using MCH.Web.Models.Account;
using MCH.Web.Utilities;

namespace MCH.Web.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult SessionExpired() 
        {
            return View();
        }

        //
        // GET: /Account/Login
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DoLogin(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
               
            }

            // if we got this far, return to the same page, there are model errors to show.
            return View("Login",model);

        }


        public ActionResult Logoff()
        {
            //this.AuthenticationService.SignOut();
            return RedirectToAction("Login", "Account");
        }


    }
}