﻿using Kendo.Mvc.UI;
using MCH.BLL.Units;
using MCH.Web.Models.AWBWizard;
using MCH.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using System.Web.UI;
using MCH.BLL.Model.Customer;
using MCH.Core.Models.Common;

namespace MCH.Web.Controllers
{
    public class PartnerController : BaseController
    {
        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetParticipantIdentifiers()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCodes = unitOfWork.GetParticipantIdentifiers().Select(item => new { Name = item.Code, Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpCodes.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetCountries()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCodes = unitOfWork.GetCountryCodes().Select(item => new { Name = item.Code, Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpCodes.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "countryId")]
        public JsonResult GetStateCodes(long? countryId)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCodes = unitOfWork.GetStateCodes(countryId).Select(item => new { Name = item.StateProvince, Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpCodes.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerList()
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpUoms = commonUnit.GetCustomerList().Select(item => new { Name = item.SearchText, Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpUoms.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomersGrid([DataSourceRequest] DataSourceRequest request)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpQuery = commonUnit.Context.Customers.Select(item => new PartnerSimple
                {
                    Id = item.Id,
                    City = item.City,
                    Country = item.Country.TwoCharacterCode,
                    Name = item.Name,
                    AccountNumber = item.AccountNumber
                });


                return Json(tmpQuery.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetAgentList()
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpUoms = commonUnit.GetAgentList().Select(item => new { Name = item.SearchText, Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpUoms.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult AddressBook(PartnerType type)
        {
            return PartialView("_AddressBook", type);
        }

        public PartialViewResult GetAgentById(long id)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var agent = unitOfWork.GetAgentById(id);
                Agent item = new Agent
                {
                    Id = agent.Id,
                    Name = agent.Name,
                    AccountNumber = agent.AccountNumber,
                    City = agent.City,
                    IATACode = agent.IataCode,
                    CASSAddress = agent.CassAddress,
                    ParticipantIdentfierId = agent.ParticipantIdentfierId
                };

                return PartialView("_AgentEditor", item);
            }
        }

        public PartialViewResult GetCustomerById(int? id)
        {
            Partner item;

            if (id.HasValue)
            {

                using (CommonUnit unitOfWork = new CommonUnit())
                {
                    var customer = unitOfWork.GetCustomerById(id.Value);
                    item = new Partner
                    {
                        Id = customer.Id.Value,
                        AccountNumber = customer.AccountNumber,
                        Address1 = customer.Address1,
                        Name = customer.Name,
                        City = customer.City,
                        StateId = customer.StateId,
                        Postal = customer.Postal,
                        CountryId = customer.CountryId,
                        Phone = customer.Phone,
                        Fax = customer.Fax,
                        Email = customer.Email,
                        CountryText = customer.CountryText,
                        StateText = customer.StateText
                        //   Type = (PartnerType)Enum.Parse(typeof(PartnerType), type)
                    };


                }
            }
            else
            {
                item = new Partner() { Id=0};
            }
            return PartialView("_PartnerEditor", item);
        }

        public JsonResult SaveCustomer(Partner partner)
        {
            try
            {
                CustomerBo  dbModel = new CustomerBo
                {
                    Id = partner.Id,
                    AccountNumber = partner.AccountNumber,
                    Address1 = partner.Address1,
                    Name = partner.Name,
                    City = partner.City,
                    StateId = partner.StateId,
                    Postal = partner.Postal,
                    CountryId = partner.CountryId,
                    Phone = partner.Phone,
                    Fax = partner.Fax,
                    Email = partner.Email
                };

                using (CommonUnit unitOfWork = new CommonUnit())
                {
                    unitOfWork.CustomersRepository.SaveCustomer(dbModel);
                }
                return Json(new { data = "Ok" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)// some error
            {
                return Json(new { data = "Error", ErrorMessage = "Server Error" }, JsonRequestBehavior.AllowGet);

            }

        }

        public JsonResult GetCustomerDataById(int id)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var customer = unitOfWork.GetCustomerById(id);
                Partner item = new Partner
                {
                    Id = customer.Id,
                    AccountNumber = customer.AccountNumber,
                    Address1 = customer.Address1,
                    Name = customer.Name,
                    City = customer.City,
                    StateId = customer.StateId,
                    Postal = customer.Postal,
                    CountryId = customer.CountryId,
                    Phone = customer.Phone,
                    Fax = customer.Fax,
                    Email = customer.Email
                };

                return Json(item, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
