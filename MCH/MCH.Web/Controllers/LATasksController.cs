﻿using Kendo.Mvc.UI;
using MCH.BLL.Units;
using MCH.Web.Models.LiveAnimal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.App_Classes;

namespace MCH.Web.Controllers
{
    public class LATasksController : BaseController
    {

        public ActionResult SearchLATasks([DataSourceRequest] DataSourceRequest request, LATaskGridFilter filterData)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {

                var data = unitOfWork.Context.GetLAChecklistTaskList(
                    (short)request.Page, (byte)request.PageSize,
                    null,
                    filterData.Search, filterData.From, filterData.To,
                    filterData.CarrierId, filterData.OriginId, filterData.DestinationId,
                    filterData.TaskStatusId, filterData.AssignedUserId,
                    filterData.WarehouseId, filterData.FlightNumber
                    );

                var result = data.Select(item => new LATaskGridRow
                {
                    AssignedTo = item.AssignedTo,
                    Customer = item.Carrier,
                    Cutoff = item.Cutoff,
                    End = item.End,
                    ETD = item.ETD,
                    FlightNumber = item.FlightNumber,
                    HasAlert = item.Alert,
                    HasCommnets = 0,
                    Id = item.Id,
                    IsReopened = item.Reopen,
                    Progress = item.Progress.HasValue ? item.Progress + "%" : String.Empty,
                    Reference = item.Reference,
                    Start = item.Start,
                    Status = item.Status,
                    Total = item.Total.HasValue ? item.Total.Value : 0
                }).ToList();


                DataSourceResult dataResult = new DataSourceResult();

                if (result.Any())
                {
                    dataResult.Total = result[0].Total;
                    dataResult.Data = result;
                    
                    return Json(dataResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var tmpResult = new { Items = new { }, TotalCount = 0 };
                    return Json(tmpResult, JsonRequestBehavior.AllowGet);
                }
            }
        }

    }
}
