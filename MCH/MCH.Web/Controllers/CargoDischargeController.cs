﻿using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Core.Models.CargoDischarge;
using MCH.Web.Utilities;
using System.Configuration;
using System.IO;
using DhtmlxComponents.Widgets;

namespace MCH.Web.Controllers
{
    public class CargoDischargeController : Controller
    {

        private DischargeUnitOfWork DischargeUnit = null;

        public JsonResult GetPeriods()
        {
            return Json(new List<ValueText> 
            { 
                new ValueText { value = "1", text = "Today" },
                new ValueText { value = "2", text = "Yesterday" },
                new ValueText { value = "3", text = "Last 3 Days" },
                //new ValueText { value = "7", text = "Last Week" }, 
                //new ValueText { value = "30", text = "Last Month" },
                //new ValueText { value = "90", text = "Last Quarter" } 
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index(string param)
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetDischargeTransactions(string awb, string hwb, DateTime ? from, DateTime ? to, string company, string driver, int ? statusId)
        {
            DxGrid<DischargeTransaction> tmpResult = new DxGrid<DischargeTransaction>();

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }

        #region Controller Lifecycle
        public CargoDischargeController()
        {
            this.DischargeUnit = new DischargeUnitOfWork();
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
            this.DischargeUnit.Dispose();
            this.DischargeUnit = null;
        } 
        #endregion

        #region Wizard Steps

        // Load  wizard container
        public PartialViewResult GetCargoDischargeView(long ? taskId)
        {
            DischargeWizard tmpModel;

            
            //TODO : load the view based on status

            // for real
            tmpModel = DischargeUnit.GetDischargeState(taskId, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId);        

            // for test
           // tmpModel = new ShipmentStepModel();
             
          //var  ship = new ProcessedShipment { Id = 484, Reference = "DUB20256343", IsHWB = true }; 
          //  tmpModel.LatestShipment = ship;
            //tmpModel.Header.DriverId = 4444;

            //tmpModel.Header = new Core.Models.CargoDischarge.DischargeState()
            //{
            //    //Consignee = "New Consignee",
            //    //ConsigneeId = 221,
            //    //DriverName = "New Driver",
            //    //DriverId = 3242,
            //    //IsConsigneeOrCarrier = true,
            //    TruckingCompanyName = "Company",
            //    DischargeId = 666
            //};

            return PartialView("_wndCargoDischange", tmpModel);
        }

        public JsonResult GetProcessedShipments([DataSourceRequest] DataSourceRequest request, long? dischargeId)
        {
            // backend GetProcessedShipments(dischargeId) 

            List<ProcessedShipment> list = this.DischargeUnit.GetProcessedShipments(dischargeId);


            #region Delete test data

            //list = new List<ProcessedShipment>();

            ////for test 
            //var ship = new ProcessedShipment { Id = 2, Reference = "Ship 2" };
            //list.Add(ship);

            //ship = new ProcessedShipment { Id = 484, Reference = "DUB20256343", IsHWB = true };
            //list.Add(ship);

            //ship = new ProcessedShipment { Id = 3, Reference = "Ship 31" };
            //list.Add(ship);

            #endregion

            return Json(list.ToCustomDataSource(request, list.Count), JsonRequestBehavior.AllowGet);
        }

        //Load First Step
        public PartialViewResult GetShipmentView(long? shipmentId, string awbNumber, string hwbNumber, bool ? isByHWB)
        {
            DisShipmentDetailsModel tmpModel = null;

            if (shipmentId.HasValue && shipmentId.Value > 0)
            {
                if (isByHWB.HasValue && isByHWB.Value)
                {
                    tmpModel = this.DischargeUnit.GetHWBById(shipmentId.Value);
                }
                else
                {
                    tmpModel = this.DischargeUnit.GetAWBById(shipmentId.Value);
                }
            }
            else if (!String.IsNullOrWhiteSpace(awbNumber))
            {
                string carrier = awbNumber.Split('-')[0];
                string awbSerialNum = awbNumber.Split('-')[1];

                tmpModel = this.DischargeUnit.GetShipmentByAWB(carrier, awbSerialNum);
            }
            else if(!String.IsNullOrWhiteSpace(hwbNumber)){
                tmpModel = this.DischargeUnit.GetShipmentByHWB(hwbNumber);
            }

            if (tmpModel == null)
            {
                return PartialView("_disShipmentNotFound");
            }
            else
            {
                return PartialView("_disShipmentStep", tmpModel);
            }
        }

        public JsonResult SaveShipmentView(SaveShipment model)
        {
            SaveShipmentResult retData = this.DischargeUnit.SaveShipment(model, SessionManager.UserInfo.UserLocalId);
            
            return Json(retData, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetDriverView()
        {
            DischargeDriverModel tmpModel = new DischargeDriverModel();

            return PartialView("_disDriverStep", tmpModel);
        }


        public PartialViewResult SaveDriverView()
        {
            var tmpFormData = Request.Form["formData"];
            var tmpFormDeserialized = ControllerHelpers.ToObjectFromJSON<DischargeDriverModel>(tmpFormData);

            var attachments = SaveAttachments(tmpFormDeserialized.Documents);

            DischargeState tmpHeader = this.DischargeUnit.SaveDriverInfo(tmpFormDeserialized, attachments, SessionManager.UserInfo.UserLocalId);

            // backend
           // tmpFormDeserialized.Documents = attachments;
           // save bla bla and return header model

          var header = new Core.Models.CargoDischarge.DischargeState()
            {
                Consignee = "New Consignee",
                ConsigneeId = 221,
                DriverName = "New Driver",
                DriverId = 3242,
                IsConsigneeOrCarrier = true,
                TruckingCompanyName = "Company",
                DischargeId = 666
            };

          return PartialView("_wizHeaderCargoDischarge", header);
        }

        private string SaveAttachments(IEnumerable<MCH.Core.Models.Common.AttachmentModel> attachments)
        {
            var snapshots = attachments.Where(d => d.IsSnapshot).Select(d => string.Format("{0}${0}#Snapshot#{1}#{2}", string.IsNullOrEmpty(d.Name) ? DateTime.Now.ToString("yyyMMddhhss") : d.Name, d.ShipperDocTypeId.HasValue && d.ShipperDocTypeId.Value != -1 ? d.ShipperDocTypeId.Value : 17, d.SnapshotData)).ToList();

            var docTypes = attachments.Where(d => !d.IsSnapshot).Select(d => new { ShipperDocTypeId = d.ShipperDocTypeId.HasValue && d.ShipperDocTypeId.Value != -1 ? d.ShipperDocTypeId.Value : 17, d.Name, d.Path }).ToList();
            var documents = new List<string>();

            var index = 0;

            string tmpAttachmentsDir = ConfigurationManager.AppSettings[Constants.Config.ATTACHMENTS_REPOSITORY];


            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];
                var actualName = DateTime.Now.ToString("yyyMMddhhss") + file.FileName;
                var path = Path.Combine(tmpAttachmentsDir, actualName);
                file.SaveAs(path);
                documents.Add(string.Format("{0}${1}#Document#{2}#Document", docTypes[index].Name, actualName, docTypes[index].ShipperDocTypeId));
                ++index;
            }

            if (Request.Files.Count == 0 && docTypes.Any())
            {
                foreach (var doc in docTypes)
                    documents.Add(string.Format("{0}${1}#Document#{2}#Document", doc.Name, doc.Path, doc.ShipperDocTypeId));
            }

            documents.AddRange(snapshots);
            return string.Join("|", documents);
        }

        public PartialViewResult GetChargesView(long? shipmentId)
        {

            DischargePaymentsModel dataModel = null; // for real GetCragorDischageCharges(shipmentId)

            TempPayments = new List<DischargePaymentRow>();

            if (dataModel != null)
            {
                TempPayments = dataModel.Payments.Select(item =>
                {
                    var tmpItem = item;
                    tmpItem.Id = item.IdDB;
                    tmpItem.Type = GetPaymentTypeById(int.Parse(tmpItem.TypeId));
                    return tmpItem;
                }).ToList(); ; // Do not remove this ever
            }
            else
            {
                dataModel = new DischargePaymentsModel();
                dataModel.Payments = new List<DischargePaymentRow>();
            }
            return PartialView("_disPaymentStep", dataModel);
        }

        public void SaveChargesView(SaveCharges model)
        {
            model.Payments = TempPayments;

            // backend Save
        }

        [HttpPost]
        public void Finalize(long dischargeId)
        {
            this.DischargeUnit.Finalize(dischargeId);
        }

        [HttpPost]
        public void Cancel(long dischargeId)
        {
            this.DischargeUnit.CancelDischarge(dischargeId);
        }
        #endregion

        public JsonResult GetPaymentOptions()
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpPaymentOptions = commonUnit.Context.PaymentTypes.Select(item => new { Id = item.Id, Name = item.PaymentType1 });

                return Json(tmpPaymentOptions.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPaymentTypesForTemplate()
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpList = commonUnit.Context.ChargeTypes.Select(item => new { Id = item.Id.ToString(), Name = item.ChargeType1 });

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPaymentOption([DataSourceRequest] DataSourceRequest request, DischargePaymentRow criteria)
        {
            int ddid;
            criteria.Id = TempPayments.Max(item => item.Id) ?? 0 + 1;
            criteria.Type = int.TryParse(criteria.TypeId, out ddid) ? GetPaymentTypeById(ddid) : "";
      
            TempPayments.Add(criteria);
            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePaymentOption([DataSourceRequest] DataSourceRequest request, DischargePaymentRow criteria)
        {
            var i = TempPayments.IndexOf(TempPayments.Where(c => c.Id == criteria.Id).First());
            TempPayments.RemoveAt(i);
            return Json(ModelState.ToDataSourceResult());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePaymentOption([DataSourceRequest] DataSourceRequest request, DischargePaymentRow criteria)
        {

            var i = TempPayments.IndexOf(TempPayments.Where(c => c.Id == criteria.Id).First());

            int ddid;
            criteria.Type = int.TryParse(criteria.TypeId, out ddid) ? GetPaymentTypeById(ddid) : "";

            TempPayments[i] = criteria;
            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }

        private List<DischargePaymentRow> TempPayments
        {
            get
            {
                if ((List<DischargePaymentRow>)Session["TempPayments"] == null)
                {
                    Session["TempPayments"] = new List<DischargePaymentRow>();
                }

                return (List<DischargePaymentRow>)Session["TempPayments"];
            }
            set
            {
                Session["TempPayments"] = value;
            }
        }

        public string GetPaymentTypeById(long id)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                return commonUnit.Context.ChargeTypes.Where(item => item.Id == id).FirstOrDefault().ChargeType1;
            }
        }
    }
}