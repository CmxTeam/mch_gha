﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using MCH.Web.Models;
using MCH.BLL.Units;
using System.Collections;
using MCH.Web.Models.TaskManager;
using MCH.Web.Utilities;
using MCH.BLL.Model.Enum;

namespace MCH.Web.Controllers
{
    public class TaskManagerController : Controller
    {
        public PartialViewResult TaskList() 
        {
            return PartialView("TaskList");
        }


        public JsonResult TaskListDatasource([DataSourceRequest] DataSourceRequest request)
        {
            IEnumerable<TasksGridRow> tmpList = null;
            DataSourceResult tmpResult = null;
            using (TasksUnitOfWork taskUnit = new TasksUnitOfWork())
            {
                tmpList = taskUnit.Context.MCHTaskManagerViews.Select(t => new TasksGridRow
                 {
                     AirlineCode = t.Airline,
                     Alerts = t.AlertCount,
                     AssignedTo = t.TaskAssignees,
                     Department = t.Department,
                     DepartmentId = t.DepartmentId,
                     DueOn = t.DueOn,
                     Ended = t.EndDate,
                     EntityId = t.EntityId,
                     EntityTypeId = t.EntityTypeId,
                     Indicators = t.Indicators,
                     Pieces = t.Pieces,
                     Priority = t.TaskPriority,
                     Progress = t.TaskProgress,
                     Reference = t.Reference,
                     Reopened = t.IsReopened,
                     Started = t.StartDate,
                     Status = t.TaskStatus,
                     TaskDate = t.TaskDate,
                     TaskId = t.TaskId,
                     TaskLocation = t.WarehouseLocation,
                     TaskType = t.TaskType,
                     TaskTypeId = t.TaskTypeId,
                     ULDs = t.Ulds,
                     Weight = t.Weight,
                     WeightUOM = t.WeightUOM,
                     WarehouseId = t.WarehouseId
                 });
               

                tmpResult = tmpList.ToDataSourceResult(request);
            }


            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult GetTaskAlertsView(long taskId)
        {
            return PartialView("_wndTaskAlerts", taskId);
        }

        public JsonResult TaskAlertsDS([DataSourceRequest] DataSourceRequest request, long taskId)
        {
            List<TaskAlertModel> tmpAlerts = null;
            using (TasksUnitOfWork tasksUnit = new TasksUnitOfWork())
            {
                var tmpTaskAlerts = tasksUnit.GetTaskAlerts(taskId);

                tmpAlerts = tmpTaskAlerts.Select(a => new TaskAlertModel { 
                    TaskId = taskId,
                    AlertDate = a.Date,
                    Message = a.AlertMessage,
                    UserName = a.UserName
                }).ToList();
            }

            return Json(tmpAlerts.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetTaskUserAssignView(long[] taskIds)
        {
            return PartialView("_wndTaskUserAssign", taskIds);
        }

        public PartialViewResult GetTaskInstrAssignView(long[] taskIds)
        {
            return PartialView("_wndTaskInstrAssign", taskIds);
        }

        [HttpPost]
        public JsonResult AssignTasksToUser(TaskAssignParams model)
        {
            using (TasksUnitOfWork tasksUnit = new TasksUnitOfWork())
            {
                try
                {
                    var tmpUserIds = model.UserIds.Split().Select(t => Int64.Parse(t)).ToArray();
                    var tmpTaskIds = model.TaskIds.Split().Select(t=> Int64.Parse(t)).ToArray();
                    tasksUnit.AssignTasksToUsers(tmpTaskIds, tmpUserIds, SessionManager.UserInfo.UserLocalId);

                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { ErrorMessage = "Internal Error during reopening the task." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult AssignTasksInstr(TaskInstrParms model)
        {
            using (TasksUnitOfWork tasksUnit = new TasksUnitOfWork())
            {
                try
                {
                    var tmpTaskIds = model.TaskIds.Split(',').Select(t => Int64.Parse(t)).ToArray();
                    tasksUnit.AddInstructions(tmpTaskIds,  SessionManager.UserInfo.UserLocalId ,model.Instructions);

                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { ErrorMessage = "Internal Error during reopening the task." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult ReopenTasks(TaskActionParam model)
        {
            using (TasksUnitOfWork tasksUnit = new TasksUnitOfWork())
            {
                try
                {
                    var tmpTaskIds = model.TaskIds.Split(',').Select(e => Int64.Parse(e)).ToArray();
                    var actionResult = tasksUnit.ReopenTasks(tmpTaskIds, SessionManager.UserInfo.UserLocalId);

                    return Json(new { ActionResults = actionResult}, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { ErrorMessage = "Internal Error during reopening the task." }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        [HttpPost]
        public JsonResult CancelTasks(TaskActionParam model)
        {
            using (TasksUnitOfWork tasksUnit = new TasksUnitOfWork())
            {
                try
                {
                    var tmpTaskIds = model.TaskIds.Split(',').Select(e => Int64.Parse(e)).ToArray();
                    var actionResult = tasksUnit.CancelTasks(tmpTaskIds, SessionManager.UserInfo.UserLocalId);

                    return Json(new { ActionResults = actionResult }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { ErrorMessage = "Internal Error during canceling the task." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

    }
}
