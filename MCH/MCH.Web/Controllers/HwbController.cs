﻿using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using MCH.BLL.Units;
using MCH.Web.Models.Hwbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.Utilities;

namespace MCH.Web.Controllers
{
    public class HwbController : Controller
    {
        public PartialViewResult GetFlightHWBsView(long flightId, long? taskId)
        {
            FlightHwbListViewModel tmpModel = new FlightHwbListViewModel();
            tmpModel.EntityId = flightId;
            tmpModel.TaskId = taskId;

            return PartialView("_FlightHwbListView", tmpModel);
        }

        public JsonResult FlightHWBListDS([DataSourceRequest] DataSourceRequest request, long flightId)
        {
            DataSourceResult tmpResult = null;

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }



        public PartialViewResult GetAwbHWBsView(long awbId, long? taskId)
        {
            FlightHwbListViewModel tmpModel = new FlightHwbListViewModel();
            tmpModel.EntityId = awbId;
            tmpModel.TaskId = taskId;

            return PartialView("_AwbHwbListView", tmpModel);
        }

        public JsonResult AwbHWBListDS([DataSourceRequest] DataSourceRequest request, long awbId)
        {
            DataSourceResult tmpResult = null;
            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                using (var coreUnit = new CoreDataHelpers())
                {
                    var tmpHwbs = unit.Context.MCH_GetAwbHwbs(awbId, (int)SessionManager.UserInfo.DefaultWarehouseId).ToList();
                    tmpResult = tmpHwbs.Select((e, i) => new AwbHwbsRow
                    {
                        Consignee = e.Consignee,
                        DescofGoods = e.DescriptionofGoods,
                        Destination = e.Destination,
                        ExportControl = e.ExportControl,
                        HwbNumber = e.HWB,
                        ImportControl = coreUnit.GetHWBImportControl(e.AwbId.Value, e.HwbId.Value, string.Empty),
                        Origin = e.Origin,
                        Pieces = e.Pieces,
                        Shipper = e.Shipper,
                        Slac = e.SLAC,
                        TotalWeight = e.TotalWeight
                    }).ToList().ToDataSourceResult(request);
                }
            }

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }
    }
}