﻿using MCH.BLL.Model.Common;
using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using MCH.Web.Models.Common;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace MCH.Web.Controllers
{
    public class CommonController : Controller
    {

        public JsonResult GetDepartments(string query)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpDepartmetns = unitOfWork.GetDepartments().OrderBy(item=> item.Name).ToList();
                if (tmpDepartmetns != null)
                {
                    tmpDepartmetns.Insert(0, new BLL.Model.Common.DepartmentModel { Id = -1, Name = "All" });
                }
                return Json(tmpDepartmetns, JsonRequestBehavior.AllowGet);
            }
        }
        

        public JsonResult GetWeightUOMs()
        {
            using (CommonUnit unit = new CommonUnit())
            {
                var tmpUOMQuery = unit.Context.UOMs.Where(u => u.UOMTypeId == 1);

                var tmpUoms = tmpUOMQuery.Select(item => new SelectItem { Name = item.TwoCharCode, Id = item.Id });

                return Json(tmpUoms.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetStates(string filter)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpStatesQuery = unitOfWork.Context.States.OrderBy(item=> item.StateProvince).AsQueryable();

                if (!String.IsNullOrWhiteSpace(filter))
                {
                    tmpStatesQuery = tmpStatesQuery.Where(e => e.TwoCharacterCode.Contains(filter) || e.StateProvince.Contains(filter));
                }


                var tmpCarriers = tmpStatesQuery.Select(item => new SelectItem { Name = item.StateProvince, Id = item.Id });

                var tmpResult = tmpCarriers.ToList();


                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUserWarehouses() 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var loggedInUser = SessionManager.UserInfo;
                var tmpWarehouses = unitOfWork.GetUserWarehouses(loggedInUser.UserId).OrderBy(item=> item.Name).ToList();
                if (tmpWarehouses != null && tmpWarehouses.Any())
                {
                    tmpWarehouses.Insert(0, new BLL.Model.Common.WarehouseModel { Id = -1, Name = "All" });
                }
                return Json(tmpWarehouses, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAccountCarriers()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpAccountCarriers = unitOfWork.Context.Accounts.Where(a=> !a.IsOwner).OrderBy(a=> a.AccountCode);
                var tmpCarriers = tmpAccountCarriers.Select(item => new { Name = (item.AccountCode + " - " + item.AccountName), Id = item.Id }).OrderBy(item=> item.Name);

                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetApprovedIACs(string filter)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarriersQuery = unitOfWork.Context.TSA_Approved_IAC_Master_List.Where(c=> c.IsActive && c.ExpirationDate > DateTime.Now);

                if (!String.IsNullOrWhiteSpace(filter))
                {
                    tmpCarriersQuery = tmpCarriersQuery.Where(e => e.ApprovalNumber.Contains(filter) || e.IACName.Contains(filter));
                }


                var tmpCarriers = tmpCarriersQuery.Select(item => new SelectItem{ Name = item.ApprovalNumber + " - " + item.IACName, Id = item.Id }).OrderBy(item=> item.Name);

                var tmpResult = tmpCarriers.ToList();


                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetApprovedTSACarriers(string filter)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarriersQuery = unitOfWork.Context.TSA_Approved_Carriers_Master_List.Where(c => c.IsActive);

                if (!String.IsNullOrWhiteSpace(filter)) {
                    tmpCarriersQuery = tmpCarriersQuery.Where(e => e.IATACode.Contains(filter) || e.Name.Contains(filter));
                }


                var tmpCarriers = tmpCarriersQuery.Select(item => new SelectItem{ Name = item.IATACode + " - " + item.Name, Id = item.Id }).OrderBy(item=> item.Name);

                var tmpResult = tmpCarriers.ToList();
                //if (tmpResult.Any())
                //{
                //    tmpResult.Insert(0, new SelectItem { Name = "-Select- ", Id = -1});
                //}

                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetApprovedKnownShippers(int? accountCarrierId, string filter)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarriersQuery = unitOfWork.Context.TSA_Approved_Carriers_KnownShippers_List.Where(c => c.IsActive && c.ExpirationDate > DateTime.Now).OrderBy(item=> item.ApprovalNumber).AsQueryable();
                if (accountCarrierId.HasValue)
                {
                    tmpCarriersQuery = tmpCarriersQuery.Where(c => c.AccountId == accountCarrierId.Value);
                }

                if (!String.IsNullOrWhiteSpace(filter))
                {
                    tmpCarriersQuery = tmpCarriersQuery.Where(e => e.ApprovalNumber.Contains(filter) || e.ShipperName.Contains(filter));
                }


                var tmpCarriers = tmpCarriersQuery.Select(item => new SelectItem{ Name = item.ApprovalNumber + " - " + item.ShipperName, Id = item.Id });

                var tmpResult = tmpCarriers.ToList();


                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetFlightTypes()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpFlightTypesQuery = unitOfWork.Context.AircraftTypes;

                var tmpFlightTypes = tmpFlightTypesQuery.Select(item => new SelectItem{ Name = item.Description, Id = item.Id });

                var tmpResutl = tmpFlightTypes.ToList();
                if (tmpResutl.Any())
                {
                    tmpResutl.Insert(0, new SelectItem { Name = "-Select-", Id = -1 });
                }

                return Json(tmpResutl, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDocumentTypesBySPH(string codes)
        {
            var tmpList = new List<SimpleListItem>();
            if(!String.IsNullOrWhiteSpace(codes)){
                string[] groupStringIds = codes.Split(',');
                long[] groupIds = null;
                if (groupStringIds.Any())
                {
                    try
                    {
                        groupIds = groupStringIds.Select(i => Int64.Parse(i.Split('|')[0])).ToArray();
                        using (SpecialHandlingUnit unit = new SpecialHandlingUnit())
                        {
                            tmpList = unit.GetDocumentTypesBySph(groupIds);
                        }
                    }
                    catch (Exception)
                    {
                        //TODO : Handle this case
                    }
                }
            }

            return Json(tmpList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetShipmentDocumentTypes() 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpShipperTypesQuery = unitOfWork.Context.ShipperVerificationDocs.Where(item=> item.ShipperTypeId == null && item.SpecialHandlingId == null).AsQueryable();

                var tmpDocumentTypes = tmpShipperTypesQuery.Select(item => new SelectItem { Name = item.DocumentName, Id = item.Id }).OrderBy(item => item.Name);

                var tmpResult = tmpDocumentTypes.ToList();

                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetDocumentTypes(int? shipperTypeId)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpShipperTypesQuery = unitOfWork.Context.ShipperVerificationDocs.AsQueryable();

                if (shipperTypeId.HasValue)
                {
                    tmpShipperTypesQuery = tmpShipperTypesQuery.Where(d => d.ShipperTypeId == shipperTypeId.Value);
                }

                var tmpDocumentTypes = tmpShipperTypesQuery.Select(item => new SelectItem{ Name = item.DocumentName, Id = item.Id }).OrderBy(item=> item.Name);

                var tmpResult = tmpDocumentTypes.ToList();

                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetGovAuthIds(int? shipperTypeId)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpShipperTypesQuery = unitOfWork.Context.CredentialTypes.AsQueryable();

                if (shipperTypeId.HasValue)
                {
                    tmpShipperTypesQuery = tmpShipperTypesQuery.Where(d => d.ShipperTypeId == shipperTypeId.Value);
                }

                var tmpDocumentTypes = tmpShipperTypesQuery.Select(item => new { Name = item.Name, Id = item.Id }).OrderBy(item=> item.Name);

                var tmpResult = tmpDocumentTypes.ToList();


                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetShipperTypes(int flightTypeId = 1)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                // select combined flight types if flight types is not specified.
                var tmpShipperTypesQuery = unitOfWork.Context.ShipperTypes.Where(sh=> sh.AircraftTypeId == flightTypeId || sh.AircraftTypeId == 3);

                var tmpShipperTypes = tmpShipperTypesQuery.Select(item => new ShipperTypeModel { Name = item.Name, Id = item.Id, GroupId = item.ShipperGroupId }).OrderBy(item=> item.Name);

                var tmpResult = tmpShipperTypes.ToList();
                if (tmpResult.Any())
                {
                    tmpResult.Insert(0, new ShipperTypeModel { Name = "-Select-", Id = -1, GroupId = -1 });
                }

                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCarriers(string query) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpCarriersQuery = unitOfWork.CarrierRepostiory.Get().Where(item=> !String.IsNullOrWhiteSpace(item.CarrierCode)).OrderBy(e=> e.CarrierCode).AsQueryable();
                if (!String.IsNullOrWhiteSpace(query)) 
                {
                    tmpCarriersQuery = tmpCarriersQuery.Where(item => 
                        (!String.IsNullOrWhiteSpace(item.CarrierName) && item.CarrierName.IndexOf(query,0,StringComparison.InvariantCultureIgnoreCase) != -1)||
                        (!String.IsNullOrWhiteSpace(item.CarrierCode) && item.CarrierCode.IndexOf(query,0,StringComparison.InvariantCultureIgnoreCase) != -1));
                }

                var tmpCarriers = tmpCarriersQuery.Select(item => new { name = (item.CarrierName + " (" + item.CarrierCode + ")"), id = item.Id }).OrderBy(item=> item.name);

                return Json(tmpCarriers, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetTaskTypes(string query) 
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTaskTypeQuery = unitOfWork.TaskTypeRepository.Get().Where(item => !String.IsNullOrWhiteSpace(item.Description));
                if (!String.IsNullOrWhiteSpace(query))
                {
                    tmpTaskTypeQuery = tmpTaskTypeQuery.Where(item =>
                        (!String.IsNullOrWhiteSpace(item.TaskName) && item.TaskName.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) != -1));
                }

                var tmpCarriers = tmpTaskTypeQuery.Select(item => new { name = item.TaskName, id = item.Id }).OrderBy(item=> item.name);

                return Json(tmpCarriers, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDepartmentTaskTypeFilter(long ? depId)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTaskTypeQuery = unitOfWork.TaskTypeRepository.Get().Where(item => !String.IsNullOrWhiteSpace(item.Description));

                if (depId.HasValue && depId != -1)
                {
                    tmpTaskTypeQuery = tmpTaskTypeQuery.Where(item => item.DepartmentId == depId.Value);
                }

                var tmpTaskTypes = tmpTaskTypeQuery.Select(item => new { name = item.TaskName, id = item.Id }).OrderBy(item => item.name).ToList();

                if (tmpTaskTypes != null && tmpTaskTypes.Any())
                {
                    tmpTaskTypes.Insert(0, new { name = "All", id = -1 });
                }

                return Json(tmpTaskTypes, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTaskTypeFilter()
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTaskTypeQuery = unitOfWork.TaskTypeRepository.Get().Where(item => !String.IsNullOrWhiteSpace(item.Description));

                var tmpTaskTypes= tmpTaskTypeQuery.Select(item => new { name = item.TaskName, id = item.Id }).OrderBy(item=> item.name).ToList();

                if (tmpTaskTypes != null && tmpTaskTypes.Any())
                {
                    tmpTaskTypes.Insert(0, new { name = "All", id = -1 });
                }

                return Json(tmpTaskTypes, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDataEntryTaskTypes() 
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTaskTypeQuery = unitOfWork.TaskTypeRepository.Get().Where(item => !String.IsNullOrWhiteSpace(item.Description) && item.IsDataEntry.HasValue && item.IsDataEntry.Value);

                var tmpTaskTypes = tmpTaskTypeQuery.Select(item => new { name = item.TaskName, id = item.Id }).OrderBy(item => item.name).ToList();

                return Json(tmpTaskTypes, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTaskStatuses(string query) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpTaskStatusesQuery = unitOfWork.StatusRepository.Get(item => item.EntityTypeId == (int)enumEntityTypes.Task);

                var tmpTaskStatuses = tmpTaskStatusesQuery.Select(item => new { name = item.DisplayName, id = item.Id });

                return Json(tmpTaskStatuses, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetStations(string query) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
               var tmpPortQuery =  unitOfWork.PortRepository.Get();

               if (!String.IsNullOrWhiteSpace(query)) 
               {
                   tmpPortQuery = tmpPortQuery.Where(item => item.IATACode.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) != -1);
               }

               var tmpStations = tmpPortQuery.Select(item => new { name = item.IATACode, id = item.Id }).OrderBy(item=> item.name);

                return Json(tmpStations, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAirports()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpPortQuery = unitOfWork.PortRepository.Get();

                var tmpStations = tmpPortQuery.Select(item => new { Name = item.IATACode, Id = item.Id }).OrderBy(item => item.Name);

                return Json(tmpStations, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRateClasses() 
        {
            using (CommonUnit unit = new CommonUnit())
            {
                var tmpRates = unit.Context.RateClassCodes.Select(item => new { Name = item.Code, Id = item.Id }).OrderBy(item => item.Name);

                return Json(tmpRates.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUsers(string query) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpPortQuery = unitOfWork.UserProfileRepository.Get();

                if (!String.IsNullOrWhiteSpace(query))
                {
                    tmpPortQuery = tmpPortQuery.Where(item => 
                        (!String.IsNullOrWhiteSpace(item.FirstName) &&  item.FirstName.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) != -1) || 
                        (!String.IsNullOrWhiteSpace(item.LastName) && item.LastName.IndexOf(query,0, StringComparison.InvariantCultureIgnoreCase)!= -1));
                }

                var tmpStations = tmpPortQuery.Select(item => new { name = item.FirstName + " " + item.LastName, id = item.Id }).OrderBy(item=> item.name);

                return Json(tmpStations, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomers(string prefixText) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCustomersQuery = unitOfWork.Context.Customers.AsQueryable();
                
                if (!String.IsNullOrWhiteSpace(prefixText))
                {
                    tmpCustomersQuery = tmpCustomersQuery.Where(item =>
                        (item.Name != null && item.Name.StartsWith(prefixText.Trim())));
                }

                var tmpCarriers = tmpCustomersQuery.Select(item => new { Text = item.Name, Value = item.Id, Code= item.AccountNumber, Type = "" }).OrderBy(item=> item.Text);

                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetShippers(string prefixText) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpShippers = unitOfWork.Context.SearchShipper(String.Empty, prefixText);

                var tmpCarriers = tmpShippers.Select(item => new { Text = item.Name, Value = item.Id, Code = item.IATACode, Type = item.STypeId }).OrderBy(item=> item.Text);

                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDriverTruckingCompanies(string filter)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarriersQuery = unitOfWork.Context.Carriers.Where(c=> c.MOTId == 3).Select(d => new SelectItem
                {
                    Id = d.Id,
                    Name = d.CarrierName.ToUpper()
                }).OrderBy(item=> item.Name).AsQueryable();
                if (!String.IsNullOrWhiteSpace(filter))
                {
                    tmpCarriersQuery = tmpCarriersQuery.Where(i => i.Name.Contains(filter));
                }

                return Json(tmpCarriersQuery.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCarrierDrivers(string filter)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarrierDriversQuery = unitOfWork.Context.CarrierDrivers.Select(d=> new SelectItem{Id = d.Id, Name = d.FullName+ "-"+ (d.State!= null ? d.State.TwoCharacterCode + 
                    " - ":String.Empty) +"-"+d.LicenseNumber}).OrderBy(item=> item.Name).AsQueryable();
                if (!String.IsNullOrWhiteSpace(filter))
                {
                    tmpCarrierDriversQuery = tmpCarrierDriversQuery.Where(i => i.Name.Contains(filter));
                }

                return Json(tmpCarrierDriversQuery.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDrivers(string prefixText) 
        {
             using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpDrivers = unitOfWork.Context.SearchCarrierDriver("",prefixText);

                var tmpCarriers = tmpDrivers.Select(item => new { 
                    Text = item.DriverName,
                    Value = item.Id, 
                    TrackingCo = item.TruckingCo,
                    TrackingCoId = item.TruckingCompanyId,
                    PrimaryIdType = item.CredentialTypeId,
                    LicenseNumber = item.LicenseNumber,
                    LicensePhoto = item.LicenseImage,
                    DriverPhoto = item.DriverPhoto
                }).OrderBy(item=> item.Text);
                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
             }
        }

        public JsonResult GetGovCredentials(string filter)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpFirstIDTypesQuery = unitOfWork.Context.CredentialTypes.Where(c => c.IsGovernmentApproved).Select(d => new SelectItem
                {
                    Id = d.Id,
                    Name = d.Name
                }).OrderBy(item => item.Name).AsQueryable();
                if (!String.IsNullOrWhiteSpace(filter))
                {
                    tmpFirstIDTypesQuery = tmpFirstIDTypesQuery.Where(i => i.Name.Contains(filter));
                }

                return Json(tmpFirstIDTypesQuery.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetApprovedCCSFs()
        {
            List<SimpleListItem> tmpList = new List<SimpleListItem>();
            using (CommonUnit unit = new CommonUnit())
            {
                var tmpQuery = unit.Context.TSA_Approved_Certified_Screening_Facilities.Where(item => item.CCSFStatus == "Active" && item.ExpirationDate > DateTime.Now).OrderBy(item=> item.TSAApprovalNumber);
                tmpList = tmpQuery.Select(item => new SimpleListItem { 
                    Id = item.Id, 
                    Name = item.TSAApprovalNumber + "-" + item.Name
                }).ToList();
            }
            return Json(tmpList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSealTypes()
        {
            List<SimpleListItem> tmpList = new List<SimpleListItem>();

            using (CommonUnit unit = new CommonUnit())
            {
                tmpList = unit.Context.ScreeningSealTypes.Select(item => new SimpleListItem {
                    Id = item.Id,
                    Name = item.Type
                }).OrderBy(item=> item.Name).ToList();
            }

            return Json(tmpList, JsonRequestBehavior.AllowGet);
        }


        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetPorts()
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpPortQuery = commonUnit.GetPorts();
                var tmpStations = tmpPortQuery.Select(item => new { Name = item.IATACode, Id = item.Id }).OrderBy(i => i.Name).ToList();

                return Json(tmpStations, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
