﻿using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Common;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class DriversController : Controller
    {

        public JsonResult GetById(long id)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpResutl = commonUnit.Context.CarrierDrivers.Select(d=> new DriverModel{
                    Id = d.Id, 
                    DriverLicense = d.LicenseNumber,
                    FullName = d.FullName,
                    STANumber = d.STANumber,
                    StateId = d.StateId
                }).SingleOrDefault(d => d.Id == id);


                return Json(tmpResutl, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddDriver(DriverModel model)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpCarrierDriver = new CarrierDriver();

                tmpCarrierDriver.StateId = model.StateId;
                tmpCarrierDriver.FullName = model.FullName;
                tmpCarrierDriver.LicenseNumber = model.DriverLicense;
                tmpCarrierDriver.STANumber = model.STANumber;

                commonUnit.Context.CarrierDrivers.Add(tmpCarrierDriver);
                commonUnit.Save();
                model.Id = tmpCarrierDriver.Id;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddTruckingCompany(TruckingCompModel model)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpCarrier = new Carrier();

                tmpCarrier.CarrierName = model.TruckingCompanyName;
                tmpCarrier.MOTId = 3;

                commonUnit.Context.Carriers.Add(tmpCarrier);
                commonUnit.Save();
                model.Id = tmpCarrier.Id;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}