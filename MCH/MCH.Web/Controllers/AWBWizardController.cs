﻿using Kendo.Mvc.UI;
using MCH.BLL.Units;
using MCH.Web.Models.AWBWizard;
using MCH.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using MCH.BLL.Model.Common;
using MCH.Web.Utilities;
using MCH.BLL.Model.AwbWizard;
using System.IO;
using System.Web.UI;
using MCH.BLL.Model.SpecialHandling;
using MCH.Core.Models.Common;

namespace MCH.Web.Controllers
{
    public class AWBWizardController : BaseController
    {

        #region Steps
        public PartialViewResult AWBWizardPopup(long? id, AWBWizardSteps? step)
        {
            if (step == null && id != null)
            {
                using (var unit = new AwbWizardUnit())
                {
                    step = GetAWBWizardNextStep(unit.GetAwbWizardStep(id.Value));
                }
            }
            AWBMainPopup model = new AWBMainPopup { AirwaybillId = id, Step = step.Value };
            return PartialView("AWBWizardPopup", model);
        }
        public PartialViewResult AWBStep(long? id)
        {
            using (CommonUnit unit = new CommonUnit())
            {
                ViewBag.AirCraftTypes = unit.GetAircraftTypes().Select(item => new SimpleType { Id = (int)item.Id, Name = item.Code }).ToList();
                ViewBag.AwbTypes = Enum.GetValues(typeof(AWBTypes)).Cast<AWBTypes>().Select(en => new SimpleType { Id = (int)en, Name = en.ToString() }).ToList();
            }

            AWBStep model = new AWBStep();

            using (AwbWizardUnit unit = new AwbWizardUnit())
            {
                var awb = unit.GetAwbStep(id);
                model = ConvertAWBToFrontend(awb);

            }
            return PartialView("AWBStep", model);
        }

        public ActionResult FindAWB(int accountid, string serialnumber, long originid, long destid)
        {


            using (AwbWizardUnit unit = new AwbWizardUnit())
            {
                var awb = unit.GetAwbByNumber(accountid, serialnumber, originid, destid);

                if (awb.Id > 0)
                {
                    using (CommonUnit cmunit = new CommonUnit())
                    {
                        ViewBag.AirCraftTypes = cmunit.GetAircraftTypes().Select(item => new SimpleType { Id = (int)item.Id, Name = item.Code }).ToList();
                        ViewBag.AwbTypes = Enum.GetValues(typeof(AWBTypes)).Cast<AWBTypes>().Select(en => new SimpleType { Id = (int)en, Name = en.ToString() }).ToList();
                    }

                    AWBStep model = ConvertAWBToFrontend(awb);
                    return PartialView("AWBStep", model);
                }
                else
                {
                    return Json(null);
                }

            }

        }

        private AWBStep ConvertAWBToFrontend(AwbStepModel awb)
        {
            AWBStep model = new AWBStep();

            model.AirwaybillId = awb.Id;
            model.CarrierId = awb.CarrierId;
            model.OriginId = awb.OriginId;
            model.AircraftType = awb.AircraftType;
            model.AirwaybillTypeId = awb.AwbTypeId;
            model.DestinationId = awb.DestinationId;

            model.AccountInformationText = awb.AccountInformationText;

            //    model.GoodsDescription = awb.GoodsDescription;
            model.AirwaybillSerialNumber = awb.AwbSerialNumber;
            //    model.OtherServiceRequests = awb.OtherServiceRequests;
            //    model.SpecialServiceRequests = awb.SpecialServiceRequests;
            //model.AccountInformation = awb.AccountInformation.Select(a => new Code
            //{
            //    Id = a.Id,
            //    CodeId = a.CodeId,
            //    CodeNote = a.CodeNote
            //}).ToList();
            model.IssuingCarriersAgent = new Agent
            {
                Id = awb.IssuingCarriersAgent.Id,
                City = awb.IssuingCarriersAgent.City,
                Name = awb.IssuingCarriersAgent.Name,
                IATACode = awb.IssuingCarriersAgent.IataCode,
                CASSAddress = awb.IssuingCarriersAgent.CassAddress,
                AccountNumber = awb.IssuingCarriersAgent.AccountNumber,
                ParticipantIdentfierId = awb.IssuingCarriersAgent.ParticipantIdentfierId
            };
            model.Shipper = new Partner
            {
                Id = awb.Shipper.Id,
                Fax = awb.Shipper.Fax,
                Name = awb.Shipper.Name,
                City = awb.Shipper.City,
                Type = PartnerType.Shipper,
                Phone = awb.Shipper.Phone,
                Postal = awb.Shipper.Postal,
                StateId = awb.Shipper.StateId,
                Address1 = awb.Shipper.Address1,
                CountryId = awb.Shipper.CountryId,
                AccountNumber = awb.Shipper.AccountNumber
            };
            model.Consignee = new Partner
            {
                Id = awb.Consignee.Id,
                Fax = awb.Consignee.Fax,
                Name = awb.Consignee.Name,
                City = awb.Consignee.City,
                Type = PartnerType.Consignee,
                Phone = awb.Consignee.Phone,
                Postal = awb.Consignee.Postal,
                StateId = awb.Consignee.StateId,
                Address1 = awb.Consignee.Address1,
                CountryId = awb.Consignee.CountryId,
                AccountNumber = awb.Consignee.AccountNumber,
            };
            model.Flights = awb.Flights.Select(f => new Flight
            {
                Id = f.Id,
                FlightNo = f.FlightNo,
                FlightDate = f.FlightDate,
                FlightToId = f.FlightToId.ToString(),
                FlightFromId = f.FlightFromId.ToString(),
                FlightCarrierId = f.CarrierId.ToString()
            }).ToList();

            if (awb.SenderReferences.FirstOrDefault() != null)
            {
                model.AirportId = awb.SenderReferences.First().AirportId;
                model.ParticipantCode = awb.SenderReferences.First().ParticipantCode;
                model.ParticipantIdentfierId = awb.SenderReferences.First().ParticipantIdentifierId;
                model.FileReference = awb.SenderReferences.First().FileReference;
            }

            // Do not remove this ever
            //var acInfCount = 6 - model.AccountInformation.Count();
            //for (int i = 0; i < acInfCount; i++)
            //{
            //    Code code = new Code();
            //    model.AccountInformation.Add(code);
            //}

            var flCount = 3 - model.Flights.Count();
            for (int i = 0; i < flCount; i++)
            {
                Flight code = new Flight();
                model.Flights.Add(code);
            }
            //

            return model;
        }
        public PartialViewResult SaveAWBStep(AWBStep step)
        {
            //step.Flights = step.Flights.Where(fl => fl.FlightFromId != null).ToList();
            //   step.AccountInformation = step.AccountInformation.Where(ac => ac.CodeId != null || !String.IsNullOrWhiteSpace(ac.CodeNote)).ToList();

            //var flights = ConvertToXml<Flight>(step.Flights);
            //  var accountInfo = ConvertToXml<Code>(step.AccountInformation);

            var datamodel = ConvertAWBToBackend(step);

            using (AwbWizardUnit unit = new AwbWizardUnit())
            {
                step.AirwaybillId = unit.SaveAwbStep(datamodel, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
            }

            return ChargesStep(step.AirwaybillId);
        }

        private AwbStepModel ConvertAWBToBackend(AWBStep step)
        {

            AwbStepModel datamodel = new AwbStepModel();

            datamodel.Id = step.AirwaybillId;
            datamodel.CarrierId = (int)step.CarrierId;
            datamodel.OriginId = step.OriginId;
            datamodel.AircraftType = step.AircraftType.Value;
            datamodel.AwbTypeId = step.AirwaybillTypeId;
            datamodel.DestinationId = step.DestinationId;

            datamodel.AccountInformationText = step.AccountInformationText;
            datamodel.AwbSerialNumber = step.AirwaybillSerialNumber;

            datamodel.IssuingCarriersAgent = new AgentModel
            {
                Id = step.IssuingCarriersAgent.Id,
                City = step.IssuingCarriersAgent.City,
                Name = step.IssuingCarriersAgent.Name,
                IataCode = step.IssuingCarriersAgent.IATACode,
                CassAddress = step.IssuingCarriersAgent.CASSAddress,
                AccountNumber = step.IssuingCarriersAgent.AccountNumber,
                ParticipantIdentfierId = step.IssuingCarriersAgent.ParticipantIdentfierId
            };
            datamodel.Shipper = new CustomerModel
            {
                Id = step.Shipper.Id
            };
            datamodel.Consignee = new CustomerModel
            {
                Id = step.Consignee.Id
            };
            datamodel.Flights = step.Flights.Where(f=>f.FlightFromId!=null).Select(f => new FlightModel
            {
                Id = f.Id,
                FlightNo = f.FlightNo,
                FlightDate = f.FlightDate,
                FlightToId = long.Parse(f.FlightToId),
                FlightFromId = long.Parse(f.FlightFromId),
                CarrierId = long.Parse(f.FlightCarrierId)
            }).ToList();

            var sd = new List<SenderReferenceModel>();
           
           sd.Add( new SenderReferenceModel {

             AirportId = step.AirportId,
                ParticipantCode = step.ParticipantCode,
                ParticipantIdentifierId = step.ParticipantIdentfierId,
                FileReference = step.FileReference

            });
           datamodel.SenderReferences = sd;

           return datamodel;
        }

        public PartialViewResult ChargesStep(long? id)
        {
            ViewBag.ChargeTypes = new List<SimpleType>
            {
                    new SimpleType { Id = 0, Name = "Prepaid" },
                    new SimpleType { Id = 1, Name = "Collect" }
            };

            AwbChargeDeclaration dataModel;

            using (var unit = new AwbWizardUnit())
            {
                dataModel = unit.GetAwbChargeDeclaration(id.Value);
            }

            ChargesStep model;

            TempRateDescrs = new List<RateDescription>();
            TempOtherCharges = new List<SimpleCharge>();

            if (dataModel != null)
            {
                model = InitializeChargesRealModel(dataModel);
               


                model.TotalWeightCharges = new SimpleChargeTypeValue { Id = 1, Title = "Total Weight Charges", Collect = 0, Prepaid = 0 };

                model.TotalTaxes = new SimpleChargeTypeValue { Id = 2, Title = "Total Taxes", Collect = model.TotalTaxes.Collect ?? 0, Prepaid = model.TotalTaxes.Prepaid ?? 0 };
                model.TotalValuationCharges = new SimpleChargeTypeValue { Id = 3, Title = "Total Valuation Charges", Collect = model.TotalValuationCharges.Collect ?? 0, Prepaid = model.TotalValuationCharges.Prepaid ?? 0 };

                model.TotalOtherChargesDueCarrier = new SimpleChargeTypeValue { Id = 4, Title = "Total Other Charges Due Carrier", Collect = 0, Prepaid = 0 };
                model.TotalOtherChargesDueAgent = new SimpleChargeTypeValue { Id = 5, Title = "Total Other Charges Due Agent", Collect = 0, Prepaid = 0 };
                model.Total = new SimpleChargeTypeValue { Id = 6, Title = "Total", Collect = 0, Prepaid = 0 };

                TempRateDescrs = model.RateDescriptionsList.Select(item =>
                {
                    var tmpItem = item;
                    tmpItem.Id = item.IdToDB;
                    tmpItem.UOMWeight = GetUomById(tmpItem.UOMWeightId);
                    return tmpItem;
                }).ToList(); // Do not remove this ever

                TempOtherCharges = model.OtherChargesList.Select(item =>
                {
                    var tmpItem = item;
                    tmpItem.Id = item.IdToDB;
                    var tempcrit = GetChargeCodeById(int.Parse(tmpItem.CodeId));
                    tmpItem.Code = tempcrit == null ? "" : tempcrit.Code;
                    tmpItem.IsCodeDueCarrier = tempcrit == null ? false : tempcrit.IsCodeDueCarrier;
                    return tmpItem;
                }).ToList(); ; // Do not remove this ever


                //Do not remove this!!
                //TotalWeightCharges
                var sum = model.RateDescriptionsList.Sum(r => r.Total) ?? 0;

                if (model.ChargeDeclarations.WTVALChargesType == 0) // Prepaid
                {
                    model.TotalWeightCharges.Prepaid = sum;
                }
                else // Collect
                {
                    model.TotalWeightCharges.Collect = sum;
                }


                // TotalOtherCharges
                var agentsum = model.OtherChargesList.Where(o => o.IsCodeDueCarrier == false).Sum(o => o.Amount) ?? 0;
                var carriersum = model.OtherChargesList.Where(o => o.IsCodeDueCarrier == true).Sum(o => o.Amount) ?? 0;

                if (model.ChargeDeclarations.OtherChargesType == 0) // Prepaid
                {
                    model.TotalOtherChargesDueAgent.Prepaid = agentsum;
                    model.TotalOtherChargesDueCarrier.Prepaid = carriersum;
                }
                else // Collect
                {
                    model.TotalOtherChargesDueAgent.Collect = agentsum;
                    model.TotalOtherChargesDueCarrier.Collect = carriersum;
                }

                model.Total.Collect = model.TotalWeightCharges.Collect + model.TotalValuationCharges.Collect + model.TotalTaxes.Collect
                                        + model.TotalOtherChargesDueAgent.Collect + model.TotalOtherChargesDueCarrier.Collect;

                model.Total.Prepaid = model.TotalWeightCharges.Prepaid + model.TotalValuationCharges.Prepaid + model.TotalTaxes.Prepaid
                                       + model.TotalOtherChargesDueAgent.Prepaid + model.TotalOtherChargesDueCarrier.Prepaid;
            }
            else
                model = InitializeChargesDummyModel(id);
            return PartialView("ChargesStep", model);
        }

        private ChargesStep InitializeChargesRealModel(AwbChargeDeclaration dataModel)
        {
            ChargesStep model = new ChargesStep();
            model.AirwaybillId = dataModel.AwbId;
            model.AirwaybillTypeId = dataModel.AirwaybillType;
            model.HandlingInformation = dataModel.HandlingInformation;
            model.ExecutedAtPlace = dataModel.ExecutedAtPlace;
            model.ExecutedBy = dataModel.ExecutedBy;
            model.ExecutedOnDate = dataModel.ExecutedOnDate.Value;
            model.CertificationName = dataModel.CertificationName;



            model.ChargeDeclarations = new ChargeDeclarations
            {
                Id = dataModel.ChargeDeclarations.Id,
                CurrencyId = dataModel.ChargeDeclarations.CurrencyId,
                NCV = dataModel.ChargeDeclarations.NCV,
                NVD = dataModel.ChargeDeclarations.NVD,
                XXX = dataModel.ChargeDeclarations.XXX,
                DeclaredValueForCarrige = dataModel.ChargeDeclarations.DeclaredValueForCarrige,
                DeclaredValueForCustoms = dataModel.ChargeDeclarations.DeclaredValueForCustoms,
                WTVALChargesType = dataModel.ChargeDeclarations.WTVALChargesType,
                OtherChargesType = dataModel.ChargeDeclarations.OtherChargesType,
                InsuranceAmount = dataModel.ChargeDeclarations.InsuranceAmount
            };

            model.SpecialHandlingRequests = dataModel.SpecialHandlingRequests.Select(a => new Code
            {
                Id = a.Id,
                CodeId = a.CodeId,
                CodeNote = a.CodeNote
            }).ToList();


            model.OtherChargesList = dataModel.OtherChargesList.Select(o => new SimpleCharge
            {
                IdToDB = o.Id,
                Id = o.Id,
                Amount = o.Amount,
                CodeId = o.CodeId.ToString()
            }).ToList();

            model.RateDescriptionsList = dataModel.RateDescriptionsList.Select(r => new RateDescription
            {
                IdToDB = r.Id,
                Id = r.Id,
                NoOfPieces = r.NoOfPieces,
                RateCharge = r.RateCharge,
                Total = r.Total,
                RateClass = r.RateClass,
                UOMWeightId = r.UOMWeightId.Value,
                ChargeableWeight = r.ChargeableWeight,
                CommodityItemNo = r.CommodityItemNo,
                GrossWeight = r.GrossWeight,
                GoodsDescription = r.GoodsDescription
            }).ToList();

            model.TotalTaxes = new SimpleChargeTypeValue
            {
                Collect = dataModel.TotalTaxes.Collect,
                Prepaid = dataModel.TotalTaxes.Prepaid,
            };

            model.TotalValuationCharges = new SimpleChargeTypeValue
            {
                Collect = dataModel.TotalValuationCharges.Collect,
                Prepaid = dataModel.TotalValuationCharges.Prepaid
            };

            model.TotalWeightCharges = new SimpleChargeTypeValue
            {

                Collect = dataModel.TotalWeightCharges.Collect,
                Prepaid = dataModel.TotalWeightCharges.Prepaid
            };


            //Do not remove this ever
            var spcount = 9 - model.SpecialHandlingRequests.Count();
            for (int i = 0; i < spcount; i++)
            {
                Code code = new Code();
                model.SpecialHandlingRequests.Add(code);
            }


            return model;
        }

        private ChargesStep InitializeChargesDummyModel(long? id)
        {
            ChargesStep model = new ChargesStep();
            model.AirwaybillId = id;

            using (var unit = new AwbWizardUnit())
            {
                model.AirwaybillTypeId = unit.GetAwbTypeByAwbId(id.Value);
            }
            model.OtherChargesList = new List<SimpleCharge>();
            model.RateDescriptionsList = new List<RateDescription>();

            var chanrgesDec = new ChargeDeclarations();

            chanrgesDec.CurrencyId = 1;
            chanrgesDec.DeclaredValueForCarrige = 0;
            chanrgesDec.DeclaredValueForCustoms = 0;
            chanrgesDec.InsuranceAmount = 0;
            chanrgesDec.WTVALChargesType = 0;
            chanrgesDec.OtherChargesType = 0;
            chanrgesDec.NVD = true;
            chanrgesDec.NCV = true;
            chanrgesDec.XXX = true;

            model.ChargeDeclarations = chanrgesDec;

            model.TotalWeightCharges = new SimpleChargeTypeValue { Id = 1, Title = "Total Weight Charges", Collect = 0, Prepaid = 0 };
            model.TotalValuationCharges = new SimpleChargeTypeValue { Id = 2, Title = "Total Valuation Charges", Collect = 0, Prepaid = 0 };
            model.TotalTaxes = new SimpleChargeTypeValue { Id = 3, Title = "Total Taxes", Collect = 0, Prepaid = 0 };
            model.TotalOtherChargesDueCarrier = new SimpleChargeTypeValue { Id = 4, Title = "Total Other Charges Due Carrier", Collect = 0, Prepaid = 0 };
            model.TotalOtherChargesDueAgent = new SimpleChargeTypeValue { Id = 5, Title = "Total Other Charges Due Agent", Collect = 0, Prepaid = 0 };
            model.Total = new SimpleChargeTypeValue { Id = 6, Title = "Total", Collect = 0, Prepaid = 0 };

            model.SpecialHandlingRequests = new List<Code>();
            for (int i = 0; i < 9; i++)
            {
                Code code = new Code();
                model.SpecialHandlingRequests.Add(code);
            }
        
            model.ExecutedOnDate = DateTime.Now;

            return model;
        }

        public PartialViewResult SaveChargesStep(ChargesStep step)
        {
            step.SpecialHandlingRequests = step.SpecialHandlingRequests.Where(s => s.CodeId != null).ToList();
            step.RateDescriptionsList = TempRateDescrs;
            step.OtherChargesList = TempOtherCharges;



            //TODO: Karen do we need these xml-s?
            //var otherCharges = ConvertToXml<SimpleCharge>(step.OtherChargesList);
            //var rateDescriptions = ConvertToXml<RateDescription>(step.RateDescriptionsList);

            using (var unit = new AwbWizardUnit())
            {

                AwbChargeDeclaration datamodel = new AwbChargeDeclaration();

                datamodel.AwbId = step.AirwaybillId;
                datamodel.CertificationName = step.CertificationName;

                datamodel.ChargeDeclarations = new ChargeDeclarationsModel
                {
                    CurrencyId = step.ChargeDeclarations.CurrencyId,
                    DeclaredValueForCarrige = step.ChargeDeclarations.DeclaredValueForCarrige,
                    DeclaredValueForCustoms = step.ChargeDeclarations.DeclaredValueForCustoms,
                    Id = step.ChargeDeclarations.Id,
                    InsuranceAmount = step.ChargeDeclarations.InsuranceAmount,
                    NCV = step.ChargeDeclarations.NCV,
                    NVD = step.ChargeDeclarations.NVD,
                    OtherChargesType = step.ChargeDeclarations.OtherChargesType,
                    WTVALChargesType = step.ChargeDeclarations.WTVALChargesType,
                    XXX = step.ChargeDeclarations.XXX
                };
                datamodel.ExecutedAtPlace = step.ExecutedAtPlace;
                datamodel.ExecutedBy = step.ExecutedAtPlace;
                datamodel.ExecutedOnDate = step.ExecutedOnDate;
                datamodel.HandlingInformation = step.HandlingInformation;

                datamodel.OtherChargesList = step.OtherChargesList.Select(item =>
                    new ChargeModel
                    {
                        Amount = item.Amount,
                        CodeId = long.Parse(item.CodeId),
                        Id = item.IdToDB,
                        IsCodeDueCarrier = item.IsCodeDueCarrier
                    })
                        .ToList();

                datamodel.RateDescriptionsList = step.RateDescriptionsList.Select(item =>
                    new RateDescriptionModel
                    {
                        Id = item.IdToDB,
                        ChargeableWeight = item.ChargeableWeight,
                        CommodityItemNo = item.CommodityItemNo,
                        GoodsDescription = item.GoodsDescription,
                        GrossWeight = item.GrossWeight,
                        NoOfPieces = item.NoOfPieces,
                        RateCharge = item.RateCharge,
                        RateClass = item.RateClass,
                        Total = item.Total,
                        UOMWeightId = (int)item.UOMWeightId
                    }).ToList();

                datamodel.SpecialHandlingRequests = step.SpecialHandlingRequests.Select(item => new SphCode { Id = item.Id, CodeId = item.CodeId }).ToList();
              //  datamodel.Total = new ChargeType { Collect = step.Total.Collect, Prepaid = step.Total.Prepaid };
             //   datamodel.TotalOtherChargesDueAgent = new ChargeType { Collect = step.TotalOtherChargesDueAgent.Collect, Prepaid = step.TotalOtherChargesDueAgent.Prepaid };
             //   datamodel.TotalOtherChargesDueCarrier = new ChargeType { Collect = step.TotalOtherChargesDueCarrier.Collect, Prepaid = step.TotalOtherChargesDueCarrier.Prepaid };
                datamodel.TotalTaxes = new ChargeType { Collect = step.TotalTaxes.Collect, Prepaid = step.TotalTaxes.Prepaid };
                datamodel.TotalValuationCharges = new ChargeType { Collect = step.TotalValuationCharges.Collect, Prepaid = step.TotalValuationCharges.Prepaid };
             //   datamodel.TotalWeightCharges = new ChargeType { Collect = step.TotalWeightCharges.Collect, Prepaid = step.TotalWeightCharges.Prepaid };

                unit.SaveAwbChargeDeclarations(datamodel, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
            }

            return HWBStep(step.AirwaybillId);
        }

        public JsonResult GetAWBRates([DataSourceRequest] DataSourceRequest request)
        {
            if (TempRateDescrs == null)
                TempRateDescrs = new List<RateDescription>();

            return Json(TempRateDescrs.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

        }

        public PartialViewResult GetRateById(int? id)
        {
            RateDescription model;
            TempDimentions = new List<Dimentions>();

            if (id.HasValue)
            {

                model = TempRateDescrs.Where(c => c.Id == id).First();

                //TempDimentions = model.Dimensions.Select(item =>
                //{
                //    var tmpItem = item;
                //    tmpItem.Id = item.IdToDB;
                //  //  tmpItem.WeightUOM = String.IsNullOrWhiteSpace(tmpItem.WeightUOMId) ? "" : GetUomById(int.Parse(tmpItem.WeightUOMId));
                //    tmpItem.DimUOM = String.IsNullOrWhiteSpace(tmpItem.DimUOMId) ? "" : GetUomById(int.Parse(tmpItem.DimUOMId));
                //    return tmpItem;
                //})
                //    .ToList();// Do not remove this ever
            }
            else
            {
                model = new RateDescription();
                //model.STC = 0;
                // model.Volume = 0;
                model.UOMWeightId = 4;
                // model.Dimensions = TempDimentions;
            }
            return PartialView("_RateAddEdit", model);
        }

        public ActionResult DeleteRate(long id)
        {
            var i = TempRateDescrs.IndexOf(TempRateDescrs.Where(c => c.Id == id).First());
            TempRateDescrs.RemoveAt(i);

            return Json(ModelState.ToDataSourceResult());
        }
        public JsonResult SaveRate(RateDescription item)
        {
            try
            {
                // item.Dimensions = TempDimentions;

                if (item.Id > 0)
                {
                    var i = TempRateDescrs.IndexOf(TempRateDescrs.Where(c => c.Id == item.Id).First());
                    item.UOMWeight = GetUomById(item.UOMWeightId) ?? "";
                    TempRateDescrs[i] = item;
                }
                else
                {
                    item.Id = TempRateDescrs.Max(i => i.Id) ?? 0 + 1;
                    item.UOMWeight = GetUomById(item.UOMWeightId) ?? "";
                    TempRateDescrs.Add(item);
                }
                return Json(new { data = "Ok" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)// some error
            {
                return Json(new { data = "Error", ErrorMessage = "Server Error" }, JsonRequestBehavior.AllowGet);

            }

        }

        public JsonResult FinalizeChargesStep(ChargesStep step)
        {
            SaveChargesStep(step);

            return FinalizeAWB(step.AirwaybillId.Value);
        }


        public JsonResult FinalizeAWB(long awbId)
        {
            using (AwbWizardUnit unit = new AwbWizardUnit())
            {
                unit.FinalizeAWBWizard(awbId, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId);
            }
            return Json(new { data = "Success" }, JsonRequestBehavior.AllowGet);
        }

        //HWB Step
        public PartialViewResult HWBStep(long? id)
        {
            HWBStep model = new HWBStep();
            //For Test Mode
            if (id != null && TempHWBStep != null)
            {
                model = TempHWBStep;
                // For real mode
                //if (id != null)
                //    model = GetFromBackendById;

                // if (model != null)
                //{
            }
            else
            {
                TempHWBs = new List<HWBSimple>();
                TempHWBAddEdits = new List<HWBAddEdit>();
                model.AirwaybillId = id;
            }

            return PartialView("HWBStep", model);
        }

        public PartialViewResult SaveHWBStep(HWBStep step)
        {
            TempHWBStep = step;


            return HWBStep(step.AirwaybillId);
        }

        public PartialViewResult HWBExtraPopup(long? id)
        {
            return PartialView("HWBExtraPopup", id);
        }

        public PartialViewResult HWBAddEdit(long? id, long? awbid)
        {
            HWBAddEdit model = new HWBAddEdit();

            ViewBag.ChargeTypes = new List<SimpleType>(){
                    new SimpleType { Id = 0, Name = "Prepaid" },
                    new SimpleType { Id = 1, Name = "Collect" }
            };


            if (id != null)
            {
                AddEditHwbModel datamodel = new AddEditHwbModel();

                using (AwbWizardUnit unit = new AwbWizardUnit())
                {
                    datamodel = unit.GetHwbModel(id.Value);
                }


                model.Id = datamodel.Id;
                model.AirwaybillId = datamodel.AirwaybillId;
                model.HWBSerialNumber = datamodel.HWBSerialNumber;

                model.OriginId = datamodel.OriginId;
                model.DestinationId = datamodel.DestinationId;

                model.TotalPieces = datamodel.TotalPieces;
                model.TotalWeight = datamodel.TotalWeight;
                model.UOMWeightId = datamodel.UOMWeightId;
                model.SLAC = datamodel.SLAC;

                model.DescriptionOfGoods = datamodel.DescriptionOfGoods;
                model.ExportControlNo = datamodel.ExportControlNo;

                model.OtherCustomsInfo = datamodel.OtherCustomsInfo != null && datamodel.OtherCustomsInfo.Any() ? datamodel.OtherCustomsInfo.Select(item => new CustomInfo
                {
                    CountryCode = item.CountryCode,
                    CountryCodeId = item.CountryCodeId,
                    CustomsInfoIdentifier = item.CustomsInfoIdentifier,
                    CustomsInfoIdentifierId = item.CustomsInfoIdentifierId,
                    Id = item.Id,
                    IdToDB = item.IdToDB,
                    InfoIdentifier = item.InfoIdentifier,
                    InfoIdentifierId = item.InfoIdentifierId,
                    SupplementaryCustomsInfo = item.SupplementaryCustomsInfo
                }).ToList() : new List<CustomInfo>();


                model.Shipper = new Partner
                {
                    Id = datamodel.Shipper.Id,
                    Fax = datamodel.Shipper.Fax,
                    Name = datamodel.Shipper.Name,
                    City = datamodel.Shipper.City,
                    Type = PartnerType.Shipper,
                    Phone = datamodel.Shipper.Phone,
                    Postal = datamodel.Shipper.Postal,
                    StateId = datamodel.Shipper.StateId,
                    Address1 = datamodel.Shipper.Address1,
                    CountryId = datamodel.Shipper.CountryId,
                    AccountNumber = datamodel.Shipper.AccountNumber
                };
                model.Consignee = new Partner
                {
                    Id = datamodel.Consignee.Id,
                    Fax = datamodel.Consignee.Fax,
                    Name = datamodel.Consignee.Name,
                    City = datamodel.Consignee.City,
                    Type = PartnerType.Consignee,
                    Phone = datamodel.Consignee.Phone,
                    Postal = datamodel.Consignee.Postal,
                    StateId = datamodel.Consignee.StateId,
                    Address1 = datamodel.Consignee.Address1,
                    CountryId = datamodel.Consignee.CountryId,
                    AccountNumber = datamodel.Consignee.AccountNumber,
                };


                model.HarmonizedCommodityCodes = datamodel.HarmonizedCommodityCodes.Select(a => new Code
                {
                    Id = a.Id,
                    CodeId = a.CodeId,
                    CodeNote = a.CodeNote
                }).ToList();


                model.ChargeDeclarations = new ChargeDeclarations
                {
                    Id = datamodel.ChargeDeclarations.Id,
                    CurrencyId = datamodel.ChargeDeclarations.CurrencyId,
                    NCV = datamodel.ChargeDeclarations.NCV,
                    NVD = datamodel.ChargeDeclarations.NVD,
                    XXX = datamodel.ChargeDeclarations.XXX,
                    DeclaredValueForCarrige = datamodel.ChargeDeclarations.DeclaredValueForCarrige,
                    DeclaredValueForCustoms = datamodel.ChargeDeclarations.DeclaredValueForCustoms,
                    WTVALChargesType = datamodel.ChargeDeclarations.WTVALChargesType,
                    OtherChargesType = datamodel.ChargeDeclarations.OtherChargesType,
                    InsuranceAmount = datamodel.ChargeDeclarations.InsuranceAmount
                };


                // Till the end Part For Ui fit after getting from Backend, DO Not Remove!!

                using (CommonUnit unitOfWork = new CommonUnit())
                {
                    model.AWBNumber = unitOfWork.GetAwbNumberById(awbid.Value);
                }



                //Do not remove this ever
                var hmcount = 9 - model.HarmonizedCommodityCodes.Count();
                for (int i = 0; i < hmcount; i++)
                {
                    Code code = new Code();
                    model.HarmonizedCommodityCodes.Add(code);
                }

                //Do not remove this ever
                TempOtherCustomsInfo = model.OtherCustomsInfo.Select(item =>
                {
                    var tmpItem = item;
                    tmpItem.Id = item.IdToDB;
                    tmpItem.CountryCode = String.IsNullOrWhiteSpace(tmpItem.CountryCodeId) ? "" : GetCountryCodeNameById(int.Parse(tmpItem.CountryCodeId));
                    tmpItem.InfoIdentifier = String.IsNullOrWhiteSpace(tmpItem.InfoIdentifierId) ? "" : GetInfoIdentifierNameById(int.Parse(tmpItem.InfoIdentifierId));
                    tmpItem.CustomsInfoIdentifier = String.IsNullOrWhiteSpace(tmpItem.CustomsInfoIdentifierId) ? "" : GetCustomsInfoIdentifierNameById(int.Parse(tmpItem.CustomsInfoIdentifierId));
                    return tmpItem;
                }).ToList(); // Do not remove this ever

            }
            else
            {
                model = InitializeHWBAddEditDummyModel(awbid.Value);
            }

            return PartialView("HWBAddEdit", model);
        }


        private HWBAddEdit InitializeHWBAddEditDummyModel(long awbid)
        {
            HWBAddEdit model = new HWBAddEdit();

            model.AirwaybillId = awbid;
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                model.AWBNumber = unitOfWork.GetAwbNumberById(awbid);
            }


            TempOtherCustomsInfo = new List<CustomInfo>();  // Do not remove this ever

            model.Shipper = new Partner() { Type = PartnerType.Shipper };
            model.Consignee = new Partner() { Type = PartnerType.Consignee };
            model.UOMWeightId = 4;

            var chanrgesDec = new ChargeDeclarations();

            chanrgesDec.CurrencyId = 1;
            chanrgesDec.DeclaredValueForCarrige = 0;
            chanrgesDec.DeclaredValueForCustoms = 0;
            chanrgesDec.InsuranceAmount = 0;
            chanrgesDec.WTVALChargesType = 0;
            chanrgesDec.OtherChargesType = 0;
            chanrgesDec.NVD = true;
            chanrgesDec.NCV = true;
            chanrgesDec.XXX = true;

            model.ChargeDeclarations = chanrgesDec;
            model.OtherCustomsInfo = new List<CustomInfo>();


            model.HarmonizedCommodityCodes = new List<Code>();
            for (int i = 0; i < 9; i++)
            {
                Code code = new Code();
                model.HarmonizedCommodityCodes.Add(code);
            }

            return model;
        }

        public ActionResult DeleteHWB(long id, long awbid)
        {

            using (AwbWizardUnit unit = new AwbWizardUnit())
            {
                unit.DeleteAwbsHWB(awbid, id);
            }

            return Json(ModelState.ToDataSourceResult());
        }
        public JsonResult SaveHWBAddEdit(HWBAddEdit step)
        {
            step.HarmonizedCommodityCodes = step.HarmonizedCommodityCodes.Where(s => s.CodeId != null).ToList();
            step.OtherCustomsInfo = TempOtherCustomsInfo;

            // For Real 
            // Save HWB (step)
            using (AwbWizardUnit unit = new AwbWizardUnit())
            {
                long tmpAwbId = unit.SaveHWB(step.ToBackendModel(), SessionManager.UserInfo.DefaultAccountId.Value, SessionManager.UserInfo.UserLocalId);
                if (tmpAwbId > 0)
                {
                    return Json(new { data = "Ok" }, JsonRequestBehavior.AllowGet);
                }
                else // some error
                {
                    if (tmpAwbId == -2) // duplicate
                    {
                        string tmpMessage = String.Format("HWB {0}-{1}-{2} already exists", GetPortNameById(step.OriginId.Value), step.HWBSerialNumber, GetPortNameById(step.DestinationId.Value));
                        return Json(new { data = "Error", ErrorMessage = tmpMessage }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { data = "Error", ErrorMessage = "Server Error" }, JsonRequestBehavior.AllowGet);

                }


            }


        }

        // This method is just for test mode, should be removed at all
        private HWBSimple ConvertHWBAddEditToHWBSimple(HWBAddEdit step)
        {
            var hwbsm = new HWBSimple
            {
                Id = step.Id.Value,
                Consignee = step.Consignee.Name,
                Destination = GetPortNameById(step.DestinationId.Value),
                ExportControl = step.ExportControlNo,
                Number = step.HWBSerialNumber,
                Origin = GetPortNameById(step.OriginId.Value),
                Pieces = step.TotalPieces.ToString(),
                Shipper = step.Shipper.Name,
                SLAC = step.SLAC.ToString(),
                Weight = step.TotalWeight + " " + GetUomById(step.UOMWeightId.Value)


            };

            return hwbsm;

        }

        public JsonResult PreviewAWB(long id)
        {
            return Json(new { url = "http://awbeditor.com/pdf/awb.pdf" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PreviewHWB(long id)
        {
            return Json(new { url = "http://www.cargologisticsgroup.com/pdf/HAWB.pdf" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        // To Remove Lara
        //public PartialViewResult ULDsStep(long? id)
        //{
        //    TempUlds = new List<UnitLoadDevice>();
        //    TempDimentions = new List<Dimentions>();

        //    ULDsAndDimensionsStep model = new ULDsAndDimensionsStep();
        //    AwbUldsDimensions dataModel;
        //    using (var unit = new AwbWizardUnit())
        //    {
        //        dataModel = unit.GetAwbUldDims(id);
        //    }

        //    if (dataModel != null)
        //    {
        //        model.SLAC = dataModel.Slac ?? 0;
        //        model.AirwaybillId = dataModel.AwbId;
        //        model.Volume = dataModel.Volume ?? 0;
        //        model.VolumeUOMId = dataModel.VolumeUOMId;
        //        model.GoodsDescriptionList = dataModel.GoodsDescriptions.Select(g => new Dimentions
        //        {
        //            IdToDB = g.Id,
        //            Length = g.Length,
        //            Pieces = g.Pieces,
        //            DimUOMId = g.DimUOMId.ToString(),
        //            Height = g.Height,
        //            //Weight = g.Weight,
        //            //WeightUOMId = g.WeightUOMId.ToString(),
        //            Width = g.Width
        //        }).ToList();
        //        model.UnitLoadDevicesList = dataModel.UnitLoadDevicess.Select(u => new UnitLoadDevice
        //        {
        //            IdToDB = u.Id,
        //            OwnerId = u.OwnerId.ToString(),
        //            Said = u.Said,
        //            ULDNo = u.ULDNo,
        //            ULDTypeId = u.ULDTypeId.ToString()
        //        }).ToList();


        //        TempUlds = model.UnitLoadDevicesList.Select(item =>
        //        {
        //            var tmpItem = item;
        //            tmpItem.Id = item.IdToDB;
        //            tmpItem.ULDType = String.IsNullOrWhiteSpace(tmpItem.ULDTypeId) ? "" : GetUldTypeNameById(int.Parse(tmpItem.ULDTypeId));
        //            tmpItem.Owner = String.IsNullOrWhiteSpace(tmpItem.OwnerId) ? "" : GetCarrierCodeById(int.Parse(tmpItem.OwnerId));
        //            return tmpItem;
        //        }).ToList(); // Do not remove this ever

        //        TempDimentions = model.GoodsDescriptionList.Select(item =>
        //        {
        //            var tmpItem = item;
        //            tmpItem.Id = item.IdToDB;
        //            // tmpItem.WeightUOM = String.IsNullOrWhiteSpace(tmpItem.WeightUOMId) ? "" : GetUomById(int.Parse(tmpItem.WeightUOMId));
        //            tmpItem.DimUOM = String.IsNullOrWhiteSpace(tmpItem.DimUOMId) ? "" : GetUomById(int.Parse(tmpItem.DimUOMId));
        //            return tmpItem;
        //        })
        //            .ToList();// Do not remove this ever
        //    }
        //    else
        //    {
        //        model.AirwaybillId = id;
        //        model.SLAC = 0;
        //        model.Volume = 0;
        //        model.UnitLoadDevicesList = TempUlds;
        //        model.GoodsDescriptionList = TempDimentions;
        //    }

        //    return PartialView("ULDsStep", model);
        //}

        //public PartialViewResult SaveULDsStep(ULDsAndDimensionsStep step)
        //{
        //    step.GoodsDescriptionList = TempDimentions;
        //    step.UnitLoadDevicesList = TempUlds;

        //    var goodsDescriptionList = ConvertToXml<Dimentions>(step.GoodsDescriptionList);
        //    var unitLoadDevicesList = ConvertToXml<UnitLoadDevice>(step.UnitLoadDevicesList);

        //    using (var unit = new AwbWizardUnit())
        //    {
        //        unit.SaveAwbUldDims(step.AirwaybillId.Value, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value,
        //            goodsDescriptionList, unitLoadDevicesList, step.SLAC, step.Volume, step.VolumeUOMId);
        //    }

        //    return SummaryStep(step.AirwaybillId);
        //}

        //public PartialViewResult SummaryStep(long? id)
        //{
        //    SummaryStep model = new SummaryStep();

        //    using (AwbWizardUnit unit = new AwbWizardUnit())
        //    {
        //        var dataModel = unit.GetAwbWizardsSummary(id.Value);
        //        model.AirwaybillTypeId = dataModel.AirwaybillType;

        //        model.AirportId = dataModel.AirportId;
        //        model.AirwaybillId = dataModel.AwbId;
        //        model.CertificationName = dataModel.CertificationName;
        //        model.ExecutedAtPlace = dataModel.ExecutedAtPlace;
        //        model.ExecutedBy = dataModel.ExecutedBy;
        //        model.ExecutedOnDate = dataModel.ExecutedOnDate.Value;
        //        model.ParticipantCode = dataModel.ParticipantCode;
        //        model.ParticipantIdentfierId = dataModel.ParticipantIdentfierId;

        //    }
        //    return PartialView("SummaryStep", model);

        //}

        //public PartialViewResult SaveSummaryStep(SummaryStep step)
        //{
        //    SaveSummaryData(step);

        //    return HWBStep(step.AirwaybillId);
        //}

        //public JsonResult FinalizeSummaryStep(SummaryStep step)
        //{
        //    SaveSummaryData(step);

        //    return FinalizeAWB(step.AirwaybillId.Value);
        //}
        //private void SaveSummaryData(SummaryStep step)
        //{
        //    using (AwbWizardUnit unit = new AwbWizardUnit())
        //    {
        //        long? awbId = unit.SaveAwbWizarsSummary(new AwbSummaryStepData
        //        {
        //            AirportId = step.AirportId,
        //            AwbId = step.AirwaybillId,
        //            CertificationName = step.CertificationName,
        //            ExecutedAtPlace = step.ExecutedAtPlace,
        //            ExecutedBy = step.ExecutedBy,
        //            ExecutedOnDate = step.ExecutedOnDate,
        //            ParticipantCode = step.ParticipantCode,
        //            ParticipantIdentfierId = step.ParticipantIdentfierId,
        //            UserId = SessionManager.UserInfo.UserLocalId,
        //            WarehouseId = SessionManager.UserInfo.DefaultWarehouseId
        //        });
        //    }
        //}


        #region Grids CRUD

        // HWBs grid
        public JsonResult GetAWBHWBs([DataSourceRequest] DataSourceRequest request, long id, string searchText)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpQuery = commonUnit.Context.AWBs_HWBs.Where(item => item.AWBId == id).Select(item => item.HWB).Select(item => new HWBSimple
                {
                    Consignee = item.Customer1.Name,
                    Destination = item.Port1.IATACode,
                    ExportControl = item.ExportControl,
                    Id = item.Id,
                    Number = item.HWBSerialNumber,
                    Origin = item.Port.IATACode,
                    Pieces = item.Pieces.ToString(),
                    Shipper = item.Customer.Name,
                    SLAC = item.SLAC.ToString(),
                    Weight = item.Weight + " " + item.UOM.TwoCharCode
                });

                if (!String.IsNullOrWhiteSpace(searchText))
                {
                    tmpQuery = tmpQuery.Where(i => i.Consignee.Contains(searchText) ||
                                                i.Shipper.Contains(searchText) ||
                                                i.Number.Contains(searchText) ||
                                                i.Origin.Contains(searchText) ||
                                                i.Destination.Contains(searchText) ||
                                                i.ExportControl.Contains(searchText) ||
                                                i.Pieces.Contains(searchText) ||
                                                i.SLAC.Contains(searchText) ||
                                                i.Weight.Contains(searchText)
                                                );
                }


                return Json(tmpQuery.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }

        }

        //OtherCustomsInfo grid
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OtherCustomsInfoInline_Create([DataSourceRequest] DataSourceRequest request, CustomInfo criteria)
        {

            int ddid;

            criteria.Id = TempOtherCustomsInfo.Max(item => item.Id) ?? +1;
            criteria.CountryCode = int.TryParse(criteria.CountryCodeId, out ddid) ? GetCountryCodeNameById(ddid) : "";
            criteria.InfoIdentifier = int.TryParse(criteria.InfoIdentifierId, out ddid) ? GetInfoIdentifierNameById(ddid) : "";
            criteria.CustomsInfoIdentifier = int.TryParse(criteria.CustomsInfoIdentifierId, out ddid) ? GetCustomsInfoIdentifierNameById(ddid) : "";

            TempOtherCustomsInfo.Add(criteria);

            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OtherCustomsInfoInline_Destroy([DataSourceRequest] DataSourceRequest request, CustomInfo criteria)
        {
            var i = TempOtherCustomsInfo.IndexOf(TempOtherCustomsInfo.Where(c => c.Id == criteria.Id).First());
            TempOtherCustomsInfo.RemoveAt(i);
            return Json(ModelState.ToDataSourceResult());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OtherCustomsInfoInline_Update([DataSourceRequest] DataSourceRequest request, CustomInfo criteria)
        {
            int ddid;
            var i = TempOtherCustomsInfo.IndexOf(TempOtherCustomsInfo.Where(c => c.Id == criteria.Id).First());
            criteria.CountryCode = int.TryParse(criteria.CountryCodeId, out ddid) ? GetCountryCodeNameById(ddid) : "";
            criteria.InfoIdentifier = int.TryParse(criteria.InfoIdentifierId, out ddid) ? GetInfoIdentifierNameById(ddid) : "";
            criteria.CustomsInfoIdentifier = int.TryParse(criteria.CustomsInfoIdentifierId, out ddid) ? GetCustomsInfoIdentifierNameById(ddid) : "";
            TempOtherCustomsInfo[i] = criteria;
            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }



        //ULDs grid
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ULDsInline_Create([DataSourceRequest] DataSourceRequest request, UnitLoadDevice criteria)
        {
            int ddid;
            criteria.Id = TempUlds.Max(item => item.Id) ?? 0 + 1;
            criteria.ULDType = int.TryParse(criteria.ULDTypeId, out ddid) ? GetUldTypeNameById(ddid) : "";
            criteria.Owner = int.TryParse(criteria.OwnerId, out ddid) ? GetCarrierCodeById(ddid) : "";
            TempUlds.Add(criteria);

            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ULDsInline_Destroy([DataSourceRequest] DataSourceRequest request, UnitLoadDevice criteria)
        {
            var i = TempUlds.IndexOf(TempUlds.Where(c => c.Id == criteria.Id).First());
            TempUlds.RemoveAt(i);
            return Json(ModelState.ToDataSourceResult());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ULDsInline_Update([DataSourceRequest] DataSourceRequest request, UnitLoadDevice criteria)
        {
            int ddid;
            var i = TempUlds.IndexOf(TempUlds.Where(c => c.Id == criteria.Id).First());
            criteria.ULDType = int.TryParse(criteria.ULDTypeId, out ddid) ? GetUldTypeNameById(ddid) : "";
            criteria.Owner = int.TryParse(criteria.OwnerId, out ddid) ? GetCarrierCodeById(ddid) : "";
            TempUlds[i] = criteria;
            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }

        //Goods grid
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GoodsInline_Create([DataSourceRequest] DataSourceRequest request, Dimentions criteria)
        {
            int ddid;
            criteria.Id = TempDimentions.Max(item => item.Id) ?? 0 + 1;
            //  criteria.WeightUOM = int.TryParse(criteria.WeightUOMId, out ddid) ? GetUomById(ddid) : "";
            criteria.DimUOM = int.TryParse(criteria.DimUOMId, out ddid) ? GetUomById(ddid) : "";
            TempDimentions.Add(criteria);

            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GoodsInline_Destroy([DataSourceRequest] DataSourceRequest request, Dimentions criteria)
        {
            var i = TempDimentions.IndexOf(TempDimentions.Where(c => c.Id == criteria.Id).First());
            TempDimentions.RemoveAt(i);
            return Json(ModelState.ToDataSourceResult());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GoodsInline_Update([DataSourceRequest] DataSourceRequest request, Dimentions criteria)
        {
            int ddid;
            var i = TempDimentions.IndexOf(TempDimentions.Where(c => c.Id == criteria.Id).First());
            // criteria.WeightUOM = int.TryParse(criteria.WeightUOMId, out ddid) ? GetUomById(ddid) : "";
            criteria.DimUOM = int.TryParse(criteria.DimUOMId, out ddid) ? GetUomById(ddid) : "";
            TempDimentions[i] = criteria;
            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }


        //Rates grid
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult RateInline_Create([DataSourceRequest] DataSourceRequest request, RateDescription criteria)
        //{
        //    int ddid;
        //    criteria.Id = TempRateDescrs.Max(item => item.Id) ?? 0 + 1;
        //    criteria.UOMWeight = int.TryParse(criteria.UOMWeightId, out ddid) ? GetUomById(ddid) : "";
        //    TempRateDescrs.Add(criteria);

        //    return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult RateInline_Destroy([DataSourceRequest] DataSourceRequest request, RateDescription criteria)
        //{
        //    var i = TempRateDescrs.IndexOf(TempRateDescrs.Where(c => c.Id == criteria.Id).First());
        //    TempRateDescrs.RemoveAt(i);
        //    return Json(ModelState.ToDataSourceResult());
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult RateInline_Update([DataSourceRequest] DataSourceRequest request, RateDescription criteria)
        //{
        //    int ddid;
        //    var i = TempRateDescrs.IndexOf(TempRateDescrs.Where(c => c.Id == criteria.Id).First());
        //    criteria.UOMWeight = int.TryParse(criteria.UOMWeightId, out ddid) ? GetUomById(ddid) : "";
        //    TempRateDescrs[i] = criteria;
        //    return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        //}

        // Charges Grid

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChargeInline_Create([DataSourceRequest] DataSourceRequest request, SimpleCharge criteria)
        {
            int ddid;
            criteria.Id = TempOtherCharges.Max(item => item.Id) ?? 0 + 1;
            var tempcrit = int.TryParse(criteria.CodeId, out ddid) ? GetChargeCodeById(ddid) : null;
            criteria.Code = tempcrit == null ? "" : tempcrit.Code;
            criteria.IsCodeDueCarrier = tempcrit == null ? false : tempcrit.IsCodeDueCarrier;

            TempOtherCharges.Add(criteria);
            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChargeInline_Destroy([DataSourceRequest] DataSourceRequest request, SimpleCharge criteria)
        {
            var i = TempOtherCharges.IndexOf(TempOtherCharges.Where(c => c.Id == criteria.Id).First());
            TempOtherCharges.RemoveAt(i);
            return Json(ModelState.ToDataSourceResult());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChargeInline_Update([DataSourceRequest] DataSourceRequest request, SimpleCharge criteria)
        {
            var i = TempOtherCharges.IndexOf(TempOtherCharges.Where(c => c.Id == criteria.Id).First());

            int ddid;
            var tempcrit = int.TryParse(criteria.CodeId, out ddid) ? GetChargeCodeById(ddid) : null;
            criteria.Code = tempcrit == null ? "" : tempcrit.Code;
            criteria.IsCodeDueCarrier = tempcrit == null ? false : tempcrit.IsCodeDueCarrier;

            TempOtherCharges[i] = criteria;
            return Json(new[] { criteria }.ToDataSourceResult(request, ModelState));
        }


        #endregion

        #region Methots for DD

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetChargeCodesForTemplate()
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpListQuery = commonUnit.GetChargeCodes();
                var tmpList = tmpListQuery.Select(item => new { Id = item.Id.ToString(), Code = item.Code, IsCodeDueCarrier = item.IsCodeDueCarrier }).OrderBy(i => i.Code).ToList();

                return Json(tmpList, JsonRequestBehavior.AllowGet);
            }

        }

        public SimpleCharge GetChargeCodeById(long id)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpPortQuery = commonUnit.GetChargeCodes();
                var tmpItem = tmpPortQuery.Where(item => item.Id == id).FirstOrDefault();

                return tmpItem == null ? null : new SimpleCharge { Code = tmpItem.Code, IsCodeDueCarrier = tmpItem.IsCodeDueCarrier };
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetPortsForTemplate()
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpPortQuery = commonUnit.GetPorts();
                var tmpStations = tmpPortQuery.Select(item => new { Name = item.IATACode, Id = item.Id.ToString() }).OrderBy(i => i.Name).ToList();

                return Json(tmpStations, JsonRequestBehavior.AllowGet);
            }

        }

        public string GetPortNameById(long id)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpPortQuery = commonUnit.GetPorts();
                var tmpStation = tmpPortQuery.Where(item => item.Id == id).FirstOrDefault();

                return tmpStation == null ? "" : tmpStation.IATACode;
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetUldTypesForTemplate()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItems = unitOfWork.GetShipmentUnitTypes().Select(item => new { Name = (item.Code), Id = item.Id.ToString() }).OrderBy(i => i.Name);

                return Json(tmpItems.ToList(), JsonRequestBehavior.AllowGet);
            }
        }
        public string GetUldTypeNameById(long id)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpULDType = unitOfWork.GetShipmentUnitTypes().Where(i => i.Id == id).FirstOrDefault();
                return tmpULDType == null ? "" : tmpULDType.Code;
            }
        }

        public JsonResult GetCarrierListForTemplate()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarriers = unitOfWork.GetCarrierList(1).Where(i => i.CarrierCode != null).Select(item => new { Name = (item.CarrierCode), Id = item.Id.ToString() }).OrderBy(i => i.Name);

                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public string GetCarrierCodeById(long id)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarrier = unitOfWork.GetCarrierList(1).Where(item => item.Id == id).FirstOrDefault();

                return tmpCarrier == null ? "" : tmpCarrier.CarrierCode;
            }
        }

        public JsonResult GetCarrierList()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarriers = unitOfWork.GetCarrierList(1).Where(i => i.CarrierCode != null).Select(item => new
                {
                    Name = (item.CarrierCode),
                    Id = item.Id,
                    Carrier3Code = item.Carrier3Code
                }).OrderBy(i => i.Name);

                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAccountCarrierList()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCarriers = unitOfWork.GetAccountCarriers(SessionManager.UserInfo.DefaultAccountId).Where(i => i.CarrierCode != null).Select(item => new
                {
                    Name = (item.CarrierCode),
                    Id = item.AccountId,
                    Carrier3Code = item.Carrier3Code
                }).OrderBy(i => i.Name);

                return Json(tmpCarriers.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetWeightUOMsForTemplate()
        {

            using (CommonUnit unit = new CommonUnit())
            {
                var tmpUOMQuery = unit.Context.UOMs.Where(u => u.UOMTypeId == 1);

                var tmpUoms = tmpUOMQuery.Select(item => new { Name = item.TwoCharCode, Id = item.Id.ToString() });

                return Json(tmpUoms.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDimUOMsForTemplate()
        {

            using (CommonUnit unit = new CommonUnit())
            {
                var tmpUOMQuery = unit.Context.UOMs.Where(u => u.UOMTypeId == 3);

                var tmpUoms = tmpUOMQuery.Select(item => new { Name = item.TwoCharCode, Id = item.Id.ToString() });

                return Json(tmpUoms.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public string GetUomById(long id)
        {
            using (CommonUnit unit = new CommonUnit())
            {
                var tmpUOMQuery = unit.Context.UOMs.Where(u => u.Id == id).FirstOrDefault();

                return tmpUOMQuery == null ? "" : tmpUOMQuery.TwoCharCode;
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetAccountingCodes()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpCodes = unitOfWork.GetAccountingCodes().Select(item => new { Name = item.Code, Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpCodes.ToList(), JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult GetCurrencies()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpList = unitOfWork.GetCurrencies().Where(i => i.Code != null).OrderBy(a => a.Id)
                     .Select(item => new { Code = (item.Code), Id = item.Id, Symbol = item.Symbol }).ToList();

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetVolumeUOMs()
        {

            using (CommonUnit unit = new CommonUnit())
            {
                var tmpUOMQuery = unit.Context.UOMs.Where(u => u.UOMTypeId == 2);

                var tmpUoms = tmpUOMQuery.Select(item => new { Name = item.TwoCharCode, Id = item.Id });

                return Json(tmpUoms.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetCustomsInfoIdentifiers()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpList = unitOfWork.GetCustomsInfoIdentifiers().Select(item => new { Name = item.Code, Id = item.Id.ToString() }).OrderBy(i => i.Name);

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public string GetCustomsInfoIdentifierNameById(long id)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItem = unitOfWork.GetCustomsInfoIdentifiers().Where(i => i.Id == id).FirstOrDefault();
                return tmpItem == null ? "" : tmpItem.Code;
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetInfoIdentifiers()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpList = unitOfWork.GetInfoIdentifiers().Select(item => new { Name = item.Code, Id = item.Id.ToString() }).OrderBy(i => i.Name);

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public string GetInfoIdentifierNameById(long id)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItem = unitOfWork.GetInfoIdentifiers().Where(i => i.Id == id).FirstOrDefault();
                return tmpItem == null ? "" : tmpItem.Code;
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetCountryCodes()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpList = unitOfWork.GetCountryCodes().Select(item => new { Name = item.Code, Id = item.Id.ToString() }).OrderBy(i => i.Name);

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public string GetCountryCodeNameById(long id)
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpItem = unitOfWork.GetCountryCodes().Where(i => i.Id == id).FirstOrDefault();
                return tmpItem == null ? "" : tmpItem.Code;
            }
        }

        public JsonResult GetAwbs()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpList = unitOfWork.GetAwbs().Select(item => new { Name = (item.SearchText), Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAwbTypes()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                List<SimpleListItem> tmpList = new List<SimpleListItem>();
                tmpList = Enum.GetValues(typeof(AWBTypes)).Cast<AWBTypes>().Select(en => new SimpleListItem { Id = (int)en, Name = en.ToString() }).ToList();

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetHarmonizedCodes()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpList = unitOfWork.GetHarmonizedCodes().Select(item => new { Name = (item.Code), Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "none")]
        public JsonResult GetSpecialHandlings()
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpList = unitOfWork.GetSpecialHandlings().Select(item => new { Name = (item.Code), Id = item.Id }).OrderBy(i => i.Name);

                return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetWeightUOMs()
        {

            using (CommonUnit unit = new CommonUnit())
            {
                var tmpUOMQuery = unit.Context.UOMs.Where(u => u.UOMTypeId == 1);

                var tmpUoms = tmpUOMQuery.Select(item => new { Name = item.TwoCharCode, Id = item.Id });

                return Json(tmpUoms.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        AWBWizardSteps GetAWBWizardNextStep(AwbWizardStep backendStep)
        {
            AWBWizardSteps step;
            switch (backendStep)
            {
                case AwbWizardStep.DetailsCompleted:
                    step = AWBWizardSteps.ChargesStep;
                    break;
                case AwbWizardStep.ChargesCompleted:
                    step = AWBWizardSteps.HWBStep;
                    break;
                //case AwbWizardStep.ULDsDimsCompleted:
                //    step = AWBWizardSteps.SummaryStep;
                //    break;
                //case AwbWizardStep.SummaryCompleted:
                //    step = AWBWizardSteps.HWBStep;
                //    break;
                case AwbWizardStep.HWBSEntryInProgress:
                    step = AWBWizardSteps.HWBStep;
                    break;
                case AwbWizardStep.EntryCompleted:
                    step = AWBWizardSteps.AWBStep;
                    break;
                default:
                    step = AWBWizardSteps.AWBStep;
                    break;
            }

            return step;

        }

        #endregion

        #region Session Grids

        private List<CustomInfo> TempOtherCustomsInfo
        {
            get
            {
                if ((List<CustomInfo>)Session["TempOtherCustomsInfo"] == null)
                {
                    Session["TempOtherCustomsInfo"] = new List<CustomInfo>();
                }

                return (List<CustomInfo>)Session["TempOtherCustomsInfo"];
            }
            set
            {
                Session["TempOtherCustomsInfo"] = value;
            }
        }

        private List<RateDescription> TempRateDescrs
        {
            get
            {
                if ((List<RateDescription>)Session["TempRateDescrs"] == null)
                {
                    Session["TempRateDescrs"] = new List<RateDescription>();
                }

                return (List<RateDescription>)Session["TempRateDescrs"];
            }
            set
            {
                Session["TempRateDescrs"] = value;
            }
        }

        private List<SimpleCharge> TempOtherCharges
        {
            get
            {
                if ((List<SimpleCharge>)Session["TempOtherCharges"] == null)
                {
                    Session["TempOtherCharges"] = new List<SimpleCharge>();
                }

                return (List<SimpleCharge>)Session["TempOtherCharges"];
            }
            set
            {
                Session["TempOtherCharges"] = value;
            }
        }

        private List<Dimentions> TempDimentions
        {
            get
            {
                if ((List<Dimentions>)Session["TempGoodsDescrs"] == null)
                {
                    Session["TempGoodsDescrs"] = new List<Dimentions>();
                }

                return (List<Dimentions>)Session["TempGoodsDescrs"];
            }
            set
            {
                Session["TempGoodsDescrs"] = value;
            }
        }

        private List<UnitLoadDevice> TempUlds
        {
            get
            {
                if ((List<UnitLoadDevice>)Session["TempUlds"] == null)
                {
                    Session["TempUlds"] = new List<UnitLoadDevice>();
                }

                return (List<UnitLoadDevice>)Session["TempUlds"];
            }
            set
            {
                Session["TempUlds"] = value;
            }
        }

        private List<HWBSimple> TempHWBs
        {
            get
            {
                if ((List<HWBSimple>)Session["TempHWBs"] == null)
                {
                    Session["TempHWBs"] = new List<HWBSimple>();
                }

                return (List<HWBSimple>)Session["TempHWBs"];
            }
            set
            {
                Session["TempHWBs"] = value;
            }
        }

        #endregion

        #region Session Steps

        private HWBStep TempHWBStep
        {
            get
            {
                return (HWBStep)Session["TempHWBStep"];
            }
            set
            {
                Session["TempHWBStep"] = value;
            }
        }

        private List<HWBAddEdit> TempHWBAddEdits
        {
            get
            {
                if ((List<HWBAddEdit>)Session["TempHWBAddEdits"] == null)
                {
                    Session["TempHWBAddEdits"] = new List<HWBAddEdit>();
                }

                return (List<HWBAddEdit>)Session["TempHWBAddEdits"];
            }
            set
            {
                Session["TempHWBAddEdits"] = value;
            }
        }


        #endregion


    }
}
