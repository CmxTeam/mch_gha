﻿using MCH.BLL.Model.CargoAcceptance;
using MCH.BLL.Units;
using MCH.Web.Models.CargoAcceptance;
using MCH.Web.Models.Screening;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class CargoAcceptanceController : Controller
    {

        #region Wizard Steps

        //wizard step dispatcher
        public PartialViewResult GetCargoAcceptanceView()
        {
            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                var tmpWizardHeader = unit.GetWizardHeader(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);

                if (tmpWizardHeader != null)
                {
                    switch (tmpWizardHeader.Status)
                    {
                        case ShipperManifestStatus.ShipperInfoVerified:
                            {
                                return this.GetDriversView();
                            }
                        case ShipperManifestStatus.DriverInfoVerified:
                            {
                                return this.GetAWBView(tmpWizardHeader.CurrentAwbId);
                            }
                        case ShipperManifestStatus.AWBDetailsVerified:
                        case ShipperManifestStatus.AWBSpecialHandlingVerified:
                        case ShipperManifestStatus.AWBContactInfoVeriried:
                        case ShipperManifestStatus.AWBScreeningVerified:
                        case ShipperManifestStatus.AWBEntryCompleted:
                            {
                                return this.GetAWBView(tmpWizardHeader.CurrentAwbId);
                            }
                        case ShipperManifestStatus.DocsFinalized:
                        case ShipperManifestStatus.Canceled:
                            {
                                return this.GetCarrierShipperView();
                            }
                        default:
                            {
                                return this.GetCarrierShipperView();
                            }
                    }
                }
                else
                {
                    return this.GetCarrierShipperView();
                }
            }
        }

        //step 1
        public PartialViewResult GetCarrierShipperView()
        {
            var acceptCarrierShipper = new AcceptCarrierShipper();
            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                var data = unit.GetShipperManifestForUserIfAny(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
                if (data != null)
                {
                    acceptCarrierShipper = new AcceptCarrierShipper()
                    {
                        NeedsRestor = 1,
                        AccountCarrierId = data.AccountId,
                        FlightTypeId = data.AircraftTypeId,
                        ShipperTypeId = data.ShipperTypeId.Value,
                        ApprovedTSACarrierId = data.ShipperTypeId.Value == 5 ? data.ShipperId : null,
                        ApprovedIACId = data.ShipperTypeId.Value == 3 || data.ShipperTypeId.Value == 4 ? data.ShipperId : null,
                        ApprovedKnownShipperId = data.ShipperTypeId.Value == 1 ? data.ShipperId : null,
                        ShipperName = data.ShipperName,
                        SignImageData = data.ShipperConsentSignature,
                        IsDocumentVerified = data.IsDocumentVerified,
                        GovAuthIdVerified = data.GovCredTypeId,
                        ShipperVerified = data.ShipperStatusVerified,
                        Documents = data.Docs.Select(d => new Models.Common.AttachmentModel
                        {
                            Id = d.Id,
                            ShipperDocTypeId = d.ShipperDocTypeId,
                            DocumentType = d.DocumentType,
                            IconKey = d.IconKey,
                            IsSnapshot = d.IsSnapshot,
                            Name = d.Name,
                            Path = d.Path,
                            SnapshotData = d.SnapshotData
                        }).ToList()
                    };
                }
                PopulateWizardHeader(acceptCarrierShipper);
            }

            return PartialView("_wndCargoAcceptance", acceptCarrierShipper);
        }

        //step 2
        public PartialViewResult GetDriversView()
        {
            var tmpDriverModel = new ACADriverModel();

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                var item = unit.GetDriverSecurityLog(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
                if (item != null)
                {
                    tmpDriverModel = new ACADriverModel
                    {
                        DriverId = item.DriverId,
                        DriverLicense = item.DriverLicense,
                        StateId = item.StateId,
                        FirstIDTypeId = item.FirstIDTypeId,
                        SecondIDTypeId = item.SecondIDTypeId,
                        FirstIDMatched = item.FirstIDMatched,
                        SecondIDMatched = item.SecondIDMatched,
                        TruckingCompId = item.TruckingCompId,
                        CapturePhoto = item.CapturePhoto,
                        CaptureID = item.CaptureID,
                        ID = item.ShipperManifestId,
                        STANumber = item.STANumber,
                        CarrierId = item.AccountId,
                        CarrierName = item.AccountName,
                        ShipperTypeName = item.ShipperTypeName,
                        CarrierShipperName = item.ShipperName
                    };
                }
                PopulateWizardHeader(tmpDriverModel);
            }

            return PartialView("_wndACADriver", tmpDriverModel);
        }

        //step 3
        public PartialViewResult GetAWBView(long? awbId)
        {
            AcaAwbModel tmpModel = new AcaAwbModel();
            tmpModel.CurrentAwbId = awbId;

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                var data = unit.GetShipperManifestAwbs(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
                tmpModel.Awbs = data.Select(d => new AcaAwbDetialsModel()
                {
                    Id = d.Id,
                    AwbSerialNumber = d.AwbSerialNumber,
                    DestinationId = d.DestinationId,
                    CarrierCode = d.CarrierCode,
                    FlightDate = d.FlightDate,
                    FlightNumber = d.FlightNumber,
                    OriginId = d.OriginId,
                    Pieces = d.Pieces,
                    Weight = d.Weight,
                    WeightUomId = d.WeightUomId
                }).ToList();

                PopulateWizardHeader(tmpModel);
            }

            return PartialView("_wndAWBView", tmpModel);
        }

        // step 4
        public PartialViewResult LoadAWBWizardStep(long? awbId, int stepId)
        {
            ShipperManifestStatus tmpStep = (ShipperManifestStatus)stepId;
            switch (tmpStep)
            {
                case ShipperManifestStatus.AWBDetailsVerified:
                    {
                        return this.GetAWBHandlingsView(awbId);
                    }
                case ShipperManifestStatus.AWBSpecialHandlingVerified:
                    {
                        return this.GetAWBShipperVerifyView(awbId);
                    }
                case ShipperManifestStatus.AWBContactInfoVeriried:
                    {
                        return this.GetAWBScreeningView(awbId);
                    }
                case ShipperManifestStatus.AWBEntryCompleted:
                    {
                        return this.GetAWBScreeningView(awbId);
                    }
                default:
                    return this.GetAWBDetialsView(awbId);
            }
        }

        //step 4.1
        public PartialViewResult GetAWBDetialsView(long? awbId)
        {
            AcaAwbDetialsModel tmpModel = new AcaAwbDetialsModel();
            tmpModel.Id = awbId.HasValue ? awbId.Value : -1;
            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                if (!awbId.HasValue || awbId == -1)
                {
                    var tmpWizardHeader = unit.GetWizardHeader(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
                    if (tmpWizardHeader != null)
                    {
                        tmpModel.CarrierCode = tmpWizardHeader.CarrierCode;
                        tmpModel.FlightTypeId = tmpWizardHeader.FlightTypeId;
                    }
                }
                else
                {
                    var data = unit.GetShipperManifestAwb(awbId.Value);
                    tmpModel = new AcaAwbDetialsModel
                    {
                        Id = data.Id,
                        ConnectedToShipper = data.ConnectedToShipper,
                        AwbSerialNumber = data.AwbSerialNumber,
                        DestinationId = data.DestinationId,
                        CarrierCode = data.CarrierCode,
                        FlightDate = data.FlightDate,
                        FlightNumber = data.FlightNumber,
                        OriginId = data.OriginId,
                        Pieces = data.Pieces,
                        Weight = data.Weight,
                        WeightUomId = data.WeightUomId,
                        FlightTypeId = data.FlightTypeId
                    };
                }
            }

            return PartialView("_subAwbDetialsView", tmpModel);
        }

        //step 4.2
        public PartialViewResult GetAWBHandlingsView(long? awbId)
        {
            AWBSpecialHandlingModel tmpModel = new AWBSpecialHandlingModel();
            tmpModel.AwbId = awbId.HasValue ? awbId.Value : 0;

            WizardPage tmpWizardHeader = PopulateWizardHeader(tmpModel);

            using (SpecialHandlingUnit unit = new SpecialHandlingUnit())
            {
                var tmpQuery = unit.GetAccountSpecialHandlings(tmpWizardHeader.CarrierId.Value);
                tmpModel.SpecialHandlings = ParsHandlings(tmpQuery);
            }

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                var item = unit.GetAwbHandlings(awbId);
                if (item != null)
                {
                    var mode = new AcaAwbSaveHandlings
                    {
                        AwbId = item.AwbId,
                        SphGroupIds = item.SphGroupIds,
                        Documents = item.Documents.Select(d => new Models.Common.AttachmentModel
                        {
                            Id = d.Id,
                            ShipperDocTypeId = d.ShipperDocTypeId,
                            IconKey = d.IconKey,
                            IsSnapshot = d.IsSnapshot,
                            Name = d.Name,
                            Path = d.Path,
                            SnapshotData = d.SnapshotData,
                            DocumentType = d.DocumentType
                        }).ToList()
                    };
                    tmpModel.Documents = mode.Documents;
                    if (tmpModel.SpecialHandlings != null && tmpModel.SpecialHandlings.Any())
                    {
                        var tmpSaveGroupIds = mode.SphGroupIds.Split(',').Select(spItem => spItem.Split('|')[0]);

                        foreach (var spcode in tmpModel.SpecialHandlings)
                        {
                            spcode.IsChecked = tmpSaveGroupIds.Contains(spcode.Id.ToString());
                        }
                    }
                }
            }

            return PartialView("_subAwbSpecialHandlingView", tmpModel);
        }



        //step 4.3
        public PartialViewResult GetAWBShipperVerifyView(long? awbId)
        {
            AcaAwbContactModel tmpModel = new AcaAwbContactModel();
            WizardPage header = PopulateWizardHeader(tmpModel);
            List<long> groupIds;

            var shipperVerification = new AcaSaveShipperVerify { AwbId = awbId.Value };

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                groupIds = unit.GetSpecialHendlingGroupsForAwb(awbId.Value, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
                if (awbId.HasValue)
                {
                    shipperVerification.ShipperVeriryFields = unit.GetAwbShipperVerifications(awbId.Value).Select(i => new AcaShipperVerifyField { FieldId = i.Id, Value = i.Code }).ToList();
                }
            }

            using (SpecialHandlingUnit unit = new SpecialHandlingUnit())
            {
                tmpModel.Fields = unit.GetShipperVerifyFields(groupIds.ToArray());
                tmpModel.AwbId = awbId.Value;

                if (tmpModel.Fields != null && tmpModel.Fields.Any())
                {
                    if (shipperVerification.ShipperVeriryFields != null && shipperVerification.ShipperVeriryFields.Any())
                    {
                        foreach (var item in tmpModel.Fields)
                        {
                            var tmpFieldInited = shipperVerification.ShipperVeriryFields.SingleOrDefault(e => e.FieldId == item.Id);
                            if (tmpFieldInited != null)
                            {
                                item.Value = tmpFieldInited.Value;
                            }
                        }
                    }
                }
                else
                {
                    // skip 4.3 step and jump to 4.4 screening
                    return this.GetAWBScreeningView(tmpModel.AwbId);
                }
            }

            return PartialView("_subAwbShipperVerifyView", tmpModel);
        }

        //step 4.4
        public PartialViewResult GetAWBScreeningView(long? awbId)
        {
            AcaAwbScreeningModel tmpModel = new AcaAwbScreeningModel();
            WizardPage tmpWizardHeader = PopulateWizardHeader(tmpModel);

            using (SpecialHandlingUnit handlingUnit = new SpecialHandlingUnit())
            {
                var tmpAltScreeningCases = handlingUnit.GetAccountAlternativeScreenings(tmpWizardHeader.CarrierId.Value, tmpWizardHeader.HeaderFlightTypeId.Value);
                tmpModel.AltScreeningOptions = tmpAltScreeningCases.ScreeningOptions;
            }

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                var data = unit.GetAWBScreenings(awbId);
                if (data != null)
                {
                    var model = new SaveAcaAwbScreening
                    {
                        AwbId = data.AwbId,
                        ApprovedCCSFId = data.ApprovedCCSFId,
                        IsTendered = data.IsTendered,
                        SelaNumbers = data.SelaNumbers.Select(n => new Models.CargoAcceptance.SealNumber
                        {
                            SealNumberValue = n.SealNumberValue,
                            SealTypeId = n.SealTypeId,
                            SealTypeName = n.SealTypeName
                        }).ToList(),
                        AlernativeScreeningOptions = data.AlernativeScreeningOptions.Select(a => new Models.Screening.SaveAltScreeningOptions
                        {
                            Id = a.Id,
                            Checked = a.Checked
                        }).ToList()
                    };

                    tmpModel.ApprovedCCSFId = model.ApprovedCCSFId;
                    tmpModel.IsTenderedAsScreened = model.IsTendered;
                    tmpModel.SealNumbers = model.SelaNumbers;
                    tmpModel.AwbId = model.AwbId;

                    if (tmpModel.AltScreeningOptions.Any() && model.AlernativeScreeningOptions.Any())
                    {
                        foreach (var aletItem in model.AlernativeScreeningOptions)
                        {
                            var tmpItem = tmpModel.AltScreeningOptions.SingleOrDefault(item => item.Id == aletItem.Id);
                            tmpItem.Checked = aletItem.Checked;
                        }
                    }
                }
                else
                {
                    tmpModel.IsTenderedAsScreened = true;
                }
            }

            return PartialView("_subAwbScreeningView", tmpModel);
        }
        #endregion

        #region Wizard savings

        [HttpPost]
        public JsonResult Finalize(SaveAcaAwbScreening model)
        {
            SaveAlternateScreening(model);

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                unit.CompleteAwb(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value, model.AwbId);
                unit.Finalize(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveCurrentAWB(SaveAcaAwbScreening model)
        {
            SaveAlternateScreening(model);
            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                unit.CompleteAwb(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value, model.AwbId);
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        private void SaveAlternateScreening(SaveAcaAwbScreening model)
        {
            string seals = null;
            string screeningLogs = null;
            if (model.IsTendered)
                seals = string.Join("|", model.SelaNumbers.Select(s => s.SealTypeId + "," + s.SealNumberValue));
            else
                screeningLogs = string.Join("|", model.AlernativeScreeningOptions.Select(s => s.Id + "," + (s.Checked ? 1 : 0)));

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                unit.SaveAlternateScreening(model.AwbId, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value, model.IsTendered, model.ApprovedCCSFId,
                    seals, screeningLogs);
            }
        }

        [HttpPost] // step 1
        public JsonResult SaveNextToDriver()
        {
            var tmpFormData = Request.Form["formData"];
            var tmpFormDeserialized = ControllerHelpers.ToObjectFromJSON<AcceptCarrierShipper>(tmpFormData);

            var attachments = SaveAttachments(tmpFormDeserialized.Documents);

            var info = new AcceptShipperInfo
            {
                UserId = SessionManager.UserInfo.UserLocalId,
                WarehouseId = SessionManager.UserInfo.DefaultWarehouseId.Value,
                AccountId = tmpFormDeserialized.AccountCarrierId,
                AircraftTypeId = tmpFormDeserialized.FlightTypeId,
                ShipperTypeId = tmpFormDeserialized.ShipperTypeId,
                ShipperId = tmpFormDeserialized.ShipperTypeId == 5 ? tmpFormDeserialized.ApprovedTSACarrierId :
                    ((tmpFormDeserialized.ShipperTypeId == 3 || tmpFormDeserialized.ShipperTypeId == 4) ? tmpFormDeserialized.ApprovedIACId :
                    (tmpFormDeserialized.ShipperTypeId == 1 ? tmpFormDeserialized.ApprovedKnownShipperId : null)),
                ShipperName = tmpFormDeserialized.ShipperName,
                ShipperConsentSignature = tmpFormDeserialized.SignImageData,
                IsDocumentVerified = tmpFormDeserialized.IsDocumentVerified,
                GovCredTypeId = tmpFormDeserialized.GovAuthIdVerified,
                ShipperStatusVerified = tmpFormDeserialized.ShipperVerified,
                Documents = attachments
            };
            long shipmentManifestId = 0;
            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                shipmentManifestId = unit.AcceptShipperInfoVerified(info);
            }

            return Json(new { Id = shipmentManifestId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost] //step 2
        public JsonResult SaveNextToAWB(ACADriverModel model)
        {
            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                unit.SaveDriverSecurityLogs(model.DriverId, SessionManager.UserInfo.UserLocalId, model.FirstIDTypeId, model.SecondIDTypeId, model.FirstIDMatched, model.SecondIDMatched,
                    model.TruckingCompId, model.CapturePhoto, model.CaptureID, null, SessionManager.UserInfo.DefaultWarehouseId.Value, model.STANumber, model.DriverLicense, model.StateId);
            }

            return Json(new { Id = model.ID }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveNextToHandling(AcaAwbDetialsModel model)
        {
            long awbId;
            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                awbId = unit.SaveAwbData(model.Id, model.AwbSerialNumber, model.FlightNumber, model.FlightDate.Value, model.OriginId.Value, model.DestinationId.Value,
                    model.Pieces.Value, model.Weight.Value, model.WeightUomId.Value, model.FlightTypeId.Value, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
            }

            return Json(new { Id = awbId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveNextToShipperVerify()
        {
            var tmpFormData = Request.Form["formData"];
            var tmpFormDeserialized = ControllerHelpers.ToObjectFromJSON<AcaAwbSaveHandlings>(tmpFormData);

            var groupIds = tmpFormDeserialized.SphGroupIds.Split(',');

            var pairs = new List<string>();

            for (var i = 0; i < groupIds.Length; ++i)
                pairs.Add(groupIds[i].Replace('|', ',').TrimEnd(','));

            var handlingCodes = string.Join("|", pairs);
            var attachments = SaveAttachments(tmpFormDeserialized.Documents);

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                unit.SaveSpecialHandlings(tmpFormDeserialized.AwbId, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value, handlingCodes, attachments);
            }
            return Json(new { Id = tmpFormDeserialized.AwbId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveShipperVerify(AcaSaveShipperVerify model)
        {
            var fields = string.Join("|", model.ShipperVeriryFields.Select(f => f.FieldId + "," + f.Value));

            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                unit.SaveAwbShipperVerification(model.AwbId, SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value, fields);
            }
            return Json(new { Id = model.AwbId }, JsonRequestBehavior.AllowGet);
        }

        #endregion


        public JsonResult CancelWizard()
        {
            using (CargoAcceptanceUnit unit = new CargoAcceptanceUnit())
            {
                unit.Cancel(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }


        #region Helpers

        private string SaveAttachments(IEnumerable<Models.Common.AttachmentModel> attachments)
        {
            var snapshots = attachments.Where(d => d.IsSnapshot).Select(d => string.Format("{0}${0}#Snapshot#{1}#{2}", string.IsNullOrEmpty(d.Name) ? DateTime.Now.ToString("yyyMMddhhss") : d.Name, d.ShipperDocTypeId.HasValue && d.ShipperDocTypeId.Value != -1 ? d.ShipperDocTypeId.Value : 17, d.SnapshotData)).ToList();

            var docTypes = attachments.Where(d => !d.IsSnapshot).Select(d => new { ShipperDocTypeId = d.ShipperDocTypeId.HasValue && d.ShipperDocTypeId.Value != -1 ? d.ShipperDocTypeId.Value : 17, d.Name, d.Path }).ToList();
            var documents = new List<string>();

            var index = 0;

            string tmpAttachmentsDir = ConfigurationManager.AppSettings[Constants.Config.ATTACHMENTS_REPOSITORY];

            
            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];
                var actualName = DateTime.Now.ToString("yyyMMddhhss") + file.FileName;
                var path = Path.Combine(tmpAttachmentsDir, actualName);
                file.SaveAs(path);
                documents.Add(string.Format("{0}${1}#Document#{2}#Document", docTypes[index].Name, actualName, docTypes[index].ShipperDocTypeId));
                ++index;
            }

            if (Request.Files.Count == 0 && docTypes.Any())
            {
                foreach (var doc in docTypes)
                    documents.Add(string.Format("{0}${1}#Document#{2}#Document", doc.Name, doc.Path, doc.ShipperDocTypeId));
            }

            documents.AddRange(snapshots);
            return string.Join("|", documents);
        }

        private static WizardPage PopulateWizardHeader(WizardPage tmpWizardPage)
        {
            WizardHeader tmpWizardHeader = null;
            using (CargoAcceptanceUnit acaUnit = new CargoAcceptanceUnit())
            {
                tmpWizardHeader = acaUnit.GetWizardHeader(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultWarehouseId.Value);
            }

            if (tmpWizardHeader != null)
            {
                tmpWizardPage.CarrierId = tmpWizardHeader.CarrierId;
                tmpWizardPage.CarrierName = tmpWizardHeader.CarrierName;
                tmpWizardPage.CarrierShipperName = tmpWizardHeader.ShipperName;
                tmpWizardPage.DriverName = tmpWizardHeader.DriverName;
                tmpWizardPage.ShipperTypeName = tmpWizardHeader.ShipperTypeName;
                tmpWizardPage.TrackingCompanyName = tmpWizardHeader.TrackingCompany;
                tmpWizardPage.HeaderFlightTypeId = tmpWizardHeader.FlightTypeId;
                tmpWizardPage.HeaderStatus = tmpWizardHeader.Status;
                tmpWizardPage.DriverPhotoData = tmpWizardHeader.CapturePhoto;
            }

            return tmpWizardPage;
        }

        private static List<SHListItem> ParsHandlings(IList<BLL.Model.SpecialHandling.DtoSpecialHandlingGroup> tmpQuery)
        {
            var tmpResult = new List<SHListItem>();

            if (tmpQuery.Any())
            {
                foreach (var i in tmpQuery)
                {
                    var tmpItem = new SHListItem
                    {
                        Code = i.Code,
                        DisplayName = i.DisplayName,
                        Id = i.Id,
                        Name = i.Name,
                        SphCodeId = i.SphCodeId
                    };
                    if (i.SubGroups != null && i.SubGroups.Any())
                    {
                        tmpItem.SubGroups = new List<SHListItem>();
                        foreach (var s in i.SubGroups)
                        {
                            var tmpSubItem = new SHListItem
                            {
                                DisplayName = s.DisplayName,
                                Id = s.Id,
                                Name = s.Name,
                                SphCodeId = s.SphCodeId,
                            };
                            tmpItem.SubGroups.Add(tmpSubItem);
                        }
                    }
                    tmpResult.Add(tmpItem);
                }
            }
            return tmpResult;
        }
        #endregion

    }
}