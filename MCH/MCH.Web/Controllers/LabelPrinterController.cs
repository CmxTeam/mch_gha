﻿using MCH.BLL.Units;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class LabelPrinterController : Controller
    {
        

        public JsonResult PrintTestLabel()
        {
            using (LabelPrintingUnit printerUnit = new LabelPrintingUnit())
            {
                printerUnit.PrintTestLabel(SessionManager.UserInfo.UserLocalId, SessionManager.UserInfo.DefaultAccountId.Value);
                //printerUnit.PrintTestLableIntermec();

                return Json(new { printed = true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}