﻿using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using MCH.BLL.Units;
using MCH.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.Utilities;
using MCH.Web.Models.WarehouseManagement;
using System.Web.Security.AntiXss;
using System.Web.Util;
using MCH.Web.Filters;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using MCH.BLL.Model.Enum;
using MCH.Web.Models.Common;
using MCH.BLL.Model.Warehouse;
using MCH.BLL.DAL.Data;

namespace MCH.Web.Controllers
{

    public class WarehouseManagementController : Controller
    {
        private MCHDBEntities Context = null;

        public WarehouseManagementController()
        {
            this.Context = new MCHDBEntities();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
            this.Context.Dispose();
            this.Context = null;
        }

        // GET: WarehouseManagement
        public PartialViewResult WarehouseManagementTabstrip()
        {
            ViewBag.ManageTypes = new List<SimpleType>
            {
                    new SimpleType { Id = 1, Name = "Manage Warehouses" },
                    new SimpleType { Id = 2, Name = "Manage Regions" },
                     new SimpleType { Id = 3, Name = "Manage Locations" }
            };


            return PartialView();
        }

        // temp method for dhtmlx json bind test
        [AllowCrossSiteJson]
        public JsonResult GetTempWarhouseLocations()
        {
            var tmpQuery = new List<dynamic>();

            for (int i = 0; i <= 54; i++)
            {
                tmpQuery.Add(new
                {
                    Id = i,
                    data = new
                    {
                        LastScan = DateTime.Now,
                        Location = "zuydzuydwest",
                        Pieces = 5,
                        TotalPieces = 12,
                        TotalWeight = 25,
                        WeightUOM = "KG",
                        User = "Poloz Mukuch",
                        Warehouse = "Atata"
                    }
                });
            }

            return Json(new { rows = tmpQuery }, JsonRequestBehavior.AllowGet);
        }

        #region Add Edit Warehouse
        public PartialViewResult AddEditWarehouse()
        {
            WarehouseAddEdit model = new WarehouseAddEdit();
            return PartialView("_Warehouse", model);
        }

        public PartialViewResult GetWarehousById(long? id)
        {
            WarehouseAddEdit model = new WarehouseAddEdit();
            using (CommonUnit unit = new CommonUnit())
            {
                var tmpitem = unit.Context.UserWarehouses.Where(item => item.WarehouseId == id).FirstOrDefault();

                if (tmpitem != null)
                {
                    model.WarehouseId = tmpitem.WarehouseId;
                    model.WarehouseName = tmpitem.Warehouse.WarehouseName;
                    model.StationId = tmpitem.Warehouse.PortId;
                    model.ShellWarehouseId = tmpitem.Warehouse.ShellWarehouseId;
                    model.Address1 = tmpitem.Warehouse.Address;
                    model.Address2 = tmpitem.Warehouse.Address2;
                    model.AliasName = tmpitem.Warehouse.Code;
                    model.City = tmpitem.Warehouse.City;
                    model.CountryId = tmpitem.Warehouse.CountryId;
                    model.StateId = tmpitem.Warehouse.StateId;
                    model.Email = tmpitem.Warehouse.ContactEmail;
                    if (!String.IsNullOrWhiteSpace(tmpitem.Warehouse.Latitude) && !String.IsNullOrWhiteSpace(tmpitem.Warehouse.Longitude))
                    {
                        model.GeoLocation = tmpitem.Warehouse.Latitude + "," + tmpitem.Warehouse.Longitude;
                    }
                    model.ManagerName = tmpitem.Warehouse.Supervisor;
                    model.Phone = tmpitem.Warehouse.Phone;
                    model.Postal = tmpitem.Warehouse.PostalCode;
                }

                return PartialView("_WarehouseEditor", model);
            }
        }

        public JsonResult SaveWarehouse(WarehouseAddEdit model)
        {
            using (WarehouseUnit unit = new WarehouseUnit())
            {

                long id = unit.SaveWarehouse(model.ToBusinesModel());

                if (id > 0)
                {
                    return Json(new { data = "Ok" }, JsonRequestBehavior.AllowGet);
                }
                else // some error
                {
                    if (id == -2) // duplicate
                    {
                        string tmpMessage = String.Format("Warehouse {0} already exists", model.WarehouseName);
                        return Json(new { data = "Error", ErrorMessage = tmpMessage }, JsonRequestBehavior.AllowGet);
                    }
                    if (id == -3) // warehouse already mapped to shell warehouse
                    {
                        string tmpMessage = "Shell Warehouse already mapped, please select another Shell Warehouse.";
                        return Json(new { data = "Error", ErrorMessage = tmpMessage }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { data = "Error", ErrorMessage = "Server Error" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion


        #region Add Edit Region
        public PartialViewResult AddEditRegion()
        {
            RegionAddEdit model = new RegionAddEdit();
            return PartialView("_Region", model);
        }

        public PartialViewResult GetRegionById(long? id)
        {
            RegionAddEdit model = new RegionAddEdit();
            using (CommonUnit unit = new CommonUnit())
            {
                var tmpitem = unit.Context.WarehouseZones.Where(item => item.Id == id).FirstOrDefault();

                if (tmpitem != null)
                {
                    model.RegionId = tmpitem.Id;
                    model.RegionName = tmpitem.Zone;
                    model.StationId = tmpitem.Warehouse.PortId;
                    model.WarehouseId = tmpitem.WarehouseId;
                }

                return PartialView("_RegionEditor", model);
            }
        }

        public JsonResult SaveRegion(RegionAddEdit model)
        {

            using (WarehouseUnit unit = new WarehouseUnit())
            {
                long id = unit.SaveWarehouseZone(model.ToBusinessModel());

                if (id > 0)
                {
                    return Json(new { data = "Ok" }, JsonRequestBehavior.AllowGet);
                }
                else // some error
                {
                    if (id == -2) // duplicate
                    {
                        string tmpMessage = String.Format("Region {0} already exists", model.RegionName);
                        return Json(new { data = "Error", ErrorMessage = tmpMessage }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { data = "Error", ErrorMessage = "Server Error" }, JsonRequestBehavior.AllowGet);

                }
            }

        }

        #endregion


        #region Add Edit Location
        public PartialViewResult AddEditLocation()
        {
            LocationAddEdit model = new LocationAddEdit();
            return PartialView("_Location", model);
        }

        public PartialViewResult GetLocationById(long? id)
        {
            LocationAddEdit model = new LocationAddEdit();
            using (CommonUnit unit = new CommonUnit())
            {
                var tmpitem = unit.Context.WarehouseLocations.Where(item => item.Id == id).FirstOrDefault();

                if (tmpitem != null)
                {
                    model.LocationId = tmpitem.Id;
                    model.LocationName = tmpitem.Location;
                    model.StationId = tmpitem.Warehouse.PortId;
                    model.WarehouseId = tmpitem.WarehouseId;
                    model.RegionId = tmpitem.ZoneId;
                    model.LocationTypeId = tmpitem.LocationTypeId;
                    model.BarCode = tmpitem.Barcode;
                }

                return PartialView("_LocationEditor", model);
            }
        }

        public JsonResult SaveLocation(LocationAddEdit model)
        {

            using (WarehouseUnit unit = new WarehouseUnit())
            {

                long id = unit.SaveWarehouseLocation(model.ToBusinessModel());

                if (id > 0)
                {
                    return Json(new { data = "Ok" }, JsonRequestBehavior.AllowGet);
                }
                else // some error
                {
                    if (id == -2) // duplicate
                    {
                        string tmpMessage = String.Format("Location {0} already exists", model.LocationName);
                        return Json(new { data = "Error", ErrorMessage = tmpMessage }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { data = "Error", ErrorMessage = "Server Error" }, JsonRequestBehavior.AllowGet);

                }
            }

        }

        #endregion


        #region Grid methods
        [AllowCrossSiteJson]
        public JsonResult GetShipmentsTemp([DataSourceRequest] DataSourceRequest request, WarehouseManagement searchFilter)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpQuery = new List<WarehouseShipmentInventory>().AsQueryable();


                var tmpDataQuery = this.Context.Dispositions.Where(d => !d.IsAdjust.HasValue || !d.IsAdjust.Value).AsQueryable();

                if (searchFilter.LocationId.HasValue)
                {
                    tmpDataQuery = tmpDataQuery.Where(item => item.WarehouseLocationId == searchFilter.LocationId);
                }

                tmpQuery = tmpDataQuery.GroupBy(e => new { e.AWB, e.HWB }).Select(item => new WarehouseShipmentInventory
                {
                    ShipmentId = item.Key.HWB != null ? item.Key.HWB.Id : item.Key.AWB.Id,
                    ShipmentReference = item.Key.HWB != null ? item.Key.HWB.HWBSerialNumber : item.Key.AWB.AWBSerialNumber,
                    Shipper = item.Key.HWB != null ? item.Key.HWB.Customer.Name : item.Key.AWB.Customer.Name,
                    TotalPieces = item.Key.HWB != null ? item.Key.HWB.Pieces : item.Key.AWB.TotalPieces,

                    Pieces = item.Key.HWB != null ? item.Key.HWB.Dispositions.Where(d => d.WarehouseLocationId == searchFilter.LocationId).Sum(e => e.Count) : item.Key.AWB.Dispositions.Sum(e => e.Count),

                    TotalWeight = item.Key.HWB != null ? (item.Key.HWB.Weight / item.Key.HWB.Pieces) * item.Key.HWB.Dispositions.Where(d => d.WarehouseLocationId == searchFilter.LocationId).Sum(e => e.Count) : (item.Key.AWB.TotalWeight / item.Key.AWB.TotalPieces) * item.Key.AWB.Dispositions.Where(d => d.WarehouseLocationId == searchFilter.LocationId).Sum(e => e.Count),

                    User = item.Key.HWB != null ? item.Key.HWB.Dispositions.Where(d => d.WarehouseLocationId == searchFilter.LocationId).OrderByDescending(e => e.Timestamp).Select(e => e.UserProfile.UserId).FirstOrDefault() : item.Key.AWB.Dispositions.OrderByDescending(e => e.Timestamp).Select(e => e.UserProfile.UserId).FirstOrDefault(),

                    Warehouse = item.FirstOrDefault().WarehouseLocation.Warehouse.WarehouseName,
                    WeightUOM = "kg",
                    Consignee = item.Key.HWB != null ? item.Key.HWB.Customer1.Name : item.Key.AWB.Customer1.Name,
                    Location = item.FirstOrDefault().WarehouseLocation.Location,

                    FirstScan = item.Key.HWB != null ? item.Key.HWB.Dispositions.Where(d => d.WarehouseLocationId == searchFilter.LocationId).OrderBy(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault() : item.Key.AWB.Dispositions.Where(d => d.WarehouseLocationId == searchFilter.LocationId).OrderBy(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault(),

                    LastScan = item.Key.HWB != null ? item.Key.HWB.Dispositions.Where(d => d.WarehouseLocationId == searchFilter.LocationId).OrderByDescending(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault() : item.Key.AWB.Dispositions.Where(d => d.WarehouseLocationId == searchFilter.LocationId).OrderByDescending(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault(),


                    IsOSD = item.Key.HWB != null ? item.Key.HWB.IsOSD : item.Key.AWB.IsOSD,

                    SpecialHandlingsIndicators = item.Key.HWB != null ? MCHDBEntities.MCH_GetSPHCodesList(item.Key.HWB.Id, (int)enumEntityTypes.HWB) : MCHDBEntities.MCH_GetSPHCodesList(item.Key.AWB.Id, (int)enumEntityTypes.AWB),

                    Origin = item.Key.HWB != null ? item.Key.HWB.Port.IATACode : item.Key.AWB.Port.IATACode,
                    Destination = item.Key.HWB != null ? item.Key.HWB.Port1.IATACode : item.Key.AWB.Port1.IATACode,
                });

                var tmpResult = tmpQuery.ToDataSourceResult(request);

                return Json(tmpResult, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetLocationsByShipmentIdTemp([DataSourceRequest] DataSourceRequest request, long shipmentId, WarehouseManagement searchFilter)
        {
            using (CommonUnit commonUnit = new CommonUnit())
            {
                var tmpQuery = new List<WarehouseLocationInventory>().AsQueryable();

                tmpQuery = this.Context.Dispositions.Where(d => !d.IsAdjust.HasValue || !d.IsAdjust.Value).Where(d => d.AWBId == shipmentId || d.HWBId == shipmentId).Where(item => item.WarehouseLocationId.HasValue).GroupBy(item => item.WarehouseLocation).Select(item => new WarehouseLocationInventory
                {
                    LastScan = item.OrderBy(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault(),
                    Location = item.Key.Location,
                    WarehouseLocationId = item.Key.Id,
                    Pieces = item.Key.Dispositions.Where(d => !d.IsAdjust.HasValue || !d.IsAdjust.Value).Where(d => d.AWBId == shipmentId || d.HWBId == shipmentId).Sum(e => e.Count),
                    TotalPieces = item.Key.Dispositions.Where(d => !d.IsAdjust.HasValue || !d.IsAdjust.Value).Sum(e => e.Count),
                    TotalWeight = item.Select(e => e.HWB != null ? e.HWB.Weight : e.AWB.TotalWeight).FirstOrDefault(),
                    User = item.OrderBy(e => e.Timestamp).Select(e => e.UserProfile.UserId).FirstOrDefault(),
                    Warehouse = item.Key.Warehouse.WarehouseName,
                    WeightUOM = "Kg"
                });

                if (searchFilter.LocationId.HasValue)
                {
                    tmpQuery = tmpQuery.Where(item => item.WarehouseLocationId == searchFilter.LocationId);
                }


                return Json(tmpQuery.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetLocationsTemp([DataSourceRequest] DataSourceRequest request, WarehouseManagement searchFilter)
        {

            var tmpQuery = new List<WarehouseLocationInventory>().AsQueryable();

            tmpQuery = this.Context.Dispositions.Where(d => !d.IsAdjust.HasValue || !d.IsAdjust.Value).Where(item => item.WarehouseLocationId.HasValue).GroupBy(item => item.WarehouseLocation).Select(item => new WarehouseLocationInventory
            {
                LastScan = item.OrderBy(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault(),
                Location = item.Key.Location,
                WarehouseLocationId = item.Key.Id,
                Pieces = item.Key.Dispositions.Sum(e => e.Count),
                TotalWeight = item.Select(e => e.HWB != null ? e.HWB.Weight : e.AWB.TotalWeight).FirstOrDefault(),
                User = item.OrderBy(e => e.Timestamp).Select(e => e.UserProfile.UserId).FirstOrDefault(),
                Warehouse = item.Key.Warehouse.WarehouseName,
                WeightUOM = "Kg"
            });

            if (searchFilter.LocationId.HasValue)
            {
                tmpQuery = tmpQuery.Where(item => item.WarehouseLocationId == searchFilter.LocationId);
            }


            return Json(tmpQuery.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);


        }

        public JsonResult GetShipmentsByLocationIdTemp([DataSourceRequest] DataSourceRequest request, long WarehouseLocationId, WarehouseManagement searchFilter)
        {

            var tmpQuery = new List<WarehouseShipmentInventory>().AsQueryable();

            tmpQuery = this.Context.Dispositions.Where(d => !d.IsAdjust.HasValue || !d.IsAdjust.Value).Where(item => item.WarehouseLocationId == WarehouseLocationId).GroupBy(e => new { e.AWB, e.HWB }).Select(item => new WarehouseShipmentInventory
            {
                ShipmentId = item.Key.HWB != null ? item.Key.HWB.Id : item.Key.AWB.Id,
                ShipmentReference = item.Key.HWB != null ? item.Key.HWB.HWBSerialNumber : item.Key.AWB.AWBSerialNumber,
                Shipper = item.Key.HWB != null ? item.Key.HWB.Customer.Name : item.Key.AWB.Customer.Name,
                TotalPieces = item.Key.HWB != null ? item.Key.HWB.Pieces : item.Key.AWB.TotalPieces,

                Pieces = item.Key.HWB != null ? item.Key.HWB.Dispositions.Where(d => d.WarehouseLocationId == WarehouseLocationId).Sum(e => e.Count) : item.Key.AWB.Dispositions.Where(d => d.WarehouseLocationId == WarehouseLocationId).Sum(e => e.Count),

                TotalWeight = item.Key.HWB != null ? (item.Key.HWB.Weight / item.Key.HWB.Pieces) * item.Key.HWB.Dispositions.Where(d => d.WarehouseLocationId == WarehouseLocationId).Sum(e => e.Count) : (item.Key.AWB.TotalWeight / item.Key.AWB.TotalPieces) * item.Key.AWB.Dispositions.Where(d => d.WarehouseLocationId == WarehouseLocationId).Sum(e => e.Count),

                User = item.Key.HWB != null ? item.Key.HWB.Dispositions.OrderByDescending(e => e.Timestamp).Select(e => e.UserProfile.UserId).FirstOrDefault() : item.Key.AWB.Dispositions.OrderByDescending(e => e.Timestamp).Select(e => e.UserProfile.UserId).FirstOrDefault(),

                Warehouse = item.FirstOrDefault().WarehouseLocation.Warehouse.WarehouseName,
                WeightUOM = "kg",
                Consignee = item.Key.HWB != null ? item.Key.HWB.Customer1.Name : item.Key.AWB.Customer1.Name,
                Location = item.FirstOrDefault().WarehouseLocation.Location,

                FirstScan = item.Key.HWB != null ? item.Key.HWB.Dispositions.OrderBy(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault() : item.Key.AWB.Dispositions.OrderBy(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault(),

                LastScan = item.Key.HWB != null ? item.Key.HWB.Dispositions.OrderByDescending(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault() : item.Key.AWB.Dispositions.OrderByDescending(e => e.Timestamp).Select(e => e.Timestamp).FirstOrDefault(),


                IsOSD = item.Key.HWB != null ? item.Key.HWB.IsOSD : item.Key.AWB.IsOSD,

                SpecialHandlingsIndicators = item.Key.HWB != null ? MCHDBEntities.MCH_GetSPHCodesList(item.Key.HWB.Id, (int)enumEntityTypes.HWB) : MCHDBEntities.MCH_GetSPHCodesList(item.Key.AWB.Id, (int)enumEntityTypes.AWB),

                Origin = item.Key.HWB != null ? item.Key.HWB.Port.IATACode : item.Key.AWB.Port.IATACode,
                Destination = item.Key.HWB != null ? item.Key.HWB.Port1.IATACode : item.Key.AWB.Port1.IATACode,
            });

            var tmpResult = tmpQuery.ToDataSourceResult(request);

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }




        #endregion


        #region Combo DataSources

        public JsonResult GetShellWarehouses()
        {
            using (WarehouseUnit unit = new WarehouseUnit())
            {
                var tmpList = unit.GetShellWarehoues();


                var list = tmpList.Select(item => new { Id = item.Id, Name = item.Name });

                return Json(list.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetWarehouseLocationTypes()
        {
            using (CommonUnit unit = new CommonUnit())
            {
                var tmpList = unit.Context.WarehouseLocationTypes.AsQueryable();


                var list = tmpList.Select(item => new { Id = item.Id, Name = item.LocationType });

                return Json(list.ToList(), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetWarehousesByStationId(long? sId)
        {
            if (sId.HasValue)
            {
                using (CommonUnit unit = new CommonUnit())
                {
                    var tmpList = unit.Context.UserWarehouses.Where(item => item.UserId == SessionManager.UserInfo.UserLocalId)
                        .Where(item => item.Warehouse.PortId == sId)
                        .Select(item => new { Id = item.WarehouseId, Name = item.Warehouse.WarehouseName });

                    return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new List<BLL.DAL.Data.UserWarehous>(), JsonRequestBehavior.AllowGet);
        }

        [AllowCrossSiteJson]
        public JsonResult GetRegionsByWarehouseId(long? sId)
        {
            if (sId.HasValue)
            {
                using (CommonUnit unit = new CommonUnit())
                {
                    var tmpList = unit.Context.WarehouseZones
                        .Where(item => item.WarehouseId == sId)
                        .Select(item => new { Id = item.Id, Name = item.Zone });

                    return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new List<BLL.DAL.Data.WarehouseZone>(), JsonRequestBehavior.AllowGet);
        }

        [AllowCrossSiteJson]
        public JsonResult GetLocationsByRegionId(long? sId)
        {
            if (sId.HasValue)
            {
                using (CommonUnit unit = new CommonUnit())
                {
                    var tmpList = unit.Context.WarehouseLocations
                        .Where(item => item.ZoneId == sId)
                        .Select(item => new { Id = item.Id, Name = item.Location });

                    return Json(tmpList.ToList(), JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new List<BLL.DAL.Data.WarehouseLocation>(), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }


}