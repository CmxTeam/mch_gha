﻿using MCH.Web.Models.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using MCH.BLL.DAL;
using MCH.Web.Utilities;
using MCH.Web.Models.Accept;
using MCH.Web.Models.Awbs;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using MCH.BLL.DAL.Data;

namespace MCH.Web.Controllers
{
    public class TasksController : BaseController
    {

        public PartialViewResult SearchTasks()
        {
            return PartialView("_SearchTasks");
        }

        public PartialViewResult GetReceiverTasks()
        {
            SearchTaskViewModel tmpModel = new SearchTaskViewModel();
            tmpModel.WarehousePortId = SessionManager.UserInfo.DefaultSiteId.Value;
            tmpModel.TaskTypeId = enumTaskTypes.CargoReceiver;

            return PartialView("_DinamicTasksList", tmpModel);
        }

        public PartialViewResult GetLoaderTasks()
        {
            SearchTaskViewModel tmpModel = new SearchTaskViewModel();
            tmpModel.WarehousePortId = SessionManager.UserInfo.DefaultSiteId.Value;
            tmpModel.TaskTypeId = enumTaskTypes.CargoLoader;


            return PartialView("_DinamicTasksList", tmpModel);
        }

        public PartialViewResult GetAcceptTasks()
        {
            SearchTaskViewModel tmpModel = new SearchTaskViewModel();
            tmpModel.WarehousePortId = SessionManager.UserInfo.DefaultSiteId.Value;
            tmpModel.TaskTypeId = enumTaskTypes.CargoAccpetWarehouse;


            return PartialView("_DinamicTasksList", tmpModel);
        }

        public PartialViewResult GetScreeningTasks()
        {
            SearchTaskViewModel tmpModel = new SearchTaskViewModel();
            tmpModel.WarehousePortId = SessionManager.UserInfo.DefaultSiteId.Value;
            tmpModel.TaskTypeId = enumTaskTypes.CargoScreener;


            return PartialView("_DinamicTasksList", tmpModel);
        }

        public PartialViewResult GetDGTasks()
        {
            SearchTaskViewModel tmpModel = new SearchTaskViewModel();
            tmpModel.WarehousePortId = SessionManager.UserInfo.DefaultSiteId.Value;
            tmpModel.TaskTypeId = enumTaskTypes.DGCheckList;


            return PartialView("_DinamicTasksList", tmpModel);
        }


        public PartialViewResult GetLATasks()
        {
            SearchTaskViewModel tmpModel = new SearchTaskViewModel();
            tmpModel.WarehousePortId = SessionManager.UserInfo.DefaultSiteId.Value;
            tmpModel.TaskTypeId =  enumTaskTypes.LiveAnimal;


            return PartialView("_DinamicTasksList", tmpModel);
        }


      

        public PartialViewResult GetAddTaskView(SearchTaskViewModel model)
        {
            enumTaskTypes tmpTaskType = model.TaskTypeId;

            switch (tmpTaskType)
            {
                case enumTaskTypes.CargoLoader:
                    {
                        return PartialView("_AddCargoLoaderTask", model);
                    }
                case enumTaskTypes.CargoReceiver:
                    {
                        return PartialView("_AddCargoReceiverTask", model);
                    }
                case enumTaskTypes.DGCheckList:
                    {
                        return PartialView("_AddDGCheckTask", model);
                    }
                case enumTaskTypes.CargoAccpetWarehouse:
                    {
                        return GetAddAcceptTask(model);
                    }
                default:
                    {
                        return PartialView("_AddCargoReceiverTask", model);
                    }
            }
        }

        public PartialViewResult GetAddAcceptTask(SearchTaskViewModel model)
        {
            AddEditAcceptTaskModel tmpModel = new AddEditAcceptTaskModel();
            tmpModel.Customer = new AcceptCustomer();
            tmpModel.Driver = new AcceptDriver();

            return PartialView("_AddCargoAcceptTask", tmpModel);
        }

        public PartialViewResult GetNewAcceptTaskView(SearchTaskViewModel model)
        {
            AddEditAcceptTaskModel tmpModel = new AddEditAcceptTaskModel();
            tmpModel.Customer = new AcceptCustomer();
            tmpModel.Driver = new AcceptDriver();

            return PartialView("_AddEditAcceptForm", tmpModel);
        }

        public PartialViewResult GetEditAccpetTask(SelectTaskFilter filter)
        {
            return null;
            //using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            //{
            //    AddEditAcceptTaskModel tmpModel = new AddEditAcceptTaskModel();

            //    tmpModel.IsEdit = true;

            //    var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == filter.TaskId);
            //    tmpModel.TaskId = filter.TaskId;

            //    var tmpCustomer = unitOfWork.Context.Shippers.FirstOrDefault(item => item.Name == tmpTask.TaskReference);

            //    tmpModel.Customer = new AcceptCustomer()
            //    {
            //        CustomerName = tmpCustomer.Name,
            //        CustomerTypeId = (int)tmpCustomer.ShipperTypeId,
            //        IATACode = tmpCustomer.IAC_Agent_Number,
            //        TaskId = filter.TaskId
            //    };

            //    var tmpDriverSecurity = null;//unitOfWork.Context.DriverSecurityLogs.Where(item => item.EntityTypeId == (int)enumEntityTypes.Task && item.EntityId == filter.TaskId).OrderByDescending(item => item.RecDate).FirstOrDefault();

            //    tmpModel.Driver = new AcceptDriver();

            //    if (tmpDriverSecurity != null)
            //    {
            //        var tmpDriver = tmpDriverSecurity.CarrierDriver;
            //        tmpModel.Driver.Name = tmpDriver.FullName;
            //        tmpModel.Driver.DriverId = tmpDriver.Id;
            //        tmpModel.Driver.IDNumber = tmpDriver.LicenseNumber;
            //        tmpModel.Driver.IDPhotoString = tmpDriver.LicenseImage;
            //        tmpModel.Driver.IsPhotoMatched = tmpDriverSecurity.PrimaryPhotoIdMatch.HasValue ? tmpDriverSecurity.PrimaryPhotoIdMatch.Value : false;
            //        tmpModel.Driver.PhotoString = tmpDriver.DriverPhoto;
            //        tmpModel.Driver.PrimaryId = tmpDriver.CredentialTypeId;
            //        tmpModel.Driver.PrimaryString = tmpDriver.CredentialType.Name;
            //        tmpModel.Driver.TruckingCoId = tmpDriver.TruckingCompanyId;
            //    }

            //    var tmpChildTaskAwbs = unitOfWork.Context.GetCargoAcceptAWBList(filter.TaskId).ToList();
            //    if (tmpChildTaskAwbs.Any())
            //    {
            //        tmpModel.AwbList = new List<AcceptAwbListItem>();
            //        tmpModel.TotalAwbs = tmpChildTaskAwbs.Count();
            //        foreach (var itemAwbTask in tmpChildTaskAwbs.ToList())
            //        {
            //            tmpModel.AwbList.Add(new AcceptAwbListItem
            //            {
            //                AwbId = itemAwbTask.AWBId,
            //                TaskId = itemAwbTask.CurrentTaskId,
            //                TotalPieces = itemAwbTask.TotalPieces,
            //                TotalVolume = itemAwbTask.TotalVolume,
            //                TotalWeight = itemAwbTask.TotalWeight,
            //                WeightUOM = itemAwbTask.WeightUom,
            //                VolumeUom = itemAwbTask.VolumeUom,
            //                Reference = itemAwbTask.AWB
            //            });
            //            tmpModel.TotalPcs += itemAwbTask.TotalPieces.HasValue ? itemAwbTask.TotalPieces.Value : 0;
            //            tmpModel.TotalVolume += itemAwbTask.TotalVolume.HasValue ? itemAwbTask.TotalVolume.Value : 0;
            //            tmpModel.TotalWeight += itemAwbTask.TotalWeight.HasValue ? itemAwbTask.TotalWeight.Value : 0;
            //            tmpModel.WeightUom = itemAwbTask.WeightUom;
            //            tmpModel.VolumeUom = itemAwbTask.VolumeUom;
            //        }
            //    }

            //    return PartialView("_AddEditAcceptForm", tmpModel);
            //}
        }

        public PartialViewResult GetChildTaskSelectList(SelectTaskFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                AddEditAcceptTaskModel tmpModel = new AddEditAcceptTaskModel();
                tmpModel.TaskId = filter.TaskId;
                var tmpChildTaskAwbs = unitOfWork.Context.GetCargoAcceptAWBList(filter.TaskId).ToList();
                if (tmpChildTaskAwbs.Any())
                {
                    tmpModel.AwbList = new List<AcceptAwbListItem>();
                    tmpModel.TotalAwbs = tmpChildTaskAwbs.Count();
                    foreach (var itemAwbTask in tmpChildTaskAwbs.ToList())
                    {
                        tmpModel.AwbList.Add(new AcceptAwbListItem
                        {
                            AwbId = itemAwbTask.AWBId,
                            TotalPieces = itemAwbTask.TotalPieces,
                            TotalVolume = itemAwbTask.TotalVolume,
                            TotalWeight = itemAwbTask.TotalWeight,
                            WeightUOM = itemAwbTask.WeightUom,
                            VolumeUom = itemAwbTask.VolumeUom,
                            Reference = itemAwbTask.AWB
                        });
                        tmpModel.TotalPcs += itemAwbTask.TotalPieces.HasValue ? itemAwbTask.TotalPieces.Value : 0;
                        tmpModel.TotalVolume += itemAwbTask.TotalVolume.HasValue ? itemAwbTask.TotalVolume.Value : 0;
                        tmpModel.TotalWeight += itemAwbTask.TotalWeight.HasValue ? itemAwbTask.TotalWeight.Value : 0;
                        tmpModel.WeightUom = itemAwbTask.WeightUom;
                        tmpModel.VolumeUom = itemAwbTask.VolumeUom;
                    }
                }

                return PartialView("_ChildTaskSelectList", tmpModel);
            }
        }

        public PartialViewResult GetTaskViewByType(SearchTaskViewModel model)
        {
            enumTaskTypes tmpTaskType = model.TaskTypeId;

            switch (tmpTaskType)
            {
                case enumTaskTypes.CargoLoader:
                    {
                        return PartialView("_CargoLoaderTasks", model);
                    }
                case enumTaskTypes.CargoReceiver:
                    {
                        return PartialView("_CargoReceiverTasks", model);
                    }
                case enumTaskTypes.DGCheckList:
                    {
                        return PartialView("_DGCheckTasks", model);
                    }
                case enumTaskTypes.LiveAnimal:
                    {
                        return PartialView("_LACheckTasks", model);
                    }
                case enumTaskTypes.CargoAccpetWarehouse:
                    {
                        return PartialView("_CargoAcceptTasks", model);
                    }
                case enumTaskTypes.CargoScreener:
                    {
                        return PartialView("_CargoScreeningTasks", model);
                    }
                default:
                    {
                        return PartialView("_CargoReceiverTasks", model);
                    }
            }
        }


        public JsonResult GetTaskLoadPlan(TaskShipmentsFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var data = unitOfWork.Context.GetCargoLoaderShipmentsPlan(
                    filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.sSearch,
                    filter.TaskId
                    );

                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        draw = filter.iDisplayStart + 1,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetTaskShipments(TaskShipmentsFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var data = unitOfWork.Context.GetCargoReceiverShipments(
                    filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.sSearch,
                    filter.TaskId
                    );
                
                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        draw = filter.iDisplayStart + 1,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetCargoScreeningTaskList(CargoScreeningTaskListfilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                IList<CLTaskRow> tmpTasks = new List<CLTaskRow>();

                var data = unitOfWork.Context.GetCargoScreeningTaskList(
                    filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.sSearch, filter.From, filter.To,
                    filter.CarrierId, filter.OriginId, filter.DestinationId,
                    filter.TaskStatusId, filter.AssignedUserId,
                    filter.WarehouseId, filter.FlightNumber
                    );

                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetLAChecklistTaskList(ChecklistTaskListfilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                IList<CLTaskRow> tmpTasks = new List<CLTaskRow>();

                var data = unitOfWork.Context.GetLAChecklistTaskList(
                    filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.sSearch, filter.From, filter.To,
                    filter.CarrierId, filter.OriginId, filter.DestinationId,
                    filter.TaskStatusId, filter.AssignedUserId,
                    filter.WarehouseId, filter.FlightNumber
                    );

                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetDGChecklistTaskList(ChecklistTaskListfilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                IList<CLTaskRow> tmpTasks = new List<CLTaskRow>();

                var data = unitOfWork.Context.GetDGChecklistTaskList(
                    filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.sSearch, filter.From, filter.To,
                    filter.CarrierId, filter.OriginId, filter.DestinationId,
                    filter.TaskStatusId, filter.AssignedUserId,
                    filter.WarehouseId, filter.FlightNumber
                    );

                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetCargoAcceptTaskList(CargoAcceptTaskListFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                IList<CLTaskRow> tmpTasks = new List<CLTaskRow>();

                var data = unitOfWork.Context.GetCargoAcceptTaskList(
                    filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.sSearch, filter.From, filter.To,
                    filter.CarrierId, filter.OriginId, filter.DestinationId,
                    filter.TaskStatusId, filter.AssignedUserId,
                    filter.WarehouseId
                    );

                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetCargoLoaderTaskList(CargoLoaderTaskListFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                IList<CLTaskRow> tmpTasks = new List<CLTaskRow>();

                var data = unitOfWork.Context.GetCargoLoaderTaskList(
                    filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.sSearch, filter.From, filter.To,
                    filter.CarrierId, filter.OriginId, filter.DestinationId,
                    filter.TaskStatusId, filter.AssignedUserId,
                    filter.WarehouseId, filter.FlightNumber
                    );

                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetTaskData(CargoLoaderTaskListFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                IList<CLTaskRow> tmpTasks = new List<CLTaskRow>();

                var data = unitOfWork.GetCargoReceiverTasksList(filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.From,
                    filter.To,
                    filter.CarrierId,
                    filter.OriginId,
                    filter.DestinationId,
                    filter.TaskStatusId,
                    filter.AssignedUserId,
                    filter.WarehouseId,
                    filter.sSearch,
                    filter.FlightNumber);

                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                {
                    sEcho = filter.sEcho,
                    iTotalRecords = tmpData.Count,
                    iTotalDisplayRecords = tmpData[0].Total,
                    aaData = tmpData
                }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                {
                    sEcho = filter.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = filter.iDisplayLength,
                    aaData = new { }
                }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public PartialViewResult GetCRTaskDetailsHeader(SelectTaskFilter model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTaskDetails = this.GetCRTaskDetialsModel(model.TaskId, unitOfWork);

                return PartialView("_TaskDetailsCRHeader", tmpTaskDetails);
            }
        }

        public PartialViewResult GetSCTaskDetailsHeader(SelectTaskFilter model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTaskDetails = this.GetSCTaskDetialsModel(model.TaskId, unitOfWork);

                return PartialView("_TaskDetailsSCHeader", tmpTaskDetails);
            }
        }

        public PartialViewResult GetCLTaskDetailsHeader(SelectTaskFilter model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTaskDetails = this.GetCRTaskDetialsModel(model.TaskId, unitOfWork);

                return PartialView("_TaskDetailsCLHeader", tmpTaskDetails);
            }
        }


        public PartialViewResult SaveCRTaskDetails(TaskDetailsCR taskDetails)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(taskDetails.TaskId);

                if (tmpTask == null)
                {
                    //TODO : Handle this error
                    throw new Exception("Task not found. ");
                }

                if (taskDetails.DueDate.HasValue)
                {
                    tmpTask.DueDate = taskDetails.DueDate;
                }

                if (taskDetails.PriorityId.HasValue)
                {
                    tmpTask.IsPriority = taskDetails.PriorityId.Value == 1 ? true : false;
                }

                if (taskDetails.Flight != null)
                {
                    var tmpFlight = unitOfWork.Context.FlightManifests.SingleOrDefault(item => item.Id == taskDetails.Flight.FlightId);
                    if (taskDetails.Flight.ETA.HasValue)
                    {
                        tmpFlight.ETA = taskDetails.Flight.ETA;

                        //TODO : Harut select the correct unloading port
                        var tmpUloadingPort = tmpFlight.UnloadingPorts.FirstOrDefault();

                        if (tmpUloadingPort != null)
                        {
                            tmpUloadingPort.POU_ETA = taskDetails.Flight.ETA;
                        }
                    }
                }
                unitOfWork.Context.SaveChanges();

                var tmpTaskDetials = this.GetCRTaskDetialsModel(taskDetails.TaskId, unitOfWork);

                return PartialView("_TaskDetailsCRHeader", tmpTaskDetials);
            }
        }


        public PartialViewResult SaveCLTaskDetails(TaskDetailsCR taskDetails)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(taskDetails.TaskId);

                if (tmpTask == null)
                {
                    //TODO : Handle this error
                    throw new Exception("Task not found. ");
                }

                if (taskDetails.DueDate.HasValue)
                {
                    tmpTask.DueDate = taskDetails.DueDate;
                }

                if (taskDetails.PriorityId.HasValue)
                {
                    tmpTask.IsPriority = taskDetails.PriorityId.Value == 1 ? true : false;
                }

                if (taskDetails.Flight != null)
                {
                    var tmpFlight = unitOfWork.Context.FlightManifests.SingleOrDefault(item => item.Id == taskDetails.Flight.FlightId);
                    if (taskDetails.Flight.ETA.HasValue)
                    {
                        tmpFlight.ETA = taskDetails.Flight.ETA;

                        //TODO : Harut select the correct unloading port
                        var tmpUloadingPort = tmpFlight.UnloadingPorts.FirstOrDefault();

                        if (tmpUloadingPort != null)
                        {
                            tmpUloadingPort.POU_ETA = taskDetails.Flight.ETA;
                        }
                    }
                    if (taskDetails.Flight.ETD.HasValue)
                    {
                        tmpFlight.ETD = taskDetails.Flight.ETD;

                        //TODO : Harut select the correct unloading port
                        var tmpUloadingPort = tmpFlight.UnloadingPorts.FirstOrDefault();

                        if (tmpUloadingPort != null)
                        {
                            tmpUloadingPort.POU_ETD = taskDetails.Flight.ETD;
                        }
                    }
                    if (taskDetails.Flight.Cutoff.HasValue)
                    {
                        tmpTask.DueDate = taskDetails.Flight.Cutoff;
                    }
                }
                unitOfWork.Context.SaveChanges();

                var tmpTaskDetials = this.GetCRTaskDetialsModel(taskDetails.TaskId, unitOfWork);

                return PartialView("_TaskDetailsCLHeader", tmpTaskDetials);
            }
        }

        public JsonResult GetTaskAssignments(TaskDetailsFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(filter.TaskId);

                if (tmpTask == null)
                {
                    // TODO : Handle this case
                }

                var data = tmpTask.TaskAssignments.AsQueryable();

                if (!String.IsNullOrWhiteSpace(filter.sSearch))
                {
                    data = data.Where(item => item.UserProfile != null &&
                         (item.UserProfile.FirstName.IndexOf(filter.sSearch, StringComparison.InvariantCultureIgnoreCase) > -1
                        || item.UserProfile.LastName.IndexOf(filter.sSearch, StringComparison.InvariantCultureIgnoreCase) > -1));
                }

                var total = data.Count();

                data = data.Skip(filter.iDisplayStart * filter.iDisplayLength).Take(filter.iDisplayLength);

                var tmpData = data.Select(item => new
                {
                    Id = item.Id,
                    UserName = item.UserProfile.FirstName + " " + item.UserProfile.LastName,
                    IsOwner = item.IsOwner ? 1 : 0,
                    Date = item.RecDate
                }).ToList();

                if (tmpData.Any())
                {
                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetTaskInstructions(TaskDetailsFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(filter.TaskId);

                if (tmpTask == null)
                {
                    // TODO : Handle this case
                }

                var data = tmpTask.TaskInstructions.AsQueryable();

                if (!String.IsNullOrWhiteSpace(filter.sSearch))
                {
                    data = data.Where(item => item.InstructionText.IndexOf(filter.sSearch, StringComparison.InvariantCultureIgnoreCase) > -1
                        || item.UserProfile.FirstName.IndexOf(filter.sSearch, StringComparison.InvariantCultureIgnoreCase) > -1
                        || item.UserProfile.LastName.IndexOf(filter.sSearch, StringComparison.InvariantCultureIgnoreCase) > -1);
                }

                var total = data.Count();

                data = data.Skip(filter.iDisplayStart * filter.iDisplayLength).Take(filter.iDisplayLength);

                var tmpData = data.Select(item => new
                {
                    Message = item.InstructionText,
                    CreatedBy = item.UserProfile.FirstName + " " + item.UserProfile.LastName,
                    Date = item.RecDate.HasValue ? item.RecDate.Value.ToShortDateString() : String.Empty
                }).ToList();

                if (tmpData.Any())
                {
                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetTaskHistory(TaskDetailsFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {


                var data = unitOfWork.Context.GetReceiverTaskHistoryItems(filter.iDisplayStart, filter.iDisplayLength, !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null, filter.sSearch, filter.TaskId);

                var tmpData = data.ToList();

                if (tmpData.Any())
                {
                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        public JsonResult AddInstruction(TaskRemark model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(model.TaskId);
                if (tmpTask != null)
                {
                    tmpTask.TaskInstructions.Add(new TaskInstruction()
                    {
                        InstructionText = model.Remark,
                        UserId = SessionManager.UserInfo.UserLocalId,
                        RecDate = DateTime.UtcNow,

                    });

                    unitOfWork.Context.SaveChanges();
                }

                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult GetTaskDetailsSC(long taskId)
        {

            TaskDetailsSC tmpModel = new TaskDetailsSC();
            tmpModel.TaskId = taskId;

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                tmpModel = this.GetSCTaskDetialsModel(taskId, unitOfWork);

                return PartialView("_TaskDetailsSC", tmpModel);
            }
        }

        public PartialViewResult GetTaskDetialsLA(long taskId)
        {
            TaskDetailsDG tmpModel = new TaskDetailsDG();
            tmpModel.TaskId = taskId;

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(taskId);
                if (tmpTask == null)
                {
                    //TODO handle this
                }

                tmpModel.Completed = tmpTask.EndDate;
                tmpModel.DueDate = tmpTask.DueDate;
                tmpModel.PriorityId = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? 1 : 2;
                tmpModel.Progress = tmpTask.ProgressPercent.HasValue ? Math.Round(tmpTask.ProgressPercent.Value, 1) : 0;
                tmpModel.Reference = tmpTask.TaskReference;
                tmpModel.StartDate = tmpTask.StartDate;
                tmpModel.StationId = tmpTask.Warehouse.PortId;
                tmpModel.StatusId = tmpTask.StatusId;
                tmpModel.Status = tmpTask.Status.DisplayName;
                tmpModel.Priority = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? "Standard" : "Expedited";
                tmpModel.TaskAssignments = tmpTask.TaskAssignments.Select(item => new MCH.Web.Models.Task.TaskAssignment
                {
                    Assigner = item.UserProfile.FirstName + " " + item.UserProfile.LastName,
                    AssignmentDate = item.AssignedOn,
                    UserId = item.UserId
                }).ToList();

                tmpModel.AWB = new Models.Awbs.AwbDetails();
                var tmpAwb = unitOfWork.Context.AWBs.SingleOrDefault(item => item.Id == tmpTask.EntityId);
                if (tmpAwb != null)
                {
                    var tmpAwbSegments = tmpAwb.LegSegments.Where(item => item.PortId == tmpAwb.DestinationId).FirstOrDefault();
                    var tmpConsignee = unitOfWork.Context.Customers.SingleOrDefault(item => item.Id == tmpAwb.ConsigneeId);
                    var tmpShipper = unitOfWork.Context.Customers.SingleOrDefault(item => item.Id == tmpAwb.ShipperId);

                    if (tmpAwbSegments != null)
                    {
                        tmpModel.AWB.CarrierName = tmpAwbSegments.Carrier != null ? tmpAwbSegments.Carrier.CarrierCode : String.Empty;
                        tmpModel.AWB.FlightNumber = tmpAwbSegments.FlightNumber;
                        tmpModel.AWB.ETD = tmpAwbSegments.ETD.Value;
                    }

                    tmpModel.AWB.Id = tmpTask.EntityId.Value;

                    tmpModel.Shipper = tmpShipper != null ? tmpShipper.Name : String.Empty;
                    tmpModel.Consignee = tmpConsignee != null ? tmpConsignee.Name : String.Empty;
                    tmpModel.PCS = tmpAwb.TotalPieces;
                    tmpModel.Volume = tmpAwb.TotalVolume + " " + tmpAwb.UOM1.ThreeCharCode;
                    tmpModel.Weight = tmpAwb.TotalWeight + " " + tmpAwb.UOM.ThreeCharCode;
                }


                return PartialView("_TaskDetailsLA", tmpModel);
            }
        }

        public PartialViewResult GetTaskDetialsDG(long taskId)
        {
            TaskDetailsDG tmpModel = new TaskDetailsDG();
            tmpModel.TaskId = taskId;

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(taskId);
                if (tmpTask == null)
                {
                    //TODO handle this
                }

                tmpModel.Completed = tmpTask.EndDate;
                tmpModel.DueDate = tmpTask.DueDate;
                tmpModel.PriorityId = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? 1 : 2;
                tmpModel.Progress = tmpTask.ProgressPercent.HasValue ? Math.Round(tmpTask.ProgressPercent.Value, 1) : 0;
                tmpModel.Reference = tmpTask.TaskReference;
                tmpModel.StartDate = tmpTask.StartDate;
                tmpModel.StationId = tmpTask.Warehouse.PortId;
                tmpModel.StatusId = tmpTask.StatusId;
                tmpModel.Status = tmpTask.Status.DisplayName;
                tmpModel.Priority = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? "Standard" : "Expedited";
                tmpModel.TaskAssignments = tmpTask.TaskAssignments.Select(item => new MCH.Web.Models.Task.TaskAssignment
                {
                    Assigner = item.UserProfile.FirstName + " " + item.UserProfile.LastName,
                    AssignmentDate = item.AssignedOn,
                    UserId = item.UserId
                }).ToList();

                tmpModel.AWB = new Models.Awbs.AwbDetails();
                var tmpAwb = unitOfWork.Context.AWBs.SingleOrDefault(item => item.Id == tmpTask.EntityId);
                if (tmpAwb != null)
                {
                    var tmpAwbSegments = tmpAwb.LegSegments.Where(item => item.PortId == tmpAwb.DestinationId).FirstOrDefault();
                    var tmpConsignee = unitOfWork.Context.Customers.SingleOrDefault(item => item.Id == tmpAwb.ConsigneeId);
                    var tmpShipper = unitOfWork.Context.Customers.SingleOrDefault(item => item.Id == tmpAwb.ShipperId);

                    if (tmpAwbSegments != null)
                    {
                        tmpModel.AWB.CarrierName = tmpAwbSegments.Carrier != null ? tmpAwbSegments.Carrier.CarrierCode : String.Empty;
                        tmpModel.AWB.FlightNumber = tmpAwbSegments.FlightNumber;
                        tmpModel.AWB.ETD = tmpAwbSegments.ETD.Value;
                    }

                    tmpModel.AWB.Id = tmpTask.EntityId.Value;

                    tmpModel.Shipper = tmpShipper != null ? tmpShipper.Name : String.Empty;
                    tmpModel.Consignee = tmpConsignee != null ? tmpConsignee.Name : String.Empty;
                    tmpModel.PCS = tmpAwb.TotalPieces;
                    tmpModel.Volume = tmpAwb.TotalVolume + " " + (tmpAwb.UOM1 != null ? tmpAwb.UOM1.ThreeCharCode : String.Empty);
                    tmpModel.Weight = tmpAwb.TotalWeight + " " + (tmpAwb.UOM != null ? tmpAwb.UOM.ThreeCharCode : String.Empty);
                }


                return PartialView("_TaskDetailsDG", tmpModel);
            }
        }

        public PartialViewResult GetTaskDetailsCL(long taskId)
        {
            TaskDetailsCR tmpModel = new TaskDetailsCR();
            tmpModel.TaskId = taskId;

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(taskId);
                if (tmpTask == null)
                {
                    //TODO handle this
                }

                tmpModel.Completed = tmpTask.EndDate;
                tmpModel.DueDate = tmpTask.DueDate;
                tmpModel.PriorityId = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? 1 : 2;
                tmpModel.Progress = tmpTask.ProgressPercent.HasValue ? Math.Round(tmpTask.ProgressPercent.Value, 1) : 0;
                tmpModel.Reference = tmpTask.TaskReference;
                tmpModel.StartDate = tmpTask.StartDate;
                tmpModel.StationId = tmpTask.Warehouse.PortId;
                tmpModel.StatusId = tmpTask.StatusId;
                tmpModel.TaskAssignments = tmpTask.TaskAssignments.Select(item => new MCH.Web.Models.Task.TaskAssignment
                {
                    Assigner = item.UserProfile.FirstName + " " + item.UserProfile.LastName,
                    AssignmentDate = item.AssignedOn,
                    UserId = item.UserId
                }).ToList();

                tmpModel.Flight = new Models.Flight.FlightDetails();
                var tmpFlight = unitOfWork.Context.GetLoaderTaskFlightDetails(tmpTask.Id).FirstOrDefault();
                if (tmpFlight != null)
                {
                    tmpModel.Flight.FlightId = tmpTask.EntityId.Value;
                    tmpModel.Flight.CarrierId = tmpFlight.CarrierId;
                    tmpModel.Flight.DestinationId = tmpFlight.DestinationId;
                    tmpModel.Flight.ETA = tmpFlight.ETA;
                    tmpModel.Flight.ETD = tmpFlight.ETD;
                    tmpModel.Flight.FligthNumber = tmpFlight.FlightNumber;
                    tmpModel.Flight.HandlingStationId = tmpTask.Warehouse.PortId;
                    tmpModel.Flight.OriginId = tmpFlight.OriginId;
                    tmpModel.Flight.TotalAWBs = tmpFlight.TotalAWBs;
                    tmpModel.Flight.TotalHAWBs = tmpFlight.TotalHWBs;
                    tmpModel.Flight.TotalPieces = tmpFlight.TotalPieces;
                    tmpModel.Flight.TotalULDs = tmpFlight.TotalULDs;
                    tmpModel.Flight.Cutoff = tmpFlight.Cutoff;
                    tmpModel.Flight.TotalVolume = tmpFlight.TotalVolume;
                    tmpModel.Flight.TotalWeight = tmpFlight.TotalWeight;
                }

                return PartialView("_TaskDetailsCL", tmpModel);

            }
        }


        public PartialViewResult GetReceiverTaskDetials(long taskId)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                TaskDetailsCR tmpModel = this.GetCRTaskDetialsModel(taskId, unitOfWork);

                return PartialView("_TaskDetailsCR", tmpModel);
            }
        }




        public PartialViewResult GetTAskDetailsByType(long taskId)
        {
            TaskDetailsCR tmpModel = new TaskDetailsCR();
            tmpModel.TaskId = taskId;

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(taskId);
                if (tmpTask == null)
                {
                    //TODO handle this
                }

                switch ((enumTaskTypes)tmpTask.TaskTypeId)
                {
                    case enumTaskTypes.CargoReceiver:
                        {
                            return GetReceiverTaskDetials(taskId);
                        }
                    case enumTaskTypes.CargoLoader:
                        {
                            return GetTaskDetailsCL(taskId);
                        }
                    case enumTaskTypes.DGCheckList:
                        {
                            return this.GetTaskDetialsDG(taskId);
                        }
                    case enumTaskTypes.LiveAnimal:
                        {
                            return this.GetTaskDetialsLA(taskId);
                        }
                    case enumTaskTypes.CargoAccpetWarehouse:
                        {
                            return this.GetEditAccpetTask(new SelectTaskFilter() { TaskId = taskId });
                        }
                    case enumTaskTypes.CargoScreener:
                        {
                            return this.GetTaskDetailsSC(taskId);
                        }
                    default:
                        // TODO : return view not found message
                        return GetReceiverTaskDetials(taskId);
                }
            }
        }

        public JsonResult DGCheckListEdited(SelectTaskFilter model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == model.TaskId);

                if (tmpTask.StatusId == (int)enumTaskStatuses.NotStarted)
                {
                    tmpTask.StatusId = (int)enumTaskStatuses.InProgress;
                    tmpTask.StartDate = DateTime.UtcNow;
                    unitOfWork.Context.TaskTransactions.Add(new TaskTransaction()
                    {
                        Description = "Task Started",
                        RecDate = DateTime.UtcNow,
                        StatusId = (int)enumTaskStatuses.InProgress,
                        TaskId = tmpTask.Id,
                        UserId = SessionManager.UserInfo.UserLocalId
                    });
                    unitOfWork.Context.SaveChanges();
                }
                else if (tmpTask.StatusId == (int)enumTaskStatuses.InProgress)
                {
                    unitOfWork.Context.Transactions.Add(new Transaction()
                    {
                        Description = "DG Checklist Being Edited",
                        EntityId = tmpTask.Id,
                        EntityTypeId = (int)enumEntityTypes.Task,
                        StatusId = tmpTask.StatusId,
                        TaskId = tmpTask.Id,
                        TaskTypeId = (int)enumTaskTypes.DGCheckList,
                        UserId = SessionManager.UserInfo.UserLocalId
                    });
                    unitOfWork.Context.SaveChanges();
                }
                return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult LACheckListEdited(SelectTaskFilter model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == model.TaskId);

                if (tmpTask.StatusId == (int)enumTaskStatuses.NotStarted)
                {
                    tmpTask.StatusId = (int)enumTaskStatuses.InProgress;
                    tmpTask.StartDate = DateTime.UtcNow;
                    unitOfWork.Context.TaskTransactions.Add(new TaskTransaction()
                    {
                        Description = "Task Started",
                        RecDate = DateTime.UtcNow,
                        StatusId = (int)enumTaskStatuses.InProgress,
                        TaskId = tmpTask.Id,
                        UserId = SessionManager.UserInfo.UserLocalId
                    });
                    unitOfWork.Context.SaveChanges();
                }
                else if (tmpTask.StatusId == (int)enumTaskStatuses.InProgress)
                {
                    unitOfWork.Context.Transactions.Add(new Transaction()
                    {
                        Description = "LA Checklist Being Edited",
                        EntityId = tmpTask.Id,
                        EntityTypeId = (int)enumEntityTypes.Task,
                        StatusId = tmpTask.StatusId,
                        TaskId = tmpTask.Id,
                        Reference = tmpTask.TaskReference,
                        TaskTypeId = (int)enumTaskTypes.LiveAnimal,
                        UserId = SessionManager.UserInfo.UserLocalId
                    });
                    unitOfWork.Context.SaveChanges();
                }
                return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult SaveAcceptAwbDetials(AcceptAwbDetailsModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpWeightUom = unitOfWork.Context.UOMs.SingleOrDefault(item => item.TwoCharCode == model.WeightUOM);
                var tmpVolumeUom = unitOfWork.Context.UOMs.SingleOrDefault(item => item.TwoCharCode == model.VolumeUOM);

                if (tmpWeightUom == null)
                {
                    tmpWeightUom = unitOfWork.Context.UOMs.SingleOrDefault(item => item.TwoCharCode == "KG");
                }
                if (tmpVolumeUom == null)
                {
                    tmpVolumeUom = unitOfWork.Context.UOMs.SingleOrDefault(item => item.TwoCharCode == "LB");
                }

                int tmpAwbTaskId = unitOfWork.Context.AddEditCargoAcceptAWBDetail(model.ParentTaskId, SessionManager.UserInfo.UserLocalId, model.AwbId, model.OriginId, model.DestinationId, model.CarrierName, model.SerialNumber, model.TotalPieces, model.TotalWeight, tmpWeightUom.Id, model.TotalVolume, tmpVolumeUom.Id, model.CarrierCode, model.FlightNumber, model.ETD);

                model.AwbId = tmpAwbTaskId;

                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAwbTaskDetailsData(SelectTaskFilter filter)
        {
            return null;

            //using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            //{
            //    AddEditAcceptTaskModel tmpModel = new AddEditAcceptTaskModel();

            //    var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == filter.TaskId);
            //    tmpModel.TaskId = filter.TaskId;

            //    var tmpCustomer = unitOfWork.Context.Shippers.FirstOrDefault(item => item.Name == tmpTask.TaskReference);

            //    tmpModel.Customer = new AcceptCustomer()
            //    {
            //        CustomerName = tmpCustomer.Name,
            //        CustomerTypeId = (int)tmpCustomer.ShipperTypeId,
            //        IATACode = tmpCustomer.IAC_Agent_Number,
            //        TaskId = filter.TaskId
            //    };

            //    var tmpDriverSecurity = unitOfWork.Context.DriverSecurityLogs.Where(item => item.EntityTypeId == (int)enumEntityTypes.Task && item.EntityId == filter.TaskId).OrderByDescending(item => item.RecDate).FirstOrDefault();

            //    tmpModel.Driver = new AcceptDriver();

            //    if (tmpDriverSecurity != null)
            //    {
            //        var tmpDriver = tmpDriverSecurity.CarrierDriver;
            //        tmpModel.Driver.Name = tmpDriver.FullName;
            //        tmpModel.Driver.DriverId = tmpDriver.Id;
            //        tmpModel.Driver.IDNumber = tmpDriver.LicenseNumber;
            //        tmpModel.Driver.IDPhotoString = tmpDriver.LicenseImage;
            //        tmpModel.Driver.IsPhotoMatched = tmpDriverSecurity.PrimaryPhotoIdMatch.HasValue ? tmpDriverSecurity.PrimaryPhotoIdMatch.Value : false;
            //        tmpModel.Driver.PhotoString = tmpDriver.DriverPhoto;
            //        tmpModel.Driver.PrimaryId = tmpDriver.CredentialTypeId;
            //        tmpModel.Driver.PrimaryString = tmpDriver.CredentialType.Name;
            //        tmpModel.Driver.TruckingCoId = tmpDriver.TruckingCompanyId;
            //    }

            //    var tmpChildTaskAwbs = unitOfWork.Context.GetCargoAcceptAWBList(filter.TaskId).ToList();
            //    if (tmpChildTaskAwbs.Any())
            //    {
            //        tmpModel.AwbList = new List<AcceptAwbListItem>();
            //        tmpModel.TotalAwbs = tmpChildTaskAwbs.Count();
            //        foreach (var itemAwbTask in tmpChildTaskAwbs.ToList())
            //        {
            //            tmpModel.AwbList.Add(new AcceptAwbListItem
            //            {
            //                AwbId = itemAwbTask.AWBId,
            //                TotalPieces = itemAwbTask.TotalPieces,
            //                TotalVolume = itemAwbTask.TotalVolume,
            //                TotalWeight = itemAwbTask.TotalWeight,
            //                WeightUOM = itemAwbTask.WeightUom,
            //                VolumeUom = itemAwbTask.VolumeUom,
            //                Reference = itemAwbTask.AWB
            //            });
            //            tmpModel.TotalPcs += itemAwbTask.TotalPieces.HasValue ? itemAwbTask.TotalPieces.Value : 0;
            //            tmpModel.TotalVolume += itemAwbTask.TotalVolume.HasValue ? itemAwbTask.TotalVolume.Value : 0;
            //            tmpModel.TotalWeight += itemAwbTask.TotalWeight.HasValue ? itemAwbTask.TotalWeight.Value : 0;
            //            tmpModel.WeightUom = itemAwbTask.WeightUom;
            //            tmpModel.VolumeUom = itemAwbTask.VolumeUom;
            //        }
            //    }
            //    return Json(tmpModel, JsonRequestBehavior.AllowGet);
            //}
        }

        public PartialViewResult GetAcceptAwbDetails(SelectAwbFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                long? tmpAwbId = filter.AwbId;

                AcceptAwbDetailsModel tmpModel = new AcceptAwbDetailsModel();
                tmpModel.TaskId = filter.TaskId;
                tmpModel.ParentTaskId = filter.ParentTaskId;

                if (!String.IsNullOrWhiteSpace(filter.TaskReference) && !filter.AwbId.HasValue)
                {
                    var tmpTask = unitOfWork.Context.Tasks.Where(item => item.TaskReference == filter.TaskReference && item.TaskTypeId == (int)enumTaskTypes.CargoAccpetWarehouse).FirstOrDefault();

                    if (tmpTask != null)
                    {
                        tmpAwbId = tmpTask.EntityId;
                    }

                    var tmpAwbDetailsByRef = unitOfWork.Context.GetCargoAcceptAWBDetailByReference(filter.TaskReference).ToList();
                    tmpModel.ParentTaskId = filter.TaskId;

                    if (tmpAwbDetailsByRef.Any())
                    {
                        var tmpAwb = tmpAwbDetailsByRef.First();

                        tmpModel.AwbId = tmpAwb.AWBId;
                        tmpModel.DestinationPort = tmpAwb.DestinationPort;
                        tmpModel.SerialNumber = tmpAwb.AWBSerialNumber;
                        tmpModel.CarrierName = tmpAwb.Carrier;
                        tmpModel.CarrierCode = tmpAwb.CarrierCode;
                        tmpModel.ETD = tmpAwb.ETD;
                        tmpModel.FlightNumber = tmpAwb.FlightNumber;
                        tmpModel.OriginPort = tmpAwb.OriginPort;
                        tmpModel.TotalPieces = tmpAwb.TotalPieces;
                        tmpModel.TotalVolume = tmpAwb.TotalVolume;
                        tmpModel.TotalWeight = tmpAwb.TotalWeight;
                        tmpModel.VolumeUOM = tmpAwb.VolumeUOM;
                        tmpModel.WeightUOM = tmpAwb.WeightUOM;
                    }
                }
                else
                {

                    var tmpAwbDetails = unitOfWork.Context.GetCargoAcceptAWBDetail(tmpAwbId).ToList();


                    tmpModel.ParentTaskId = filter.TaskId;

                    if (tmpAwbDetails.Any())
                    {
                        var tmpAwb = tmpAwbDetails.First();

                        tmpModel.AwbId = tmpAwb.AWBId;
                        tmpModel.DestinationPort = tmpAwb.DestinationPort;
                        tmpModel.SerialNumber = tmpAwb.AWBSerialNumber;
                        tmpModel.CarrierName = tmpAwb.Carrier;
                        tmpModel.CarrierCode = tmpAwb.CarrierCode;
                        tmpModel.ETD = tmpAwb.ETD;
                        tmpModel.FlightNumber = tmpAwb.FlightNumber;
                        tmpModel.OriginPort = tmpAwb.OriginPort;
                        tmpModel.TotalPieces = tmpAwb.TotalPieces;
                        tmpModel.TotalVolume = tmpAwb.TotalVolume;
                        tmpModel.TotalWeight = tmpAwb.TotalWeight;
                        tmpModel.VolumeUOM = tmpAwb.VolumeUOM;
                        tmpModel.WeightUOM = tmpAwb.WeightUOM;
                    }
                    else
                    {

                    }
                }

                return PartialView("_AcceptAwbDetailsView", tmpModel);
            }
        }





        #region Helper Methods
        private TaskDetailsSC GetSCTaskDetialsModel(long taskId, TasksUnitOfWork unitOfWork)
        {
            TaskDetailsSC tmpModel = new TaskDetailsSC();
            tmpModel.TaskId = taskId;

            var tmpTask = unitOfWork.TaskRepository.GetByID(taskId);
            if (tmpTask == null)
            {
                //TODO handle this
            }

            tmpModel.Completed = tmpTask.EndDate;
            tmpModel.DueDate = tmpTask.DueDate;
            tmpModel.PriorityId = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? 1 : 2;
            tmpModel.Progress = tmpTask.ProgressPercent.HasValue ? Math.Round(tmpTask.ProgressPercent.Value, 1) : 0;
            tmpModel.Reference = tmpTask.TaskReference;
            tmpModel.StartDate = tmpTask.StartDate;
            tmpModel.StationId = tmpTask.Warehouse.PortId;
            tmpModel.Station = tmpTask.Warehouse.Port.IATACode;
            tmpModel.StatusId = tmpTask.StatusId;
            tmpModel.Status = tmpTask.Status.Description;
            tmpModel.Priority = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? "Standard" : "Expedited";


            var tmpTaskScreenHeader = unitOfWork.Context.GetScreeningHeader(taskId).FirstOrDefault();
            if (tmpTaskScreenHeader != null)
            {
                tmpModel.AlarmCounts = tmpTaskScreenHeader.AlarmCounts;
                tmpModel.ClearedCounts = tmpTaskScreenHeader.ClearedCounts;
                tmpModel.Completed = tmpTaskScreenHeader.Completed;
                tmpModel.PassedCount = tmpTaskScreenHeader.PassedCounts;
                tmpModel.ScreeningReady = tmpTaskScreenHeader.ScreeningReady;
                tmpModel.ScreenStatus = tmpTaskScreenHeader.ScreenStatus;
                tmpModel.TotalPCsToScreen = tmpTaskScreenHeader.TotalPcsToScreen;
                tmpModel.TotalScreenings = tmpTaskScreenHeader.TotalScreenings;
            }

            return tmpModel;
        }


        private TaskDetailsCR GetCRTaskDetialsModel(long taskId, TasksUnitOfWork unitOfWork)
        {
            TaskDetailsCR tmpModel = new TaskDetailsCR();
            tmpModel.TaskId = taskId;

            var tmpTask = unitOfWork.TaskRepository.GetByID(taskId);
            if (tmpTask == null)
            {
                //TODO handle this
            }

            tmpModel.Completed = tmpTask.EndDate;
            tmpModel.DueDate = tmpTask.DueDate;
            tmpModel.PriorityId = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? 1 : 2;
            tmpModel.Progress = tmpTask.ProgressPercent.HasValue ? Math.Round(tmpTask.ProgressPercent.Value, 1) : 0;
            tmpModel.Reference = tmpTask.TaskReference;
            tmpModel.StartDate = tmpTask.StartDate;
            tmpModel.StationId = tmpTask.Warehouse.PortId;
            tmpModel.Station = tmpTask.Warehouse.Port.IATACode;
            tmpModel.StatusId = tmpTask.StatusId;
            tmpModel.Status = tmpTask.Status.Description;
            tmpModel.Priority = tmpTask.IsPriority.HasValue && tmpTask.IsPriority.Value ? "Standard" : "Expedited";
            tmpModel.TaskAssignments = tmpTask.TaskAssignments.Select(item => new MCH.Web.Models.Task.TaskAssignment
            {
                Assigner = item.UserProfile.FirstName + " " + item.UserProfile.LastName,
                AssignmentDate = item.AssignedOn,
                UserId = item.UserId
            }).ToList();

            tmpModel.Flight = new Models.Flight.FlightDetails();
            var tmpFlight = unitOfWork.Context.GetTaskFlightDetails(tmpTask.Id).FirstOrDefault();
            if (tmpFlight != null)
            {
                tmpModel.Flight.FlightId = tmpTask.EntityId.Value;
                tmpModel.Flight.CarrierId = tmpFlight.CarrierId;
                tmpModel.Flight.Carrier = tmpFlight.CarrierCode;
                tmpModel.Flight.DestinationId = tmpFlight.DestinationId;
                tmpModel.Flight.Destination = tmpFlight.Destination;
                tmpModel.Flight.ETA = tmpFlight.ETA;
                tmpModel.Flight.ETD = tmpFlight.ETD;
                tmpModel.Flight.FligthNumber = tmpFlight.FlightNumber;
                tmpModel.Flight.HandlingStationId = tmpTask.Warehouse.PortId;
                tmpModel.Flight.HandlingStation = tmpTask.Warehouse.Port.IATACode;
                tmpModel.Flight.OriginId = tmpFlight.OriginId;
                tmpModel.Flight.Origin = tmpFlight.Origin;
                tmpModel.Flight.TotalAWBs = tmpFlight.TotalAWBs;
                tmpModel.Flight.TotalHAWBs = tmpFlight.TotalHWBs;
                tmpModel.Flight.TotalPieces = tmpFlight.TotalPieces;
                tmpModel.Flight.TotalULDs = tmpFlight.TotalULDs;
                tmpModel.Flight.Cutoff = tmpFlight.Cutoff;
                tmpModel.Flight.TotalVolume = tmpFlight.TotalVolume;
                tmpModel.Flight.TotalWeight = tmpFlight.TotalWeight;
            }
            return tmpModel;
        }
        #endregion

    }
}
