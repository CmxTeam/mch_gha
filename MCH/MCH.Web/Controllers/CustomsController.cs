﻿using MCH.BLL.DAL;
using MCH.BLL.Model.Customs;
using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using MCH.Web.Models.Customs;
using MCH.Web.Models.DGChecklist;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using MCH.BLL.DAL.Data;

namespace MCH.Web.Controllers
{
    public class CustomsController : BaseController
    {

        public PartialViewResult GetCustomsHistoryView(GetCustomsHistoryModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpModel = new CustomsHistoryViewModel()
                {
                    ShipmentId = model.ShipmentId,
                    TaskId = model.TaskId
                };

                var tmpAwb = unitOfWork.Context.AWBs.SingleOrDefault(item => item.Id == model.ShipmentId);
                List<BLL.DAL.Core.GetAWBCustomsHeader_Result> tmpCustomsHeader;

                using (var coreUnit = new CoreDataHelpers())
                {
                    var awb = tmpAwb.Carrier.Carrier3Code + "-" + tmpAwb.AWBSerialNumber;
                    var awbFullNumber = tmpAwb.Port.IATACode + tmpAwb.Carrier.Carrier3Code + tmpAwb.AWBSerialNumber + tmpAwb.Port1.IATACode;
                    tmpCustomsHeader = coreUnit.Context.GetAWBCustomsHeader(awb, awbFullNumber).OrderBy(item => item.Part).ToList();                    
                }

                tmpModel.Parts = tmpCustomsHeader.Select(item => new SelectListItem { Text = item.Part, Value = item.Part });
                if (tmpCustomsHeader.Any())
                {
                    var tmpSelectByPart = tmpCustomsHeader[0];
                    tmpModel.Consignee = tmpSelectByPart.AgentName;
                    tmpModel.AWB = tmpSelectByPart.AwbNumber;
                }

                return PartialView("_CustomsHistoryView", tmpModel);
            }
        }

        public PartialViewResult GetCustomsHistoryDetails(CustomsHistoryDetailsModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpModel = new CustomsDetailsViewModel()
                {
                    ShipmentId = model.ShipmentId,
                    TaskId = model.TaskId
                };
                var tmpAwb = unitOfWork.Context.AWBs.Single(a => a.Id == model.ShipmentId);
                BLL.DAL.Core.GetAWBCustomsHeader_Result tmpSelectByPart;

                using (var coreUnit = new CoreDataHelpers())
                {
                    var awb = tmpAwb.Carrier.Carrier3Code + "-" + tmpAwb.AWBSerialNumber;
                    var awbFullNumber = tmpAwb.Port.IATACode + tmpAwb.Carrier.Carrier3Code + tmpAwb.AWBSerialNumber + tmpAwb.Port1.IATACode;
                    tmpSelectByPart = coreUnit.Context.GetAWBCustomsHeader("", "").Where(item => item.Part == model.Part).FirstOrDefault();
                }

               // var tmpSelectByPart = unitOfWork.Context.GetAWBCustomsHeader(model.ShipmentId).Where(item => item.Part == model.Part).FirstOrDefault();
                if (tmpSelectByPart != null)
                {
                    tmpModel.WayBillId = tmpSelectByPart.WaybillId;
                    tmpModel.CarrierFlight = tmpSelectByPart.CarrierFlight;
                    tmpModel.AirrivalAirport = tmpSelectByPart.ArrivalAirport;
                    tmpModel.ArrivalDate = tmpSelectByPart.ArrivalDate.HasValue ? tmpSelectByPart.ArrivalDate.Value.ToShortDateString() : String.Empty;
                    tmpModel.AirportTerminal = tmpSelectByPart.ArrivalTerminal;
                    tmpModel.Status = tmpSelectByPart.CStatus + " " + (tmpSelectByPart.TxnDate.HasValue ? tmpSelectByPart.TxnDate.Value.ToString("MM/dd/yyyy HH:mm") : String.Empty);
                    tmpModel.QTY = tmpSelectByPart.Qty;
                    tmpModel.EntityType = tmpSelectByPart.EntryType;
                    tmpModel.EntityNo = tmpSelectByPart.EntryNo;
                }
               

                return PartialView("_CustomsHistoryDetails", tmpModel);
            }
        }


        public JsonResult CustomsHisotryDS([DataSourceRequest] DataSourceRequest request,  long? TaskId , long ShipmentId , long ? WayBillId )
        {
            DataSourceResult tmpResult = null;
            using (var unitOfWork = new CoreUnitOfWork())
            {
                var tmpCustomsDetails = unitOfWork.Context.GetAWBCustomsDetails((short)request.Page, (byte)request.PageSize, null, String.Empty, WayBillId);

                tmpResult = tmpCustomsDetails.Select(item => new CustomsRow
                {
                    Code = item.Code,
                    Description = item.CDescription,
                    EntryNo = item.EntryNo,
                    EntryType = item.EntryType,
                    HWB = item.HWB,
                    Qty = item.Qty,
                    Status = item.Status,
                    StatusTime = item.StatusTime,
                    Text = item.CText,
                    Total = item.Total
                }).ToList().ToDataSourceResult(request);
            }
            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }


        [Obsolete]
        public JsonResult GetCustomsHistoryData(CustomsHistoryFilter filter)
        {
            List<CustomsRow> tmpList = new List<CustomsRow>();

            using (var unitOfWork = new CoreUnitOfWork())
            {
                var tmpCustomsDetails = unitOfWork.Context.GetAWBCustomsDetails(filter.iDisplayStart, filter.iDisplayLength, !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null, filter.sSearch, filter.WayBillId);

                tmpList = tmpCustomsDetails.Select(item => new CustomsRow
                {
                    Code = item.Code,
                    Description = item.CDescription,
                    EntryNo = item.EntryNo,
                    EntryType = item.EntryType,
                    HWB = item.HWB,
                    Qty = item.Qty,
                    Status = item.Status,
                    StatusTime = item.StatusTime,
                    Text = item.CText,
                    Total = item.Total
                }).ToList();

                if (tmpList.Any())
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpList.Count,
                        iTotalDisplayRecords = tmpList[0].Total,
                        aaData = tmpList
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = tmpList
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        public PartialViewResult GetNotocView(GetNotocViewModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpNototMasterDetails = unitOfWork.Context.GetNotocMaster(model.FlightId).FirstOrDefault();
                if (tmpNototMasterDetails != null)
                {
                    var tmpModel = new NotocDetailsModel()
                    {
                        TaskId = model.TaskId,
                        NotocId = tmpNototMasterDetails.NotocId,
                        Carrier = tmpNototMasterDetails.Carrier,
                        FlightNumber = tmpNototMasterDetails.FlightNumber,
                        FlightId = model.FlightId,
                        FlightDate = tmpNototMasterDetails.FlightDate,
                        StationOfLoading = tmpNototMasterDetails.StationOfLoading,
                        Origin = tmpNototMasterDetails.Origin,
                        Destination = tmpNototMasterDetails.Destination,
                        AircraftRegNumber = tmpNototMasterDetails.AircraftRegistration,
                        LoadingSupervisor = tmpNototMasterDetails.LoadingSupervisor,
                        OtherInformation = tmpNototMasterDetails.OtherInfo,
                        PreparedBy = tmpNototMasterDetails.PreparedBy,
                        CheckedBy = tmpNototMasterDetails.CheckedBy
                    };
                    return PartialView("_TaskNotocView", tmpModel);
                }
                else
                {
                    return PartialView("_TaskNotocView", new NotocDetailsModel() { TaskId = model.TaskId, FlightId = model.FlightId });
                }
            }
        }


        public JsonResult GetNotocDGData(NotocDGGridFilter filter)
        {
            var tmpList = new List<NotocDGRow>();

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpNotocDGData = unitOfWork.Context.GetNotocDGDetails(filter.FlightId).ToList();
                if (tmpNotocDGData.Any())
                {
                    foreach (var item in tmpNotocDGData)
                    {
                        tmpList.Add(new NotocDGRow
                        {
                            Id = item.DetailId,
                            NotocId = item.NotocId,
                            IMPCode = item.IMPCode,
                            NetQTIorTI = item.PackingTypeAndQty,
                            NumberOfPackages = item.NoOfpackages,
                            PackingGroup = item.PackingGroup,
                            PSN = item.ProperShippingName,
                            RadioActiveCat = item.RadioactiveMaterials,
                            SubRisk = item.SubRisk,
                            ULDNumber = item.ULD,
                            UldId = item.UldId,
                            UNClass = item.UNClass,
                            UNClassId = item.UNClassId,
                            UNNumber = item.UNNumber,
                            Awb = item.AWB,
                            AwbId = item.AwbId,
                            CAO = item.AircraftTypeCode,
                            CAOId = item.AircraftTypeId,
                            Compartment = item.AircraftPosition,
                            Destination = item.Destination,
                            ERGCode = item.EmergencyResponseCode,
                            FlightId = filter.FlightId
                        });
                    }
                }
            }

            return Json(new
            {
                sEcho = filter.sEcho,
                iTotalRecords = tmpList.Count,
                iTotalDisplayRecords = tmpList.Count,
                aaData = tmpList
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveNotocMaster(NotocMasterModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                unitOfWork.Context.UpdateNotocMaster(model.FlightId, SessionManager.UserInfo.UserLocalId, model.CheckedBy, model.LoadingSupervisor, null, model.OtherInfo);
                var jsonData = new { };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetNotocNonHazData(NotocNonHazGridFilter filter)
        {
            var tmpList = new List<NotocNoHazRow>();

            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpNotocDGData = unitOfWork.Context.GetNotocOtherCargoDetails(filter.FlightId).ToList();
                if (tmpNotocDGData.Any())
                {
                    foreach (var item in tmpNotocDGData)
                    {
                        tmpList.Add(new NotocNoHazRow
                        {
                            Awb = item.AWB,
                            AwbId = item.AwbId,
                            Code = item.Code,
                            Description = item.ContentsDescription,
                            Destination = item.Destination,
                            Id = item.DetailId,
                            LoadingPosition = item.AircraftPosition,
                            PackageCount = item.NoOfpackages,
                            SupInfo = item.SupplementaryInfo,
                            ULDNumber = item.ULD,
                            UldId = item.UldId,
                            NotocId = item.NotocId.HasValue ? item.NotocId.Value : 0,
                            FlightId = filter.FlightId
                        });
                    }
                }
            }

            return Json(new
            {
                sEcho = filter.sEcho,
                iTotalRecords = tmpList.Count,
                iTotalDisplayRecords = tmpList.Count,
                aaData = tmpList
            }, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult GetAddEditNotocDGView(NotocDGRow model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpModel = model;
                if (model.Id != 0)
                {
                    var tmpNotocDGData = unitOfWork.Context.GetNotocDGDetails(model.FlightId).Where
                        (item => item.DetailId == model.Id).Select(item => new NotocDGRow
                        {
                            Id = item.DetailId,
                            NotocId = item.NotocId,
                            IMPCode = item.IMPCode,
                            NetQTIorTI = item.PackingTypeAndQty,
                            NumberOfPackages = item.NoOfpackages,
                            PackingGroup = item.PackingGroup,
                            PSN = item.ProperShippingName,
                            RadioActiveCat = item.RadioactiveMaterials,
                            SubRisk = item.SubRisk,
                            ULDNumber = item.ULD,
                            UldId = item.UldId,
                            UNClass = item.UNClass,
                            UNClassId = item.UNClassId,
                            UNNumber = item.UNNumber,
                            Awb = item.AWB,
                            AwbId = item.AwbId,
                            CAO = item.AircraftTypeCode,
                            CAOId = item.AircraftTypeId,
                            Compartment = item.AircraftPosition,
                            Destination = item.Destination,
                            ERGCode = item.EmergencyResponseCode,
                            FlightId = model.FlightId
                        }).FirstOrDefault();
                    tmpModel = tmpNotocDGData;
                }
                return PartialView("_AddEditNotocDG", tmpModel);
            }
        }

        public PartialViewResult GetAddEditNotocSPView(NotocNoHazRow model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpModel = model;
                if (model.Id != 0)
                {
                    var tmpNotocSPData = unitOfWork.Context.GetNotocOtherCargoDetails(model.FlightId).Where
                        (item => item.DetailId == model.Id).Select(item => new NotocNoHazRow
                        {
                            Awb = item.AWB,
                            AwbId = item.AwbId,
                            Code = item.Code,
                            Description = item.ContentsDescription,
                            Destination = item.Destination,
                            Id = item.DetailId,
                            LoadingPosition = item.AircraftPosition,
                            PackageCount = item.NoOfpackages,
                            SupInfo = item.SupplementaryInfo,
                            ULDNumber = item.ULD,
                            UldId = item.UldId,
                            NotocId = item.NotocId.HasValue ? item.NotocId.Value : 0,
                            FlightId = model.FlightId
                        }).FirstOrDefault();
                    tmpModel = tmpNotocSPData;
                }
                return PartialView("_AddEditNotocNoHaz", tmpModel);
            }
        }


        public JsonResult DeleteNotocDG(long Id)
        {
            var jsonResult = new { Status = true, Message = "" };
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                try
                {
                    var tmpItemToDelete = unitOfWork.Context.FlightNotocDGDetails.FirstOrDefault(item => item.Id == Id);
                    if (tmpItemToDelete != null)
                    {
                        unitOfWork.Context.FlightNotocDGDetails.Remove(tmpItemToDelete);
                        unitOfWork.Context.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not delete Dangerous Goods info." };
                }
            }

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FinalizeCheckListResult(ChecklistResultsParam param)
        {
            using (CustomsUnit unitOfWork = new CustomsUnit())
            {
                var userName = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname;
                unitOfWork.FinalizeChecklistResult(param);
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveCheckListResult(ChecklistResultsParam param)
        {
            using (CustomsUnit unitOfWork = new CustomsUnit())
            {
                var userName = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname;
                unitOfWork.SaveCheckListResult(param);
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddEditNotocDG(NotocDGRow model)
        {
            var jsonResult = new { Status = true, Message = "" };
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                try
                {
                    int tmpResult = unitOfWork.Context.AddEditNotocDGDetails(model.FlightId, model.AwbId, model.PSN, model.UNClassId, model.UNNumber, model.PackingGroup, model.Packages, model.NetQTIorTI, model.RadioActiveCat, model.IMPCode, model.ERGCode, model.UldId, model.CAOId, model.Compartment, model.Id, model.SubRisk);
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not update Dangerous Goods info." };
                }
            }

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteNotocSP(long Id)
        {
            var jsonResult = new { Status = true, Message = "" };
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                try
                {
                    var tmpItemToDelete = unitOfWork.Context.FlightNotocOtherCargoDetails.FirstOrDefault(item => item.Id == Id);
                    if (tmpItemToDelete != null)
                    {
                        unitOfWork.Context.FlightNotocOtherCargoDetails.Remove(tmpItemToDelete);
                        unitOfWork.Context.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not delete Cargo Special info." };
                }
            }

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddEditNotocOtherInfo(NotocNoHazRow model)
        {
            var jsonResult = new { Status = true, Message = "" };
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                try
                {
                    int tmpResult = unitOfWork.Context.AddEditNotocOtherCargoDetails(model.FlightId, model.AwbId, model.Description, model.PackageCount, model.Qty.ToString(), model.SupInfo, model.Code, model.UldId, model.LoadingPosition, model.Id);
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not update Other Special Cargo info." };
                }
            }

            return Json(jsonResult, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetLAChecklistView(AwbDGChecklistParam param)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                AwbDGChecklistModel tmpModel = new AwbDGChecklistModel();

                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == param.TaskId);

                // if Task is Not Assigned, assign to the logged in user

                //TODO: Harut fix this
                if (tmpTask.StatusId == (int)enumTaskStatuses.NotAssigned)
                {
                    tmpTask.TaskAssignments.Add(new TaskAssignment() { UserId = SessionManager.UserInfo.UserLocalId, AssignedBy = SessionManager.UserInfo.UserLocalId, RecDate = DateTime.Now, AssignedOn = DateTime.Now });
                    unitOfWork.Context.SaveChanges();
                }

                var tmpLA = unitOfWork.Context.LARCheckLists.OrderByDescending(item => item.RecDate).FirstOrDefault();

                if (tmpLA == null)
                {
                    //TODO : Harut handle this case, return error view with message
                }

                var tmpAwg = unitOfWork.Context.AWBs.SingleOrDefault(i => i.Id == param.AwbId);

                var tmpLAResult = unitOfWork.Context.LARCheckListResultsParents.SingleOrDefault(item => item.EntityId.HasValue && item.EntityId.Value == param.AwbId && item.EntityTypeId == (int)enumEntityTypes.AWB);

                var tmpUserWarehouse = unitOfWork.Context.UserWarehouses.FirstOrDefault(item => item.IsDefault && item.UserId == SessionManager.UserInfo.UserLocalId);

                var tmpWarehouse = tmpUserWarehouse != null ? tmpUserWarehouse.Warehouse : null;

                var tmpPlace = tmpWarehouse != null ? (tmpWarehouse.State != null ? tmpWarehouse.State.TwoCharacterCode : String.Empty) + "," + tmpWarehouse.City : String.Empty;

                DGListModel tmpResult = new DGListModel
                {
                    EntityId = param.AwbId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    TaskId = param.TaskId,
                    Username = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname,
                    ShipmentNumber = tmpAwg.AWBSerialNumber,
                    Place = tmpPlace

                };

                if (tmpLAResult != null)
                {
                    tmpResult.Place = tmpLAResult.Place;
                    tmpResult.CommentedOn = tmpLAResult.CompletedOn;
                    tmpResult.Comments = tmpLAResult.Comments;
                    tmpResult.Username = tmpLAResult.UserName;
                    tmpResult.SignatureData = tmpLAResult.SignatureData;
                    tmpResult.Place = tmpPlace;
                }

                var tmpLAListData = tmpLA.LARCheckListItemHeaders.Where(item => item.IsActive.HasValue && item.IsActive.Value).Select(item => new DGListHeader
                {
                    Id = item.Id,
                    Name = item.ListItemHeader,
                    Items = item.LARCheckListItems.Where(e => e.ParentId == null).Select(e => new DGCheckItem
                    {
                        Id = e.Id,
                        Options = e.ValueList,
                        ParentId = e.ParentId,
                        SubItems = e.LARCheckListItems1.Select(p => new DGCheckItem
                        {
                            Id = p.Id,
                            Options = p.ValueList,
                            Number = p.SortOrder,
                            Text = p.ListItemText,
                            SelectedOption = tmpLAResult != null && tmpLAResult.LARCheckListResults.FirstOrDefault(u => u.CheckListItemId == p.Id) != null ? tmpLAResult.LARCheckListResults.First(u => u.CheckListItemId == p.Id).ResultValue : String.Empty
                        }).ToList(),
                        Text = e.ListItemText,
                        Number = e.SortOrder,
                        SelectedOption = tmpLAResult != null && tmpLAResult.LARCheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id) != null ? tmpLAResult.LARCheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id).ResultValue : String.Empty
                    }).ToList()
                }).ToList();

                tmpResult.CheckList = tmpLAListData;

                return PartialView("_LAChecklist", tmpResult);
            }
        }

        public PartialViewResult GetDGChecklistView(AwbDGChecklistParam param)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {

                var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == param.TaskId);

                // if Task is Not Assigned, assign to the logged in user
                if (tmpTask.StatusId == (int)enumTaskStatuses.NotAssigned)
                {
                    tmpTask.TaskAssignments.Add(new TaskAssignment() { UserId = SessionManager.UserInfo.UserLocalId, AssignedBy = SessionManager.UserInfo.UserLocalId, RecDate = DateTime.Now, AssignedOn = DateTime.Now });
                    unitOfWork.Context.SaveChanges();
                }

                var tmpDG = unitOfWork.Context.DGRCheckLists.OrderByDescending(item => item.RecDate).FirstOrDefault();

                if (tmpDG == null)
                {
                    //TODO : Harut handle this case, return error view with message
                }

                var tmpAwg = unitOfWork.Context.AWBs.SingleOrDefault(i => i.Id == param.AwbId);

                var tmpDGResult = unitOfWork.Context.DGRCheckListResultsParents.SingleOrDefault(item => item.EntityId.HasValue && item.EntityId.Value == param.AwbId && item.EntityTypeId == (int)enumEntityTypes.AWB);

                var tmpUserWarehouse = unitOfWork.Context.UserWarehouses.FirstOrDefault(item => item.IsDefault && item.UserId == SessionManager.UserInfo.UserLocalId);

                var tmpWarehouse = tmpUserWarehouse != null ? tmpUserWarehouse.Warehouse : null;

                var tmpPlace = tmpWarehouse != null ? (tmpWarehouse.State != null ? tmpWarehouse.State.TwoCharacterCode : String.Empty) + "," + tmpWarehouse.City : String.Empty;

                DGListModel tmpResult = new DGListModel
                {
                    EntityId = param.AwbId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    TaskId = param.TaskId,
                    Username = SessionManager.UserInfo.Firstname + " " + SessionManager.UserInfo.Lastname,
                    ShipmentNumber = tmpAwg.AWBSerialNumber,
                    Place = tmpPlace

                };

                if (tmpDGResult != null)
                {
                    tmpResult.Place = tmpDGResult.Place;
                    tmpResult.CommentedOn = tmpDGResult.CompletedOn;
                    tmpResult.Comments = tmpDGResult.Comments;
                    tmpResult.Username = tmpDGResult.UserName;
                    tmpResult.SignatureData = tmpDGResult.SignatureData;
                    tmpResult.Place = tmpPlace;
                }

                var tmpDGListData = tmpDG.DGRCheckListItemHeaders.Where(item => item.IsActive.HasValue && item.IsActive.Value).Select(item => new DGListHeader
                {
                    Id = item.Id,
                    Name = item.ListItemHeader,
                    Items = item.DGRCheckListItems.Where(e => e.ParentId == null).Select(e => new DGCheckItem
                    {
                        Id = e.Id,
                        Options = e.ValueList,
                        ParentId = e.ParentId,
                        SubItems = e.DGRCheckListItems1.Select(p => new DGCheckItem
                        {
                            Id = p.Id,
                            Options = p.ValueList,
                            Number = p.SortOrder,
                            Text = p.ListItemText,
                            SelectedOption = tmpDGResult != null && tmpDGResult.DGRCheckListResults.FirstOrDefault(u => u.CheckListItemId == p.Id) != null ? tmpDGResult.DGRCheckListResults.First(u => u.CheckListItemId == p.Id).ResultValue : String.Empty
                        }).ToList(),
                        Text = e.ListItemText,
                        Number = e.SortOrder,
                        SelectedOption = tmpDGResult != null && tmpDGResult.DGRCheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id) != null ? tmpDGResult.DGRCheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id).ResultValue : String.Empty
                    }).ToList()
                }).ToList();

                tmpResult.CheckList = tmpDGListData;

                return PartialView("_DGChecklist", tmpResult);
            }
        }

    }
}
