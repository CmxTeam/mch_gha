﻿using MCH.Web.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace MCH.Web.Controllers
{
    [SessionAuthentication]
    public class BaseController : Controller
    {

        public EmptyResult KeepAlive() 
        {
            return new EmptyResult();
        }

        public new RedirectToRouteResult RedirectToAction(string action, string controller, object model)
        {
            return base.RedirectToAction(action, controller, model);
        }

        protected static string ConvertToXml<T>(IEnumerable<T> fields)
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            string xmlString;
            using (var memStream = new MemoryStream())
            {
                serializer.Serialize(memStream, fields);
                var encoding = Encoding.UTF8;
                var xmlBytes = new byte[memStream.Length];
                memStream.Seek(0, SeekOrigin.Begin);
                memStream.Read(xmlBytes, 0, Convert.ToInt32(memStream.Length));
                xmlString = encoding.GetString(xmlBytes);
            }
            return xmlString;
        }

        protected static string ConvertToXml<T>(T fields)
        {
            var serializer = new XmlSerializer(typeof(T));
            string xmlString;
            using (var memStream = new MemoryStream())
            {
                serializer.Serialize(memStream, fields);
                var encoding = Encoding.UTF8;
                var xmlBytes = new byte[memStream.Length];
                memStream.Seek(0, SeekOrigin.Begin);
                memStream.Read(xmlBytes, 0, Convert.ToInt32(memStream.Length));
                xmlString = encoding.GetString(xmlBytes);
            }
            return xmlString;
        }
    }
}
