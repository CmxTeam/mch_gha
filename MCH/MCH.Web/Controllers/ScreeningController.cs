﻿using MCH.BLL.Units;
using MCH.Web.Models.Screening;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class ScreeningController : BaseController
    {

        public JsonResult GetTaskScreenings(ScreeningTaskFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var data = unitOfWork.Context.GetScreeningTransactions(
                    filter.iDisplayStart, filter.iDisplayLength,
                    !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null,
                    filter.sSearch, filter.TaskId);

                var tmpData = data.ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult ClearAlarm(SelectedScreenings model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                unitOfWork.Context.ProcessScreeningClearAlarm(model.TaskId, SessionManager.UserInfo.UserLocalId, model.Comment);

                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
