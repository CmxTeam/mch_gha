﻿using Kendo.Mvc.UI;
using MCH.BLL.Units;
using MCH.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using MCH.Web.Models.TrackAndTrace;
using MCH.Web.Utilities;
using MCH.BLL.Model.Enum;
using MCH.BLL.DAL.Data;

namespace MCH.Web.Controllers
{
    public class TrackAndTraceController : Controller
    {
        public PartialViewResult Index()
        {
            return PartialView("_TrackList");
        }

        public JsonResult TrackListDatasource([DataSourceRequest] DataSourceRequest request, TrackTraceSearchParam param)
        {
            IEnumerable<TrackGridRow> tmpList = new List<TrackGridRow>();
            DataSourceResult tmpResult = null;
            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                switch (param.SearchType)
                {
                    case enumTrackTraceSearchType.AWB:
                        {
                            tmpList = this.SearchByAWB(param.Carrier, param.AWB, unit);
                            break;
                        }
                    case enumTrackTraceSearchType.HWB:
                        {
                            tmpList = this.SearchByHWB(param.HWB, unit);
                            break;
                        }
                    case enumTrackTraceSearchType.Flight:
                        {
                            tmpList = this.SearchByFlight(param.AirlineCode, param.FlightNumber, param.OriginCode, param.DestinationCode, param.FlightDate, unit);
                            break;
                        }
                    case enumTrackTraceSearchType.ULD:
                        {
                            tmpList = this.SearchByULD(param.ULD, unit);
                            break;
                        }
                    default:
                        break;
                }
                tmpResult = tmpList.ToDataSourceResult(request);
            }


            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }


        private IEnumerable<TrackGridRow> SearchByAWB(string carrierCode, string awbNumber, TasksUnitOfWork unit)
        {
            IEnumerable<TrackGridRow> tmpList = new List<TrackGridRow>();

            if (!String.IsNullOrWhiteSpace(carrierCode) && !String.IsNullOrWhiteSpace(awbNumber))
            {
                long? tmpUserPortId = SessionManager.UserInfo.DefaultSiteId;
                long? tmpUserWarehouse = SessionManager.UserInfo.DefaultWarehouseId;

                var tmpAWB = unit.Context.AWBs.FirstOrDefault(a => a.AWBSerialNumber == awbNumber && a.Carrier.Carrier3Code == carrierCode);
                if (tmpAWB != null)
                {

                    if (tmpAWB.OriginId == tmpUserPortId)
                    {
                        //export

                        var tmpQuery = unit.Context.LoadingPlans.Where(lp => lp.AWBId == tmpAWB.Id);

                        tmpList = tmpQuery.Select(lp => new TrackGridRow
                        {
                            AirlineCode = lp.AWB.Account != null ? lp.AWB.Account.AccountCarriers.FirstOrDefault().Carrier.CarrierCode : string.Empty,
                            AWB = lp.AWB.Carrier.Carrier3Code + "-" + lp.AWB.AWBSerialNumber,
                            Carrier = lp.AWB.Carrier.Carrier3Code,
                            Description = lp.AWB.DescriptionOfGoods,
                            Destination = lp.AWB.Port1.IATACode,
                            Origin = lp.AWB.Port.IATACode,
                            Pieces = lp.AWB.TotalPieces,
                            Weight = lp.AWB.TotalWeight,
                            WeightUOM = lp.AWB.UOM.TwoCharCode,
                            EntityId = lp.AWB.Id,
                            EntityTypeId = (int)enumEntityTypes.AWB,
                            ETA = lp.FlightManifest.ETA,
                            ETD = lp.FlightManifest.ETD,
                            FlightNumber = lp.FlightManifest.FlightNumber,
                            Status = lp.AWB.Status.DisplayName,
                            StatusTime = MCHDBEntities.MCH_ConvertToLocalDateTime((int)tmpUserWarehouse.Value, lp.AWB.StatusTimestamp),
                            Location = unit.Context.Dispositions.Where(d => d.AWBId == lp.AWBId).OrderByDescending(e => e.Timestamp).Select(e => e.WarehouseLocation.Location).FirstOrDefault()
                        }).Distinct();
                    }
                    else
                    {
                        //import

                        var tmpQuery = unit.Context.ManifestAWBDetails.Where(mad => mad.AWBId == tmpAWB.Id);

                        tmpList = tmpQuery.Select(mad => new TrackGridRow
                        {
                            AirlineCode = mad.AWB.Account != null ? mad.AWB.Account.AccountCarriers.FirstOrDefault().Carrier.CarrierCode : string.Empty,
                            AWB = mad.AWB.Carrier.Carrier3Code + "-" + mad.AWB.AWBSerialNumber,
                            Carrier = mad.AWB.Carrier.Carrier3Code,
                            Description = mad.AWB.DescriptionOfGoods,
                            Destination = mad.AWB.Port1.IATACode,
                            Origin = mad.AWB.Port.IATACode,
                            Pieces = mad.AWB.TotalPieces,
                            Weight = mad.AWB.TotalWeight,
                            WeightUOM = mad.AWB.UOM.TwoCharCode,
                            EntityId = mad.AWB.Id,
                            EntityTypeId = (int)enumEntityTypes.AWB,
                            ETA = mad.FlightManifest.ETA,
                            ETD = mad.FlightManifest.ETD,
                            FlightNumber = mad.FlightManifest.FlightNumber,
                            Status = mad.AWB.Status.DisplayName,
                            StatusTime = MCHDBEntities.MCH_ConvertToLocalDateTime((int)tmpUserWarehouse.Value, mad.AWB.StatusTimestamp),
                            Location = unit.Context.Dispositions.Where(d => d.AWBId == mad.AWBId).OrderByDescending(e => e.Timestamp).Select(e => e.WarehouseLocation.Location).FirstOrDefault()
                        }).Distinct();
                    }
                }

            }
            return tmpList;
        }

        private IEnumerable<TrackGridRow> SearchByHWB(string hwbNumber, TasksUnitOfWork unit)
        {
            IEnumerable<TrackGridRow> tmpList = new List<TrackGridRow>();

            if (!String.IsNullOrWhiteSpace(hwbNumber))
            {
                long? tmpUserPortId = SessionManager.UserInfo.DefaultSiteId;
                long? tmpUserWarehouse = SessionManager.UserInfo.DefaultWarehouseId;

                var tmpHWB = unit.Context.HWBs.FirstOrDefault(i => i.HWBSerialNumber == hwbNumber);
                if (tmpHWB != null)
                {
                    long tmpAWBId = tmpHWB.AWBs_HWBs.First().AWB.Id;
                    string tmpHWBSerial = tmpHWB.HWBSerialNumber;

                    if (tmpHWB.AWBs_HWBs.First().AWB.OriginId == tmpUserPortId)
                    {
                        //export
                        var tmpQuery = unit.Context.LoadingPlans.Where(lp => lp.AWBId == tmpAWBId);

                        tmpList = tmpQuery.Select(lp => new TrackGridRow
                        {
                            AirlineCode = lp.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.Account.AccountCarriers.FirstOrDefault().Carrier.CarrierCode,
                            AWB = lp.AWB.Carrier.Carrier3Code + "-" + lp.AWB.AWBSerialNumber,
                            HWB = lp.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.HWBSerialNumber,
                            Carrier = lp.AWB.Carrier.Carrier3Code,
                            Description = lp.AWB.DescriptionOfGoods,
                            Destination = lp.AWB.Port1.IATACode,
                            Origin = lp.AWB.Port.IATACode,
                            Pieces = lp.AWB.TotalPieces,
                            Weight = lp.AWB.TotalWeight,
                            WeightUOM = lp.AWB.UOM.TwoCharCode,
                            EntityId = lp.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.Id,
                            EntityTypeId = (int)enumEntityTypes.HWB,
                            ETA = lp.FlightManifest.ETA,
                            ETD = lp.FlightManifest.ETD,
                            FlightNumber = lp.FlightManifest.FlightNumber,
                            Status = lp.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.Status.DisplayName,
                            StatusTime = MCHDBEntities.MCH_ConvertToLocalDateTime((int)tmpUserWarehouse.Value, lp.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.StatusTimestamp),
                            Location = unit.Context.Dispositions.Where(d => d.AWBId == lp.AWBId).OrderByDescending(e => e.Timestamp).Select(e => e.WarehouseLocation.Location).FirstOrDefault()
                        }).Distinct();
                    }
                    else
                    {
                        //import

                        var tmpQuery = unit.Context.ManifestAWBDetails.Where(e => e.AWBId == tmpAWBId);

                        tmpList = tmpQuery.Select(e => new TrackGridRow
                        {
                            AirlineCode = e.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.Account.AccountCarriers.FirstOrDefault().Carrier.CarrierCode,
                            AWB = e.AWB.Carrier.Carrier3Code + "-" + e.AWB.AWBSerialNumber,
                            HWB = e.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.HWBSerialNumber,
                            Carrier = e.AWB.Carrier.Carrier3Code,
                            Description = e.AWB.DescriptionOfGoods,
                            Destination = e.AWB.Port1.IATACode,
                            Origin = e.AWB.Port.IATACode,
                            Pieces = e.AWB.TotalPieces,
                            Weight = e.AWB.TotalWeight,
                            WeightUOM = e.AWB.UOM.TwoCharCode,

                            EntityId = e.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.Id,
                            EntityTypeId = (int)enumEntityTypes.HWB,

                            ETA = e.FlightManifest.ETA,
                            ETD = e.FlightManifest.ETD,
                            FlightNumber = e.FlightManifest.FlightNumber,
                            Status = e.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.Status.DisplayName,
                            StatusTime = MCHDBEntities.MCH_ConvertToLocalDateTime((int)tmpUserWarehouse.Value, e.AWB.AWBs_HWBs.FirstOrDefault(h => h.HWBId == tmpHWB.Id).HWB.StatusTimestamp),
                            Location = unit.Context.Dispositions.Where(d => d.AWBId == e.AWBId).OrderByDescending(p => p.Timestamp).Select(p => p.WarehouseLocation.Location).FirstOrDefault()
                        }).Distinct();
                    }
                }

            }
            return tmpList;
        }

        private IEnumerable<TrackGridRow> SearchByFlight(string airline, string flight, string origin, string dest, DateTime? flightDate, TasksUnitOfWork unit)
        {
            IEnumerable<TrackGridRow> tmpList = new List<TrackGridRow>();

            if (!string.IsNullOrWhiteSpace(flight))
            {
                long? tmpUserPortId = SessionManager.UserInfo.DefaultSiteId;
                long? tmpUserWarehouse = SessionManager.UserInfo.DefaultWarehouseId;


                var tmpQuery = unit.Context.FlightManifests.Where(f => f.FlightNumber.Contains(flight));


                #region Apply Filters
                if (!string.IsNullOrWhiteSpace(airline))
                {
                    tmpQuery = tmpQuery.Where(f => f.Carrier.CarrierCode == airline);
                }
                if (!String.IsNullOrWhiteSpace(origin))
                {
                    tmpQuery = tmpQuery.Where(f => f.Port.IATACode == origin);
                }
                if (!String.IsNullOrWhiteSpace(dest))
                {
                    tmpQuery = tmpQuery.Where(f => f.Port1.IATACode == dest);
                }
                if (flightDate.HasValue && flightDate > DateTime.MinValue)
                {
                    tmpQuery = tmpQuery.Where(f => f.ETD == flightDate.Value);
                } 
                #endregion

                //TODO : check flight weight in db
                tmpList = tmpQuery.Select(u => new TrackGridRow
                {
                    AirlineCode = u.Carrier.CarrierCode,
                    FlightNumber = u.FlightNumber,
                    Origin = u.Port.IATACode,
                    Destination = u.Port1.IATACode,
                    ETA = u.ETA,
                    ETD = u.ETD,
                    Pieces = u.TotalPieces,
                    TotalULDs = u.TotalULDs,
                    TotalAWBs = u.TotalAWBs,
                    Location = u.StagingLocationId.HasValue ? u.WarehouseLocation.Location : u.WarehouseLocation1.Location,
                    Status = u.Status.DisplayName,
                    StatusTime = MCHDBEntities.MCH_ConvertToLocalDateTime((int)tmpUserWarehouse.Value, u.StatusTimestamp),
                    WeightUOM = u.ULDs.Any() ? u.ULDs.FirstOrDefault().UOM.TwoCharCode : u.ManifestAWBDetails.FirstOrDefault().AWB.UOM.TwoCharCode,
                    Weight = u.ULDs.Any()? u.ULDs.Sum(e=> e.TotalWeight) : u.ManifestAWBDetails.Sum(e=> e.AWB.TotalWeight),

                    EntityId = u.Id,
                    EntityTypeId = (int)enumEntityTypes.FlightManifest

                });
            }

            return tmpList;
        }

        private IEnumerable<TrackGridRow> SearchByULD(string uldCodeCarrierNumber, TasksUnitOfWork unit)
        {
            IEnumerable<TrackGridRow> tmpList = new List<TrackGridRow>();
            if (!String.IsNullOrWhiteSpace(uldCodeCarrierNumber))
            {
                long? tmpUserPortId = SessionManager.UserInfo.DefaultSiteId;
                long? tmpUserWarehouse = SessionManager.UserInfo.DefaultWarehouseId;

                tmpList = unit.Context.ULDs.Where(u => uldCodeCarrierNumber.Trim() == (u.ShipmentUnitType.Code + "" + u.SerialNumber + "" + u.Carrier.CarrierCode)).Select(u => new TrackGridRow
                {
                    AirlineCode = u.Carrier.CarrierCode,
                    ULD = u.ShipmentUnitType.Code + "" + u.SerialNumber + "" + u.Carrier.CarrierCode,
                    FlightNumber = u.FlightManifest.FlightNumber,
                    Origin = u.FlightManifest.Port.IATACode,
                    Destination = u.FlightManifest.Port1.IATACode,
                    ETA = u.FlightManifest.ETA,
                    ETD = u.FlightManifest.ETD,
                    IsBUP = u.IsBUP,
                    IsTrasfer = u.ManifestAWBDetails.Where(m => m.AWB.DestinationId != tmpUserPortId.Value).Any(),
                    Pieces = u.TotalPieces,
                    Weight = u.TotalWeight,
                    WeightUOM = u.UOM.TwoCharCode,
                    Location = u.WarehouseLocation.Location,
                    Status = u.Status.DisplayName,
                    StatusTime = MCHDBEntities.MCH_ConvertToLocalDateTime((int)tmpUserWarehouse.Value, u.StatusTimestamp),

                    EntityId = u.Id,
                    EntityTypeId = (int)enumEntityTypes.ULD

                }).Distinct();

            }

            return tmpList;
        }

    }
}