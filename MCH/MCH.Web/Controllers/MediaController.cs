﻿using MCH.BLL.Units;
using MCH.Web.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace MCH.Web.Controllers
{
    public class MediaController : BaseController
    {

        [HttpGet]
        public JsonResult GetTaskMedia(long taskId)
        {
            using (PhotoCaptureUnit unit = new PhotoCaptureUnit())
            {
                TaskMedia tmpData = new TaskMedia();
                tmpData.TaskId = taskId;
                

                var tmpMediaXml = unit.GetTaskMedia(taskId);

                if (tmpMediaXml.Any()) 
                {
                    tmpData.MediaArray = new List<MediaModel>();
                    foreach (var node in tmpMediaXml)
                    {
                        var tmpDescription = new MediaDescription();
                        tmpDescription.DateTaken = node.SelectSingleNode("KeywordsXML/Keywords/DateTaken").InnerText;
                        tmpDescription.Location = node.SelectSingleNode("KeywordsXML/Keywords/Location").InnerText;
                        tmpDescription.UserId = node.SelectSingleNode("KeywordsXML/Keywords/UserID").InnerText;
                        tmpDescription.Reason = node.SelectSingleNode("KeywordsXML/Keywords/Reason").InnerText;
                        tmpDescription.PCS = node.SelectSingleNode("KeywordsXML/Keywords/PCS").InnerText;

                        var tmpMedia = new MediaModel {
                            ImageLargUrl = node.SelectSingleNode("Image").InnerText,
                            ImageMainUrl = node.SelectSingleNode("Image").InnerText,
                            ImageThumbUrl = node.SelectSingleNode("Image").InnerText,
                            Title = node.Attributes["Reference"].InnerText,
                            Description = tmpDescription.ToString()
                        };

                        tmpData.MediaArray.Add(tmpMedia);
                    }
                }

                return Json(tmpData, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
