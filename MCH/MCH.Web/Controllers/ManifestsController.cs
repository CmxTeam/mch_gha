﻿using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using MCH.BLL.Units;
using MCH.Web.Models.Manifest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.Utilities;
using MCH.BLL.Model.Enum;

namespace MCH.Web.Controllers
{
    public class ManifestsController : Controller
    {
        public PartialViewResult GetEntityManifestsView(long entityId, enumEntityTypes entityTypeId, long? taskId)
        {
            EntityManifestsViewModel tmpModel = new EntityManifestsViewModel();
            tmpModel.EntityId = entityId;
            tmpModel.EntityTypeId = entityTypeId;
            tmpModel.TaskId = taskId;
            return PartialView("_EntityManifestsView", tmpModel);
        }

        public JsonResult EntityManifestsDS([DataSourceRequest] DataSourceRequest request, long entityId, enumEntityTypes entityTypeId, long ? taskId)
        {
            DataSourceResult tmpResult = null;
            long ? tmpWarehouseId = SessionManager.UserInfo.DefaultWarehouseId;

            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                var tmpQuery = unit.Context.MCH_GetFlightManifests(entityId, taskId, (int)tmpWarehouseId).Select(e => new EntityManifestRow
                {
                    Id = e.Id,
                    AwbNumber = e.AWB,
                    ControlNumber = e.ControlNumber,
                    Destination = e.AWBDest,
                    Origin = e.AwbOrigin,
                    Part = e.AwbPart,
                    Pieces = e.AwbPiecesInULD,
                    SpecialHandling = e.SHC,
                    TotalPieces = e.TotalAwbPcs,
                    Transfer = e.TransferPort,
                    ULDNumber = e.ULD,
                    Weight = e.AWBULDWeight,
                    WeightUOM = e.WeightUOM
                }).ToList();

                tmpResult = tmpQuery.ToDataSourceResult(request);
            }

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }
    }
}