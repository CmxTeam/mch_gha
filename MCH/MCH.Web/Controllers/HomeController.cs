﻿using MCH.Web.Utilities;
using MCH.Web.Utilities.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.Models.Common;

namespace MCH.Web.Controllers
{
    public class HomeController : BaseController
    {
        
        public ActionResult Index()
        {
            if (SessionManager.UserInfo != null && !String.IsNullOrWhiteSpace(SessionManager.UserInfo.DefaultMenuItem.NavigationPath) && !SessionManager.Loaded)
            {
                string tmpUserDefaultMenu = SessionManager.UserInfo.DefaultMenuItem.NavigationPath;
                string tmpActionName = tmpUserDefaultMenu.Split('/')[1];
                string tmpControllerName = tmpUserDefaultMenu.Split('/')[0];
                ViewBag.DefaultAction = tmpActionName;
                ViewBag.DefaultController = tmpControllerName;
                SessionManager.Loaded = true;
            }
            List<MenuItemModel> tmpList = new List<MenuItemModel>();
            if (SessionManager.UserInfo.AppNavigations != null)
            {
               tmpList = SessionManager.UserInfo.AppNavigations.Select(n => new MenuItemModel {Name = n.Name, Icon = n.IconBase64, NavigationLink = n.NavigationPath, Title = n.Text, Id = n.Id }).ToList();
            }

            return View(tmpList);
        }

        public ActionResult TestPage() 
        {
            return View();
        }

    }
}