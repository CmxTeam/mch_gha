﻿using Kendo.Mvc.UI;
using MCH.BLL.Units;
using MCH.Web.Models.Awbs;
using MCH.Web.Models.CargoAcceptance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class AwbController : BaseController
    {

        public JsonResult GetUNClasses(UNClassSimpleFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpUnClasses = unitOfWork.Context.UNClasses.AsQueryable();
                if (filter != null) { 

                if (!String.IsNullOrWhiteSpace(filter.Term))
                {
                    tmpUnClasses = tmpUnClasses.Where(item => item.Description.IndexOf(filter.Term) > 0);
                }
                if (filter.Id.HasValue && filter.Id.Value != 0) 
                {
                    tmpUnClasses = tmpUnClasses.Where(item => item.Id == filter.Id);
                }
                }

                var tmpJsonData = tmpUnClasses.Select(item => new { id = item.Id, text = item.Description });

                return Json(tmpJsonData.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AircraftTypes(AirCraftSimpleFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpAirType = unitOfWork.Context.AircraftTypes.AsQueryable();
                if (filter != null)
                {
                    if (!String.IsNullOrWhiteSpace(filter.Term))
                    {
                        tmpAirType = tmpAirType.Where(item => item.AircraftTypeCode.IndexOf(filter.Term) > 0);
                    }

                    if (filter.Id.HasValue && filter.Id.Value != 0)
                    {
                        tmpAirType = tmpAirType.Where(item => item.Id == filter.Id);
                    }
                }

                var tmpJsonData = tmpAirType.Select(item => new { id = item.Id, text = item.AircraftTypeCode });

                return Json(tmpJsonData.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAwbsBy(AwbsListSimpleFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpAwbs = unitOfWork.Context.AWBs.AsQueryable();

                if (filter != null)
                {
                    if (filter.FlightId.HasValue && filter.FlightId.Value != 0)
                    {
                        tmpAwbs = unitOfWork.Context.ManifestAWBDetails.Where(item => item.FlightManifestId == filter.FlightId).Select(item => item.AWB);
                    }

                    if (filter.CarrierId.HasValue && filter.CarrierId.Value != 0)
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.CarrierId == filter.CarrierId.Value);
                    }
                    if (filter.DestinationId.HasValue && filter.DestinationId.Value == filter.DestinationId.Value)
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.DestinationId == filter.DestinationId.Value);
                    }
                    if (filter.OriginId.HasValue && filter.OriginId.Value != 0)
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.OriginId == filter.OriginId.Value);
                    }

                    if (!String.IsNullOrWhiteSpace(filter.Term))
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.AWBSerialNumber.IndexOf(filter.Term) > 0);
                    }
                    if (filter.Id.HasValue && filter.Id.Value != 0) 
                    {
                        tmpAwbs = tmpAwbs.Where(item => item.Id == filter.Id);
                    }

                }

                var tmpJsonData = tmpAwbs.Select(item => new { id = item.Id, text = item.AWBSerialNumber });

                return Json(tmpJsonData.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AWBLookup(string number, string code)
        {
            using (CommonUnit unit = new CommonUnit())
            {
               var tmpAWBDetials = unit.GetAwbDetails(number, code);
               AcaAwbDetialsModel tmpLookupResutl = null;

               if (tmpAWBDetials != null)
               {
                   tmpLookupResutl = new AcaAwbDetialsModel
                   {
                      ConnectedToShipper = tmpAWBDetials.ConnectedToShipper,
                      DestinationId = tmpAWBDetials.DestinationId,
                      FlightNumber = tmpAWBDetials.FlightNumber,
                      FlightDate = tmpAWBDetials.FlightDate,
                      Id = tmpAWBDetials.Id.HasValue ? tmpAWBDetials.Id.Value : 0,
                      OriginId = tmpAWBDetials.OriginId,
                      Pieces = tmpAWBDetials.TotalPieces,
                      Weight = tmpAWBDetials.TotalWeight,
                      WeightUom = tmpAWBDetials.WeightUOM,
                      WeightUomId = tmpAWBDetials.WeightUOMId
                   };
               }

               return Json(tmpLookupResutl, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult GetFlightAWBsView(long flightId, long? taskId)
        {
            FlightAWBListViewModel tmpModel = new FlightAWBListViewModel();
            tmpModel.FlightId = flightId;
            tmpModel.TaskId = taskId;

            return PartialView("_FlightAwbListView", tmpModel);
        }

        public JsonResult FlightAWBListDS([DataSourceRequest] DataSourceRequest request, long flightId)
        {
            DataSourceResult tmpResult = null;

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }

    }
}
