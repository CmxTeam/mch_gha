﻿using MvcReportViewer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class ReportsController : Controller
    {
        public PartialViewResult GetReportByName(ReportModel model)
        {
            if (model.ReportParams != null)
            {
                model.AdjustDictionary();
            }

            return PartialView("CustomReport", model);
        }
    }
}