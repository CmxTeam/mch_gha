﻿using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

using MCH.BLL.DAL;
using MCH.BLL.Units;
using MCH.Web.Models.Uld;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.BLL.DAL.Data;

namespace MCH.Web.Controllers
{
    public class UldController : BaseController
    {
        public JsonResult GetTaskUlds(TaskUldFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                IList<TaskULDRow> tmpTasks = new List<TaskULDRow>();

                var tmpTask = unitOfWork.TaskRepository.GetByID(filter.TaskId);
                var tmpFlight = unitOfWork.FlightRepository.GetByID(tmpTask.EntityId);

                if (tmpTask == null || tmpFlight == null)
                {
                    //TODO handle this case
                }

                var data = tmpFlight.ULDs.AsQueryable();

                if (!String.IsNullOrWhiteSpace(filter.sSearch))
                {
                    data = data.Where(item => item.SerialNumber.IndexOf(filter.sSearch, StringComparison.InvariantCultureIgnoreCase) > -1);
                }

                var total = data.Count();

                data = data.Skip(filter.iDisplayStart * filter.iDisplayLength).Take(filter.iDisplayLength);


                var tmpData = data.Select(item => new TaskULDRow
                {
                    Prefix = item.ShipmentUnitType != null ? item.ShipmentUnitType.Code : String.Empty,
                    Type = item.ULDType != null ? item.ULDType.Code : String.Empty,
                    Reference = item.SerialNumber,
                    TotalWeight = item.TotalWeight,
                    TotalVolume = item.TotalVolume,
                    TareWeight = item.TareWeight,
                    FreightWeight = item.FreightWeight,
                    NumberAwbs = item.FlightManifest.ManifestAWBDetails.Count,
                    Id = item.Id,
                    Pieces = item.TotalUnitCount,
                    STC = item.TotalPieces,
                    IsBUP = item.IsBUP,
                    StatusId = item.StatusID,
                    Location = item.WarehouseLocation != null ? item.WarehouseLocation.Location : String.Empty
                }).ToList();

                if (tmpData.Any())
                {

                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult MarkULDBUP(TaskULDAction model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                if (!String.IsNullOrWhiteSpace(model.ULDStringIds))
                {
                    var tmpUldIds = model.ULDStringIds.Split(',').Select(item => Int64.Parse(item));

                    var uldsToUpdate = unitOfWork.Context.ULDs.Where(item => tmpUldIds.Contains(item.Id));

                    if (uldsToUpdate.Any())
                    {
                        foreach (var uldItem in uldsToUpdate)
                        {
                            uldItem.IsBUP = model.IsBUP;
                        }

                        unitOfWork.Context.SaveChanges();
                    }
                }
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAddUldData(long taskId)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(taskId);

                var tmpULDTypes = unitOfWork.Context.ULDTypes.Select(item => new { Id = item.Id, Text = item.Code }).ToList();

                var tmpUnitCodes = unitOfWork.Context.ShipmentUnitTypes.Select(item => new { Id = item.Id, Text = item.Code }).ToList();
                tmpUnitCodes.Insert(0, new { Id = -1, Text = " Not Selected" });

                return Json(new
                {
                    UldTypes = tmpULDTypes,
                    UnitTypes = tmpUnitCodes
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteULDs(DeleteUldsModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var jsonResult = new { Status = true, Message = "" };
                try
                {
                    var tmpUldIds = model.ULDIdsString.Split(',');
                    if (tmpUldIds.Any())
                    {
                        foreach (var ta in tmpUldIds)
                        {
                            long tmpUldId = Int64.Parse(ta);
                            var tmpUldToDelete = unitOfWork.Context.ULDs.SingleOrDefault(item => item.Id == tmpUldId);

                            // delete uld if STC equals to 0
                            if (tmpUldToDelete != null && (tmpUldToDelete.TotalPieces == null || tmpUldToDelete.TotalPieces == 0))
                            {
                                unitOfWork.Context.ULDs.Remove(tmpUldToDelete);
                            }
                        }
                        unitOfWork.Context.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    jsonResult = new { Status = false, Message = "Could not delete ULD." };
                }

                return Json(jsonResult, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddCargoLoaderFlightLegULD(AddTaskULDModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var DataContext = unitOfWork.Context;

                var uldType = DataContext.ULDTypes.Single(t => t.Id == model.UldTypeId);
                var task = DataContext.Tasks.Single(t => t.Id == model.TaskId);
                var flight = DataContext.FlightManifests.SingleOrDefault(f => f.Id == task.EntityId && task.EntityTypeId == 1);
                var tmpUnloadingPort = flight.UnloadingPorts.FirstOrDefault();
                var tmpPrefix = DataContext.ShipmentUnitTypes.SingleOrDefault(item => item.Id == model.PrefixId);

                if (flight == null)
                {
                    return Json(new { Status = false, Error = "Flight not found for selected Task." }, JsonRequestBehavior.AllowGet);
                }
                if (tmpUnloadingPort == null)
                {
                    return Json(new { Status = false, Error = "Flight doesn't have unloading ports specified." }, JsonRequestBehavior.AllowGet);
                }

                var uld = new ULD
                {
                    ULDType = uldType,
                    IsBUP = true,
                    FlightManifest = flight,
                    UnloadingPort = tmpUnloadingPort
                };

                if (uldType.Code.ToUpper() == "LOOSE")
                {
                    uld.UnitTypeId = DataContext.ShipmentUnitTypes.Single(t => t.Code.ToUpper() == "LOOSE").Id;
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(model.Serial) && flight.ULDs.SingleOrDefault(u => u.SerialNumber == model.Serial) != null)
                    {
                        return Json(new { Status = false, Error = "Uld already exists for flight." }, JsonRequestBehavior.AllowGet);
                    }
                    uld.SerialNumber = model.Serial;
                    uld.ShipmentUnitType = tmpPrefix;
                    flight.TotalULDs += 1;
               } 

                var transaction = new Transaction
                {
                    EntityTypeId = 4,
                    EntityId = uld.Id,
                    StatusId = 20,
                    StatusTimestamp = DateTime.UtcNow,                   
                    Reference = "ULD# " + uld.ULDType.Code + "" + (uld.ShipmentUnitType != null ? uld.ShipmentUnitType.Code : String.Empty) + uld.SerialNumber,
                    Description = "ULD" + uld.ULDType.Code + "-" + (string.IsNullOrEmpty(uld.SerialNumber) ? string.Empty : (uld.ShipmentUnitType != null ? uld.ShipmentUnitType.Code : String.Empty) + uld.SerialNumber) + "Added",
                    TaskTypeId = 5,
                    TransactionActionId = 1,
                    TaskId = model.TaskId,
                    UserId = SessionManager.UserInfo.UserLocalId
                };

                DataContext.ULDs.Add(uld);
                DataContext.Transactions.Add(transaction);
                DataContext.SaveChanges();

                return Json(new { Status = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetULDSimpleList(UldSimpleFilter filter) 
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpUlds = unitOfWork.Context.ULDs.AsQueryable();

                if (filter != null) 
                {
                    if (filter.AwbId.HasValue) 
                    {
                        tmpUlds = unitOfWork.Context.ManifestAWBDetails.Where(item => item.AWBId == filter.AwbId.Value).Select(item=> item.ULD);
                    }

                    if (!String.IsNullOrWhiteSpace(filter.Term)) 
                    {
                        tmpUlds = tmpUlds.Where(item => item.SerialNumber.IndexOf(filter.Term) > 0);
                    }
                }

                var tmpJsonData = tmpUlds.Select(item => new { id = item.Id, text = item.SerialNumber });

                return Json(tmpJsonData.ToList(), JsonRequestBehavior.AllowGet);
            }
        }


        public PartialViewResult GetFlightULDsView(long flightId, long? taskId)
        {
            FlightULDsViewModel tmpModel = new FlightULDsViewModel();
            tmpModel.EntityId = flightId;
            tmpModel.TaskId = taskId;

            return PartialView("_FlightUldsView", tmpModel);
        }

        public JsonResult FlightULDsDS([DataSourceRequest] DataSourceRequest request, long flightId)
        {
            DataSourceResult tmpResult = null;

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetAwbULDsView(long AwbId, long? taskId)
        {
            FlightULDsViewModel tmpModel = new FlightULDsViewModel();
            tmpModel.EntityId = AwbId;
            tmpModel.TaskId = taskId;

            return PartialView("_AwbUldsView", tmpModel);
        }


        public JsonResult AwbULDsDS([DataSourceRequest] DataSourceRequest request, long awbId)
        {
            DataSourceResult tmpResult = null;

            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                var tmpUlds = unit.Context.MCH_GetAwbUlds(awbId, (int)SessionManager.UserInfo.DefaultWarehouseId);
                tmpResult = tmpUlds.Select((e, i) => new AwbUldsRow
                {
                   BUP = e.BUP,
                   Location = e.Location,
                   STC = e.STC,
                   Id = e.UldId,
                   Number = e.ULDNo,
                   Owner = e.UldOwner,
                   Type = e.UldType
                }).ToList().ToDataSourceResult(request);
            }

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }

        
    }
}
