﻿using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using MCH.Web.Models.Awbs;
using MCH.Web.Models.Common;
using MCH.Web.Models.EntityType;
using MCH.Web.Models.Flight;
using MCH.Web.Models.Hwbs;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class EntityViewersController : Controller
    {
        public PartialViewResult GetEntityViewerByType(long? taskId, enumEntityTypes entityTypeId, long entityId)
        {
            switch (entityTypeId)
            {
                case enumEntityTypes.FlightManifest:
                    {
                        return GetFlightViewer(entityId, taskId);
                    }
                case enumEntityTypes.AWB:
                    {
                        return GetAWBViewer(entityId, taskId);
                    }
                case enumEntityTypes.HWB:
                    {
                        return GetHWBViewer(entityId, taskId);
                    }
                case enumEntityTypes.ULD:
                    {
                        return PartialView("_ErrorView", "ULD Viewer is not implemented.");
                    }
                case enumEntityTypes.Task:
                    {
                        return PartialView("_ErrorView", "Task Viewer is not implemented.");
                    }
                default:
                    {
                        return PartialView("_ErrorView", "Unknown Entity Type");
                    }
            }
        }

        public PartialViewResult GetFlightViewer(long entityId, long? taskId)
        {
            FlightViewerModel tmpModel = new FlightViewerModel();

            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                var tmpFlight = unit.Context.MCH_GetFlightViewerModel(entityId).SingleOrDefault();
                if (tmpFlight != null)
                {
                    tmpModel.AircraftRegNumber = tmpFlight.AircraftRegNumber;
                    tmpModel.CarrierId = tmpFlight.CarrierId;
                    tmpModel.CarrierName = tmpFlight.CarrierName;
                    tmpModel.ETA = tmpFlight.ETA;
                    tmpModel.ETD = tmpFlight.ETD;
                    tmpModel.FlightId = tmpFlight.FlightId;
                    tmpModel.FlightNumber = tmpFlight.FlightNumber;
                    tmpModel.LoadingPortId = tmpFlight.LoadingPortId;
                    tmpModel.LoadingPortName = tmpFlight.LoadingPortName;
                    tmpModel.TotalAWBs = tmpFlight.TotalAwbs;
                    tmpModel.TotalPieces = tmpFlight.TotalPieces;
                    tmpModel.TotalULDs = tmpFlight.TotalULDs;
                    tmpModel.TotalWeight = tmpFlight.TotalWeight;
                    tmpModel.UloadingPortName = tmpFlight.UnloadingPortName;
                    tmpModel.UnloadingPort = tmpFlight.UnloadingPortId;
                    tmpModel.WeightUOM = tmpFlight.WeightUOM;
                    tmpModel.TaskId = taskId;
                }

                if (taskId.HasValue)
                {
                    tmpModel.TaskDetails = new Models.Common.TaskDetialsModel();
                    var tmpTask = unit.Context.MCH_GetTaskDetailsModel(taskId).SingleOrDefault();
                    if (tmpTask != null)
                    {
                        tmpModel.TaskDetails.EndedAt = tmpTask.EndedAt;
                        tmpModel.TaskDetails.LastUpdated = tmpTask.LastUpdated;
                        tmpModel.TaskDetails.Location = tmpTask.Location;
                        tmpModel.TaskDetails.Progress = tmpTask.Progress;
                        tmpModel.TaskDetails.StartedAt = tmpTask.StartedAt;
                        tmpModel.TaskDetails.Status = tmpTask.Status;
                        tmpModel.TaskDetails.TypeId = tmpTask.TypeId;
                        tmpModel.TaskDetails.TypeName = tmpTask.TypeName;

                    }
                    tmpModel.TaskDetails.AssignedTo = unit.Context.MCH_GetTaskAssignees(taskId).Select(item => new TaskAssigneModel
                    {
                        UserId = item.UserId,
                        Username = item.UserName
                    }).ToList();
                }
            }

            return PartialView("_FlightViewer", tmpModel);
        }

        public PartialViewResult GetAWBViewer(long entityId, long? taskId)
        {
            AwbViewerModel tmpModel = new AwbViewerModel();
            tmpModel.TaskDetails = new TaskDetialsModel();

            using (ImportExportUnit unit = new ImportExportUnit())
            {
                var tmpAwb = unit.DataContext.MCH_GetAwbViewerModel(entityId, (int) SessionManager.UserInfo.DefaultWarehouseId).SingleOrDefault();
                
                if (tmpAwb != null)
                {
                    tmpModel.AwbId = entityId;
                    tmpModel.AwbNumber = tmpAwb.AWB;
                    tmpModel.Consignee = tmpAwb.Consignee;
                    tmpModel.CurrentLocation = tmpAwb.Locations;
                    tmpModel.Description = tmpAwb.GoodsDescription;
                    tmpModel.Destination = tmpAwb.Destination;
                    tmpModel.ExportControlNumber = unit.GetAWBExportControl(entityId);
                    tmpModel.ImportControlNumber = unit.GetAWBImportControl(entityId, "");
                    tmpModel.Origin = tmpAwb.Origin;
                    tmpModel.Shipper = tmpAwb.Shipper;
                    tmpModel.TotalHwbs = tmpAwb.TotalHWBs;
                    tmpModel.TotalPieces = tmpAwb.TotalPieces;
                    tmpModel.TotalSlac = tmpAwb.TotalSlac;
                    tmpModel.TotalUlds = tmpAwb.TotalULDs;
                    tmpModel.TotalWeight = tmpAwb.TotalWeight;
                }
                if (taskId.HasValue)
                {
                    tmpModel.TaskDetails = new Models.Common.TaskDetialsModel();
                    var tmpTask = unit.DataContext.MCH_GetTaskDetailsModel(taskId).SingleOrDefault();
                    if (tmpTask != null)
                    {
                        tmpModel.TaskDetails.EndedAt = tmpTask.EndedAt;
                        tmpModel.TaskDetails.LastUpdated = tmpTask.LastUpdated;
                        tmpModel.TaskDetails.Location = tmpTask.Location;
                        tmpModel.TaskDetails.Progress = tmpTask.Progress;
                        tmpModel.TaskDetails.StartedAt = tmpTask.StartedAt;
                        tmpModel.TaskDetails.Status = tmpTask.Status;
                        tmpModel.TaskDetails.TypeId = tmpTask.TypeId;
                        tmpModel.TaskDetails.TypeName = tmpTask.TypeName;

                    }
                    tmpModel.TaskDetails.AssignedTo = unit.DataContext.MCH_GetTaskAssignees(taskId).Select(item => new TaskAssigneModel
                    {
                        UserId = item.UserId,
                        Username = item.UserName
                    }).ToList();
                }
            }

            return PartialView("_AWBViewer", tmpModel);
        }

        public PartialViewResult GetHWBViewer(long entityId, long? taskId)
        {
            HwbViewerModel tmpModel = new HwbViewerModel();
            tmpModel.TaskDetails = new TaskDetialsModel();

            return PartialView("_HWBViewer", tmpModel);
        }

        #region Media list for entities
        public PartialViewResult GetEntityMediaView(long entityId, enumEntityTypes entityTypeId, long? taskId)
        {
            EntityViewerModel tmpModel = new EntityViewerModel
            {
                EntityId = entityId,
                EntityTypeId = entityTypeId,
                TaskId = taskId
            };
            return PartialView("_MediaViewer", tmpModel);
        }

        public JsonResult GetEntityMediaDS([DataSourceRequest] DataSourceRequest request, long entityId, enumEntityTypes entityTypeId, long? taskId)
        {
            IList<EntityMediaRow> tmpList = null;

            //TODO : Aram/Harut bind to query based on the entityTypeId and take the Data source based on the request.

            return Json(tmpList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region History for entities

        public PartialViewResult GetEntityHistoryView(long entityId, enumEntityTypes entityTypeId, long? taskId)
        {
            EntityViewerModel tmpModel = new EntityViewerModel
            {
                EntityId = entityId,
                EntityTypeId = entityTypeId,
                TaskId = taskId
            };
            return PartialView("_HistoryViewer", tmpModel);
        }

        public JsonResult GetEntityHistoryDS([DataSourceRequest] DataSourceRequest request, long entityId, enumEntityTypes entityTypeId, long? taskId)
        {
            DataSourceResult tmpList = null;

            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                var tmpHistoryQuery = unit.Context.Transactions.Where(item => item.EntityId == entityId && item.EntityTypeId == (int)entityTypeId).Select(item=> new EntityHistoryRow{Description = item.Description, Id = item.Id, Reference = item.Reference, Timestamp = item.StatusTimestamp, User = item.UserProfile.UserId});

                tmpList = tmpHistoryQuery.ToDataSourceResult(request);
            }

            return Json(tmpList, JsonRequestBehavior.AllowGet);
        }
        #endregion


    }
}