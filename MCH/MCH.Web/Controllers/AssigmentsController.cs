﻿using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using MCH.Web.Models.Assignment;
using MCH.Web.Models.Task;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class AssigmentsController : BaseController
    {
        //
        // GET: /Assigments/

        public PartialViewResult ViewTaskAssignments(long TaskId)
        {
            return PartialView("_TaskAssignments", new TaskDetailsHistory { TaskId = TaskId });
        }


        public JsonResult GetNotAssignedUsers(TaskBaseModel tmpModel)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(tmpModel.TaskId);

                var tmpTaskAssignmentIds = tmpTask.TaskAssignments.Select(e => e.UserId).ToList();
                // get not assigned users
                var tmpEmployees = unitOfWork.Context.UserProfiles.Where(item => !tmpTaskAssignmentIds.Contains(item.Id));

               

                return Json(tmpEmployees.Select(item => new { Id = item.Id, Text = item.FirstName + " " + item.LastName }).ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTaskAssignmentOptions(TaskBaseModel tmpModel)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(tmpModel.TaskId);

                var tmpTaskAssignmentIds = tmpTask.TaskAssignments.Select(e => e.UserId).ToList();
                // get not assigned users
                var tmpEmployees = unitOfWork.Context.UserProfiles.Where(item => !tmpTaskAssignmentIds.Contains(item.Id)).ToList();
                var tmpOwnersOptions = tmpEmployees.ToList();

                var tmpOwner = tmpTask.TaskAssignments.Where(item => item.IsOwner).FirstOrDefault();
                if (tmpOwner != null)
                {
                    var realOwner = unitOfWork.Context.UserProfiles.SingleOrDefault(item => item.Id == tmpOwner.UserId);
                    tmpOwnersOptions.Insert(0, realOwner);
                }

                var tmpJsonData = new
                {
                    UserOptions = tmpEmployees.Select(item => new { Id = item.Id, Text = item.FirstName + " " + item.LastName }).ToList(),
                    OwnerOptions = tmpOwnersOptions.Select(item => new { Id = item.Id, Text = item.FirstName + " " + item.LastName }).ToList()
                };

                return Json(tmpJsonData, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult AssignTaskToUser(TaskUserAssignments model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpUsers = model.UserIdString.Split(',');

                var tmpTask = unitOfWork.TaskRepository.GetByID(model.TaskId);

                if (tmpUsers.Any())
                {
                    foreach (var item in tmpUsers)
                    {
                        unitOfWork.Context.TaskAssignments.Add(new MCH.BLL.DAL.Data.TaskAssignment
                        {
                            TaskId = model.TaskId,
                            UserId = Int64.Parse(item),
                            RecDate = DateTime.UtcNow,
                            AssignedBy = SessionManager.UserInfo.UserLocalId,
                            AssignedOn = DateTime.UtcNow,
                            IsOwner = (Int64.Parse(item) == model.OwnerId)
                        });
                    }

                    //TODO: Harut => fix this
                    //if (tmpTask.StatusId == (int)enumTaskStatuses.NotAssigned) 
                    //{
                    //    tmpTask.StatusId = (int)enumTaskStatuses.NotStarted;
                    //}
                }

                unitOfWork.Context.SaveChanges();

                return Json(new { }, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult DeleteAssignments(TaskAssignmentsModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var tmpTask = unitOfWork.TaskRepository.GetByID(model.TaskId);

                var tmpAssignmentIds = model.AssignmentsIdsString.Split(',');
                if (tmpAssignmentIds.Any())
                {
                    foreach (var ta in tmpAssignmentIds)
                    {
                        long tmpAssignmentId = Int64.Parse(ta);
                        var tmpAssignmentToDelete = tmpTask.TaskAssignments.SingleOrDefault(item => item.Id == tmpAssignmentId);
                        if (tmpAssignmentToDelete != null)
                        {
                            unitOfWork.Context.TaskAssignments.Remove(tmpAssignmentToDelete);
                        }
                    }

                    unitOfWork.Context.SaveChanges();
                }
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

    }
}
