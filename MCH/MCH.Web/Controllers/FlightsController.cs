﻿using Kendo.Mvc.UI;
using MCH.BLL.Units;
using MCH.Web.Models.Flight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using MCH.BLL.Model.Enum;

namespace MCH.Web.Controllers
{
    public class FlightsController : Controller
    {
        // GET: Flights
        public PartialViewResult GetFlightLegs(long entityId, long? taskId, enumEntityTypes entityTypeId)
        {
            switch (entityTypeId)
            {
                case enumEntityTypes.FlightManifest:
                    {
                        return PartialView("_FlightLegsGrid", entityId);
                    }
                case enumEntityTypes.AWB:
                    {
                        return PartialView("_AwbFlightLegsGrid", entityId);
                    }
                case enumEntityTypes.HWB:
                case enumEntityTypes.ULD:
                case enumEntityTypes.Task:
                    { 
                        return PartialView("_FlightLegsGrid", entityId);
                    }
                default:
                    {
                        return PartialView("_FlightLegsGrid", entityId);
                    }
            }

        }

        public JsonResult AwbFlightLegsDS([DataSourceRequest] DataSourceRequest request, long awbId)
        {
            DataSourceResult tmpResult = null;
            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                var tmpItems = unit.Context.MCH_AwbGetFlightLegs(awbId);

                tmpResult = tmpItems.Select((e, i) => new FlightLegRow
                {
                    Carrier = e.CarrierCode,
                    Destination = e.Destination,
                    ETD = e.ETD,
                    FlightNumber = e.FlightNumber,
                    Origin = e.Origin,
                    Seq = e.Sequence
                }).ToList().ToDataSourceResult(request);
            }

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FligthLegsDS([DataSourceRequest] DataSourceRequest request, long flightId)
        {
            DataSourceResult tmpResult = null;
            using (TasksUnitOfWork unit = new TasksUnitOfWork())
            {
                var tmpItems = unit.Context.MCH_GetFlightLegs(flightId);
                var tmpPorts = unit.Context.Ports;

                tmpResult = tmpItems.Select((e, i) => new FlightLegRow
                {
                    Carrier = e.CarrierCode,
                    Destination = tmpPorts.SingleOrDefault(item => item.Id == e.DestinationId).IATACode,
                    ETA = e.ETA,
                    ETD = e.ETD,
                    FlightNumber = e.FlightNumber,
                    Origin = tmpPorts.SingleOrDefault(item => item.Id == e.OriginId).IATACode,
                    Seq = i + 1
                }).ToList().ToDataSourceResult(request);
            }

            return Json(tmpResult, JsonRequestBehavior.AllowGet);
        }
    }
}