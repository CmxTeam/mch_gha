﻿using MCH.BLL.DAL;
using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using MCH.Web.Models.Accept;
using MCH.Web.Models.Awbs;
using MCH.Web.Models.Task;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class AcceptController : BaseController
    {

        public JsonResult CompleteAcceptForm(SelectTaskFilter model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                unitOfWork.Context.CompleteAccept(model.TaskId, SessionManager.UserInfo.UserLocalId);
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveCustomer(AcceptCustomer model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                long? retTaskId = null;

                #region Customer Validation
                if (String.IsNullOrWhiteSpace(model.CustomerName))
                {
                    return Json(new { TaskId = retTaskId, Result = false, ErrorMessage = "Customer Name is required." }, JsonRequestBehavior.AllowGet);
                }
                if (String.IsNullOrWhiteSpace(model.IATACode))
                {
                    return Json(new { TaskId = retTaskId, Result = false, ErrorMessage = "IATA Code is Required." }, JsonRequestBehavior.AllowGet);
                }
                #endregion

                if (model.TaskId.HasValue && model.TaskId != 0)
                {
                    var tmpTask = unitOfWork.Context.Tasks.Single(item => item.Id == model.TaskId);
                    tmpTask.TaskReference = model.CustomerName;
                    unitOfWork.Context.SaveChanges();
                    retTaskId = model.TaskId.Value;
                }
                else
                {
                    var tmpWarehouse = unitOfWork.Context.UserWarehouses.FirstOrDefault(item => item.UserId == SessionManager.UserInfo.UserLocalId && item.IsDefault);

                    var tmpResult = unitOfWork.Context.CreateAcceptTask(SessionManager.UserInfo.UserLocalId, model.CustomerName, model.IATACode).First();

                    retTaskId = tmpResult.Value;
                }

                return Json(new { TaskId = retTaskId, Result = true, ErrorMessage = String.Empty }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveDriver(AcceptDriver model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                ObjectParameter tmpDriverId = new ObjectParameter("new_id", typeof(long));
                var tmpFullName = model.Name.Split(' ');
                string tmpFirstName = tmpFullName[0];
                string tmpLastName = tmpFullName.Length > 1 ? tmpFullName[1] : String.Empty;
                string tmpThumbImage = String.Empty;
                if (!String.IsNullOrWhiteSpace(model.PhotoString))
                {
                    Image tmpDriverPhoto = ImageUtils.Base64ToImage(model.PhotoString.Split(',')[1]);
                    Image tmpDriverThumb = tmpDriverPhoto.GetThumbnailImage(100, 100, () => false, IntPtr.Zero);
                    tmpThumbImage = ImageUtils.ImageToBase64(tmpDriverThumb, ImageFormat.Png);
                    tmpThumbImage = tmpThumbImage.Insert(0, "data:image/png;base64,");
                }

                unitOfWork.Context.AddEditDriver(model.DriverId, tmpFirstName, tmpLastName, model.TruckingCoId, model.PhotoString, tmpThumbImage, model.PrimaryId, model.IDPhotoString, model.IDNumber, tmpDriverId);

                unitOfWork.Context.AddEditDriverSecurityLog(model.TaskId, SessionManager.UserInfo.UserLocalId, (long)tmpDriverId.Value, model.TaskId, (int)enumEntityTypes.Task, model.PrimaryId, model.IsPhotoMatched, null, null);

                return Json(new { DriverId = (long)tmpDriverId.Value, Result = true, ErrorMessage = String.Empty }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetAwbScreeningLogs(AcceptScreeningDataFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var data = unitOfWork.Context.GetCargoAcceptAWBScreeningLog(filter.iDisplayStart, filter.iDisplayLength, !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null, filter.sSearch, filter.AwbId, filter.TaskId);

                var tmpData = data.ToList();

                if (tmpData.Any())
                {
                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        draw = filter.iDisplayStart + 1,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetAwbTaskHistory(AcceptScreeningDataFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var data = unitOfWork.Context.GetReceiverTaskHistoryItems(filter.iDisplayStart, filter.iDisplayLength, !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null, filter.sSearch, filter.TaskId);

                var tmpData = data.ToList();

                if (tmpData.Any())
                {
                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        draw = filter.iDisplayStart + 1,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetAwbAttachments(AcceptScreeningDataFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var data = unitOfWork.Context.GetCargoAcceptAWBAttachments(filter.iDisplayStart, filter.iDisplayLength, !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null, filter.sSearch, filter.AwbId, filter.TaskId);

                var tmpData = data.ToList();

                if (tmpData.Any())
                {
                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        draw = filter.iDisplayStart + 1,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetAwbAttributes(AcceptScreeningDataFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var data = unitOfWork.Context.GetCargoAcceptAWBAttributes(filter.iDisplayStart, filter.iDisplayLength, !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null, filter.sSearch, filter.AwbId, filter.TaskId);

                var tmpData = data.ToList();

                if (tmpData.Any())
                {
                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        draw = filter.iDisplayStart + 1,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetAwbconsignmentDetails(AcceptScreeningDataFilter filter)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                var data = unitOfWork.Context.GetCargoAcceptAWBConsignmentDetails(filter.iDisplayStart, filter.iDisplayLength, !string.IsNullOrWhiteSpace(filter.iSortCol_0) ? filter.iSortCol_0 + " " + filter.sSortDir_0 : null, filter.sSearch, filter.AwbId, filter.TaskId);

                var tmpData = data.ToList();

                if (tmpData.Any())
                {
                    var tmpTaskDataSourece = new { };
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        draw = filter.iDisplayStart + 1,
                        iTotalRecords = tmpData.Count,
                        iTotalDisplayRecords = tmpData[0].Total,
                        aaData = tmpData
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = 0,
                        iTotalDisplayRecords = filter.iDisplayLength,
                        aaData = new { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult SaveAwbAttachment(AddAwbAttachmentsModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                unitOfWork.Context.AddCargoAcceptAWBAttachments(model.AwbId, model.ParentTaskId, SessionManager.UserInfo.UserLocalId, model.AttachmentImage, model.FileName, model.FileName, model.Description);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveAwbAttribute(AcceptAwbAttributeModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                unitOfWork.Context.Attributes.SingleOrDefault(item => item.Id == model.AttributeId);

                string tmpAttrValue = String.Empty;
                tmpAttrValue = GetAttributeValueByType(model, unitOfWork);

                unitOfWork.Context.AddCargoAcceptAWBAttributes(model.ParentTaskId, SessionManager.UserInfo.UserLocalId, model.AwbId, model.AttributeId, tmpAttrValue);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteAwbAttachments(DeleteAwbAttachmentsModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                if (!String.IsNullOrWhiteSpace(model.AwbAttachmentIds))
                {
                    var tmpIds = model.AwbAttachmentIds.Split(',');
                    foreach (var item in tmpIds)
                    {
                        long tmpAttachId = 0;
                        if (long.TryParse(item, out tmpAttachId))
                        {
                            unitOfWork.Context.RemoveCargoAcceptAWBAttachments(tmpAttachId, SessionManager.UserInfo.UserLocalId);
                        }
                    }
                }
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteAwbAttributes(DeleteAwbAttributeModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {

                if (!String.IsNullOrWhiteSpace(model.AwbAttributeIds))
                {
                    var tmpIds = model.AwbAttributeIds.Split(',');
                    foreach (var attrId in tmpIds)
                    {
                        long tmpAttrIdValue = 0;
                        if (long.TryParse(attrId, out tmpAttrIdValue))
                        {
                            unitOfWork.Context.RemoveCargoAcceptAWBAttributes(model.TaskId, SessionManager.UserInfo.UserLocalId, model.AwbId, tmpAttrIdValue);
                        }
                    }
                }
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult GetAddEditScreeningView(SelectAwbFilter model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                AcceptAwbScreeningModel tmpModel = new AcceptAwbScreeningModel();
                tmpModel.AwbId = model.AwbId;
                tmpModel.TaskId = model.TaskId;
                tmpModel.ParentTaskId = model.ParentTaskId;
                tmpModel.FormId = model.FormId;

                return PartialView("_AddEditAcceptScreening", tmpModel);
            }
        }

        public PartialViewResult GetAddEditAttributeView(SelectAwbFilter model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                AcceptAwbAttributeModel tmpModel = new AcceptAwbAttributeModel();
                tmpModel.AwbId = model.AwbId;
                tmpModel.TaskId = model.TaskId;
                tmpModel.ParentTaskId = model.ParentTaskId;
                tmpModel.FormId = model.FormId;

                return PartialView("_AddEditAcceptAttribute", tmpModel);
            }
        }

        public JsonResult SaveAwbScreeningLog(AcceptAwbScreeningModel model)
        {
            using (TasksUnitOfWork unitOfWork = new TasksUnitOfWork())
            {
                if (model.Screened)
                {
                    unitOfWork.Context.AddCargoAcceptAWBScreeningLog(SessionManager.UserInfo.UserLocalId, model.AwbId, model.ParentTaskId, model.Screened, model.CSSFId, model.SealType, model.SealNumber, model.Remarks, model.ScreeingMethodId);
                }
                else
                {
                    unitOfWork.Context.AddCargoAcceptAWBScreeningLog(SessionManager.UserInfo.UserLocalId, model.AwbId, model.ParentTaskId, model.Screened, null, null, null, model.Remarks, null);
                }



                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Helper Methods
        private static string GetAttributeValueByType(AcceptAwbAttributeModel model, TasksUnitOfWork unitOfWork)
        {
            StringBuilder tmpAttr = new StringBuilder();
            switch ((enumAttributes)model.AttributeId)
            {
                case enumAttributes.HAZMAT:
                    {
                        if (!String.IsNullOrWhiteSpace(model.UNNumber))
                        {
                            tmpAttr.Append("UN NUMBER=");
                            tmpAttr.Append(model.UNNumber);
                            tmpAttr.Append(";");
                        }
                        if (model.UNClassId.HasValue && model.UNClassId.Value != 0)
                        {
                            var tmpUNClass = unitOfWork.Context.UNClasses.Single(item => item.Id == model.UNClassId);

                            tmpAttr.Append("UN CLASS=");
                            tmpAttr.Append(tmpUNClass.Description);
                            tmpAttr.Append(";");
                        }
                        if (!String.IsNullOrWhiteSpace(model.UNShippingGame))
                        {
                            tmpAttr.Append("UN SHIPPING NAME=");
                            tmpAttr.Append(model.UNShippingGame);
                            tmpAttr.Append(";");
                        }
                        break;
                    }
                case enumAttributes.PERISHABLES:
                    {
                        if (!String.IsNullOrWhiteSpace(model.TempFrom))
                        {
                            tmpAttr.Append("TEMPERATURE FROM=");
                            tmpAttr.Append(model.TempFrom);
                            tmpAttr.Append(model.TempDegree);
                            tmpAttr.Append(";");
                        }
                        if (!String.IsNullOrWhiteSpace(model.TempTo))
                        {
                            tmpAttr.Append("TEMPERATURE TO=");
                            tmpAttr.Append(model.TempTo);
                            tmpAttr.Append(model.TempDegree);
                            tmpAttr.Append(";");
                        }
                        break;
                    }
                case enumAttributes.DRYICE:
                    {
                        if (model.Quantity != 0)
                        {
                            tmpAttr.Append("QUANTITY=");
                            tmpAttr.Append(model.Quantity);
                            tmpAttr.Append(model.Unit);
                            tmpAttr.Append(";");
                        }
                        break;
                    }
                case enumAttributes.ENGINE:
                    {
                        if (!String.IsNullOrWhiteSpace(model.SerialNumber))
                        {
                            tmpAttr.Append("SERIAL NUMBER=");
                            tmpAttr.Append(model.SerialNumber);
                            tmpAttr.Append(";");
                        }
                        break;
                    }
                case enumAttributes.OVERSIZED:
                    {
                        break;
                    }
                case enumAttributes.EXPEDITED:
                    {
                        break;
                    }
                case enumAttributes.VEHICLE:
                    {
                        if (!String.IsNullOrWhiteSpace(model.Vin))
                        {
                            tmpAttr.Append("VIN=");
                            tmpAttr.Append(model.Vin);
                            tmpAttr.Append(";");
                        }

                        if (model.StateId.HasValue && model.StateId.Value != 0)
                        {
                            var tmpState = unitOfWork.Context.States.Single(item => item.Id == model.StateId);

                            tmpAttr.Append("STATE=");
                            tmpAttr.Append(tmpState.TwoCharacterCode);
                            tmpAttr.Append(";");
                        }
                        break;
                    }
                case enumAttributes.HIGHVALUE:
                    {
                        break;
                    }
                case enumAttributes.LIVEANIMALS:
                    {
                        break;
                    }
                case enumAttributes.HUMANREMAINS:
                    {
                        break;
                    }
                case enumAttributes.LICENSE:
                    {
                        if (!String.IsNullOrWhiteSpace(model.NoEEI))
                        {
                            tmpAttr.Append("NO EEI=");
                            tmpAttr.Append(model.NoEEI);
                            tmpAttr.Append(";");
                        }
                        if (!String.IsNullOrWhiteSpace(model.AES))
                        {
                            tmpAttr.Append("AES=");
                            tmpAttr.Append(model.AES);
                            tmpAttr.Append(";");
                        }
                        if (!String.IsNullOrWhiteSpace(model.ITN))
                        {
                            tmpAttr.Append("ITN=");
                            tmpAttr.Append(model.ITN);
                            tmpAttr.Append(";");
                        }
                        if (!String.IsNullOrWhiteSpace(model.AESPost))
                        {
                            tmpAttr.Append("AES POST=");
                            tmpAttr.Append(model.AESPost);
                            tmpAttr.Append(";");
                        }
                        break;
                    }
                case enumAttributes.OTHERHANDLING:
                    {
                        if (model.HandlingCodeId.HasValue && model.HandlingCodeId.Value != 0)
                        {
                            var tmpHandlingCode = unitOfWork.Context.SpecialHandlingCodes.Single(item => item.Id == model.HandlingCodeId.Value);

                            tmpAttr.Append("CODE=");
                            tmpAttr.Append(tmpHandlingCode.Code);
                            tmpAttr.Append(";");
                        }
                        break;
                    }
                default:
                    break;
            }

            return tmpAttr.ToString();
        }

        private static void AddUpdateCustomer(AcceptCustomer model, TasksUnitOfWork unitOfWork)
        {
            var tmpCustomer = unitOfWork.Context.SearchShipper(null, model.CustomerName).FirstOrDefault();
            var tmpShipper = unitOfWork.Context.Shippers.SingleOrDefault(item => item.Name == model.CustomerName);
            // not existing customer
            if (tmpCustomer == null)
            {
                unitOfWork.Context.AddShipper(SessionManager.UserInfo.UserLocalId, model.CustomerName, model.IATACode, model.CustomerTypeId, String.Empty, String.Empty, null, string.Empty, string.Empty, null);
            }
            else
            {
                tmpShipper.ShipperTypeId = model.CustomerTypeId;
                tmpShipper.IAC_Agent_Number = model.IATACode;
            }
        }
        #endregion
    }
}
