﻿using MCH.BLL.Model.Enum;
using MCH.BLL.Units;
using MCH.Web.Models.Common;
using MCH.Web.Models.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        [HttpGet]
        public PartialViewResult GetDashboard()
        {
            using (DashboardUnit dashboardUnit = new DashboardUnit())
            {
                DashboardModel tmpModel = new DashboardModel();

                tmpModel.TaskTypes = dashboardUnit.TaskTypeRepository.Get().Select(item => new SelectComboItem { Value = item.Id, Text = item.Description, DefaultIndex = 1 });
                tmpModel.Warehouses = dashboardUnit.WarehouseRepository.Get().Select(item => new SelectComboItem { Value = item.Id, Text = item.Code, DefaultIndex = 1 });

                return PartialView("_Dashboard", tmpModel);
            }
        }

        public JsonResult GetMenuTaskCounts() 
        {
            using (DashboardUnit dashboardUnit = new DashboardUnit())
            {
                var tmpData = dashboardUnit.Context.Tasks.Where(item => item.ParentTaskId == null).GroupBy(item => item.TaskTypeId).Select(item=> new {TaskTypeId  = item.Key, Count = item.Count()}).ToList();
                return Json(tmpData, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public PartialViewResult GetDashPanel(DashPanelModel model)
        {
            using (DashboardUnit dashboardUnit = new DashboardUnit())
            {
                DashTileContainer tmpModel = new DashTileContainer();
                tmpModel.StationId = model.StationId;
                tmpModel.From = model.FromDate;
                tmpModel.To = model.ToDate;

                tmpModel.Tiles = new List<DashTaskTileModel>();

                var tasksByType = dashboardUnit.TaskRepository.Get().Where(item => item.Warehouse != null && item.Warehouse.PortId == model.StationId && item.TaskDate <= model.ToDate && item.TaskDate >= model.FromDate && item.ParentTaskId == null).GroupBy(item => item.TaskType);

                


                if (tasksByType.Any()) 
                {
                    var tmpAllTaskStatuses = dashboardUnit.Context.Statuses.Where(item => item.EntityTypeId == (int)enumEntityTypes.Task && item.IsWorkFlowItem).ToList();

                    foreach (var item in tasksByType)
                    {
                        DashTaskTileModel tmpTile = new DashTaskTileModel();
                        tmpTile.TotalCount = item.Count();
                        tmpTile.TypeId = item.Key.Id;
                        tmpTile.TypeName = item.Key.Description;

                        var taskByStatus = item.GroupBy(tItem => tItem.Status.Id);
                        if (taskByStatus.Any()) 
                        {
                            tmpTile.TasksByStatus = new List<TaskByStatusModel>();
                            foreach (var sItem in tmpAllTaskStatuses)
                            {
                                TaskByStatusModel tmpByStatus = new TaskByStatusModel();
                                tmpByStatus.StatusId = sItem.Id;
                                tmpByStatus.StatusName = sItem.DisplayName;

                                //TODO : Harut fix this
                                var tmpMatchedStatus = taskByStatus.SingleOrDefault(e => e.Key == sItem.Id);

                                tmpByStatus.TotalCount = tmpMatchedStatus != null ? tmpMatchedStatus.Count() : 0;

                                tmpTile.TasksByStatus.Add(tmpByStatus);
                            }
                        }
                        tmpModel.Tiles.Add(tmpTile);
                    }
                }

                return PartialView("_DashPanel", tmpModel);
            }
        }

    }
}
