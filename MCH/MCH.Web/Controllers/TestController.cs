﻿using Kendo.Mvc.UI;
using MCH.Web.Models.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;

namespace MCH.Web.Controllers
{
    public class TestController : BaseController
    {
        //
        // GET: /Test/

        public PartialViewResult TestPage()
        {
            return PartialView("_Page");
        }

        public JsonResult GetDynamic([DataSourceRequest] DataSourceRequest request) 
        {
            var tmpList = new List<DynamicGridRow>();
            for (int i = 0; i < 100; i++)
            {
                tmpList.Add(new DynamicGridRow { 
                    Id = i + 1,
                    Name = "Name" + (i + 1),
                    FirstName="First Name"+(i+1),
                    LastName="Last Name"+(i+1),
                    CreateDate = DateTime.Now });
            }

            return Json(tmpList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

    }
}
