﻿using MCH.BLL.Units;
using MCH.Web.Models.Alert;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCH.Web.Controllers
{
    public class AlertsController : BaseController
    {

        public PartialViewResult GetTaskAltersListView(TaskAlertsListModel model) 
        {
            using (CommonUnit unitOfWork = new CommonUnit())
            {
                var tmpTask = unitOfWork.Context.Tasks.SingleOrDefault(item => item.Id == model.TaskId);

                model.FlightId = tmpTask.EntityId;

                return PartialView("_TaskAlertsList", model);
            }
        }

        public JsonResult GetTaskAlertsList(TaskAlertListFliter filter) 
        {
            using (CommonUnit unitOfWork = new CommonUnit()) 
            {
                var tmpPort = unitOfWork.Context.Ports.SingleOrDefault(item=> item.Id == SessionManager.UserInfo.DefaultSiteId);

                var tmpAlters = unitOfWork.Context.GetFlightAlerts(tmpPort.IATACode, filter.FlightId, true).ToList();

                return Json(new
                {
                    sEcho = filter.sEcho,
                    iTotalRecords = tmpAlters.Count,
                    iTotalDisplayRecords = tmpAlters.Count,
                    aaData = tmpAlters
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
