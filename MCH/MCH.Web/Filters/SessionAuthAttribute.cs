﻿using CMX.Shell.Security;
using MCH.BLL.Units;
using MCH.Web.Controllers;
using MCH.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace MCH.Web.Filters
{
    public class SessionAuthentication : ActionFilterAttribute
    {
        public void AuthenticateUserSession(string argSessionParam, ActionExecutingContext filterContext)
        {
            ISessionAuthenticationService tmpService = new AuthenticationService();

            UserInfo tmpUserInfo = null;

            string[] securityToken = argSessionParam.Split('_');

            tmpUserInfo = tmpService.AuthenticateUser(securityToken[0], Int64.Parse(securityToken[1]));

            if (tmpUserInfo != null)
            {
                // check if the user has all the settings for the current app and updates the local cache for the shell settings
                var tmpUserCheckResult = this.CheckAndSetupUserLocals(tmpUserInfo);
                if (!tmpUserCheckResult.Any())
                {
                    FormsAuthentication.SetAuthCookie(tmpUserInfo.Username, false);
                    SessionManager.UserInfo = tmpUserInfo;
                }
                else
                {
                    tmpUserCheckResult.Add("Test Error Message");
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "UserConfError",
                        ViewData = new ViewDataDictionary(tmpUserCheckResult)
                    };
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {{ "Controller", "Account" },
                                      { "Action", "SessionExpired" } });
            }
        }

        public void AuthenticateLocal(ActionExecutingContext filterContext)
        {
            ISessionAuthenticationService tmpService = new AuthenticationService();

            UserInfo tmpUserInfo = null;


            //used only to debug the app out of the shell
            // has limited access
            //tmpUserInfo = tmpService.AuthenticateUserCredentials("oneil.parker@cargomatrix.com", "Oneil123#");


            // is shell admin
           tmpUserInfo = tmpService.AuthenticateUserCredentials("vineeta.gupta@cargomatrix.com", "Vineeta123#");
            //tmpUserInfo = tmpService.AuthenticateUserCredentials("karenclausen", "Karen123#");

            if (tmpUserInfo != null)
            {
                // check if the user has all the settings for the current app and updates the local cache for the shell settings
                var tmpUserCheckResult = this.CheckAndSetupUserLocals(tmpUserInfo);
                if (!tmpUserCheckResult.Any())
                {
                    FormsAuthentication.SetAuthCookie(tmpUserInfo.Username, false);
                    SessionManager.UserInfo = tmpUserInfo;
                }
                else
                {
                    tmpUserCheckResult.Add("Test Error Message");
                    filterContext.Result = new ViewResult { 
                        ViewName = "UserConfError",
                        ViewData = new ViewDataDictionary(tmpUserCheckResult)
                    };
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {{ "Controller", "Account" },
                                      { "Action", "SessionExpired" } });
            }
        }

        /// <summary>
        /// Checks the user shell settings and updates the local cache.
        /// </summary>
        /// <param name="argUser">UserInfo object got from Shell Auth. service.</param>
        /// <returns> List of error messages if something is wrong with user settings.</returns>
        private List<string> CheckAndSetupUserLocals(UserInfo argUser)
        {
            var tmpErrorMessages = new List<string>();
            using (UserUnitOfWork userUnit = new UserUnitOfWork())
            {
                var tmpUserProfile = userUnit.GetUserByShellId(argUser.UserId);
                if (tmpUserProfile != null)
                {
                    argUser.UserLocalId = tmpUserProfile.Id;
                    tmpErrorMessages = userUnit.UpdateShellUser(argUser);
                }
                else
                {
                    tmpErrorMessages = userUnit.AddShellUser(argUser);
                }
            }

            return tmpErrorMessages;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string tmpSessionParam = filterContext.HttpContext.Request.QueryString[ConfigurationKeys.SESSION_TOKEN];

            if (!String.IsNullOrWhiteSpace(tmpSessionParam))
            {
                if (filterContext.HttpContext.Request.IsAuthenticated)
                {
                    FormsAuthentication.SignOut();
                }

                this.AuthenticateUserSession(tmpSessionParam, filterContext);
            }
            else if (!filterContext.HttpContext.Request.IsAuthenticated || SessionManager.UserInfo == null)
            {
#if DEBUG
                this.AuthenticateLocal(filterContext);
#endif
#if ! DEBUG

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {{ "Controller", "Account" },
                                      { "Action", "SessionExpired" } });
#endif
            }
            else
            {

            }

            base.OnActionExecuting(filterContext);
        }
    }
}