﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCH.Web.Models.Common;

namespace MCH.Web.HtmlHelpers
{
    public static class DynamicMenuBuilder
    {
        public static MvcHtmlString DynamicMenu(this HtmlHelper htmlHelpers, List<MenuItemModel> menuItemList)
        {
            string menu= "<div class='tab-pane fade active in'>";
            for (int i = 0; i < menuItemList.Count; i++)
            {
                menu += CreateMenuItem(menuItemList[i]);
            }
            menu += "</div>";
            return MvcHtmlString.Create(menu);
        }

        private static string CreateMenuItem(MenuItemModel menuItem)
        {
            var item = @"<a href='" + menuItem.NavigationLink + @"' class='media pull-left thumbnail' name='" + menuItem.Name + @"' style='width: 300px;padding: 10px;margin:5px'>
                <div class='pull-left thumbnail'>
                    <img src='" + menuItem.Icon + @"' class='media-object'>
                </div>
                <div class='media-body'>
                    <label class='media-heading' style='white-space:nowrap;max-width:150px'>" + menuItem.Title + @"</label>
                    <p/>
                    <label style='margin-left:10px;max-height:50px'>" + menuItem.Description + @"</label>
                </div>
            </a>";
            return item;   
        }
    }
}