﻿using System.Web;
using System.Web.Optimization;

namespace MCH.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/dhtmlx").Include(
                         "~/Scripts/dhtmlx.js"));

            bundles.Add(new ScriptBundle("~/bundles/dischargeList").Include(
                         "~/Scripts/modules/discharge/discharge-list.js")
                         .Include("~/Scripts/modules/discharge/cargo-pickup.js")
                         .Include("~/Scripts/modules/discharge/document-pickup.js"));
           

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/dhtmlx.css",
                      "~/Content/Site.css"));
        }
    }
}