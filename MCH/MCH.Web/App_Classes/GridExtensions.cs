﻿using Kendo.Mvc.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.Web.App_Classes
{
    public static class GridExtensions
    {

        public static DataSourceResult ToCustomDataSource(this IList source, DataSourceRequest request, int total)
        {
            request.Page = 1;
            var dataResult = source.ToCustomDataSource(request, total);
            dataResult.Total = total;

            return dataResult;
        }

        public static int ToJulianDate(this DateTime date)
        {
            return date.DayOfYear;
        }

        public static DateTime FromJulianDate(int jDate)
        {
            return new DateTime(DateTime.Now.Year, 1, 1).AddDays(jDate - 1);
        }
    }
}