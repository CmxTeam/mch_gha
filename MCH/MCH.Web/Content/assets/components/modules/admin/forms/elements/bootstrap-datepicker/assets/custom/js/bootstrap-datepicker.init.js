if (typeof $.fn.bdatepicker == 'undefined')
    $.fn.bdatepicker = $.fn.datepicker.noConflict();
$(function()
{

	/* DatePicker */
	// default
	$(".bt-date-default").bdatepicker({
		format: 'yyyy-mm-dd',
		startDate: "2013-02-14"
	});

	// component
	$('.bt-date').bdatepicker({
		format: "dd MM yyyy",
		startDate: "2013-02-14"
	});

	// today button
	$('.bt-date-button').bdatepicker({
		format: "dd MM yyyy",
		startDate: "2013-02-14",
		todayBtn: true
	});

	// advanced
	$('.bt-date-hours').bdatepicker({
		format: "dd MM yyyy - hh:ii",
        autoclose: true,
        todayBtn: true,
        startDate: "2013-02-14 10:00",
        minuteStep: 10
	});
	
	// meridian
	$('.bt-date-hours-meridian').bdatepicker({
		format: "dd MM yyyy - HH:ii P",
	    showMeridian: true,
	    autoclose: true,
	    startDate: "2013-02-14 10:00",
	    todayBtn: true
	});

	// other
	if ($('.bt-date-other').length) $(".bt-date-other").bdatepicker({ showOtherMonths:true });
	if ($('.bt-date-inline').length) $('.bt-date-inline').bdatepicker({ inline: true, showOtherMonths:true });

});