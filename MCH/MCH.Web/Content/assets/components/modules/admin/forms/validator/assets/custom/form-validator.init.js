$.validator.setDefaults(
{
    submitHandler: function ()
    {
        this.currentForm.submit();
    },
	showErrors: function(map, list) 
	{
		this.currentElements.parents('label:first, div:first').find('.has-error').remove();
		this.currentElements.parents('.form-group:first').removeClass('has-error');
		
		$.each(list, function(index, error) 
		{
			var ee = $(error.element);
			var eep = ee.parents('label:first').length ? ee.parents('label:first') : ee.parents('div:first');
			
			ee.parents('.form-group:first').addClass('has-error');
			eep.find('.has-error').remove();
			eep.append('<p class="has-error help-block">' + error.message + '</p>');
		});
	}
});

$(function()
{
	// validate signup form on keyup and submit
	$(".validateSubmitForm").validate({
		rules: {
			Firstname: "required",
			Lastname: "required",
			Username: {
				required: true,
				minlength: 2
			},
			Password: {
				required: true,
				minlength: 5
			},
			ConfirmPassword: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			Email: {
				required: true,
				email: true
			},
			Topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
			Agree: "required"
		},
		messages: {
			Firstname: "Please enter your firstname",
			Lastname: "Please enter your lastname",
			Username: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			},
			Password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			ConfirmPassword: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			Email: "Please enter a valid email address",
			Agree: "Please accept our policy"
		}
	});

	// propose username by combining first- and lastname
	$("#username").focus(function() {
		var firstname = $("#firstname").val();
		var lastname = $("#lastname").val();
		if(firstname && lastname && !this.value) {
			this.value = firstname + "." + lastname;
		}
	});

	//code to hide topic selection, disable for demo
	var newsletter = $("#newsletter");
	// newsletter topics are optional, hide at first
	var inital = newsletter.is(":checked");
	var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
	var topicInputs = topics.find("input").attr("disabled", !inital);
	// show when newsletter is checked
	newsletter.click(function() {
		topics[this.checked ? "removeClass" : "addClass"]("gray");
		topicInputs.attr("disabled", !this.checked);
	});
});