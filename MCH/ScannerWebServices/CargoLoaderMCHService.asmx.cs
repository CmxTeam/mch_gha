﻿using CMX.Framework.Utils.Security;
using MCH.BLL.DAL;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ScannerWebServices
{
    public class CargoLoaderMCHService : ServiceBase
    {
        [WebMethod]
        public CargoLoaderFlight[] GetCargoLoaderFlights(string station, int userID, TaskStatuses status, string carrierNo, string destination, string flightNo, ReceiverSortFields sortBy)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork()) 
            {
                return loaderUnit.GetCargoLoaderFlights(station, userID, status, carrierNo, destination, flightNo, sortBy);
            }
        }

        [WebMethod]
        public CargoLoaderFlight GetCargoLoaderFlight(long flightManifestId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlight(flightManifestId,taskId);
            }
        }

        [WebMethod]
        public CargoLoaderFlightLeg[] GetCargoLoaderFlightLegs(long flightManifestId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlightLegs(flightManifestId);
            }
        }

        [WebMethod]
        public AlertModel[] GetFlightAlerts(string warehousePort,long manifestId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetFlightAlerts(warehousePort, manifestId);
            }                       
        }

        [WebMethod]
        public AlertModel[] GetLegAlerts(long legId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetLegAlerts(legId);
            } 
        }

        [WebMethod]
        public CargoLoaderFlightLeg GetCargoLoaderFlightLeg(long legId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlightLeg(legId);
            } 
        }

        [WebMethod]
        public TransactionStatus UpdateFlightLegLocation(long legId, long locationId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateFlightLegLocation(legId,locationId, userId);
            } 
        }

        [WebMethod]
        public TransactionStatus UpdateCargoLoaderFlightLegUld(long uldLegId, string uldSerialNo, string prefix, long userId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateCargoLoaderFlightLegUld(uldLegId, uldSerialNo, prefix, userId, taskId);
            } 
        }

        [WebMethod]
        public CargoLoaderFlightLegUld[] GetCargoLoaderFlightUlds(long flightId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetCargoLoaderFlightUlds(flightId);
            } 
        }

        [WebMethod]
        public string[] GetUldPrefix()
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetUldPrefix();
            } 
        }

        [WebMethod]
        public CargoLoaderFlightLegUld[] GetFlightDestinationBUPs(long legId, FlightDestinationTypes type, string uldType)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetFlightDestinationBUPs(legId, type, uldType);
            } 
        }

        [WebMethod]
        public int GetForkLiftCount(long userId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetForkLiftCount(userId, taskId);
            } 
        }

        [WebMethod]
        public UldType[] GetUldTypes()
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetUldTypes();
            } 
        }

        [WebMethod]
        public TransactionStatus UpdateUldWeight(long uldId, decimal value)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateUldWeight(uldId, value);
            } 
        }

        [WebMethod]
        public TransactionStatus UpdateUldTareWeight(long uldId, decimal value)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateUldTareWeight(uldId, value);
            } 
        }

        [WebMethod]
        public TransactionStatus FinalizeCargoLoader(long taskId, long flightId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.FinalizeCargoLoader(taskId, flightId, userId);
            } 
        }

        [WebMethod]
        public TransactionStatus AddCargoLoaderFlightLegULD(long uldTypeId, string prefix, string serial, long taskId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.AddCargoLoaderFlightLegULD(uldTypeId, prefix, serial,taskId, userId);
            } 
        }

        [WebMethod]
        public TransactionStatus UpdateFlightLegUldLocation(long uldId, int locationId, long taskId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateFlightLegUldLocation(uldId, locationId, taskId, userId);
            } 
        }

        [WebMethod]
        public TransactionStatus DeleteCargoLoaderFlightLegUld(long uldId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DeleteCargoLoaderFlightLegUld(uldId, userId);
            } 
        }

        [WebMethod]
        public AwbModel[] GetLoadingPlan(long flightId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetLoadingPlan(flightId, taskId);
            } 
        }

        [WebMethod]
        public AwbModel GetLoadingPlanForAwb(long awbId, long taskId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetLoadingPlanForAwb(awbId, taskId);
            } 
        }

        [WebMethod]
        public UldViewItem[] GetUldView(long flightId, long uldId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetUldView(flightId, uldId);
            } 
        }

        [WebMethod]
        public ValidatedShipment ValidateLoaderShipment(long flightId, long taskId, string shipment)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.ValidateLoaderShipment(flightId, taskId, shipment);
            } 
        }

        [WebMethod]
        public TransactionStatus DropPiecesIntoForklift(long userId, long taskId, long awbId, int count)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropPiecesIntoForklift(userId, taskId, awbId, count);
            } 
        }

        [WebMethod]
        public CargoLoaderFlightLegUld[] AvailableULDsForBuild(long flightId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.AvailableULDsForBuild(flightId);
            } 
        }

        [WebMethod]
        public LoadForkliftViewItem[] GetForkliftView(long taskId, long userId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.GetForkliftView(taskId, userId);
            } 
        }

        [WebMethod]
        public TransactionStatus RemovePiecesFromForklift(long detailsId, int count)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.RemovePiecesFromForklift(detailsId, count);
            } 
        }

        //[WebMethod]
        //public TransactionStatus DropSelectedItemsIntoLocationTest(long taskId, long userId, int locationId)
        //{
        //    var detailIds = new long [] { 35 };
        //    return DropSelectedItemsIntoLocationImpl(taskId, userId, locationId, () => { return DataContext.ForkliftDetails.Where(fd => detailIds.Contains(fd.Id)); });
        //}

        [WebMethod]
        public TransactionStatus DropSelectedItemsIntoLocation(long taskId, long userId, int locationId, long[] detailIds)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropSelectedItemsIntoLocation(taskId, userId, locationId, detailIds);
            } 
        }

        [WebMethod]
        public TransactionStatus DropAllForkliftPiecesIntoLocation(long taskId, long userId, int locationId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropAllForkliftPiecesIntoLocation(taskId, userId, locationId);
            }
        }

        [WebMethod]
        public TransactionStatus DropSelectedItemsIntoULD(long taskId, long userId, long uldId, long[] detailIds)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropSelectedItemsIntoULD(taskId, userId, uldId, detailIds);
            }
        }

        [WebMethod]
        public TransactionStatus DropAllForkliftPiecesIntoULD(long taskId, long userId, long uldId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.DropAllForkliftPiecesIntoULD(taskId, userId, uldId);
            }
        }

        [WebMethod]
        public TransactionStatus SetFlightBuildLocation(long taskId, long userId, int locationId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.SetFlightBuildLocation(taskId, userId, locationId);
            }
        }

        [WebMethod]
        public TransactionStatus RemoveAwbFromUld(long manifestAwbDetaildId, long uldId, long taskId, long userId, int locationId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.RemoveAwbFromUld(manifestAwbDetaildId, uldId, taskId, userId, locationId);
            }
        }

        [WebMethod]
        public TransactionStatus UpdateFlightBuildLocation(long flightManifestId, long userId, long taskId, int locationId)
        {
            using (LoaderUnitOfWork loaderUnit = new LoaderUnitOfWork())
            {
                return loaderUnit.UpdateFlightBuildLocation(flightManifestId, userId, taskId, locationId);
            }
        }
    }
}