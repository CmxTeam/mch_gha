﻿using MCH.BLL.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ScannerWebServices
{
    [WebService(Namespace = "http://cargomchscannerservice.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ServiceBase : System.Web.Services.WebService
    {
        
    }
}