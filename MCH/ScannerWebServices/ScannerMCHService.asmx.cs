﻿using MCH.BLL.DAL;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Units;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Services;

namespace ScannerWebServices
{
    public class ScannerMCHService : ServiceBase
    {
        
        [WebMethod]
        public WarehouseLocationModel[] GetWarehouseLocations(string station, LocationType location)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetWarehouseLocations(station, location);
            }
        }

        [WebMethod]
        public LocationItem GetLocationIdByLocationBarcode(string station, string barcode)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetLocationIdByLocationBarcode(station, barcode);
            }
        }

        [WebMethod]
        public void AddTaskSnapshotReference(long taskId, string reference)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                scannerUnit.AddTaskSnapshotReference(taskId, reference);
            }
        }

        [WebMethod]
        public bool ExecuteNonQuery(string station, string query)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.ExecuteNonQuery(station, query);
            }
        }

        [WebMethod]
        public DataTable ExecuteQuery(string station, string query)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.ExecuteQuery(station, query);
            }
        }

        [WebMethod]
        public AwbInfo GetAwbInfo(string station, string shipment)
        {
            using (ScannerUnitOfWork scannerUnit = new ScannerUnitOfWork())
            {
                return scannerUnit.GetAwbInfo(station, shipment);
            }
        }
    }
}
