﻿using MCH.BLL.DAL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Helpers
{
    internal static class ModelStructHelpers
    {
        internal static string CustomerShortString(Customer customer)
        {
            string tmpName = String.Empty;
            if (customer != null)
            {
                StringBuilder tmpNameBuild = new StringBuilder(customer.Name);

                if (!string.IsNullOrWhiteSpace(customer.AddressLine1))
                {
                    tmpNameBuild.Append(", ");
                    tmpNameBuild.Append(customer.AddressLine1);
                }
                if (!string.IsNullOrWhiteSpace(customer.PostalCode))
                {
                    tmpNameBuild.Append(", ");
                    tmpNameBuild.Append(customer.PostalCode);
                }

                if (customer.State != null && !string.IsNullOrWhiteSpace(customer.State.StateProvince))
                {
                    tmpNameBuild.Append(", ");
                    tmpNameBuild.Append(customer.State.StateProvince);
                }
                if (customer.Country != null && !string.IsNullOrWhiteSpace(customer.Country.TwoCharacterCode))
                {
                    tmpNameBuild.Append(", ");
                    tmpNameBuild.Append(customer.Country.TwoCharacterCode);
                }

                tmpName = tmpNameBuild.ToString();
            }

            return tmpName;
        }
    }
}
