//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    
    public partial class GetPutedAwayList_Result
    {
        public string Type { get; set; }
        public long EntityId { get; set; }
        public string Reference { get; set; }
        public string Reference1 { get; set; }
        public Nullable<int> IsBUP { get; set; }
        public Nullable<int> WarehouseLocationId { get; set; }
        public int Count { get; set; }
        public string Location { get; set; }
        public long DispositionId { get; set; }
        public Nullable<int> Status { get; set; }
        public string PaId { get; set; }
        public string StatusImagePath { get; set; }
    }
}
