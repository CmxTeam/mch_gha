//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class LabelTemplatesZPL
    {
        public int Id { get; set; }
        public string LabelName { get; set; }
        public Nullable<int> AccountId { get; set; }
        public string LabelTemplateZPL { get; set; }
        public Nullable<int> LabelTypeId { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual PrinterLabelType PrinterLabelType { get; set; }
    }
}
