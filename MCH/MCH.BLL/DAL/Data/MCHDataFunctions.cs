﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.DAL.Data
{
    public partial class MCHDBEntities
    {
        [DbFunction("MCHDBModel.Store", "MCH_GetSPHCodesList")]
        public static string MCH_GetSPHCodesList(long EntityId, int EntityTypeId)
        {
            throw new NotSupportedException("Direct calls are not supported.");
        }

        [DbFunction("MCHDBModel.Store", "MCH_ConvertToLocalDateTime")]
        public static DateTime? MCH_ConvertToLocalDateTime(int WarehouseId, DateTime ? Date) 
        {
            throw new NotSupportedException("Direct calls are not supported.");
        }
    }
}
