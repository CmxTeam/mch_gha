//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DistributionList
    {
        public DistributionList()
        {
            this.EmailDistributionLists = new HashSet<EmailDistributionList>();
            this.EmailTemplates = new HashSet<EmailTemplate>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<bool> IsMain { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual ICollection<EmailDistributionList> EmailDistributionLists { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
    }
}
