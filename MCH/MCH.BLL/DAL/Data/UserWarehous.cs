//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserWarehous
    {
        public int Id { get; set; }
        public Nullable<long> UserId { get; set; }
        public Nullable<int> WarehouseId { get; set; }
        public bool IsDefault { get; set; }
    
        public virtual UserProfile UserProfile { get; set; }
        public virtual Warehouse Warehouse { get; set; }
    }
}
