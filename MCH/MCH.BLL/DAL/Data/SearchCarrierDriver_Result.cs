//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    
    public partial class SearchCarrierDriver_Result
    {
        public Nullable<long> Sec { get; set; }
        public Nullable<int> Total { get; set; }
        public long Id { get; set; }
        public string DriverName { get; set; }
        public string TruckingCo { get; set; }
        public Nullable<int> TruckingCompanyId { get; set; }
        public string DriverPhoto { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseImage { get; set; }
        public Nullable<int> CredentialTypeId { get; set; }
        public string CredType { get; set; }
    }
}
