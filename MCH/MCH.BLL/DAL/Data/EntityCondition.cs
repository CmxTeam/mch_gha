//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class EntityCondition
    {
        public long Id { get; set; }
        public Nullable<int> EntityTypeId { get; set; }
        public Nullable<long> EntityId { get; set; }
        public Nullable<int> ConditionId { get; set; }
        public Nullable<int> Count { get; set; }
        public string UserName { get; set; }
        public Nullable<System.DateTime> Timestamp { get; set; }
        public Nullable<long> TaskId { get; set; }
        public Nullable<int> TaskTypeId { get; set; }
    
        public virtual Condition Condition { get; set; }
        public virtual EntityType EntityType { get; set; }
        public virtual TaskType TaskType { get; set; }
    }
}
