//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    
    public partial class GetCargoAcceptAWBAttachments_Result
    {
        public Nullable<long> Sec { get; set; }
        public Nullable<int> Total { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Description { get; set; }
        public long AWBId { get; set; }
        public string UploadedBy { get; set; }
        public string AttachmentImage { get; set; }
    }
}
