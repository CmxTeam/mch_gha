//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class WarehouseZone
    {
        public WarehouseZone()
        {
            this.WarehouseLocations = new HashSet<WarehouseLocation>();
        }
    
        public int Id { get; set; }
        public Nullable<int> WarehouseId { get; set; }
        public string Zone { get; set; }
    
        public virtual ICollection<WarehouseLocation> WarehouseLocations { get; set; }
        public virtual Warehouse Warehouse { get; set; }
    }
}
