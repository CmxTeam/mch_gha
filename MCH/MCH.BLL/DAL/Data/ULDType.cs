//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ULDType
    {
        public ULDType()
        {
            this.Overpacks = new HashSet<Overpack>();
            this.ULDs = new HashSet<ULD>();
        }
    
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<int> Length { get; set; }
        public Nullable<int> Width { get; set; }
        public Nullable<int> Height { get; set; }
        public Nullable<int> DimUOMId { get; set; }
        public Nullable<double> MaxWeight { get; set; }
        public Nullable<double> TareWeight { get; set; }
        public Nullable<int> WeightUOMId { get; set; }
        public Nullable<double> MaxVolume { get; set; }
        public Nullable<int> VolumeUOMId { get; set; }
        public Nullable<int> Size { get; set; }
        public Nullable<bool> IsDefault { get; set; }
    
        public virtual ICollection<Overpack> Overpacks { get; set; }
        public virtual ICollection<ULD> ULDs { get; set; }
        public virtual UOM UOM { get; set; }
        public virtual UOM UOM1 { get; set; }
        public virtual UOM UOM2 { get; set; }
    }
}
