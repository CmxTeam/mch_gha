//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FlightAttachment
    {
        public long Id { get; set; }
        public System.DateTime RecDate { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Description { get; set; }
        public long FlightManifestID { get; set; }
        public string UserName { get; set; }
        public byte[] DocumentImage { get; set; }
    
        public virtual FlightManifest FlightManifest { get; set; }
    }
}
