//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FlightManifest
    {
        public FlightManifest()
        {
            this.FlightAttachments = new HashSet<FlightAttachment>();
            this.FlightNotocMasters = new HashSet<FlightNotocMaster>();
            this.FlightSnapshots = new HashSet<FlightSnapshot>();
            this.FlightTransactions = new HashSet<FlightTransaction>();
            this.HostPlus_CIMPOutboundQueue = new HashSet<HostPlus_CIMPOutboundQueue>();
            this.LoadingPlans = new HashSet<LoadingPlan>();
            this.ManifestAWBDetails = new HashSet<ManifestAWBDetail>();
            this.ManifestDetails = new HashSet<ManifestDetail>();
            this.ManifestPackageDetails = new HashSet<ManifestPackageDetail>();
            this.ULDs = new HashSet<ULD>();
            this.UnloadingPorts = new HashSet<UnloadingPort>();
        }
    
        public long Id { get; set; }
        public Nullable<int> CarrierId { get; set; }
        public string FlightNumber { get; set; }
        public Nullable<System.DateTime> ETA { get; set; }
        public Nullable<System.DateTime> ETD { get; set; }
        public Nullable<int> TotalPieces { get; set; }
        public Nullable<int> TotalULDs { get; set; }
        public Nullable<int> TotalAWBs { get; set; }
        public Nullable<int> StatusId { get; set; }
        public Nullable<System.DateTime> StatusTimestamp { get; set; }
        public Nullable<int> ReceivedPieces { get; set; }
        public Nullable<bool> IsOSD { get; set; }
        public Nullable<int> StagingLocationId { get; set; }
        public string RecoveredBy { get; set; }
        public Nullable<System.DateTime> RecoveredOn { get; set; }
        public Nullable<int> RecoveredULDs { get; set; }
        public Nullable<int> PutAwayPieces { get; set; }
        public Nullable<double> PercentRecovered { get; set; }
        public Nullable<double> PercentReceived { get; set; }
        public Nullable<double> PercentPutAway { get; set; }
        public string AircraftRegistration { get; set; }
        public Nullable<System.DateTime> ArrivalPortETA { get; set; }
        public Nullable<long> OriginId { get; set; }
        public Nullable<long> DestinationId { get; set; }
        public Nullable<bool> IsComplete { get; set; }
        public Nullable<System.DateTime> RecDate { get; set; }
        public Nullable<double> PercentOverall { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> RecoveredLocationId { get; set; }
        public string StagedBy { get; set; }
        public Nullable<System.DateTime> StagedOn { get; set; }
        public Nullable<bool> IsOnhold { get; set; }
        public Nullable<int> TenderedCarrierId { get; set; }
        public Nullable<long> TenderedDriverId { get; set; }
        public byte[] TenderedDriverSignature { get; set; }
        public Nullable<int> TenderedTotalPieces { get; set; }
        public string TenderedBy { get; set; }
        public Nullable<System.DateTime> TenderedTimestamp { get; set; }
        public Nullable<bool> IsPassenger { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual CarrierDriver CarrierDriver { get; set; }
        public virtual Carrier Carrier { get; set; }
        public virtual Carrier Carrier1 { get; set; }
        public virtual ICollection<FlightAttachment> FlightAttachments { get; set; }
        public virtual Port Port { get; set; }
        public virtual Port Port1 { get; set; }
        public virtual WarehouseLocation WarehouseLocation { get; set; }
        public virtual Status Status { get; set; }
        public virtual WarehouseLocation WarehouseLocation1 { get; set; }
        public virtual ICollection<FlightNotocMaster> FlightNotocMasters { get; set; }
        public virtual ICollection<FlightSnapshot> FlightSnapshots { get; set; }
        public virtual ICollection<FlightTransaction> FlightTransactions { get; set; }
        public virtual ICollection<HostPlus_CIMPOutboundQueue> HostPlus_CIMPOutboundQueue { get; set; }
        public virtual ICollection<LoadingPlan> LoadingPlans { get; set; }
        public virtual ICollection<ManifestAWBDetail> ManifestAWBDetails { get; set; }
        public virtual ICollection<ManifestDetail> ManifestDetails { get; set; }
        public virtual ICollection<ManifestPackageDetail> ManifestPackageDetails { get; set; }
        public virtual ICollection<ULD> ULDs { get; set; }
        public virtual ICollection<UnloadingPort> UnloadingPorts { get; set; }
    }
}
