//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ForkliftDetail
    {
        public long Id { get; set; }
        public Nullable<long> HWBId { get; set; }
        public Nullable<long> AWBId { get; set; }
        public Nullable<int> Pieces { get; set; }
        public Nullable<long> ULDId { get; set; }
        public Nullable<long> TaskId { get; set; }
        public Nullable<System.DateTime> Timestamp { get; set; }
        public Nullable<long> PackageId { get; set; }
        public Nullable<long> UserId { get; set; }
    
        public virtual AWB AWB { get; set; }
        public virtual HWB HWB { get; set; }
        public virtual Package Package { get; set; }
        public virtual ULD ULD { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
