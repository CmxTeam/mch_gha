//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    
    public partial class MCH_GetFlightManifests_Result
    {
        public Nullable<long> Id { get; set; }
        public string ULD { get; set; }
        public string AWB { get; set; }
        public string AwbPart { get; set; }
        public Nullable<int> TotalAwbPcs { get; set; }
        public Nullable<int> AwbPiecesInULD { get; set; }
        public Nullable<int> AwbTaskReadyPcs { get; set; }
        public string GoodsDescription { get; set; }
        public string AwbOrigin { get; set; }
        public string AWBDest { get; set; }
        public string TransferPort { get; set; }
        public Nullable<double> AWBULDWeight { get; set; }
        public string WeightUOM { get; set; }
        public string SHC { get; set; }
        public string ControlNumber { get; set; }
    }
}
