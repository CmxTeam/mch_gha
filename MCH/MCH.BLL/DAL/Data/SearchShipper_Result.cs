//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    
    public partial class SearchShipper_Result
    {
        public Nullable<long> Sec { get; set; }
        public Nullable<int> Total { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string IATACode { get; set; }
        public string SType { get; set; }
        public Nullable<long> STypeId { get; set; }
        public string CCSF { get; set; }
        public int CustomerId { get; set; }
    }
}
