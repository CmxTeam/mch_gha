//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    
    public partial class GetCheckInList_Result
    {
        public long PackageId { get; set; }
        public string RefNumber { get; set; }
        public int TotalPieces { get; set; }
        public int ReceivedPcs { get; set; }
        public string Location { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string HWBSerialNumber { get; set; }
        public string ULDNumber { get; set; }
    }
}
