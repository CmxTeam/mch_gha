//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class AircraftType
    {
        public AircraftType()
        {
            this.AlternateScreeningCases = new HashSet<AlternateScreeningCas>();
            this.AWBs = new HashSet<AWB>();
            this.FlightNotocDGDetails = new HashSet<FlightNotocDGDetail>();
            this.ShipperManifests = new HashSet<ShipperManifest>();
            this.ShipperManifests_AWBs = new HashSet<ShipperManifests_AWBs>();
            this.ShipperTypes = new HashSet<ShipperType>();
        }
    
        public int Id { get; set; }
        public string AircraftTypeCode { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<AlternateScreeningCas> AlternateScreeningCases { get; set; }
        public virtual ICollection<AWB> AWBs { get; set; }
        public virtual ICollection<FlightNotocDGDetail> FlightNotocDGDetails { get; set; }
        public virtual ICollection<ShipperManifest> ShipperManifests { get; set; }
        public virtual ICollection<ShipperManifests_AWBs> ShipperManifests_AWBs { get; set; }
        public virtual ICollection<ShipperType> ShipperTypes { get; set; }
    }
}
