//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    
    public partial class MCH_GetBuildShipments_Result
    {
        public string References { get; set; }
        public int Flag { get; set; }
        public Nullable<long> AwbId { get; set; }
        public long DetailId { get; set; }
        public string AWB { get; set; }
        public Nullable<int> TotalPieces { get; set; }
        public int ScannedPieces { get; set; }
        public string Locations { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public int Status { get; set; }
        public Nullable<double> PercentProgress { get; set; }
    }
}
