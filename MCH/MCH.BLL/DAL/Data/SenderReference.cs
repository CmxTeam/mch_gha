//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SenderReference
    {
        public long Id { get; set; }
        public System.DateTime RecDate { get; set; }
        public long AwbId { get; set; }
        public Nullable<long> SenderPortId { get; set; }
        public string SenderFunctionCode { get; set; }
        public string SenderOfficeCode { get; set; }
        public string SenderFileReference { get; set; }
        public Nullable<int> ParticipantIdentifierId { get; set; }
        public string ParticipantCode { get; set; }
        public Nullable<long> ParticipantPortId { get; set; }
    
        public virtual AWB AWB { get; set; }
        public virtual ParticipantIdentifier ParticipantIdentifier { get; set; }
        public virtual Port Port { get; set; }
        public virtual Port Port1 { get; set; }
    }
}
