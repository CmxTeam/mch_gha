//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class Waybill
    {
        public Waybill()
        {
            this.Details = new HashSet<Detail>();
        }
    
        public int WaybillID { get; set; }
        public int ManifestID { get; set; }
        public string Company { get; set; }
        public string Office { get; set; }
        public string Master { get; set; }
        public string MasterIndicator { get; set; }
        public string Client { get; set; }
        public string DepartureAirport { get; set; }
        public string ArrivalAirport { get; set; }
        public string ArrivalTerminal { get; set; }
        public Nullable<System.DateTime> ArrivalDate { get; set; }
        public string Carrier { get; set; }
        public string FlightNo { get; set; }
        public string Agentcode { get; set; }
        public string AgentIataCasscode { get; set; }
        public string AgentName { get; set; }
        public string AgentPlace { get; set; }
        public string SignatoryCarrierName { get; set; }
        public string SignatoryCarrierLocation { get; set; }
        public Nullable<System.DateTime> SignatoryCarrierDateTime { get; set; }
        public string SignatoryConsignorName { get; set; }
        public string CargoMovementType { get; set; }
        public string CargoMovementCountryCode { get; set; }
    
        public virtual ICollection<Detail> Details { get; set; }
        public virtual Manifest Manifest { get; set; }
    }
}
