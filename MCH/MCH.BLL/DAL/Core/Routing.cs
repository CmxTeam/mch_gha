//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MCH.BLL.DAL.Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class Routing
    {
        public int RoutingID { get; set; }
        public int DetailID { get; set; }
        public Nullable<int> ItemNo { get; set; }
        public string DepartAirport { get; set; }
        public string DepartAirportCountry { get; set; }
        public string DepartAirportCity { get; set; }
        public string ArrivalAirport { get; set; }
        public string ArrivalAirportCountry { get; set; }
        public string ArrivalAirportCity { get; set; }
        public string FlightNo { get; set; }
        public string CarrierCode { get; set; }
        public Nullable<System.DateTime> DepartDateTime { get; set; }
        public Nullable<System.DateTime> ArrivalDateTime { get; set; }
        public string DepartTerminal { get; set; }
        public string DepartTerminalName { get; set; }
        public string ArrivalTerminal { get; set; }
        public string ArrivalTerminalName { get; set; }
        public string TransferType { get; set; }
        public string AircraftType { get; set; }
        public string AircraftRegistrationNo { get; set; }
        public string CodeShareFlightNo { get; set; }
        public string CodeShareCarrierCode { get; set; }
        public string IataIcaoCode { get; set; }
        public string StageCode { get; set; }
    
        public virtual Detail Detail { get; set; }
    }
}
