﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Exceptions
{
    public static class ErrorCodes
    {
        public static class ULDErrorCodes
        {
            public static readonly int ULD_NOT_FOUND = 1000;
        }

        public static class PrinterErrorCodes
        {
            public static readonly int PRINTER_NOT_FOUND = 2000;

            public static readonly int LABEL_NOT_FOUND = 2001;
        }

        public static class UserErrorCodes
        {
            public static readonly int USER_NOT_FOUND = 3000;
        }
    }
}
