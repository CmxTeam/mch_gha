﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Exceptions
{
    public class MCHBusinessException:Exception
    {
        public MCHBusinessException():base(){}

        public MCHBusinessException(string message):base(message){}

        public MCHBusinessException(int errorCodes) : base() { }

        public MCHBusinessException(string message, int errorCodes) : base(message) { }

        public MCHBusinessException(string message, Exception innerEx):base(message, innerEx){ }


    }
}
