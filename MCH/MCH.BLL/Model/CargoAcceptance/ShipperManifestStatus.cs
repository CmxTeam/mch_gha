﻿namespace MCH.BLL.Model.CargoAcceptance
{
    public enum ShipperManifestStatus
    {
        ShipperInfoVerified = 35,
        DriverInfoVerified = 36,
        AWBDetailsVerified = 37,
        AWBSpecialHandlingVerified = 38,
        AWBContactInfoVeriried = 39,
        AWBScreeningVerified = 40,
        AWBEntryCompleted = 41,
        DocsFinalized = 42,
        Canceled = 43
    }
}
