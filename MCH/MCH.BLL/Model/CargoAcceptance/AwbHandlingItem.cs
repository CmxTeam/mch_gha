﻿using MCH.BLL.Model.Common;
using System.Collections.Generic;

namespace MCH.BLL.Model.CargoAcceptance
{
    public class AwbHandlingItem
    {
        public long AwbId { get; set; }
        public string SphCodeIds { get; set; }
        public string SphGroupIds { get; set; }
        public List<AttachmentModel> Documents { get; set; }
    }
}
