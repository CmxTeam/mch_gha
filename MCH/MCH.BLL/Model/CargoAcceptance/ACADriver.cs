﻿namespace MCH.BLL.Model.CargoAcceptance
{
    public class ACADriver
    {
        public long ShipperManifestId { get; set; }
        public long? DriverId { get; set; }        
        public long? TruckingCompId { get; set; }
        public string STANumber { get; set; }
        public string DriverLicense { get; set; }
        public int? StateId { get; set; }
        public string CapturePhoto { get; set; }
        public string CaptureID { get; set; }        
        public int? FirstIDTypeId { get; set; }
        public bool? FirstIDMatched { get; set; }
        public int? SecondIDTypeId { get; set; }
        public bool? SecondIDMatched { get; set; }


        public long AccountId { get; set; }
        public string AccountName { get; set; }
        public string ShipperName { get; set; }
        public string ShipperTypeName { get; set; }
    }
}
