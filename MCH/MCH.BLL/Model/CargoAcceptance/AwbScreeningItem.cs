﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.CargoAcceptance
{
    public class AwbScreeningItem
    {
        public long AwbId { get; set; }
        public long ? ApprovedCCSFId { get; set; }
        public bool IsTendered { get; set; }
        public List<SealNumber> SelaNumbers { get; set; }
        public List<SaveAltScreeningOptions> AlernativeScreeningOptions { get; set; }
    }

    public class SealNumber
    {
        public int Id { get; set; }
        public int SealTypeId { get; set; }
        public string SealTypeName { get; set; }        
        public string SealNumberValue { get; set; }
    }

    public class SaveAltScreeningOptions
    {
        public long Id { get; set; }
        public bool Checked { get; set; }
    }
}
