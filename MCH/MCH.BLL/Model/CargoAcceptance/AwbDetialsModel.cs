﻿using System;

namespace MCH.BLL.Model.CargoAcceptance
{
    public class AwbDetialsModel
    {
        public long Id { get; set; }        
        public string CarrierCode { get; set; }        
        public string AwbSerialNumber { get; set; }
        public string FlightNumber { get; set; }
        public DateTime? FlightDate { get; set; }
        public long? OriginId { get; set; }
        public long? DestinationId { get; set; }
        public int? Pieces { get; set; }        
        public double? Weight { get; set; }
        public string WeightUom { get; set; }
        public long? WeightUomId { get; set; }
        public int? FlightTypeId { get; set; }

        public bool ConnectedToShipper { get; set; }

    }
}
