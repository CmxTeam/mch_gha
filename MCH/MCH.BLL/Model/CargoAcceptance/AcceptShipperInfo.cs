﻿using MCH.BLL.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.CargoAcceptance
{
    public class AcceptShipperInfo
    {
        public long UserId { get; set; }
        public long WarehouseId { get; set; }
        public int AccountId { get; set; }        
        public int AircraftTypeId { get; set; }        
        public int? ShipperTypeId { get; set; }
        public int? ShipperId { get; set; }           
        public string ShipperName { get; set; }
        public bool ShipperStatusVerified { get; set; }        
        public bool IsDocumentVerified { get; set; }
        public int? GovCredTypeId { get; set; }
        public string ShipperConsentSignature { get; set; }        
        public string Documents { get; set; }
        public List<AttachmentModel> Docs { get; set; }
    }
}
