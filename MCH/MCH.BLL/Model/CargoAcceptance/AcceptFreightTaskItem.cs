﻿using System;
using System.Collections.Generic;

namespace MCH.BLL.Model.CargoAcceptance
{
    public class AcceptFreightTaskItem
    {
        public bool IsSealVerified { get; set; }
        public string SealNumber { get; set; }
        public bool CompleteOffLoad { get; set; }
        public bool IsOffLoaded { get; set; }
        public long TaskId { get; set; }
        public Byte[] DriverImageThumbnail { get; set; }
        public string DriverName { get; set; }
        public string DriverCompany { get; set; }
        public string Shipper { get; set; }
        public int Awbs { get; set; }
        public int Pcs { get; set; }
        public string Locations { get; set; }
        public DateTime Date { get; set; }
        public Double Progress { get; set; }
        public string CarrierCode { get; set; }
        public List<string> Reference { get; set; }
        
    }
}
