﻿namespace MCH.BLL.Model.CargoAcceptance
{
    public class WizardHeader
    {       
        public long CarrierId { get; set; }
        public string CarrierName { get; set; }
        public string ShipperTypeName { get; set; }
        public string ShipperName { get; set; }
        public string DriverName { get; set; }
        public string CapturePhoto { get; set; }
        public string TrackingCompany { get; set; }
        public ShipperManifestStatus Status { get; set; }
        public string CarrierCode { get; set; }
        public int FlightTypeId { get; set; }
        public long? CurrentAwbId { get; set; }
    }
}
