﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.AwbWizard
{
    public class AwbUldsDimensions
    {
        public long AwbId { get; set; }
        public long? VolumeUOMId { get; set; }
        public double? Volume { get; set; }
        public int? Slac { get; set; }
        public List<GoodDescriptionModel> GoodsDescriptions { get; set; }
        public List<UnitLoadDeviceModel> UnitLoadDevicess { get; set; }
    }
}