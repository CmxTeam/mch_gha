﻿namespace MCH.BLL.Model.AwbWizard
{
    public class ChargeModel
    {        
        public long? Id { get; set; }
        public long? CodeId { get; set; }        
        public bool IsCodeDueCarrier { get; set; }        
        public decimal? Amount { get; set; }
    }
}