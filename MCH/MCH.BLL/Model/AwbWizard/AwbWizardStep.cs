﻿namespace MCH.BLL.Model.AwbWizard
{
    public enum AwbWizardStep
    {
        DetailsCompleted = 44,
        ChargesCompleted = 45,
        //ULDsDimsCompleted = 46,
        //SummaryCompleted = 47,
        HWBSEntryInProgress = 48,
        EntryCompleted = 49
    }
}