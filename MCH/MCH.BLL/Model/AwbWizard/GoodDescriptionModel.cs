﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.AwbWizard
{
    public class GoodDescriptionModel
    {
        public long Id { get; set; }        
        public int ? Pieces { get; set; }
        public double? Weight { get; set; }
        public int? WeightUOMId { get; set; }        
        public double ? Length { get; set; }
        public double ? Width { get; set; }
        public double ? Height { get; set; }
        public int ? DimUOMId { get; set; }        
    }
}