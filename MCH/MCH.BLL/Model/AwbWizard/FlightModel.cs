﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.AwbWizard
{
    public class FlightModel
    {
        public long Id { get; set; }
        public long? FlightFromId { get; set; }        
        public long? FlightToId { get; set; }        
        public long? CarrierId { get; set; }        
        public string FlightNo { get; set; }
        public DateTime? FlightDate { get; set; }
    }
}