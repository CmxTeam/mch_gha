﻿using MCH.BLL.Model.Common;
using MCH.BLL.Model.Customs;
using MCH.BLL.Model.SpecialHandling;
using MCH.Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.AwbWizard
{
    public class AddEditHwbModel
    {
        public long? Id { get; set; }

        public long? AirwaybillId { get; set; }

        public string AWBNumber { get; set; }

        public long? OriginId { get; set; }

        public long? DestinationId { get; set; }

        public string HWBSerialNumber { get; set; }


        public long? TotalPieces { get; set; }
        public decimal? TotalWeight { get; set; }
        public long? UOMWeightId { get; set; }
        public long? SLAC { get; set; }

        public string DescriptionOfGoods { get; set; }


        public List<CustomsInfo> OtherCustomsInfo { get; set; }
        public string ExportControlNo { get; set; }
        public PartnerModel Shipper { get; set; }
        public PartnerModel Consignee { get; set; }
        public List<SphCode> HarmonizedCommodityCodes { get; set; }

        //public List<SphCode> SpecialHandlingRequests { get; set; }
        public ChargeDeclarationModel ChargeDeclarations { get; set; }
    }

    
}
