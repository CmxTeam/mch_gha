﻿namespace MCH.BLL.Model.AwbWizard
{
    public class UnitLoadDeviceModel
    {
        public long Id { get; set; }                
        public string ULDNo { get; set; }
        public int ? ULDTypeId { get; set; }        
        public int ? OwnerId { get; set; }       
        public long ? Said { get; set; }
    }
}