﻿namespace MCH.BLL.Model.AwbWizard
{
    public class ChargeDeclarationsModel
    {
        public long? Id { get; set; }
        public long? CurrencyId { get; set; }

        public int? WTVALChargesType { get; set; }

        public int? OtherChargesType { get; set; }

        public decimal? DeclaredValueForCarrige { get; set; }

        public bool NVD { get; set; }

        public decimal? DeclaredValueForCustoms { get; set; }

        public bool NCV { get; set; }

        public decimal? InsuranceAmount { get; set; }

        public bool XXX { get; set; }
    }
}