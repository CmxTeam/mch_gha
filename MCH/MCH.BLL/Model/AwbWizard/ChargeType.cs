﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.AwbWizard
{
    public class ChargeType
    {       
        public decimal? Prepaid { get; set; }
        public decimal? Collect { get; set; }
    }
}
