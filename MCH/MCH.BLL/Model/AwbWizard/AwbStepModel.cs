﻿using MCH.BLL.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.AwbWizard
{
    public class AwbStepModel
    {
        public long? Id { get; set; }
        public int? AwbTypeId { get; set; }
        public string AwbSerialNumber { get; set; }
        public long? OriginId { get; set; }
        public long? DestinationId { get; set; }
        public int AircraftType { get; set; }
        public int? CarrierId { get; set; }
        public List<FlightModel> Flights { get; set; }
        public string GoodsDescription { get; set; }
        public string SpecialServiceRequests { get; set; }
        public string OtherServiceRequests { get; set; }
        public CustomerModel Shipper { get; set; }
        public CustomerModel Consignee { get; set; }
        public AgentModel IssuingCarriersAgent { get; set; }
        public string AccountInformationText { get; set; }
        public IEnumerable<SenderReferenceModel> SenderReferences { get; set; }
        
    }
}