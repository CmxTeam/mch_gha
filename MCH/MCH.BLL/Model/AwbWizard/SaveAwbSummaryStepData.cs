﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.AwbWizard
{
    public class AwbSummaryStepData
    {
        public long  ? AwbId { get; set; }

        public long? AirportId { get; set; }

        public string CertificationName { get; set; }

        public string ExecutedAtPlace { get; set; }

        public string ExecutedBy { get; set; }

        public DateTime ? ExecutedOnDate { get; set; }

        public string ParticipantCode { get; set; }

        public int? ParticipantIdentfierId { get; set; }

        public long UserId { get; set; }

        public long? WarehouseId { get; set; }
        public long AwbTypeId { get; set; }

        public int? AirwaybillType { get; set; }
    }
}
