﻿namespace MCH.BLL.Model.AwbWizard
{
    public class CodeModel
    {
        public long Id { get; set; }
        public long? CodeId { get; set; }
        public string CodeNote { get; set; }
    }
}