﻿using System.Collections.Generic;
namespace MCH.BLL.Model.AwbWizard
{
    public class RateDescriptionModel
    {
        public long? Id { get; set; }
        public long? NoOfPieces { get; set; }        
        public double? GrossWeight { get; set; }
        public int? UOMWeightId { get; set; }        
        public string RateClass { get; set; }        
        public string CommodityItemNo { get; set; }
        public double? ChargeableWeight { get; set; }
        public decimal? RateCharge { get; set; }        
        public decimal? Total { get; set; }


        public string GoodsDescription { get; set; }
    }
}