﻿using MCH.BLL.Model.SpecialHandling;
using System;
using System.Collections.Generic;

namespace MCH.BLL.Model.AwbWizard
{
    public class AwbChargeDeclaration
    {
        public long? AwbId { get; set; }
        public ChargeDeclarationsModel ChargeDeclarations { get; set; }
        public ChargeType TotalWeightCharges { get; set; }
        public ChargeType TotalValuationCharges { get; set; }
        public ChargeType TotalTaxes { get; set; }
        public ChargeType TotalOtherChargesDueCarrier { get; set; }
        public ChargeType TotalOtherChargesDueAgent { get; set; }
        public ChargeType Total { get; set; }
        public List<ChargeModel> OtherChargesList { get; set; }
        public List<RateDescriptionModel> RateDescriptionsList { get; set; }
        public List<SphCode> SpecialHandlingRequests { get; set; }

        public string HandlingInformation { get; set; }
        public int? AirwaybillType { get; set; }
        public string CertificationName { get; set; }        
        public string ExecutedAtPlace { get; set; }
        public DateTime? ExecutedOnDate { get; set; }
        public string ExecutedBy { get; set; }
    }
}