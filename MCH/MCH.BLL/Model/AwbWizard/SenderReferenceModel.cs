﻿namespace MCH.BLL.Model.AwbWizard
{
    public class SenderReferenceModel
    {
        public long Id { get; set; }
        public long AwbId { get; set; }
        public string FileReference { get; set; }
        public int? ParticipantIdentifierId { get; set; }
        public string ParticipantCode { get; set; }
        public long? AirportId { get; set; }
    }
}