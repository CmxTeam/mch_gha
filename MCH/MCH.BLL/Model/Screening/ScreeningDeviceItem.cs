﻿namespace MCH.BLL.Model.Screening
{
    public class ScreeningDeviceItem
    {
        public ScreeningDeviceTypes DeviceType { get; set; }
        public long DeviceId { get; set; }
        public string SerialNumber { get; set; }
        public long? UserId { get; set; }
        public bool IsActive { get; set; } 
    }
}
