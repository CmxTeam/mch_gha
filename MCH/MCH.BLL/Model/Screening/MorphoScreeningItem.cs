﻿namespace MCH.BLL.Model.Screening
{
    public class MorphoScreeningItem
    {
        public ScreeningDeviceItem DeviceInfo { get; set; }
        public string SubstancesFound { get; set; }
        public string FileName { get; set; }
        public string SystemMessage { get; set; }
        public string ScreeningResults { get; set; }
    }
}
