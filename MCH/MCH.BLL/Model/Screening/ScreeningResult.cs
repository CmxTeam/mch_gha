﻿namespace MCH.BLL.Model.Screening
{
    public enum ScreeningResult
    {
        PASS = 0,
        ALARM = 1,
        SUSPECT = 2
    }
}
