﻿namespace MCH.BLL.Model.Screening
{
    public class CCSFItem
    {
        public long CCSFId { get; set; }
        public string CertificationNumber { get; set; }
    }
}
