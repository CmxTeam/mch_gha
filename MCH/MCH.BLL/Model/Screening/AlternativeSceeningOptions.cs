﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Screening
{
   public class AlternativeSceeningOption
    {
       public long Id { get; set; }
       public long? GroupId { get; set; }
       public string Name { get; set; }
       public bool Checked { get; set; }
       public int AircraftTypeId { get; set; }

    }
}
