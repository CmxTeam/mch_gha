﻿namespace MCH.BLL.Model.Screening
{
    public class ScreeningInspectionRemark
    {
        public long Id { get; set; }
        public string Remark { get; set; }
    }
}
