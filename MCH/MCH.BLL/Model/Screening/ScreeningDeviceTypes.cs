﻿namespace MCH.BLL.Model.Screening
{
    public enum ScreeningDeviceTypes
    {
        ETD=0,
        XRAY = 1,
        CANINE = 2,
        PHYSICAL = 3,
        CUSTOMER = 4,
    }
}