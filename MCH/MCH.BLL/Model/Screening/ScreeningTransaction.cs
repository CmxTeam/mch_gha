﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Screening
{
    public class ScreeningTransaction
    {
        public long UserId { get; set; }
        public long TaskId { get; set; }
        public int Pieces { get; set; }
        public ScreeningDeviceItem Data { get; set; }
        public ScreeningResult Result { get; set; }
        public string SubstancesFound { get; set; }
        public string SampleNumber { get; set; }
        public long? PrescreenedCCSFId { get; set; }
        public string Comment { get; set; }
    }
}
