﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Screening
{
    public class ScreeningTaskItem
    {
        public long TaskId { get; set; }
        public string Message { get; set; }
        public long? Awb { get; set; }
        public long? Hwb { get; set; }
        public string ShipmentReference { get; set; }
        public int TotalPieces { get; set; }
        public int ScreenedPieces { get; set; }
        public bool HasAlarm { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }        
    }
}