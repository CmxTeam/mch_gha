﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Screening
{
    public class DtoAwbAlternativeScreenings
    {
        public List<AlternativeSceeningOption> ScreeningOptions {get;set;}
    }
}
