﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Tasks
{
    public class TaskCategory
    {
        public string CategoryName { get; set; }
        public long CategoryId { get; set; }
        public bool IsDefault { get; set; }
    }
}
