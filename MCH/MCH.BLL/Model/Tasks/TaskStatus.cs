﻿namespace MCH.BLL.Model.Tasks
{
    public class TaskStatus
    {
        public long TaskId {get; set;}
        public int Status {get; set;}
        public string Message { get; set; }
    }
}
