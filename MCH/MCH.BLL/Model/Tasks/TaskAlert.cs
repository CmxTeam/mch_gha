﻿using System;

namespace MCH.BLL.Model.Tasks
{
    public class TaskAlert
    {
        public string AlertMessage { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; }
    }
}
