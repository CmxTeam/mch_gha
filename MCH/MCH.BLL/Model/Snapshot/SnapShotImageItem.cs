﻿using System.Collections.Generic;

namespace MCH.BLL.Model.Snapshot
{
    public class SnapShotImageItem
    {
        public long SnapshotTaskId { get; set; }
        public long UserId { get; set; }
        public int PieceCount { get; set; }
        public string SnapshotImage { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public List<SnapShotImageConditionItem> SnapShotImageConditionItems { get; set; }
    }
}
