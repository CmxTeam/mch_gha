﻿namespace MCH.BLL.Model.Snapshot
{
    public class SnapShotTask
    {
        public long SnapShotTaskId { get; set; }
        public string Message { get; set; }
        public string SnapShotReference { get; set; }
    }
}
