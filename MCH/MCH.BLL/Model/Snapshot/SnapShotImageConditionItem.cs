﻿namespace MCH.BLL.Model.Snapshot
{
    public class SnapShotImageConditionItem
    {
        public long ConditionId { get; set; }
        public decimal X { get; set; }
        public decimal Y { get; set; }
    }
}
