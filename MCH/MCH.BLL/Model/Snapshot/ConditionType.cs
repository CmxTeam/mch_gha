﻿namespace MCH.BLL.Model.Snapshot
{
    public class ConditionType
    {
        public int ConditionId { get; set; }
        public string Condition { get; set; }
        public string IconName { get; set; }
        public bool IsDamage { get; set; }
        public bool IsDefault { get; set; }
    }
}
