﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Common
{
    public class AgentModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }        
        public string AccountNumber { get; set; }
        public string City { get; set; }
        public string IataCode { get; set; }
        public string CassAddress { get; set; }
        public int? ParticipantIdentfierId { get; set; }
    }
}
