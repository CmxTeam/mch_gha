﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Common
{
    public class SimpleListItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
