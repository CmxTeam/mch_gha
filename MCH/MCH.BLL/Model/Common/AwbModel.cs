﻿using System;

namespace MCH.BLL.Model.Common
{
    public class AwbModel
    {
        public long? Id { get; set; }
        public int CarrierId { get; set; }
        public string CarrierNumber { get; set; }
        public string AWBNumber { get; set; }
        public long OriginId { get; set; }
        public string Origin { get; set; }
        public long DestinationId { get; set; }
        public string Destination { get; set; }
        public int TotalPieces { get; set;}
        public double TotalWeight { get; set; }
        public int WeightUOMId { get; set; }
        public string WeightUOM { get; set; }
        public string FlightNumber { get; set; }
        public DateTime? FlightDate { get; set; }

        public bool ConnectedToShipper { get; set; }
    }
}
