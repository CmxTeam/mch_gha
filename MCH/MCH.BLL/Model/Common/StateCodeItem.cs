﻿namespace MCH.BLL.Model.Common
{
    public class StateCodeItem
    {
        public long Id{get; set;} 
        public string StateProvince{get; set;}
        public string TwoCharacterCode { get; set; } 
    }
}
