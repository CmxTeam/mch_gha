﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Common
{
    public class MenuItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string ChildItems { get; set; }
        public int Order { get; set; }
        public string IconKey { get; set; }
        public string NavigationPath { get; set; }
        public string IconBase64 { get; set; }
        public bool IsDefault { get; set; }
        public string MenuParam { get; set; }
        public List<long> Restrictions { get; set; }
        public long CategoryId { get; set; }
        public int OpenAssignedTaskCount { get; set; }
    }
}
