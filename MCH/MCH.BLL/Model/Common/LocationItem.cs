﻿namespace MCH.BLL.Model.Common
{
    public class LocationItem
    {
        public string Location { get; set; }
        public string LocationBarcode { get; set; }
        public long LocationId { get; set; }
        public string LocationPrefix { get; set; }
    }
}
