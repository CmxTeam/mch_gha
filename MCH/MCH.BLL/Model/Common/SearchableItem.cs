﻿namespace MCH.BLL.Model.Common
{
    public class SearchableItem
    {
        public long Id { get; set; }
        public string SearchText { get; set; }
    }
}
