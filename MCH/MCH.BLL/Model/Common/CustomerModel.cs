﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Common
{
    public class CustomerModel
    {
        public int? Id { get; set; }        
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public string Postal { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public string StateText { get; set; }

        public string CountryText { get; set; }


    }
}
