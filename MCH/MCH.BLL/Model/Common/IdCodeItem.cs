﻿namespace MCH.BLL.Model.Common
{
    public class IdCodeItem
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }
    }
}