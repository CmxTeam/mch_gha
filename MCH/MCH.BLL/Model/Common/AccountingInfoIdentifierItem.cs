﻿namespace MCH.BLL.Model.Common
{
    public class AccountingInfoIdentifierItem
    {
        public long Id { get; set; }
        public string Code { get; set; }
    }
}
