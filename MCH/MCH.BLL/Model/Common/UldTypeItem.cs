﻿namespace MCH.BLL.Model.Common
{
    public class UldTypeItem
    {
        public long UldTypeId { get; set; }
        public string UldType { get; set; }
    }
}
