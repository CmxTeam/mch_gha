﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Common
{
    public class DriverModel
    {
        public long ? Id { get; set; }
        public string FullName { get; set; }
        public string STANumber { get; set; }
        public string DriverLicense { get; set; }
        public int? StateId { get; set; }
    }
}
