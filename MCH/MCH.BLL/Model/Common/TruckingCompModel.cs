﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Common
{
    public class TruckingCompModel
    {
        public long? Id { get; set; }
        public string TruckingCompanyName { get; set; }
    }
}
