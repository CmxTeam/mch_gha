﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Common
{
    public class ChargeCodeModel
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public bool IsCodeDueCarrier { get; set; }
        public string Description { get; set; }
    }
}
