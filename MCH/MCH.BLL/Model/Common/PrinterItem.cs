﻿namespace MCH.BLL.Model.Common
{
    public class PrinterItem
    {
        public string PrinterName { get; set; }
        public int PrinterId { get; set; }
        public bool IsDefault { get; set; }
        public PrinterTypes PrinterType { get; set; }
    }
}