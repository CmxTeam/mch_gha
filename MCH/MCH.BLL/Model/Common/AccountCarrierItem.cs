﻿namespace MCH.BLL.Model.Common
{
    public class AccountCarrierItem
    {
        public long AccountId { get; set; }
        public long CarrierId { get; set; }        
        public string CarrierCode { get; set; }
        public string Carrier3Code { get; set; }
        public string CarrierName { get; set; }
    }
}
