﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Common
{
    public class ChargeDeclarationModel
    {
        public long? Id { get; set; }
        public long? CurrencyId { get; set; }

        //  public long? WTVALCharges { get; set; }

        public int? WTVALChargesType { get; set; }

        //  public long? OtherCharges { get; set; }

        public int? OtherChargesType { get; set; }

        public decimal? DeclaredValueForCarrige { get; set; }

        public bool NVD { get; set; }

        public decimal? DeclaredValueForCustoms { get; set; }

        public bool NCV { get; set; }

        public decimal? InsuranceAmount { get; set; }

        public bool XXX { get; set; }
    }
}
