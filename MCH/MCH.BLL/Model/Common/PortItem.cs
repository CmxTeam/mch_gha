﻿namespace MCH.BLL.Model.Common
{
    public class PortItem
    {
        public long Id {get; set;} 
        public string IATACode{get; set;}  
        public string Port {get; set;} 
    }
}
