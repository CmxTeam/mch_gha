﻿namespace MCH.BLL.Model.Common
{
    public class CarrierItem
    {
        public long Id { get; set; }
        public string CarrierCode { get; set; }
        public string CarrierName { get; set; }
        public string Carrier3Code { get; set; }
    }
}
