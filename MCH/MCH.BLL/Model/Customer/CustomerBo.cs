﻿using MCH.BLL.Model.Common;
using MCH.Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Customer
{
    public class CustomerBo
    {
        public int? Id { get; set; }

        public enumPartnerTypes Type { get; set; }
        public string Name { get; set; }

        public string AccountNumber { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }

        public string Postal { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }
}
