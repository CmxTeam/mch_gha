﻿namespace MCH.BLL.Model.Relocate
{
    public enum ReferenceTypes
    {
        AWB = 2,
        HWB = 3,
        ULD = 4
    }
    public class RelocateTaskItem
    {
        public long? RelocateTaskId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Reference { get; set; }
        public long? ReferenceId { get; set; }
        public ReferenceTypes ReferenceType { get; set; }
        public int? Skids { get; set; }
        public int? TotalPieces { get; set; }
        public int? ScannedPieces { get; set; }
        public string Locations { get; set; }
    }

}
