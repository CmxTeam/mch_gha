﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Forklift
{
    public class ForkliftModel
    {
        public long UserId { get; set; }
        public long TaskTypeId { get; set; }
        public long TaskId { get; set; }
    }
}
