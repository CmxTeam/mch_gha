﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Inventory
{
    public class InventoryShipmentItem
    {
        public long AwbId { get; set; }
        public string Awb { get; set; }
        public long HwbId { get; set; }
        public string Hwb { get; set; }
        public int TotalPieces { get; set; }
        public int ScannedPieces { get; set; }
        public string Locations { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public double Weight { get; set; }
        public string WeightUOM { get; set; }
    }
}
