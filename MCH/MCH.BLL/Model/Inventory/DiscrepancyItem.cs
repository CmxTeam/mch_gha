﻿namespace MCH.BLL.Model.Inventory
{
    public class DiscrepancyItem
    {
        public long AwbId { get; set; }
        public string Awb { get; set; }
        public long HwbId { get; set; }
        public string Hwb { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public int TotalPieces { get; set; }
        public int ScannedPieces { get; set; }
        public string DiscrepancyType { get; set; }
        public int DiscrepancyCount { get; set; }
    }
}