﻿using MCH.BLL.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MCH.BLL.Model.Inventory
{
    public class InventoryLocationViewItem
    {
        public int ScannedPieces {get;set;}
        public LocationItem Location { get; set; }
    }
}
