﻿using System;
namespace MCH.BLL.Model.Customs
{
    public class CheckListShipmentItem
    {
        public CheckListStatusTypes Status { get; set; }

        public long TaskId { get; set; }

        public long? AwbId { get; set; }

        public string AwbNumber { get; set; }

        public string CarrierCode { get; set; }

        public string FlightNumber { get; set; }

        public DateTime? ETD { get; set; }

        public int Pieces { get; set; }

        public string Locations { get; set; }

        public double Progress { get; set; }

        public override string ToString()
        {
            return AwbNumber;
        }
    }
}
