﻿using System;
using System.Collections.Generic;

namespace MCH.BLL.Model.Customs
{
    public class CheckListHeader
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<CheckListItem> Items { get; set; }
    }

    public class CheckListItem
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public long? ParentId { get; set; }
        public string Options { get; set; }
        public IEnumerable<CheckListItem> SubItems { get; set; }
        public string SelectedOption { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string Comment { get; set; }
        public int? Number { get; set; }
    }

    public class CheckListModel
    {
        public int? AccountId { get; set; }
        public int EntityTypeId { get; set; }
        public long EntityId { get; set; }
        public string Username { get; set; }
        public string Place { get; set; }
        public DateTime? CommentedOn { get; set; }
        public string Comments { get; set; }
        public long? TaskId { get; set; }
        public string ShipmentNumber { get; set; }
        public string SignatureData { get; set; }
        public string Year { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }        
        public List<CheckListHeader> CheckList { get; set; }
    }
}
