﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Customs
{
    public class CustomsInfo
    {
        public long? Id { get; set; }
        public long? IdToDB { get; set; }
        public string CountryCodeId { get; set; }

        public string InfoIdentifierId { get; set; }

        public string CustomsInfoIdentifierId { get; set; }


        public string CountryCode { get; set; }

        public string InfoIdentifier { get; set; }

        public string CustomsInfoIdentifier { get; set; }

        public string SupplementaryCustomsInfo { get; set; }
    }
}
