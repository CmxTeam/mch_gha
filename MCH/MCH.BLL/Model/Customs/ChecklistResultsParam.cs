﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.Customs
{
    public class ChecklistResultsParam
    {
        public long EntityId { get; set; }
        public string SignatureData { get; set; }
        public long TaskId { get; set; }
        public string Comments { get; set; }
        public IEnumerable<CheckListResultItem> Items { get; set; }
        public long UserId { get; set; }
    }

    public class CheckListResultItem
    {
        public int ItemId { get; set; }
        public string OptionName { get; set; }
    }
}