﻿namespace MCH.BLL.Model.Customs
{
    public enum CheckListStatusTypes
    {
        Open = 0,
        Pending = 1,
        InProgress = 2,
        Completed = 3,
    }
}