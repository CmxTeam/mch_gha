﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Customs
{
    public class ChecklistAnswers
    {

        public long EntityId { get; set; }
        public string SignatureData { get; set; }
        public long TaskId { get; set; }
        public string Comments { get; set; }
        public IEnumerable<CheckListItemAnswer> Items { get; set; }
    }

    public class CheckListItemAnswer
    {
        public int ItemId { get; set; }
        public string OptionName { get; set; }
    }
}
