﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.SpecialHandling
{
    public class DtoVerifyField
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string TypeName { get; set; }
        public bool IsRequired { get; set; }
        public string Value { get; set; }
    }

    public class DtoSphGroupFields
    {
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public string GroupCode { get; set; }
        public IEnumerable<DtoVerifyField> Fields { get; set; }
    }
}
