﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.SpecialHandling
{
    public class SphCode
    {
        public long Id { get; set; }
        public long? CodeId { get; set; }
        public string CodeNote { get; set; }
    }
}
