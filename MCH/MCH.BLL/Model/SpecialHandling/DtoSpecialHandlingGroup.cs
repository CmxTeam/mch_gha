﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.SpecialHandling
{
    public class DtoSpecialHandlingGroup
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long ? SphCodeId { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public List<DtoSpecialHandlingGroup> SubGroups { get; set; }
    }
}
