﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Model.Enum
{
    public enum enumTaskTypes : int
    {
        CargoReceiver = 1,
        CargoInventory = 2,
        CargoDischarge = 3,
        CargoSnapshot = 4,
        CargoLoader = 5,
        CargoTender = 6,
        CargoScreener = 7,
        DGCheckList = 8,
        CargoAccpetWarehouse = 9,
        LiveAnimal = 10,
        HighValue = 11,
        HumanRemains =12,
        CargoAcceptOffice = 13
    }
}
