﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Model.Enum
{
    public enum enumEntityTypes : int
    {
        FlightManifest = 1,
        AWB = 2,
        HWB = 3,
        ULD = 4,
        Task = 6,
        Discharge = 10
    }
}
