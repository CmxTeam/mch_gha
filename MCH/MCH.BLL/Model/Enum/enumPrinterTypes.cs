﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Enum
{
    public enum enumPrinterTypes:int
    {
        Document = 1,
        Label = 2
    }
}
