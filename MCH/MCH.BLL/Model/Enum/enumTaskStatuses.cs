﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Model.Enum
{
    public enum enumTaskStatuses : int
    {
        NotAssigned = 11,
        NotStarted = 12,
        InProgress = 13,
        Completed = 14,
        Reopened = 15,
        Disabled = 16,
        TaskAssigned = 17,
        TaskUnAssigned = 18,
        TaskUpdated = 19
    }
}
