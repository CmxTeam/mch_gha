﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Model.Enum
{
    public enum enumAttributes : int
    {
        HAZMAT = 1,
        PERISHABLES = 2,
        DRYICE = 3,
        ENGINE = 4,
        OVERSIZED = 5,
        EXPEDITED = 6,
        VEHICLE = 7,
        HIGHVALUE = 8,
        LIVEANIMALS = 9,
        HUMANREMAINS = 10,
        LICENSE = 11,
        OTHERHANDLING = 12,
    }
}
