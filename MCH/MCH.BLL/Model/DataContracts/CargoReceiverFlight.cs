﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MCH.BLL.Model.DataContracts
{        
    public class CargoReceiverFlight
    {
        public string CarrierLogo { get; set; }
        public string CarrierCode { get; set; }
        [XmlElement(IsNullable = true)]
        public string CarrierName { get; set; }
        public string FlightNumber { get; set; }
        public DateTime? ETA { get; set; } 
        public string Origin { get; set; }
        public int ULDCount { get; set; }
        public int AWBCount { get; set; }
        public int TotalPieces { get; set; }
        public long TaskId { get; set; }
        public long FlightManifestId { get; set; }
        [XmlElement(IsNullable = true)]
        public string RecoverLocation { get; set; }
        public string RecoveredBy { get; set; }
        public DateTime? RecoveredDate { get; set; }
        public int RecoveredULDs { get; set; }
        public int ReceivedPieces { get; set; }
        public Int64 Flag { get; set; }
        public TaskStatuses Status { get; set; }
        public string[] References { get; set; }
    }
}