﻿namespace MCH.BLL.Model.DataContracts
{
    public enum ReceiverSortFields
    {
        Carrier = 1,
        FlightNo,
        Destination
    }
}