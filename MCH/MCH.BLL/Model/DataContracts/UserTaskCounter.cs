﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.DataContracts
{
    [DataContract]
    public class UserTaskCounter
    {
        [DataMember]
        public int TaskTypeId { get; set; }

        [DataMember]
        public int OpenAssignedTaskCount { get; set; }

    }
}
