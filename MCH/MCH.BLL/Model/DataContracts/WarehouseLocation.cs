﻿namespace MCH.BLL.Model.DataContracts
{
    public class WarehouseLocationModel
    {
        public long LocationId { get; set; }
        public string Location { get; set; }
        public string LocationBarcode { get; set; }
        public LocationType LocationType { get; set; }
    }
}