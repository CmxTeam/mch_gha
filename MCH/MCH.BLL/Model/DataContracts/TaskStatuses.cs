﻿using System;
namespace MCH.BLL.Model.DataContracts
{
    public enum TaskStatuses
    {
        All = 1,
        Completed,
        InProgress, 
        Open, 
        Pending
    }
}