﻿namespace MCH.BLL.Model.DataContracts
{
    public class UldType
    {
        public long Id { get; set; }
        public string Code { get; set; }
    }
}