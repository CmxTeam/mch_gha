﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class CargoLoaderFlight
    {
        public long TaskId { get; set; }
        public long FlightManifestId { get; set; }
        public string CarrierCode { get; set; }
        public string CarrierName { get; set; }
        public string CarrierLogo { get; set; }
        public string FlightNumber { get; set; }
        public DateTime? ETD { get; set; }
        public DateTime? CutOfTime { get; set; }
        public string Origin { get; set; }
        public double Progress { get; set; }
        public int TotalAwbs { get; set; }
        public int TotalSTC { get; set; }
        public int TotalPcs { get; set; }
        public int TotalUlds { get; set; }
        public int TotalLoose { get; set; }
        public int LoadedAwbs { get; set; }
        public int LoadedSTC { get; set; }
        public int LoadedPcs { get; set; }
        public int LoadedUlds { get; set; }
        public int LoadedLoose { get; set; }
        public long Flag { get; set; }
        public TaskStatuses Status { get; set; }
        public string[] Destinations { get; set; }
        public string Location { get; set; }
    }
}