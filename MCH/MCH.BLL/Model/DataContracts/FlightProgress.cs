﻿namespace MCH.BLL.Model.DataContracts
{
    public class FlightProgress
    {
        public int TotalPieces { get; set; }
        public int ReceivedPieces { get; set; }
        public double Progress { get; set; }
    }
}