﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.DataContracts
{
    [DataContract]
    public class WarehouseSimpleModel
    {
        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public int? ExternalId { get; set; }
    }
}
