﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class UldView : ForkliftView
    {
        public string[] References { get; set; }
        public long Flag { get; set; }        
    }

    public class UldExtendedView : UldView
    {
        public long? ManifestId { get; set; }
        public string Flight { get; set; }
        public System.DateTime? ETA { get; set; }
    }
}