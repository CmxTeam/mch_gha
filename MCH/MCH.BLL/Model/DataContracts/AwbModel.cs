﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCH.BLL.Model.DataContracts
{
    public class AwbModel
    {
        public long AWBID { get; set; }
        public string AWBNumber { get; set; }
        public int TotalPieces { get; set; }
        public int TotalSTC { get; set; }
        public string LoadingIndicator { get; set; }
        public int LoadPcs { get; set; }
        public int ForkliftPcs { get; set; }
        public int LoadedPcs { get; set; }
        public double Weight { get; set; }
        public string WeightUOM { get; set; }
        public long Flags { get; set; }
        public string Locations { get; set; }
        public TaskStatuses Status { get; set; }
        public int AvailablePcs { get; set; }
    }
}