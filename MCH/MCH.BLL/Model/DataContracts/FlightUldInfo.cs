﻿using System.Collections.Generic;
namespace MCH.BLL.Model.DataContracts
{
    public class FlightUldInfo
    {
        public long FlightManifestId { get; set; }
        public int TotalUlds { get; set; }
        public int RecoveredUlds { get; set; }
        public double PercentRecovered { get; set; }
        public Uld[] Ulds { get; set; }
        public int TotalPieces { get; set; }
        public int PutAwayPieces { get; set; }
        public double PercentPutAway { get; set; }
    }

    public class FlightUldInfoExtended : FlightUldInfo
    {
        public long TaskId { get; set; }
        public long ManifestId { get; set; }
        public string RecoveredLocation { get; set; }
    }
}