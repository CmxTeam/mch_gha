﻿using MCH.BLL.Model.Enum;
using System;

namespace MCH.BLL.Model.Discharge
{
    public class CargoDischargeTaskItem
    {
        public string Company { get; set; }
        public string Driver { get; set; }
        public string DriverPhoto { get; set; }
        public int? TotalAwbs { get; set; }
        public int? TotalPieces { get; set; }
        public int? ScannedAwbs { get; set; }
        public int? ScannedPieces { get; set; }
        public long TaskId { get; set; }
        public double? PercentProgress { get; set; }
        public long? Flags { get; set; }
        public string Locations { get; set; }
        public enumCargoDischargeStatusTypes Status { get; set; }
        
        public override string ToString()
        {
            return string.Format("{0} {1}", Company, Driver);
        }

    }
}
