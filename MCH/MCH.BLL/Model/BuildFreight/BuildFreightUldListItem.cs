﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCH.BLL.Model.Enum;

namespace MCH.BLL.Model.BuildFreight
{
    public class BuildFreightUldListItem
    {
        public long UldId { get; set; }
        public string UldType { get; set; }
        public string Uld { get; set; }
        public string UldSerialNo { get; set; }
        public string UldPrefix { get; set; }
        public string CarrierCode { get; set; }
        public string Locations { get; set; }
        public bool? IsBUP { get; set; }
        public int TotalPieces { get; set; }
        public int? TotalAwbs { get; set; }
        public double Weight { get; set; }
        public double TareWeight { get; set; }
        public string WeightUOM { get; set; }
        public string StageLocation { get; set; }
        public string StagedBy { get; set; }
        public DateTime? StagedDate { get; set; }
        public long Flag { get; set; }
        public enumBuildFreightStatusTypes Status { get; set; }
    }
}
