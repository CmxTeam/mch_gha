﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.BuildFreight
{
    public class BuildUldViewItem
    {
        public long DetailId { get; set; }
        public long? AwbId { get; set; }
        public string AWB { get; set; }
        public int? LoadCount { get; set; }
    }

}
