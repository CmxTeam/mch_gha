﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCH.BLL.Model.Enum;

namespace MCH.BLL.Model.BuildFreight
{
    public class BuildFreightShipmentItem
    {

        public List<string> References { get; set; }
        public long Flag { get; set; }
        public long? AwbId { get; set; }
        public long? DetailId { get; set; }
        public string AWB { get; set; }
        public int? TotalPieces { get; set; }
        public int ScannedPieces { get; set; }
        public string Locations { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public enumBuildFreightStatusTypes Status { get; set; }
        public double? PercentProgress { get; set; }
    }
}
