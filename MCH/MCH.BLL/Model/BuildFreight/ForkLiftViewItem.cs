﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.BuildFreight
{
    public class ForkLiftViewItem
    {
        public long? AwbId { get; set; }
        public long? UldId { get; set; }
        public string Uld { get; set; }
        public string AWB { get; set; }
        public int? TotalPieces { get; set; }
        public int? ForkliftPieces { get; set; }
        public int? ReceivedPieces { get; set; }
        public int? AvailablePieces { get; set; }
        public string Locations { get; set; }
        public long DetailId { get; set; }
    }
}
