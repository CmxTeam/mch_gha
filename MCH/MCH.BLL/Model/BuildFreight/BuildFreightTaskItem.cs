﻿using System;
using MCH.BLL.Model.Enum;


namespace MCH.BLL.Model.BuildFreight
{
    public class BuildFreightTaskItem
    {
        public string CarrierCode { get; set; }
        public string FlightNumber { get; set; }
        public DateTime? ETD { get; set; }
        public string Destination { get; set; }
        public int? TotalUlds { get; set; }
        public int? TotalAwbs { get; set; }
        public int? TotalPieces { get; set; }
        public int? ScannedUlds { get; set; }
        public int? ScannedAwbs { get; set; }
        public int? ScannedPieces { get; set; }
        public long TaskId { get; set; }
        public long? FlightManifestId { get; set; }
        public double? PercentProgress { get; set; }
        public long Flag { get; set; }
        public string Locations { get; set; }
        public enumBuildFreightStatusTypes Status { get; set; }
    }
}
