﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Warehouse
{
    public class WarehouseLocationBo
    {
        public string Barcode { get; set; }

        public long? LocationId { get; set; }

        public string LocationName { get; set; }

        public long? LocationTypeId { get; set; }

        public int? ZoneId { get; set; }

        public long? PortId { get; set; }

        public long? WarehouseId { get; set; }
    }
}
