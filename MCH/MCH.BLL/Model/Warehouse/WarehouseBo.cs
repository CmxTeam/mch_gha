﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Model.Warehouse
{
    public class WarehouseBo
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Supervisor { get; set; }
        public int ? CountryId { get; set; }
        public int ? SateId { get; set; }
        public  long ? PortId {get;set;}
        public string GeoLocation { get; set; }
        public string FirmCode { get; set; }
        public int ? ShellWarehouseId { get; set; }


        public string ContactEmail { get; set; }

        public string Address1 { get; set; }
    }
}
