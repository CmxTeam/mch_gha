﻿using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Repositories
{
    public class CustomersRepository : GenericRepository<Customer>
    {
        private MCHDBEntities db;

        public CustomersRepository(MCHDBEntities context)
            : base(context)
        {
            this.db = context;
        }

        public CustomerBo SaveCustomer(CustomerBo model)
        {
            if (model.Id.HasValue && model.Id.Value > 0)
            {
                //edit

                var tmpCustomer = this.db.Customers.SingleOrDefault(item => item.Id == model.Id);
                if (tmpCustomer != null)
                {
                    tmpCustomer.Name = model.Name;
                    tmpCustomer.AccountNumber = model.AccountNumber;
                    tmpCustomer.AddressLine1 = model.Address1;
                    tmpCustomer.City = model.City;
                    tmpCustomer.CountryId = model.CountryId.Value;
                    tmpCustomer.PostalCode = model.Postal;
                    tmpCustomer.StateId = model.StateId;

                    var tmpContact = tmpCustomer.Contacts.Any() ? tmpCustomer.Contacts.First() : new Contact();

                    tmpContact.Email = model.Email;
                    tmpContact.Fax = model.Fax;
                    tmpContact.Phone = model.Phone;

                    if (!tmpCustomer.Contacts.Any())
                    {
                        tmpCustomer.Contacts.Add(tmpContact);
                    }

                    this.db.SaveChanges();
                }
            }
            else
            {
                //add
                var tmpCustomer = new Customer();
                var tmpContact = new Contact();

                tmpCustomer.Name = model.Name;
                tmpCustomer.AccountNumber = model.AccountNumber;
                tmpCustomer.AddressLine1 = model.Address1;
                tmpCustomer.City = model.City;
                tmpCustomer.CountryId = model.CountryId.Value;
                tmpCustomer.PostalCode = model.Postal;
                tmpCustomer.StateId = model.StateId;

                tmpContact.Email = model.Email;
                tmpContact.Fax = model.Fax;
                tmpContact.Phone = model.Phone;

                tmpCustomer.Contacts.Add(tmpContact);

                this.db.Customers.Add(tmpCustomer);

                this.db.SaveChanges();

                model.Id = tmpCustomer.Id;
            }

            return model;
        }
    }
}
