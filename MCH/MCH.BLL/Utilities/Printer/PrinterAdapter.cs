﻿using MCH.BLL.Utilities.Log;
using System;
using System.Runtime.InteropServices;

namespace MCH.BLL.Utilities.Printer
{
    public class PrinterAdapter
    {
        private EventLogWriter evLogger = null;
        public PrinterAdapter(PrinterSettings prSettings)
        {
            this.evLogger = new EventLogWriter("MCH_Logger_File", "Application");
            Settings = prSettings;
        }

        private PrinterSettings m_settings;
        public PrinterSettings Settings
        {
            get { return m_settings; }
            set { m_settings = value; }
        }

        public bool Print(byte[] data)
        {
            Int32 dwCount = data.Length;
            GCHandle h = GCHandle.Alloc(data, GCHandleType.Pinned);
            bool res = SendBytesToPrinter(Settings.PrinterName, h.AddrOfPinnedObject(), dwCount);
            h.Free();
            return res;
        }

        //SendBytesToPrinter()
        //When the function is given a printer name and an unmanaged array
        //of bytes, the function sends those bytes to the print queue.
        //Returns true on success, false on failure.
        private bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
        {
            Int32 dwError = 0;
            Int32 dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFOA di = new DOCINFOA();
            bool bSuccess = false;


            di.pDocName = "Labels";
            di.pDataType = "RAW";

            if (OpenPrinter(szPrinterName.Normalize(), ref hPrinter, IntPtr.Zero))
            {
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    if (StartPagePrinter(hPrinter))
                    {
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, ref dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }

            if (bSuccess == false)
            {
                dwError = Marshal.GetLastWin32Error();
                this.evLogger.Log("Could not open connection to the Printer : " + szPrinterName + ", Error Code Is : " + dwError);
            }
            return bSuccess;
        }


        #region Win32 API Declarations
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }

        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi,
            ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, ref IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, ref Int32 dwWritten);

        #endregion

    }

    public class PrinterSettings
    {
        public int id;
        public char PrinterType;
        public string PrinterName;
        public int PrinterPort;
        public int AlignLeft;
        public int AlignTop;
        public int AlignTearOff;
        public int Darkness;
        public int PrintSpeed;
        public int Width;
        public int Length;
    }
}
