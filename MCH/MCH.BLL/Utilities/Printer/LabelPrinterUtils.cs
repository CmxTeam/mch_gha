﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Utilities.Printer
{
   public class LabelPrinterUtils
    {
        public static bool ProcessLable(string lableName, Dictionary<string, string> parameters, string connectionString, long accountID, string printer, int count)
        {

            string query = "SELECT TOP 1 LabelTemplateZPL FROM LabelTemplatesZPL WHERE LabelName = @LABELNAME AND AccountId = @ACCOUNTID";

            if (accountID == 0)
            {
                query = "SELECT TOP 1 LabelTemplateZPL FROM LabelTemplatesZPL WHERE LabelName = @LABELNAME";
            }
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string labelTemplate;
                try
                {
                    using (SqlCommand commandLabel = new SqlCommand(query, connection))
                    {
                        commandLabel.CommandType = CommandType.Text;
                        commandLabel.Parameters.Add(new SqlParameter("@LABELNAME", lableName));
                        if (accountID != 0)
                        {
                            commandLabel.Parameters.Add(new SqlParameter("@ACCOUNTID", accountID));
                        }
                        commandLabel.Connection.Open();
                        labelTemplate = commandLabel.ExecuteScalar().ToString();
                        commandLabel.Connection.Close();
                    }
                }
                catch (Exception)
                {
                    throw new ApplicationException("Failed getting label template from DB");
                }

                using (SqlCommand command = new SqlCommand(lableName, connection))
                {
                    SqlDataReader reader;
                    try
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        foreach (string paramName in parameters.Keys)
                        {
                            command.Parameters.Add(new SqlParameter(paramName, parameters[paramName]));
                        }
                        command.Connection.Open();
                        reader = command.ExecuteReader();
                    }
                    catch (Exception)
                    {

                        throw new ApplicationException("Failed getting label data from DB");
                    }

                    bool res = true;

                    while (reader.Read())
                    {
                        string label = labelTemplate;
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            label = label.Replace("@@" + reader.GetName(i) + "@@", (reader[i] == DBNull.Value ? "" : reader[i]).ToString());
                        }
                        res = res && PrintLable(label, printer, 1);
                        label = "";
                    }
                    reader.Close();

                    command.Connection.Close();
                    return res;
                }
            }
        }

        public static bool ProcessLable(string lableTemplate, SqlDataReader reader, string printer, int count)
        {
            bool res = true;
            try
            {

                while (reader.Read())
                {
                    string label = lableTemplate;
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        label = label.Replace("@@" + reader.GetName(i) + "@@", (reader[i] == DBNull.Value ? "" : reader[i]).ToString());
                    }
                    res = res && PrintLable(label, printer, count);
                    label = "";
                }
                reader.Close();
                return res;
            }
            catch (Exception)
            {
                throw new ApplicationException("Failed printing label.");
            }
        }

        public static bool PrintLable(string label, string printer, int count)
        {
            PrinterSettings settings = new PrinterSettings { PrinterName = printer };
            byte[] page = Encoding.GetEncoding(850).GetBytes(label);

            PrinterAdapter pr = new PrinterAdapter(settings);
            count = count == 0 ? 1 : count;

            for (int i = 0; i < count; i++)
            {
                if (!pr.Print(page))
                    return false;
            }
            return true;
        }
   }
}
