﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Utilities
{
    public class Constants
    {
        public static class Settings
        {
            public static class Keys
            {
                public static readonly string PRINTER_CONNECTION_STRING = ConfigurationManager.AppSettings["PrinterConnectionString"];
            }
        }
    }
}
