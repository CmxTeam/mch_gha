﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Utilities.Log
{
    public class EventLogWriter
    {
        string sSource;
        string sLog;

        public EventLogWriter(string source, string application)
        {
            this.sSource = source;
            this.sLog = application;


            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);


        }

        public void Log(string message)
        {
            EventLog.WriteEntry(sSource, message,
                EventLogEntryType.Warning, 234);
        }

    }
}
