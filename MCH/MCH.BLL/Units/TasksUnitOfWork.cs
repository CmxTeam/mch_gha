﻿using CMX.Framework.Utils.Security;
using CMX.Shell.ServiceProxy;
using CMX.Shell.ServiceProxy.MobileAuthServiceReference;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.Enum;
using MCH.BLL.Model.Tasks;
using MCH.BLL.Repositories;
using MCH.Core.Models.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Units
{
    public class TasksUnitOfWork : DataUnitOfWork
    {
        private GenericRepository<TaskType> taskTypeRepository;
        private GenericRepository<Warehouse> warehouseRepository;
        private GenericRepository<Task> taskRepository;
        private GenericRepository<FlightManifest> flightRepository;

        public int GetTaskTypeCategories(int taskId)
        {
            int taskCategoryId = 0;
            var taskType = base.DataContext.TaskTypes.SingleOrDefault(p => p.Id == taskId);

            if (taskType != null)
            {
                taskCategoryId = taskType.CategoryId ?? 0;
            }

            return taskCategoryId;
        }

        public List<MCH.BLL.Model.Tasks.TaskCategory> GetTaskTypeCategories(List<long> IDs)
        {
            List<MCH.BLL.Model.Tasks.TaskCategory> result = new List<Model.Tasks.TaskCategory>();
            if (IDs == null || IDs.Count == 0)
            {
                return result;
            }

            result = base.DataContext.TaskCategories.Where(p => IDs.Contains(p.Id)).
                Select(p=>new MCH.BLL.Model.Tasks.TaskCategory 
                                { 
                                    CategoryId = p.Id, 
                                    CategoryName = p.Category, 
                                    IsDefault = p.IsDefault  
                                }                
                ).ToList();


            return result;
        }

        public IList<MenuItem> GetTaskTypeMenueItems(GetRoleMenuModel model, ref TransactionalOfArrayOfAppNavigation2uZVmtVa tmpData)
        {
            using (var authProxy = new ServiceClientWrapper<IMobileAuthServiceChannel>())
            {
                tmpData = authProxy.Client.GetAppRoleMenus(new AppRoleMenuDto
                {
                    ApplicationTypeId = (ApplicationTypes)(int)model.ApplicationTypeId,
                    AppName = model.AppName,
                    RoleId = model.RoleId
                });

                int outParser = 0;
                var tmpTaskCounts = this.GetTasksCountsForType(tmpData.Data.Where(e=> !String.IsNullOrWhiteSpace(e.MenuParam) && Int32.TryParse(e.MenuParam, out outParser)).Select(e => Int32.Parse(e.MenuParam)).ToArray(), model.UserId, model.WarehouseId);

                
                List<MenuItem> result = new List<MenuItem>();
                IList<MenuItem> tmpResult = new List<MenuItem>();
                List<AppNavigation> list = tmpData.Data.ToList();

                foreach (var item in list)
                {
                    ConvertToMenuItemRecursively(item, result, tmpTaskCounts);
                }
               
                return result;
            }
        }

        private void ConvertToMenuItemRecursively(AppNavigation item, List<MenuItem> list, Dictionary<int, int> taskCounts)
        {
            int Id = 0;
            var tmpMenuItem = new MenuItem
                {
                    Id = item.Id,
                    CategoryId = !String.IsNullOrWhiteSpace(item.MenuParam) && Int32.TryParse(item.MenuParam, out Id) ? GetTaskTypeCategoryId(Id) : 0,
                    IconBase64 = item.IconBase64,
                    IconKey = item.IconKey,
                    IsDefault = item.IsDefault,
                    MenuParam = item.MenuParam,
                    Name = item.Name,
                    NavigationPath = item.NavigationPath,
                    
                    Restrictions = item.Restrictions.ToList(),
                    Order = item.Order,
                    Text = item.Text
                };

            tmpMenuItem.OpenAssignedTaskCount = taskCounts.ContainsKey(Id) ? taskCounts[Id] : 0;

            list.Add(tmpMenuItem);
                

            if (item.ChildItems != null && item.ChildItems.Count() > 0)
            {
                foreach (var child in item.ChildItems)
                {
                    ConvertToMenuItemRecursively(child, list, taskCounts);
                }
            }

        }

        private int GetTaskTypeCategoryId(int taskTypeId)
        {
            var tmpTaskType = this.DataContext.TaskTypes.SingleOrDefault(tt => tt.Id == taskTypeId);
            if (tmpTaskType != null)
            {
                return tmpTaskType.CategoryId.HasValue ? tmpTaskType.CategoryId.Value: 0;
            }
            else { return 0; }
        }

        private Dictionary<int, int> GetTasksCountsForType(int[] taskTypeIds, long userId, long ? warehouseId)
        {
            Dictionary<int, int> tmpTaskCounters = new Dictionary<int, int>();

            if (taskTypeIds != null && taskTypeIds.Any())
            {
                var tmpUser = this.DataContext.UserProfiles.SingleOrDefault(item => item.Id == userId);

                if (tmpUser != null)
                {
                    var tmpOpenAndAssignedTasks = tmpUser.TaskAssignments.Select(item => item.Task).GroupBy(item => item.Id).Select(g => g.FirstOrDefault())
                        .Where(item => item.TaskTypeId.HasValue && taskTypeIds.Contains(item.TaskTypeId.Value) && (item.StatusId == (int)enumTaskStatuses.NotStarted || item.StatusId == (int)enumTaskStatuses.InProgress));

                    if (warehouseId.HasValue && warehouseId.Value != 0)
                    {
                        tmpOpenAndAssignedTasks = tmpOpenAndAssignedTasks.Where(item => item.WarehouseId == warehouseId);
                    }

                    if (tmpOpenAndAssignedTasks.Any())
                    {
                        tmpTaskCounters = tmpOpenAndAssignedTasks
                        .GroupBy(item => item.TaskTypeId).Select(item => new  { Value = item.Count(), Key = item.Key.Value }).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                    }
                }
            }
            return tmpTaskCounters;
        }

        public GenericRepository<TaskType> TaskTypeRepository
        {
            get
            {
                if (this.taskTypeRepository == null)
                    this.taskTypeRepository = new GenericRepository<TaskType>(base.DataContext);
                return taskTypeRepository;
            }
        }

        public GenericRepository<Warehouse> WarehouseRepository
        {
            get
            {
                if (this.warehouseRepository == null)
                    this.warehouseRepository = new GenericRepository<Warehouse>(base.DataContext);
                return warehouseRepository;
            }
        }

        public GenericRepository<Task> TaskRepository
        {
            get
            {
                if (this.taskRepository == null)
                    this.taskRepository = new GenericRepository<Task>(base.DataContext);
                return taskRepository;
            }
        }

        public GenericRepository<FlightManifest> FlightRepository
        {
            get
            {
                if (this.flightRepository == null)
                    this.flightRepository = new GenericRepository<FlightManifest>(base.DataContext);
                return flightRepository;
            }
        }

        public IEnumerable<GetCargoReceiverTaskList_Result> GetCargoReceiverTasksList(Nullable<short> pStartIndex, Nullable<byte> pPageSize, string pOrderByAndDir, Nullable<System.DateTime> pStart, Nullable<System.DateTime> pEnd, Nullable<long> pCarrierId, Nullable<long> pOriginId, Nullable<long> pDestinationId, Nullable<int> pTaskStatusId, Nullable<long> pUserId, Nullable<long> pWarehouseId, string pGeneralSearch, string pFlightNumber)
        {
            return base.DataContext.GetCargoReceiverTaskList(pStartIndex,
                pPageSize,
                pOrderByAndDir,
                pGeneralSearch,
                pStart,
                pEnd,
                pCarrierId,
                pOriginId,
                pDestinationId,
                pTaskStatusId,
                pUserId,
                pWarehouseId,
                pFlightNumber).AsEnumerable();
        }

        public void AssignTasksToUsers(long[] taskIds, long[] toUserIds, long userId)
        {
            Context.MCH_AssignTasksToUsers(string.Join(",", taskIds), string.Join(",", toUserIds), userId);
        }

        public List<TaskStatus> CancelTasks(long[] taskIds, long userId)
        {
            var result = Context.MCH_CancelTasks(string.Join(",", taskIds), userId);
            return result.Select(r => new TaskStatus
            {
                Message = r.TMessage,
                Status = r.TStatus.Value,
                TaskId = r.TaskId.Value
            }).ToList();
        }

        public List<TaskStatus> ReopenTasks(long[] taskIds, long userId)
        {
            var result = Context.MCH_ReopenTasks(string.Join(",", taskIds), userId);
            return result.Select(r => new TaskStatus
            {
                Message = r.TMessage,
                Status = r.TStatus.Value,
                TaskId = r.TaskId.Value
            }).ToList();
        }

        public void AddInstructions(long[] taskIds, long userId, string comment)
        {
            Context.MCH_AddTaskInstructions(string.Join(",", taskIds), userId, comment);
        }

        public List<TaskAlert> GetTaskAlerts(long taskId)
        {
            return Context.MCH_GetTaskAlerts(taskId).Select(t => new TaskAlert
            {
                Date = t.AlertDate.Value,
                AlertMessage = t.AlertMessage,
                UserName = t.UserName
            }).ToList();
        }
    }
}
