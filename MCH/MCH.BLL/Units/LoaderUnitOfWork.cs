﻿using CMX.Framework.Utils.Security;
using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Units
{
    public class LoaderUnitOfWork:DataUnitOfWork
    {
        public CargoLoaderFlight[] GetCargoLoaderFlights(string station, int userID, TaskStatuses status, string carrierNo, string destination, string flightNo, ReceiverSortFields sortBy)
        {
            var warehouseId = GetWarehouseId(station);

            return DataContext.GetCargoLoaderFlights(warehouseId, carrierNo, destination, flightNo, (int)status, userID, (int)sortBy).ToList()
                .Select(f => new CargoLoaderFlight
                {
                    TaskId = f.TaskId,
                    FlightManifestId = f.FlightManifestId,
                    CarrierCode = f.CarrierCode,
                    CarrierName = f.CarrierName,
                    CarrierLogo = f.CarrierLogo,
                    FlightNumber = f.FlightNumber,
                    ETD = f.ETD,
                    CutOfTime = f.CutOffTime,
                    Origin = f.Origin,
                    Progress = f.Progress.HasValue ? f.Progress.Value : 0,
                    TotalAwbs = f.TotalAWBs.HasValue ? f.TotalAWBs.Value : 0,
                    TotalSTC = f.TotalSTC.HasValue ? f.TotalSTC.Value : 0,
                    TotalPcs = f.TotalPcs.HasValue ? f.TotalPcs.Value : 0,
                    TotalUlds = f.TotalULds.HasValue ? f.TotalULds.Value : 0,
                    TotalLoose = f.TotalLoose.HasValue ? f.TotalLoose.Value : 0,
                    LoadedAwbs = f.LoadedAWBs.HasValue ? f.LoadedAWBs.Value : 0,
                    LoadedSTC = f.LoadedSTC.HasValue ? f.LoadedSTC.Value : 0,
                    LoadedPcs = f.LoadedPcs.HasValue ? f.LoadedPcs.Value : 0,
                    LoadedUlds = f.LoadedULDs.HasValue ? f.LoadedULDs.Value : 0,
                    LoadedLoose = f.LoadedLoose.HasValue ? f.LoadedLoose.Value : 0,
                    Flag = f.Flags.HasValue ? f.Flags.Value : 0,
                    Status = (TaskStatuses)f.StatusId,
                    Location = string.IsNullOrEmpty(f.StagingLocation) ? "N/A" : f.StagingLocation,
                    Destinations = f.Destination.Split(',')
                }).ToArray();
        }

        public CargoLoaderFlight GetCargoLoaderFlight(long flightManifestId, long taskId)
        {
            var flight = DataContext.GetCargoLoaderFlight(flightManifestId, taskId).SingleOrDefault();
            if (flight == null) return null;

            return new CargoLoaderFlight
            {
                TaskId = flight.TaskId,
                FlightManifestId = flight.FlightManifestId,
                CarrierCode = flight.CarrierCode,
                CarrierName = flight.CarrierName,
                CarrierLogo = flight.CarrierLogo,
                FlightNumber = flight.FlightNumber,
                ETD = flight.ETD,
                CutOfTime = flight.CutOffTime,
                Origin = flight.Origin,
                Progress = flight.Progress.HasValue ? flight.Progress.Value : 0,
                TotalAwbs = flight.TotalAWBs.HasValue ? flight.TotalAWBs.Value : 0,
                TotalSTC = flight.TotalSTC.HasValue ? flight.TotalSTC.Value : 0,
                TotalPcs = flight.TotalPcs.HasValue ? flight.TotalPcs.Value : 0,
                TotalUlds = flight.TotalULds.HasValue ? flight.TotalULds.Value : 0,
                TotalLoose = flight.TotalLoose.HasValue ? flight.TotalLoose.Value : 0,
                LoadedAwbs = flight.LoadedAWBs.HasValue ? flight.LoadedAWBs.Value : 0,
                LoadedSTC = flight.LoadedSTC.HasValue ? flight.LoadedSTC.Value : 0,
                LoadedPcs = flight.LoadedPcs.HasValue ? flight.LoadedPcs.Value : 0,
                LoadedUlds = flight.LoadedULDs.HasValue ? flight.LoadedULDs.Value : 0,
                LoadedLoose = flight.LoadedLoose.HasValue ? flight.LoadedLoose.Value : 0,
                Flag = flight.Flags.HasValue ? flight.Flags.Value : 0,
                Status = (TaskStatuses)flight.StatusId,
                Location = string.IsNullOrEmpty(flight.StagingLocation) ? "N/A" : flight.StagingLocation,
                Destinations = flight.Destination.Split(',')
            };
        }

        public CargoLoaderFlightLeg[] GetCargoLoaderFlightLegs(long flightManifestId)
        {
            return new List<CargoLoaderFlightLeg>
            {
                new CargoLoaderFlightLeg
                {
                    LegId = 1,
                    Origin = "BOS",
                    Destination = "AMS",
                    Location = "DOR 1",
                    Status = TaskStatuses.Pending,
                    TotalAwbs = 25,
                    TotalPcs = 89,
                    TotalSTC = 150,
                    Progress = 45,
                    Flag = 12,
                    Ulds = new List<UldBase>{ new UldBase{UldId = 1, UldSerialNo = "12345", UldType = "PMC" }, 
                        new UldBase{UldId = 2, UldSerialNo = "10002", UldType = "PMC" } }.ToArray()
                },
                new CargoLoaderFlightLeg
                {
                    LegId = 2,
                    Origin = "BOS",
                    Destination = "AMS",
                    Location = "DOR 1",
                    Status = TaskStatuses.Pending,
                    TotalAwbs = 25,
                    TotalPcs = 89,
                    TotalSTC = 150,
                    Progress = 45,
                    Flag = 12,
                    Ulds = new List<UldBase>{ new UldBase{UldId = 1, UldSerialNo = "12345", UldType = "PMC" }, 
                        new UldBase{UldId = 2, UldSerialNo = "12345", UldType = "PMC" } }.ToArray()
                },
                new CargoLoaderFlightLeg
                {
                    LegId = 3,
                    Origin = "BOS",
                    Destination = "AMS",
                    Location = "N/A",
                    Status = TaskStatuses.Pending,
                    TotalAwbs = 25,
                    TotalPcs = 89,
                    TotalSTC = 150,
                    Progress = 45,
                    Flag = 12,
                    Ulds = new List<UldBase>{ new UldBase{UldId = 1, UldSerialNo = "12345", UldType = "PMC" }, 
                        new UldBase{UldId = 3, UldSerialNo = "123456", UldType = "PMC" } }.ToArray()
                }
            }.ToArray();
        }

        public AlertModel[] GetFlightAlerts(string warehousePort, long manifestId)
        {
            var alerts = DataContext.GetFlightAlerts(warehousePort, manifestId, false);
            return alerts.Select(a => new AlertModel
            {
                Date = a.AlertDate.HasValue ? a.AlertDate.Value : DateTime.UtcNow,
                Message = a.AlertMessage,
                SetBy = a.SetBy
            }).ToArray();
        }

        public AlertModel[] GetLegAlerts(long legId)
        {
            return new List<AlertModel>() 
            {
                new AlertModel
                {
                    Date = DateTime.Now,
                    Message = "Flight Leg Alert",
                    SetBy = "SetBy"
                }
            }.ToArray();
        }

        public CargoLoaderFlightLeg GetCargoLoaderFlightLeg(long legId)
        {
            return new CargoLoaderFlightLeg
            {
                LegId = 2,
                Origin = "BOS",
                Destination = "AMS",
                Location = "DOR 1",
                Status = TaskStatuses.Pending,
                TotalAwbs = 25,
                TotalPcs = 89,
                TotalSTC = 150,
                Progress = 84,
                Flag = 12,
                Ulds = new List<UldBase>{ new UldBase{UldId = 1, UldSerialNo = "12345", UldType = "PMC" }, 
                        new UldBase{UldId = 2, UldSerialNo = "12345", UldType = "PMC" } }.ToArray()
            };
        }

        public TransactionStatus UpdateFlightLegLocation(long legId, long locationId, long userId)
        {
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus UpdateCargoLoaderFlightLegUld(long uldLegId, string uldSerialNo, string prefix, long userId, long taskId)
        {
            var uld = DataContext.ULDs.Single(u => u.Id == uldLegId);

            if (uld.FlightManifest.ULDs.Any(u => u.SerialNumber.ToUpper() == uldSerialNo && u.Id != uldLegId))
                return new TransactionStatus { Status = false, Error = "Invalid ULD Serial Number." };

            uld.ShipmentUnitType = DataContext.ShipmentUnitTypes.Single(t => t.Code.ToUpper() == prefix.ToUpper());
            uld.SerialNumber = uldSerialNo;

            var transaction = new Transaction
            {
                EntityTypeId = 4,
                EntityId = uld.Id,
                StatusId = 21,
                StatusTimestamp = DateTime.UtcNow,
                Reference = "ULD# " + uld.ShipmentUnitType.Code + uld.SerialNumber,
                Description = "ULD " + uld.ULDType.Code + "-" + (string.IsNullOrEmpty(uld.SerialNumber) ? string.Empty : uld.ShipmentUnitType.Code + uld.SerialNumber) + " Updated",
                TaskTypeId = 5,
                TransactionActionId = 1,
                TaskId = taskId,
                UserId = userId
            };
            DataContext.Transactions.Add(transaction);
            DataContext.SaveChanges();

            return new TransactionStatus { Status = true };
        }

        public CargoLoaderFlightLegUld[] GetCargoLoaderFlightUlds(long flightId)
        {
            var flight = DataContext.FlightManifests.Single(f => f.Id == flightId);

            return flight.ULDs.Select(u => new CargoLoaderFlightLegUld
            {
                UldId = u.Id,
                Location = u.WarehouseLocation == null ? "N/A" : u.WarehouseLocation.Location,
                UldType = u.ULDType == null ? string.Empty : u.ULDType.Code,
                UldPrefix = u.ShipmentUnitType.Code,
                UldSerialNo = u.SerialNumber,
                TotalAwbs = u.ManifestAWBDetails.Select(d => d.AWBId).Distinct().Count(),
                TotalSTC = u.TotalPieces.HasValue ? u.TotalPieces.Value : 0,
                IsBup = u.IsBUP.HasValue ? u.IsBUP.Value : false,
                Weight = u.TotalWeight.HasValue ? (decimal)u.TotalWeight.Value : 0,
                WeightUOM = u.UOM == null ? "N/A" : u.UOM.TwoCharCode,
                TareWeight = u.TareWeight.HasValue ? (decimal)u.TareWeight.Value : 0
            }).ToArray();
        }

        public string[] GetUldPrefix()
        {
            return DataContext.ShipmentUnitTypes.Where(s => !string.IsNullOrEmpty(s.Code)).Select(s => s.Code).ToArray();
        }

        public CargoLoaderFlightLegUld[] GetFlightDestinationBUPs(long legId, FlightDestinationTypes type, string uldType)
        {
            return new List<CargoLoaderFlightLegUld>
            {
                new CargoLoaderFlightLegUld
                {
                    UldId = 1,
                    Location = "DOR1",
                    UldType = "PMC",
                    UldSerialNo = "11111",
                    TotalAwbs = 10,
                    TotalSTC = 25
                },
                new CargoLoaderFlightLegUld
                {
                    UldId = 2,
                    Location = "DOR1",
                    UldType = "PMC",
                    UldSerialNo = "1212121",
                    TotalAwbs = 10,
                    TotalSTC = 25
                },
            }.ToArray();
        }

        public int GetForkLiftCount(long userId, long taskId)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            var details = DataContext.ForkliftDetails.Where(f => f.UserId == userId && f.TaskId == taskId).ToList();
            return details.Sum(f => f.Pieces.HasValue ? f.Pieces.Value : 0);
        }

        public UldType[] GetUldTypes()
        {
            var types = DataContext.ULDTypes.Where(t => t.Code.ToUpper() != "LOOSE").Select(t => new UldType
            {
                Id = t.Id,
                Code = t.Code
            }).ToList();

            var loose = DataContext.ULDTypes.Single(t => t.Code.ToUpper() == "LOOSE");

            types.Insert(0, new UldType
            {
                Id = loose.Id,
                Code = loose.Code
            });

            return types.ToArray();
        }

        public TransactionStatus UpdateUldWeight(long uldId, decimal value)
        {
            try
            {
                DataContext.ULDs.Single(u => u.Id == uldId).TotalWeight = (double)value;
                DataContext.SaveChanges();
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        public TransactionStatus UpdateUldTareWeight(long uldId, decimal value)
        {
            try
            {
                DataContext.ULDs.Single(u => u.Id == uldId).TareWeight = (double)value;
                DataContext.SaveChanges();
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        public TransactionStatus FinalizeCargoLoader(long taskId, long flightId, long userId)
        {
            DataContext.FinalizeCargoLoader(flightId, taskId, userId);
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus AddCargoLoaderFlightLegULD(long uldTypeId, string prefix, string serial, long taskId, long userId)
        {
            var uldType = DataContext.ULDTypes.Single(t => t.Id == uldTypeId);
            var task = DataContext.Tasks.Single(t => t.Id == taskId);
            var flight = DataContext.FlightManifests.Single(f => f.Id == task.EntityId && task.EntityTypeId == 1);

            if (uldType.Code.ToUpper() == "LOOSE" && flight.ULDs.Any(u => u.ShipmentUnitType.Code.ToUpper() == "LOOSE"))
                return new TransactionStatus { Status = false, Error = "Flight already has LOOSE ULD" };

            var uld = new ULD
            {
                StatusID = 20,
                TotalUnitCount = 1,
                TotalPieces = 0,
                ULDType = uldType,
                IsBUP = false,
                FlightManifest = flight,
                UnloadingPort = flight.UnloadingPorts.First()
            };

            if (uldType.Code.ToUpper() == "LOOSE")
            {
                uld.ShipmentUnitType = DataContext.ShipmentUnitTypes.Single(t => t.Code.ToUpper() == "LOOSE");
            }
            else
            {
                if (flight.ULDs.SingleOrDefault(u => u.SerialNumber == serial) != null)
                    return new TransactionStatus { Status = false, Error = "Uld already exists for flight." };
                uld.ShipmentUnitType = DataContext.ShipmentUnitTypes.Single(t => t.Code.ToUpper() == prefix.ToUpper());
                uld.SerialNumber = serial;
                flight.TotalULDs += 1;
            }

            var transaction = new Transaction
            {
                EntityTypeId = 4,
                EntityId = uld.Id,
                StatusId = 20,
                StatusTimestamp = DateTime.UtcNow,
                Reference = "ULD# " + uld.ShipmentUnitType.Code + uld.SerialNumber,
                Description = "ULD " + uld.ULDType.Code + "-" + (string.IsNullOrEmpty(uld.SerialNumber) ? string.Empty : uld.ShipmentUnitType.Code + uld.SerialNumber) + " Added",
                TaskTypeId = 5,
                TransactionActionId = 1,
                TaskId = taskId,
                UserId = userId
            };

            DataContext.ULDs.Add(uld);
            DataContext.Transactions.Add(transaction);
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus UpdateFlightLegUldLocation(long uldId, int locationId, long taskId, long userId)
        {
            var uld = DataContext.ULDs.Single(u => u.Id == uldId);
            var task = DataContext.Tasks.Single(t => t.Id == taskId && t.EntityTypeId == 1);
            var flight = DataContext.FlightManifests.Single(f => f.Id == task.EntityId);

            uld.WarehouseLocation = DataContext.WarehouseLocations.Single(l => l.Id == locationId);

            var disposition = new Disposition
            {
                ULD = uld,
                TaskId = taskId,
                Count = 1,
                WarehouseLocation = uld.WarehouseLocation,
                UserId = userId,
                Timestamp = DateTime.UtcNow
            };

            var transaction = new Transaction
            {
                StatusId = 23,
                TaskTypeId = 5,
                TransactionActionId = 1,
                Reference = flight.FlightNumber,
                Description = "ULD" + (uld.ULDType != null ? uld.ULDType.Code : string.Empty) + "-" + (string.IsNullOrEmpty(uld.SerialNumber) ? string.Empty : uld.ShipmentUnitType.Code + uld.SerialNumber) + "located at:" + uld.WarehouseLocation.Location,
                UserId = userId                
            };

            DataContext.Dispositions.Add(disposition);
            DataContext.Transactions.Add(transaction);
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus DeleteCargoLoaderFlightLegUld(long uldId, long userId)
        {
            var uld = DataContext.ULDs.Single(u => u.Id == uldId);

            if (uld.ManifestAWBDetails.Count > 0 || uld.ManifestDetails.Count > 0 || uld.ManifestPackageDetails.Count > 0)
                return new TransactionStatus { Status = false, Error = "ULD is not empty." };

            if (uld.ShipmentUnitType.Code.ToUpper() != "LOOSE")
                uld.FlightManifest.TotalULDs -= 1;

            /*var transaction = new Transaction
            {
                StatusId = 20,
                TaskTypeId = 5,
                TransactionActionId = 1,
                Reference = uld.FlightManifest.FlightNumber,
                UserName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId,
                Description = "ULD" + uld.ULDType.Code + "-" + (string.IsNullOrEmpty(uld.SerialNumber) ? string.Empty : uld.ShipmentUnitType.Code + uld.SerialNumber) + "Removed"
            };*/

            DataContext.Dispositions.RemoveRange(uld.Dispositions);
            DataContext.ULDs.Remove(uld);
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public AwbModel[] GetLoadingPlan(long flightId, long taskId)
        {
            var items = DataContext.GetLoadingPlan(flightId, taskId).ToList();
            return items.Select(i => new AwbModel
            {
                AWBID = i.AWBID,
                AWBNumber = i.AWBNumber,
                TotalPieces = i.TotalPieces.HasValue ? i.TotalPieces.Value : 0,
                TotalSTC = i.TotalSTC,
                LoadingIndicator = i.LoadingIndicator,
                LoadPcs = i.LoadPcs.HasValue ? i.LoadPcs.Value : 0,
                ForkliftPcs = i.ForkliftPcs,
                LoadedPcs = i.LoadedPcs,
                Weight = i.Weight.HasValue ? i.Weight.Value : 0,
                WeightUOM = i.WeightUOM,
                Flags = i.Flags.HasValue ? i.Flags.Value : 0,
                Locations = i.Locations,
                Status = (TaskStatuses)Enum.Parse(typeof(TaskStatuses), i.Status),
                AvailablePcs = i.AvailablePcs.HasValue ? i.AvailablePcs.Value : 0
            }).ToArray();
        }

        public AwbModel GetLoadingPlanForAwb(long awbId, long taskId)
        {
            var item = DataContext.GetLoadingPlanForAwb(awbId, taskId).FirstOrDefault();
            return new AwbModel
            {
                AWBID = item.AWBID,
                AWBNumber = item.AWBNumber,
                TotalPieces = item.TotalPieces.HasValue ? item.TotalPieces.Value : 0,
                TotalSTC = item.TotalSTC,
                LoadingIndicator = item.LoadingIndicator,
                LoadPcs = item.LoadPcs.HasValue ? item.LoadPcs.Value : 0,
                ForkliftPcs = item.ForkliftPcs,
                LoadedPcs = item.LoadedPcs,
                Weight = item.Weight.HasValue ? item.Weight.Value : 0,
                WeightUOM = item.WeightUOM,
                Flags = item.Flags.HasValue ? item.Flags.Value : 0,
                Locations = item.Locations,
                Status = (TaskStatuses)Enum.Parse(typeof(TaskStatuses), item.Status),
                AvailablePcs = item.AvailablePcs.HasValue ? item.AvailablePcs.Value : 0
            };
        }

        public UldViewItem[] GetUldView(long flightId, long uldId)
        {
            var awbs = DataContext.ManifestAWBDetails.Where(d => d.ULDId == uldId && d.FlightManifestId == flightId && d.AWB != null).ToList();

            return awbs.Select(a => new UldViewItem
            {
                DetailId = a.Id,
                AWBID = a.AWBId.Value,
                AWBNumber = a.AWB.Carrier.Carrier3Code + '-' + a.AWB.AWBSerialNumber,
                LoadCount = a.LoadCount.HasValue ? a.LoadCount.Value : 0
            }).ToArray();
        }

        public ValidatedShipment ValidateLoaderShipment(long flightId, long taskId, string shipment)
        {
            var i = DataContext.ValidateLoaderShipment(flightId, taskId, shipment).FirstOrDefault();
            if (i == null)
                return new ValidatedShipment { TransactionStatus = new TransactionStatus { Status = false, Error = "Shipment not found on Flight" } };

            return new ValidatedShipment
            {
                AwbId = i.AWBID,
                Awb = i.AWBNumber,
                TransactionStatus = new TransactionStatus { Status = true },
                AvailablePieces = i.AvailablePcs.HasValue ? i.AvailablePcs.Value : 0
            };
            //return new AwbModel
            //{
            //    AWBID = i.AWBID,
            //    AWBNumber = i.AWBNumber,
            //    TotalPieces = i.TotalPieces.HasValue ? i.TotalPieces.Value : 0,
            //    TotalSTC = i.TotalSTC,
            //    LoadingIndicator = i.LoadingIndicator,
            //    LoadPcs = i.LoadPcs.HasValue ? i.LoadPcs.Value : 0,
            //    ForkliftPcs = i.ForkliftPcs,
            //    LoadedPcs = i.LoadedPcs,
            //    Weight = i.Weight.HasValue ? i.Weight.Value : 0,
            //    WeightUOM = i.WeightUOM,
            //    Flags = i.Flags.HasValue ? i.Flags.Value : 0,
            //    Locations = i.Locations,
            //    Status = (TaskStatuses)Enum.Parse(typeof(TaskStatuses), i.Status),
            //    AvailablePcs = i.AvailablePcs.HasValue ? i.AvailablePcs.Value : 0
            //};
        }

        public TransactionStatus DropPiecesIntoForklift(long userId, long taskId, long awbId, int count)
        {
            var user = DataContext.UserProfiles.Single(u => u.Id == userId);
            var forkliftDetail = DataContext.ForkliftDetails.SingleOrDefault(d => d.AWBId == awbId && d.TaskId == taskId && d.UserId == userId);

            if (forkliftDetail != null)
                forkliftDetail.Pieces += count;
            else
            {
                DataContext.ForkliftDetails.Add(new ForkliftDetail
                {
                    AWBId = awbId,
                    TaskId = taskId,
                    Pieces = count,
                    UserId = userId
                });
            }
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public CargoLoaderFlightLegUld[] AvailableULDsForBuild(long flightId)
        {
            var items = DataContext.ULDs.Where(u => u.FlightManifestId == flightId && u.IsBUP == false).ToList();
            return items.Select(u => new CargoLoaderFlightLegUld
            {
                UldId = u.Id,
                Location = u.WarehouseLocation == null ? "N/A" : u.WarehouseLocation.Location,
                UldType = u.ULDType.Code,
                UldPrefix = u.ShipmentUnitType.Code,
                UldSerialNo = u.SerialNumber,
                TotalAwbs = u.ManifestAWBDetails.Select(d => d.AWBId).Distinct().Count(),
                TotalSTC = u.TotalPieces.HasValue ? u.TotalPieces.Value : 0,
                IsBup = u.IsBUP.HasValue ? u.IsBUP.Value : false,
                Weight = u.TotalWeight.HasValue ? (decimal)u.TotalWeight.Value : 0,
                WeightUOM = u.UOM == null ? "N/A" : u.UOM.TwoCharCode,
                TareWeight = u.TareWeight.HasValue ? (decimal)u.TareWeight.Value : 0
            }).ToArray();
        }

        public LoadForkliftViewItem[] GetForkliftView(long taskId, long userId)
        {
            var items = DataContext.GetLoaderForkliftView(userId, taskId);
            return items.Select(i => new LoadForkliftViewItem
            {
                DetailsId = i.DetailsId,
                AwbId = i.AwbId.HasValue ? i.AwbId.Value : 0,
                AwbNumber = i.AwbSerialNumber,
                UserId = i.UserId.HasValue ? i.UserId.Value : 0,
                TaskId = i.TaskId.HasValue ? i.TaskId.Value : 0,
                ForkliftPieces = i.ForkliftPieces.HasValue ? i.ForkliftPieces.Value : 0,
                TTLForkliftPcs = i.TTLForkliftPcs.HasValue ? i.TTLForkliftPcs.Value : 0,
                LoadPieces = i.LoadPieces.HasValue ? i.LoadPieces.Value : 0,
                LoadedPcs = i.LoadedPcs.HasValue ? i.LoadedPcs.Value : 0,
                AvailablePcs = i.AvailablePcs.HasValue ? i.AvailablePcs.Value : 0,
                Counter = 0
            }).ToArray();
        }

        public TransactionStatus RemovePiecesFromForklift(long detailsId, int count)
        {
            var forkliftView = DataContext.ForkliftDetails.Single(d => d.Id == detailsId);
            if (forkliftView.Pieces > count)
                forkliftView.Pieces -= count;
            else if (forkliftView.Pieces == count)
                DataContext.ForkliftDetails.Remove(forkliftView);
            else
                return new TransactionStatus { Status = false, Error = "Error! How can Forklift have more pieces than you want to remove" };
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        //[WebMethod]
        //public TransactionStatus DropSelectedItemsIntoLocationTest(long taskId, long userId, int locationId)
        //{
        //    var detailIds = new long [] { 35 };
        //    return DropSelectedItemsIntoLocationImpl(taskId, userId, locationId, () => { return DataContext.ForkliftDetails.Where(fd => detailIds.Contains(fd.Id)); });
        //}

        public TransactionStatus DropSelectedItemsIntoLocation(long taskId, long userId, int locationId, long[] detailIds)
        {
            return DropSelectedItemsIntoLocationImpl(taskId, userId, locationId, () => { return DataContext.ForkliftDetails.Where(fd => detailIds.Contains(fd.Id)); });
        }

        public TransactionStatus DropAllForkliftPiecesIntoLocation(long taskId, long userId, int locationId)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            return DropSelectedItemsIntoLocationImpl(taskId, userId, locationId, () => { return DataContext.ForkliftDetails.Where(fd => fd.TaskId == taskId && fd.UserId == userId); });
        }

        private TransactionStatus DropSelectedItemsIntoLocationImpl(long taskId, long userId, int locationId, Func<IEnumerable<ForkliftDetail>> detailsIdsFunc)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            var task = DataContext.Tasks.Single(t => t.Id == taskId && t.EntityTypeId == 1 && t.TaskTypeId == 5);
            var flight = DataContext.FlightManifests.Single(f => f.Id == task.EntityId);
            var uld = DataContext.ULDs.Single(u => u.FlightManifestId == flight.Id && u.UnitTypeId == 6);

            var forkliftDetails = detailsIdsFunc();

            foreach (var forkliftDetail in forkliftDetails)
            {
                var awbDetail = DataContext.ManifestAWBDetails.SingleOrDefault(d => d.FlightManifestId == flight.Id && d.AWBId == forkliftDetail.AWBId && d.ULDId == uld.Id);
                if (awbDetail == null)
                {
                    DataContext.ManifestAWBDetails.Add(new ManifestAWBDetail
                    {
                        AWBId = forkliftDetail.AWBId,
                        FlightManifestId = flight.Id,
                        LoadCount = forkliftDetail.Pieces,
                        ULDId = uld.Id
                    });
                }
                else
                    awbDetail.LoadCount += forkliftDetail.Pieces;

                DataContext.Transactions.Add(new Transaction
                {
                    TaskId = taskId,
                    StatusId = 22,
                    Description = forkliftDetail.AWB.Carrier.Carrier3Code + "-" + forkliftDetail.AWB.AWBSerialNumber + " for " + forkliftDetail.Pieces + " of AWB built as LOOSE",
                    TransactionActionId = 1,
                    StatusTimestamp = DateTime.UtcNow,
                    TaskTypeId = 5,
                    EntityTypeId = 2,
                    EntityId = forkliftDetail.AWBId,
                    Reference = forkliftDetail.AWB.Carrier.Carrier3Code + "-" + forkliftDetail.AWB.AWBSerialNumber,
                    UserId = userId
                });

                DataContext.Dispositions.Add(new Disposition
                {
                    TaskId = taskId,
                    AWBId = forkliftDetail.AWBId,
                    Count = forkliftDetail.Pieces,
                    Timestamp = DateTime.UtcNow,
                    ULDId = uld.Id,
                    WarehouseLocationId = locationId
                });
            }

            flight.Status = DataContext.Statuses.Single(s => s.Name == "Build In Progress");
            flight.PercentOverall = flight.ManifestAWBDetails.Sum(d => d.LoadCount) / (double)flight.TotalPieces * 100;

            DataContext.ForkliftDetails.RemoveRange(forkliftDetails);

            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus DropSelectedItemsIntoULD(long taskId, long userId, long uldId, long[] detailIds)
        {
            return DropSelectedItemsIntoULDImpl(taskId, userId, uldId, () => { return DataContext.ForkliftDetails.Where(fd => detailIds.Contains(fd.Id)); });
        }

        public TransactionStatus DropAllForkliftPiecesIntoULD(long taskId, long userId, long uldId)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            return DropSelectedItemsIntoULDImpl(taskId, userId, uldId, () => { return DataContext.ForkliftDetails.Where(fd => fd.TaskId == taskId && fd.UserId == userId); });
        }

        private TransactionStatus DropSelectedItemsIntoULDImpl(long taskId, long userId, long uldId, Func<IEnumerable<ForkliftDetail>> detailsIdsFunc)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            var task = DataContext.Tasks.Single(t => t.Id == taskId && t.EntityTypeId == 1 && t.TaskTypeId == 5);
            var flight = DataContext.FlightManifests.Single(f => f.Id == task.EntityId);
            var uld = DataContext.ULDs.Single(u => u.Id == uldId);
            var details = detailsIdsFunc();
            foreach (var forkliftDetail in details)
            {
                var awbDetail = DataContext.ManifestAWBDetails.SingleOrDefault(d => d.FlightManifestId == flight.Id && d.AWBId == forkliftDetail.AWBId && d.ULDId == uld.Id);
                if (awbDetail == null)
                {
                    DataContext.ManifestAWBDetails.Add(new ManifestAWBDetail
                    {
                        AWBId = forkliftDetail.AWBId,
                        FlightManifestId = flight.Id,
                        LoadCount = forkliftDetail.Pieces,
                        ULDId = uld.Id
                    });
                }
                else
                    awbDetail.LoadCount += forkliftDetail.Pieces;

                DataContext.Transactions.Add(new Transaction
                {
                    TaskId = taskId,
                    EntityTypeId = 2,
                    EntityId = forkliftDetail.AWBId,
                    StatusId = 22,
                    TaskTypeId = 5,
                    StatusTimestamp = DateTime.UtcNow,
                    Description = forkliftDetail.AWB.Carrier.Carrier3Code + "-" + forkliftDetail.AWB.AWBSerialNumber + " for " + forkliftDetail.Pieces + " of AWB dropped in ULD " + uld.ShipmentUnitType.Code + uld.SerialNumber,
                    TransactionActionId = 1,
                    Reference = forkliftDetail.AWB.Carrier.Carrier3Code + "-" + forkliftDetail.AWB.AWBSerialNumber,
                    UserId = userId
                });

                var disposition = DataContext.Dispositions.SingleOrDefault(disp => disp.TaskId == taskId && disp.AWBId == forkliftDetail.AWBId && disp.ULDId == uld.Id);
                if (disposition == null)
                {
                    DataContext.Dispositions.Add(new Disposition
                    {
                        TaskId = taskId,
                        AWBId = forkliftDetail.AWBId,
                        Count = forkliftDetail.Pieces,
                        Timestamp = DateTime.UtcNow,
                        ULDId = uld.Id
                    });
                }
                else
                {
                    disposition.Count += forkliftDetail.Pieces;
                }
                uld.TotalPieces += forkliftDetail.Pieces;
            }

            flight.Status = DataContext.Statuses.Single(s => s.Name == "Build In Progress");
            var sum = flight.ManifestAWBDetails.Sum(d => d.LoadCount);
            flight.PercentOverall = sum / (double)flight.TotalPieces * 100;

            DataContext.ForkliftDetails.RemoveRange(details);
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus SetFlightBuildLocation(long taskId, long userId, int locationId)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            var task = DataContext.Tasks.Single(t => t.Id == taskId && t.EntityTypeId == 1 && t.TaskTypeId == 5);
            var flight = DataContext.FlightManifests.Single(f => f.Id == task.EntityId);

            flight.StagingLocationId = locationId;
            flight.StagedBy = userName;
            flight.StatusId = 12;
            flight.StatusTimestamp = DateTime.UtcNow;
            flight.StagedOn = DateTime.UtcNow;

            //TODO:Harut => fix this
            //task.StatusId = 13;
            task.StatusTimestamp = DateTime.UtcNow;

            DataContext.TaskTransactions.Add(new TaskTransaction
            {
                TaskId = taskId,
                Description = "Task Started, In Progress",
                RecDate = DateTime.UtcNow,
                UserId = userId
            });

            DataContext.Transactions.Add(new Transaction
            {
                StatusId = 21,
                TransactionActionId = 1,
                TaskId = taskId,
                Description = "Flight " + flight.Carrier.CarrierCode + flight.FlightNumber + " Build Started",
                UserId = userId
            });

            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus RemoveAwbFromUld(long manifestAwbDetaildId, long uldId, long taskId, long userId, int locationId)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            var uld = DataContext.ULDs.Single(u => u.Id == uldId);
            var detail = DataContext.ManifestAWBDetails.SingleOrDefault(d => d.Id == manifestAwbDetaildId && d.ULDId == uldId);

            if (detail != null)
            {
                var loadCount = detail.LoadCount;
                var dispositions = DataContext.Dispositions.Where(dis => dis.TaskId == taskId && dis.AWBId == detail.AWBId && dis.ULDId == uldId);

                foreach (var disposition in dispositions)
                {
                    if (disposition.Count <= loadCount)
                    {
                        disposition.WarehouseLocationId = locationId;
                        disposition.Timestamp = DateTime.UtcNow;
                        disposition.ULD = null;
                        loadCount -= disposition.Count;
                    }
                    else
                    {
                        DataContext.Dispositions.Add(new Disposition
                        {
                            TaskId = taskId,
                            AWBId = detail.AWBId,
                            Count = loadCount,
                            Timestamp = DateTime.UtcNow,
                            WarehouseLocationId = locationId
                        });
                        disposition.Count -= loadCount;
                        loadCount = 0;
                    }
                }

                if (loadCount > 0)
                {
                    DataContext.Dispositions.Add(new Disposition
                    {
                        TaskId = taskId,
                        AWBId = detail.AWBId,
                        Count = loadCount,
                        Timestamp = DateTime.UtcNow,
                        WarehouseLocationId = locationId
                    });
                }

                DataContext.Transactions.Add(new Transaction
                {                    
                    StatusId = 22,
                    Description = detail.LoadCount + " of AWB removed from ULD " + uld.ShipmentUnitType.Code + uld.SerialNumber,
                    TransactionActionId = 1,
                    UserId = userId
                });

                uld.TotalPieces = Math.Max(uld.TotalPieces.Value - detail.LoadCount.Value, 0);
                DataContext.ManifestAWBDetails.Remove(detail);
                uld.FlightManifest.PercentOverall = uld.FlightManifest.ManifestAWBDetails.Sum(d => d.LoadCount) / (double)uld.FlightManifest.TotalPieces * 100;

                DataContext.SaveChanges();
            }
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus UpdateFlightBuildLocation(long flightManifestId, long userId, long taskId, int locationId)
        {
            var task = DataContext.Tasks.Single(t => t.Id == taskId);
            var userName = DataContext.UserProfiles.Single(p => p.Id == userId).UserId;
            var flight = DataContext.FlightManifests.Single(m => m.Id == flightManifestId);
            var location = DataContext.WarehouseLocations.Single(l => l.Id == locationId);

            flight.Status = DataContext.Statuses.Single(s => s.Name == "Build Started");
            flight.StagingLocationId = locationId;
            task.StartDate = DateTime.UtcNow;

            //TODO:Harut fix this 
            //task.Status = DataContext.Statuses.Single(s => s.Name == "In Progress");

            DataContext.Transactions.Add(new Transaction
            {
                Description = "'Flight Build Area Set As " + location.Location,
                StatusId = 21,
                TransactionActionId = 1,
                Reference = flight.FlightNumber,
                Task = task,
                UserId = userId
            });

            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }
    }
}
