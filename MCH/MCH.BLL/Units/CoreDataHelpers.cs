﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class CoreDataHelpers : CoreUnitOfWork
    {
        public string GetHWBImportControl(long awbId, long hawbId, string part)
        {
            string awbNumber;
            string hwbNumber;
            using (var commonUnit = new CommonUnit())
            {
                awbNumber = commonUnit.GetAwbNumberById(awbId);
                hwbNumber = commonUnit.GetHwbNumberById(awbId);
            }

            var wayBillIds = Context.Waybills.Where(w => w.Master == awbNumber).Select(w => w.WaybillID).ToList();

            var statuses = Context.Details.Where(d => d.House == hwbNumber && wayBillIds.Any(w => w == d.WaybillID))
                .ToList().Where(d => string.IsNullOrEmpty(part) || d.PartialIdCode == part)
                .SelectMany(d => d.Statuses).ToList();
            var result = statuses.OrderByDescending(s => s.TransactionDateTime).Select(s => s.CustomsCode + " " + s.EntryNo).FirstOrDefault();
            return result;
        }

        public bool HasAWBCustomsInfo(long awbId)
        {
            string awbNumber;            
            using (var commonUnit = new CommonUnit())
            {
                awbNumber = commonUnit.GetAwbNumberById(awbId);                
            }

            var detail = Context.Details.FirstOrDefault(d => d.Waybill.Master == awbNumber && (d.House == string.Empty || d.House == null));
            return detail != null;
        }
    }
}
