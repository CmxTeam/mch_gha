﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using MCH.BLL.Model.Constants;
using ReportExecution2005;
using Reporting;
using System.Drawing;

namespace MCH.BLL.Units
{
    public class ReportPrintingUnit : DataUnitOfWork
    {
        public void PrintReport(string reportPath, string reportName, Dictionary<string, string> pars, string printerName,
            int numberOfCopies)
        {
            List<ParameterValue> reportParams = pars.Select(k => new ParameterValue()
                                                                 {
                                                                     Name = k.Key,
                                                                     Value = k.Value
                                                                 }).ToList();
            ReportManager.PrintReport(reportPath, reportName, reportParams.ToArray(), printerName, numberOfCopies);
        }

        public void PrintAttachment(long attachmentId, string printerName, int numberOfCopies)
        {
            var att = DataContext.Attachments.Single(s => s.Id == attachmentId);
            var base64Data = Regex.Match(att.AttachmentImage, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;

            //If has image.
            if (!string.IsNullOrEmpty(base64Data))
            {
                byte[] imageBytes = Convert.FromBase64String(base64Data);
                ReportManager.PrintImage(imageBytes, printerName, numberOfCopies);
            }
            else
            {
                string path = Path.Combine(ConfigurationManager.AppSettings[Constants.ATTACHMENTS_REPOSITORY], att.FileName);
                var img = Image.FromFile(path);
                using (var ms = new MemoryStream())
                {
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    var arr = ms.ToArray();
                    ReportManager.PrintImage(arr, printerName, numberOfCopies);
                }
            }
        }
    }
}
