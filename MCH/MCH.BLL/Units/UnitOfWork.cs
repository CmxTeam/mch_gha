﻿using MCH.BLL.DAL.Core;
using MCH.BLL.DAL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{

    public enum ContextTypes
    {
        Core,
        Data,
        All
    }

    public class UnitOfWork : IDisposable
    {
        protected readonly MCHCoreEntities coreContext = new MCHCoreEntities();
        protected readonly MCHDBEntities dataContext = new MCHDBEntities();

        public void Save(ContextTypes contextType)
        {
            switch (contextType)
            {
                case ContextTypes.Core:
                    {
                        this.coreContext.SaveChanges();
                        break;
                    }
                case ContextTypes.Data:
                    {
                        this.dataContext.SaveChanges();
                        break;
                    }
                case ContextTypes.All:
                    {
                        this.dataContext.SaveChanges();
                        this.coreContext.SaveChanges();
                        break;

                    }
                default:
                    break;
            }

        }

        public MCHDBEntities DataContext
        {
            get { return this.dataContext; }
        }

        public MCHCoreEntities CoreContext {
            get { return this.coreContext; }
        }

        #region IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.dataContext.Dispose();
                    this.coreContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
