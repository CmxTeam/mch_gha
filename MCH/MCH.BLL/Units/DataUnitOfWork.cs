﻿using MCH.BLL.DAL.Data;
using MCH.BLL.Exceptions;
using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL
{
    public class DataUnitOfWork : IDisposable
    {
        protected readonly MCHDBEntities DataContext = new MCHDBEntities();

        protected long GetWarehouseId(string station)
        {
            return DataContext.Warehouses.First(w => w.Port.IATACode == station).Id;
        }

        protected long GetAppUserId(long shellUserId)
        {
            var tmpUser = this.Context.UserProfiles.SingleOrDefault(u => u.ShellUserId == shellUserId);

            if(tmpUser == null){
                throw new MCHBusinessException(String.Format("User not found for Id : {0}", shellUserId), ErrorCodes.UserErrorCodes.USER_NOT_FOUND);
            } 
            return tmpUser.Id;
        }

        public void Save()
        {
            DataContext.SaveChanges();
        }

        public MCHDBEntities Context 
        {
            get { return this.DataContext; }
        }

        #region IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    DataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        } 
        #endregion
    }
}
