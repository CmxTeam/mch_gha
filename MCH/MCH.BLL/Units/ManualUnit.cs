﻿using MCH.Core.Models.Manual;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class ManualUnit : UnitOfWork
    {
        public List<Core.Models.Manual.AwbHwbModel> GetAwbHwbs(long awbId)
        {
            var tmpResult = new List<AwbHwbModel>();

            var tmpAwb = this.DataContext.AWBs.SingleOrDefault(a => a.Id == awbId);

            if (tmpAwb != null)
            {
                var tmpHwbs = this.DataContext.HWBs.Where(h => h.AWBs_HWBs.FirstOrDefault().AWBId == awbId).Select(h => new AwbHwbModel
                {
                    HwbId = h.Id,
                    Origin = h.Port.IATACode,
                    HWB = h.HWBSerialNumber,
                    Destination = h.Port1.IATACode,
                    HPieces = h.Pieces,
                    HWeight = h.Weight,
                    ShipperName = h.Customer.Name,
                    ConsigneeName = h.Customer1.Name
                }).ToList();
            }

            return tmpResult;
        }
    }
}
