﻿using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class InventoryUnit : DataUnitOfWork
    {
        public void SetLocationAsEmpty(long userId, int locationId, long taskId)
        {
            var disposition = Context.Dispositions.SingleOrDefault(d => d.TaskId == taskId && d.WarehouseLocationId == locationId && d.UserId == userId);
            if (disposition != null)
            {
                disposition.Count = 0;
                disposition.Timestamp = DateTime.UtcNow;
            }
            else
            {
                Context.Dispositions.Add(new Disposition
                {
                    TaskId = taskId,
                    Count = 0,
                    WarehouseLocationId = locationId,
                    UserId = userId,
                    Timestamp = DateTime.UtcNow
                });
            }

            Save();
        }

        public bool IsInventoryLocationEmpty(long userId, int locationId, long taskId)
        {
            var disposition = Context.Dispositions.Where(d => d.TaskId == taskId && d.WarehouseLocationId == locationId && d.UserId == userId);

            if (!disposition.Any()) return true;

            return disposition.Sum(d => d.Count) <= 0;
        }

        public long GetInventoryTaskId(long userId, long warehouseId)
        {
            var appUserId = base.GetAppUserId(userId);
            var task = Context.MCH_GetInventoryTaskId(appUserId, warehouseId).Single();
            return task.Value;
        }

        public InventoryShipmentItem ValidateShipment(long warehouseId, string barcode, long taskId, long userId)
        {
            var shipment = Context.MCH_ValidateShipment(base.GetAppUserId(userId), warehouseId, barcode, taskId).Single();

            return new InventoryShipmentItem
            {
                AwbId = shipment.AwbId.Value,
                Awb = shipment.Awb,
                HwbId = shipment.HwbId,
                Hwb = shipment.Hwb,
                Origin = shipment.Origin,
                Destination = shipment.Destination,
                Locations = shipment.Locations,
                TotalPieces = shipment.TotalPieces.HasValue ? shipment.TotalPieces.Value : 0,
                ScannedPieces = shipment.ScannedPieces,
                Weight = shipment.Weight.HasValue ? shipment.Weight.Value : 0,
                WeightUOM = shipment.WeightUOM
            };
        }

        public List<InventoryShipmentItem> GetLocationsHistory(long warehouseId, string barcode, long taskId, long userId)
        {
            return Context.MCH_LocationsHistory(base.GetAppUserId(userId), warehouseId, barcode, taskId).
                Select(p => new InventoryShipmentItem
                {
                    AwbId = p.AwbId.Value,
                    Awb = p.Awb,
                    HwbId = p.HwbId.Value,
                    Hwb = p.Hwb,
                    Origin = p.Origin,
                    Destination = p.Destination,
                    Locations = p.Locations,
                    TotalPieces = p.TotalPieces.HasValue ? p.TotalPieces.Value : 0,
                    ScannedPieces = p.ScannedPieces ?? 0,
                    Weight = p.Weight.HasValue ? p.Weight.Value : 0,
                    WeightUOM = p.WeightUOM
                }).ToList();
        }

        public void EditAwbPiecesCount(long awbId, int oldWarehouseLocationId, long taskId, int pcs, long userId)
        {
            Context.MCH_EditAwbPiecesCount(base.GetAppUserId(userId), awbId, oldWarehouseLocationId, pcs, taskId); 
        }

        public void EditHwbPiecesCount(long hwbId, int oldWarehouseLocationId, long taskId, int pcs, long userId)
        {
            Context.MCH_EditHwbPiecesCount(base.GetAppUserId(userId), hwbId, oldWarehouseLocationId, pcs, taskId);
        }

        public void ScanShipment(long userId, long awbId, long hwbId, int locationId, int pieces, long taskId)
        {
            Context.MCH_ScanShipment(base.GetAppUserId(userId), awbId, hwbId, locationId, pieces, taskId);
        }

        public void FinalizeInventory(long userId, long taskId)
        {
            Context.MCH_FinalizeInventory(base.GetAppUserId(userId), taskId);
        }

        public List<InventoryLocationViewItem> GetMyInventory(long userId, long taskId)
        {
            long appUserId = base.GetAppUserId(userId);
            var userName = GetUserName(appUserId);

            return Context.Dispositions.Where(d => d.TaskId == taskId && d.UserId == appUserId)
                .GroupBy(g => g.WarehouseLocation).Select(g => new InventoryLocationViewItem
                {
                    Location = new Model.Common.LocationItem
                    {
                        LocationId = g.Key.Id,
                        Location = g.Key.Location
                    },
                    ScannedPieces = g.Sum(dis => dis.Count.HasValue ? dis.Count.Value : 0)
                }).ToList();
        }

        public List<InventoryShipmentItem> GetMyInventoryShipments(long userId, long locationId, long taskId)
        {
            var shipments = Context.MCH_GetMyInventoryShipments(base.GetAppUserId(userId), locationId, taskId);

            return shipments.Select(shipment => new InventoryShipmentItem
            {
                AwbId = shipment.AwbId,
                Awb = shipment.Awb,
                HwbId = shipment.HwbId,
                Hwb = shipment.Hwb,
                Origin = shipment.Origin,
                Destination = shipment.Destination,
                Locations = shipment.Locations,
                TotalPieces = shipment.TotalPieces.HasValue ? shipment.TotalPieces.Value : 0,
                ScannedPieces = shipment.ScannedPieces.HasValue ? shipment.ScannedPieces.Value : 0,
                Weight = shipment.Weight.HasValue ? shipment.Weight.Value : 0,
                WeightUOM = shipment.WeightUOM
            }).ToList();
        }

        private string GetUserName(long userId)
        {
            var user = Context.UserProfiles.FirstOrDefault(u => u.Id == userId);
            return user == null ? null : user.UserId;
        }

        public void ResetInventory(long awbId, long hwbId, long userId, long taskId)
        {
            Context.MCH_ResetInventory(awbId, hwbId, taskId, base.GetAppUserId(userId));
        }

        public List<DiscrepancyItem> GetInventoryDiscrepancies(long taskId)
        {
            var item = Context.MCH_GetInventoryDiscrepancies(taskId);
            return item.Select(i => new DiscrepancyItem
            {
                Awb = i.AWB,
                AwbId = i.AwbId.GetValueOrDefault(),
                Origin = i.Origin,
                Destination = i.Destination,
                DiscrepancyCount = i.DiscrepancyCount.GetValueOrDefault(),
                Hwb = i.HWB,
                HwbId = i.HwbId.GetValueOrDefault(),
                TotalPieces = i.TPCS.GetValueOrDefault(),
                DiscrepancyType = i.Discrepancy
            }).ToList();
        }
    }
}
