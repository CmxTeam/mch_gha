﻿using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Department = MCH.BLL.Model.Common.DepartmentModel;
namespace MCH.BLL.Units
{
    public class CommonUnit : DataUnitOfWork
    {
        private GenericRepository<Carrier> carrierRepository;
        private GenericRepository<Status> statusRepository;
        private GenericRepository<Port> portRepository;
        private GenericRepository<UserProfile> userProfileRepository;
        private CustomersRepository customersRepository;

        public static string JoinSpecialhanglngCodes(SpecialHandling s)
        {
            string tmpResult = String.Empty;
            if (s.SpecialHandlingCode.IsPerishable.Value)
            {
                tmpResult = "PER";
            }
            else if (s.SpecialHandlingCode.IsHazmatCode.Value)
            {
                tmpResult = "HAZ";
            }
            else if (s.SpecialHandlingCode.IsHuman.Value)
            {
                tmpResult = "HUM";
            }
            else if (s.SpecialHandlingCode.IsTempControlled.Value)
            {
                tmpResult = "TEMP";
            }
            else if (s.SpecialHandlingCode.IsHighValue.Value)
            {
                tmpResult = "VAL";
            }
            else if (s.SpecialHandlingCode.IsLiveAnimal.Value)
            {
                tmpResult = "AVI";
            }
            return tmpResult;
        }

        public CustomersRepository CustomersRepository
        {
            get
            {
                if (this.customersRepository == null)
                    this.customersRepository = new CustomersRepository(base.DataContext);

                return this.customersRepository;
            }
        }

        public GenericRepository<Carrier> CarrierRepostiory
        {
            get
            {
                if (this.carrierRepository == null)
                    this.carrierRepository = new GenericRepository<Carrier>(base.DataContext);

                return this.carrierRepository;
            }
        }

        public GenericRepository<Status> StatusRepository
        {
            get
            {
                if (this.statusRepository == null)
                {
                    this.statusRepository = new GenericRepository<Status>(base.DataContext);
                }
                return this.statusRepository;
            }
        }

        public GenericRepository<Port> PortRepository
        {
            get
            {
                if (this.portRepository == null)
                {
                    this.portRepository = new GenericRepository<Port>(base.DataContext);
                }
                return this.portRepository;
            }
        }

        public GenericRepository<UserProfile> UserProfileRepository
        {
            get
            {
                if (this.userProfileRepository == null)
                {
                    this.userProfileRepository = new GenericRepository<UserProfile>(base.DataContext);
                }
                return this.userProfileRepository;
            }
        }

        public IQueryable<DepartmentModel> GetDepartments()
        {
            return this.Context.Departments.Select(d => new DepartmentModel { Id = d.Id, Name = d.Name });
        }

        public IQueryable<WarehouseModel> GetUserWarehouses(long shellUserId)
        {
            return this.Context.UserWarehouses.Where(w => w.UserProfile.ShellUserId == shellUserId).OrderBy(uw => uw.IsDefault).Select(uw => new WarehouseModel
            {
                Id = uw.Warehouse.Id,
                Name = uw.Warehouse.Code,
                PortCode = uw.Warehouse.Port != null ? uw.Warehouse.Port.IATACode : String.Empty
            });
        }

        public List<LocationItem> GetLocations(List<long> locationTypes)
        {
            return Context.WarehouseLocations.Where(w => locationTypes.Contains(w.WarehouseLocationType.Id))
                .Select(w => new LocationItem
                {
                    LocationId = w.Id,
                    Location = w.Location,
                    LocationBarcode = w.Barcode,
                    LocationPrefix = w.WarehouseLocationType.Code
                }).OrderBy(l=> l.Location).ToList();
        }

        public MCH.BLL.Model.Common.AwbModel GetAwbDetails(string serialNumber, string carrier3Code)
        {
            var awb = Context.AWBs.Where(a => a.AWBSerialNumber == serialNumber).OrderBy(a => a.RecDate).FirstOrDefault();
            if (awb == null) return null;
            var legSegment = awb.LegSegments.SingleOrDefault(l => l.Sequence == 1);
            DateTime? flightDate = null;
            string flightNumber = null;
            if (legSegment != null)
            {
                flightDate = legSegment.ETD;
                flightNumber = legSegment.FlightNumber;
            }

            var tmpShipperAwb = this.Context.ShipperManifests_AWBs.Where(item => item.AwbId == awb.Id);


            return new Model.Common.AwbModel
            {
                ConnectedToShipper = tmpShipperAwb.Any(),
                Id = awb.Id,
                AWBNumber = awb.AWBSerialNumber,
                CarrierId = awb.CarrierId.Value,
                CarrierNumber = awb.Carrier.Carrier3Code,
                Origin = awb.Port.IATACode,
                OriginId = awb.Port.Id,
                Destination = awb.Port1.IATACode,
                DestinationId = awb.Port1.Id,
                FlightDate = flightDate,
                FlightNumber = flightNumber,
                TotalPieces = awb.TotalPieces.HasValue ? awb.TotalPieces.Value : 0,
                TotalWeight = awb.TotalWeight.HasValue ? awb.TotalWeight.Value : 0,
                WeightUOM = awb.UOM.TwoCharCode,
                WeightUOMId = awb.UOM.Id
            };
        }

        public List<MCH.BLL.Model.Common.UldTypeItem> GetUldTypes(long warehouseId)
        {
            return Context.ULDTypes.Select(t => new MCH.BLL.Model.Common.UldTypeItem
            {
                UldType = t.Code,
                UldTypeId = t.Id
            }).ToList();
        }

        public List<PortItem> GetPorts()
        {
            return Context.Ports.Select(p => new PortItem
            {
                Id = p.Id,
                IATACode = p.IATACode,
                Port = p.Port1
            }).OrderBy(p=> p.IATACode).ToList();
        }

        public List<CarrierItem> GetCarrierList(long motId)
        {
            return Context.Carriers.Where(c => c.MOTId == motId).Select(c => new CarrierItem
            {
                Id = c.Id,
                CarrierName = c.CarrierName,
                CarrierCode = c.CarrierCode,
                Carrier3Code = c.Carrier3Code
            }).ToList();
        }

        public List<AccountCarrierItem> GetAccountCarriers(long? accountId)
        {
            return Context.Accounts.Where(a => !a.IsOwner && (!accountId.HasValue || accountId == 0 || a.ShellAccountId == accountId.Value))
                .SelectMany(a => a.AccountCarriers).Where(a => a.Carrier != null && a.IsActive == true).Select(a => new AccountCarrierItem
            {
                AccountId = a.AccountId.Value,
                CarrierId = a.CarrierId.Value,
                Carrier3Code = a.Carrier.Carrier3Code,
                CarrierCode = a.Carrier.CarrierCode,
                CarrierName = a.Carrier.CarrierName
            }).ToList();
        }

        public List<AccountingInfoIdentifierItem> GetAccountingCodes()
        {
            return Context.AccountingInfoIdentifiers.Select(a => new AccountingInfoIdentifierItem
            {
                Id = a.Id,
                Code = a.Code
            }).ToList();
        }

        public List<SearchableItem> GetCustomerList()
        {
            return Context.Customers.Select(c => new SearchableItem
            {
                Id = c.Id,
                SearchText = c.Name + "-" + c.City
            }).ToList();
        }

        public CustomerModel GetCustomerById(int id)
        {
            var item = Context.Customers.Where(a => a.Id == id).FirstOrDefault();
            if (item == null) return new CustomerModel();

            string fax = null;
            string phone = null;
            string email = null;
            var contact = item.Contacts.FirstOrDefault();
            if (contact != null)
            {
                fax = contact.Fax;
                phone = contact.Phone;
                email = contact.Email;
            }

            return new CustomerModel
            {
                Id = item.Id,
                AccountNumber = item.AccountNumber,
                Address1 = item.AddressLine1,
                City = item.City,
                CountryId = item.CountryId,
                StateId = item.StateId,
                Name = item.Name,
                Postal = item.PostalCode,
                Fax = fax,
                Phone = phone,
                Email = email,
                StateText = (item.State != null && item.State.TwoCharacterCode != null) ? item.State.TwoCharacterCode : "",
                CountryText = (item.Country != null && item.Country.TwoCharacterCode != null) ? item.Country.TwoCharacterCode : ""
            };
        }

        public List<SearchableItem> GetAgentList()
        {
            return Context.Agents.Select(c => new SearchableItem
            {
                Id = c.Id,
                SearchText = c.NumericCode + "-" + c.Name
            }).ToList();
        }

        public AgentModel GetAgentById(long id)
        {
            var item = Context.Agents.Where(a => a.Id == id).FirstOrDefault();
            if (item == null) return new AgentModel();

            return new AgentModel
            {
                Id = item.Id,
                AccountNumber = item.AccountNumber,
                CassAddress = item.CASSAddress,
                City = item.Place,
                IataCode = item.NumericCode,
                Name = item.Name,
                ParticipantIdentfierId = item.ParticipantId
            };
        }

        public List<IdCodeItem> GetSpecialHandlings()
        {
            return Context.SpecialHandlingCodes.Select(c => new IdCodeItem
            {
                Id = c.Id,
                Code = c.Code
            }).ToList();
        }

        public List<IdCodeItem> GetHarmonizedCodes()
        {
            return Context.ImportExportCodes.Select(c => new IdCodeItem
            {
                Id = c.Id,
                Code = c.Code
            }).ToList();
        }

        public List<SearchableItem> GetAwbs()
        {
            return Context.AWBs.Select(a => new SearchableItem
            {
                Id = a.Id,
                SearchText = a.Port.IATACode + "-" + a.Carrier.Carrier3Code + "-" + a.AWBSerialNumber + "-" + a.Port1.IATACode
            }).ToList();
        }

        public List<IdCodeItem> GetCountryCodes()
        {
            return Context.Countries.Select(c => new IdCodeItem
            {
                Id = c.Id,
                Code = c.TwoCharacterCode
            }).ToList();
        }

        public List<IdCodeItem> GetParticipantIdentifiers()
        {
            return Context.ParticipantIdentifiers.Select(p => new IdCodeItem
            {
                Id = p.Id,
                Code = p.Code
            }).ToList();
        }

        public List<IdCodeItem> GetInfoIdentifiers()
        {
            return Context.LineIdentifiers.Select(c => new IdCodeItem
            {
                Id = c.Id,
                Code = c.Code
            }).ToList();
        }

        public List<IdCodeItem> GetCustomsInfoIdentifiers()
        {
            return Context.CustomsInfoIdentifiers.Select(c => new IdCodeItem
            {
                Id = c.Id,
                Code = c.Code
            }).ToList();
        }

        public List<IdCodeItem> GetCurrencies()
        {
            return Context.Currencies.Select(c => new IdCodeItem
            {
                Id = c.Id,
                Code = c.Currency3Code,
                Symbol = c.CurrencySign
            }).ToList();
        }

        public List<StateCodeItem> GetStateCodes(long? countryId)
        {
            return Context.States.Where(s => !countryId.HasValue || s.CountryId == countryId).Select(s => new StateCodeItem
                {
                    Id = s.Id,
                    StateProvince = s.StateProvince,
                    TwoCharacterCode = s.TwoCharacterCode
                }).ToList();
        }

        public List<ChargeCodeModel> GetChargeCodes()
        {
            return this.Context.ChargeCodes.Select(d => new ChargeCodeModel { Id = d.Id, Code = d.Code, Description = d.Description, IsCodeDueCarrier = d.IsCodeDueCarrier }).ToList();
        }

        public List<IdCodeItem> GetShipmentUnitTypes()
        {
            return Context.ShipmentUnitTypes.Select(s => new IdCodeItem
            {
                Id = s.Id,
                Code = s.Code
            }).ToList();
        }

        public List<IdCodeItem> GetUldUnitTypes()
        {
            return Context.ULDTypes.Select(t => new IdCodeItem { Id = t.Id, Code = t.Code }).ToList();
        }

        public List<IdCodeItem> GetAircraftTypes()
        {
            return Context.AircraftTypes.Select(t => new IdCodeItem { Id = t.Id, Code = t.Description }).ToList();
        }

        public string GetAwbNumberById(long id)
        {
            var awb = Context.AWBs.SingleOrDefault(a => a.Id == id);
            if (awb == null) return null;
            return awb.Carrier.Carrier3Code + "-" + awb.AWBSerialNumber;
        }

        public string GetHwbNumberById(long id)
        {
            var hawb = Context.HWBs.SingleOrDefault(a => a.Id == id);
            if (hawb == null) return null;
            return hawb.HWBSerialNumber;
        }

        public int? GetForkliftCount(long taskId, long userId)
        {
            return Context.GetForkliftCounter(taskId, GetAppUserId(userId)).FirstOrDefault();
        }
    }
}
