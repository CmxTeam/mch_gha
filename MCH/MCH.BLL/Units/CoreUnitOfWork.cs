﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCH.BLL.DAL.Core;

namespace MCH.BLL.Units
{
    public class CoreUnitOfWork : IDisposable
    {
        protected readonly MCHCoreEntities DataContext = new MCHCoreEntities();

        public void Save()
        {
            DataContext.SaveChanges();
        }

        public MCHCoreEntities Context
        {
            get { return this.DataContext; }
        }

        #region IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    DataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
