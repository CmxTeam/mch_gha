﻿using MCH.BLL.Model.Snapshot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class SnapshotUnit : DataUnitOfWork
    {
        public List<ConditionType> GetConditionTypes(int companyId)
        {            
            return Context.Conditions.Select(c => new ConditionType 
            {
                Condition = c.Condition1,
                ConditionId = c.Id,
                IconName = string.Empty,
                IsDamage = c.isDamage,
                IsDefault = c.IsDefault
            }).ToList();
        }

        public SnapShotTask GetSnapShotTask(long taskId, long warehouseId, long userId, string barcode)
        {
            var data = Context.MCH_GetSnapShotTask(taskId, GetAppUserId(userId), barcode, warehouseId).Single();

            return new SnapShotTask
            {
                SnapShotTaskId = data.SnapshotTaskId.Value,
                Message = data.ErrMsg,
                SnapShotReference = data.SnapShotReference
            };
        }
       
        public void UploadSnapshotImage(SnapShotImageItem item)
        {
            var conditions = string.Join("|", item.SnapShotImageConditionItems.Select(c => c.ConditionId + "," + c.X + "," + c.Y));
            Context.MCH_UploadSnapshotImage(item.SnapshotTaskId, GetAppUserId(item.UserId), item.PieceCount, item.SnapshotImage, item.Latitude, item.Longitude, conditions);
        }       
    }
}
