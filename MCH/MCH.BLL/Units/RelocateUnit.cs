﻿using MCH.BLL.Model.Relocate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class RelocateUnit : DataUnitOfWork
    {
        public RelocateTaskItem GetRelocateTask(long warehouseId, long userId, string barcode)
        {
            long appUserId = GetAppUserId(userId);
            var data = Context.MCH_GetRelocateTask(appUserId, barcode, warehouseId);

            return data.Select(t => new RelocateTaskItem
            {
                 RelocateTaskId = t.RelocateTaskId,
                 Origin = t.Origin,
                 Destination = t.Destination,
                 Reference = t.Reference,
                 ReferenceId = t.ReferenceId,
                 ReferenceType = (ReferenceTypes)t.ReferenceTypeId,
                 Skids = t.Skids,
                 TotalPieces = t.TotalPiecies,
                 ScannedPieces = t.ScannedPieces,
                 Locations = t.Locations
            }).FirstOrDefault();
        }


        public void RelocatePieces(long referenceId, ReferenceTypes referenceType , long oldLocationId, long newLocationId, int pcs, long taskId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            Context.MCH_RelocatePieces(referenceId, (int)referenceType, oldLocationId, newLocationId, pcs, taskId, appUserId);
        }

    }
}
