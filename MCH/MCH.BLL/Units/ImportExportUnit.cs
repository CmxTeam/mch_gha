﻿using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCH.BLL.DAL.Core;

namespace MCH.BLL.Units
{
    public class ImportExportUnit : UnitOfWork
    {
        public string GetAWBImportControl(long awbId, string part)
        {
            string tmpImportControl = String.Empty;

            var tmpAwb = this.DataContext.AWBs.SingleOrDefault(item => item.Id == awbId);

            string tmpAWBNumber = tmpAwb.Carrier.Carrier3Code + "" + tmpAwb.AWBSerialNumber;

            var tmpWaybillIds = this.CoreContext.Waybills.Where(wb => wb.Master == tmpAWBNumber).Select(wy => wy.WaybillID).ToList();

            var tmpImportQuery = this.CoreContext.Statuses.Where(ds => ds.CustomsCode == "1D" &&
                ds.Detail.House == "" &&
                tmpWaybillIds.Contains(ds.Detail.WaybillID));

            if (!String.IsNullOrWhiteSpace(part))
            {
                tmpImportQuery = tmpImportQuery.Where(ds => ds.Detail.PartialIdCode == part);
            }

            var tmpImportResult = tmpImportQuery.OrderByDescending(ds => ds.TransactionDateTime).Select(DS =>
            new
            {
                EntryType = DS.EntryType,
                EntryNo = DS.EntryNo
            }).FirstOrDefault();

            if (tmpImportResult != null)
            {
                string tmpEntryType = String.Empty;

                if (tmpImportResult.EntryType == "61")
                {
                    tmpEntryType = "IT";
                }
                else if (tmpImportResult.EntryType == "62") 
                {
                    tmpEntryType = "IE";
                }
                else if (tmpImportResult.EntryType == "63")
                {
                    tmpEntryType = "TE";
                }

                tmpImportControl = tmpEntryType + " " + tmpImportResult.EntryNo;
            }

            return tmpImportControl;
        }

        public string GetAWBExportControl(long awbId)
        {
            string tmpExport = String.Empty;
            var tmpAwb = this.DataContext.AWBs.SingleOrDefault(item => item.Id == awbId);

            if (tmpAwb != null)
            {
                tmpExport = String.Join(",", this.DataContext.AWBs_HWBs.Where(item => item.AWBId == awbId).Select(e => e.HWB.ExportControl).ToList());
            }

            return tmpExport;
        }
    }
}
