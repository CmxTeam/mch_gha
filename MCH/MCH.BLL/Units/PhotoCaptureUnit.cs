﻿using MCH.BLL.PhotoCaptureServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace MCH.BLL.Units
{
    public class PhotoCaptureUnit : DataUnitOfWork
    {

        public List<XmlNode> GetTaskMedia(long taskId)
        {
            List<XmlNode> tmpImages = new List<XmlNode>();
            using (PhotoCaptureSoapClient tmpProxy = new PhotoCaptureSoapClient())
            {
                var tmpTaskRefs = base.Context.TaskSnapshotReferences.Where(item => item.TaskId == taskId);
                var tmpTask = base.DataContext.Tasks.SingleOrDefault(item => item.Id == taskId);
                if (tmpTask != null && tmpTaskRefs.Any())
                {
                    foreach (var refItems in tmpTaskRefs)
                    {
                        var tmpNode = tmpProxy.ExtractAllToClient(tmpTask.Warehouse.WarehouseName, refItems.Reference);
                        var tmpNodeList = tmpNode.SelectNodes("InnerImage");
                        if (tmpNodeList.Count > 0) 
                        {
                            foreach (XmlNode innerNode in tmpNodeList)
                            {
                                tmpImages.Add(innerNode);
                            }
                        }
                        
                    }

                }
            }

            return tmpImages;
        }
    }
}
