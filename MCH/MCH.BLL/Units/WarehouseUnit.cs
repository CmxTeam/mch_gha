﻿using CMX.Shell.ServiceProxy.GlobalSettingsService;
using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.Warehouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class WarehouseUnit : DataUnitOfWork
    {

        public long SaveWarehouseLocation(WarehouseLocationBo model)
        {
            if (model.LocationId.HasValue && model.LocationId.Value != 0)
            {
                //edit
                var tmpLocation = Context.WarehouseLocations.SingleOrDefault(w => w.Id == model.LocationId);
                if (tmpLocation != null)
                {
                    var tmpExistingLocation = Context.WarehouseLocations.FirstOrDefault(w => w.Location == model.LocationName.ToUpper() && w.WarehouseId == model.WarehouseId && w.ZoneId == model.ZoneId && w.LocationTypeId == model.LocationTypeId && w.Barcode == model.Barcode.ToUpper() && w.Id != model.LocationId);

                    if (tmpExistingLocation != null)
                    {
                        return -2;
                    }

                    tmpLocation.Barcode = model.Barcode.ToUpper();
                    tmpLocation.Location = model.LocationName.ToUpper();
                    tmpLocation.WarehouseId = (int)model.WarehouseId;
                    tmpLocation.LocationTypeId = (int)model.LocationTypeId;
                    tmpLocation.ZoneId = model.ZoneId;

                    Context.SaveChanges();
                }
            }
            else
            {
                var tmpExistingLocation = Context.WarehouseLocations.FirstOrDefault(w => w.Location == model.LocationName.ToUpper() && w.WarehouseId == model.WarehouseId && w.ZoneId == model.ZoneId && w.LocationTypeId == model.LocationTypeId && w.Barcode == model.Barcode.ToUpper());

                if (tmpExistingLocation != null)
                {
                    return -2;
                }

                var tmpNewLocation = new WarehouseLocation();
                tmpNewLocation.Barcode = model.Barcode.ToUpper();
                tmpNewLocation.Location = model.LocationName.ToUpper();
                tmpNewLocation.WarehouseId = (int)model.WarehouseId;
                tmpNewLocation.LocationTypeId = (int)model.LocationTypeId;
                tmpNewLocation.ZoneId = model.ZoneId;

                Context.WarehouseLocations.Add(tmpNewLocation);
                Context.SaveChanges();
                model.LocationId = tmpNewLocation.Id;
            }

            return model.LocationId.Value;
        }

        public long SaveWarehouseZone(WarehouseZoneBo model)
        {
            if (model.ZoneId.HasValue && model.ZoneId.Value != 0)
            {
                //edit
                var tmpZone = Context.WarehouseZones.SingleOrDefault(w => w.Id == model.ZoneId);
                if (tmpZone != null)
                {
                    var tmpExistingZone = Context.WarehouseZones.FirstOrDefault(w => w.Zone == model.Name.ToUpper() && w.WarehouseId == model.WarehouseId && w.Id != model.ZoneId);

                    if (tmpExistingZone != null)
                    {
                        return -2;
                    }

                    tmpZone.Zone = model.Name.ToUpper();
                    tmpZone.WarehouseId = (int)model.WarehouseId;

                    Context.SaveChanges();
                }
            }
            else
            {
                //add new 
                var tmpExistingZone = Context.WarehouseZones.FirstOrDefault(w => w.Zone == model.Name.ToUpper() && w.WarehouseId == model.WarehouseId);

                if (tmpExistingZone != null)
                {
                    return -2;
                }

                var tmpNewZone = new WarehouseZone();
                tmpNewZone.WarehouseId = (int)model.WarehouseId;
                tmpNewZone.Zone = model.Name.ToUpper();

                Context.WarehouseZones.Add(tmpNewZone);
                Context.SaveChanges();
                model.ZoneId = tmpNewZone.Id;
            }

            return model.ZoneId.Value;
        }

        public long SaveWarehouse(WarehouseBo model)
        {
            if (model.Id.HasValue && model.Id.Value != 0)
            {
                //edit
                var tmpWarehouse = Context.Warehouses.SingleOrDefault(w => w.Id == model.Id);
                if (tmpWarehouse != null)
                {
                    var tmpExistingWarehouse = Context.Warehouses.FirstOrDefault(w => w.WarehouseName == model.Name && w.Id != model.Id.Value);

                    if (tmpExistingWarehouse != null)
                    {
                        return -2;
                    }

                    var tmpWarehouselinked = Context.Warehouses.FirstOrDefault(w => w.ShellWarehouseId == model.ShellWarehouseId && w.Id != model.Id.Value);
                    if (tmpWarehouselinked != null)
                    {
                        return -3;
                    }

                    tmpWarehouse.WarehouseName = model.Name;
                    tmpWarehouse.Address = model.Address1;
                    tmpWarehouse.Address2 = model.Address2;
                    tmpWarehouse.City = model.City;
                    tmpWarehouse.Code = model.Code;
                    tmpWarehouse.ContactEmail = model.ContactEmail;
                    tmpWarehouse.CountryId = model.CountryId;

                    if (!string.IsNullOrWhiteSpace(model.GeoLocation))
                    {
                        string tmpLatitude = model.GeoLocation.Split(',')[0];

                        tmpWarehouse.Latitude = tmpLatitude;

                        tmpLatitude = model.GeoLocation.Split(',')[1];

                        tmpWarehouse.Longitude = tmpLatitude;

                    }

                    tmpWarehouse.Phone = model.Phone;
                    tmpWarehouse.PortId = model.PortId;
                    tmpWarehouse.PostalCode = model.PostalCode;
                    tmpWarehouse.StateId = model.SateId;
                    tmpWarehouse.Supervisor = model.Supervisor;
                    tmpWarehouse.ShellWarehouseId = model.ShellWarehouseId;

                    Context.SaveChanges();
                }
            }
            else
            {
                //add new 
                var tmpExistingWarehouse = Context.Warehouses.FirstOrDefault(w => w.WarehouseName == model.Name);

                if (tmpExistingWarehouse != null)
                {
                    return -2;
                }

                var tmpWarehouselinked = Context.Warehouses.FirstOrDefault(w => w.ShellWarehouseId == model.ShellWarehouseId);
                if (tmpWarehouselinked != null)
                {
                    return -3;
                }

                var tmpNewWarehouse = new Warehouse();

                tmpNewWarehouse.WarehouseName = model.Name;
                tmpNewWarehouse.Address = model.Address1;
                tmpNewWarehouse.Address2 = model.Address2;
                tmpNewWarehouse.City = model.City;
                tmpNewWarehouse.Code = model.Code;
                tmpNewWarehouse.ContactEmail = model.ContactEmail;
                tmpNewWarehouse.CountryId = model.CountryId;

                if (!string.IsNullOrWhiteSpace(model.GeoLocation))
                {
                    string tmpLatitude = model.GeoLocation.Split(',')[0];

                    tmpNewWarehouse.Latitude = tmpLatitude;

                    tmpLatitude = model.GeoLocation.Split(',')[1];

                    tmpNewWarehouse.Longitude = tmpLatitude;
                }

                tmpNewWarehouse.Phone = model.Phone;
                tmpNewWarehouse.PortId = model.PortId;
                tmpNewWarehouse.PostalCode = model.PostalCode;
                tmpNewWarehouse.StateId = model.SateId;
                tmpNewWarehouse.Supervisor = model.Supervisor;

                Context.Warehouses.Add(tmpNewWarehouse);
                Context.SaveChanges();
                model.Id = tmpNewWarehouse.Id;
            }

            return model.Id.Value;
        }


        #region Shell Warehouse Methods
        public List<SimpleListItem> GetShellWarehoues()
        {
            List<SimpleListItem> tmpResults = new List<SimpleListItem>();
            using (GlobalSettingsServiceClient shellSettingsClient = new GlobalSettingsServiceClient())
            {
                var tmpTransWarehouses = shellSettingsClient.GetWarehouses();

                if (tmpTransWarehouses != null && tmpTransWarehouses.Data != null)
                {
                    tmpResults = tmpTransWarehouses.Data.Select(item => new SimpleListItem
                    {
                        Id = item.Id,
                        Name = item.Name
                    }).ToList();
                }
            }

            return tmpResults;
        }

        public List<SimpleListItem> GetShellSites()
        {
            List<SimpleListItem> tmpResults = new List<SimpleListItem>();
            using (GlobalSettingsServiceClient shellSettingsClient = new GlobalSettingsServiceClient())
            {
                var tmpTransSites = shellSettingsClient.GetSites();

                if (tmpTransSites != null && tmpTransSites.Data != null)
                {
                    tmpResults = tmpTransSites.Data.Select(item => new SimpleListItem
                    {
                        Id = item.Id,
                        Name = item.Name
                    }).ToList();
                }
            }

            return tmpResults;
        }

        public List<SimpleListItem> GetShellUserSites(long argUserId)
        {
            List<SimpleListItem> tmpResults = new List<SimpleListItem>();
            using (GlobalSettingsServiceClient shellSettingsClient = new GlobalSettingsServiceClient())
            {
                var tmpTransSites = shellSettingsClient.GetUserSites(argUserId);

                if (tmpTransSites != null && tmpTransSites.Data != null)
                {
                    tmpResults = tmpTransSites.Data.Select(item => new SimpleListItem
                    {
                        Id = item.Id,
                        Name = item.Name
                    }).ToList();
                }
            }

            return tmpResults;
        }

        public List<SimpleListItem> GetShellSiteWarehoues(long siteId)
        {
            List<SimpleListItem> tmpResults = new List<SimpleListItem>();
            using (GlobalSettingsServiceClient shellSettingsClient = new GlobalSettingsServiceClient())
            {
                var tmpTransWarehouses = shellSettingsClient.GetSiteWarehouses(siteId);

                if (tmpTransWarehouses != null && tmpTransWarehouses.Data != null)
                {
                    tmpResults = tmpTransWarehouses.Data.Select(item => new SimpleListItem
                    {
                        Id = item.Id,
                        Name = item.Name
                    }).ToList();
                }
            }

            return tmpResults;
        }

        public List<SimpleListItem> GetShellUserWarehoues(long userId)
        {
            List<SimpleListItem> tmpResults = new List<SimpleListItem>();
            using (GlobalSettingsServiceClient shellSettingsClient = new GlobalSettingsServiceClient())
            {

                var tmpTransWarehouses = shellSettingsClient.GetUserWarehouses(userId);

                if (tmpTransWarehouses != null && tmpTransWarehouses.Data != null)
                {
                    tmpResults = tmpTransWarehouses.Data.Select(item => new SimpleListItem
                    {
                        Id = item.Id,
                        Name = item.Name
                    }).ToList();
                }
            }

            return tmpResults;
        }
        #endregion
    }
}
