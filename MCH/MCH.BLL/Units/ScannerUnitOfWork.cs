﻿using MCH.BLL.DAL;
using MCH.BLL.Model.DataContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using MCH.BLL.Model.Enum;
using MCH.BLL.Model.Common;

namespace MCH.BLL.Units
{
    public class ScannerUnitOfWork : DataUnitOfWork
    {

        public UserTaskCounter[] GetUserTaskCounts(long argUserId, int[] argTaskTypes, long? argWarehouseId)
        {
            List<UserTaskCounter> tmpTaskCounters = new List<UserTaskCounter>();

            if (argTaskTypes != null && argTaskTypes.Any())
            {
                var tmpUser = this.DataContext.UserProfiles.SingleOrDefault(item => item.ShellUserId == argUserId);

                if (tmpUser != null)
                {
                    var tmpOpenAndAssignedTasks = tmpUser.TaskAssignments.Select(item => item.Task).GroupBy(item => item.Id).Select(g => g.FirstOrDefault())
                        .Where(item => item.TaskTypeId.HasValue && argTaskTypes.Contains(item.TaskTypeId.Value) && (item.StatusId == (int)enumTaskStatuses.NotStarted || item.StatusId == (int)enumTaskStatuses.InProgress));

                    if (argWarehouseId.HasValue && argWarehouseId.Value != 0)
                    {
                        tmpOpenAndAssignedTasks = tmpOpenAndAssignedTasks.Where(item => item.WarehouseId == argWarehouseId);
                    }

                    if (tmpOpenAndAssignedTasks.Any())
                    {
                        tmpTaskCounters = tmpOpenAndAssignedTasks
                        .GroupBy(item => item.TaskTypeId).Select(item => new UserTaskCounter { OpenAssignedTaskCount = item.Count(), TaskTypeId = item.Key.Value }).ToList();
                    }
                }
            }

            return tmpTaskCounters.ToArray();
        }

        public WarehouseSimpleModel[] GetUserWarehouses(long[] argWarehouseIds, long argUserId)
        {
            List<WarehouseSimpleModel> tmpWarehouses = new List<WarehouseSimpleModel>();


            if (argWarehouseIds != null)
            {
                var tmpWarehouseData = this.DataContext.Warehouses.Where(item => item.ShellWarehouseId.HasValue && argWarehouseIds.Contains(item.ShellWarehouseId.Value)).Select(item => new WarehouseSimpleModel { Id = item.Id, Code = item.Code, ExternalId = item.ShellWarehouseId });

                if (tmpWarehouseData.Any())
                {
                    tmpWarehouses = tmpWarehouseData.ToList();
                }
            }

            return tmpWarehouses.ToArray();
        }

        public WarehouseLocationModel[] GetWarehouseLocations(string station, LocationType location)
        {
            var query = DataContext.WarehouseLocations.Where(wl => wl.Warehouse.Port.IATACode == station && wl.LocationTypeId != 7).Select(wl => new
            {
                LocationId = wl.Id,
                Location = wl.Location,
                LocationBarcode = wl.Barcode,
                LocationType = wl.WarehouseLocationType.LocationType
            }).ToList();

            return query.Select(i => new WarehouseLocationModel
            {
                LocationId = i.LocationId,
                Location = i.Location,
                LocationBarcode = i.LocationBarcode,
                LocationType = i.LocationType == null ? LocationType.NA : (LocationType)Enum.Parse(typeof(LocationType), i.LocationType)
            }).ToArray();
        }

        public LocationItem GetLocationIdByLocationBarcode(string station, string barcode)
        {
            var loc = DataContext.WarehouseLocations.FirstOrDefault(l => (l.Barcode == barcode || l.Location == barcode) && l.Warehouse.Port.IATACode == station);
            return loc == null ? null : new LocationItem
            {
                LocationId = loc.Id,
                Location = loc.Location,
                LocationBarcode = loc.Barcode,
                LocationPrefix = loc.WarehouseLocationType.Code
            };
        }

        public void AddTaskSnapshotReference(long taskId, string reference)
        {
            DataContext.AddTaskSnapshotReference(taskId, reference);
        }

        public bool ExecuteNonQuery(string station, string query)
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings[station].ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                    finally
                    {
                        if (connection.State != System.Data.ConnectionState.Closed)
                            connection.Close();
                    }
                }
            }
        }

        public DataTable ExecuteQuery(string station, string query)
        {
            try
            {
                var datatable = new DataTable();
                datatable.TableName = "Data";
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings[station].ConnectionString))
                {
                    using (var adapter = new SqlDataAdapter(query, connection))
                    {
                        adapter.Fill(datatable);
                    }
                }
                return datatable;
            }
            catch
            {
                return null;
            }
        }

        public AwbInfo GetAwbInfo(string station, string shipment)
        {
            shipment = shipment.Replace(" ", "").Replace("-", "").ToLower();

            var awb = DataContext.AWBs.Where(a => a.Carrier.Carrier3Code + a.AWBSerialNumber == shipment).ToList().LastOrDefault();
            if (awb == null)
                return null;
            var alerts = DataContext.GetAwbAlerts(station, awb.Id, false).ToList();
            return new AwbInfo
            {
                AwbId = awb.Id,
                SerialNumber = awb.AWBSerialNumber,
                Carrier = awb.Carrier.CarrierCode,
                Pieces = awb.TotalPieces.HasValue ? awb.TotalPieces.Value : 0,
                Origin = awb.Port.IATACode,
                Destination = awb.Port1.IATACode,
                HasAlarm = alerts.Any(),
                //Alert = alerts.Select(a => new Models.Alert 
                //{
                //    Date = a.AlertDate.HasValue ? a.AlertDate.Value : DateTime.UtcNow,
                //    Message = a.AlertMessage,
                //    SetBy = a.SetBy
                //}).ToArray()
            };
        }

        public List<PrinterItem> GetPrinters(long userId, PrinterTypes printerType)
        {
            long appUserId = GetAppUserId(userId);
            return Context.Printers.Where(p => p.UserPrinters.Any(up => up.UserId == appUserId) && p.PrinterType.Id == (int)printerType).Select(p => new PrinterItem
            {
                PrinterId = p.Id,
                IsDefault = p.IsDefault,
                PrinterName = p.Name,
                PrinterType = (PrinterTypes)p.PrinterType.Id
            }).ToList();
        }

        #region [ -- Private Methods -- ]

        private string EncodePassword(string pass, string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            byte[] saltString = Convert.FromBase64String(salt);
            byte[] result;

            var hashAlgorithm = this.GetHashAlgorithm();

            if (hashAlgorithm is KeyedHashAlgorithm)
            {
                var keyedHashAlgorithm = (KeyedHashAlgorithm)hashAlgorithm;
                if (keyedHashAlgorithm.Key.Length == saltString.Length)
                    keyedHashAlgorithm.Key = saltString;
                else if (keyedHashAlgorithm.Key.Length < saltString.Length)
                {
                    byte[] keyArray = new byte[keyedHashAlgorithm.Key.Length];
                    Buffer.BlockCopy((Array)saltString, 0, (Array)keyArray, 0, keyArray.Length);
                    keyedHashAlgorithm.Key = keyArray;
                }
                else
                {
                    byte[] keyArray = new byte[keyedHashAlgorithm.Key.Length];
                    int dstOffset = 0;
                    while (dstOffset < keyArray.Length)
                    {
                        int count = Math.Min(saltString.Length, keyArray.Length - dstOffset);
                        Buffer.BlockCopy((Array)saltString, 0, (Array)keyArray, dstOffset, count);
                        dstOffset += count;
                    }
                    keyedHashAlgorithm.Key = keyArray;
                }
                result = keyedHashAlgorithm.ComputeHash(bytes);
            }
            else
            {
                byte[] buffer = new byte[saltString.Length + bytes.Length];
                Buffer.BlockCopy((Array)saltString, 0, (Array)buffer, 0, saltString.Length);
                Buffer.BlockCopy((Array)bytes, 0, (Array)buffer, saltString.Length, bytes.Length);
                result = hashAlgorithm.ComputeHash(buffer);
            }

            return Convert.ToBase64String(result);
        }

        private HashAlgorithm GetHashAlgorithm()
        {
            return HashAlgorithm.Create("SHA1");
        }



        #endregion


    }
}
