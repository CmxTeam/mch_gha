﻿using CMX.Framework.Utils.Security;
using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Model.Enum;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;

namespace MCH.BLL.Units
{
    public class ReceiverUnitOfWork : DataUnitOfWork
    {
        public CargoReceiverFlight[] GetCargoReceiverFlights(string station, int userID, TaskStatuses status, string carrierNo, string origin, string flightNo, ReceiverSortFields sortBy)
        {
            long appUserId = GetAppUserId(userID);
            var warehouseId = GetWarehouseId(station);
            var data = DataContext.GetCargoReceiverFlights(warehouseId, string.IsNullOrEmpty(carrierNo) ? null : carrierNo,
                string.IsNullOrEmpty(origin) ? null : origin, string.IsNullOrEmpty(flightNo) ? null : flightNo, (int)status, (int)appUserId, (int)sortBy);
            return data.Select(d => new CargoReceiverFlight
            {
                CarrierLogo = d.CarrierLogo,
                CarrierName = d.CarrierName,
                CarrierCode = d.CarrierCode,
                FlightNumber = d.FlightNumber,
                Origin = d.Origin,
                ULDCount = d.UldCount.Value,
                AWBCount = d.AWBCount.Value,
                TotalPieces = d.TotalPieces.Value,
                FlightManifestId = d.FlightManifestId.Value,
                RecoverLocation = d.RecoveredLocation,
                RecoveredULDs = d.RecoveredUlds.Value,
                RecoveredBy = d.RecoeveredBy,
                ReceivedPieces = d.ReceivedPieces.HasValue ? d.ReceivedPieces.Value : 0,
                TaskId = d.TaskId.Value,
                RecoveredDate = d.RecoveredDate,
                Flag = d.Flags.HasValue ? d.Flags.Value : 0,
                ETA = d.Eta,
                Status = (TaskStatuses)d.StatusId,
                References = d.Refs.Split(',')
            }).ToArray();
        }

        public TransactionStatus RecoverFlight(string station, long taskId, long userId)
        {
            try
            {
                long appUserId = GetAppUserId(userId);
                DataContext.RecoverFlight(appUserId, taskId);
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }

        }

        public FlightUldInfo GetFlightULDs(long flightManifestId, RecoverStatuses status, string filter)
        {
            var manifest = DataContext.FlightManifests.Single(fm => fm.Id == flightManifestId);

            var ulds = manifest.ULDs
                .Where(u => string.IsNullOrEmpty(filter) || u.SerialNumber.Contains(filter) || u.ShipmentUnitType.Code.Contains(filter)).ToList()
                .Select(u => new Uld
                {
                    UldId = u.Id,
                    UldType = u.ULDType != null ? u.ULDType.Code : string.Empty,
                    UldPrefix = u.ShipmentUnitType.Code,
                    UldSerialNo = u.SerialNumber,
                    CarrierCode = u.Carrier == null ? string.Empty : u.Carrier.CarrierCode,
                    TotalUnitCount = u.TotalUnitCount.HasValue ? u.TotalUnitCount.Value : 0,
                    IsBUP = u.IsBUP.HasValue ? u.IsBUP.Value : false,
                    Location = u.WarehouseLocation == null ? "N/A" : u.WarehouseLocation.Location,
                    PercentRecovered = u.PercentRecovered.HasValue ? u.PercentRecovered.Value : 0,
                    RecoveredCount = u.TotalReceivedCount.HasValue ? u.TotalReceivedCount.Value : 0,
                    TotalPieces = u.TotalPieces.HasValue ? u.TotalPieces.Value : 0,
                    ReceivedPieces = u.ReceivedPieces.HasValue ? u.ReceivedPieces.Value : 0,
                    PercentReceived = new Func<double>(() =>
                    {
                        var awbsCount = u.ManifestAWBDetails.Count();
                        return awbsCount == 0 ? 0 : u.ManifestAWBDetails.Sum(a => !a.ReceivedCount.HasValue || !a.LoadCount.HasValue || a.LoadCount.Value == 0 ? 0 : (Math.Min(a.ReceivedCount.Value / (double)a.LoadCount.Value * 100, 100))) / awbsCount;
                    })(),
                    //Status = u.RecoveredOn == null ? RecoverStatuses.Pending : (u.RecoveredOn != null && u.ReceivedPieces < u.TotalPieces ? RecoverStatuses.InProgress : (u.RecoveredOn != null && u.TotalPieces == u.ReceivedPieces ? RecoverStatuses.Complete : RecoverStatuses.NA)),
                    Status = new Func<RecoverStatuses>(() =>
                    {
                        var awbsCount = u.ManifestAWBDetails.Count();
                        var progress = awbsCount == 0 ? 0 : u.ManifestAWBDetails.Sum(a => !a.ReceivedCount.HasValue || !a.LoadCount.HasValue || a.LoadCount.Value == 0 ? 0 : (Math.Min(a.ReceivedCount.Value / (double)a.LoadCount.Value * 100, 100))) / u.ManifestAWBDetails.Count();
                        return u.RecoveredOn == null ? RecoverStatuses.Pending : (progress < 100 ? RecoverStatuses.InProgress : RecoverStatuses.Complete);
                    })(),
                    References = new Func<string[]>(() =>
                    {
                        var hwbs = DataContext.ManifestDetails.Where(h => h.ULDId == u.Id).Select(h => h.HWB.HWBSerialNumber).ToList();
                        var customers = DataContext.ManifestAWBDetails.Where(a => a.ULDId == u.Id).Select(a => a.AWB.Customer.Name).ToList();
                        hwbs.AddRange(customers);
                        return hwbs.ToArray();
                    })(),
                    Flag = DataContext.GetFlagsSP(u.Id, "uld", null).Single().Value
                })
                .Where(u => status == RecoverStatuses.All ||
                    (status == u.Status) ||
                    (status == RecoverStatuses.NotCompleted && (u.Status != RecoverStatuses.Complete)))
                .OrderBy(u => u.CarrierCode).ThenBy(u => u.UldType).ThenBy(u => u.UldSerialNo).ToArray();

            return new FlightUldInfo
            {
                FlightManifestId = flightManifestId,
                PercentRecovered = manifest.PercentRecovered.HasValue ? manifest.PercentRecovered.Value : 0,
                RecoveredUlds = manifest.RecoveredULDs.HasValue ? manifest.RecoveredULDs.Value : 0,
                TotalUlds = manifest.TotalULDs.HasValue ? manifest.TotalULDs.Value : 0,
                TotalPieces = manifest.TotalPieces.HasValue ? manifest.TotalPieces.Value : 0,
                PercentPutAway = manifest.PercentPutAway.HasValue ? manifest.PercentPutAway.Value : 0,
                PutAwayPieces = manifest.PutAwayPieces.HasValue ? manifest.PutAwayPieces.Value : 0,
                Ulds = ulds
            };
        }

        public FlightUldInfo GetFlightULD(long flightManifestId, long uldId)
        {
            var manifest = DataContext.FlightManifests.Single(fm => fm.Id == flightManifestId);
            var ulds = manifest.ULDs
                .Where(u => u.Id == uldId).ToList()
                .Select(u => new Uld
                {
                    UldId = u.Id,
                    UldType = u.ULDType != null ? u.ULDType.Code : string.Empty,
                    UldPrefix = u.ShipmentUnitType.Code,
                    UldSerialNo = u.SerialNumber,
                    CarrierCode = u.Carrier == null ? string.Empty : u.Carrier.CarrierCode,
                    TotalUnitCount = u.TotalUnitCount.HasValue ? u.TotalUnitCount.Value : 0,
                    IsBUP = u.IsBUP.HasValue ? u.IsBUP.Value : false,
                    Location = u.WarehouseLocation == null ? "N/A" : u.WarehouseLocation.Location,
                    PercentRecovered = u.PercentRecovered.HasValue ? u.PercentRecovered.Value : 0,
                    RecoveredCount = u.TotalReceivedCount.HasValue ? u.TotalReceivedCount.Value : 0,
                    TotalPieces = u.TotalPieces.HasValue ? u.TotalPieces.Value : 0,
                    ReceivedPieces = u.ReceivedPieces.HasValue ? u.ReceivedPieces.Value : 0,
                    PercentReceived = new Func<double>(() =>
                    {
                        var awbsCount = u.ManifestAWBDetails.Count();
                        return awbsCount == 0 ? 0 : u.ManifestAWBDetails.Sum(a => !a.ReceivedCount.HasValue || !a.LoadCount.HasValue || a.LoadCount.Value == 0 ? 0 : (Math.Min(a.ReceivedCount.Value / (double)a.LoadCount.Value * 100, 100))) / awbsCount;
                    })(),
                    //Status = u.RecoveredOn == null ? RecoverStatuses.Pending : (u.RecoveredOn != null && u.ReceivedPieces < u.TotalPieces ? RecoverStatuses.InProgress : (u.RecoveredOn != null && u.TotalPieces == u.ReceivedPieces ? RecoverStatuses.Complete : RecoverStatuses.NA)),
                    Status = new Func<RecoverStatuses>(() =>
                    {
                        var awbsCount = u.ManifestAWBDetails.Count();
                        var progress = awbsCount == 0 ? 0 : u.ManifestAWBDetails.Sum(a => !a.ReceivedCount.HasValue || !a.LoadCount.HasValue || a.LoadCount.Value == 0 ? 0 : (Math.Min(a.ReceivedCount.Value / (double)a.LoadCount.Value * 100, 100))) / u.ManifestAWBDetails.Count();
                        return u.RecoveredOn == null ? RecoverStatuses.Pending : (progress < 100 ? RecoverStatuses.InProgress : RecoverStatuses.Complete);
                    })(),
                    References = new Func<string[]>(() =>
                    {
                        var hwbs = DataContext.ManifestDetails.Where(h => h.ULDId == u.Id).Select(h => h.HWB.HWBSerialNumber).ToList();
                        var customers = DataContext.ManifestAWBDetails.Where(a => a.ULDId == u.Id).Select(a => a.AWB.Customer.Name).ToList();
                        hwbs.AddRange(customers);
                        return hwbs.ToArray();
                    })(),
                    Flag = DataContext.GetFlagsSP(u.Id, "uld", null).Single().Value
                }).OrderBy(u => u.CarrierCode).ThenBy(u => u.UldType).ThenBy(u => u.UldSerialNo).ToArray();

            return new FlightUldInfo
            {
                FlightManifestId = flightManifestId,
                PercentRecovered = manifest.PercentRecovered.HasValue ? manifest.PercentRecovered.Value : 0,
                RecoveredUlds = manifest.RecoveredULDs.HasValue ? manifest.RecoveredULDs.Value : 0,
                TotalUlds = manifest.TotalULDs.HasValue ? manifest.TotalULDs.Value : 0,
                TotalPieces = manifest.TotalPieces.HasValue ? manifest.TotalPieces.Value : 0,
                PercentPutAway = manifest.PercentPutAway.HasValue ? manifest.PercentPutAway.Value : 0,
                PutAwayPieces = manifest.PutAwayPieces.HasValue ? manifest.PutAwayPieces.Value : 0,
                Ulds = ulds
            };
        }

        public TransactionStatus SwitchBUPMode(string station, long uldId, long userId, long manifestId)
        {
            try
            {
                long appUserId = GetAppUserId(userId);
                var uld = DataContext.ULDs.Single(u => u.Id == uldId);
                uld.IsBUP = uld.IsBUP.HasValue ? !uld.IsBUP.Value : true;
               
                //TODO: Harut fix task
                var transaction = new Transaction
                {
                    EntityTypeId = 4,
                    EntityId = uld.Id,
                    StatusId = 2,
                    StatusTimestamp = DateTime.UtcNow,                    
                    Reference = "ULD# " + uld.ShipmentUnitType.Code + uld.SerialNumber,
                    Description = "ULD Mode switched to not BUP",
                    TaskTypeId = 1,
                    TransactionActionId = 2,
                    TaskId = DataContext.Tasks.Single(t => t.EntityTypeId == 1 && t.EntityId == manifestId && 
                                                      t.TaskTypeId == 1 && t.Warehouse.Port.IATACode == station).Id,
                    UserId = appUserId
                };

                if (uld.IsBUP.Value)
                    transaction.Description = "ULD Mode switched to BUP";

                DataContext.Transactions.Add(transaction);
                DataContext.SaveChanges();

                if (uld.LocationId.HasValue)
                    DataContext.MCH_ULDRecoverUpdateForBUP(uldId, manifestId, appUserId);


                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        public TransactionStatus UpdateFlightETA(string station, long flightManifestId, long taskId, long userId, DateTime eta)
        {
            try
            {
                long appUserId = GetAppUserId(userId);
                var unloadingPort = DataContext.UnloadingPorts.Where(p => p.FlightManifestId == flightManifestId && p.Port.IATACode == station).Single();
                unloadingPort.POU_ETA = eta;

                var task = DataContext.Tasks.Where(t => t.Id == taskId).Single();
                task.DueDate = eta.AddHours(4);

                var flightManifest = DataContext.FlightManifests.Where(f => f.Id == flightManifestId).Single();

                var taskTransaction = new TaskTransaction
                {
                    RecDate = DateTime.UtcNow,
                    StatusTimestamp = DateTime.UtcNow,
                    TaskId = taskId,
                    UserId = appUserId,
                    Status = DataContext.Statuses.SingleOrDefault(s => s.Name == "Task Update"),
                    Description = "Flight ETA Updated"
                };

                var transaction = new Transaction
                {
                    EntityTypeId = 1,
                    EntityId = flightManifestId,
                    StatusId = 2,
                    StatusTimestamp = DateTime.UtcNow,
                    Reference = "FLT# " + flightManifest.Carrier.CarrierCode + flightManifest.FlightNumber,
                    Description = "Flight ETA Updated To: " + eta,
                    TaskTypeId = 1,
                    TransactionActionId = 1,
                    TaskId = taskId,
                    UserId = userId
                };

                DataContext.Transactions.Add(transaction);
                DataContext.SaveChanges();
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        public int GetForkliftCount(long userId, long taskId)
        {
            long appUserId = GetAppUserId(userId);
            var userName = DataContext.UserProfiles.Where(u => u.Id == appUserId).Single().UserId;
            var details = DataContext.ForkliftDetails.Where(f => f.UserId == userId && f.TaskId == taskId).ToList();
            return details.Sum(f => f.Pieces.HasValue ? f.Pieces.Value : 0);
        }

        public Uld GetFlightULD(long uldId)
        {
            var uld = DataContext.ULDs.Where(u => u.Id == uldId).Single();
            var awbsCount = uld.ManifestAWBDetails.Count();
            var progress = awbsCount == 0 ? 0 : uld.ManifestAWBDetails.Sum(a => !a.ReceivedCount.HasValue || !a.LoadCount.HasValue || a.LoadCount.Value == 0 ? 0 : (Math.Min(a.ReceivedCount.Value / (double)a.LoadCount.Value * 100, 100))) / uld.ManifestAWBDetails.Count();

            return new Uld
            {
                UldId = uld.Id,
                UldType = uld.ULDType != null ? uld.ULDType.Code : string.Empty,
                UldPrefix = uld.ShipmentUnitType.Code,
                UldSerialNo = uld.SerialNumber,
                ULD = uld.ShipmentUnitType.Code + uld.SerialNumber,
                CarrierCode = uld.Carrier == null ? string.Empty : uld.Carrier.CarrierCode,
                TotalUnitCount = uld.TotalUnitCount.HasValue ? uld.TotalUnitCount.Value : 0,
                IsBUP = uld.IsBUP.HasValue ? uld.IsBUP.Value : false,
                Location = uld.WarehouseLocation == null ? "N/A" : uld.WarehouseLocation.Location,
                PercentRecovered = uld.PercentReceived.HasValue ? uld.PercentReceived.Value : 0,
                RecoveredCount = uld.TotalReceivedCount.HasValue ? uld.TotalReceivedCount.Value : 0,
                TotalPieces = uld.TotalPieces.HasValue ? uld.TotalPieces.Value : 0,
                ReceivedPieces = uld.ReceivedPieces.HasValue ? uld.ReceivedPieces.Value : 0,
                PercentReceived = progress,
                Flag = DataContext.GetFlagsSP(uld.Id, "uld", null).Single().Value,
                Status = uld.RecoveredOn == null ? RecoverStatuses.Pending : (progress < 100 ? RecoverStatuses.InProgress : RecoverStatuses.Complete)
                //Status = uld.RecoveredOn == null ? RecoverStatuses.Pending : (uld.RecoveredOn != null && uld.TotalPieces < uld.ReceivedPieces ? RecoverStatuses.InProgress : (uld.RecoveredOn != null && uld.TotalPieces == uld.ReceivedPieces ? RecoverStatuses.Complete : RecoverStatuses.NA))
            };
        }

        public void RecoverUld(long uldId, long userId, long flightManifestId, long locationId)
        {
            long appUserId = GetAppUserId(userId);
            var uld = DataContext.ULDs.Single(u => u.Id == uldId);
            var userNameModel = DataContext.UserProfiles.Single(u => u.Id == appUserId);
            var userName = userNameModel.FirstName + " "  + userNameModel.LastName;

            if (uld.IsBUP.HasValue && uld.IsBUP.Value)
            {
                DataContext.ProcessUldAsBup(uldId, flightManifestId, userName, locationId);
            }
            else
            {
                DataContext.RecoverFlightULD(userName, flightManifestId, uldId, locationId);
            }
        }

        public void DropForkliftPieces(long taskId, long userId, int locationId)
        {
            var userName = GetUserName(userId);
            var task = DataContext.Tasks.Single(t => t.Id == taskId);
            var flightManifest = DataContext.FlightManifests.Single(f => f.Id == task.EntityId && task.EntityTypeId == 1);
            var forkliftDetails = DataContext.ForkliftDetails.Where(d => d.TaskId == taskId && d.UserId == userId);

            //TODO: Harut fix this

            var unloadingPort = DataContext.UnloadingPorts.Single(up => up.PortId == task.Warehouse.PortId && up.FlightManifestId == flightManifest.Id);


            foreach (var detail in forkliftDetails)
            {
                var pfItems = unloadingPort.PutAwayPieces + detail.Pieces.Value;
                var prItems = unloadingPort.ReceivedPieces + detail.Pieces.Value;
                unloadingPort.PutAwayPieces = pfItems;
                unloadingPort.PercentPutAway = pfItems / (double)unloadingPort.TotalPieces * 100;
                unloadingPort.ReceivedPieces = prItems;
                unloadingPort.PercentReceived = prItems / (double)unloadingPort.TotalPieces * 100;
                unloadingPort.PercentOverall = unloadingPort.PercentPutAway + unloadingPort.PercentRecovered / 2;

                task.ProgressPercent = unloadingPort.PercentPutAway + unloadingPort.PercentRecovered / 2;

                var uld = DataContext.ULDs.SingleOrDefault(u => u.Id == detail.ULDId);

                if (uld != null)
                {
                    var pItems = (uld.PutAwayPieces.HasValue ? uld.PutAwayPieces.Value : 0) + detail.Pieces;
                    var rItems = (uld.ReceivedPieces.HasValue ? uld.ReceivedPieces.Value : 0) + detail.Pieces;
                    uld.PutAwayPieces = pItems;
                    uld.PercentPutAway = pItems / (double)uld.TotalPieces * 100;
                    uld.ReceivedPieces = rItems;
                    uld.PercentReceived = rItems / (double)uld.TotalPieces * 100;
                }

                var awb = DataContext.AWBs.SingleOrDefault(a => a.Id == detail.AWBId);

                if (awb != null)
                {
                    var pItems = (awb.PutAwayPieces.HasValue ? awb.PutAwayPieces.Value : 0) + detail.Pieces;
                    var rItems = (awb.ReceivedPieces.HasValue ? awb.ReceivedPieces.Value : 0) + detail.Pieces;
                    awb.PutAwayPieces = pItems;
                    awb.PercentPutAway = pItems / (double)awb.TotalPieces * 100;
                    awb.ReceivedPieces = rItems;
                    awb.PercentReceived = rItems / (double)awb.TotalPieces * 100;

                    var manifestAwbDetails = DataContext.ManifestAWBDetails.FirstOrDefault(m => m.ULDId == detail.ULDId && m.AWBId == detail.AWBId && m.UnloadingPortId == uld.UnloadingPortId);

                    if (manifestAwbDetails != null)
                    {
                        pItems = (manifestAwbDetails.PutAwayPieces.HasValue ? manifestAwbDetails.PutAwayPieces.Value : 0) + detail.Pieces;
                        rItems = (manifestAwbDetails.ReceivedCount.HasValue ? manifestAwbDetails.ReceivedCount.Value : 0) + detail.Pieces;
                        manifestAwbDetails.PutAwayPieces = pItems;
                        manifestAwbDetails.PercentPutAway = pItems / (double)manifestAwbDetails.LoadCount * 100;
                        manifestAwbDetails.ReceivedCount = rItems;
                        manifestAwbDetails.PercentReceived = rItems / (double)manifestAwbDetails.LoadCount * 100;
                    }

                    var disposition = new Disposition
                    {
                        TaskId = taskId,
                        AWB = awb,
                        HWBId = null,
                        Count = detail.Pieces,
                        WarehouseLocationId = locationId,
                        UserId = userId,
                        Timestamp = DateTime.UtcNow
                    };

                    DataContext.Transactions.Add(new Transaction
                    {
                        TaskId = taskId,
                        EntityTypeId = 2,
                        EntityId = awb.Id,                      
                        StatusId = 22,
                        TaskTypeId = 1,
                        StatusTimestamp = DateTime.UtcNow,
                        Description = detail.Pieces + " of " + awb.Carrier.Carrier3Code + "-" + awb.AWBSerialNumber + " located at " + DataContext.WarehouseLocations.Single(l => l.Id == locationId).Location,
                        TransactionActionId = 1,
                        Reference = awb.Carrier.Carrier3Code + "-" + awb.AWBSerialNumber,
                        UserId = userId
                    });

                    DataContext.Dispositions.Add(disposition);
                }
            }
            DataContext.ForkliftDetails.RemoveRange(forkliftDetails);
            DataContext.SaveChanges();
        }

        public void DropPiecesToLocation(long taskId, long userId, int locationId, long forkliftDetailsId, int pieces)
        {
            var userName = GetUserName(userId);
            var task = DataContext.Tasks.Single(t => t.Id == taskId);
            var flightManifest = DataContext.FlightManifests.Single(f => f.Id == task.EntityId && task.EntityTypeId == 1);
            var detail = DataContext.ForkliftDetails.Single(d => d.Id == forkliftDetailsId);

            //TODO: Harut fix this 
            var unloadingPort = DataContext.UnloadingPorts.Single(up => up.PortId == task.Warehouse.PortId && up.FlightManifestId == flightManifest.Id);

            var pfItems = unloadingPort.PutAwayPieces + pieces;
            var prItems = unloadingPort.ReceivedPieces + pieces;
            unloadingPort.PutAwayPieces = pfItems;
            unloadingPort.PercentPutAway = pfItems / (double)unloadingPort.TotalPieces * 100;
            unloadingPort.ReceivedPieces = prItems;
            unloadingPort.PercentReceived = prItems / (double)unloadingPort.TotalPieces * 100;
            unloadingPort.PercentOverall = unloadingPort.PercentPutAway + unloadingPort.PercentRecovered / 2;

            task.ProgressPercent = unloadingPort.PercentPutAway + unloadingPort.PercentRecovered / 2;

            var uld = DataContext.ULDs.SingleOrDefault(u => u.Id == detail.ULDId);

            if (uld != null)
            {
                var pItems = (uld.PutAwayPieces.HasValue ? uld.PutAwayPieces.Value : 0) + pieces;
                var rItems = (uld.ReceivedPieces.HasValue ? uld.ReceivedPieces.Value : 0) + pieces;
                uld.PutAwayPieces = pItems;
                uld.PercentPutAway = pItems / (double)uld.TotalPieces * 100;
                uld.ReceivedPieces = rItems;
                uld.PercentReceived = rItems / (double)uld.TotalPieces * 100;
            }

            var awb = DataContext.AWBs.SingleOrDefault(a => a.Id == detail.AWBId);

            if (awb != null)
            {
                var pItems = (awb.PutAwayPieces.HasValue ? awb.PutAwayPieces.Value : 0) + pieces;
                var rItems = (awb.ReceivedPieces.HasValue ? awb.ReceivedPieces.Value : 0) + pieces;
                awb.PutAwayPieces = pItems;
                awb.PercentPutAway = pItems / (double)awb.TotalPieces * 100;
                awb.ReceivedPieces = rItems;
                awb.PercentReceived = rItems / (double)awb.TotalPieces * 100;

                var manifestAwbDetails = DataContext.ManifestAWBDetails.FirstOrDefault(m => m.ULDId == detail.ULDId && m.AWBId == detail.AWBId && m.UnloadingPortId == unloadingPort.Id);

                if (manifestAwbDetails != null)
                {
                    var pItem = (manifestAwbDetails.PutAwayPieces.HasValue ? manifestAwbDetails.PutAwayPieces.Value : 0) + pieces;
                    var rItem = (manifestAwbDetails.ReceivedCount.HasValue ? manifestAwbDetails.ReceivedCount.Value : 0) + pieces;
                    manifestAwbDetails.PutAwayPieces = pItem;
                    manifestAwbDetails.PercentPutAway = pItem / (double)manifestAwbDetails.LoadCount * 100;
                    manifestAwbDetails.ReceivedCount = rItem;
                    manifestAwbDetails.PercentReceived = rItem / (double)manifestAwbDetails.LoadCount * 100;
                }

                var disposition = new Disposition
                {
                    TaskId = taskId,
                    AWB = awb,
                    HWBId = null,
                    Count = pieces,
                    WarehouseLocationId = locationId,
                    UserId = userId,
                    Timestamp = DateTime.UtcNow
                };

                DataContext.Transactions.Add(new Transaction
                {
                    TaskId = taskId,
                    EntityTypeId = 2,
                    EntityId = awb.Id,
                    StatusId = 22,
                    TaskTypeId = 1,
                    StatusTimestamp = DateTime.UtcNow,
                    Description = detail.Pieces + " of " + awb.Carrier.Carrier3Code + "-" + awb.AWBSerialNumber + " located at " + DataContext.WarehouseLocations.Single(l => l.Id == locationId).Location,
                    TransactionActionId = 1,
                    Reference = awb.Carrier.Carrier3Code + "-" + awb.AWBSerialNumber,
                    UserId = userId
                });

                DataContext.Dispositions.Add(disposition);
            }

            if (detail.Pieces == pieces)
            {
                DataContext.ForkliftDetails.Remove(detail);
            }
            else if (detail.Pieces > pieces)
            {
                detail.Pieces = detail.Pieces - pieces;
            }

            DataContext.SaveChanges();
        }

        public ValidatedShipment ValidateShipment(long userId, long taskId, long uldId, string shipment)
        {
            long appUserId = GetAppUserId(userId);
            shipment = shipment.Replace(" ", "").Replace("-", "").ToLower();
            var status = new ObjectParameter("Status", typeof(int));
            var valShipment = DataContext.ValidateShipment(appUserId, taskId, uldId, shipment, status).FirstOrDefault();
            if (valShipment == null)
            {
                if ((int)status.Value == 2)
                    return new ValidatedShipment { TransactionStatus = new TransactionStatus { Status = false, Error = "Shipment is on a different ULD" } };
                return new ValidatedShipment { TransactionStatus = new TransactionStatus { Status = false, Error = "Shipment not found on Flight" } };
            }

            return new ValidatedShipment
            {
                TransactionStatus = new TransactionStatus { Status = true },
                //Hwb = valShipment.Hwb,
                AvailablePieces = valShipment.AvailablePieces.HasValue ? valShipment.AvailablePieces.Value : 0,
                Awb = valShipment.Awb,
                AwbId = valShipment.AwbId.Value,

                //ReceivedPieces = valShipment.ReceivedPieces.HasValue ? valShipment.ReceivedPieces.Value : 0,
                //TotalPieces = valShipment.TotalPieces.HasValue ? valShipment.TotalPieces.Value : 0,
                //ForkliftPieces = valShipment.ForkliftPieces.HasValue ? valShipment.ForkliftPieces.Value : 0,
                DetailId = valShipment.DetailId,
                Origin = valShipment.Origin,
                Destination = valShipment.Destination
                //IsSplit = valShipment.IsSplit == 1 ? true : false
            };
        }

        public MCH.BLL.Model.DataContracts.ForkliftView[] GetForkliftView(long taskId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            return DataContext.GetForkliftView(taskId, appUserId).Select(f => new MCH.BLL.Model.DataContracts.ForkliftView
            {
                AwbId = f.AWBId.HasValue ? f.AWBId.Value : 0,
                AWB = f.AWB,
                Uld = f.ULD,
                UldId = f.ULDId.HasValue ? f.ULDId.Value : 0,
                DetailId = f.DetailId,
                ForkliftPieces = f.ForkliftPieces,
                Locations = f.Locations,
                ReceivedPieces = f.ReceivedPcs.HasValue ? f.ReceivedPcs.Value : 0,
                TotalPieces = f.TotalPcs.HasValue ? f.TotalPcs.Value : 0,
                Counter = 0,
                AvailablePieces = (f.TotalPcs.HasValue ? f.TotalPcs.Value : 0) - (f.ForkliftPieces + (f.ReceivedPcs.HasValue ? f.ReceivedPcs.Value : 0))
            }).ToArray();
        }

        public UldView[] GetUldView(long taskId, long userId, long uldid, int status)
        {
            long appUserId = GetAppUserId(userId);
            return DataContext.GetULDView(taskId, appUserId, uldid, status).Select(f => new UldView
            {
                AwbId = f.AWBId.HasValue ? f.AWBId.Value : 0,
                AWB = f.AWB,
                Uld = f.ULD,
                UldId = f.ULDId.HasValue ? f.ULDId.Value : 0,
                DetailId = f.DetailId,
                ForkliftPieces = f.ForkliftPieces,
                Locations = f.Locations,
                ReceivedPieces = f.ReceivedPcs.HasValue ? f.ReceivedPcs.Value : 0,
                TotalPieces = f.TotalPcs.HasValue ? f.TotalPcs.Value : 0,
                Flag = f.AWBId.HasValue ? DataContext.GetFlagsSP(f.AWBId, "awb", null).Single().Value : 0,
                References = f.Refs.Split(','),
                AvailablePieces = (f.TotalPcs.HasValue ? f.TotalPcs.Value : 0) - (f.ForkliftPieces + (f.ReceivedPcs.HasValue ? f.ReceivedPcs.Value : 0)),
                Origin = f.Origin,
                Destination = f.Destination
            }).ToArray();
        }

        public UldView[] GetFlightView(long taskId, long manifestId)
        {
            return DataContext.GetFlightView(taskId, manifestId).Select(f => new UldView
            {
                AwbId = f.AWBId.HasValue ? f.AWBId.Value : 0,
                AWB = f.AWB,
                Uld = f.ULD,
                UldId = f.ULDId.HasValue ? f.ULDId.Value : 0,
                DetailId = f.DetailId,
                ForkliftPieces = f.ForkliftPieces,
                Locations = f.Locations,
                ReceivedPieces = f.ReceivedPcs.HasValue ? f.ReceivedPcs.Value : 0,
                TotalPieces = f.TotalPcs.HasValue ? f.TotalPcs.Value : 0,
                AvailablePieces = (f.TotalPcs.HasValue ? f.TotalPcs.Value : 0) - (f.ForkliftPieces + (f.ReceivedPcs.HasValue ? f.ReceivedPcs.Value : 0)),
                References = f.Refs.Split(','),
                Flag = DataContext.GetFlagsSP(f.AWBId, "awb", null).Single().Value,
            }).ToArray();
        }

        public TransactionStatus AddForkliftPieces(long detailsId, long taskId, int quantity, long userid)
        {
            try
            {
                var userName = GetUserName(userid);
                DataContext.AddForkliftPieces(detailsId, taskId, quantity, userName);
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        public CargoReceiverFlight GetFlightManifestById(long manifestId, long taskId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var data = DataContext.GetFlightManifestById(manifestId, taskId, appUserId);
            return data.Select(d => new CargoReceiverFlight
            {
                CarrierLogo = d.CarrierLogo,
                CarrierName = d.CarrierName,
                CarrierCode = d.CarrierCode,
                FlightNumber = d.FlightNumber,
                Origin = d.Origin,
                ULDCount = d.UldCount.Value,
                AWBCount = d.AWBCount.Value,
                TotalPieces = d.TotalPieces.Value,
                FlightManifestId = d.FlightManifestId.Value,
                RecoverLocation = d.RecoveredLocation,
                RecoveredULDs = d.RecoveredUlds.Value,
                RecoveredBy = d.RecoeveredBy,
                ReceivedPieces = d.ReceivedPieces.HasValue ? d.ReceivedPieces.Value : 0,
                TaskId = d.TaskId.Value,
                RecoveredDate = d.RecoveredDate,
                Flag = d.Flags.HasValue ? d.Flags.Value : 0,
                ETA = d.Eta,
                Status = (TaskStatuses)d.StatusId
            }).FirstOrDefault();
        }

        public void RemoveItemsFromForklift(long forkliftDetailsId, int count)
        {
            var detail = DataContext.ForkliftDetails.SingleOrDefault(f => f.Id == forkliftDetailsId);
            if (detail == null) return;

            if (detail.Pieces <= count)
                DataContext.ForkliftDetails.Remove(detail);
            else
                detail.Pieces = detail.Pieces - count;

            DataContext.SaveChanges();
        }

        public void RemoveAllItemsFromForklift(long userId, long taskId)
        {
            var userName = GetUserName(userId);
            var detail = DataContext.ForkliftDetails.SingleOrDefault(f => f.UserId == userId && f.TaskId == taskId);
            if (detail == null) return;
            DataContext.ForkliftDetails.Remove(detail);

            DataContext.SaveChanges();
        }

        public AlertModel[] GetFlightAlerts(string port, long manifestId)
        {
            return DataContext.GetFlightAlerts(port, manifestId, false).Select(a => new AlertModel()
            {
                Date = a.AlertDate.HasValue ? a.AlertDate.Value : DateTime.UtcNow,
                Message = a.AlertMessage,
                SetBy = a.SetBy
            }).ToArray();
        }

        public AlertModel[] GetUldAlerts(string port, long uldId)
        {
            return DataContext.GetUldAlerts(port, uldId, false).Select(a => new AlertModel()
            {
                Date = a.AlertDate.HasValue ? a.AlertDate.Value : DateTime.UtcNow,
                Message = a.AlertMessage,
                SetBy = a.SetBy
            }).ToArray();
        }

        public AlertModel[] GetTaskAlerts(string port, long taskId)
        {
            return DataContext.GetTaskAlerts(port, taskId, false).Select(a => new AlertModel()
            {
                Date = a.AlertDate.HasValue ? a.AlertDate.Value : DateTime.UtcNow,
                Message = a.AlertMessage,
                SetBy = a.SetBy
            }).ToArray();
        }

        public AlertModel[] GetAwbAlerts(string port, long awbId)
        {
            return DataContext.GetAwbAlerts(port, awbId, false).Select(a => new AlertModel()
            {
                Date = a.AlertDate.HasValue ? a.AlertDate.Value : DateTime.UtcNow,
                Message = a.AlertMessage,
                SetBy = a.SetBy
            }).ToArray();
        }

        public FlightProgress GetFlightProgress(long manifestId, long taskId)
        {
            try
            {
                var awbs = DataContext.ManifestAWBDetails.Where(a => a.FlightManifestId == manifestId).ToList();
                var awbsCount = awbs.Count();
                return new FlightProgress
                {
                    TotalPieces = awbs.Sum(a => a.LoadCount).Value,
                    ReceivedPieces = awbs.Sum(a => a.ReceivedCount).Value,
                    Progress = awbsCount == 0 ? 0 : awbs.Sum(a => !a.ReceivedCount.HasValue || !a.LoadCount.HasValue || a.LoadCount.Value == 0 ? 0 : (Math.Min(a.ReceivedCount.Value / (double)a.LoadCount.Value * 100, 100))) / awbs.Count()
                };
            }
            catch
            {
                return new FlightProgress { TotalPieces = 0, ReceivedPieces = 0 };
            }
        }

        public TransactionStatus FinalizeReceiver(long taskId, long manifestId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            DataContext.FinalizeReceiver(manifestId, taskId, appUserId);
            return new TransactionStatus { Status = true };
        }

        public ScannedShipmentInfo[] ScanShipment(long userId, long taskId, string shipmentNumber)
        {
            shipmentNumber = shipmentNumber.Replace(" ", "").Replace("-", "").ToLower();
            var task = DataContext.Tasks.Single(t => t.Id == taskId);

            var result = new List<ScannedShipmentInfo>();
            var awbUlds = DataContext.ManifestAWBDetails
                .Where(d => d.AWB.Carrier.Carrier3Code + d.AWB.AWBSerialNumber == shipmentNumber && d.ULDId.HasValue && d.FlightManifestId == task.EntityId && task.EntityTypeId == 1)
                .Select(d => d.ULDId).ToList();

            var hwbUlds = DataContext.ManifestDetails
                .Where(d => d.HWB.HWBSerialNumber == shipmentNumber && d.ULDId.HasValue && d.FlightManifestId == task.EntityId && task.EntityTypeId == 1)
                .Select(d => d.ULDId).ToList();

            foreach (var id in awbUlds)
            {
                result.Add(new ScannedShipmentInfo
                {
                    Uld = GetFlightULD(id.Value),
                    Shipment = ValidateShipment(userId, taskId, id.Value, shipmentNumber)
                });
            }

            foreach (var id in hwbUlds)
            {
                result.Add(new ScannedShipmentInfo
                {
                    Uld = GetFlightULD(id.Value),
                    Shipment = ValidateShipment(userId, taskId, id.Value, shipmentNumber)
                });
            }

            return result.ToArray();
        }

        public TransactionStatus AddToForklift(long taskId, long userId, long uldId, long hwbId, long awbId, int count)
        {
            try
            {
                var user = GetUserName(userId);
                DataContext.AddToForklift(taskId, user, uldId, hwbId == 0 ? (long?)null : hwbId, awbId, count);
                return new TransactionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus { Status = false, Error = ex.Message };
            }
        }

        //TODO: Harut check this with Vineeta
        public AvailableItmes[] GetAvailableCarriers(string station, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var items = (from f in DataContext.FlightManifests
                         join t in DataContext.Tasks on new { Id = (long?)f.Id, EntityTypeId = (int?)1 } equals new { Id = t.EntityId, EntityTypeId = t.EntityTypeId }
                         where t.EndDate == null && t.TaskTypeId == 1 && t.Warehouse.Code == station && !t.TaskAssignments.Any(ta => ta.UserId == appUserId)
                         select new AvailableItmes { Id = f.Carrier.Id, Name = f.Carrier.CarrierCode, Station = t.Warehouse.Code }).ToList();




            return items.GroupBy(g => new { g.Id, g.Name, g.Station }).Select(g => g.First()).ToArray();
        }


        //TODO: Harut check this with Vineeta
        public DateTime[] GetAvailableEtds(long carrierId, string station, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var items = (from f in DataContext.FlightManifests
                         join t in DataContext.Tasks on new { Id = (long?)f.Id, EntityTypeId = (int?)1 } equals new { Id = t.EntityId, EntityTypeId = t.EntityTypeId }
                         where t.EndDate == null && t.TaskTypeId == 1 && f.Carrier.Id == carrierId && t.Warehouse.Code == station && f.ETD.HasValue && !t.TaskAssignments.Any(ta => ta.UserId == appUserId)
                         select f.ETD.Value).ToList();



            return items.Distinct().ToArray();
        }


        //TODO: Harut check this with Vineeta
        public AvailableFlight[] GetAvailableFlights(long carrierId, DateTime etd, string station, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var items = (from f in DataContext.FlightManifests
                         join t in DataContext.Tasks on new { Id = (long?)f.Id, EntityTypeId = (int?)1 } equals new { Id = t.EntityId, EntityTypeId = t.EntityTypeId }
                         where t.EndDate == null && t.TaskTypeId == 1 && f.ETD == etd && f.Carrier.Id == carrierId && t.Warehouse.Code == station && !t.TaskAssignments.Any(ta => ta.UserId == appUserId)
                         select new AvailableFlight { Id = f.Id, Name = f.FlightNumber, Station = t.Warehouse.Code, TaskId = t.Id }).ToList();



            return items.GroupBy(g => new { g.Id, g.Name, g.Station, g.TaskId }).Select(g => g.First()).ToArray();
        }       

        //TODO: Harut check this with Vineeta
        public AvailableFlight[] GetAvailableFlightsByEta(long carrierId, DateTime eta, string station, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var items = (from f in DataContext.FlightManifests
                         join t in DataContext.Tasks on new { Id = (long?)f.Id, EntityTypeId = (int?)1 } equals new { Id = t.EntityId, EntityTypeId = t.EntityTypeId }
                         where t.EndDate == null && t.TaskTypeId == 1 &&
                         f.UnloadingPorts.Any(up => up.Port.IATACode == station) && f.UnloadingPorts.FirstOrDefault(up => up.Port.IATACode == station).POU_ETA == eta
                         && f.Carrier.Id == carrierId && t.Warehouse.Code == station && !t.TaskAssignments.Any(ta => ta.UserId == appUserId)
                         select new AvailableFlight { Id = f.Id, Name = f.FlightNumber, Station = t.Warehouse.Code, TaskId = t.Id }).ToList();



            return items.GroupBy(g => new { g.Id, g.Name, g.Station, g.TaskId }).Select(g => g.First()).ToArray();
        }     


        //TODO: Harut check this with Vineeta
        public DateTime[] GetAvailableEtas(long carrierId, string station, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var items = (from f in DataContext.FlightManifests
                         join t in DataContext.Tasks on new { Id = (long?)f.Id, EntityTypeId = (int?)1 } equals new { Id = t.EntityId, EntityTypeId = t.EntityTypeId }
                         where t.EndDate == null && t.TaskTypeId == 1 && f.Carrier.Id == carrierId && t.Warehouse.Code == station &&
                         f.UnloadingPorts.Any(up => up.Port.IATACode == station) && f.UnloadingPorts.FirstOrDefault(up => up.Port.IATACode == station).POU_ETA.HasValue
                         && !t.TaskAssignments.Any(ta => ta.UserId == appUserId)
                         select f.UnloadingPorts.FirstOrDefault(up => up.Port.IATACode == station).POU_ETA.Value).ToList();


            return items.Distinct().OrderByDescending(i => i).ToArray();
        }

        public TransactionStatus LinkTaskToUserId(long taskId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var isOwner = !DataContext.TaskAssignments.Any(a => a.TaskId == taskId && a.UserId != appUserId);
            var task = DataContext.Tasks.Single(t => t.Id == taskId);
            task.TaskDate = DateTime.UtcNow;

            DataContext.TaskAssignments.Add(new TaskAssignment
            {
                TaskId = taskId,
                UserId = appUserId,
                AssignedOn = DateTime.UtcNow,
                RecDate = DateTime.UtcNow,
                AssignedBy = DataContext.UserProfiles.Single(u => u.UserId == "HostPlus").Id,
                IsOwner = isOwner
            });
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public int GetAwbPiecesCountInLocation(long awbId, long locationId, long taskId)
        {
            var count = DataContext.Dispositions.Where(d => d.WarehouseLocationId == locationId && d.AWBId == awbId && (taskId == 0 || d.TaskId == taskId)).Sum(d => d.Count);
            return count.HasValue ? count.Value : 0;
        }

        public int GetHwbPiecesCountInLocation(long hwbId, long locationId, long taskId)
        {
            var count = DataContext.Dispositions.Where(d => d.WarehouseLocationId == locationId && d.HWBId == hwbId && (taskId == 0 || d.TaskId == taskId)).Sum(d => d.Count);
            return count.HasValue ? count.Value : 0;
        }

        public TransactionStatus RelocateAwbPieces(long awbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var dispositionTo = this.DataContext.Dispositions.Where(d => d.WarehouseLocationId == newLocationId && d.AWBId == awbId && d.TaskId == taskId).FirstOrDefault();

            var dispositionFrom = this.DataContext.Dispositions.Where(d => d.WarehouseLocationId == oldLocationId && d.AWBId == awbId && d.TaskId == taskId).FirstOrDefault();

            if (pcs > dispositionFrom.Count)
            {
                return new TransactionStatus { Status = false, Error = "Invalid piece count." };
            }

            var tmpUser = this.DataContext.UserProfiles.SingleOrDefault(u => u.Id == appUserId);

            dispositionFrom.Count = dispositionFrom.Count - pcs;

            if (dispositionTo != null)
            {
                int newPieceCount = (dispositionTo.Count.HasValue ? dispositionTo.Count.Value : 0) + pcs;
                dispositionTo.Count = newPieceCount;
            }
            else
            {
                DataContext.Dispositions.Add(new Disposition
                    {
                        Count = pcs,
                        WarehouseLocationId = newLocationId,
                        AWBId = awbId,
                        Timestamp = DateTime.UtcNow,
                        TaskId = taskId,
                        UserId = userId
                    });
            }

            var tmpToLocation = this.DataContext.WarehouseLocations.SingleOrDefault(l=> l.Id == newLocationId);

            this.DataContext.Transactions.Add(new Transaction {
                EntityTypeId = (int)enumEntityTypes.AWB,
                EntityId = awbId, 
                StatusId = 56, 
                StatusTimestamp = DateTime.UtcNow,
                Reference = this.DataContext.MCH_GetEntityReference((int)enumEntityTypes.AWB, awbId).FirstOrDefault(),
                Description = "Inventory : PCS " + pcs + " Moved To: " + tmpToLocation.Location,
                TaskTypeId = 2, 
                TransactionActionId = 1, 
                TaskId = taskId,
                UserId = userId
            });

            this.DataContext.SaveChanges();

            
            return new TransactionStatus { Status = true };
        }


       

        public TransactionStatus RelocateHwbPieces(long hwbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var dispositionFrom = this.DataContext.Dispositions.Where(d => d.WarehouseLocationId == oldLocationId && d.HWBId == hwbId && d.TaskId == taskId).FirstOrDefault();

            if (pcs > dispositionFrom.Count)
            {
                return new TransactionStatus { Status = false, Error = "Invalid piece count." };
            }

            var dispositionTo = this.DataContext.Dispositions.Where(d => d.WarehouseLocationId == newLocationId && d.HWBId == hwbId && d.TaskId == taskId).FirstOrDefault();

            var tmpUser = this.DataContext.UserProfiles.SingleOrDefault(u => u.Id == appUserId);

            dispositionFrom.Count = dispositionFrom.Count - pcs;

            if (dispositionTo != null)
            {
                int newPieceCount = (dispositionTo.Count.HasValue ? dispositionTo.Count.Value : 0) + pcs;
                dispositionTo.Count = newPieceCount;
            }
            else
            {
                DataContext.Dispositions.Add(new Disposition
                {
                    Count = pcs,
                    WarehouseLocationId = newLocationId,
                    HWBId = hwbId,
                    Timestamp = DateTime.UtcNow,
                    TaskId = taskId,
                    UserId = userId
                });
            }

            var tmpToLocation = this.DataContext.WarehouseLocations.SingleOrDefault(l => l.Id == newLocationId);

            this.DataContext.Transactions.Add(new Transaction
            {
                EntityTypeId = (int)enumEntityTypes.HWB,
                EntityId = hwbId,
                StatusId = 56,
                StatusTimestamp = DateTime.UtcNow,
                Reference = this.DataContext.MCH_GetEntityReference((int)enumEntityTypes.HWB, hwbId).FirstOrDefault(),
                Description = "Inventory : PCS " + pcs + " Moved To: " + tmpToLocation.Location,
                TaskTypeId = 2,
                TransactionActionId = 1,
                TaskId = taskId,
                UserId = tmpUser.Id
            });

            this.DataContext.SaveChanges();

            return new TransactionStatus { Status = true };
        }

        public TransactionStatus DischargeAwbPieces(long awbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId, string truckNumber)
        {

            var dispositionTo = this.DataContext.Dispositions.Where(d => d.WarehouseLocationId == newLocationId && d.AWBId == awbId && d.TaskId == taskId).FirstOrDefault();

            var dispositionFrom = this.DataContext.Dispositions.Where(d => d.WarehouseLocationId == oldLocationId && d.AWBId == awbId && d.TaskId == taskId).FirstOrDefault();

            if (pcs > dispositionFrom.Count)
            {
                return new TransactionStatus { Status = false, Error = "Invalid piece count." };
            }

            var tmpUser = this.DataContext.UserProfiles.SingleOrDefault(u => u.Id == GetAppUserId(userId));

            dispositionFrom.Count = dispositionFrom.Count - pcs;

            if (dispositionTo != null)
            {
                int newPieceCount = (dispositionTo.Count.HasValue ? dispositionTo.Count.Value : 0) + pcs;
                dispositionTo.Count = newPieceCount;
                dispositionTo.IsAdjust = true;
            }
            else
            {
                DataContext.Dispositions.Add(new Disposition
                {
                    Count = pcs,
                    WarehouseLocationId = newLocationId,
                    AWBId = awbId,
                    Timestamp = DateTime.UtcNow,
                    TaskId = taskId,
                    IsAdjust = true,
                    UserId = tmpUser.Id
                });
            }

            var tmpDispositionToAdjust = this.DataContext.Dispositions.Where(d => d.AWBId == awbId && d.TaskId != taskId);

            foreach (var item in tmpDispositionToAdjust)
            {
                item.IsAdjust = true;
            }

            var tmpToLocation = this.DataContext.WarehouseLocations.SingleOrDefault(l => l.Id == newLocationId);

            this.DataContext.Transactions.Add(new Transaction
            {
                EntityTypeId = (int)enumEntityTypes.AWB,
                EntityId = awbId,
                StatusId = 56,
                StatusTimestamp = DateTime.UtcNow,
                Reference = this.DataContext.MCH_GetEntityReference((int)enumEntityTypes.AWB, awbId).FirstOrDefault(),
                Description = "Discharge : PCS " + pcs + " TRUCK: " + truckNumber,
                TaskTypeId = 2,
                TransactionActionId = 1,
                TaskId = taskId,
                UserId = userId
            });

            this.DataContext.SaveChanges();


            return new TransactionStatus { Status = true };
        }


        public TransactionStatus DischargeHwbPieces(long hwbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId, string truckNumber)
        {
            var dispositionFrom = this.DataContext.Dispositions.Where(d => d.WarehouseLocationId == oldLocationId && d.HWBId == hwbId && d.TaskId == taskId).FirstOrDefault();

            if (pcs > dispositionFrom.Count)
            {
                return new TransactionStatus { Status = false, Error = "Invalid piece count." };
            }
            
            var dispositionTo = this.DataContext.Dispositions.Where(d => d.WarehouseLocationId == newLocationId && d.HWBId == hwbId && d.TaskId == taskId).FirstOrDefault();

            var tmpUser = this.DataContext.UserProfiles.SingleOrDefault(u => u.Id == GetAppUserId(userId));

            dispositionFrom.Count = dispositionFrom.Count - pcs;

            if (dispositionTo != null)
            {
                int newPieceCount = (dispositionTo.Count.HasValue ? dispositionTo.Count.Value : 0) + pcs;
                dispositionTo.Count = newPieceCount;
                dispositionTo.IsAdjust = true;
            }
            else
            {
                DataContext.Dispositions.Add(new Disposition
                {
                    Count = pcs,
                    WarehouseLocationId = newLocationId,
                    HWBId = hwbId,
                    Timestamp = DateTime.UtcNow,
                    TaskId = taskId,
                    IsAdjust = true,
                    UserId = userId
                });
            }

            var tmpDispositionToAdjust = this.DataContext.Dispositions.Where(d => d.HWBId == hwbId && d.TaskId != taskId);

            foreach (var item in tmpDispositionToAdjust)
            {
                item.IsAdjust = true;
            }

            var tmpToLocation = this.DataContext.WarehouseLocations.SingleOrDefault(l => l.Id == newLocationId);

            this.DataContext.Transactions.Add(new Transaction
            {
                EntityTypeId = (int)enumEntityTypes.HWB,
                EntityId = hwbId,
                StatusId = 56,
                StatusTimestamp = DateTime.UtcNow,
                Reference = this.DataContext.MCH_GetEntityReference((int)enumEntityTypes.HWB, hwbId).FirstOrDefault(),
                Description = "Discharge : PCS " + pcs + " TRUCK: " + truckNumber,
                TaskTypeId = 2,
                TransactionActionId = 1,
                TaskId = taskId,
                UserId = tmpUser.Id
            });

            this.DataContext.SaveChanges();

            return new TransactionStatus { Status = true };
        }

        #region Obsolite Relocation methods

        //public TransactionStatus RelocateAwbPieces(long awbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        //{
        //    var dispositions = DataContext.Dispositions.Where(d => d.WarehouseLocationId == oldLocationId && d.AWBId == awbId);



        
        //    foreach (var disposition in dispositions.Where(d => d.Count.HasValue))
        //    {
        //        if (disposition.Count <= pcs)
        //        {
        //            disposition.WarehouseLocationId = newLocationId;
        //            pcs -= disposition.Count.Value;
        //        }
        //        else
        //        {
        //            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;

        //            disposition.Count = disposition.Count - pcs;
        //            DataContext.Dispositions.Add(new Disposition
        //            {
        //                Count = pcs,
        //                WarehouseLocationId = newLocationId,
        //                AWBId = awbId,
        //                Timestamp = DateTime.UtcNow,
        //                TaskId = taskId,
        //                UserName = userName
        //            });
        //            pcs = 0;
        //        }

        //        if (pcs <= 0)
        //            break;
        //    }

        //    DataContext.SaveChanges();
        //    return new TransactionStatus { Status = true };
        //}


        //public TransactionStatus RelocateHwbPieces(long hwbId, int oldLocationId, int newLocationId, int pcs, long taskId, long userId)
        //{
        //    var dispositions = DataContext.Dispositions.Where(d => d.WarehouseLocationId == oldLocationId && d.HWBId == hwbId);

        //    foreach (var disposition in dispositions.Where(d => d.Count.HasValue))
        //    {
        //        if (disposition.Count <= pcs)
        //        {
        //            disposition.WarehouseLocationId = newLocationId;
        //            pcs -= disposition.Count.Value;
        //        }
        //        else
        //        {
        //            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;

        //            disposition.Count = disposition.Count - pcs;
        //            DataContext.Dispositions.Add(new Disposition
        //            {
        //                Count = pcs,
        //                WarehouseLocationId = newLocationId,
        //                HWBId = hwbId,
        //                Timestamp = DateTime.UtcNow,
        //                TaskId = taskId,
        //                UserName = userName
        //            });
        //            pcs = 0;
        //        }

        //        if (pcs <= 0)
        //            break;
        //    }

        //    DataContext.SaveChanges();
        //    return new TransactionStatus { Status = true };
        //} 
        #endregion

        public long GetLocationId(string locationName, string station)
        {
            var location = DataContext.WarehouseLocations.SingleOrDefault(w => w.Location == locationName && w.Warehouse.Code == station);
            return location == null ? 0 : location.Id;
        }

        public IEnumerable<CargoReceiverFlight> GetFlightManifestByShipment(long userId, long warehouseId, string shipmentRef)
        {
            long appUserId = GetAppUserId(userId);
            var items = Context.MCH_FindFlightManifestAndTaskByShipment(shipmentRef, appUserId, warehouseId).Where(m => m.ManifestId.HasValue && m.TaskId.HasValue).ToList();

            foreach (var item in items)
            {
                yield return GetFlightManifestById(item.ManifestId.Value, item.TaskId.Value, userId);
            }
        }

        public UldView[] GetShipmentUldView(long taskId, long manifestId, string shipmentRef)
        {
            var ulds = GetFlightView(taskId, manifestId);
            return ulds.Where(u => u.AWB == shipmentRef).ToArray();
        }

        public AvailableFlight[] GetAvailableFlightsByCarrier(long carrierId, string station, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var items = (from f in DataContext.FlightManifests
                         join t in DataContext.Tasks on new { Id = (long?)f.Id, EntityTypeId = (int?)1 } equals new { Id = t.EntityId, EntityTypeId = t.EntityTypeId }
                         where t.EndDate == null && t.TaskTypeId == 1 && f.Carrier.Id == carrierId && t.Warehouse.Code == station && !t.TaskAssignments.Any(ta => ta.UserId == appUserId)
                         select new AvailableFlight { Id = f.Id, Name = f.FlightNumber, Station = t.Warehouse.Code, TaskId = t.Id }).ToList();

            return items.GroupBy(g => new { g.Id, g.Name, g.Station, g.TaskId }).Select(g => g.First()).ToArray();
        }

        public AvailableFlight[] GetAvailableFlightsByFlightNo(long carrierId, string flightNo, string station, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var items = (from f in DataContext.FlightManifests
                         join t in DataContext.Tasks on new { Id = (long?)f.Id, EntityTypeId = (int?)1 } equals new { Id = t.EntityId, EntityTypeId = t.EntityTypeId }
                         where t.EndDate == null && t.TaskTypeId == 1 && f.Carrier.Id == carrierId
                               && t.Warehouse.Code == station && !t.TaskAssignments.Any(ta => ta.UserId == appUserId)
                               && f.FlightNumber == flightNo
                         select new AvailableFlight { Id = f.Id, Name = f.FlightNumber, Station = t.Warehouse.Code, TaskId = t.Id }).ToList();

            return items.GroupBy(g => new { g.Id, g.Name, g.Station, g.TaskId }).Select(g => g.First()).ToArray();

        }   

        public List<FlightUldInfoExtended> GetCargoReceiverUlds(long warehouseId, RecoverStatuses status, string filter)
        {
        
            List<FlightUldInfoExtended> res = new List<FlightUldInfoExtended>();

            var allFMandTasks = (from T in DataContext.Tasks join U in DataContext.ULDs 
                                   on T.EntityId equals U.FlightManifestId
                                where T.TaskTypeId == 1 && T.EntityTypeId == 1 && T.WarehouseId == warehouseId && U.FlightManifest != null &&
                                      (string.IsNullOrEmpty(filter) || U.SerialNumber.Contains(filter) || U.ShipmentUnitType.Code.Contains(filter))
                               select new {U.FlightManifest,T}).Distinct().ToArray();

            foreach(var items in allFMandTasks)
            {
                var ulds = items.FlightManifest.ULDs
                .Where(u => string.IsNullOrEmpty(filter) || u.SerialNumber.Contains(filter) || u.ShipmentUnitType.Code.Contains(filter)).ToList()
                .Select(u => new Uld
                {
                    UldId = u.Id,
                    UldType = u.ULDType != null ? u.ULDType.Code : string.Empty,
                    UldPrefix = u.ShipmentUnitType.Code,
                    UldSerialNo = u.SerialNumber,
                    CarrierCode = u.Carrier == null ? string.Empty : u.Carrier.CarrierCode,
                    TotalUnitCount = u.TotalUnitCount.HasValue ? u.TotalUnitCount.Value : 0,
                    IsBUP = u.IsBUP.HasValue ? u.IsBUP.Value : false,
                    Location = u.WarehouseLocation == null ? "N/A" : u.WarehouseLocation.Location,
                    PercentRecovered = u.PercentRecovered.HasValue ? u.PercentRecovered.Value : 0,
                    RecoveredCount = u.TotalReceivedCount.HasValue ? u.TotalReceivedCount.Value : 0,
                    TotalPieces = u.TotalPieces.HasValue ? u.TotalPieces.Value : 0,
                    ReceivedPieces = u.ReceivedPieces.HasValue ? u.ReceivedPieces.Value : 0,
                    PercentReceived = new Func<double>(() =>
                    {
                        var awbsCount = u.ManifestAWBDetails.Count();
                        return awbsCount == 0 ? 0 : u.ManifestAWBDetails.Sum(a => !a.ReceivedCount.HasValue || !a.LoadCount.HasValue || a.LoadCount.Value == 0 ? 0 : (Math.Min(a.ReceivedCount.Value / (double)a.LoadCount.Value * 100, 100))) / awbsCount;
                    })(),
                    //Status = u.RecoveredOn == null ? RecoverStatuses.Pending : (u.RecoveredOn != null && u.ReceivedPieces < u.TotalPieces ? RecoverStatuses.InProgress : (u.RecoveredOn != null && u.TotalPieces == u.ReceivedPieces ? RecoverStatuses.Complete : RecoverStatuses.NA)),
                    Status = new Func<RecoverStatuses>(() =>
                    {
                        var awbsCount = u.ManifestAWBDetails.Count();
                        var progress = awbsCount == 0 ? 0 : u.ManifestAWBDetails.Sum(a => !a.ReceivedCount.HasValue || !a.LoadCount.HasValue || a.LoadCount.Value == 0 ? 0 : (Math.Min(a.ReceivedCount.Value / (double)a.LoadCount.Value * 100, 100))) / u.ManifestAWBDetails.Count();
                        return u.RecoveredOn == null ? RecoverStatuses.Pending : (progress < 100 ? RecoverStatuses.InProgress : RecoverStatuses.Complete);
                    })(),
                    References = new Func<string[]>(() =>
                    {
                        var hwbs = DataContext.ManifestDetails.Where(h => h.ULDId == u.Id).Select(h => h.HWB.HWBSerialNumber).ToList();
                        var customers = DataContext.ManifestAWBDetails.Where(a => a.ULDId == u.Id).Select(a => a.AWB.Customer.Name).ToList();
                        hwbs.AddRange(customers);
                        return hwbs.ToArray();
                    })(),
                    Flag = DataContext.GetFlagsSP(u.Id, "uld", null).Single().Value
                })
                .Where(u => status == RecoverStatuses.All ||
                    (status == u.Status) ||
                    (status == RecoverStatuses.NotCompleted && (u.Status != RecoverStatuses.Complete)))
                .OrderBy(u => u.CarrierCode).ThenBy(u => u.UldType).ThenBy(u => u.UldSerialNo).ToArray();

                res.Add(new FlightUldInfoExtended
                    {               
                        FlightManifestId = items.FlightManifest.Id,
                        PercentRecovered = items.FlightManifest.PercentRecovered.HasValue ? items.FlightManifest.PercentRecovered.Value : 0,
                        RecoveredUlds = items.FlightManifest.RecoveredULDs.HasValue ? items.FlightManifest.RecoveredULDs.Value : 0,
                        TotalUlds = items.FlightManifest.TotalULDs.HasValue ? items.FlightManifest.TotalULDs.Value : 0,
                        TotalPieces = items.FlightManifest.TotalPieces.HasValue ? items.FlightManifest.TotalPieces.Value : 0,
                        PercentPutAway = items.FlightManifest.PercentPutAway.HasValue ? items.FlightManifest.PercentPutAway.Value : 0,
                        PutAwayPieces = items.FlightManifest.PutAwayPieces.HasValue ? items.FlightManifest.PutAwayPieces.Value : 0,
                        Ulds = ulds,
                        TaskId = items.T.Id,
                        ManifestId = items.FlightManifest.Id,
                        RecoveredLocation = DataContext.WarehouseLocations.Where(l => l.Id == items.FlightManifest.RecoveredLocationId).Select(s => s.Location).FirstOrDefault()
                    });           
            };
            return res;
        }



        #region [ -- Private Methods -- ]

        private string GetUserName(long userId)
        {
            return DataContext.UserProfiles.Where(u => u.ShellUserId == userId).Single().UserId;
        }

        #endregion

    }
}
