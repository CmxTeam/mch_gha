﻿using CMX.Framework.Utils.Security;
using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.DataContracts;
using MCH.BLL.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MCH.BLL.Units
{
    public class ForkliftUnitOfWork : DataUnitOfWork
    {
        public int GetForkliftCountGeneric(long userId, long taskTypeId, long taskId)
        {
            return GetForkLifts(userId,taskTypeId,taskId).Sum(f => f.Pieces.HasValue ? f.Pieces.Value : 0);
        }
        public void RemoveAllItemsFromForkliftGeneric(long userId, long taskTypeId, long taskId)
        {
            var details = GetForkLifts(userId, taskTypeId, taskId);
            if (details != null)
            {
                DataContext.ForkliftDetails.RemoveRange(details);
                DataContext.SaveChanges();
            }
        }
        public ForkliftViewGeneric[] GetForkliftViewByTaskType(long taskTypeId, long taskId, long userId)
        {
            return DataContext.GetForkliftViewGeneric(taskId, userId, taskTypeId).Select(f => new ForkliftViewGeneric
            {
                TaskId = f.TaskId.HasValue ? f.TaskId.Value : 0,
                AwbId = f.AWBId.HasValue ? f.AWBId.Value : 0,
                AWB = f.AWB,
                Uld = f.ULD,
                UldId = f.ULDId.HasValue ? f.ULDId.Value : 0,
                DetailId = f.DetailId,
                ForkliftPieces = f.ForkliftPieces,
                Locations = f.Locations,
                ReceivedPieces = f.ReceivedPcs.HasValue ? f.ReceivedPcs.Value : 0,
                TotalPieces = f.TotalPcs.HasValue ? f.TotalPcs.Value : 0,
                Counter = 0,
                AvailablePieces = (f.TotalPcs.HasValue ? f.TotalPcs.Value : 0) - (f.ForkliftPieces + (f.ReceivedPcs.HasValue ? f.ReceivedPcs.Value : 0))
            }).ToArray();
            
        }


        #region Private Methods
        private IList<ForkliftDetail> GetForkLifts(long userId, long taskTypeId, long taskId)
        {
            var userName = GetUserName(userId);
            IList<ForkliftDetail> details;
            if (taskId != 0)
                details = DataContext.ForkliftDetails.Where(f => f.UserId == userId && f.TaskId == taskId).ToList();
            else
                details = DataContext.ForkliftDetails.Where(f => f.UserId == userId &&
                    DataContext.Tasks.Where(t => t.TaskTypeId == taskTypeId)
                    .Select(t => t.Id).ToList<long>().Contains(f.TaskId.HasValue ? f.TaskId.Value : 0)).ToList();
            return details;
        }

        private string GetUserName(long userId)
        {
            return DataContext.UserProfiles.Where(u => u.Id == userId).Single().UserId;
        }
        #endregion
        
    }
}
