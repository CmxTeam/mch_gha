﻿using CMX.Shell.Security;
using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class UserUnitOfWork:DataUnitOfWork
    {

        private GenericRepository<UserProfile> userRepository;


        public GenericRepository<UserProfile> UserRepository
        {
            get
            {
                if (this.userRepository == null)
                    this.userRepository = new GenericRepository<UserProfile>(base.DataContext);
                return userRepository;
            }
        }


        public UserProfile GetUserByShellId(long shellId) 
        {
            return this.UserRepository.Get(e => e.ShellUserId == shellId).SingleOrDefault();
        }

        public List<string> AddShellUser(UserInfo argUser)
        {
            List<string> tmpUserConfErrors = new List<string>();

            UserProfile tmpUser = new UserProfile();
            tmpUser.Email = argUser.EmailAddress;
            tmpUser.FirstName = argUser.Firstname;
            tmpUser.LastName = argUser.Lastname;
            tmpUser.ShellUserId = argUser.UserId;
            tmpUser.UserId = argUser.Username;


            if (argUser.DefaultAccountId.HasValue)
            {
                var tmpAccount = this.Context.Accounts.SingleOrDefault(item => item.ShellAccountId == argUser.DefaultAccountId && item.IsOwner);
                if (tmpAccount != null)
                {
                    argUser.DefaultAccountId = tmpAccount.Id;
                }
                else
                {
                    tmpUserConfErrors.Add(String.Format("User default account not found for Id : {0}", argUser.DefaultAccountId));
                }
            }
            else
            {
                tmpUserConfErrors.Add("User doesn't have a default account configured.");
            }

            Port tmpStation = null;
            if (argUser.DefaultSiteId.HasValue)
            {
                 tmpStation = this.Context.Ports.SingleOrDefault(item => item.Id == argUser.DefaultSiteId);
                if (tmpStation != null) {
                    tmpUser.UserStations.Add(new UserStation { IsDefault = true, PortId = argUser.DefaultSiteId });
                }
                else {
                    tmpUserConfErrors.Add(String.Format("User default station not found for Id : {0}", argUser.DefaultSiteId));
                }
            }
            else
            {
                tmpUserConfErrors.Add("User doesn't have a default station configured.");
            }

            Warehouse tmpWarehouse = null;
            if (argUser.DefaultWarehouseId.HasValue)
            {
                 tmpWarehouse = this.Context.Warehouses.SingleOrDefault(item => item.Id == argUser.DefaultWarehouseId);
                if (tmpWarehouse != null)
                {
                    tmpUser.UserWarehouses.Add(new UserWarehous {IsDefault = true,WarehouseId = tmpWarehouse.Id });
                }
                else
                {
                    tmpUserConfErrors.Add(String.Format("User default warehouse not found for Id : {0}", argUser.DefaultWarehouseId));
                }
            }
            else
            {
                tmpUserConfErrors.Add("User doesn't have a default warehouse configured.");
            }

            if (!tmpUserConfErrors.Any())
            {
                this.Context.UserProfiles.Add(tmpUser);
                this.Context.SaveChanges();

                argUser.UserLocalId = tmpUser.Id;
                argUser.DefaultSiteId = tmpStation.Id;
                argUser.DefaultWarehouseId = tmpWarehouse.Id;
            }

            return tmpUserConfErrors;
        }

        public List<string> UpdateShellUser(UserInfo argUser)
        {
            List<string> tmpUserConffErrors = new List<string>();
            var tmpUser = this.GetUserByShellId(argUser.UserId);
            if (tmpUser != null)
            {
                
            }

            return tmpUserConffErrors;
        }
    }
}
