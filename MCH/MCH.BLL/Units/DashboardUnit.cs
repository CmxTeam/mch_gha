﻿using MCH.BLL.DAL.Data;
using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCH.BLL.Units
{
    public class DashboardUnit:DataUnitOfWork
    {
        private GenericRepository<TaskType> taskTypeRepository;
        private GenericRepository<Warehouse> warehouseRepository;
        private GenericRepository<Task> taskRepository;


        public GenericRepository<TaskType> TaskTypeRepository
        {
            get
            {
                if (this.taskTypeRepository == null)
                    this.taskTypeRepository = new GenericRepository<TaskType>(base.DataContext);
                return taskTypeRepository;
            }
        }

        public GenericRepository<Warehouse> WarehouseRepository
        {
            get
            {
                if (this.warehouseRepository == null)
                    this.warehouseRepository = new GenericRepository<Warehouse>(base.DataContext);
                return warehouseRepository;
            }
        }

        public GenericRepository<Task> TaskRepository
        {
            get
            {
                if (this.taskRepository == null)
                    this.taskRepository = new GenericRepository<Task>(base.DataContext);
                return taskRepository;
            }
        }

    }
}
