﻿using MCH.BLL.Model.CargoAcceptance;
using MCH.BLL.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class CargoAcceptanceUnit : DataUnitOfWork
    {
        public AcceptShipperInfo GetShipperManifestForUserIfAny(long userId, long warehouseId)
        {
            var shipperManifest = DataContext.ShipperManifests.SingleOrDefault(m => m.UserId == userId && m.WarehouseId == warehouseId && m.StatusId != 42);
            if (shipperManifest == null)
                return null;

            var docs = Context.Attachments.Where(a => a.EntityID == shipperManifest.Id && a.EntityTypeId == 7).Select(a => new AttachmentModel
            {
                Id = a.Id,
                Name = a.Name,                
                IsSnapshot = a.FileType == "Snapshot" ? true : false,
                SnapshotData = a.FileType == "Snapshot" ? a.AttachmentImage : null,
                Path = a.FileType == "Snapshot" ? null : a.FileName,
                ShipperDocTypeId = a.DocumentTypeId,
                DocumentType = a.ShipperVerificationDoc.DocumentName
            }).ToList();

            return new AcceptShipperInfo
            {
                AccountId = shipperManifest.AccountId,
                AircraftTypeId = shipperManifest.AircraftTypeId,
                IsDocumentVerified = true,
                ShipperId = shipperManifest.ShipperId.HasValue ? (int)shipperManifest.ShipperId:new Nullable<int>(),
                ShipperName = shipperManifest.ShipperName,
                ShipperTypeId = (int)shipperManifest.ShipperTypeId,
                ShipperStatusVerified = shipperManifest.ShipperStatusVerified,
                GovCredTypeId = shipperManifest.GovCredTypeId,
                ShipperConsentSignature = shipperManifest.ShipperConsentSignature,
                Docs = docs
            };
        }

        public long AcceptShipperInfoVerified(AcceptShipperInfo info)
        {
            var id = Context.MCH_AcceptShipperInfoVerified(info.AccountId, info.AircraftTypeId, info.ShipperTypeId, info.ShipperId, info.ShipperName, info.ShipperStatusVerified,
                info.GovCredTypeId, info.ShipperConsentSignature, null, info.UserId, info.WarehouseId, info.Documents);
            return id.Single().Value;
        }

        public void SaveDriverSecurityLogs(long? carrierDriverId, long userId, int? primaryIDTypeId, int? secondaryIdTypeId, bool? primaryPhotoIdMatch, bool? secondaryPhotoIdMatch,
                long? truckingCompanyId, string driverPhoto, string idPhoto, string driverThumbnail, long warehouseId, string sta, string driverLicense, int? stateId)
        {
            Context.MCH_SaveDriverSecurityLogs(carrierDriverId, userId, primaryIDTypeId, secondaryIdTypeId, primaryPhotoIdMatch, secondaryPhotoIdMatch, (int)truckingCompanyId,
                driverPhoto, idPhoto, driverThumbnail, warehouseId, sta, driverLicense, stateId);
        }

        public ACADriver GetDriverSecurityLog(long userId, long warehouseId)
        {
            var shipperManifest = DataContext.ShipperManifests.SingleOrDefault(m => m.UserId == userId && m.WarehouseId == warehouseId && m.StatusId != 42);
            if (shipperManifest == null || shipperManifest.DriverSecurityLog == null) return null;

            var driverInfo = shipperManifest.DriverSecurityLog;

            return new ACADriver
            {
                ShipperManifestId = shipperManifest.Id,
                DriverId = driverInfo.CarrierDriverId,
                DriverLicense = driverInfo.CarrierDriver.LicenseNumber,
                StateId = driverInfo.CarrierDriver.StateId,
                FirstIDTypeId = driverInfo.PrimaryIDTypeId,
                SecondIDTypeId = driverInfo.SecondaryIdTypeId,
                FirstIDMatched = driverInfo.PrimaryPhotoIdMatch,
                SecondIDMatched = driverInfo.SecondaryPhotoIdMatch,
                TruckingCompId = driverInfo.TruckingCompanyId,
                CapturePhoto = driverInfo.DriverPhoto,
                CaptureID = driverInfo.IDPhoto,
                STANumber = driverInfo.CarrierDriver.STANumber,
                AccountId = shipperManifest.AccountId,
                AccountName = shipperManifest.Account.AccountName,
                ShipperTypeName = shipperManifest.ShipperType.Name,
                ShipperName = shipperManifest.ShipperName
            };
        }

        public ShipperManifestStatus GetUserCurrentStep(long userId, long warehouseId)
        {
            var shipperManifest = DataContext.ShipperManifests.SingleOrDefault(m => m.UserId == userId && m.WarehouseId == warehouseId && m.StatusId != 42);
            return shipperManifest == null ? ShipperManifestStatus.ShipperInfoVerified : (ShipperManifestStatus)shipperManifest.StatusId;
        }

        public WizardHeader GetWizardHeader(long userId, long warehouseId)
        {
            var shipperManifest = DataContext.ShipperManifests.SingleOrDefault(m => m.UserId == userId && m.WarehouseId == warehouseId && m.StatusId != 42);
            if (shipperManifest == null)
                return new WizardHeader { Status = ShipperManifestStatus.Canceled };

            string driverName = null;
            string trackingCompany = null;
            string capturePhoto = null;

            if (shipperManifest.DriverSecurityLog != null)
            {
                if (shipperManifest.DriverSecurityLog.CarrierDriver != null)
                    driverName = shipperManifest.DriverSecurityLog.CarrierDriver.FullName;
                if (shipperManifest.DriverSecurityLog.Carrier != null)
                    trackingCompany = shipperManifest.DriverSecurityLog.Carrier.CarrierName;
                capturePhoto = shipperManifest.DriverSecurityLog.DriverPhoto;
            }

            long? currentAwbId = null;
            var currentAwb = shipperManifest.ShipperManifests_AWBs.FirstOrDefault(a => a.AWB.StatusId >= 37 && a.AWB.StatusId <= 40);
            if (currentAwb != null)
                currentAwbId = currentAwb.AWB.Id;

            return new WizardHeader
            {
                CarrierId = shipperManifest.AccountId,
                CarrierName = shipperManifest.Account.AccountName,
                CarrierCode = shipperManifest.Account.AccountCarriers.First().Carrier.Carrier3Code,
                FlightTypeId = shipperManifest.AircraftTypeId,
                ShipperTypeName = shipperManifest.ShipperType.Name,
                ShipperName = shipperManifest.ShipperName,
                Status = (ShipperManifestStatus)shipperManifest.StatusId,
                DriverName = driverName,
                CapturePhoto = capturePhoto,
                TrackingCompany = trackingCompany,
                CurrentAwbId = currentAwbId
            };
        }

        public AcceptFreightTaskItem GetAcceptFreightTask(long taskId)
        {
            var task = Context.Tasks.SingleOrDefault(t => t.TaskTypeId == 9 && t.Id == taskId);
            if (task == null)
                return null;

            var shipmentmanifest = Context.ShipperManifests.Single(m => m.Id == task.EntityId);
            var photo = Convert.FromBase64String(shipmentmanifest.DriverSecurityLog.DriverPhoto);

            return new AcceptFreightTaskItem
            {
                TaskId = task.Id,
                CarrierCode = shipmentmanifest.Account.AccountCarriers.Single(c => c.IsActive.HasValue && c.IsActive.Value).Carrier.Carrier3Code,
                DriverName = shipmentmanifest.DriverSecurityLog.CarrierDriver.FirstName + shipmentmanifest.DriverSecurityLog.CarrierDriver.FirstName,
                DriverCompany = shipmentmanifest.DriverSecurityLog.Carrier.CarrierName,
                DriverImageThumbnail = photo,
                Shipper = shipmentmanifest.ShipperName,
                Awbs = shipmentmanifest.ShipperManifests_AWBs.Count,
                Progress = task.ProgressPercent.HasValue ? task.ProgressPercent.Value : 0,
                Pcs = task.Pieces.HasValue ? task.Pieces.Value : 0,
                IsSealVerified = true,
                SealNumber = "",
                Date = DateTime.Now,
                CompleteOffLoad = true,
                IsOffLoaded = true,
                Locations = "",
                Reference = new List<string>()
            };
        }

        public long SaveAwbData(long id, string awbSerialNumber, string flightNumber, DateTime flightDate, long originId, long destinationId,
            int pieces, double weight, long weightUomId, int flightTypeId, long userId, long warehouseId)
        {
            var awbId = Context.MCH_SaveAwbData(id, awbSerialNumber, flightNumber, flightDate, originId, destinationId, pieces, weight, weightUomId, flightTypeId, userId, warehouseId);
            return awbId.Single().Value;
        }

        public void SaveSpecialHandlings(long awbId, long userId, long warehouseId, string handlingCodes, string attachments)
        {
            Context.MCH_SaveSpecialHandlings(awbId, userId, warehouseId, handlingCodes, attachments);
        }

        public List<long> GetSpecialHendlingGroupsForAwb(long awbId, long userId, long warehouseId)
        {
            var shipperManifest = DataContext.ShipperManifests.SingleOrDefault(m => m.UserId == userId && m.WarehouseId == warehouseId && m.StatusId != 42);
            return Context.SpecialHandlingLogs.Where(s => s.ShipperManifestId == shipperManifest.Id && s.UserId == userId && s.AwbId == awbId).Select(s => s.SPHGroupId).ToList();
        }

        public List<AwbDetialsModel> GetShipperManifestAwbs(long userId, long warehouseId)
        {
            var shipperManifest = DataContext.ShipperManifests.SingleOrDefault(m => m.UserId == userId && m.WarehouseId == warehouseId && m.StatusId != 42);

            return shipperManifest.ShipperManifests_AWBs.Select(s =>
            {
                DateTime? flightDate = null;
                string flightNumber = null;

                var legSegment = s.AWB.LegSegments.FirstOrDefault();
                if (legSegment != null)
                {
                    flightDate = legSegment.ETD;
                    flightNumber = legSegment.FlightNumber;
                }

                return new AwbDetialsModel
                {
                    Id = s.AwbId,
                    AwbSerialNumber = s.AWB.AWBSerialNumber,
                    CarrierCode = s.AWB.Carrier.Carrier3Code,
                    DestinationId = s.AWB.DestinationId,
                    FlightDate = flightDate,
                    FlightNumber = flightNumber,
                    OriginId = s.AWB.OriginId,
                    Pieces = s.AWB.TotalPieces,
                    Weight = s.AWB.TotalWeight,
                    WeightUomId = s.AWB.VolumeUOMId,
                    FlightTypeId = s.AWB.ShipperManifests_AWBs.Single().AircraftTypeId
                };
            }).ToList();
        }

        public AwbDetialsModel GetShipperManifestAwb(long awbId)
        {
            var awb = Context.AWBs.Single(a => a.Id == awbId);
            DateTime? flightDate = null;
            string flightNumber = null;

            var legSegment = awb.LegSegments.FirstOrDefault();
            if (legSegment != null)
            {
                flightDate = legSegment.ETD;
                flightNumber = legSegment.FlightNumber;
            }

            var tmpShipperAwb = this.Context.ShipperManifests_AWBs.Where(item => item.AwbId == awb.Id);

            return new AwbDetialsModel
                {
                    Id = awb.Id,
                    ConnectedToShipper = tmpShipperAwb.Any(),
                    AwbSerialNumber = awb.AWBSerialNumber,
                    CarrierCode = awb.Carrier.Carrier3Code,
                    DestinationId = awb.DestinationId,
                    FlightDate = flightDate,
                    FlightNumber = flightNumber,
                    OriginId = awb.OriginId,
                    Pieces = awb.TotalPieces,
                    Weight = awb.TotalWeight,
                    WeightUomId = awb.VolumeUOMId,
                    FlightTypeId = awb.ShipperManifests_AWBs.Single().AircraftTypeId
                };
        }

        public void SaveAwbShipperVerification(long awbId, long userId, long warehouseId, string fields)
        {
            Context.MCH_SaveAwbShipperVerification(awbId, userId, warehouseId, fields);
        }

        public List<IdCodeItem> GetAwbShipperVerifications(long awbId)
        {
            return Context.ShipperVerificationLogs.Where(s => s.AwbId == awbId).Select(s => new IdCodeItem
            {
                Id = s.FieldId,
                Code = s.Value
            }).ToList();
        }

        public void SaveAlternateScreening(long awbId, long userId, long warehouseId, bool isTendered, long ? approvedCCSFId, string seals, string screeningLogs)
        {
            DataContext.MCH_SaveAlternateScreening(awbId, userId, warehouseId, isTendered, approvedCCSFId, seals, screeningLogs);
        }

        public void Finalize(long userId, long warehouseId)
        {
            Context.MCH_FinalizeWizard(userId, warehouseId);
        }

        public void CompleteAwb(long userId, long warehouseId, long awbId)
        {
            Context.MCH_CompleteAwb(userId, warehouseId, awbId);
        }

        public void Cancel(long userId, long warehouseId)
        {
            DataContext.MCH_CancelWizard(userId, warehouseId);
        }

        public AwbHandlingItem GetAwbHandlings(long? awbId)
        {
            if (!awbId.HasValue) return null;

            var data = Context.SpecialHandlingLogs.Where(s => s.AwbId == awbId).ToList()
                .Select(s => s.SPHGroupId + "|" + (s.SPHSubGroupId.HasValue ? s.SPHSubGroupId.ToString() : "0"));

            var docs = Context.Attachments.Where(a => a.EntityID == awbId && a.EntityTypeId == 2).Select(a => new AttachmentModel
            {
                Id = a.Id,
                Name = a.Name,
                DocumentType = a.ShipperVerificationDoc.DocumentName,
                IsSnapshot = a.FileType == "Snapshot" ? true : false,
                SnapshotData = a.FileType == "Snapshot" ? a.AttachmentImage : null,
                Path = a.FileType == "Snapshot" ? null : a.FileName,
                ShipperDocTypeId = a.DocumentTypeId
            }).ToList();

            return new AwbHandlingItem
            {
                AwbId = awbId.Value,
                SphGroupIds = string.Join(",", data),
                Documents = docs
            };
        }

        public AwbScreeningItem GetAWBScreenings(long? awbId)
        {
            if (!awbId.HasValue) return null;

            var data = Context.ScreeningVerificationLogs.SingleOrDefault(s => s.AwbId == awbId);
            if (data == null) return null;

            return new AwbScreeningItem
            {
                AwbId = awbId.Value,
                IsTendered = data.TenderedAsScreened,
                ApprovedCCSFId = data.CCSFId,
                AlernativeScreeningOptions = data.AlternateScreeningLogs.Select(a => new SaveAltScreeningOptions
                {
                    Id = a.AltScreeningCaseId,
                    Checked = a.Value
                }).ToList(),
                SelaNumbers = data.ScreeningSealLogs.Select(s => new SealNumber 
                {
                     SealTypeId = s.SealTypeId,
                     SealTypeName = Context.ScreeningSealTypes.SingleOrDefault(item=> item.Id == s.SealTypeId) != null ? Context.ScreeningSealTypes.SingleOrDefault(item=> item.Id == s.SealTypeId).Type : String.Empty,
                     SealNumberValue = s.SealNumber
                }).ToList()
            };
        }
    }
}
