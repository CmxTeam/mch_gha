﻿using MCH.BLL.Model.Screening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class ScreeningUnit : DataUnitOfWork
    {
        public List<ScreeningDeviceItem> GetScreeningDevices(int warehouseId, ScreeningDeviceTypes device)
        {
            var screeningDevices = Context.ScreeningDevices.Where(d => d.WarehouseId == warehouseId).Select(d => d.SerialNo).ToList();

            return Context.ITDXes.ToList().Where(i => i.IsActive &&  screeningDevices.Contains(i.SerialNumber))
                .Where(i => i.LastConnected.HasValue && (DateTime.UtcNow - i.LastConnected.Value).Hours == 0)
                .Select(i => new ScreeningDeviceItem
                {
                    DeviceId = i.DeviceId,
                    SerialNumber = i.SerialNumber,
                    DeviceType = ScreeningDeviceTypes.ETD,
                    UserId = i.UserId,
                    IsActive = i.IsActive
                }).ToList();
        }

        public ScreeningDeviceItem GetDeviceInfo(long deviceId)
        {
            var device = Context.ITDXes.Where(i => i.IsActive && i.DeviceId == deviceId).SingleOrDefault();
            return device == null ? new ScreeningDeviceItem() : new ScreeningDeviceItem
            {
                DeviceId = device.DeviceId,
                SerialNumber = device.SerialNumber,
                DeviceType = ScreeningDeviceTypes.ETD,
                UserId = device.UserId,
                IsActive = device.IsActive
            };
        }

        public void KeepDeviceAlive(long deviceId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var device = Context.ITDXes.Single(i => i.DeviceId == deviceId && i.UserId == appUserId);
            device.LastConnected = DateTime.UtcNow;            
            Save();
        }

        public void LinkDeviceToUser(long deviceId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var device = Context.ITDXes.Single(i => i.DeviceId == deviceId);
            device.FileName = string.Empty;
            device.SystemMessage = string.Empty;
            device.SystemReadiness = string.Empty;
            device.ScreeningResults = string.Empty;
            device.UserId = appUserId;
            Save();
        }

        public List<MorphoScreeningItem> GetMorphoScreeningInfo(long deviceId, long userId)
        {
            var appUserId = GetAppUserId(userId);
            return Context.ITDXes.Where(i => i.UserId == appUserId && i.DeviceId == deviceId)
                .Select(i => new MorphoScreeningItem
                {
                    FileName = i.FileName,
                    ScreeningResults = i.ScreeningResults,
                    SubstancesFound = i.SubstancesFound,
                    SystemMessage = i.SystemMessage,
                    DeviceInfo = new ScreeningDeviceItem 
                    {
                        DeviceId = i.DeviceId,
                        DeviceType = ScreeningDeviceTypes.ETD,
                        SerialNumber = i.SerialNumber,
                        UserId = i.UserId,
                        IsActive = i.IsActive
                    }
                }).ToList();
        }

        public ScreeningTaskItem GetScreeningTask(long warehouseid, long taskId, long userId, string barcode)
        {
            long appUserId = GetAppUserId(userId);
            var data = Context.MCH_GetScreeningTask(taskId, appUserId, barcode, warehouseid).Single();
            return new ScreeningTaskItem
            {
                Awb = data.Awb,
                Hwb = data.Hwb,
                Destination = data.Destination,
                HasAlarm = data.HasAlarm.Value,
                Message = data.Message,
                Origin = data.Origin,
                ScreenedPieces = data.ScreenedPieces,
                ShipmentReference = data.ShipmentReference,
                TaskId = data.TaskId.Value,
                TotalPieces = data.TotalPieces.Value
            };
        }
        
        public void SaveScreeningTransaction(ScreeningTransaction model)
        {
            long appUserId = GetAppUserId(model.UserId);
            Context.MCH_SaveScreeningTransaction(appUserId, model.TaskId, model.Pieces, Enum.GetName(typeof(ScreeningDeviceTypes), model.Data.DeviceType), model.Data.DeviceId, model.Data.SerialNumber, model.SampleNumber, Enum.GetName(typeof(ScreeningResult), model.Result), model.SubstancesFound, model.PrescreenedCCSFId, model.Comment);
        }

        public CCSFItem ValidateCertificationNumber(string certificationNumber)
        {            
            var item = Context.TSA_Approved_Certified_Screening_Facilities.Where(t => t.TSAApprovalNumber == certificationNumber && t.CCSFStatus == "Active").ToList().SingleOrDefault(t => t.ExpirationDate >= DateTime.UtcNow.Date);
            if (item == null) return null;

            return new CCSFItem
            {
                CCSFId = item.Id,
                CertificationNumber = item.TSAApprovalNumber
            };
        }

        public List<ScreeningInspectionRemark> GetScreeningInspectionRemarks(int warehouseId)
        {
            return Context.Remarks.Where(r => r.RemarkType == "SCREENING" && r.WarehouseId == warehouseId).Select(r => new ScreeningInspectionRemark
                {
                    Id = r.Id,
                    Remark = r.Remark1
                }).ToList();
        }
    }
}
