﻿using MCH.BLL.DAL.Data;
using MCH.BLL.Exceptions;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.Enum;
using MCH.BLL.Utilities;
using MCH.BLL.Utilities.Printer;
using MCH.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class LabelPrintingUnit : DataUnitOfWork
    {


        public void PrintTestLabel(long userId, long accountId)
        {
            var tmpPrinterSettings = GetUserDefaultLabelPrinter(userId);

            if (tmpPrinterSettings == null)
            {
                throw new MCHBusinessException("Printer not found.", ErrorCodes.PrinterErrorCodes.PRINTER_NOT_FOUND);
            }

            //var tmpLabel = this.DataContext.LabelTemplatesZPLs.Where(l => l.AccountId == tmpLocalAccount.Id && l.LabelTypeId == (int)enumPrinterLabelTypes.ULDLabel).Select(l => l.LabelTemplateZPL).FirstOrDefault();

            string tmpLabelTest = @"~CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^PR4,4^JUS^LRN^CI0^XZ
^XA
^DFR:ULD.GRF
^FS
^MMT
^PW812
^LL1218
^LS0
^BY4,3,417^FT591,1071^BCB,,Y,N^FN01^FS
^XZ

^XA
^XFR:ULD.GRF
^FN01^FD5646546544 ULD lksdfklsdfjsdf^FS
^XZ
";

            string tmpPrinterName = @"\\" + tmpPrinterSettings.PrintServer.Name + @"\" + tmpPrinterSettings.Name;
            //string tmpPrinterName =  tmpPrinterSettings.Name;

            //string tmpPrinterName = "10.0.0.134";

            LabelPrinterUtils.PrintLable(tmpLabelTest, tmpPrinterName, 1);
        }

        public bool PrintTestDocument(long userId, int accountId, int numberCompies)
        {

            return true;
        }

        public void PrintULDLable(long uldId, long userId, int accountId)
        {

        }

        public void PrintAWBLabel(long awbId, long userId,long overPackItemId, int count)
        {
            var tmpPrinterSettings = this.GetUserDefaultLabelPrinter(GetAppUserId(userId));

            if (tmpPrinterSettings == null)
            {
                throw new MCHBusinessException("Printer not found.", ErrorCodes.PrinterErrorCodes.PRINTER_NOT_FOUND);
            }

            var lblName =
                DataContext.PrinterLabelTypes.Where(l => l.Id == (long)enumPrinterLabelTypes.AWBLabel)
                    .Select(l => l.Name)
                    .FirstOrDefault();
            if (lblName != null)
            {
                var accountId = DataContext.AWBs.Where(a => a.Id == awbId).Select(a => a.AccountId).FirstOrDefault() ?? 1;

                string tmpPrinterName = @"\\" + tmpPrinterSettings.PrintServer.Name + @"\" + tmpPrinterSettings.Name;

                Dictionary<string, string> param = new Dictionary<string, string>() { {"@ENTITYID", awbId.ToString() },
                                                                                      {"@OVERPACKITEMID", overPackItemId.ToString()},
                                                                                      {"@COUNT" , count.ToString()}};

                string conStr = Constants.Settings.Keys.PRINTER_CONNECTION_STRING;

                LabelPrinterUtils.ProcessLable(lblName, param, conStr, accountId, tmpPrinterName, count);
            }
        }

        public void PrintHWBLabel(long hwbId, long userId,long overPackItemId, int count)
        {
            var tmpPrinterSettings = this.GetUserDefaultLabelPrinter(GetAppUserId(userId));

            if (tmpPrinterSettings == null)
            {
                throw new MCHBusinessException("Printer not found.", ErrorCodes.PrinterErrorCodes.PRINTER_NOT_FOUND);
            }

            var lblName =
                DataContext.PrinterLabelTypes.Where(l => l.Id == (long)enumPrinterLabelTypes.HWBLabel)
                    .Select(l => l.Name)
                    .FirstOrDefault();
            if (lblName != null)
            {
                var accountId = DataContext.HWBs.Where(a => a.Id == hwbId).Select(a => a.AccountId).FirstOrDefault() ?? 1;

                string tmpPrinterName = @"\\" + tmpPrinterSettings.PrintServer.Name + @"\" + tmpPrinterSettings.Name;

                Dictionary<string, string> param = new Dictionary<string, string>() { {"@ENTITYID", hwbId.ToString() },
                                                                                      {"@OVERPACKITEMID", overPackItemId.ToString()},
                                                                                      {"@COUNT" , count.ToString()}};

                string conStr = Constants.Settings.Keys.PRINTER_CONNECTION_STRING;

                LabelPrinterUtils.ProcessLable(lblName, param, conStr, accountId, tmpPrinterName, count);
            }

        }

        public void SetUserDefault(long userId, int printerId)
        {
            var tmpUserProfile = this.DataContext.UserProfiles.SingleOrDefault(u => u.ShellUserId == userId);
            if (tmpUserProfile == null)
            {
                throw new MCHBusinessException("User not found.", ErrorCodes.UserErrorCodes.USER_NOT_FOUND);
            }

            var tmpPrinter = this.DataContext.Printers.SingleOrDefault(p => p.Id == printerId);
            if (tmpPrinter == null)
            {
                throw new MCHBusinessException("Printer not found.", ErrorCodes.PrinterErrorCodes.PRINTER_NOT_FOUND);
            }

            if (tmpUserProfile.UserPrinters.Any())
            {
                foreach (var item in tmpUserProfile.UserPrinters)
                {
                    item.IsDefault = false;
                }
            }

            var tmpUserPrinter = tmpUserProfile.UserPrinters.Where(up => up.PrinterId == printerId).FirstOrDefault();

            if (tmpUserPrinter == null)
            {
                tmpUserPrinter = new DAL.Data.UserPrinter { IsDefault = true, PrinterId = printerId };
                tmpUserProfile.UserPrinters.Add(tmpUserPrinter);
            }
            else
            {
                tmpUserPrinter.IsDefault = true;
            }

            this.DataContext.SaveChanges();

        }

        public List<Model.Common.PrinterItem> GetPrinters(long? userId, Model.Common.PrinterTypes? printerType)
        {
            var tmpPrinters = this.DataContext.Printers.AsQueryable();

            long? appUserId = null;

            if (userId.HasValue && userId.Value > 0)
            {
                appUserId = GetAppUserId(userId.Value);
                tmpPrinters = tmpPrinters.Where(p => p.UserPrinters.Any(up => up.UserId == appUserId));
            }

            if (printerType.HasValue)
            {
                tmpPrinters = tmpPrinters.Where(p => p.TypeId == (int)printerType.Value);
            }

            return tmpPrinters.Select(p => new PrinterItem { IsDefault = p.UserPrinters.Where(up => up.UserId == appUserId).FirstOrDefault().IsDefault, PrinterId = p.Id, PrinterName = p.Name, PrinterType = (PrinterTypes)p.TypeId }).ToList();
        }


        private Printer GetUserDefaultLabelPrinter(long userId)
        {
            return this.DataContext.UserPrinters.Where(up => up.UserId == userId).OrderByDescending(up=>up.IsDefault).Select(up => up.Printer).Where(p => p.TypeId == (int)enumPrinterTypes.Label).FirstOrDefault();
        }
    }
}
