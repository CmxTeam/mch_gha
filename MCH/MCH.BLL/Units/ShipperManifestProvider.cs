﻿using MCH.BLL.DAL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class ShipperManifestProvider : DataUnitOfWork
    {
        public void AddShipperManifestAwb(long userId, long warehouseId, MCH.BLL.Model.Common.AwbModel awb)
        {
            var shipperManifest = TryFindShipperManifest(userId, warehouseId);
            if (shipperManifest == null)
                throw new Exception("Shipper manifest cannot be null at this step");

            Context.MCH_AddShipperManifestAwb(awb.Id, awb.CarrierId, awb.AWBNumber, awb.FlightNumber, awb.FlightDate, awb.OriginId, awb.DestinationId, awb.TotalPieces,
                awb.TotalWeight, awb.WeightUOMId, userId, shipperManifest.Id);
        }

        public IEnumerable<MCH.BLL.Model.Common.AwbModel> GetShipperManifestAwbs(long userId, long warehouseId)
        {
            var shipperManifest = TryFindShipperManifest(userId, warehouseId);
            if (shipperManifest == null) return new List<MCH.BLL.Model.Common.AwbModel>();

            return shipperManifest.ShipperManifests_AWBs.Select(s => s.AWB).Select(awb => new MCH.BLL.Model.Common.AwbModel
            {
                Id = awb.Id,
                AWBNumber = awb.AWBSerialNumber,
                CarrierId = awb.CarrierId.Value,
                CarrierNumber = awb.Carrier.Carrier3Code,
                Origin = awb.Port.IATACode,
                OriginId = awb.Port.Id,
                Destination = awb.Port1.IATACode,
                DestinationId = awb.Port1.Id,
                FlightDate = awb.LegSegments.SingleOrDefault(l => l.Sequence == 1) == null ? null : awb.LegSegments.SingleOrDefault(l => l.Sequence == 1).ETD,
                FlightNumber = awb.LegSegments.SingleOrDefault(l => l.Sequence == 1) == null ? null : awb.LegSegments.SingleOrDefault(l => l.Sequence == 1).FlightNumber,
                TotalPieces = awb.TotalPieces.HasValue ? awb.TotalPieces.Value : 0,
                TotalWeight = awb.TotalWeight.HasValue ? awb.TotalWeight.Value : 0,
                WeightUOM = awb.UOM.TwoCharCode,
                WeightUOMId = awb.UOM.Id
            }).ToList();
        }

        private ShipperManifest TryFindShipperManifest(long userId, long warehouseId)
        {
            var shipperManifest = Context.ShipperManifests.SingleOrDefault(s => s.UserId == userId && s.WarehouseId == warehouseId);
            return shipperManifest == null ? null : shipperManifest;
        }
    }
}
