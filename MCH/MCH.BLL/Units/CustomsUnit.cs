﻿using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Customs;
using MCH.BLL.Model.Enum;
using MCH.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CheckListItem = MCH.BLL.Model.Customs.CheckListItem;

namespace MCH.BLL.Units
{
    public class CustomsUnit : DataUnitOfWork
    {
        private GenericRepository<UserProfile> userRepository;

        public GenericRepository<UserProfile> UserRepository
        {
            get
            {
                if (this.userRepository == null)
                    this.userRepository = new GenericRepository<UserProfile>(base.DataContext);
                return userRepository;
            }
        }

        public CheckListModel GetCheckListModel(long taskId, long userId, long awbId)
        {
            CheckListModel tmpResult = null;
            var tmpTask = this.Context.Tasks.Single(item => item.Id == taskId);
            var tmpUser = UserRepository.GetByID(GetAppUserId(userId));

            // if the Task is Not Assigned, assign to the passed user
            if (tmpTask.StatusId == (int)enumTaskStatuses.NotAssigned)
            {
                if (!Context.TaskAssignments.Where(t => t.TaskId == taskId && t.UserId == tmpUser.Id).Any())
                {
                    tmpTask.TaskAssignments.Add(new TaskAssignment() { UserId = tmpUser.Id, AssignedBy = tmpUser.Id, RecDate = DateTime.UtcNow, AssignedOn = DateTime.UtcNow });
                    this.Context.SaveChanges();
                }                
            }

            var tmpChecklist = this.Context.CheckLists.OrderByDescending(item => item.RecDate).FirstOrDefault(item => item.TaskTypeId == tmpTask.TaskTypeId);

            if (tmpChecklist != null)
            {
                var tmpAwg = this.Context.AWBs.SingleOrDefault(i => i.Id == awbId);

                var checklistResult = this.Context.CheckListResultsParents.SingleOrDefault(item => item.EntityId == awbId && item.EntityTypeId == (int)enumEntityTypes.AWB);

                var tmpUserWarehouse = this.Context.UserWarehouses.FirstOrDefault(item => item.IsDefault && item.UserId == tmpUser.Id);

                var tmpWarehouse = tmpUserWarehouse != null ? tmpUserWarehouse.Warehouse : null;

                var tmpPlace = tmpWarehouse != null ? (tmpWarehouse.State != null ? tmpWarehouse.State.TwoCharacterCode : String.Empty) + "," + tmpWarehouse.City : String.Empty;

                tmpResult = new CheckListModel
                {
                    EntityId = awbId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    TaskId = taskId,
                    Username = tmpUser.FirstName + " " + tmpUser.LastName,
                    ShipmentNumber = tmpAwg.AWBSerialNumber,
                    Place = tmpPlace,
                    Year = tmpChecklist.Year,
                    Title = tmpChecklist.Header,
                    Note = tmpChecklist.HeaderText
                };

                if (checklistResult != null)
                {
                    tmpResult.Place = checklistResult.Place;
                    tmpResult.CommentedOn = checklistResult.CompletedOn;
                    tmpResult.Comments = checklistResult.Comments;
                    tmpResult.Username = tmpUser.FirstName + tmpUser.LastName;
                    tmpResult.SignatureData = checklistResult.SignatureData;
                    tmpResult.Place = tmpPlace;
                }

                var tmpListData = tmpChecklist.CheckListItemHeaders.Where(item => item.IsActive).Select(item => new CheckListHeader
                {
                    Id = item.Id,
                    Name = item.ListItemHeader,
                    Items = item.CheckListItems.Where(e => e.ParentId == null).Select(e => new MCH.BLL.Model.Customs.CheckListItem
                    {
                        Id = e.Id,
                        Options = e.ValueList,
                        ParentId = e.ParentId,
                        SubItems = e.CheckListItems1.Select(p => new CheckListItem
                        {
                            Id = p.Id,
                            Options = p.ValueList,
                            Number = p.SortOrder,
                            Text = p.ListItemText,
                            SelectedOption = checklistResult != null && checklistResult.CheckListResults.FirstOrDefault(u => u.CheckListItemId == p.Id) != null ? checklistResult.CheckListResults.First(u => u.CheckListItemId == p.Id).ResultValue : String.Empty
                        }).ToList(),
                        Text = e.ListItemText,
                        Number = e.SortOrder,
                        SelectedOption = checklistResult != null && checklistResult.CheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id) != null ? checklistResult.CheckListResults.FirstOrDefault(u => u.CheckListItemId == e.Id).ResultValue : String.Empty
                    }).ToList()
                }).ToList();

                tmpResult.CheckList = tmpListData;
            }

            return tmpResult;
        }

        public List<CheckListShipmentItem> GetCheckListTasks(long warehouseId, int menuId, long userId, CheckListStatusTypes status)
        {
            return Context.MCH_GetCheckListTasks(warehouseId, menuId, GetAppUserId(userId), (int)status).Select(i => new CheckListShipmentItem
            {
                AwbId = i.AWBId,
                TaskId = i.TaskId,
                AwbNumber = i.AWBNumber,
                CarrierCode = i.CarrierCode,
                ETD = i.ETD,
                FlightNumber = i.FlightNumber,
                Locations = i.Locations,
                Pieces = i.Pieces.HasValue ? i.Pieces.Value : 0,
                Progress = i.Progress.HasValue ? i.Progress.Value : 0,
                Status = (CheckListStatusTypes)i.Status
            }).ToList();
        }

        public void FinalizeChecklistResult(ChecklistResultsParam param)
        {
            var user = UserRepository.GetByID(GetAppUserId(param.UserId));
            var userName = user.FirstName + " " + user.LastName;
            SaveChecklistInner(param, userName, user.Id);
            var tmpTask = Context.Tasks.Single(item => item.Id == param.TaskId);

            //TODO: Harut fix this
            Context.Transactions.Add(new Transaction()
            {
                Description = tmpTask.TaskType.Code + " Checklist Completed",                
                EntityId = param.EntityId,
                EntityTypeId = (int)enumEntityTypes.AWB,
                //StatusId = tmpTask.StatusId,                    
                TaskId = tmpTask.Id,
                TaskTypeId = tmpTask.TaskTypeId,
                UserId = user.Id
            });


            //TODO:Harut fix this
            tmpTask.EndDate = DateTime.Now;
            //tmpTask.StatusId = (int)enumTaskStatuses.Completed;

            Context.TaskTransactions.Add(new TaskTransaction()
            {
                Description = "Task Completed",
                RecDate = DateTime.Now,
                StatusTimestamp = DateTime.Now,
                StatusId = (int)enumTaskStatuses.Completed,
                TaskId = tmpTask.Id,
                UserId = param.UserId
            });

            Context.HostPlus_CIMPOutboundQueue.Add(new HostPlus_CIMPOutboundQueue()
            {
                AWBId = param.EntityId,
                Emailed = false,
                Timestamp = DateTime.Now,
                Transmitted = false,
                StationId = tmpTask.Warehouse.PortId.Value,
                MessageType = tmpTask.TaskType.Code
            });

            Save();
        }

        public void SaveCheckListResult(ChecklistResultsParam param)
        {
            var user = UserRepository.GetByID(GetAppUserId(param.UserId));
            var userName = user.FirstName + " " + user.LastName;
            SaveChecklistInner(param, userName, user.Id);
            var tmpTask = Context.Tasks.Single(item => item.Id == param.TaskId);

            //TODO:Harut fix this
            Context.Transactions.Add(new Transaction()
                {
                    Description = tmpTask.TaskType.Code + " Checklist Saved",
                    EntityId = param.EntityId,
                    EntityTypeId = (int)enumEntityTypes.AWB,
                    //StatusId = tmpTask.StatusId,
                    TaskId = tmpTask.Id,
                    TaskTypeId = tmpTask.TaskTypeId,
                    UserId = user.Id
                });

            Context.SaveChanges();
        }

        private void SaveChecklistInner(ChecklistResultsParam param, string userName, long userId)
        {
            var tmpDBResult = Context.CheckListResultsParents.Where(item => item.EntityId == param.EntityId && item.EntityTypeId == (int)enumEntityTypes.AWB).FirstOrDefault();
            var tmpUserWarehouse = Context.UserWarehouses.FirstOrDefault(item => item.IsDefault && item.UserId == userId);
            var tmpWarehouse = tmpUserWarehouse != null ? tmpUserWarehouse.Warehouse : null;
            var tmpPlace = tmpWarehouse != null ? (tmpWarehouse.State != null ? tmpWarehouse.State.TwoCharacterCode : String.Empty) + "," + tmpWarehouse.City : String.Empty;

            if (tmpDBResult == null)
            {
                tmpDBResult = new CheckListResultsParent();
                tmpDBResult.CompletedOn = DateTime.UtcNow;
                tmpDBResult.EntityId = param.EntityId;
                tmpDBResult.EntityTypeId = (int)enumEntityTypes.AWB;
                tmpDBResult.TaskId = param.TaskId;
                tmpDBResult.RecDate = DateTime.UtcNow;

                Context.CheckListResultsParents.Add(tmpDBResult);
            }

            tmpDBResult.Comments = param.Comments;
            tmpDBResult.SignatureData = param.SignatureData;
            tmpDBResult.UserId = userId;
            tmpDBResult.Place = tmpPlace;

            if (param.Items != null && param.Items.Any())
            {
                foreach (var item in param.Items)
                {
                    var tmpResultItemQuery = Context.CheckListResults.Where(e => e.CheckListItemId == item.ItemId);
                    if (tmpDBResult.Id != 0)
                    {
                        tmpResultItemQuery = tmpResultItemQuery.Where(e => e.ParentId == tmpDBResult.Id);
                    }

                    var tmpResutlItem = tmpResultItemQuery.FirstOrDefault();
                    if (tmpResutlItem == null)
                    {
                        tmpResutlItem = new CheckListResult();
                        tmpResutlItem.CheckListResultsParent = tmpDBResult;
                        tmpResutlItem.CheckListItemId = item.ItemId;
                        tmpResutlItem.CreateDate = DateTime.UtcNow;

                        Context.CheckListResults.Add(tmpResutlItem);
                    }
                    tmpResutlItem.LastUpdated = DateTime.UtcNow;
                    tmpResutlItem.ResultValue = item.OptionName;
                    tmpResutlItem.UpdatedBy = userName;

                }
            }
        }
    }
}
