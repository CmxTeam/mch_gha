﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMX.Framework.Utils.Security;
using MCH.BLL.DAL.Data;
using MCH.BLL.Model.BuildFreight;
using MCH.BLL.Model.Enum;

namespace MCH.BLL.Units
{
    public class BuildFreightUnit : DataUnitOfWork
    {
        public List<BuildFreightTaskItem> GetBuildFreightTasks(long warehouseId, long userId,
            enumBuildFreightStatusTypes status)
        {
            var buildTasks = Context.MCH_GetBuildFreightTasks(userId, warehouseId, (int)status, null, null);

            return buildTasks.Select(t => new BuildFreightTaskItem
                                          {
                                              CarrierCode = t.CarrierCode,
                                              FlightNumber = t.FlightNumber,
                                              ETD = t.ETD,
                                              Destination = t.Destination,
                                              TotalUlds = t.TotalUlds,
                                              TotalAwbs = t.TotalAwbs,
                                              TotalPieces = t.TotalPieces,
                                              ScannedUlds = t.ScannedUlds,
                                              ScannedAwbs = t.ScannedAwbs,
                                              ScannedPieces = t.ScannedPieces,
                                              TaskId = t.TaskId,
                                              FlightManifestId = t.FlightManifestId,
                                              PercentProgress = t.Progress,
                                              Flag = t.Flag,
                                              Locations = t.Locations,
                                              Status = (enumBuildFreightStatusTypes)t.Status
                                          }
                                    ).ToList();
        }

        public BuildFreightTaskItem GetBuildFreightTask(long taskId, long flightManifestId)
        {
            var buildTask = Context.MCH_GetBuildFreightTasks(null, null, null, taskId, flightManifestId);

            return buildTask.Select(t => new BuildFreightTaskItem
                                            {
                                                CarrierCode = t.CarrierCode,
                                                FlightNumber = t.FlightNumber,
                                                ETD = t.ETD,
                                                Destination = t.Destination,
                                                TotalUlds = t.TotalUlds,
                                                TotalAwbs = t.TotalAwbs,
                                                TotalPieces = t.TotalPieces,
                                                ScannedUlds = t.ScannedUlds,
                                                ScannedAwbs = t.ScannedAwbs,
                                                ScannedPieces = t.ScannedPieces,
                                                TaskId = t.TaskId,
                                                FlightManifestId = t.FlightManifestId,
                                                PercentProgress = t.Progress,
                                                Flag = t.Flag,
                                                Locations = t.Locations,
                                                Status = (enumBuildFreightStatusTypes)t.Status
                                            }
                                    ).FirstOrDefault();
        }

        public void StageLocationTask(long taskId, long userId, long locationId)
        {
            Context.MCH_StageLocationTask(taskId, userId, locationId);
        }

        public TransactionStatus AddUld(long userId, long flightManifestId, long uldUnitTypeId, string serialNumber,
            string carrierCode, long taskId)
        {
            if (Context.ULDs.Any(u => u.SerialNumber == serialNumber && u.FlightManifestId == flightManifestId))
            {
                return new TransactionStatus() { Status = false, Error = "ULD Already exists for the flight" };
            }

            try
            {
                var shipmentUnitType = DataContext.ShipmentUnitTypes.Single(t => t.Id == uldUnitTypeId);
                var carrier = DataContext.Carriers.First(t => t.CarrierCode == carrierCode);
                var user = DataContext.UserProfiles.First(u => u.Id == userId);

                var uld = new ULD()
                          {
                              ShipmentUnitType = shipmentUnitType,
                              SerialNumber = serialNumber,
                              Carrier = carrier,
                              StatusID = 20,
                              StatusTimestamp = DateTime.UtcNow,
                              FlightManifestId = flightManifestId,
                              IsBUP = false,
                              TotalUnitCount = shipmentUnitType.Code == "LOOSE" ? 0 : 1,
                              TotalPieces = 0
                          };

                DataContext.ULDs.Add(uld);
                DataContext.SaveChanges();
                DataContext.Entry(uld).GetDatabaseValues();

                var trans = new Transaction()
                            {
                                EntityTypeId = 4,
                                EntityId = uld.Id,
                                StatusId = 20,
                                StatusTimestamp = DateTime.UtcNow,
                                Reference = "ULD# " + shipmentUnitType.Code + (serialNumber ?? "") + (carrier.CarrierCode ?? ""),
                                Description = "ULD added",
                                TaskTypeId = 5,
                                TaskId = taskId,
                                TransactionActionId = 1,
                                UserId = userId
                            };

                DataContext.Transactions.Add(trans);
                DataContext.SaveChanges();

                return new TransactionStatus() { Status = true };
            }
            catch (Exception)
            {
                return new TransactionStatus() { Status = false, Error = "Internal Server Error." };
            }
        }

        public TransactionStatus EditUld(long uldId, long userId, long flightManifestId, long uldUnitTypeId,
            string serialNumber, string carrierCode, long taskId)
        {
            try
            {
                var tmpUld = Context.ULDs.Single(u => u.Id == uldId);

                var shipmentUnitType = DataContext.ShipmentUnitTypes.Single(t => t.Id == uldUnitTypeId);
                var carrier = DataContext.Carriers.First(t => t.CarrierCode == carrierCode);
                var user = DataContext.UserProfiles.First(u => u.Id == userId);

                tmpUld.SerialNumber = serialNumber;
                tmpUld.ShipmentUnitType = shipmentUnitType;
                tmpUld.Carrier = carrier;
                tmpUld.FlightManifestId = flightManifestId;


                var trans = new Transaction()
                {
                    EntityTypeId = 4,
                    EntityId = uldId,
                    StatusId = 20,
                    StatusTimestamp = DateTime.UtcNow,
                    Reference = "ULD# " + shipmentUnitType.Code + (serialNumber ?? "") + (carrier.CarrierCode ?? ""),
                    Description = "ULD updated to " + shipmentUnitType.Code + (serialNumber ?? "") + (carrier.CarrierCode ?? ""),
                    TaskTypeId = 5,
                    TaskId = taskId,
                    TransactionActionId = 1,
                    UserId = userId
                };

                DataContext.Transactions.Add(trans);
                DataContext.SaveChanges();

                return new TransactionStatus() { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus() { Status = false, Error = "Internal Server Error." };
            }
        }

        public TransactionStatus DeleteUld(long userId, long flightManifestId, long uldId, long taskId)
        {
            try
            {
                var tmpUld = Context.ULDs.Single(u => u.Id == uldId);
                if (tmpUld.TotalPieces > 0)
                {
                    return new TransactionStatus() { Status = false, Error = "Uld is not empty. Cannot be removed." };
                }

                var user = DataContext.UserProfiles.First(u => u.Id == userId);

                var trans = new Transaction()
                {
                    EntityTypeId = 4,
                    EntityId = uldId,
                    StatusId = 20,
                    StatusTimestamp = DateTime.UtcNow,
                    Reference = "ULD# " + ((tmpUld.ShipmentUnitType == null ? "" : tmpUld.ShipmentUnitType.Code) ?? "") + (tmpUld.SerialNumber ?? "") + (tmpUld.Carrier.CarrierCode ?? ""),
                    Description = "ULD " + ((tmpUld.ShipmentUnitType == null ? "" : tmpUld.ShipmentUnitType.Code) ?? "") + (tmpUld.SerialNumber ?? "") + (tmpUld.Carrier.CarrierCode ?? "") + " removed from flight",
                    TaskTypeId = 5,
                    TaskId = taskId,
                    TransactionActionId = 1,
                    UserId = userId
                };

                DataContext.ULDs.Remove(tmpUld);
                DataContext.Transactions.Add(trans);
                DataContext.SaveChanges();

                return new TransactionStatus() { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus() { Status = false, Error = "Internal Server Error." };
            }
        }

        public TransactionStatus SwitchBUPMode(long taskId, long uldId, long userId, long manifestId)
        {
            try
            {
                var tmpUld = Context.ULDs.Single(u => u.Id == uldId);
                if (tmpUld.TotalPieces == 0 && (!tmpUld.IsBUP.HasValue || tmpUld.IsBUP == false))
                {
                    return new TransactionStatus() { Status = false, Error = "Uld cannot be set to BUP." };
                }
                if (!tmpUld.IsBUP.HasValue || tmpUld.IsBUP == false)
                    tmpUld.IsBUP = true;
                else
                    tmpUld.IsBUP = false;

                var user = DataContext.UserProfiles.First(u => u.Id == userId);

                var trans = new Transaction()
                {
                    EntityTypeId = 4,
                    EntityId = uldId,
                    StatusId = 20,
                    StatusTimestamp = DateTime.UtcNow,
                    Reference = "ULD# " + tmpUld.ShipmentUnitType.Code + (tmpUld.SerialNumber ?? "") + ((tmpUld.Carrier == null ? "" : tmpUld.Carrier.CarrierCode) ?? ""),
                    Description = tmpUld.IsBUP.Value ? "ULD set to BUP" : "ULD reset from BUP",
                    TransactionActionId = 1,
                    TaskId = taskId,
                    UserId = userId
                };

                DataContext.Transactions.Add(trans);
                DataContext.SaveChanges();

                return new TransactionStatus() { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus() { Status = false, Error = "Internal Server Error." };
            }
        }

        public List<BuildFreightUldListItem> GetBuildFreightUldListItems(long userId, long flightManifestId, enumBuildFreightStatusTypes status, long taskId)
        {
            var buildULDList = Context.MCH_GetBuildFreightUldList(userId, flightManifestId, (int)status, taskId, null);
            return buildULDList.Select(u => new BuildFreightUldListItem()
                                            {
                                                UldId = u.UldId,
                                                UldType = u.UldType,
                                                Uld = u.Uld,
                                                UldSerialNo = u.UldSerialNo,
                                                UldPrefix = u.UldPrefix,
                                                CarrierCode = u.CarrierCode,
                                                Locations = u.Locations,
                                                IsBUP = u.IsBUP,
                                                TotalPieces = u.TotalPieces,
                                                TotalAwbs = u.TotalAwbs,
                                                Weight = u.Weight,
                                                TareWeight = u.TareWeight,
                                                WeightUOM = u.WeightUOM,
                                                StageLocation = u.StageLocation,
                                                StagedBy = u.StagedBy,
                                                StagedDate = u.StagedDate,
                                                Flag = u.Flag,
                                                Status = (enumBuildFreightStatusTypes)(u.Status ?? 5)
                                            }
                                        ).ToList();
        }

        public BuildFreightUldListItem GetBuildFreightUld(long userId, long flightManifestId, long uldId)
        {
            var buildULD = Context.MCH_GetBuildFreightUldList(userId, flightManifestId, null, null, uldId);
            return buildULD.Select(u => new BuildFreightUldListItem()
                                            {
                                                UldId = u.UldId,
                                                UldType = u.UldType,
                                                Uld = u.Uld,
                                                UldSerialNo = u.UldSerialNo,
                                                UldPrefix = u.UldPrefix,
                                                CarrierCode = u.CarrierCode,
                                                Locations = u.Locations,
                                                IsBUP = u.IsBUP,
                                                TotalPieces = u.TotalPieces,
                                                TotalAwbs = u.TotalAwbs,
                                                Weight = u.Weight,
                                                TareWeight = u.TareWeight,
                                                WeightUOM = u.WeightUOM,
                                                StageLocation = u.StageLocation,
                                                StagedBy = u.StagedBy,
                                                StagedDate = u.StagedDate,
                                                Flag = u.Flag,
                                                Status = (enumBuildFreightStatusTypes)(u.Status ?? 5)
                                            }
                                       ).FirstOrDefault();
        }

        public List<BuildFreightShipmentItem> GetShipments(long userId, long flightManifestId, long taskId, enumBuildFreightStatusTypes status)
        {

            var shipments = Context.MCH_GetBuildShipments(userId, flightManifestId, taskId, (int)status, null);
            return shipments.Select
                                (s =>
                                    new BuildFreightShipmentItem()
                                    {
                                        References = s.References.Split(',').ToList(),
                                        AwbId = s.AwbId,
                                        DetailId = s.DetailId,
                                        AWB = s.AWB,
                                        TotalPieces = s.TotalPieces,
                                        ScannedPieces = s.ScannedPieces,
                                        Locations = s.Locations,
                                        Origin = s.Origin,
                                        Destination = s.Destination,
                                        Status = (enumBuildFreightStatusTypes)s.Status,
                                        PercentProgress = s.PercentProgress
                                    }
                                ).ToList();
        }

        public List<BuildFreightShipmentItem> GetShipmentsByUld(long userId, long flightManifestId, long taskId, long uldId)
        {
            var shipments = Context.MCH_GetBuildShipments(userId, flightManifestId, taskId, null, uldId);
            return shipments.Select
                                (s =>
                                    new BuildFreightShipmentItem()
                                    {
                                        References = s.References.Split(',').ToList(),
                                        AwbId = s.AwbId,
                                        DetailId = s.DetailId,
                                        AWB = s.AWB,
                                        TotalPieces = s.TotalPieces,
                                        ScannedPieces = s.ScannedPieces,
                                        Locations = s.Locations,
                                        Origin = s.Origin,
                                        Destination = s.Destination,
                                        Status = (enumBuildFreightStatusTypes)s.Status,
                                        PercentProgress = s.PercentProgress
                                    }
                                ).ToList();
        }

        public Transactional<BuildFreightShipmentItem> ScanShipment(long userId, long taskId, long flightManifestId, string shipmentNumber)
        {
            Transactional<BuildFreightShipmentItem> retval = new Transactional<BuildFreightShipmentItem>();
            try
            {
                var s = Context.MCH_ScanBuildShipment(userId, taskId, flightManifestId, shipmentNumber).First();
                var errorMessage = s.Message;

                if (errorMessage.ToLower() != "success")
                {
                    retval.Data = null;
                    retval.Transaction = new TransactionStatus() { Error = errorMessage, Status = false };
                }
                else
                {
                    retval.Data = new BuildFreightShipmentItem()
                                  {
                                      References = s.References.Split(',').ToList(),
                                      AwbId = s.AwbId,
                                      DetailId = s.DetailId,
                                      AWB = s.AWB,
                                      TotalPieces = s.TotalPieces,
                                      ScannedPieces = s.ScannedPieces,
                                      Locations = s.Locations,
                                      Origin = s.Origin,
                                      Destination = s.Destination,
                                      Status = (enumBuildFreightStatusTypes)s.Status,
                                      PercentProgress = s.PercentProgress
                                  };

                    retval.Transaction = new TransactionStatus() { Status = true };
                }
            }
            catch (Exception ex)
            {
                retval.Data = null;
                retval.Transaction = new TransactionStatus() { Error = "Internal Server Error.", Status = false };
            }
            return retval;
        }

        public TransactionStatus FinalizeBuild(long taskId, long flightManifestId, long userId)
        {
            try
            {
                Context.MCH_FinalizeBuild(taskId, flightManifestId, userId);
                return new TransactionStatus() { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus() { Status = false, Error = "Internal Server Error." };
            }
        }

        public TransactionStatus UpdateBuildULDWeight(long userId, long taskId, long flightManifestId, long uldId, double weight, double tareWeight, string weightUOM)
        {
            try
            {
                var uld = Context.ULDs.First(u => u.Id == uldId && u.FlightManifestId == flightManifestId);
                var user = DataContext.UserProfiles.First(u => u.Id == userId);

                var weightUom = Context.UOMs.FirstOrDefault(um => um.UOMName == weightUOM);
                uld.TotalWeight = weight;
                uld.WeightUOMId = weightUom == null ? null : (int?)weightUom.Id;
                uld.TareWeight = tareWeight;
                uld.FreightWeight = weight - tareWeight;
                uld.StatusID = 24;
                uld.StatusTimestamp = DateTime.UtcNow;

                var trans = new Transaction()
                {
                    EntityTypeId = 4,
                    EntityId = uld.Id,
                    StatusId = 24,
                    StatusTimestamp = DateTime.UtcNow,
                    Reference = "ULD# " + uld.ShipmentUnitType.Code + (uld.SerialNumber ?? "") + (uld.Carrier.CarrierCode ?? ""),
                    Description = String.Format("ULD {0} Total Weight Captured: {1}", uld.SerialNumber, weight),
                    TaskId = taskId,
                    TransactionActionId = 1,
                    UserId = userId
                };

                Context.Transactions.Add(trans);
                Context.SaveChanges();

                return new TransactionStatus() { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus() { Status = false, Error = "Internal Server Error." };
            }
        }

        public TransactionStatus StageBuildULD(long uldId, long userId, long flightManifestId, int locationId)
        {
            try
            {
                var uld = Context.ULDs.First(u => u.Id == uldId && u.FlightManifestId == flightManifestId);
                var user = DataContext.UserProfiles.First(u => u.Id == userId);
                var location = DataContext.WarehouseLocations.First(l => l.Id == locationId);

                uld.LocationId = locationId;
                uld.RecoveredBy = (user.FirstName ?? "") + " " + (user.LastName ?? "");
                uld.RecoveredOn = DateTime.UtcNow;
                uld.StatusID = 25;
                uld.StatusTimestamp = DateTime.UtcNow;

                var trans = new Transaction()
                {
                    EntityTypeId = 4,
                    EntityId = uld.Id,
                    StatusId = 25,
                    StatusTimestamp = DateTime.UtcNow,
                    Reference = "ULD# " + uld.ShipmentUnitType.Code + (uld.SerialNumber ?? "") + (uld.Carrier.CarrierCode ?? ""),
                    Description = String.Format("ULD {0} Built and Staged At: {1} By: {2}", uld.SerialNumber, location.Location, (user.FirstName ?? "") + " " + (user.LastName ?? "")),
                    TransactionActionId = 1,
                    UserId = userId
                };

                Context.Transactions.Add(trans);
                Context.SaveChanges();

                return new TransactionStatus() { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus() { Status = false, Error = "Internal Server Error." };
            }
        }

        public BuildUldViewItem GetBuildULDView(long detailId)
        {
            var buildULD = Context.MCH_GetBuildULDView(detailId);

            return buildULD.Select(u => new BuildUldViewItem()
                                        {
                                            DetailId = u.DetailId,
                                            AwbId = u.AwbId,
                                            AWB = u.AWB,
                                            LoadCount = u.LoadCount
                                        }).FirstOrDefault();
        }

        public TransactionStatus RemovePiecesFromUld(long userId, long uldId, long detailId, int pieces)
        {
            try
            {
                var awbDetail = Context.ManifestAWBDetails.Single(d => d.Id == detailId);
                var uld = Context.ULDs.Single(d => d.Id == uldId);
                var user = DataContext.UserProfiles.First(u => u.Id == userId);

                if (pieces > awbDetail.LoadCount)
                {
                    return new TransactionStatus() { Status = false, Error = "Number of pieces is more than loaded in ULD." };
                }
                if (pieces == awbDetail.LoadCount)
                {
                    Context.ManifestAWBDetails.Remove(awbDetail);
                }
                else
                {
                    awbDetail.LoadCount = awbDetail.LoadCount - pieces;
                }

                var trans = new Transaction()
                {
                    EntityTypeId = 4,
                    EntityId = uld.Id,
                    StatusId = 22,
                    StatusTimestamp = DateTime.UtcNow,
                    Reference = "ULD# " + uld.ShipmentUnitType.Code + (uld.SerialNumber ?? "") + (uld.Carrier.CarrierCode ?? ""),
                    Description = String.Format("AWB {0} PCS {1} removed from ULD {2}", awbDetail.AWB.AWBSerialNumber, pieces, uld.ShipmentUnitType.Code + (uld.SerialNumber ?? "") + (uld.Carrier.CarrierCode ?? "")),
                    TransactionActionId = 1,
                    UserId = userId
                };

                Context.Transactions.Add(trans);

                if (uld.StatusID == 24 || uld.StatusID == 25 || uld.StatusID == 26)
                {
                    uld.RecoveredBy = null;
                    uld.RecoveredOn = null;
                    uld.TareWeight = 0;
                    uld.TotalWeight = 0;
                    uld.FreightWeight = 0;
                }
                uld.StatusID = 22;
                uld.StatusTimestamp = DateTime.UtcNow;

                Context.SaveChanges();

                return new TransactionStatus() { Status = true };
            }
            catch (Exception ex)
            {
                return new TransactionStatus() { Status = false, Error = "Internal Server Error." };
            }
        }

        public List<ForkLiftViewItem> GetForkliftView(long taskId, long userId)
        {
            var buildForklift = Context.MCH_GetForkliftView(taskId, userId);
            return buildForklift.Select(s => new ForkLiftViewItem
                                             {
                                                 AwbId = s.AWBId,
                                                 UldId = s.ULDId,
                                                 AWB = s.AWB,
                                                 Uld = s.ULD,
                                                 TotalPieces = s.TotalPieces,
                                                 ForkliftPieces = s.ForkliftPieces,
                                                 ReceivedPieces = s.ReceivedPieces,
                                                 AvailablePieces = s.AvailablePieces,
                                                 Locations = s.Locations,
                                                 DetailId = s.DetailId
                                             }).ToList();
        }

        public TransactionStatus AddForkliftPieces(long awbId, long taskId, long flightManifestId, int pieces, long userId)
        {
            var user = DataContext.UserProfiles.Single(u => u.Id == userId);
            var forkliftDetail = DataContext.ForkliftDetails.SingleOrDefault(d => d.AWBId == awbId && d.TaskId == taskId && d.UserId == userId);

            if (forkliftDetail != null)
                forkliftDetail.Pieces += pieces;
            else
            {
                DataContext.ForkliftDetails.Add(new ForkliftDetail
                {
                    AWBId = awbId,
                    TaskId = taskId,
                    Pieces = pieces,
                    Timestamp = DateTime.UtcNow,
                    UserId = userId
                });
            }
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }

        public TransactionStatus RemoveItemsFromForklift(long forkliftDetailsId, int count, long locationId)
        {
            var forkliftDetail = DataContext.ForkliftDetails.Single(f => f.Id == forkliftDetailsId);

            if (count > forkliftDetail.Pieces)
            {
                return new TransactionStatus() { Status = false, Error = "Number of pieces is more than on forklift." };
            }
            else if (count == forkliftDetail.Pieces)
            {
                DataContext.ForkliftDetails.Remove(forkliftDetail);
            }
            else
            {
                forkliftDetail.Pieces = count;
            }
            DataContext.SaveChanges();

            return new TransactionStatus { Status = true };
        }

        public TransactionStatus DropForkliftPieces(long taskId, long userId, long uldId, long locationId, long detailId)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            var forkliftDetail = DataContext.ForkliftDetails.Single(f => f.Id == detailId);
            var task = DataContext.Tasks.Single(t => t.Id == taskId && t.EntityTypeId == 1);
            var flight = DataContext.FlightManifests.Single(f => f.Id == task.EntityId);
            var location = DataContext.WarehouseLocations.Single(l => l.Id == locationId);
            var mad =
                DataContext.ManifestAWBDetails.SingleOrDefault(d => d.ULDId == uldId && d.AWBId == forkliftDetail.AWBId && d.FlightManifestId == task.EntityId);
            if (mad != null)
            {
                mad.LoadCount += forkliftDetail.Pieces;
            }
            else
            {
                DataContext.ManifestAWBDetails.Add(new ManifestAWBDetail()
                                                   {
                                                       FlightManifestId = task.EntityId,
                                                       AWBId = forkliftDetail.AWBId,
                                                       ULDId = uldId,
                                                       LoadCount = forkliftDetail.Pieces
                                                   });
            }

            DataContext.Transactions.Add(new Transaction
            {
                TaskId = taskId,
                StatusId = 22,
                Description = string.Format("PCS: {0} Dropped Into Flight {1} ULD {2} At Loc: {3}", forkliftDetail.Pieces, flight.Carrier.CarrierCode + flight.FlightNumber, forkliftDetail.ULD.SerialNumber, location.Location),
                TransactionActionId = 1,
                StatusTimestamp = DateTime.UtcNow,
                TaskTypeId = 5,
                EntityTypeId = 1,
                EntityId = flight.Id,
                Reference = forkliftDetail.AWB.Carrier.Carrier3Code + "-" + forkliftDetail.AWB.AWBSerialNumber,
                UserId = userId
            });

            DataContext.ForkliftDetails.Remove(forkliftDetail);

            DataContext.SaveChanges();

            return new TransactionStatus { Status = true };
        }

        public TransactionStatus DropAllForkliftPieces(long taskId, long userId, long uldId, long locationId)
        {
            var userName = DataContext.UserProfiles.Single(u => u.Id == userId).UserId;
            var forkliftDetails = DataContext.ForkliftDetails.Where(f => f.TaskId == taskId && f.UserId == userId);

            var task = DataContext.Tasks.Single(t => t.Id == taskId && t.EntityTypeId == 1);
            var flight = DataContext.FlightManifests.Single(f => f.Id == task.EntityId);
            var location = DataContext.WarehouseLocations.Single(l => l.Id == locationId);

            foreach (var det in forkliftDetails)
            {
                var mad = DataContext.ManifestAWBDetails.SingleOrDefault(d => d.ULDId == uldId && d.AWBId == det.AWBId && d.FlightManifestId == task.EntityId);
                if (mad != null)
                {
                    mad.LoadCount += det.Pieces;
                }
                else
                {
                    DataContext.ManifestAWBDetails.Add(new ManifestAWBDetail()
                    {
                        FlightManifestId = task.EntityId,
                        AWBId = det.AWBId,
                        ULDId = det.ULD.Id,
                        LoadCount = det.Pieces
                    });
                }

                DataContext.Transactions.Add(new Transaction
                {
                    TaskId = taskId,
                    StatusId = 22,
                    Description = string.Format("PCS: {0} Dropped Into Flight {1} ULD {2} At Loc: {3}", det.Pieces, flight.Carrier.CarrierCode + flight.FlightNumber, det.ULD.SerialNumber, location.Location),
                    TransactionActionId = 1,
                    StatusTimestamp = DateTime.UtcNow,
                    TaskTypeId = 5,
                    EntityTypeId = 1,
                    EntityId = flight.Id,
                    Reference = det.AWB.Carrier.Carrier3Code + "-" + det.AWB.AWBSerialNumber,
                    UserId = userId
                });
            }

            DataContext.ForkliftDetails.RemoveRange(forkliftDetails);
            DataContext.SaveChanges();
            return new TransactionStatus { Status = true };
        }
    }
}
