﻿using MCH.BLL.DAL;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.Screening;
using MCH.BLL.Model.SpecialHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class SpecialHandlingUnit : DataUnitOfWork
    {
        public List<DtoSpecialHandlingGroup> GetAccountSpecialHandlings(long carrierAccountId)
        {
            var tmpResult = new List<DtoSpecialHandlingGroup>();


            var tmpQuery = this.Context.AccountSpecialHandlings.Where(a => a.AccountId == carrierAccountId).Select(item => item.SpecialHandlingGroup);

            if (!tmpQuery.Any())
            {
                tmpQuery = this.Context.AccountSpecialHandlings.Where(a => a.AccountId == null).Select(item => item.SpecialHandlingGroup);
            }

            if (tmpQuery.Any())
            {
                foreach (var i in tmpQuery)
                {
                    var tmpItem = new DtoSpecialHandlingGroup
                    {
                        Code = i.Code,
                        DisplayName = i.DisplayName,
                        Id = i.Id,
                        Name = i.GroupName,
                        SphCodeId = i.SPHCodeId
                    };
                    if (i.SpecialHandlingSubGroups.Any())
                    {
                        tmpItem.SubGroups = new List<DtoSpecialHandlingGroup>();
                        foreach (var s in i.SpecialHandlingSubGroups)
                        {
                            var tmpSubItem = new DtoSpecialHandlingGroup
                            {
                                DisplayName = s.DisplayName,
                                Id = s.Id,
                                Name = s.Name,
                                SphCodeId = s.SPHCodeId,
                            };
                            tmpItem.SubGroups.Add(tmpSubItem);
                        }
                    }
                    tmpResult.Add(tmpItem);
                }
            }

            return tmpResult;
        }

        public List<SimpleListItem> GetDocumentTypesBySph(long[] ids)
        {
            List<SimpleListItem> tmpResult = new List<SimpleListItem>();

            if (ids != null && ids.Any())
            {
                tmpResult = this.Context.ShipperVerificationDocs.Where(item => ids.Contains(item.SpecialHandlingId.Value)).Select(item => new SimpleListItem
                {
                    Id = item.Id,
                    Name = item.DocumentName
                }).ToList();
            }

            return tmpResult;
        }

        public List<DtoVerifyField> GetShipperVerifyFields(long[] sphGroupIds)
        {            
            var tmpVerifyFieldSettings = this.Context.SpecialHandlingVerificationSettings.Where(item => sphGroupIds.Contains(item.SPHGroupId)).Select(item => item.ShipperVerificationField).Distinct();

            return  tmpVerifyFieldSettings.Select(item => new DtoVerifyField
            {
                Id = item.Id,
                Name = item.Name,
                Label = item.DisplayName,
                TypeName = item.FieldType.Name,
                IsRequired = item.SpecialHandlingVerificationSettings.Where(p => sphGroupIds.Contains(p.SPHGroupId)).Select(e => e.IsMandatory).Any()
            }).ToList();            
        }

        public DtoAwbAlternativeScreenings GetAccountAlternativeScreenings(long carrierAccountId, int aircraftTypeId)
        {
            var tmpScreeningCasesQuery = this.Context.AccountAlternateScreenings.Where(item => item.AccountId == carrierAccountId);

            if (!tmpScreeningCasesQuery.Any())
            {
                tmpScreeningCasesQuery = this.Context.AccountAlternateScreenings.Where(item => item.AccountId == null);
            }

            var tmpScreeningOptions = tmpScreeningCasesQuery.Select(e => e.AlternateScreeningCas).Where(p=> p.AircraftTypeId == aircraftTypeId);

            DtoAwbAlternativeScreenings tmpModel = new DtoAwbAlternativeScreenings();

            tmpModel.ScreeningOptions = tmpScreeningOptions.Select(item => new AlternativeSceeningOption
            {
                AircraftTypeId = item.AircraftTypeId,
                GroupId = item.SpecialHandlingGroupId,
                Id = item.Id,
                Name = item.Requirement
            }).ToList();

            return tmpModel;
        }


    }
}
