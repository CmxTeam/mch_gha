﻿using MCH.BLL.DAL.Data;
using MCH.BLL.Helpers;
using MCH.BLL.Model.Discharge;
using MCH.BLL.Model.Enum;
using MCH.Core.Model.Enum;
using MCH.Core.Models.CargoDischarge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class DischargeUnitOfWork : UnitOfWork
    {

        public CargoDischargeTaskItem GetCargoDischargeTask(long taskId)
        {
            return this.DataContext.MCH_CargoDischargeTaskItem(null, null, taskId).
                Select(item => new CargoDischargeTaskItem
                {
                    Company = item.Company,
                    Driver = item.Driver,
                    DriverPhoto = item.DriverPhoto,
                    TotalAwbs = item.TotalAwbs,
                    TotalPieces = item.TotalPieces,
                    ScannedAwbs = item.ScannedAwbs,
                    ScannedPieces = item.ScannedPieces,
                    TaskId = item.TaskId,
                    PercentProgress = item.PercentProgress,
                    Flags = item.Flags,
                    Locations = item.Location,
                    Status = (enumCargoDischargeStatusTypes)(item.Status.HasValue ? item.Status.Value : 0)

                }).SingleOrDefault();
        }

        public List<CargoDischargeTaskItem> GetCargoDischargeTasks(long warehouseId, enumCargoDischargeStatusTypes status)
        {
            return this.DataContext.MCH_CargoDischargeTaskItem(warehouseId, (int)status, null).
                Select(item => new CargoDischargeTaskItem
                {
                    Company = item.Company,
                    Driver = item.Driver,
                    DriverPhoto = item.DriverPhoto,
                    TotalAwbs = item.TotalAwbs,
                    TotalPieces = item.TotalPieces,
                    ScannedAwbs = item.ScannedAwbs,
                    ScannedPieces = item.ScannedPieces,
                    TaskId = item.TaskId,
                    PercentProgress = item.PercentProgress,
                    Flags = item.Flags,
                    Locations = item.Location,
                    Status = (enumCargoDischargeStatusTypes)(item.Status.HasValue ? item.Status.Value : 0)

                }).ToList();
        }

        public DischargeWizard GetDischargeState(long? taskId, long userId, long ? warehouseId)
        {
            DischargeWizard tmpModel = new DischargeWizard();
            tmpModel.Header = new DischargeState();
            tmpModel.LatestShipment = new ProcessedShipment();
            tmpModel.ProcessedShipments = new List<ProcessedShipment>();

            MCH.BLL.DAL.Data.Task tmpDischargeTask = null;
            Discharge tmpDischarge = null;

            #region Get task by id or user latest for Discharge
            if (taskId.HasValue && taskId.Value > 0)
            {
                tmpDischargeTask = this.DataContext.Tasks.SingleOrDefault(t => t.Id == taskId.Value);
            }
            else
            {
                tmpDischargeTask = this.DataContext.TaskAssignments.Where(ta => ta.UserId == userId).Select(ta => ta.Task).Where(t => t.EntityTypeId == (int)enumEntityTypes.Discharge).OrderBy(t => t.Id).FirstOrDefault();
            }
            #endregion

            if (tmpDischargeTask != null)
            {
                tmpDischarge = this.DataContext.Discharges.SingleOrDefault(d => d.Id == tmpDischargeTask.EntityId.Value);
            }
            else
            {
                tmpDischarge = new Discharge
                {
                    CheckInTimestamp = DateTime.UtcNow,
                    StatusId = (int)enumDischargeStatuses.Draft,
                    StatusTimestamp = DateTime.UtcNow
                };

                this.DataContext.Discharges.Add(tmpDischarge);

                this.DataContext.SaveChanges();

                tmpDischargeTask = new MCH.BLL.DAL.Data.Task
                {
                    EntityTypeId = (int)enumEntityTypes.Discharge,
                    ProgressPercent = 0,
                    TaskCreationDate = DateTime.UtcNow,
                    TaskDate = DateTime.UtcNow,
                    EntityId = tmpDischarge.Id,
                    StatusId = 55, //Discharge-Draft
                    TaskReference = "Cargo Discharge",
                    TaskTypeId = (int)enumTaskTypes.CargoDischarge,
                    WarehouseId = (int)warehouseId
                };

                this.DataContext.Tasks.Add(tmpDischargeTask);

                tmpDischargeTask.TaskAssignments.Add(new TaskAssignment { IsOwner = true, AssignedBy = userId, AssignedOn = DateTime.UtcNow, RecDate = DateTime.UtcNow, UserId = userId });

                tmpDischargeTask.TaskTransactions.Add(new TaskTransaction
                {
                    Description = "Cargo Discharge Task Created.",
                    RecDate = DateTime.UtcNow,
                    StatusId = 55,
                    StatusTimestamp = DateTime.UtcNow,
                    TaskId = tmpDischargeTask.Id,
                    UserId = userId
                });

                this.DataContext.SaveChanges();
            }

            FillDischargeHeader(tmpModel, tmpDischarge);

            if (tmpDischarge.DischargeDetails.Any())
            {
                tmpModel.ProcessedShipments = tmpDischarge.DischargeDetails.Select(d => new ProcessedShipment
                {
                    Id = d.Id,
                    IsHWB = d.HWBId.HasValue,
                    Reference = d.Reference
                }).ToList();

                tmpModel.LatestShipment = tmpDischarge.DischargeDetails.OrderByDescending(d => d.Id).Select(d => new ProcessedShipment
                {
                    Id = d.Id,
                    IsHWB = d.HWBId.HasValue,
                    Reference = d.Reference
                }).FirstOrDefault();
            }


            return tmpModel;
        }

        public SaveShipmentResult SaveShipment(SaveShipment model, long userId)
        {
            SaveShipmentResult tmpResult = new SaveShipmentResult();

            Discharge tmpDischarge = this.DataContext.Discharges.SingleOrDefault(d => d.Id == model.DischargId);

            MCH.BLL.DAL.Data.Task tmpDischargeTask = tmpDischargeTask = this.DataContext.Tasks.Where(t => t.EntityTypeId == (int)enumTaskTypes.CargoDischarge && t.EntityId == model.DischargId && t.ParentTaskId == null).FirstOrDefault();

            DischargeDetail tmpDetails = new DischargeDetail();
            tmpDetails.StatusId = (int)enumDischargeShipmentStatuses.ShipmentVerified;

            if (model.IsHWB)
            {
                tmpDetails.HWBId = model.ShipmentId;
                tmpDetails.Reference = "HWB-" + this.DataContext.MCH_GetEntityReference((int)enumEntityTypes.HWB, model.ShipmentId).FirstOrDefault();
            }
            else
            {
                tmpDetails.AWBId = model.ShipmentId;
                tmpDetails.Reference = "AWB-" + this.DataContext.MCH_GetEntityReference((int)enumEntityTypes.AWB, model.ShipmentId).FirstOrDefault();
            }

            if (model.Parts.Any())
            {
                tmpDetails.AvailablePieces = model.Parts.Sum(p => p.QuantityAffected);

                foreach (var shipmentPart in model.Parts)
                {
                    DischargeShipment tmpDisShipment = new DischargeShipment();
                    tmpDisShipment.Part = shipmentPart.Part;
                    tmpDisShipment.Pieces = shipmentPart.QuantityAffected;

                    tmpDetails.DischargeShipments.Add(tmpDisShipment);
                }
            }

            tmpDischarge.DischargeDetails.Add(tmpDetails);

            tmpDischargeTask.StatusId = 50;// shipment verified

            this.DataContext.TaskTransactions.Add(new TaskTransaction
            {
                Description = "Cargo Discharge Shipment Verified",
                RecDate = DateTime.UtcNow,
                StatusId = 50,
                StatusTimestamp = DateTime.UtcNow,
                TaskId = tmpDischargeTask.Id,
                UserId = userId
            });

            this.DataContext.SaveChanges();

            tmpResult.DischargeId = tmpDischarge.Id;
            tmpResult.DischargeDetailId = tmpDetails.Id;
            tmpResult.Reference = tmpDetails.Reference;

            // Driver was not saved yet, Discharge is new created
            if (!tmpDischarge.DriverLogId.HasValue)
            {
                tmpResult.StepName = "2 Driver Details";
            }

            return tmpResult;
        }

        public DisShipmentDetailsModel GetAWBById(long shipmentId)
        {
            DisShipmentDetailsModel tmpModel = null;

            var tmpAwb = this.DataContext.AWBs.SingleOrDefault(a => a.Id == shipmentId);

            if (tmpAwb != null)
            {
                tmpModel = this.GetAWBData(tmpAwb);

                //TODO add pickup details and documents
            }

            return tmpModel;
        }

        public DisShipmentDetailsModel GetHWBById(long shipmentId)
        {
            DisShipmentDetailsModel tmpModel = null;

            var tmpHwb = this.DataContext.HWBs.SingleOrDefault(h => h.Id == shipmentId);
            if (tmpHwb != null)
            {
                tmpModel = this.GetHWBData(tmpHwb);

                //TODO add pickup details and documents
            }

            return tmpModel;
        }

        public DisShipmentDetailsModel GetShipmentByAWB(string carrier, string awbNumber)
        {
            DisShipmentDetailsModel tmpModel = null;

            var tmpAWB = this.DataContext.AWBs.Where(a => a.AWBSerialNumber == awbNumber && a.Carrier.Carrier3Code == carrier).FirstOrDefault();

            if (tmpAWB != null)
            {
                tmpModel = this.GetAWBData(tmpAWB);
            }

            return tmpModel;
        }

        public DisShipmentDetailsModel GetShipmentByHWB(string hwbNumber)
        {
            DisShipmentDetailsModel tmpModel = null;

            var tmpHwb = this.DataContext.HWBs.SingleOrDefault(h => h.HWBSerialNumber == hwbNumber);
            if (tmpHwb != null)
            {
                tmpModel = this.GetHWBData(tmpHwb);
            }

            return tmpModel;
        }

        public List<ProcessedShipment> GetProcessedShipments(long? dischargeId)
        {
            List<ProcessedShipment> tmpList = new List<ProcessedShipment>();

            var tmpDischarge = this.DataContext.Discharges.SingleOrDefault(d => d.Id == dischargeId);
            if (tmpDischarge != null)
            {
                tmpList = tmpDischarge.DischargeDetails.Select(dd => new ProcessedShipment
                {
                    Id = dd.Id,
                    IsHWB = dd.HWBId.HasValue,
                    Reference = dd.Reference
                }).ToList();
            }

            return tmpList;
        }

        public DischargeState SaveDriverInfo(DischargeDriverModel driverInfo, string attachments, long userId)
        {
            var tmpDischarge = this.DataContext.Discharges.SingleOrDefault(d => d.Id == driverInfo.DischargeId);
            if (tmpDischarge != null)
            {
                var tmpDriverSecurityLog = new DriverSecurityLog
                {
                    CarrierDriverId = driverInfo.DriverId.Value,
                    DriverPhoto = driverInfo.CaptureID,
                    IDPhoto = driverInfo.CaptureID,
                    PrimaryIDTypeId = driverInfo.FirstIDTypeId,
                    PrimaryPhotoIdMatch = driverInfo.FirstIDMatched,
                    RecDate = DateTime.UtcNow,
                    SecondaryIdTypeId = driverInfo.SecondIDTypeId,
                    SecondaryPhotoIdMatch = driverInfo.SecondIDMatched,
                    TruckingCompanyId = (int)driverInfo.TruckingCompId,
                    UserId = userId,
                };

                this.DataContext.DriverSecurityLogs.Add(tmpDriverSecurityLog);
                this.DataContext.SaveChanges();

                tmpDischarge.DriverLogId = tmpDriverSecurityLog.Id;


            }

            return this.GetCurrentState(driverInfo.DischargeId);
        }
       

        public void CancelDischarge(long dischargeId)
        {
            var tmpDishcarge = this.DataContext.Discharges.SingleOrDefault(d => d.Id == dischargeId);

        }

        public void Finalize(long dischargeId)
        {
            var tmpDischarge = this.DataContext.Discharges.SingleOrDefault(d => d.Id == dischargeId);

        }

        #region Helper Methods

        private DischargeState GetCurrentState(long dischargId)
        {
            DischargeState tmpState = new DischargeState();

            return tmpState;
        }

        private DisShipmentDetailsModel GetHWBData(DAL.Data.HWB tmpHwb)
        {
            DisShipmentDetailsModel tmpModel = new DisShipmentDetailsModel();
            // tmpModel.PickupDetails = new DischargePickupDetails();

            var tmpAWB = tmpHwb.AWBs_HWBs.First().AWB;

            tmpModel.ShipmentDetails = new ShipmentDetailsSection();
            tmpModel.ShipmentDetails.AvaileblePieces = tmpHwb.AvailablePieces;

            tmpModel.ShipmentDetails.AwbId = tmpAWB.Id;
            tmpModel.ShipmentDetails.AwbNumber = tmpAWB.Carrier.Carrier3Code + "-" + tmpAWB.AWBSerialNumber;

            tmpModel.ShipmentDetails.HwbId = tmpHwb.Id;
            tmpModel.ShipmentDetails.HwbNumber = tmpHwb.HWBSerialNumber;

            tmpModel.ShipmentDetails.Consignee = ModelStructHelpers.CustomerShortString(tmpHwb.Customer);
            tmpModel.ShipmentDetails.Destination = tmpHwb.Port1.IATACode;
            tmpModel.ShipmentDetails.Pieces = tmpHwb.Pieces;
            tmpModel.ShipmentDetails.ULDs = tmpHwb.ManifestDetails.Select(e => e.ULD).Count();
            tmpModel.ShipmentDetails.Weight = tmpHwb.Weight;
            tmpModel.ShipmentDetails.WeightUOM = tmpHwb.UOM.TwoCharCode;

            tmpModel.CustomsClearance = this.GetHWBCustomsClearance(tmpHwb.HWBSerialNumber);

            tmpModel.ShipmentDetails.IsSplit = tmpModel.CustomsClearance.Where(e => e.Part != "*").Any();

            tmpModel.CustomsFullHistory = this.GetHWBCustomsClearanceFull(tmpHwb.HWBSerialNumber);

            if (tmpModel.CustomsClearance.Any())
            {
                if (tmpModel.CustomsClearance.All(e => e.Code == "CC"))
                {
                    tmpModel.ShipmentDetails.ReleaseStatus = ShipmentCustomsStatuses.ReleaseAuthorized;
                }
                else if (tmpModel.CustomsClearance.Any(e => e.Code == "CC"))
                {
                    tmpModel.ShipmentDetails.ReleaseStatus = ShipmentCustomsStatuses.PartialAuthorized;
                }
                else
                {
                    tmpModel.ShipmentDetails.ReleaseStatus = ShipmentCustomsStatuses.OnHold;
                }
            }

            return tmpModel;
        }

        private DisShipmentDetailsModel GetAWBData(DAL.Data.AWB tmpAWB)
        {
            DisShipmentDetailsModel tmpModel = new DisShipmentDetailsModel();
            // tmpModel.PickupDetails = new DischargePickupDetails();

            tmpModel.ShipmentDetails = new ShipmentDetailsSection();
            tmpModel.ShipmentDetails.AvaileblePieces = tmpAWB.AvailablePieces;

            tmpModel.ShipmentDetails.AwbId = tmpAWB.Id;
            tmpModel.ShipmentDetails.AwbNumber = tmpAWB.Carrier.Carrier3Code + "-" + tmpAWB.AWBSerialNumber;

            tmpModel.ShipmentDetails.Consignee = ModelStructHelpers.CustomerShortString(tmpAWB.Customer);
            tmpModel.ShipmentDetails.Destination = tmpAWB.Port1.IATACode;
            tmpModel.ShipmentDetails.Pieces = tmpAWB.TotalPieces;
            tmpModel.ShipmentDetails.ULDs = tmpAWB.ULDCount;
            tmpModel.ShipmentDetails.Weight = tmpAWB.TotalWeight;
            tmpModel.ShipmentDetails.WeightUOM = tmpAWB.UOM.TwoCharCode;



            tmpModel.CustomsClearance = this.GetAWBCustomsClearance(tmpAWB.Carrier.Carrier3Code + "" + tmpAWB.AWBSerialNumber);

            tmpModel.ShipmentDetails.IsSplit = tmpModel.CustomsClearance.Where(e => e.Part != "*").Any();

            tmpModel.CustomsFullHistory = this.GetAWBCustomsClearanceFull(tmpAWB.Carrier.Carrier3Code + "" + tmpAWB.AWBSerialNumber);

            if (tmpModel.CustomsClearance.Any())
            {
                if (tmpModel.CustomsClearance.All(e => e.Code == "TR"))
                {
                    tmpModel.ShipmentDetails.ReleaseStatus = ShipmentCustomsStatuses.ReleaseAuthorized;
                }
                else if (tmpModel.CustomsClearance.Any(e => e.Code == "TR"))
                {
                    tmpModel.ShipmentDetails.ReleaseStatus = ShipmentCustomsStatuses.PartialAuthorized;
                }
                else
                {
                    tmpModel.ShipmentDetails.ReleaseStatus = ShipmentCustomsStatuses.OnHold;
                }
            }

            return tmpModel;
        }

        private List<ShipmentCustomsRow> GetAWBCustomsClearance(string argMasterNumber)
        {
            List<ShipmentCustomsRow> tmpCustoms = new List<ShipmentCustomsRow>();

            var tmpCustomsHistory = this.CoreContext.GetAwbCustomsHistory(argMasterNumber, true);


            tmpCustoms = tmpCustomsHistory.Select(e => new ShipmentCustomsRow
            {
                CustomsDate = e.CustomsDateTime,
                CustomStaits = e.CustomsCode,
                Description = e.CodeDescription,
                Code = e.Code,
                EntryNo = e.EntryNo,
                Id = (int)e.RowNumber,
                Part = e.PartialIdCode,
                QuantityAffected = (int)e.QuantityAffected,
                TransactionDate = e.TransactionDateTime,
                ReadyForRelease = true,
            }).ToList();


            return tmpCustoms;
        }

        private List<ShipmentCustomsRow> GetHWBCustomsClearance(string argHouse)
        {
            List<ShipmentCustomsRow> tmpCustoms = new List<ShipmentCustomsRow>();

            var tmpCustomsHistory = this.CoreContext.GetHwbCustomsHistory(argHouse, true);


            tmpCustoms = tmpCustomsHistory.Select(e => new ShipmentCustomsRow
            {
                CustomsDate = e.CustomsDateTime,
                CustomStaits = e.CustomsCode,
                Code = e.Code,
                Description = e.CodeDescription,
                EntryNo = e.EntryNo,
                Id = (int)e.RowNumber,
                Part = e.PartialIdCode,
                QuantityAffected = (int)e.QuantityAffected,
                TransactionDate = e.TransactionDateTime
            }).ToList();


            return tmpCustoms;
        }

        private List<ShipmentCustomsRow> GetHWBCustomsClearanceFull(string argHouse)
        {
            List<ShipmentCustomsRow> tmpCustoms = new List<ShipmentCustomsRow>();

            var tmpCustomsHistory = this.CoreContext.GetHwbCustomsHistory(argHouse, false);


            tmpCustoms = tmpCustomsHistory.Select(e => new ShipmentCustomsRow
            {
                CustomsDate = e.CustomsDateTime,
                CustomStaits = e.CustomsCode,
                Code = e.Code,
                Description = e.CodeDescription,
                EntryNo = e.EntryNo,
                Id = (int)e.RowNumber,
                Part = e.PartialIdCode,
                QuantityAffected = e.QuantityAffected.HasValue ? (int)e.QuantityAffected.Value : 0,
                TransactionDate = e.TransactionDateTime,
                ReadyForRelease = false
            }).ToList();


            return tmpCustoms;
        }

        private List<ShipmentCustomsRow> GetAWBCustomsClearanceFull(string argMasterNumber)
        {
            List<ShipmentCustomsRow> tmpCustoms = new List<ShipmentCustomsRow>();

            var tmpCustomsHistory = this.CoreContext.GetAwbCustomsHistory(argMasterNumber, false);


            tmpCustoms = tmpCustomsHistory.Select(e => new ShipmentCustomsRow
            {
                CustomsDate = e.CustomsDateTime,
                CustomStaits = e.CustomsCode,
                Code = e.Code,
                Description = e.CodeDescription,
                EntryNo = e.EntryNo,
                Id = e.RowNumber.HasValue ? (int)e.RowNumber : 0,
                Part = e.PartialIdCode,
                QuantityAffected = e.QuantityAffected.HasValue ? (int)e.QuantityAffected:0,
                TransactionDate = e.TransactionDateTime
            }).ToList();


            return tmpCustoms;
        }

        private static void FillDischargeHeader(DischargeWizard tmpModel, Discharge tmpDischarge)
        {
            tmpModel.Header.DischargeId = tmpDischarge.Id;
            tmpModel.Header.Carrier = tmpDischarge.Carrier != null ? tmpDischarge.Carrier.CarrierName:String.Empty;
            tmpModel.Header.CarrierId = tmpDischarge.CarrierId;
            tmpModel.Header.Consignee = tmpDischarge.Customer != null ? tmpDischarge.Customer.Name:String.Empty;
            tmpModel.Header.ConsigneeId = tmpDischarge.Customer != null ? tmpDischarge.Customer.Id: new Nullable<int>();
            tmpModel.Header.DriverId = tmpDischarge.DriverSecurityLog != null ? tmpDischarge.DriverSecurityLog.CarrierDriverId: new Nullable<long>();
            tmpModel.Header.DriverName = tmpDischarge.DriverSecurityLog != null ? tmpDischarge.DriverSecurityLog.CarrierDriver.FirstName + " " + tmpDischarge.DriverSecurityLog.CarrierDriver.LastName: String.Empty;
            tmpModel.Header.DriverPhotoData = tmpDischarge.DriverSecurityLog != null ? tmpDischarge.DriverSecurityLog.IDPhoto: String.Empty;

            tmpModel.Header.IsConsigneeOrCarrier = tmpDischarge.ConsigneeId.HasValue;
            tmpModel.Header.TransferManifest = tmpDischarge.TransferManifest;
            tmpModel.Header.TruckingCompanyId = tmpDischarge.DriverSecurityLog != null ? tmpDischarge.DriverSecurityLog.TruckingCompanyId : new Nullable<int>();
            tmpModel.Header.TruckingCompanyName = tmpDischarge.DriverSecurityLog != null ? tmpDischarge.DriverSecurityLog.Carrier.CarrierName: String.Empty;
            tmpModel.Header.Status = (enumDischargeStatuses)tmpDischarge.StatusId;
        }

        #endregion
    }
}
