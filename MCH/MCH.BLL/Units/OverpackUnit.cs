﻿using MCH.BLL.DAL.Data;
using MCH.BLL.Model.Enum;
using MCH.Core.Models.Common;
using MCH.Core.Models.Overpack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCH.BLL.Units
{
    public class OverpackUnit : DataUnitOfWork
    {
        public OverPackTaskItem GetOverpackTask(long? taskId, long userId, string barcode, long warehouseId)
        {
            OverPackTaskItem tmpItem = null;
            long appUserId = GetAppUserId(userId);
            var tmpResult = this.DataContext.MCH_GetOverpackTask(taskId, appUserId, barcode, warehouseId).FirstOrDefault();

            if (tmpResult != null)
            {
                tmpItem = new OverPackTaskItem
                {
                    Awb = tmpResult.AWB,
                    AwbId = tmpResult.AwbId,
                    Destination = tmpResult.Destination,
                    Hwb = tmpResult.HWB,
                    HwbId = tmpResult.HWBId,
                    Origin = tmpResult.Origin,
                    OverPackTaskId = tmpResult.OverpackTaskId.HasValue ? tmpResult.OverpackTaskId.Value : 0,
                    Skids = tmpResult.Skids,
                    TotalPieces = tmpResult.TotalPieces.HasValue ? tmpResult.TotalPieces.Value : 0,
                    TotalSkidPieces = tmpResult.TotalSkidPieces
                };
            }

            return tmpItem;
        }

        public List<OverPackItem> GetOverpackItems(long taskId)
        {
            List<OverPackItem> tmpResult = null;
            var tmpTask = this.DataContext.Tasks.SingleOrDefault(t => t.Id == taskId);

            if (tmpTask != null)
            {
                enumEntityTypes entityTypeId = (enumEntityTypes)tmpTask.EntityTypeId;



                if (entityTypeId == enumEntityTypes.AWB)
                {
                    var awbOverpacks = this.DataContext.Overpacks.Where(o => o.WarehouseId == tmpTask.WarehouseId && o.AwbId == tmpTask.EntityId);

                    List<OverPackItem> tmpOverAwbs = awbOverpacks.Select(o => new OverPackItem
                    {
                        OverPackItemId = o.Id,

                        OverPackItemNumber = o.PieceNumber,
                        OverpackType = o.ULDType.Code,
                        OverpackTypeId = o.ULDType.Id,
                        TotalPieces = o.AWB.TotalPieces,
                        Pieces = o.PiecesContained,
                        SkidOwnerType = o.IsCustomerBuilt ? SkidOwnerTypes.CUSTOMER : SkidOwnerTypes.QAS,
                    }).ToList();

                    tmpResult = tmpOverAwbs;
                }
                else if (entityTypeId == enumEntityTypes.HWB)
                {
                    var hwbOverpacks = this.DataContext.Overpacks.Where(o => o.WarehouseId == tmpTask.WarehouseId && o.HwbId == tmpTask.EntityId);

                    List<OverPackItem> tmpOverHwbs = hwbOverpacks.Select(o => new OverPackItem
                    {
                        OverPackItemId = o.Id,
                        OverPackItemNumber = o.PieceNumber,
                        OverpackType = o.ULDType.Code,
                        OverpackTypeId = o.ULDType.Id,
                        TotalPieces = o.HWB.Pieces,
                        Pieces = o.PiecesContained,
                        SkidOwnerType = o.IsCustomerBuilt ? SkidOwnerTypes.CUSTOMER : SkidOwnerTypes.QAS,
                    }).ToList();

                    tmpResult = tmpOverHwbs;
                }
            }

            return tmpResult;
        }

        public List<OverpackTypeItem> GetOverpackTypes(long? warehouseId)
        {
            return this.DataContext.ULDTypes.Select(ut => new OverpackTypeItem { OverpackType = ut.Code, OverpackTypeId = ut.Id, IsDefault = ut.IsDefault }).ToList();
        }

        public void DeleteOverpack(long overpackId, long taskId, long userId)
        {
            long appUserId = GetAppUserId(userId);
            var tmpTask = this.DataContext.Tasks.SingleOrDefault(t => t.Id == taskId);
            if (tmpTask == null)
            {
                throw new Exception("Task Not Found.");
            }

            enumEntityTypes entityType = (enumEntityTypes)tmpTask.EntityTypeId;

            var tmpOverpacks = this.DataContext.Overpacks.Where(o => o.WarehouseId == tmpTask.WarehouseId.Value);

            int overpackContained = 0;
            int? pieces = null;

            if (entityType == enumEntityTypes.HWB)
            {
                tmpOverpacks = tmpOverpacks.Where(o => o.HwbId == tmpTask.EntityId.Value);
                var otherOverpacks = tmpOverpacks.Where(o => o.Id != overpackId).FirstOrDefault();
                if (otherOverpacks != null)
                {
                    pieces = otherOverpacks.HWB.Pieces;
                }

                var overpackContainedQuery = this.DataContext.Overpacks.Where(o => o.WarehouseId == tmpTask.WarehouseId && o.Id != overpackId && o.HwbId == tmpTask.EntityId.Value);

                overpackContained = overpackContainedQuery.Select(o => (int ?)o.PiecesContained).Sum() ?? 0;

            }
            else if (entityType == enumEntityTypes.AWB)
            {
                tmpOverpacks = tmpOverpacks.Where(o => o.AwbId == tmpTask.EntityId.Value);
                var otherOverpacks = tmpOverpacks.Where(o => o.Id != overpackId).FirstOrDefault();
                if (otherOverpacks != null)
                {
                    pieces = otherOverpacks.AWB.TotalPieces;
                }
                var overpackContainedQuery = this.DataContext.Overpacks.Where(o => o.WarehouseId == tmpTask.WarehouseId && o.Id != overpackId && o.AwbId.HasValue && o.AwbId == tmpTask.EntityId.Value);

                overpackContained = overpackContainedQuery.Select(o=> (int?)o.PiecesContained).Sum() ?? 0;

            }

          

            tmpOverpacks = tmpOverpacks.OrderBy(o => o.Timestamp);

            var tmpOverPackToDelete = tmpOverpacks.SingleOrDefault(o => o.Id == overpackId);

            if (tmpOverPackToDelete == null)
            {
                throw new Exception("No shipment unit found.");
            }

            this.DataContext.Overpacks.Remove(tmpOverPackToDelete);

            this.DataContext.SaveChanges();

            #region Recalculate Pieces
            Dictionary<long, int> overIdPieces = new Dictionary<long, int>();

            var orderOverPacks = tmpOverpacks.ToList();

            for (int i = 0; i < orderOverPacks.Count; i++)
            {
                overIdPieces.Add(orderOverPacks[i].Id, i + 1);
            }

            if (overIdPieces.Any())
            {
                foreach (var item in overIdPieces)
                {
                    var tmpOverPack = this.DataContext.Overpacks.SingleOrDefault(o => o.Id == item.Key);
                    tmpOverPack.PieceNumber = item.Value;
                }
            }
            #endregion

            if (pieces.HasValue)
            {
                var tmpPercent = (overpackContained / pieces) * 100;
                tmpTask.ProgressPercent = tmpPercent;
            }
            else
            {
                tmpTask.ProgressPercent = 0;
            }

            tmpTask.StatusId = 13;

            this.DataContext.SaveChanges();
        }


        public long AddOverpack(long overPackTaskId, long userId, long OverpackTypeId, SkidOwnerTypes skidOwnerType, int pieces)
        {
            long tmpOverpackId = -1;

            var tmpTask = this.DataContext.Tasks.SingleOrDefault(t => t.Id == overPackTaskId);

            var tmpUser = this.DataContext.UserProfiles.SingleOrDefault(u => u.Id == userId);

            int? taskPercent = 0;

            enumEntityTypes entityType = (enumEntityTypes)tmpTask.EntityTypeId;
            var tmpOverpackTotalPieces = this.DataContext.Overpacks.Where(o => o.HwbId == tmpTask.EntityId).Sum(o => o.PiecesContained);
            if (entityType == enumEntityTypes.AWB)
            {
                var tmpEntityPieces = this.DataContext.AWBs.SingleOrDefault(a => a.Id == tmpTask.EntityId).TotalPieces;

                if ((tmpOverpackTotalPieces + pieces) > tmpEntityPieces)
                {
                    throw new Exception("Pieces are over the total pieces for the Shipment.");
                }

                var tmpLastPieces = this.DataContext.Overpacks.Where(o => o.AwbId == tmpTask.EntityId).Max(o => o.PieceNumber);

                this.DataContext.Overpacks.Add(new Overpack { AwbId = tmpTask.EntityId, PiecesContained = pieces, Timestamp = DateTime.UtcNow, UserId = userId, PieceNumber = tmpLastPieces + 1, WarehouseId = tmpTask.WarehouseId.Value, IsCustomerBuilt = (skidOwnerType == SkidOwnerTypes.QAS ? true : false) });

                this.DataContext.Transactions.Add(new Transaction
                {
                    EntityTypeId = tmpTask.EntityTypeId,
                    EntityId = tmpTask.EntityId,
                    StatusId = 57,
                    StatusTimestamp = DateTime.UtcNow,                  
                    Reference = this.DataContext.MCH_GetEntityReference(tmpTask.EntityTypeId, tmpTask.EntityId).FirstOrDefault(),
                    Description = "Overpack Added - # " + tmpLastPieces + "PCS:" + pieces,
                    TaskTypeId = 17,
                    TransactionActionId = 1,
                    TaskId = tmpTask.Id,
                    UserId = userId
                });

                taskPercent = ((tmpOverpackTotalPieces + pieces) / tmpEntityPieces) * 100;

            }
            else if (entityType == enumEntityTypes.HWB)
            {
                var tmpEntityPieces = this.DataContext.HWBs.SingleOrDefault(a => a.Id == tmpTask.EntityId).Pieces;

                if ((tmpOverpackTotalPieces + pieces) > tmpEntityPieces)
                {
                    throw new Exception("Pieces are over the total pieces for the Shipment.");
                }

                var tmpLastPieces = this.DataContext.Overpacks.Where(o => o.HwbId == tmpTask.EntityId).Max(o => o.PieceNumber);

                this.DataContext.Overpacks.Add(new Overpack { HwbId = tmpTask.EntityId, PiecesContained = pieces, Timestamp = DateTime.UtcNow, UserId = userId, PieceNumber = tmpLastPieces + 1, WarehouseId = tmpTask.WarehouseId.Value, IsCustomerBuilt = (skidOwnerType == SkidOwnerTypes.QAS ? true : false) });

                this.DataContext.Transactions.Add(new Transaction
                {
                    EntityTypeId = tmpTask.EntityTypeId,
                    EntityId = tmpTask.EntityId,
                    StatusId = 57,
                    StatusTimestamp = DateTime.UtcNow,                 
                    Reference = this.DataContext.MCH_GetEntityReference(tmpTask.EntityTypeId, tmpTask.EntityId).FirstOrDefault(),
                    Description = "Overpack Added - # " + tmpLastPieces + "PCS:" + pieces,
                    TaskTypeId = 17,
                    TransactionActionId = 1,
                    TaskId = tmpTask.Id,
                    UserId = userId

                });

                taskPercent = ((tmpOverpackTotalPieces + pieces) / tmpEntityPieces) * 100;
            }

            tmpTask.StatusId = 13;
            tmpTask.ProgressPercent = taskPercent;

            this.DataContext.SaveChanges();

            return tmpOverpackId;
        }

        public SpCommonResult InsertOverpack(long? taskId, long userId, int? OverpackTypeId, SkidOwnerTypes skidOwnerType, int? pieces)
        {
            long appUserId = GetAppUserId(userId);
            return this.DataContext.MCH_AddOverpack(OverpackTypeId, pieces, appUserId, skidOwnerType == SkidOwnerTypes.CUSTOMER ? true : false, taskId).Select(e => new SpCommonResult { Success = e.Success == 0 ? false : true, Message = e.ErrMsg }).FirstOrDefault();
        }


        public SpCommonResult UpdateOverPack(long? overpackId, int? overpackTypeId, int piecesContained, long userId, SkidOwnerTypes isCustomerBuild, long taskId)
        {
            long appUseId = GetAppUserId(userId);
            return this.DataContext.MCH_EditOverpack(overpackId, overpackTypeId, piecesContained, appUseId, isCustomerBuild == SkidOwnerTypes.CUSTOMER ? true : false, taskId).Select(e => new SpCommonResult { Success = e.Success == 0 ? false : true, Message = e.ErrMsg }).FirstOrDefault();
        }
    }
}
