﻿using MCH.BLL.DAL;
using MCH.BLL.DAL.Data;
using MCH.BLL.Helper;
using MCH.BLL.Model.AwbWizard;
using MCH.BLL.Model.Common;
using MCH.BLL.Model.Customs;
using MCH.BLL.Model.Enum;
using MCH.BLL.Model.SpecialHandling;
using MCH.Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MCH.BLL.Units
{
    public class AwbWizardUnit : DataUnitOfWork
    {
        public AwbWizardStep GetAwbWizardStep(long awbId)
        {
            var awb = Context.AWBs.SingleOrDefault(a => a.Id == awbId);
            if (awb == null) return AwbWizardStep.DetailsCompleted;

            return (AwbWizardStep)awb.StatusId;
        }

        /// <summary>
        /// Step 1 get
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AwbStepModel GetAwbStep(long? id)
        {
            if (!id.HasValue) return new AwbStepModel
            {
                AircraftType = 1,
                AwbTypeId = 0,
                AccountInformationText = String.Empty,
                Shipper = new CustomerModel(),
                Consignee = new CustomerModel(),
                Flights = new List<FlightModel> { new FlightModel(), new FlightModel(), new FlightModel() },
                IssuingCarriersAgent = new AgentModel(),
                SenderReferences = new List<SenderReferenceModel> { }
            };

            var awb = Context.AWBs.Single(a => a.Id == id);
            return FillAwbModel(awb);
        }

        /// <summary>
        /// Step 1 save
        /// </summary>        
        public long SaveAwbStep(AwbStepModel awbStepModel, long userId, long? warehouseId)
        {
            var flights = awbStepModel.Flights.ToXml<FlightModel>();
            string agentName = null;
            int? agentId = null;
            string agentAccountNumber = null;
            string agentCity = null;
            string agentIataCode = null;
            string agentCassAddress = null;
            int? agentParticipantId = null;
            int? consigneeId = null;
            int? shipperId = null;
            string fileReference = null;
            int? participantIdentifierId = null;
            string participantCode = null;
            long? airportId = null;

            if (awbStepModel.IssuingCarriersAgent != null)
            {
                agentName = awbStepModel.IssuingCarriersAgent.Name;
                agentId = awbStepModel.IssuingCarriersAgent.Id; 
                agentAccountNumber = awbStepModel.IssuingCarriersAgent.AccountNumber;
                agentCity = awbStepModel.IssuingCarriersAgent.City;
                agentIataCode = awbStepModel.IssuingCarriersAgent.IataCode;
                agentCassAddress = awbStepModel.IssuingCarriersAgent.CassAddress;
                agentParticipantId = awbStepModel.IssuingCarriersAgent.ParticipantIdentfierId;                     
            }

            if(awbStepModel.Consignee != null)
            {
                consigneeId = awbStepModel.Consignee.Id;
            }

            if(awbStepModel.Shipper != null)
            {
                shipperId = awbStepModel.Shipper.Id;
            }

            if (awbStepModel.SenderReferences != null && awbStepModel.SenderReferences.Count() > 0)
            {
                fileReference = awbStepModel.SenderReferences.First().FileReference;
                participantIdentifierId = awbStepModel.SenderReferences.First().ParticipantIdentifierId;
                participantCode = awbStepModel.SenderReferences.First().ParticipantCode;
                airportId = awbStepModel.SenderReferences.First().AirportId;
            }

            var id = Context.MCH_SaveAwbStepWizard(awbStepModel.Id, awbStepModel.OriginId, awbStepModel.DestinationId, awbStepModel.CarrierId, 
                awbStepModel.AwbSerialNumber, awbStepModel.AircraftType, awbStepModel.AwbTypeId, awbStepModel.AccountInformationText,
                flights, agentId, agentName, agentAccountNumber, agentCity, agentIataCode, agentCassAddress, agentParticipantId,
                consigneeId, shipperId, userId, warehouseId, fileReference, participantIdentifierId, participantCode, airportId).Single();

            return id.Value;
        }

        /// <summary>
        /// Step 2 save
        /// </summary>        
        public void SaveAwbChargeDeclarations(AwbChargeDeclaration awbChargeDeclaration, long userId, long warehouseId)
        {
            var otherCharges = awbChargeDeclaration.OtherChargesList.ToXml<ChargeModel>();
            var rateDescriptions = awbChargeDeclaration.RateDescriptionsList.ToXml<RateDescriptionModel>();
            var chargeDeclaration = awbChargeDeclaration.SpecialHandlingRequests.ToXml<SphCode>();
               
            Context.MCH_SaveAwbChargeDeclarationsWizard
                (
                    awbChargeDeclaration.AwbId, 
                    userId, 
                    warehouseId, 
                    awbChargeDeclaration.ChargeDeclarations.Id,
                    awbChargeDeclaration.ChargeDeclarations.CurrencyId,
                    awbChargeDeclaration.ChargeDeclarations.WTVALChargesType,
                    awbChargeDeclaration.ChargeDeclarations.OtherChargesType,
                    awbChargeDeclaration.ChargeDeclarations.DeclaredValueForCarrige,
                    awbChargeDeclaration.ChargeDeclarations.NVD,
                    awbChargeDeclaration.ChargeDeclarations.DeclaredValueForCustoms,
                    awbChargeDeclaration.ChargeDeclarations.NCV,
                    awbChargeDeclaration.ChargeDeclarations.InsuranceAmount,
                    awbChargeDeclaration.ChargeDeclarations.XXX, 
                    otherCharges, 
                    rateDescriptions,
                    awbChargeDeclaration.ChargeDeclarations.WTVALChargesType.Value == 1 ? awbChargeDeclaration.TotalValuationCharges.Collect.Value : awbChargeDeclaration.TotalValuationCharges.Prepaid.Value,
                    awbChargeDeclaration.ChargeDeclarations.WTVALChargesType.Value == 1 ? awbChargeDeclaration.TotalTaxes.Collect.Value : awbChargeDeclaration.TotalTaxes.Prepaid.Value,
                    chargeDeclaration,
                    awbChargeDeclaration.HandlingInformation,
                    awbChargeDeclaration.AirwaybillType,
                    awbChargeDeclaration.CertificationName,
                    awbChargeDeclaration.ExecutedAtPlace, 
                    awbChargeDeclaration.ExecutedOnDate, 
                    awbChargeDeclaration.ExecutedBy
                );
        }
        
        /// <summary>
        /// Step 2 get
        /// </summary>        
        public AwbChargeDeclaration GetAwbChargeDeclaration(long awbId)
        {
            var awb = Context.AWBs.Single(a => a.Id == awbId);
            var chargeDeclaration = awb.ChargeDeclarations.SingleOrDefault();

            if (chargeDeclaration == null) return null;

            var summary = awb.ChargeSummaries.Single();

            var rateDescription = new AwbChargeDeclaration
            {
                AwbId = awbId,
                ChargeDeclarations = new ChargeDeclarationsModel
                {
                    Id = chargeDeclaration.Id,
                    CurrencyId = chargeDeclaration.CurrencyId,
                    NCV = chargeDeclaration.IsNoCustomsValue,
                    NVD = chargeDeclaration.IsNoCarriageValue,
                    XXX = chargeDeclaration.IsNoInsuranceValue,
                    WTVALChargesType = chargeDeclaration.IsWeightCollect ? 1 : 0,
                    OtherChargesType = chargeDeclaration.IsOtherChargesCollect ? 1 : 0,
                    InsuranceAmount = chargeDeclaration.InsuranceDeclaredValue,
                    DeclaredValueForCarrige = chargeDeclaration.CarriageDeclaredValue,
                    DeclaredValueForCustoms = chargeDeclaration.CustomsDeclaredValue
                },

                
               SpecialHandlingRequests = awb.SpecialHandlings.Select(item => new SphCode
                    {
                        CodeId = item.SHCodeId
                    }).ToList(),
              

                OtherChargesList = awb.OtherCharges.Select(o => new ChargeModel
                {
                    Id = o.Id,
                    Amount = o.Amount,
                    CodeId = o.CodeId,
                }).ToList(),
                RateDescriptionsList = awb.RateDescriptions.Select(d => new RateDescriptionModel
                {
                    Id = d.Id,
                    NoOfPieces = d.Line_Pieces,
                    UOMWeightId = d.Line_WeightUOMId,
                    ChargeableWeight = d.Line_ChargeableWeight,
                    CommodityItemNo = d.Line_Commodity_ItemNo,
                    GrossWeight = d.Line_GrossWeight,
                    RateCharge = d.Line_Rate,
                    RateClass = d.RateClassCode1 != null ? d.RateClassCode1.Code : String.Empty,
                    Total = d.Line_TotalCharges,
                    GoodsDescription = d.Line_GoodsDescription
                }).ToList(),
                TotalTaxes = new MCH.BLL.Model.AwbWizard.ChargeType
                {
                    Collect = chargeDeclaration.IsWeightCollect ? summary.TotalChargesTax : null,
                    Prepaid = chargeDeclaration.IsWeightCollect ? null : summary.TotalChargesTax,
                },
                TotalValuationCharges = new MCH.BLL.Model.AwbWizard.ChargeType
                {
                    Collect = chargeDeclaration.IsWeightCollect ? summary.TotalChargesValuation : null,
                    Prepaid = chargeDeclaration.IsWeightCollect ? null : summary.TotalChargesValuation,
                },
                TotalWeightCharges = new MCH.BLL.Model.AwbWizard.ChargeType
                {
                    Collect = chargeDeclaration.IsWeightCollect ? summary.TotalChargesWeight : null,
                    Prepaid = chargeDeclaration.IsWeightCollect ? null : summary.TotalChargesWeight,
                },


                HandlingInformation = String.Concat((awb.SpecialServiceRequest1 ?? String.Empty),
                                                    (awb.SpecialServiceRequest2 ?? String.Empty),
                                                    (awb.SpecialServiceRequest3 ?? String.Empty),
                                                    (awb.OtherServiceInfo1 ?? String.Empty),
                                                    (awb.OtherServiceInfo2 ?? String.Empty)),                
                AirwaybillType = awb.AirwaybillType,
                CertificationName = awb.ShipperCertification,
                ExecutedAtPlace = awb.ExecutedAtPlace,
                ExecutedOnDate = awb.ExecutedOnDate,
                ExecutedBy = awb.ExecutedBy
            };

            return rateDescription;
        }

        public int? GetAwbTypeByAwbId(long awbId)
        {
            int? airwaybillType = null;
            var awb = Context.AWBs.SingleOrDefault(p => p.Id == awbId);

            if (awb != null)
            {
                airwaybillType = awb.AirwaybillType; 
            }

            return airwaybillType;
        }

        public void SaveAwbUldDims(long awbId, long userId, long warehouseId, string goodDescriptions, string unitLoadDevices, int? slac, double? volume, long? volumeUomId)
        {
            Context.MCH_SaveAwbUldDimsWizard(awbId, userId, warehouseId, goodDescriptions, unitLoadDevices, slac, volume, volumeUomId);
        }

        public AwbUldsDimensions GetAwbUldDims(long? awbId)
        {
            if (!awbId.HasValue) return null;

            var awb = Context.AWBs.SingleOrDefault(a => a.Id == awbId);

            return new AwbUldsDimensions
            {
                AwbId = awbId.Value,
                Slac = awb.SLAC,
                Volume = awb.TotalVolume,
                VolumeUOMId = awb.VolumeUOMId,
                GoodsDescriptions = awb.Dimensions.Select(d => new GoodDescriptionModel
                {
                    Id = d.Id,
                    DimUOMId = d.DimUOMId,
                    Height = d.Height,
                    Length = d.Length,
                    Pieces = d.Pieces,
                    Weight = d.Weight,
                    WeightUOMId = d.WeightUOMId,
                    Width = d.Width
                }).ToList(),
                UnitLoadDevicess = awb.AWBs_ULDs.Select(au => new UnitLoadDeviceModel
                {
                    Id = au.ULD.Id,
                    OwnerId = au.ULD.OwnerId,
                    Said = au.ULD.TotalPieces,
                    ULDNo = au.ULD.SerialNumber,
                    ULDTypeId = au.ULD.UnitTypeId
                }).ToList()
            };
        }


        public long? SaveAwbWizarsSummary(AwbSummaryStepData awbSummary)
        {
            this.Context.MCH_SaveAwbSummaryWizard(awbSummary.AwbId, awbSummary.UserId, awbSummary.WarehouseId, awbSummary.AirportId, awbSummary.CertificationName,
                awbSummary.ExecutedAtPlace, awbSummary.ExecutedBy, awbSummary.ExecutedOnDate, awbSummary.ParticipantCode, awbSummary.ParticipantIdentfierId);

            return awbSummary.AwbId;
        }

        public AwbSummaryStepData GetAwbWizardsSummary(long awbId)
        {
            var tmpAwb = this.Context.AWBs.SingleOrDefault(item => item.Id == awbId);

            AwbSummaryStepData tmpModel = null;

            if (tmpAwb != null)
            {
                tmpModel = new AwbSummaryStepData();
                tmpModel.AwbId = awbId;
                tmpModel.AirwaybillType = tmpAwb.AirwaybillType;
                tmpModel.CertificationName = tmpAwb.ShipperCertification;
                tmpModel.ExecutedAtPlace = tmpAwb.ExecutedAtPlace;
                tmpModel.ExecutedBy = tmpAwb.ExecutedBy;
                tmpModel.ExecutedOnDate = tmpAwb.ExecutedOnDate ?? DateTime.Now;
                var tmpSenderRef = tmpAwb.SenderReferences.FirstOrDefault();
                if (tmpSenderRef != null)
                {
                    tmpModel.AirportId = tmpSenderRef.ParticipantPortId;
                    tmpModel.ParticipantCode = tmpSenderRef.ParticipantCode;
                    tmpModel.ParticipantIdentfierId = tmpSenderRef.ParticipantIdentifierId;
                }
            }

            return tmpModel;
        }

        public void FinalizeAWBWizard(long awbId, long userId, long? warehouseId)
        {
            this.Context.MCH_FinalizeAwbWizard(awbId, userId, warehouseId);
        }

        public AddEditHwbModel GetHwbModel(long hwbId)
        {
            AddEditHwbModel tmpModel = new AddEditHwbModel();
            var tmpHwb = this.Context.HWBs.SingleOrDefault(item => item.Id == hwbId);
            var tmpAwb = tmpHwb.AWBs_HWBs.FirstOrDefault();

            if (tmpHwb != null)
            {
                tmpModel.HWBSerialNumber = tmpHwb.HWBSerialNumber;
                tmpModel.OriginId = tmpHwb.OriginId;
                tmpModel.DestinationId = tmpHwb.DestinationId;
                tmpModel.DescriptionOfGoods = tmpHwb.DescriptionOfGoods;
                tmpModel.ExportControlNo = tmpHwb.ExportControl;
                tmpModel.SLAC = tmpHwb.SLAC;
                tmpModel.TotalPieces = tmpHwb.Pieces;
                tmpModel.TotalWeight = Convert.ToDecimal(tmpHwb.Weight);
                tmpModel.UOMWeightId = tmpHwb.WeightUOMId;

                tmpModel.AWBNumber = tmpAwb.AWB.AWBSerialNumber;
                tmpModel.AirwaybillId = tmpAwb.AWB.Id;


                tmpModel.ChargeDeclarations = new ChargeDeclarationModel();
                if (tmpHwb.ChargeDeclarations.Any())
                {
                    var tmpChargeDeclaration = tmpHwb.ChargeDeclarations.FirstOrDefault();
                    tmpModel.ChargeDeclarations.CurrencyId = tmpChargeDeclaration.CurrencyId;
                    tmpModel.ChargeDeclarations.DeclaredValueForCustoms = tmpChargeDeclaration.CustomsDeclaredValue;
                    tmpModel.ChargeDeclarations.DeclaredValueForCarrige = tmpChargeDeclaration.CarriageDeclaredValue;
                    tmpModel.ChargeDeclarations.InsuranceAmount = tmpChargeDeclaration.InsuranceDeclaredValue;
                    tmpModel.ChargeDeclarations.NVD = tmpChargeDeclaration.IsNoCarriageValue;
                    tmpModel.ChargeDeclarations.NCV = tmpChargeDeclaration.IsNoCustomsValue;
                    tmpModel.ChargeDeclarations.XXX = tmpChargeDeclaration.IsNoInsuranceValue;
                    tmpModel.ChargeDeclarations.OtherChargesType = tmpChargeDeclaration.IsOtherChargesCollect ? 1 : 0;
                    tmpModel.ChargeDeclarations.WTVALChargesType = tmpChargeDeclaration.IsWeightCollect ? 1 : 0;
                }

                tmpModel.Consignee = new PartnerModel();
                if (tmpHwb.Customer1 != null)
                {
                    tmpModel.Consignee.AccountNumber = tmpHwb.Customer1.AccountNumber;
                    tmpModel.Consignee.Address1 = tmpHwb.Customer1.AddressLine1;
                    tmpModel.Consignee.City = tmpHwb.Customer1.City;
                    tmpModel.Consignee.CountryId = tmpHwb.Customer1.CountryId;
                    var tmpConContact = tmpHwb.Customer1.Contacts.FirstOrDefault();
                    tmpModel.Consignee.Fax = tmpConContact != null ? tmpConContact.Fax : null;
                    tmpModel.Consignee.Id = tmpHwb.Customer1.Id;
                    tmpModel.Consignee.Name = tmpHwb.Customer1.Name;
                    tmpModel.Consignee.Phone = tmpConContact != null ? tmpConContact.Phone : null;
                    tmpModel.Consignee.Postal = tmpHwb.Customer1.PostalCode;
                    tmpModel.Consignee.StateId = tmpHwb.Customer1.StateId;
                    tmpModel.Consignee.Type = enumPartnerTypes.Consignee;
                }

                tmpModel.Shipper = new PartnerModel();
                if (tmpHwb.Customer != null)
                {
                    tmpModel.Shipper.AccountNumber = tmpHwb.Customer.AccountNumber;
                    tmpModel.Shipper.Address1 = tmpHwb.Customer.AddressLine1;
                    tmpModel.Shipper.City = tmpHwb.Customer.City;
                    tmpModel.Shipper.CountryId = tmpHwb.Customer.CountryId;
                    var tmpContact = tmpHwb.Customer.Contacts.FirstOrDefault();
                    tmpModel.Shipper.Fax = tmpContact != null ? tmpContact.Fax : null;
                    tmpModel.Shipper.Id = tmpHwb.Customer.Id;
                    tmpModel.Shipper.Name = tmpHwb.Customer.Name;
                    tmpModel.Shipper.Phone = tmpContact != null ? tmpContact.Phone : null;
                    tmpModel.Shipper.Postal = tmpHwb.Customer.PostalCode;
                    tmpModel.Shipper.StateId = tmpHwb.Customer.StateId;
                    tmpModel.Shipper.Type = enumPartnerTypes.Shipper;
                }

                tmpModel.OtherCustomsInfo = new List<Model.Customs.CustomsInfo>();
                if (tmpHwb.OCIs.Any())
                {
                    tmpModel.OtherCustomsInfo = tmpHwb.OCIs.Select(item => new CustomsInfo
                    {
                        SupplementaryCustomsInfo = item.SupCustomsInfo,
                        CountryCodeId = item.CountryId.HasValue ? item.CountryId.ToString() : String.Empty,
                        CountryCode = item.Country != null ? item.Country.TwoCharacterCode : String.Empty,
                        CustomsInfoIdentifier = item.CustomsInfoIdentifier != null ? item.CustomsInfoIdentifier.Code : string.Empty,
                        CustomsInfoIdentifierId = item.CustomsInfoIdentifierId.HasValue ? item.CustomsInfoIdentifierId.ToString() : string.Empty,
                        IdToDB = item.Id,
                        InfoIdentifier = item.LineIdentifier != null ? item.LineIdentifier.Code : string.Empty,
                        InfoIdentifierId = item.LineIdentifierId.HasValue ? item.LineIdentifierId.ToString() : string.Empty,

                    }).ToList();
                }
            
                tmpModel.HarmonizedCommodityCodes = new List<SphCode>();
                if (tmpHwb.HWBHTSSets.Any())
                {
                    tmpModel.HarmonizedCommodityCodes = tmpHwb.HWBHTSSets.Select(item => new SphCode
                    {
                        CodeId = item.ImportExportCodeId
                    }).ToList();
                }
                
            }


            return tmpModel;
        }

        public long SaveHWB(AddEditHwbModel addEditHwbModel, long userAccountId, long userId)
        {
            long savedHwbId = -1;

            var tmpAwb = this.Context.AWBs.SingleOrDefault(item => item.Id == addEditHwbModel.AirwaybillId);

            var hwb = Context.HWBs.SingleOrDefault(h => h.HWBSerialNumber == addEditHwbModel.HWBSerialNumber &&
                    h.OriginId == addEditHwbModel.OriginId && h.DestinationId == addEditHwbModel.DestinationId && h.AccountId == tmpAwb.AccountId && addEditHwbModel.Id != h.Id );
            if (hwb != null)
                return -2; // can not add duplicate value

            
            var tmpHwb = this.Context.HWBs.SingleOrDefault(item => item.Id == addEditHwbModel.Id);
            var tmpUser = this.Context.UserProfiles.SingleOrDefault(item => item.Id == userId);
            var tmpTask = this.Context.Tasks.SingleOrDefault(item => item.EntityTypeId == 2 && item.EntityId == addEditHwbModel.AirwaybillId && item.TaskTypeId == 16);

            if (tmpAwb != null)
            {
                using (var transaction = this.Context.Database.BeginTransaction())
                {
                    try
                    {
                        if (tmpHwb == null)
                        {
                            tmpHwb = new HWB();
                            tmpAwb.AWBs_HWBs.Add(new AWBs_HWBs { AWBId = tmpAwb.Id, HWB = tmpHwb, RecDate = DateTime.UtcNow });
                        }
                        else
                        {
                            if (!tmpAwb.AWBs_HWBs.Select(item => item.HWBId == tmpHwb.Id).Any())
                            {
                                tmpAwb.AWBs_HWBs.Add(new AWBs_HWBs { AWBId = tmpAwb.Id, HWB = tmpHwb, RecDate = DateTime.UtcNow });
                            }
                        }

                        tmpHwb.HWBSerialNumber = addEditHwbModel.HWBSerialNumber;
                        tmpHwb.OriginId = addEditHwbModel.OriginId;
                        tmpHwb.DestinationId = addEditHwbModel.DestinationId;
                        tmpHwb.DescriptionOfGoods = addEditHwbModel.DescriptionOfGoods;
                        tmpHwb.ExportControl = addEditHwbModel.ExportControlNo;
                        tmpHwb.SLAC = addEditHwbModel.SLAC.HasValue ? (int)addEditHwbModel.SLAC.Value : 0;
                        tmpHwb.Pieces = addEditHwbModel.TotalPieces.HasValue ? (int)addEditHwbModel.TotalPieces.Value : 0;

                        const int kgUomId = 4;
                        var selectedUom = (int)addEditHwbModel.UOMWeightId.Value;
                        tmpHwb.Weight = selectedUom == kgUomId ? Convert.ToDouble(addEditHwbModel.TotalWeight) : Convert.ToDouble(addEditHwbModel.TotalWeight) * 0.453592;
                        tmpHwb.WeightUOMId = kgUomId;
                        tmpHwb.AccountId = tmpAwb.AccountId;

                        if (addEditHwbModel.ChargeDeclarations != null)
                        {
                            var tmpCharegeDeclaration = tmpHwb.ChargeDeclarations.FirstOrDefault();
                            if (tmpCharegeDeclaration == null)
                            {
                                tmpCharegeDeclaration = new ChargeDeclaration();
                                tmpHwb.ChargeDeclarations.Add(tmpCharegeDeclaration);
                            }
                            tmpCharegeDeclaration.CurrencyId = (int)addEditHwbModel.ChargeDeclarations.CurrencyId;
                            tmpCharegeDeclaration.CustomsDeclaredValue = addEditHwbModel.ChargeDeclarations.DeclaredValueForCustoms;
                            tmpCharegeDeclaration.CarriageDeclaredValue = addEditHwbModel.ChargeDeclarations.DeclaredValueForCarrige;
                            tmpCharegeDeclaration.InsuranceDeclaredValue = addEditHwbModel.ChargeDeclarations.InsuranceAmount;
                            tmpCharegeDeclaration.IsNoCarriageValue = addEditHwbModel.ChargeDeclarations.NVD;
                            tmpCharegeDeclaration.IsNoCustomsValue = addEditHwbModel.ChargeDeclarations.NCV;
                            tmpCharegeDeclaration.IsNoInsuranceValue = addEditHwbModel.ChargeDeclarations.XXX;
                            tmpCharegeDeclaration.IsOtherChargesCollect = addEditHwbModel.ChargeDeclarations.OtherChargesType == 0 ? false : true;
                            tmpCharegeDeclaration.IsWeightCollect = addEditHwbModel.ChargeDeclarations.WTVALChargesType == 0 ? false : true;
                            tmpCharegeDeclaration.RecDate = DateTime.UtcNow;
                        }

                        if (addEditHwbModel.Consignee != null)
                        {
                            tmpHwb.ConsigneeId = addEditHwbModel.Consignee.Id;
                        }

                        if (addEditHwbModel.Shipper != null)
                        {
                            tmpHwb.ShipperId = addEditHwbModel.Shipper.Id;
                        }                        

                        if (addEditHwbModel.OtherCustomsInfo != null && addEditHwbModel.OtherCustomsInfo.Any())
                        {
                            if (tmpHwb.OCIs.Any())
                            {
                                tmpHwb.OCIs.Clear();
                            }
                            foreach (var item in addEditHwbModel.OtherCustomsInfo)
                            {
                                tmpHwb.OCIs.Add(new OCI
                                {
                                    SupCustomsInfo = item.SupplementaryCustomsInfo,
                                    CountryId = !String.IsNullOrEmpty(item.CountryCodeId) ? Int32.Parse(item.CountryCodeId) : new Nullable<int>(),
                                    CustomsInfoIdentifierId = !String.IsNullOrEmpty(item.CustomsInfoIdentifierId) ? Int32.Parse(item.CustomsInfoIdentifierId) : new Nullable<int>(),
                                    LineIdentifierId = !String.IsNullOrEmpty(item.InfoIdentifierId) ? Int32.Parse(item.InfoIdentifierId) : new Nullable<int>()
                                });
                            }
                        }

                    
                        if (addEditHwbModel.HarmonizedCommodityCodes != null && addEditHwbModel.HarmonizedCommodityCodes.Any())
                        {
                            if (tmpHwb.HWBHTSSets.Any())
                            {
                                tmpHwb.HWBHTSSets.Clear();
                            }
                            foreach (var item in addEditHwbModel.HarmonizedCommodityCodes)
                            {
                                if (item.CodeId.HasValue)
                                {
                                    tmpHwb.HWBHTSSets.Add(new HWBHTSSet
                                    {
                                        ImportExportCodeId = (int)item.CodeId.Value
                                    });
                                }

                            }
                        }

                        string tmpTransactionRef = this.Context.MCH_GetEntityReference(2, addEditHwbModel.AirwaybillId).FirstOrDefault();

                        var tmpTransaction = new Transaction
                        {
                            EntityTypeId = 2,
                            EntityId = addEditHwbModel.AirwaybillId,
                            StatusId = 48,
                            Description = "AWB HWBS Entry In Progress",
                            StatusTimestamp = DateTime.UtcNow,
                            Reference = tmpTransactionRef,
                            TaskTypeId = 16,
                            TaskId = tmpTask.Id,
                            UserId = userId
                        };


                        this.Context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }

                savedHwbId = tmpHwb.Id;
            }


            return savedHwbId;
        }

        public void DeleteAwbsHWB(long awbId, long hwbId)
        {
            var tmpAwbsHwb = this.Context.AWBs_HWBs.Where(item => item.AWBId == awbId && item.HWBId == hwbId).SingleOrDefault();
            if (tmpAwbsHwb != null)
            {
                using (var transaction = this.Context.Database.BeginTransaction())
                {
                    try
                    {
                        tmpAwbsHwb.HWB.SpecialHandlings.Clear();

                        tmpAwbsHwb.HWB.OCIs.Clear();

                        this.Context.HWBs.Remove(tmpAwbsHwb.HWB);

                        this.Context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }

            }
        }

        public AwbStepModel GetAwbByNumber(int accountId, string serialNumber, long originId, long destinationId)
        {
            var awb = Context.AWBs.Where(a => a.AccountId == accountId && 
                a.OriginId == originId && a.DestinationId == destinationId).ToList()
                .SingleOrDefault(a => a.AWBSerialNumber == serialNumber.Substring(4) && a.Carrier.Carrier3Code == serialNumber.Substring(0, 3));
            if (awb == null) return new AwbStepModel
            {
                AircraftType = 1,
                AccountInformationText = String.Empty,
                Shipper = new CustomerModel(),
                Consignee = new CustomerModel(),
                Flights = new List<FlightModel> { new FlightModel(), new FlightModel(), new FlightModel() },
                IssuingCarriersAgent = new AgentModel()
            };
            
            return FillAwbModel(awb);
        }

        private AwbStepModel FillAwbModel(AWB awb)
        {
            CustomerModel shipper;
            CustomerModel consignee;

            List<FlightModel> flights = new List<FlightModel>();
            var legSegments = awb.LegSegments.ToList();

            for (var i = 0; i < legSegments.Count; ++i)
            {
                var legSegment = legSegments[i];
                flights.Add(new FlightModel
                {
                    Id = legSegment.Id,
                    CarrierId = legSegment.CarrierId,
                    FlightNo = legSegment.FlightNumber,
                    FlightDate = legSegment.ETD.Value,
                    FlightToId = legSegment.PortId,
                    FlightFromId = i == 0 ? awb.OriginId.Value : legSegments[i - 1].PortId
                });
            }

            using (var unit = new CommonUnit())
            {
                shipper = awb.ShipperId.HasValue ? unit.GetCustomerById(awb.ShipperId.Value) : new CustomerModel();
                consignee = awb.ConsigneeId.HasValue ? unit.GetCustomerById(awb.ConsigneeId.Value) : new CustomerModel();
            }


            var model = new AwbStepModel
            {
                Id = awb.Id,
                AwbSerialNumber = awb.Carrier.Carrier3Code + "-" + awb.AWBSerialNumber,
                CarrierId = awb.AccountId,
                OriginId = awb.OriginId.Value,
                DestinationId = awb.DestinationId.Value,
                GoodsDescription = awb.DescriptionOfGoods,
                SpecialServiceRequests = (!String.IsNullOrWhiteSpace(awb.SpecialServiceRequest1) ? awb.SpecialServiceRequest1 : String.Empty) + (!String.IsNullOrWhiteSpace(awb.SpecialServiceRequest2) ? awb.SpecialServiceRequest2 : String.Empty) + (!String.IsNullOrWhiteSpace(awb.SpecialServiceRequest3) ? awb.SpecialServiceRequest3 : string.Empty),
                OtherServiceRequests = !String.IsNullOrWhiteSpace(awb.OtherServiceInfo1) ? awb.OtherServiceInfo1 : string.Empty + (!String.IsNullOrWhiteSpace(awb.OtherServiceInfo2) ? awb.OtherServiceInfo2 : string.Empty),
                AircraftType = awb.AircraftTypeId.HasValue ? awb.AircraftTypeId.Value : 1,
                AwbTypeId = awb.AirwaybillType,
                AccountInformationText = awb.AccountingInformations.Select(i => i.AccountingInfo).ToList().ToCombinedString(),
                IssuingCarriersAgent = awb.Agent != null ? new AgentModel
                {
                    Id = awb.Agent.Id,
                    City = awb.Agent.Place,
                    Name = awb.Agent.Name,
                    CassAddress = awb.Agent.CASSAddress,
                    IataCode = awb.Agent.NumericCode,
                    AccountNumber = awb.Agent.AccountNumber,
                    ParticipantIdentfierId = awb.Agent.ParticipantId
                } : new AgentModel(),
                Shipper = shipper,
                Consignee = consignee,
                Flights = flights,
                SenderReferences = awb.SenderReferences.Select(sr => new SenderReferenceModel 
                {
                    Id = sr.Id,
                    AwbId = sr.AwbId,
                    FileReference = sr.SenderFileReference,
                    ParticipantIdentifierId = sr.ParticipantIdentifierId,
                    ParticipantCode = sr.ParticipantCode,
                    AirportId = sr.ParticipantPortId

                }).ToList()
            };

            return model;
        }
    }
}