﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MCH.BLL.Helper
{
    public static class Extensions
    {
        public static string ToXml<T>(this IEnumerable<T> fields)
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            string xmlString;
            using (var memStream = new MemoryStream())
            {
                serializer.Serialize(memStream, fields);
                var encoding = Encoding.UTF8;
                var xmlBytes = new byte[memStream.Length];
                memStream.Seek(0, SeekOrigin.Begin);
                memStream.Read(xmlBytes, 0, Convert.ToInt32(memStream.Length));
                xmlString = encoding.GetString(xmlBytes);
            }
            return xmlString;
        }

        public static string ToXml<T>(this T fields)
        {
            var serializer = new XmlSerializer(typeof(T));
            string xmlString;
            using (var memStream = new MemoryStream())
            {
                serializer.Serialize(memStream, fields);
                var encoding = Encoding.UTF8;
                var xmlBytes = new byte[memStream.Length];
                memStream.Seek(0, SeekOrigin.Begin);
                memStream.Read(xmlBytes, 0, Convert.ToInt32(memStream.Length));
                xmlString = encoding.GetString(xmlBytes);
            }
            return xmlString;
        }

        public static string ToCombinedString(this List<String> fields)
        {
            string result = "";

            if(fields == null || fields.Count == 0)
            {
                return result;
            }

            foreach (var item in fields)
            {
                result += item; 
            }

            return result;
        }
             
    }
}
