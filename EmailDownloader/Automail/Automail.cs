﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Reporting;

namespace Automail
{
    public class Automail : IDisposable
    {
        #region [--Constructor--]

        public Automail(string connection, string attachmentFolder, string reportServerFolder)
        {
            ConnectionString = connection;
            AttachmentFolder = attachmentFolder;
            ReportServerFolder = reportServerFolder;
        }

        #endregion

        #region [--Public methods--]

        public void ProcessAutomail()
        {
            using (var db = new MCHCoreDBDataContext(ConnectionString))
            {
                List<HostPlus_AutoEmail> automails = (from mail in db.HostPlus_AutoEmails
                                                      where mail.StartDate <= DateTime.Now &&
                                                            mail.EndDate >= DateTime.Now &&
                                                            (!mail.NextDate.HasValue || mail.NextDate.Value <= DateTime.Now) &&
                                                            mail.Frequency != ""
                                                      select mail).ToList();

                foreach (HostPlus_AutoEmail autoEmail in automails)
                {
                    try
                    {
                        const string fileParam = "@@FILE@@";
                        _paramList = new Dictionary<string, string>();
                        _reports = new List<Report>();

                        //TODO: Process automail:
                        //Check if day is good. MailDays string like 'MON, TUE, WED, THU, FRI, SAT, SUN'
                        if (string.IsNullOrEmpty(autoEmail.MailDays))
                            continue;

                        string now = DateTime.Now.ToString("ddd").ToUpper();
                        bool isAGoodDayToAutoEmail = autoEmail.MailDays.ToUpper().Split(',').Any(p => p.Trim() == now);

                        if (isAGoodDayToAutoEmail)
                        {
                            bool shouldSend = true;
                            //If report, create report.
                            long mailId = autoEmail.RecId;
                            List<HostPlus_AutoEmail_Report> reports = (from reps in db.HostPlus_AutoEmail_Reports
                                                                       where reps.EmailId.Equals(mailId)
                                                                       select reps).ToList();
                            //Get reports.
                            foreach (var rep in reports)
                            {
                                if (!string.IsNullOrEmpty(rep.SqlQuery))
                                {
                                    string[] sqlParams = rep.SqlParameters.Split(',');
                                    string query = ReplaceParameters(autoEmail.SqlQuery);
                                    using (var conn = new SqlConnection(ConnectionString))
                                    {
                                        using (var comm = new SqlCommand(query, conn))
                                        {
                                            conn.Open();
                                            using (var reader = comm.ExecuteReader())
                                            {
                                                while (reader.Read())
                                                {
                                                    int i = 0;
                                                    foreach (string param in sqlParams)
                                                    {
                                                        var pair = param.Split('=');
                                                        if (pair.Length == 2)
                                                        {
                                                            AddParam(pair[0], pair[1]);
                                                        }
                                                        else
                                                        {
                                                            AddParam(param, reader.GetValue(i).ToString());
                                                        }
                                                        ++i;
                                                    }
                                                    _reports.Add(new Report()
                                                    {
                                                        Name = rep.Report,
                                                        Param = ReplaceParameters(rep.Parameters),
                                                        File = ReplaceParameters(rep.FileName),
                                                        Type = rep.ReportType
                                                    });
                                                }
                                            }
                                            conn.Close();
                                        }
                                    }
                                }
                                else
                                {
                                    _reports.Add(new Report()
                                    {
                                        Name = rep.Report,
                                        Param = ReplaceParameters(rep.Parameters),
                                        File = ReplaceParameters(rep.FileName),
                                        Type = rep.ReportType
                                    });
                                }

                                if (!string.IsNullOrEmpty(rep.SqlNonQuery))
                                {
                                    using (var conn = new SqlConnection(ConnectionString))
                                    {
                                        using (var comm = new SqlCommand(ReplaceParameters(rep.SqlNonQuery), conn))
                                        {
                                            comm.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }

                            string bodyDetail = "";
                            string attachment = "";

                            if (string.IsNullOrEmpty(autoEmail.SqlQuery))
                            {
                                bodyDetail = ReplaceParameters(autoEmail.BodyDetail);
                            }
                            else
                            {
                                string tmpBody = "";
                                string tmpAttachment = "";
                                using (var conn = new SqlConnection(ConnectionString))
                                {
                                    using (var comm = new SqlCommand(ReplaceParameters(autoEmail.SqlQuery), conn))
                                    {
                                        conn.Open();
                                        using (var reader = comm.ExecuteReader())
                                        {
                                            if (!string.IsNullOrEmpty(autoEmail.SqlParameters))
                                            {
                                                string[] mailSqlParams = autoEmail.SqlParameters.Split(',');
                                                shouldSend = reader.HasRows;
                                                while (reader.Read())
                                                {
                                                    _paramList = new Dictionary<string, string>();
                                                    int i = 0;
                                                    foreach (string param in mailSqlParams)
                                                    {
                                                        var pair = param.Split('=');
                                                        if (pair.Length == 2)
                                                        {
                                                            AddParam(pair[0], pair[1]);
                                                        }
                                                        else
                                                        {
                                                            AddParam(param, reader.GetValue(i).ToString());
                                                        }
                                                        ++i;
                                                    }

                                                    tmpAttachment += ReplaceParameters(fileParam) + ",";
                                                    tmpBody += ReplaceParameters(autoEmail.BodyDetail);
                                                }
                                            }
                                        }
                                        conn.Close();
                                    }
                                }
                                bodyDetail = tmpBody.Trim();
                                if (!string.IsNullOrEmpty(autoEmail.Attachment))
                                    attachment = autoEmail.Attachment.Replace(fileParam, tmpAttachment);
                            }

                            if (shouldSend)
                            {
                                string tmpBody = (autoEmail.BodyHeader == null ? "" : ReplaceParameters(autoEmail.BodyHeader)) + Environment.NewLine
                                    + (bodyDetail ?? "") + Environment.NewLine + (autoEmail.BodyFooter == null ? "" : ReplaceParameters(autoEmail.BodyFooter));
                                var mail = new HostPlus_Email()
                                {
                                    Body = tmpBody,
                                    Subject = ReplaceParameters(autoEmail.Subject),
                                    MailTo = ReplaceParameters(autoEmail.MailTo),
                                    MailForm = autoEmail.MailFrom,
                                    MailCc = autoEmail.MailCc,
                                    MailBcc = autoEmail.MailBcc,
                                    MailDate = DateTime.Now,
                                    Attachment = attachment,
                                    Gateway = autoEmail.Gateway,
                                    IsZip = autoEmail.IsZip,
                                    Status = 0,
                                    RecDate = DateTime.Now,
                                    UserId = "H+",
                                    IsAutoEmail = 1,
                                    Tries = 0
                                };
                                db.HostPlus_Emails.InsertOnSubmit(mail);

                                foreach (var report in _reports)
                                {
                                    string path = CreateReport(report);
                                    var mailAtt = new HostPlus_EmailAttachment()
                                    {
                                        FileName = path,
                                        HostPlus_Email = mail
                                    };
                                    db.HostPlus_EmailAttachments.InsertOnSubmit(mailAtt);
                                }
                            }
                        }

                        DateTime tmpNextDate = GetNextDate(autoEmail.Frequency);
                        if (tmpNextDate == DateTime.MinValue)
                        {
                            autoEmail.NextDate = DateTime.Now;
                            autoEmail.EndDate = DateTime.Now;
                        }
                        else
                        {
                            autoEmail.NextDate = tmpNextDate;
                        }
                        autoEmail.LastDate = DateTime.Now;
                        db.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        DateTime tmpNextDate = GetNextDate(autoEmail.Frequency);
                        if (tmpNextDate == DateTime.MinValue)
                        {
                            autoEmail.NextDate = DateTime.Now;
                            autoEmail.EndDate = DateTime.Now;
                        }
                        else
                        {
                            autoEmail.NextDate = tmpNextDate;
                        }
                        autoEmail.LastDate = DateTime.Now;
                        autoEmail.TransmitionError = ex.ToString();
                        db.SubmitChanges();
                    }
                }
            }
        }



        #endregion

        #region [--Properties--]

        public string ConnectionString { get; private set; }
        public string Gateway { get; private set; }
        public string AttachmentFolder { get; private set; }
        public string ReportServerFolder { get; private set; }

        #endregion

        #region [-- Private Methods--]

        private string ReplaceParameters(string query)
        {
            if (_paramList == null || string.IsNullOrEmpty(query))
                return query;
            string retString = _paramList.Keys.Aggregate(query,
                (current, param) => current.Replace(param, _paramList[param]));

            retString = retString.Replace("@@GATEWAY@@", Gateway);
            retString = retString.Replace("@@NOW@@", DateTime.Now.ToString("M-d-yyyy h:mm:ss tt"));
            retString = retString.Replace("@@TODAY@@", DateTime.Now.Date.ToString("M-d-yyyy"));
            retString = retString.Replace("@@YESTURDAY@@",
                DateTime.Now.Date.AddDays(-1).ToString("M-d-yyyy"));
            retString = retString.Replace("@@YESTERDAY@@",
                DateTime.Now.Date.AddDays(-1).ToString("M-d-yyyy"));
            retString = retString.Replace("@@TOMORROW@@",
                DateTime.Now.Date.AddDays(1).ToString("M-d-yyyy"));
            retString = retString.Replace("@@LASTWEEK@@",
                DateTime.Now.Date.AddDays(-7).ToString("M-d-yyyy"));
            retString = retString.Replace("@@NEXTWEEK@@",
                DateTime.Now.Date.AddDays(7).ToString("M-d-yyyy"));
            retString = retString.Replace("@@MONTHSTART@@",
                DateTime.Now.Date.AddDays(-(DateTime.Now.Day - 1)).ToString("M-d-yyyy"));
            retString = retString.Replace("@@YEARSTART@@",
                DateTime.Now.Date.AddDays(-(DateTime.Now.Day - 1))
                    .AddMonths(-(DateTime.Now.Month - 1))
                    .ToString("M-d-yyyy"));
            retString = retString.Replace("@@YEAREND@@",
                DateTime.Now.Date.AddMonths(12 - DateTime.Now.Month).AddDays(31 - DateTime.Now.Day).ToString("M-d-yyyy"));
            retString = retString.Replace("@@1WEEKAGO@@",
                DateTime.Now.Date.AddDays(-7).ToString("M-d-yyyy h:mm:ss tt"));
            retString = retString.Replace("@@1MONTHAGO@@",
                DateTime.Now.Date.AddMonths(-1).ToString("M-d-yyyy h:mm:ss tt"));
            retString = retString.Replace("@@LASTMONTH@@",
                DateTime.Now.Date.AddMonths(-1).AddDays(-(DateTime.Now.Day - 1)).ToString("M-d-yyyy h:mm:ss tt"));
            retString = retString.Replace("@@LASTMONTHEND@@",
                DateTime.Now.Date.AddDays(-DateTime.Now.Day).ToString("M-d-yyyy h:mm:ss tt"));
            retString = retString.Replace("@@LASTWEEKSTART@@",
                DateTime.Now.Date.AddDays(-((int)DateTime.Now.DayOfWeek + 7)).ToString("M-d-yyyy h:mm:ss tt"));
            retString = retString.Replace("@@LASTWEEKEND@@",
                DateTime.Now.Date.AddDays(-((int)DateTime.Now.DayOfWeek + 1)).ToString("M-d-yyyy h:mm:ss tt"));

            for (int i = 1; i <= 52; i++)
                retString = retString.Replace("@@" + i + "WEEKSAGO@@",
                    DateTime.Now.Date.AddDays(-(7 * i)).ToString("M-d-yyyy h:mm:ss tt"));

            return retString;
        }

        private string CreateReport(Report report)
        {
            string[] paramPair = report.Param.Split(',');
            var paramList = (from param in paramPair select param.Split('=') into nameVal where nameVal.Length == 2 select new ReportExecution2005.ParameterValue() { Name = nameVal[0], Value = nameVal[1] }).ToList();

            string tmpAttachmentPath = AttachmentFolder;
            string type = report.Type;
            string tmpfilename = report.File + "_" + DateTime.Now.ToString("MMddyyyyHHmmss") + GetFileExtention(ref type);

            ReportManager.SaveReport(ReportServerFolder, report.Name, paramList.ToArray(), type, tmpAttachmentPath, tmpfilename);
            return Path.Combine(tmpAttachmentPath, tmpfilename);
        }

        private string GetFileExtention(ref string reportType)
        {
            if (string.IsNullOrEmpty(reportType))
            {
                reportType = "PDF";
                return ".pdf";
            }
            switch (reportType.ToUpper())
            {
                case "EXCEL":
                case "XLS":
                    return ".xls";
                case "PDF":
                    return ".pdf";
                case "CSV":
                    return ".csv";
                default:
                    {
                        reportType = "PDF";
                        return ".pdf";
                    }
            }
        }

        private void AddParam(string name, string value)
        {
            if (_paramList.ContainsKey(name))
                _paramList[name] = value;
            else
            {
                _paramList.Add(name, value);
            }
        }

        public DateTime GetNextDate(string frequency)
        {
            string[] str = frequency.Split('=');

            switch (str[0])
            {
                case "T":
                    return DateTime.Now.AddSeconds(int.Parse(str[1]));
                    break;
                case "S":
                    {
                        string dateStr = DateTime.Now.ToString("MM/dd/yyyy") + " " + str[1];
                        DateTime tmpDate = DateTime.Parse(dateStr);

                        if (tmpDate > DateTime.Now)
                            return tmpDate;
                        else
                            return tmpDate.AddDays(1);
                        break;
                    }
                case "D":
                    return DateTime.Now.AddDays(int.Parse(str[1]));
                    break;
                case "M":
                    string[] tmpStrings = str[1].Trim().Split(',');
                    if (tmpStrings.Length == 1)
                        return DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(int.Parse(tmpStrings[0])).AddMonths(1);
                    else if (tmpStrings.Length == 2)
                    {
                        string tmpDateStr = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(int.Parse(tmpStrings[0])).AddMonths(1).ToString("MM/dd/yyyy") + " " + tmpStrings[1];
                        return DateTime.Parse(tmpDateStr);
                    }
                    else
                        return DateTime.Now.AddMonths(1);
                    break;
                default:
                    return DateTime.MinValue;
            }
        }

        #endregion

        #region [-- Private fields--]

        private Dictionary<string, string> _paramList;
        private List<Report> _reports;

        #endregion

        #region [--Dispose--]

        public void Dispose()
        {

        }

        #endregion
    }

    internal class Report
    {
        public string Name { get; set; }
        public string File { get; set; }
        public string Param { get; set; }
        public string Type { get; set; }
    }

}
