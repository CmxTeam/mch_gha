﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;

namespace MailClient
{
    public class MailSender : IDisposable
    {
        private readonly string _connectionString;
        private readonly string _smtp;
        private readonly string _smtpAccount;
        private readonly string _smtpPassword;

        public MailSender(string smtp, string smtpAccount, string smtpPassword, string connctionString)
        {
            _smtp = smtp;
            _connectionString = connctionString;
            _smtpAccount = smtpAccount;
            _smtpPassword = smtpPassword;
        }

        public void SendEmails()
        {
            var envelopes = GetEnvelopes();
            var successfullySentEmails = new Dictionary<long, string>();

            foreach (var envelope in envelopes)
            {
                try
                {
                    SendEmailImpl(envelope);
                    successfullySentEmails.Add(envelope.Id, string.Empty);
                }
                catch (Exception e)
                {
                    ExecuteScalar(string.Format("Update HostPlus_Emails Set Tries = isnull(Tries, 0) + 1, MailDate = getutcdate(), TransmitionError = {0} where RecId = {1}", e.Message, envelope.Id));
                }
            }

            if (successfullySentEmails.Count > 0)
            {
                var ids = string.Join(",", successfullySentEmails.Select(m => m.Key));
                ExecuteScalar(string.Format("Update HostPlus_Emails Set Status = 1, Tries = isnull(Tries, 0) + 1, MailDate = getutcdate() where RecId in ({0})", ids));
            }
        }

        private void SendEmailImpl(Envelope envelope)
        {
            var mailMessage = new MailMessage(envelope.From, envelope.To)
                                  {
                                      Subject = envelope.Subject,
                                      Priority = MailPriority.High,
                                      Body = envelope.Body
                                  };
            if (!string.IsNullOrEmpty(envelope.Cc))
                mailMessage.CC.Add(envelope.Cc);
            if (!string.IsNullOrEmpty(envelope.Bcc))
                mailMessage.Bcc.Add(envelope.Bcc);
            if (envelope.Attachments != null && envelope.Attachments.Count > 0)
            {
                foreach (string attachment in envelope.Attachments)
                {
                    mailMessage.Attachments.Add(new Attachment(envelope.IsZip ? Utility.CompressFile(attachment) : attachment, MediaTypeNames.Application.Octet));
                }
            }

            var smtpClient = new SmtpClient(_smtp);
            
            if (!String.IsNullOrEmpty(_smtpAccount))
                smtpClient.Credentials = new NetworkCredential(_smtpAccount, _smtpPassword);

            smtpClient.Send(mailMessage);

            Thread.Sleep(2000);
        }

        private IEnumerable<Envelope> GetEnvelopes()
        {
            var dataset = Fill("Select RecId, MailFrom, MailTo, MailCc, MailBcc, Subject, Body from HostPlus_Emails where Status = 0 and Tries < 5");
            IEnumerable<Envelope> envelopes = dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new Envelope
                 {
                     Id = Convert.ToInt64(r["RecId"].ToString()),
                     From = r["MailFrom"].ToString(),
                     To = r["MailTo"].ToString(),
                     Bcc = r["MailBcc"].ToString(),
                     Cc = r["MailCc"].ToString(),
                     Subject = r["Subject"].ToString(),
                     Body = r["Body"].ToString(),
                     IsZip = r["IsZip"] != null && (int)r["IsZip"]!=0
                 }).ToList();

            foreach (var env in envelopes)
            {
                var attachmentDataset = Fill("SELECT FileName FROM HostPlus_EmailAttachments WHERE EmailId = " + env.Id);
                List<string> attachments =
                    attachmentDataset.Tables[0].Rows.OfType<DataRow>().Select(r => r["FileName"].ToString()).ToList();

                env.Attachments = attachments;
            }

            return envelopes;
        }

        private void ExecuteScalar(string commandString)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(commandString, connection))
                {
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                    }
                }
            }
        }

        private DataSet Fill(string commandString)
        {
            var dataSet = new DataSet();
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(commandString, connection))
                {
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataSet);
                    }
                }
            }
            return dataSet;
        }

        public void Dispose()
        {

        }
    }
}
