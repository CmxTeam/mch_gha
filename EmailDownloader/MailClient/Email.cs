﻿using System;

namespace MailClient
{
    public class Email
    {
        public string Body { get; set; }
        public string Uid { get; set; }
        public DateTime DateSent { get; set; }
        public string Subject { get; set; }
    }
}