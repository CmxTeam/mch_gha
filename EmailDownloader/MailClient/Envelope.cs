﻿using System.Collections.Generic;

namespace MailClient
{
    public class Envelope
    {
        public long Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsZip { get; set; }
        public List<string> Attachments { get; set; }
    }
}