﻿namespace CimpTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProcessIncomingFfmButton = new System.Windows.Forms.Button();
            this.rawMessageParserButton = new System.Windows.Forms.Button();
            this.ougoingFileButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ProcessIncomingFfmButton
            // 
            this.ProcessIncomingFfmButton.Location = new System.Drawing.Point(13, 13);
            this.ProcessIncomingFfmButton.Name = "ProcessIncomingFfmButton";
            this.ProcessIncomingFfmButton.Size = new System.Drawing.Size(134, 23);
            this.ProcessIncomingFfmButton.TabIndex = 0;
            this.ProcessIncomingFfmButton.Text = "ProcessIncomingFfm";
            this.ProcessIncomingFfmButton.UseVisualStyleBackColor = true;
            this.ProcessIncomingFfmButton.Click += new System.EventHandler(this.ProcessIncomingFfmButtonClick);
            // 
            // rawMessageParserButton
            // 
            this.rawMessageParserButton.Location = new System.Drawing.Point(13, 80);
            this.rawMessageParserButton.Name = "rawMessageParserButton";
            this.rawMessageParserButton.Size = new System.Drawing.Size(134, 23);
            this.rawMessageParserButton.TabIndex = 1;
            this.rawMessageParserButton.Text = "Save Raw Messages";
            this.rawMessageParserButton.UseVisualStyleBackColor = true;
            this.rawMessageParserButton.Click += new System.EventHandler(this.RawMessageParserButtonClick);
            // 
            // ougoingFileButton
            // 
            this.ougoingFileButton.Location = new System.Drawing.Point(13, 139);
            this.ougoingFileButton.Name = "ougoingFileButton";
            this.ougoingFileButton.Size = new System.Drawing.Size(134, 23);
            this.ougoingFileButton.TabIndex = 2;
            this.ougoingFileButton.Text = "Process outgoing file";
            this.ougoingFileButton.UseVisualStyleBackColor = true;
            this.ougoingFileButton.Click += new System.EventHandler(this.OutgoingFileButtonClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 202);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Process Messages Folder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ProcessFolderClick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 262);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Process AIR AMS";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(13, 329);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(150, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Process Outgoing CIMP";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 386);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ougoingFileButton);
            this.Controls.Add(this.rawMessageParserButton);
            this.Controls.Add(this.ProcessIncomingFfmButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ProcessIncomingFfmButton;
        private System.Windows.Forms.Button rawMessageParserButton;
        private System.Windows.Forms.Button ougoingFileButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

