﻿using System;
using System.Windows.Forms;
using Cimp;

namespace CimpTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ProcessIncomingFfmButtonClick(object sender, EventArgs e)
        {

            var cimp = new Cimp.FileProcessor("SERVER=uscmxwsrdb; DATABASE=MCHDBDev; UID=appcon; PWD=AlphaRomeo880;", "BOSEI8X", "SERVER=uscmxwsrdb; DATABASE=MCHCoreDB; UID=appcon; PWD=AlphaRomeo880;");
            //var cimp = new Cimp.FileProcessor("SERVER=10.0.0.230; DATABASE=MCHDB; UID=appcon; PWD=AlphaRomeo880;", "BOSEI8X", "SERVER=10.0.0.230; DATABASE=MCHCoreDB; UID=appcon; PWD=AlphaRomeo880;");
            //cimp.ProcessDatabaseMessages();

            //var cimp = new Cimp.FileProcessor("", "", "");
            //cimp.ProcessFileMessages(@"C:\Users\Aram.Barseghyan.CARGOMATRIX\Desktop\testftp");
            cimp.ProcessDatabaseMessages();

            //cimp.ProcessFileMessages(@"C:\Users\Aram.Barseghyan.CARGOMATRIX\Desktop\testftp");    
            //cimp.ProcessFileMessages(@"C:\Users\Aram.Barseghyan.CARGOMATRIX\Desktop\TestCimp");
        }

        private void RawMessageParserButtonClick(object sender, EventArgs e)
        {
            var rwaProcessor = new Cimp.RawMessageProcessor("SERVER=uscmxwsrdb; DATABASE=MCHCoreDB; UID=appcon; PWD=AlphaRomeo880;", @"C:\Users\maria.topchian\Desktop\QUANTEM", @"C:\Users\maria.topchian\Desktop\QUANTEM\archive");
            rwaProcessor.ProcessFiles();
        }

        private void OutgoingFileButtonClick(object sender, EventArgs e)
        {
            var processor = new OutgoingMessageProcessor("SERVER=uscmxwsrdb; DATABASE=MCHCoreDB; UID=appcon; PWD=AlphaRomeo880;", @"C:\Users\Aram.Barseghyan.CARGOMATRIX\Desktop\rawmessaages");
            processor.ProcessMessages(".SND");
        }

        private void ProcessFolderClick(object sender, EventArgs e)
        {
            var cimp = new Cimp.FileProcessor("SERVER=uscmxwsrdb; DATABASE=MCHTestDB; UID=appcon; PWD=AlphaRomeo880;", "BOSTS8X", "SERVER=uscmxwsrdb; DATABASE=MCHCoreDB; UID=appcon; PWD=AlphaRomeo880;");

            cimp.ProcessFileMessages(@"C:\Users\maria.topchian\Desktop\rawmessaages");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var cimp = new Cimp.AirAmsProcessor(
                "SERVER=uscmxwsrdb; DATABASE=MCHCoreDB; UID=appcon; PWD=AlphaRomeo880;",
                @"C:\Users\maria.topchian\Desktop\rawmessaages", @"C:\Users\maria.topchian\Desktop\rawmessaages\Archive");
            cimp.Process();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var cimp = new Cimp.OutgoingCimpProcessor("SERVER=10.0.0.20; DATABASE=MCHDBDev; UID=appcon; PWD=AlphaRomeo880;", "SERVER=10.0.0.20; DATABASE=MCHCoreDB; UID=appcon; PWD=AlphaRomeo880;");
            cimp.ProcessOutgoingCimp();
        }
    }
}
