﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace Cimp
{
    public class AirAmsProcessor : IDisposable
    {
        private readonly string _connectionString;
        private readonly string _folder;
        private readonly string _archiveFolder;

        public AirAmsProcessor(string connection, string folder, string archiveFolder)
        {
            _connectionString = connection;
            _folder = folder;
            _archiveFolder = archiveFolder;
        }

        public void Process()
        {
            if (string.IsNullOrEmpty(_folder)) return;
            try
            {
                var files = Directory.GetFiles(_folder, "*.xml");
                string errorDir = Path.Combine(_folder, "ERROR");

                foreach (var fileName in files)
                {
                    var fs = new FileStream(fileName, FileMode.Open);
                    var sr = new StreamReader(fs);

                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    fs.Close();

                    try
                    {
                        ProcessXml(xmlString);

                        if (!Directory.Exists(_archiveFolder))
                            Directory.CreateDirectory(_archiveFolder);

                        string newName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Path.GetFileName(fileName);
                        System.IO.File.Move(fileName, Path.Combine(_archiveFolder, newName));
                    }
                    catch (Exception)
                    {
                        if (!Directory.Exists(errorDir))
                            Directory.CreateDirectory(errorDir);

                        string newName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Path.GetFileName(fileName);
                        System.IO.File.Move(fileName, Path.Combine(errorDir, newName));
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed processing AIR AMS: " + ex.ToString());
            }

        }

        private bool ProcessXml(string xml)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand("dam.ParseAndSaveManifest", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@xManifest", xml));
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
            return true;
        }

        public void Dispose()
        {

        }
    }
}
