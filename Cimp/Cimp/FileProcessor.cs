﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cimp.Messages;

namespace Cimp
{
    public class FileProcessor : BaseProcessor
    {
        private readonly string _connectionString;
        private readonly string _errorLoggingConnectionString;
        private readonly string _mailBoxName;

        public FileProcessor(string connectionString, string mailBoxName, string errorLoggingConnectionString)
        {
            _mailBoxName = mailBoxName;
            _connectionString = connectionString;
            _errorLoggingConnectionString = errorLoggingConnectionString;
        }

        public void ProcessDatabaseMessages()
        {
            var dataTable = new DataTable();
            using (var connection = new SqlConnection(_errorLoggingConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "Select FileName, RawMessage, SenderMailBox From CIMPMessageLogs Where ProcessingStatus = 0 and Direction = N'IN' and MailboxName = @MailboxName order by SentTime";

                    command.Parameters.Add(new SqlParameter("@MailboxName", _mailBoxName));

                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataTable);
                    }
                }
            }

            var files = dataTable.Rows.OfType<DataRow>().Select(r => new File { Name = r["FileName"].ToString(), Content = r["RawMessage"].ToString(), SenderMailBox = r["SenderMailBox"].ToString() });
            ProcessMessages(files);
        }

        public void ProcessFileMessages(string localFolder)
        {
            if (string.IsNullOrEmpty(localFolder)) return;
            var files = ReadFilesContent(localFolder);
            ProcessMessages(files);
        }

        private void ProcessMessages(IEnumerable<File> files)
        {
            foreach (var file in files)
            {
                Message message;
                if (file.Type == "FFM")
                {
                    message = new Ffm(file.Content, _connectionString, _mailBoxName.Substring(0, 3), file.SenderMailBox);
                }
                else if (file.Type == "FHL")
                {
                    message = new Fhl(file.Content, _connectionString, file.SenderMailBox);
                }
                else if (file.Type == "FSN")
                {
                    message = new Fsn(file.Content, _connectionString, file.SenderMailBox);
                }
                else if (file.Type == "FBL")
                {
                    message = new Fbl(file.Content, _connectionString, file.SenderMailBox);
                }
                else if (file.Type == "FWB")
                {
                    message = new Fwb(file.Content, _connectionString, file.SenderMailBox);
                }
                else
                {
                    Log(file.Name, "File type not supported.", file.Content, file.Type);
                    continue;
                }

                if (message.IsInitialisedSuccessfully)
                {
                    string errorMessage;
                    message.Save(out errorMessage);
                    Log(file.Name, errorMessage, file.Content, file.Type);
                }
                else
                {
                    Log(file.Name, message.InitialiserErrorMessage, file.Content, file.Type);
                }
            }
        }

        private void Log(string fileName, string errorMessage, string fileContent, string type)
        {
            using (var connection = new SqlConnection(_errorLoggingConnectionString))
            {
                using (var command = new SqlCommand("[LogCimpProcess]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        command.Parameters.Add(new SqlParameter("@FileName", fileName));
                        command.Parameters.Add(new SqlParameter("@ErrorMessage", string.IsNullOrEmpty(errorMessage) ? (object)DBNull.Value : errorMessage));
                        command.Parameters.Add(new SqlParameter("@FileContent", fileContent));
                        command.Parameters.Add(new SqlParameter("@MessageType", type));
                        command.Parameters.Add(new SqlParameter("@MailboxName", _mailBoxName));

                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                    }
                }
            }
        }
    }
}