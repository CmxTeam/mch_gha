﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Cimp
{
    public abstract class BaseProcessor
    {
        protected static IEnumerable<File> ReadFilesContent(string localFolder, string filters = "*.txt")
        {
            var fileNames = GetFileNames(localFolder, filters);
            var result = new List<File>();            

            foreach (var fileFullName in fileNames)
            {
                var fileExtension = fileFullName.Substring(fileFullName.LastIndexOf('.')).ToUpper();                
                var parts = fileFullName.ToUpper().Replace(fileExtension, "").Split("\\".ToCharArray());                                
                var fileName = parts[parts.GetUpperBound(0)];

                using (var stream = new FileStream(fileFullName, FileMode.Open, FileAccess.Read))
                {
                    using (var br = new BinaryReader(stream))
                    {
                        var bits = br.ReadBytes(Convert.ToInt32(br.BaseStream.Length));
                        br.Close();
                        stream.Close();

                        var encoding = Encoding.UTF8;
                        result.Add(new File { Name = fileName, FileExtension = fileExtension, Content = encoding.GetString(bits) });
                    }
                }
            }
            return result;
        }

        private static IEnumerable<string> GetFileNames(string sourceFolder, string filters)
        {
            return filters.Split('|').SelectMany(filter => Directory.GetFiles(sourceFolder, filter)).ToArray();
        }
    }
}
