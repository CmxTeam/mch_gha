﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Cimp.Messages.MessageParts
{
    public static class Utilitiy
    {
        public static IList<string> SplitLinesAndRemoveEmptyEntries(this string messageContent, string[] separator)
        {
            return messageContent.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(line => line.Trim()).ToList();
        }

        public static IList<string> SplitLines(this string messageContent, string[] separator)
        {            
            return messageContent.Split(separator, StringSplitOptions.None).Select(line => line.Trim()).ToList();
        }

        public static T GetValueOfIndex<T>(this IList<string> source, int index)
        {
            return source == null || index >= source.Count ? default(T) : (T)Convert.ChangeType(source[index], typeof(T));
        }

        public static string GetValueOfIndex(this IList<string> source, int index)
        {
            return source == null || index >= source.Count ? null : source[index];
        }

        public static T Substring<T>(this string line, int startIndex)
        {
            return (T)Convert.ChangeType(line.Substring<string>(startIndex, line.Length - startIndex), typeof(T));
        }

        public static T Substring<T>(this string line, int startIndex, int maxLenght, int decimalPlaces = 1)
        {            
            var shift = 0;
            return SubstringImp<T>(line, startIndex, maxLenght, ref shift, (sh, ml, l) => 0, decimalPlaces);
        }

        public static T Substring<T>(this string line, int startIndex, int maxLenght, ref int shift, int decimalPlaces = 1)
        {            
            return SubstringImp<T>(line, startIndex, maxLenght, ref shift, (sh, ml, l) => sh + ml - l, decimalPlaces);
        }

        private static T SubstringImp<T>(string line, int startIndex, int maxLenght, ref int shift, Func<int, int, int, int> action, int decimalPlaces = 1)
        {
            if (startIndex >= line.Length)
                return default(T);

            var subString = startIndex + maxLenght >= line.Length ? line.Substring(startIndex) : line.Substring(startIndex, maxLenght);
            string regex = null;

            if (typeof(T) == typeof(int))
                regex = @"(\d+)";

            if (typeof(T) == typeof(decimal))
                regex = @"\d{1,7}(\.\d{" + decimalPlaces + "})?";

            if (regex != null)
            {
                var match = Regex.Match(subString, regex).Value;
                shift = action(shift, maxLenght, match.Length);
                return (T)Convert.ChangeType(match, typeof(T));
            }
            return (T)Convert.ChangeType(subString, typeof(T));
        }
    }
}