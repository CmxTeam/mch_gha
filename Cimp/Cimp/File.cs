﻿using Cimp.Messages.MessageParts;

namespace Cimp
{
    public class File
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public string FileExtension { get; set; }
        public string SenderMailBox { get; set; }
        public string Type
        {
            get { return Content.SplitLines(new[] { "\n" })[0].SplitLines(new[] {"/"})[0]; }
        }
    }
}