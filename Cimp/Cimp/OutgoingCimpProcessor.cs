﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Cimp
{
    public class OutgoingCimpProcessor : IDisposable
    {
        private readonly string _connectionString;
        private readonly string _globalConnectionString;

        public OutgoingCimpProcessor(string connection, string globalConnection = "")
        {
            _connectionString = connection;
            _globalConnectionString = globalConnection;
        }

        public void ProcessOutgoingCimp()
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "ProcessCIMPOutbound";

                        command.Connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader["ErrorMessage"] != DBNull.Value)
                                {
                                    throw new ApplicationException(reader["ErrorMessage"].ToString());
                                }
                                else
                                {
                                    InsertMessageLog(reader["CarrMB"].ToString(), reader["RawMsg"].ToString(), reader["SenderMB"].ToString(), reader["PriorityCode"].ToString(), reader["MessageType"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Processing CIMP failed: " + ex.ToString());
            }
        }

        private void InsertMessageLog(string carrMB, string rawMsg, string senderMB, string priorityCode, string messageType)
        {
            using (SqlConnection conn = new SqlConnection(_globalConnectionString))
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    comm.CommandText = "Insert Into MCHCoreDB.dbo.CIMPMessageLogs (Timestamp,FileName,MessageType,MailBoxName,RawMessage,ProcessingStatus,DownloadedTime, SenderMailBox,Direction,Priority) VAlues (getutcdate(),'',@MessageType,@CarrMB,@RawMsg,0,getutcdate(),@SenderMB,'OUT', @PriorityCode)";
                    comm.CommandType = CommandType.Text;
                    comm.Connection = conn;
                    comm.Parameters.Add(new SqlParameter("@CarrMB", carrMB));
                    comm.Parameters.Add(new SqlParameter("@RawMsg", rawMsg));
                    comm.Parameters.Add(new SqlParameter("@SenderMB", senderMB));
                    comm.Parameters.Add(new SqlParameter("@PriorityCode", priorityCode));
                    comm.Parameters.Add(new SqlParameter("@MessageType", messageType));
                    comm.Connection.Open();
                    comm.ExecuteNonQuery();
                    comm.Connection.Close();
                }
            }
        }

        public void Dispose()
        {

        }
    }
}
