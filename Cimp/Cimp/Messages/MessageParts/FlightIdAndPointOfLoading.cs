﻿using System;

namespace Cimp.Messages.MessageParts
{
    public class FlightIdAndPointOfLoading
    {        
        private readonly DateTimeInfo _loadingDateTimeInfo;
        private readonly DateTimeInfo _arrivalDateTimeInfo;        
        private readonly DateTimeInfo _scheduleOfUnloadingDeparture;

        public FlightIdAndPointOfLoading(string line1, string line2)
        {
            var parts1 = line1.SplitLines(Constants.SlantSeparator);

            MessageSequenceNumber = parts1.GetValueOfIndex<int>(0);
            CarrierCode = parts1.GetValueOfIndex(1).Substring<string>(0, 2);
            FlightNumber = parts1.GetValueOfIndex(1).Substring<string>(2);
            _loadingDateTimeInfo = parts1.GetValueOfIndex(2) == null ? null : new DateTimeInfo(parts1.GetValueOfIndex(2));
            AirportCodeOfLoading = parts1.GetValueOfIndex(3);
            AircraftRegistration = parts1.GetValueOfIndex(4);
            IsoCountryCode = parts1.GetValueOfIndex(5);
            _arrivalDateTimeInfo = parts1.GetValueOfIndex(6) == null ? null : new DateTimeInfo(parts1.GetValueOfIndex(6));
            AirportCodeOfArrival = parts1.GetValueOfIndex(7);

            var parts2 = line2.SplitLines(Constants.SlantSeparator);            
            _scheduleOfUnloadingDeparture = parts2.GetValueOfIndex<string>(3) == null ? null : new DateTimeInfo(parts2.GetValueOfIndex(3));
        }
        
        public int MessageSequenceNumber { get; private set; }
        public string CarrierCode { get; private set; }
        public string FlightNumber { get; private set; }
        public string AirportCodeOfLoading { get; private set; }
        public string AircraftRegistration { get; private set; }
        public string IsoCountryCode { get; private set; }
        public string AirportCodeOfArrival { get; private set; }
        public bool IsComplete { get; set; }
        
        public DateTime? ScheduleOfLoading
        {
            get { return DateTimeInfo.ConvertToDate(_loadingDateTimeInfo, _scheduleOfUnloadingDeparture); }
        }

        public DateTime? ScheduleOfArrival
        {
            get { return DateTimeInfo.ConvertToDate(_arrivalDateTimeInfo, _scheduleOfUnloadingDeparture); }
        }        
    }
}