﻿using System;

namespace Cimp.Messages.MessageParts
{
    public class MovementInformation
    {
        private readonly DateTimeInfo _dateTimeInfo;

        public MovementInformation()
        {
        }

        public MovementInformation(string line)            
        {
            var parts = line.SplitLines(new[] { @"/" });
            AirportOfNextDestination = parts.GetValueOfIndex(1).Substring<string>(0, 3);
            CarrierCode = parts.GetValueOfIndex(1).Substring<string>(3, 2);
            FlightNumber = parts.GetValueOfIndex(1).Substring<string>(5);
            _dateTimeInfo = new DateTimeInfo(parts.GetValueOfIndex(2));            
        }

        public string AirportOfNextDestination { get; set; }
        public string CarrierCode { get; set; }
        public string FlightNumber { get; set; }        
        public DateTime? Date
        {
            get { return _dateTimeInfo.GetDate(); }
            set { }
        }
    }
}