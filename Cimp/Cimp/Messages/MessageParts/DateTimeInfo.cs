﻿using System;
using System.Globalization;

namespace Cimp.Messages.MessageParts
{
    public class DateTimeInfo
    {
        public DateTimeInfo(string line)
        {
            if(string.IsNullOrEmpty(line)) return;

            Day = line.Substring<int>(0, 2);
            Month = line.Substring<string>(2, 3);
            Time = line.Substring<string>(5);
            if (string.IsNullOrEmpty(Time)) return;
            
            if (Time.Contains(":"))
            {
                Hour = Time.SplitLines(new[] {":"}).GetValueOfIndex(0);
                Minute = Time.SplitLines(new[] {":"}).GetValueOfIndex(1);
            }
            else
            {
                Hour = Time.Substring(0, 2);
                Minute = Time.Substring(2, 2);
            }
        }

        public int Day { get; private set; }
        public string Month { get; private set; }
        public string Time { get; private set; }
        public string Hour { get; private set; }
        public string Minute { get; private set; }

        public DateTime? GetDate()
        {            
            DateTime result;
            if (DateTime.TryParse(Month + "/" + Day + "/" + DateTime.Now.Year + " " + Time,
                CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
            {
                return result;
            }

            return null;
        }

        public static DateTime? ConvertToDate(DateTimeInfo dateTimeInfo, DateTimeInfo monthScheduleOfDeparture)
        {
            if (dateTimeInfo == null) return null;

            DateTime result;
            var year = DateTime.Now.Year;
            if (dateTimeInfo.Month == "DEC" && monthScheduleOfDeparture != null && monthScheduleOfDeparture.Month == "JAN")
                year = DateTime.Now.Year + 1;

            if (DateTime.TryParse(dateTimeInfo.Month + "/" + dateTimeInfo.Day + "/" + year + " " + (string.IsNullOrEmpty(dateTimeInfo.Time) ? "" : (dateTimeInfo.Time.Contains(":") ? dateTimeInfo.Time : (dateTimeInfo.Time.Substring(0, 2) + ":" + dateTimeInfo.Time.Substring(2, 2)))),
                CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
            {
                return result;
            }
            return null;
        }
    }
}