﻿using System.Collections.Generic;
using System.Linq;

namespace Cimp.Messages.MessageParts.Fhl
{
    public class HouseWaybillSummaryDetails
    {
        private readonly SpecialHandlingCode _specialHandlingCode;

        public HouseWaybillSummaryDetails()
        {
        }

        public HouseWaybillSummaryDetails(string line)
        {
            var parts = line.SplitLines(Constants.SlantSeparator);
            SerialNumber = parts.GetValueOfIndex(1);
            AirportOfOrigin = parts.GetValueOfIndex(2).Substring<string>(0, 3);
            AirportOfDestination = parts.GetValueOfIndex(2).Substring<string>(3, 3);
            NumberOfPieces = parts.GetValueOfIndex(3).Substring<int>(0);
            WeightCode = parts.GetValueOfIndex(4).Substring<string>(0, 1);
            Weight = parts.GetValueOfIndex(4).Substring<decimal>(1);
            Slac = TryGetSlac(parts.GetValueOfIndex(5));
            DescriptionOfGoods = Slac.HasValue ? parts.GetValueOfIndex(6) : parts.GetValueOfIndex(5);
            _specialHandlingCode = new SpecialHandlingCode();
            //SpecialHandlingCodes = new SpecialHandlingCode();
            if((Slac.HasValue && parts.Count > 7) || (!Slac.HasValue && parts.Count > 6))
            {                                    
                _specialHandlingCode.AddRange(Slac.HasValue ? parts.Skip(7) : parts.Skip(6));
            }

            Descriptions = new List<DescriptionOfGoods>();
            Tarifs = new List<TarifInformation>();
            CustomsInformations = new List<CustomsInformation>();
            ChargeDeclaration = new ChargeDeclaration();
        }

        public string SerialNumber { get; set; }
        public string AirportOfOrigin { get; set; }
        public string AirportOfDestination { get; set; }
        public int NumberOfPieces { get; set; }
        public string WeightCode { get; set; }
        public decimal Weight { get; set; }
        public int? Slac { get; set; }
        public string DescriptionOfGoods { get; set; }
        public SpecialHandlingCode SpecialHandlingCodes
        {
            get { return _specialHandlingCode; }
            set { }
        }
        public List<DescriptionOfGoods> Descriptions { get; private set; }
        public List<TarifInformation> Tarifs { get; private set; }
        public List<CustomsInformation> CustomsInformations { get; private set; }
        public ChargeDeclaration ChargeDeclaration { get; set; }
        public Vendor Shipper { get; set; }
        public Vendor Consignee { get; set; }

        private static int? TryGetSlac(string part)
        {
            int result;
            if(int.TryParse(part, out result))            
                return result;
            return null;
        }
    }
}
