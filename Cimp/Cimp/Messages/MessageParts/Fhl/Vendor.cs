﻿using System.Collections.Generic;
using System.Linq;

namespace Cimp.Messages.MessageParts.Fhl
{
    public class Vendor
    {
        public Vendor()
        {
        }

        public Vendor(string line, string streetLine, string placeLine, string isoLine)
        {
            Name = line.Substring(4);
            StreetAddress = streetLine.Substring(1);
            var placeParts = placeLine.SplitLines(Constants.SlantSeparator);
            Place = placeParts.GetValueOfIndex(1);
            Province = placeParts.GetValueOfIndex(2);
            ContactDetails = new List<ContactDetail>();
            if (isoLine == null) return;

            var isoParts = isoLine.SplitLines(Constants.SlantSeparator);
            IsoCountryCode = isoParts.GetValueOfIndex(1);
            if(IsEven(isoParts))
            {
                PopulateDetails(isoParts.Skip(2).ToList());
            }
            else
            {
                PostCode = isoParts.GetValueOfIndex(2);
                PopulateDetails(isoParts.Skip(3).ToList());
            }
        }

        public string Name { get; set; }
        public string StreetAddress { get; set; }
        public string Place { get; set; }
        public string Province { get; set; }
        public string IsoCountryCode { get; set; }
        public string PostCode { get; set; }
        public List<ContactDetail> ContactDetails { get; private set; }

        private void PopulateDetails(IList<string> parts)
        {
            for(var i = 0; i < parts.Count() - 1; i = i + 2)            
                ContactDetails.Add(new ContactDetail(parts.GetValueOfIndex(i), parts.GetValueOfIndex(i + 1)));            
        }

        private static bool IsEven(IEnumerable<string> parts)
        {
            return parts.Count() % 2 == 0;
        }
    }
}
