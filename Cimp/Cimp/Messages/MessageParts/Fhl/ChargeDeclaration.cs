﻿namespace Cimp.Messages.MessageParts.Fhl
{
    public class ChargeDeclaration
    {
        public ChargeDeclaration()
        {
        }

        public ChargeDeclaration(string line)
        {
            var parts = line.SplitLines(Constants.SlantSeparator);
            IsoCurrencyCode = parts.GetValueOfIndex(1);
            WeightValuationCharges = parts.GetValueOfIndex(2).Substring<string>(0, 1);
            OtherCharges = parts.GetValueOfIndex(2).Substring<string>(1);
            DeclaredValueforCarriage = parts.GetValueOfIndex<string>(3);
            DeclaredValueforCustoms = parts.GetValueOfIndex<string>(4);
            DeclaredValueforInsurance = parts.GetValueOfIndex<string>(5);
        }

        public string IsoCurrencyCode { get; set; }
        public string WeightValuationCharges { get; set; }
        public string OtherCharges { get; set; }
        public string DeclaredValueforCarriage { get; set; }
        public string DeclaredValueforCustoms { get; set; }
        public string DeclaredValueforInsurance { get; set; }
    }
}