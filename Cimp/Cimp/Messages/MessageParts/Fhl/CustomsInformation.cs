﻿namespace Cimp.Messages.MessageParts.Fhl
{
    public class CustomsInformation
    {
        public CustomsInformation()
        {
        }

        public CustomsInformation(string line)
        {
            var parts = line.SplitLines(Constants.SlantSeparator);
            IsoCountryCode = parts.GetValueOfIndex(1);
            InformationIdentifier = parts.GetValueOfIndex(2);
            CustomsInformationIdentifier = parts.GetValueOfIndex(3);
            SuplementaryCustomsInformation = parts.GetValueOfIndex(4);
        }

        public string IsoCountryCode { get; set; }
        public string InformationIdentifier { get; set; }
        public string CustomsInformationIdentifier { get; set; }
        public string SuplementaryCustomsInformation { get; set; }
    }
}