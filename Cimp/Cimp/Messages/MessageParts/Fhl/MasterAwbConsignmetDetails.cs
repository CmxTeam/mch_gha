﻿using System.Collections.Generic;

namespace Cimp.Messages.MessageParts.Fhl
{
    public class MasterAwbConsignmetDetails
    {
        public MasterAwbConsignmetDetails()
        {

        }

        public MasterAwbConsignmetDetails(string line)
        {
            var shiftResult = 0;
            var parts = line.SplitLines(Constants.SlantSeparator);
            AirlinePrefix = parts.GetValueOfIndex(1).Substring<string>(0, 3);
            AwbSerialNumber = parts.GetValueOfIndex(1).Substring<int>(4, 8, ref shiftResult);
            AirportOfOrigin = parts.GetValueOfIndex(1).Substring<string>(12 - shiftResult, 3);
            AirportOfDestination = parts.GetValueOfIndex(1).Substring<string>(15 - shiftResult, 3);
            shiftResult = 0;
            ShipmentDescriptinOfCode = parts.GetValueOfIndex(2).Substring<string>(0, 1);
            NumberOfPieces = parts.GetValueOfIndex(2).Substring<int>(1, 4, ref shiftResult);
            WeightCode = parts.GetValueOfIndex(2).Substring<string>(5 - shiftResult, 1);
            Weight = parts.GetValueOfIndex(2).Substring<decimal>(6 - shiftResult);
            HouseWaybillSummaryDetails = new List<HouseWaybillSummaryDetails>();
        }

        public string AirlinePrefix { get; set; }
        public int AwbSerialNumber { get; set; }
        public string AirportOfOrigin { get; set; }
        public string AirportOfDestination { get; set; }
        public string ShipmentDescriptinOfCode { get; set; }
        public int NumberOfPieces { get; set; }
        public string WeightCode { get; set; }
        public decimal Weight { get; set; }

        public List<HouseWaybillSummaryDetails> HouseWaybillSummaryDetails { get; private set; }
    }
}
