﻿namespace Cimp.Messages.MessageParts.Fhl
{
    public class DescriptionOfGoods
    {
        public DescriptionOfGoods()
        {
        }

        public DescriptionOfGoods(string line)
        {
            Description = line.Substring(4);
        }

        public string Description { get; set; }
    }
}