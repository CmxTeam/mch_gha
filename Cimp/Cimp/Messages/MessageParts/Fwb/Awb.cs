﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cimp.Messages.MessageParts.Fwb
{
    public class Awb
    {
        public Awb()
        {
            LegSegments = new List<LegSegment>();
        }
        public Awb(string line)
        {
            var shiftResult = 0;
            var parts = line.SplitLines(new[] { @"/" });
            AirlinePrefix = parts.GetValueOfIndex(0).Substring<string>(0, 3);
            AwbSerialNumber = parts.GetValueOfIndex(0).Substring<string>(4, 8);
            AirportOfOrigin = parts.GetValueOfIndex(0).Substring<string>(12, 3);
            AirportOfDestination = parts.GetValueOfIndex(0).Substring<string>(15, 3);
            QuantityShipmentDescriptionCode = parts.GetValueOfIndex(1).Substring<string>(0, 1);
            QuantityNummberOfPieces = parts.GetValueOfIndex(1).Substring<int>(1, 4, ref shiftResult);
            WeightCode = parts.GetValueOfIndex(1).Substring<string>(5 - shiftResult, 1);
            Weight = parts.GetValueOfIndex(1).Substring<decimal>(6 - shiftResult, 9, ref shiftResult);
            var volumeDencityIndicator = parts.GetValueOfIndex(1).Substring<string>(15 - shiftResult, 2);
            if (volumeDencityIndicator == "DG")
            {
                DencityIndicator = 1;
                DencityGroup = parts.GetValueOfIndex(1).Substring<int>(17 - shiftResult, 2, ref shiftResult);
            }
            else
            {
                DencityIndicator = 0;
                VolumeCode = volumeDencityIndicator;
                VolumeAmount = parts.GetValueOfIndex(1).Substring<decimal>(17 - shiftResult, 12, ref shiftResult);
            }

            LegSegments = new List<LegSegment>();
            SpecialHandlingCodes = new SpecialHandlingCode();
        }


        public string AirlinePrefix { get; set; }
        public string AwbSerialNumber { get; set; }
        public string AirportOfOrigin { get; set; }
        public string AirportOfDestination { get; set; }
        public string QuantityShipmentDescriptionCode { get; set; }
        public int QuantityNummberOfPieces { get; set; }
        public string WeightCode { get; set; }
        public decimal Weight { get; set; }
        public string VolumeCode { get; set; }
        public decimal VolumeAmount { get; set; }
        public int DencityIndicator { get; set; }
        public int DencityGroup { get; set; }
        public string SpecialServiceRequest1 { get; set; }
        public string SpecialServiceRequest2 { get; set; }
        public string SpecialServiceRequest3 { get; set; }

        public string OtherServiceInformation1 { get; set; }
        public string OtherServiceInformation2 { get; set; }
        public string OtherServiceInformation3 { get; set; }
        public SpecialHandlingCode SpecialHandlingCodes { get; set; }

        public Partner Shipper { get; set; }
        public Partner Consignee { get; set; }
        public Partner AlsoNotify { get; set; }
        public Agent AwbAgent { get; set; }

        public List<LegSegment> LegSegments { get; set; }
        
        public void TryAddSpecialServicesRequests(List<string> lines)
        {
            string line = lines[0];
            SpecialServiceRequest1 = line.SplitLines(new[] {@"/"}).GetValueOfIndex(1);

            if (lines.Count > 1)
            {
                line = lines[1];
                SpecialServiceRequest2 = line.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            }
            if (lines.Count > 2)
            {
                line = lines[2];
                SpecialServiceRequest3 = line.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            }
        }

        public void TryAddOtherServiceInformations(List<string> lines)
        {
            string line = lines[0];
            OtherServiceInformation1 = line.SplitLines(new[] { @"/" }).GetValueOfIndex(1);

            if (lines.Count > 1)
            {
                line = lines[1];
                OtherServiceInformation2 = line.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            }
            if (lines.Count > 2)
            {
                line = lines[2];
                OtherServiceInformation3 = line.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            }
        }

        public void TryAddSpecialHandlingCodes(string line)
        {
            var parts = line.SplitLines(new[] {@"/"});
            parts.RemoveAt(0);
            SpecialHandlingCodes.AddRange(parts);
        }

    }
}
