﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Cimp.Messages.MessageParts.Fwb
{
    public class FlightBooking
    {
        public string CarrierCode { get; set; }
        public string FlightNumber { get; set; }
        public string Day { get; set; }

        public static List<FlightBooking> GetFlightBookings(string line)
        {
            var retList = new List<FlightBooking>();
            var parts = line.SplitLines(new[] { @"/" });
            for (var i = 1; i < parts.Count; i = i + 2)
            {
                var tmp = new FlightBooking
                          {
                              CarrierCode = parts.GetValueOfIndex(i).Substring<string>(0, 2),
                              FlightNumber = parts.GetValueOfIndex(i).Substring<string>(3),
                              Day = parts.GetValueOfIndex(i + 1)
                          };
                retList.Add(tmp);
            }
            return retList;
        }
    }
}
