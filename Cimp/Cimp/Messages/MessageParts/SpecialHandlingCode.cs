﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Cimp.Messages.MessageParts
{
    public class SpecialHandlingCode
    {
        private readonly List<string> _codes;
        
        public SpecialHandlingCode()
        {
            _codes = new List<string>();
        }
        
        public List<string> Codes
        {
            get { return _codes.Where(c => !string.IsNullOrWhiteSpace(c)).ToList(); }
            set { }
        }

        public void AddRange(IEnumerable<string> codes)
        {
            _codes.AddRange(codes);
        }

        public void AddLine(string line)
        {
            _codes.AddRange(line.Split(new[] { "/" }, StringSplitOptions.None));
        }
    }
}