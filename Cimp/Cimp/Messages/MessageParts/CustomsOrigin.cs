﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cimp.Messages.MessageParts
{
    public class CustomsOrigin
    {
        public CustomsOrigin()
        {

        }

        public string LineIdentifier { get { return "COR"; } }
        public string CustomsOriginCode { get; private set; }
    }
}
