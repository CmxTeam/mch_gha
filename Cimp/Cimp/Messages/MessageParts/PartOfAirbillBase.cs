﻿namespace Cimp.Messages.MessageParts
{
    public abstract class PartOfAirbillBase
    {
        protected PartOfAirbillBase()
        {

        }

        protected PartOfAirbillBase(string airbillPrefix, string awbSerialNumber)
        {
            AirbillPrefix = airbillPrefix;
            AwbSerialNumber = awbSerialNumber;
        }

        public string AirbillPrefix { get; set; }
        public string AwbSerialNumber { get; set; }
    }
}