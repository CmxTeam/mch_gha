﻿namespace Cimp.Messages.MessageParts.Fsn
{
    public class Awb
    {
        public Awb(string airportLine, string awbLine)
        {
            AirportOfArrival = airportLine.Substring<string>(0, 3);
            CargoTerminalOperator = airportLine.Substring<string>(3);
            var parts = awbLine.SplitLines(Constants.Hyphen);
            AirWaybillPrefix = parts.GetValueOfIndex(0);
            AwbSerialNumber = parts.GetValueOfIndex(1);
            if (parts.Count <= 2) return;

            var hawbParts = parts.GetValueOfIndex(2).SplitLines(Constants.SlantSeparator);
            HawbNumber = hawbParts.GetValueOfIndex(0);
            PackageTrackingIdentifier = hawbParts.GetValueOfIndex(1);
        }

        public void SetArrival(string line)
        {
            var parts = line.SplitLines(Constants.SlantSeparator);
            var carrierFlight = parts.GetValueOfIndex(1);
            var index = char.IsNumber(carrierFlight[2]) ? 2 : 3;
            ImportingCarrier = carrierFlight.Substring(0, index);
            FlightNumber = carrierFlight.Substring(index);
            var dateParts = parts.GetValueOfIndex(2).SplitLines(Constants.Hyphen);
            ScheduledArrivalDate = new DateTimeInfo(dateParts.GetValueOfIndex(0));
            PartArrivalReference = dateParts.GetValueOfIndex(1);
        }

        public void SetAirlineStatusNotification(string line)
        {
            Status = line.Substring(3, 1);
            Explanation = line.Substring(4);
        }

        public void SetCbpStatusNotification(string line)
        {
            var parts = line.SplitLines(Constants.SlantSeparator);
            ActionCode = parts.GetValueOfIndex(1).SplitLines(Constants.Hyphen).GetValueOfIndex(0);
            NumberOfPieces = parts.GetValueOfIndex(1).SplitLines(Constants.Hyphen).GetValueOfIndex<int>(1);
            TransactionDate = new DateTimeInfo(parts.GetValueOfIndex(2));
            if(parts.Count <= 3) return;
            EntryType = parts.GetValueOfIndex(3).Substring<string>(0, 2);
            EntryNumber = parts.GetValueOfIndex(3).Substring<string>(2);
            Remarks = parts.GetValueOfIndex(4);
        }

        #region [ -- Properties-- ]

        public string CargoTerminalOperator { get; private set; }
        public string AirportOfArrival { get; private set; }
        public string AirWaybillPrefix { get; private set; }
        public string AwbSerialNumber { get; private set; }
        public string HawbNumber { get; private set; }
        public string PackageTrackingIdentifier { get; private set; }
        public string ImportingCarrier { get; private set; }
        public string FlightNumber { get; private set; }
        public DateTimeInfo ScheduledArrivalDate { get; private set; }
        public string PartArrivalReference { get; private set; }
        public string Status { get; private set; }
        public string Explanation { get; private set; }
        public string ActionCode { get; private set; }
        public int NumberOfPieces { get; private set; }
        public DateTimeInfo TransactionDate { get; private set; }
        public string EntryType { get; private set; }
        public string EntryNumber { get; private set; }
        public string Remarks { get; private set; }

        #endregion
    }
}
