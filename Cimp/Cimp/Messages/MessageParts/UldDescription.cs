﻿using System.Collections.Generic;

namespace Cimp.Messages.MessageParts
{
    public class UldDescription
    {
        private string _volumeAvailableCode;
        
        public UldDescription()
        {
            Loose = true;
            ConsignmentDetails = new List<ConsignmentDetail>();
        }

        public UldDescription(string line)
        {
            var parts = line.SplitLines(new[] { @"/" });
            Type = parts.GetValueOfIndex(1).Substring<string>(0, 3);
            SerialNumber = parts.GetValueOfIndex(1).Substring<string>(3, 5);
            OwnerCode = parts.GetValueOfIndex(1).Substring<string>(8, 2);
            LoadingIndicator = parts.GetValueOfIndex(1).Substring<string>(11, 1);
            Remarks = parts.GetValueOfIndex(2);
            ConsignmentDetails = new List<ConsignmentDetail>();
            MovementInfos = new List<MovementInformation>();            
            Loose = false;
        }
        
        public string Type { get;  set; }
        public string SerialNumber { get; set; }
        public string OwnerCode { get;  set; }
        public string LoadingIndicator { get; set; }
        public string Remarks { get; set; }

        public string VolumeAvailableCode
        {
            get { return _volumeAvailableCode; }
            set
            {
                if (value == null) return;
                _volumeAvailableCode = value.TrimStart('/');
            }
        }

        public bool Loose { get;  set; }
        public List<ConsignmentDetail> ConsignmentDetails { get; private set; }
        public List<MovementInformation> MovementInfos { get; private set; }        
    }
}