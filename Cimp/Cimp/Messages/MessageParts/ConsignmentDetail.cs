﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cimp.Messages.MessageParts.Fhl;

namespace Cimp.Messages.MessageParts
{
    public class ConsignmentDetail
    {
        #region [ -- Private Fields -- ]

        private string _otherServiceInfo1;
        private string _otherServiceInfo2;
        private string _customsOrigin;
        private string _customsReference;
        private string _movementPriorityCode;
        private string _nilCargoCode;
        private DateTimeInfo _scheduleOfUnloadingArrival;
        private DateTimeInfo _scheduleOfUnloadingDeparture;

        #endregion

        #region [ -- Constructors -- ]

        public ConsignmentDetail()
        {

        }

        public ConsignmentDetail(string line, string uldSerialNumber, string unloadingPortInfo)
            : this(line, unloadingPortInfo)
        {
            UldSerialNumber = uldSerialNumber;
        }

        public ConsignmentDetail(string line, string unloadingPortInfo)
        {
            PopulateUnloadingPortInfo(unloadingPortInfo);
            var shiftResult = 0;
            var parts = line.SplitLines(new[] { @"/" });
            AirlinePrefix = parts.GetValueOfIndex(0).Substring<string>(0, 3);
            AwbSerialNumber = parts.GetValueOfIndex(0).Substring<string>(4, 8);
            AirportOfOrigin = parts.GetValueOfIndex(0).Substring<string>(12, 3);
            AirportOfDestination = parts.GetValueOfIndex(0).Substring<string>(15, 3);
            QuantityShipmentDescriptionCode = parts.GetValueOfIndex(1).Substring<string>(0, 1);
            QuantityNummberOfPieces = parts.GetValueOfIndex(1).Substring<int>(1, 4, ref shiftResult);
            WeightCode = parts.GetValueOfIndex(1).Substring<string>(5 - shiftResult, 1);
            Weight = parts.GetValueOfIndex(1).Substring<decimal>(6 - shiftResult, 9, ref shiftResult);

            var volumeDencityIndicator = parts.GetValueOfIndex(1).Substring<string>(15 - shiftResult, 2);
            if (volumeDencityIndicator == "DG")
            {
                DencityIndicator = 1;
                DencityGroup = parts.GetValueOfIndex(1).Substring<int>(17 - shiftResult, 2, ref shiftResult);
                shiftResult += 10;
            }
            else
            {
                DencityIndicator = 0;
                VolumeCode = volumeDencityIndicator;
                VolumeAmount = parts.GetValueOfIndex(1).Substring<decimal>(17 - shiftResult, 12, ref shiftResult);
            }

            if (QuantityShipmentDescriptionCode != "T")
            {
                TotalShipmentDescriptionCode = parts.GetValueOfIndex(1).Substring<string>(29 - shiftResult, 1);
                TotalNummberOfPieces = parts.GetValueOfIndex(1).Substring<int>(30 - shiftResult, 4, ref shiftResult);
            }
            else
            {
                TotalShipmentDescriptionCode = QuantityShipmentDescriptionCode;
                TotalNummberOfPieces = QuantityNummberOfPieces;
            }
            ManifestDescriptionOfGoods = parts.GetValueOfIndex(2);
            MovementInfos = new List<MovementInformation>();
            DimensionsInformations = new List<DimensionsInformation>();
            OtherCustomsInformations = new List<CustomsInformation>();
        }

        #endregion

        public string UldSerialNumber { get; set; }
        public string AirlinePrefix { get; set; }
        public string AwbSerialNumber { get; set; }
        public string AirportOfOrigin { get; set; }
        public string AirportOfDestination { get; set; }
        public string QuantityShipmentDescriptionCode { get; set; }
        public int QuantityNummberOfPieces { get; set; }
        public string WeightCode { get; set; }
        public decimal Weight { get; set; }
        public string VolumeCode { get; set; }
        public decimal VolumeAmount { get; set; }
        public int DencityIndicator { get; set; }
        public int DencityGroup { get; set; }
        public string TotalShipmentDescriptionCode { get; set; }
        public int TotalNummberOfPieces { get; set; }
        public string ManifestDescriptionOfGoods { get; set; }

        public string OtherServiceInfo1
        {
            get { return _otherServiceInfo1; }
            set
            {
                if (value == null) return;
                _otherServiceInfo1 = value.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            }
        }

        public string OtherServiceInfo2
        {
            get { return _otherServiceInfo2; }
            set
            {
                if (value == null) return;
                _otherServiceInfo2 = value.TrimStart('/');
            }
        }

        public string CustomsOrigin
        {
            get { return _customsOrigin; }
            set
            {
                if (value == null) return;
                _customsOrigin = value.SplitLines(new[] { @"/" }).Last();
            }
        }

        public string CustomsReference
        {
            get { return _customsReference; }
            set
            {
                if (value == null) return;
                _customsReference = value.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            }
        }

        public string MovementPriorityCode
        {
            get { return _movementPriorityCode; }
            set
            {
                if (value == null) return;
                _movementPriorityCode = value.TrimStart('/');
            }
        }

        public string UnloadingAirportCode { get; set; }

        public int IsNilCargo
        {
            get
            {
                return _nilCargoCode == "NIL" ? 1 : 0;
            }
            set { /* requires for serialization */}
        }

        public DateTime? ScheduleOfUnloadingArrival
        {
            get { return _scheduleOfUnloadingArrival == null ? null : _scheduleOfUnloadingArrival.GetDate(); }
            set { /* requires for serialization */}
        }

        public DateTime? ScheduleOfUnloadingDeparture
        {
            get { return _scheduleOfUnloadingDeparture == null ? null : _scheduleOfUnloadingDeparture.GetDate(); }
            set { /* requires for serialization */}
        }

        public SpecialHandlingCode SpecialHandlingCodes { get; set; }
        public List<MovementInformation> MovementInfos { get; private set; }
        public List<DimensionsInformation> DimensionsInformations { get; private set; }
        public List<CustomsInformation> OtherCustomsInformations { get; private set; }

        private void PopulateUnloadingPortInfo(string unloadingPortInfo)
        {
            var parts = unloadingPortInfo.SplitLines(new[] { @"/" });
            UnloadingAirportCode = parts.GetValueOfIndex(0);
            _nilCargoCode = parts.GetValueOfIndex(1);
            _scheduleOfUnloadingArrival = parts.GetValueOfIndex<string>(2) == null ? null : new DateTimeInfo(parts.GetValueOfIndex(2));
            _scheduleOfUnloadingDeparture =  parts.GetValueOfIndex<string>(3) == null ? null : new DateTimeInfo(parts.GetValueOfIndex(3));
        }
    }
}
