﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cimp.Messages.MessageParts.Fhl;

namespace Cimp.Messages
{
    public class Fhl : Message
    {
        private readonly MasterAwbConsignmetDetails _masterAwbConsignmetDetails;

        public Fhl(string messageContent, string connectionString, string senderMailBox)
            : base(messageContent, connectionString, senderMailBox)
        {
            try
            {
                _masterAwbConsignmetDetails = new MasterAwbConsignmetDetails(Content[1]);
                InitializeDetails(Content.Skip(2));
                IsInitialisedSuccessfully = true;
            }
            catch (Exception e)
            {
                IsInitialisedSuccessfully = false;
                InitialiserErrorMessage = e.Message;
            }
        }

        private void InitializeDetails(IEnumerable<string> lines)
        {
            var index = 0;
            foreach (var line in lines)
            {
                if (line.StartsWith("/"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("HBS"))
                {
                    var hbs = new HouseWaybillSummaryDetails(line);
                    var specialHandlingCode = TryFindSpecialHandlingCodes(index, lines);
                    if (specialHandlingCode != null)
                        hbs.SpecialHandlingCodes.AddLine(specialHandlingCode);
                    _masterAwbConsignmetDetails.HouseWaybillSummaryDetails.Add(hbs);

                    ++index;
                    continue;
                }

                if (line.StartsWith("TXT"))
                {
                    _masterAwbConsignmetDetails.HouseWaybillSummaryDetails.Last().Descriptions.Add(new DescriptionOfGoods(line));
                    ++index;
                    continue;
                }

                if (line.StartsWith("HTS"))
                {
                    _masterAwbConsignmetDetails.HouseWaybillSummaryDetails.Last().Tarifs.Add(new TarifInformation(line));
                    ++index;
                    continue;
                }

                if (line.StartsWith("OCI"))
                {
                    _masterAwbConsignmetDetails.HouseWaybillSummaryDetails.Last().CustomsInformations.Add(new CustomsInformation(line));
                    foreach (var subLine in TryFindDerivativeLines(index, lines))
                        _masterAwbConsignmetDetails.HouseWaybillSummaryDetails.Last().CustomsInformations.Add(new CustomsInformation(subLine));
                    ++index;
                    continue;
                }

                if (line.StartsWith("SHP"))
                {
                    var subLines = TryFindDerivativeLines(index, lines).ToList();
                    _masterAwbConsignmetDetails.HouseWaybillSummaryDetails.Last().Shipper = new Vendor(line, subLines[0], subLines[1], subLines.Count > 2 ? subLines[2] : null);
                    ++index;
                    continue;
                }

                if (line.StartsWith("CNE"))
                {
                    var subLines = TryFindDerivativeLines(index, lines).ToList();
                    _masterAwbConsignmetDetails.HouseWaybillSummaryDetails.Last().Consignee = new Vendor(line, subLines[0], subLines[1], subLines.Count > 2 ? subLines[2] : null);
                    ++index;
                    continue;
                }

                if (line.StartsWith("CVD"))
                {
                    _masterAwbConsignmetDetails.HouseWaybillSummaryDetails.Last().ChargeDeclaration = new ChargeDeclaration(line);
                    ++index;
                    continue;
                }
            }
        }

        protected override string StoredProcedureName
        {
            get { return "ProcessFhl"; }
        }

        protected override IEnumerable<KeyValuePair<string, object>> Parameters
        {
            get
            {
                yield return new KeyValuePair<string, object>("@AirlinePrefix", _masterAwbConsignmetDetails.AirlinePrefix);
                yield return new KeyValuePair<string, object>("@AwbSerialNumber", _masterAwbConsignmetDetails.AwbSerialNumber);
                yield return new KeyValuePair<string, object>("@AirportOfOrigin", _masterAwbConsignmetDetails.AirportOfOrigin);
                yield return new KeyValuePair<string, object>("@AirportOfDestination", _masterAwbConsignmetDetails.AirportOfDestination);
                yield return new KeyValuePair<string, object>("@ShipmentDescriptinOfCode", _masterAwbConsignmetDetails.ShipmentDescriptinOfCode);
                yield return new KeyValuePair<string, object>("@NumberOfPieces", _masterAwbConsignmetDetails.NumberOfPieces);
                yield return new KeyValuePair<string, object>("@Weight", _masterAwbConsignmetDetails.Weight);
                yield return new KeyValuePair<string, object>("@WeightCode", _masterAwbConsignmetDetails.WeightCode);
                yield return new KeyValuePair<string, object>("@SenderMailbox", SenderMailBox);
                yield return new KeyValuePair<string, object>("@XmlContent", ConvertToXml<HouseWaybillSummaryDetails>(_masterAwbConsignmetDetails.HouseWaybillSummaryDetails));
            }
        }

        private static string TryFindSpecialHandlingCodes(int index, IEnumerable<string> lines)
        {
            var result = lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/")).FirstOrDefault();
            if (result == null) return null;

            var parts = result.Split('/').Skip(1);
            if (parts.Count() <= 9 && parts.All(p => p.Length == 3))
                return result;

            return null;
        }

        private static IEnumerable<string> TryFindDerivativeLines(int index, IEnumerable<string> lines)
        {
            return lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/"));
        }
    }
}
