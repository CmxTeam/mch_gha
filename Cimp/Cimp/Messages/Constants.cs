﻿namespace Cimp.Messages
{
    public static class Constants
    {
        public static readonly string[] SlantSeparator = new[] { @"/" };
        public static readonly string[] Hyphen = new[] { @"-" };
    }
}