﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cimp.Messages.MessageParts.Fsn;

namespace Cimp.Messages
{
    public class Fsn : Message
    {
        private readonly Awb _awb;

        public Fsn(string messageContent, string connectionString, string senderMailBox)
            : base(messageContent, connectionString, senderMailBox)
        {
            try
            {
                _awb = new Awb(Content[1], Content[2]);
                InitializeDetails(Content.Skip(3));
                IsInitialisedSuccessfully = true;
            }
            catch (Exception e)
            {
                IsInitialisedSuccessfully = false;
                InitialiserErrorMessage = e.Message;
            }
        }

        private void InitializeDetails(IEnumerable<string> lines)
        {

            foreach (var line in lines)
            {
                if (line.StartsWith("ARR"))
                {
                    _awb.SetArrival(line);
                    continue;
                }

                if (line.StartsWith("CSN"))
                {
                    _awb.SetCbpStatusNotification(line);
                    continue;
                }

                if (line.StartsWith("ASN"))
                {
                    _awb.SetAirlineStatusNotification(line);
                    continue;
                }
            }
        }

        protected override string StoredProcedureName
        {
            get { return "ProcessFsn"; }
        }

        protected override IEnumerable<KeyValuePair<string, object>> Parameters
        {
            get
            {
                yield return new KeyValuePair<string, object>("@CargoTerminalOperator", _awb.CargoTerminalOperator);
                yield return new KeyValuePair<string, object>("@AirportOfArrival", _awb.AirportOfArrival);
                yield return new KeyValuePair<string, object>("@AirWaybillPrefix", _awb.AirWaybillPrefix);
                yield return new KeyValuePair<string, object>("@AwbSerialNumber", _awb.AwbSerialNumber);
                yield return new KeyValuePair<string, object>("@HawbNumber", _awb.HawbNumber ?? string.Empty);
                yield return new KeyValuePair<string, object>("@PackageTrackingIdentifier", _awb.PackageTrackingIdentifier ?? string.Empty);
                yield return new KeyValuePair<string, object>("@ImportingCarrier", _awb.ImportingCarrier);
                yield return new KeyValuePair<string, object>("@FlightNumber", _awb.FlightNumber);
                yield return new KeyValuePair<string, object>("@ScheduledArrivalDay", _awb.ScheduledArrivalDate.Day);
                yield return new KeyValuePair<string, object>("@ScheduledArrivalMonth", _awb.ScheduledArrivalDate.Month);
                yield return new KeyValuePair<string, object>("@PartArrivalReference", _awb.PartArrivalReference);
                yield return new KeyValuePair<string, object>("@Status", _awb.Status);
                yield return new KeyValuePair<string, object>("@Explanation", _awb.Explanation);
                yield return new KeyValuePair<string, object>("@ActionCode", _awb.ActionCode);
                yield return new KeyValuePair<string, object>("@NumberOfPieces", _awb.NumberOfPieces);
                yield return new KeyValuePair<string, object>("@TransactionDay", _awb.TransactionDate.Day);
                yield return new KeyValuePair<string, object>("@TransactionMonth", _awb.TransactionDate.Month);
                yield return new KeyValuePair<string, object>("@TransactionHour", _awb.TransactionDate.Hour);
                yield return new KeyValuePair<string, object>("@TransactionMinute", _awb.TransactionDate.Minute);
                yield return new KeyValuePair<string, object>("@EntryType", _awb.EntryType);
                yield return new KeyValuePair<string, object>("@EntryNumber", _awb.EntryNumber);
                yield return new KeyValuePair<string, object>("@Remarks", _awb.Remarks);
            }
        }
    }
}