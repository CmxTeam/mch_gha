﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cimp.Messages.MessageParts;

namespace Cimp.Messages
{
    public class RawMessage : Message
    {
        private readonly string _receivingMailbox;
        private readonly string _senderMailbox;
        private readonly string _messageType;
        private readonly string _rawMessage;
        private readonly string _fileName;
        private readonly string _priority;

        public RawMessage(string messageContent, string connectionString, string fileName)
            : base(messageContent, connectionString, "")
        {
            if (string.IsNullOrEmpty(Content[0]))
                Content.RemoveAt(0);

            _fileName = fileName;
            _receivingMailbox = FindReceivingMailbox();
            _senderMailbox = FindSenderMailbox();
            _priority = GetPriority();
            _messageType = Content[2].SplitLines(new[] { "/" }).First().Substring(1);

            _rawMessage = string.Join("\n", Content.Skip(2)).TrimStart((char)2).TrimEnd((char)3);
        }

        protected override string StoredProcedureName
        {
            get
            {
                return @"Insert Into CIMPMessageLogs (FileName, RawMessage, ProcessingStatus, DownloadedTime, SentTime, MailboxName, MessageType, SenderMailbox, Direction, Priority) values 
                (@FileName, @RawMessage, @ProcessingStatus, @DownloadedTime, @SentTime, @MailboxName, @MessageType, @SenderMailbox, N'IN', @Priority)";
            }
        }

        public override System.Data.CommandType CommandType
        {
            get
            {
                return System.Data.CommandType.Text;
            }
        }

        protected override IEnumerable<KeyValuePair<string, object>> Parameters
        {
            get
            {
                yield return new KeyValuePair<string, object>("@FileName", _fileName);
                yield return new KeyValuePair<string, object>("@RawMessage", _rawMessage);
                yield return new KeyValuePair<string, object>("@ProcessingStatus", 0);
                yield return new KeyValuePair<string, object>("@DownloadedTime", DateTime.UtcNow);
                yield return new KeyValuePair<string, object>("@SentTime", DateTime.UtcNow);
                yield return new KeyValuePair<string, object>("@MailboxName", _receivingMailbox);
                yield return new KeyValuePair<string, object>("@MessageType", _messageType);
                yield return new KeyValuePair<string, object>("@SenderMailbox", _senderMailbox);
                yield return new KeyValuePair<string, object>("@Priority", _priority);
            }
        }

        private string GetPriority()
        {
            var parts = Content[0].SplitLines(new[] { " " });
            if (parts.Count > 1)
                return parts.First().Trim().TrimStart((char)1);
            else
                return "QK";
        }

        private string FindReceivingMailbox()
        {
            var parts = Content[0].SplitLines(new[] { " " });
            if (parts.Count > 1)
                return parts.Last();
            else
                return parts.First().Trim().TrimStart((char)1);
        }

        private string FindSenderMailbox()
        {
            return Content[1].SplitLines(new[] { " " }).First().TrimStart('.');
        }
    }
}
