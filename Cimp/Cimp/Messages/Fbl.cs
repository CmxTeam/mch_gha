﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Cimp.Messages.MessageParts;
using Cimp.Messages.MessageParts.Fbl;

namespace Cimp.Messages
{
    public class Fbl : Message
    {
        #region [-- Private Fields --]

        private readonly List<UnloadingPort> _unloadingPorts;
        private readonly FlightIdAndPointOfLoading _flightIdAndPointOfLoading;

        private string _currentUnloadingInfo;
        private int _trancate;

        #endregion

        #region [-- Constructors --]
        public Fbl(string messageContent, string connectionString, string senderMailBox)
            : base(messageContent, connectionString, senderMailBox)
        {
            try
            {
                _flightIdAndPointOfLoading = new FlightIdAndPointOfLoading(Content[1], Content[2]);
                _unloadingPorts = new List<UnloadingPort>();
                InitializeDetails(Content.Skip(2).ToList());
                IsInitialisedSuccessfully = true;
            }
            catch (Exception e)
            {
                IsInitialisedSuccessfully = false;
                InitialiserErrorMessage = e.Message;
            }
        }
        #endregion

        #region [-- Initializers --]

        private void InitializeDetails(IEnumerable<string> lines)
        {
            var index = 0;
            foreach (var line in lines)
            {
                if (line.StartsWith("/"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("ULD"))
                {
                    var lastAwb = GetLastFblAwb();
                    int total;
                    lastAwb.Ulds.AddRange(Uld.GetUldDescriptionFbls(line, out total));
                    lastAwb.TotalUlds = total;
                    ++index;
                    continue;
                }

                if (line.StartsWith("DIM"))
                {
                    var lastAwb = GetLastFblAwb();
                    var awbPrefix = lastAwb.AirlinePrefix;
                    var awbNumber = lastAwb.AwbSerialNumber;
                    lastAwb.Dimensions.Add(new DimensionsInformation(awbPrefix, awbNumber, line));

                    //Following DIM line can be additional DIM lines, or AWB origin info line. 
                    // Need to understand what type of line that is. Need to deal with last line in list.
                    //The rest, if any, will be dims, as only 1 origin line possible.
                    var derLines = TryFindDerivativeLines(index - _trancate, lines.Skip(_trancate)).ToList();

                    if (derLines.Count > 0)
                    {
                        for (int i = 0; i < derLines.Count - 1; i++)
                        {
                            lastAwb.Dimensions.Add(new DimensionsInformation(awbPrefix, awbNumber, derLines[i]));
                        }
                        lastAwb.AddDimOrOrirginInfo(derLines.Last());
                    }

                    ++index;
                    continue;
                }

                if (line.StartsWith("LAST"))
                {
                    _flightIdAndPointOfLoading.IsComplete = true;
                    ++index;
                    continue;
                }

                if (line.StartsWith("CONT"))
                {
                    _flightIdAndPointOfLoading.IsComplete = false;
                    ++index;
                    continue;
                }

                if (line.StartsWith("OSI"))
                {
                    var lastAwb = GetLastFblAwb();
                    lastAwb.OtherServiceInfo1 = line;
                    lastAwb.OtherServiceInfo2 = TryFindDerivativeLines(index - _trancate, lines.Skip(_trancate)).FirstOrDefault();
                    ++index;
                    continue;
                }

                if (line.StartsWith("SSR"))
                {
                    var lastAwb = GetLastFblAwb();
                    lastAwb.SpecialServiceRequest1 = line;
                    lastAwb.SpecialServiceRequest2 = TryFindDerivativeLines(index - _trancate, lines.Skip(_trancate)).FirstOrDefault();
                    ++index;
                    continue;
                }

                if (Regex.IsMatch(line, "^[A-Za-z]{3}"))
                {
                    var port = new UnloadingPort(line);
                    _unloadingPorts.Add(port);
                    _trancate = index;
                    ++index;
                    continue;
                }

                var awb = new Awb(line);
                _unloadingPorts.Last().Awbs.Add(awb);
                ++index;
            }
        }


        private static IEnumerable<string> TryFindDerivativeLines(int index, IEnumerable<string> lines)
        {
            return lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/"));
        }

        private Awb GetLastFblAwb()
        {
            return _unloadingPorts.Last().Awbs.Last();
        }

        #endregion

        #region [ -- Overriden Methods -- ]
        protected override string StoredProcedureName
        {
            get { return "ProcessFbl"; }
        }

        protected override IEnumerable<KeyValuePair<string, object>> Parameters
        {
            get
            {
                yield return new KeyValuePair<string, object>("@MessageSequenceNumber", _flightIdAndPointOfLoading.MessageSequenceNumber);
                yield return new KeyValuePair<string, object>("@IsComplete", _flightIdAndPointOfLoading.IsComplete);
                yield return new KeyValuePair<string, object>("@CarrierCode", _flightIdAndPointOfLoading.CarrierCode);
                yield return new KeyValuePair<string, object>("@FlightNumber", _flightIdAndPointOfLoading.FlightNumber);
                yield return new KeyValuePair<string, object>("@Etd", _flightIdAndPointOfLoading.ScheduleOfLoading ?? (object)DBNull.Value);
                yield return new KeyValuePair<string, object>("@LoadingAirportCode", _flightIdAndPointOfLoading.AirportCodeOfLoading);
                yield return new KeyValuePair<string, object>("@AircraftRegistration", _flightIdAndPointOfLoading.AircraftRegistration ?? (object)DBNull.Value);
                yield return new KeyValuePair<string, object>("@TotalPieces", _unloadingPorts.Sum(p => p.Awbs.Sum(u => u.QuantityNummberOfPieces)));
                yield return new KeyValuePair<string, object>("@TotalAwbs", _unloadingPorts.Sum(p => p.Awbs.Count()));
                yield return new KeyValuePair<string, object>("@TotalUlds", _unloadingPorts.Sum(p => p.Awbs.Sum(u => u.Ulds.Count)));
                yield return new KeyValuePair<string, object>("@SenderMailbox", SenderMailBox);
                yield return new KeyValuePair<string, object>("@XmlContent", ConvertToXml<UnloadingPort>(_unloadingPorts));
            }
        }
        #endregion
    }
}
