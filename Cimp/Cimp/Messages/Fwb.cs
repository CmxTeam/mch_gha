﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Cimp.Messages.MessageParts;
using Cimp.Messages.MessageParts.Fwb;

namespace Cimp.Messages
{
    public class Fwb : Message
    {
        #region [-- Private Fields --]

        private Awb _awb;

        private string _currentUnloadingInfo;
        private int _trancate;

        #endregion

        #region [ -- Constructors -- ]
        public Fwb(string messageContent, string connectionString, string senderMailBox)
            : base(messageContent, connectionString, senderMailBox)
        {
            try
            {
                _awb = new Awb(Content[1]);
                InitializeDetails(Content.Skip(2).ToList());
                IsInitialisedSuccessfully = true;
            }
            catch (Exception e)
            {
                IsInitialisedSuccessfully = false;
                InitialiserErrorMessage = e.Message;
            }
        }
        #endregion


        #region [-- Initializers --]

        private void InitializeDetails(IEnumerable<string> lines)
        {
            var index = 0;
            foreach (var line in lines)
            {
                if (line.StartsWith("/"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("FLT"))
                {
                    _awb.LegSegments.AddRange(LegSegment.CerateLegSegments(line));
                    ++index;
                    continue;
                }

                if (line.StartsWith("RTG"))
                {
                    LegSegment.ProcessLegSegments(_awb.LegSegments, line);
                    ++index;
                    continue;
                }

                if (line.StartsWith("SHP"))
                {
                    var shipperLines = new List<string>() { line };
                    shipperLines.AddRange(TryFindDerivativeLines(index, lines));
                    _awb.Shipper = new Partner(shipperLines);
                    ++index;
                    continue;
                }

                if (line.StartsWith("CNE"))
                {
                    var consigneeLines = new List<string>() { line };
                    consigneeLines.AddRange(TryFindDerivativeLines(index, lines));
                    _awb.Consignee = new Partner(consigneeLines);
                    ++index;
                    continue;
                }

                if (line.StartsWith("AGT"))
                {
                    var agentLines = new List<string>() { line };
                    agentLines.AddRange(TryFindDerivativeLines(index, lines));
                    _awb.AwbAgent = new Agent(agentLines);
                    ++index;
                    continue;
                }

                if (line.StartsWith("SSR"))
                {
                    var specialLines = new List<string>() { line };
                    specialLines.AddRange(TryFindDerivativeLines(index, lines));
                    _awb.TryAddSpecialServicesRequests(specialLines);
                    ++index;
                    continue;
                }

                if (line.StartsWith("NFY"))
                {
                    var alsoNotifyLines = new List<string>() { line };
                    alsoNotifyLines.AddRange(TryFindDerivativeLines(index, lines));
                    _awb.AlsoNotify = Partner.GetAlsoNotifyPartner(alsoNotifyLines);
                    ++index;
                    continue;
                }

                if (line.StartsWith("ACC"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("CVD"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("RTD"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("OTH"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("PPD"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("COL"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("CER"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("ISU"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("OSI"))
                {
                    var otherLines = new List<string>() { line };
                    otherLines.AddRange(TryFindDerivativeLines(index, lines));
                    _awb.TryAddOtherServiceInformations(otherLines);
                    ++index;
                    continue;
                }

                if (line.StartsWith("CDC"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("REF"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("COR"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("COI"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("SII"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("ARD"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("SPH"))
                {
                    _awb.TryAddSpecialHandlingCodes(line);
                    ++index;
                    continue;
                }

                if (line.StartsWith("NOM"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("SRI"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("OPI"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("OCI"))
                {
                    ++index;
                    continue;
                }
            }
        }
        #endregion

        private static IEnumerable<string> TryFindDerivativeLines(int index, IEnumerable<string> lines)
        {
            return lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/"));
        }


        #region [ -- Overriden Methods -- ]
        protected override string StoredProcedureName
        {
            get { return "ProcessFwb"; }
        }

        protected override IEnumerable<KeyValuePair<string, object>> Parameters
        {
            get
            {
                yield return new KeyValuePair<string, object>("@SenderMailbox", SenderMailBox);
                yield return new KeyValuePair<string, object>("@XmlContent", ConvertToXml(_awb));
            }
        }

        #endregion
    }
}
