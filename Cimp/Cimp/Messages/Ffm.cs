﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Cimp.Messages.MessageParts;
using System.Linq;
using Cimp.Messages.MessageParts.Fhl;

namespace Cimp.Messages
{
    public class Ffm : Message
    {
        #region [-- Private Fields --]

        private readonly string _station;
        private readonly List<UldDescription> _uldDescriptions;
        private readonly FlightIdAndPointOfLoading _flightIdAndPointOfLoading;

        private string _currentUnloadingInfo;
        private int _trancate;

        #endregion

        #region [-- Constructors --]

        public Ffm(string messageContent, string connectionString, string station, string senderMailBox)
            : base(messageContent, connectionString, senderMailBox)
        {
            try
            {
                _station = station;
                _currentUnloadingInfo = Content[2];
                _flightIdAndPointOfLoading = new FlightIdAndPointOfLoading(Content[1], Content[2]);
                _uldDescriptions = new List<UldDescription>();
                InitializeDetails(Content.Skip(3).ToList());
                IsInitialisedSuccessfully = true;
            }
            catch (Exception e)
            {
                IsInitialisedSuccessfully = false;
                InitialiserErrorMessage = e.Message;
            }
        }

        #endregion

        #region [-- Initializers --]

        private void InitializeDetails(IEnumerable<string> lines)
        {
            var index = 0;
            foreach (var line in lines)
            {
                if (line.StartsWith("/"))
                {
                    ++index;
                    continue;
                }

                if (line.StartsWith("ULD"))
                {
                    var uld = new UldDescription(line);
                    uld.MovementInfos.AddRange(TryFindUldMovementInfo(index - _trancate, lines.Skip(_trancate)).Select(info => new MovementInformation(info)));
                    uld.VolumeAvailableCode = TryFindUldPriorityCode(index - _trancate, lines.Skip(_trancate));
                    _uldDescriptions.Add(uld);
                    ++index;
                    continue;
                }

                if (line.StartsWith("LAST"))
                {
                    _flightIdAndPointOfLoading.IsComplete = true;
                    ++index;
                    continue;
                }

                if (line.StartsWith("CONT"))
                {
                    _flightIdAndPointOfLoading.IsComplete = false;
                    ++index;
                    continue;
                }

                if (line.StartsWith("OCI"))
                {
                    var lastAwb = GetLastConsignmentDetal();
                    lastAwb.OtherCustomsInformations.Add(new CustomsInformation(line));
                    lastAwb.OtherCustomsInformations.AddRange(TryFindDerivativeLines(index - _trancate, lines.Skip(_trancate)).Select(l => new CustomsInformation(l)));
                    ++index;
                    continue;
                }

                if (line.StartsWith("COR"))
                {
                    var lastAwb = GetLastConsignmentDetal();
                    lastAwb.CustomsOrigin = line;
                    ++index;
                    continue;
                }

                if (line.StartsWith("SCI"))
                {
                    var lastAwb = GetLastConsignmentDetal();
                    lastAwb.CustomsReference = line;
                    lastAwb.CustomsOrigin = line;
                    ++index;
                    continue;
                }

                if (line.StartsWith("OSI"))
                {
                    var lastAwb = GetLastConsignmentDetal();
                    lastAwb.OtherServiceInfo1 = line;
                    lastAwb.OtherServiceInfo2 = TryFindDerivativeLines(index - _trancate, lines.Skip(_trancate)).FirstOrDefault();
                    ++index;
                    continue;
                }

                if (line.StartsWith("DIM"))
                {
                    var lastAwb = GetLastConsignmentDetal();
                    var awbPrefix = lastAwb.AirlinePrefix;
                    var awbNumber = lastAwb.AwbSerialNumber;
                    lastAwb.DimensionsInformations.Add(new DimensionsInformation(awbPrefix, awbNumber, line));
                    ++index;
                    continue;
                }

                if (Regex.IsMatch(line, "^[A-Za-z]{3}"))
                {
                    _currentUnloadingInfo = line;
                    _trancate = index;
                    ++index;
                    continue;
                }

                var parentUld = TryFindUld(index - _trancate, lines.Skip(_trancate));
                var lastUld = _uldDescriptions.LastOrDefault(u => u.Loose == false);
                var awb = parentUld != null && parentUld.SerialNumber == lastUld.SerialNumber ?
                    new ConsignmentDetail(line, parentUld.SerialNumber, _currentUnloadingInfo) : new ConsignmentDetail(line, _currentUnloadingInfo);
                var specialHandlingCode = TryFindSpecialHandlingCodes(index - _trancate, lines.Skip(_trancate));
                awb.MovementInfos.AddRange(TryFindAwbMovementInfo(index - _trancate, lines.Skip(_trancate)).Select(info => new MovementInformation(info)));
                awb.MovementPriorityCode = TryFindAwbPriorityCode(index - _trancate, lines.Skip(_trancate));
                if (specialHandlingCode != null)
                {
                    if (awb.SpecialHandlingCodes == null)
                        awb.SpecialHandlingCodes = new SpecialHandlingCode();
                    awb.SpecialHandlingCodes.AddLine(specialHandlingCode);
                }

                if (awb.UldSerialNumber == null)
                {
                    var looseUld = new UldDescription();
                    looseUld.ConsignmentDetails.Add(awb);
                    _uldDescriptions.Add(looseUld);
                }
                else
                    _uldDescriptions.Last(u => u.Loose == false).ConsignmentDetails.Add(awb);
                ++index;
            }
        }

        #endregion

        #region [ -- Overriden Methods -- ]

        protected override string StoredProcedureName
        {
            get { return "ProcessFlightSchedules"; }
        }

        protected override IEnumerable<KeyValuePair<string, object>> Parameters
        {
            get
            {
                yield return new KeyValuePair<string, object>("@Station", _station);
                yield return new KeyValuePair<string, object>("@MessageSequenceNumber", _flightIdAndPointOfLoading.MessageSequenceNumber);
                yield return new KeyValuePair<string, object>("@IsComplete", _flightIdAndPointOfLoading.IsComplete);
                yield return new KeyValuePair<string, object>("@CarrierCode", _flightIdAndPointOfLoading.CarrierCode);
                yield return new KeyValuePair<string, object>("@FlightNumber", _flightIdAndPointOfLoading.FlightNumber);
                yield return new KeyValuePair<string, object>("@Etd", _flightIdAndPointOfLoading.ScheduleOfLoading ?? (object)DBNull.Value);
                yield return new KeyValuePair<string, object>("@LoadingAirportCode", _flightIdAndPointOfLoading.AirportCodeOfLoading);
                yield return new KeyValuePair<string, object>("@AircraftRegistration", _flightIdAndPointOfLoading.AircraftRegistration ?? (object)DBNull.Value);
                yield return new KeyValuePair<string, object>("@FirstEta", _flightIdAndPointOfLoading.ScheduleOfArrival ?? (object)DBNull.Value);
                yield return new KeyValuePair<string, object>("@ArrivalAirportCode", _flightIdAndPointOfLoading.AirportCodeOfArrival ?? (object)DBNull.Value);
                yield return new KeyValuePair<string, object>("@IsoCountryCode", _flightIdAndPointOfLoading.IsoCountryCode ?? (object)DBNull.Value);
                yield return new KeyValuePair<string, object>("@TotalPieces", _uldDescriptions.Sum(u => u.ConsignmentDetails.Sum(c => c.QuantityNummberOfPieces)));
                yield return new KeyValuePair<string, object>("@TotalAwbs", _uldDescriptions.SelectMany(u => u.ConsignmentDetails.Select(c => c.AwbSerialNumber)).Distinct().Count());
                yield return new KeyValuePair<string, object>("@TotalUlds", _uldDescriptions.Count(u => !u.Loose) + (_uldDescriptions.Any(u => u.Loose) ? 1 : 0));
                yield return new KeyValuePair<string, object>("@SenderMailbox", SenderMailBox);
                yield return new KeyValuePair<string, object>("@XmlContent", ConvertToXml<UldDescription>(_uldDescriptions));
            }
        }

        #endregion

        #region [ -- Helpers -- ]

        private ConsignmentDetail GetLastConsignmentDetal()
        {
            return _uldDescriptions.SelectMany(u => u.ConsignmentDetails).ToList().Last();
        }

        private static IEnumerable<string> TryFindDerivativeLines(int index, IEnumerable<string> lines)
        {
            return lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/"));
        }

        private static string TryFindSpecialHandlingCodes(int index, IEnumerable<string> lines)
        {
            var result = lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/")).FirstOrDefault();
            if (result == null) return null;

            var parts = result.Split('/').Skip(1);
            if (parts.Count() <= 9 && parts.All(p => p.Length == 3))
                return result;

            return null;
        }

        private static string TryFindUldPriorityCode(int index, IEnumerable<string> lines)
        {
            return lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/")).FirstOrDefault(l => l.Length == 2);
        }

        private static string TryFindAwbPriorityCode(int index, IEnumerable<string> lines)
        {
            return lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/") || l.StartsWith("DIM")).FirstOrDefault(l => l.Length == 2);
        }

        private static IEnumerable<string> TryFindUldMovementInfo(int index, IEnumerable<string> lines)
        {
            return lines.Skip(index + 1).TakeWhile(l => l.StartsWith("/") && l.Length > 2);
        }

        private static IEnumerable<string> TryFindAwbMovementInfo(int index, IEnumerable<string> lines)
        {
            var shc = TryFindSpecialHandlingCodes(index, lines);
            if (shc != null)
                index = index + 1;
            return lines.Skip(index + 1).TakeWhile(l => (l.StartsWith("/") || l.StartsWith("DIM")) && l.Length > 2).Where(l => !l.StartsWith("DIM"));
        }

        private static UldDescription TryFindUld(int index, IEnumerable<string> lines)
        {
            var previousLines = lines.Take(index).Reverse();
            var uldLine = previousLines.FirstOrDefault(p => p.StartsWith("ULD"));
            return uldLine == null ? null : new UldDescription(uldLine);
        }

        #endregion
    }
}
