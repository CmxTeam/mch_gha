﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Cimp.Messages.MessageParts;
using System.Linq;

namespace Cimp.Messages
{
    public abstract class Message
    {        
        private readonly string[] _newLineSeparator = new[] { "\n" };

        protected Message(string messageContent, string connectionString, string senderMailBox)
        {            
            ConnecttionString = connectionString;
            Content = messageContent.SplitLinesAndRemoveEmptyEntries(_newLineSeparator);
            SenderMailBox = senderMailBox;
        }

        public bool Save(out string errorMessage)
        {
            try
            {
                Save();
                errorMessage = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return false;
            }
        }

        protected void Save()
        {            
            using (var connection = new SqlConnection(ConnecttionString))
            {
                using (var command = new SqlCommand(StoredProcedureName, connection))
                {
                    command.CommandType = CommandType;
                    foreach (var parameter in Parameters)                    
                        command.Parameters.Add(new SqlParameter(parameter.Key, parameter.Value ?? DBNull.Value));

                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        protected static string ConvertToXml<T>(IEnumerable<T> fields)
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            string xmlString;
            using (var memStream = new MemoryStream())
            {
                serializer.Serialize(memStream, fields);
                var encoding = Encoding.UTF8;
                var xmlBytes = new byte[memStream.Length];
                memStream.Seek(0, SeekOrigin.Begin);
                memStream.Read(xmlBytes, 0, Convert.ToInt32(memStream.Length));
                xmlString = encoding.GetString(xmlBytes);
            }
            return xmlString;
        }

        protected static string ConvertToXml<T>(T fields)
        {
            var serializer = new XmlSerializer(typeof(T));
            string xmlString;
            using (var memStream = new MemoryStream())
            {
                serializer.Serialize(memStream, fields);
                var encoding = Encoding.UTF8;
                var xmlBytes = new byte[memStream.Length];
                memStream.Seek(0, SeekOrigin.Begin);
                memStream.Read(xmlBytes, 0, Convert.ToInt32(memStream.Length));
                xmlString = encoding.GetString(xmlBytes);
            }
            return xmlString;
        }

        public virtual CommandType CommandType
        {
            get { return CommandType.StoredProcedure; }
        }

        public bool IsInitialisedSuccessfully { get; set; }
        public string InitialiserErrorMessage { get; set; }
        
        protected string ConnecttionString { get; private set; }
        protected IList<string> Content { get; private set; }
        protected string SenderMailBox { get; set; }
        protected abstract string StoredProcedureName { get; }
        protected abstract IEnumerable<KeyValuePair<string, object>> Parameters { get; }        
    }
}