﻿namespace Cimp
{
    public class ExecutionStatus
    {       
        public string Error { get; set; }
        public bool Status { get; set; }
    }
}